jQuery(function(){
  var toggles = jQuery('.toggle a'),
      codes = jQuery('.code');
  
  toggles.on("click", function(event){
    event.preventDefault();
    var jQuerythis = jQuery(this);
    
    if (!jQuerythis.hasClass("active")) {
      toggles.removeClass("active");
      jQuerythis.addClass("active");
      codes.hide().filter(this.hash).show();
    }
  });
  toggles.first().click();
});
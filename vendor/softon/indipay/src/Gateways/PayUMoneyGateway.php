<?php namespace Softon\Indipay\Gateways;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Softon\Indipay\Exceptions\IndipayParametersMissingException;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Session;
use Illuminate\Support\Facades\Redirect;

class PayUMoneyGateway implements PaymentGatewayInterface {

    protected $parameters = array();
    protected $testMode = false;
    protected $merchantKey = '';
    protected $salt = '';
    protected $hash = '';
    protected $liveEndPoint = 'https://secure.payu.in/_payment';
    protected $testEndPoint = 'https://test.payu.in/_payment';
    public $response = '';

    
    function __construct()
    {

     $merchant_fund_payment=session::get('merchnat_payment');
     $merchant_id=session::get('merchnat_payment_id');
     $nm_paymentsettings=DB::table('nm_paymentsettings')
        ->select('ps_payu_key','ps_payu_salt','ps_paypal_pay_mode')->get();
     foreach ($nm_paymentsettings as $value) {
       $this->merchantKey=$value->ps_payu_key;
       $this->salt= $value->ps_payu_salt;
       $ps_paypal_pay_mode= $value->ps_paypal_pay_mode;
       if($ps_paypal_pay_mode==0){ // false for Testing the Gateway [For production true]
        $pay_mode='true';
        }else{
        $pay_mode='false';
        }
      }

     if($merchant_fund_payment!=1){ // customer purchase
        if(Session::get('customerid')==''){
         return Redirect::to('checkout')->with('msg', 'Invalid Transaction! Need Proper Credentials!.');   
        }
        $this->merchantKey = $this->merchantKey;
        $this->salt = $this->salt;
        $this->testMode = $pay_mode; // false for Testing the Gateway [For production true]
        $this->parameters['key'] = $this->merchantKey;
        $this->parameters['txnid'] = $this->generateTransactionID();
        $this->parameters['surl'] = url('response_sucess');
        $this->parameters['furl'] = url('response_failed'); 
        $this->parameters['service_provider'] = ''; //required in live mode 
     } 
/*For merchant fund transfer*/
     elseif($merchant_fund_payment==1){
        if(Session::get('userid')==''){
         return Redirect::to('all_fund_request')->with('msg', 'Invalid Transaction! Need Proper Credentials!.');   
        }
        $payment_deatils=DB::table('nm_merchant')->select('mer_email','mer_phone','mer_fname','mer_payu_key','mer_payu_salt')->where('mer_id','=',$merchant_id)->where('mer_staus','!=',0)->get();
      foreach ($payment_deatils as $payment_deatils) {
             $this->merchantKey=$payment_deatils->mer_payu_key;
             $this->salt=$payment_deatils->mer_payu_salt;
             $merchant_fname=$payment_deatils->mer_fname;
             $merchant_email=$payment_deatils->mer_email;
             $merchant_phone=$payment_deatils->mer_phone;
        } 
        $this->merchantKey = $this->merchantKey;
        $this->salt = $this->salt;
        $this->testMode = $pay_mode; // false for Testing the Gateway [For production true]
        $this->parameters['key'] = $this->merchantKey;
        $this->parameters['txnid'] = $this->generateTransactionID(); 
        $this->parameters['surl'] = url('fund_payu_success');
        $this->parameters['furl'] = url('fund_payu_failed'); 
        $this->parameters['service_provider'] = ''; //required in live mode 

     }
    } 

    public function getEndPoint()
    {
        if($this->testMode=='true'){
          return $this->testEndPoint; 
        }else {
         return $this->liveEndPoint;
        }
    }

    public function request($parameters)
    {

        $this->parameters = array_merge($this->parameters,$parameters);

        $this->checkParameters($this->parameters);

        $this->encrypt();

        return $this;

    }

    /**
     * @return mixed
     */
    public function send()
    {
//echo $this->getEndPoint(); exit;
        Log::info('Indipay Payment Request Initiated: ');
        return View::make('indipay::payumoney')->with('hash',$this->hash)
                             ->with('parameters',$this->parameters)
                             ->with('endPoint',$this->getEndPoint());

    }


    /**
     * Check Response
     * @param $request
     * @return array
     */
    public function response($request)
    {
        $response = $request->all();

        $response_hash = $this->decrypt($response);

        if($response_hash!=$response['hash']){
            return 'Hash Mismatch Error';
        }

        return $response;
    }


    /**
     * @param $parameters
     * @throws IndipayParametersMissingException
     */
    public function checkParameters($parameters)
    {
      
        $validator = Validator::make($parameters, [
            'key' => 'required',
            'txnid' => 'required',
            'surl' => 'required|url',
            'furl' => 'required|url',
            'firstname' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'productinfo' => 'required',
      //      'service_provider' => 'required',
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {

            return Redirect::back()->with('msg', 'Invalid Transaction! Need Proper Credentials!.');

            //throw new IndipayParametersMissingException;
        }

    }

    /**
     * PayUMoney Encrypt Function
     *
     */
    protected function encrypt()
    {
        $this->hash = '';
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';

        foreach($hashVarsSeq as $hash_var) {
            $hash_string .= isset($this->parameters[$hash_var]) ? $this->parameters[$hash_var] : '';
            $hash_string .= '|';
        }

       
         $hash_string .= $this->salt;
        $this->hash = strtolower(hash('sha512', $hash_string));
       
    }

    /**
     * PayUMoney Decrypt Function
     *
     * @param $plainText
     * @param $key
     * @return string
     */
    protected function decrypt($response)
    {

        $hashSequence = "status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key";
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = $this->salt."|";

        foreach($hashVarsSeq as $hash_var) {
            $hash_string .= isset($response[$hash_var]) ? $response[$hash_var] : '';
            $hash_string .= '|';
        }

        $hash_string = trim($hash_string,'|');

        return strtolower(hash('sha512', $hash_string));
    }



    public function generateTransactionID()
    {
        return substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    }




}
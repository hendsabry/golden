<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Excel;
use Session;
use Lang;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\AdminModel;
use App\ProductReview;
use App\Category;
use App\AdminMenu;
use App\AdminUserGroup;
use App\User;
use App\Products;
use App\Merchant;


class ReviewController extends Controller {
    public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }       

    public function index() {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }

        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();//Login user detail detail

        $users = ProductReview::orderBy('review_date','desc')->with('getUser')->with('getCategory')->with('getWorker')->with('getProduct')->paginate(10);
        //get review data 
        foreach ($users as $key => $value) {
        $value['review_date'] = date('M d, Y', strtotime($value->review_date));
        }

        $category =  Category::select('mc_id')->where('parent_id','=',0)->where('mc_status','=',1)->get()->toArray();
        $parent_id = array();
        foreach ($category as $key => $value) {
        $parent_id[] = $value['mc_id'];
        }
        $sub_category = Category::select('mc_id')->whereIn('parent_id',$parent_id)->where('mc_status','=',1)->get()->toArray();
        $sub_category_id = array();
        foreach ($sub_category as $key => $value) {
        $sub_category_id[] = $value['mc_id'];
        }
        $vendors = Category::whereIn('parent_id',$sub_category_id)->where('mc_status','=',1)->get();
        //get verndors
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.review_rating.review_rating_list',compact('users'))->with("routemenu",$session_message)->with('user',$user)->with('vendors',$vendors)->with('admin_menu',$admin_menu);;
        }else {
        return Redirect::to('siteadmin');
        } 
    }


    public function deleteRating($id){
        ProductReview::where('comment_id','=',$id)->delete();//delete review and rating 
        return Redirect::to('/admin/review');
    }

    public function changeStatus(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        $dta= array();
        $dta['status'] = $status;
        ProductReview::where('comment_id','=',$id)->update($dta); // change status of review and rating 
         $product = ProductReview::where('comment_id','=',$id)->with('getUser')->with('getCategory')->with('getWorker')->with('getProduct')->first();
         if ( isset($product->getUser[0]) && $product->getUser[0] != '') {
        $sub_ad = User::where('cus_id','=',$product->customer_id)->first();
        $email = $sub_ad->email;
        $baseurl = url('/') ;
        if (isset($sub_ad->cus_name)) {
        $username = $sub_ad->cus_name;
        }else{
        $username = '';
        }
        if (isset($sub_ad->gender)) {
        if($sub_ad->gender == '0'){
        $respect = 'Mr';
        }else if ($sub_ad->gender == 'female') {
        $respect = 'Ms';
        }else{
        $respect = '';
        }
        }else{
        $respect = '';
        }
        $this->confirmation_user_email = $email;
        $this->confirmation_user_name = "Golden Cage";
        $status = '';
        if ($product->status == 1) {
          $subject = 'Your Rating is Successfully Approval ! Golden Cage'; 
                  $status = 'Approved';
        }else{
            $subject = 'Your Rating is Dis-Approval ! Golden Cage'; 
            $status = 'Dis-Approved';
        }

        if (isset($product->ratings)) {
           $ratings = $product->ratings;
        }else{
             $ratings = '';
        }
        $products_name = '';
        $products_name_ar = '';
         $vendor_name = '';

        if (isset($product->product_id)) {
           $products = Products::where('pro_id','=',$product->product_id)->first();
           if (isset($products)) {
              $products_name = $products->pro_title;
              $products_name_ar = $products->pro_title_ar;
           }
          $mer =  Merchant::where('mer_id','=',$products->pro_mc_id)->first();
          if (isset($mer)) {
            $vendor_name = $mer->mer_fname.' '.$mer->mer_lname;
          }else{
            $vendor_name = '';
          }
          
        }
        
        Mail::send('admin.mail.review_user',
        array(
        'user_name' => $username,
        'user_email' => $email,
        'site_url' => $baseurl,
        'status' => $status,
        'respect' => $respect,
        'vendor_name' => $vendor_name,
        'ratings' => $ratings,
        'products_name' => $products_name,
        'products_name_ar' => $products_name_ar
        ), function($message) use($subject)
        {
        $message->to($this->confirmation_user_email,$this->confirmation_user_name)->subject($subject);
        });
         }
        return 1 ;
    }


    public function viewReview($id) {

        if (Session::has('userid')) {

        if(Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') 
        {
        $session_message = trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING');

        }
        else 
        {
        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING');
        }


        $user =  AdminModel::where('adm_id','=',Session::get('userid'))->with('getCity')->with('getCountry')->first();

        $review = ProductReview::where('comment_id','=',$id)->where('status','=',1)->with('getUser')->with('getCategory')->with('getWorker')->with('getProduct')->first();
        $groups =  AdminUserGroup::where('id','=',Session::get('access_group_id'))->first();
        $group =  explode(",",$groups->group_permission);
        $admin_menu = AdminMenu::whereIn('id',$group)->where('status','=',1)->get();
        return view('admin.review_rating.review_view')->with("routemenu",$session_message)->with('user',$user)->with('review',$review)->with('admin_menu',$admin_menu);
        }else {
        return Redirect::to('siteadmin');
        } 
    }

    public function searchData(Request $request)
    {
        $created_date = trim($request->get('created_date'));
        if($created_date != '')
        {
        $created_date = date("Y-m-d", strtotime($created_date));
        }else{
        $created_date = '';
        }   
        $status  = trim($request->get('status'));
        $rating  = trim($request->get('rating'));
        $vendor  = trim($request->get('vendor'));
        if($status != "" &&  $rating != '' &&  $vendor != '' &&  $created_date != ''){
        $users = ProductReview::where('status','=',$status)->where('ratings','=',$rating)->where('shop_id','=',$vendor)->where('review_date','like',$created_date.'%')->orderBy('review_date','desc')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status == "" &&  $rating != '' &&  $vendor != '' &&  $created_date != '') {
        $users = ProductReview::where('ratings','=',$rating)->where('shop_id','=',$vendor)->where('review_date','like',$created_date.'%')->orderBy('review_date','desc')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status == "" &&  $rating == '' &&  $vendor != '' &&  $created_date != '') {
        $users = ProductReview::where('shop_id','=',$vendor)->where('review_date','like',$created_date.'%')->orderBy('review_date','desc')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status == "" &&  $rating == '' &&  $vendor != '' &&  $created_date == '') {
        $users = ProductReview::where('shop_id','=',$vendor)->orderBy('review_date','desc')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status == "" &&  $rating == '' &&  $vendor == '' &&  $created_date == '') {
        $users = ProductReview::orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }elseif ($status != "" &&  $rating == '' &&  $vendor == '' &&  $created_date == '') {
        $users = ProductReview::where('status','=',$status)->orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }elseif ($status == "" &&  $rating != '' &&  $vendor == '' &&  $created_date == '') {
        $users = ProductReview::where('ratings','=',$rating)->orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }elseif ($status != "" &&  $rating != '' &&  $vendor != '' &&  $created_date == '') {
        $users = ProductReview::where('status','=',$status)->where('ratings','=',$rating)->where('shop_id','=',$vendor)->orderBy('review_date','desc')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status != "" &&  $rating != '' &&  $vendor == '' &&  $created_date != '') {
        $users = ProductReview::where('status','=',$status)->where('ratings','=',$rating)->where('review_date','like',$created_date.'%')->orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }elseif ($status != "" &&  $rating != '' &&  $vendor == '' &&  $created_date == '') {
        $users = ProductReview::where('status','=',$status)->where('ratings','=',$rating)->orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }elseif ($status != "" &&  $rating == '' &&  $vendor != '' &&  $created_date != '') {
        $users = ProductReview::where('status','=',$status)->orderBy('review_date','desc')->with('getCategory')->where('shop_id','=',$vendor)->where('review_date','like',$created_date.'%')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status != "" &&  $rating == '' &&  $vendor != '' &&  $created_date == '') {
        $users = ProductReview::where('status','=',$status)->where('shop_id','=',$vendor)->orderBy('review_date','desc')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status != "" &&  $rating == '' &&  $vendor == '' &&  $created_date != '') {
        $users = ProductReview::where('status','=',$status)->where('review_date','like',$created_date.'%')->orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }elseif ($status == "" &&  $rating != '' &&  $vendor != '' &&  $created_date == '') {
        $users = ProductReview::where('ratings','=',$rating)->where('shop_id','=',$vendor)->orderBy('review_date','desc')->with('getCategory')->with('getUser')->paginate(10);
        }elseif ($status == "" &&  $rating != '' &&  $vendor == '' &&  $created_date != '') {
        $users = ProductReview::where('ratings','=',$rating)->where('review_date','like',$created_date.'%')->orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }elseif ($status == "" &&  $rating == '' &&  $vendor == '' &&  $created_date != '') {
        $users = ProductReview::where('review_date','like',$created_date.'%')->orderBy('review_date','desc')->with('getCategory')->with('getUser')->with('getWorker')->with('getProduct')->paginate(10);
        }else{
        $users = ProductReview::with('getUser')->orderBy('review_date','desc')->with('getCategory')->with('getWorker')->with('getProduct')->paginate(10);
        }

        foreach ($users as $key => $value) {
        $value['review_date'] = date('M d, Y', strtotime($value->review_date));
        }
        return compact('users'); 
    }

}
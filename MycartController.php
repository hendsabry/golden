<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//
use App\BusinessOccasionType;
use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;

use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Internalfooditems;
use App\ProductCategory;
use App\CartExternalFoodDish;
//------------wisitech code end -----------//
class MycartController extends Controller
{  
    public function __construct(){
        parent::__construct();
		
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function savecartitem(Request $request)
    {
    	
         if(!Session::has('customerdata.token')) 
                {
            return Redirect::to('login-signup');
            }  
    
        if(Session::get('searchdata.basecategoryid')!=''){
        $basecategory_id=Session::get('searchdata.basecategoryid');
        }else {
        $basecategory_id=Session::get('searchdata.maincategoryid');
        }
    $cart_fooddateitem=array();
    $cart_fooddessertitem=array();
    $internal_foods=array();
        $userid=Session::get('customerdata.user_id');
        $productsessionid=Session::get('searchdata.prodid');
         $productid=$productsessionid[0];
        $lang=Session::get('lang_file');
        $basecategoryid=Session::get('searchdata.basecategoryid');
        $cid=Session::get('searchdata.catgeoryid');
        $categoryid=$cid[0];
        $scid=Session::get('searchdata.typeofhallid');
        $subcategoryid=$scid[0];
        $shid=Session::get('searchdata.shopid');
        $shopid=$shid[0];
        $bid=Session::get('searchdata.branchid');
        $branchid=$bid[0];
        
        $adoptedservices=Session::get('searchdata.hallchoosenservices');
        
         $occasiontype=Session::get('searchdata.weddingoccasiontype');
        
        
        $ProductCategoryselected= new ProductCategory();
        $eventtype=$ProductCategoryselected->eventtypeoption($occasiontype,$lang);
          $otype=$eventtype[0]->title;
           
           /////////////// get cart data from db ///////////////
                
                
            ////////////////// Hall product info ////////////
                
                
                  $cart = Cart::where('user_id', '=', $userid)->first();
                $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->first();
                $hall = Products::select('pro_id','pro_mc_id')->where('pro_id', '=', $cart_pro->product_id)->first();

                $productid=$cart_pro->product_id;
                $mkbasecatgory='';
                ////////////////// product info ////////////
             $ProductCategoryselected= new ProductCategory();
           $productinfo=$ProductCategoryselected->productinfo($productid,$lang);
          //print_r($productinfo);
          //die;
           if($productinfo[0]->pro_disprice==''){
             $newproductprice=$productinfo[0]->pro_price;
           }else{
                $newproductprice=$productinfo[0]->pro_disprice;
           }

                $branch_city = Category::select('mc_id','city_id')->where('mc_id', '=', $hall->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                if (!empty($cart) && $cart_pro->cart_type = 'hall') {
                    $productservices=array();
                    $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'hall')->with('getProductOptionValue')->get();
                    //echo "<pre>";
                   //print_r($cart_option_value);
                    //die;
                    $productservices=$cart_option_value;
                    $internal_food = CartInternalFoodDish::where('cart_id', '=', $cart->id)->with('getServiceAttribute')->with('getFood')->with('getContainer')->get();

                    $hall = CartProduct::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'hall')->with('getHall')->get();
                    $cart_service = array();
                    $internal_foods = array();
                    $hall_data = array();
                   
                    if (isset($cart_option_value)) { //get service
                        foreach($cart_option_value as $key => $value) {
                            if ($lang=='en_lang') {
                                $cart_service[$key]['option_title'] = $value->getProductOption['option_title'];
                            } else {
                                $cart_service[$key]['option_title'] = $value->getProductOption['option_title_ar'];
                            }

                            $cart_service[$key]['price'] = $value->getProductOption['price'];
                            $cart_service[$key]['no_person'] = $value->getProductOption['no_person'];
                            $cart_service[$key]['status'] = $value->getProductOption['status'];
                        }
                        
                    }
//print_r($cart_service);
                    if (isset($internal_food)) {
                        foreach($internal_food as $key => $value) { //get internal food

                        $container_price = InternalFoodContainerPrice::where('dish_id', '=', $value->internal_food_dish_id)->where('container_id','=',$value->container_id)->first();    
                          
                            if ($lang=='en_lang') {
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title;
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            } else {
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title_ar;
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name_ar;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title_ar;
                            }
                            $internal_foods[$key]['dish_id'] = $value->getFood[0]->id;
                            $internal_foods[$key]['quantity'] = $internal_food[$key]->quantity;
                            $internal_foods[$key]['container_price'] = $container_price->container_price;
                            $internal_foods[$key]['dish_image'] = $value->getFood[0]->dish_image;
                            $internal_foods[$key]['status'] = $value->getFood[0]->status;

                            $internal_foods[$key]['no_people'] = $value->getContainer[0]->no_people;
                            
                           if(!empty($container_price)) {
                         
                                 $internal_foods[$key]['final_price'] = $container_price->container_price * $internal_food[$key]->quantity;

                           } 
                            
                        }
                    }
                
                    if (isset($hall)) {
                        foreach($hall as $key => $value) { //get hall
                            if ($lang=='en_lang') {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name;
                            } else {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title_ar;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc_ar;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name_ar;
                            }
                            $hall_data[$key]['price'] = $value->getHall[0]->pro_price;
                            $hall_data[$key]['pro_qty'] = $value->getHall[0]->pro_qty;
                            $hall_data[$key]['pro_Img'] = $value->getHall[0]->pro_Img;
                            $hall_data[$key]['pro_status'] = $value->getHall[0]->pro_status;
                        }
                    }
                    
                    
                } 

           
                /////////////////////// end Hall cart data from db ////////////  
                  
                  
                   
             ////////////////////////// food cart info start here//////////


                        $cart_food = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('cart_id', '=', $cart->id)
                        ->where('cart_sub_type', '=', 'buffet')->count();
                    //echo $cart_food;
                    if($cart_food>0){
                           //$cart_fooditem = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('cart_id', '=', $cart->id)->where('cart_sub_type', '=', 'buffet')->get();
                           $cart_fooditem = CartProduct::where('cart_type','food')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status')->where('cart_sub_type','buffet')->where('cart_sub_type','buffet')->get();
                          // echo "<pre>";
                           //dd($cart_fooditem);
                    if(count($cart_fooditem) > 0)
                    {
                       foreach($cart_fooditem as $cartproduct) 
                       {
                         $product_id=$cartproduct->getProduct[0]->pro_id;
                            $cart_food_dish = CartExternalFoodDish::where('cart_id', $cart->id)->where('external_food_dish_id', $product_id)->get();
                             $cartproduct->cart_external_food_dish = $cart_food_dish;
                             foreach($cart_food_dish as $getbuffetcontainer) 
                             {                                    
                                    $shopinfo = DB::table('nm_category')->select('mc_id','mc_name','mc_name_ar')->where('mc_id', '=', $getbuffetcontainer->category_id)->get();
                                    $cartproduct->branch_info = $shopinfo;
                                    $container_price = DB::table('nm_product_option_value')->select('id', 'option_title', 'short_name')->where('id', '=', $getbuffetcontainer->container_id)->get();
                                    $cartproduct->dish_container_info = $container_price;
                             }
                       }
                      }
                      else
                      {
                        $cart_fooditem = array();
                      }
 
                    }
            /////////////////// food cart info ends here ////////////////

            //////////////////////////// dates cart start here///////////////

                         $cart_datefood = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('cart_sub_type', '=', 'dates')->count();

                            if($cart_datefood>0){
                          
                           $cart_fooddateitem = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status')->where('cart_sub_type', '=', 'dates')->where('cart_sub_type', '=', 'dates')->get();
                          
                       foreach ($cart_fooddateitem as $cartdateproduct) {
                          $product_id=$cartdateproduct->getProduct[0]->pro_id;
                           $cart_food_datesdish = CartServiceAttribute::where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                             $cartdateproduct->cart_date_food_dish = $cart_food_datesdish;

                             
                             $dateshopby_price = DB::table('nm_cart_option_value')->where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                                    $cartdateproduct->cart_option_value = $dateshopby_price;

                             foreach ($cart_food_datesdish as $getdatescontainer) {
                                    
                                    $dateshopinfo = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar')->where('mc_id', '=', $getdatescontainer->category_id)->get();
                                    $cartdateproduct->shop_info = $dateshopinfo;

                                    


                             }
                       }
 
                    }
       
                    //////////////////////// dates cart end here///////////

 //////////////////////////// dessert cart start here///////////////
                    


                        $cart_dessertfood = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->where('cart_sub_type', '=', 'dessert')->count();

                            if($cart_dessertfood>0){
                          
                           $cart_fooddessertitem = CartProduct::where('cart_type', '=', 'food')->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status')->where('cart_sub_type', '=', 'dessert')->where('cart_sub_type', '=', 'dessert')->get();
                          
                       foreach ($cart_fooddessertitem as $cartdessertproduct) {
                          $product_id=$cartdessertproduct->getProduct[0]->pro_id;
                           $cart_food_dessertsdish = CartServiceAttribute::where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                             $cartdessertproduct->cart_dessert_food_dish = $cart_food_dessertsdish;

                             
                             $dessertshopby_price = DB::table('nm_cart_option_value')->where('cart_id', $cart->id)->where('product_id', $product_id)->get();
                                    $cartdessertproduct->cart_option_value = $dessertshopby_price;

                             foreach ($cart_food_dessertsdish as $getdessertcontainer) {
                                    
                                    $dessertshopinfo = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar')->where('mc_id', '=', $getdessertcontainer->category_id)->get();
                                    $cartdessertproduct->shop_info = $dessertshopinfo;

                                    


                             }
                       }
 
                    }
//////////////////////////// dessert cart end here///////////////
           
//////////////////////////// Tailor cart start here///////////////      
$countCart = CartProduct::where('cart_type','tailor')->where('cart_id',$cart->id)->where('cart_sub_type','shopping')->count();
if($countCart>0)
{
    $lang            = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $carttailortitem = CartProduct::where('cart_type','tailor')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->where('cart_sub_type','shopping')->get();
    }
    else
    {
      $carttailortitem = CartProduct::where('cart_type','tailor')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->where('cart_sub_type','shopping')->get();
    } 
    
    foreach($carttailortitem as $carttailorproduct) 
    {
      $product_id = $carttailorproduct->getProduct[0]->pro_id;                      
      $mment = DB::table('nm_cart_body_measurement')->where('cart_id',$cart->id)->where('product_id',$product_id)->get();   
      $carttailorproduct->body_measurement = $mment;            
    }
}
//////////////////////////// Tailor cart end here///////////////

//////////////////////////// Abaya cart start here///////////////      
$countCart = CartProduct::where('cart_type','abaya')->where('cart_id',$cart->id)->where('cart_sub_type','shopping')->count();
if($countCart>0)
{
    $lang            = Session::get('lang_file');
    if(isset($lang) && $lang == 'ar_lang')
    {
      $cartabayatitem = CartProduct::where('cart_type','abaya')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_price,pro_desc_ar as pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->where('cart_sub_type','shopping')->get();
    }
    else
    {
      $cartabayatitem = CartProduct::where('cart_type','abaya')->where('cart_id',$cart->id)->with('getProduct:pro_id,pro_title,pro_price,pro_desc,pro_qty,pro_Img,attribute_id,pro_status,pro_mr_id')->where('cart_sub_type','shopping')->get();
    } 
    
    foreach($cartabayatitem as $cartabayaproduct) 
    {
      $product_id = $cartabayaproduct->getProduct[0]->pro_id;                      
      $mment = DB::table('nm_cart_body_measurement')->where('cart_id',$cart->id)->where('product_id',$product_id)->get();   
      $cartabayaproduct->body_measurement = $mment;            
    }
}
//////////////////////////// Abaya cart end here///////////////         
          
                
return view('mycart', compact('mainmenuitemtype','mainmenuwithItemAndContainer','category_id','containerlistitem','mkbasecatgory','productinfo','newproductprice','productservices','otype','cart_service','internal_foods','hall_data','cart_fooditem','cart_fooddateitem','cart_fooddessertitem','carttailortitem','cartabayatitem'));
 





   }
}

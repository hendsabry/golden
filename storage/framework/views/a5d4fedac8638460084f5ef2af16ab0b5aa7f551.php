<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="myaccount_right">
      <div class="dash_select"> <a href="<?php echo e(route('my-account-ocassion')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.BACK_TO_OCCASION_LIST')!= '')  ?  trans(Session::get('lang_file').'.BACK_TO_OCCASION_LIST'): trans($OUR_LANGUAGE.'.BACK_TO_OCCASION_LIST')); ?></a> </div>
      <div class="field_group top_spacing_margin_occas">
        <div class="main_user">
		  <?php 
		  //echo '<pre>';print_r($productdetails);die;
		  if(!count($productdetails)<1){?>
          <div class="types_ocss">
            <div class="types_ocs_left">
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')); ?>: </div>
                <div class="wed_text">
                <?php
				if(isset($productdetails[0]->search_occasion_id) && $productdetails[0]->search_occasion_id!=''){
                $setTitle = Helper::getOccasionName($productdetails[0]->search_occasion_id);
                ?>
                <?php $mc_name='title'?>
                <?php if(Session::get('lang_file')!='en_lang'): ?>
                <?php $mc_name= 'title_ar'; ?>
                <?php endif; ?>
                <?php echo $setTitle->$mc_name; 
				}
				else
				{
					 if(isset($productdetails[0]) && $productdetails[0]!='0')
					 { 
						 if(Session::get('lang_file')!='en_lang')
						 {
						   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
						 }
						 else
						 {
							$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
						 } 
						 foreach($getArrayOfOcc as $key=>$ocval)
						 {
						  if($productdetails[0]->main_occasion_id==$key)
						  {
						   $occasion_name = $ocval;
						  }
						 }				  
				       echo $occasion_name;
				   }
				}
				?> 
                </div>
              </div>
               <?php if(isset($productdetails[0]->occasion_date) && $productdetails[0]->occasion_date!=''): ?>
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.OCCASION_DATE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_DATE'): trans($OUR_LANGUAGE.'.OCCASION_DATE')); ?>:</div>
                <div class="wed_text">
                 
                  <?php echo e(Carbon\Carbon::parse($productdetails[0]->occasion_date)->format('d M Y')); ?>

                  
                </div>
              </div>

				<?php endif; ?>

			  <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')); ?>:</div>
                <div class="wed_text">
                  <?php echo e($productdetails[0]->order_id); ?>

                </div>
              </div>
            </div>
			<?php 
			$getSearchData = Helper::searchDetails($productdetails[0]->order_id);
			  $getShipping = Helper::getShippingmyaccount($productdetails[0]->order_id,$productdetails[0]->product_sub_type);
			?>
            <div class="types_ocs_right">
			  <?php if(isset($getSearchData->budget) && $getSearchData->budget!='' && strtolower($getSearchData->budget)!='n/a'): ?>
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?>:</div>
                <div class="wed_text">
                  <?php if(isset($getSearchData->budget) && $getSearchData->budget!=''): ?>
                  SAR <?php echo e(number_format($getSearchData->budget,2)); ?>

                  <?php endif; ?>
                </div>
              </div>
			  <?php endif; ?>
			  <?php if(isset($getSearchData->total_member) && $getSearchData->total_member!=''): ?>
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_ATTENDANCE'): trans($OUR_LANGUAGE.'.NO_OF_ATTENDANCE')); ?>:</div>
                <div class="wed_text">
                 <?php if(isset($getSearchData->total_member) && $getSearchData->total_member!=''): ?>
                  <?php echo e($getSearchData->total_member); ?>

                  <?php endif; ?>
                </div>
              </div>
			  <?php endif; ?>
            </div>
          </div>
		  
          <div class="list_of_service">
            <h1 class="list_heading"><?php echo e((Lang::has(Session::get('lang_file').'.LIST_OF_SERVICES')!= '')  ?  trans(Session::get('lang_file').'.LIST_OF_SERVICES'): trans($OUR_LANGUAGE.'.LIST_OF_SERVICES')); ?></h1>
            <div class="main_box_table_area">
              <div class="myaccount-table mts order-detail-table">
                <div class="mytr mytr-head">
                   
                  <div class="mytable_heading grey"> <?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> </div>
                  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?></div>
                  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')); ?></div>
                  <?php //if(isset($productdetails[0]->product_type) && ($productdetails[0]->product_sub_type=='singer' || $productdetails[0]->product_sub_type=='band')){}else{ ?>                                     
                  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')); ?></div>
                  <?php //} ?>
                  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')); ?></div>
				  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= '')  ?  trans(Session::get('lang_file').'.VIEW_DETAILS'): trans($OUR_LANGUAGE.'.VIEW_DETAILS')); ?></div>
                </div>                 
                <?php  
				$i=1;$basetotal = 0; $couponcode = 0; $totalinsu=0; if(isset($productdetails) && count($productdetails) > 0){ foreach($productdetails as $val){ 
							if(isset($val->product_type) && $val->product_type=='hall' && $val->product_sub_type=='hall'){
									$cp=$val->paid_total_amount;
							}else{
									$cp=$val->sum;
							}
					$basetotal = ($basetotal+$cp);  
					$totalinsu=$totalinsu+$val->insurance_amount;

				 $couponcode = Helper::getorderedfromcoupanamount($val->order_id); ?>
                <div class="mytr mybacg">               
                  <div class="mytd mies miestd1" data-title=" <?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?>">
                    <?php 
					if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='beauty_centers')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مراكز تجميل';
					   }
					   else
					   {
						   $service_name = 'Beauty Centers'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='spa')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'منتجع صحي';
					   }
					   else
					   {
						   $service_name = 'Spa'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='makeup_artists')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فنان ماكياج';
					   }
					   else
					   {
						   $service_name = 'Makeup Artist'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='makeup')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'منتجات المكياج';
					   }
					   else
					   {
						   $service_name = 'Makeup Products'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='men_saloon')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'حلاق';
					   }
					   else
					   {
						   $service_name = 'Barber'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='car_rental' && $val->product_sub_type=='car')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'تأجير السيارات';
					   }
					   else
					   {
						   $service_name = 'Car Rentals'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='clinic' && $val->product_sub_type=='cosmetic')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مستحضرات التجميل والليزر';
					   }
					   else
					   {
						   $service_name = 'Cosmetics And Laser'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='clinic' && $val->product_sub_type=='skin')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'طب الأسنان و الأمراض الجلدية';
					   }
					   else
					   {
						   $service_name = 'Dental And Dermatology'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='travel' && $val->product_sub_type=='travel')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'طب الأسنان و الأمراض الجلدية';
					   }
					   else
					   {
						   $service_name = 'Travel Agencies'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='tailor')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'الخياطين';
					   }
					   else
					   {
						   $service_name = 'Tailors'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='dress')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فساتين';
					   }
					   else
					   {
						   $service_name = 'Dresses'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='perfume')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'العطور';
					   }
					   else
					   {
						   $service_name = 'Perfumes'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='abaya')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'العباءة';
					   }
					   else
					   {
						   $service_name = 'Abaya'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='gold')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مجوهرات';
					   }
					   else
					   {
						   $service_name = 'Jewellery'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='hall' && $val->product_sub_type=='hall')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فندق القاعات';
					   }
					   else
					   {
						   $service_name = 'Hotal Halls'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='food' && $val->product_sub_type=='dates')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'تواريخ';
					   }
					   else
					   {
						   $service_name = 'Dates'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='food' && $val->product_sub_type=='buffet')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'البوفيهات';
					   }
					   else
					   {
						   $service_name = 'Buffets'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='cosha')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'اكتشف';
					   }
					   else
					   {
						   $service_name = 'Kosha'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && ($val->product_sub_type=='photography' || $val->product_sub_type=='video'))
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'استوديو تصوير';
					   }
					   else
					   {
						   $service_name = 'Photography Studio'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='hospitality')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'الاستقبال والضيافة';
					   }
					   else
					   {
						   $service_name = 'Reception & Hospitality'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='music' && $val->product_sub_type=='acoustic')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'نظام الصوت';
					   }
					   else
					   {
						   $service_name = 'Sound System'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='roses')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'ورود';
					   }
					   else
					   {
						   $service_name = 'Roses'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='events')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'حدث مميز';
					   }
					   else
					   {
						   $service_name = 'Special Event'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='singer' && $val->product_sub_type=='singer')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مطرب';
					   }
					   else
					   {
						   $service_name = 'Singer'; 
					   }						 			  
				       echo $service_name;					  
					}
                    else if(isset($val->product_type) && $val->product_type=='band' && $val->product_sub_type=='band')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فرق شعبية';
					   }
					   else
					   {
						   $service_name = 'Popular Bands'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='recording' && $val->product_sub_type=='recording')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'تسجيل';
					   }
					   else
					   {
						   $service_name = 'Recording'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='food' && $val->product_sub_type=='dessert')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'الحلوى';
					   }
					   else
					   {
						   $service_name = 'Desserts'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='invitations')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'دعوة الكترونية';
					   }
					   else
					   {
						   $service_name = 'Electronic Invitation'; 
					   }						 			  
				       echo $service_name;					  
					}
					/*if(isset($val->category_id) && $val->category_id!='')
					{
                       $dget_category_name = Helper::getshopname($val->category_id);
					   $mc_name = 'mc_name';
                       if(Session::get('lang_file')!='en_lang')
					   {
                          $mc_name= 'mc_name_ar'; 
					   }
					   echo $dget_category_name->$mc_name;
					}
					*/
							$electstatus='';
						if($val->product_sub_type=='invitations' && $val->product_type=='occasion'){				
							$electstatus=Helper::getInvitationOrderdate($val->order_id,$val->id);
					      }
					      
                    ?>
                  </div>
                  <div class="mytd mies miestd2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">
                    <?php                   
                    if(isset($val->merchant_id) && $val->merchant_id!='0')
					{
					  $get_vendor_name = Helper::getServiceProviderName($val->merchant_id);
					  echo $get_vendor_name;
                    }
					else
					{?>
					  <?php echo e((Lang::has(Session::get('lang_file').'.ADMIN')!= '')  ?  trans(Session::get('lang_file').'.ADMIN'): trans($OUR_LANGUAGE.'.ADMIN')); ?>

					<?php } 
					if(isset($val->shop_id) && $val->shop_id!='')
					{
                       if(isset($val->product_type) && ($val->product_sub_type=='singer' || $val->product_sub_type=='band' || $val->product_sub_type=='recording'))
                       {
                         $enq_id = Helper::quoteRequested($val->shop_id);
                         $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
						 echo ' / '.$dget_category->singer_name;                       
                       }
                       else
                       {
                         $dget_category_name = Helper::getshopname($val->shop_id);
                         $mc_name = 'mc_name';
                         if(Session::get('lang_file')!='en_lang')
					     {
                           $mc_name= 'mc_name_ar'; 
					     }
					     echo ' / '.$dget_category_name->$mc_name;
                       }
					   
					}
					?>
                  </div>
                  <div class="mytd mies miestd3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')); ?>">
                    <?php if(isset($val) && $val!=''): ?>
                    <?php echo e(Carbon\Carbon::parse($val->order_date)->format('d M Y')); ?>

                    <?php endif; ?>
                  </div>
                   <?php if(isset($val->product_type) && ($val->product_sub_type=='singer' || $val->product_sub_type=='band' || $val->product_sub_type=='recording')){?>
				   <div class="mytd mies miestd4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')); ?>"><?php if(Lang::has(Session::get('lang_file').'.CONFIRMED')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONFIRMED')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONFIRMED')); ?> <?php endif; ?></div>
				   <?php }else{ 
				   	//echo 'vinay'.$electstatus;
				   	?>
                  <?php if(isset($val) && $val!=''): ?>
                   <?php if($val->status==1 || $electstatus=='process'): ?>
                  <div class="mytd mies miestd4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')); ?>"><?php if(Lang::has(Session::get('lang_file').'.Process')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Process')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Process')); ?> <?php endif; ?></div>
                  <?php elseif($val->status==2 || $electstatus=='complete'): ?>
                  <div class="mytd mies miestd4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')); ?>"><?php if(Lang::has(Session::get('lang_file').'.Delivered')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Delivered')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Delivered')); ?> <?php endif; ?></div>
                   <?php elseif($val->status==3): ?>
                  <div class="mytd mies miestd4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')); ?>"><?php if(Lang::has(Session::get('lang_file').'.Hold')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Hold')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Hold')); ?> <?php endif; ?></div>
                   <?php elseif($val->status==4): ?>
                  <div class="mytd mies miestd4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')); ?>"><?php if(Lang::has(Session::get('lang_file').'.Failed')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Failed')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Failed')); ?> <?php endif; ?></div>
                  <?php endif; ?>
                  <?php endif; ?>
				  <?php } ?>
                  <div class="mytd mies miestd5" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')); ?>">
                    <?php if(isset($val->product_type) && $val->product_type!='hall'): ?>
                    SAR <?php echo e(number_format($val->sum,2)); ?>

                    <?php else: ?>
                    SAR <?php echo e(number_format($val->paid_total_amount,2)); ?>

                    <?php endif; ?>
                  </div>
				  <div class="mytd mies miestd6" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.VIEW_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.VIEW_DETAIL'): trans($OUR_LANGUAGE.'.VIEW_DETAIL')); ?>">

				  	<?php if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='cosha'){ $newscid=$val->shop_id; }else{
				  		$newscid=$val->category_id;
				  	 }?>


                    <a href="<?php echo e(route('serviceorder',[$val->cus_id,$val->product_type,$val->product_sub_type,$val->order_id,$newscid,$val->product_id,$val->merchant_id,$val->shop_id])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.VIEW_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.VIEW_DETAIL'): trans($OUR_LANGUAGE.'.VIEW_DETAIL')); ?></a>

                    <?php if($val->product_sub_type=='invitations' && $val->product_type=='occasion'){ ?>


                    <a href="<?php echo e(route('invitation-attended-list',[$val->order_id])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Attended_List')!= '')  ?  trans(Session::get('lang_file').'.Attended_List'): trans($OUR_LANGUAGE.'.Attended_List')); ?></a>

               <?php } ?>

                  </div>
                </div>
                <?php $i++;} }else{ ?>
                <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
                <?php } ?>

              </div>
			<div class="main_total">
				<div class="vat-tax-line">
					<?php if($couponcode >=1): ?>
					<?php echo e((Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')); ?>: 
					<?php echo 'SAR '.number_format($couponcode,2); ?>
					<?php endif; ?>
					<br />
					<?php if(Lang::has(Session::get('lang_file').'.VAT')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.VAT')); ?><?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VAT')); ?> <?php endif; ?>: 
					<?php $totalvatprice=$basetotal-$couponcode-$totalinsu;
					$vatamount = Helper::calculatevat($productdetails[0]->order_id,$totalvatprice);
					echo 'SAR '.number_format(($vatamount),2);
					?>					

				</div>
					<?php if($getShipping>1){ ?>
					<div class="vat-tax-line">
				 
					<?php echo e((Lang::has(Session::get('lang_file').'.SHIPPING')!= '')  ?  trans(Session::get('lang_file').'.SHIPPING'): trans($OUR_LANGUAGE.'.SHIPPING')); ?>: 
					<?php echo 'SAR '.number_format($getShipping,2); ?>
				 
				 		 

				</div>
				<?php } ?>

				<br />



			<?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: 
			<?php if(isset($productdetails) && $productdetails!=''){
			echo 'SAR '.number_format($getShipping+$basetotal+$productdetails[0]->order_taxAmt-$couponcode,2);
			} ?>
			</div>
 


			  
            </div>
          </div>
		  <?php }else{?>
			<div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
			<?php } ?>
        </div>
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
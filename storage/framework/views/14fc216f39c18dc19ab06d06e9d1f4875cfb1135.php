<?php $__env->startSection('content'); ?>
    <?php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='')
        {
        $Current_Currency = 'SAR';
        }
       // $cururl = request()->segment(count(request()));
    ?>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2>Dessert Shop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php echo e($fooddateshopdetails[0]->mc_name); ?></h2>
                                <h2><?php echo e($fooddateshopdetails[0]->address); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php echo e($fooddateshopdetails[0]->mc_discription); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <?php if($fooddateshopdetails[0]->latitude!='' && $fooddateshopdetails[0]->longitude!=''){  $lat=$fooddateshopdetails[0]->latitude;   $long=$fooddateshopdetails[0]->longitude;    ?>


                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        <?php }  ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $fooddateshopreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="item">
                                    <div class="tump">
                                        <img src="<?php echo e($customerreview->cus_pic); ?>" alt="">
                                    </div>
                                    <div class="caption">
                                        <h2><?php echo e($customerreview->cus_name); ?></h2>
                                        <div class="stars">
                                            <img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png">
                                        </div>
                                        <p><?php echo e($customerreview->comments); ?></p>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                        <?php echo e($fooddateshopreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe class="service-video" src="<?php echo e($fooddateshopdetails[0]->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                    </div>
                </div>
            </div>

            <div class="desserts">
                <div class="slide-cats owl-carousel" dir="ltr">
                    <?php $__currentLoopData = $fooddateshopproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <div class="item-cat">
                        <a href=""><img src="<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>" alt="">
                            <p><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></p>
                        </a>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>


                <div class="box p-15">
                    <div class="row">
                        



                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="<?php echo e(isset($fooddateshopleftproduct->pro_Img) ? $fooddateshopleftproduct->pro_Img : ''); ?>" alt="" />
                                </div>
                                <div class="caption">
                                    <a href=""><?php echo e($fooddateshopleftproduct->pro_title); ?></a>
                                    <p><?php echo e($fooddateshopleftproduct->pro_desc); ?></p>
                                    <div class="flex-end">
                                        <?php if($fooddateshopleftproduct->product_price[0]->discount>0){ $netprice=$fooddateshopleftproduct->product_price[0]->discount_price; }else{ $netprice=$fooddateshopleftproduct->product_price[0]->product_option_value_id; } ?>
                                        <div class="price">SAR <?php echo e(isset($netprice) ? $netprice : '0'); ?></div>
                                        
                                        <?php echo Form::open(['url' => 'dessertshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

                                           <input type="hidden" id="product_id" name="product_id" value="<?php echo e($fooddateshopleftproduct->pro_id); ?>">
                                           <input type="hidden" id="21" name="pr21" value="<?php echo e(isset($netprice) ? $netprice : '0'); ?>">
                                          <input type="hidden" id="23" name="pr23" value="<?php echo e(isset($fooddateshopleftproduct->product_price[1]->product_option_value_id) ? $fooddateshopleftproduct->product_price[1]->product_option_value_id : '0'); ?>">
                                         <input type="hidden" name="shopby" id="shopby" value="">
                                                <input type="hidden" id="cart_type" name="cart_type" value="food">
                                                <input type="hidden" id="attribute_id" name="attribute_id" value="34">
                                                <input type="hidden" id="prodqty" name="prodqty" value="<?php echo e($fooddateshopleftproduct->pro_qty); ?>">
                                                <input type="hidden" id="shop_id" name="shop_id" value="<?php echo e($shop_id); ?>">
                                                <input type="hidden" id="category_id" name="category_id" value="<?php echo e($subcategory_id); ?>">
                                                <input type="hidden" id="subcategory_id" name="subcategory_id" value="<?php echo e($category_id); ?>">
                                                <input type="hidden" id="branch_id" name="branch_id" value="<?php echo e($branch_id); ?>">
                                                <span id="error"></span>
                                                <input type="submit" class="form-btn" id="sbtn" value="<?php if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Add_to_Cart')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Add_to_Cart')); ?> <?php endif; ?>">

                                        <!-- container-section -->
                                        <?php echo Form::close(); ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>

            <center>
                <?php echo e($fooddateshopproducts->links()); ?>

            </center>

        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
 <style type="text/css">
      .ms-drop ul li label input[type="checkbox"] {
      height: 20px;
      display: inline-block;
      width: 20px;
      position: relative;
      border: 1px solid red;
      opacity: 1;
      }
      </style>
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/themes/css/multiple-select.css" />

<?php $hall_leftmenu =1; 

if(isset($getDb->food_type) && ($getDb->food_type=='internal'  || $getDb->food_type=='both'))
{  $showMenu =1; } else { $showMenu =0; } 

?> 
<!--start merachant-->
<div class="merchant_vendor"> 
  
  <!--left panel--> 
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  <!--left panel end--> 
  <!--right panel-->
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALL_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALL_INFO')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALL_INFO')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?> 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> <?php $getBinfo = Helper::getparentCat($_REQUEST['bid']);  
                $getBinfos = Helper::getparentCat( $getBinfo->parent_id );


              ?> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              <?php
              if(isset($getDb->hall_address_image) && $getDb->hall_dimension !='')
              {
              
              $getDim = $getDb->hall_dimension;
              
              $getAllareas = explode('X',$getDim);
              $HallLength = $getAllareas[0];
              $HallWidth = $getAllareas[1];
              $HallArea = $getAllareas[2];
              
              }
              
              if(isset($getDb->food_type) && $getDb->food_type !='')
              {
              $hallfoodmenu = $getDb->food_type;
              $getHType = $getDb->hall_type;
              }
              else
              {
              $hallfoodmenu ='';
              $getHType = '';
              }
              
              ?>
              <?php echo Form::open(array('url' => '/add_hallinfo','method' => 'POST', 'id'=> 'add_hallinfo','enctype' =>'multipart/form-data', 'name' =>'add_hallinfo' )); ?>

              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HOTEL_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HOTEL_NAME'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english"> <?php echo e($getBinfos->mc_name); ?> </div>
                    <div class="arabic ar"> <?php echo e($getBinfos->mc_name_ar); ?> </div>
                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); ?> </span> </label>
                  <div class="info100">
                    <div class="english"> <?php echo e($getBinfo->mc_name); ?> </div>
                    <div class="arabic ar"> <?php echo e($getBinfo->mc_name_ar); ?> </div>
                  </div>
                </div>
              </div>

               <div class="form_row common_field">
              <div class="form_row_left">
              <label class="form_label">
                <span class="english"><?php echo lang::get('mer_en_lang.Select_Category'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Select_Category'); ?> </span>

          </label>
              <div class="info100">  
              <select class="small-sel" name="category[]" id="category"   multiple="multiple">
           
                 <?php $wedding_name='Wedding And Occasions'?>
                 <?php $business_name='Business Meeting'?>
                 <?php if($mer_selected_lang_code !='en'): ?>
                 <?php $wedding_name='زفاف و مناسبات'?>
                 <?php $business_name='اجتماع عمل'?>
                 
            <?php endif; ?>

             <?php
                  $halltype_one = 0; $halltype_two = 0;
  if(isset($getDb->hall_category_type) && $getDb->hall_category_type !='')
              {
                  $halltype = explode(',', $getDb->hall_category_type);

                  if(isset($halltype[0]) && $halltype[0]==1)
                  {
                  $halltype_two = 0;
                  $halltype_one = 1;
                  }
                  if(isset($halltype[1]) && $halltype[1]==2)
                  {
                  $halltype_two = 2;
                  $halltype_one = 1;
 
                  }
                  if(isset($halltype[0]) && $halltype[0]==2)
                  {
                  $halltype_two = 2;
                  $halltype_one = 0;
                  }
                }
                  ?>

                  
                <option value="1" <?php echo e(isset($halltype_one) && $halltype_one ==1 ? 'selected' : ''); ?>><?php echo e($wedding_name); ?></option>
                <option value="2" <?php echo e(isset($halltype_two) && $halltype_two ==2 ? 'selected' : ''); ?>><?php echo e($business_name); ?></option>
              </select>
              </div>
              </div>
              </div>

              <input type="hidden" name="bid" value="<?php echo e(isset($_REQUEST['bid']) ? $_REQUEST['bid'] : ''); ?>">
              <input type="hidden" name="pro_id" value="<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>">
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALL_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALL_NAME'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="<?php echo e(isset($getDb->pro_title) ? $getDb->pro_title : ''); ?>" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="hallname_ar"  maxlength="35"   data-validation="length required" 
		 data-validation-error-msg="يرجى إدخال اسم القاعة" value="<?php echo e(isset($getDb->pro_title_ar) ? $getDb->pro_title_ar : ''); ?>" data-validation-length="max35">
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALL_TYPE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALL_TYPE'); ?> </span> </label>
                  <div class="info100">
                    <div class="save-card-line">
                      <input id="men" type="checkbox" value="men"  name="halltype[]" <?php if($getHType=='men' || $getHType=='both'): ?> CHECKED <?php endif; ?>  data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="men"> <span class="english"><?php echo lang::get('mer_en_lang.MER_MEN'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_MEN'); ?> </span> </label>
                    </div>
                    <div class="save-card-line">
                      <input type="checkbox" value="women"  id="women" name="halltype[]" <?php if($getHType=='women' || $getHType=='both'): ?> CHECKED <?php endif; ?> data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="women"> <span class="english"><?php echo lang::get('mer_en_lang.MER_WOMEN'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_WOMEN'); ?> </span> </label>
                    </div>
                    <br>
                    <span for="halltype[]" generated="true" class="error"> </span> </div>
                </div>
              </div>
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.CAPACITY'); ?> <span class="" style="font-size: 10pt; font-weight: normal;">(<?php echo lang::get('mer_en_lang.People'); ?>)</span></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.CAPACITY'); ?> <span class="" style="font-size: 10pt; font-weight: normal;">(<?php echo lang::get('mer_ar_lang.People'); ?>)</span> </span> </label>
                  <div class="engslish">
                    <input type="text" class="small-sel" name="hallcapicity"  maxlength="35"  data-validation-error-msg="يرجى إدخال اسم القاعة" value="<?php echo e(isset($getDb->hallcapicity) ? $getDb->hallcapicity : ''); ?>" data-validation-length="max35">
                  </div>
                  <?php if($errors->has('hallcapicity')): ?> <span class="error"> <?php echo e($errors->first('hallcapicity')); ?> </span> <?php endif; ?> </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALL_PRICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALL_PRICE'); ?> </span> </label>
                  <input type="text" name="price"  maxlength="8" onkeypress="return isNumber(event)" class="small-sel"  data-validation="length required" data-validation-error-msg="Please enter hall price" value="<?php echo e(isset($getDb->pro_price) ? $getDb->pro_price : ''); ?>" data-validation-length="max30">
                  <br/>
                </div>
              </div>

               <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); ?> %</span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); ?>  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="<?php echo e(isset($getDb->pro_discount_percentage) ? $getDb->pro_discount_percentage : ''); ?>" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>

                     <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Insuranceamount'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Insuranceamount'); ?> </span> </label>
                    <div class="ensglish">
                      <input type="text" class="small-sel" required="" name="Insuranceamount"  maxlength="15"   data-validation="length required" 
                    data-validation-error-msg="يرجى إدخال اسم القاعة" value="<?php echo e(isset($getDb->Insuranceamount) ? $getDb->Insuranceamount : ''); ?>" data-validation-length="max35">
                    </div>
                    <?php if($errors->has('Insuranceamount')): ?> <span class="error"> <?php echo e($errors->first('Insuranceamount')); ?> </span> <?php endif; ?> 
                    </div>


              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALLDIM'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALLDIM'); ?> </span> </label>
                  <div class="info100">
                    <div class="hall_dimension" >
                      <div class="hall_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALLLENGTH'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALLLENGTH'); ?> </span> </div>
                      <div class="hall_input">
                        <input type="text" name="hall_length"  maxlength="7" onkeypress="return isNumber(event)" data-validation="length required" data-validation-error-msg="Please enter hall length" value="<?php echo e(isset($HallLength) ? $HallLength : ''); ?>" data-validation-length="max7">
                      </div>
                      <div class="hall-parameter"> <span class="english"><?php echo lang::get('mer_en_lang.MER_FT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_FT'); ?> </span> </div>
                    </div>
                    <div class="hall_dimension" >
                      <div class="hall_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALLWIDTH'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALLWIDTH'); ?> </span> </div>
                      <div class="hall_input">
                        <input type="text"  onkeypress="return isNumber(event)" maxlength="7"  name="hall_width"  data-validation="required" data-validation-error-msg="Please enter hall width" value="<?php echo e(isset($HallWidth) ? $HallWidth : ''); ?>" data-validation-length="max7">
                      </div>
                      <div class="hall-parameter"> <span class="english"><?php echo lang::get('mer_en_lang.MER_FT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_FT'); ?> </span> </div>
                    </div>
                    <div class="hall_dimension">
                      <div class="hall_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALLarea'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALLarea'); ?> </span> </div>
                      <div class="hall_input">
                        <input type="text" onkeypress="return isNumber(event)" name="hall_area"  maxlength="7"  data-validation="required" data-validation-error-msg="Please enter hall area" value="<?php echo e(isset($HallArea) ? $HallArea : ''); ?>" data-validation-length="max7">
                      </div>
                      <div class="hall-parameter"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SQFT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SQFT'); ?> </span> </div>
                    </div>
                  </div>
                </div>



                <div class="form_row_right">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_FOOD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_FOOD'); ?> </span> </label>
                  <div class="info100 tooltip_icon">

                    <?php if($getBinfos->parent_id != 6): ?>
                    <div class="save-card-line">
                      <input type="checkbox" id="internal" value="internal" name="hallfoodmenu[]" <?php if($hallfoodmenu=='internal' || $hallfoodmenu=='both'): ?> CHECKED <?php endif; ?> data-validation="checkbox_group" data-validation-qty="min1">
                      <label for="internal"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALLINTERNAL'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALLINTERNAL'); ?> </span> <a class="tooltip_area" href="javascript:void(0);"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.List_the_food_choices_available_with_your_hall.?')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.List_the_food_choices_available_with_your_hall.?')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.List_the_food_choices_available_with_your_hall.?')); ?> <?php endif; ?>
                      </span></a> </label>
                    </div>
                    <div class="save-card-line">
                      <input value="external" id="external" type="checkbox" name="hallfoodmenu[]" <?php if($hallfoodmenu=='external'  || $hallfoodmenu=='both'): ?> CHECKED <?php endif; ?> data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="external"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALLEXTERNAL'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALLEXTERNAL'); ?> </span> <a class="tooltip_area" href="javascript:void(0);"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Do_you_allow_food_from_outside?')); ?> <?php endif; ?></span></a> </label>
                    </div>
                    <br/>
                    <span for="hallfoodmenu[]" generated="true" class="error"> </span>
                    <?php else: ?>
                
                      <div class="save-card-line">
                      <input value="external" id="external" type="checkbox" name="hallfoodmenu[]" <?php if($hallfoodmenu=='external'  || $hallfoodmenu=='both'): ?> CHECKED <?php endif; ?> data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="external"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALLEXTERNAL'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALLEXTERNAL'); ?> </span> <a class="tooltip_area" href="javascript:void(0);"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Do_you_allow_food_from_outside?')); ?> <?php endif; ?></span></a> </label>
                    </div>
                   <?php endif; ?>


                     </div>
                </div>
                

              </div>
              <div class="form_row ">
                <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HALL_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HALL_IMAGE'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value1"></div>
                    </div>
                    </label>
                    <input type="file" name="hallimage" id="company_logo" class="info-file">
                  </div>
                  
         
            <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
           
  
    <div class="form-upload-img"><?php if(isset($getDb->pro_Img) && $getDb->pro_Img !=''): ?> <img src="<?php echo e($getDb->pro_Img); ?>" width="150" height="150"> <?php endif; ?> </div>
 
                  <?php if($errors->has('hallimage')): ?> <span class="error"> <?php echo e($errors->first('hallimage')); ?> </span> <?php endif; ?> </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_About'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_About'); ?> </span> </label>
                  <div class="info100 english">
                    <textarea maxlength="500" name="about" id="about" rows="4" cols="50"  onkeyup="limitTextCount('about', 'divcount', 500,'<?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?>');" onkeydown="limitTextCount('about', 'divcount', 500,'<?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?>');"><?php echo e(isset($getDb->about) ? $getDb->about : ''); ?></textarea>
                  </div>
                   <div class="info100 arabic ar">
                    <textarea maxlength="500" name="about_ar" id="about_ar" rows="4" cols="50"  onkeyup="limitTextCount('about_ar', 'divcount_ar', 500,'<?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?>');" onkeydown="limitTextCount('about_ar', 'divcount_ar', 500,'<?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?>');"><?php echo e(isset($getDb->about_ar) ? $getDb->about_ar : ''); ?></textarea>
                  </div>
                  <?php 
                  if(isset($getDb->about) && $getDb->about!='') 
                  {
                     $chkChar = 500 - strlen(trim($getDb->about)); 
                  }
                  else
                  {
                     $chkChar = 500;
                  }
                  if(isset($getDb->about_ar) && $getDb->about_ar!='') 
                  {
                     $chkChar_ar = 500 - mb_strlen(trim($getDb->about_ar),'utf8'); 
                  }
                  else
                  {
                     $chkChar_ar = 500;
                  }
                 
                  ?>
                    <div id="divcount" class="english"><?php echo e(isset($chkChar) ? $chkChar : '500'); ?> <?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?></div>
                      <div id="divcount_ar" class="arabic ar"><?php echo e(isset($chkChar_ar) ? $chkChar_ar : '500'); ?>  <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); ?></div>
                </div>
              </div>
              <input type="hidden" name="bid" value="<?php echo e(isset($_REQUEST['bid']) ? $_REQUEST['bid'] : ''); ?>">
              <input type="hidden" name="pro_id" value="<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>">
              
              <div class="form-btn-section english">
              	<div class="form_row_left">
                <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                </div>
              </div>
              
              <div class="form-btn-section arabic ar">
              <div class="form_row_left arbic_right_btn">
                <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                </div>
              </div>
              
              <?php echo Form::close(); ?> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end right panel--> 
</div>
<!--end merachant--> 

<script>       
	$("form").data("validator").settings.ignore = "";
</script>
 
<script type="text/javascript">
  
$("#add_hallinfo").validate({
                  ignore: [],
                  rules: {
                  hallname: {
                       required: true,
                      },

                       hallname_ar: {
                       required: true,
                      },


                       category: {
                       required: true,
                      },

                    

                      hall_length: {
                       required: true,
                      },
                       hall_width: {
                       required: true,
                      },
                       hall_area: {
                       required: true,

                      },

                       'hallfoodmenu[]': {
                       required: true,
                      },

                       'halltype[]': {
                       required: true,
                      },


                       price: {
                       required: true,
                       min: 1,
                      },

                     <?php if(isset($getDb->pro_Img)!=''): ?>  
                      hallimage: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },
                      <?php else: ?>
                        hallimage: {
                           required:true,
                           accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>

                      hall_addressimg: {
                           accept:"png|jpe?g|gif",
                      },
                       hallcapicity: {
                       required: true,
                      },

                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                   
           messages: {
            
                 category: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SELECT_CATEGORY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SELECT_CATEGORY')); ?> <?php endif; ?>",
                      }, 

             hallname: {
         required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_HALL_NAME'); ?> ", 
                      },  

                 hallname_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_HALL_NAME_AR'); ?> ",
                      },  
 
                          hall_length: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_LENTH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_LENTH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_LENTH')); ?> <?php endif; ?>",
                      },   


                       hall_width: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_WIDTH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_WIDTH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_WIDTH')); ?> <?php endif; ?> ",
                      },

                       hall_area: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_AREA')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_AREA')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_AREA')); ?> <?php endif; ?> ",
                      },

                       'hallfoodmenu[]': {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_FOOD_MENMU')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_FOOD_MENMU')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_FOOD_MENMU')); ?> <?php endif; ?> ",
                      },

                       'halltype[]': {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_TYPE')); ?> <?php endif; ?> ",
                      },


                       price: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_PRICE')); ?> <?php endif; ?> ",
                      },
                        hallimage: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                      },     

                       hall_addressimg: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },   
                          hallcapicity: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_CAPACITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_CAPACITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_CAPACITY')); ?> <?php endif; ?>",
                      },   
                    
                     
                },
                invalidHandler: function(e, validation){
                   // console.log("invalidHandler : event", e);
                    //console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    var valdata1=validation.errorList;

                     console.log("invalidHandler : validationerror",valdata1 ); 




                    var i;
                    var erroren=0;
                    var start=9;
                    var lang ='ar';
                    var val=0
                    
                    for (i = 0; i < start ;  i++) {
                      
                      if (typeof valdata1[i] !== 'undefined') {  
                        var data=valdata1[i].element.classList;
                        $.inArray('ar',data);
                                        console.log("invalidHandler : validation", $.inArray('ar',data)); 
                                              if($.inArray('ar',data) == -1) 
                                              {
                                                  val=1;

                                              } 

                                          }                                          
                                     }
                                        <?php if($mer_selected_lang_code !='ar'): ?>
                                              if(val==0)
                                              {

                                                $('.arabic_tab').trigger('click');  
                                              }
                                              else
                                              {

                                                $('.english_tab').trigger('click');
                                              }
                                        <?php else: ?>

                                         if(val==0)
                                              {
                                                     $('.english_tab').trigger('click');
                                               
                                              }
                                              else
                                              {

                                                    $('.arabic_tab').trigger('click');  

                                              
                                              }
                                       <?php endif; ?>       
                                        

                       

 
 

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<script src="<?php echo e(url('')); ?>/themes/js/jquery.min.js"></script> 
<script src="<?php echo e(url('')); ?>/themes/js/multiple-select.js"></script> 
<?php if(Session::get('lang_file') =='ar_lang'): ?> 

<script>
    $(function() {
        $('#category').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });
      });
</script>
 <?php else: ?> 
<script>
    $(function() {
        $('#category').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });
     });
</script> 
<?php endif; ?> 


<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Rosestbig_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>

      <?php if(request()->autoid ==''): ?>
      <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADDPACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADDPACKAGE')); ?> <?php endif; ?> </h5>
      <?php else: ?>
      <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATEPACKAGE')); ?>  
      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE')); ?> <?php endif; ?> </h5>
      <?php endif; ?>

        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <style type="text/css">
      .ms-drop ul li label input[type="checkbox"] {
      height: 20px;
      display: inline-block;
      width: 20px;
      position: relative;
      border: 1px solid red;
      opacity: 1;
      }
      </style>
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/themes/css/multiple-select.css" />
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?> </div>
              <?php endif; ?>
              <!-- Display Message after submition -->
                <form name="form1" method="post" id="add-container" action="<?php echo e(route('store-rosespackages')); ?>"" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PACKEGENAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKEGENAME'); ?> </span></label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" maxlength="80" type="text" name="title" value="<?php echo e(isset($getinfos->pro_title) ? $getinfos->pro_title : ''); ?>" id="title" required="">
                      </div>
                      <div class="arabic">
                        <input class="arabic ar" maxlength="80" id="title_ar"  name="title_ar" value="<?php echo e(isset($getinfos->pro_title_ar) ? $getinfos->pro_title_ar : ''); ?>"  type="text" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PACKAGEIMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKAGEIMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="img" class="info-file" type="file" >
                        <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                        <?php if(isset($getinfos->pro_Img) && $getinfos->pro_Img!=''): ?>
                        <div class="form-upload-img"><img src="<?php echo e(isset($getinfos->pro_Img) ? $getinfos->pro_Img : ''); ?>"></div>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_PRICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRICE'); ?> </span> </label>
                    <div class="info100">
                      <div>
                        <input class="xs_small notzero" maxlength="9" onkeypress="return isNumber(event)"  type="text" name="price" value="<?php echo e(isset($getinfos->pro_price) ? $getinfos->pro_price : ''); ?>" id="price" required="">
                      </div>
                    </div>
                  </div>   
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DISCOUNT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DISCOUNT'); ?> </span> </label>
                    <div class="info100">
              <input type="text" class="xs_small notzero" maxlength="2"  onkeypress="return isNumber(event)"  name="discount" value="<?php echo e(isset($getinfos->pro_discount_percentage) ? $getinfos->pro_discount_percentage : ''); ?>" id="discount" >
                    </div>
                  </div>
                </div>


                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.FlOWERTYPE'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.FlOWERTYPE'); ?> </span> </label>
                    <div class="info100">                  
                        <select name="flowertype[]" id="flowertype" class="small-sel" multiple=""> 
                        <?php $__currentLoopData = $simpleProductsMerchant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <?php $ProID = $values->pro_id; ?>  
                        <option value="<?php echo e($values->pro_id); ?>" <?php if(in_array($ProID, $ChkrelArray)): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?>>  <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($values->pro_title_ar); ?> <?php else: ?>  <?php echo e($values->pro_title); ?>  <?php endif; ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  
                        </select>
                   <span for="flowertype" generated="true" class="error">  </span>
                    </div>
                  </div>

                  <div class="form_row_right">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.WRAPTYPE'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.WRAPTYPE'); ?> </span> </label>
                    <div class="info100">
                      <div>
                        <select name="wraptype" id="wraptype" class="small-sel">
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?> </option>
                        <?php $getWrapingtype = Helper::getWrappingType('25'); ?>
                        <?php $__currentLoopData = $getWrapingtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $valus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                        <?php $ids = $valus->id; ?>   
                        <option value="<?php echo e($valus->id); ?>" <?php if(isset($GetrelatedWType) && $GetrelatedWType== $ids): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?>> <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($valus->option_title_ar); ?> <?php else: ?> <?php echo e($valus->option_title); ?>  <?php endif; ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              
                <div class="form_row common_field">
				          <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.WRAPDESIGN'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.WRAPDESIGN'); ?> </span> </label>
                  <div class="info100">
                    <div>
                      <select name="wrapdesign" id="wrapdesign" class="small-sel">
                  <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?> </option>
                  <?php $getWrapingtype = Helper::getWrappingType('26'); ?>
                  <?php $__currentLoopData = $getWrapingtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $valus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   
                     <?php $idss = $valus->id; ?>   
                  <option value="<?php echo e($valus->id); ?>" <?php if(isset($idss) && $idss== $GetrelatedWDesign): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?>>
                    <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($valus->option_title_ar); ?> <?php else: ?> <?php echo e($valus->option_title); ?>  <?php endif; ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
				  </div>
				  <div class="form_row_right">
				    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.QTYSTOCK'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.QTYSTOCK'); ?> </span> </label>
                  <div class="info100">
                    <div>
                    <input class="xs_small notzero" onkeypress="return isNumber(event)"  maxlength="9" type="text" name="qty" value="<?php echo e(isset($getinfos->pro_qty) ? $getinfos->pro_qty : ''); ?>" id="qty" required="">
                    </div>
                  </div>
				  </div>
          
                </div>
                <div class="form_row ">
				          <div class="form_row_left">
                   <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea class="english" cols="50" rows="4" maxlength="500" id="description" name="description"><?php echo e(isset($getinfos->pro_desc) ? $getinfos->pro_desc : ''); ?></textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea cols="50" rows="4" id="description_ar"  maxlength="500" name="description_ar"><?php echo e(isset($getinfos->pro_desc_ar) ? $getinfos->pro_desc_ar : ''); ?></textarea>
                      </div>
                    </div>
				  </div>
				  
                </div>
                <!-- form_row -->
                <!-- form_row -->
                <div class="form_row">
                <div class="form_row_left">
                <div class="english">
                  
                <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                <input type="hidden" name="itemid"  value="<?php echo e(request()->itemid); ?>">
                <input type="hidden" name="autoid"  value="<?php echo e(request()->autoid); ?>">
                  <input type="submit" name="submit" value="Submit">
                </div>
                <div class="arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                </div></div>
                <!-- form_row -->
              </form>
              <!-- one-call-form -->
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
        <!-- global_area -->
      </div>
    </div>
    <!-- right_panel -->
  </div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                    title: {
                    required: true,
                    },
                    title_ar: {
                    required: true,
                    },
                    price: {
                    required: true,
                    },
                     "flowertype[]": {
                    required: true,
                    },
                    wraptype: {
                    required: true,
                    },
                    wrapdesign: {
                    required: true,
                    },
                    qty: {
                    required: true,
                    },
                    description: {
                    required: true,
                    },
                    description_ar: {
                    required: true,
                    },
                    <?php if(isset($getinfos->pro_Img) && $getinfos->pro_Img!=''): ?>
                    img: {
                    required: false,
                    accept:"png|jpe?g|gif",
                    },
                    <?php else: ?>
                    img: {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },
                   <?php endif; ?>
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
                title: {
              required:  " <?php echo e(trans('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                },  

                title_ar: {
                required:  " <?php echo e(trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                },
                price: {
                required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?>",
                },
                qty: {
                required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_QUANTITY')); ?> <?php endif; ?>",
                },
                "flowertype[]": {
                required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_FLOWER_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_SELECT_FLOWER_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_FLOWER_TYPE')); ?> <?php endif; ?>",
                }, 
                wraptype: {
                required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_WRAP_TYPE')); ?> <?php endif; ?>",
                },
                wrapdesign: {
                required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_DESIGN')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_DESIGN')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_WRAP_DESIGN')); ?> <?php endif; ?>",
                },

                description: {
             required:  " <?php echo e(trans('mer_en_lang.MER_VALIDATION_ABOUT')); ?> ",
                }, 

                description_ar: {
                required:  " <?php echo e(trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR')); ?> ",
                },
                img: {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                },  

                      
                },
                invalidHandler: function(e, validation){
                   
                  var valdata=validation.invalid;

 <?php if($mer_selected_lang_code !='en'): ?>
                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                   if (typeof valdata.description != "undefined" || valdata.description != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }
                  if (typeof valdata.flowertype != "undefined" || valdata.flowertype != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                  if (typeof valdata.wraptype != "undefined" || valdata.wraptype != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                  if (typeof valdata.wrapdesign != "undefined" || valdata.wrapdesign != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }
                  if (typeof valdata.qty != "undefined" || valdata.qty != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                 
                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }
                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }




<?php else: ?>


                   if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.flowertype != "undefined" || valdata.flowertype != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.wraptype != "undefined" || valdata.wraptype != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.wrapdesign != "undefined" || valdata.wrapdesign != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.qty != "undefined" || valdata.qty != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.description != "undefined" || valdata.description != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                <?php endif; ?>

                  },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
 <script src="<?php echo e(url('')); ?>/themes/js/jquery.min.js"></script>
<script src="<?php echo e(url('')); ?>/themes/js/multiple-select.js"></script>
<?php if($mer_selected_lang_code !='en'): ?>
<script>
    $(function() {
        $('#flowertype').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });

          

    });
</script>
<?php else: ?>

<script>
    $(function() {
        $('#flowertype').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });

          

    });
</script>

<?php endif; ?>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
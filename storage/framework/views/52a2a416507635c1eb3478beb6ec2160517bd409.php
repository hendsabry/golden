<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $goldandjewelrybig_leftmenu  =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="right_panel">
    <div class="inner">
      <header> <?php if(request()->serviceid==''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_GOLD_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_GOLD_PRICE')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_GOLD_PRICE')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_GOLD_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_GOLD_PRICE')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_GOLD_PRICE')); ?> <?php endif; ?> </h5>
        <?php endif; ?> </header>
      
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
               <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?>
              <span id='allreq' style="display: none;"> <?php if(Lang::has(Session::get('mer_lang_file').'.Complete_all_the_price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Complete_all_the_price')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Complete_all_the_price')); ?> <?php endif; ?></span>
              <form name="form" id="goldprice" method="post" action="<?php echo e(route('storegoldandjewelryprice')); ?>" enctype="multipart/form-data" onsubmit="return validate()">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <?php for ($i=0; $i <=4 ; $i++) { 
                      if($i==0){ $c=16;}
                      if($i==1){ $c=18;}
                      if($i==2){ $c=21;}
                      if($i==3){ $c=22;}
                      if($i==4){ $c=24;}
                    ?>
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english">Caliber <?php echo e($c); ?> (price/gm)</span> </label>
                    <div class="info100">
                      <div class="english">
                      <input type="text" maxlength="6" class="english" name="price[]" value="<?php echo e(isset($getCalprice[$i]->price) ? $getCalprice[$i]->price : ''); ?>" id="title" class="xs_small valid" onkeypress="return isNumber(event)">
                      <input type="hidden" name="title[]" value="<?php echo e($c); ?>">
                      </div>                    
                    </div>
                  </div>
                   
                 <?php }  ?>                   
                  
                 
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                      <input type="hidden" name="hid" value="<?php echo e(request()->hid); ?>">
                      <input type="hidden" name="itemid" value="<?php echo e(request()->itemid); ?>">
                      <input type="hidden" name="serviceid" value="<?php echo e(request()->serviceid); ?>">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script type="text/javascript">

function validate(){

  var eduInput = document.getElementsByName('price[]');
  for (i=0; i<eduInput.length; i++)
    {
     if (eduInput[i].value == "")
      {
       jQuery('#allreq').css('display','');    
       return false;
      }
    }
}

</script>
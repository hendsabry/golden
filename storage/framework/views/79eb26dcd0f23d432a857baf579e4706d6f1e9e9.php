<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $acoustics_leftmenu =1; ?> 
 
<!--start merachant-->
<div class="merchant_vendor">
 <?php
  $id                = $id;
  $hid               = $hid;
  $oid               = $oid;
  $cusid             = $cusid;
  $opid              = $opid;
   $Cid=$opid;
  $getCustomer       = Helper::getuserinfo($cusid);
  $shipaddress       = Helper::getgenralinfo($oid);  
  $cityrecord        = Helper::getcity($shipaddress->shipping_city);
  $getorderedproduct = Helper::getorderedproduct($oid);
  $getsearchedproduct = Helper::searchorderedDetails($oid);
?>
  <!--left panel-->
 <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!--left panel end-->
  <!--right panel-->


  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order_Detail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order_Detail')); ?> <?php endif; ?>
        <a href="<?php echo e(url('/')); ?>/acoustic-order/<?php echo e($id); ?>/<?php echo e($hid); ?>/<?php echo e($opid); ?>" class="order-back-page"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BACK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?> <?php endif; ?></a></h5></div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDRESS')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} ?> <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="<?php echo e(url('')); ?>/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?></label>
                    <div class="info100" > <?php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      ?> </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} ?></div>
                  </div>
                  <?php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ ?>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.transaction_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.transaction_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.transaction_id')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} ?></div>
                  </div>

                  <?php } ?>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD')); ?> <?php endif; ?></label>
                    <div class="info100"> <?php echo e($productdata[0]->shippingMethod); ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE')); ?> <?php endif; ?></label>
                    <div class="info100"> SAR  <?php echo e($productdata[0]->shippingPrice); ?></div>
                    <!-- box -->
                  </div>

                    
                  <?php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ ?>
                    <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONDATE')); ?> <?php endif; ?></label>
                    <div class="info100"> 
                       
                      <?php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      { 
                       $ordertime=strtotime($getsearchedproduct->occasion_date); 
                       $orderedtime = date("d M Y",$ordertime);                    
                      echo $orderedtime;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      ?> </div>
                  </div>
                  <?php } ?>

                </div>
                <!--global end-->
              </div>
              <!-- hall-od-top -->
               <?php $pro_title='pro_title';   ?>
                   <?php if($mer_selected_lang_code !='en'): ?>
                                         <?php $pro_title='pro_title_'.$mer_selected_lang_code; ?>
                   <?php endif; ?> 
                <?php 
      $basetotal = 0;
      $i = 1;
	   $couponcode=0; $couponcodeinfo='';
      if(count($productdata) > 0){
      foreach($productdata as $val)
      {
		  $basetotal            = ($basetotal+$val->total_price);
		  $pid                  = $val->product_id;

      $couponcode=$couponcode+$val->coupon_code_amount;;
        $couponcodeinfo=$val->coupon_code;

        
        $getrentedProductattr = Helper::getorderedProductattr($val->product_id,$val->order_id);

		  if(isset($pid) && ($pid!='' || $pid!='0'))
		  {
		     $serviceInfo = Helper::getProduckInfo($val->product_id);		  
		     if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$priceperday = $serviceInfo->pro_disprice;}else{$priceperday = $serviceInfo->pro_price;}			 
		  }
      ?>
      <?php if($val->product_id!=27){ ?>
      <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
        <div class="style_area">
          <?php if($i==1){?>
          <div class="style_head"><?php if(Lang::has(Session::get('mer_lang_file').'.item_information')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.item_information')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.item_information')); ?> <?php endif; ?></div>
          <?php } ?>
          <div class="style_box_type">
            <div class="sts_box">
			 <?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){ ?>
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_category_name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_category_name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_category_name')); ?> <?php endif; ?></div>
                <div class="style_label_text"> <?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){echo @$serviceInfo->$pro_title;} ?></div>
              </div>
			 <?php }else{
			 $enq_id = Helper::quoteRequested($val->shop_id);
			 $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
			 ?>
			   
			 <?php }
			 if(isset($val->total_price) && $val->total_price!=''){  ?>
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?></div>
                <div class="style_label_text"> SAR <?php echo e(number_format($val->total_price,2)); ?></div>
              </div>
			  <?php }
			  if(isset($val->quantity) && ($val->quantity!='' || $val->quantity!='0')){ ?> 
              <div class="style_left">			  
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_QUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_QUANTITY')); ?> <?php endif; ?></div>
                <div class="style_label_text"> <?php echo e(isset($val->quantity) ? $val->quantity : ''); ?></div>
              </div>
			  <?php } ?>
            </div>
			<?php if(isset($serviceInfo->pro_Img) && ($serviceInfo->pro_Img!='')){?>
            <div class="sts_box">
			 <?php  if($priceperday!='' || $priceperday!='0'){ ?>
             <!--  <div class="style_left"> 
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.Price_Per_KM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Price_Per_KM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Price_Per_KM')); ?> <?php endif; ?></div>
                <div class="style_label_text"> SAR <?php echo e(isset($priceperday) ? $priceperday : ''); ?></div>
              </div> -->
			  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){?>
            
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_IMAGE')); ?> <?php endif; ?></div>
                <div class="style_label_text"> <?php if($serviceInfo->pro_Img): ?> <img src="<?php echo e($serviceInfo->pro_Img); ?>" alt="" width="150"> <?php else: ?>
                  No image
                  <?php endif; ?> </div>
              
              
            </div>
			<?php } ?>

			  <?php } ?> 

             <?php if(isset($getrentedProductattr) && $getrentedProductattr!=''){ ?>

           
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BOOKING_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BOOKING_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BOOKING_DATE')); ?> <?php endif; ?></div>
                <div class="style_label_text"> 
                  <?php $productbook=strtotime($getrentedProductattr->rental_date); 
                       $orderedproductbook = date("d M Y",$productbook);                    
                      echo $orderedproductbook;
                      ?>
                 <?php echo e($getrentedProductattr->rental_time); ?>

                  
                </div>
                
              </div>
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.RETURNING_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.RETURNING_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.RETURNING_DATE')); ?> <?php endif; ?></div>
                <div class="style_label_text"> 
                    <?php $productreturn=strtotime($getrentedProductattr->return_date); 
                       $orderedproductreturn = date("d M Y",$productreturn);                    
                      echo $orderedproductreturn;
                      ?>
                   <?php echo e($getrentedProductattr->return_time); ?>

                  
                </div>
              </div>
               <?php } ?>
            </div>

    <?php  if(isset($val->insurance_amount) && ($val->insurance_amount!='' || $val->insurance_amount>'0')){ ?> 
            <div class="sts_box">
                
                <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.INSURANCEAMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.INSURANCEAMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.INSURANCEAMOUNT')); ?> <?php endif; ?></div>
                <div class="style_label_text"> 
                    
                   SAR <?php echo e(number_format($val->insurance_amount,2)); ?>

                  
                </div>
              </div>


            </div>
<?php } ?>



			<?php } ?>
          </div>
        </div>
      </div>
      <?php $i++;} } ?>
      <div class="merchant-order-total-area">
                <div class="merchant-order-total-line">
                      <?php if(isset($couponcode) && $couponcode>=1): ?>
             <?php echo e((Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')); ?>: 

             <?php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          ?>  
          <br>

          <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')); ?>: <?php echo e($couponcodeinfo); ?>

 <br>
          <?php endif; ?>



            <?php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             ?>


                  <?php echo e((Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')); ?>: 

                    <?php $totalvatprice=$basetotal;
                     $vatamount = Helper::calculatevat($shipaddress->order_id,$calnettotalamount);
                     echo 'SAR '.number_format(($vatamount),2);
                ?>

          </div>
		  <div class="merchant-order-total-line">
        <?php echo e((Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($MER_OUR_LANGUAGE.'.TOTAL_PRICE')); ?>:
        <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($calnettotalamount+$vatamount),2);
				 } ?>
      </div> </div>
      <?php } ?>

            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
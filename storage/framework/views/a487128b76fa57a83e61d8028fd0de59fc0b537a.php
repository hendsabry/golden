<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
?>
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($vendordetails->mc_img); ?>" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
          <?php } ?>
          <!--<li><a href="#choose_package"><?php echo e((Lang::has(Session::get('lang_file').'.Choose_Package')!= '')  ?  trans(Session::get('lang_file').'.Choose_Package'): trans($OUR_LANGUAGE.'.Choose_Package')); ?></a></li>-->
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?><li><img src="<?php echo e(str_replace('thumb_','',$vendordetails->image)); ?>" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
               <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>               
                    <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"><?php echo e($vendordetails->mc_name); ?></div>
          <div class="detail_hall_description"><?php echo e($vendordetails->address); ?></div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
          <div class="detail_about_hall"><div class="comment more"><?php echo e($vendordetails->mc_discription); ?></div></div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: <span><?php
		    $getcityname = Helper::getcity($vendordetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?></span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
	    <a name="video" class="linking">&nbsp;</a>
       
<?php if(trim($vendordetails->mc_video_url)!=''): ?>
        <div class="service-video-area">
          <div class="service-video-cont"><?php echo e($vendordetails->mc_video_description); ?></div>
          <div class="service-video-box">
            <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
<?php endif; ?>

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
				 <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				 <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
				  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>			  
                </ul>
              </div>
            </section>
          </div>
        </div>
		<?php } ?>
      </div>
      <!-- service-mid-wrapper -->
	  <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="<?php echo e(url('')); ?>/abayareadymadedetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.READY_MADE')!= '')  ?  trans(Session::get('lang_file').'.READY_MADE'): trans($OUR_LANGUAGE.'.READY_MADE')); ?></a></li>
                <li><a href="<?php echo e(url('')); ?>/abayatailersdetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>" class="select"><?php echo e((Lang::has(Session::get('lang_file').'.TAILOR')!= '')  ?  trans(Session::get('lang_file').'.TAILOR'): trans($OUR_LANGUAGE.'.TAILOR')); ?></a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  <?php 
         $k=count($productlist);  ?>


         <?php if($k >=1): ?>
      <div class="service-display-section"> 
	  <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
		 <div class="style">Styles</div>
          <div class="diamond_main_wrapper dmd">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> <?php  $i=1; ?>
              
                <?php if($k<6){ ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($k==3){ $cl='category_wrapper'.$i; }else{ $cl=''; } $img = str_replace('thumb_','',$getallcats->pro_Img); ?>
                  <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                    <div class="category_wrapper <?php echo e($cl); ?>" style="background:url(<?php echo e(isset($img) ? $img : ''); ?>);">
                      <div class="category_title">
                        <div class="category_title_inner">
                          <?php echo e($getallcats->pro_title); ?>

                        </div>
                      </div>
                    </div>
                    </a> </div>
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 6th-------------->
                <?php }elseif($k==6){ ?>
                <?php $j=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                  <?php if($j==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($j==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($j==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($j==6){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php echo e($getallcats->pro_title); ?>

                                </div>
                              </div>
                            </div>
                            </a> <?php if($j==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==4){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($j==5){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($j==6){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $j=$j+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 7th-------------->
                <?php }elseif($k==7){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                  
                  <?php if($l==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($l==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($l==7){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php echo e($getallcats->pro_title); ?>

                                </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==7){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 8th-------------->
                <?php }elseif($k==8){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                  <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                  <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                  
                  <?php if($l==1){ $classrd='category_wrapper1'; ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_3of5 rows5row"> <?php } ?> 
                      <?php if($l==4){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?>
                          <?php if($l==8){ $classrd='category_wrapper9'; ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php echo e($getallcats->pro_title); ?>

                                </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==8){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                
                <?php }elseif($k==9){ ?>
                <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                  <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
                  
                  
                  <?php if($i==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($i==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($i==4){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($i==7){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?> 
                          <?php if($i==9){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner">
                            <?php echo e($getallcats->pro_title); ?>

                            </span></span> </span> </a> <?php if($i==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    <?php if($i==9){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?> 
                  
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php } ?> 
				
				</div>
          </div>
		  </div>
		  <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
		  <div align="center"><?php echo e($productlist->links()); ?></div> 
		  </div>
        <!-- service-display-right -->
	
		<?php if(isset($productlist[0]->pro_disprice) && $productlist[0]->pro_disprice!='0'){$getPrice = $productlist[0]->pro_disprice;}else{$getPrice = $productlist[0]->pro_price;}?>
        <div class="service-display-left tailors-l">
		 <input type="hidden" id="category_id" name="category_id" value="<?php echo e($category_id); ?>">
		 <input type="hidden" id="subcat_id" name="subcat_id" value="<?php echo e($subcat_id); ?>">
		 <input type="hidden" id="shop_id" name="shop_id" value="<?php echo e($shop_id); ?>">
		 <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9"/>
		 <input type="hidden" name="actiontype" value="abayatailersdetail">
		 <input type="hidden" name="cart_sub_type" value="abaya">
		 <input type="hidden" name="cart_type" value="shopping">
		 <b><?php echo e(Session::get('status')); ?></b>
          <span id="selectedproduct">
		  <input type="hidden" id="product_id" name="product_id" value="<?php if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!=''){echo $productlist[0]->pro_id;}?>">
		  <input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>">
		  <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->pro_mr_id) && $productlist[0]->pro_mr_id!=''){echo $productlist[0]->pro_mr_id;}?>">
		  <div class="service-left-display-img product_gallery">

         <?php    $pro_id = $productlist[0]->pro_id;


          ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 



      </div>
          <div class="service-product-name"><?php if(isset($productlist[0]->pro_title) && $productlist[0]->pro_title!=''){echo $productlist[0]->pro_title;}?></div>
		  <div class="service-beauty-description"><?php if(isset($productlist[0]->pro_desc) && $productlist[0]->pro_desc!=''){echo nl2br($productlist[0]->pro_desc);}?>
        <br>
        
      </div>
		  </span>
<div class="service-beauty-description"><?php echo e((Lang::has(Session::get('lang_file').'.NO_OF_DAY')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_DAY'): trans($OUR_LANGUAGE.'.NO_OF_DAY')); ?>: <span id="no_of_day"><?php echo e($productlist[0]->pro_no_of_purchase); ?> </span></div>


<?php 
      if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!='')
      { 
        $z = 0;
        $getalldata = Helper::getProduckOptionInfo($productlist[0]->pro_id,$productlist[0]->pro_mr_id); 

      if(count($getalldata) > 0)
      {
      ?>
         

        
<div class="service_selecrrow dress_selecrrow" id="ptattrsizeenone">
  <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Fabrics')!= '')  ?  trans(Session::get('lang_file').'.Fabrics'): trans($OUR_LANGUAGE.'.Fabrics')); ?></div>
<div class="checkout-form-bottom" id="ptattrsize">
  <div id="content-1" class="content mCustomScrollbar">
<div class="fabs_op">
<?php foreach($getalldata as $val){ ?>
     <?php if($val->fabric_title!=''): ?>
	<div class="fabric_block">
	<div class="fabric_img_block"> <img src="<?php echo $val->fabric_img;?>"> </div>
	<div class="fabric_title_block">  <?php echo $val->fabric_title;?></div>
	<div class="fabric_price_block">  <?php echo $Current_Currency; ?> <?php echo $val->fabric_price;?></div>
	<div class="choose-line-radio"> 
    <input type="radio" value="<?php echo $val->short_name;?>" name="product_fabrics"   onchange="return showPriceDetail(this.value)">   
	<label for="name">&nbsp;</label></div>
	</div>
   <?php endif; ?>
<?php } ?>
</div>
</div>
</div>
</div>
<?php  $z++;}} ?>
<span id="plzselectf" <?php if($z!=0){ ?>style="display:none;"<?php } ?>>


<div class="service_quantity_box abayaqty">
       <div class="service_qunt"><?php echo e((Lang::has(Session::get('lang_file').'.Quantity')!= '')  ?  trans(Session::get('lang_file').'.Quantity'): trans($OUR_LANGUAGE.'.Quantity')); ?> </div>
              <div class="service_qunatity_row">
                <div class="td td2 quantity food-quantity" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.Quantity')!= '')  ?  trans(Session::get('lang_file').'.Quantity'): trans($OUR_LANGUAGE.'.Quantity')); ?>">
                  <div class="quantity">
                    <button type="button" id="sub" class="sub" onclick="return pricecalculation('remove');"></button>
                    <input type="number" name="itemqty" id="qty" value="1" min="1" max="9" onblur="return pricecalculation('add');">
                    <button type="button" id="add" class="add" onclick="return pricecalculation('add');"></button>
                  </div>
                </div>
              </div>
            </div>



  
          <div class="container_total_price abaya_price"><span class="newprice"><?php if(isset($getPrice) && $getPrice!='' && $getPrice >=1 && $getPrice < $productlist[0]->pro_price){echo '<del>SAR '.number_format($productlist[0]->pro_price,2).'</del> <br />';} ?></span><br/> <?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: <span class="cont_final_price total_price">SAR <?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->pro_price){echo number_format($getPrice,2);}else{echo number_format($productlist[0]->pro_price,2);} ?></span></div>
          <div class="akaya_bodymesurment"><a href="#pop" class="try_now_page_one body_btn"><?php echo e((Lang::has(Session::get('lang_file').'.BODY_MEASUREMENT')!= '')  ?  trans(Session::get('lang_file').'.BODY_MEASUREMENT'): trans($OUR_LANGUAGE.'.BODY_MEASUREMENT')); ?></a></div>
		  </span>
          <!--<div class="addto_cart">
            <input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn addto_cartbtn">
          </div>-->
			<div id="try_now_page_one_popup" class="new-popup measurement-popup-outer" style="display:none;"> 
          <?php echo Form::open(['url' => 'addtocartforshopping', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

   <input type="hidden" id="category_id" name="category_id" value="<?php echo e($category_id); ?>">
     <input type="hidden" id="subcat_id" name="subcat_id" value="<?php echo e($subcat_id); ?>">
     <input type="hidden" id="shop_id" name="shop_id" value="<?php echo e($shop_id); ?>">
     <input type="hidden" name="itemqty" id="itemqty_on_popup" value="1" min="1" max="9"/>
     <input type="hidden" name="actiontype" value="abayatailersdetail">
     <input type="hidden" name="cart_sub_type" value="abaya">
     <input type="hidden" name="cart_type" value="shopping">
     <input type="hidden" name="fabric_type" value="">
     <b><?php echo e(Session::get('status')); ?></b>
          <span id="selectedproduct">
      <input type="hidden" id="product_ids" name="product_id" value="<?php if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!=''){echo $productlist[0]->pro_id;}?>">
      <input type="hidden" id="priceIds" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>"> 
      <input type="hidden" id="vendor_ids" name="vendor_id" value="<?php if(isset($productlist[0]->pro_mr_id) && $productlist[0]->pro_mr_id!=''){echo $productlist[0]->pro_mr_id;}?>">


			 <a href="javascript:void(0);" class="b-close">X</a>
		 
			  <div class="measurement-popup">  
				<div class="main_title"><?php echo e((Lang::has(Session::get('lang_file').'.BODY_MEASUREMENT')!= '')  ?  trans(Session::get('lang_file').'.BODY_MEASUREMENT'): trans($OUR_LANGUAGE.'.BODY_MEASUREMENT')); ?></div>
				<div class="measurement-popup-img"><img src="<?php echo e(url('')); ?>/themes/images/measurement.jpg" alt=""  /></div>
				<div class="measurement-divder">&nbsp;</div>
				<div class="measurement-form">
				  <div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')); ?></div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="length" id="length" onkeypress="return isNumber(event)"  onkeyup="return islengh(event,this.value)" onkeydown="return islengh(event,this.value)"  maxlength="5"/>
             
					  <div class="parametar"><?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></div>
					</div>
             <label for="length" class="error "></label><span class="len" style="color: red;"></span>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')); ?></div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="chest_size" id="chest_size" onkeypress="return isNumber(event)" maxlength="4" onkeyup="return islenghs(event,this.value,1)" onkeydown="return islenghs(event,this.value,1)"/>
         
					  <div class="parametar"><?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></div>
					</div>
           <label for="chest_size" class="error "></label><span class="len1" style="color: red;"></span>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')); ?></div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="waist_size" id="waist_size" onkeypress="return isNumber(event)" maxlength="4" onkeyup="return islenghs(event,this.value,2)" onkeydown="return islenghs(event,this.value,2)"/>
           
					  <div class="parametar"><?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></div>
					</div>
          <label for="waist_size" class="error"></label><span class="len2" style="color: red;"></span>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')); ?></div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="shoulders" id="shoulders" onkeypress="return isNumber(event)" maxlength="4" onkeyup="return islenghs(event,this.value,3)" onkeydown="return islenghs(event,this.value,3)"/>
            
					  <div class="parametar"><?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></div>
					</div>
           <label for="shoulders" class="error"></label><span class="len3" style="color: red;"></span>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')); ?></div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="neck" id="neck" onkeypress="return isNumber(event)" maxlength="4" onkeyup="return islenghs(event,this.value,4)" onkeydown="return islenghs(event,this.value,4)"/>
      
					  <div class="parametar"><?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></div>
					</div>
          <label for="arm_length" class="error"></label><span class="len4" style="color: red;"></span>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')); ?></div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="arm_length" id="arm_length" onkeypress="return isNumber(event)" maxlength="4" onkeyup="return islenghs(event,this.value,5)" onkeydown="return islenghs(event,this.value,5)"/>
           
					  <div class="parametar"><?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></div>
					</div>
          <label for="arm_length" class="error "></label><span class="len5" style="color: red;"></span>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')); ?></div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="wrist_diameter" id="wrist_diameter" onkeypress="return isNumber(event)" maxlength="4" onkeyup="return islenghs(event,this.value,6)" onkeydown="return islenghs(event,this.value,6)"/>
          
					  <div class="parametar"><?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></div>
					</div>
          <label for="wrist_diameter" class="error "></label><span class="len6" style="color: red;"></span>
				  </div>
				  <!-- measurement-form-line -->
				  <!--div class="measurement-form-line">
					<div class="measurement-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.CUTTING')!= '')  ?  trans(Session::get('lang_file').'.CUTTING'): trans($OUR_LANGUAGE.'.CUTTING')); ?></div>
					<div class="measurement-form-bottom">
					  <div class="measurement-radio-line">
						<input type="radio" name="cutting" id="Normal" value="Normal"/>
						<label for="Normal"><?php echo e((Lang::has(Session::get('lang_file').'.NORMAL')!= '')  ?  trans(Session::get('lang_file').'.NORMAL'): trans($OUR_LANGUAGE.'.NORMAL')); ?></label>
					  </div>
					  <div class="measurement-radio-line">
						<input type="radio" name="cutting" id="Ample" value="Ample"/>
						<label for="Ample"><?php echo e((Lang::has(Session::get('lang_file').'.AMPLE')!= '')  ?  trans(Session::get('lang_file').'.AMPLE'): trans($OUR_LANGUAGE.'.AMPLE')); ?></label>
					  </div>
					  <div class="measurement-radio-line">
						<input type="radio" name="cutting" id="Curved" value="Curved"/>
						<label for="Curved"><?php echo e((Lang::has(Session::get('lang_file').'.CURVED')!= '')  ?  trans(Session::get('lang_file').'.CURVED'): trans($OUR_LANGUAGE.'.CURVED')); ?></label>
					  </div>
					  <label for="cutting" class="error"></label>
					</div>
				  </div-->
				  <!-- measurement-form-line -->
				</div>
				<!-- measurement-form -->
				<div class="measurement-btn-area">
				  <input style="margin-top:60px;" type="submit" class="form-btn big-btn btn-info-wisitech" value="<?php echo e((Lang::has(Session::get('lang_file').'.CONTINUE_WITH_THESE_MEASURES')!= '')  ?  trans(Session::get('lang_file').'.CONTINUE_WITH_THESE_MEASURES'): trans($OUR_LANGUAGE.'.CONTINUE_WITH_THESE_MEASURES')); ?>" />
				</div>
			  </div>
          <?php echo Form::close(); ?>

			</div>
        </div>
	<?php else: ?>
<div class='service-display-section' style='text-align:center;'> No record found </div>

  <?php endif; ?>
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
	
         <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   
	  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
</div>
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script>
/* Mobile Number Validation */
 function islengh(evt,lenval) {
  
   lenval = parseFloat(lenval);
if(lenval >=100 && lenval < 1000)
{
 $('.len').html('');
 
}
else
{

var Please_enter_correct_value = "<?php if(Lang::has(Session::get('lang_file').'.Please_enter_correct_value')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Please_enter_correct_value')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Please_enter_correct_value')); ?> <?php endif; ?>";
 
  $('.len').html(Please_enter_correct_value);
    $('.btn-info-wisitech').prop('disabled','disabled');
}



var length = parseFloat($('#length').val());
var chest_size = parseFloat($('#chest_size').val());
var waist_size = parseFloat($('#waist_size').val());
var shoulders = parseFloat($('#shoulders').val());
 var arm_length = parseFloat($('#arm_length').val());
var neck = parseFloat($('#neck').val());
var wrist_diameter = parseFloat($('#wrist_diameter').val());
 

    if((chest_size >=10 && chest_size < 100)  && (waist_size >=10 &&  waist_size < 100) && (shoulders >=10  && shoulders < 100) && (arm_length >=10 && arm_length < 100) && (neck >=10 && neck < 100) && (wrist_diameter >=10 && wrist_diameter < 100) && (length >=100 && length < 1000))
    {
 $('.btn-info-wisitech').prop('disabled',false);
      
    }




 
 }

  function islenghs(evt,lenval,pos) {
 
   lenval = parseFloat(lenval);
if(lenval >=10 && lenval < 100)
{
 $('.len'+pos).html('');
 
}
else
{
  var Please_enter_correct_value = "<?php if(Lang::has(Session::get('lang_file').'.Please_enter_correct_value')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Please_enter_correct_value')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Please_enter_correct_value')); ?> <?php endif; ?>";
  $('.len'+pos).html(Please_enter_correct_value);
    $('.btn-info-wisitech').prop('disabled','disabled');
}
 
var length = parseFloat($('#length').val());
var chest_size = parseFloat($('#chest_size').val());
var waist_size = parseFloat($('#waist_size').val());
var shoulders = parseFloat($('#shoulders').val());
 var arm_length = parseFloat($('#arm_length').val());
var neck = parseFloat($('#neck').val());
var wrist_diameter = parseFloat($('#wrist_diameter').val());
 

    if((chest_size >=10 && chest_size < 100)  && (waist_size >=10 &&  waist_size < 100) && (shoulders >=10  && shoulders < 100) && (arm_length >=10 && arm_length < 100) && (neck >=10 && neck < 100) && (wrist_diameter >=10 && wrist_diameter < 100) && (length >=100 && length < 1000))
    {
 $('.btn-info-wisitech').prop('disabled',false);
      
    }

 }


</script>
<script type="text/javascript">
function showPriceDetail(act)
{
    var product_id = $('#product_id').val();
    jQuery('[name=fabric_type]').val(act);
	if(act!='')
	{
	   $('#plzselectf').show();
	   $.ajax({
	   type:"GET",
	   url:"<?php echo e(url('getfabric')); ?>?product_id="+product_id+'&product_fabric='+act,
	   async: false,
	   success:function(res)
	   { 
		   var totalprice = parseInt(res).toFixed(2);
           totalprice = getChangedPrice(totalprice); 
           <?php $Cur = Session::get('currency'); ?>
		   $('#priceId').val(totalprice);
		   //alert(totalprice);
		   $('#mser_priceId').val(totalprice);
		   $('.total_price').html('<?php echo $Cur;?> '+totalprice);
	   }
	 });
	}
	else
	{
	  $('#plzselectf').hide();
	}
}
function getChangedPrice(price)
{
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
}
function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
}
function nl2br (str, is_xhtml) 
{   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}


function showProductDetail(str,vendorId)
{
	   $.ajax({
	   type:"GET",
	   url:"<?php echo e(url('getShoppingProductInfo')); ?>?product_id="+str+'&vendor_id='+vendorId,
	   success:function(res)
	   {               
		if(res)
		{ 
		  $('html, body').animate({
				scrollTop: ($('.service-display-left').first().offset().top)
		  },500);
		  var json = JSON.stringify(res);
		  var obj = JSON.parse(json);
          console.log(obj);
		  length=obj.productdateshopinfo.length;
		  //alert(length);
		  if(length>0)
		  {
	         for(i=0; i<length; i++)
		     {        
			  if(obj.productdateshopinfo[i].pro_discount_percentage !='' && obj.productdateshopinfo[i].pro_discount_percentage >=1){var getPrice = parseFloat(obj.productdateshopinfo[i].pro_disprice).toFixed(2); } else {var getPrice = parseFloat(obj.productdateshopinfo[i].pro_price).toFixed(2);}

			  if(getPrice < obj.productdateshopinfo[i].pro_price)
			  {        
				  $('.newprice').html('<del>SAR '+obj.productdateshopinfo[i].pro_price+'</del>');	
				  $('.total_price').html('SAR '+getPrice);
			  }	
			  else
			  {
			     $('.total_price').html('SAR '+obj.productdateshopinfo[i].pro_price);	
				 $('.newprice').html('');	
			  }
 
        $('#no_of_day').html(obj.productdateshopinfo[i].pro_no_of_purchase); 
            $('#priceIds').val(getPrice);
            $('#vendor_ids').val(obj.productdateshopinfo[i].pro_mr_id);
            $('#product_ids').val(obj.productdateshopinfo[i].pro_id);


              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+getPrice+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+nl2br(obj.productdateshopinfo[i].pro_desc)+'</div>');     
              checkgallery(str);


                   
		     }
		  }	 

      if($.trim(obj.productattrsize) !=1)
      {
        $('#ptattrsize').html(obj.productattrsize);
      $('#ptattrsizeenone').css('display','block');
      $('#plzselectf').hide();
      }
      else
      {
        $('#ptattrsizeenone').css('display','none');
      $('#plzselectf').show();
      }
      $('html, body').animate({
        scrollTop: ($('.service-display-left').first().offset().top)
      },500); 


	    }
	   }
	 });
}
</script>
<script>
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script>
/* Mobile Number Validation */
 //  function isNumber(evt) {
 //   evt = (evt) ? evt : window.event;
 //   var charCode = (evt.which) ? evt.which : evt.keyCode;
 //   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
 //   return false;
 //   }
 //   return true;
 // }
</script>
<script>
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {       
          "length" : {
            required : true,
    
            maxlength: 6
          },  
		  "chest_size" : {
            required : true,

            maxlength: 6
          },
		  "waist_size" : {
            required : true,
       
            maxlength: 6
          },
		  "shoulders" : {
            required : true,
          
            maxlength: 6
          },
		  "neck" : {
            required : true,
    
            maxlength: 6
          },  
		  "arm_length" : {
            required : true,
     
            maxlength: 6
          },  
		  "wrist_diameter" : {
            required : true,
      
            maxlength: 6
          },  
		    
         },
         messages: {
          "length": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_LENGTH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_LENGTH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_LENGTH')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_MAX')); ?> <?php endif; ?>'
          },
		  "chest_size": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CHEST_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CHEST_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_CHEST_SIZE')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_MAX')); ?> <?php endif; ?>'
          },
		  "waist_size": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WAIST_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WAIST_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_WAIST_SIZE')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_MAX')); ?> <?php endif; ?>'
          },
		  "shoulders": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_SHOULDERS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_SHOULDERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_SHOULDERS')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_MAX')); ?> <?php endif; ?>'
          },
		  "neck": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NECK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NECK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NECK')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_MAX')); ?> <?php endif; ?>'
          },		  
		  "arm_length": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ARM_LENGTH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ARM_LENGTH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ARM_LENGTH')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_MAX')); ?> <?php endif; ?>'
          },
		  "wrist_diameter": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WRIST_DIA')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WRIST_DIA')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_WRIST_DIA')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_MAX')); ?> <?php endif; ?>'
          },
		  "cutting": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_REDIO_BUTTON')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_REDIO_BUTTON')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_REDIO_BUTTON')); ?> <?php endif; ?>'
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
</script>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
</script> 

<script type="text/javascript">
  



function pricecalculation(act)
{
  var no=1;
  var product_id      = document.getElementById('product_id').value;
  var currentquantity = document.getElementById('qty').value;
  var unititemprice   = document.getElementById('priceId').value;
  //alert(unititemprice);
  if(act=='add')
  {
    var qty = parseInt(currentquantity)+parseInt(no);     
  }
  else
  { 
    if(parseInt(currentquantity)==1)
    {
      var qty=parseInt(currentquantity)
    }
    else
    {
      var qty=parseInt(currentquantity)-parseInt(no);
    }
  }
    <?php $Cur = Session::get('currency'); ?>      
    var totalprice = $('#priceIds').val();
    var Tp =  parseFloat(qty) * parseFloat(unititemprice); 
    $('.total_price').html('<?php echo $Cur;?> '+Tp);
    //$('#priceId').val(Tp);
    $('#mser_priceId').val(Tp);
    $('#itemqty').val(qty);
    $('#itemqty_on_popup').val(qty);
}
 

</script>
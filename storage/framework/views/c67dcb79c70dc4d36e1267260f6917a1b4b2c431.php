<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
 
if($Current_Currency =='') { 
  $Current_Currency = 'SAR'; 
 } 
 
  ?>
  
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
    <!-- Display Message after submition -->
    
    <!-- Display Message after submition -->
    <form name="form" id="quote_view" method="post" action="<?php echo e(url('updaterequestaquoteview')); ?>" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

	<input type="hidden" name="last_id" value="<?php echo e($id); ?>" />
    <div class="myaccount_right" id="change_password">
	<div class="dash_select"> <a href="<?php echo e(route('my-request-a-quote')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE_DETAIL')); ?></a> </div>
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE_DETAIL')); ?></h1>
      <div class="field_group top_spacing_margin_occas myprofile_page"> 
	  
	    <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>   
		
		   
        <div class="checkout-box"> 
          <div class="checkout-form"> 
		    <div class="singer_price"><?php echo e(currency($vendoramt->price, 'SAR',$Current_Currency)); ?></div>
            <div class="vendor_reply"><b><?php if(Lang::has(Session::get('lang_file').'.VENDOR_REPLY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VENDOR_REPLY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VENDOR_REPLY')); ?> <?php endif; ?></b>: <?php  echo nl2br($vendoramt->comment);?></div>
		    <?php if(!empty($allinquiry->the_groooms_name)){ ?>
		    <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.THE_GROOM_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.THE_GROOM_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.THE_GROOM_NAME')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="groom_name" id="groom_name" value="<?php echo e($allinquiry->the_groooms_name); ?>" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->hall)){ ?>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HALL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HALL')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="hall" id="hall" type="text" maxlength="100" value="<?php echo e($allinquiry->hall); ?>" readonly="readonly">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->occasion_type)){
          $ocid=$allinquiry->occasion_type_id; 
          $ocassionnameformhelper=Helper::getOccasionName($ocid); 
          $mctitle_name = 'title'; 
        if(Session::get('lang_file')!='en_lang')
          {
              $mctitle_name = 'title_ar'; 
            }
            if($ocid>0){ $octype=$ocassionnameformhelper->$mctitle_name; }else{  $octype=$allinquiry->occasion_type; }
       ?>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.OCCASION_TYPE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_TYPE'): trans($OUR_LANGUAGE.'.OCCASION_TYPE')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="text" maxlength="150" name="occasion" id="occasion" value="<?php echo e($octype); ?>" readonly="readonly" class="t-box" />
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->city_id)){ ?>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?></div>
                <div class="checkout-form-bottom">
				  <?php 
				  $getcityname = Helper::getcity($allinquiry->city_id);
				  $mc_name='ci_name';if(Session::get('lang_file')!='en_lang'){$mc_name= 'ci_name_ar';} ?>
                  <input type="text" maxlength="150" name="city" id="city"  value="<?php echo e(isset($getcityname->$mc_name) ? $getcityname->$mc_name : ''); ?>" readonly="readonly"  class="t-box" />
                </div>                
              </div>
            </div>
			<?php }if(!empty($allinquiry->location)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOCATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOCATION')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box" type="text" name="location" maxlength="100" id="location" value="<?php echo e(isset($allinquiry->location) ? $allinquiry->location : ''); ?>" readonly="readonly">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->duration)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.DURATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DURATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DURATION')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="duration" id="duration" type="text" maxlength="50" value="<?php echo e(isset($allinquiry->duration) ? $allinquiry->duration : ''); ?>" readonly="readonly">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->date)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DATE')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box cal-t datetimepicker" name="date" id="date" value="<?php echo e(date('d M Y',strtotime($allinquiry->date) )); ?> <?php echo e($allinquiry->time); ?>" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->recording_section)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.RECORDING_SECTION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.RECORDING_SECTION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.RECORDING_SECTION')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="recording_section" id="recording_section" value="<?php echo e(isset($allinquiry->recording_section) ? $allinquiry->recording_section : ''); ?>" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->music_type)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.MUSIC_TYPE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MUSIC_TYPE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MUSIC_TYPE')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="music_type" id="music_type" value="<?php echo e(isset($allinquiry->music_type) ? $allinquiry->music_type : ''); ?>" readonly="readonly"  type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->singer_name)){

$mid=$allinquiry->music_id; 
          $singernameformhelper=Helper::nm_music($mid); 
          $singertitle_name = 'name'; 
        if(Session::get('lang_file')!='en_lang')
          {
              $singertitle_name = 'name_ar'; 
            }
            if($mid>0){ $ocsingertype=$singernameformhelper->$singertitle_name; }else{  $ocsingertype=$allinquiry->singer_name; }
       ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.SINGER_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SINGER_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SINGER_NAME')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="singer_name" id="singer_name" value="<?php echo e($ocsingertype); ?>" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->occasion_type)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="text" maxlength="150" name="occasion" id="occasion" value="<?php echo e($octype); ?>" readonly="readonly" class="t-box" />
                </div>
              </div>
            </div>
			<?php }  ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.Your_COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Your_COMMENTS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Your_COMMENTS')); ?> <?php endif; ?></div>
                <div class="checkout-form-bottom">
                  <textarea class="text-area" name="comments" readonly="readonly" id="comments"><?php echo e(isset($allinquiry->user_comment) ? $allinquiry->user_comment : ''); ?></textarea>
                </div>
              </div>
            </div>
		 
			<!--div class="checkout-form-cell">
                <div class="checkout-form-top"></div>
                <div class="checkout-form-bottom">
                  <div class="gender-radio-box">
                    <input id="confirm" name="status" type="radio" value="3" <?php if($allinquiry->status==3){echo 'Checked';} ?>>
                    <label  for="confirm"><?php echo e((Lang::has(Session::get('lang_file').'.CONFIRMED')!= '')  ?  trans(Session::get('lang_file').'.CONFIRMED'): trans($OUR_LANGUAGE.'.CONFIRMED')); ?></label>
                  </div>
                  <div class="gender-radio-box">
                    <input id="deny" name="status" type="radio" value="4" <?php if($allinquiry->status==4){echo 'Checked';} ?>>
                    <label for="deny"><?php echo e((Lang::has(Session::get('lang_file').'.DENY')!= '')  ?  trans(Session::get('lang_file').'.DENY'): trans($OUR_LANGUAGE.'.DENY')); ?></label>
                  </div>
                </div>
                <?php if($errors->has('gender')): ?><span class="error"><?php echo e($errors->first('gender')); ?></span><?php endif; ?>
              </div-->
              <!-- checkout-form-cell -->
            </div>
			<?php 
			if($allinquiry->status!=3){if($allinquiry->status!=4){ ?>
			<!--div class="myprf_btn">
        <input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.UPDATE')!= '')  ?  trans(Session::get('lang_file').'.UPDATE'): trans($OUR_LANGUAGE.'.UPDATE')); ?>" class="form-btn btn-info-wisitech">
      </div-->
	  


  <div class="myprf_btn confirmandpay">
        <input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.Confirm_and_pay')!= '')  ?  trans(Session::get('lang_file').'.Confirm_and_pay'): trans($OUR_LANGUAGE.'.Confirm_and_pay')); ?>" class="form-btn btn-info-wisitech">
      </div>
 <div class="myprf_btn deny">
        <input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.DENY_BTN')!= '')  ?  trans(Session::get('lang_file').'.DENY_BTN'): trans($OUR_LANGUAGE.'.DENY_BTN')); ?>" class="form-btn btn-info-wisitech">
      </div>


<?php }} ?>

            <!-- checkout-form-row -->
          </div>
		  
          <!-- checkout-form -->
        </div>      
		
        <!-- checkout-box -->
      </div>    
      
      </form>      
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script>
jQuery(document).ready(function(){
jQuery.validator.addMethod("laxEmail", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
}, "Please enter valid email address."); 
 jQuery("#change_password").validate({
    rules: {          
          "old_password" : {
            required : true
          },  
          "new_password" : {
            required : true,
            minlength: 8,
          },
          "confirm_password" : {
            required : true,
            equalTo: "#new_password"
          },              
         },
         messages: {
          "old_password": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_OLD_PASSWORD')); ?> <?php endif; ?>"
          },
          "new_password": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD')); ?> <?php endif; ?>",
            minlength:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')); ?> <?php endif; ?>"
          },
          "confirm_password": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')); ?> <?php endif; ?>",
            equalTo:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')); ?> <?php endif; ?>"
          },          
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#change_password").valid()) {        
    jQuery('#change_password').submit();
   }
  });
  
   jQuery("#old_password").blur(function(){
   var str = jQuery('#old_password').val();
        jQuery.ajax({
		   type: 'get',
		   data: 'password='+str,
		   url: '<?php echo url('change-pass-imagewithajax'); ?>',
		   success: function(responseText)
		   {
			jQuery('#psserrror').html(responseText);
			//location.reload();
		  }       
		});
   });
});
</script>
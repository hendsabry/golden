<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap occ">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="myaccount_right">
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')); ?></h1>
      
      <?php /*?>{!! Form::open(array('url'=>"my-account-ocassion",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'filter')) !!}
      <div class="dash_select">
        <div class="search-box-field">
          <select name="search_key" id="search_key" onChange="document.filter.submit();">
            <option value="all"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='all'){echo 'Selected="Selected"';}?>>{{ (Lang::has(Session::get('lang_file').'.ALL')!= '')  ?  trans(Session::get('lang_file').'.ALL'): trans($OUR_LANGUAGE.'.ALL')}}</option>
            <option value="date"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='date'){echo 'Selected="Selected"';}?>>Date</option>
          </select>
        </div>
      </div>
      {!! Form::close() !!}
<?php */?>
      <div class="field_group top_spacing_margin_occas">
	    <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>
        <div class="main_user">
		 <?php if(count($setOrder) > 0): ?>
          <div class="myaccount-table">
            <div class="mytr">
              <div class="mytable_heading"></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')); ?></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')); ?></div>
              <div class="mytable_heading order_id "><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')); ?></div>
            </div>
            <?php $i = 1; ?>
            <?php if(count($setOrder) >0): ?>
            <?php $__currentLoopData = $setOrder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>           
            <div class="mytr">
              <div class="mytd " data-title="Sl. No."><?php echo e($i); ?></div>
              <div class="mytd " data-title="Occasions Name">
			  <?php
			  if(isset($val->search_occasion_id) && $val->search_occasion_id!='0'){
              $setTitle = Helper::getOccasionName($val->search_occasion_id);
              ?>
              <?php $mc_name='title'?>
              <?php if(Session::get('lang_file')!='en_lang'): ?>
              <?php $mc_name= 'title_ar'; ?>
              <?php endif; ?>
               <a href="<?php echo e(route('order-details',['id'=>$val->order_id])); ?>"><?php echo e(@$setTitle->$mc_name); ?></a>
			   <?php } else{
			   if(isset($val->main_occasion_id) && $val->main_occasion_id!='0'){ 
				 if(Session::get('lang_file')!='en_lang')
				 {
				   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
				 }
				 else
				 {
					$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
				 } 
				 foreach($getArrayOfOcc as $key=>$ocval)
				 {
				  if($val->main_occasion_id==$key)
				  {
				   $occasion_name = $ocval;
				  }
				 }	
				 ?>
				 <a href="<?php echo e(route('order-details',['id'=>$val->order_id])); ?>"> <?php echo $occasion_name; ?> </a>			   
			   <?php }} ?>
            </div>
              <div class="mytd " data-title="Date"><?php echo e(Carbon\Carbon::parse($val->order_date)->format('d M Y')); ?></div>
              <div class="mytd order_id" data-title="Order ID"><a href="<?php echo e(route('order-details',['id'=>$val->order_id])); ?>"><?php echo e($val->order_id); ?></a></div>
            </div>
            <?php $i++; ?> 
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
            <?php endif; ?>
         </div>
		 <?php else: ?>
		 <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
		 <?php endif; ?>
        </div>        
      </div>
      
    </div>
    <?php echo e($setOrder->links()); ?>

    <!-- page-right-section -->
  </div>

  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script type="text/javascript">
    /*function SearchData(str)
    {
     $.ajax({
     type: 'get',
     data: 'search_key='+str,
     url: '<?php echo url('my-account-ocassion-withajax'); ?>',
     success: function(responseText){  
      alert(responseText);
      $('#email_id_error_msg').html(responseText);  
      if(responseText!=''){
        $("#email_id").css('border', '1px solid red'); 
        $("#email_id").focus();
      }
      else
        $("#email_id").css('border', '1px solid #ccc');  
     }    
     });  
   }*/
</script>
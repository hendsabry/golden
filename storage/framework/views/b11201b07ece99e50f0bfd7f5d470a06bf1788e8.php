<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
?>

<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($vendordetails->mc_img); ?>" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
          <?php if(trim($vendordetails->mc_video_url) !=''): ?>
          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>
          <?php endif; ?>
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
          <?php } ?>
         <li><a href="#choose_package"><?php echo e((Lang::has(Session::get('lang_file').'.Choose_PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.Choose_PRODUCT'): trans($OUR_LANGUAGE.'.Choose_PRODUCT')); ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?>
                <li><img src="<?php echo e(str_replace('thumb_','',$vendordetails->image)); ?>" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"><?php echo e($vendordetails->mc_name); ?></div>
          <div class="detail_hall_description"><?php echo e($vendordetails->address); ?></div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
          <div class="detail_about_hall">
            <div class="comment more"><?php echo e($vendordetails->mc_discription); ?></div>
          </div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: <span>
            <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
            </span></div>

             <?php if($vendordetails->latitude!='' && $vendordetails->longitude!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          <?php }  ?>

        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
        
<?php if(trim($vendordetails->mc_video_url) !=''): ?>

        <div class="service-video-area">
          <div class="service-video-cont"><?php echo e($vendordetails->mc_video_description); ?></div>
          <div class="service-video-box">
            <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
<?php endif; ?>


        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="<?php echo e(url('')); ?>/abayareadymadedetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>" class="select"><?php echo e((Lang::has(Session::get('lang_file').'.READY_MADE')!= '')  ?  trans(Session::get('lang_file').'.READY_MADE'): trans($OUR_LANGUAGE.'.READY_MADE')); ?></a></li>
                <li><a href="<?php echo e(url('')); ?>/abayatailersdetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.TAILOR')!= '')  ?  trans(Session::get('lang_file').'.TAILOR'): trans($OUR_LANGUAGE.'.TAILOR')); ?></a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>

   <?php  
      $k=count($productlist);   

         ?>
<?php if($k >=1): ?>
      <!-- service_bottom -->
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> <?php  $i=1; ?>
             
                <?php if($k<6){ ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <?php if($k==3){ $cl='category_wrapper'.$i; }else{ $cl=''; } ?>
                  <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                    <div class="category_wrapper <?php echo e($cl); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                      <div class="category_title">
                        <div class="category_title_inner"> <?php echo e($getallcats->pro_title); ?> </div>
                      </div>
                    </div>
                    </a> </div>
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 6th-------------->
                <?php }elseif($k==6){ ?>
                <?php $j=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                  <?php if($j==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($j==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($j==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($j==6){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"> <?php echo e($getallcats->pro_title); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($j==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==4){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($j==5){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($j==6){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $j=$j+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 7th-------------->
                <?php }elseif($k==7){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                  
                  <?php if($l==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($l==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($l==7){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"> <?php echo e($getallcats->pro_title); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==7){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 8th-------------->
                <?php }elseif($k==8){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                  <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                  <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                  
                  <?php if($l==1){ $classrd='category_wrapper1'; ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_3of5 rows5row"> <?php } ?> 
                      <?php if($l==4){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?>
                          <?php if($l==8){ $classrd='category_wrapper9'; ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"> <?php echo e($getallcats->pro_title); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==8){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php }elseif($k==9){ ?>
                <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                  <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
                  
                  
                  <?php if($i==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($i==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($i==4){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($i==7){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?> 
                          <?php if($i==9){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner"> <?php echo e($getallcats->pro_title); ?> </span></span> </span> </a> <?php if($i==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    <?php if($i==9){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?> 
                  
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php } ?> </div>
            </div>
          </div>
          <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
          <div align="center"><?php echo e($productlist->links()); ?></div> 
      </div>
 
        <!-- service-display-right -->
        <?php echo Form::open(['url' => 'addtocartforshopping', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

    <?php if(isset($productlist[0]->pro_disprice) && $productlist[0]->pro_disprice!='0'){$getPrice = $productlist[0]->pro_disprice;}else{$getPrice = $productlist[0]->pro_price;}?>
        <div class="service-display-left">
          <input type="hidden" id="category_id" name="category_id" value="<?php echo e($category_id); ?>">
          <input type="hidden" id="subcat_id" name="subcat_id" value="<?php echo e($subcat_id); ?>">
          <input type="hidden" id="shop_id" name="shop_id" value="<?php echo e($shop_id); ?>">
          <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9" />
          <input type="hidden" name="actiontype" value="abayareadymadedetail">
          <input type="hidden" name="cart_sub_type" value="abaya">
          <input type="hidden" name="cart_type" value="shopping">
      <b><?php echo e(Session::get('status')); ?></b>
          <span id="selectedproduct">
          <input type="hidden" id="product_id" name="product_id" value="<?php if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!=''){echo $productlist[0]->pro_id;}?>">
          <input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>">
          <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->pro_mr_id) && $productlist[0]->pro_mr_id!=''){echo $productlist[0]->pro_mr_id;}?>">
          <div class="service-left-display-img product_gallery">

 
            <?php    $pro_id = $productlist[0]->pro_id; ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        


            <a name="common_linking" class="linking">&nbsp;</a><!--<img src="<?php if(isset($productlist[0]->pro_Img) && $productlist[0]->pro_Img!=''){echo $productlist[0]->pro_Img;}?>" alt="" />--></div>
          <div class="service-product-name">
            <?php if(isset($productlist[0]->pro_title) && $productlist[0]->pro_title!=''){echo $productlist[0]->pro_title;}?>
          </div>
          <div class="service-beauty-description">
            <?php if(isset($productlist[0]->pro_desc) && $productlist[0]->pro_desc!=''){echo nl2br($productlist[0]->pro_desc);}?>
          </div>
          </span>

 





      <?php 
      if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!='')
      {
        $getalldata = Helper::getProduckOptionInfo($productlist[0]->pro_id,$productlist[0]->pro_mr_id); 
       
      if(count($getalldata) > 0)
      {
      ?>
        <div class="service_selecrrow dress_selecrrow" id="ptattrsizeenone">
            <?php  $k=0; $jk=0;
            foreach($getalldata as $val){ ?>
           <?php if(isset($val->value) && $val->value!='' && $val->value>0){  $jk=$k+1; }

         } ?>

           <?php if($jk>0){ ?>
        <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.SIZE_DESSESS')!= '')  ?  trans(Session::get('lang_file').'.SIZE_DESSESS'): trans($OUR_LANGUAGE.'.SIZE_DESSESS')); ?></div>
        <div class="checkout-form-bottom" id="ptattrsize">
          <select class="checkout-small-box" name="product_size" id="product_size">
          <option value=""><?php echo e((Lang::has(Session::get('lang_file').'.SELECT')!= '')  ?  trans(Session::get('lang_file').'.SELECT'): trans($OUR_LANGUAGE.'.SELECT')); ?></option>
          <?php foreach($getalldata as $val){ ?>
            <?php if(isset($val->value) && $val->value!='' && $val->value>0){ ?>
            <option value="<?php echo $val->option_title;?>">Size
              <?php echo str_replace('Size', ' ', $val->option_title);
             ?>
                

              </option>
            <?php } ?>
          <?php } ?>
          </select>
        </div>
        <?php } else{ ?>
           <div class="checkout-form-bottom" id="ptattrsize"><?php if(Lang::has(Session::get('lang_file').'.Sold_Out')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Sold_Out')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Sold_Out')); ?> <?php endif; ?></div>
        <?php } ?>
        </div>
      <?php }} ?>
	  <div class="size-chart-line"><a href="#pop" class="try_now_page_one">Size Chart</a></div>
	   
          <div class="service-radio-line" id="cartop" style="display: none;">
            <div class="service_quantity_box">
       <div class="service_qunt"><?php echo e((Lang::has(Session::get('lang_file').'.QUANTITY')!= '')  ?  trans(Session::get('lang_file').'.QUANTITY'): trans($OUR_LANGUAGE.'.QUANTITY')); ?></div>
              <div class="service_qunatity_row">
                <div class="td td2 quantity food-quantity" data-title="Total Quality">
                  <div class="quantity">
                    <button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
                    <input type="number" name="itemqty" id="qty" value="1" min="1" max="9" onblur="return pricecalculation('add');" />
                    <button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button>
                  </div>
          <label for="qty" class="error"></label>
                </div>
              </div>
            </div>
          </div>
          <div class="total_food_cost padT0">
            
            <div class="total_price"><span class="newprice"><?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->pro_price){echo '<del>'.currency($productlist[0]->pro_price, 'SAR',$Current_Currency).'</del> <br />';} ?></span><br/><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: <span id="cont_final_price">  <?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->pro_price){echo currency($getPrice, 'SAR',$Current_Currency);}else{echo currency($productlist[0]->pro_price, 'SAR',$Current_Currency);} ?> </span></div>
          </div>
         
          <div class="btn_row" id="hideid" style="display: none;">

  <input type="submit" name="submit" id="submit"  <?php if($productlist[0]->pro_qty < 1): ?> disabled="disabled"  <?php endif; ?> <?php if($productlist[0]->pro_qty >= 1): ?> value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" <?php else: ?> value="<?php echo e((Lang::has(Session::get('lang_file').'.Sold_Out')!= '')  ?  trans(Session::get('lang_file').'.Sold_Out'): trans($OUR_LANGUAGE.'.Sold_Out')); ?>" <?php endif; ?> class="form-btn btn-info-wisitech">
 
          </div>
            <div class="terms_conditions"><a  href="<?php echo e($vendordetails->terms_conditions); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')); ?></a></div> <a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a>
        </div>
        
        <?php echo Form::close(); ?>

        <!-- service-display-left -->


<?php else: ?>
<div class="service-display-section">  
        <div class="service-display-right">
No product listed
</div></div>

<?php endif; ?>






      </div>
      <!--service-display-section-->
   <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>

<div id="try_now_page_one_popup" class="new-popup sizechart-popup" style="display:none;"> 
			 <a href="javascript:void(0);" class="b-close">X</a>
			  
			  <div class="table">
<div class="tr">
<div class="table_heading">US</div>
<div class="table_heading">UK</div>
<div class="table_heading">Germany</div>
<div class="table_heading">France</div>
<div class="table_heading">Italy</div>
<div class="table_heading">Kore</div>
 
</div> <!-- tr -->

<div class="tr">
<div class="td td1" data-title="US">0</div>
<div class="td td2" data-title="UK">4</div>                     
<div class="td td3" data-title="Germany">30</div>
<div class="td td4" data-title="France">32</div>   
<div class="td td5" data-title="Italy">36</div>   
<div class="td td6" data-title="Kore">44</div>   
 
 </div> <!-- tr -->

<div class="tr">
<div class="td td1" data-title="US">2</div>
<div class="td td2" data-title="UK">6</div>                     
<div class="td td3" data-title="Germany">32</div>
<div class="td td4" data-title="France">34</div>   
<div class="td td5" data-title="Italy">38</div>   
<div class="td td6" data-title="Kore">44</div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">4</div>
<div class="td td2" data-title="UK">8</div>                     
<div class="td td3" data-title="Germany">34</div>
<div class="td td4" data-title="France">36</div>   
<div class="td td5" data-title="Italy">40</div>   
<div class="td td6" data-title="Kore">55</div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">6</div>
<div class="td td2" data-title="UK">10</div>                     
<div class="td td3" data-title="Germany">36</div>
<div class="td td4" data-title="France">38</div>   
<div class="td td5" data-title="Italy">42</div>   
<div class="td td6" data-title="Kore">55</div>   
  
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">8</div>
<div class="td td2" data-title="UK">12</div>                     
<div class="td td3" data-title="Germany">38</div>
<div class="td td4" data-title="France">40</div>   
<div class="td td5" data-title="Italy">44</div>   
<div class="td td6" data-title="Kore">66</div>   
  
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">10</div>
<div class="td td2" data-title="UK">14</div>                     
<div class="td td3" data-title="Germany">40</div>
<div class="td td4" data-title="France">42</div>   
<div class="td td5" data-title="Italy">46</div>   
<div class="td td6" data-title="Kore">66</div>   

 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">12</div>
<div class="td td2" data-title="UK">16</div>                     
<div class="td td3" data-title="Germany">42</div>
<div class="td td4" data-title="France">44</div>   
<div class="td td5" data-title="Italy">48</div>   
<div class="td td6" data-title="Kore">77</div>   

 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">14</div>
<div class="td td2" data-title="UK">18</div>                     
<div class="td td3" data-title="Germany">44</div>
<div class="td td4" data-title="France">46</div>   
<div class="td td5" data-title="Italy">50</div>   
<div class="td td6" data-title="Kore">77</div>   
   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">16</div>
<div class="td td2" data-title="UK">20</div>                     
<div class="td td3" data-title="Germany">46</div>
<div class="td td4" data-title="France">48</div>   
<div class="td td5" data-title="Italy">52</div>   
<div class="td td6" data-title="Kore">88</div>   
  
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">18</div>
<div class="td td2" data-title="UK">22</div>                     
<div class="td td3" data-title="Germany">48</div>
<div class="td td4" data-title="France">50</div>   
<div class="td td5" data-title="Italy">54</div>   
<div class="td td6" data-title="Kore">88</div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">20</div>
<div class="td td2" data-title="UK">24</div>                     
<div class="td td3" data-title="Germany">50</div>
<div class="td td4" data-title="France">52</div>   
<div class="td td5" data-title="Italy">56</div>   
<div class="td td6" data-title="Kore"></div>   
  </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">22</div>
<div class="td td2" data-title="UK">26</div>                     
<div class="td td3" data-title="Germany">52</div>
<div class="td td4" data-title="France">54</div>   
<div class="td td5" data-title="Italy">58</div>   
<div class="td td6" data-title="Kore"></div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">24</div>
<div class="td td2" data-title="UK">28</div>                     
<div class="td td3" data-title="Germany">54</div>
<div class="td td4" data-title="France">56</div>   
<div class="td td5" data-title="Italy">60</div>   
<div class="td td6" data-title="Kore"></div>   
  </div> <!-- tr -->
					 
					 </div>
			  </div> <!-- new-popup -->
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
   function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }

$('body').on('change','#product_size',function(){ 

  var product_id      = document.getElementById('product_id').value;
  var product_size    = document.getElementById('product_size').value;
    var currentquantity = document.getElementById('qty').value;
    $('#hideid').css('display','block');
    if(currentquantity !='')
    {
      var currentquantity = 1;
    }
  
$.ajax({
     type:"GET",
     url:"<?php echo e(url('getSizeQuantity')); ?>?product_id="+product_id+'&product_size='+product_size+'&qty='+currentquantity,
    async: false,
     success:function(res)
     {               
    <?php $Cur = Session::get('currency'); ?>
     if(res!='ok')
     {
     alert(res);
        var aadd = "<?php if(Lang::has(Session::get('lang_file').'.Sold_Out')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Sold_Out')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Sold_Out')); ?> <?php endif; ?>";
        $('#cartop').css('display','none');

        $('#submit').prop('disabled','disabled');
        $('#submit').val(aadd);
        
 


       $('#showmsg').show();
       $('#hidemsgab').hide();
       $('#showmsgab').show();
       var qtyupdated=parseInt(currentquantity);    
       var upqty = qtyupdated - 1;

     
       //document.getElementById('hideid').style.display = "none";
       // document.getElementById('excessqty').style.display = "block";
     }
     else
     {
        
 $('#cartop').css('display','block');
        $('#submit').prop('disabled',false);
        var aadd = "<?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?>";

        $('#submit').val(aadd);

 

      var producttotal = qty*unititemprice;
      producttotal = getChangedPrice(producttotal);
      document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
      document.getElementById('itemqty').value=qty;
      document.getElementById('hideid').style.display = "block";
     }
     }
   });

});





function pricecalculation(act)
{
  var no=1;
  var product_id      = document.getElementById('product_id').value;
  var product_size    = document.getElementById('product_size').value;
  var currentquantity = document.getElementById('qty').value;
  var unititemprice   = document.getElementById('priceId').value;
  if(act=='add')
  {
    var qty = parseInt(currentquantity)+parseInt(no);     
  }
  else
  { 
    if(parseInt(currentquantity)==1)
    {
      var qty=parseInt(currentquantity)
    }
    else
    {
      var qty=parseInt(currentquantity)-parseInt(no);
    }
  }
  
  if(product_size)
  {
   $.ajax({
     type:"GET",
     url:"<?php echo e(url('getSizeQuantity')); ?>?product_id="+product_id+'&product_size='+product_size+'&qty='+qty,
    async: false,
     success:function(res)
     {               
    <?php $Cur = Session::get('currency'); ?>
     if(res!='ok')
     {
    
         $('.action_popup').fadeIn(500);
       $('.overlay').fadeIn(500);
       $('#showmsg').show();
       $('#hidemsgab').hide();
       $('#showmsgab').show();
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty').value = qtyupdated - 1;

  
        /*var aadd = "<?php if(Lang::has(Session::get('lang_file').'.Sold_Out')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Sold_Out')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Sold_Out')); ?> <?php endif; ?>";

        $('#submit').prop('disabled','disabled');
        $('#submit').val(aadd);
        
 


       $('#showmsg').show();
       $('#hidemsgab').hide();
       $('#showmsgab').show();
       var qtyupdated=parseInt(currentquantity);    */  
       //document.getElementById('qty').value = qtyupdated - 1;
       //document.getElementById('hideid').style.display = "none";
        //document.getElementById('excessqty').style.display = "block";
     }
     else
     {
        
 
        $('#submit').prop('disabled',false);
        var aadd = "<?php if(Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADD_TO_CART')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADD_TO_CART')); ?> <?php endif; ?>";

        $('#submit').val(aadd);

 

      var producttotal = qty*unititemprice;
      producttotal = getChangedPrice(producttotal);
      document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
      document.getElementById('itemqty').value=qty;
      document.getElementById('hideid').style.display = "block";
     }
     }
   });
  }
  else
  {

    var producttotal = qty*unititemprice;
      producttotal = getChangedPrice(producttotal);
    document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
    document.getElementById('itemqty').value=qty;
  }
      
}
function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}



function showProductDetail(str,vendorId)
{
     $.ajax({
     type:"GET",
     url:"<?php echo e(url('getShoppingProductInfo')); ?>?product_id="+str+'&vendor_id='+vendorId+'&from=abaya',
     success:function(res)
     {               
    if(res)
    { 
      $('html, body').animate({
        scrollTop: ($('.service-display-left').first().offset().top)
      },500);
      var json = JSON.stringify(res);
      var obj = JSON.parse(json);
          console.log(obj);
      length=obj.productdateshopinfo.length;
     <?php $Cur = Session::get('currency'); ?>
      document.getElementById('qty').value=1;
      if(length>0)
      {
           for(i=0; i<length; i++)
         {      
        if(obj.productdateshopinfo[i].pro_disprice !='0' && obj.productdateshopinfo[i].pro_disprice >=1){var getPrice = parseFloat(obj.productdateshopinfo[i].pro_disprice).toFixed(2); } else {var getPrice = parseFloat(obj.productdateshopinfo[i].pro_price).toFixed(2);}   

        if(getPrice < obj.productdateshopinfo[i].pro_price)
        {      
        TotalPricea = getChangedPrice(obj.productdateshopinfo[i].pro_price);
        getPrice = getChangedPrice(getPrice);  
          $('.newprice').html('<del><?php echo $Cur; ?> '+TotalPricea+'</del>');  
          $('#cont_final_price').html(' <?php echo $Cur; ?> '+getPrice);
        } 
        else
        {  getPrices = getChangedPrice(obj.productdateshopinfo[i].pro_price);  
           $('#cont_final_price').html(' <?php echo $Cur; ?> '+getPrices); 
         $('.newprice').html(''); 
        } 
              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+getPrice+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img product_gallery"> </div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+nl2br(obj.productdateshopinfo[i].pro_desc)+'</div>');          
            checkgallery(str);   
         }
      }
      if($.trim(obj.productattrsize) !=1)
      {
        $('#ptattrsize').html(obj.productattrsize);
      $('#ptattrsizeenone').css('display','block');
      }
      else
      {
        $('#ptattrsizeenone').css('display','none');
      }   
      }
     }
   });
}
</script>






<script>
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script>
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {       
          "product_size" : {
            required : true
          },    
         },
         messages: {
          "product_size": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php endif; ?>'
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
</script>
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

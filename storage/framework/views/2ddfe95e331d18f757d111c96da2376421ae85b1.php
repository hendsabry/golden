<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $reception_hospitality_leftmenu =1;
$cid = request()->cid;
$id = request()->id;
 ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if(isset($cid) && $cid !=''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATEPACKAGE')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.ADD_PACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADD_PACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADD_ITEM')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <form name="form1"  id ="menucategory" method="post" action="<?php echo e(route('recption-update-packege')); ?>" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?>

              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PACKEGENAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKEGENAME'); ?> </span> </label>
                  <div class="info100">
                    <div class="english">
                      <input class="english" id="packeg_name" maxlength="90"  name="packeg_name"  value="<?php echo e(isset($packeg->pro_title) ? $packeg->pro_title : ''); ?>"  required=""  type="text" >
                    </div>
                    <div class="arabic">
                      <input class="arabic ar" id="packeg_name_ar" maxlength="90"  name="packeg_name_ar"  value="<?php echo e(isset($packeg->pro_title_ar) ? $packeg->pro_title_ar : ''); ?>"  required=""  type="text" >
                    </div>
                  </div>
                </div>
              </div>
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_NATIONALITY')); ?> <?php endif; ?> </label>
                  <div class="info100">
                  <select name="staff_nationality"  id="staff_nationality">
                    <option value="" ><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')); ?>  
                    <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_NATIONALITY')); ?> <?php endif; ?> </option>
                    
                    
          
                           <?php $co_name='co_name'?>
                           <?php if($mer_selected_lang_code !='en'): ?>
                                                 <?php $co_name= 'co_name_'.$mer_selected_lang_code; ?>
                           <?php endif; ?>  

                           <?php $__currentLoopData = $countrylist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php

                           ?>
                             
                    
                    <option value="<?php echo e($val->co_id); ?>" <?php if(isset($packeg->staff_nationality) && $val->co_id==$packeg->staff_nationality): ?> selected <?php endif; ?> >
                    <div class="english">
                    <?php echo e($val->$co_name); ?>

                    </div>
                    </option>
                    
                    
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      
                  
                  </select>
                  <?php if($errors->has('workers_nationality')): ?> <span class="help-block text-red"> <?php echo e($errors->first('workers_nationality')); ?> </span> <?php endif; ?>
                  <input type="hidden" name="shopid" value="<?php echo e(request()->shopid); ?>">
                </div>
              </div>
              </div>
              <div class="form_row common_field ">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.NO_STAFF'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.NO_STAFF'); ?> </span> </label>
                  <div class="info100">
                    <input id="no_of_staff" class="xs_small" onkeypress="return isNumber(event);" maxlength="10"  name="no_of_staff" value="<?php echo e(isset($packeg->no_of_staff) ? $packeg->no_of_staff : ''); ?>"  type="text" >
                  </div>
                </div>
              </div>
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PRICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PRICE'); ?> </span> </label>
                  <div class="info100">
                    <input id="price" class="xs_small" onkeypress="return isNumber(event);" maxlength="10"  name="price" value="<?php echo e(isset($packeg->pro_price) ? $packeg->pro_price : ''); ?>"   type="text" >
                  </div>
                </div>
              </div>

<div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_IMAGE'); ?> </span> </label>
                    
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                      <input id="company_logo" name="mc_img" class="info-file" type="file">
                      <input type="hidden" name="updatemc_img" value="<?php echo e(isset($packeg->pro_Img) ? $packeg->pro_Img : ''); ?>">
                      <input type="hidden" name="id" value="<?php echo e($id); ?>">
                      <div class="form-upload-img"><?php if(isset($packeg->pro_Img) && $packeg->pro_Img !=''): ?>
                       <img src="<?php echo e(isset($packeg->pro_Img) ? $packeg->pro_Img : ''); ?>" width="150" height="150"> 
                       <?php endif; ?></div>
                    </div>
                    
                    </div>


   <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english"><?php echo lang::get('mer_en_lang.QTYSTOCK'); ?></span> 
                        <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.QTYSTOCK'); ?> </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="<?php echo e(isset($packeg->pro_qty) ? $packeg->pro_qty : ''); ?>"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>


                <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); ?> %</span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); ?>  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="<?php echo e(isset($packeg->pro_discount_percentage) ? $packeg->pro_discount_percentage : ''); ?>" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.DESCRIPTION'); ?> </span> </label>
                  <div class="info100">
                    <div class="">
                      <textarea class="english" id="packegdescription"  name="packegdescription" ><?php echo e(isset($packeg->pro_desc) ? $packeg->pro_desc : ''); ?></textarea>
                    </div>
                    <div class="" >
                      <textarea class="arabic ar" id="packegdescription_ar"  name="packegdescription_ar" ><?php echo e(isset($packeg->pro_desc_ar) ? $packeg->pro_desc_ar : ''); ?></textarea>
                    </div>
                    <?php  $id = request()->id; $cid = request()->cid; ?>
                    <input type="hidden" name="id" value="<?php echo e(isset($id) ? $id : ''); ?>">
                    <input type="hidden" name="cid" value="<?php echo e(isset($cid) ? $cid : ''); ?>">
                    <input type="hidden" name="shopid" value="<?php echo e(isset($shopid) ? $shopid : ''); ?>">
                  </div>
                </div>
              </div>
              <?php $k=1; ?>
              <?php $__currentLoopData = $getAttr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   
              
              <?php $items_a = Helper::getProductByAttribute($shopid,$value->id);     ?>                 
              
              <?php if(count($items_a)>0): ?>
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label" for="intezxrnal<?php echo e($k); ?>"> <span class="english"><?php echo e($value->attribute_title); ?></span> <span  class="arabic ar"> <?php echo e($value->attribute_title_ar); ?> </span> </label>
                  <div class="info100"> <?php $__currentLoopData = $items_a; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="save-card-line">
                      <input type="radio" data-validation="checkbox_group" name="items<?php echo e($value->id); ?>[]" value="<?php echo e($value1->pro_id); ?>" id="internal<?php echo e($value1->pro_id); ?>"  class="items<?php echo e($value->id); ?> cbox_child_<?php echo e($k); ?>" <?php if(in_array($value1->
                      pro_id, $selecteditemsdata)): ?> checked="checked" <?php endif; ?>>
                      <label for="internal<?php echo e($value1->pro_id); ?>"> <span class="english"><?php echo e(isset($value1->pro_title) ? $value1->pro_title : ''); ?></span> <span class="arabic ar"><?php echo e(isset($value1->pro_title_ar) ? $value1->pro_title_ar : ''); ?></span> </label>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <span for="items<?php echo e($value->id); ?>[]" generated="true" class="error"></span> </div>
              </div>
              <?php $k=$k+1; ?>
              <?php endif; ?>
              
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <div class="form_row">
                <div class="form_row_left"> <?php if($cid==''): ?>
                  <div class="english">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                  </div>
                  <?php else: ?>
                  <div class="english">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="Update">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="تحديث">
                  </div>
                  <?php endif; ?> </div>
              </div>
            </form>
            <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>       
	$("form").data("validator").settings.ignore = "";
</script>
<script type="text/javascript">
  var vali ='mer_en_lang.MER_VALIDATION_PRICE'
$("#menucategory").validate({
                  ignore: [],
                  rules: {

                    packeg_name: {
                       required: true,
                      },
                     packeg_name_ar: {
                       required: true,
                      },
                     workers_nationality: {
                     required: true,
                    },
                     no_of_staff: {
                     required: true,
                    },
                    price: {
                     required: true,
                    },
                    
                 
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
             messages: {
             packeg_name: {
               required: "<?php echo e(trans('mer_en_lang.MER_VALIDATION_PACKEG_NAME')); ?>",
                      }, 
               packeg_name_ar: {
               required: "<?php echo e(trans('mer_ar_lang.MER_VALIDATION_PACKEG_NAME_AR')); ?>",
                      }, 
               no_of_staff: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_NO_STAFF')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_NO_STAFF')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_NO_STAFF')); ?> <?php endif; ?>",
                      },
              
               workers_nationality: {
               required: " <?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_WORKERS_NATIONALITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_WORKERS_NATIONALITY')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_WORKERS_NATIONALITY')); ?> <?php endif; ?> ",
                      },  
                      price: {
               required: " <?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?> ",
                      },                                                           
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                   <?php if($mer_selected_lang_code !='ar'): ?>


                    if (typeof valdata.packeg_name != "undefined" || valdata.packeg_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                    else if (typeof valdata.no_of_staff != "undefined" || valdata.no_of_staff != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }    
                    else if (typeof valdata.packegdescription != "undefined" || valdata.packegdescription != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.workers_nationality != "undefined" || valdata.workers_nationality != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }                    
                  
                    else if (typeof valdata.packeg_name_ar != "undefined" || valdata.packeg_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                    else if (typeof valdata.packegdescription_ar != "undefined" || valdata.packegdescription_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                <?php else: ?>
                   
                      if (typeof valdata.packeg_name_ar != "undefined" || valdata.packeg_name_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    } 
                    else if (typeof valdata.no_of_staff != "undefined" || valdata.no_of_staff != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }    
                    else if (typeof valdata.packegdescription_ar != "undefined" || valdata.packegdescription_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.workers_nationality != "undefined" || valdata.workers_nationality != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }                    
                  
                    else if (typeof valdata.packeg_name != "undefined" || valdata.packeg_name != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                    else if (typeof valdata.packegdescription != "undefined" || valdata.packegdescription != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                   <?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

 var createValidation = function() {
<?php $__currentLoopData = $getAttr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   

<?php $items_a = Helper::getProductByAttribute($shopid,$value->id);     ?>                 
         
<?php if(count($items_a)>0): ?> 

        $(".items<?php echo e($value->id); ?>").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
              required: true,
              messages: {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PACKEGE_ITEMS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PACKEGE_ITEMS')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PACKEGE_ITEMS')); ?> <?php endif; ?>"
              }
            });
          });
        <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

};

$(function() {
   createValidation();
});

 


 $('.cbox').click(function(event) {
  if(this.checked) {
     var child = $(this).data('child');
      $('.'+child).each(function() {
          this.checked = true;
      });
  }
  else {
         var child = $(this).data('child');
      $('.'+child).each(function() {
          this.checked = false;
      });
  }
});



</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
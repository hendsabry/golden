<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
	$Current_Currency = 'SAR'; 
  } 
  $cururl = request()->segment(count(request()));
?>
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($fooddateshopdetails[0]->mc_img); ?>" alt="" /></a></div>
      </div>
      <!-- vendor_header_left -->
     <div class="vendor_welc">

           <?php echo $__env->make("includes.language-changer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div class="vendor_name username"> <?php echo e((Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')); ?> <span>
            <?php 
      if(Session::has('customerdata.user_id')) 
          {
        $userid  = Session::get('customerdata.user_id');
        $getInfo = Helper::getuserinfo($userid); 
        if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
      }
      ?>
            <ul class="vendor_header_navbar">
              <li><a href="<?php echo e(route('my-account-profile')); ?>"<?php if($cururl=='my-account-profile') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-ocassion')); ?>"<?php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-studio')); ?>"<?php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-security')); ?>"<?php if($cururl=='my-account-security') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-wallet')); ?>"<?php if($cururl=='my-account-wallet') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-review')); ?>"<?php if($cururl=='my-account-review') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')); ?></a></li>
              <li><a href="<?php echo e(route('my-request-a-quote')); ?>"<?php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')); ?></a></li>
              <li><a href="<?php echo e(route('change-password')); ?>"<?php if($cururl=='change-password') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')); ?></a></li>
              <li><a href="<?php echo e(url('login-signup/logoutuseraccount')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')); ?></a></li>
            </ul>
            </span></div>
          <?php if(Session::get('customerdata.token')!=''): ?>
          <?php $getcartnoitems = Helper::getNumberOfcart(); ?>
          <?php if($getcartnoitems>0): ?> <a href="<?php echo e(url('mycart')); ?>" class="vendor_cart"><img src="<?php echo e(url('')); ?>/themes/images/basket.png" /><span><?php echo e($getcartnoitems); ?></span></a> <?php endif; ?>
          <?php endif; ?> </div>
        <!-- vendor_header_right -->
        <?php if(count($otherbarnch)>1){  ?>
        <?php $jk=0;  ?>  
        <div class="select_catg">

          <div class="select_lbl"><?php if(Lang::has(Session::get('lang_file').'.Other_Branches')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Other_Branches')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Other_Branches')); ?> <?php endif; ?></div>
          <div class="search-box-field">
            <select class="select_drp" id="dynamic_select">
              <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Branch')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Branch')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Branch')); ?> <?php endif; ?></option>
                             
                        <?php $__currentLoopData = $otherbarnch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $otherbarnches): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <?php if($otherbarnch->ischild[$jk]>0): ?>
                          
                <option value="<?php echo e(url('')); ?>/beautyshop/<?php echo e($halltype); ?>/<?php echo e($category_id); ?>/<?php echo e($shop_id); ?>/<?php echo e($otherbarnches->mc_id); ?>"> <?php echo e($otherbarnches->mc_name); ?> </option>
                <?php endif; ?>
              
                          <?php $jk++; ?> 
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>
        </div>
        <?php } ?> </div>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php if(Lang::has(Session::get('lang_file').'.About_Shop')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.About_Shop')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.About_Shop')); ?> <?php endif; ?></a></li>
          <?php if(isset($fooddateshopdetails[0]->mc_video_url) && $fooddateshopdetails[0]->mc_video_url!=''){ ?>
          <li><a href="#video"><?php if(Lang::has(Session::get('lang_file').'.Video')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Video')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Video')); ?> <?php endif; ?></a></li>
          <?php } if(count($fooddateshopreview) > 0){ ?>
          <li><a href="#our_client"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></a></li>
          <?php } ?>
          <li><a href="#choose_package"><?php if(Lang::has(Session::get('lang_file').'.Choose_Package')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Choose_Package')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Choose_Package')); ?> <?php endif; ?></a></li>
        </ul>
      </div>
    </div>
  </div>


  <!-- common_navbar -->
  <div class="inner_wrap service-wrap diamond_space">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php 
                $im=$shopgallery->image;
                $gimg=str_replace('thumb_','',$im);
                ?>
                <li> <img src="<?php echo e($gimg); ?>" /> </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallerythumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li> <img src="<?php echo e($shopgallerythumb->image); ?>" /> </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
          </section>
        </div>

        <div class="service_detail">
          <!-- Example DataTables Card-->
          <?php $cid=$fooddateshopdetails[0]->city_id;
          $getCity = Helper::getcity($cid);
          if(Session::get('lang_file') !='en_lang'){
          $city_name= 'ci_name_ar';
          }else{
          $city_name= 'ci_name'; 
          }  
          
          ?>
          <div class="detail_title"><?php echo e($fooddateshopdetails[0]->mc_name); ?></div>
          <div class="detail_hall_description"><?php echo e($fooddateshopdetails[0]->address); ?></div>
          <div class="detail_hall_subtitle"><?php if(Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ABOUT_SHOP')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?> <?php endif; ?></div>
          <div class="detail_about_hall">
            <div class="comment more"><?php echo e($fooddateshopdetails[0]->mc_discription); ?></div>
          </div>
          <div class="detail_hall_dimention"><?php if(Lang::has(Session::get('lang_file').'.CITY')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.CITY')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.CITY')); ?><?php endif; ?>: <span><?php echo e($getCity->$city_name); ?></span></div>
          <div class="detail_hall_dimention"><?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.TIME')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?><?php endif; ?>: <span><?php echo e($fooddateshopdetails[0]->opening_time); ?> - <?php echo e($fooddateshopdetails[0]->closing_time); ?> </span></div>
          <?php $homevisitcharges=currency($fooddateshopdetails[0]->home_visit_charge, 'SAR',$Current_Currency, $format = false);
          ?>
          <?php if($fooddateshopdetails[0]->google_map_address!=''){  $lat=$fooddateshopdetails[0]->latitude;   $long=$fooddateshopdetails[0]->longitude;    ?>
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          <?php }  ?> </div>
      </div>

      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
        <?php if(isset($fooddateshopdetails[0]->mc_video_url) && $fooddateshopdetails[0]->mc_video_url!=''){ ?>
        <div class="service-video-area">
          <div class="service-video-cont">     <?php if(Session::get('lang_file') !='en_lang'): ?>  <?php echo e(isset($fooddateshopdetails[0]->mc_video_description_ar) ? $fooddateshopdetails[0]->mc_video_description_ar : ''); ?>    <?php else: ?>      <?php echo e(isset($fooddateshopdetails[0]->mc_video_description) ? $fooddateshopdetails[0]->mc_video_description : ''); ?> <?php endif; ?></div>
          <div class="service-video-box">
            <iframe class="service-video" src="<?php echo e(isset($fooddateshopdetails[0]->mc_video_url) ? $fooddateshopdetails[0]->mc_video_url : ''); ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <?php } ?>
        <!-- service-video-area -->
        <?php if(count($fooddateshopreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <?php $__currentLoopData = $fooddateshopreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($customerreview->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e(isset($customerreview->comments) ? $customerreview->comments : ''); ?></div>
                        <div class="testim_name"><?php echo e(isset($customerreview->cus_name) ? $customerreview->cus_name : ''); ?></div>
                        <div class="testim_star"><img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png"></div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php }  ?>
      </div>  

      <!-- service-mid-wrapper -->
      <?php if($category_id==21){ $tbl_field='service_id'; }else{ $tbl_field='shop_id'; } ?>
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>  <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <?php $k=1;
                  if(isset($servicecategoryAndservices) && count($servicecategoryAndservices)>0){
                 ?>
                <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php              
                if(count($categories->serviceslist)>0){
                if($k==1){ $cl='select'; $HH = $categories->serviceslist[0]->pro_id; }else{ $cl=''; } 
                ?>
                <li><a href="#<?php echo e($k); ?>" data-tabid="<?php echo e($k); ?>" class="cat tabIDs<?php echo e($k); ?> <?php echo e($cl); ?>" data-toggle="tab" onclick="getbeautyservice('<?php echo e($categories->serviceslist[0]->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','<?php echo e($k); ?>','service');"><?php echo e(isset($categories->attribute_title) ? $categories->attribute_title : ''); ?> </a></li>
                <?php $k++; } ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php if(count($shopavailablepackage)>0){ ?>
                <li><a href="#100" data-tabid="<?php echo e($k); ?>" class="cat" data-toggle="tab" onclick="getbeautyservice('<?php echo e($shopavailablepackage[0]->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','<?php echo e($k); ?>','package');"><?php if(Lang::has(Session::get('lang_file').'.BridalPackage')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BridalPackage')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.BridalPackage')); ?> <?php endif; ?></a></li>
                <?php }} ?>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>

      <div class="service-display-section">
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer"> <?php  $z=1;   count($servicecategoryAndservices);   ?>
              <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productservice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php
              $getC = $productservice->serviceslist->count(); 
              if($getC >= 9)
              {
              $pagination = ceil($getC/9); 
              }
              else
              { 
              $pagination =0;
              }
              
              ?>
              
              <?php     $k=count($productservice->serviceslist); 
              if($k >0 ){
              ?>

              <?php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } ?>
              <div class="diamond_wrapper_main tab-pane fade <?php echo e($addactcon); ?>"  id="<?php echo e($z); ?>"> <?php if($k < 6){ ?>
                <?php  $i=1;     ?>
                <div class="diamond_wrapper_inner "> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($k==3){ $cl='category_wrapper'.$i; }else{ $cl=''; } ?>
                    <?php 
        $im=$getallcats->pro_Img;
       $gimg=str_replace('thumb_','',$im);
        ?>
                  <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','service');">
                    <div class="category_wrapper <?php echo e($cl); ?>" style="background:url(<?php echo e(isset($gimg) ? $gimg : ''); ?>);">
                      <div class="category_title">
                        <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                      </div>
                    </div>
                    </a> </div>
                  <?php $i=$i+1; ?>
                  
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 6th-------------->
                <?php }elseif($k==6){ ?>
                <?php $j=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                  <?php if($j==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($j==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($j==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($j==6){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','service');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($j==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==4){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($j==5){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($j==6){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $j=$j+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 7th-------------->
                <?php }elseif($k==7){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                  
                  <?php if($l==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($l==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($l==7){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','service');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==7){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 8th-------------->
                <?php }elseif($k==8){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                  <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                  <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                  
                  <?php if($l==1){ $classrd='category_wrapper1'; ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_3of5 rows5row"> <?php } ?> 
                      <?php if($l==4){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?>
                          <?php if($l==8){ $classrd='category_wrapper9'; ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','service');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==8){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!---------- 9th ------------------->
                <?php }elseif($k>=9){ ?>
                <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                  <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($i==1) { $k=9; }else{ $k=$i;}
                  if($i<=9){
                  ?>
                  
                  
                  <?php if($i==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($i==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($i==4){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($i==7){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?> 
                          <?php if($i==9){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','service');"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></span></span> </span> </a> <?php if($i==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    <?php if($i==9){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?> 
                  
                  <?php $i=$i+1; } ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php } ?> </div>
              <?php $z=$z+1; }?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <!-------------- package--------->
              <div class="diamond_wrapper_main tab-pane fade"  id="100"> <?php 
                $k=count($shopavailablepackage);
                if($k < 6){ ?>
                <?php  $i=1;     ?>
                <div class="diamond_wrapper_inner "> <?php $__currentLoopData = $shopavailablepackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','package');">
                    <div class="category_wrapper" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                      <div class="category_title">
                        <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                      </div>
                    </div>
                    </a> </div>
                  <?php $i=$i+1; ?>
                  
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 6th-------------->
                <?php }elseif($k==6){ ?>
                <?php $j=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $shopavailablepackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                  <?php if($j==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($j==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($j==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($j==6){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','package');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($j==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==4){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($j==5){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($j==6){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $j=$j+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 7th-------------->
                <?php }elseif($k==7){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $shopavailablepackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                  
                  <?php if($l==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($l==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($l==7){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','package');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==7){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 8th-------------->
                <?php }elseif($k==8){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $shopavailablepackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                  <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                  <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                  
                  <?php if($l==1){ $classrd='category_wrapper1'; ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_3of5 rows5row"> <?php } ?> 
                      <?php if($l==4){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?>
                          <?php if($l==8){ $classrd='category_wrapper9'; ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','package');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==8){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!---------- 9th ------------------->
                <?php }elseif($k==9){ ?>
                <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                  <?php $__currentLoopData = $shopavailablepackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
                  
                  
                  <?php if($i==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($i==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($i==4){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($i==7){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?> 
                          <?php if($i==9){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','','package');"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></span></span> </span> </a> <?php if($i==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    <?php if($i==9){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?> 
                  
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php } ?> </div>
              <!------- end package------------->
            </div>
          </div>
          <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
          <!--<div class="diamond_shadow"><?php echo e($fooddateshopproducts->
          links()); ?> </span></div>
        -->
      
        
        <?php  $PP=1; $jk=0; ?>
        <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productservice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php 
        $TotalPage = $productservice->serviceslist->count(); 
        if($TotalPage > 9)
        {
        $paginations = ceil($TotalPage/9);
        }
        else
        {
        $paginations =0;
        }
        
        if(count($productservice->serviceslist) < 1) {continue; }
        ?>
        
        <?php if($paginations!=0): ?>
        <div class="pagnation-area" id="tab<?php echo e($PP); ?>" style="display: none;" >
          <ul class="pagination">
            <li class="disabled"><span>«</span></li>
            <?php for($KO=1;$KO<=$paginations;$KO++): ?>
            <li class="<?php if($KO==1): ?> active <?php endif; ?> paginate" data-page="<?php echo e($KO); ?>" data-attribute_id="<?php echo e($productservice->id); ?>"  data-branchid="<?php echo e($branchid); ?>" data-tblfield="<?php echo e($tbl_field); ?>" data-tabid="<?php echo e($PP); ?>"><span><?php echo e($KO); ?></span></li>
            <?php endfor; ?>
            <li><a href="#?page=<?php echo e($paginations); ?>" rel="next">»</a></li>
          </ul>
        </div>
        <?php endif; ?>
        
        <?php  $PP=$PP+1; $jk++; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>


      <!-- service-display-right -->
      <?php echo Form::open(['url' => 'beautyshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

      <!-- container-section -->
	  
      <div class="service-display-left" id="selectedproduct">
	    <b class="added"><?php echo e(Session::get('status')); ?></b>
        <div class="service-left-display-img product_gallery"><a name="common_linking" class="linking">&nbsp;</a>
             <?php    $pro_id = $beautyshopleftproduct->pro_id; ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          </div>
        <div class="service-product-name"><?php echo e($beautyshopleftproduct->pro_title); ?></div>
        <div class="service-beauty-description"><?php echo e($beautyshopleftproduct->pro_desc); ?></div>
        <div class="container-section">
          <div class="duration"> <?php if(Lang::has(Session::get('lang_file').'.DURATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DURATION')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.DURATION')); ?> <?php endif; ?><span><?php echo e($beautyshopleftproduct->service_hour); ?> Hrs</span> </div>
          <div class="beauty-prise"> <?php if($beautyshopleftproduct->pro_discount_percentage!=''){ $productprice= $beautyshopleftproduct->pro_price*(100-$beautyshopleftproduct->pro_discount_percentage)/100;}else{ $productprice='';}
            if($productprice==''){ ?> <span> <?php echo e(currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)); ?> </span> <?php }else{ ?> <span class="strike"><?php echo e(currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)); ?> </span><span> <?php echo e(currency($productprice, 'SAR',$Current_Currency)); ?></span> <?php  } ?> </div>
        </div>
        <!-- container-section -->
      </div>
      <div class="service-display-left bgs">
        <div class="leftbar_title"><?php if(Lang::has(Session::get('lang_file').'.BOOK_APPOINTMENT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BOOK_APPOINTMENT')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.BOOK_APPOINTMENT')); ?> <?php endif; ?> </div>
        <div class="books_form">
          <div class="books_male_area">
            <?php if(isset($fooddateshopdetails[0]->service_availability) && $fooddateshopdetails[0]->service_availability==1){ ?>
            <div class="books_male_left">
              <input id="Home" name="appointmentfor" type="radio" value="home" Onchange="addextracharges(<?php echo e($homevisitcharges); ?>,'add');">
              <label for="Home"><?php if(Lang::has(Session::get('lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?> </label>
            </div>
            <?php }if(isset($fooddateshopdetails[0]->service_availability) && $fooddateshopdetails[0]->service_availability==2){ ?>
            <div class="books_male_left">
              <input id="Shop" name="appointmentfor" type="radio" value="shop" Onchange="addextracharges(<?php echo e($homevisitcharges); ?>,'remove');">
              <label for="Shop"><?php if(Lang::has(Session::get('lang_file').'.SHOP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHOP')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SHOP')); ?> <?php endif; ?></label>
            </div>
            <?php }if(isset($fooddateshopdetails[0]->service_availability) && $fooddateshopdetails[0]->service_availability==3){ ?>
            <div class="books_male_left">
              <input id="Home" name="appointmentfor" type="radio" value="home" Onchange="addextracharges(<?php echo e($homevisitcharges); ?>,'add');">
              <label for="Home"><?php if(Lang::has(Session::get('lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?> </label>
            </div>
            <div class="books_male_left">
              <input id="Shop" name="appointmentfor" type="radio" value="shop" Onchange="addextracharges(<?php echo e($homevisitcharges); ?>,'remove');">
              <label for="Shop"><?php if(Lang::has(Session::get('lang_file').'.SHOP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHOP')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SHOP')); ?> <?php endif; ?></label>
            </div>
            <?php } ?>
            <label for="appointmentfor" class="error"></label>
          </div>
          <div id="shophomevisitcharges" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.HOME_CHARGES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME_CHARGES')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.HOME_CHARGES')); ?> <?php endif; ?> - <?php echo e(Session::get('currency')); ?> <?php echo e($homevisitcharges); ?></div>
          <div id="shopaddr" style="display: none;"><?php echo e($fooddateshopdetails[0]->address); ?></div>
          <span id="btime" style="display: none;">
          <div class="books_male_area">
            <div class="book_label"><?php if(Lang::has(Session::get('lang_file').'.Date')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Date')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Date')); ?> <?php endif; ?></div>
            <div class="book_input">
              <input readonly name="bookingdate" type="text"  id="bookingdate" autocomplete="off" class="t-box cal-t" onchange="getdatefield();" />
            </div>
          </div>
          <div class="books_male_area" id="timefld" style="display: none;">
            <div class="book_label"><?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TIME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?> <?php endif; ?></div>
            <div class="book_input"><input type="text" name="bookingpackagetime" id="bookingtime" onkeydown="OnKeyDown(event)" autocomplete="off" class="t-box sel-time"></div>
          </div>
          </span> </div>

            <div id="staff_area_heading" class="staff_area chosestaffheading"><?php if(Lang::has(Session::get('lang_file').'.Choose_Staff')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Choose_Staff')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Choose_Staff')); ?> <?php endif; ?></div>

        <div class="availibity_staff" id="availibitystaff" style="display: none;"><a href="javascript:void(0);" onclick="staffavailabilitychart('<?php echo e($branchid); ?>'); "><?php if(Lang::has(Session::get('lang_file').'.Staff_Availibility_Chart')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Staff_Availibility_Chart')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Staff_Availibility_Chart')); ?> <?php endif; ?></a></div>
            <span><label for="staffid" class="error"></label></span>
       
        <span id="ourstaff" style="display: none;"> <?php 
         
          $servicestaffid='';
        if(isset($servicestaffexpert) && $servicestaffexpert!=''){ ?>
        <?php  if(count($servicestaffexpert)>0){ ?>
        <div class="staff_area" id="staffarea">
          <div class="leftbar_title"><?php if(Lang::has(Session::get('lang_file').'.Choose_Staff')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Choose_Staff')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Choose_Staff')); ?> <?php endif; ?></div>
		  jjjj
          <div class="choose-line-section tft"> 

            <?php $__currentLoopData = $servicestaffexpert; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staffandreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php  
            if(count($staffandreview->staffinformation)  < 1){ continue; }
            ?>
            <?php 
            $noofreviews=0;
            if(isset($staffandreview->staffreviews) && $staffandreview->staffreviews!=''){
            $noofreviews=sizeof($staffandreview->staffreviews); 
            }
            $starrating=0;
            if(isset($staffandreview->staffratingsavg) && $staffandreview->staffratingsavg!=''){
            $starrating=ceil($staffandreview->staffratingsavg); 
            }	
            
           
            $servicestaffid.= $staffandreview->staffinformation[0]->id.',';
            
            ?>
      
            <div class="choose-line">
              <div class="choose-line-img"><img src="<?php echo e(isset($staffandreview->staffinformation[0]->image) ? $staffandreview->staffinformation[0]->image : ''); ?>" alt="" />
			  	
			  </div>
              <div class="choose-line-cont">
                <div class="choose-line-neme"><?php echo e(isset($staffandreview->staffinformation[0]->staff_member_name) ? $staffandreview->staffinformation[0]->staff_member_name : ''); ?></div>
                <div class="choose-line-experience"> <?php echo e(isset($staffandreview->staffinformation[0]->experience) ? $staffandreview->staffinformation[0]->experience : ''); ?> <?php if(Lang::has(Session::get('lang_file').'.Yrs_Exp')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Yrs_Exp')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Yrs_Exp')); ?> <?php endif; ?></div>
				<div class="choose-line-radio">
                  <input id="name" name="staffid" type="radio" value="<?php echo e(isset($staffandreview->staffinformation[0]->id) ? $staffandreview->staffinformation[0]->id : ''); ?>" onclick="checkstaffavailability('<?php echo e(isset($staffandreview->staffinformation[0]->id) ? $staffandreview->staffinformation[0]->id : ''); ?>');">
                  <label for="name">&nbsp;</label>
                </div>
                
                
              </div>
			  
			  <div class="rts"><div class="choose-line-neme" id="st<?php echo e($staffandreview->staffinformation[0]->id); ?>"></div>
                <div class="choose-line-neme" id="booking<?php echo e($staffandreview->staffinformation[0]->id); ?>"></div>
                <div class="choose-line-review"> <?php if($starrating>0){ ?> <img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e(isset($starrating) ? $starrating : ''); ?>.png"> <?php } ?>
                  <?php if($noofreviews>0){ ?> <span class="review_popuplick"><?php echo e(isset($noofreviews) ? $noofreviews : ''); ?> Review</span> <?php } ?> </div></div>
            </div>
		
            <!-- choose-line -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   </div>
          <!-- choose-line-section -->
        </div>
        <?php } ?>
        <?php } ?> </span>
          

         <span id="finalsbt" style="display: none;">
        <div class="total_food_cost">
          <div class="total_price"> <?php if($productprice!=''){ $totalprice=$productprice; }else{$totalprice=$beautyshopleftproduct->pro_price;} ?>
            <?php if(Lang::has(Session::get('lang_file').'.Total_Price')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Total_Price')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Total_Price')); ?><?php endif; ?>: <?php echo e(Session::get('currency')); ?> <span id="totalprice"> <?php echo e(currency($totalprice, 'SAR',$Current_Currency)); ?> </span></div>
        
       
         
		
        <div class="btn_row"> <?php if($category_id==21){ $sub_type='spa'; }else{ $sub_type='beauty_centers'; } ?>


          
          <input type="hidden" name="service_duration" id="service_duration" value="<?php echo e($beautyshopleftproduct->service_hour); ?>">
          <input type="hidden" name="opening_time" id="opening_time" value="<?php echo e(isset($fooddateshopdetails[0]->opening_time) ? $fooddateshopdetails[0]->opening_time : ''); ?>">
          <input type="hidden" name="closing_time" id="closing_time" value="<?php echo e(isset($fooddateshopdetails[0]->closing_time) ? $fooddateshopdetails[0]->closing_time : ''); ?>">
          <input type="hidden" name="category_id" value="<?php echo e($subsecondcategoryid); ?>">
          <input type="hidden" name="subsecondcategoryid" value="<?php echo e($category_id); ?>">
          <input type="hidden" name="cart_sub_type" value="<?php echo e($sub_type); ?>">
          <input type="hidden" name="branch_id" value="<?php echo e($branchid); ?>">
          <input type="hidden" name="shop_id" value="<?php echo e($shop_id); ?>">
          <input type="hidden" name="typeserice" id="typeserice" value="">
          <input type="hidden" name="vendor_id" value="<?php echo e($beautyshopleftproduct->pro_mr_id); ?>">
          <input type="hidden" name="product_id" id="product_id" value="<?php echo e($beautyshopleftproduct->pro_id); ?>">
          <input type="hidden" name="product_price" id="product_price" value="<?php echo e(currency($totalprice, 'SAR',$Current_Currency, $format = false)); ?>">
          <input type="hidden" name="servicestaffid" id="servicestaffid" value="<?php echo e($servicestaffid); ?>">
          <input type="hidden" name="todaydate" id="todaydate" value="<?php echo e(date('jMY')); ?>">

          <input type="hidden" name="product_orginal_price" id="product_orginal_price" value="<?php echo e(currency($totalprice, 'SAR',$Current_Currency, $format = false)); ?>">
          <input type="submit" name="booknow" value="<?php if(Lang::has(Session::get('lang_file').'.booknow')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.booknow')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.booknow')); ?> <?php endif; ?>" class="form-btn addto_cartbtn">
        </div>
        
		<div class="terms_conditions"> <a href="<?php echo e($fooddateshopdetails[0]->terms_conditions); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')); ?></a>
		<a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a> 
		</div>
        </span>
        <!-- container-section -->
      </div>
      <!-- service-display-left -->
    </div>
    <?php echo Form::close(); ?> </div>
  <!-- service-display-left -->
</div>

<!--service-display-section-->
<!--service-display-section-->
<?php 
        $sesionData = session()->all();
        if(isset($sesionData) && $sesionData!='')
        {
           $url = url('search-results?weddingoccasiontype='.$sesionData['searchdata']['weddingoccasiontype'].'&noofattendees='.$sesionData['searchdata']['noofattendees'].'&budget='.$sesionData['searchdata']['budget'].'&cityid='.$sesionData['searchdata']['cityid'].'&occasiondate='.$sesionData['searchdata']['occasiondate'].'&gender='.$sesionData['searchdata']['gender'].'&basecategoryid='.$sesionData['searchdata']['mainselectedvalue'].'&maincategoryid='.$sesionData['searchdata']['maincategoryid'].'&mainselectedvalue='.$sesionData['searchdata']['mainselectedvalue']); 
        }       
      ?>
           <div class="sticky_other_service"> <a href="<?php echo e($url); ?>">
             <div class="sticky_serivce"><?php if(Lang::has(Session::get('lang_file').'.OTHER_SERVICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OTHER_SERVICE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OTHER_SERVICE')); ?> <?php endif; ?></div>
             <div class="sticku_service_logo"><img src="<?php echo e(url('')); ?>/themes/images/logo.png"></div>
             </a> </div>
           <!-- other_serviceinc -->
           <!-- other_serviceinc -->
           </div>
           <!-- detail_page -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<div class="avalb_popup_overlay"></div>
<div class="availibility_popup">
  <div class="avalb_date"><?php if(Lang::has(Session::get('lang_file').'.DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DATE')); ?> <?php endif; ?> <span id="bookingdatepopup"></span> <a href="javascript:void(0);" class="avalb_close_popup">X</a></div>
  <div class="avalb_tbl_row">
    <div class="avalb_tbl_col3"><?php if(Lang::has(Session::get('lang_file').'.IMAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.IMAGE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.IMAGE')); ?> <?php endif; ?></div>
    <div class="avalb_tbl_col3"><?php if(Lang::has(Session::get('lang_file').'.NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NAME')); ?> <?php endif; ?></div>
    <div class="avalb_tbl_col3"><?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TIME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?> <?php endif; ?></div>
  </div>
<span id="staffpopup"></span>
</div>





<div class="review_overlay"></div>
<div class="user_review">
  <a href="javascript:void(0);" class="user_review_close">X</a>
  <div class="user_review_heading">Reviews By Customer</div>
  
  <div id="content-1" class="content mCustomScrollbar">
  
  <div class="rating_raea_box">
<span id="staffreviewsection"></span>

  </div>
  </div>
</div>


<script type="text/javascript">
  
  function getdatefield(){
   
       jQuery('.sel-time').css("display", "block");
      jQuery('.staffid').css("display", "block");
      jQuery('.sel-time').val('');
      jQuery(".staffid").prop("checked", false);


/////////start 4th Mar////////

var endtime=jQuery('#closing_time').val(); 
               var startdbtime=jQuery('#opening_time').val();  
               
                   var currentTime= moment();    // e.g. 11:00 pm
                    var mstartTime = moment(startdbtime, "HH:mm a");
                    var mendTime = moment(endtime, "HH:mm a");
                   var amIBetween = currentTime.isBetween(mstartTime , mendTime);       
      
      jQuery('#bookingtime').timepicker('remove');

      var todaydate=jQuery('#todaydate').val();
var bookingdate=jQuery('#bookingdate').val();
     

var n= bookingdate.split(" ").slice(0, 3).join('');

if(todaydate==n){
 var timeNow = new Date();
        var currhours   = timeNow.getHours();
         var currmins   = timeNow.getMinutes();
         var hours   = timeNow.getHours()+1;
        var ampm = hours >= 12 ? 'pm' : 'am';
if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;
}else{
         var starttime=currhours+':'+'30 '+ampm;
}
}else{
       //var newstarttime = starttime;
         var  starttime=jql('#opening_time').val();   
}
                   var endtime=jql('#closing_time').val();
                   var service_duration=jql('#service_duration').val();
                  var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A'); 
  jql('#bookingtime').timepicker('remove');           
  jql('#bookingtime').timepicker({
          'minTime': starttime,
          'maxTime': netime,
          'dynamic': true,
          'timeFormat': 'g:i A',          
          }); 


/////////end /////////



  }

</script>

<!----------------  staff chart availability------------>
<script>
  jQuery(document).ready(function(){  
    jQuery('.availibity_staff a').click(function(){   
      jQuery('.availibility_popup').css('display','block');
      jQuery('.avalb_popup_overlay').css('display','block');      
    });
    jQuery('.avalb_close_popup').click(function(){
      jQuery('.availibility_popup').css('display','none');
      jQuery('.avalb_popup_overlay').css('display','none');     
    });
  });
  
</script>

    <script type="text/javascript">
  function staffavailabilitychart(branchid){
       var bookingdate=jql('#bookingdate').val();
      var product_id=jql('#product_id').val();
      var servicestaffid=jql('#servicestaffid').val();
     // var branch_id=parseInt(branchid);

       if(servicestaffid){
        jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('beautyshop/staffavailabilitychart')); ?>?branchid="+branchid+"&servicestaffid="+servicestaffid+"&bookingdate="+bookingdate+"&product_id="+product_id,
           success:function(res){               
              if(res){
                 var json = JSON.stringify(res);
              var obj = JSON.parse(json);
        
                 console.log(obj);
                
                  length=obj.bookingopen.length;
                  jql('#bookingdatepopup').html(bookingdate);
                 
                  if(length>0){
                      jql('#staffpopup').html('');
                      for(jk=0; jk<length; jk++)
                        {
                          bookedslot=obj.bookingopen[jk].bookedslot.length; 
                                            
                      jql('#staffpopup').append('<div class="avalb_row"><div class="avalb_tbl_col3"><img src="'+obj.bookingopen[jk].image+'"></div><div class="avalb_tbl_col3">'+obj.bookingopen[jk].staff_member_name+'</div><div class="avalb_tbl_col3"><span id="tslot'+jk+'"></span></div>');
                                if(bookedslot>0){                           
                                for(k=0; k<length; k++){                                
                                  jql('#tslot'+jk).append('<div class="time_slot">'+obj.bookingopen[jk].bookedslot[k].start_time+'-'+obj.bookingopen[jk].bookedslot[k].end_time+'</div>'); 
                                  }
                                 }else{
                                        jql('#tslot'+jk).append('<div class="time_slot">-</div>');  
                              }
                      
                            //jql('#staffpopup').append('</div>');
                      } 
                   
                  
                  }else{
                    jql('#staffpopup').html('<div class="avalb_row">No Data Found</div>');
                  }              
                 

             }
           }
        });
    }


    
  


  }

</script>




<!-------------- end  staff chart availability -------->

<!--------start check staff availability-------->
<script type="text/javascript">
	function checkstaffavailability(selectedstaffid,st){
			var service_duration=jql('#service_duration').val();
			var staffid=selectedstaffid;
			var bookingdate=jql('#bookingdate').val();
			var bookingtime=jql('#bookingtime').val();

			 if(staffid){
        jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('beautyshop/staffavailability')); ?>?staffid="+staffid+"&service_duration="+service_duration+"&bookingdate="+bookingdate+"&bookingtime="+bookingtime,
           success:function(res){               
	            if(res){
	            	 var json = JSON.stringify(res);
				      var obj = JSON.parse(json);
				
	               console.log(obj);
	            	
	            		length=obj.bookingopen.length;
	            		if(length>0){
	            			jql('#bookingtime').val('');	            			
	            			jql('input[name="staffid"]').prop('checked', false);
	            			jql('#st'+staffid).html('<span class="already"><?php echo e((Lang::has(Session::get('lang_file').'.Already_Booked_at')!= '')  ?  trans(Session::get('lang_file').'.Already_Booked_at'): trans($OUR_LANGUAGE.'.Already_Booked_at')); ?></span><span class="slot"><?php echo e((Lang::has(Session::get('lang_file').'.Pleaseselectanotherslot')!= '')  ?  trans(Session::get('lang_file').'.Pleaseselectanotherslot'): trans($OUR_LANGUAGE.'.Pleaseselectanotherslot')); ?></span>');
	            			bookingrecorddata=obj.bookingrecord.length;
	            			
	            			if(bookingrecorddata>0){
	            				jql('#booking'+staffid).html('<ul id="bslot" style="display:none;">');
	            				for(k=0; k<bookingrecorddata; k++)
									      {
	            					jql('#booking'+staffid).append('<li>'+obj.bookingrecord[k].start_time+' - '+obj.bookingrecord[k].end_time+'</li>');
	            					}
	            				jql('#booking'+staffid).append('<ul>');
	            			}
	            		}else{
	            			jql('#st'+staffid).html('');
	            		}
	            		
	            	 

	           }
           }
        });
    }


		
	


	}

</script>
<!-------------- end check staff availability -------->
<!------------- home charges calculation---------->
<script type="text/javascript">
	
	function addextracharges(hc,act){
		
			jQuery('#bookdt').css('display','');
			jQuery('#bookingtime').val('');
			jQuery('#btime').css('display','');
			jQuery('#ourstaff').css('display','');
			jQuery('#finalsbt').css('display','');
        
			var productprice=document.getElementById('product_price').value;
			var product_orginal_price=document.getElementById('product_orginal_price').value;
			if(act=='add'){
			var homeservicescharges=parseFloat(product_orginal_price) + parseFloat(hc);
			jQuery('#shopaddr').css('display','none');
      jQuery('#shophomevisitcharges').css('display','');
			}
			if(act=='remove'){
			var homeservicescharges=parseFloat(product_orginal_price);
			jQuery('#shopaddr').css('display','');
      jQuery('#shophomevisitcharges').css('display','none');
			}
			jQuery('[name=product_price]').val(homeservicescharges);
				//jQuery('#totalprice').html(homeservicescharges);
				var fprice=homeservicescharges.toFixed(2);
				jQuery('#totalprice').html(fprice);

			
					
	}
</script>
<!--------------- end calculation---------->
<!---------- validation ---------->
<script type="text/javascript">
jQuery("#cartfrm").validate({
                  ignore: [],
                  rules: {
                  	appointmentfor : {required :true},
				  	       bookingdate: {
                       required: true,
                      },                
                    staffid:{
                              required: true,
                              },
                       bookingtime: {
                       required: true,
                      
                      },
                      
					   
                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
           		appointmentfor:{
           			required: "<?php echo (Lang::has(Session::get('lang_file').'.BOOKINGFOR')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGFOR'): trans($OUR_LANGUAGE.'.BOOKINGFOR'); ?>",
           		},
		   		bookingdate: {
               required:  "<?php echo (Lang::has(Session::get('lang_file').'.BOOKINGDATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGDATE'): trans($OUR_LANGUAGE.'.BOOKINGDATE'); ?>", 
                      },
                 bookingtime: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.BOOKINGTIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGTIME'): trans($OUR_LANGUAGE.'.BOOKINGTIME'); ?>",
                            },
			         staffid: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.STAFFSLECT')!= '')  ?  trans(Session::get('lang_file').'.STAFFSLECT'): trans($OUR_LANGUAGE.'.STAFFSLECT'); ?>",
                            },
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });




</script>
<!---------- end validation------>
<!------------------ tabs----------------->
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
</script>
<script src="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery-clockpicker.min.js"></script>
<link href="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery-clockpicker.min.css" rel="stylesheet" />
<script src="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
<!------------ end tabs------------------>
<script type="text/javascript">
  function showstafflist(){
    jQuery('.staff_area').css('display','block'); 
   jQuery('#stav').css('display','block'); 
   var typeserice=jQuery('#typeserice').val();   
   if(typeserice=='package'){
          jQuery('#availibitystaff').css('display','none'); 
          jQuery('#staff_area_heading').css('display','none'); 
   }else{
      jQuery('#availibitystaff').css('display','block'); 
      jQuery('#staff_area_heading').css('display','block'); 
   }

  }

</script>
<!--------- date picker script-------->
<script type="text/javascript">
            // When the document is ready
			var jql = jQuery.noConflict();
            jql(window).load(function () {

               var endtime=jQuery('#closing_time').val(); 
               var startdbtime=jQuery('#opening_time').val();  
               
                   var currentTime= moment();    // e.g. 11:00 pm
                    var mstartTime = moment(startdbtime, "HH:mm a");
                    var mendTime = moment(endtime, "HH:mm a");
                   var amIBetween = currentTime.isBetween(mstartTime , mendTime);

                   var current = new Date(); 
                    var followingDay = new Date(current.getTime() + 86400000);
                   
  var d = new Date(); 
  //d.setDate(d.getDate() + 4);
  var currMonth = d.getMonth();
  var currYear = d.getFullYear();
   if(amIBetween===true){
  var currDay = d;
   }else{
  var currDay = followingDay;
  }
  var ymMonth = (d.getMonth() + 3)
  var dd = d.getDate();
  var startDate = new Date(currYear, currMonth, currDay);
  var endsDate = new Date(currYear, ymMonth, dd);
  jql('#bookingdate').datepicker({
  //format: "yyyy-mm-dd",
   autoclose: true,
  format: "d M yyyy",      
  startDate:currDay,
  endDate:endsDate,
  }); 

                
                jql("#bookingdate").datepicker("setDate", currDay);

                /*jql('#bookingtime').clockpicker({
                    autoclose: true
                });*/


                    jQuery('#bookingtime').timepicker('remove');

      var todaydate=jQuery('#todaydate').val();
var bookingdate=jQuery('#bookingdate').val();
     

var n= bookingdate.split(" ").slice(0, 3).join('');

if(todaydate==n){
 var timeNow = new Date();

        var currhours   = timeNow.getHours();
         var currmins   = timeNow.getMinutes();
         var hours   = timeNow.getHours()+1;
        var ampm = hours >= 12 ? 'pm' : 'am';
if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;
}else{
         var starttime=currhours+':'+'30 '+ampm;
}
}else{
       //var newstarttime = starttime;
         var  starttime=jql('#opening_time').val();   
}


                 	
                   var endtime=jql('#closing_time').val();
                   var service_duration=jql('#service_duration').val();
                	var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A'); 
	jql('#bookingtime').timepicker('remove');				    
		
	jql('#bookingtime').timepicker({
					'minTime': starttime,
					'maxTime': netime,
					'dynamic': true,
 					'timeFormat': 'g:i A',					
					}); 
	 
                  
            });
        </script>

    <!-----worker time management-------->





<script type="text/javascript">
  function workeravaility(wid){   

    var workerid=wid;
      var bookingdate=jql('#bookingdate').val();
var workerbookedslot=[];
 
      if(workerid){

 


        jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('beautyshop/workeravailability')); ?>?staffid="+workerid+"&bookingdate="+bookingdate,
           success:function(res){               
              if(res){
                 var json = JSON.stringify(res);
              var obj = JSON.parse(json);
        
                 console.log(obj);
                
                  workerbookedslots=obj.workerbookingrecord.length;
                  var produt_service_duration=jQuery('#service_duration').val()-0.5;
                  if(workerbookedslots>0){
                    for(lk=0; lk<workerbookedslots; lk++)
                    {
                    //alert(obj.workerbookingrecord[lk].start_time);
                  

                     var inRange = [];
                    var newstartbookedslot=obj.workerbookingrecord[lk].start_time;                        
                   var newservicebookingslottime=moment(newstartbookedslot, 'h:mm A').subtract('hours', produt_service_duration).format('h:mm A');
                    
                     // inRange.push(obj.workerbookingrecord[lk].start_time.split(" ").slice(0, 3).join(''));
                       inRange.push(newservicebookingslottime.split(" ").slice(0, 3).join(''));
                      inRange.push(obj.workerbookingrecord[lk].end_time.split(" ").slice(0, 3).join(''));
                      workerbookedslot.push(inRange);
                     }
                  }else{
                    //jql('#st'+staffid).html('');
                  }
                  
                
             }

        var timeNow = new Date();
		
		var currhours   = timeNow.getHours();

         var currmins   = timeNow.getMinutes();
        var hours   = timeNow.getHours()+1;
		
        var ampm = hours >= 12 ? 'pm' : 'am';
		if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;
		 }else{
         var newstarttime=currhours+':'+'30 '+ampm;
        }
		 
		 
          var todaydate=jQuery('#todaydate').val(); 
          var bookingdate=jQuery('#bookingdate').val();
           var n= bookingdate.split(" ").slice(0, 3).join('');
           var endtime=jQuery('#closing_time').val();  

           var startdbtime=jQuery('#opening_time').val();    

             var service_duration=jQuery('#service_duration').val();
                   var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A');   
         
         if(todaydate==n){
              var starttime=newstarttime; 
                  var currentTime= moment();    // e.g. 11:00 pm
                    var mstartTime = moment(startdbtime, "HH:mm a");
                    var mendTime = moment(netime, "HH:mm a");
                   var amIBetween = currentTime.isBetween(mstartTime , mendTime);

                        if(amIBetween===true){
                        jQuery('#bookingtime'+wid).css("display", "block");
                        jQuery('#staffid'+wid).css("display", "block");

                                  
                    jQuery('#bookingtime'+wid).timepicker('remove');      
                    
                  jQuery('#bookingtime'+wid).timepicker({
                        'minTime': starttime,
                        'maxTime': netime,
                        'dynamic': true,
                        'timeFormat': 'g:i A',  
                        'disableTimeRanges': workerbookedslot        
                        }); 
                      }else{
                          jQuery('#bookingtime'+wid).timepicker('remove');
                          jQuery('#bookingtime'+wid).css("display", "none");
                          jQuery('#staffid'+wid).css("display", "none");
                          

                      }



                  }else{
             var starttime=jQuery('#opening_time').val();
                    jQuery('#bookingtime'+wid).css("display", "block");
                        jQuery('#staffid'+wid).css("display", "block");

                  jQuery('#bookingtime'+wid).timepicker('remove');      
                    
                  jQuery('#bookingtime'+wid).timepicker({
                        'minTime': starttime,
                        'maxTime': netime,
                        'dynamic': true,
                        'timeFormat': 'g:i A',  
                        'disableTimeRanges': workerbookedslot        
                        }); 
               }
                
           // var starttime=jQuery('#opening_time').val();
                 


                 


                  

                   //alert(amIBetween);



                   

          }
        });
    }

  }


</script>

<!----------------->
<!--------- end date picker script------>
<script type="text/javascript">
 function getChangedPrice(price)
  {
  //alert(price);
  var UpdatedPrice = '';
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
     success:function(res)
     { 
	 //alert(res); 
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }
		jQuery('.cat').click(function(){
		
		});
function checkgallery(str)
{
jQuery.ajax({
type:"GET",
url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
success:function(res)
{ 
jQuery('.product_gallery').html(res);
}
});

}

function getbeautyservice(selecteddateproduct,branchid,tbl,Ks,typeserice){ 

        if(typeserice=='package'){
            jQuery('#timefld').css('display','block'); 
        }else{
          jQuery('#timefld').css('display','none'); 

        }
			if(Ks!=undefined)
			{
			jQuery('.pagnation-area').hide()
			jQuery('#tab'+Ks).show()
			}
        jQuery('#availibitystaff').css('display','none'); 
         jQuery('#staff_area_heading').css('display','none');
      jQuery('[name=servicestaffid]').val('');
      //alert(typeserice);
      if(typeserice!=undefined)
      {
      jQuery('[name=typeserice]').val(typeserice);
    }else{
      jQuery('[name=typeserice]').val(ks);
    }
		  jQuery('#btime').css('display','none');
			jQuery('#ourstaff').css('display','none');
			jQuery('#finalsbt').css('display','none');
			jQuery('input[name=appointmentfor]').attr('checked',false);
			jQuery('#shopaddr').css('display','none');
      jQuery('#shophomevisitcharges').css('display','none');
          <?php  if (Lang::has(Session::get('lang_file').'.DURATION')!= '') $dur=  trans(Session::get('lang_file').'.DURATION'); else  $dur= trans($OUR_LANGUAGE.'.DURATION');
        if (Lang::has(Session::get('lang_file').'.Choose_Staff')!= '') $ChooseStaff=  trans(Session::get('lang_file').'.Choose_Staff'); else  $ChooseStaff= trans($OUR_LANGUAGE.'.Choose_Staff');
         if (Lang::has(Session::get('lang_file').'.Yrs_Exp')!= '') $Yrs_Exp=  trans(Session::get('lang_file').'.Yrs_Exp'); else  $Yrs_Exp= trans($OUR_LANGUAGE.'.Yrs_Exp');
		 if (Lang::has(Session::get('lang_file').'.Staff_Availibility_Chart')!= '') $StaffAvailibilitychart=  trans(Session::get('lang_file').'.Staff_Availibility_Chart'); else  $StaffAvailibilitychart= trans($OUR_LANGUAGE.'.Staff_Availibility_Chart');

      if (Lang::has(Session::get('lang_file').'.Time')!= '') $StaffAvailibilityTime=  trans(Session::get('lang_file').'.Time'); else  $StaffAvailibilityTime= trans($OUR_LANGUAGE.'.Time');
 if (Lang::has(Session::get('lang_file').'.REVIEW')!= '') $staff_REVIEW=  trans(Session::get('lang_file').'.REVIEW'); else  $staff_REVIEW= trans($OUR_LANGUAGE.'.REVIEW');
           ?>

		var dateproductid=selecteddateproduct;

			  if(selecteddateproduct){
        jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('beautyshop/getcartproduct')); ?>?product_id="+dateproductid+"&branchid="+branchid+"&tblfld="+tbl,
           success:function(res){               
            if(res){
            	 var json = JSON.stringify(res);
			var obj = JSON.parse(json);
			//alert(obj);
 
            	
				 <?php $Cur = Session::get('currency'); ?>


            		length=obj.productsericeinfo.length;
            		//alert(obj.productprice);
                
            		
            		if(length>0){
		 for(i=0; i<length; i++)
			{
				
        if(obj.productprice==obj.productsericeinfo[i].pro_price){
					var nprice=obj.productsericeinfo[i].pro_price;
					
					 nprice = getChangedPrice(nprice); 
					var productrealprice='<span><?php echo $Cur; ?> '+nprice+'</span>';
					var productrealpriceamount=obj.productsericeinfo[i].pro_price;
						productrealpriceamount = getChangedPrice(productrealpriceamount); 

				}else{

					npriace = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					pp = getChangedPrice(obj.productprice);
         var newpp=parseFloat(pp).toFixed(2);
					var productrealprice='<span class="strike"><?php echo $Cur; ?> '+npriace+' </span><span> <?php echo $Cur; ?> '+newpp+'</span>';
					var productrealpriceamount=obj.productprice;
					productrealpriceamount = getChangedPrice(productrealpriceamount); 
				}
				//alert(nprice);
				jQuery('#selectedproduct').html('<div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productsericeinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productsericeinfo[i].pro_desc+'</div><div class="container-section"><div class="duration"><?php echo $dur; ?> <span>'+obj.productsericeinfo[i].service_hour+' Hrs</span></div><span id="packageservices"></span><div class="beauty-prise prs">'+productrealprice+'</div></div>');

        checkgallery(obj.productsericeinfo[i].pro_id);
				jQuery('[name=product_id]').val(obj.productsericeinfo[i].pro_id);				
				jQuery('[name=service_duration]').val(obj.productsericeinfo[i].service_hour);
				jQuery('[name=product_price]').val(productrealpriceamount);
				jQuery('[name=product_orginal_price]').val(productrealpriceamount);
				jQuery('#totalprice').html(productrealprice);
			}
			
			}

			expertstaff=obj.expertstaff.length;

            jQuery("#ourstaff").html("");
            var servicestaffid=[];
            if(expertstaff>0){
 
		 for(k=0; k<expertstaff; k++)
			{
 
				     
				servicestaffid.push(obj.expertstaff[k].staff_id);
        var reviews=obj.expertstaff[k].staffreviews.length;
    
				if(reviews>0){
					var starreview='<div class="choose-line-review"><img src="<?php echo e(url('/')); ?>/themes/images/star'+obj.expertstaff[k].staffratingsavg+'.png"><span class="review_popuplick" onclick="checkstaffreview('+obj.expertstaff[k].staff_id+');">'+obj.expertstaff[k].staffratingscount+' <?php echo $staff_REVIEW; ?></span></div>';
				}else{ var starreview='';}
				if(k==0){ var staffheading='<div class="leftbar_title"><?php echo $ChooseStaff; ?></div>'; 
					var staval='';  var staffheading='';
				}else{ var staffheading=''; var staval='';}
			jQuery('#ourstaff').append('<div class="staff_area">'+staffheading+''+staval+'<div class="choose-line-section tft"><div class="choose-line"><div class="choose-line-img"><img src="'+obj.expertstaff[k].staffinformation[0].image+'" alt="" /></div><div class="choose-line-cont"><div class="choose-line-neme">'+obj.expertstaff[k].staffinformation[0].staff_member_name+'</div><div class="choose-line-experience">'+obj.expertstaff[k].staffinformation[0].experience+' <?php echo $Yrs_Exp; ?></div>'+starreview+'</div><div><div class="choose-line-radio ja"><input id="staffid'+obj.expertstaff[k].staff_id+'" name="staffid" class="staffid" type="radio" value="'+obj.expertstaff[k].staff_id+'" onclick="checkstaffavailability('+obj.expertstaff[k].staff_id+');"><label for="name">&nbsp;</label><div class="book_input bks"><input name="bookingtime'+obj.expertstaff[k].staff_id+'" type="text" id="bookingtime'+obj.expertstaff[k].staff_id+'" onkeydown="OnKeyDown(event)" autocomplete="off" class="t-box sel-time" placeholder="<?php echo $StaffAvailibilityTime; ?>" onMouseOver="workeravaility('+obj.expertstaff[k].staff_id+');"  onchange="selectedworker('+obj.expertstaff[k].staff_id+');" /></div></div></div><div class="rts"><div class="choose-line-neme" id="st'+obj.expertstaff[k].staff_id+'"></div><div class="choose-line-neme" id="booking'+obj.expertstaff[k].staff_id+'"></div></div></div></div></div>');
		    
				}
			 jQuery('[name=servicestaffid]').val(servicestaffid);
			}

			packageiteminformation=obj.packageiteminformation.length;
			//alert(packageiteminformation);
			if(packageiteminformation>0){
			jQuery('#packageservices').append('<div class="packagetitle"> <?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?></div>');
				for(jk=0; jk<packageiteminformation; jk++)
						{
							//alert(obj.packageiteminformation[jk].iteminformation[0].pro_title);



 TotalPrices = getChangedPrice(obj.packageiteminformation[jk].iteminformation[0].pro_price); 

			jQuery('#packageservices').append('<div class="hair_box">'+obj.packageiteminformation[jk].iteminformation[0].pro_title+' - <?php echo $Cur; ?> '+TotalPrices+'</div>');
						}
			}
			
			var cdt = jQuery('input[name=bookingtime]');

	
jQuery('html, body').animate({
       scrollTop: (jQuery('.service-display-left').offset().top)
    }, 'slow');

           }
           }
        });
    }


		
	}

</script>


<script>
function checkstaffreview(stid){
  //alert(stid);
jQuery('.user_review').css('display','block');
    jQuery('.review_overlay').css('display','block');
var product_id=jQuery('#product_id').val();

     if(stid){
        jQuery.ajax({
           type:"GET",          
            url:"<?php echo e(url('beautyshop/getstaffreview')); ?>?staffid="+stid+"&product_id="+product_id,
           success:function(res){               
              if(res){
                 var json = JSON.stringify(res);
              var obj = JSON.parse(json);
        
                 console.log(obj);
                
                  workernoreview=obj.workerreview.length;
                  
                  if(length>0){
                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    jQuery('#staffreviewsection').html('');
                    for(jk=0; jk<workernoreview; jk++)
                      {
                        if(obj.workerreview[jk].revieweduser[0].cus_pic==''){ var cus_pic="<?php echo e(url('/')); ?>/themes/images/reviewuser.png"; }else{ var cus_pic=obj.workerreview[jk].revieweduser[0].cus_pic; }
                        var posteddate = new Date(obj.workerreview[jk].created_at);
                        var getmonth=monthNames[posteddate.getMonth()];
                        var getdate=posteddate.getDate();
                        var getyear=posteddate.getFullYear();
                        var formateddate=getdate+' '+getmonth+' '+getyear;                        
                          jQuery('#staffreviewsection').append('<div class="rating_box"><div class="rpx_box"><div class="review_left"><img src="'+cus_pic+'" alt="" /></div><div class="review_right"><div class="review_box_type"><div class="review_name_area"><div class="review_cont_type"><div class="review_name">'+obj.workerreview[jk].revieweduser[0].cus_name+'</div><div class="review_star">Posted<span> '+formateddate+'</span></div></div><div class="review_date"><img src="<?php echo e(url('/')); ?>/themes/images/star'+obj.workerreview[jk].ratings+'.png" alt="" /></div></div></div></div></div><div class="rating_desc_po"><div class="review_desc">'+obj.workerreview[jk].comments+'</div></div></div>');
                     }     
                  }else{
                    jQuery('#staffreviewsection').html('');
                  }
                  
                 

             }
           }
        });
    }



}

  jQuery(document).ready(function(){
  
    jQuery('.review_overlay, .user_review_close').click(function(){
  jQuery('.user_review').hide();
    jQuery('.review_overlay').hide();
  });
  });

</script>
<script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>




<script type="text/javascript">
  

  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

</script>





<script type="text/javascript">
jQuery('.paginate').click(function(){
var page = jQuery(this).data('page');
var attribute_id = jQuery(this).data('attribute_id');
var branchid = jQuery(this).data('branchid');
 
if(page!='')
{

    jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('newbeautyshop')); ?>?page="+page+"&attribute_id="+attribute_id+"&branchid="+branchid,
           success:function(res){               
            if(res){
				jQuery('.diamond_main_wrapper').html(res);

			  }
			}
			});
            	

}


})
<?php if(isset($HH) && $HH!=''){ ?>
jQuery(window).load(function(){
   
   // getbeautyservice('88','111','service_id','','service');
 //getbeautyservice('<?php echo $HH;?>','<?php echo $branchid; ?>','shop_id','1','service');
  getbeautyservice('<?php echo $HH;?>','<?php echo $branchid; ?>','<?php echo $tbl_field;?>','1','service');
})

<?php } ?>
function OnKeyDown(event) { event.preventDefault(); };
</script>
<script type="text/javascript">
  
  function selectedworker(workerid){   
    
    jQuery("#staffid"+workerid).prop("checked", true); 

  }

</script>
<script>

      // bind change event to select
      jQuery('#dynamic_select').on('change', function () {
      
          var url = jQuery(this).val(); // get selected value          
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
  
</script>

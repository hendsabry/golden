<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $hotel_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_BRANCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_BRANCH')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_BRANCH')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
        <!-- Display Message after submition --> 
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?>
              <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 

                <form name="form1" method="post" id="addbranch" action="<?php echo e(route('storebranch')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HOTEL_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HOTEL_NAME'); ?> </span> </label>
                    <div class="info100">
                      <input class="english" type="text" readonly="" name="hotel1" value="<?php echo e($alldata->mc_name); ?>" id="hotal" >
                      <input class="arabic ar" type="text" readonly="" name="hotel1" value="<?php echo e($alldata->mc_name_ar); ?>" id="hotal" >
                      <input type="hidden" name="hotel" value="<?php echo e($alldata->mc_id); ?>" id="hotal">
                    </div>
                    </div>
                    
                    <div class="form_row_right common_field">
                    	<label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SELEST_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); ?> </span> </label>
                    	<div class="info100">
                      <select class="small-sel" name="city_id"  id="city_id">
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_CITY')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY')); ?> <?php endif; ?></option>

                        <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?> <?php echo e($cbval->co_name); ?> <?php endif; ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $ci_name='ci_name'?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_'.$mer_selected_lang_code; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>"><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                      </select>
                    </div>
                    </div>
                    
                  </div>
                  <!-- form_row -->

                  <div class="form_row">
    <div class="form_row_left  ">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" maxlength="40" name="mc_name" value=""  class="english" id="hotal_name" >
                      </div>
                      <div  class="arabic ar" >
                        <input id="ssb_name_ar" class="arabic ar"  maxlength="40"  name="mc_name_ar"  type="text" onChange="check();">
                      </div>
                    </div>
                    </div>

                  <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); ?> </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="manager"  id="manager">
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_SELEST_MANAGER.'.MER_SELEST_MANAGER')); ?> <?php endif; ?></option>
                        
                      <?php $__currentLoopData = $manager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      
                        <option value="<?php echo e($val->mer_id); ?>"><?php echo e($val->mer_fname); ?></option>
                        
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    
                      </select>
                    </div>
                    </div>
                    
                
                    
                  </div>
                  <!-- form_row -->

                  <div class="form_row">
                  	<div class="form_row_left">
                    <label class="form_label"> 
                    	<span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESS'); ?></span> 
                        <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESS'); ?> </span> 
                    	
                    </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" maxlength="140" name="address" value=""  class="english" id="hotal_name" >
                      </div>
                      <div  class="arabic ar" >
                        <input id="ssb_name_ar" class="arabic ar"  maxlength="140"  name="address_ar"  type="text">
                      </div>
                    </div>
                    </div>
                    
                    <!--div class="form_row_right common_field">
                    	<label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Insuranceamount'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Insuranceamount'); ?> </span> </label>
                        <div class="ensglish">
                          <input type="text" class="small-sel notzero" name="Insuranceamount" onkeypress="return isNumber(event)"  maxlength="15"   data-validation="length required" 
                        data-validation-error-msg="يرجى إدخال اسم القاعة" value="<?php echo e(isset($getDb->Insuranceamount) ? $getDb->Insuranceamount : ''); ?>" data-validation-length="max35">
                        </div>
                        <?php if($errors->has('Insuranceamount')): ?> <span class="error"> <?php echo e($errors->first('Insuranceamount')); ?> </span> <?php endif; ?>
                      
                    </div-->
                    
                  </div>
                  <!-- form_row -->
                                    
                  <div class="form_row common_field">
                  	<div class="form_row_left">
                        <label class="form_label posrel"> 
                        <span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); ?></span> 
                        <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); ?> </span> 
                        	<a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span></a>
                        </label>
                        <div class="info100">
                          <input type="url" name="google_map_address"  maxlength="150"  data-validation="length required" 
             data-validation-error-msg="Please enter google map address"  value="<?php echo e(isset($getDb->google_map_address) ? $getDb->google_map_address : ''); ?>" data-validation-length="max100">
                        </div>                        
                    </div>




   <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($getDb->longitude) ? $getDb->longitude : ''); ?>" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($getDb->latitude) ? $getDb->latitude : ''); ?>"  readonly="" name="latitude" />
          </div>
                </div>



                    
                  	<div class="form_row_right">
                    	<label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); ?> </span> </label>
                    	<div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" required="" value="">
                      </div>
                    </div>
                        
                        
                    </div>
                  </div><!-- form_row -->                  
                  
                  
                  	
                    <!--<div class="form_row_left common_field">
                    	<label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); ?> </span> 
                        
                        <a href="javascript:void(0);" class="address_image_tooltip"><span class="add_img_tooltip"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_view_title')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_view_title')); ?> <?php endif; ?></span></a>
                        </label>
                        <div class="input-file-area">
                          <label for="company_logo1">
                          <div class="file-btn-area">
                            <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                            <div class="file-value" id="file_value2"></div>
                          </div>
                          </label>
                          <input type="file" name="hall_addressimg" id="company_logo1" class="info-file">
                        </div>
                        <div class="form-upload-img"><?php if(isset($getDb->hall_address_image) && $getDb->hall_address_image !=''): ?> <img src="hallpics/thumb_<?php echo e($getDb->hall_address_image); ?>" width="150" height="150"> <?php endif; ?></div>
                        <?php if($errors->has('hall_addressimg')): ?> <span class="error"> <?php echo e($errors->first('hall_addressimg')); ?> </span> <?php endif; ?> 
                    </div>-->
                    
                    <div class="form_row_right english">
                    	<label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.TERMSANDCONDITIONS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.TERMSANDCONDITIONS'); ?> </span> </label>
                    	<div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo9">
                        <div class="file-btn-area">
                          <div id="file_value8" class="file-value"></div>
                          <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo9" name="mc_tnc" class="info-file" type="file" required="" value="">
                      </div>
                      <span class="certifications"></span> </div>
                    </div>
                    
                  
               
				    
                    <div class="arabic ar">                           
                	<div class="form_row">
                    	<div class="form_row_right arbic_right_btn">
                    	<label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.TERMSANDCONDITIONS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.TERMSANDCONDITIONS'); ?> </span> </label>
                    	<div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo11">
                        <div class="file-btn-area">
                          <div id="file_value10" class="file-value"></div>
                          <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo11" name="mc_tnc_ar" class="info-file" type="file" required="" value="">
                      </div>
                      <span class="certifications"></span> </div>
                    </div>
                    </div>            
                    </div>   
                                    
                  <div class="form_row">
                  <div class="arabic ar form_row_left arbic_right_btn">
                    <input type="submit" name="submit" value="خضع">
                    </div>
                    <div class="form_row_left english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  </div>
                  
                  
                  
                </form>
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },

                       manager: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                      mc_img: {
                           required:true,
                           accept:"png|jpe?g|gif",
                      },

                        hall_addressimg: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },


                        mc_tnc_ar: {
                           required:true,
                           accept:"pdf",
                      },

                        mc_tnc: {
                           required:true,
                           accept:"pdf",
                      },
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             city_id: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?>",
                      },  

                 manager: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR')); ?> <?php endif; ?>",
                      },  
                         mc_tnc: {
         
                 required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_TNC'); ?>",
                      },

          mc_tnc_ar: {
         
                 required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_TNC'); ?>",
                      },



                         mc_name: {
         
               required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_BRANCH'); ?>",
                      },   

                          mc_name_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_BRANCH_AR'); ?>",
                      },   


                       description: {
           
                required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
  
                       description_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
                        mc_img: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },  

                       hall_addressimg: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },                       
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

      <?php if($mer_selected_lang_code !='en'): ?>
                          if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                          {
                          $('.english_tab').trigger('click'); 

                          }
                          if (typeof valdata.description != "undefined" || valdata.description != null) 
                          {
                          $('.english_tab').trigger('click');


                          }

                          if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                          {

                          $('.english_tab').trigger('click');     

                          }
                          if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                          {
                          $('.arabic_tab').trigger('click'); 


                          }
                          if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                          {
                          $('.arabic_tab').trigger('click'); 


                          }
                          if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                          {

                          $('.arabic_tab').trigger('click');


                          }
                          if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                          {

                          $('.arabic_tab').trigger('click');     


                          }
                          if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                          {

                          $('.arabic_tab').trigger('click');     

                          }



                          if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                          {

                          $('.arabic_tab').trigger('click');     

                          }
<?php else: ?> 


                if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                {

                $('.arabic_tab').trigger('click');     


                }

                if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                {

                $('.arabic_tab').trigger('click');     

                }
                if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                {

                $('.arabic_tab').trigger('click');     

                }

                if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                {
                $('.english_tab').trigger('click'); 

                }
                if (typeof valdata.description != "undefined" || valdata.description != null) 
                {
                $('.english_tab').trigger('click');


                }
                if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                {
                $('.english_tab').trigger('click'); 


                }
                if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                {
                $('.english_tab').trigger('click'); 


                }
                if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                {

                $('.english_tab').trigger('click');


                }

                if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                {

                $('.english_tab').trigger('click');     

                }


<?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

 
jQuery("#company_logo2").change(function(){
     
 var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   jQuery("#file_value3").html('');         
jQuery(".certifications").html('Please upload PDF file');

         }
         else
         {
         jQuery("#file_value3").html(fake); 
         }
 
});



/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }






</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
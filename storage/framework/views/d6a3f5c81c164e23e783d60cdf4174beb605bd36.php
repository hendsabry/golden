<?php 
$currentpage=Route::getCurrentRoute()->uri();
$issession= count(Session::get('searchdata'));
if($currentpage == '/'){ $page='home'; }else{ $page='inner';}
if($issession>0 && $page!='home'){ $emodify='modify-extra';  }else{ $emodify='';}
 ?>
<div class="after-modify-search <?php echo e($emodify); ?>" <?php if($page=='inner'){ ?> style="display:none" <?php } ?>> 
<form action="<?php echo e(route('search-results')); ?>" name="searchweddingoccasion" id="searchweddingoccasion">
<div class="search-form">
<div class="search-box search-box1">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.TypeofBusiness')!= '')  ?  trans(Session::get('lang_file').'.TypeofBusiness'): trans($OUR_LANGUAGE.'.TypeofBusiness')); ?></div>
<div class="search-box-field">
<span>
<select id="bussinesslist" name="bussinesslist">
<?php $__currentLoopData = $bussinesstype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $woevents): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if(Session::get('searchdata.bussinesslist')==$woevents->id){ $selected='selected="selected"'; }else{ $selected='';} ?>
<option value="<?php echo e($woevents->id); ?>" <?php echo e($selected); ?> ><?php echo e($woevents->title); ?> </option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
</span>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')); ?></div>
<div class="search-box-field"><input type="text" class="search-t-box" name="noofattendees"  value="<?php echo e(Session::get('searchdata.noofattendees')); ?>" id="noofattendees" onkeypress="return isNumberKey(event)"/></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?></div>
<div class="search-box-field"><input type="text" class="search-t-box" name="budget" value="<?php echo e(Session::get('searchdata.budget')); ?>" id="budget" onkeypress="return isNumberKey(event)" placeholder="<?php echo e(Session::get('currency')); ?>" /><span id="berror"></span></div>
</div> <!-- search-box -->

<div class="search-box search-box4">  
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div>
<div class="search-box-field">
 <select name="cityid" id="cityid">
    <option value=""><?php if(Lang::has(Session::get('lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?></option>
                <?php $getC = Helper::getCountry(); $selectedcity=''; ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="color: #d2cece;"><?php if($selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?>  <?php echo e($cbval->co_name); ?>   <?php endif; ?></option>
                         <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php

                          if(Session::get('searchdata.cityid')!=''){ 
                          $user_currentcity   = Session::get('searchdata.cityid');  
                          }else {
                          $user_currentcity   = Session::get('user_currentcity');
                          }
                              
                        $cityID = $val->ci_id;
  
                           ?>
                        <?php if($selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php if($user_currentcity == $cityID){  echo "SELECTED";}  ?>  ><?php echo e($val->$ci_name); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.StartDate')!= '')  ?  trans(Session::get('lang_file').'.StartDate'): trans($OUR_LANGUAGE.'.StartDate')); ?></div>
<div class="search-box-field"><input type="text" name="startdate" class="search-t-box cal-t" id="startdate" value="<?php echo e(Session::get('searchdata.startdate')); ?>"  readonly  /></div>
</div> <!-- search-box -->


 <div class="search-box search-box6">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.EndDate')!= '')  ?  trans(Session::get('lang_file').'.EndDate'): trans($OUR_LANGUAGE.'.EndDate')); ?></div>
<div class="search-box-field"><input type="text" class="search-t-box cal-t" name="enddate" id="enddate" value="<?php echo e(Session::get('searchdata.enddate')); ?>"  readonly/></div>
</div> <!-- search-box -->


 <!-- search-box -->

<div class="search-btn">
  <input type="hidden" name="basecategoryid" id="basecategoryid" value="<?php echo e(Session::get('searchdata.basecategoryid')); ?>" />
<input type="hidden" name="maincategoryid" id="" value="<?php echo e(Session::get('searchdata.maincategoryid')); ?>" />
<input type="hidden" name="mainselectedvalue" class="mysel" value="<?php echo e(Session::get('searchdata.mainselectedvalue')); ?>"/>

<input type="hidden" name="language_type" id="language_type" value="<?php echo e(Session::get('lang_file')); ?>" />
<input type="submit" class="form-btn" value="<?php echo e((Lang::has(Session::get('lang_file').'.SEARCH')!= '')  ?  trans(Session::get('lang_file').'.SEARCH'): trans($OUR_LANGUAGE.'.SEARCH')); ?>" name="submit"/></div>
</div> <!-- search-form -->

</form>
</div>

<?php if($issession>0 && $page!='home'){ ?>
<div class="modify-search-form">
<div class="search-box search-box1">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.TypeofBusiness')!= '')  ?  trans(Session::get('lang_file').'.TypeofBusiness'): trans($OUR_LANGUAGE.'.TypeofBusiness')); ?></div>
<div class="search-noneedit">
<?php 
 $oid = Session::get('searchdata.bussinesslist');
 $getoccasiontype = Helper::business_occasion_type($oid);
 if(count($getoccasiontype) > 0){
 if(Session::get('lang_file') !='en_lang')
 {
   $title_name= 'title_ar';
 }
 else
 {
    $title_name= 'title'; 
 }  
 echo $getoccasiontype->$title_name;
 } ?>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.noofattendees')); ?></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.budget')); ?></div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div>
<div class="search-noneedit">
<?php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; ?> </div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.StartDate')!= '')  ?  trans(Session::get('lang_file').'.StartDate'): trans($OUR_LANGUAGE.'.StartDate')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.startdate')); ?> </div>
</div> <!-- search-box -->
<div class="search-box search-box6">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.EndDate')!= '')  ?  trans(Session::get('lang_file').'.EndDate'): trans($OUR_LANGUAGE.'.EndDate')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.enddate')); ?> </div>
</div> <!-- search-box -->
 
<?php if(!Request::is('checkout')): ?> 
<div class="search-btn"><a href="javascript:void(0)" class="form-btn" ><?php echo e((Lang::has(Session::get('lang_file').'.ModifySearch')!= '')  ?  trans(Session::get('lang_file').'.ModifySearch'): trans($OUR_LANGUAGE.'.ModifySearch')); ?></a></div>
<?php endif; ?>
 

</div>
<?php } ?>

  
 <script type="text/javascript">
 
  
            // When the document is ready
      var jq = $.noConflict();
            jq(document).ready(function () {
                
            jq("#startdate").datepicker({
              format: "d M, yyyy",
               startDate: new Date(),
              autoclose: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                jq('#enddate').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                jq('#enddate').datepicker('setStartDate', null);
            });

            jq("#enddate").datepicker({
               format: "d M, yyyy",
               autoclose: true,
               startDate: new Date(),
            }).on('changeDate', function (selected) {
               var endDate = new Date(selected.date.valueOf());
               //jq('#startdate').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
               jq('#startdate').datepicker('setEndDate', null);
            });
         
            
            });
        </script>


<script type="text/javascript">

jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }
    return isNaN(value) && isNaN($(params).val()) 
        || (parseFloat(value) > parseFloat($(params).val())); 
},'Must be greater than {0}.');

jQuery("#searchforbussiness").validate({
                  ignore: [],
                  rules: {            
             cityid: {
                       required: true,
                      },
             startdate: {
                       required: true,
                      },
             enddate: {
                       required: true,              
                  },
                  },
                 highlight: function(element) {
                 jQuery(element).removeClass('error');
                },
             
           messages: {
          
          cityid: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= '')  ?  trans(Session::get('lang_file').'.ENTER_YOUR_CITY'): trans($OUR_LANGUAGE.'.ENTER_YOUR_CITY'); ?>",
                      },
           startdate: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.startdate')!= '')  ?  trans(Session::get('lang_file').'.startdate'): trans($OUR_LANGUAGE.'.startdate'); ?>",
                      },    
                       enddate: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.enddate')!= '')  ?  trans(Session::get('lang_file').'.enddate'): trans($OUR_LANGUAGE.'.enddate'); ?>",
                      },
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 


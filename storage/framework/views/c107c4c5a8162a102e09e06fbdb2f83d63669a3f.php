<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="UTF-8" />
<title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="_token" content="<?php echo csrf_token(); ?>"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
<?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
<?php endif; ?>
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
<?php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
<!--END GLOBAL STYLES -->
<!-- PAGE LEVEL STYLES -->
<link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
<link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head><!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="padTop53 " >
<!-- MAIN WRAPPER -->
<div id="wrap" >
<!-- HEADER SECTION -->
<?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- END HEADER SECTION -->
<?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--PAGE CONTENT -->
<div class=" col-sm-9 col-xs-12 right-side-bar">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="right_col" role="main">
      <div class="" id="appUser">
        <div class="page-title">
          <div class="mainView-details bordr-bg align-head">
            <div class="title_left title-block arabic-right-align">
              <h3 class="maxwith"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_VENDOR_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDOR_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDOR_DETAIL')); ?></h3>
              <div class="back-btn-area"> <a href="/admin/vendor_management" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a> </div>
            </div>
          </div>
        </div>
      </div>
	  
      <div class="clearfix"></div>
      <div class="viewListtab">
	  <?php if(Session::has('message')): ?>
       <div class="alert alert-success no-border">Pay amount successfully </div>
      <?php endif; ?>
        <div class="userinformation bordr-bg paddzero small-width-column2">
          <ul class="paddleftright">
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?></label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->mer_fname); ?> <?php echo e($sub_admin->mer_lname); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL')); ?></label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->mer_email); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_PHONE')); ?></label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->country_code); ?> - <?php echo e($sub_admin->mer_phone); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_GENDER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GENDER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GENDER')); ?></label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->gender); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AGE')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php echo((date('Y') - date('Y',strtotime($sub_admin->dob))));?> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_YEAR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_YEAR') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_YEAR')); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_BIRTH')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php echo date('F j, Y', strtotime($sub_admin->dob ));?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->mer_address1); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->mer_staus); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AVAIL_PLAN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AVAIL_PLAN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AVAIL_PLAN')); ?></label>
              </div>
              <div class="col-sm-9">
                <p></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_WALLET_AMT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_WALLET_AMT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_WALLET_AMT')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->wallet); ?> SAR</p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SALES_MANAGER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_MANAGER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_MANAGER')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php if($manager != ''): ?> <?php echo e($manager->adm_fname); ?> <?php echo e($manager->adm_lname); ?> <?php endif; ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SALES_REPRESENTATIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_REPRESENTATIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_REPRESENTATIVE')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php if(isset($sales_rep)): ?> <?php echo e($sales_rep->adm_fname); ?> <?php echo e($sales_rep->adm_lname); ?> <?php endif; ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SALES_CODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_CODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_CODE')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->sales_rep_code); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NATIONAL_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NATIONAL_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NATIONAL_ID')); ?> </label>
              </div>
              <div class="col-sm-9">
                <p><?php echo e($sub_admin->national_id); ?></p>
              </div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SERVICES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICES')); ?> </label>
              </div>
              <?php if(isset($services)): ?>
              <div class="col-sm-9"> <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <p><?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?> <?php echo e($service->mc_name_ar); ?> <?php else: ?> <?php echo e($service->mc_name); ?> <?php endif; ?></p>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
              <?php else: ?>
              <div class="col-sm-9">
                <p>N/A</p>
              </div>
              <?php endif; ?> </li>

<?php if(count($getaccDetail)>=1): ?>

              <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><b><?php echo e((Lang::has(Session::get('admin_lang_file').'.Bank_details')!= '') ?  trans(Session::get('admin_lang_file').'.Bank_details') :  trans($ADMIN_OUR_LANGUAGE.'.Bank_details')); ?> <b></label>
              </div>
              <div class="col-sm-9"><br/><br/></div>
              
              <div class="col-sm-9"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.Bank_name')!= '') ?  trans(Session::get('admin_lang_file').'.Bank_name') :  trans($ADMIN_OUR_LANGUAGE.'.Bank_name')); ?> : <?php echo e(isset($getaccDetail->bank_name) ? $getaccDetail->bank_name : ''); ?>  </div>
                <div class="col-sm-9">  <?php echo e((Lang::has(Session::get('admin_lang_file').'.Bank_acc_no')!= '') ?  trans(Session::get('admin_lang_file').'.Bank_acc_no') :  trans($ADMIN_OUR_LANGUAGE.'.Bank_acc_no')); ?> :  <?php echo e(isset($getaccDetail->account_number) ? $getaccDetail->account_number : ''); ?>  </div>
                  <div class="col-sm-9"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.Bank_notes')!= '') ?  trans(Session::get('admin_lang_file').'.Bank_notes') :  trans($ADMIN_OUR_LANGUAGE.'.Bank_notes')); ?> :  <?php echo e(isset($getaccDetail->note) ? $getaccDetail->note : ''); ?>  </div>
               </li>
 

  <?php else: ?>
  <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.Bank_details')!= '') ?  trans(Session::get('admin_lang_file').'.Bank_details') :  trans($ADMIN_OUR_LANGUAGE.'.Bank_details')); ?> </label>
              </div>
              <div class="col-sm-9">  <?php echo e((Lang::has(Session::get('admin_lang_file').'.no_record_found')!= '') ?  trans(Session::get('admin_lang_file').'.no_record_found') :  trans($ADMIN_OUR_LANGUAGE.'.no_record_found')); ?>   </div>

            </li>

<?php endif; ?> 


          </ul>
          <div id="appUserList">
            <div class="clearfix"></div>
            <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="page-title">
                    <div class="title_left">
                      <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_REQUEST')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_REQUEST') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_REQUEST')); ?></h3>
                    </div>
                  </div>
                  <div class="x_content UsersList">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ORDER_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_ID')); ?></th>
                            <th><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?></th>
							<th><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYABLE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYABLE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYABLE')); ?></th>
                            <th><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_STATUS')); ?></th>
                            <th><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME')); ?></th>
                            <th><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION')); ?></th>
                          </tr>
                        </thead>
                        <?php 
						//echo '<pre>';print_r($getTotalAmount);die;
						?>
                        <tbody>
                          <?php 
						if(count($getTotalAmount)>0){
						$i=1;
						foreach($getTotalAmount as $val)
						{ 
						 //$get_sub_cat =  Helper::getSubCategoryType($val->merchant_id,$val->product_id,$val->order_id);
						 //echo $get_sub_cat->product_sub_type.'<br>';
						 //array(4=>'',5,6,8,9,10,12,14,15,16,17,127,19,20,21,22,23,25,26,27,29,30,32,34,36,94,95,87,37);
						?>
                          <tr>
                            <td><?php echo e($val->order_id); ?></td>
                            <td>SAR <?php echo e($val->amount); ?></td>
							<td>SAR <?php echo e($val->payable_amount); ?></td>
                            <td><?php if(isset($val->is_paid) && $val->is_paid==0){echo 'Pending';}else{echo 'Paid';} ?></td>
                            <td><?php echo date('Y M d',strtotime($val->created_at)); ?></td>
                            
							  <?php if(isset($val->is_paid) && $val->is_paid==0){ ?>
							  <td class="last"><a href="<?php echo e(url('/admin/update_payment_of_merchant')); ?>/<?php echo $val->id; ?>/<?php echo $val->merchant_id; ?>" onClick="return confirm('Do you want to pay amount SAR <?php echo $val->payable_amount; ?>')"  class="btn view-delete">Pay</a></td>
                              <?php } else {  ?>
							  <!--<button  @click="Pay(ud.vendor_id)" class="btn view-delete"><i class="fa">Pay</i> </button>-->
							  <td class="last"><a href="javascript:void(0);" style="color:#00FF00;">Success</td>
                              <?php } ?>
                            
                          </tr>
                          <?php $i++; } }else{ ?>
                          <tr>
                            <td colspan="7"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
					  </div>                  
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
       
		<div class="ven-pagination pull-right mt-lg"><?php echo e($getTotalAmount->links()); ?></div>
		 </div>
      </div>
    </div>
  </div>
</div>
<!--END MAIN WRAPPER -->
<!-- FOOTER -->
<?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--END FOOTER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- END GLOBAL SCRIPTS -->
<!-- PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
        $.ajaxSetup({
          headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
      </script>
<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
        window._users = [<?php echo $users->toJson(); ?>];
      </script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/vendortransactionrequest.js"></script>
</body>
<!-- END BODY -->
</html>

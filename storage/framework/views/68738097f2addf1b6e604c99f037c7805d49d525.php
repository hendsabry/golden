<?php 
$currentpage=Route::getCurrentRoute()->uri();
$issession= count(Session::get('searchdata'));
  $page='inner'; 
  $emodify=''; 
 ?>
 

 
<div class="modify-search-form-90">
<div class="search-box search-box1">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')); ?></div>
<div class="search-noneedit">
<?php 
 $oid = Session::get('searchdata.weddingoccasiontype');
 $getoccasiontype = Helper::business_occasion_type($oid);
 if(count($getoccasiontype) > 0){
 if(Session::get('lang_file') !='en_lang')
 {
   $title_name= 'title_ar';
 }
 else
 {
    $title_name= 'title'; 
 }  
 echo $getoccasiontype->$title_name;
 } ?>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.noofattendees')); ?></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.budget')); ?></div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div>
<div class="search-noneedit">
<?php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; ?> </div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.occasiondate')); ?> </div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')); ?></div>
<div class="search-noneedit">
 <?php if(Session::get('searchdata.gender')=='male'){ 
		if (Lang::has(Session::get('lang_file').'.MALE')!= '') { echo trans(Session::get('lang_file').'.MALE'); } else  { echo trans($OUR_LANGUAGE.'.MALE'); }
}
?>

<?php if(Session::get('searchdata.gender')=='female'){ 
 if (Lang::has(Session::get('lang_file').'.FEMALE')!= '') { echo trans(Session::get('lang_file').'.FEMALE'); } else  { echo trans($OUR_LANGUAGE.'.FEMALE'); }
}
?>
<?php if(Session::get('searchdata.gender')=='Both'){ 
if (Lang::has(Session::get('lang_file').'.BOTH')!= '') { echo trans(Session::get('lang_file').'.BOTH'); } else  { echo trans($OUR_LANGUAGE.'.BOTH'); }
}
?>
</div>
</div> <!-- search-box -->
<?php if(!Request::is('checkout')): ?> 
<div class="search-btn"><a href="javascript:void(0)" class="form-btn" ><?php echo e((Lang::has(Session::get('lang_file').'.ModifySearch')!= '')  ?  trans(Session::get('lang_file').'.ModifySearch'): trans($OUR_LANGUAGE.'.ModifySearch')); ?></a></div>
<?php endif; ?>


</div>
 

 
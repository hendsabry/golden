
<?php //print_r(session::all()); ?>
<?php  $current_route = Route::getCurrentRoute()->uri();

if(isset($routemenu))
{

$menus=$routemenu;
$menu = strtoupper($menus);
}
 ?>
<div oncontextmenu="return false"></div>
  <script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vuex/2.3.1/vuex.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.1/vue-resource.min.js"></script> 
<!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script>  -->
     <script type="text/javascript">
 var __lc = {};
__lc.license = 4302571;

(function() {
 var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
 lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>



  <div id="top" class="container">

             <nav class="navbar navbar-inverse navbar-fixed-top">
             
               
                <ul class="nav navbar-top-links navbar-right">
               
                    <!-- MESSAGES SECTION -->
                    <li class="dropdown">
                        <a href="javascript:void(0);">
                            <span class="label label-warning"></span> 
                            <i class="icon-envelope-alt"></i>
                        </a>
		  </li>
		   <li class="dropdown">
                        <a href="javascript:void(0);">
                            <span class="label label-success"></span>    <i class="icon-comment-alt"></i>
                        </a>
		  </li>
		
            <li class="dropdown">
                        <a href="javascript:void(0);"></span>    <i class="icon-bookmark-empty"></i>
                        </a>
		  </li>
                    <!--END MESSAGES SECTION -->
                   
                           <select name="Language_change" id="Language_change" onchange="Lang_change()">
                                    <?php foreach($Admin_Active_Language as $Active_Lang) {?>
                                        <option value="<?php echo e($Active_Lang->lang_code); ?>" <?php if($admin_selected_lang_code == $Active_Lang->lang_code): ?>selected <?php endif; ?> ><?php echo e($Active_Lang->lang_name); ?></option>
                                    <?php } ?>
                           </select>
                     
                     <li><a href="<?php echo e(url('admin_profile')); ?>" class="btn btn-default"><i class="icon-user"></i> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_MY_PROFILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MY_PROFILE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MY_PROFILE')); ?> </a>
                            </li>
                            <li><a href="<?php echo e(url('admin_settings')); ?>" class="btn btn-default"><i class="icon-gear"></i> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_SETTINGS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS')); ?> </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo e(url('admin_logout')); ?>" class="btn btn-default"><i class="icon-signout"></i> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_LOGOUT')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_LOGOUT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LOGOUT')); ?> </a>
                            </li>
                    
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>
            
        </div>
<script type="text/javascript">
    function Lang_change() 
    {
        var language_code = $("#Language_change option:selected").val();
        $.ajax
        ({
            type:'POST',
            url:"<?php echo url('admin_new_change_languages');?>",
            data:{'language_code':language_code},
            success:function(data)
            {
                window.location.reload();
            }
        });
    }
</script>


<?php 
$currentpage=Route::getCurrentRoute()->uri();
$issession= count(Session::get('searchdata'));
if($currentpage == '/'){ $page='home'; }else{ $page='inner';}
if($issession>0 && $page!='home'){ $emodify='modify-extra';  }else{ $emodify='';}
 ?>
<div class="after-modify-search <?php echo e($emodify); ?>" <?php if($page=='inner'){ ?> style="display:none" <?php } ?>> 
<form action="<?php echo e(route('search-results')); ?>" name="searchweddingoccasion2" id="searchweddingoccasion2">
<div class="search-form">
<div class="search-box search-box1">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')); ?></div>
<div class="search-box-field">
<span>
<?php //$bussinesstype; ?>
<?php $setOccasion = Helper::getAllOccasionNameList(2); ?>
<select id="weddingandoccasionlist" name="weddingoccasiontype">
 <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select')); ?> <?php endif; ?></option>
<?php foreach($setOccasion as $woevents){ ?>
<?php if(Session::get('searchdata.weddingoccasiontype')==$woevents->id) { $occasionselected='selected'; }else{ $occasionselected='';} 
  if(Session::get('lang_file') !='en_lang')
 {
   $otitle_name= 'title_ar';
 }
 else
 {
    $otitle_name= 'title'; 
 } 
?>
<option value="<?php echo e($woevents->id); ?>" <?php echo e($occasionselected); ?>  ><?php echo e($woevents->$otitle_name); ?> </option>
 <?php } ?>
</select>
</span>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')); ?></div>
<div class="search-box-field"><input type="text" class="search-t-box" name="noofattendees"  value="<?php echo e(Session::get('searchdata.noofattendees')); ?>" id="noofattendees" maxlength="12" onkeypress="return isNumberKey(event)"/></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?></div>
<div class="search-box-field"><input type="text" class="search-t-box" name="budget" value="<?php echo e(Session::get('searchdata.budget')); ?>" id="budget" maxlength="12" onkeypress="return isNumberKey(event);" placeholder="<?php echo e(Session::get('currency')); ?>" /><span id="berror"></span></div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div>
<div class="search-box-field">
 <select name="cityid" id="cityid">
    <option value=""><?php if(Lang::has(Session::get('lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?>
	</option>
               <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="color: #d2cece;"><?php if($selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?>  <?php echo e($cbval->co_name); ?>   <?php endif; ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php  
  
                          if(Session::get('searchdata.cityid')!=''){ 
                          $user_currentcity   = Session::get('searchdata.cityid');  
                          }else {
                          $user_currentcity   = Session::get('user_currentcity');
                          }
                              
                        $cityID = $val->ci_id;
 

                            ?>
                        <?php if($selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php if($user_currentcity == $cityID){  echo "SELECTED";}  ?>><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
              </select>
</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')); ?></div>
<div class="search-box-field"><input type="text" class="search-t-box cal-t" name="occasiondate" id="DA" value="<?php echo e(Session::get('searchdata.occasiondate')); ?>" readonly /></div>
</div> <!-- search-box -->



<div class="search-box search-box8">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')); ?></div>
<div class="search-box-field">
 <select name="gender">
    <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select')); ?> <?php endif; ?></option>
    <option value="male" <?php if(Session::get('searchdata.gender')=='male'){ ?> selected="selected" <?php } ?>><?php if(Lang::has(Session::get('lang_file').'.MALE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MALE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MALE')); ?> <?php endif; ?></option>
	<option value="female" <?php if(Session::get('searchdata.gender')=='female'){ ?> selected="selected" <?php } ?>><?php if(Lang::has(Session::get('lang_file').'.FEMALE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.FEMALE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.FEMALE')); ?> <?php endif; ?></option>
	<option value="Both" <?php if(Session::get('searchdata.gender')=='Both'){ ?> selected="selected" <?php } ?>><?php if(Lang::has(Session::get('lang_file').'.BOTH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BOTH')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.BOTH')); ?> <?php endif; ?></option>
	 </select>
</div>
</div>

 <!-- search-box -->

<div class="search-btn">
<input type="hidden" name="basecategoryid" id="basecategoryid" value="<?php echo e(Session::get('searchdata.basecategoryid')); ?>" />
<input type="hidden" name="maincategoryid" id="" value="<?php echo e(Session::get('searchdata.maincategoryid')); ?>" />
<input type="hidden" name="mainselectedvalue" class="mysel" value="<?php echo e(Session::get('searchdata.mainselectedvalue')); ?>"/>
<input type="hidden" name="language_type" id="language_type" value="<?php echo e(Session::get('lang_file')); ?>" />
<input type="submit" class="form-btn" value="<?php echo e((Lang::has(Session::get('lang_file').'.SEARCH')!= '')  ?  trans(Session::get('lang_file').'.SEARCH'): trans($OUR_LANGUAGE.'.SEARCH')); ?>" name="submit" /></div>
</div> <!-- search-form -->

</form>
</div>

<?php if($issession>0 && $page!='home'){ ?>
<div class="modify-search-form">
<div class="search-box search-box1">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')); ?></div>
<div class="search-noneedit">
<?php 
 $oid = Session::get('searchdata.weddingoccasiontype');
 $getoccasiontype = Helper::business_occasion_type($oid);
 if(count($getoccasiontype) > 0){
 if(Session::get('lang_file') !='en_lang')
 {
   $title_name= 'title_ar';
 }
 else
 {
    $title_name= 'title'; 
 }  
 echo $getoccasiontype->$title_name;
 } ?>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.noofattendees')); ?></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.budget')); ?></div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div>
<div class="search-noneedit">
<?php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; ?> </div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')); ?></div>
<div class="search-noneedit"><?php echo e(Session::get('searchdata.occasiondate')); ?> </div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')); ?></div>
<div class="search-noneedit">
 <?php if(Session::get('searchdata.gender')=='male'){ 
		if (Lang::has(Session::get('lang_file').'.MALE')!= '') { echo trans(Session::get('lang_file').'.MALE'); } else  { echo trans($OUR_LANGUAGE.'.MALE'); }
}
?>

<?php if(Session::get('searchdata.gender')=='female'){ 
 if (Lang::has(Session::get('lang_file').'.FEMALE')!= '') { echo trans(Session::get('lang_file').'.FEMALE'); } else  { echo trans($OUR_LANGUAGE.'.FEMALE'); }
}
?>
<?php if(Session::get('searchdata.gender')=='Both'){ 
if (Lang::has(Session::get('lang_file').'.BOTH')!= '') { echo trans(Session::get('lang_file').'.BOTH'); } else  { echo trans($OUR_LANGUAGE.'.BOTH'); }
}
?>
</div>
</div> <!-- search-box -->
<?php if(!Request::is('checkout')): ?> 
<div class="search-btn"><a href="javascript:void(0)" class="form-btn" ><?php echo e((Lang::has(Session::get('lang_file').'.ModifySearch')!= '')  ?  trans(Session::get('lang_file').'.ModifySearch'): trans($OUR_LANGUAGE.'.ModifySearch')); ?></a></div>
<?php endif; ?>


</div>
<?php } ?>

 <script type="text/javascript">
            // When the document is ready
			var jql = jQuery.noConflict();
            jql(document).ready(function () {
                jql('#DA').datepicker({
                    //format: "dd/mm/yyyy",
					format: "d M, yyyy",
          endDate: '+14m',
					autoclose: true,
					           startDate: new Date(),
                }); 
                jql("#DA").datepicker("setDate", new Date());
            });
        </script>

<?php if(!Request::is('checkout')): ?> 

<script>     
       jQuery("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
jQuery("#searchweddingoccasion2").validate({
                  ignore: [],
                  rules: {				  	
					   cityid: {
                       required: true,
                      },					   
                  },
                 highlight: function(element) {
                  jQuery(element).removeClass('error');
                },
             
           messages: {
		   		  cityid: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= '')  ?  trans(Session::get('lang_file').'.ENTER_YOUR_CITY'): trans($OUR_LANGUAGE.'.ENTER_YOUR_CITY'); ?>",
                      },	  
                     
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
<?php endif; ?>


<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
    <link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
   <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
                   <div class="right_col" role="main">
            <?php if(Session::has('message')): ?>
                <div class="alert alert-danger no-border">
                <button type="button" class="close" data-dismiss="alert"><span>�</span><span class="sr-only">Close</span></button>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php endif; ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content padduser-block">
                  <div class="top_back_btn">
                  <div class="title-block">
                        <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_USER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EDIT_USER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_USER')); ?></h3>
                        </div>
                        <div class="back-btn-area">
                       <a href="/admin/user_management" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a>
                      </div>
                      
                    </div>
                  
                    <form action="/admin/user_management/edit/<?php echo e($customer->cus_id); ?>" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left form-section" enctype="multipart/form-data" method="post">
                      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?> <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="cus_name" required class="form-control col-md-7 col-xs-12" value="<?php echo e($customer->cus_name); ?>" maxlength="20">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL')); ?> <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="email" id="title" name="email" required class="form-control col-md-7 col-xs-12" value="<?php echo e($customer->email); ?>" maxlength="70">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BANK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BANK_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BANK_NAME')); ?> <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="bank_name" required class="form-control col-md-7 col-xs-12" value="<?php echo e($customer->bank_name); ?>" maxlength="20">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.ACCOUNT_NO')!= '') ?  trans(Session::get('admin_lang_file').'.ACCOUNT_NO') :  trans($ADMIN_OUR_LANGUAGE.'.ACCOUNT_NO')); ?> <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="bank_account" name="bank_account" required class="form-control col-md-7 col-xs-12" value="<?php echo e($customer->bank_account); ?>" maxlength="70">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO')); ?> <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="row row-area">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        <select name="country_code" class="selectpicker form-control" data-live-search="true" required="required">
                        <option  value=""><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY_CODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY_CODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY_CODE')); ?></option>
                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($country->country_code); ?>" <?php if ($country->country_code == $customer->country_code): ?> selected
                          <?php endif ?>><?php echo e($country->country_code); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                       
                        
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="cus_phone" required class="form-control col-md-7 col-xs-12" value="<?php echo e(isset($customer->cus_phone) ? $customer->cus_phone : ''); ?>" minlength="6" maxlength="15" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                        </div>
                        </div>
                        </div>
                      </div>
                       <div class="form-group gender">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_GENDER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GENDER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GENDER')); ?></label>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">
                        <input type="radio" class="flat" name="gender" id="active_1" value="0" <?php if($customer->gender == 0): ?> checked <?php endif; ?> />
							<label for="active_1" class="gender-name">
                        <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_MALE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MALE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_MALE')); ?></label> 
                        
                        </div> 
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">
                        <input type="radio" class="flat" name="gender" id="inactive_1" value="1" <?php if($customer->gender == 1): ?> checked <?php endif; ?> />
							<label for="inactive_1" class="gender-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_FEMALE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FEMALE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FEMALE')); ?></label>  
                        
                      </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_BIRTH')); ?><span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="demo" name="dob" required class="form-control col-md-7 col-xs-12" value="<?php echo(date('d-m-Y', strtotime($customer->dob)));?>">
                        </div>
                      </div>
                       <div class="form-group">
                          <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NATIONAL_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NATIONAL_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NATIONAL_ID')); ?>

                          </label>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                              <?php if($customer->national_id != ''): ?>
                          <div class="upload_image"><figure class="block-img"><img src="<?php echo e($customer->national_id); ?>" ></figure></div>
                           <?php endif; ?>
                            <input type="file" name="national_id" id="national_id" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                           <!-- <input type="text" name="national_id"  class="form-control col-md-7 col-xs-12" value="<?php echo e($customer->national_id); ?>"  maxlength="15">-->
                          </div>
                        </div>
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?><span class="required"></span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <textarea class="form-control" rows="3"
                          name="cus_address1" id="my_form" placeholder='Description' maxlength="200"><?php echo e($customer->cus_address1); ?></textarea>
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_COUNTRY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY')); ?> <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <select name="cus_country" class="selectpicker form-control" data-live-search="true" required="required" id="country">
                        <option disabled value="">--Select Country--</option>
                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($country->co_id); ?>" <?php if ($country->co_id == $customer->cus_country): ?> selected
                          <?php endif ?>>  <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?> <?php echo e($country->co_name_ar); ?> <?php else: ?> <?php echo e($country->co_name); ?> <?php endif; ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CITY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY')); ?></label>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <select class="selectpicker form-control" data-live-search="true" name="cus_city"  id="city">
                          <option disabled value="">Please City</option>
                           <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($city->ci_id); ?>" <?php if ($city->ci_id == $customer->cus_city): ?> selected
                          <?php endif ?>>  <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?> <?php echo e($city->ci_name_ar); ?> <?php else: ?> <?php echo e($city->ci_name); ?> <?php endif; ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                        </div>
                      </div>


                      


                      <!-- <div class="form-group">
                          <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.MAP_URL')!= '') ?  trans(Session::get('admin_lang_file').'.MAP_URL') :  trans($ADMIN_OUR_LANGUAGE.'.MAP_URL')); ?>

                          </label>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <input type="url" name="map_url" class="form-control col-md-7 col-xs-12" value="<?php echo e($customer->map_url); ?>">
                          </div>
                        </div>


                       <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IMAGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE')); ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 ">
                          <?php if($customer->cus_pic != ''): ?>
                          <div class="upload_image"><figure class="block-img"><img src="<?php echo e($customer->cus_pic); ?>" ></figure></div>
                           <?php endif; ?> 
                             <div class="custom-file">
                        <label for="cus_pic" class="custom-file-upload">
                        <div class="browse-text"><i class="fa fa-cloud-upload"></i> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_BROWSE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BROWSE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_BROWSE')); ?></div>
                        <span class="commit"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE')); ?></span>
                        </label>
                       <input type="file" name="cus_pic" id="cus_pic" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                           </div>                        
                        </div>
                      </div>


					  <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.ADDRESS_IMAGE')!= '') ?  trans(Session::get('admin_lang_file').'.ADDRESS_IMAGE') :  trans($ADMIN_OUR_LANGUAGE.'.ADDRESS_IMAGE')); ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 ">
                          <?php if($customer->address_image != ''): ?>
                          <div class="upload_image"><figure class="block-img"><img src="<?php echo e($customer->address_image); ?>" ></figure></div>
                           <?php endif; ?> 
                        <div class="custom-file">
                        <label for="address_image" class="custom-file-upload">
                        <div class="browse-text"><i class="fa fa-cloud-upload"></i> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_BROWSE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BROWSE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_BROWSE')); ?></div>
                        <span class="commit1"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE')); ?></span>
                        </label>
                       <input type="file" name="address_image" id="address_image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                           </div>                        
                        </div>
                      </div>-->
                       <div class="form-group gender">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?></label>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">
                        <input type="radio" class="flat" name="cus_status" id="active" value="1" <?php if($customer->cus_status == 1): ?> checked <?php endif; ?>  required />
						  <label for="active" class="gender-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE')); ?> </label>
                       
                        </div> 
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">                       
							 <input type="radio" class="flat" name="cus_status" id="inactive" value="0" <?php if($customer->cus_status == 0): ?> checked <?php endif; ?>  />
							<label for="inactive" class="gender-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE')); ?></label>  
                      </div>
                      </div>


                        <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NEW_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NEW_PASSWORD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_NEW_PASSWORD')); ?>

                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="password" id="upassword" name="upassword"  class="form-control col-md-7 col-xs-12" value="" maxlength="20">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD')); ?> 
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="password" id="cpassword" name="cpassword" class="form-control col-md-7 col-xs-12" value="" maxlength="20">
                        </div>
                      </div>



                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 col-md-offset-4">
                          <a class="btn btn-primary" type="button" href="/admin/user_management"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL')); ?></a>
                          <button id="reset" class="btn btn-primary" type="reset"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
                          <input name="submit" type="submit" class="btn btn-success" value="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?>">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

        </div>

        </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script> 
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
      $('#demo').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        endDate: '+0d',   
        });
      });
    </script>
<script type="text/javascript">
  $(document).on('change','#country',function(){
        $.ajax({
          url: "/get_city",
          data: {'ci_con_id':$('#country').val(),'flag':'country',<?php if (Session::get('admin_lang_file') == 'admin_ar_lang') {?> 'language':'ar'<?php } else ?>'language':'en' <?php ?>},
          cache: false,
          success: function(html){
            setTimeout(function(){     
                $("#city").html(html);
                }, 500);
          }
        });
    });
</script>
<script type="text/javascript">
  jQuery("input#cus_pic").change(function () {
  var imag =  jQuery(this).val();
  var output = imag.split("\\").pop();
  $('.commit').text(output);
});
jQuery("input#address_image").change(function () {
  var imag =  jQuery(this).val();
  var output = imag.split("\\").pop();
  $('.commit1').text(output);
});
</script>
<script type="text/javascript">
  $("#reset1").click(function(e) {
    $('.commit1').text('<?php echo( (Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE')); ?>');
});
</script>
</body>

    <!-- END BODY -->
</html>


<script type="text/javascript">

jQuery("#demo-form2").validate({
                  ignore: [],
                  rules: {
                          upassword: {
                          required: false,
                           minlength: 8
                          },
                           cpassword : {
                             minlength : 8,
                             equalTo : "#upassword"
                           }
                          },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
             
                },
             
           messages: {
         
          upassword: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($ADMIN_OUR_LANGUAGE.'.USER_PASSWORD'); ?>",
          minlength: "<?php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans($ADMIN_OUR_LANGUAGE.'.PLEASE_ENTER_CHARACTERS'); ?>",
                      },
             cpassword: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.USER_CONFIRM_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_CONFIRM_PASSWORD'): trans($ADMIN_OUR_LANGUAGE.'.USER_CONFIRM_PASSWORD'); ?>",
          minlength: "<?php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans($ADMIN_OUR_LANGUAGE.'.PLEASE_ENTER_CHARACTERS'); ?>",
        equalTo: "<?php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE'): trans($ADMIN_OUR_LANGUAGE.'.PLEASE_ENTER_THE_SAME_VALUE'); ?>",
                      },
                     
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 


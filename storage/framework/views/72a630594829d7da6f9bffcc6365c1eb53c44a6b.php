 <header>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo e(url('/')); ?>" class="logo"><img class="img-responsive" src="<?php echo e(url('newWebsite')); ?>/images/gc-logo.png" alt="" /></a>
                </div>
                <div class="col-md-7">
                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav">
                                    <li class="active">
                                        <a href="<?php echo e(url('/')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '')  ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a>
                                    </li>

                                    <li><a href="<?php echo e(url('/occasions')); ?>">#<?php echo e((Lang::has(Session::get('lang_file').'.Occasions')!= '')  ?  trans(Session::get('lang_file').'.Occasions'): trans($OUR_LANGUAGE.'.Occasions')); ?></a></li>

                                    <li><a href="<?php echo e(url('/testimonials')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Testimonials')!= '')  ?  trans(Session::get('lang_file').'.Testimonials'): trans($OUR_LANGUAGE.'.Testimonials')); ?></a></li>

                                    <li><a href="<?php echo e(url('/about-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUTUS')!= '')  ?  trans(Session::get('lang_file').'.ABOUTUS'): trans($OUR_LANGUAGE.'.ABOUTUS')); ?></a></li>

                                    <li><a href="<?php echo e(url('/contact-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ContactUs')!= '')  ?  trans(Session::get('lang_file').'.ContactUs'): trans($OUR_LANGUAGE.'.ContactUs')); ?></a></li>         
                                 </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="col-md-3">
                    <ul class="list-inline list-link">
                        

                        <?php if(Session::get('customerdata.token')!=''): ?>
                            <?php $getcartnoitems = Helper::getNumberOfcart(); ?>
                            <?php if($getcartnoitems>0): ?>
                                <li><a href="<?php echo e(url('mycart')); ?>" class="vendor_cart"><img src="<?php echo e(url('newWebsite')); ?>/images/cart-icon.png"><span>Cart</span> (<?php echo e($getcartnoitems); ?>)</a></li>
                            <?php endif; ?>
                        <?php endif; ?>

                        <li><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/login-icon.png"> Login</a></li>


            <?php if(Session::get('lang_file') == 'ar_lang'): ?>
                <li class="lang_select" data-val="en"  onclick="Lang_change('en')"> 
                <a href="#" class="lang_select" data-val="en"  onclick="Lang_change('en')">
                <div class="lang_select" data-val="en"  onclick="Lang_change('en')">English</div>     
            <?php else: ?>        
                <li  class="lang_select" data-val="ar" onclick="Lang_change('ar')"> 
                <a href="#"  class="lang_select" data-val="ar" onclick="Lang_change('ar')">
                 <div class="lang_select" data-val="ar" onclick="Lang_change('ar')"><?php echo e((Lang::has(Session::get('lang_file').'.ARABIC')!= '')  ?  trans(Session::get('lang_file').'.ARABIC'): trans($OUR_LANGUAGE.'.ARABIC')); ?></div>
            <?php endif; ?> 
             </a> </li>



                    </ul>
                </div>
            </div>
        </div>
    </header>
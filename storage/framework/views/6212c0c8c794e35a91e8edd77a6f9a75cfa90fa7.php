<div id="others">
<?php
if($showData_One==1)
{
?>
 <?php if(count($getbranchhalls)>0){ ?>
<div class="carousel-row" id="withinmybuget">
 		<div class="carousel-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Within_Budget')!= '')  ?  trans(Session::get('lang_file').'.Within_Budget'): trans($OUR_LANGUAGE.'.Within_Budget')); ?> </div>
		<div class="clear"></div>
        <div class="flexslider carousel" >
          <ul class="slides">		
		  <?php $__currentLoopData = $getbranchhalls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $withinbudgethalls): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	  
         	 
					<li>
					<span class="carousel-product-box">
					<span class="carousel-product-img"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($withinbudgethalls->pro_id); ?>"><img src="<?php echo e(isset($withinbudgethalls->pro_img) ? $withinbudgethalls->pro_img : ''); ?>" /></a></span>
					<span class="carousel-product-cont">
					<span class="carousel-product-name"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($withinbudgethalls->pro_id); ?>"><?php echo e(isset($withinbudgethalls->pro_title) ? $withinbudgethalls->pro_title : ''); ?></a></span>
					<span class="carousel-product-prise">
						<?php if(isset($withinbudgethalls->pro_discount_percentage) && $withinbudgethalls->pro_discount_percentage>0): ?>
							<strike>SAR <?php echo e(isset($withinbudgethalls->pro_price) ? $withinbudgethalls->pro_price : ''); ?> </strike> <br />
SAR <?php echo e(isset($withinbudgethalls->pro_disprice) ? $withinbudgethalls->pro_disprice : ''); ?>

						<?php else: ?>
							SAR <?php echo e(isset($withinbudgethalls->pro_price) ? $withinbudgethalls->pro_price : ''); ?>

						<?php endif; ?>

						</span>
					<span class="carousel-product-view"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($withinbudgethalls->pro_id); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.View_Details')!= '')  ?  trans(Session::get('lang_file').'.View_Details'): trans($OUR_LANGUAGE.'.View_Details')); ?></a></span>
					</span>
					</span>
					</li>	
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	 	
				

          </ul>
        </div>
		</div>
		<?php }else{ ?>
		<div class="carousel-row" id="withinmybuget"><div>No hall no available</div></div>
		<?php } ?>

 <?php  }   if($showData_Two==1){ ?> 
<!------------------- above budget--------------------->
 
<div class="carousel-row" id="outofmybuget">
<div class="carousel-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Above_Budget')!= '')  ?  trans(Session::get('lang_file').'.Above_Budget'): trans($OUR_LANGUAGE.'.Above_Budget')); ?> </div>
	<div class="clear"></div>
	<?php if(count($abovecustomerbudget)>0){ ?>		
		<div class="flexslider carousel">
          <ul class="slides">
        <?php $__currentLoopData = $abovecustomerbudget; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $abovebudgethalls): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($abovebudgethalls->pro_id); ?>"><img src="<?php echo e(isset($abovebudgethalls->pro_img) ? $abovebudgethalls->pro_img : ''); ?>" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($abovebudgethalls->pro_id); ?>"><?php echo e(isset($abovebudgethalls->pro_title) ? $abovebudgethalls->pro_title : ''); ?></a></span>
		   <span class="carousel-product-prise">
		   			<?php if(isset($abovebudgethalls->pro_discount_percentage) && $abovebudgethalls->pro_discount_percentage>0): ?>
							<strike>SAR <?php echo e(isset($abovebudgethalls->pro_price) ? $abovebudgethalls->pro_price : ''); ?> </strike> <br />
SAR <?php echo e(isset($abovebudgethalls->pro_disprice) ? $abovebudgethalls->pro_disprice : ''); ?>

						<?php else: ?>
							SAR <?php echo e(isset($abovebudgethalls->pro_price) ? $abovebudgethalls->pro_price : ''); ?>

						<?php endif; ?>

		   	</span>
		   <span class="carousel-product-view"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($abovebudgethalls->pro_id); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.View_Details')!= '')  ?  trans(Session::get('lang_file').'.View_Details'): trans($OUR_LANGUAGE.'.View_Details')); ?></a></span>
		   </span>
		   </span>
  	    		</li>	
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	 	 	
				

          </ul>
        </div>
        <?php }else{ ?>
		<div><?php echo e((Lang::has(Session::get('lang_file').'.Not_found_above_budget')!= '')  ?  trans(Session::get('lang_file').'.Not_found_above_budget'): trans($OUR_LANGUAGE.'.Not_found_above_budget')); ?></div>
		<?php } ?>
	</div>

 <?php  }   if($showData_three==1){ ?> 

<!------------------- offer ------------------------>

<div class="carousel-row" id="offermybuget">
<div class="carousel-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Offers')!= '')  ?  trans(Session::get('lang_file').'.Offers'): trans($OUR_LANGUAGE.'.Offers')); ?> </div>
	<div class="clear"></div>	
	<?php if(count($offercustomerbudget)>0){ 

	?>

		<div class="flexslider carousel">
          <ul class="slides">
         	 
			  <?php $__currentLoopData = $offercustomerbudget; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offerbudgethalls): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($offerbudgethalls->pro_id); ?>">
		   	<img src="<?php echo e($offerbudgethalls->pro_Img); ?>" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($offerbudgethalls->pro_id); ?>"><?php echo e(isset($offerbudgethalls->pro_title) ? $offerbudgethalls->pro_title : ''); ?></a></span>
		   <span class="carousel-product-prise">
		   	<?php if($offerbudgethalls->pro_disprice>0){?>
		   	<strike>SAR <?php echo e(isset($offerbudgethalls->pro_price) ? $offerbudgethalls->pro_price : ''); ?> </strike> <br />
SAR <?php echo e(isset($offerbudgethalls->pro_disprice) ? $offerbudgethalls->pro_disprice : ''); ?>

<?php } else{?>
SAR <?php echo e(isset($offerbudgethalls->pro_price) ? $offerbudgethalls->pro_price : ''); ?>

<?php }?>
</span>
		   <span class="carousel-product-view"><a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($offerbudgethalls->pro_id); ?>">
		   <?php echo e((Lang::has(Session::get('lang_file').'.View_Details')!= '')  ?  trans(Session::get('lang_file').'.View_Details'): trans($OUR_LANGUAGE.'.View_Details')); ?>

		</a></span>
		   </span>
		   </span>
  	    		</li>			 	
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	 		

          </ul>
        </div>
        <?php }else{ ?>
		<div><?php echo e((Lang::has(Session::get('lang_file').'.Not_found_offer_budget')!= '')  ?  trans(Session::get('lang_file').'.Not_found_offer_budget'): trans($OUR_LANGUAGE.'.Not_found_offer_budget')); ?></div>
		<?php } ?>
	</div>

 <?php  }     ?> 

</div> 

<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
<?php $Payment_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.PAYMENT_SUCCESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PAYMENT_SUCCESS')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PAYMENT_SUCCESS')); ?> <?php endif; ?> </h5>
 </header>
    </div>
    <?php if(Session::has('message')): ?>
    <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
    <?php endif; ?>
   <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box commonbox">
             
        Thank you for payment.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Eventbig_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.special_event')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.special_event')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.special_event')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      </header>
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <form name="form1" method="post" id="reception" action="<?php echo e(route('update-shop-info-specialevent',['id' => $parent_id,'shopid' => request()->shopid])); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_NAME'); ?> </span></label>
                    <div class="info100">
                    <div class="english">
                      <input type="text" name="mc_name" id="mc_name" value="<?php echo e(isset($fetchdata->mc_name) ? $fetchdata->mc_name : ''); ?>" id="mc_name"  class="english"  data-validation-length="max80" maxlength="80">
                    </div>
                    <div class="arabic ar">
                      <input  class="arabic ar" value="<?php echo e(isset($fetchdata->mc_name_ar) ? $fetchdata->mc_name_ar : ''); ?>" name="mc_name_ar"  id="mc_name_ar"   type="text" data-validation-length="max80"  maxlength="80" >
                    </div>
                  </div>
                  </div>
                  <div class="form_row_right common_field">
                    <label class="form_label posrel"><span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS_URL'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS_URL'); ?> </span> <a href="javascript:void(0);" class="tooltip_area_wrap"> <span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span> </a> </label>
                    <div class="info100">
                    <input type="url" name="google_map_address" id="google_map_address" value="<?php echo e(isset($fetchdata->google_map_address) ? $fetchdata->google_map_address : ''); ?>" data-validation-length="max80">
                  </div>
                  </div>
                  </div>
                        
  <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->longitude) ? $fetchdata->longitude : ''); ?>" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->latitude) ? $fetchdata->latitude : ''); ?>"  readonly="" name="latitude" />
          </div>
                  </div>


                <div class="form_row">
                
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_ADDRES'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_ADDRES'); ?> </span> </label>
                    <div class="info100">
                    <div class="english">
                      <input type="text"  name="address" value="<?php echo e(isset($fetchdata->address) ? $fetchdata->address : ''); ?>" >
                    </div>
                    <div class="arabic ar">
                      <input type="text"  name="address_ar" value="<?php echo e(isset($fetchdata->address_ar) ? $fetchdata->address_ar : ''); ?>" >
                    </div>
                  </div>
                  </div>


<div class="form_row_right common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SELEST_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); ?> </span> </label>
                  <div class="info100">
                    <select class="small-sel" name="city_id"  id="city_id">
                       
  
<option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?></option>
                          
               <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                          <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?>  <?php echo e($cbval->co_name); ?>   <?php endif; ?></option>
                          
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php $ci_id = $val->ci_id; ?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                          <option value="<?php echo e($val->ci_id); ?>" <?php echo e(isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''); ?> ><?php echo e($val->$ci_name); ?></option>
                          
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                  </div>
                  </div>
                </div>                
                                
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label"><span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_LOGO'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_LOGO'); ?> </span></label>
                    <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"><span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span></div>
                      </div>
                      </label>
                      <span class="msg_img_replace"> <?php if(!isset($getDb)): ?>
                      <?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?>
                      <?php endif; ?> </span>
                      <input id="company_logo" name="mc_img" class="info-file" type="file" >
                      <input type="hidden" name="updateimage" value="<?php echo e(isset($fetchdata->mc_img) ? $fetchdata->mc_img : ''); ?>">
                      <?php if(isset($fetchdata->mc_img) &&  $fetchdata->mc_img!=''): ?>
                      <?php
                      $img = $fetchdata->mc_img;
                    
                      ?>
                      <div class="form-upload-img"> <img src="<?php echo e(isset($img) ? $img : ''); ?>"  ></div>
                      <?php endif; ?> </div>
                  </div>
                  </div>
                  
                  <!--div class="form_row_right">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label posrel"><span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); ?> </span> <a href="javascript:void(0);" class="address_image_tooltip"> <span class="add_img_tooltip"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_address_img')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_address_img')); ?> <?php endif; ?></span> </a> </label>
                    
                    <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div id="file_value2" class="file-value"></div>
                        <div class="file-btn"><span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span></div>
                      </div><span class="msg_img_replace"> <?php if(!isset($getDb)): ?>
                      <?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?>
                      <?php endif; ?> </span>
                      </label>
                      
                      <input id="company_logo1" name="address_image" class="info-file" type="file" >
                      <?php if(isset($fetchdata->address_image) &&  $fetchdata->address_image!=''): ?>
                      <div class="form-upload-img"> <img src="<?php echo e($fetchdata->address_image); ?>" width="150" height="150"> </div>
                      <input type="hidden" name="update_add_img" value="<?php echo e(isset($fetchdata->address_image) ? $fetchdata->address_image : ''); ?>">
                      <?php endif; ?> </div>
                  </div>
                  </div-->
                  
                </div>
                 <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>
                    <div class="info100" >
                      <div class="english">
                        <textarea class="english" maxlength="5000" name="about" id="about" rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription) ? $fetchdata->mc_discription : ''); ?> </textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea class="arabic ar" name="about_ar" maxlength="5000" id="about_ar " rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription_ar) ? $fetchdata->mc_discription_ar : ''); ?></textarea>
                      </div>
                    </div>
                  </div>
               


                <div class="form_row">
                <div class="form_row_left english">  
                  <input type="hidden" name="mc_id" value="">
                  <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.TERMSANDCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.TERMSANDCONDITION'); ?> </span> </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo9">
                      <div class="file-btn-area">
                        <div id="file_value8" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <input id="company_logo9" name="mc_tnc" accept="pdf" class="info-file" type="file" >
                      <div class="pdf_msg">Please upload only PDF file</div>
                      <?php if(isset($fetchdata->terms_conditions) &&  $fetchdata->terms_conditions!=''): ?>
                      
                      <div class="form-upload-img"> <a class="pdf_icon" href="<?php echo e($fetchdata->terms_conditions); ?>" target="_blank"> <img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?></span></a> </div>
                      <input type="hidden" name="updatemc_tnc" value="<?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?>">
                      <?php endif; ?> </div>
                  </div>
                  </div>
                  <div class="form_row_left arabic ar">  
                 
                  <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.TERMSANDCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.TERMSANDCONDITION'); ?> </span> </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo10">
                      <div class="file-btn-area">
                        <div id="file_value9" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <input id="company_logo10" name="mc_tnc_ar" accept="pdf" class="info-file" type="file" >
                      <div class="pdf_msg">يرجى تحميل ملف PDF فقط </div>
                      <?php if(isset($fetchdata->terms_conditions_ar) &&  $fetchdata->terms_conditions_ar!=''): ?>
                      
                      <div class="form-upload-img"> <a class="pdf_icon" href="<?php echo e($fetchdata->terms_conditions_ar); ?>" target="_blank"> <img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?></span></a> </div>
                      <input type="hidden" name="updatemc_tnc_ar" value="<?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?>">
                      <?php endif; ?> </div>
                  </div>
                  </div>
                </div>
                       <div class="form_row_left common_field">
                    <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?> </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?> </option>

                      <option value="1" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==1){?> SELECTED <?php } ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?> </option>
                     <option value="0" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==0){?> SELECTED <?php } ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?>  </option>
                   </select>
                    
                    </div>
                  </div>                         
                <div class="form_row">
                  <div class="form_row_left">
                  <div class="form-btn-section english">
                    <input type="hidden" name="shopid"  value="<?php echo e(request()->shopid); ?>">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="Update">
                  </div>
                  <div class="form-btn-section arabic ar">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="تحديث">
                  </div>
                  </div>
                </div>
                
                
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>       
$("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
$("#reception").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      }, 

                     mc_name: {
                       required: true,
                      },
                       address: {
                       required: true,
                      },
                      address_ar: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },
                      google_map_address: {
                       required: false,
                      },
    <?php if(isset($fetchdata->terms_conditions) &&  $fetchdata->terms_conditions!=''): ?>
                      mc_img: {
                      
                       accept:"png|jpe?g|gif",
                      },

                       address_image: {
                     
                       accept:"png|jpe?g|gif",
                      },

                       mc_tnc: {
                   
                       accept:"pdf",
                      },
                      mc_tnc_ar: {
                   
                       accept:"pdf",
                      },
              <?php else: ?>

               

                       address_image: { 
                       required: false,                  
                       accept:"png|jpe?g|gif",
                      },

                       mc_tnc: {  
                       required: true,                
                       accept:"pdf",
                      },

                   mc_tnc_ar: {  
                       required: true,                
                       accept:"pdf",
                      },
              <?php endif; ?> 

               about: {
                       required: true,
                      },
              about_ar: {
                       required: true,
                      },
                   
            <?php if(isset($fetchdata->mc_img) &&  $fetchdata->mc_img!=''): ?>
                    mc_img: {
                    required: false,
                       accept:"png|jpe?g|gif",
                      },

                   <?php else: ?>
                   
                        mc_img: {
                    required: true,
                       accept:"png|jpe?g|gif",
                      },

                  <?php endif; ?>    
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             
             city_id: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?>",
                      },
             mc_name: {
            
                 required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_SHOP_NAME'); ?>",
                      },  

                  mc_name_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOP_NAME_AR'); ?>",
                      },

                      address: {
             
                        required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
  
                       address_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },

                        google_map_address: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_URL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_URL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SHOP_URL')); ?> <?php endif; ?>",
                      },    

                        mc_img: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      }, 

                       about: {

                         required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); ?>",
               
                      },
  
                       about_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); ?>",
                      },

                       address_image: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },    
                      mc_tnc: {
              required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_TNC'); ?>",
                 accept: "<?php if(Lang::has(Session::get('lang_file').'.VALIDFILEPDF')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VALIDFILEPDF')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.VALIDFILEPDF')); ?> <?php endif; ?>"
                      },   
                           mc_tnc_ar: {
                required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_TNC'); ?>",
                 accept: "<?php echo lang::get('mer_ar_lang.VALIDFILEPDF'); ?>"
             
                      },                  
                     
                },
               invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                <?php if($mer_selected_lang_code !='en'): ?>

                    if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }    
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  

                    if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  

                     if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }               
                  
                    if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }


                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }


                    if (typeof valdata.address_image != "undefined" || valdata.address_image != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }

                    

                  <?php else: ?>
                   if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  
                    
                     if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    } 

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  

                    if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  

                     if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }               
                  
                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 

                    if (typeof valdata.address_image != "undefined" || valdata.address_image != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    } 

                     if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    

<?php endif; ?>




                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
﻿ <?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $hall_leftmenu =1; ?> 

       <div class="merchant_vendor"> 


         <!-- HEADER SECTION -->

        <!-- END HEADER SECTION -->
      <div class="profile_box">

         <!--PAGE CONTENT -->
        <div class="profile_area">
           
                <div class="inner">
                    
            <div class="row change_psw_page">
<div class="col-lg-12">
<header>
           <h5 class="global_head"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')!= '')   ? trans(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_CHANGE_PASSWORD')); ?>

        
            
        </header>

 <?php if(Session::has('pwd_error')): ?>

<div class="error"><?php echo Session::get('pwd_error'); ?></div>
 
		<?php endif; ?>
 <?php if(Session::has('success')): ?>

 <div class="alert alert-info"><?php echo e(Session::get('success')); ?></div>
	 
		<?php endif; ?>
    <div class="box">
        


	 





        <div id="div-1" class="global_area">
  <?php echo Form::open(array('url'=>'change_password_submit','class'=>'form-horizontal', 'id'=>'forgotpass')); ?>   
	<div id="error_msg" class="passerror"> </div>        


		<input type="hidden" value="<?php echo e($mer_id); ?>" name="merchant_id" id="merchant_id">
 
                <div class="form_row">
			<div class="form_row_left">
                    <label class="form_label"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_OLD_PASSWORD')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_OLD_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_OLD_PASSWORD')); ?></label>

                    <div class="info100">
					<?php echo e(Form::password('oldpwd', array('class' => 'pass_input','id' => 'oldpwd'))); ?>

                       
                    </div>
					</div>
					
                </div>
				
                <div class="form_row">
                <div class="form_row_left">
					 <label class="form_label"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_NEW_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_NEW_PASSWORD') :  trans($MER_OUR_LANGUAGE.'.MER_NEW_PASSWORD')); ?></label>
					   <div class="info100">
					<?php echo e(Form::password('pwd', array('class' => 'pass_input','id' => 'pwd'))); ?>

                      
                    </div>
					
					</div>
				</div>
                <div class="form_row">
				<div class="form_row_left">
                    <label for="text2" class="form_label"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD')  : trans($MER_OUR_LANGUAGE.'.MER_CONFIRM_PASSWORD')); ?></label>

                    <div class="info100">
					<?php echo e(Form::password('confirmpwd', array('class' => 'pass_input','id' => 'confirmpwd'))); ?>

                        
                    </div>
					</div>
                </div>
				<div class="form_row">
				<?php echo Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'form_label', 'for' => 'pass1'])); ?>

                   

                    <div class="info100">
                     <div class="form_row_left">
                     <button id="updatepwd"   class="form_button" style="color:#fff"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE'): trans($MER_OUR_LANGUAGE.'.MER_UPDATE')); ?></button>
                    </div>
                   
                    </div>
					  
                </div>

                
				<?php echo e(Form::close()); ?>

        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 </div>   </div>
        </div>
    

<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>   

<script type="text/javascript">
  
$("#forgotpass").validate({
                  ignore: [],
                  rules: {
                  oldpwd: {
                       required: true,
                      },

                       pwd: {
                       required: true,
                      },

                       confirmpwd: {
                       required: true,
                      },
 
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             








           messages: {
             oldpwd: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_OLD_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_OLD_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_OLD_PASSWORD')); ?> <?php endif; ?>",
                      },  

                 pwd: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_NEW_PASSWORD')); ?> <?php endif; ?>",
                      },  

                         confirmpwd: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD')); ?> <?php endif; ?>",
                      },   
                    
                     
                },

                invalidHandler: function(e, validation){
                     

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
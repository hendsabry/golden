<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $hall_leftmenu =1; ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor">
<?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
  <div class="inner">
   <div class="service_listingrow"> 
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </h5>
    </div>

<?php
$id = request()->id;
$sid = request()->sid;
?>

<?php echo Form::open(array('url'=>"hall-order/{$hid}/{$bid}",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?>



 <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
      <div class="filter_area">
      <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?> 
        <div class="search_filter"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></div>
           
      

    <div class="order-filter-line order-line">
    <div class="of-date-box">
       <input type="hidden" id="hid" name="hid" value="<?php echo e($hid); ?>">
        <input type="hidden" id="bid" name="bid" value="<?php echo e($bid); ?>">
      <input type="text" class="cal-t" value="<?php echo e(request()->date_to); ?>" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.Start_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Start_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Start_Date')); ?> <?php endif; ?>" name="date_to" autocomplete="off" id="date_to" /><input type="text" value="<?php echo e(request()->from_to); ?>" class="cal-t" autocomplete="off" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.End_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.End_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.End_Date')); ?> <?php endif; ?>" id="datepicker"  name="from_to"/></div><div class="of-orders">

      <!--<select name="order_days">
      <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT_ORDERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT_ORDERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_ORDERS')); ?> <?php endif; ?></option>
      <option value="10_days" <?php if(request()->order_days =='10_days'): ?> <?php echo e('selected'); ?> <?php endif; ?> ><?php if(Lang::has(Session::get('mer_lang_file').'.Last_10_Days')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Last_10_Days')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Last_10_Days')); ?> <?php endif; ?> </option>
      <option  value="1_month" <?php if(request()->order_days =='1_month'): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php if(Lang::has(Session::get('mer_lang_file').'.Last_1_Month')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Last_1_Month')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Last_1_Month')); ?> <?php endif; ?> </option> 
      <option  value="6_month" <?php if(request()->order_days =='6_month'): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php if(Lang::has(Session::get('mer_lang_file').'.Last_6_Months')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Last_6_Months')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Last_6_Months')); ?> <?php endif; ?> </option>
      <option  value="1_year" <?php if(request()->order_days =='1_year'): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php if(Lang::has(Session::get('mer_lang_file').'.Last_1_Years')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Last_1_Years')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Last_1_Years')); ?> <?php endif; ?> </option>
      </select>-->
      </div>
    <div class="search-box-field mems ">
            <select name="status" id="status" class="city_type">
              <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_STATUS')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_STATUS')); ?> <?php endif; ?> </option>
              <option value="1" <?php if(request()->status =='1'): ?> <?php echo e('selected'); ?> <?php endif; ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.IN_PROCESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.IN_PROCESS')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.IN_PROCESS')); ?> <?php endif; ?></option>

              <option value="2" <?php if(request()->status =='2'): ?> <?php echo e('selected'); ?> <?php endif; ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.COMPLETE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COMPLETE')); ?> <?php endif; ?></option>
            </select>
            <input type="hidden" id="serachfirstfrm" name="serachfirstfrm" value="1">
            <input name="" type="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''): ?><?php echo e(trans(Session::get('mer_lang_file').'.MER_APPLY')); ?><?php else: ?><?php echo e(trans($MER_OUR_LANGUAGE.'.MER_APPLY')); ?><?php endif; ?>" class="applu_bts" />
          </div>
       <span id="todata" class="error"></span>
      <?php echo Form::close(); ?>


       <?php echo Form::open(array('url'=>"hall-order?hid={$hid}&bid={$bid}",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter2')); ?>


      <div class="filter_right" style="width:100%;">  
        <input type="hidden" id="hid" name="hid" value="<?php echo e($hid); ?>">
        <input type="hidden" id="bid" name="bid" value="<?php echo e($bid); ?>">
         <input name="searchkeyword" type="text" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SEARCH')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?> <?php endif; ?>" value="<?php echo e(request()->searchkeyword); ?>" />
          <input type="button" class="icon_sch" id="submitdata"  onclick="submit();" />
        </div>
      <?php echo Form::close(); ?>

    </div> <!-- order-filter-line -->
    
      </div>
      <!-- filter_area --> 
      
    
      <div class="global_area"><!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap"> 
      <div class="panel-body panel panel-default">
      <?php if($getorderedproducts->count() < 1): ?>
                <div class="no-record-area"> <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?> </div>
                <?php else: ?>
      <div class="table merchant-hall-table order_table">
      <div class="tr">
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Order_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Order_id')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Order_id')); ?> <?php endif; ?></div>
					  
					  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL')); ?> <?php endif; ?></div>

                      
             
            <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PAYMENTREQUEST')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST')); ?> <?php endif; ?></div>
                  
                    
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_AMOUNT')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')); ?> <?php endif; ?></div>
                      <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PAID_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PAID_AMOUNT')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PAID_AMOUNT')); ?> <?php endif; ?></div>
					  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Insuranceamount')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Insuranceamount')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS')); ?> <?php endif; ?></div>
                    <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?></div>
                  </div>

               
           <?php $__currentLoopData = $getorderedproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderedproductbasicinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <?php $getCustomer = Helper::getuserinfo($orderedproductbasicinfo->cus_id); 
                        $isalreadymaderequest = Helper::ispaymentreuqest($orderedproductbasicinfo->order_id,$orderedproductbasicinfo->product_id);
                    
                        $ordertime=strtotime($orderedproductbasicinfo->created_at);
                       $orderedtime = date("d M Y",$ordertime);

                    ?> 
    

           <div class="tr">
<div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Order_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Order_id')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Order_id')); ?> <?php endif; ?>">
                    <?php echo e(isset($orderedproductbasicinfo->order_id) ? $orderedproductbasicinfo->order_id : ''); ?></div>
                     <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL')); ?> <?php endif; ?>"><?php echo e(isset($getCustomer->cus_name) ? $getCustomer->cus_name : ''); ?><br>
                    <?php echo e(isset($getCustomer->email) ? $getCustomer->email : ''); ?></div>
                    
           
            <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE')); ?> <?php endif; ?>"><?php echo e($orderedtime); ?></div>
					  
                     <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PAYMENTREQUEST')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST')); ?> <?php endif; ?>">
                       <?php
                       $vatamou=$orderedproductbasicinfo->paid_total_amount-$orderedproductbasicinfo->insurance_amount;
                      $vatamonu = Helper::calculatevat($orderedproductbasicinfo->order_id,$vatamou);
                        $totalnetamount=$orderedproductbasicinfo->paid_total_amount + $vatamonu;
                      ?>
                      <?php 
                        if($orderedproductbasicinfo->status!=1){
                      if ($isalreadymaderequest>0){ 
                      ?> <?php if(Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.COMPLETE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COMPLETE')); ?> <?php endif; ?> 
                   <?php }     else{ ?>
                     <span id="paymentstatus"> <a href="#" onclick="paymentrequest(<?php echo e($mer_id); ?>,<?php echo e($orderedproductbasicinfo->product_id); ?>,<?php echo e($orderedproductbasicinfo->order_id); ?>,<?php echo e($totalnetamount); ?>);">
                      <?php if(Lang::has(Session::get('mer_lang_file').'.REQUEST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.REQUEST')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.REQUEST')); ?> <?php endif; ?>
                    </a>
                      </span>
                      <?php } }else{ ?>
                      <?php if(Lang::has(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.WAIT_FOR_ORDER_COMPLETE')); ?> <?php endif; ?>
                    <?php } ?>
                  </div>
                     
                    
                     <div class="td td5" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_AMOUNT')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')); ?> <?php endif; ?>">
                        <?php $remaingamount=$orderedproductbasicinfo->total_price - $orderedproductbasicinfo->paid_total_amount; ?>
                      <?php echo e(number_format((float)$remaingamount, 2, '.', '')); ?></div>
					  
					  
                      <div class="td td6" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PAID_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PAID_AMOUNT')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PAID_AMOUNT')); ?> <?php endif; ?>">
                        
                      <?php echo e(number_format((float)$totalnetamount, 2, '.', '')); ?></div>
					  
					  <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Insuranceamount')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Insuranceamount')); ?> <?php endif; ?>">
					  <?php
					   if(isset($orderedproductbasicinfo->refund_amount) && ($orderedproductbasicinfo->refund_amount!='0' || $orderedproductbasicinfo->refund_amount!='0.00' || $orderedproductbasicinfo->refund_amount!=''))
					   { 
					     if(Lang::has(Session::get('mer_lang_file').'.MER_REQUEST_SENT')!= ''){ echo trans(Session::get('mer_lang_file').'.MER_REQUEST_SENT');}else{ echo trans($MER_OUR_LANGUAGE.'.MER_REQUEST_SENT');} 
						 
						 }else{
					     if(isset($orderedproductbasicinfo->insurance_amount) && ($orderedproductbasicinfo->insurance_amount!='0' || $orderedproductbasicinfo->insurance_amount!='0.00' || $orderedproductbasicinfo->insurance_amount!='')){echo number_format($orderedproductbasicinfo->insurance_amount,2);?><p><a class="dialog_link" rel="<?php echo $orderedproductbasicinfo->id.'-'.$orderedproductbasicinfo->insurance_amount; ?>" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.REQUEST_FOR_AMT_RETURN')); ?> <?php endif; ?></a></p><?php }else{ echo number_format($orderedproductbasicinfo->insurance_amount,2);}
					  }
					  ?>
					  </div>
					  
                     <div class="td td7" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS')); ?> <?php endif; ?>">
                      <?php if($orderedproductbasicinfo->status==1){ ?>
                      <select name="ost" onchange="updateproductstatus('<?php echo e($orderedproductbasicinfo->id); ?>',this.value);">
                        <option value="1"><?php if(Lang::has(Session::get('mer_lang_file').'.IN_PROCESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.IN_PROCESS')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.IN_PROCESS')); ?> <?php endif; ?></option>
                        <option value="2"> <?php if(Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.COMPLETE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COMPLETE')); ?> <?php endif; ?></option>
                      </select>
                        <?php }else{ ?> 

                        <?php if(Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.COMPLETE')); ?>  
                          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COMPLETE')); ?> <?php endif; ?> 

            <?php } ?> 


                    </div>
					
                        <div class="td td8 view_center" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?>"><a href="<?php echo e(url('')); ?>/hall-orders-details?hid=<?php echo e($hid); ?>&bid=<?php echo e($bid); ?>&cust_id=<?php echo e($orderedproductbasicinfo->cus_id); ?>&orderid=<?php echo e($orderedproductbasicinfo->order_id); ?>"><img src="<?php echo e(url('')); ?>/public/assets/img/view-icon.png" title="<?php if(Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_view_title')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_view_title')); ?> <?php endif; ?>" alt="" /></a></div>
                     
                 </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>  
      </div>
      
      </div>
      
      </div></div></div></div>
              
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
	<div class="overlay_popup"></div>
<div id="dialog" title="<?php if(Lang::has(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN_INSUR')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN_INSUR')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.REQUEST_FOR_AMT_RETURN_INSUR')); ?> <?php endif; ?>" style="display:none;">
  <form name="frm_refund" id="frm_refund" action="<?php echo e(route('refund_insurance_amount_update')); ?>" method="post" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

  <input type="hidden" name="insuranceid" class="insuranceid"/>
  <input type="hidden" name="at_in" value="" class="at_in_id showdataid"/>
  <div class="dialog-row">
  <div class="dialog-label"> <?php if(Lang::has(Session::get('mer_lang_file').'.DEPOSITED_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DEPOSITED_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DEPOSITED_AMOUNT')); ?> <?php endif; ?> </div>
  <div class="dialog-field"><strong>SAR <span class="showdataid"></span></strong></div>
  </div>
    <div class="dialog-row">
  <div class="dialog-label"><?php if(Lang::has(Session::get('mer_lang_file').'.RETURN_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.RETURN_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.RETURN_AMOUNT')); ?> <?php endif; ?></div>
  <div class="dialog-field">
  <input type="text" class="showdataid amt-t amt_t_new" name="refund_amount" id="refund_amount" required onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" onblur="return validAmount();"/>
  </div>
  <span class="showerror_msg"></span>
  </div>
    <div class="dialog-row">
  <div class="dialog-label">Note</div>
  <div class="dialog-field"><textarea name="note" id="note"/></textarea></div>
  </div>
  <div class="dialog-btn-row">
<input type="submit" class="buttonshowhide" name="refund" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_REFUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_REFUND')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_REFUND')); ?> <?php endif; ?>"/>
  </div>

  </form>
</div>

<!-- merchant_vendor --> 
 <script>
 $(function() {
$( "#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
});

 $(function() {
 $("#datepicker").change(function () {
    var endDate  = document.getElementById("datepicker").value;
    var startDate = document.getElementById("date_to").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("datepicker").value = "";
         <?php if($mer_selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");
        <?php else: ?>
        $('#todata').html("End date should be greater than Start date");
        <?php endif; ?>
        $('#to').show();
    }
    else
    {
         $('#todata').html("");

    }
});
 });

</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script type="text/javascript">
  function updateproductstatus(orderproductid,statusvalue){
  
var orderproductid=orderproductid;
  var statusvalue=statusvalue;
        $.ajax({ 
                      
              url:"<?php echo e(url('abaya-order/getproductstatus')); ?>",     
            type: 'post', // performing a POST request
            data : {
              orderproductid: orderproductid,
              orderstatus: statusvalue,
              _token: '<?php echo e(csrf_token()); ?>'
            },
            datatype: 'application/json',
                  success: function(data) {
               var json = JSON.stringify(data);
                var obj = JSON.parse(json);
          console.log(obj);
          
              },
                  error: function() { alert('Failed!'); return false; },
        
        });
  }

</script>
<script type="text/javascript">
  
  function submitfrm(){ 
 
    document.searchfrm.submit();
  }
</script>

<?php     
	$putY = Session::get('menuss');
	if($mer_selected_lang_code !='en' && $putY==5) 
	{
	$hotalHall ='كبير'; 
	}
	elseif($mer_selected_lang_code !='ar' && $putY==5) 
	{
	$hotalHall ='Large';
	}
	elseif($mer_selected_lang_code !='en' && $putY==6) 
	{
	$hotalHall ='صغير';
	}
	elseif($mer_selected_lang_code !='ar' && $putY==6) 
	{
	$hotalHall ='Small';
	}
	else
	{
	$hotalHall ='';
	}
 ?>

<script type="text/javascript">
  
  function paymentrequest(merchant_id,product_id,order_id,amount){

      $.ajax({ 
                      
              url:"<?php echo e(url('/requestpayment')); ?>",     
            type: 'post', // performing a POST request
            data : {
              main_cat_id: '<?=$putY?>',
              merchant_id: merchant_id,
              product_id: product_id,
              order_id: order_id,
              amount: amount,
              _token: '<?php echo e(csrf_token()); ?>'
            },
            datatype: 'application/json',
                  success: function(data) {
               var json = JSON.stringify(data);
                var obj = JSON.parse(json);
          console.log(obj);
            
              document.getElementById('paymentstatus').innerHTML = 'Complete';
          
              },
                  error: function() { /*alert('Failed!');*/ return false; },
        
        });

  }
</script>
<script>
$('.dialog_link').click(function() 
{
    $('#dialog').css('display','block');
	$('.overlay_popup').css('display','block');
    var relval = $(this).attr('rel');
	var arraydata = relval.split('-');
	//alert(arraydata[0]);
	$('.insuranceid').val(arraydata[0]);
	$('.showdataid').val(arraydata[1]);
	$('.showdataid').html(arraydata[1]);
	$("#dialog").dialog();
	$('.ui-button').addClass('demo_pop');
	return false;
});

$(document).on('click', '.demo_pop', function() 
{
    $('.overlay_popup').hide();
});
/*
$('body').on('click','#ui-datepicker-div', function(){
	  $('.overlay_popup').hide();
});
*/
function validAmount()
{
 var old_ant = $('.at_in_id').val();
 var new_ant = $('.amt_t_new').val();
 if(old_ant !='' && new_ant !='')
 {
   if(new_ant > old_ant)
   {
    $('.showerror_msg').html('<span style="color:red;">Not Valid Amount</span>');
	$('.buttonshowhide').css('display','none');
    return false;
   }
   else
   {
     $('.showerror_msg').html('');
     $('.buttonshowhide').css('display','block');
     return true;
   }
 }
 else
 {
   return false;
 }
}
</script>

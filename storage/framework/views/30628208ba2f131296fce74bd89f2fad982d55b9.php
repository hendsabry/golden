<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>

    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
         <div class="right_col" role="main">
          <div id="appUserList">
            <div class="clearfix"></div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="page-title">
              <div class="title_left">
                  <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION')); ?></h3>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hight_overhid">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                <input id="from_date"  placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_DATE')); ?>" class="form-control" type="text">
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
              <select v-model="order_type" class="form-control dtmagin">
                 <option value="" selected="selected"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_TYPE')); ?></option>
                  <option  value="1"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_Hall')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_Hall') : trans($ADMIN_OUR_LANGUAGE.'.BACK_Hall')); ?></option>
                  <option  value="2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATES') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DATES')); ?></option>
                  <option  value="3"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_BUFFET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BUFFET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BUFFET')); ?></option>
              </select>
             </div>
             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
            <button type="submit" :disabled="isProcessing" @click="search" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
            <button type="submit" :disabled="isProcessing" @click="clearFilter" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
              </div>
            </div>
	 <input id="lang" class="form-control" value="<?php echo e(Session::get('admin_lang_code')); ?>" type="hidden">
            </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="add-manage-details">
        <ul class="nav nav-tabs">
        <li id="en" class="active"><a data-toggle="tab" href="#home"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CREDITED')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CREDITED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CREDITED')); ?></a></li>
        <li id="ar"><a data-toggle="tab" href="#menu1"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DEBITED')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DEBITED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DEBITED')); ?></a></li>
        </ul>
        <div class="tab-content">
        <div id="home" class="tab-pane bordr-bg fade in active english-sec">
          <div class="paddmar-block padd-l-r UsersList">
                <button type="submit" :disabled="isProcessing" @click="downloadCsvFilterCredit" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DOWNLOAD_CSV')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DOWNLOAD_CSV') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DOWNLOAD_CSV')); ?></button>
 <div class="table-responsive paddnone">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title numrical_2"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_TYPE')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYER_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYER_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYER_TYPE')); ?></th>
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_AGAINST')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_AGAINST') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_AGAINST')); ?></span>
                            </th>
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?></span>
                            </th>
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME')); ?></span>
                            </th>
                            <!--th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ASSOCIATED_VENDOR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ASSOCIATED_VENDOR') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ASSOCIATED_VENDOR')); ?></span>
                            </th-->
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_MODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_MODE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_MODE')); ?></span>
                            </th>
                             <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION')); ?></span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-if="users" v-for="(ud, index) in users">
                            <td>
                          {{ index+1 }} 
                            </td>
                            <td>{{ ud.transaction_id }}</td>
                            <td>{{ ud.get_user[0].cus_name }}</td>
                            <td>{{ ud.order_id}}</td>
                            <td>{{ud.order_amt }}</td>
                            <td>{{ud.order_date}}</td>
                            <!--td> <p >
                              {{ud.merchant_name}}
                            </p>
                            </td-->
                             <td>{{ud.order_paytype}}</td>
                            <td class="last">
                            <button  @click="ViewCredited(ud.order_id)" class="btn view-delete"><i class="fa fa-eye"></i> </button>
                            </td>
                          </tr>
                          <tr v-if="users ==''">
                            <td colspan="7"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?></td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
              </div>        
              </div>

        </div>
        <div id="menu1" class="tab-pane bordr-bg fade arabic_sec">
          <div class="paddmar-block padd-l-r UsersList">
                  <button type="submit" :disabled="isProcessing" @click="downloadCsvFilterDebited" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DOWNLOAD_CSV')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DOWNLOAD_CSV') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DOWNLOAD_CSV')); ?></button>
                    <div class="table-responsive paddnone">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title numrical_2"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_TYPE')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYER_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYER_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYER_TYPE')); ?></th>
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_AGAINST')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_AGAINST') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_AGAINST')); ?></span>
                            </th>
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?></span>
                            </th>
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME')); ?></span>
                            </th>
                            <!--th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ASSOCIATED_VENDOR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ASSOCIATED_VENDOR') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ASSOCIATED_VENDOR')); ?></span>
                            </th-->
                            <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_MODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_MODE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_MODE')); ?></span>
                            </th>
                             <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION')); ?></span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-if="marchant" v-for="(ud, index) in marchant">
                            <td>
                          {{ index+1 }}
                            </td>
                            <td>{{ ud.transation_id }}</td>
                            <td>
							<span v-if="ud.status == 2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN')); ?></span>
							<span v-else>{{ ud.payer_name }}</span>
							</td>
                            <td>{{ ud.order_id}}</td>
                            <td>{{ ud.amount}}</td>
                            <td>{{ ud.created_at_formate}}</td>
                            <!--td><p v-if="ud.get_marchant[0]">{{ ud.get_marchant[0].mer_fname}} {{ ud.get_marchant[0].mer_lname}}</p></td-->
                            <td><p v-if="ud.get_order[0]">{{ud.get_order[0].order_paytype}}</p></td>
                            <td class="last">
                            <button  @click="ViewDebited(ud.id)" class="btn view-delete"><i class="fa fa-eye"></i> </button>
                            </td>
                          </tr>
                          <tr v-if="marchant ==''">
                            <td colspan="7"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?></td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination_1.current_page_1 > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage_1(pagination_1.current_page_1 - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber_1"
              v-bind:class="[ page == isActived_1 ? 'active' : '']"> <a href="#"
              @click.prevent="changePage_1(page)">{{ page }}</a> </li>
              <li v-if="pagination_1.current_page_1 < pagination_1.last_page_1"> <a href="#" aria-label="Next"
              @click.prevent="changePage_1(pagination_1.current_page_1 + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
                    </div>        
                  </div>
      </div>
        </div>
        </div>
        </div>
                </div>
              </div>
          </div>
        </div>
          </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
      $('#from_date').datepicker({
        format: "MM dd, yyyy",
        autoclose: true,
        todayHighlight: true    
        });

        $('#from_date').on('change',function(){
          var dated = $(this).val();
        });
      });
    </script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._users = [<?php echo $users->toJson(); ?>];
window._marchant = [<?php echo $marchant->toJson(); ?>];
</script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/payment.js"></script>  
</body>

    <!-- END BODY -->
</html>
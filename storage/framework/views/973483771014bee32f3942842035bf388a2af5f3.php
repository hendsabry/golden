<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Coshabig_leftmenu =1; ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor">
<?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
  <div class="inner">
   <div class="service_listingrow"> 
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </h5>
    </div>

<?php
$id = request()->id;
$sid = request()->sid;
?>

<?php echo Form::open(array('url'=>"cosha-order/{$id}/{$sid}",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?>



 <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
      <div class="filter_area">
        <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?> 
        <div class="search_filter"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></div>
           
      

    <div class="order-filter-line order-line">
    <div class="of-date-box"><input type="text" autocomplete="off" class="cal-t" value="<?php echo e(request()->date_to); ?>" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.Start_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Start_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Start_Date')); ?> <?php endif; ?>" name="date_to" id="date_to" /><input type="text" autocomplete="off" value="<?php echo e(request()->from_to); ?>" class="cal-t" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.End_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.End_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.End_Date')); ?> <?php endif; ?>" id="datepicker"  name="from_to"/></div><div class="of-orders">
 
      </div>
    <div class="search-box-field mems ">
            <select name="status" id="status" class="city_type">
              <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_STATUS')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_STATUS')); ?> <?php endif; ?> </option>
              <option value="1" <?php if(request()->status =='1'): ?> <?php echo e('selected'); ?> <?php endif; ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE_FILTTER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE_FILTTER')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE_FILTTER')); ?> <?php endif; ?></option>

              <option value="2" <?php if(request()->status =='2'): ?> <?php echo e('selected'); ?> <?php endif; ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_INACTIVE_FILTTER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_INACTIVE_FILTTER')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_INACTIVE_FILTTER')); ?> <?php endif; ?></option>
            </select>
            <input type="hidden" id="serachfirstfrm" name="serachfirstfrm" value="1">
            <input name="" type="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''): ?><?php echo e(trans(Session::get('mer_lang_file').'.MER_APPLY')); ?><?php else: ?><?php echo e(trans($MER_OUR_LANGUAGE.'.MER_APPLY')); ?><?php endif; ?>" class="applu_bts" />
          </div>
       <span id="todata" class="error"></span>
      <?php echo Form::close(); ?>


       <?php echo Form::open(array('url'=>"cosha-order/{$id}/{$sid}",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter2')); ?>


      <div class="filter_right" style="width:100%;">  
         <input name="searchkeyword" type="text" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SEARCH')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?> <?php endif; ?>" value="<?php echo e(request()->searchkeyword); ?>" />
          <input type="button" class="icon_sch" id="submitdata"  onclick="submit();" />
        </div>
      <?php echo Form::close(); ?>

    </div> <!-- order-filter-line -->
    
      </div>
      <!-- filter_area --> 
	  
	    <div class="global_area"><!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap"> 
			<div class="panel-body panel panel-default">
 
         <?php if($getorderedproducts->count() < 1): ?> 
                <div class="no-record-area">
                   <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?>
              </div>

                  <?php else: ?>
			
			<div class="table merchant-order-table order_table">
			<div class="tr">
        <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL')); ?> <?php endif; ?></div>
					  
					  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PAYMENTREQUEST')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST')); ?> <?php endif; ?></div>
                  
                    
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?></div>
						  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Insuranceamount')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Insuranceamount')); ?> <?php endif; ?></div>  
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS')); ?> <?php endif; ?></div>
                    <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?></div>
                  </div>
            <?php  $couponAmt=0; $couponcodeinfo=''; ?>
                  <?php $__currentLoopData = $getorderedproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderedproductbasicinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php $getCustomer = Helper::getuserinfo($orderedproductbasicinfo->cus_id); 
                  $ordertime=strtotime($orderedproductbasicinfo->created_at);
                  $orderedtime = date("d M Y",$ordertime);
                   $couponAmt     = Helper::getorderedfromcoupanamount($orderedproductbasicinfo->order_id);
                         $couponcodeinfo=$orderedproductbasicinfo->coupon_code;
                      $isalreadymaderequest = Helper::ispaymentreuqest($orderedproductbasicinfo->order_id,$orderedproductbasicinfo->product_id);
                      $wisiinsurance=0;
                      $wisiinsurance=$orderedproductbasicinfo->insurance_amou;
                  ?> 
    
           <div class="tr">
                     <div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?>"><?php echo e($orderedproductbasicinfo->order_id); ?></div>
                     <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL')); ?> <?php endif; ?>">
                      <?php if(isset($getCustomer->cus_name) && $getCustomer->cus_name!=''){echo $getCustomer->cus_name;}else{echo 'N/A';} if(isset($getCustomer->email) && $getCustomer->email!=''){echo '<br />'.$getCustomer->email;}else{echo '<br />N/A';} ?>
                  </div>
           
            <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?>"><?php echo e($orderedtime); ?></div>
                     <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PAYMENTREQUEST')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST')); ?> <?php endif; ?>">
                      <?php
                         $nam=$orderedproductbasicinfo->sum-$wisiinsurance-$couponAmt;
                      $vatamonu = Helper::calculatevat($orderedproductbasicinfo->order_id,$nam);
                        $totalnetamount=$nam + $vatamonu;
                      ?>

<?php 
                          if($orderedproductbasicinfo->status!=1){
                      if (isset($isalreadymaderequest) && $isalreadymaderequest>0){ ?> <?php if(Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.COMPLETE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COMPLETE')); ?> <?php endif; ?>  <?php } else{ ?>
                     <span id="paymentstatus"> <a href="#" onclick="paymentrequest(<?php echo e(isset($mer_id) ? $mer_id : ''); ?>,<?php echo e($orderedproductbasicinfo->product_id); ?>,<?php echo e($orderedproductbasicinfo->order_id); ?>,<?php echo e($totalnetamount); ?>);">
                      <?php if(Lang::has(Session::get('mer_lang_file').'.REQUEST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.REQUEST')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.REQUEST')); ?> <?php endif; ?>
                    </a>
                      </span>
                      <?php }  }else{ ?>
                      <?php if(Lang::has(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.WAIT_FOR_ORDER_COMPLETE')); ?> <?php endif; ?>
                    <?php } ?>
                     </div>
                     
                    
                     <div class="td td5" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?>"> 

                      <?php echo e(number_format((float)$totalnetamount, 2, '.', '')); ?></div>
					  
					  
					  <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Insuranceamount')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Insuranceamount')); ?> <?php endif; ?>">
					  <?php
					   if(isset($orderedproductbasicinfo->refund_amount) && ($orderedproductbasicinfo->refund_amount!='0' || $orderedproductbasicinfo->refund_amount!='0.00' || $orderedproductbasicinfo->refund_amount!=''))
					   { 
					     if(Lang::has(Session::get('mer_lang_file').'.MER_REQUEST_SENT')!= ''){ echo trans(Session::get('mer_lang_file').'.MER_REQUEST_SENT');}else{ echo trans($MER_OUR_LANGUAGE.'.MER_REQUEST_SENT');} 
						 
						 }else{
					     if(isset($orderedproductbasicinfo->insurance_amou) && ($orderedproductbasicinfo->insurance_amou!='0' || $orderedproductbasicinfo->insurance_amou!='0.00' || $orderedproductbasicinfo->insurance_amou!='')){echo number_format($orderedproductbasicinfo->insurance_amou,2);?><p><a class="dialog_link" rel="<?php echo $orderedproductbasicinfo->id.'-'.$orderedproductbasicinfo->insurance_amou; ?>" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.REQUEST_FOR_AMT_RETURN')); ?> <?php endif; ?></a></p><?php }else{ echo number_format($orderedproductbasicinfo->insurance_amou,2);}
					  }
					  ?>
					  </div>
					  
                     <div class="td td6" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDERSTATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDERSTATUS')); ?> <?php endif; ?>">
                    
                      <?php if($orderedproductbasicinfo->status==1): ?> 
                      <select name="ost" onchange="updateproductstatus('<?php echo e($orderedproductbasicinfo->id); ?>',this.value);">
                      <option value="1"><?php if(Lang::has(Session::get('mer_lang_file').'.IN_PROCESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.IN_PROCESS')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.IN_PROCESS')); ?> <?php endif; ?></option>
                      <option value="2"> <?php if(Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.COMPLETE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COMPLETE')); ?> <?php endif; ?></option>
                      </select>
                      <?php else: ?>
                      <?php if(Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.COMPLETE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COMPLETE')); ?> <?php endif; ?>  

                      <?php endif; ?>


                    </div>
                        <div class="td td7 view_center" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?>"><a href="<?php echo e(url('')); ?>/cosha-order-details/<?php echo e($id); ?>/<?php echo e($sid); ?>/<?php echo e($orderedproductbasicinfo->id); ?>/<?php echo e($orderedproductbasicinfo->cus_id); ?>/<?php echo e($orderedproductbasicinfo->order_id); ?>"><img src="<?php echo e(url('')); ?>/public/assets/img/view-icon.png" title="<?php if(Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_view_title')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_view_title')); ?> <?php endif; ?>" alt="" /></a></div>
                     
                 </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     
  

 
			
			</div>
			<?php endif; ?>


			</div>
			
			</div></div></div></div>
              
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

	<div class="overlay_popup"></div>
<div id="dialog" title="<?php if(Lang::has(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN_INSUR')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN_INSUR')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.REQUEST_FOR_AMT_RETURN_INSUR')); ?> <?php endif; ?>" style="display:none;">
  <form name="frm_refund" id="frm_refund" action="<?php echo e(route('refund_insurance_amount_update')); ?>" method="post" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

  <input type="hidden" name="insuranceid" class="insuranceid"/>
  <input type="hidden" name="at_in" value="" class="at_in_id showdataid"/>
  <div class="dialog-row">
  <div class="dialog-label"> <?php if(Lang::has(Session::get('mer_lang_file').'.DEPOSITED_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DEPOSITED_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DEPOSITED_AMOUNT')); ?> <?php endif; ?> </div>
  <div class="dialog-field"><strong>SAR <span class="showdataid"></span></strong></div>
  </div>
    <div class="dialog-row">
  <div class="dialog-label"><?php if(Lang::has(Session::get('mer_lang_file').'.RETURN_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.RETURN_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.RETURN_AMOUNT')); ?> <?php endif; ?></div>
  <div class="dialog-field">
  <input type="text" class="showdataid amt-t amt_t_new" name="refund_amount" id="refund_amount" required onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" onblur="return validAmount();"/>
  </div>
  <span class="showerror_msg"></span>
  </div>
    <div class="dialog-row">
  <div class="dialog-label">Note</div>
  <div class="dialog-field"><textarea name="note" id="note"/></textarea></div>
  </div>
  <div class="dialog-btn-row">
<input type="submit" class="buttonshowhide" name="refund" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_REFUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_REFUND')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_REFUND')); ?> <?php endif; ?>"/>
  </div>

  </form>
</div>




<script>
 $(function() {
$( "#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
});

 $(function() {
 $("#datepicker").change(function () {
    var endDate  = document.getElementById("datepicker").value;
    var startDate = document.getElementById("date_to").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("datepicker").value = "";
         <?php if($mer_selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");
        <?php else: ?>
        $('#todata').html("End date should be greater than Start date");
        <?php endif; ?>
        $('#to').show();
    }
    else
    {
         $('#todata').html("");

    }
});
 });

</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<script>
$('.dialog_link').click(function() 
{
    $('#dialog').css('display','block');
	$('.overlay_popup').css('display','block');
    var relval = $(this).attr('rel');
	var arraydata = relval.split('-');
	//alert(arraydata[0]);
	$('.insuranceid').val(arraydata[0]);
	$('.showdataid').val(arraydata[1]);
	$('.showdataid').html(arraydata[1]);
	$("#dialog").dialog();
	$('.ui-button').addClass('demo_pop');
	return false;
});

$(document).on('click', '.demo_pop', function() 
{
    $('.overlay_popup').hide();
});
/*
$('body').on('click','#ui-datepicker-div', function(){
	  $('.overlay_popup').hide();
});
*/
function validAmount()
{
 var old_ant = $('.at_in_id').val();
 var new_ant = $('.amt_t_new').val();
 if(old_ant !='' && new_ant !='')
 {
   //alert(old_ant);
   //alert(new_ant);
   if(new_ant > old_ant)
   {
    $('.showerror_msg').html('<span style="color:red;">Not Valid Amount</span>');
	$('.buttonshowhide').css('display','none');
    return false;
   }
   else
   {
     $('.showerror_msg').html('');
     $('.buttonshowhide').css('display','block');
     return true;
   }
 }
 else
 {
   return false;
 }
}

</script>



<script type="text/javascript">
  function updateproductstatus(orderproductid,statusvalue){
  
var orderproductid=orderproductid;
  var statusvalue=statusvalue;
        $.ajax({ 
                      
              url:"<?php echo e(url('abaya-order/getproductstatus')); ?>",     
            type: 'post', // performing a POST request
            data : {
              orderproductid: orderproductid,
              orderstatus: statusvalue,
              _token: '<?php echo e(csrf_token()); ?>'
            },
            datatype: 'application/json',
                  success: function(data) {
               var json = JSON.stringify(data);
                var obj = JSON.parse(json);
              location.reload();
          
              },
                  error: function() { /*alert('Failed!');*/ return false; },
        
        });
  }

</script>
<script type="text/javascript">
  
  function submitfrm(){ 
  alert('test'); 
    document.searchfrm.submit();
  }
</script>
 <script type="text/javascript">
  
  function paymentrequest(merchant_id,product_id,order_id,amount){

      $.ajax({ 
                      
              url:"<?php echo e(url('/requestpayment')); ?>",     
            type: 'post', // performing a POST request
            data : {
			  main_cat_id: 12,
              merchant_id: merchant_id,
              product_id: product_id,
              order_id: order_id,
              amount: amount,
              _token: '<?php echo e(csrf_token()); ?>'
            },
            datatype: 'application/json',
                  success: function(data) {
               var json = JSON.stringify(data);
                var obj = JSON.parse(json);
          console.log(obj);
            
              document.getElementById('paymentstatus').innerHTML = 'Complete';
          
              },
                  error: function() { /*alert('Failed!');*/ return false; },
        
        });

  }
</script>


<footer class="footer-section">  

<div class="footer-wrapper">

<div class="footer-menu"><a href="<?php echo e(url('/')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '')  ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a> <span>|</span> <a href="<?php echo e(url('/about-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUTUS')!= '')  ?  trans(Session::get('lang_file').'.ABOUTUS'): trans($OUR_LANGUAGE.'.ABOUTUS')); ?></a> <span>|</span> <a href="<?php echo e(url('/testimonials')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Testimonials')!= '')  ?  trans(Session::get('lang_file').'.Testimonials'): trans($OUR_LANGUAGE.'.Testimonials')); ?> </a> <span>|</span> <a href="<?php echo e(url('/occasions')); ?>">#<?php echo e((Lang::has(Session::get('lang_file').'.Occasions')!= '')  ?  trans(Session::get('lang_file').'.Occasions'): trans($OUR_LANGUAGE.'.Occasions')); ?> </a> <span>|</span> <a href="<?php echo e(url('/privacy-policy')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.PrivacyPolicy')!= '')  ?  trans(Session::get('lang_file').'.PrivacyPolicy'): trans($OUR_LANGUAGE.'.PrivacyPolicy')); ?> </a> <span>|</span> <a href="<?php echo e(url('/return-policy')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ReturnPolicy')!= '')  ?  trans(Session::get('lang_file').'.ReturnPolicy'): trans($OUR_LANGUAGE.'.ReturnPolicy')); ?> </a> <span>|</span> <a href="<?php echo e(url('/contact-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ContactUs')!= '')  ?  trans(Session::get('lang_file').'.ContactUs'): trans($OUR_LANGUAGE.'.ContactUs')); ?> </a> | <span><a href="<?php echo e(url('/sitemerchant')); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Merchant_Login')!= '')  ?  trans(Session::get('lang_file').'.Merchant_Login'): trans($OUR_LANGUAGE.'.Merchant_Login')); ?>  </a></span></div> <!-- footer-menu -->



<?php 

$getFbLinks        = Helper::getfblinks();

$gettwitterLinks   = Helper::gettwitterlinks();

$getgoogleLinks    = Helper::getgooglelinks();

$getinstagramLinks = Helper::getinstagramlinks();

$getpinterestLinks = Helper::getpinterestlinks();

$getyoutubeLinks   = Helper::getyoutubelinks();

$getphone          =  Helper::getphone();

$getSettingaddress =  Helper::getSettingaddress();

$getSettingemail   =  Helper::getSettingemail();

?>





<div class="footer-info-section">

<div class="footer-info-box address-box">

<div class="footer-info-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Address')!= '')  ?  trans(Session::get('lang_file').'.Address'): trans($OUR_LANGUAGE.'.Address')); ?> </div>

<div class="foo-address-line foo-address"><?php echo e($getSettingaddress); ?></div>

<div class="foo-address-line foo-phone"><?php echo e(isset($getphone) ? $getphone : ''); ?></div>

<div class="foo-address-line foo-mail"><a href="mailto:<?php echo e(isset($getSettingemail) ? $getSettingemail : ''); ?>"><?php echo e(isset($getSettingemail) ? $getSettingemail : ''); ?></a></div>

</div> <!-- footer-info-box -->



<div class="footer-info-box payment-options-box">

<div class="footer-info-heading"><?php echo e((Lang::has(Session::get('lang_file').'.PaymentOptions')!= '')  ?  trans(Session::get('lang_file').'.PaymentOptions'): trans($OUR_LANGUAGE.'.PaymentOptions')); ?> </div>

<div class="payment-option">&nbsp;</div>

</div> <!-- footer-info-box -->



<div class="footer-info-box secure-shopping-box">

<div class="footer-info-heading"><?php echo e((Lang::has(Session::get('lang_file').'.SecureShopping')!= '')  ?  trans(Session::get('lang_file').'.SecureShopping'): trans($OUR_LANGUAGE.'.SecureShopping')); ?> </div>

<div class="footer-norton"><img src="<?php echo e(url('/')); ?>/themes/images/norton.jpg" alt="" /></div>

</div> <!-- footer-info-box -->



<div class="footer-info-box follow-box">

<div class="footer-info-heading"><?php echo e((Lang::has(Session::get('lang_file').'.FollowUS')!= '')  ?  trans(Session::get('lang_file').'.FollowUS'): trans($OUR_LANGUAGE.'.FollowUS')); ?> </div>

 





<div class="footer-so-line">

<?php if($getFbLinks !=''): ?>

<a href="<?php echo e($getFbLinks); ?>" target="_blank" title="Facebook" class="so-icon facebook-icon">&nbsp;</a>

<?php endif; ?>

<?php if($gettwitterLinks !=''): ?>

<a href="<?php echo e($gettwitterLinks); ?>" target="_blank" title="twitter" class="so-icon twitter-icon">&nbsp;</a>

<?php endif; ?>

<?php if($getgoogleLinks !=''): ?>

<a href="<?php echo e($getgoogleLinks); ?>" target="_blank" title="Google+" class="so-icon google-plus-icon">&nbsp;</a>

<?php endif; ?>

<?php if($getinstagramLinks !=''): ?>

<a href="<?php echo e($getinstagramLinks); ?>" target="_blank" title="Instagram" class="so-icon instagram-icon">&nbsp;</a>

<?php endif; ?>

<?php if($getpinterestLinks !=''): ?>

<a href="<?php echo e($getpinterestLinks); ?>" target="_blank" title="Pinterest" class="so-icon pinterest-icon">&nbsp;</a>

<?php endif; ?>

<?php if($getyoutubeLinks !=''): ?>

<a href="<?php echo e($getyoutubeLinks); ?>" target="_blank" title="YouTube" class="so-icon youtube-icon">&nbsp;</a>

<?php endif; ?>



</div>





</div> <!-- footer-info-box -->



</div> <!-- footer-info-section -->

<div class="footer-menu mobile-footer-menu"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.PrivacyPolicy')!= '')  ?  trans(Session::get('lang_file').'.PrivacyPolicy'): trans($OUR_LANGUAGE.'.PrivacyPolicy')); ?> </a> <span>|</span> <a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.ReturnPolicy')!= '')  ?  trans(Session::get('lang_file').'.ReturnPolicy'): trans($OUR_LANGUAGE.'.ReturnPolicy')); ?>  </a> </div>

<div class="footer-copyright">&copy; <?php echo date("Y"); ?> Goldencages.com <?php echo e((Lang::has(Session::get('lang_file').'.AllRightsReserved')!= '')  ?  trans(Session::get('lang_file').'.AllRightsReserved'): trans($OUR_LANGUAGE.'.AllRightsReserved')); ?>. </div>





 </div> <!-- footer-wrapper -->



</footer> <!-- footer-section -->



<div class="overlay"></div>







<!-- flex slider -->

<!-- flex slider -->

<script type="text/javascript">

    /*jQuery(function(){

      SyntaxHighlighter.all();

    });*/

	

	/* slider thumbmail */

    jQuery(window).load(function(){

      jQuery('#carousel').flexslider({

        animation: "slide",

        controlNav: false,

        animationLoop: false,

        slideshow: false,

        itemWidth: 80,

        itemMargin: 10,

        asNavFor: '#slider'

      });



      jQuery('#slider').flexslider({

        animation: "slide",

        controlNav: false,

        animationLoop: false,

        slideshow: false,

        sync: "#carousel",

        start: function(slider){

          jQuery('body').removeClass('loading');

        }

      });

	  	  	  

	/* Testimonial */

	

	  jQuery('.flexslider1').flexslider({

		animation: "slide"

	  });

 









    });

  </script>





<script>

jQuery(document).ready(function(){

	

/* mobile menu */	

jQuery('.heamburger').click(function(){

	jQuery('.left_nav').slideToggle(500);	

	jQuery('.overlay').css('display','block');

});

jQuery('.overlay').click(function(){

	jQuery('.left_nav').slideUp(500);	

	jQuery('.overlay').css('display','none');

});



/* mobile menu */	

jQuery('.vendor_heamburger').click(function(){

	jQuery('.vendor_navigation').slideToggle(500);	

	jQuery('.popup_overlay').css('display','block');

});

jQuery('.popup_overlay').click(function(){

	jQuery('.vendor_navigation').slideUp(500);	

	jQuery('.popup_overlay').css('display','none');

});



/* Home page search Popup Bussiness*/

jQuery('.bussiness_link').click(function(){

	jQuery('.bussiness_search_popup').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});



jQuery('.popup_overlay').click(function(){

	jQuery('.bussiness_search_popup').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});





/* Home page search Popup Wedding */

jQuery('.wedding_ocaasion').click(function(){

	jQuery('.search_popup').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});

jQuery('.popup_overlay, .close_search').click(function(){

	jQuery('.search_popup').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});



/* Search page */

jQuery('.mobile-search').click(function(){

	jQuery('.search-section').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});

jQuery('.popup_overlay, .mobile-back-arrow').click(function(){

	jQuery('.search-section').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});





/* Modify Row  */

jQuery('.form-btn').click(function(){

	jQuery('.modify-search-form').fadeOut(0);

	jQuery('.after-modify-search').fadeIn(0);

});



/* Oops Popup 

jQuery('.budgetopen').click(function(){

	jQuery('.oops_popup').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});

jQuery('.popup_overlay, .oops_btn').click(function(){

	jQuery('.oops_popup').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});

*/

/**/





/* Service Lightbox */

jQuery('.serv_lightbox').click(function(){

	jQuery('.services_popup').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});

jQuery('.serv_pop_close, .popup_overlay').click(function(){

	jQuery('.services_popup').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});



/* Search page */

jQuery('.mobile-budget').click(function(){

	jQuery('.customer-budget-section').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});

jQuery('.popup_overlay, .mobile-back-arrow').click(function(){

	jQuery('.customer-budget-section').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});



/* Search page 

jQuery('.sticky_serivce').click(function(){

	jQuery('.other_serviceinc').css('right','0');

	jQuery('.othrserv_overl').fadeIn(500);

});*/

jQuery('.serv_delete, .othrserv_overl').click(function(){

	jQuery('.othrserv_overl').fadeOut(500);

	jQuery('.other_serviceinc').css('right','-300px');

});



  /* Review Popup */

jQuery('.review_popup_link').click(function(){

	var relData = $(this).attr('rel');

	var ArrData = relData.split('-');

	jQuery('.order_id').val(ArrData[0])

	jQuery('.shop_id').val(ArrData[1])

	jQuery('.vendor_id').val(ArrData[2])

	jQuery('.parentId').val(ArrData[3])

	jQuery('.commentId').val(ArrData[4])

	jQuery('.comts').val(ArrData[5])

	jQuery('.starinput').val(ArrData[6])

	if(ArrData[6]!='')

	{

	 if(ArrData[6]==1)

	 {

	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a>';

	 }

	 if(ArrData[6]==2)

	 {

	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a>'; 

	 }

	 if(ArrData[6]==3)

	 {

	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a>'; 

	 }

	 if(ArrData[6]==4)

	 {

	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a>'; 

	 }

	 if(ArrData[6]==5)

	 {

	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a>'; 

	 }

	 jQuery('.updateClass').html(starselect);

	}

	jQuery('.review_popup').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});

jQuery('.popup_overlay').click(function(){

  jQuery('.review_popup').fadeOut(500);

  jQuery('.popup_overlay').fadeOut(500);

}); 



/* Checkout Payment Tabs */

jQuery('.paymt_tab li a').click(function(event){

	event.preventDefault();

	jQuery(this).parent().addClass('current-payment');

	jQuery(this).parent().siblings().removeClass('current-payment');		

	var tab = jQuery(this).attr('href');

	jQuery('.payment-option-inner-box').not(tab).hide();

	jQuery(tab).show();

});





/* Common Popup */

jQuery('.open_popup').click(function()

{    

	jQuery('.common_popup').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);



	jQuery('#occasions_date').val('');

	jQuery('#return_date').val('');

	jQuery('#rentfinal_price').html('');

	

 

});

jQuery('.popup_overlay, .close_popup_btn').click(function(){

	jQuery('.common_popup').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});







/* openpop Popup */

jQuery('.rentpopup_open').click(function(){

	jQuery('.openpop').fadeIn(500);

	jQuery('.popup_overlay').fadeIn(500);

});

jQuery('.popup_overlay, .close_popup_btn').click(function(){

	jQuery('.openpop').fadeOut(500);

	jQuery('.popup_overlay').fadeOut(500);

});



});



</script>



<link rel="stylesheet" href="<?php echo e(url('/')); ?>/themes/js/scroll/jquery.mCustomScrollbar.css">	

<script src="<?php echo e(url('/')); ?>/themes/js/scroll/jquery.mCustomScrollbar.concat.min.js"></script>

<script>

jQuery(document).ready(function() { 

jQuery("#content-1").mCustomScrollbar({

	scrollButtons: {

		enable: true

	}

});



jQuery("#content-5").mCustomScrollbar({

	axis:"x",

	theme:"dark-thin",

	autoExpandScrollbar:true,

	advanced:{autoExpandHorizontalScroll:true}

});



jQuery("#menu_header").mCustomScrollbar({

	axis:"x",

	theme:"dark-thin",

	autoExpandScrollbar:true,

	advanced:{autoExpandHorizontalScroll:true}

});



jQuery("#kosha-tab").mCustomScrollbar({

	axis:"x",

	theme:"dark-thin",

	autoExpandScrollbar:true,

	advanced:{autoExpandHorizontalScroll:true}

});



});

</script>



<!-- animate tabs -->

<script type="text/javascript">

jQuery(function(){

jQuery('a[href^="#"]').click(function(e){

var target = jQuery(this).attr('href');

var strip = target.slice(1);

var anchor = jQuery("a[name='" + strip +"']");

e.preventDefault();

jQuery('html, body').animate({

scrollTop: anchor.offset().top - 50

},'slow')

});

});

</script>



<script>

jQuery("#company_logo").change(function(){

	jQuery("#file_value1").html(this.value);

});

</script>

<!-- for tab -->

<script>

function sel(num){

	for(i=1;i<=20;i++){

		if(i==num){

			jQuery('#a'+i).addClass('select');

			jQuery('#s'+i).addClass('select');

			jQuery('#table'+i).show();

		}

		else{

			jQuery('#a'+i).removeClass('select');

			jQuery('#s'+i).removeClass('select');

			jQuery('#table'+i).hide();

		}

	}

}

</script>







<script type="text/javascript">

	function Lang_change(str) 

	{ 

		var language_code = str;

		var token =  <?php csrf_token(); ?>

		jQuery.ajax

		({

			type:'GET',

            url:"<?php echo url('new_change_languages');?>",

            data:{'Language_change':language_code,'csrf-token':token},

            success:function(data)

			{

				//alert(data);

				window.location.reload();

            }

        });

	}

</script>





<script src="<?php echo e(url('/')); ?>/themes/js/ImageMaps.js"></script>

<script src="<?php echo e(url('/')); ?>/themes/js/imageMapResizer.js"></script>

<script>

jQuery(document).ready(function(e) {

jQuery('img[usemap]').rwdImageMaps(); 

 jQuery('area').on('click', function() {

 });

});

</script>

<script src="<?php echo e(url('/')); ?>/themes/js/jquery_bpopup_min.js"></script>

   <script language="javascript">

   $(".try_now_page_one").click(function(e) {

   e.preventDefault();

   $("#try_now_page_one_popup").bPopup(

   {

   modalClose:false

   });

   return false;

   });

   </script>





<?php if(isset($lat) && isset($long) &&  $lat!='' && $long!=''){ ?>

    <script type="text/javascript">

        var map;

        

        function initMap() {                            

            var latitude = <?php echo $lat; ?>; // YOUR LATITUDE VALUE

            var longitude = <?php echo $long; ?>; // YOUR LONGITUDE VALUE            

            var myLatLng = {lat: latitude, lng: longitude};            

            map = new google.maps.Map(document.getElementById('map'), {

              center: myLatLng,

              zoom: 12                    

            });

                    

            var marker = new google.maps.Marker({

              position: myLatLng,

              map: map,

              title: latitude + ', ' + longitude 

            });            

        }

        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5aRWiRPg8adQYr1uK51Efo_GZe8lZ1N8&callback=initMap" ></script>

<?php } ?>





  

  <script language="JavaScript">



 /*

    document.onkeypress = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

           //alert('No F-12');

            return false;

        }

    }

    document.onmousedown = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

            //alert('No F-keys');

            return false;

        }

    }

document.onkeydown = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

            //alert('No F-keys');

            return false;

        }

    }

 



 

var message="Sorry, right-click has been disabled";

function clickIE() {if (document.all) {(message);return false;}}

function clickNS(e) {if

(document.layers||(document.getElementById&&!document.all)) {

if (e.which==2||e.which==3) {(message);return false;}}}

if (document.layers)

{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}

else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}

document.oncontextmenu=new Function("return false")



function disableCtrlKeyCombination(e)

{

var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j' , 'w');

var key;

var isCtrl;

if(window.event)

{

key = window.event.keyCode;     //IE

if(window.event.ctrlKey)

isCtrl = true;

else

isCtrl = false;

}

else

{

key = e.which;     //firefox

if(e.ctrlKey)

isCtrl = true;

else

isCtrl = false;

}

if(isCtrl)

{

for(i=0; i<forbiddenKeys.length; i++)

{

//case-insensitive comparation

if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())

{

alert('Key combination CTRL + '+String.fromCharCode(key) +' has been disabled.');

return false;

}

}

}

return true;

}*/

</script>

 



</body>

</html>
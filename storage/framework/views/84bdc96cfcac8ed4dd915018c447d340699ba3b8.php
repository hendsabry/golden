<?php $__env->startSection('content'); ?>
    <script type="text/javascript" src="<?php echo e(url('/themes/js/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
    <link href="<?php echo e(url('/themes/js/magnific-popup/magnific-popup.min.css')); ?>" rel="stylesheet" />
    <script src="<?php echo e(url('/themes/js/height.js')); ?>"></script>
    <div class="outer_wrapper">
        <div class="inner_wrap">
            <div class="hashtag-occasions-wrapper">
                <div class="form_title"><?php if(Lang::has(Session::get('lang_file').'.Occasionss')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Occasionss')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Occasionss')); ?> <?php endif; ?> </div>
                <div class="occasions-shorting-line">
                    <form action="<?php echo e(url('/occasions')); ?>" method="POST" name="filter">
                        <?php echo e(csrf_field()); ?>

                        <select name="occasion" id="occasion" onchange="submit(this.value);">
                            <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Occasion_Type')); ?> <?php endif; ?></option>
                            <?php
                            if(Session::get('lang_file')!='en_lang')
                            {
                                $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
                            }
                            else
                            {
                                $getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding Occasion');
                            }
                            foreach($getArrayOfOcc as $key=>$ocval){?>
                            <option value="" disabled="" style="color: #d2cece;"><?php echo e($ocval); ?></option>
                            <?php
                            $setOccasion = Helper::getAllOccasionNameList($key);
                            foreach($setOccasion as $val){ ?>
                            <option value="<?php echo e($val->id); ?>"<?php if(isset($_REQUEST['occasion']) && $_REQUEST['occasion']==$val->id){ echo 'selected="selected"'; }?>><?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
                            </option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </form>
                    <span><?php if(Lang::has(Session::get('lang_file').'.Short_By')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Short_By')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Short_By')); ?> <?php endif; ?>  </span> </div>
                <?php if(count($infy)>=1): ?>
                    <?php $i=0;    ?>
                    <?php $__currentLoopData = $infy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="hashtag-occasions-box <?php echo e($i); ?>">
                            <?php for($k=1;$k<=count($val);$k++): ?>
                                <?php $J = $k-1;     ?>
                                <?php if($k!=1): ?> <div class="hashtag-multi-occasions"> <?php endif; ?>
                                    <div class="hashtag-info-section">
                                        <?php if($k==1): ?>
                                            <div class="hashtag-info-left-section">
                                                <div class="hashtag-info-img"><img src="<?php echo e(url('/themes/images/user-default.jpg')); ?>" alt="" /></div>
                                                <div class="hashtag-info-name"><?php echo e(isset($infy[$i][0]->posted_by_name) ? $infy[$i][0]->posted_by_name : ''); ?></div>
                                            </div>
                                    <?php endif; ?>
                                    <!-- hashtag-info-left-section -->
                                        <div class="hashtag-info-right-section">
                                            <div class="hashtag-occasion-name"><?php if(Session::get('lang_file') != 'ar_lang'): ?> <?php echo e(isset($infy[$i][$J]->occasion_name) ? $infy[$i][$J]->occasion_name : ''); ?> <?php else: ?> <?php echo e(isset($infy[$i][$J]->occasion_name_ar) ? $infy[$i][$J]->occasion_name_ar : ''); ?> <?php endif; ?></div>
                                            <div class="hashtag-occasion-det"><?php echo e(Carbon\Carbon::parse($infy[$i][$J]->occasion_date)->format('d M Y')); ?>, <?php echo e(isset($infy[$i][$J]->occasion_venue) ? $infy[$i][$J]->occasion_venue : ''); ?></div>
                                        </div>
                                        <!-- hashtag-info-right-section -->
                                    </div>
                                    <!-- hashtag-info-section -->
                                    <div class="hashtag-occasions-gallery">
                                        <ul class="slides test">
                                        <!--li><a class="popup-link" href="<?php echo e(str_replace('thumb-','',$infy[$i][$J]->images)); ?>"><img src="<?php echo e(isset($infy[$i][$J]->images) ? $infy[$i][$J]->images : ''); ?>" alt="" /></a></li-->
                                            <?php $__currentLoopData = $infy[$i][$J]->image_url; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><a class="popup-link_<?php echo e($i); ?>" href="<?php echo e(str_replace('thumb-','',$vals)); ?>"><img src="<?php echo e(isset($vals) ? $vals : ''); ?>" alt="" /></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                    <script type="text/javascript">
                                        jQuery('.popup-link_<?php echo $i;?>').magnificPopup({
                                            type: 'image',
                                            gallery:{enabled:true}
                                        });
                                    </script>
                                    <?php if($k!=1): ?> </div> <?php endif; ?>
                            <?php endfor; ?>

                            <span class="viewall"><a href="<?php echo e(url('/occasion-details')); ?>/<?php echo e($infy[$i][0]->posted_by); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.View_All')!= '')  ?  trans(Session::get('lang_file').'.View_All'): trans($OUR_LANGUAGE.'.View_All')); ?> </a></span>
                        </div>
                        <?php $i=$i+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <div class="norecord">  <?php echo e((Lang::has(Session::get('lang_file').'.NO_RESULT_FOUND')!= '')  ?  trans(Session::get('lang_file').'.NO_RESULT_FOUND'): trans($OUR_LANGUAGE.'.NO_RESULT_FOUND')); ?>  </div>
                <?php endif; ?>
            </div>
            <div class="paginate">
                <?php echo e($getOccsaions->links()); ?>

            </div>
            <!-- occasions-wrapper -->
        </div>
        <!-- inner_wrap -->
    </div>
    <!-- outer_wrapper -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php //print_r(Session::all()); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>

    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
   <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
                    <div class="right_col" role="main">
          <div id="appUserList">
            <div class="clearfix"></div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
              <div class="page-title">
               <div class="container">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="title-block">
                  <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION')); ?></h3>
                   </div>
              <div class="back-btn-area ">
              <a href="/admin/designcard/invitation" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a>
              </div>
              <?php if ($is_send > 0) {
                ?>
              <div class="back-btn-area">
              <a href="/admin/invitation/send/<?php echo e($id); ?>/<?php echo e($id2); ?>/<?php echo e($id3); ?>" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SEND_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SEND_INVITATION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SEND_INVITATION')); ?></a>
              </div>
               <?php }?>
               </div>
               </div>
               </div>
               <input id="lang" class="form-control" value="<?php echo e(Session::get('admin_lang_code')); ?>" type="hidden">
              </div>

                  <div class="x_content UsersList">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?></th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_CONTACT_NUMBER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_CONTACT_NUMBER') : trans($BACK_INVITATION_CONTACT_NUMBER.'.OCCASION_TYPE')); ?></th>
                            <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL')); ?></span>
                            </th>
                            <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SEND_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SEND_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SEND_INVITATION')); ?></span>
                            </th>
                          </tr>
                          <input type="hidden" id="package_id" value="<?php echo e($id); ?>">
                          <input type="hidden" id="customer_id" value="<?php echo e($id2); ?>">
                           <input type="hidden" id="order_id" value="<?php echo e($id3); ?>">
                        </thead>
                        <tbody>
                          <tr v-if="users" v-for="(ud, index) in users">
                            <td>
                          {{ index+1 }}
                            </td>
                            <td>{{ ud.invite_name }}</td>
                              <td>{{ ud.invite_phone }}</td>
                              <td>{{ ud.invite_email }}</td>
                              <td><P v-if="ud.send_msg == 1">Yes</P><P v-else>No</P></td>
                          </tr>
                          <tr v-if="users ==''">
                                        <td colspan="5"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND_USER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND_USER') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND_USER')); ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>        
                  </div>
                  <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
                </div>
              </div>            
          </div>
        </div>
          </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
      $('#from_date').datepicker({
        format: "MM dd, yyyy",
        autoclose: true,
        todayHighlight: true    
        });

        $('#from_date').on('change',function(){
          var dated = $(this).val();
          //$('#from_date').val(dated);
          //$('#fill_date').val(dated);
        
        });
      });
    </script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._users = [<?php echo $users->toJson(); ?>];
</script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/electronicinvitation_invite.js"></script>  
</body>

    <!-- END BODY -->
</html>

<?php $__env->startSection('style'); ?>
<style type="text/css">
	.packages-item{
		display: none;
	}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
?>
 <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></h2>
                                <h2><?php echo e($vendordetails->address); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                	<?php
							        $getcityname = Helper::getcity($vendordetails->city_id); 
							        $mc_name = 'ci_name'; 
							        if(Session::get('lang_file')!='en_lang')
							      {
							          $mc_name = 'ci_name_ar'; 
							        }
							        echo $getcityname->$mc_name; 
							      ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>
                            	<?php if($lang != 'en_lang'): ?> <?php echo e(nl2br($vendordetails->mc_discription)); ?> <?php else: ?> <?php echo e(nl2br($vendordetails->mc_discription)); ?> <?php endif; ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                                   <?php if($vendordetails->google_map_address!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
							          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
							         </div>
							          <?php }  ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                        	<?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
                   <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                       <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php echo e($allreview->links()); ?>

                        <!-- <ul class="pagenation">
                            <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">10</a></li>
                            <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box"><iframe width="100%" height="315" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <center>
                     <ul class="link">
                        <li>
                            <div class="image"><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/roses-04.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>" <?php if(request()->type!=2): ?> class="select" <?php endif; ?>><?php echo e((Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')); ?></a></h2>
                        </li>
                        <li class="active">
                            <div class="image"><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/roses-05.png" alt=""></a></div>
                            <h2><a  <?php if(request()->type==2): ?> class="select" <?php endif; ?> href="<?php echo e(route('single-roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Single')!= '')  ?  trans(Session::get('lang_file').'.Single'): trans($OUR_LANGUAGE.'.Single')); ?></a></h2>
                        </li>
                    </ul>
                </center>
               
                <div class="row">
                    <div class="col-md-8">
                        <div class="">
                            <div class="row">
                            	<?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="col-md-6">
                                    <div class="item-style clearfix">
                                        <div class="tump">
                                            <img src="<?php echo e($val->pro_Img); ?>" alt="">
                                        </div>
                                        <div class="caption">
                                            <a href=""><?php echo e($val->pro_title); ?></a>
                                            <p><?php echo e($val->pro_desc); ?></p>
                                            <div class="flex-end">
                                                <div class="price">
                                                	
                                                	<?php echo e($val->pro_price); ?>

                                         
                                            	</div>
                                                <a href="" class="cart" id="<?php echo e($val->pro_id); ?>">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box p-15">
                            <h3 class="head-title" style="margin-top:0">Your Packages</h3>
                            <div class="all-item">
                            	<?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="packages-item" id="package_<?php echo e($val->pro_id); ?>">
                                    <div class="img">
                                        <img src="<?php echo e($val->pro_Img); ?>" alt="" />
                                    </div>
                                    <div class="title">
                                        <p><?php echo e($val->pro_title); ?></p>
                                        <p>Quantity: 1</p>
                                        <p class="price"><?php echo e($val->pro_price); ?> SAR</p>
                                        <a href="" class="delete" id="<?php echo e($val->pro_id); ?>"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                            </div>
                            <h3 class="head-title"><?php if(Lang::has(Session::get('lang_file').'.Wrapping_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Wrapping_Type')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Wrapping_Type')); ?> <?php endif; ?></h3>
                            <div class="row">
                            	<?php $__currentLoopData = $product_Option_value_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-4">
                                    <div class="item-radio">
                                        <img src="<?php echo e($type->image); ?>" alt="" />
                                        <input id="name" class="Checktype" data-price="<?php echo e(currency($type->price,'SAR',$Current_Currency, $format = false)); ?>" name="product_option_type" type="radio"  value="<?php echo e($type->id); ?>">
                                        <p class="price"><?php echo e(currency($type->price,'SAR',$Current_Currency)); ?> </p>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                            </div>
                            <h3 class="head-title"><?php if(Lang::has(Session::get('lang_file').'.Wrapping_Design')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Wrapping_Design')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Wrapping_Design')); ?> <?php endif; ?></h3>
                            <div class="row">
                            	<?php $__currentLoopData = $product_Option_value_design; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $design): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-4">
                                    <div class="item-radio">
                                        <img src="<?php echo e($design->image); ?>" alt="" />
                                          <input id="name4" class="Checkdesign"  data-price="<?php echo e(currency($design->price,'SAR',$Current_Currency, $format = false)); ?>" name="product_option_design" type="radio" value="<?php echo e($design->id); ?>">
                                        <p class="price"><?php echo e(currency($design->price,'SAR',$Current_Currency)); ?></p>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              
                            </div>
                            <br />
                            <p class="fw-700"><?php if(Lang::has(Session::get('lang_file').'.Total_Price')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Total_Price')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Total_Price')); ?> <?php endif; ?> : <span class="price">1500 SAR</span></p>
                            <div class="form-group" style="margin-top:15px">
                                <div class="koha-ready">
                                    <center>
                                        <a href="">Terms and conditions</a>
                                        <a href="" class="addtocart">Add To Cart</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <center>
            	<?php echo e($product->links()); ?>

          <!--       <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul> -->
            </center>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
$(document).ready(function(){
  $(".cart").click(function(e){
  	e.preventDefault();
    var id =$(this).attr('id');
    $("#package_"+id).show();
  });
  $(".delete").click(function(e){
  	e.preventDefault();
    var id =$(this).attr('id');
    $("#package_"+id).hide();
  });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
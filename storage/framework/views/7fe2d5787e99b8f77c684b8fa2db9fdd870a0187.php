<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap stud">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="myaccount_right">
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')); ?> </h1>  

      <div class="field_group studio_wrapper">

        <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>

        <div class="studio_wrap">
          <div class="studio_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.UPLOAD_YOUR_PHOTO')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD_YOUR_PHOTO'): trans($OUR_LANGUAGE.'.UPLOAD_YOUR_PHOTO')); ?></div>
          <?php echo Form::open(array('url'=>"insert_studio",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'studio_frm','id'=>'studio_frm')); ?>

          <div class="studio_form">
            <div class="common_row">
              <div class="col2">
                <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?></div>
                <div class="search-box-field">
                  <select class="checkout-small-box" name="city" id="city">
                    <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></option>
                     <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="color: #d2cece;"><?php echo e($cbval->co_name); ?></option>
                         <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php if(isset($_REQUEST['city']) && $_REQUEST['city']==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} ?>
                        <?php if($selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php echo e($selectedcity); ?> ><?php echo e($val->$ci_name); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                  <?php if($errors->has('city')): ?><span class="error"><?php echo e($errors->first('city')); ?></span><?php endif; ?>
                </div>
              </div>
              <div class="col2">
                <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.DATE')!= '')  ?  trans(Session::get('lang_file').'.DATE'): trans($OUR_LANGUAGE.'.DATE')); ?></div>
                <div class="search-box-field">
                  <input type="text" class="search-t-box cal-t" name="studio_date" id="studio_date" value="<?php echo Input::old('studio_date'); ?>" readonly="">
                  <?php if($errors->has('studio_date')): ?><span class="error"><?php echo e($errors->first('studio_date')); ?></span><?php endif; ?>
                </div>
              </div>
              <div class="col2">
                <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')); ?></div>
                <div class="search-box-field">
                  <select name="occasion" id="occasion">
                    <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Occasion_Type')); ?> <?php endif; ?></option>
					  <?php 
					     if(Session::get('lang_file')!='en_lang')
               {
                 $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف'); 
               }
               else
               {
                  $getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding Occasion'); 
               }
					     foreach($getArrayOfOcc as $key=>$ocval){?>
					    <option value="" disabled="" style="color: #d2cece;"><?php echo e($ocval); ?></option>
						<?php 
						$setOccasion = Helper::getAllOccasionNameList($key);
						foreach($setOccasion as $val){ ?>
						<option value="<?php echo e($val->id); ?>"<?php if(isset($_REQUEST['occasion']) && $_REQUEST['occasion']==$val->id){ echo 'selected="selected"'; }?>><?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
					   </option>
						<?php } ?>
					 <?php } ?>
                  </select>
                  <?php if($errors->has('occasion')): ?><span class="error"><?php echo e($errors->first('occasion')); ?></span><?php endif; ?>
                </div>
              </div>
              <div class="col2">
                <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.VENUE')!= '')  ?  trans(Session::get('lang_file').'.VENUE'): trans($OUR_LANGUAGE.'.VENUE')); ?></div>
                <div class="search-box-field">
                  <input type="text" class="t-box" name="venue" id="venue" value="<?php echo Input::old('venue'); ?>">
                  <?php if($errors->has('venue')): ?><span class="error"><?php echo e($errors->first('venue')); ?></span><?php endif; ?>
                </div>
              </div>
              <div class="col2 browse_row" style="height:auto;">
                <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.UPLOAD_YOUR_PIC')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD_YOUR_PIC'): trans($OUR_LANGUAGE.'.UPLOAD_YOUR_PIC')); ?></div>
                <div class="myac-file-box">
   
                    <label for="company_logo">
                      <div class="file-btn-area">
                      <div class="file-btn"><?php echo e((Lang::has(Session::get('lang_file').'.UPLOAD')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD'): trans($OUR_LANGUAGE.'.UPLOAD')); ?></div>
                      <div  class="file-value" id="file_value"></div>
                    </div>
                    </label>
                
                  <input type="file" name="upload_your_pic" class="info-file" id="company_logo" accept="image/gif, image/jpeg, image/png">                   
                </div>
              </div>
              <?php if($errors->has('upload_your_pic')): ?><span class="error"><?php echo e($errors->first('upload_your_pic')); ?></span><?php endif; ?> 
            
             <div id="img_upload"></div>   
             <div class="add-more-btn-line">
                <a id="add_button" class="add-more-link" href="javascript:void(0);"><?php if(Lang::has(Session::get('lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a> 
                 <?php $Count =  1;?>
                  <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
                </div>
                 
              </div> 
                   
            </div>
            <div class="common_row">
              <div class="col2">
                <input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ?  trans(Session::get('lang_file').'.SUBMIT'): trans($OUR_LANGUAGE.'.SUBMIT')); ?>" class="form-btn btn-info-wisitech">
              </div>
            </div>
          </div>
          <?php echo Form::close(); ?>

          <!-- studio_form -->
          <div class="studio_gallary">
            <?php echo Form::open(array('url'=>"my-account-studio",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter','id'=>'filter')); ?>

            <div class="col2">
              <select class="checkout-small-box" name="gallery_city" id="gallery_city" onchange="myFunction()">
                    <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></option>
                <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="color: #d2cece;"><?php echo e($cbval->co_name); ?></option>
                         <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php if(isset($_REQUEST['gallery_city']) && $_REQUEST['gallery_city']==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} ?>
                        <?php if($selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php echo e($selectedcity); ?> ><?php echo e($val->$ci_name); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
            </div>
            <?php echo Form::close(); ?>

            <div class="studio_gallary_row">
              <!--<div class="alert alert-info pictureformat" style="display: none;"></div>-->
              <?php if(count($setImagelist) > 0){ ?>
              <?php $__currentLoopData = $setImagelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="gallary_col3" id="del<?php echo e($value->id); ?>">
			   <div class="studio_gallary_white_box">
                <div class="studio_gallary_img gallery-img-height"><a href="<?php echo e(route('ocassion-more-image',['id'=>$value->id])); ?>"><img src="<?php echo e($value->images); ?>"><span class="total_imgcount"><span class="total_imgcount_plus">+</span> <?php $totalImage = Helper::getAllOccasionImage($value->id); echo count($totalImage);?></span></a></div>                
				<div class="studio_gallary_inner_box">
				<div class="studio-occasions occasions-head"><?php 
				  $getoccasionname = Helper::getOccasionName($value->occasion_type); 
				  $mc_name  = 'title';
				  if(Session::get('lang_file')!='en_lang')
				  {
					 $mc_name = 'title_ar';
				  }
				  
				?><a href="<?php echo e(route('ocassion-more-image',['id'=>@$value->id])); ?>"><?php echo @$getoccasionname->$mc_name; ?></a></div>	
				<div class="studio-occasions"><?php echo e(ucfirst($value->occasion_venue)); ?>, 
				<?php 
				  $getcitynamedata = Helper::getcity($value->city_id); 
				  $mc_name  = 'ci_name';
				  if(Session::get('lang_file')!='en_lang')
				  {
					 $mc_name = 'ci_name_ar';
				  }
				  echo $getcitynamedata->$mc_name;
				?>
				</div>				
				<div class="studio-occasions"><?php echo e(Carbon\Carbon::parse($value->studio_date)->format('d M Y')); ?></div>				
				</div>
				</div>
				<div rel="<?php echo e($value->id); ?>" class="studio_captoion"></div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
             <?php } 
             else
             { ?>
              <?php if(Lang::has(Session::get('lang_file').'.NO_RESULTS_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RESULTS_FOUND')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RESULTS_FOUND')); ?> <?php endif; ?>
            <?php } ?>            
            </div>
			
          </div>
          <!-- studio_gallary -->
        </div>
        <!-- studio_wrap -->
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->

<!-- Lightbox- Studio Details  -->
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"> Are you sure to delete image folder?</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/><a class="action_yes status_yes" href="javascript:void(0);"> Yes </a> <a class="action_no" href="javascript:void(0);"> No </a>  </div>
  </div>
</div>

<!-- Lightbox- Studio Details END -->

<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<script type="text/javascript">
jQuery('.studio_captoion').click(function()
{
var str = jQuery(this).attr('rel');
	jQuery('#delid').val(str);	
	jQuery('.action_popup').fadeIn(500);	
	jQuery('.overlay_popup').fadeIn(500);
});

jQuery('.action_no').click(function()
{
	jQuery('.action_popup').fadeOut(500);	
	jQuery('.overlay_popup').fadeOut(500);
});
</script>


  <script type="text/javascript">
    jQuery(document).ready(function(){
    var maxField = 9;  
    var addButton = jQuery('#add_button');  
    var wrapper = jQuery('#img_upload');
    var x = 1;
    var y = x+1;  
    jQuery(addButton).click(function(){
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="myac-file-box"><div id="remove_button"><a href="javascript:void(0);" title="Remove Field">&nbsp;</a></div><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('lang_file').'.UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="uploadyourpicmore[]" data-info='+x+'></div> ';
    jQuery(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    jQuery(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    jQuery(this).parent('div').remove(); 
    x--;  
    document.getElementById('count').value = parseInt(x);
    });
    });
  </script>
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
	var jq = $.noConflict();
	jq( function() {
	  jq( "#studio_date" ).datepicker({
	    dateFormat: 'd M yy',
		  maxDate: '0'
	  });
	} );
  </script> 
  
  <script type="text/javascript"> 
  jQuery(document).ready(function(){
  jQuery("#studio_frm").validate({
    rules: {          
          "city" : {
            required : true
          },  
          "studio_date" : {
            required : true
          },  
          "occasion" : {
            required : true
          },
          "venue" : {
            required : true
          },  
          "upload_your_pic" : {
            required:true
          },     
         },
         messages: {
          "city": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_CITY')); ?> <?php endif; ?>"
          },
          "studio_date": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_STUDIO_DATE')); ?> <?php endif; ?>"
          }, 
          "occasion": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_OCCASION')); ?> <?php endif; ?>"
          },
          "venue": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_VENUE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_VENUE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_VENUE')); ?> <?php endif; ?>"
          },
          "upload_your_pic": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_UPLOAD_IMAGE')); ?> <?php endif; ?>"
          },      
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#studio_frm").valid()) {        
    jQuery('#studio_frm').submit();
   }
  });
});

jQuery('body').on('change','#company_logo', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value").html(fake);
});

jQuery('body').on('change','#company_logo3', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value2").html(fake);
});

jQuery('body').on('change','#company_logo4', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value3").html(fake);
});

jQuery('body').on('change','#company_logo5', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value4").html(fake);
});

jQuery('body').on('change','#company_logo6', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value5").html(fake);
});

jQuery('body').on('change','#company_logo7', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value6").html(fake);
});

jQuery('body').on('change','#company_logo8', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value7").html(fake);
});

jQuery('body').on('change','#company_logo9', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value8").html(fake);
});

jQuery('body').on('change','#company_logo10', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value9").html(fake);
});

jQuery('body').on('change','#company_logo11', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value10").html(fake);
});

jQuery('body').on('change','#company_logo12', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value11").html(fake);
});

jQuery('body').on('change','#company_logo13', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value12").html(fake);
});

jQuery('body').on('change','#company_logo14', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value13").html(fake);
});

jQuery('body').on('change','#company_logo15', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value14").html(fake);
});

jQuery('body').on('change','#company_logo16', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value15").html(fake);
});

jQuery('body').on('change','#company_logo17', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value16").html(fake);
});

jQuery('body').on('change','#company_logo18', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value17").html(fake);
});


jQuery('.action_yes').click(function()
{ 
	//var str = jQuery(this).attr('rel');
	var str = jQuery('#delid').val();
    jQuery.ajax({
       type: 'get',
       data: 'occasion_id='+str,
       url: '<?php echo url('delete-my-occasion-imagewithajax'); ?>',
       success: function(responseText){
        jQuery('#del'+responseText).hide();
        //jQuery('.pictureformat').text("Deleted successfully.");
        jQuery('.action_popup').fadeOut(500); 
		jQuery('.overlay_popup').fadeOut(500);
		//location.reload();
      }       
    });
});
function myFunction() {
   document.getElementById("filter").submit();
}
/*jQuery(document).ready(function() {
		jQuery('#filter').on('change', function() {
			this.form.submit();
		});
	});*/
</script>


<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $buffet_leftmenu =1; ?>

<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <div class="right_panel">

    <div class="inner">

      <header>

        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </h5>

        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>

      </header>

      <!-- Display Message after submition --> 

      <?php if(Session::has('message')): ?>

      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>

      <?php endif; ?>

      <div class="error arabiclang"></div>

      <!-- Display Message after submition -->

      <div class="global_area">

        

        <div class="box">

          <form name="form1" method="post" id="addbranch" action="<?php echo e(route('store-branch')); ?>" enctype="multipart/form-data">

            <?php echo e(csrf_field()); ?>


            

              

                <div class="form_row">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_RESTURANT_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_RESTURANT_NAME'); ?> </span> </label>

                    <div class="info100">

                      <div class="english"> <?php echo e(isset($shopName->mc_name) ? $shopName->mc_name : ''); ?> </div>

                      <div class="arabic ar"> <?php echo e(isset($shopName->mc_name_ar) ? $shopName->mc_name_ar : ''); ?>


                        <input type="hidden" name="pid" value="<?php echo e(isset($pid) ? $pid : ''); ?>">

                        <input type="hidden" name="parent_id" value="<?php echo e($parent_id); ?>">

                        <input type="hidden" name="sid" value="<?php echo e($sid); ?>">

                      </div>

                    </div>

                  </div>

                </div>

                <div class="form_row">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); ?> </span> </label>

                    <div class="info100" >

                      <div class="english">

                        <input type="text" class="english" name="mc_name" maxlength="235"  data-validation="length required" 

		                  value="<?php echo e(isset($fetchdata->mc_name) ? $fetchdata->mc_name : ''); ?>" data-validation-length="max35">

                      </div>

                      <div class="arabic ar">

                        <input type="text" class="arabic ar" name="mc_name_ar" value="<?php echo e(isset($fetchdata->mc_name_ar) ? $fetchdata->mc_name_ar : ''); ?>"  maxlength="235" >

                      </div>

                    </div>

                  </div>

                   <div class="form_row_left common_field no_padding">

                    <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); ?> </span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span></a> </label>

                    <div class="info100">

                      <input type="url" name="google_map_address"  maxlength="250"  data-validation="length required" 

                   value="<?php echo e(isset($fetchdata->google_map_address) ? $fetchdata->google_map_address : ''); ?>" data-validation-length="max100">

                    </div>

                  </div>





               

                  <span id="maperror"></span>

                  <div class="form_row_left common_field">

          <div class="lat_left">

                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>

                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->longitude) ? $fetchdata->longitude : ''); ?>" readonly="" name="longitude" />

          </div>

          <div class="lat_right">

                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>

                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->latitude) ? $fetchdata->latitude : ''); ?>"  readonly="" name="latitude" />

          </div>

                </div>

                <div class="form_row common_field">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); ?> </span> </label>

                    <div class="input-file-area">

                      <label for="company_logo1">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>

                        <div class="file-value" id="file_value2"></div>

                      </div>

                      </label>

                      <input type="file" name="branchimage" id="company_logo1" class="info-file" accept="image/*" >

                    </div>

                    <?php if(!isset($getDb)): ?> <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span> <?php endif; ?>

                     <?php if(isset($fetchdata->mc_img) && $fetchdata->mc_img !=''): ?>

                    <div class="form-upload-img"> <img src="<?php echo e($fetchdata->mc_img); ?>"> </div> <?php endif; ?>

                  </div>

                  <!--

                  <div class="form_row_right">

                    <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); ?> </span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div" style="text-align:center;"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_address_img')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_address_img')); ?> <?php endif; ?></span></a> </label>

                    <div class="input-file-area">

                      <label for="company_logo7">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>

                        <div class="file-value" id="file_value6"></div>

                      </div>

                      </label>

                      <input accept="image/*" type="file" name="address_image" id="company_logo7" class="info-file">

                    </div>

                    <?php if(!isset($getDb)): ?> <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span> <?php endif; ?>

                    

                    <?php if(isset($fetchdata->address_image) && $fetchdata->address_image !=''): ?>

                    <div class="form-upload-img"> <img src="<?php echo e($fetchdata->address_image); ?>"></div> <?php endif; ?>

                  </div>-->







                </div>

                <div class="form_row">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.BACK_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.BACK_ADDRESS'); ?> </span> </label>

                    <div class="info100" >

                      <div class="english">

                        <input type="text" class="english" name="address" maxlength="235"  data-validation="length required" 

		                    value="<?php echo e(isset($fetchdata->address) ? $fetchdata->address : ''); ?>" data-validation-length="max35">

                      </div>

                      <div class="arabic ar">

                        <input type="text" class="arabic ar" name="address_ar" value="<?php echo e(isset($fetchdata->address_ar) ? $fetchdata->address_ar : ''); ?>"  maxlength="255" >

                      </div>

                    </div>

                  </div>

                  <div class="form_row_right common_field" >

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CITY'); ?> </span> </label>

                    <div class="info100" >

                     

                        <select name="city_id" id="status" class="city_type">

                          <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?></option>

                          

               <?php $getC = Helper::getCountry(); ?>

                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        

                          <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> <?php echo e(isset($cbval->co_name) ? $cbval->co_name : ''); ?>   </option>

                          

                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              

                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                         <?php $ci_id = $val->ci_id; ?>

                        <?php if($mer_selected_lang_code !='en'): ?>

                        <?php $ci_name= 'ci_name_ar'; ?>

                        <?php else: ?>

                         <?php $ci_name= 'ci_name'; ?>

                        <?php endif; ?>   

                        

                          <option value="<?php echo e($val->ci_id); ?>" <?php echo e(isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''); ?> ><?php echo e($val->$ci_name); ?></option>

                          

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>





            

                        </select>

                      

                    </div>

                  </div>

                </div>

                <div class="form_row ">





  <div class="form_row_left">

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>

                    <div class="info100" >

                      <div class="english">

                        <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription) ? $fetchdata->mc_discription : ''); ?> </textarea>

                      </div>

                      <div class="arabic ar">

                        <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription_ar) ? $fetchdata->mc_discription_ar : ''); ?></textarea>

                      </div>

                    </div>

                  </div>









                  <div class="form_row_right common_field" >

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); ?> </span> </label>

                    <div class="info100">

                      <select class="small-sel" name="manager"  id="manager">

                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')); ?>  

                        <?php else: ?>  <?php echo e(trans($MER_SELEST_MANAGER.'.MER_SELEST_MANAGER')); ?> <?php endif; ?></option>

                         

                      <?php $__currentLoopData = $manager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                       

                        <option value="<?php echo e($val->mer_id); ?>" <?php echo e(isset($fetchdata->branch_manager_id) && $fetchdata->branch_manager_id ==$val->mer_id ? 'selected' : ''); ?>><?php echo e($val->mer_fname); ?></option>

                         

                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

 

                      </select>

                    </div>

                  </div>

                  <input type="hidden" class="english small-sel" name="Insuranceamount" maxlength="35"  data-validation="length required" value="0" data-validation-length="max35">



                  



                </div>

                <div class="form_row ">

                

                  <div class="form_row_left english">

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>

                    <div class="input-file-area">

                      <label for="company_logo">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>

                        <div class="file-value" id="file_value1"></div>

                      </div>

                      </label>

                      <input type="file" name="mc_tnc" id="company_logo" class="info-file">

                    </div>

                    <div class="pdf_msg">

<?php if(Lang::has(Session::get('mer_lang_file').'.Please_upload_only_PDF_file')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Please_upload_only_PDF_file')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Please_upload_only_PDF_file')); ?> <?php endif; ?>

 </div>

                    <?php if(isset($fetchdata->terms_conditions)!=''): ?>

                    

                    <div class="pdf_icon"> <a href="<?php echo e($fetchdata->terms_conditions); ?>" target="_blank"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> </a> </div>

                    <input type="hidden" value="<?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?>" name="tmcvalue">

                    <?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?>


                    <?php endif; ?> </div>

                </div>



 <div class="form_row ">

                

                  <div class="form_row_left arabic ar">

                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>

                    <div class="input-file-area">

                      <label for="company_logo9">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>

                        <div class="file-value" id="file_value8"></div>

                      </div>

                      </label>

                      <input type="file" name="mc_tncar" id="company_logo9" class="info-file">

                    </div>

                    <div class="pdf_msg">

                    يرجى تحميل ملف PDF فقط

                    </div>

                    <?php if(isset($fetchdata->terms_conditions_ar)!=''): ?>

                    

                    <div class="pdf_icon"> <a href="<?php echo e($fetchdata->terms_conditions_ar); ?>" target="_blank"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> </a> </div>

                    <input type="hidden" value="<?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?>" name="tmcvaluear">

                    <?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?>


                    <?php endif; ?> </div>

                </div>



 



                

                <div class="form-btn-section english">

                  <div class="form_row_left">

                    <input type="hidden" value="<?php echo e(isset($fetchdata->mc_id) ? $fetchdata->mc_id : ''); ?>" name="itemid">

                    <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">

                  </div>

                </div>

                <div class="form-btn-section arabic ar">

                  <div class="form_row_left arbic_right_btn">

                    <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">

                  </div>

                </div>

              

          </form>

            </div>

        

      </div>

      <!-- global_area --> 

    </div>

  </div>

  <!-- right_panel --> 

</div>

<!-- merchant_vendor --> 

<script>

       

            $("form").data("validator").settings.ignore = "";

 </script> 

<script type="text/javascript">

  

$("#addbranch").validate({

                  ignore: [],

                  rules: {

                  city_id: {

                       required: true,

                      },



                       /*manager: {

                       required: true,

                      },*/



                       mc_name: {

                       required: true,

                      },



                      mc_name_ar: {

                       required: true,

                      },

                       address: {

                       required: true,

                      },

                       address_ar: {

                       required: false,

                      },



                       about: {

                       required: true,

                      },

                       about_ar: {

                       required: true,

                      },

                       <?php if(isset($fetchdata->mc_img)!=''): ?>  

                        branchimage: {

                           required:false,

                           accept:"png|jpeg|jpg|gif",        

                      },

                       <?php else: ?>

                        branchimage: {

                           required:true,

                           accept:"png|jpeg|jpg|gif",        

                      },

                      <?php endif; ?>



                      <?php if(isset($fetchdata->address_image)!=''): ?>  

                        address_image: {

                           required:false,

                           accept:"png|jpeg|jpg|gif",        

                      },

                       <?php else: ?>

                        address_image: {

                           required:false,

                           accept:"png|jpeg|jpg|gif",        

                      },

                      <?php endif; ?>









                      

                       <?php if(isset($fetchdata->terms_conditions)!=''): ?> 

                        mc_tnc: {

                           required:false,

                           accept:"pdf",

                      },

                       mc_tncar: {

                           required:false,

                           accept:"pdf",

                      },

                    <?php else: ?>

                         mc_tnc: {

                           required:true,

                           accept:"pdf",

                      },

                        mc_tncar: {

                           required:true,

                           accept:"pdf",

                      },

                      <?php endif; ?>



                     



                  },

                 highlight: function(element) {

            $(element).removeClass('error');

                },

             

           messages: {

             city_id: {

               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?>",

                      },  



                 /*manager: {

               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR')); ?> <?php endif; ?>",

                      },  */

                          mc_tnc: {

                                required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",

                                accept: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",

                        },



                       mc_tncar: {

                                required: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?>",

                                accept: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?>",

                      },



                         mc_name: {

             

                   required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_BRANCH'); ?>",

                      },   



                          mc_name_ar: {

               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_BRANCH_AR'); ?>",

                      },   



             



                       address: {

            

               required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); ?>",

                      },

  

                       address_ar: {

               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); ?>",

                      },

                        

                       about: {

          

                 required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); ?>",

                      },

  

                       about_ar: {

               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); ?>",

                      },

                  branchimage: {

                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",

                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",





                                    



                      },                                 

                      address_image: {

                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",

                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",





                                    



                      }, 

                },



                invalidHandler: function(e, validation){

                                    

                    var valdata=validation.invalid;

 console.log("invalidHandler : validation", valdata);

                <?php if($mer_selected_lang_code !='en'): ?>

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 

                    {

                        $('.english_tab').trigger('click'); 

                      

                    }

                     if (typeof valdata.address != "undefined" || valdata.address != null) 

                    {

                        $('.english_tab').trigger('click');

                           



                    }

                    if (typeof valdata.about != "undefined" || valdata.about != null) 

                    {

                        $('.english_tab').trigger('click');

                           



                    }

                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }

                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }

                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 

                    {

                        $('.english_tab').trigger('click'); 

                    



                    }

                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                  if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }



                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }



                    if (typeof valdata.mc_tncar != "undefined" || valdata.mc_tncar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }





                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

          <?php else: ?>





             if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }



                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

                     if (typeof valdata.mc_tncar != "undefined" || valdata.mc_tncar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

 

            if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 

                    {

                        $('.english_tab').trigger('click'); 

                      

                    }

                     if (typeof valdata.description != "undefined" || valdata.description != null) 

                    {

                        $('.english_tab').trigger('click');

                           



                    }

                      if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }



                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }

                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 

                    {

                        $('.english_tab').trigger('click'); 

                    



                    }

                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                  



<?php endif; ?>



                    },



                submitHandler: function(form) {



                  var mapAdd = jQuery('input[name=google_map_address]').val();

if(mapAdd !='')

{

 var long = jQuery('input[name=longitude]').val();

    var lat  =  jQuery('input[name=latitude]').val();

  if(long =='' && lat=='')

  {

  var allOk = 0;



  }

  else

  {

   var allOk = 1; 

  }

}

else

{

var allOk = 1;

}



if(allOk == 1)

{

  form.submit();

}

else

{

  $('#maperror').html("<span class='error'><?php if(Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.get_Lat_Long_issue')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue')); ?> <?php endif; ?></span>");

}

                    

                }

            });



 



</script> 

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
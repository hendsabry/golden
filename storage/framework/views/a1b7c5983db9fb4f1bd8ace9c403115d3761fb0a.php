<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="myaccount_right">
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')); ?> </h1>
      <div class="dash_select"> <a href="<?php echo e(route('my-account-studio')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.BACK_TO_STUDIO_LIST')!= '')  ?  trans(Session::get('lang_file').'.BACK_TO_STUDIO_LIST'): trans($OUR_LANGUAGE.'.BACK_TO_STUDIO_LIST')); ?></a> </div>
      <div class="field_group studio_wrapper"> <?php if(Session::has('message')): ?>
        <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>
        <div class="studio_wrap">
          <div class="studio_gallary">
            <div class="occasions-details-area">
              <?php 
                   $oc_name  = 'occasion_name';
          if(Session::get('lang_file')!='en_lang')
          {
           $oc_name = 'occasion_name_ar'; 
          }

              ?>

              <?php echo e($getImagelist->$oc_name); ?> - <?php echo e(ucfirst($getImagelist->occasion_venue)); ?>, <?php 
				  $getcitynamedata = Helper::getcity($getImagelist->city_id); 
				  $mc_name  = 'ci_name';
				  if(Session::get('lang_file')!='en_lang')
				  {
					 $mc_name = 'ci_name_ar';
				  }
				  echo $getcitynamedata->$mc_name;
				?></div>
            <div class="occasions-details-date"><?php echo e(Carbon\Carbon::parse($getImagelist->studio_date)->format('d M Y')); ?></div>
            <div class="alert alert-info pictureformat" style="display: none;"></div>
            <div class="studio_gallary_row"> <?php if(count($getOccasionImage) > 0){ ?>
              <?php $__currentLoopData = $getOccasionImage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="gallary_col3" id="del<?php echo e($value->id); ?>">
                <div class="studio_gallary_img gallery-details-img-height"><img src="<?php echo e($value->image_url); ?>"></div>
                <div rel="<?php echo e($value->id); ?>" class="studio_captoion"></div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
              <?php } 
              else
              { ?>
              <?php if(Lang::has(Session::get('lang_file').'.NO_RESULTS_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RESULTS_FOUND')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RESULTS_FOUND')); ?> <?php endif; ?>
              <?php } ?> </div>
          </div>
          <!-- studio_gallary -->
        </div>
        <!-- studio_wrap -->
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"> Are you sure to delete image?</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/><a class="action_yes status_yes" href="javascript:void(0);"> Yes </a> <a class="action_no" href="javascript:void(0);"> No </a>  </div>
  </div>
</div>

<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script type="text/javascript">
jQuery('.studio_captoion').click(function()
{
    var str = jQuery(this).attr('rel');
	jQuery('#delid').val(str);	
	jQuery('.action_popup').fadeIn(500);	
	jQuery('.overlay_popup').fadeIn(500);
});

jQuery('.action_no').click(function()
{
	jQuery('.action_popup').fadeOut(500);	
	jQuery('.overlay_popup').fadeOut(500);
});

jQuery('.action_yes').click(function()
{ 
	//var str = jQuery(this).attr('rel');
	var str = jQuery('#delid').val();
    jQuery.ajax({
       type: 'get',
       data: 'id='+str,
       url: '<?php echo url('delete-my-occasion-one-imagewithajax'); ?>',
       success: function(responseText){
        jQuery('#del'+responseText).hide();
        //jQuery('.pictureformat').text("Deleted successfully.");
        jQuery('.action_popup').fadeOut(500); 
		jQuery('.overlay_popup').fadeOut(500);
		//location.reload();
      }       
    });
});
</script>
<?php //print_r(session::all());?>

<?php  $current_route = Route::getCurrentRoute()->uri(); ?>

<?php if(isset($routemenu)): ?>
<?php
$menu=$routemenu;
$menu = strtolower($menu); ?> <?php endif; ?>

<?php if(Session::get('merchantid')): ?>

 <?php   $merchantid=Session::get('merchantid'); ?>

<?php endif; ?>

<div oncontextmenu="return false"></div>
<div id="top" >
  <div class="mobile_header">
  
  
 <div class="mobile_menu"><div class="bs-example">
    <nav class="navbar navbar-default"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <div type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </div>
      </div>
      <!-- Collection of nav links, forms, and other content for toggling -->
      <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="<?php echo e(url('sitemerchant_dashboard')); ?>" <?php if($current_route=="sitemerchant_dashboard"){?> class="active"<?php } ?> ><?php echo e((Lang::has(Session::get('mer_lang_file').'.Services')!= '') ? trans(Session::get('mer_lang_file').'.Services') : trans($MER_OUR_LANGUAGE.'.Services')); ?></a></li>
          <li><a href="<?php echo e(url('edit_merchant_info',['id' =>$merchantid])); ?>" <?php if($menu=="settings"){?> class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ? trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS')); ?></a> </li>

          <?php if(Session::get('LoginType') !='4'): ?>
          <!--li><a href="<?php echo e(url('transations')); ?>" <?php if(Request::is('transations')) {?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.TRANSACTIONS')!= '') ?  trans(Session::get('mer_lang_file').'.TRANSACTIONS') : trans($MER_OUR_LANGUAGE.'.TRANSACTIONS')); ?> </a> </li-->
          <li><a href="<?php echo e(url('fund-request')); ?>"  <?php if(Request::is('fund-request')) {?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.FUND_REQUESTS')!= '') ? trans(Session::get('mer_lang_file').'.FUND_REQUESTS') : trans($MER_OUR_LANGUAGE.'.FUND_REQUESTS')); ?> </a> </li>
         
          <li>
          	<a href="#" <?php if(Request::is('account-details') || Request::is('order-reporting') || Request::is('recieve-payment') || Request::is('my-wallet')) {?>class="active"<?php } ?> > <?php if(Lang::has(Session::get('mer_lang_file').'.Payment')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Payment')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Payment')); ?> <?php endif; ?></a>
            <ul class="submenu">
            	<li><a href="<?php echo e(url('account-details')); ?>" <?php if(Request::is('account-details')) {?>class="active"<?php } ?> ><?php if(Lang::has(Session::get('mer_lang_file').'.AccountDetail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.AccountDetail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.AccountDetail')); ?> <?php endif; ?> </a></li>
                <li><a href="<?php echo e(url('order-reporting')); ?>" <?php if(Request::is('order-reporting')) {?>class="active"<?php } ?> ><?php if(Lang::has(Session::get('mer_lang_file').'.OrderReport')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OrderReport')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OrderReport')); ?> <?php endif; ?> </a></li>
                <li><a href="<?php echo e(url('recieve-payment')); ?>" <?php if(Request::is('recieve-payment')) {?>class="active"<?php } ?> ><?php if(Lang::has(Session::get('mer_lang_file').'.ReceivedPayements')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ReceivedPayements')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ReceivedPayements')); ?> <?php endif; ?>  </a></li>

 

                 <li><a href="<?php echo e(url('my-wallet')); ?>" <?php if(Request::is('my-wallet')) {?>class="active"<?php } ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MyWallet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MyWallet')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MyWallet')); ?> <?php endif; ?>  </a></li>
            </ul>

          </li>
         
       
        <li><a href="<?php echo e(url('listmanager')); ?>" <?php if($current_route=="listmanager"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGER') : trans($MER_OUR_LANGUAGE.'.MER_MANAGER')); ?> </a> </li>
        <?php endif; ?>  

 <li><a href="<?php echo e(url('manage_assigned_invitation')); ?>" <?php if($current_route=="manage_assigned_invitation"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.Assigned_Invitation')!= '') ?  trans(Session::get('mer_lang_file').'.Assigned_Invitation') : trans($MER_OUR_LANGUAGE.'.Assigned_Invitation')); ?> </a> </li>
 
<li><a href="<?php echo e(url('account-details')); ?>" <?php if($current_route=="account-details"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.AccountDetail')!= '') ?  trans(Session::get('mer_lang_file').'.AccountDetail') : trans($MER_OUR_LANGUAGE.'.AccountDetail')); ?> </a> </li>

<li><a href="<?php echo e(url('order-reporting')); ?>" <?php if($current_route=="order-reporting"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.OrderReport')!= '') ?  trans(Session::get('mer_lang_file').'.OrderReport') : trans($MER_OUR_LANGUAGE.'.OrderReport')); ?> </a> </li>

<li><a href="<?php echo e(url('recieve-payment')); ?>" <?php if($current_route=="recieve-payment"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.ReceivedPayements')!= '') ?  trans(Session::get('mer_lang_file').'.ReceivedPayements') : trans($MER_OUR_LANGUAGE.'.ReceivedPayements')); ?> </a> </li>
<li><a href="<?php echo e(url('my-wallet')); ?>" <?php if($current_route=="my-wallet"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MyWallet')!= '') ?  trans(Session::get('mer_lang_file').'.MyWallet') : trans($MER_OUR_LANGUAGE.'.MyWallet')); ?> </a> </li>
 
        </ul>
      </div>
    </nav>

      


  </div>
<span class="back_btn_mobile"><a href="#" onclick="window.history.back();"><?php if($mer_selected_lang_code == 'ar'): ?> عوده <?php else: ?> Back <?php endif; ?></a></span>
</div>
  <div class="mobile_right">
  <span class="language">
        <?php if($mer_selected_lang_code == 'ar'): ?>
           <span class="lang_select" data-val="en"  onclick="Lang_change('en')">English</span> 
     
        <?php else: ?>        
        <span class="lang_select" data-val="ar" onclick="Lang_change('ar')">عربى</span>
        <?php endif; ?> 
      </span>
	  <span class="bar_type">|</span>
	  <div class="profile-mobile">
            <div class="dropbtn"><span class="blacks"><?php echo e((Lang::has(Session::get('mer_lang_file').'.HI')!= '') ?  trans(Session::get('mer_lang_file').'.HI') : trans($MER_OUR_LANGUAGE.'.HI')); ?> <?php echo e(Session::get('merchantname')); ?></span></div>
            <div class="dropdown-content">
              <li><a href="<?php echo e(url('edit_merchant_info/'.$merchantid)); ?>" ><i class="icon-user"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.PROFILE')!= '') ? trans(Session::get('mer_lang_file').'.PROFILE') : trans($MER_OUR_LANGUAGE.'.PROFILE')); ?> </a> </li>
			   <li><a href="<?php echo e(url('change_merchant_password/'.$merchantid)); ?>" ><i class="icon-signout"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.CHANGE_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.CHANGE_PASSWORD')); ?> </a> </li>

<?php
$useragent=$_SERVER['HTTP_USER_AGENT'];

if(!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
{
?>

              
			  <?php } ?>
			  <li><a href="<?php echo e(url('merchant_logout')); ?>" ><i class="icon-signout"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.LOGOUT')!= '') ? trans(Session::get('mer_lang_file').'.LOGOUT') : trans($MER_OUR_LANGUAGE.'.LOGOUT')); ?> </a> </li>
            </div>
          </div>
		
		  </div>
	  
	  </div>
  <div class="clear"></div>
  <div class="header_box">
    <nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 10px;">      
      <!-- LOGO SECTION -->
      <header class="navbar-header"> <a href="<?php echo e(url('vendor-dashboard')); ?>" class="navbar-brand"> <img src="<?php echo e($SITE_LOGO); ?>" alt="<?php echo e((Lang::has(Session::get('mer_lang_file').'.LOGO')!= '') ? trans(Session::get('mer_lang_file').'.LOGO') : trans($MER_OUR_LANGUAGE.'.LOGO')); ?>" /></a> </header>
      <!-- END LOGO SECTION -->       
      <div class="top_lang">
        <div class="nav navbar-top-links navbar-right">
          
          <!-- MESSAGES SECTION --> 
<div class="top-nav-inner">
          <span class="language">
        <?php if($mer_selected_lang_code == 'ar'): ?>
        <span class="lang_select" data-val="en"  onclick="Lang_change('en')">English</span> 
        
        <?php else: ?>        
        <span class="lang_select" data-val="ar" onclick="Lang_change('ar')">عربى</span>
        <?php endif; ?> 
      </span> <span class="bar_type">|</span>
          <div class="profile">
            <div class="dropbtn"><span class="blacks"><?php echo e((Lang::has(Session::get('mer_lang_file').'.HI')!= '') ?  trans(Session::get('mer_lang_file').'.HI') : trans($MER_OUR_LANGUAGE.'.HI')); ?>  <?php echo e(Session::get('merchantname')); ?></span></div>
            <div class="dropdown-content">
              <li><a href="<?php echo e(url('edit_merchant_info/'.$merchantid)); ?>" ><i class="icon-user"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.PROFILE')!= '') ? trans(Session::get('mer_lang_file').'.PROFILE') : trans($MER_OUR_LANGUAGE.'.PROFILE')); ?> </a> </li>
			   <li><a href="<?php echo e(url('change_merchant_password/'.$merchantid)); ?>" ><i class="icon-signout"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.CHANGE_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.CHANGE_PASSWORD')); ?> </a> </li>

         




              <li><a href="<?php echo e(url('merchant_logout')); ?>" ><i class="icon-signout"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.LOGOUT')!= '') ? trans(Session::get('mer_lang_file').'.LOGOUT') : trans($MER_OUR_LANGUAGE.'.LOGOUT')); ?> </a> </li>
            </div>
          </div>
          </div>
          <!-- <li><a href="<?php echo e(url('merchant_settings')); ?>" class="btn btn-default"><i class="icon-gear"></i> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ? trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS')); ?> </a>
                            </li>-->
          
     
          
          <!--END ADMIN SETTINGS -->
        </div>
        <?php if(Session::get('LoginType') !='4'): ?>
         
        <?php endif; ?>
      </div>
    </nav>
    

    
  </div> <!-- header_box -->
  <div class="bs-example desktop">
    <nav class="navbar navbar-default"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <div type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </div>
      </div>
      <!-- Collection of nav links, forms, and other content for toggling -->
      <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="<?php echo e(url('sitemerchant_dashboard')); ?>" <?php if($current_route=="sitemerchant_dashboard"){?> class="active"<?php } ?> ><?php echo e((Lang::has(Session::get('mer_lang_file').'.Services')!= '') ? trans(Session::get('mer_lang_file').'.Services') : trans($MER_OUR_LANGUAGE.'.Services')); ?></a></li>
          <li><a href="<?php echo e(url('edit_merchant_info',['id' =>$merchantid])); ?>" <?php if($menu=="settings"){?> class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ? trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS')); ?></a> </li>

          <?php if(Session::get('LoginType') !='4'): ?>
          <!--li><a href="<?php echo e(url('transations')); ?>" <?php if(Request::is('transations')) {?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.TRANSACTIONS')!= '') ?  trans(Session::get('mer_lang_file').'.TRANSACTIONS') : trans($MER_OUR_LANGUAGE.'.TRANSACTIONS')); ?> </a> </li-->
          <li><a href="<?php echo e(url('fund-request')); ?>"  <?php if(Request::is('fund-request')) {?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.FUND_REQUESTS')!= '') ? trans(Session::get('mer_lang_file').'.FUND_REQUESTS') : trans($MER_OUR_LANGUAGE.'.FUND_REQUESTS')); ?> </a> </li>
         
          <li>
          	<a href="#" <?php if(Request::is('account-details') || Request::is('order-reporting') || Request::is('recieve-payment') || Request::is('my-wallet')) {?>class="active"<?php } ?> > <?php if(Lang::has(Session::get('mer_lang_file').'.Payment')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Payment')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Payment')); ?> <?php endif; ?></a>
            <ul class="submenu">
            	<li><a href="<?php echo e(url('account-details')); ?>" <?php if(Request::is('account-details')) {?>class="active"<?php } ?> ><?php if(Lang::has(Session::get('mer_lang_file').'.AccountDetail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.AccountDetail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.AccountDetail')); ?> <?php endif; ?> </a></li>
                <li><a href="<?php echo e(url('order-reporting')); ?>" <?php if(Request::is('order-reporting')) {?>class="active"<?php } ?> ><?php if(Lang::has(Session::get('mer_lang_file').'.OrderReport')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OrderReport')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OrderReport')); ?> <?php endif; ?> </a></li>
                <li><a href="<?php echo e(url('recieve-payment')); ?>" <?php if(Request::is('recieve-payment')) {?>class="active"<?php } ?> ><?php if(Lang::has(Session::get('mer_lang_file').'.ReceivedPayements')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ReceivedPayements')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ReceivedPayements')); ?> <?php endif; ?>  </a></li>

 

                 <li><a href="<?php echo e(url('my-wallet')); ?>" <?php if(Request::is('my-wallet')) {?>class="active"<?php } ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MyWallet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MyWallet')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MyWallet')); ?> <?php endif; ?>  </a></li>
            </ul>

          </li>
         
          <li><a href="<?php echo e(url('listmanager')); ?>" <?php if($current_route=="listmanager"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGER') : trans($MER_OUR_LANGUAGE.'.MER_MANAGER')); ?> </a> </li>

        <li><a href="<?php echo e(url('manage-shipping')); ?>" <?php if($current_route=="manage-shipping"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MANAGE_SHIPPING')!= '') ?  trans(Session::get('mer_lang_file').'.MANAGE_SHIPPING') : trans($MER_OUR_LANGUAGE.'.MANAGE_SHIPPING')); ?> </a> </li>
		
		 <li><a href="<?php echo e(url('manage_assigned_invitation')); ?>" <?php if($current_route=="manage_assigned_invitation"){?>class="active"<?php } ?>> <?php echo e((Lang::has(Session::get('mer_lang_file').'.Assigned_Invitation')!= '') ?  trans(Session::get('mer_lang_file').'.Assigned_Invitation') : trans($MER_OUR_LANGUAGE.'.Assigned_Invitation')); ?> </a> </li>
		
        <?php endif; ?>  


          
        </ul>
      </div>
    </nav>
  </div>
</div>


<!--Loader & alert-->
<div id="loader" style="position: absolute; display: none;">
  <div class="loader-inner"></div>
  <div class="loader-section"></div>
</div>
<div id="loadalert" class="alert-success" style="margin-top:18px; display: none; position: fixed; z-index: 9999; width: 50%; text-align: center; left: 25%; padding: 10px;"> <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a> <strong>Success!</strong> </div>
<!--Loader & alert--> 
<script type="text/javascript">
	function Lang_change(str) 
	{
    $.ajax
    ({
      type:'GET',
      url:"<?php echo url('merchant_new_change_languages');?>",
      data:{'language_code': str},
      success:function(data)
      {
      window.location.reload();
      }
    }); 
	}
</script> 
 
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Display Message after submition -->
    <!-- Display Message after submition -->
    <form name="form" id="update_my_profile" method="post" action="<?php echo e(route('update-my-profile')); ?>" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

	
    <div class="myaccount_right">
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')); ?></h1>
      <div class="field_group top_spacing_margin_occas myprofile_page">   
	  <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>     
        <div class="checkout-box">
          <div class="checkout-form">
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.NAME')!= '')  ?  trans(Session::get('lang_file').'.NAME'): trans($OUR_LANGUAGE.'.NAME')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="text" maxlength="150" name="name" id="name" value="<?php echo e(isset($customerdetails->cus_name) ? $customerdetails->cus_name : ''); ?>" class="t-box" />
                </div>
                <?php if($errors->has('name')): ?><span class="error"><?php echo e($errors->first('name')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.EMAIL')!= '')  ?  trans(Session::get('lang_file').'.EMAIL'): trans($OUR_LANGUAGE.'.EMAIL')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="email" maxlength="150" name="email" id="email" value="<?php echo e(isset($customerdetails->email) ? $customerdetails->email : ''); ?>" class="t-box" />
                </div>
                <?php if($errors->has('email')): ?><span class="error"><?php echo e($errors->first('email')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
            </div>
			<?php if(isset($customerdetails->cus_phone) && $customerdetails->cus_phone!=''){$rec_array = explode('-',$customerdetails->cus_phone);} ?>
            <!-- checkout-form-row -->
            <div class="checkout-form-row">
              <div class="checkout-form-cell country_row">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.TelephoneNumber')!= '')  ?  trans(Session::get('lang_file').'.TelephoneNumber'): trans($OUR_LANGUAGE.'.TelephoneNumber')); ?></div>
                <div class="checkout-form-bottom">
                  <?php $concode=$customerdetails->country_code; //$concode=ltrim($rec_array[0],'+'); ?>
				  <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode">
                <?php $__currentLoopData = $getCountry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ccode): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($Ccode->country_code); ?>" <?php if($concode==$Ccode->country_code){ ?> selected <?php } ?>>+<?php echo e($Ccode->country_code); ?></option>
                  
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
                  <input type="text" maxlength="13" name="telephone_number" id="telephone_number" onkeypress="return isNumber(event)" value="<?php if(isset($rec_array[1]) && $rec_array[1]!=''){ echo $rec_array[1]; }?>" class="t-box checkout-small-box mobleno"/>
                </div>
				<label for="country_code" class="error"></label>
				<label for="telephone_number" class="error"></label>
                <?php if($errors->has('telephone_number')): ?><span class="error"><?php echo e($errors->first('telephone_number')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')); ?></div>
                <div class="checkout-form-bottom">
                  <div class="gender-radio-box">
                    <input id="male" name="gender" type="radio" value="0" <?php if($customerdetails->gender==0){echo 'Checked';} ?>>
                    <label for="male"><?php echo e((Lang::has(Session::get('lang_file').'.MALE')!= '')  ?  trans(Session::get('lang_file').'.MALE'): trans($OUR_LANGUAGE.'.MALE')); ?></label>
                  </div>
                  <div class="gender-radio-box">
                    <input id="female" name="gender" type="radio" value="1"<?php if($customerdetails->gender==1){echo 'Checked';} ?>>
                    <label for="female"><?php echo e((Lang::has(Session::get('lang_file').'.FEMALE')!= '')  ?  trans(Session::get('lang_file').'.FEMALE'): trans($OUR_LANGUAGE.'.FEMALE')); ?></label>
                  </div>
                </div>
                <?php if($errors->has('gender')): ?><span class="error"><?php echo e($errors->first('gender')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
            </div>
            <!-- checkout-form-row -->
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.DATE_OF_BIRTH')!= '')  ?  trans(Session::get('lang_file').'.DATE_OF_BIRTH'): trans($OUR_LANGUAGE.'.DATE_OF_BIRTH')); ?></div>
                <div class="checkout-form-bottom">
                <input type="text" class="t-box cal-t checkout-small-box" name="dob" id="dob" value="<?php echo e(isset($customerdetails->dob) ? $customerdetails->dob : ''); ?>" autocomplete="off"/>
                </div>
                <?php if($errors->has('dob')): ?><span class="error"><?php echo e($errors->first('dob')); ?></span><?php endif; ?>
              </div>
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?></div>
                <div class="checkout-form-bottom">
                  <select class="checkout-small-box" name="city" id="city">
                    <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></option>
                <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="color: #d2cece;"><?php echo e($cbval->co_name); ?></option>
                         <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php if($customerdetails->cus_city==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} ?>
                        <?php if($selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php echo e($selectedcity); ?> ><?php echo e($val->$ci_name); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>                  
                </div>
                <?php if($errors->has('city')): ?><span class="error"><?php echo e($errors->first('city')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
            </div>
            <!-- checkout-form-row -->            
            <!-- checkout-form-row -->
            <div class="checkout-form-row">
              <div class="checkout-form-cell address-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.ADDRESS')!= '')  ?  trans(Session::get('lang_file').'.ADDRESS'): trans($OUR_LANGUAGE.'.ADDRESS')); ?> </div><div class="findme"  onClick="getmylocation();"> <i class="fa fa-map-marker fa-3" aria-hidden="true"></i> <?php echo e((Lang::has(Session::get('lang_file').'.GET_MY_CURRENT_LOCATION')!= '')  ?  trans(Session::get('lang_file').'.GET_MY_CURRENT_LOCATION'): trans($OUR_LANGUAGE.'.GET_MY_CURRENT_LOCATION')); ?> </div>    
                <div class="checkout-form-bottom">
                  <input type="text" class="t-box" name="address" id="address" value="<?php echo e(isset($customerdetails->cus_address1) ? $customerdetails->cus_address1 : ''); ?>"/>
                  <?php if($errors->has('address')): ?><span class="error"><?php echo e($errors->first('address')); ?></span><?php endif; ?>
                  <input type="text" class="t-box" name="address2" id="address2" value="<?php echo e(isset($customerdetails->cus_address2) ? $customerdetails->cus_address2 : ''); ?>"/>

                </div> 

              </div>
              <!-- checkout-form-cell -->
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.PIN_CODE')!= '')  ?  trans(Session::get('lang_file').'.PIN_CODE'): trans($OUR_LANGUAGE.'.PIN_CODE')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="text" class="t-box checkout-small-box" name="pin_code" onkeypress="return isNumber(event)" id="pin_code" value="<?php echo e(isset($customerdetails->ship_postalcode) ? $customerdetails->ship_postalcode : ''); ?>" maxlength="6" />
                </div>
                <?php if($errors->has('pin_code')): ?><span class="error"><?php echo e($errors->first('pin_code')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
            </div>
            <!-- checkout-form-row -->
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="col2 browse_row profile_page">
                  <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.UPLOAD_YOUR_NATIONAL_ID')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD_YOUR_NATIONAL_ID'): trans($OUR_LANGUAGE.'.UPLOAD_YOUR_NATIONAL_ID')); ?></div>
                  <div class="info-box50 input-file-box">
                    <div class="input-field-name">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div class="file-btn"><?php echo e((Lang::has(Session::get('lang_file').'.UPLOAD')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD'): trans($OUR_LANGUAGE.'.UPLOAD')); ?></div>
                        <div id="file_value1" class="file-value"></div>
                      </div>
                      </label>                      
                    </div>
                    <input id="company_logo" name="customer_pic" class="info-file" type="file" accept="image/gif, image/jpeg, image/png">
                    <input name="oldpic" type="hidden" value="<?php echo e(isset($customerdetails->cus_pic) ? $customerdetails->cus_pic : ''); ?>">
                    <?php if($errors->has('customer_pic')): ?><span class="error"><?php echo e($errors->first('customer_pic')); ?></span><?php endif; ?>
                  </div>   
                </div>
				<div class="browse_img"><?php if(isset($customerdetails->cus_pic) && $customerdetails->cus_pic!=''){ ?> <img src="<?php echo e($customerdetails->cus_pic); ?>"><?php } ?></div>                               
                               
              </div> 
			  
			  <!--<div class="checkout-form-cell">                <div class="col2 browse_row profile_page">
                  <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.ADDRESS_IMAGE')!= '')  ?  trans(Session::get('lang_file').'.ADDRESS_IMAGE'): trans($OUR_LANGUAGE.'.ADDRESS_IMAGE')); ?></div>
                  <div class="info-box50 input-file-box">
                    <div class="input-field-name">
                      <label for="address_image">
                      <div class="file-btn-area">
                        <div class="file-btn"><?php echo e((Lang::has(Session::get('lang_file').'.UPLOAD')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD'): trans($OUR_LANGUAGE.'.UPLOAD')); ?></div>
                        <div id="file_value2" class="file-value"></div>
                      </div>
                      </label>
                    </div>
                    <input id="address_image" name="address_image" class="info-file" type="file" accept="image/gif, image/jpeg, image/png">
                    <input name="oldaddress_image" type="hidden" value="<?php echo e(isset($customerdetails->address_image) ? $customerdetails->address_image : ''); ?>">
                  </div>   
                </div>
				<div class="browse_img"><?php if(isset($customerdetails->address_image) && $customerdetails->address_image!=''){?><img src="<?php echo e($customerdetails->address_image); ?>" width="100" height="100"><?php } ?></div>
      </div>-->


			  </div>
			  
			 <!-- <div class="checkout-form-row">
			  
			  <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.MAP_URL')!= '')  ?  trans(Session::get('lang_file').'.MAP_URL'): trans($OUR_LANGUAGE.'.MAP_URL')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="url" maxlength="150" name="map_url" id="map_url" value="<?php echo e(isset($customerdetails->map_url) ? $customerdetails->map_url : ''); ?>" class="t-box" />
                </div>                
              </div>
			  
			  </div>-->
           
			<div class="checkout-form-row">
			<h3 class="payment_heading"><?php echo e((Lang::has(Session::get('lang_file').'.PAYMENT_INFORMATION')!= '')  ?  trans(Session::get('lang_file').'.PAYMENT_INFORMATION'): trans($OUR_LANGUAGE.'.PAYMENT_INFORMATION')); ?></h3>
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.BANK_NAME')!= '')  ?  trans(Session::get('lang_file').'.BANK_NAME'): trans($OUR_LANGUAGE.'.BANK_NAME')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="text" maxlength="100" name="bank_name" id="bank_name" value="<?php echo e(isset($customerdetails->bank_name) ? $customerdetails->bank_name : ''); ?>" class="t-box" />
                </div>
                <?php if($errors->has('bank_name')): ?><span class="error"><?php echo e($errors->first('bank_name')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.ACCOUNT_NO')!= '')  ?  trans(Session::get('lang_file').'.ACCOUNT_NO'): trans($OUR_LANGUAGE.'.ACCOUNT_NO')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="text" maxlength="20" name="bank_account" id="bank_account" onkeypress="return isNumber(event)" value="<?php echo e(isset($customerdetails->bank_account) ? $customerdetails->bank_account : ''); ?>" class="t-box" />
                </div>
                <?php if($errors->has('bank_account')): ?><span class="error"><?php echo e($errors->first('bank_account')); ?></span><?php endif; ?>
              </div>
              <!-- checkout-form-cell -->
            </div>
            <!-- checkout-form-row -->
            <input name="address_latitude" id="address_latitude" type="hidden" class="form-control" value="<?php echo e(isset($customerdetails->address_latitude) ? $customerdetails->address_latitude : ''); ?>" />
            <input name="address_longitude" id="address_longitude" type="hidden" class="form-control" value="<?php echo e(isset($customerdetails->address_longitude) ? $customerdetails->address_longitude : ''); ?>" />
            <span style="color:red" id="regerror"></span></div>  

          
          <!-- checkout-form -->
          <div class="myprf_btn">
            <input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.SAVE')!= '')  ?  trans(Session::get('lang_file').'.SAVE'): trans($OUR_LANGUAGE.'.SAVE')); ?>" class="form-btn btn-info-wisitech">
          </div>
        </div>      
        <!-- checkout-box -->
      </div>      
      </form>      
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script>
/* Mobile Number Validation */
 function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
 }
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
var jq = $.noConflict();
jq( function() {
  jq( "#dob" ).datepicker({
    dateFormat: "d M yy",
    changeMonth: true,
    changeYear: true,
    yearRange: "-50:-18" // last hundred years
  });
} );
</script> 
<script>
jQuery(document).ready(function(){
jQuery.validator.addMethod("laxEmail", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
}, "Please enter valid email address."); 
 jQuery("#update_my_profile").validate({
    rules: {          
          "name" : {
            required : true,
            maxlength: 100
          },
          "email" : {
            required : true,
            email: true,
            laxEmail:true
          },
		  "country_code" : {
            required : true
          },
          "telephone_number" : {
            required : true,
            digits: true, 
            minlength: 5,
            maxlength: 13
          },  
          "gender" : {
            required : true
          },
          "dob" : {
            required : true
          },          
          "city" : {
            required : true
          },           
          "pin_code" : {
            required : true,
            digits: true,
            maxlength: 6
          },
          "address" : {
            required : true
          }, 

<?php if(isset($customerdetails->cus_pic) && $customerdetails->cus_pic==''): ?>
          "customer_pic" : {
            required:true 
          },  
 <?php endif; ?>


		  "bank_name" : {
            required:true 
          }, 
		  "bank_account" : {
            required:true 
          },        
         },
         messages: {
          "name": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NAME')); ?> <?php endif; ?>",
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_NAME_HERE_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_NAME_HERE_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NAME_HERE_MAX')); ?> <?php endif; ?>'
          },
          "email": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php endif; ?>',
            email: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE_VALID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE_VALID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_HERE_VALID')); ?> <?php endif; ?>'
          },
		  "country_code": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.COUNTRY_CODE_MSG')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY_CODE_MSG')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COUNTRY_CODE_MSG')); ?> <?php endif; ?>'
          },
          "telephone_number": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_TEEPHONE_NUMBER_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_TEEPHONE_NUMBER_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_TEEPHONE_NUMBER_HERE')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>', 
            minlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MIN')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NUMBER_HERE_MIN')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NUMBER_HERE_MAX')); ?> <?php endif; ?>'
          },
          "gender": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_GENDER_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_GENDER_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_GENDER_HERE')); ?> <?php endif; ?>'
          },
          "dob": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_DOB_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_DOB_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_DOB_HERE')); ?> <?php endif; ?>'
          },
          "city": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_CITY_HERE')); ?> <?php endif; ?>'
          },
          "pin_code": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_PIN_CODE_HERE')); ?> <?php endif; ?>',
            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',
            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_PIN_CODE_HERE_MAX')); ?> <?php endif; ?>'
          },
          "address": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ADDRESS')); ?> <?php endif; ?>'
          },
          "customer_pic": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_UPLOAD_IMAGE')); ?> <?php endif; ?>'
          },
		  "bank_name": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_BANK_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_BANK_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_BANK_NAME')); ?> <?php endif; ?>'
          },
		  "bank_account": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ACCOUNT_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ACCOUNT_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ACCOUNT_NO')); ?> <?php endif; ?>'
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#update_my_profile").valid()) {        
    jQuery('#update_my_profile').submit();
   }
  });
});

jQuery('body').on('change','#company_logo', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value1").html(fake);
});

jQuery('body').on('change','#address_image', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value2").html(fake);
});

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1FYlmvxqIC6a2MxCylBYAehBV75ERKyA&v=3.exp"></script>

<script type="text/javascript"> 
function getmylocation(){

  var geocoder = new google.maps.Geocoder();

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
} 
//Get the latitude and the longitude;
function successFunction(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    codeLatLng(lat, lng)
}

function errorFunction(){
    alert("Geocoder failed");
}

  function initialize() {
    geocoder = new google.maps.Geocoder();



  }

  function codeLatLng(lat, lng) {

    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      console.log(results)
        if (results[1]) {
         //formatted address
    // print_r(results[0]);
    //alert(results[0].join('\n'));
        // alert(results[0].formatted_address)
        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
          if (results[0].address_components[i].types[b] == "postal_code") {
                    //this is the object you are looking for
                    postal_code= results[0].address_components[i];
                    break;
                }
            }
        }
        //city data
        //alert(city.short_name + " " + city.long_name);
    //alert(postal_code.short_name + " " + postal_code.long_name);
    document.getElementById("address_latitude").value=lat;
    document.getElementById("address_longitude").value=lng;

     document.getElementById("pin_code").value=postal_code.short_name;
    document.getElementById("address").value=results[0].formatted_address;
      } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });
  } }
</script> 

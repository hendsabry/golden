<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 
<title><?php if(Lang::has(Session::get('lang_file').'.Golden_Cages')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Golden_Cages')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Golden_Cages')); ?> <?php endif; ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(url('/')); ?>/themes/images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="<?php echo e(url('/')); ?>/themes/css/reset.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/css/common-style.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/css/stylesheet.css" rel="stylesheet" />


<link href="<?php echo e(url('/')); ?>/themes/slider/css/demo.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/slider/css/flexslider.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/css/user-my-account.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/css/interface.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/css/arabic.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/css/interface-media.css" rel="stylesheet" />
<link href="<?php echo e(url('/')); ?>/themes/css/diamond.css" rel="stylesheet" />
<!-- custome scroll 
<link href="mousewheel/jquery.mCustomScrollbar.css" rel="stylesheet" />
-->
<!--<link href="css/diamond.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />-->

<script src="<?php echo e(url('/')); ?>/themes/js/jquery-lib.js"></script>
<script src="<?php echo e(url('/')); ?>/themes/js/height.js"></script>
<script src="<?php echo e(url('/')); ?>/themes/js/jquery.flexslider.js"></script>
<script src="<?php echo e(url('/')); ?>/themes/js/modernizr.js"></script>
<script src="<?php echo e(url('/')); ?>/themes/js/jquery.mousewheel.js"></script>
<script src="<?php echo e(url('/')); ?>/themes/js/demo.js"></script>
<script src="<?php echo e(url('/')); ?>/themes/js/froogaloop.js"></script>
<script src="<?php echo e(url('/')); ?>/themes/js/jquery.easing.js"></script>


</head>
<?php  if(Session::get('lang_file')=='ar_lang'){ $sitecss="arabic"; }else{ $sitecss=""; } ?>
<body class="<?php echo e($sitecss); ?>">
<?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
  $cururl = request()->segment(count(request()));
?>

<div class="outer_wrapper">
<div class="vendor_header">
	<div class="inner_wrap">                
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($ShopInfo[0]->mc_img); ?>" alt="" /></a></div>                      	
        </div>
		<div class="vendor_header_right"> 
		<!-- vendor_header_left -->
         <div class="vendor_welc">

      <?php echo $__env->make("includes.language-changer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="vendor_name username">  <?php echo e((Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')); ?> <span>
      <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
			
			<ul class="vendor_header_navbar">
				<li><a href="<?php echo e(route('my-account-profile')); ?>"<?php if($cururl=='my-account-profile') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')); ?></a></li>
        <li><a href="<?php echo e(route('my-account-ocassion')); ?>"<?php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')); ?></a></li>
        <li><a href="<?php echo e(route('my-account-studio')); ?>"<?php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')); ?></a></li>
        <li><a href="<?php echo e(route('my-account-security')); ?>"<?php if($cururl=='my-account-security') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')); ?></a></li>
        <li><a href="<?php echo e(route('my-account-wallet')); ?>"<?php if($cururl=='my-account-wallet') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')); ?></a></li>
        <li><a href="<?php echo e(route('my-account-review')); ?>"<?php if($cururl=='my-account-review') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')); ?></a></li>
        <li><a href="<?php echo e(route('my-request-a-quote')); ?>"<?php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')); ?></a></li>
        <li><a href="<?php echo e(route('change-password')); ?>"<?php if($cururl=='change-password') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')); ?></a></li>
				<li><a href="<?php echo e(url('login-signup/logoutuseraccount')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')); ?></a></li>
			</ul>
      </span></div>
	  <?php if(Session::get('customerdata.token')!=''): ?>
			<?php $getcartnoitems = Helper::getNumberOfcart(); ?>
			<?php if($getcartnoitems>0): ?>
    <a href="<?php echo e(url('mycart')); ?>" class="vendor_cart"><img src="<?php echo e(url('')); ?>/themes/images/basket.png" /><span><?php echo e($getcartnoitems); ?></span></a>
	<?php endif; ?>
			<?php endif; ?>
	
	 </div>
        <!-- vendor_header_right -->
		  <?php if(isset($otherhalllist) && count($otherhalllist)){  ?>
            <div class="select_catg">

                    <div class="select_lbl"><?php if(Lang::has(Session::get('lang_file').'.OTHER_HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OTHER_HALL')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.OTHER_HALL')); ?> <?php endif; ?></div>

                <div class="search-box-field">
                  <select class="select_drp" id="dynamic_select">
                      <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_HALL')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_HALL')); ?> <?php endif; ?></option>

                         <?php $__currentLoopData = $otherhalllist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $otherhalls): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($category_id); ?>/<?php echo e($otherhalls->pro_mc_id); ?>/<?php echo e($otherhalls->pro_id); ?>"> <?php echo e(isset($otherhalls->pro_title) ? $otherhalls->pro_title : ''); ?>  </option>

                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>

		   <?php } else { if(count($otherbarnch)>1){ ?>
            <div class="select_catg">
            	<div class="select_lbl"><?php if(Lang::has(Session::get('lang_file').'.Other_Branches')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Other_Branches')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Other_Branches')); ?> <?php endif; ?></div>

                <div class="search-box-field">
                	<select class="select_drp" id="dynamic_select">
                    	<option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Branch')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Branch')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Branch')); ?> <?php endif; ?></option>

                         <?php $__currentLoopData = $otherbarnch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $otherbarnches): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($category_id); ?>"> <?php echo e($otherbarnches->mc_name); ?>  </option>

                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>
           <?php } } ?>
		 </div>




	</div>
</div><!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">
      <ul>
	  <?php if($ShophallbasicInfo[0]->pro_desc!=''){ ?>
        <li><a href="#about_shop" class="active"><?php if(Lang::has(Session::get('lang_file').'.About_Shop')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.About_Shop')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.About_Shop')); ?> <?php endif; ?></a></li>
		<?php } ?>
    <?php if($ShophallbasicInfo[0]->video_url!=''){ ?>
        <li><a href="#video"><?php if(Lang::has(Session::get('lang_file').'.Video')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Video')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Video')); ?> <?php endif; ?></a></li>
		<?php } ?>
		
		<?php if(count($serviceoptionvalue[0])>0){ ?>
        <li><a href="#prepaid"><?php if(Lang::has(Session::get('lang_file').'.Services')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Services')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Services')); ?> <?php endif; ?></a></li>
		<?php } ?>
		
       <!-- <li><a href="#choose_package"><?php if(Lang::has(Session::get('lang_file').'.Choose_Package')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Choose_Package')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Choose_Package')); ?> <?php endif; ?></a></li>-->
	   <?php  if($ShophallbasicInfo[0]->food_type!=''){ ?>
	    <li><a href="#food"><?php if(Lang::has(Session::get('lang_file').'.Food')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Food')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Food')); ?> <?php endif; ?></a></li>
		<?php } ?>
    <?php if(count($customerReviewOnProduct)>0){ ?>
        <li><a href="#our_client"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></a></li>
    <?php } ?>
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->

 



<div class="inner_wrap">
<?php if(Session::has('message')): ?>
<div class="alert error"><?php echo e(Session::get('message')); ?></div>
<?php endif; ?>
<div class="detail_page hall-extra-class">	
    <a name="about_shop"></a>	
    <div class="service_detail_row">
    	
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
                <?php $__currentLoopData = $shopgalleryimage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productgalleryimages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li>
  	    	    <img src="<?php echo e(str_replace('thumb_','',$productgalleryimages->image)); ?>" />
  	    		</li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  	    		       	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
              <?php $__currentLoopData = $shopgalleryimage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productgallerythumbnailimages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<li>
  	    	    <img src="<?php echo e($productgallerythumbnailimages->image); ?>" />
  	    		</li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
  	    	
           
          </ul>
        </div>
      		</section>
            
        </div>
        <a name="about_shop"></a>
        <div class="service_detail">
			<?php   if( $ShophallbasicInfo[0]->pro_discount_percentage >0 ){ 
      $productrealprice= $ShophallbasicInfo[0]->pro_disprice; 
    } else{
     $productrealprice= $ShophallbasicInfo[0]->pro_price; } ?>

      <?php if( $ShophallbasicInfo[0]->Insuranceamount !='' ){ $productInsuranceamount= $ShophallbasicInfo[0]->Insuranceamount; } else{ $productInsuranceamount= ''; } ?>
			
        	<div class="detail_title"><?php echo e($ShophallbasicInfo[0]->pro_title); ?></div>
            <div class="detail_hall_description"><?php echo e($ShophbasicInfo[0]->address); ?></div>
             <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?><span>
            <?php
        $getcityname = Helper::getcity($ShophbasicInfo[0]->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
            </span></div>
            <div class="detail_hall_subtitle"><?php if(Lang::has(Session::get('lang_file').'.About_Hall')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.About_Hall')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.About_Hall')); ?> <?php endif; ?> </div>
            
            <div class="detail_about_hall">
				
              <div class="comment more"><?php echo e($ShophallbasicInfo[0]->about); ?> </div></div>
             

            

            <div class="hall-det-line"><span><?php if(Lang::has(Session::get('lang_file').'.Hall_Dimension')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Hall_Dimension')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Hall_Dimension')); ?><?php endif; ?>:</span> 
                <?php if(isset($ShophallbasicInfo[0]->hall_dimension) && $ShophallbasicInfo[0]->hall_dimension!=''){
                $d=explode('X',$ShophallbasicInfo[0]->hall_dimension); ?><br>
                  <?php if(Lang::has(Session::get('lang_file').'.LENGTH')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.LENGTH')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.LENGTH')); ?><?php endif; ?>- <?php echo e($d[0]); ?> <?php if(Lang::has(Session::get('lang_file').'.Meter')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Meter')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Meter')); ?><?php endif; ?>,
                   <?php if(Lang::has(Session::get('lang_file').'.Width')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Width')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Width')); ?> <?php endif; ?><?php echo e($d[1]); ?> <?php if(Lang::has(Session::get('lang_file').'.Meter')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Meter')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Meter')); ?><?php endif; ?>,
                    <?php if(Lang::has(Session::get('lang_file').'.Area')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Area')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Area')); ?> <?php endif; ?><?php echo e($d[2]); ?> <?php if(Lang::has(Session::get('lang_file').'.sqmeter')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.sqmeter')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.sqmeter')); ?> <?php endif; ?>
              <?php }?>
               </div>


  <div class="hall-det-line"><span><?php if(Lang::has(Session::get('lang_file').'.Hall_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Hall_Type')); ?>: <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Hall_Type')); ?>: <?php endif; ?> </span>
    <?php if($ShophallbasicInfo[0]->hall_type=='both'){     
      if (Lang::has(Session::get('lang_file').'.Men_and_Women')!= ''){ 
      $ht=trans(Session::get('lang_file').'.Men_and_Women'); 
    }else{
      $ht=trans($OUR_LANGUAGE.'.Men_and_Women');
    }
  }else{
      $ht=ucwords($ShophallbasicInfo[0]->hall_type);
} ?>
  <?php echo e($ht); ?>

 </div>

             <div class="hall-det-line hall-det-line-last"><span><?php if(Lang::has(Session::get('lang_file').'.Hall_CAPICITY')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Hall_CAPICITY')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Hall_CAPICITY')); ?><?php endif; ?>: </span> <?php if(Lang::has(Session::get('lang_file').'.FOR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.FOR')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.FOR')); ?> <?php endif; ?> <?php echo e($ShophallbasicInfo[0]->hallcapicity); ?> <?php if(Lang::has(Session::get('lang_file').'.PEOPLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PEOPLE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PEOPLE')); ?> <?php endif; ?></div>
            <div class="detail_hall_price">
                     
      <?php if( $ShophallbasicInfo[0]->pro_discount_percentage >0){ ?>   
<strike>  
 <?php echo e(currency($ShophallbasicInfo[0]->pro_price, 'SAR',$Current_Currency)); ?>

 
    </strike> 
        
 <?php echo e(currency($ShophallbasicInfo[0]->pro_disprice, 'SAR',$Current_Currency)); ?>

    

        <?php }else{ ?> 
     
 <?php echo e(currency($ShophallbasicInfo[0]->pro_price, 'SAR',$Current_Currency)); ?>


      <?php } ?> 


    </div>

 
  <?php if($ShophbasicInfo[0]->latitude!='' && $ShophbasicInfo[0]->longitude!=''){ ?>
   <?php 
     $lat=$ShophbasicInfo[0]->latitude;  
      $long=$ShophbasicInfo[0]->longitude;  ?> 

          <div class="hall_map" id="map" width="450" height="230" style="height: 230px!important;"></div>
          <?php }  ?>

    
			 
			<?php if(count($shopfacilities)>0){
      //echo "<pre>";
      //print_r($shopfacilities);
           ?>
            <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.Facilities_Offered_Here')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Facilities_Offered_Here')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Facilities_Offered_Here')); ?> <?php endif; ?> </div>
            <ul class="detail_facilites_list">
				
				 <?php $__currentLoopData = $shopfacilities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shophallfacilities): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            	<li>
              <?php echo e(isset($shophallfacilities->option_title) ? $shophallfacilities->option_title : ''); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
            </ul>
			<?php } ?>

       
        </div> 
        
    </div> <!-- service_detail_row -->
        
    <div class="service_container">
        <div class="services_detail">
		<span id="serviceerror" class="error" style="color:#FF0000"></span>
		<a name="prepaid"></a>
    <?php $jk=0; ?>
			 <?php $__currentLoopData = $shopproductoption; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopmainoptions): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			 	 <?php if(count($serviceoptionvalue[$jk])>0){ ?>
					<div class="service_list_row">
            	
                <div class="common_title"><?php echo e($shopmainoptions->option_title); ?></div>
                
                
                 
                    <ul class="service_catagory">
            <?php $__currentLoopData = $serviceoptionvalue[$jk]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopsuboptions): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $cprice=number_format($shopsuboptions->price,2,'.','') ?>


            
                  <li><input name="service[]" type="checkbox" id="service<?php echo e($shopsuboptions->id); ?>" value="yes" onClick="return addservices(this.id,this.value,'<?php echo e($shopmainoptions->id); ?>','<?php echo e($shopsuboptions->id); ?>','<?php echo e($shopmainoptions->option_title); ?>','<?php echo e($shopsuboptions->option_title); ?>','<?php echo e(currency($cprice, 'SAR',$Current_Currency, $format = false)); ?>');"><label for="service<?php echo e($shopsuboptions->id); ?>"><?php echo e($shopsuboptions->option_title); ?><span class="service_price"> <?php echo e(currency($shopsuboptions->price, 'SAR',$Current_Currency)); ?>  </span></label></li>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
                </ul>





            </div>
			 
			  <?php $jk++; } ?>
			 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        	<?php if($ShophallbasicInfo[0]->food_type!=''): ?>
        
		 <div class="service_list_row">
            	<a name="food"></a>
                <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.Food')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Food')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Food')); ?> <?php endif; ?></div>
                <ul class="service_catagory">
					<?php if($ShophallbasicInfo[0]->food_type=='external' || $ShophallbasicInfo[0]->food_type=='both'): ?>
                	<li><input name="foodtype" type="radio" id="foodservice025" value="external" onClick="return foodredirect(this.value);"><label for="foodservice025"><?php if(Lang::has(Session::get('lang_file').'.External_Food')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.External_Food')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.External_Food')); ?> <?php endif; ?></label></li>
					<?php endif; ?>
          <?php if($isinternalfood>0): ?>
					<?php if($ShophallbasicInfo[0]->food_type=='internal' || $ShophallbasicInfo[0]->food_type=='both'): ?>
                    <li><input name="foodtype" type="radio" id="foodservice026" value="internal" onClick="return foodredirect(this.value);"><label for="foodservice026"><?php if(Lang::has(Session::get('lang_file').'.Internal_Food')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Internal_Food')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Internal_Food')); ?> <?php endif; ?></label></li>
					<?php endif; ?>
					<?php endif; ?>
                </ul>
            </div>
		<?php endif; ?>
		
            <?php if(count($customerReviewOnProduct)>0){ ?>
            <div class="service_list_row service_testimonial">
            	<a name="our_client"></a>
                <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></div>
                <div class="testimonial_slider">
                	<section class="slider">
                        <div class="flexslider1">
                          <ul class="slides">
						  <?php $__currentLoopData = $customerReviewOnProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="<?php echo e($customerreview->cus_pic); ?>"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description"><?php echo e($customerreview->comments); ?></div>
                                    	<div class="testim_name"><?php echo e($customerreview->cus_name); ?></div>
                                        <div class="testim_star"><img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png"></div>
                                    </div>
                                </div>
                            </li>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           
                          </ul>
                        </div>
                      </section>
                </div>
            </div>
            <?php } ?>

<?php if($ShophallbasicInfo[0]->video_url!=''){ ?>
            <div class="service_list_row service_testimonial video_hall">
                <a name="video" class="linking">&nbsp;</a>
        <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.Video')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Video')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Video')); ?> <?php endif; ?></div>
        <div class="service-video-area">
         <div class="service-video-cont"><?php echo e($ShophallbasicInfo[0]->mc_video_description); ?></div>
          <div class="service-video-box">
      
      
            <iframe class="service-video" src="<?php echo e($ShophallbasicInfo[0]->video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>

</div>

<?php } ?>

        </div>
        
        <div class="services_listing">
        	<ul id="myContainer">
            	<li>
                	<span class="service_name"><?php echo e($ShophallbasicInfo[0]->pro_title); ?></span>
                    <span class="service_price"> <?php echo e(currency($productrealprice, 'SAR',$Current_Currency)); ?> </span>
                </li>
                <?php if($productInsuranceamount!=''): ?>
             	<li>
                  <span class="service_name"><?php if(Lang::has(Session::get('lang_file').'.Insurance_amount')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Insurance_amount')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Insurance_amount')); ?> <?php endif; ?></span>
                    <span class="service_price"><?php echo e(currency($productInsuranceamount, 'SAR',$Current_Currency)); ?></span>
                </li>
                <?php endif; ?>
            </ul>
            <?php if(isset($productInsuranceamount) && $productInsuranceamount!='') {$finalprice=$productrealprice+$productInsuranceamount; } else { $finalprice=$productrealprice; } ?>
			<ul class="total_price_ul">
            	<li class="total_cost">
                	<span class="service_name"><?php if(Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TOTAL_PRICE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?> <?php endif; ?></span>
                    <span class="service_price">
                         <?php $fprice=number_format($finalprice,2,'.','') ?>
                     <?php echo e(Session::get('currency')); ?> <span id="fulltotal"><?php echo e(currency($fprice, 'SAR',$Current_Currency, $format = false)); ?> </span></span>
                </li>
            </ul>
			  <?php echo Form::open(['url' => 'additemstocart', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

			<div class="reserve_btn_row" id="rnow" style="display: block;">
          
        
      <input type="hidden" name="baseprice" id="baseprice" value="<?php echo e(currency($productrealprice, 'SAR',$Current_Currency, $format = false)); ?>">
      <input type="hidden" name="total" id="total" value="<?php echo e(currency($finalprice, 'SAR',$Current_Currency, $format = false)); ?>">
      <input type="hidden" name="productInsuranceamount" id="productInsuranceamount" value="<?php echo e(currency($productInsuranceamount, 'SAR',$Current_Currency, $format = false)); ?>">
      <input type="hidden" name="cart_type" id="cart_type" value="hall">
 
      <input type="hidden" name="shop_id" id="shop_id" value="<?php echo e(request()->sid); ?>">
      <input type="hidden" name="branch_id" id="branch_id" value="<?php echo e(request()->bid); ?>">
      <input type="hidden" name="product_id" id="product_id" value="<?php echo e(request()->haid); ?>">
 <?php if($isbooked<1): ?>

      <input type="hidden" name="food_type" id="food_type" value="internalfood">
            <input type="submit" name="submit" value="<?php if(Lang::has(Session::get('lang_file').'.Reserve_Now')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Reserve_Now')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Reserve_Now')); ?><?php endif; ?>" class="reserve_now">
   <?php else: ?>
    
    <div class="reserve_btn_row" id="aleadybookednowcart">
        <span class="reserve_now"><?php if(Lang::has(Session::get('lang_file').'.Already_Booked_at')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Already_Booked_at')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Already_Booked_at')); ?><?php endif; ?></span>
    </div>

   <?php endif; ?>

        </div>
   <?php echo Form::close(); ?>



        <div class="reserve_btn_row" id="foodnow" style="display: none;">
       <?php if($isbooked<1): ?>   

        <a href="<?php echo e(url('foodmenuitem/')); ?>/internal" class="reserve_now foodnow"><?php if(Lang::has(Session::get('lang_file').'.CONTINUE')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.CONTINUE')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.CONTINUE')); ?><?php endif; ?> </a>        



        
          <?php else: ?>
    
   
        <span class="reserve_now"><?php if(Lang::has(Session::get('lang_file').'.Already_Booked_at')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Already_Booked_at')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Already_Booked_at')); ?><?php endif; ?></span>
   

        <?php endif; ?>
        </div>
<div class="hall_terms_conditions"><a  href="<?php echo e(isset($ShophbasicInfo[0]->terms_conditions) ? $ShophbasicInfo[0]->terms_conditions : ''); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')); ?></a></div>


        </div>
        
        
    </div> <!-- service_container -->

	 <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div> <!-- innher_wrap -->

   
    
</div> <!-- outer_wrapper -->

<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $lang = Session::get('lang_file'); ?>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"><?php echo e(Session::get('status')); ?><span id="showmsg"><?php echo e((Lang::has(Session::get('lang_file').'.Hall_external_content')!= '')  ?  trans(Session::get('lang_file').'.Hall_external_content'): trans($OUR_LANGUAGE.'.Hall_external_content')); ?></span></div>
    <div class="action_btnrow" align="center"> 
    
    <span id="showmsgab"><a class="action_yes status_yes" href="javascript:void(0);"> <?php if(Lang::has($lang.'.OK')!= ''): ?> <?php echo e(trans($lang.'.OK')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.OK')); ?> <?php endif; ?> </a></span>
    </div>
  </div>
</div>
       
 
<script type="text/javascript">
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
 jQuery('html, body').animate({
  scrollTop: (jQuery('.service-display-left').first().offset().top)
 },500);
});
</script>



<script language="javascript">
function foodredirect(val){
  if(val=='external')
  {

jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
 
  }
  if (!jQuery("#foodservice026").is(":checked")) {
     document.getElementById("rnow").style.display = "";
     document.getElementById("foodnow").style.display = "none";  
     
  }else{
       document.getElementById("rnow").style.display = "none";
     document.getElementById("foodnow").style.display = "";
     
  }

  if (!jQuery("#foodservice025").is(":checked")) {
     document.getElementById("food_type").value = 'internalfood';
    
  }else{
       document.getElementById("food_type").value = 'extrnalfood';
       document.getElementById("rnow").style.display = "";
     document.getElementById("foodnow").style.display = "none";
  }

  

}
</script>





<script language="javascript">
  jQuery('input:checkbox').removeAttr('checked');
function validate_form()
{
valid = true;

if(jQuery('input[type=checkbox]:checked').length == 0)
{
    //alert ( "ERROR! Please select at least one service" );
//	document.getElementById("serviceerror").innerHTML='ERROR! Please select at least one service';
   // valid = false;
}

return valid;
}

</script>


<script language="javascript">

 function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }



function addservices(idval,val,mainserviceid,subserviceid,mainservicename,subservicename,subserviceprice){
//alert(val);
document.getElementById("serviceerror").innerHTML='';
var checkbox = jQuery( "#"+idval );
checkbox.val( checkbox[0].checked ? "no" : "yes" );
var cid=idval+''+subserviceid;
//var baseprice=document.getElementById('baseprice').value;
var baseprice=document.getElementById('total').value;

var productInsuranceamount=document.getElementById('productInsuranceamount').value;
<?php $Cur = Session::get('currency'); ?>
var formatedsubserviceprice=parseFloat(subserviceprice).toFixed(2);
	if(val=='yes'){
		var newprice=+baseprice + +subserviceprice;
		   var div = document.createElement('div');
    div.className = 'row';
div.innerHTML ='<li id='+cid+'><span class="service_name">'+subservicename+'</span><span class="service_price"><?php echo $Cur; ?> '+formatedsubserviceprice+'</span></li>';
   document.getElementById('myContainer').appendChild(div);
   
    jQuery.ajax({
         url: "<?php echo e(url('branchhalldetail/setremoveservicessession')); ?>",
         data: { 'subserviceid': subserviceid,'isadd':'yes' }
    }); 
   
   
   }else{
    var toprice=document.getElementById('total').value+ +productInsuranceamount;
   
   	var newprice=+toprice - +subserviceprice;
     document.getElementById(cid).remove();
	  jQuery.ajax({
         url: "<?php echo e(url('branchhalldetail/setremoveservicessession')); ?>",
         data: { 'subserviceid': subserviceid,'isadd':'no' }
    }); 
   }
   document.getElementById('total').value=newprice.toFixed(2);
		document.getElementById("fulltotal").innerHTML=newprice.toFixed(2);
}

function removeRow(input) {
    document.getElementById('myContainer').removeChild(input.parentNode);
}
</script>




<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function() {
  var showChar = 200;
  var ellipsestext = "...";
var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });
});
</script>

<script>
    jQuery(function(){
      // bind change event to select
      jQuery('#dynamic_select').on('change', function () {
          var url = jQuery(this).val(); // get selected value          
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="myaccount_right">
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')); ?></h1>
      <div class="field_group top_spacing_margin_occas">
	  <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>
        <div class="main_user"> 
          <?php 
		  if(count($setRewiev) > 0)
		  {
            foreach($setRewiev as $val){
		  ?>
          <div class="types_ocss">
            <div class="types_ocs_left">
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')); ?>: </div>
                <div class="wed_text"> 
					<?php 
					if(isset($val->search_occasion_id) && $val->search_occasion_id!='0')
					{
						$setTitle = Helper::getOccasionName($val->search_occasion_id);
						if(Session::get('lang_file')!='en_lang')
						{
							$mc_name= 'title_ar';
							$setTitle->$mc_name;
						}
						else
						{
							$mc_name='title';
						}
						echo $setTitle->$mc_name;
					}
					else
					{
					  if(isset($val->main_occasion_id) && $val->main_occasion_id!='0')
					  { 
						 if(Session::get('lang_file')!='en_lang')
						 {
						   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'الزفاف والمناسبات');
						 }
						 else
						 {
							$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
						 } 
						 foreach($getArrayOfOcc as $key=>$ocval)
						 {
						  if($val->main_occasion_id==$key)
						  {
						   $occasion_name = $ocval;
						  }
						 }
						 echo $occasion_name;					
					 }
				   }
                  ?> 
				  </div>
              </div>
            </div>
            <div class="types_ocs_right">
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.OCCASION_DATE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_DATE'): trans($OUR_LANGUAGE.'.OCCASION_DATE')); ?>:</div>
                <div class="wed_text"><?php if(isset($val->occasion_date) && $val->occasion_date!=''){ echo date('d M Y',strtotime($val->occasion_date)); } else{ echo 'N/A'; } ?></div>
              </div>
            </div>
          </div>
          <div class="list_of_service">
            <div class="main_box_table_area">
              <div class="myaccount-table mts">
                <div class="mytr">
                  <div class="mytable_heading grey"> <?php echo e((Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')); ?> </div>
				 <div class="mytable_heading grey"> <?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> </div>
                  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?></div>
                  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')); ?></div>
                  <div class="mytable_heading grey"><?php echo e((Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')); ?></div>
                  <div class="mytable_heading order_id grey">&nbsp;</div>
                </div>
				<?php 
				$getAllOrder = Helper::getAllOrderWithService($val->order_id); 
				foreach($getAllOrder as $orderval)
				{ 
								  
				?>
                <div class="mytr mybacg">
                  <div class="mytd mies " data-title="Services"> <?php echo e($val->order_id); ?></div>
				  <div class="mytd mies " data-title="Services">

            <?php echo e(ucwords($orderval->product_sub_type)); ?> 
				  <?php 
            if($orderval->product_sub_type=='band' || $orderval->product_sub_type=='singer'){
              if(isset($orderval->product_id) && $orderval->product_id!='')
            {
              //echo $orderval->order_id;
             // $singerinfo = Helper::getMusicInfo($orderval->product_id);
             //$catshop=$singerinfo->name;
            }

          }else{
					if(isset($orderval->category_id) && $orderval->category_id!='')
					{
                       $dget_category_name = Helper::getshopname($orderval->category_id);
					   $mc_name = 'mc_name';
                       if(Session::get('lang_file')!='en_lang')
					   {
                          $mc_name= 'mc_name_ar'; 
					   }
					   //$catshop=$dget_category_name->$mc_name;
            if(isset($dget_category_name->$mc_name) && $dget_category_name->$mc_name !='')
            {
            $catshop=$dget_category_name->$mc_name;
            }
            else
            {
            $catshop='N/A';
            }
         

					}

          }
             ?>
				  </div>
				  <div class="mytd mies " data-title="Service Provider">
				  <?php 

          if(isset($catshop) && $catshop!=''){
          echo $catshop.' / ';
        }


				    if(isset($orderval->merchant_id) && $orderval->merchant_id!='0')
					{
						$get_vendor_name = Helper::getServiceProviderName($orderval->merchant_id);
						echo $get_vendor_name;
                    }
					?>
				  </div>
                  <div class="mytd mies " data-title="Order Date">
				    <?php if(isset($orderval->created_at) && $orderval->created_at!=''): ?>
                     <?php echo e(Carbon\Carbon::parse($orderval->created_at)->format('d M Y')); ?>

                     <?php else: ?>
                     <?php echo $na='N/A'; ?> 
                    <?php endif; ?>
				  </div>
                   <?php if(isset($orderval->status) && $orderval->status!=''): ?>
                   <?php if($orderval->status==1): ?>
                  <div class="mytd mies" data-title="Status"><?php if(Lang::has(Session::get('lang_file').'.Pending')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Pending')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Pending')); ?> <?php endif; ?></div>
                  <?php elseif($orderval->status==2): ?>
                  <div class="mytd mies" data-title="Status"><?php if(Lang::has(Session::get('lang_file').'.Delivered')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Delivered')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Delivered')); ?> <?php endif; ?></div>
                   <?php elseif($orderval->status==3): ?>
                  <div class="mytd mies" data-title="Status"><?php if(Lang::has(Session::get('lang_file').'.Hold')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Hold')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Hold')); ?> <?php endif; ?></div>
                   <?php elseif($orderval->status==4): ?>
                  <div class="mytd mies" data-title="Status"><?php if(Lang::has(Session::get('lang_file').'.Failed')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Failed')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Failed')); ?> <?php endif; ?></div>
                  <?php endif; ?>
                  <?php endif; ?>
				  <?php if(isset($orderval->status) && $orderval->status!=''): ?>
                   <?php if($orderval->status==1): ?>
                   <div class="mytd mies order_id review_popup_link"><?php echo e((Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')); ?></div>
                   <?php elseif($orderval->status==2): ?>
                   <div class="mytd mies order_id review_popup_link"><a href="<?php echo e(route('reviewdisplay',[$orderval->shop_id,$orderval->order_id,$orderval->merchant_id])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')); ?></a></div>
                   <?php elseif($orderval->status==3): ?>
                   <div rel="" class="mytd mies order_id review_popup_link"><a href="javascript:void(0)"><?php echo e((Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')); ?></a></div>
                   <?php elseif($orderval->status==4): ?>
                  <div rel="" class="mytd mies order_id review_popup_link"><a href="javascript:void(0)"><?php echo e((Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')); ?></a></div>
                  <?php endif; ?>
                  <?php endif; ?>
                </div>
				<?php } ?>
                <!-- review_popup -->
              </div>
            </div>
          </div>
          <?php } } else{ ?>
          <div class="no-record-area" align="center"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div>
          <?php } ?>
          </div>
      </div>
	  <div align="left"><?php echo e($setRewiev->links()); ?><div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
</div>
</div>
</div></div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

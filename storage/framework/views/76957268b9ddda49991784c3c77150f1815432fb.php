<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="inner_wrap"> 
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
      <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper main_category">
      <?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="page-right-section">
        <?php if(Session::has('message')): ?>
<span class="error" align="center" style="font-size: 14px;"><?php echo e(Session::get('message')); ?></span>
        <?php endif; ?>
        <div class="diamond_main_wrapper"> <?php if(Session::get('lang_file')!='en_lang'): ?> <img src="<?php echo e(url('')); ?>/themes/images/occasion_co_ordinator_AR.jpg" alt="" usemap="#homeMap" hidefocus="true"> <?php else: ?> <img src="<?php echo e(url('')); ?>/themes/images/occasion_co_ordinator_EN.jpg" alt="" usemap="#homeMap" hidefocus="true"> <?php endif; ?>
          <map name="homeMap" id="homeMap">
            <area shape="poly" coords="572,112,469,12,299,178,292,188,295,189,648,190" href="<?php echo e(url('')); ?>/kosha-shops/11/12" />
            <area shape="poly" coords="108,372,466,374,469,471,465,547,434,552,103,550,12,465" href="<?php echo e(url('')); ?>/electronic-invitations/11/16" />
            <area shape="poly" coords="283,555,830,555,767,621,654,733,560,733,282,732,105,555" href="<?php echo e(url('')); ?>/roses/11/15" />
            <area shape="poly" coords="652,736,283,734,280,732,290,741,384,833,470,920" href="<?php echo e(url('')); ?>/special-events/11/17" />
            <area shape="poly" coords="649,192,288,192,108,369,109,371,830,371,826,371" href="<?php echo e(url('')); ?>/photography-studio/11/127" />
            <area shape="poly" coords="467,374,829,375,921,464,830,552,467,552" href="<?php echo e(url('')); ?>/reception-and-hospitality/11/14" />
          </map>
        </div>
        <center>
        </center>
        <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
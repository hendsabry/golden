<!-- HALL SECTION LEFT MENU START-->
<?php
 $checkVal= 0; 
?>

<?php if(isset($ship_leftmenu) && $ship_leftmenu==1 ): ?>


<div class="hall-menu-outer "><div class="hall_menu"></div></div>
<div  class="left_panel">

<div class="sidenav"> 
  <a class="active"  href="#"> <?php if(Lang::has(Session::get('mer_lang_file').'.MANAGE_SHIPPING')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MANAGE_SHIPPING')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MANAGE_SHIPPING')); ?> <?php endif; ?> </a>

</div>
</div>


<?php endif; ?>


<?php if(isset($hall_leftmenu) && $hall_leftmenu==1): ?>
<?php
$hallmenu = '';
$ids = request()->id;
$showMenus = 0; 
 $showMenu = 0;
if(isset($_REQUEST['hid']) && $_REQUEST['hid'] !='')
{ $hid = $_REQUEST['hid'];
  $showMenu = Helper::checkinnerfood($hid); 
}


if(Request::is('list-hall')) {     $listhall = 'active';  } 
if(Request::is('sitemerchant_dashboard')) {     $sitemerchantdashboard = 'active'; } 
if(Request::is('hall-review-comments')) {     $hallreviewcomments = 'active'; $hallmenu = 'hall_riview'; $showMenus =$showMenu;} 
if(Request::is('hall-order')) {     $hallorder = 'active'; $showMenus =$showMenu;} 
if(Request::is('list-hall-offer') || Request::is('add-hall-offer') || Request::is("edit-hall-offer/$ids")) {     $listhalloffer = 'active'; $showMenus =$showMenu;} 
if(Request::is('list-container-package')) {     $listcontainerpackage = 'active'; $showMenus =$showMenu; } 
if(Request::is('list-hall-menu') || Request::is('add-hall-menu') || Request::is("edit-hall-menu/$ids")) {     $listhallmenu = 'active'; $showMenus =$showMenu;}
if(Request::is('editmenucategroy')) {     $menulist = 'active'; $showMenus =$showMenu; } 
if(Request::is('categorymenu')) {     $menulist = 'active'; $showMenus =$showMenu; } 
if(Request::is('menulist')) {     $menulist = 'active'; $showMenus =$showMenu;} 
if(Request::is("edit-container-package/$ids")) {     $listcontainerpackage = 'active'; $showMenus =$showMenu; }
if(Request::is('add-container-package')) {     $listcontainerpackage = 'active'; $showMenus =$showMenu;} 
if(Request::is('hall-paid-services')) {     $hallpaidservices = 'active'; $showMenus =$showMenu; } 
if(Request::is('hall-free-services')) {     $hallfreeservices = 'active'; $showMenus =$showMenu; }
if(Request::is('hall-picture')) {     $hallpicture = 'active'; $hallmenu = 'menuright'; $checkVal=1; $showMenus =$showMenu; } 
if(Request::is('hall-info')) {     $hallinfo = 'active';  $showMenus =$showMenu; } 
?>

<div class="hall-menu-outer "><div class="hall_menu <?php echo e($hallmenu); ?>"></div></div>
<div  class="left_panel">

<div class="sidenav"> 
  <a class="<?php echo e(isset($listhall) ? $listhall : ''); ?>"  href="<?php echo e(url('/list-hall')); ?><?php if(isset($_REQUEST['bid']) && $_REQUEST['bid']!=''): ?>/<?php echo e(isset($_REQUEST['bid']) ? $_REQUEST['bid'] : ''); ?> <?php endif; ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HallLIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HallLIST')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HallLIST')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($hallinfo) ? $hallinfo : ''); ?>" href="<?php echo e(url('/hall-info')); ?><?php if(isset($_REQUEST['bid']) && $_REQUEST['bid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALL_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALL_INFO')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALL_INFO')); ?> <?php endif; ?> </a>

<?php
if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')
{
?>

<a  class="<?php echo e(isset($hallpicture) ? $hallpicture : ''); ?>" href="<?php echo e(url('/hall-picture')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLPICTUREANDVIDEO')); ?> <?php endif; ?> </a>
   
  <div class="dropdown-btn"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLSERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLSERVICES')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLSERVICES')); ?> <?php endif; ?>  
    
  </div>
  <div class="dropdown-container">
    <a  class="<?php echo e(isset($hallfreeservices) ? $hallfreeservices : ''); ?>" href="<?php echo e(url('/hall-free-services')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_FREESERVICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FREESERVICE')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FREESERVICE')); ?> <?php endif; ?> </a>
  <a class="<?php echo e(isset($hallpaidservices) ? $hallpaidservices : ''); ?>"  href="<?php echo e(url('/hall-paid-services')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_HALLPAIDSERVICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_HALLPAIDSERVICE')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_HALLPAIDSERVICE')); ?> <?php endif; ?> </a>
    
  </div>
<?php if($showMenus ==1): ?>
  <a class="<?php echo e(isset($menulist) ? $menulist : ''); ?>" href="<?php echo e(url('/menulist')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLMENUCATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLMENUCATEGORY')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLMENUCATEGORY')); ?> <?php endif; ?> </a>
 
 <a class="<?php echo e(isset($listcontainerpackage) ? $listcontainerpackage : ''); ?>" href="<?php echo e(url('/list-container-package')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container')); ?> <?php endif; ?> </a>

<a class="<?php echo e(isset($listhallmenu) ? $listhallmenu : ''); ?>" href="<?php echo e(url('/list-hall-menu')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DISH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DISH')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DISH')); ?> <?php endif; ?> </a>
 
<?php endif; ?>


 
<a class="<?php echo e(isset($listhalloffer) ? $listhalloffer : ''); ?>" href="<?php echo e(url('/list-hall-offer')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Offer')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Offer')); ?> <?php endif; ?> </a>
 
<a class="<?php echo e(isset($hallorder) ? $hallorder : ''); ?>"  href="<?php echo e(url('/hall-order')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
 <a class="<?php echo e(isset($hallreviewcomments) ? $hallreviewcomments : ''); ?>"  href="<?php echo e(url('/hall-review-comments')); ?><?php if(isset($_REQUEST['hid']) && $_REQUEST['hid']!=''): ?>?hid=<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>&bid=<?php echo e($_REQUEST['bid']); ?> <?php endif; ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
 
 <?php
}

 ?>

</div>
 
</div>
<?php endif; ?>
<!-- HALL SECTION LEFT MENU END-->


<?php if(isset($serviceleftmenu) && $serviceleftmenu==1): ?>
 <div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
  <a class="<?php echo e(isset($sitemerchantdashboard) ? $sitemerchantdashboard : ''); ?>" href="<?php echo e(url('/sitemerchant_dashboard')); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Services')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Services')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Services')); ?> <?php endif; ?> </a>

</div>
</div>
<?php endif; ?>


<?php if(isset($wallet_inner_leftmenu) && $wallet_inner_leftmenu==1): ?>
<?php
if(Request::is("my-wallet") || Request::is("wallet-addamount")) { $wlt = 'active'; } 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
  <a class="<?php echo e(isset($wlt) ? $wlt : ''); ?>" href="<?php echo e(route('my-wallet')); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MyWallet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MyWallet')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MyWallet')); ?> <?php endif; ?> </a>
</div>
</div>
<?php endif; ?>
 

<!-- RECEPTION AND HOSPITALITY--> 

<?php if(isset($reception_hospitality_leftmenu) && $reception_hospitality_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$ShopId = request()->shopid;
$itemid =  request()->itemid;
$cid = request()->cid;
if(Request::is("reception-hospitality/$idd/$ShopId"))            {     $receptionhospitality = 'active';        } 
if(Request::is("reception-shop-picture/$idd/$ShopId"))           {   $checkVal=1;  $receptionshoppicture = 'active';        } 
if(Request::is("reception-list-attributes/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId/$itemid"))        {     $receptionlistattributes = 'active';     } 
if(Request::is("reception-list-informations/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId/$cid"))      {     $receptionlistinformations = 'active';   } 
if(Request::is("reception-list-workers/$idd/$ShopId"))           {     $receptionlistworkers = 'active';        } 
if(Request::is("reception-list-packages/$idd/$ShopId") || Request::is("reception-add-packege/$idd/$ShopId")  || Request::is("reception-add-packege/$idd/$ShopId/$cid"))          {     $receptionlistpackages = 'active';       } 
if(Request::is("reception-list-orders/$idd/$ShopId"))            {     $receptionlistorders = 'active';         } 
if(Request::is("reception-list-reviewcomments/$idd/$ShopId"))    {     $receptionlistreviewcomments = 'active'; }  
if(Request::is("reception-shop-list/$idd/$ShopId"))          {     $receptionshoplist = 'active'; }   
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
<?php if($ShopId ==''): ?>
   <a class="<?php echo e(isset($receptionhospitality) ? $receptionhospitality : ''); ?>" href="<?php echo e(route('reception-add-shopinfo',['id' => request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.SHOPINFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SHOPINFO')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SHOPINFO')); ?> <?php endif; ?> </a>
<?php endif; ?>
<?php if($ShopId !=''): ?>

  <a class="<?php echo e(isset($receptionhospitality) ? $receptionhospitality : ''); ?>" href="<?php echo e(route('reception-hospitality',['id' => request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.SHOPINFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SHOPINFO')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SHOPINFO')); ?> <?php endif; ?> </a>





  <a class="<?php echo e(isset($receptionshoppicture) ? $receptionshoppicture : ''); ?>" href="<?php echo e(route('reception-shop-picture',['id' => request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>



  <a class="<?php echo e(isset($receptionlistattributes) ? $receptionlistattributes : ''); ?>" href="<?php echo e(route('reception-list-attributes',['id' =>request()->id ,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CATEGORY')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CATEGORY')); ?> <?php endif; ?> </a>



  <a class="<?php echo e(isset($receptionlistinformations) ? $receptionlistinformations : ''); ?>" href="<?php echo e(route('reception-list-informations',['id' =>request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.ITEAMINFORMATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ITEAMINFORMATION')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ITEAMINFORMATION')); ?> <?php endif; ?> </a>



  <a class="<?php echo e(isset($receptionlistworkers) ? $receptionlistworkers : ''); ?>" href="<?php echo e(route('reception-list-workers',['id' =>request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.WORKERSNATIONLITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WORKERSNATIONLITY')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.WORKERSNATIONLITY')); ?> <?php endif; ?> </a>



  <a class="<?php echo e(isset($receptionlistpackages) ? $receptionlistpackages : ''); ?>" href="<?php echo e(route('reception-list-packages',['id' =>request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PACKAGES')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PACKAGES')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($receptionlistorders) ? $receptionlistorders : ''); ?>" href="<?php echo e(route('reception-list-orders',['id' =>request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.ORDERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDERS')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDERS')); ?> <?php endif; ?> </a>


 <a class="<?php echo e(isset($receptionlistreviewcomments) ? $receptionlistreviewcomments : ''); ?>" href="<?php echo e(route('reception-list-reviewcomments',['id' =>request()->id,'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- Start Singer--> 

<?php if(isset($singer_leftmenu) && $singer_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;
if(Request::is("manage-singer/$idd/$hid"))            {     $managesinger = 'active';        } 
if(Request::is("singer-photo-video/$idd/$hid"))           {   $checkVal=1;  $singerphotovideo = 'active';        } 
if(Request::is("singer-comments-review/$idd/$hid"))        {     $singercommentsreview = 'active';     } 
if(Request::is("singer-quoted-requested-list/$idd/$hid"))      {     $singerrequestedlist = 'active';   } 
if(Request::is("singer-order-list/$idd/$hid"))      {     $singerorderlist = 'active';   } 
 ?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="<?php echo e(isset($managesinger) ? $managesinger : ''); ?>" href="<?php echo e(route('manage-singer',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Singer_Info_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Singer_Info_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Singer_Info_Menu')); ?> <?php endif; ?></a>

 <?php if($hid !=''): ?>
<a class="<?php echo e(isset($singerphotovideo) ? $singerphotovideo : ''); ?>" href="<?php echo e(route('singer-photo-video',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Singer_Photo_Video_Menu')); ?> <?php endif; ?></a>
<a class="<?php echo e(isset($singercommentsreview) ? $singercommentsreview : ''); ?>" href="<?php echo e(route('singer-comments-review',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Review_Comments_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Review_Comments_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Review_Comments_Menu')); ?> <?php endif; ?></a>
<a class="<?php echo e(isset($singerrequestedlist) ? $singerrequestedlist : ''); ?>" href="<?php echo e(route('singer-quoted-requested-list',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu')); ?> <?php endif; ?></a>
<a class="<?php echo e(isset($singerorderlist) ? $singerorderlist : ''); ?>" href="<?php echo e(route('singer-order-list',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDERS')); ?> <?php endif; ?></a>


<?php endif; ?>

</div>
</div>
<?php endif; ?>


<!-- End Singer--> 




<!-- Start Acoustics--> 

<?php if(isset($acoustics_leftmenu) && $acoustics_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$Cid= request()->Cid;
$hid = request()->hid;
$itmid = request()->autoid;
$itmids = request()->itmid;
if(Request::is("acoustics/$idd/$hid") || Request::is("acoustics/$idd/$hid/$Cid")) {$managesinger = 'active';} 
if(Request::is("acoustics-photo-video/$idd/$hid") || Request::is("acoustics-photo-video/$idd/$hid/$Cid")) {$checkVal=1; $singerphotovideo = 'active';} 
if(Request::is("acoustics-comments-review/$idd/$hid") || Request::is("acoustics-comments-review/$idd/$hid/$Cid")) {$singercommentsreview = 'active';} 
if(Request::is("acoustics-quoted-requested-list/$idd/$hid") || Request::is("acoustics-quoted-requested-list/$idd/$hid/$Cid") || Request::is("acoustics-quoted-requested-view/$idd/$hid/$itmids")) {$singerrequestedlist = 'active';}

if(Request::is("acoustic-offer/$idd/$hid") || Request::is("acoustic-offer/$idd/$hid/$Cid") || Request::is("acoustic-addoffer/$idd/$hid") || Request::is("acoustic-addoffer/$idd/$hid/$itmid")) {$acousticpffer = 'active';}  

if(Request::is("product-item/$idd/$hid")|| Request::is("product-item/$idd/$hid/$Cid")|| Request::is("product-additem/$idd/$hid") || Request::is("product-additem/$idd/$hid/$itmid")) {$products = 'active';}

if(Request::is("acoustic-order/$idd/$hid") || Request::is("acoustic-order/$idd/$hid/$Cid")) {$acousticorder = 'active';} 
 

 ?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="<?php echo e(isset($managesinger) ? $managesinger : ''); ?>" href="<?php echo e(route('acoustics',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Acousticsinfo')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Acousticsinfo')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Acousticsinfo')); ?> <?php endif; ?></a>

 <?php if($hid!=''): ?>
<a class="<?php echo e(isset($singerphotovideo) ? $singerphotovideo : ''); ?>" href="<?php echo e(route('acoustics-photo-video',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Singer_Photo_Video_Menu')); ?> <?php endif; ?></a>


<a class="<?php echo e(isset($products) ? $products : ''); ?>" href="<?php echo e(route('product-item',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRODUCTS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS')); ?> <?php endif; ?></a>

 <a class="<?php echo e(isset($acousticpffer) ? $acousticpffer : ''); ?>" href="<?php echo e(route('acoustic-offer',['id' => request()->id,'hid'=>request()->hid,'Cid' => request()->Cid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
 
<a class="<?php echo e(isset($acousticorder) ? $acousticorder : ''); ?>" href="<?php echo e(route('acoustic-order',['id' => request()->id,'hid'=>request()->hid,'Cid' => request()->Cid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>

<a class="<?php echo e(isset($singercommentsreview) ? $singercommentsreview : ''); ?>" href="<?php echo e(route('acoustics-comments-review',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Review_Comments_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Review_Comments_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Review_Comments_Menu')); ?> <?php endif; ?></a>

<a class="<?php echo e(isset($singerrequestedlist) ? $singerrequestedlist : ''); ?>" href="<?php echo e(route('acoustics-quoted-requested-list',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu')); ?> <?php endif; ?></a>
 <?php endif; ?>

</div>
</div>
<?php endif; ?>


<!-- End Acoustics--> 


<!-- Start Popular Band--> 

<?php if(isset($popular_band_leftmenu) && $popular_band_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;

if(Request::is("popular-band-info/$idd/$hid"))                      {    $popularbandinfo = 'active';        } 
if(Request::is("popular-band-photo-video/$idd/$hid"))               {  $checkVal=1;  $popularbandphotovideo = 'active';        } 
if(Request::is("popular-band-comments-review/$idd/$hid"))           {   $popularbandcommentsreview = 'active';     } 
if(Request::is("popular-band-quoted-requested-list/$idd/$hid"))     {   $popularbandquotedrequestedlist = 'active';   } 
if(Request::is("popular-band-order-list/$idd/$hid"))      {     $popularbandorderlist = 'active';   } 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($popularbandinfo) ? $popularbandinfo : ''); ?>" href="<?php echo e(route('popular-band-info',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_POPULAR_BAND_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_POPULAR_BAND_INFO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_POPULAR_BAND_INFO')); ?> <?php endif; ?></a>
<?php if($hid!=''): ?>
<a class="<?php echo e(isset($popularbandphotovideo) ? $popularbandphotovideo : ''); ?>" href="<?php echo e(route('popular-band-photo-video',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Popupr_Band_Photo_video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Popupr_Band_Photo_video')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Popupr_Band_Photo_video')); ?> <?php endif; ?></a>

<a class="<?php echo e(isset($popularbandcommentsreview) ? $popularbandcommentsreview : ''); ?>" href="<?php echo e(route('popular-band-comments-review',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.popular_band_comments_review')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.popular_band_comments_review')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.popular_band_comments_review')); ?> <?php endif; ?></a>

<a class="<?php echo e(isset($popularbandquotedrequestedlist) ? $popularbandquotedrequestedlist : ''); ?>" href="<?php echo e(route('popular-band-quoted-requested-list',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu')); ?> <?php endif; ?></a>

<a class="<?php echo e(isset($popularbandorderlist) ? $popularbandorderlist : ''); ?>" href="<?php echo e(route('popular-band-order-list',['id' => request()->id,'hid' => request()->hid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDERS')); ?> <?php endif; ?></a>
 <?php endif; ?>


 

</div>
</div>
<?php endif; ?>


<!-- End Singer--> 





 

<!-- Buffet Menu Start ajit--> 

<?php if(isset($buffet_leftmenu) && $buffet_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$item = request()->itemid;
$autoid = request()->autoid;
if(Request::is("buffet-info/$idd/$sid") ||  Request::is("buffet-info/$idd/$sid/$item"))            {     $buffetinfo      = 'active';  }   
if(Request::is("buffet-picture/$idd/$sid/$item"))            {  $checkVal=1;   $buffetpicture         = 'active';  } 
if(Request::is("buffet-menu-list/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item/$autoid"))      {     $buffetmenlist         = 'active';  }

 
if(Request::is("buffet-orders/$idd/$sid/$item"))  {     $buffetorder         = 'active';  } 
if(Request::is("buffet-list-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item/$autoid"))            {     $buffetmenlistcontainer         = 'active';  }
 
if(Request::is("buffet-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item/$autoid"))            {     $buffetdish        = 'active';  }
if(Request::is("buffet-reviews-and-comments/$idd/$sid/$item"))            {     $buffetreviewcomment        = 'active';  }      

?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($buffetlistbranch) ? $buffetlistbranch : ''); ?>" href="<?php echo e(route('buffet-list-branch',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?> </a>


 <a class="<?php echo e(isset($buffetinfo) ? $buffetinfo : ''); ?>" href="<?php echo e(route('buffet-info',['id' => request()->id,'sid' =>request()->sid,'itemid' =>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>


<?php if($item!=''): ?>
  <a class="<?php echo e(isset($buffetpicture) ? $buffetpicture : ''); ?>" href="<?php echo e(route('buffet-picture',['id' => request()->id,'sid' => request()->sid,'itemid' =>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
  <a class="<?php echo e(isset($buffetmenlist) ? $buffetmenlist : ''); ?>" href="<?php echo e(route('buffet-menu-list',['id' => request()->id,'sid' => request()->sid,'itemid' =>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLMENU')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLMENU')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLMENU')); ?> <?php endif; ?> </a>
  
    <a class="<?php echo e(isset($buffetmenlistcontainer) ? $buffetmenlistcontainer : ''); ?>" href="<?php echo e(route('buffet-list-container',['id' => request()->id,'sid'=>request()->sid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container')); ?> <?php endif; ?> </a>

   <a class="<?php echo e(isset($buffetdish) ? $buffetdish : ''); ?>" href="<?php echo e(route('buffet-dish',['id' => request()->id,'sid' =>request()->sid,'itemid'=>request('itemid')])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DISH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DISH')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DISH')); ?> <?php endif; ?> </a>
<a class="<?php echo e(isset($buffetorder) ? $buffetorder : ''); ?>" href="<?php echo e(route('buffet-orders',['id' => request()->id,'sid'=>request()->sid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($buffetreviewcomment) ? $buffetreviewcomment : ''); ?>" href="<?php echo e(route('buffet-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
  <?php endif; ?>
</div>
</div>
<?php endif; ?>
<!-- Buffet Menu End ajit--> 




<?php if(isset($listmanagerblade) && $listmanagerblade==1): ?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">

 <a class="<?php echo e(isset($listmanager) ? $listmanager : ''); ?>" href="<?php echo e(url('/listmanager')); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_MANAGER')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_MANAGER')); ?> <?php endif; ?> </a>
</div>
</div>


<?php endif; ?>

<?php if(isset($hotel_leftmenu) && $hotel_leftmenu==1): ?>


<?php 
$id = request()->id;
 if(Request::is("edithotelname/$id")) {     $hotelnamelist = 'active';  } 
 if(Request::is("service/$id")) {     $hotelnamelist = 'active';  } 

if(Request::is('listmanager')) {     $listmanager = 'active'; } 
if(Request::is('list-branch-all')) {     $listbranchall = 'active'; } 
   $putsY = session()->get('menuss');   
if(Request::is('hotelnamelist/4')) {     $hotelnamelist = 'active'; } 

?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">
<?php if($id==4 || $putsY==4): ?>

<?php if($id==4){ $putY = Session::put('menuss',4); } ?>
  <a class="<?php echo e(isset($hotelnamelist) ? $hotelnamelist : ''); ?>" href="<?php echo e(url('/hotelnamelist/4')); ?>">  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HOTELS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HOTELS')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HOTELS')); ?> <?php endif; ?> </a>


 
 <?php elseif($id==5 || $putsY==5): ?>
<?php if($id==5){ $putY = Session::put('menuss',5); }

 if($mer_selected_lang_code !='en') 
 {
$hotalHall ='كبير'; 
 }
 else
 {
   $hotalHall ='Large';
 }

   

 ?>
  <a class="<?php echo e(isset($hotelnamelist) ? $hotelnamelist : ''); ?>" href="<?php echo e(url('/hotelnamelist/5')); ?>"><?php echo e(isset($hotalHall) ? $hotalHall : ''); ?> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HOTELS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HOTELS')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HOTELS')); ?> <?php endif; ?> </a>
 <?php elseif($id==6 ||  $putsY==6): ?>
<?php if($id==6){ $putY = Session::put('menuss',6); }

 if($mer_selected_lang_code !='en') 
 {
$hotalHall ="صالات الأفراح";
 }
 else
 {
  $hotalHall ='Small';
 }

 ?>
  <a class="<?php echo e(isset($hotelnamelist) ? $hotelnamelist : ''); ?>" href="<?php echo e(url('/hotelnamelist/6')); ?>"><?php echo e(isset($hotalHall) ? $hotalHall : ''); ?> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HOTELS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HOTELS')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HOTELS')); ?> <?php endif; ?> </a>

<?php endif; ?>
   </div>
   </div>
<?php endif; ?>

<!-- Buffet Branch 3 links--> 

<?php if(isset($buffeth_leftmenu) && $buffeth_leftmenu==1): ?>  
<?php 
$ids = request()->id; 
$sid = request()->sid;
if(Request::is("buffet-resturant/$ids") || Request::is("buffet-add-resturant/$ids") || Request::is("buffet-add-resturant/$ids/$sid")){   $buffetresturant = 'active'; } 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4"> 

  <a class="<?php echo e(isset($buffetresturant) ? $buffetresturant : ''); ?>" href="<?php echo e(route('buffet-resturant',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_RESTURANT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_RESTURANT')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_RESTURANT')); ?> <?php endif; ?> </a>
 

   </div>
   </div>
<?php endif; ?>


<!-- Beauty Branch 3 links--> 

<?php if(isset($beauty_leftmenu) && $beauty_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
 
if(Request::is("beauty-shop/$id") || Request::is("beauty-add-shop/$id") || Request::is("beauty-add-shop/$id/$hid")) {$beautyshop = 'active';} 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">

  <a class="<?php echo e(isset($beautyshop) ? $beautyshop : ''); ?>" href="<?php echo e(route('beauty-shop',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP')); ?> <?php endif; ?> </a>
 
 

   </div>
   </div>
<?php endif; ?>


<!-- Start For Beauty Big Menu -->

<?php if(isset($beautybig_leftmenu) && $beautybig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid  = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
$workerid =  request()->workerid;
if(Request::is("beauty-shop-branchh/$idd"))            {     $beautyshopbranch      = 'active';  }   
if(Request::is("beauty-picture/$idd/$hid/$itemid"))            {   $checkVal=1;  $beautytpicture         = 'active';  } 
if(Request::is("beauty-shop-info/$idd/$hid") || Request::is("beauty-shop-info/$idd/$hid/$itemid"))           {     $beautyshopinfo          = 'active';  }



if(Request::is("beauty-service-category/$idd/$hid/$itemid") || Request::is("beauty-add-service-catogory/$idd/$hid/$itemid") || Request::is("beauty-add-service-catogory/$idd/$hid/$itemid/$catitemid"))           {     $beautyservicecat          = 'active';  }
 if(Request::is("beauty-service/$idd/$hid/$itemid") || Request::is("beauty-add-service/$idd/$hid/$itemid") || Request::is("beauty-add-service/$idd/$hid/$itemid/$serviceid"))                {     $beautyservice              = 'active';  }
 
if(Request::is("beauty-package/$idd/$hid/$itemid") || Request::is("beauty-add-package/$idd/$hid/$itemid") || Request::is("beauty-add-package/$idd/$hid/$itemid/$serviceid"))  {     $beautypackage   = 'active';  }
if(Request::is("beauty-worker/$idd/$hid/$itemid") || Request::is("beauty-add-worker/$idd/$hid/$itemid") || Request::is("beauty-add-worker/$idd/$hid/$itemid/$workerid"))  {     $beautyworker   = 'active';  }
if(Request::is("buffet-add-branch/$idd"))  {     $buffetlistbranch   = 'active';  }
if(Request::is("beauty-order/$idd/$hid/$itemid"))  {     $beautyorders   = 'active';  }
if(Request::is("beauty-add-worker/$idd"))  {     $beautyworker   = 'active';  }
if(Request::is("beauty-reviews-and-comments/$idd/$hid/$itemid"))  {     $beautyreviewsandcomments   = 'active';  }
if(Request::is("beauty-offer/$idd/$hid/$itemid") || Request::is("beauty-add-offer/$idd/$hid/$itemid")  || Request::is("beauty-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $beautyoffer   = 'active';  }




?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($beautyshopbranch) ? $beautyshopbranch : ''); ?>" href="<?php echo e(route('beauty-shop-branch',['id' => request()->id,'hid' => $hid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?> </a>


 <a class="<?php echo e(isset($beautyshopinfo) ? $beautyshopinfo : ''); ?>" href="<?php echo e(route('beauty-shop-info',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>
  


<?php if(request()->itemid!=''): ?>

   <a class="<?php echo e(isset($beautytpicture) ? $beautytpicture : ''); ?>" href="<?php echo e(route('beauty-picture',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($beautyservicecat) ? $beautyservicecat : ''); ?>" href="<?php echo e(route('beauty-service-category',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($beautyservice) ? $beautyservice : ''); ?>" href="<?php echo e(route('beauty-service',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICES')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($beautypackage) ? $beautypackage : ''); ?>" href="<?php echo e(route('beauty-package',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PACKAGES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.PACKAGES')); ?> <?php endif; ?> </a>
  
   <a class="<?php echo e(isset($beautyworker) ? $beautyworker : ''); ?>" href="<?php echo e(route('beauty-worker',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.WORKERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WORKERS')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.WORKERS')); ?> <?php endif; ?> </a>


<a class="<?php echo e(isset($beautyorders) ? $beautyorders : ''); ?>" href="<?php echo e(route('beauty-order',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($beautyoffer) ? $beautyoffer : ''); ?>" href="<?php echo e(route('beauty-offer',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($beautyreviewsandcomments) ? $beautyreviewsandcomments : ''); ?>" href="<?php echo e(route('beauty-reviews-and-comments',['id' => request()->id,'hid' => $hid,'itemid' => $itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

<?php endif; ?>
</div>
</div>
<?php endif; ?>
<!-- End For Beauty Big Menu -->



<?php if(isset($oudandperfumes_leftmenu) && $oudandperfumes_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("oudandperfumes-shop/$id") || Request::is("oudandperfumes-add-shop/$id")  || Request::is("oudandperfumes-add-shop/$id/$hid")) { $oudandperfumesshop = 'active';}  
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav 4">
<a class="<?php echo e(isset($oudandperfumesshop) ? $oudandperfumesshop : ''); ?>" href="<?php echo e(route('oudandperfumes-shop',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP')); ?> <?php endif; ?> </a>

</div>
</div>
<?php endif; ?>
<!-- Start For oudandperfumes Big Menu  -->
<?php if(isset($oudandperfumesbig_leftmenu) && $oudandperfumesbig_leftmenu==1): ?> 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
if(Request::is("oudandperfumes-shop-branch/$idd"))            {     $oudandperfumesshopbranch      = 'active';  }   
if(Request::is("oudandperfumes-picture/$idd/$hid/$itemid"))            {  $checkVal=1;   $oudandperfumespicture         = 'active';  } 
if(Request::is("oudandperfumes-shop-info/$idd/$hid") || Request::is("oudandperfumes-shop-info/$idd/$hid/$itemid"))           {     $oudandperfumesshopinfo          = 'active';  }
if(Request::is("oudandperfumes-service-category/$idd/$hid/$itemid"))           {     $oudandperfumesservicecat          = 'active';  }
if(Request::is("oudandperfumes-add-service-catogory/$idd/$hid/$itemid") || Request::is("oudandperfumes-add-service-catogory/$idd/$hid/$itemid/$catitemid"))  {$oudandperfumesservicecat  = 'active';  }
if(Request::is("oudandperfumes-service/$idd/$hid/$itemid"))                {     $oudandperfumesservice              = 'active';  }
if(Request::is("oudandperfumes-add-service/$idd/$hid/$itemid") || Request::is("oudandperfumes-add-service/$idd/$hid/$itemid/$serviceid"))  {     $oudandperfumesservice   = 'active';  }
if(Request::is("oudandperfumes-package/$idd/$hid/$itemid"))  {     $oudandperfumespackage   = 'active';  }
if(Request::is("oudandperfumes-add-package/$idd/$hid/$itemid") || Request::is("oudandperfumes-add-package/$idd/$hid/$itemid/$serviceid"))  {     $oudandperfumespackage   = 'active';  }
if(Request::is("oudandperfumes-worker/$idd/$hid/$itemid"))  {     $oudandperfumesworker   = 'active';  }
if(Request::is("oudandperfumes-add-branch/$idd/$hid/$itemid"))  {     $oudandperfumeslistbranch   = 'active';  }
if(Request::is("oudandperfumes-order/$idd/$hid/$itemid"))  {     $oudandperfumesorders   = 'active';  }
if(Request::is("oudandperfumes-add-worker/$idd/$hid/$itemid"))  {     $oudandperfumesworker   = 'active';  }
if(Request::is("oudandperfumes-reviews-and-comments/$idd/$hid/$itemid"))  {     $oudandperfumesreviewsandcomments   = 'active';  }
if(Request::is("oudandperfumes-offer/$idd/$hid/$itemid"))  {     $oudandperfumesoffer   = 'active';  }
if(Request::is("oudandperfumes-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $oudandperfumesoffer   = 'active';  }
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="<?php echo e(isset($oudandperfumesshopbranch) ? $oudandperfumesshopbranch : ''); ?>" href="<?php echo e(route('oudandperfumes-shop-branch',['id' => request()->id,'hid'=>request()->hid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?> </a>


 <a class="<?php echo e(isset($oudandperfumesshopinfo) ? $oudandperfumesshopinfo : ''); ?>" href="<?php echo e(route('oudandperfumes-shop-info',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>
  
  <?php if($itemid !=''): ?>
   <a class="<?php echo e(isset($oudandperfumespicture) ? $oudandperfumespicture : ''); ?>" href="<?php echo e(route('oudandperfumes-picture',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($oudandperfumesservicecat) ? $oudandperfumesservicecat : ''); ?>" href="<?php echo e(route('oudandperfumes-service-category',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_CATEGORY')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($oudandperfumesservice) ? $oudandperfumesservice : ''); ?>" href="<?php echo e(route('oudandperfumes-service',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRODUCTS')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS')); ?> <?php endif; ?> </a>
  
 


<a class="<?php echo e(isset($oudandperfumesorders) ? $oudandperfumesorders : ''); ?>" href="<?php echo e(route('oudandperfumes-order',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($oudandperfumesoffer) ? $oudandperfumesoffer : ''); ?>" href="<?php echo e(route('oudandperfumes-offer',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($oudandperfumesreviewsandcomments) ? $oudandperfumesreviewsandcomments : ''); ?>" href="<?php echo e(route('oudandperfumes-reviews-and-comments',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

  <?php endif; ?>

</div>
</div>
<?php endif; ?>

<!-- End For oudandperfumes Big Menu -->

<?php if(isset($goldandjewelry_leftmenu) && $goldandjewelry_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("goldandjewelry-shop/$id") || Request::is("goldandjewelry-add-shop/$id")  || Request::is("goldandjewelry-add-shop/$id/$hid")) { $goldandjewelryshop = 'active';}  
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav 4">
<a class="<?php echo e(isset($goldandjewelryshop) ? $goldandjewelryshop : ''); ?>" href="<?php echo e(route('goldandjewelry-shop',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP')); ?> <?php endif; ?> </a>

</div>
</div>
<?php endif; ?>
<!-- Start For goldandjewelry Big Menu  -->
<?php if(isset($goldandjewelrybig_leftmenu) && $goldandjewelrybig_leftmenu==1): ?> 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
if(Request::is("goldandjewelry-shop-branch/$idd"))            {     $goldandjewelryshopbranch      = 'active';  }   
if(Request::is("goldandjewelry-picture/$idd/$hid/$itemid"))            {   $checkVal=1;  $goldandjewelrypicture         = 'active';  } 
if(Request::is("goldandjewelry-shop-info/$idd/$hid") || Request::is("goldandjewelry-shop-info/$idd/$hid/$itemid"))           {     $goldandjewelryshopinfo          = 'active';  }
if(Request::is("goldandjewelry-service-category/$idd/$hid/$itemid"))           {     $goldandjewelryservicecat          = 'active';  }
if(Request::is("goldandjewelry-add-service-catogory/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-service-catogory/$idd/$hid/$itemid/$catitemid"))  {$goldandjewelryservicecat  = 'active';  }
if(Request::is("goldandjewelry-service/$idd/$hid/$itemid"))                {     $goldandjewelryservice              = 'active';  }
if(Request::is("goldandjewelry-add-service/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-service/$idd/$hid/$itemid/$serviceid"))  {     $goldandjewelryservice   = 'active';  }
if(Request::is("goldandjewelry-package/$idd/$hid/$itemid"))  {     $goldandjewelrypackage   = 'active';  }
if(Request::is("goldandjewelry-add-package/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-package/$idd/$hid/$itemid/$serviceid"))  {     $goldandjewelrypackage   = 'active';  }
if(Request::is("goldandjewelry-worker/$idd/$hid/$itemid"))  {     $goldandjewelryworker   = 'active';  }
if(Request::is("goldandjewelry-add-branch/$idd/$hid/$itemid"))  {     $goldandjewelrylistbranch   = 'active';  }
if(Request::is("goldandjewelry-order/$idd/$hid/$itemid"))  {     $goldandjewelryorders   = 'active';  }
if(Request::is("goldandjewelry-add-worker/$idd/$hid/$itemid"))  {     $goldandjewelryworker   = 'active';  }
if(Request::is("goldandjewelry-reviews-and-comments/$idd/$hid/$itemid"))  {     $goldandjewelryreviewsandcomments   = 'active';  }
 
if(Request::is("goldandjewelry-offer/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-offer/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $goldandjewelryoffer   = 'active';  }

 ?>


<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="<?php echo e(isset($goldandjewelryshopbranch) ? $goldandjewelryshopbranch : ''); ?>" href="<?php echo e(route('goldandjewelry-shop-branch',['id' => request()->id,'hid'=>request()->hid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?> </a>


 <a class="<?php echo e(isset($goldandjewelryshopinfo) ? $goldandjewelryshopinfo : ''); ?>" href="<?php echo e(route('goldandjewelry-shop-info',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>
  
  <?php if($itemid !=''): ?>
   <a class="<?php echo e(isset($goldandjewelrypicture) ? $goldandjewelrypicture : ''); ?>" href="<?php echo e(route('goldandjewelry-picture',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($goldandjewelrypicture) ? $goldandjewelrypicture : ''); ?>" href="<?php echo e(route('goldandjewelry-manageprice',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_GOLD_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_GOLD_PRICE')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_GOLD_PRICE')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($goldandjewelryservicecat) ? $goldandjewelryservicecat : ''); ?>" href="<?php echo e(route('goldandjewelry-service-category',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_CATEGORY')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($goldandjewelryservice) ? $goldandjewelryservice : ''); ?>" href="<?php echo e(route('goldandjewelry-service',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRODUCTS')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS')); ?> <?php endif; ?> </a>
  
 


<a class="<?php echo e(isset($goldandjewelryorders) ? $goldandjewelryorders : ''); ?>" href="<?php echo e(route('goldandjewelry-order',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($goldandjewelryoffer) ? $goldandjewelryoffer : ''); ?>" href="<?php echo e(route('goldandjewelry-offer',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($goldandjewelryreviewsandcomments) ? $goldandjewelryreviewsandcomments : ''); ?>" href="<?php echo e(route('goldandjewelry-reviews-and-comments',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

  <?php endif; ?>

</div>
</div>
<?php endif; ?>

<!-- End For goldandjewelry Big Menu -->

<!-- Beauty Branch 3 links--> 

<?php if(isset($spa_leftmenu) && $spa_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("spa-shop/$id") || Request::is("spa-add-shop/$id")  || Request::is("spa-add-shop/$id/$hid")) {$spashop = 'active';}  
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">

  <a class="<?php echo e(isset($spashop) ? $spashop : ''); ?>" href="<?php echo e(route('spa-shop',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP')); ?> <?php endif; ?> </a>
  
   </div>
   </div>
<?php endif; ?>



<!-- Start For Spa Big Menu  -->

<?php if(isset($spabig_leftmenu) && $spabig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
if(Request::is("spa-shop-branch/$idd"))            {     $spashopbranch      = 'active';  }   
if(Request::is("spa-picture/$idd/$hid/$itemid"))            {  $checkVal=1;   $spapicture         = 'active';  } 
if(Request::is("spa-shop-info/$idd/$hid") || Request::is("spa-shop-info/$idd/$hid/$itemid"))           {     $spashopinfo          = 'active';  }
if(Request::is("spa-service-category/$idd/$hid/$itemid"))           {     $spaservicecat          = 'active';  }
if(Request::is("spa-add-service-catogory/$idd/$hid/$itemid") || Request::is("spa-add-service-catogory/$idd/$hid/$itemid/$catitemid"))  {$spaservicecat  = 'active';  }
if(Request::is("spa-service/$idd/$hid/$itemid"))                {     $spaservice              = 'active';  }
if(Request::is("spa-add-service/$idd/$hid/$itemid") || Request::is("spa-add-service/$idd/$hid/$itemid/$serviceid"))  {     $spaservice   = 'active';  }
if(Request::is("spa-package/$idd/$hid/$itemid"))  {     $spapackage   = 'active';  }
if(Request::is("spa-add-package/$idd/$hid/$itemid") || Request::is("spa-add-package/$idd/$hid/$itemid/$serviceid"))  {     $spapackage   = 'active';  }
if(Request::is("spa-worker/$idd/$hid/$itemid"))  {     $spaworker   = 'active';  }
if(Request::is("spa-add-branch/$idd/$hid/$itemid"))  {     $spalistbranch   = 'active';  }
if(Request::is("spa-order/$idd/$hid/$itemid"))  {     $spaorders   = 'active';  }
if(Request::is("spa-add-worker/$idd/$hid/$itemid"))  {     $spaworker   = 'active';  }
if(Request::is("spa-reviews-and-comments/$idd/$hid/$itemid"))  {     $spareviewsandcomments   = 'active';  }
if(Request::is("spa-offer/$idd/$hid/$itemid"))  {     $spaoffer   = 'active';  }
if(Request::is("spa-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $spaoffer   = 'active';  }
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($spashopbranch) ? $spashopbranch : ''); ?>" href="<?php echo e(route('spa-shop-branch',['id' => request()->id,'hid'=>request()->hid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?> </a>


 <a class="<?php echo e(isset($spashopinfo) ? $spashopinfo : ''); ?>" href="<?php echo e(route('spa-shop-info',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>
  
  <?php if($itemid !=''): ?>
   <a class="<?php echo e(isset($spapicture) ? $spapicture : ''); ?>" href="<?php echo e(route('spa-picture',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($spaservicecat) ? $spaservicecat : ''); ?>" href="<?php echo e(route('spa-service-category',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spaservice) ? $spaservice : ''); ?>" href="<?php echo e(route('spa-service',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICES')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spapackage) ? $spapackage : ''); ?>" href="<?php echo e(route('spa-package',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PACKAGES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.PACKAGES')); ?> <?php endif; ?> </a>
  
   <a class="<?php echo e(isset($spaworker) ? $spaworker : ''); ?>" href="<?php echo e(route('spa-worker',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.WORKERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WORKERS')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.WORKERS')); ?> <?php endif; ?> </a>


<a class="<?php echo e(isset($spaorders) ? $spaorders : ''); ?>" href="<?php echo e(route('spa-order',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spaoffer) ? $spaoffer : ''); ?>" href="<?php echo e(route('spa-offer',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spareviewsandcomments) ? $spareviewsandcomments : ''); ?>" href="<?php echo e(route('spa-reviews-and-comments',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

  <?php endif; ?>

</div>
</div>
<?php endif; ?>

<!-- End For spa Big Menu -->
 
<!-- Start For Makup Artist Big Menu -->

<?php if(isset($MakupArtistbig_leftmenu) && $MakupArtistbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid= request()->autoid;
if(Request::is("makeup-artist-shop-info/$idd/$sid")) {$makeupartist = 'active';}   
if(Request::is("makeup-artist-picture/$idd/$sid")) {$checkVal=1; $artistpicture = 'active';} 
if(Request::is("makeup-artist-service-category/$idd/$sid") || Request::is("makeup-artist-add-service-catogory/$idd/$sid")  || Request::is("makeup-artist-add-service-catogory/$idd/$sid/$autoid")) {$artistservicecat = 'active';}
if(Request::is("makeup-artist-service/$idd/$sid") || Request::is("makeup-artist-add-service/$idd/$sid") || Request::is("makeup-artist-add-service/$idd/$sid/$autoid")) {$artistservice = 'active';}
if(Request::is("makeup-artist-order/$idd/$sid"))   {$spaorders = 'active';}
if(Request::is("makeup-artist-package/$idd/$sid")) {$artistpackage = 'active';}
if(Request::is("makeup-artist-worker/$idd/$sid") || Request::is("makeup-artist-add-worker/$idd/$sid")  || Request::is("makeup-artist-add-worker/$idd/$sid/$autoid")) {$artistworker = 'active';}
if(Request::is("makeup-artist-reviews-and-comments/$idd/$sid")) {$spareviewsandcomments = 'active';}
if(Request::is("makeup-artist-offer/$idd/$sid") || Request::is("makeup-artist-add-offer/$idd/$sid") || Request::is("makeup-artist-add-offer/$idd/$sid/$autoid")) {$spaoffer = 'active';}
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($makeupartist) ? $makeupartist : ''); ?>" href="<?php echo e(route('makeup-artist-shop-info',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
  
   <a class="<?php echo e(isset($artistpicture) ? $artistpicture : ''); ?>" href="<?php echo e(route('makeup-artist-picture',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($artistservicecat) ? $artistservicecat : ''); ?>" href="<?php echo e(route('makeup-artist-service-category',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY')); ?> <?php endif; ?> </a>
  
  
  
  <a class="<?php echo e(isset($artistservice) ? $artistservice : ''); ?>" href="<?php echo e(route('makeup-artist-service',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICES')); ?> <?php endif; ?> </a>
  
  
    <a class="<?php echo e(isset($artistworker) ? $artistworker : ''); ?>" href="<?php echo e(route('makeup-artist-worker',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.WORKERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WORKERS')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.WORKERS')); ?> <?php endif; ?> </a>  


<a class="<?php echo e(isset($spaorders) ? $spaorders : ''); ?>" href="<?php echo e(route('makeup-artist-order',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spaoffer) ? $spaoffer : ''); ?>" href="<?php echo e(route('makeup-artist-offer',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spareviewsandcomments) ? $spareviewsandcomments : ''); ?>" href="<?php echo e(route('makeup-artist-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

</div>
</div>
<?php endif; ?>

<!-- End For Makup Artist Big Menu -->
 
<!-- Start tailor Big Menu -->

<?php if(isset($Tailorbig_leftmenu) && $Tailorbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
if(Request::is("tailor-shop-info/$idd/$itemid")) {$tailorshopinfo = 'active';}   
if(Request::is("tailor-picture/$idd/$itemid")) {$checkVal=1; $tailorpicture = 'active';} 
if(Request::is("tailor-product/$idd/$itemid") || Request::is("tailor-addproduct/$idd/$itemid")|| Request::is("tailor-addproduct/$idd/$itemid/$autoid")) {$tailorproduct = 'active';}

if(Request::is("tailor-fabric/$idd/$itemid") || Request::is("tailor-addfabric/$idd/$itemid")|| Request::is("tailor-addfabric/$idd/$itemid/$autoid")) {$tailorfabric = 'active';}


if(Request::is("tailor-order/$idd/$itemid")) {$spaorders = 'active';}
if(Request::is("tailor-reviews-and-comments/$idd/$itemid")) {$spareviewsandcomments = 'active';}
if(Request::is("tailor-offer/$idd/$itemid") || Request::is("tailor-add-offer/$idd/$itemid") || Request::is("tailor-add-offer/$idd/$itemid/$autoid") ) {$tailorpffer = 'active';}
?> 
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($tailorshopinfo) ? $tailorshopinfo : ''); ?>" href="<?php echo e(route('tailor-shop-info',['id' => request()->id,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
    
    <?php if($itemid!=''): ?>
   <a class="<?php echo e(isset($tailorpicture) ? $tailorpicture : ''); ?>" href="<?php echo e(route('tailor-picture',['id' => request()->id,'itemid' =>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($tailorfabric) ? $tailorfabric : ''); ?>" href="<?php echo e(route('tailor-fabric',['id' =>request()->id,'itemid' =>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Fabric')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Fabric')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Fabric')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($tailorproduct) ? $tailorproduct : ''); ?>" href="<?php echo e(route('tailor-product',['id' =>request()->id,'itemid' =>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Stitch_Designs')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Stitch_Designs')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Stitch_Designs')); ?> <?php endif; ?> </a>

<a class="<?php echo e(isset($spaorders) ? $spaorders : ''); ?>" href="<?php echo e(route('tailor-order',['id' => request()->id,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($tailorpffer) ? $tailorpffer : ''); ?>" href="<?php echo e(route('tailor-offer',['id' => request()->id,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spareviewsandcomments) ? $spareviewsandcomments : ''); ?>" href="<?php echo e(route('tailor-reviews-and-comments',['id' => request()->id,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
   <?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For tailor Big Menu -->



<!-- Start For Photography Big Menu -->

<?php if(isset($photographybig_leftmenu) && $photographybig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$itemid = request()->sid;
$autoid = request()->autoid;
if(Request::is("photography-shop-info/$idd/$itemid") || Request::is("photography-shop-info/$idd")) {$makeupartist = 'active';}   
if(Request::is("photography-picture/$idd/$itemid")) {$checkVal=1; $photographypicture = 'active';} 
if(Request::is("photography-package/$idd/$itemid") || Request::is("photography-add-package/$idd/$itemid") || Request::is("photography-add-package/$idd/$itemid/$autoid")) {$photographypackage = 'active';}
if(Request::is("photography-order/$idd/$itemid")) {$photographyorders = 'active';}
if(Request::is("photography-reviews-and-comments/$idd/$itemid")) {$photographyreviewsandcomments = 'active';}
if(Request::is("photography-offer/$idd/$itemid") || Request::is("photography-add-offer/$idd/$itemid") || Request::is("photography-add-offer/$idd/$itemid/$autoid")) {$photographyoffer = 'active';}

?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($makeupartist) ? $makeupartist : ''); ?>" href="<?php echo e(route('photography-shop-info',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
  <?php if($itemid !=''): ?>
 <a class="<?php echo e(isset($photographypicture) ? $photographypicture : ''); ?>" href="<?php echo e(route('photography-picture',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
  
 <a class="<?php echo e(isset($photographypackage) ? $photographypackage : ''); ?>" href="<?php echo e(route('photography-package',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PACKAGES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.PACKAGES')); ?> <?php endif; ?> </a>

<a class="<?php echo e(isset($photographyorders) ? $photographyorders : ''); ?>" href="<?php echo e(route('photography-order',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($photographyoffer) ? $photographyoffer : ''); ?>" href="<?php echo e(route('photography-offer',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($photographyreviewsandcomments) ? $photographyreviewsandcomments : ''); ?>" href="<?php echo e(route('photography-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>    
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For Photography Big Menu -->

<!-- Start For abaya Big Menu -->

<?php if(isset($Abayabig_leftmenu) && $Abayabig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
if(Request::is("abaya-shop-info/$idd/$sid")) {$abayashop = 'active';}   

if(Request::is("abayatailor-fabric/$idd/$sid") || Request::is("abayatailor-addfabric/$idd/$sid")) {$abayafabshop = 'active';}   

if(Request::is("abaya-picture/$idd/$sid")) {$checkVal=1; $abayapicure = 'active';} 
if(Request::is("abaya-category/$idd/$sid")|| Request::is("abaya-add-catogory/$idd/$sid") || Request::is("abaya-add-catogory/$idd/$sid/$autoid")) {$abayacategory = 'active';} 
if(Request::is("abaya-service-item/$idd")) {$abayaservice = 'active';}
if(Request::is("abaya-order/$idd/$sid")) {$abayaorder = 'active';}
if(Request::is("abaya-item/$idd/$sid")|| Request::is("abaya-add-item/$idd/$sid") || Request::is("abaya-add-item/$idd/$sid/$autoid")) {$abayaItem = 'active';}
if(Request::is("abaya-offer/$idd/$sid")|| Request::is("abaya-add-offer/$idd/$sid") || Request::is("abaya-add-offer/$idd/$sid/$autoid")) {$abayaoffer = 'active';}
if(Request::is("abaya-reviews-and-comments/$idd/$sid")) {$spareviewsandcomments = 'active';}
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="<?php echo e(isset($abayashop) ? $abayashop : ''); ?>" href="<?php echo e(route('abaya-shop-info',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
  
  <?php if($sid!=''): ?>
   <a class="<?php echo e(isset($abayapicure) ? $abayapicure : ''); ?>" href="<?php echo e(route('abaya-picture',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

   <!-- <a class="<?php echo e(isset($abayacategory) ? $abayacategory : ''); ?>" href="<?php echo e(route('abaya-category',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.CATEGORY')); ?> <?php endif; ?> </a>  -->   


  <a class="<?php echo e(isset($abayafabshop) ? $abayafabshop : ''); ?>" href="<?php echo e(route('abayatailor-fabric',['id' =>request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Fabric')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Fabric')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Fabric')); ?> <?php endif; ?> </a>

    <a class="<?php echo e(isset($abayaItem) ? $abayaItem : ''); ?>" href="<?php echo e(route('abaya-item',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.abaya_item')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.abaya_item')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.abaya_item')); ?> <?php endif; ?> </a>
  
<a class="<?php echo e(isset($abayaorder) ? $abayaorder : ''); ?>" href="<?php echo e(route('abaya-order',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($abayaoffer) ? $abayaoffer : ''); ?>" href="<?php echo e(route('abaya-offer',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spareviewsandcomments) ? $spareviewsandcomments : ''); ?>" href="<?php echo e(route('abaya-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For abaya Big Menu -->
 
<!-- Start For dresses Big Menu -->

<?php if(isset($dressesbig_leftmenu) && $dressesbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
if(Request::is("dresses-shop-info/$idd/$sid")) {$dressesshop = 'active';}   
if(Request::is("dresses-picture/$idd/$sid")) {$checkVal=1; $dressespicure = 'active';} 
if(Request::is("dresses-category/$idd/$sid")|| Request::is("dresses-add-catogory/$idd/$sid") || Request::is("dresses-add-catogory/$idd/$sid/$autoid")) {$dressescategory = 'active';}
if(Request::is("dresses-service-item/$idd")) {$dressesservice = 'active';}
if(Request::is("dresses-order/$idd/$sid")) {$dressesorder = 'active';}
if(Request::is("dresses-item/$idd/$sid")|| Request::is("dresses-add-item/$idd/$sid") || Request::is("dresses-add-item/$idd/$sid/$autoid")) {$dressesItem = 'active';}
if(Request::is("dresses-offer/$idd/$sid")|| Request::is("dresses-add-offer/$idd/$sid") || Request::is("dresses-add-offer/$idd/$sid/$autoid")) {$dressesoffer = 'active';}
if(Request::is("dresses-reviews-and-comments/$idd/$sid")) {$dressesreviewsandcomments = 'active';}

if(Request::is("dresses-size/$idd/$sid") || Request::is("dresses-add-size/$idd/$sid") || Request::is("dresses-add-size/$idd/$sid/$autoid") ) {$dressessize = 'active';}   
 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="<?php echo e(isset($dressesshop) ? $dressesshop : ''); ?>" href="<?php echo e(route('dresses-shop-info',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
  
  <?php if($sid!=''): ?>
   <a class="<?php echo e(isset($dressespicure) ? $dressespicure : ''); ?>" href="<?php echo e(route('dresses-picture',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

   <!---<a class="<?php echo e(isset($dressessize) ? $dressessize : ''); ?>" href="<?php echo e(route('dresses-size',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.SIZE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SIZE')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.SIZE')); ?> <?php endif; ?> </a>   --> 

   <a class="<?php echo e(isset($dressescategory) ? $dressescategory : ''); ?>" href="<?php echo e(route('dresses-category',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.CATEGORY')); ?> <?php endif; ?> </a>    

    <a class="<?php echo e(isset($dressesItem) ? $dressesItem : ''); ?>" href="<?php echo e(route('dresses-item',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.dresses_item')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.dresses_item')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.dresses_item')); ?> <?php endif; ?> </a>
  
<a class="<?php echo e(isset($dressesorder) ? $dressesorder : ''); ?>" href="<?php echo e(route('dresses-order',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($dressesoffer) ? $dressesoffer : ''); ?>" href="<?php echo e(route('dresses-offer',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($dressesreviewsandcomments) ? $dressesreviewsandcomments : ''); ?>" href="<?php echo e(route('dresses-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For dresses Big Menu -->

<!-- Start For Cosha Big Menu -->

<?php if(isset($Coshabig_leftmenu) && $Coshabig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
if(Request::is("cosha-shop-info/$idd/$sid")) {$coshashop = 'active';}   
if(Request::is("cosha-picture/$idd/$sid")) {$checkVal=1; $coshapicure = 'active';} 
if(Request::is("cosha-category/$idd/$sid")|| Request::is("cosha-add-catogory/$idd/$sid") || Request::is("cosha-add-catogory/$idd/$sid/$autoid")) {$coshacategory = 'active';}
if(Request::is("cosha-service-item/$idd")) {$coshaservice = 'active';}
if(Request::is("cosha-order/$idd/$sid")) {$coshaorder = 'active';}
if(Request::is("cosha-item/$idd/$sid")|| Request::is("cosha-add-item/$idd/$sid") || Request::is("cosha-add-item/$idd/$sid/$autoid")) {$coshaItem = 'active';}
if(Request::is("cosha-offer/$idd/$sid")|| Request::is("cosha-add-offer/$idd/$sid") || Request::is("cosha-add-offer/$idd/$sid/$autoid")) {$coshaoffer = 'active';}
if(Request::is("cosha-reviews-and-comments/$idd/$sid")) {$spareviewsandcomments = 'active';}
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="<?php echo e(isset($coshashop) ? $coshashop : ''); ?>" href="<?php echo e(route('cosha-shop-info',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
  
  <?php if($sid!=''): ?>
   <a class="<?php echo e(isset($coshapicure) ? $coshapicure : ''); ?>" href="<?php echo e(route('cosha-picture',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

   <a class="<?php echo e(isset($coshacategory) ? $coshacategory : ''); ?>" href="<?php echo e(route('cosha-category',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.CATEGORY')); ?> <?php endif; ?> </a>    

    <a class="<?php echo e(isset($coshaItem) ? $coshaItem : ''); ?>" href="<?php echo e(route('cosha-item',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.cosha_item')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.cosha_item')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.cosha_item')); ?> <?php endif; ?> </a>
  
<a class="<?php echo e(isset($coshaorder) ? $coshaorder : ''); ?>" href="<?php echo e(route('cosha-order',['id' => request()->id,'sid' =>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($coshaoffer) ? $coshaoffer : ''); ?>" href="<?php echo e(route('cosha-offer',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($spareviewsandcomments) ? $spareviewsandcomments : ''); ?>" href="<?php echo e(route('cosha-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For Cosha Big Menu -->

<!-- Start For Special Event Big Menu -->

<?php if(isset($Eventbig_leftmenu) && $Eventbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$shopid = request()->shopid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
if(Request::is("event-shop-info/$idd") || Request::is("event-shop-info/$idd/$shopid")) {$eventshop = 'active';}   
if(Request::is("event-picture/$idd/$shopid")) {$checkVal=1; $eventpicure = 'active';} 
if(Request::is("event-package/$idd/$shopid") || Request::is("event-add-package/$idd/$shopid")  || Request::is("event-add-package/$idd/$shopid/$itemid")) {$eventpackage = 'active';} 
if(Request::is("event-category/$idd/$shopid")) {$eventcategory = 'active';}
if(Request::is("event-service/$idd/$shopid")) {$eventservice = 'active';}
if(Request::is("event-item-information/$idd/$shopid") || Request::is("event-item-add-information/$idd/$shopid") || Request::is("event-item-add-information/$idd/$shopid/$cid") ) {$eventItemservice = 'active';}
if(Request::is("event-order/$idd/$shopid")) {$eventorder = 'active';}
if(Request::is("event-package/$idd/$shopid")) {$eventpackage = 'active';}
if(Request::is("event-worker/$idd/$shopid")) {$eventworker = 'active';}
if(Request::is("event-reviews-and-comments/$idd/$shopid")) {$eventreviewsandcomments = 'active';}
if(Request::is("event-item/$idd/$shopid") ) {$eventItem = 'active';} 
if(Request::is("event-offer/$idd/$shopid") || Request::is("event-add-offer/$idd/$shopid") || Request::is("event-add-offer/$idd/$shopid/$autoid")) {$eventoffer = 'active';}

?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="<?php echo e(isset($eventshop) ? $eventshop : ''); ?>" href="<?php echo e(route('event-shop-info',['id' => request()->id, 'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.special_event_info')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.special_event_info')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.special_event_info')); ?> <?php endif; ?> </a>
  
  <?php if($shopid!=''): ?>
   <a class="<?php echo e(isset($eventpicure) ? $eventpicure : ''); ?>" href="<?php echo e(route('event-picture',['id' => request()->id, 'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
  
   <a class="<?php echo e(isset($eventpackage) ? $eventpackage : ''); ?>" href="<?php echo e(route('event-package',['id' => request()->id, 'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CATEGORY')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CATEGORY')); ?> <?php endif; ?> </a>  

  <a class="<?php echo e(isset($eventItemservice) ? $eventItemservice : ''); ?>" href="<?php echo e(route('event-item-information',['id' => request()->id, 'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.item_information')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.item_information')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.item_information')); ?> <?php endif; ?> </a>
  
 

  
<a class="<?php echo e(isset($eventorder) ? $eventorder : ''); ?>" href="<?php echo e(route('event-order',['id' => request()->id, 'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($eventoffer) ? $eventoffer : ''); ?>" href="<?php echo e(route('event-offer',['id' => request()->id, 'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($eventreviewsandcomments) ? $eventreviewsandcomments : ''); ?>" href="<?php echo e(route('event-reviews-and-comments',['id' => request()->id, 'shopid' => request()->shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For Special Event Big Menu -->









<!-- Start For Car rental Menu -->

<?php if(isset($Carrental_leftmenu) && $Carrental_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
if(Request::is("car-rental/$idd") || Request::is("car-rental/$idd/$shopid")) {$eventshop = 'active';}   
if(Request::is("car-picture/$idd/$shopid")) {$checkVal=1; $eventpicure = 'active';} 

 
 
if(Request::is("car-items/$idd/$shopid") || Request::is("car-add-items/$idd/$shopid") || Request::is("car-add-items/$idd/$shopid/$autoid") ) {$eventItemservice = 'active';}

if(Request::is("car-order/$idd/$shopid")) {$eventorder = 'active';}
 
 
if(Request::is("car-reviews-and-comments/$idd/$shopid")) {$eventreviewsandcomments = 'active';}
if(Request::is("car-item/$idd/$shopid") ) {$eventItem = 'active';}

 
if(Request::is("car-offer/$idd/$shopid") || Request::is("car-add-offer/$idd/$shopid") || Request::is("car-add-offer/$idd/$shopid/$autoid")) {$eventoffer = 'active';}


?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="<?php echo e(isset($eventshop) ? $eventshop : ''); ?>" href="<?php echo e(route('car-rental',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.CarRentalInfo')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CarRentalInfo')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.CarRentalInfo')); ?> <?php endif; ?> </a>
  <?php if($shopid!=''): ?>
   <a class="<?php echo e(isset($eventpicure) ? $eventpicure : ''); ?>" href="<?php echo e(route('car-picture',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
 

  <a class="<?php echo e(isset($eventItemservice) ? $eventItemservice : ''); ?>" href="<?php echo e(route('car-items',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Car_Models')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Car_Models')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Car_Models')); ?> <?php endif; ?> </a>
   
<a class="<?php echo e(isset($eventorder) ? $eventorder : ''); ?>" href="<?php echo e(route('car-order',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($eventoffer) ? $eventoffer : ''); ?>" href="<?php echo e(route('car-offer',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($eventreviewsandcomments) ? $eventreviewsandcomments : ''); ?>" href="<?php echo e(route('car-reviews-and-comments',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>




<!-- Start For Car rental Menu -->

<?php if(isset($Travel_leftmenu) && $Travel_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;

if(Request::is("travel-info/$idd") || Request::is("travel-info/$idd/$shopid")) {$eventshop = 'active';}   
if(Request::is("travel-picture/$idd/$shopid")) {$checkVal=1; $eventpicure = 'active';} 

 
 
if(Request::is("travel-items/$idd/$shopid") || Request::is("travel-add-items/$idd/$shopid") || Request::is("travel-add-items/$idd/$shopid/$autoid") ) {$eventItemservice = 'active';}

if(Request::is("travel-order/$idd/$shopid")) {$eventorder = 'active';}
 
 
if(Request::is("travel-reviews-and-comments/$idd/$shopid")) {$eventreviewsandcomments = 'active';}
if(Request::is("travel-item/$idd/$shopid") ) {$eventItem = 'active';}

 
if(Request::is("travel-offer/$idd/$shopid") || Request::is("travel-add-offer/$idd/$shopid") || Request::is("travel-add-offer/$idd/$shopid/$autoid")) {$eventoffer = 'active';}


?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="<?php echo e(isset($eventshop) ? $eventshop : ''); ?>" href="<?php echo e(route('travel-info',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.TravelInfo')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.TravelInfo')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.TravelInfo')); ?> <?php endif; ?> </a>
  <?php if($shopid!=''): ?>
   <a class="<?php echo e(isset($eventpicure) ? $eventpicure : ''); ?>" href="<?php echo e(route('travel-picture',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
 

  <a class="<?php echo e(isset($eventItemservice) ? $eventItemservice : ''); ?>" href="<?php echo e(route('travel-items',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PACKAGES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.PACKAGES')); ?> <?php endif; ?> </a>
   
<a class="<?php echo e(isset($eventorder) ? $eventorder : ''); ?>" href="<?php echo e(route('travel-order',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($eventoffer) ? $eventoffer : ''); ?>" href="<?php echo e(route('travel-offer',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($eventreviewsandcomments) ? $eventreviewsandcomments : ''); ?>" href="<?php echo e(route('travel-reviews-and-comments',['id' => request()->id, 'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For Special Event Big Menu -->












<!-- Start For Makup Artist Big Menu -->

<?php if(isset($Rosestbig_leftmenu) && $Rosestbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
if(Request::is("roses-shop-info/$idd") || Request::is("roses-shop-info/$idd/$itemid")) {$inforoses = 'active';}   
if(Request::is("roses-picture/$idd/$itemid")) {$checkVal=1; $rosespicture = 'active';} 
if(Request::is("roses-package/$idd/$itemid") || Request::is("roses-add-package/$idd/$itemid") || Request::is("roses-add-package/$idd/$itemid/$autoid")) {$rosespackage = 'active';}
if(Request::is("roses-order/$idd/$itemid")) {$artistservice = 'active';}
if(Request::is("roses-type/$idd/$itemid") || Request::is("roses-add-type/$idd/$itemid") || Request::is("roses-add-type/$idd/$itemid/$autoid") ) {$rosestype = 'active';}
if(Request::is("roses-order/$idd/$itemid")) {$rosesorders = 'active';}
if(Request::is("roses-reviews-and-comments/$idd/$itemid")) {$rosesreviewsandcomments = 'active';}
if(Request::is("roses-offer/$idd/$itemid") || Request::is("roses-add-offer/$idd/$itemid/$autoid") || Request::is("roses-add-offer/$idd/$itemid") ) {$rosesoffer = 'active';} 
if(Request::is("wrapping-type/$idd/$itemid") || Request::is("roses-add-wraping-type/$idd/$itemid") || Request::is("roses-add-wraping-type/$idd/$itemid/$autoid")) {$wrappingtype = 'active';}
if(Request::is("wrapping-design/$idd/$itemid") || Request::is("roses-add-wraping-design/$idd/$itemid") || Request::is("roses-add-wraping-design/$idd/$itemid/$autoid")) {$wrappingdesign = 'active';}

?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($inforoses) ? $inforoses : ''); ?>" href="<?php echo e(route('roses-shop-info',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
  <?php if($itemid!=''): ?>
   <a class="<?php echo e(isset($rosespicture) ? $rosespicture : ''); ?>" href="<?php echo e(route('roses-picture',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>


  <a class="<?php echo e(isset($wrappingtype) ? $wrappingtype : ''); ?>" href="<?php echo e(route('wrapping-type',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.WRAPPINGTYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WRAPPINGTYPE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.WRAPPINGTYPE')); ?> <?php endif; ?> </a>

    <a class="<?php echo e(isset($wrappingdesign) ? $wrappingdesign : ''); ?>" href="<?php echo e(route('wrapping-design',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.WRAPPINGDESIGN')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WRAPPINGDESIGN')); ?>   <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.WRAPPINGDESIGN')); ?> <?php endif; ?> </a>


  <a class="<?php echo e(isset($rosestype) ? $rosestype : ''); ?>" href="<?php echo e(route('roses-type',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.ROSESTYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ROSESTYPE')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.ROSESTYPE')); ?> <?php endif; ?> </a>

    <a class="<?php echo e(isset($rosespackage) ? $rosespackage : ''); ?>" href="<?php echo e(route('roses-package',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PACKAGES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.PACKAGES')); ?> <?php endif; ?> </a>
   


<a class="<?php echo e(isset($rosesorders) ? $rosesorders : ''); ?>" href="<?php echo e(route('roses-order',['id'=>request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?><?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($rosesoffer) ? $rosesoffer : ''); ?>" href="<?php echo e(route('roses-offer',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
   
  
  <a class="<?php echo e(isset($rosesreviewsandcomments) ? $rosesreviewsandcomments : ''); ?>" href="<?php echo e(route('roses-reviews-and-comments',['id' => request()->id,'itemid' => request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For Makup Artist Big Menu -->




<!-- Start For Men's Barber Big Menu -->

<?php if(isset($mensbig_leftmenu) && $mensbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;

if(Request::is("mens-shop-info/$idd") || Request::is("mens-shop-info/$idd/$sid"))            {     $mens_review      = 'active';  }    
if(Request::is("mens-picture/$idd/$sid"))   {   $checkVal=1;  $menspicture          = 'active';  }



if(Request::is("mens-service-category/$idd/$sid") || Request::is("mens-add-service-catogory/$idd/$sid") || Request::is("mens-add-service-catogory/$idd/$sid/$autoid"))  {     $mensservicecategory          = 'active';  }

if(Request::is("mens-service/$idd/$sid") || Request::is("mens-add-service/$idd/$sid")  || Request::is("mens-add-service/$idd/$sid/$autoid"))  {$mensservice     = 'active';  }


if(Request::is("mens-worker/$idd/$sid") || Request::is("mens-add-worker/$idd/$sid") || Request::is("mens-add-worker/$idd/$sid/$autoid"))   {     $mensworker    = 'active';  }

if(Request::is("mens-order/$idd/$sid"))  {  $mensorder   = 'active';  }

if(Request::is("mens-offer/$idd/$sid") || Request::is("mens-add-offer/$idd/$sid") || Request::is("mens-add-offer/$idd/$sid/$autoid"))  {  $mensoffer   = 'active';  }

if(Request::is("mens-reviews-and-comments/$idd/$sid"))  {  $mensreview   = 'active';  }
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">


 <a class="<?php echo e(isset($mens_review) ? $mens_review : ''); ?>" href="<?php echo e(route('mens-shop-info',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.mens_barbe_info')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mens_barbe_info')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.mens_barbe_info')); ?> <?php endif; ?> </a>

  <?php if($sid!=''): ?>
   <a class="<?php echo e(isset($menspicture) ? $menspicture : ''); ?>" href="<?php echo e(route('mens-picture',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($mensservicecategory) ? $mensservicecategory : ''); ?>" href="<?php echo e(route('mens-service-category',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($mensservice) ? $mensservice : ''); ?>" href="<?php echo e(route('mens-service',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICES')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICES')); ?> <?php endif; ?> </a>
  
 
  
   <a class="<?php echo e(isset($mensworker) ? $mensworker : ''); ?>" href="<?php echo e(route('mens-worker',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.WORKERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WORKERS')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.WORKERS')); ?> <?php endif; ?></a>


<a class="<?php echo e(isset($mensorder) ? $mensorder : ''); ?>" href="<?php echo e(route('mens-order',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($mensoffer) ? $mensoffer : ''); ?>" href="<?php echo e(route('mens-offer',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_OFFER')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_OFFER')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($mensreview) ? $mensreview : ''); ?>" href="<?php echo e(route('mens-reviews-and-comments',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- Start For Men's Barber Big Menu End -->





<!-- Sratr For Dessert Front Menu -->

<?php if(isset($dessert_leftmenu) && $dessert_leftmenu==1): ?>
<?php 

$id= request()->id;
$services_id= request()->services_id;
$shopid= request()->shopid;
 
 
if(Request::is("dessert-shop/$id"))    {  $id = request()->id;   $buffetresturant = 'active'; } 
 

if(Request::is("dessert-add-shop/$services_id"))    {  $id = request()->services_id;   $buffetresturant = 'active'; } 

if(Request::is("dessert-list-branch/$services_id/$shopid"))    {  $id = request()->services_id;   $buffetresturant = 'active'; } 
?>


<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav">
<a class="<?php echo e(isset($buffetresturant) ? $buffetresturant : ''); ?>" href="<?php echo e(route('dessert-shop',['id' =>$id])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP')); ?> <?php endif; ?></a>



   </div>
   </div>


<?php endif; ?>
<!-- End For Dessert Front Menu -->

<!-- Sratr For Dessert Inner Menu -->
<?php if(isset($dessert_inner_leftmenu) && $dessert_inner_leftmenu==1): ?>
 
<?php
$idd = request()->id;

$services_id = request()->services_id;
$shopid = request()->shopid;
$itemid = request()->itemid;
$autoid = request()->autoid;
$offer_id = request()->offer_id;
if(Request::is("buffet-list-branch/$idd"))            {     $buffetlistbranch      = 'active';  }   
if(Request::is("dessert-picture-video/$services_id/$shopid/$itemid")) { $checkVal=1;    $buffetpicture         = 'active';  } 
if(Request::is("buffet-main-cource/$idd"))           {     $buffetmaincource          = 'active';  }
if(Request::is("buffet-dish/$idd"))           {     $buffetdish          = 'active';  }
if(Request::is("buffet-desert/$idd"))                {     $buffedesert               = 'active';  }
if(Request::is("buffet-orders/$idd"))                {     $buffeorders               = 'active';  }
if(Request::is("buffet-reviews-and-comments/$idd"))  {     $buffereviewsandcomments   = 'active';  }
if(Request::is("buffet-menu-list/$idd"))  {     $buffetmenu   = 'active';  }
if(Request::is("add-buffet-menu-category/$idd"))  {     $buffetmenu   = 'active';  }
if(Request::is("buffet-add-branch/$idd"))  {     $buffetlistbranch   = 'active';  }
if(Request::is("buffet-list-container/$idd"))  {     $buffetlistcontainer   = 'active';  }
if(Request::is("dessert-info/$services_id/$shopid") || Request::is("dessert-info/$services_id/$shopid/$itemid"))  {     $buffetinfo   = 'active';  }


if(Request::is("dessert-type/$services_id/$shopid/$itemid") || Request::is("dessert-add-type/$services_id/$shopid/$itemid") || Request::is("dessert-add-type/$services_id/$shopid/$itemid/$autoid"))  {     $buffetType   = 'active';  }


if(Request::is("dessert-list-offer/$services_id/$shopid/$itemid") || Request::is("dessert-add-offer/$services_id/$shopid/$itemid") || Request::is("dessert-add-offer/$services_id/$shopid/$itemid/$offer_id"))  {     $dessertlistoffer   = 'active';  }

 if(Request::is("dessert-order/$services_id/$shopid/$itemid"))  {     $dessertorders   = 'active';  }
 if(Request::is("dessert-reviews-and-comments/$services_id/$shopid/$itemid"))  {     $dessertreviewsandcomments   = 'active';  }



$shopid = request()->shopid;
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($buffetlistbranch) ? $buffetlistbranch : ''); ?>" href="<?php echo e(route('dessert-list-branch',['services_id' => request()->services_id,'shopid'=>$shopid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?></a>


 <a class="<?php echo e(isset($buffetinfo) ? $buffetinfo : ''); ?>" href="<?php echo e(route('dessert-info',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid' => request()->itemid] )); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>

<?php if(request()->itemid!=''): ?>
  <a class="<?php echo e(isset($buffetpicture) ? $buffetpicture : ''); ?>" href="<?php echo e(route('dessert-picture-video',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
  
  <a class="<?php echo e(isset($buffetType) ? $buffetType : ''); ?>" href="<?php echo e(route('dessert-type',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Type_Dishes')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Type_Dishes')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Type_Dishes')); ?> <?php endif; ?></a>
  
   <a class="<?php echo e(isset($dessertlistoffer) ? $dessertlistoffer : ''); ?>" href="<?php echo e(route('dessert-list-offer',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Offer')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Offer')); ?> <?php endif; ?></a>

  
<a class="<?php echo e(isset($dessertorders) ? $dessertorders : ''); ?>" href="<?php echo e(route('dessert-order',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($dessertreviewsandcomments) ? $dessertreviewsandcomments : ''); ?>" href="<?php echo e(route('dessert-reviews-and-comments',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

  <?php endif; ?>
</div>
</div>
<?php endif; ?>
<!-- End For Dessert Inner Menu -->


<!-- Sratr For Dates Inner Menu -->
<?php if(isset($dates_inner_leftmenu) && $dates_inner_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$offerid  = request()->offerid;
if(Request::is("dates-info/$idd") || Request::is("dates-info/$idd/$sid")) {$dateinfo = 'active';  }   
if(Request::is("dates-picture-video/$idd/$sid")) {$checkVal=1; $datespicture = 'active';  } 
if(Request::is("buffet-main-cource/$idd")) {$buffetmaincource = 'active';  }
if(Request::is("dates-type/$idd/$sid") || Request::is("dates-add-type/$idd/$sid") || Request::is("dates-add-type/$idd/$sid/$autoid")) {$datetype = 'active';  }

 if(Request::is("dates-order/$idd/$sid")) {$datesorders = 'active';  }
  if(Request::is("dates-reviews-and-comments/$idd/$sid")) {$datescomment = 'active';  }
 

if(Request::is("dates-list-offer/$idd/$sid") || Request::is("dates-add-offer/$idd/$sid") || Request::is("dates-add-offer/$idd/$sid/$offerid")) {$datelistoffer = 'active';  }
 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($dateinfo) ? $dateinfo : ''); ?>" href="<?php echo e(route('dates-info',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>

<?php if($sid!=''): ?>
  <a class="<?php echo e(isset($datespicture) ? $datespicture : ''); ?>" href="<?php echo e(route('dates-picture-video',['id' => request()->id,'sid' => request()->sid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
  
 
  <a class="<?php echo e(isset($datetype) ? $datetype : ''); ?>" href="<?php echo e(route('dates-type',['id' => request()->id,'sid' => request()->sid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Types')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Types')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Types')); ?> <?php endif; ?></a>
  
   <a class="<?php echo e(isset($datelistoffer) ? $datelistoffer : ''); ?>" href="<?php echo e(route('dates-list-offer',['id' => request()->id,'sid' => request()->sid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Offer')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Offer')); ?> <?php endif; ?></a>

  
<a class="<?php echo e(isset($datesorders) ? $datesorders : ''); ?>" href="<?php echo e(route('dates-order',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($datescomment) ? $datescomment : ''); ?>" href="<?php echo e(route('dates-reviews-and-comments',['id' => request()->id,'sid' => request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>
  <?php endif; ?>
</div>
</div>
<?php endif; ?>
<!-- End For Dates Inner Menu -->



<!-- Laser Cosmetic 3 links--> 

<?php if(isset($laser_cosmetic_leftmenu) && $laser_cosmetic_leftmenu==1): ?>
<?php if(Request::is('/laser-cosmetic-shop/4')) {$lasershop = 'active';} 
if(Request::is('laser-cosmetic-shop-branch/4')) {$laserbranch = 'active';} 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>

<div  class="left_panel">
<div class="sidenav 4">

  <a class="<?php echo e(isset($lasershop) ? $lasershop : ''); ?>" href="<?php echo e(route('laser-cosmetic-shop',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Clinic')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Clinic')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Clinic')); ?> <?php endif; ?> </a>
 
</div>
</div>
<?php endif; ?>



<!-- Start For Laser Cosmetic Big Menu -->

<?php if(isset($laserbig_leftmenu) && $laserbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;

$incid = request()->incid;

if(Request::is("laser-cosmetic-shop/$idd")) {$lasershop = 'active';} 

if(Request::is("laser-cosmetic-shop-branch/$idd/$sid") || Request::is("laser-cosmetic-shop-info/$idd/$sid")) {$laserbranch = 'active';} 

if(Request::is("laser-cosmetic-shop-info/$idd/$sid/$autoid")) {$laseshopinfo = 'active';} 

if(Request::is("laser-cosmetic-picture/$idd/$sid/$autoid")) {$checkVal=1; $lasepicture = 'active';} 

if(Request::is("laser-cosmetic-shop-info/$idd/$sid")) {$laseshopinfo = 'active'; }

if(Request::is("laser-cosmetic-shop-info/$idd/$sid/$autoid")) {$laseshopinfo = 'active'; }

if(Request::is("doctors/$idd/$sid/$autoid") || Request::is("add-doctors/$idd/$sid/$autoid") || Request::is("add-doctors/$idd/$sid/$autoid/$incid")) {$doctors = 'active'; }


if(Request::is("laser-cosmetic-reviews-and-comments/$idd/$sid/$autoid")) {$lasereviewsandcomments = 'active'; }

if(Request::is("laser-cosmetic-order/$idd/$sid/$autoid")) {$laseorders = 'active'; }

if(Request::is("laser-offer/$idd/$sid/$autoid") || Request::is("laser-addoffer/$idd/$sid/$autoid") || Request::is("laser-addoffer/$idd/$sid/$autoid/$incid")) {$laseroffer = 'active'; }


?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="<?php echo e(isset($lasershop) ? $lasershop : ''); ?>" href="<?php echo e(route('laser-cosmetic-shop',['id' => request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Clinic')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Clinic')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Clinic')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($laserbranch) ? $laserbranch : ''); ?>" href="<?php echo e(route('laser-cosmetic-shop-branch',['id' =>request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BRANCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BRANCH')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BRANCH')); ?> <?php endif; ?> </a>

  <?php if($autoid!=''): ?>
 <a class="<?php echo e(isset($laseshopinfo) ? $laseshopinfo : ''); ?>" href="<?php echo e(route('laser-cosmetic-shop-info',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($lasepicture) ? $lasepicture : ''); ?>" href="<?php echo e(route('laser-cosmetic-picture',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>

<a class="<?php echo e(isset($doctors) ? $doctors : ''); ?>" href="<?php echo e(route('doctors',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.DRLIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DRLIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.DRLIST')); ?> <?php endif; ?> </a>

  <a class="<?php echo e(isset($laseroffer) ? $laseroffer : ''); ?>" href="<?php echo e(route('laser-offer',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Offer')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Offer')); ?> <?php endif; ?> </a>   
    



 <a class="<?php echo e(isset($lasereviewsandcomments) ? $lasereviewsandcomments : ''); ?>" href="<?php echo e(route('laser-cosmetic-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

<a class="<?php echo e(isset($laseorders) ? $laseorders : ''); ?>" href="<?php echo e(route('laser-cosmetic-order',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>
  
<?php endif; ?>


</div>
</div>
<?php endif; ?>

<!-- End For Laser Cosmetic Big Menu -->



<!-- Skin & Teeth 3 links--> 

<?php if(isset($skin_teeth_leftmenu) && $skin_teeth_leftmenu==1): ?>
<?php if(Request::is('/skin-teeth-shop/4')) {$skinshop = 'active';} 
if(Request::is('skin-teeth-shop-branch/4')) {$skinbranch = 'active';} 
?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>

<div  class="left_panel">
<div class="sidenav 4">

  <a class="<?php echo e(isset($skinshop) ? $skinshop : ''); ?>" href="<?php echo e(route('skin-teeth-shop',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Clinic')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Clinic')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Clinic')); ?> <?php endif; ?> </a>
 



</div>
</div>
<?php endif; ?>



<!-- Start For Skin & Teeth Big Menu -->

<?php if(isset($SkinTeethbig_leftmenu) && $SkinTeethbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$incid = request()->incid;
if(Request::is("skin-teeth-shop-branch/$idd/$sid") || Request::is("skin-teeth-shop-info/$idd/$sid")) {$skinshopbranch = 'active';} 

if(Request::is("skin-teeth-shop-info/$idd/$sid/$autoid")) {$skinshopinfo = 'active';} 

if(Request::is("skin-teeth-doctors/$idd/$sid/$autoid") || Request::is("skin-teeth-add-doctors/$idd/$sid/$autoid") || Request::is("skin-teeth-add-doctors/$idd/$sid/$autoid/$incid")) {$skindoctor = 'active'; }

if(Request::is("skin-teeth-picture/$idd/$sid/$autoid")) {$checkVal=1; $skinpicture = 'active'; }       

if(Request::is("skin-teeth-reviews-and-comments/$idd/$sid/$autoid")) {$skinreview = 'active'; }

if(Request::is("skin-teeth-offer/$idd/$sid/$autoid") || Request::is("skin-addoffer/$idd/$sid/$autoid") || Request::is("skin-addoffer/$idd/$sid/$autoid/$incid")) {$skinoffer = 'active'; }

if(Request::is("skin-teeth-order/$idd/$sid/$autoid")) {$skinorder = 'active'; }

?>   
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
<a class="<?php echo e(isset($skinshop) ? $skinshop : ''); ?>" href="<?php echo e(route('skin-teeth-shop',['id' =>request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.Clinic')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Clinic')); ?>  
<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Clinic')); ?> <?php endif; ?> </a>

 <a class="<?php echo e(isset($skinshopbranch) ? $skinshopbranch : ''); ?>" href="<?php echo e(route('skin-teeth-shop-branch',['id' => request()->id,'sid'=>request()->sid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?> </a>
 
   <?php if($autoid!=''): ?>
 <a class="<?php echo e(isset($skinshopinfo) ? $skinshopinfo : ''); ?>" href="<?php echo e(route('skin-teeth-shop-info',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </a>
  <a class="<?php echo e(isset($skinpicture) ? $skinpicture : ''); ?>" href="<?php echo e(route('skin-teeth-picture',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a>
<a class="<?php echo e(isset($skindoctor) ? $skindoctor : ''); ?>" href="<?php echo e(route('skin-teeth-doctors',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.DRLIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DRLIST')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.DRLIST')); ?> <?php endif; ?> </a>  
    
 <a class="<?php echo e(isset($skinoffer) ? $skinoffer : ''); ?>" href="<?php echo e(route('skin-teeth-offer',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Offer')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Offer')); ?> <?php endif; ?> </a>  


 <a class="<?php echo e(isset($skinreview) ? $skinreview : ''); ?>" href="<?php echo e(route('skin-teeth-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>

<a class="<?php echo e(isset($skinorder) ? $skinorder : ''); ?>" href="<?php echo e(route('skin-teeth-order',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?>  
  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a>

 <?php endif; ?>
</div>
</div>
<?php endif; ?>

<!-- End For Skin & Teeth Big Menu -->
 

<!-- Start For Makeup Menu -->

<?php if(isset($makeup_leftmenu) && $makeup_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
if(Request::is("makeup-shop-info/$idd/$itemid") || Request::is("makeup-shop-info/$idd"))           {     $makeupshopinfo          = 'active';  }
if(Request::is("makeup-picture-video/$idd/$itemid"))      { $checkVal=1;    $makeuppicturevideo      = 'active';  }

if(Request::is("makeup-product/$idd/$itemid") || Request::is("makeup-add-product/$idd/$itemid") || Request::is("makeup-add-product/$idd/$itemid/$autoid") )      {     $makeupproduct      = 'active';  } 



if(Request::is("makeup-order/$idd/$itemid"))      {     $makeuporder      = 'active';  } 
if(Request::is("makeup-offer/$idd/$itemid") || Request::is("makeup-add-offer/$idd/$itemid")  || Request::is("makeup-add-offer/$idd/$itemid/$autoid"))      {     $makeupoffer      = 'active';  } 


if(Request::is("makeup-reviews-comments/$idd/$itemid"))      {     $makeupreviewscomments      = 'active';} 
if(Request::is("makeup-service-category/$idd/$itemid"))      {     $makeupservicecategory      = 'active';} 

if(Request::is("makeup-service-add-category/$idd/$itemid") || Request::is("makeup-service-add-category/$idd/$itemid/$autoid"))      {     $makeupservicecategory      = 'active';} 

?>
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">
  <a class="<?php echo e(isset($makeupshopinfo) ? $makeupshopinfo : ''); ?>" href="<?php echo e(route('makeup-shop-info',['id' => request()->id,'itemid'=>request()->itemid])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </a>
   <?php if($itemid!='' ): ?>
  <a class="<?php echo e(isset($makeuppicturevideo) ? $makeuppicturevideo : ''); ?>" href="<?php echo e(route('makeup-picture-video',['id' => request()->id,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Pictures_Video')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video')); ?> <?php endif; ?> </a> 

  <a class="<?php echo e(isset($makeupservicecategory) ? $makeupservicecategory : ''); ?>" href="<?php echo e(route('makeup-service-category',['id' => request()->id,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CATEGORIES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CATEGORIES')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CATEGORIES')); ?> <?php endif; ?> </a> 
     
  <a class="<?php echo e(isset($makeupproduct) ? $makeupproduct : ''); ?>" href="<?php echo e(route('makeup-product',['id' => request()->id,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.product_list')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.product_list')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.product_list')); ?> <?php endif; ?> </a> 
     
        
  
  <a class="<?php echo e(isset($makeuporder) ? $makeuporder : ''); ?>" href="<?php echo e(route('makeup-order',['id' => request()->id,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order')); ?> <?php endif; ?> </a> 
  
    <a class="<?php echo e(isset($makeupoffer) ? $makeupoffer : ''); ?>" href="<?php echo e(route('makeup-offer',['id' => request()->id,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Offer')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Offer')); ?> <?php endif; ?> </a> 
  
    <a class="<?php echo e(isset($makeupreviewscomments) ? $makeupreviewscomments : ''); ?>" href="<?php echo e(route('makeup-reviews-comments',['id' => request()->id,'itemid'=>request()->itemid])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Reviewandcomments')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments')); ?> <?php endif; ?> </a>  
  
  <?php endif; ?>
  
</div>
</div>
<?php endif; ?>

<!-- End For Makeup Menu -->
 
<div class="overlay"></div>
<!-- Menu Script start -->
<script>
$('.hall-menu-outer').click(function(){
$('.sidenav').slideToggle(500); 
$('.overlay').css('display','block'); 
});
$('.overlay').click(function(){
$('.sidenav').slideUp(500); 
$('.overlay').css('display','none');  
});
</script>

<!-- Menu Script End -->

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>
 <?php if(isset($checkVal) && $checkVal==1): ?>
  
<script>
  $(window).load(function(){

  jQuery('input[name=youtubevideo]').on("focusout", function(e){   
  var url = jQuery('input[name=youtubevideo]').val();
 
 jQuery('.youtubeerror').remove();


 var res = url.split("https://youtu.be/");

    if(res[1]== undefined)
    {
      jQuery('input[name=youtubevideo]').val('');
    jQuery('.ex-show').last().append("<span class='error youtubeerror'><?php if(Lang::has(Session::get('mer_lang_file').'.Please_enter_valid_url')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Please_enter_valid_url')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Please_enter_valid_url')); ?> <?php endif; ?></span>");
    }
    else if(res[1].length != 11)
    {
      jQuery('input[name=youtubevideo]').val('');
    jQuery('.ex-show').last().append("<span class='error youtubeerror'><?php if(Lang::has(Session::get('mer_lang_file').'.Please_enter_valid_url')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Please_enter_valid_url')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Please_enter_valid_url')); ?> <?php endif; ?></span>");

    }
    else
    {
jQuery('.youtubeerror').remove();

    }


  });



 $("input[name='submit']").click(function(e){      
      var aboutVideo = jQuery("#about").val();
      var aboutVideoAr = jQuery("#about_ar").val();
      if($.trim(aboutVideoAr)== '' && $.trim(aboutVideo)== '')
      {
      return true;
      }
      if($.trim(aboutVideoAr)!= '' && $.trim(aboutVideo)!= '')
      {
      return true;
      }
 
      <?php if($mer_selected_lang_code !='ar'): ?>

      if($.trim(aboutVideoAr)== '' )
      {   e.preventDefault();
      $('.arabic_tab').trigger('click');
      }
      if($.trim(aboutVideo)=='')
      {   e.preventDefault();
      $('.english_tab').trigger('click');  
      }

      <?php else: ?>

      if($.trim(aboutVideo)=='')
      {   e.preventDefault();
      $('.english_tab').trigger('click');  
      }
      if($.trim(aboutVideoAr)== '' )
      {   e.preventDefault();
      $('.arabic_tab').trigger('click');
      }


      <?php endif; ?>

 });

 
$('body').on('change', '.info-file', function() {  
        var fileUpload = $(this)[0];   
        var record = $(this);
       var getlab = $(this).data('lbl');            
                var reader = new FileReader();
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                     var image = new Image();
                    image.src = e.target.result;
                    image.onload = function () {
                        var height = this.height;
                        var width = this.width;
                       if (height < 445 && width < 775)
                        {                            
                             <?php if($mer_selected_lang_code !='ar'): ?>
                             $('.pictureformat').html("Please upload image of minimum dimension 776px X 450px (width and height)");
                             <?php else: ?>
                              $('.pictureformat').html("يرجى تحميل صورة الحد الأدنى للبعد 776 بكسل × 450 بكسل (العرض والارتفاع)");
                             <?php endif; ?>
                              $('#'+getlab).html('');
                              record.val(''); 
                            //return false;
                        }

                        if (height > 505 || width > 805)
                        {
                        <?php if($mer_selected_lang_code !='ar'): ?>
                        $('.pictureformat').html("Please upload image of maximum dimension 800px X 500px (width and height)");
                        <?php else: ?>
                        $('.pictureformat').html("يرجى تحميل صورة بالحد الأقصى للبعد 800 بكسل × 500 بكسل (العرض والارتفاع)");
                        <?php endif; ?>
                        $('#'+getlab).html('');
                        record.val(''); 
                        }

                        //return true;
                    };
                }
           
    });
  <?php if($mer_selected_lang_code !='ar'): ?>
   $('.pictureformat').html("<div class='ex-show' style='color:#737373;'>Please upload image between 776px X 450px to 800px X 500px (Width X Height)</div");
    <?php else: ?>
  $('.pictureformat').html("<div class='ex-show' style='color:#737373;'>يرجى تحميل الصورة بين 776 بكسل × 450 بكسل إلى 800 بكسل × 500 بكسل (العرض × الارتفاع)</div");
      <?php endif; ?>
 });
  
</script>
<?php endif; ?>


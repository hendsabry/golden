<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
    <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="inner_wrap"> 

	<div class="search-section">
	<div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
	<?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
	 <!-- search-section -->
    <!-- search-section -->
    <div class="page-left-right-wrapper">
       <?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
   
      <div class="page-right-section">
	  <?php if(count($vendordetails) > 0 ){ ?>
        <div class="diamond-area">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> <?php if($category_id==10){ $foodshop='datesshop'; }else{ $foodshop='foodshopbranch'; } ?>
                <?php  $i=1;     ?>
                <?php  $k=count($vendordetails);  ?>
                <?php if($k<6){ ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $vendordetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $img = str_replace('thumb_','',$getallcats->mc_img); ?>

                  <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a <?php if($getallcats->isproduct!=''){ ?> href="<?php echo e(route('perfumesbranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id])); ?>" <?php } else{ ?> onclick="noproductdata();"; <?php } ?>>
                    <div class="category_wrapper <?php if($k==3): ?> category_wrapper<?php echo e($k); ?> <?php endif; ?>" style="background:url(<?php echo e(isset($img) ? $img : ''); ?>);">
                      <div class="category_title">
                        <div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div>
                      </div>
                    </div>
                    </a> </div>
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 6th-------------->
                <?php }elseif($k==6){ ?>
                <?php $j=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $vendordetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                  <?php if($j==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($j==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($j==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($j==6){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a <?php if($getallcats->isproduct!=''){ ?> href="<?php echo e(route('perfumesbranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id])); ?>" <?php } else{ ?> onclick="noproductdata();"; <?php } ?>>
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($j==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==4){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($j==5){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($j==6){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $j=$j+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 7th-------------->
                <?php }elseif($k==7){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $vendordetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                  
                  <?php if($l==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($l==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($l==7){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a <?php if($getallcats->isproduct!=''){ ?> href="<?php echo e(route('perfumesbranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id])); ?>" <?php } else{ ?> onclick="noproductdata();"; <?php } ?>>
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==7){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 8th-------------->
                <?php }elseif($k==8){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $vendordetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                  <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                  <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                  
                  <?php if($l==1){ $classrd='category_wrapper1'; ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_3of5 rows5row"> <?php } ?> 
                      <?php if($l==4){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?>
                          <?php if($l==8){ $classrd='category_wrapper9'; ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a <?php if($getallcats->isproduct!=''){ ?> href="<?php echo e(route('perfumesbranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id])); ?>" <?php } else{ ?> onclick="noproductdata();"; <?php } ?>>
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==8){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!---------- 9th ------------------->
                <?php }elseif($k==9){ ?>
                <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                  <?php $__currentLoopData = $vendordetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>             
                  
                  <?php if($i==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($i==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($i==4){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($i==7){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?> 
                          <?php if($i==9){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a <?php if($getallcats->isproduct!=''){ ?> href="<?php echo e(route('perfumesbranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id])); ?>" <?php } else{ ?> onclick="noproductdata();"; <?php } ?>> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);"> <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span></span> </span> </a> <?php if($i==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    <?php if($i==9){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?> 
                  
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php } ?> </div>
            </div>
			
          </div>
		  <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
		  <div class="" align="center"> <?php echo e($vendordetails->links()); ?></div>
		<?php }else{ ?>
        <div class="no-record-area" align="center"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div>
        <?php } ?>
        <!-- diamond-area -->
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
<div class="oops_popup">
  <div class="oops_title">Oops!</div>
  <div class="ooops_desciption">You have exceeded your budget! Happens. 
    Simply revisit your plan or increase your budget. </div>
  <a href="javascript:void(0)" class="oops_btn">OK</a> </div>
<!-- oops_popup -->
<div class="services_popup">
  <div class="serv_popup_row">
    <div class="service_popup_title">Taj Hall 1</div>
    <a href="javascript:void(0)" class="serv_pop_close">X</a> </div>
  <!-- serv_popup_row -->
  <div class="serv_popupimg"><img src="<?php echo e(url('')); ?>/themes/images/business.png"></div>
  <div class="serv_popup_row">
    <div class="serv_price">SAR 20,000</div>
    <a href="javascript:void(0)" class="serv_detail_link">View Details</a> </div>
  <!-- servicepopup_toprow -->
</div>
<!-- services_popup -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"><?php if(Lang::has(Session::get('lang_file').'.NO_SERVICES_AVAILABLE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_SERVICES_AVAILABLE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.NO_SERVICES_AVAILABLE')); ?> <?php endif; ?></div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/>
      <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>
  </div>
</div>
<script type="text/javascript">
function noproductdata()
{
 jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
}
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
});
</script>
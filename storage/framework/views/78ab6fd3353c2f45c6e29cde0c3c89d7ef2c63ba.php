<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
    <!-- Display Message after submition -->
    
    <!-- Display Message after submition -->
    <form name="form" id="change_password" method="post" action="<?php echo e(route('change-password-update')); ?>" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

    <div class="myaccount_right">
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')); ?></h1>
      <div class="field_group top_spacing_margin_occas myprofile_page">  
	  <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>      
        <div class="checkout-box">
          <div class="checkout-form">
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.OLD_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.OLD_PASSWORD'): trans($OUR_LANGUAGE.'.OLD_PASSWORD')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="password" maxlength="100" name="old_password" id="old_password" value="<?php echo Input::old('old_password'); ?>" class="t-box" />
                </div>
                <?php if(Session::has('error')): ?><span class="error"><?php echo e(Session::get('error')); ?></span><?php endif; ?>
                <?php if($errors->has('old_password')): ?><span class="error"><?php echo e($errors->first('old_password')); ?></span><?php endif; ?>
				<span id="psserrror"></span>
              </div>
            </div>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.NEW_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.NEW_PASSWORD'): trans($OUR_LANGUAGE.'.NEW_PASSWORD')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="password" maxlength="150" name="new_password" id="new_password" value="<?php echo Input::old('new_password'); ?>" class="t-box" />
                </div>
                <?php if($errors->has('new_password')): ?><span class="error"><?php echo e($errors->first('new_password')); ?></span><?php endif; ?>
              </div>
            </div>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.CONFIRM_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.CONFIRM_PASSWORD'): trans($OUR_LANGUAGE.'.CONFIRM_PASSWORD')); ?></div>
                <div class="checkout-form-bottom">
                  <input type="password" maxlength="150" name="confirm_password" id="confirm_password" value="<?php echo Input::old('confirm_password'); ?>" class="t-box" />
                </div>
                <?php if($errors->has('confirm_password')): ?><span class="error"><?php echo e($errors->first('confirm_password')); ?></span><?php endif; ?>
              </div>
            </div>
              <!-- checkout-form-cell -->
            </div>
			<div class="myprf_btn">
        <input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')); ?>" class="form-btn btn-info-wisitech">
      </div>
            <!-- checkout-form-row -->
          </div>
		  
          <!-- checkout-form -->
        </div>      
		
        <!-- checkout-box -->
      </div>    
      
      </form>      
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script>
jQuery(document).ready(function(){
jQuery.validator.addMethod("laxEmail", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
}, "Please enter valid email address."); 
 jQuery("#change_password").validate({
    rules: {          
          "old_password" : {
            required : true
          },  
          "new_password" : {
            required : true,
            minlength: 8,
          },
          "confirm_password" : {
            required : true,
            equalTo: "#new_password"
          },              
         },
         messages: {
          "old_password": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_OLD_PASSWORD')); ?> <?php endif; ?>"
          },
          "new_password": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD')); ?> <?php endif; ?>",
            minlength:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')); ?> <?php endif; ?>"
          },
          "confirm_password": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')); ?> <?php endif; ?>",
            equalTo:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')); ?> <?php endif; ?>"
          },          
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#change_password").valid()) {        
    jQuery('#change_password').submit();
   }
  });
  
   jQuery("#old_password").blur(function(){
   var str = jQuery('#old_password').val();
        jQuery.ajax({
		   type: 'get',
		   data: 'password='+str,
		   url: '<?php echo url('change-pass-imagewithajax'); ?>',
		   success: function(responseText)
		   {
			jQuery('#psserrror').html(responseText);
			//location.reload();
		  }       
		});
   });
});
</script>
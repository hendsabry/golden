<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
<meta charset="UTF-8" />
<title><?php echo $SITENAME; ?>|
<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_SETTINGS');} ?>
</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="_token" content="<?php echo csrf_token(); ?>"/>
<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
<!-- GLOBAL STYLES -->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/main.css" />
<?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
<?php endif; ?>
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/theme.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
<?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?> ">
<?php endif; ?>
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
<link rel="stylesheet" href="http://wisitech.in/public/assets/css/font-awesome/css/font-awesome.min.css" />
<!--END GLOBAL STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head><!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

<!-- MAIN WRAPPER -->
<div id="wrap"> 
  
  <!-- HEADER SECTION --> 
  <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  <!-- END HEADER SECTION --> 
  <!-- MENU SECTION --> 
  <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  <!--END MENU SECTION -->
  
  <div class=" col-sm-9 col-xs-12 right-side-bar panel_body_content">
    <div id="content">
      <div class="inner setting_content_area">
        <div class="row">
          <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
            <ul class="breadcrumb">
              <li class=""><a href="<?php echo url('')?>/siteadmin_dashboard">
                <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_HOME');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME');} ?>
                </a></li>
              <li class="active"><a >
                <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_SETTINGS');} ?>
                </a></li>
            </ul>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
            <div class="back-btn-area "> <a href="/siteadmin_dashboard" class="profile_back_btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a> </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="box dark">
              <header>
                <div class="icons"><i class="icon-edit"></i></div>
                <h5>
                  <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_SETTINGS');} ?>
                </h5>
              </header>
              <?php if($errors->any()): ?> 
                <div class="alert alert-danger alert-dismissable msg"><?php echo implode('', $errors->all(':message
                  ')); ?>

                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                </div>
              <?php endif; ?>
              <?php if( Session::has('warning')): ?> 
                <div class="alert alert-danger alert-dismissable msg"><?php echo Session::get('warning'); ?>

                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="warning">×</button>
                </div>
              <?php endif; ?>
              
              <?php if(Session::has('success')): ?>
              <div class="alert alert-success alert-dismissable"><?php echo Session::get('success'); ?>

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              </div>
              <?php endif; ?>
              <div id="div-1" class="accordion-body collapse in body content_body small-width-column"> <?php echo Form::open(array('url'=>'admin_settings_submit','class'=>'form-horizontal','id'=>'demo-form2', 'enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

                <?php foreach($admin_setting_details as $admin_get) { }?>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_FIRST_NAME')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_FIRST_NAME');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_FIRST_NAME');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="first_name" class="form-control" value="<?php echo $admin_get->adm_fname; ?>" type="text" required
                    maxlength="20">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_NAME')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_LAST_NAME');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_NAME');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="last_name" class="form-control" value="<?php echo $admin_get->adm_lname; ?>" type="text" required
                    maxlength="20">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_EMAIL');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="email" class="form-control" value="<?php echo $admin_get->adm_email; ?>" type="email" required
                    maxlength="70">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NUMBER');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="phone" class="form-control" value="<?php echo $admin_get->adm_phone; ?>" type="text" maxlength="15" minlength="6"  oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADDRESS1');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="address_one" class="form-control" value="<?php echo $admin_get->adm_address1; ?>" type="text" required
                    maxlength="100">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADDRESS2');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2');} ?>
                  </label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="address_two" class="form-control" value="<?php echo $admin_get->adm_address2 ?>" type="text"s
                    maxlength="100">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_COUNTRY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <select class="form-control" name="country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" required="required">
                      <option value="">--
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SELECT');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT');} ?>
                      --</option>
                      <?php foreach($country_details as $country_fetch){ ?>
                      
                          <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
                          
                      <option value="<?php echo $country_fetch->co_id; ?>"  <?php if($country_fetch->co_id == $admin_get->adm_co_id){ echo 'selected'; } ?> ><?php echo $country_fetch->co_name_ar; ?></option>
                      
                          <?php else: ?>
                          
                      <option value="<?php echo $country_fetch->co_id; ?>"  <?php if($country_fetch->co_id == $admin_get->adm_co_id){ echo 'selected'; } ?> ><?php echo $country_fetch->co_name; ?></option>
                      

                          <?php endif; ?>
           				   
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_CITY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <select class="selectpicker form-control" data-live-search="true" name="city" required="required" id="city">
                      <option disabled value=""><?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_CITY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SELECT_CITY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_CITY');} ?></option>
                      
                           <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                      <option value="<?php echo e($city->ci_id); ?>" <?php if ($city->ci_id == $admin_get->adm_ci_id): ?> selected
                          <?php endif ?>> <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?> <?php echo e($city->ci_name_ar); ?> <?php else: ?> <?php echo e($city->ci_name); ?> <?php endif; ?></option>
                      
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          
                    </select>
                  </div>
                </div>




                <div class="form-group">
                        <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="first-name"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NEW_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NEW_PASSWORD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_NEW_PASSWORD')); ?>

                        </label>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                          <input type="password" id="upassword" name="upassword"  class="form-control col-md-7 col-xs-12" value="" maxlength="20">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD')); ?> 
                        </label>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                          <input type="password" id="cpassword" name="cpassword" class="form-control col-md-7 col-xs-12" value="" maxlength="20">
                        </div>
                      </div>






                
                <div class="form-group">
                  <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="pass1"><span class="text-sub"></span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">


                    <input name="submit" type="submit" class="btn btn-warning btn-sm btn-grad" value="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_UPDATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_UPDATE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_UPDATE')); ?>">

                    
                    <a href="<?php echo url('siteadmin_dashboard'); ?>" style="color:#000" class="btn btn-default btn-sm btn-grad" >
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_BACK')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_BACK');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_BACK');} ?>
                    </a> </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--END PAGE CONTENT --> 
    
  </div>
</div>

<!--END MAIN WRAPPER --> 

<!-- FOOTER --> 
<?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!--END FOOTER --> 

<!-- GLOBAL SCRIPTS --> 
<script src="<?php echo url(''); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script> 
<script>
	$( document ).ready(function() {
			 var passData = 'city_id_ajax=<?php echo $admin_get->adm_ci_id; ?>';
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: {'city_id_ajax':'<?php echo $admin_get->adm_ci_id; ?>','country_id_ajax':'<?php echo $admin_get->adm_co_id; ?>'},
				  url: '<?php echo url('ajax_select_city_edit'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});
	});
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
	}
	</script> 
<script type="text/javascript">
  $(document).on('change','#select_mer_country',function(){
        $.ajax({
          url: "/get_city",
          data: {'ci_con_id':$('#select_mer_country').val(),'flag':'country',<?php if (Session::get('admin_lang_file') == 'admin_ar_lang') {?> 'language':'ar'<?php } else ?>'language':'en' <?php ?>},
          cache: false,
          success: function(html){
            setTimeout(function(){     
                $("#city").html(html);
                }, 500);
          }
        });
    });
</script> 
<script src="<?php echo url(''); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo url(''); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
<!-- END GLOBAL SCRIPTS --> 

<script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script> 
<script type="text/javascript">
  $(document).on('click','#warning',function(){
       $.ajax({
          url: "/session_flush",
          data: {},
          type: "POST",
          cache: false,
          success: function(html){
          }
        });
    });
</script>
</body>
<!-- END BODY -->
</html>
  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">

jQuery("#demo-form2").validate({
                  ignore: [],
                  rules: {
                          upassword: {
                          required: false,
                           minlength: 8
                          },
                           cpassword : {
                             minlength : 8,
                             equalTo : "#upassword"
                           }
                          },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
             
                },
             
           messages: {
         
          upassword: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($OUR_LANGUAGE.'.USER_PASSWORD'); ?>",
          minlength: "<?php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_CHARACTERS'); ?>",
                      },
             cpassword: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.USER_CONFIRM_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_CONFIRM_PASSWORD'): trans($OUR_LANGUAGE.'.USER_CONFIRM_PASSWORD'); ?>",
          minlength: "<?php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_CHARACTERS'); ?>",
        equalTo: "<?php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_THE_SAME_VALUE'); ?>",
                      },
                     
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 

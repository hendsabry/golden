<?php echo $navbar; ?> 
<!-- Navbar ================================================== --> 

<?php $city_det = DB::table('nm_emailsetting')->first(); ?>
 <?php $data = app('App\Help'); ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/themes/css/multiple-select.css" />
<!-- Header End====================================================================== --> 

<?php if(Session::get('lang_file') =='ar_lang'): ?>
<div id="mainBody" class="egis rtl"> <?php else: ?>
  <div id="mainBody" class="egis"> <?php endif; ?> 
    
    <!-- Sidebar ================================================== --> 
    <!-- Sidebar end=============================================== -->
    <div class="register_area">
 
      <ul class="breadcrumb not_here">
        <li><a href="index"><?php if(Lang::has(Session::get('lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="divider">/</span></li>
        <li class="active"><?php if(Lang::has(Session::get('lang_file').'.MERCHANT_SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MERCHANT_SIGN_UP')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MERCHANT_SIGN_UP')); ?> <?php endif; ?></li>
      </ul>
      <?php if(Session::has('result')): ?>
      <div class="alert alert-success alert-dismissable"><?php echo Session::get('result'); ?>

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      </div>
      <?php endif; ?>
      <div class="register_logo"><img src="<?php echo e(url('public/assets/logo/Logo_1517904811_Logo_hfgh.png')); ?>"></div>

     <div class="form-signin">
<div class="lang">	
<div class="lng_text"> <?php if(Session::get('lang_file') == 'ar_lang'): ?> اختار اللغة  <?php else: ?>  Select Language <?php endif; ?></div>
<div class="lnag_sel">

<select name="lang"  onchange="Lang_change(this.value)">
	<option value="ar" <?php if(Session::get('lang_file') == 'ar_lang'): ?> selected  <?php endif; ?>>عربى</option>
	<option value="en" <?php if(Session::get('lang_file') == 'en_lang'): ?> selected  <?php endif; ?>>English</option>

</select>

 </div>
</div></div>
      
      <div class="our_text">
        <div class="own"><?php if(Lang::has(Session::get('lang_file').'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')); ?> <?php endif; ?> </div>
        <p class="wecome_box"><?php if(Lang::has(Session::get('lang_file').'.WELCOME_TO_MERCHANT_SIGN_UP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WELCOME_TO_MERCHANT_SIGN_UP')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.WELCOME_TO_MERCHANT_SIGN_UP')); ?> <?php endif; ?></p>
		<p style="padding:10px; margin:0px; font-size:11pt;">
<?php if(Session::get('lang_file') == 'ar_lang'): ?>  هل لديك حساب بالفعل ؟  <?php else: ?>  Already have an account? <?php endif; ?>

       <a href="<?php echo e(url('/sitemerchant')); ?>" style="color:#95aeb3; text-decoration:underline;"><?php if(Session::get('lang_file') == 'ar_lang'): ?> تسجيل الدخول  <?php else: ?> Sign In <?php endif; ?> </a></p>
      </div>
      <?php if($errors->any()): ?> <br>
      <ul class="message_box_error">
        <div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>
          ')); ?>

          <button type="button" class="close" data-dismiss="alert" aria-hidden="true" ></button>
        </div>
      </ul>
      <?php endif; ?>
      
      <?php if(Session::has('mail_exist')): ?>
      <div class="alert alert-warning alert-dismissable already"><?php echo Session::get('mail_exist'); ?>

        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      </div>
      <?php endif; ?>
      <div class="content"> <?php echo Form::open(array('url'=>'merchant_signup','class'=>'testform ','id'=>'testform','enctype'=>'multipart/form-data','accept-charset' => 'UTF-8')); ?>

        <div class="personal-data signup_arabic">
          <div class="register_form">
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.FIRST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.FIRST_NAME')); ?> <?php endif; ?></label>
                <input type="text" maxlength="100" id="first_name" name="first_name"  class="t_box" value="<?php echo Input::old('first_name'); ?>" tabindex="1"  >
              </div>
              <div class="row_right">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LAST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LAST_NAME')); ?> <?php endif; ?></label>
                <input type="text" id="last_name" maxlength="100" class="t_box" name="last_name" value="<?php echo Input::old('last_name'); ?>"  tabindex="2">
              </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.E-MAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.E-MAIL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.E-MAIL')); ?> <?php endif; ?></label>
                <input type="email" id="email_id" class="t_box " name="email_id" value="<?php echo Input::old('email_id'); ?>"  tabindex="3" onchange="check_email();">
                <div id="email_id_error_msg"  class="error"> </div>
              </div>
              <div class="row_right">
			  
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.CONTACT_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CONTACT_NUMBER')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CONTACT_NUMBER')); ?> <?php endif; ?></label>
				<div class="country_row">
				<div class="signup_rowright">
				<!--<input type="text" id="country_code" maxlength="15" class="t_box contactnumber" name="country_code" value="<?php echo Input::old('country_code'); ?>" onkeypress="return isNumber(event)"  data-error="Country Code">-->
        <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode" required>      
      <?php $__currentLoopData = $getCountry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ccode): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($Ccode->country_code); ?>">+<?php echo e($Ccode->country_code); ?></option>        
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
                <input type="text" id="phone_no" maxlength="15" class="t_box contact contactnumber" name="phone_no" value="<?php echo Input::old('phone_no'); ?>"   tabindex="6" onkeypress="return isNumber(event)"  data-minlength="15" data-maxlength="15" data-error="Less Number">
				</div>
				<label for="country_code" class="error"></label>
				<label for="phone_no" class="error"></label>
				</div>
              </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUNTRY')); ?> <?php endif; ?></label>
                <select class="t_box" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" tabindex="4" >
                  <option value=""> <?php if(Lang::has(Session::get('lang_file').'.SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_COUNTRY')); ?> <?php endif; ?></option>
                  
                           <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                           <?php if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en'): ?> 
                           
                  <?php  $co_name = 'co_name'; ?>
                  
                           <?php else: ?>  
                  <?php  $co_name = 'co_name_'.Session::get('lang_code');  ?>                  
                           <?php endif; ?>
                   
                  <option value="<?php echo e($country_fetch->co_id); ?>" <?php if(Input::old('select_mer_country')==$country_fetch->co_id){ echo "selected"; }?>><?php echo $country_fetch->$co_name; ?></option>
                  
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                </select>
              </div>
              <div class="row_right">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CITY')); ?> <?php endif; ?></label>
                <?php if(Input::old('select_mer_city')): ?>
                <select class="t_box" name="select_mer_city" id="select_mer_city" tabindex="5" >
                  
                           <?php $__currentLoopData = $get_city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php if(Input::old('select_mer_country')==$city->ci_con_id): ?>
                            <?php if($city->ci_name!=''): ?>
                  <option value="<?php echo $city->ci_id;?>" <?php if(Input::old('select_mer_city')==$city->ci_id){ echo "selected";} ?>><?php echo $city->ci_name; ?></option>
                   <?php endif; ?>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                </select>
                <?php else: ?>
                <select class="t_box" name="select_mer_city" id="select_mer_city" tabindex="5" >
                  <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></option>
                </select>
                <?php endif; ?> </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS1')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS1')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS1')); ?> <?php endif; ?></label>
                <input type="text" id="addreess_one" class="t_box" name="addreess_one" value="<?php echo Input::old('addreess_one'); ?>"  tabindex="7">
              </div>
              <div class="row_right">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.ADDRESS2')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ADDRESS2')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ADDRESS2')); ?> <?php endif; ?></label>
                <input type="text" id="address_two" name="address_two" class="t_box" value="<?php echo Input::old('address_two'); ?>"   tabindex="8">
              </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.ZIPCODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ZIPCODE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ZIPCODE')); ?> <?php endif; ?></label>
                <input type="text" id="zip_code" name="zip_code" maxlength="6"  class="t_box zip" value="<?php echo Input::old('zip_code'); ?>" onkeypress="return isNumber(event)" 
                               data-minlength="6" data-maxlength="6" data-error="Less Number">
                <div class="clear"></div>
                <div id="zip_error_msg"></div>
              </div>
              <div class="row_right">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.UPLOADPIC')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UPLOADPIC')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UPLOADPIC')); ?> <?php endif; ?></label>
                <div class="input-file-area">
                  <label for="file">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
                    <div class="file-btn"><?php if(Lang::has(Session::get('lang_file').'.UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UPLOAD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UPLOAD')); ?> <?php endif; ?></div>
                  </div>
                  </label>
                  <input class="info-file" accept="image/*" type="file" placeholder="<?php echo e($STORE_WIDTH); ?> x <?php echo e($STORE_HEIGHT); ?>"  id="file" name="file" value="<?php echo Input::old('file'); ?>" required>
                </div>
              </div>
            </div>
            <div class="services_register signup_arabic">
              <div class="select_service_heading"> <?php if(Lang::has(Session::get('lang_file').'.SelectServices')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SelectServices')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SelectServices')); ?> <?php endif; ?></div>
              <div class="service-section"> <?php $j=1;?>
                <?php $__currentLoopData = $getCatlists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catlists): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>     
                <?php                
                   $par_id = $catlists->mc_id;
                   if($par_id == '1'){ continue;}
                $SecondCats=Helper::getChildCats($par_id);
                ?>
                
                <?php $__currentLoopData = $SecondCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Seccatlists): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                $DataId = $Seccatlists->mc_id; $i=1;
                ?>
                <div class="select_service_section">
                  <div class="meta_area">
                    <div class="meta_block secondlabel">
                      <label class="numer_text"> <?php if(Config::get('app.locale') == 'ar'): ?> 
                        <?php echo e($j); ?> &nbsp;<?php echo e($Seccatlists->mc_name_ar); ?>

                        <?php else: ?> 
                        <?php echo e($j); ?> &nbsp;   <?php echo e($Seccatlists->mc_name); ?>

                        <?php endif; ?> </label>
                    </div>
                    <select id="ms_<?php echo e($i); ?>" class="multicheck" multiple="multiple" required name="services[]">
                 <?php   $NotCat = array('87','37');  ?>
                      <?php                
                       $par_id = $Seccatlists->mc_id;
                      $ThirdCats=Helper::getChildCats($par_id);
                      ?>

                  <?php if(count($ThirdCats) >=1 && !in_array($par_id, $NotCat)): ?>

                      <?php $__currentLoopData = $ThirdCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $thicatlists): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php
                        $DataIds = $thicatlists->mc_id;
                      ?>
                      
                      <option value="<?php echo e($thicatlists->mc_id); ?>">   <?php if(Config::get('app.locale') == 'ar'): ?>  <?php echo e($thicatlists->mc_name_ar); ?> <?php else: ?> <?php echo e($thicatlists->mc_name); ?> <?php endif; ?> </option>
                       
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>

                    
                      <option value="<?php echo e($Seccatlists->mc_id); ?>"><?php if(Config::get('app.locale') == 'ar'): ?>  <?php echo e($Seccatlists->mc_name_ar); ?> <?php else: ?> <?php echo e($Seccatlists->mc_name); ?> <?php endif; ?> </option>
                       

                    <?php endif; ?>


                      
                    </select>
                  </div>
                </div>
                <?php  $i=$i+1;   $j=$j+1; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
              <div class="row_area">
                <div class="row_left">
                  <label for="text1" class="numer_text">
<?php if(Lang::has(Session::get('lang_file').'.CertificateChamber')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.CertificateChamber')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.CertificateChamber')); ?> <?php endif; ?>
<?php if(Session::get('lang_file') == 'ar_lang'): ?>

<span style="font-size: 10pt; font-weight: normal;">(ملف PDF / الصورة فقط)</span>
<?php else: ?>
<span style="font-size: 10pt; font-weight: normal;">(Only PDF / Image file)</span>
<?php endif; ?>


                  </label>
                  <div class="input-file-area">
                    <label for="file1">
                    <div class="file-btn-area">
                      <div id="file_value2" class="file-value"></div>
                      <div class="file-btn"><?php if(Lang::has(Session::get('lang_file').'.UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UPLOAD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.UPLOAD')); ?> <?php endif; ?></div>
                    </div>
                    </label>
                    <input class="info-file" required="true" type="file" id="file1" name="file1" >
                  </div>
                  <span class="error certifications"></span>
                </div>

                <div class="row_right">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.Shop_link_in_maroof')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Shop_link_in_maroof')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Shop_link_in_maroof')); ?> <?php endif; ?></label>
                <div class="input-file-area">
                 <input type="text" id="shop_link_in_maroof" name="shop_link_in_maroof" class="t_box" maxlength="250">
                </div>
              </div>

              </div>

    <div class="row_area">
 
   <div class="row_left">
                <label for="text1" class="label_text"><?php if(Lang::has(Session::get('lang_file').'.sales_rep_code')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.sales_rep_code')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.sales_rep_code')); ?> <?php endif; ?></label>
                <div class="input-file-area">
                 <input type="text" id="sales_rep_code" name="sales_rep_code" class="t_box" maxlength="80">
                </div>
              </div>

</div>

            </div>
          </div>
        </div>
      </div>
      <?php echo Form::close(); ?> </div>
  </div>
</div>



<!-- Placed at the end of the document so the pages load faster ============================================= --> 
<script src="<?php echo e(url('')); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script> 
<script src="<?php //echo url('');?>/themes/js/jquery.steps.js"></script> 
<script src="<?php echo e(url('')); ?>/themes/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo e(url('')); ?>/themes/js/bootshop.js"></script> 
<!--    <script src="<?php // echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script>--> 

<script>
   $( document ).ready(function() {
    $('#zip_code').keypress(function (q){
          if(q.which!=8 && q.which!=0 && q.which!=13 && (q.which<48 || q.which>57))
    {
              originalprice.css('border', '1px solid red'); 
      $('#zip_error_msg').html('<?php if (Lang::has(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED')!= '') { echo  trans(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED');}  else { echo trans($OUR_LANGUAGE.'.NUMBERS_ONLY_ALLOWED');} ?>');
      originalprice.focus();
      return false;
          }
    else
    {     
              originalprice.css('border', ''); 
      $('#zip_error_msg').html('');         
    }
          });
   
    
    $('.close').click(function() {
      $('.alert').hide();
    });
   $('#submit').click(function() {



      var file       = $('#file');
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
          if(file.val() == "")
      {
      file.focus();
    file.css('border', '1px solid red');    
    return false;
    }     
    else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {         
    file.focus();
    file.css('border', '1px solid red');    
    return false;
    }     
    else
    {
    file.css('border', '');         
    }
   });
   });
</script> 

<script>
   function select_city_ajax(city_id)
   {
     var passData = 'city_id='+city_id;
     //alert(passData);
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('ajax_select_city'); ?>',
          success: function(responseText){  
         // alert(responseText);
           if(responseText)
           { 
          $('#select_city').html(responseText);            
           }
        }   
      });   
   }
   
   function select_mer_city_ajax(city_id)
   {
     var passData = 'city_id='+city_id;
    // alert(passData);
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('ajax_select_city'); ?>',
          success: function(responseText){  
         // alert(responseText);
           if(responseText)
           { 
          $('#select_mer_city').html(responseText);            
           }
        }   
      }); 
   }
</script> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"></script> 
<script src="<?php echo e(url('')); ?>/themes/js/simpleform.min.js"></script> 

<script type="text/javascript">
   $(".testform").simpleform({
    speed : 500,
    transition : 'fade',
    progressBar : true,
    showProgressText : true,
    validate: true
   });
   
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }
   
   /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

 

   function validateForm(formID, Obj){
   
    switch(formID){
      case 'testform' :
        Obj.validate({
          rules: {

                first_name: {
                required: true
                },
                last_name: {
                required: true
                },

                email_id: {
                required: true
                //email_id: true
                },
				
                country_code: {
                required: true
                },

                phone_no: {
                required: true
                },

                select_mer_country: {
                required: true
                },
                select_mer_city: {
                required: true,

                },       
                addreess_one: {
                required: true
                },        


                zip_code: {
                required: true
                },

                file: {
                required: true
                },       

 
                file1: {
               	 required: function(element){
            		return $("#shop_link_in_maroof").val()=="";
        			}
                },       
			 shop_link_in_maroof: {
               	 required: function(element){
            		return $("#file1").val()=="";
        			}
                },  
            
            
          },
          messages: {
            email: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?> <?php endif; ?>",
              email: "<?php if(Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')); ?> <?php endif; ?>"
            },

        file1: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.VALIDFILEPDF')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VALIDFILEPDF')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VALIDFILEPDF')); ?> <?php endif; ?>",
              accept: "<?php if(Lang::has(Session::get('lang_file').'.VALIDFILEPDF')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VALIDFILEPDF')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VALIDFILEPDF')); ?> <?php endif; ?>"
            },
		shop_link_in_maroof: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.VALIDSHOPLINK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VALIDSHOPLINK')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VALIDSHOPLINK')); ?> <?php endif; ?>",
              accept: "<?php if(Lang::has(Session::get('lang_file').'.VALIDSHOPLINK')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VALIDSHOPLINK')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.VALIDSHOPLINK')); ?> <?php endif; ?>"
            },
             
            zip_code: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ZIPCODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ZIPCODE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ZIPCODE')); ?> <?php endif; ?>"
            },
             
            website: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_WEBSITE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_WEBSITE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_WEBSITE')); ?> <?php endif; ?>"
            },
            location: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_LOCATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_LOCATION')); ?> <?php endif; ?>"
            },
            
            commission: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_COMMISSION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_COMMISSION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_COMMISSION')); ?> <?php endif; ?>"
            },
            file: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')); ?> <?php endif; ?>"
            },
            select_country: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')); ?> <?php endif; ?>"
            },
            select_city: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_CITY')); ?> <?php endif; ?>"
            },
            
            email_id: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?> <?php endif; ?>",
              email: "<?php if(Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')); ?> <?php endif; ?>"
            },
            first_name: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?> <?php endif; ?>"
            },
            select_mer_city: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_CITY')); ?> <?php endif; ?>",
              
            },
            addreess_one: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')); ?> <?php endif; ?>"
            },
                   
            last_name: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_LAST_NAME')); ?> <?php endif; ?>"
            },
            select_mer_country: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')); ?> <?php endif; ?>"
            },
			country_code: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.COUNTRY_CODE_MSG')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY_CODE_MSG')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COUNTRY_CODE_MSG')); ?> <?php endif; ?>"
            },
            phone_no: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_PHONE_NO')); ?> <?php endif; ?>"
            },
            address_two: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')); ?> <?php endif; ?>"
            }
            
          }
        });
      return Obj.valid();
      break;
   
      case 'testform2' :
        Obj.validate({
          rules: {
            email_id: {
              required: true,
             // email_id: true
            },
            first_name: {
              required: true
            },
            select_mer_city: {
              required: true,
              
            },
            addreess_one: {
              required: true
            },
                     
            last_name: {
              required: true
            },
            select_mer_country: {
              required: true
            },
            phone_no: {
              required: true
            },
            address_two: {
              required: true
            }
          },
          messages: {
            email_id: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')); ?> <?php endif; ?>",
              email: "<?php if(Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')); ?> <?php endif; ?>"
            },
            first_name: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_FIRST_NAME')); ?> <?php endif; ?>"
            },
            select_mer_city: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_CITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_CITY')); ?> <?php endif; ?>",
              
            },
            addreess_one: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')); ?> <?php endif; ?>"
            },
            payment_account: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_PAYMENT_ACCOUNT')); ?> <?php endif; ?>"
            },
            
            last_name: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_LAST_NAME')); ?> <?php endif; ?>"
            },
            select_mer_country: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')); ?> <?php endif; ?>"
            },
            phone_no: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_PHONE_NO')); ?> <?php endif; ?>"
            },
            address_two: {
              required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')); ?> <?php endif; ?>"
            }
          }
        });
      return Obj.valid();
      break;
    }
   }
</script> 

<script type="text/javascript">
   $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script> 

<script src="<?php echo e(url('')); ?>/themes/js/jquery.min.js"></script> 
<script src="<?php echo e(url('')); ?>/themes/js/multiple-select.js"></script> 
<?php if(Session::get('lang_file') =='ar_lang'): ?> 

<script>
    $(function() {
        $('#ms_1,#ms_2,#ms_3,#ms_4,#ms_5,#ms_6,#ms_7,#ms_8,#ms_9,#ms_10').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });

          

    });
</script>
 
<?php else: ?> 
<script>
    $(function() {
        $('#ms_1,#ms_2,#ms_3,#ms_4,#ms_5,#ms_6,#ms_7,#ms_8,#ms_9,#ms_10').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });

          

    });
</script> 
<?php endif; ?> 

 

<script>
jQuery("#file").change(function(){
   var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value1").html(fake);
});
jQuery("#file1").change(function(){
   var fake = this.value.replace("C:\\fakepath\\", "");   


 var fileExtension = ['pdf','jpeg', 'jpg', 'png', 'gif', 'bmp'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   jQuery("#file_value2").html('');         
   jQuery("#file1").val('');   
jQuery(".certifications").html('');

         }
         else
         {
         jQuery("#file_value2").html(fake); 
         }



});




</script> 
  
<script type="text/javascript">
  function Lang_change(str) 
  { 
    var language_code = str;
    var token =  <?php csrf_token(); ?>
    jQuery.ajax
    ({
      type:'GET',
            url:"<?php echo url('new_change_languages');?>",
            data:{'Language_change':language_code,'csrf-token':token},
            success:function(data)
      {
        //alert(data);
        window.location.reload();
            }
        });
  }
</script>

<?php if(Session::get('lang_file') =='ar_lang'): ?> 
<script>
  jQuery(window).load(function(){
jQuery("#submit-button").val('خضع');
jQuery(".bottom_sign").html('هل لديك حساب؟    <a href="sitemerchant">تسجيل الدخول   </a>');


  })
</script> 
<?php endif; ?>
<div class="login_bottom"></div>
</body></html>
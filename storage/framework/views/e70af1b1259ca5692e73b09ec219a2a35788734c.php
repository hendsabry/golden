<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper diamond_fullwidth">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="inner_wrap"> 
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
      <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </div>
    <!-- search-section -->
    <!-- search-section -->
    <div class="page-left-right-wrapper">
      <?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   
      <div class="page-right-section">
        
		<div class="diamond_main_wrapper"> <?php if(Session::get('lang_file')!='en_lang'): ?>
		 <img src="<?php echo e(url('')); ?>/themes/images/diamond_ar_7.jpg" alt="" usemap="#hall-subcategory" hidefocus="true"> 
		 <?php else: ?> 
		 <img src="<?php echo e(url('')); ?>/themes/images/diamond_7.jpg" alt="" usemap="#hall-subcategory"> 
		 <?php endif; ?>
          <map name="hall-subcategory" id="hall-subcategory">
                <area shape="poly" coords="572,112,469,12,299,178,292,188,295,189,648,190" href="<?php echo e(url('')); ?>/tailerslist/31/32" />
				<area shape="poly" coords="108,372,835,374,926,466,833,551,434,552,103,550,12,465" href="<?php echo e(url('')); ?>/perfumeslist/31/94" />
				<area shape="poly" coords="283,555,830,555,767,621,654,733,560,733,282,732,105,555" href="<?php echo e(url('')); ?>/abayalist/31/36" />
				<area shape="poly" coords="652,736,283,734,280,732,290,741,384,833,470,920" href="<?php echo e(url('')); ?>/jewelerylist/31/95" />
				<area shape="poly" coords="649,192,288,192,108,369,109,371,830,371,826,371" href="<?php echo e(url('')); ?>/dresseslist/31/34" />
          </map>
        </div>
		
        <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
		
		
        <!-- diamond-area -->
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
<div class="oops_popup">
  <div class="oops_title">Oops!</div>
  <div class="ooops_desciption">You have exceeded your budget! Happens. 
    Simply revisit your plan or increase your budget. </div>
  <a href="javascript:void(0)" class="oops_btn">OK</a> </div>
<!-- oops_popup -->
<div class="services_popup">
  <div class="serv_popup_row">
    <div class="service_popup_title">Taj Hall 1</div>
    <a href="javascript:void(0)" class="serv_pop_close">X</a> </div>
  <!-- serv_popup_row -->
  <div class="serv_popupimg"><img src="<?php echo e(url('')); ?>/themes/images/business.png"></div>
  <div class="serv_popup_row">
    <div class="serv_price">SAR 20,000</div>
    <a href="javascript:void(0)" class="serv_detail_link">View Details</a> </div>
  <!-- servicepopup_toprow -->
</div>
<!-- services_popup -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $Tailorbig_leftmenu =1; ?>
<!--start merachant-->
<div class="merchant_vendor"> 
<?php
  $id                = $id;
  $opid              = $opid;
  $oid               = $oid;
  $cusid             = $cusid;
  $hid               = $hid;
  $prodid            = $prodid;
  $ordid=$oid;
  $getCustomer       = Helper::getuserinfo($cusid);
  $shipaddress       = Helper::getgenralinfo($oid);  
  $cityrecord        = Helper::getcity($shipaddress->shipping_city);
  $getorderedproduct = Helper::getorderedproduct($oid);
?>
  <!--left panel-->
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!--left panel end-->
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order_Detail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order_Detail')); ?> <?php endif; ?>
        <a href="<?php echo e(url('/')); ?>/tailor-order/<?php echo e($id); ?>/<?php echo e($prodid); ?>" class="order-back-page"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BACK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?> <?php endif; ?></a></h5></div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      <?php //echo '<pre>';print_r($getCustomer);die; ?>
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDRESS')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} ?> <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="<?php echo e(url('')); ?>/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?></label>
                    <div class="info100" > <?php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      ?> </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} ?></div>
                  </div>
                  <?php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ ?>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.transaction_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.transaction_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.transaction_id')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} ?></div>
                  </div>
                  <?php } ?>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD')); ?> <?php endif; ?></label>
                    <div class="info100"><?php echo e($productdata[0]->shippingMethod); ?> </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE')); ?> <?php endif; ?></label>
                    <div class="info100"> SAR  <?php echo e($productdata[0]->shippingPrice); ?></div>
                    <!-- box -->
                  </div>
          <?php         $getsearchedproduct = Helper::searchorderedDetails($oid);
  if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ ?>
                    <!--<div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONDATE')); ?> <?php endif; ?></label>
                    <div class="info100"> <?php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                      echo $getsearchedproduct->occasion_date;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      ?> </div>
                  </div>
                  <?php } ?>
                </div>-->
                <!--global end-->
              </div>
              <!-- hall-od-top -->
			  <div class="tailor_product">
                <div class="tailorpro-heading"><?php if(Lang::has(Session::get('mer_lang_file').'.PRODUCT_INFORMATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PRODUCT_INFORMATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PRODUCT_INFORMATION')); ?> <?php endif; ?></div>
              <?php 
              $basetotal = 0;
              $i = 1;
               $couponcode=0; $couponcodeinfo='';
              if(count($productdata) > 0){
              foreach($productdata as $val)
              {
              $basetotal            = ($basetotal+$val->total_price);
              $pid                  = $val->product_id;
              $serviceInfo          = Helper::getProduckInfo($val->product_id);		  
              if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}

              $couponcode=$couponcode+$val->coupon_code_amount;;
        $couponcodeinfo=$val->coupon_code;
$lan=Session::get('mer_lang_file');
        $fabinfo=Helper::getfabricnamefororder($val->fabric_id,$lan);
              ?>   
                   
                <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                  <div class="style_area">
                    <div class="style_head"><?php if(Lang::has(Session::get('mer_lang_file').'.STYLE_AND_FABRIC_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.STYLE_AND_FABRIC_INFO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.STYLE_AND_FABRIC_INFO')); ?> <?php endif; ?></div>
                    <div class="style_box_type">
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.STYLE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.STYLE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.STYLE')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <?php echo $serviceInfo->pro_title; ?></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.STYLE_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.STYLE_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.STYLE_IMAGE')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.STYLE_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.STYLE_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.STYLE_PRICE')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> SAR <?php echo $getPrice; ?></div>
                        </div>
                      </div>
					  <?php if(isset($val->fabric_name) && $val->fabric_name!=''){ ?>
                      <div class="sts_box">
					    <?php if(isset($val->fabric_name) && $val->fabric_name!=''){ ?>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.FABRICS_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.FABRICS_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.FABRICS_NAME')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <?php echo $fabinfo->fabric_name; ?></div>
                        </div>
						<?php } if(isset($val->fabric_img) && $val->fabric_img!=''){ ?>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.FABRICS_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.FABRICS_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.FABRICS_IMAGE')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <img src="<?php echo $val->fabric_img; ?>" width="80" height="80"/></div>
                        </div>
						<?php } if(isset($val->fabric_price) && $val->fabric_price!=''){ ?>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.FABRICS_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.FABRICS_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.FABRICS_PRICE')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> SAR <?php echo $val->fabric_price; ?></div>
                        </div>
						<?php }  ?>
                      </div>
					  <?php } ?>
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.QUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <?php echo $val->quantity; ?></div>
                        </div>
                        <div class="style_right">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.DELIVERY_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DELIVERY_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DELIVERY_DATE')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> 
						  <?php 
							$date = $val->created_at; //existing date

							if(isset($serviceInfo->deliver_day) && $serviceInfo->deliver_day!=''){
							if($serviceInfo->deliver_day==1){$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' day'));}else{$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' days'));}
							echo $newDate;
							}
						  ?> 
						  </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="style_area">
                    <div class="style_head"><?php if(Lang::has(Session::get('mer_lang_file').'.BODY_MEASUREMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BODY_MEASUREMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BODY_MEASUREMENT')); ?> <?php endif; ?></div>
                    <div class="style_box_type">
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.LENGTH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LENGTH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LENGTH')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->length; ?><?php if(Lang::has(Session::get('mer_lang_file').'.CM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CM')); ?> <?php endif; ?></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.CHEST_SIZE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CHEST_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CHEST_SIZE')); ?> <?php endif; ?> </div>
                          <div class="style_label_text"><?php echo $val->bookingdetail->chest_size; ?><?php if(Lang::has(Session::get('mer_lang_file').'.CM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CM')); ?> <?php endif; ?></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.WAIST_SIZE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WAIST_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.WAIST_SIZE')); ?> <?php endif; ?> </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->waistsize; ?><?php if(Lang::has(Session::get('mer_lang_file').'.CM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CM')); ?> <?php endif; ?></div>
                        </div>
                      </div>
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.SHOULDERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SHOULDERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SHOULDERS')); ?> <?php endif; ?> </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->soulders; ?><?php if(Lang::has(Session::get('mer_lang_file').'.CM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CM')); ?> <?php endif; ?></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.NECK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NECK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.NECK')); ?> <?php endif; ?> </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->neck; ?><?php if(Lang::has(Session::get('mer_lang_file').'.CM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CM')); ?> <?php endif; ?></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ARM_LENGTH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ARM_LENGTH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ARM_LENGTH')); ?> <?php endif; ?> </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->arm_length; ?><?php if(Lang::has(Session::get('mer_lang_file').'.CM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CM')); ?> <?php endif; ?></div>
                        </div>
                      </div>
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.WRIST_DIAMETER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WRIST_DIAMETER')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.WRIST_DIAMETER')); ?> <?php endif; ?> </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->wrist_diameter; ?><?php if(Lang::has(Session::get('mer_lang_file').'.CM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CM')); ?> <?php endif; ?></div>
                        </div>
                        <!--<div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ARM_LENGTH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ARM_LENGTH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ARM_LENGTH')); ?> <?php endif; ?> </div>
                          <div class="style_label_text"> 20</div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">Cutting</div>
                          <div class="style_label_text"> 20</div>
                        </div>-->
                      </div>
                    </div>
                  </div>
                </div>             
              <?php $i++;} ?>
              <div class="merchant-order-total-area">
                <div class="merchant-order-total-line">


                   <?php if(isset($couponcode) && $couponcode>=1): ?>
             <?php echo e((Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')); ?>: 

             <?php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          ?>  
          <br>

          <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')); ?>: <?php echo e($couponcodeinfo); ?>

 <br>
          <?php endif; ?>



            <?php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             ?>

          <?php echo e((Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')); ?>: 
          <?php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); ?>

          <?php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          ?> 



                 </div>
				  <div class="merchant-order-total-line">
                <?php echo e((Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($MER_OUR_LANGUAGE.'.TOTAL_PRICE')); ?>:
                <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);
				 } ?>
              </div></div>
              <?php } ?>
			   </div>
            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
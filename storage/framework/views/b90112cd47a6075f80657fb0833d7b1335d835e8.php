<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $wallet_inner_leftmenu =1; ?>
<div class="merchant_vendor">
<?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
 <div class="inner">
      <header>
    
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_MyWallet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_MyWallet')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_MyWallet')); ?> <?php endif; ?></h5>
       
      </header>
     
      
       <!-- Display Message after submition --> 
            <?php if(Session::has('message')): ?>
            <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
            <?php endif; ?> 
            <!-- Display Message after submition -->
      <div class="global_area">
	  <div class="row">
        <div class="col-lg-12">
          <div class="box">             

              <form name="form1" id="updatewallet" method="post" action="<?php echo e(route('updateWallet')); ?>">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">
                   <?php if(Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')); ?> <?php endif; ?>
                  </label>
                  <div class="info100">              
                    <input type="text"  name="amount" onkeypress="return isNumber(event)"  maxlength="9" id="amount" required="" >
                  </div>
                  </div>
          		</div> <!-- form_row -->
                <div class="form_row"> 
				<div class="form_row_left">
                  <input type="submit" name="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.Pay_Now')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Pay_Now')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Pay_Now')); ?> <?php endif; ?>"> 
				  </div>
                </div> 
              </form>

          </div>
        </div>
      </div>
	  </div>
    </div>
  </div>
</div>
</div>
<!-- merchant_vendor --> 
  
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#updatewallet").validate({
                  ignore: [],
                  rules: {
                  amount: {
                       required: true,
                      },                       
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             amount: {
         required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_AMOUNT')); ?> <?php endif; ?>",
                      },  
 
                                                  
                     
                },
                invalidHandler: function(e, validation){
                var valdata=validation.invalid;
                     },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $lang = Session::get('lang_file'); ?>
<div class="action_popup">
  <div class="action_active_popup">
  <!--<div class="close_rent">X</div>-->
    <div class="action_content add_pop"><span class="succ_addd"><?php echo e(Session::get('status')); ?></span><span id="showmsg" style="display:none;"><?php echo e((Lang::has(Session::get('lang_file').'.SORRY_QTY_PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.SORRY_QTY_PRODUCT'): trans($OUR_LANGUAGE.'.SORRY_QTY_PRODUCT')); ?></span></div>
    <div class="action_btnrow bdr_top_pop" align="center"><input type="hidden" id="delid" value=""/>
      <span id="hidemsgab"><a class="action_yes status_yes contis" href="javascript:void(0);"> <?php if(Lang::has($lang.'.CONTINUE_SHOPPING')!= ''): ?> <?php echo e(trans($lang.'.CONTINUE_SHOPPING')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.CONTINUE_SHOPPING')); ?> <?php endif; ?> </a> <a class="action_yes yes" href="<?php echo e(url('mycart')); ?>"> <?php if(Lang::has($lang.'.VIEW_CART')!= ''): ?> <?php echo e(trans($lang.'.VIEW_CART')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.VIEW_CART')); ?> <?php endif; ?></a></span>
	  <span id="showmsgab" style="display:none;"><a class="action_yes status_yes yes" href="javascript:void(0);"> <?php if(Lang::has($lang.'.OK')!= ''): ?> <?php echo e(trans($lang.'.OK')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.OK')); ?> <?php endif; ?> </a></span>
	  </div>
  </div>
</div>
       
<?php if(Session::has('status')): ?>
<script type="text/javascript">
jQuery(document).ready(function()
{
 jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
});
jQuery('.overlay, .close_rent').click(function(){
jQuery('.action_popup').fadeOut(500);
 jQuery('.overlay').fadeOut(500);
 jQuery('.action_popup').hide();
})


</script>
<?php endif; ?>
<script type="text/javascript">
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
 jQuery('html, body').animate({
	scrollTop: (jQuery('.service-display-left').first().offset().top)
 },500);
});
</script>


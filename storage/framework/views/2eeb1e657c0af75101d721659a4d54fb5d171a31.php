<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor">  
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
   <div class="service_listingrow">
             <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_REPORTING')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_REPORTING')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_REPORTING')); ?> <?php endif; ?></h5>
			</div> 
<?php echo Form::open(array('url'=>"order-reporting",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?> 

        <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
        <div class="filter_area">
        <div class="order-filter-line">
         
        <div class="of-date-box studio-date-box">
           <input name="dateFrom"  class="cal-t"  placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.DateFrom')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DateFrom')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DateFrom')); ?> <?php endif; ?>" id="dateFrom"  type="text"  maxlength="12"   value="<?php echo e(request()->dateFrom); ?>" /> 
       
         <input name="dateTo" id="dateTo" type="text" maxlength="12" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.DateTo')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DateTo')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DateTo')); ?> <?php endif; ?>"  class="cal-t studio-date" value="<?php echo e(request()->dateTo); ?>" />
         <span for="dateTo" generated="true" class="error" id="todata"> </span>

         <input name="" value="<?php if(Lang::has(Session::get('mer_lang_file').'.Apply')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Apply')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Apply')); ?> <?php endif; ?>" class="applu_bts" type="submit">
        </div>
        </div>
        </div>
      <!-- filter_area -->   
      <?php echo Form::close(); ?>

  <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
<div>&nbsp;</div>
  <div class="global_area">
	<div class="row">
	<div class="col-lg-12">
	
	
	<div id="chart-div"></div>
	<?php echo \Lava::render('DonutChart', 'IMDB', 'chart-div'); ?>
	<!--PAGE CONTENT PART WILL COME INSIDE IT END-->
	 
  <div class="clear"><br/></div>

<?php echo Form::open(array('url'=>"order-reporting",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?> 
<input type="hidden" name="dateFrom" value="<?php echo e(request()->dateFrom); ?>">
<input type="hidden" name="dateTo" value="<?php echo e(request()->dateTo); ?>">
<div class="order_page_select" style="max-width: 200px; width: 100%; float: left;">
<select name="year"  onchange="submit(this.value)"> 
	<option value="">Year</option>
	<?php  for($i=2018;$i<=2025;$i++){ ?>
	<option value="<?php echo e($i); ?>" <?php if($year== $i): ?> <?php echo e('Selected'); ?>   <?php endif; ?>><?php echo e($i); ?></option>
 <?php  } ?>
</select>
</div>
<form >
  <div class="clear"><br/></div>

 <div id="perf_div" style="overflow: hidden; clear: both; width: 100%;"></div>
	<?php echo \Lava::render('ColumnChart', 'Finances', 'perf_div'); ?>

	</div>
	</div>
 
</div>
</div>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         <?php if($mer_selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        <?php else: ?>
        $('#todata').html("End date should be greater than From date");
        <?php endif; ?>
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>     
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

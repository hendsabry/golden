<?php $__env->startSection('content'); ?>
    <?php
    if(Session::has('customerdata.user_id'))
    {
        $nuserid  = Session::get('customerdata.user_id');
        $getnInfo = Helper::getuserinfo($nuserid);
        if($getnInfo->cus_name==''){ $cuname='';}else{ $cuname=$getnInfo->cus_name;}
    }
    ?>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2>Sound SystemShop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>
                                    <?php
                                    $name = 'name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $name = 'name_ar';
                                    }
                                    echo $singerDetails->$name;
                                    ?>
                                </h2>
                                <h2>
                                    <?php
                                    $address = 'address';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $address = 'address_ar';
                                    }
                                    echo $singerDetails->$address;
                                    ?>
                                </h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($singerDetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>
                                <?php
                                        ?>
                                <?php $about='about'?>
                                <?php if(Session::get('lang_file')!='en_lang'): ?>
                                    <?php $about= 'about_ar'; ?>
                                <?php endif; ?>
                                <?php echo nl2br($singerDetails->$about); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                        <?php
                            $long  = @$singerDetails->longitude;
                            $lat = @$singerDetails->latitude;
                        ?>


                        <?php if($long !='' && $lat!=''): ?>
                            <input type="hidden" id="long" value="<?php echo e($long); ?>">
                            <input type="hidden" id="lat" value="<?php echo e($lat); ?>">
                            <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;"> </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?>
                                            <?php for($i = 0 ; $i < $val->ratings ; $i++ ): ?>
                                                <i class="fa fa-star"></i>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php echo e($allreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                <?php
                    if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){
                    $mc_name = 'video_description';
                        if(Session::get('lang_file')!='en_lang')
                        {
                          $mc_name = 'video_description_ar';
                        }
                        echo $singerDetails->$mc_name;
                        }
                        ?>
                    <div class="p-15 box"><iframe width="100%" height="315" src="<?php echo e($singerDetails->video_url); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li>
                            <div class="image"><a href="<?php echo e(route('acousticequipment',[$id,$sid,$lid])); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/sound-system1.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('acousticequipment',[$id,$sid,$lid])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.EQUIPMENT')!= '')  ?  trans(Session::get('lang_file').'.EQUIPMENT'): trans($OUR_LANGUAGE.'.EQUIPMENT')); ?></a></h2>
                        </li>
                        <li class="active">
                            <div class="image"><a href="<?php echo e(route('acousticrecording',[$id,$sid,$lid])); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/Recording1.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('acousticrecording',[$id,$sid,$lid])); ?>" class="select"><?php echo e((Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')); ?></a></h2>
                        </li>
                    </ul>
                </center>

                <div class="box p-15">
                    <?php echo Form::open(array('url'=>"insert_acoustic",'class'=>'form','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'acoustic_frm','id'=>'acoustic_frm')); ?>

                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="quote_for" value="recording"/>
                    <input type="hidden" name="shop_id" value="<?php echo e($sid); ?>"/>
                    <input type="hidden" name="music_id" value="<?php echo e($lid); ?>"/>
                    <input type="hidden" name="vendor_id" value="<?php echo e($singerDetails->vendor_id); ?>"/>
                    <?php
                    if(Session::has('customerdata.user_id'))
                    {
                        $userid  = Session::get('customerdata.user_id');
                    }
                    ?>
                    <input type="hidden" name="user_id" value="<?php if(isset($userid) && $userid!=''){echo $userid;}?>"/>
                        <h3 class="head-title"><?php echo e($singerDetails->$name); ?></h3>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label><?php if(Lang::has(Session::get('lang_file').'.THE_GROOM_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.THE_GROOM_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.THE_GROOM_NAME')); ?> <?php endif; ?></label>
                                    <input class="t-box form-control" name="groom_name" maxlength="75" id="groom_name" value="<?php echo e($cuname); ?>" type="text">
                                    <div><?php if($errors->has('groom_name')): ?><span class="error"><?php echo e($errors->first('groom_name')); ?></span><?php endif; ?> </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php echo e((Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')); ?></label>
                                    <select name="occasion" id="occasion" class="t-box form-control">
                                        <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Occasion_Type')); ?> <?php endif; ?></option>
                                        <?php
                                        if(Session::get('lang_file')!='en_lang')
                                        {
                                            $getArrayOfOcc = array( '2'=>'مناسبة الزفاف');
                                        }
                                        else
                                        {
                                            $getArrayOfOcc = array( '2'=>'Wedding Occasion');
                                        }
                                        foreach($getArrayOfOcc as $key=>$ocval){?>
                                        <option value="" disabled="" style="color: #d2cece;"><?php echo e($ocval); ?></option>
                                        <?php
                                        $setOccasion = Helper::getAllOccasionNameList($key);
                                        foreach($setOccasion as $val){ ?>
                                        <option value="<?php echo e($val->id); ?>"<?php if(isset($occasiontype) && $occasiontype==$val->id){ echo 'selected="selected"'; }?>>
                                            <?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
                                        </option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div><?php if($errors->has('occasion')): ?><span class="error"><?php echo e($errors->first('occasion')); ?></span><?php endif; ?> </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php if(Lang::has(Session::get('lang_file').'.HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HALL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HALL')); ?> <?php endif; ?></label>
                                    <input class="t-box form-control" name="hall" id="hall" type="text" maxlength="75" value="<?php echo Input::old('hall'); ?>">
                                    <div><?php if($errors->has('hall')): ?><span class="error"><?php echo e($errors->first('hall')); ?></span><?php endif; ?> </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php if(Lang::has(Session::get('lang_file').'.SONG_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SONG_NAME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.SONG_NAME')); ?> <?php endif; ?></label>
                                    <input type="text" class="t-box form-control" name="song_name" maxlength="75" id="song_name" value="<?php echo Input::old('song_name'); ?>">
                                   <div> <?php if($errors->has('song_name')): ?><span class="error"><?php echo e($errors->first('song_name')); ?></span><?php endif; ?> </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php if(Lang::has(Session::get('lang_file').'.MUSIC_TYPE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MUSIC_TYPE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.MUSIC_TYPE')); ?> <?php endif; ?></label>
                                    <input class="t-box form-control" name="music_type" id="music_type" maxlength="75" value="<?php echo Input::old('music_type'); ?>" type="text">
                                     <div>   <?php if($errors->has('music_type')): ?><span class="error"><?php echo e($errors->first('music_type')); ?></span><?php endif; ?> </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?></label>
                                    <select class="checkout-small-box form-control" name="city" id="city">
                                        <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></option>
                                        <?php $getC = Helper::getCountry(); ?>
                                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="" disabled="" style="color: #d2cece;"><?php echo e($cbval->co_name); ?></option>
                                            <?php $getCity = Helper::getCityb($cbval->co_id); ?>
                                            <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(isset($city_id) && $city_id==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} ?>
                                                <?php if($selected_lang_code !='en'): ?>
                                                    <?php $ci_name= 'ci_name_ar'; ?>
                                                <?php else: ?>
                                                    <?php $ci_name= 'ci_name'; ?>
                                                <?php endif; ?>
                                                <option value="<?php echo e($val->ci_id); ?>" <?php echo e($selectedcity); ?> ><?php echo e($val->$ci_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                   <div> <?php if($errors->has('city')): ?><span class="error"><?php echo e($errors->first('city')); ?></span><?php endif; ?>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group data">
                                    <label><?php if(Lang::has(Session::get('lang_file').'.DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DATE')); ?> <?php endif; ?> / <?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TIME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?> <?php endif; ?></label>
                                    <input class="t-box form-control cal-t datetimepicker" autocomplete="off" name="date" id="date" value="<?php echo Input::old('date'); ?>" type="text" >
                                    <div><?php if($errors->has('date')): ?><span class="error"><?php echo e($errors->first('date')); ?></span><?php endif; ?> </div>
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <center>
                            <input type="submit" name="submit" class="form-btn btn-info-wisitech" value="<?php if(Lang::has(Session::get('lang_file').'.GET_A_QUOTE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GET_A_QUOTE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.GET_A_QUOTE')); ?> <?php endif; ?>" />
                        </center>
                    <?php echo Form::close(); ?>

                </div>

            </div>


        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker();
        });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
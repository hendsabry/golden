<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $skin_teeth_leftmenu =1; ?>
<div class="merchant_vendor">
<?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
  <div class="inner">
     <header>

<?php if(request()->sid==''): ?>

      <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_add_clinic')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_add_clinic')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_add_clinic')); ?> <?php endif; ?> </h5>
      <?php else: ?>

       <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_update_clinic')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_update_clinic')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_update_clinic')); ?> <?php endif; ?> </h5>
<?php endif; ?>

        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
		  <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <form name="form1" id="form1" method="post" action="<?php echo e(route('store-skin-teeth-add-shop')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CLINIC_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CLINIC_NAME'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="<?php echo e(isset($fetchfirstdata->mc_name) ? $fetchfirstdata->mc_name : ''); ?>"   class="english" maxlength="140">
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" class="arabic ar" value="<?php echo e(isset($fetchfirstdata->mc_name_ar) ? $fetchfirstdata->mc_name_ar : ''); ?> " name="mc_name_ar" type="text" data-validation-length="max80"  maxlength="40" >
                        <input type="hidden" name="parentid" value="<?php echo e(request()->id); ?>">
                         <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
                          <input type="hidden" name="autoid" value="<?php echo e(request()->autoid); ?>">
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="form_row common_field">
                   <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_IMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <?php if(!isset($getDb)): ?>
                            <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                          <?php endif; ?>
                          
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file updateimage" type="file">
                         <?php if(isset($fetchfirstdata->mc_img) && $fetchfirstdata->mc_img!=''): ?>
                        <div class="form-upload-img">
                   
                         <img src="<?php echo e(isset($fetchfirstdata->mc_img) ? $fetchfirstdata->mc_img : ''); ?>">
                          </div>
                           <?php endif; ?>
                      </div>
                    </div>
                    </div>
                  </div>
                  <?php if($sid==''): ?>
                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  </div></div>
                  <?php else: ?>
                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                    <input type="submit" name="submit" value="Update">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="تحديث">
                  </div>
                  </div></div>
                  <?php endif; ?>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>       
$("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
$("#form1").validate({
                  ignore: [],
                  rules: {
                  mc_name: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mc_name: {
              required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_CLINIC_NAME'); ?>",
               
                      },  

                 mc_name_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_RESTURNANT_NAME_AR'); ?>",
                      },    

                   
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
<?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.arabic_tab').trigger('click');     

                    }
                      if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }
            <?php else: ?>
 
              if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }

                      if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
 
            <?php endif; ?>


                    
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
  var createValidation = function() {
       
<?php if(isset($fetchfirstdata->mc_img) && $fetchfirstdata->mc_img  !=''): ?> 

        $(".updateimage").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
              required: false,
               accept:"png|jpe?g|gif",

              messages: {
                accept: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
              }
            });
          });

 <?php else: ?>
 

 $(".updateimage").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
              required: true,
              accept:"png|jpe?g|gif",

              messages: {
                accept: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>"
              }
            });
          });


        <?php endif; ?>


};

$(function() {
   createValidation();
});
</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
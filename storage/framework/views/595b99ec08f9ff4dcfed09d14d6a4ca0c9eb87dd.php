<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $dessert_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if(request()->shopid ==''): ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_SHOP')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_SHOP')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPDATE_SHOP')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPDATE_SHOP')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> <?php echo Form::open(array('url'=>'dessert-add-update-shop','id'=>'addhotel','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

              <div class="one-call-form">
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); ?> </span> </label>
                    <input type="hidden" name="service_id" id="service_id" value="<?php echo e($services_id); ?>">
                    <input type="hidden" name="shop_id" id="shop_id" value="<?php echo e($shopid); ?>">
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="<?php echo e(isset($alldata->mc_name) ? $alldata->mc_name : ''); ?>" id="hotal_name"  class="english"  data-validation-length="max80" maxlength="40">
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" class="arabic ar" name="mc_name_ar"  id="mc_name_ar"   type="text" data-validation-length="max80"  maxlength="40" value="<?php echo e(isset($alldata->mc_name_ar) ? $alldata->mc_name_ar : ''); ?>" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_IMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" >
                        <?php if(isset($alldata->mc_img) && $alldata->mc_img!=''): ?>
                        <div class="form-upload-img"> <img src="<?php echo e($alldata->mc_img); ?>"> </div>
                        <?php endif; ?> </div>
                    </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <div class="english">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </div>
              <?php echo e(Form::close()); ?> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>       
$("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
$("#addhotel").validate({
                  ignore: [],
                  rules: {
                  mc_name: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },
                    <?php if(isset($alldata->mc_img) && $alldata->mc_img!=''): ?>
                    mc_img: {
                    required: false,
                    accept:"png|jpe?g|gif",
                    },
                    <?php else: ?>
                    mc_img: {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },

                    <?php endif; ?>     

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mc_name: {
                 required: "<?php echo lang::get('mer_en_lang.PLEASE_ENTER_SHOP_NAME'); ?>",
                      },  

                 mc_name_ar: {
               required: "<?php echo lang::get('mer_ar_lang.PLEASE_ENTER_SHOP_NAME'); ?>",
                      },    
                    
                        mc_img: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },                       
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


<?php if($mer_selected_lang_code !='en'): ?>

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click'); 
                             

                    }

<?php else: ?>
  if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click'); 
                             

                    }

if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }

<?php endif; ?>
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
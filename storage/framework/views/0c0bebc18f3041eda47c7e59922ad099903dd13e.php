<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $ship_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MANAGE_SHIPPING')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MANAGE_SHIPPING')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MANAGE_SHIPPING')); ?> <?php endif; ?> </h5>
         <span style="display: none;"><?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </span></header>
        <!-- Display Message after submition --> 
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?>
              <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box box_shipping"> 
                <?php  if($getShipping !=''){
                $buffet = explode(',',$getShipping->Buffet);
                $Desert = explode(',',$getShipping->Desert);
                $Dates = explode(',',$getShipping->Dates);
                $Cosha =  explode(',',$getShipping->Cosha);
                $Roses = explode(',',$getShipping->Roses);
                $Special_Events = explode(',',$getShipping->Special_Events);
                $Makeup_Product =  explode(',',$getShipping->Makeup_Product);
                $Acoustics = explode(',',$getShipping->Acoustics);
                $Tailors =  explode(',',$getShipping->Tailors);
                $Dresses =  explode(',',$getShipping->Dresses);
                $Abaya = explode(',',$getShipping->Abaya);
                $Oud_and_Perfumes =  explode(',',$getShipping->Oud_and_Perfumes);
                $Gold_and_Jewellery = explode(',',$getShipping->Gold_and_Jewellery);

              }
              else
              {

                 $buffet = array(9,10);
                $Desert = array(9,10);
                $Dates = array(9,10);
                $Cosha = array(9,10);
                $Roses = array(9,10);
                $Special_Events = array(9,10);
                $Makeup_Product =  array(9,10);
                $Acoustics =array(9,10);
                $Tailors = array(9,10);
                $Dresses =  array(9,10);
                $Abaya = array(9,10);
                $Oud_and_Perfumes =   array(9,10);
                $Gold_and_Jewellery = array(9,10);


              }
                
 

                ?>
                <form name="manageshipping" method="post" id="manageshipping" action="<?php echo e(route('manageshipping')); ?>">
                  <?php echo e(csrf_field()); ?>

               
                    <!--div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.buffet'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.buffet'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_buffet[]" <?php if(isset($buffet[0]) && $buffet[0]=='1'): ?> checked  <?php endif; ?> value="1"  id="Aramex1" > <label for="Aramex1">Aramex</label>
                    <input  type="checkbox" name="needshipping_buffet[]" value="2" <?php if(isset($buffet[1]) && $buffet[1]=='2'): ?> checked  <?php endif; ?>   <?php if($buffet[0]== '2'): ?> checked <?php endif; ?>  id="pick1" ><label for="pick1"> Pick Logistics</label>
                     </div>
                    </div-->
                    
                    

                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Dessert'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Dessert'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_dessert[]" <?php if(isset($Desert[0]) && $Desert[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex2" > <label for="Aramex2">Aramex</label>
                    <input  type="checkbox" name="needshipping_dessert[]" <?php if(isset($Desert[1]) && $Desert[1]=='2'): ?> checked  <?php endif; ?>   <?php if($Desert[0]== '2'): ?> checked <?php endif; ?> value="2" id="pick2" ><label for="pick2"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Dates'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Dates'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_dates[]" <?php if(isset($Dates[0]) && $Dates[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex3" > <label for="Aramex3">Aramex</label>
                    <input  type="checkbox" name="needshipping_dates[]" <?php if(isset($Dates[1]) && $Dates[1]=='2'): ?> checked  <?php endif; ?>  <?php if($Dates[0]== '2'): ?> checked <?php endif; ?>  value="2" id="pick3" ><label for="pick3"> Pick Logistics</label>
                     </div>
                    </div>


                    <!--div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.cosha'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.cosha'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_cosha[]" <?php if(isset($Cosha[0]) && $Cosha[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex4" > <label for="Aramex4">Aramex</label>
                    <input  type="checkbox" name="needshipping_cosha[]" <?php if(isset($Cosha[1]) && $Cosha[1]=='2'): ?> checked  <?php endif; ?>  <?php if($Cosha[0]== '2'): ?> checked <?php endif; ?>  value="2" id="pick4" ><label for="pick4"> Pick Logistics</label>
                     </div>
                    </div-->



                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Roses'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Roses'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_roses[]" <?php if(isset($Roses[0]) && $Roses[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex5" > <label for="Aramex5">Aramex</label>
                    <input  type="checkbox" name="needshipping_roses[]" <?php if(isset($Roses[1]) && $Roses[1]=='2'): ?> checked  <?php endif; ?>  <?php if($Roses[0]== '2'): ?> checked <?php endif; ?>  value="2" id="pick5" ><label for="pick5"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.special_event'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.special_event'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_special_event[]"  <?php if(isset($Special_Events[0]) && $Special_Events[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex6" > <label for="Aramex6">Aramex</label>
                    <input  type="checkbox" name="needshipping_special_event[]" <?php if(isset($Special_Events[1]) && $Special_Events[1]=='2'): ?> checked  <?php endif; ?> <?php if($Special_Events[0]== '2'): ?> checked <?php endif; ?> value="2" id="pick6" ><label for="pick6"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MAKEUP'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MAKEUP'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_makeup[]"  <?php if(isset($Makeup_Product[0]) && $Makeup_Product[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex7" > <label for="Aramex7">Aramex</label>
                    <input  type="checkbox" name="needshipping_makeup[]"  <?php if(isset($Makeup_Product[1]) && $Makeup_Product[1]=='2'): ?> checked  <?php endif; ?> <?php if($Makeup_Product[0]== '2'): ?> checked <?php endif; ?> value="2"  id="pick7" ><label for="pick7"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.acoustics'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.acoustics'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_acoustics[]" <?php if(isset($Acoustics[0]) && $Acoustics[0]=='1'): ?> checked  <?php endif; ?>   value="1" id="Aramex8" > <label for="Aramex8">Aramex</label>
                    <input  type="checkbox" name="needshipping_acoustics[]" <?php if(isset($Acoustics[1]) && $Acoustics[1]=='2'): ?> checked  <?php endif; ?>   <?php if($Acoustics[0]== '2'): ?> checked <?php endif; ?>  value="2"  id="pick8" ><label for="pick8"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.tailor'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.tailor'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_tailor[]" <?php if(isset($Tailors[0]) && $Tailors[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex9" > <label for="Aramex9">Aramex</label>
                    <input  type="checkbox" name="needshipping_tailor[]" <?php if(isset($Tailors[1]) && $Tailors[1]=='2'): ?> checked  <?php endif; ?>  <?php if($Tailors[0]== '2'): ?> checked <?php endif; ?> value="2" id="pick9" ><label for="pick9"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.dresses'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.dresses'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_dresses[]" <?php if(isset($Dresses[0]) && $Dresses[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex11" > <label for="Aramex11">Aramex</label>
                    <input  type="checkbox" name="needshipping_dresses[]" <?php if(isset($Dresses[1]) && $Dresses[1]=='2'): ?> checked  <?php endif; ?>  <?php if($Dresses[0]== '2'): ?> checked <?php endif; ?> value="2"  id="pick11" ><label for="pick11"> Pick Logistics</label>
                     </div>
                    </div>



                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.abaya'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.abaya'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_abaya[]" <?php if(isset($Abaya[0]) && $Abaya[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex12" > <label for="Aramex12">Aramex</label>
                    <input  type="checkbox" name="needshipping_abaya[]" <?php if(isset($Abaya[1]) && $Abaya[1]=='2'): ?> checked  <?php endif; ?> <?php if($Abaya[0]== '2'): ?> checked <?php endif; ?>  value="2" id="pick12" ><label for="pick12"> Pick Logistics</label>
                     </div>
                    </div>



                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.oudandperfumes'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.oudandperfumes'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_oudandperfumes[]" <?php if(isset($Oud_and_Perfumes[0]) && $Oud_and_Perfumes[0]=='1'): ?> checked  <?php endif; ?>   value="1" id="Aramex13" > <label for="Aramex13">Aramex</label>
                    <input  type="checkbox" name="needshipping_oudandperfumes[]" <?php if(isset($Oud_and_Perfumes[1]) && $Oud_and_Perfumes[1]=='2'): ?> checked  <?php endif; ?> <?php if($Oud_and_Perfumes[0]== '2'): ?> checked <?php endif; ?>   value="2" id="pick13" ><label for="pick13"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.goldandjewelry'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.goldandjewelry'); ?> </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_goldandjewelry[]" <?php if(isset($Gold_and_Jewellery[0]) && $Gold_and_Jewellery[0]=='1'): ?> checked  <?php endif; ?>  value="1" id="Aramex14" > <label for="Aramex14">Aramex</label>
                    <input  type="checkbox" name="needshipping_goldandjewelry[]" <?php if(isset($Gold_and_Jewellery[1]) &&  trim($Gold_and_Jewellery[1])== '2'): ?> checked  <?php endif; ?> <?php if($Gold_and_Jewellery[0]== '2'): ?> checked <?php endif; ?>  value="2"  id="pick14" ><label for="pick14"> Pick Logistics</label>
                     </div>
                    </div>




                                    
                  <div class="form_row">
                  <div class="arabic ar form_row_left arbic_right_btn">
                    <input type="submit" name="submit" value="تحديث">
                    </div>
                    <div class="form_row_left english">
                    <input type="submit" name="submit" value="Update">
                  </div>
                  </div>
                  
                  
                  
                </form>
                
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
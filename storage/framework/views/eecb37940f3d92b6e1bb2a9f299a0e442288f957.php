<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $spabig_leftmenu =1; ?>
<div class="merchant_vendor">   <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if(request()->serviceid==''): ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADDPACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADDPACKAGE')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATEPACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?> </div>
              <?php endif; ?>
              <!-- Display Message after submition -->
              <form name="form1" id="addmanager" method="post" action="<?php echo e(route('storespapackageservice')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"><span class="english"><?php echo lang::get('mer_en_lang.PACKEGENAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKEGENAME'); ?> </span></label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" maxlength="60" type="text" name="title" value="<?php echo e(isset($getservice->pro_title) ? $getservice->pro_title : ''); ?>" id="title" required="">
                      </div>
                      <div class="arabic">
                        <input class="arabic ar" maxlength="60" id="title_ar"  name="title_ar" value="<?php echo e(isset($getservice->pro_title_ar) ? $getservice->pro_title_ar : ''); ?>"  type="text" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PACKAGEIMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKAGEIMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="img" class="info-file" type="file">
                        <?php if(isset($getservice->pro_Img) && $getservice->pro_Img !=''): ?><div class="form-upload-img"><img src="<?php echo e(isset($getservice->pro_Img) ? $getservice->pro_Img : ''); ?>"></div> <?php endif; ?> </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_PRICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRICE'); ?> </span> </label>
                    <div class="info100">
                      <div>
                        <input class="xs_small" maxlength="9" type="text" name="pro_price"  value="<?php echo e(isset($getservice->pro_price) ? $getservice->pro_price : ''); ?>" id="pro_price">
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DISCOUNT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DISCOUNT'); ?> </span> </label>
                    <div class="info100">
                      <input type="text" class="xs_small" onkeypress="return isNumber(event)"  name="discount" maxlength="7" value="<?php echo e(isset($getservice->pro_discount_percentage) ? $getservice->pro_discount_percentage : ''); ?>" id="discount">
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_Duration'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Duration'); ?> </span> </label>
                    <div class="info100">
                      <input class="xs_small" onkeypress="return isNumber(event)" type="text" name="duration" maxlength="5" value="<?php echo e(isset($getservice->service_hour) ? $getservice->service_hour : ''); ?>"  id="mer_duration"   required="">
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.SELECTSERVICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.SELECTSERVICE'); ?> </span> </label>
                    <div class="info100"> <div><?php $__currentLoopData = $getAttr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catvals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php  $Itemsarray = array();
                      $getProducts = Helper::getrelateditems($catvals->id,request()->itemid); 
                      if($getProducts->count() < 1) {continue; }
                      ?>
                      <!--Loop start-->
                      <div class="cat_box">
                        <div class="save-card-line">
                          <label for="men"> <?php if($mer_selected_lang_code =='en'): ?>  <?php echo e(isset($catvals->attribute_title) ? $catvals->attribute_title : ''); ?> <?php else: ?> <?php echo e(isset($catvals->attribute_title_ar) ? $catvals->attribute_title_ar : ''); ?> <?php endif; ?> </span> </label>
                        </div>
                        <div class="category_area"> <?php $__currentLoopData = $getProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proitems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <!--Loop -->
                      <div class="save-card-line"> <?php
                        $pID = $proitems->pro_id;
                        foreach($getPkg as $valp)
                        {
                        $Itemsarray[] = $valp->item_id;
                        }
                        ?>
                        <input type="checkbox" id="men<?php echo e($proitems->pro_id); ?>" <?php if(in_array($pID, $Itemsarray)): ?> CHECKED <?php endif; ?>  name="productitems[]" value="<?php echo e(isset($proitems->pro_id) ? $proitems->pro_id : ''); ?>">
                        <label for="men<?php echo e($proitems->pro_id); ?>"> <span class="englishd"><?php if($mer_selected_lang_code =='en'): ?>  <?php echo e(isset($proitems->pro_title) ? $proitems->pro_title : ''); ?> <?php else: ?>  <?php echo e(isset($proitems->pro_title_ar) ? $proitems->pro_title_ar : ''); ?>  <?php endif; ?><span class="sar">(SAR <?php echo e(isset($proitems->pro_price) ? $proitems->pro_price : ''); ?>)</span> </span> </label>
                      </div>
                      <!--Loop end -->
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <!--Loop End -->
					  </div>
                      <span for="productitems[]" generated="true" class="error"> </span> </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                      <input type="hidden" name="hid" value="<?php echo e(request()->hid); ?>">
                      <input type="hidden" name="itemid" value="<?php echo e(request()->itemid); ?>">
                      <input type="hidden" name="serviceid" value="<?php echo e(request()->serviceid); ?>">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
                <!-- form_row -->
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
        <!-- global_area -->
      </div>
    </div>
    <!-- right_panel -->
  </div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">
    
$("#addmanager").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       pro_price: {
                       required: true,
                      },
                      duration: {
                       required: true,
                      },
                      "productitems[]": {
                       required: true,
                      },

                      <?php if(!isset($getservice->pro_Img)): ?>     
                      img: {
                      required: true,
                      accept:"png|jpe?g|gif",
                      },
                      <?php else: ?>
                      img: {
                      accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
             
                  required:  " <?php echo e(trans('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                      },  

                title_ar: {
               required:  " <?php echo e(trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                      },
                      "productitems[]": {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_SERVICE')); ?> <?php endif; ?> ",
                      },
                   pro_price: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE')); ?> <?php endif; ?>",
                      },
                       duration: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_DURATION')); ?> <?php endif; ?>",
                      },                   
                     img: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    var valdata=validation.invalid;


             <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                     if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                     
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }

                    if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }

                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }


<?php endif; ?>
                    
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
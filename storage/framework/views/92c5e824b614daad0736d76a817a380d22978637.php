<?php $current_route = Route::getCurrentRoute()->uri(); ?>
<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
   <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
   <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?> 
        <?php
        $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  
        <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
        <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<style>
 .navbar-top-links li {display: inline-block!important;}  
 .navbar-top-links li a{padding:6px 8px 6px 8px; margin-bottom:0; border-radius: 0px;} 

</style>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
         <!-- HEADER SECTION -->
        <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
            <div class="inner" style="min-height: 700px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box arabic-right-align_box">
                        	<header>
                <div class="icons"><i class="icon-dashboard"></i></div>
                <h5><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DASHBOARD')); ?> <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?> <?php endif; ?></h5>
            </header>
             
  <?php $sold_cnt=0; ?>
 <?php $__currentLoopData = $soldproductscnt; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $soldres): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php if($soldres->pro_no_of_purchase >= $soldres->pro_qty): ?>
	
		<?php $sold_cnt++; ?>

	<?php endif; ?>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            	
                        <div style="text-align: center;"> 
                        <a class="quick-btn1" href="/admin/user_management">
                                <i class="icon-check icon-2x"></i>
                                <span><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMERS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_CUSTOMERS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMERS')); ?> <?php endif; ?></span>
                                <span class="label label-danger"><?php echo e($customers); ?></span>
                            </a>                  
                              <a class="quick-btn1 active" href="/admin/vendor_management">
                                <i class="icon-check icon-2x"></i>
                                <span><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANTS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MERCHANTS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANTS')); ?> <?php endif; ?></span>
                                <span class="label label-danger"><?php echo e($merchantscnt); ?></span>
                            </a>
							<a class="quick-btn1" href="/admin/sub_admin">
                                <i class="icon-check-minus icon-2x"></i>
                                <span><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN')); ?> <?php endif; ?></span>
                                <span class="label label-success"> <?php echo e($subadmins); ?></span>
                            </a> 
                        </div>                        
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                            <div class="panel-heading heading-2">
                                 <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_NEW_CUSTOMERS_MONTH_WISE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_NEW_CUSTOMERS_MONTH_WISE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_NEW_CUSTOMERS_MONTH_WISE')); ?> <?php endif; ?>
                                                      </div>
                             
                            <div class="panel-body">   
                                <?php if($cus_count!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>
                                   
							         <div class="demo-container bordercolr" id="chart1"></div>
                                <?php else: ?>
                                     No Customers List.
                                <?php endif; ?>         
							</div>
                            </div>
                    </div>
                </div>

                 <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                            <div class="panel-heading heading-2">
                             <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_NEW_MERCHANTS_MONTH_WISE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_NEW_MERCHANTS_MONTH_WISE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_NEW_MERCHANTS_MONTH_WISE')); ?> <?php endif; ?>
                                                      </div>
                             
                            <div class="panel-body">   
                                <?php if($merchant_chart!='0,0,0,0,0,0,0,0,0,0,0,0'): ?> 
                                     <div class="demo-container bordercolr" id="chart7"></div>
                                <?php else: ?>
                                     No Customers List.
                                <?php endif; ?>         
                            </div>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                                <div class="panel-heading heading-2">
                                   <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT')); ?> <?php endif; ?>
                                </div>
                             
                            <div class="panel-body">
                                   <?php if($transaction_chart!='0,0,0,0,0,0,0,0,0,0,0,0'): ?> 
                                  <div class="demo-container bordercolr" id="chart5" style="S width:1015px; height:470px;"></div>
                                  <?php else: ?>
                                    No Transactions to list.
                                  <?php endif; ?>  
                                </div>
                            </div>
                    </div> 
                </div>



                <div class="row hidden">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                            <div class="panel-heading heading-2">
                               <?php if(Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMER_AND_PRODUCT_COUNT_DEAL_COUNT')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMER_AND_PRODUCT_COUNT_DEAL_COUNT')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_CUSTOMER_AND_PRODUCT_COUNT_DEAL_COUNT')); ?> <?php endif; ?> 
                            </div>
                             <div class=" panel-body pie-chart">
                            <div class="col-lg-4 ">
                            
			<div class="demo-container bordercolr">
			<?php if($admin_users+$fb_users+$website_users!=0): ?>
			<div id="chart6" class="boxHeight"></div>
			 <div class="table-responsive relative-td">
            <table width="100%" border="0">
                              <tbody><tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_USER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ADMIN_USER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_USER')); ?> <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($admin_users); ?></span>
                                </td>
                                 <td style="background:#eaa228 ">
                                    <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FACEBOOK_USER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FACEBOOK_USER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FACEBOOK_USER')); ?> <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($fb_users); ?></span>
                                </td>
                                
                                <td style="background:#C5B47F">
                                    <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_USER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_WEBSITE_USER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_USER')); ?> <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($website_users); ?></span>
                                    
                                </td>                   
                              </tr>
                            </tbody></table></div>
				<?php else: ?> 
					No Customers List.
				<?php endif; ?> 
		    </div>
          
		</div>
       
       <div class="col-lg-4 ">
                              
			<div class="demo-container bordercolr"> 
			<?php if($activeproductscnt+$sold_cnt+$inactive_cnt!=0): ?>
			<div id="chart10" class="boxHeight"></div>
			 <div class="table-responsive relative-td">
            <table width="100%" border="0">
                              <tbody><tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ACTIVE_PRODUCTS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE_PRODUCTS')); ?>  <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($activeproductscnt); ?></span>
                                </td>
                                <td style="background:#eaa228">
                                <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SOLD_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SOLD_PRODUCTS')); ?>   <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SOLD_PRODUCTS')); ?> <?php endif; ?></label>
                                <span class="label label-danger"><?php echo e($sold_cnt); ?></span>
                                </td>
                                <td style="background:#C5B47F">
                                <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_INACTIVE_PRODUCTS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_INACTIVE_PRODUCTS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_INACTIVE_PRODUCTST')); ?>  <?php endif; ?></label>
                                <span class="label label-danger"><?php echo e($inactive_cnt); ?></span>
                                </td>
                                 
                                                              
                              </tr>
                            </tbody></table> </div> 
				<?php else: ?> 
					No Product List.
				<?php endif; ?> 
		    </div>
          
		</div>
		
		<div class="col-lg-4 ">
                              
			<div class="demo-container bordercolr">
			<?php if($active_cnt+$archievd_cnt+$inactivedeal_cnt!=0): ?>
			<div id="chart11" class="boxHeight"></div>
			      <div class="table-responsive relative-td">
            <table width="100%" border="0">
                              <tbody><tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_ACTIVE_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE_DEALS')); ?> <?php endif; ?></label>
                                    <span class="label label-danger"><?php echo e($active_cnt); ?></span>
                                </td>
                                <td style="background:#eaa228">
                                <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_EXPIRED_DEALS')); ?> <?php endif; ?></label>
                                <span class="label label-danger"><?php echo e($archievd_cnt); ?></span>
                                </td>
                                <td style="background:#C5B47F">
                                    <label class="label label-archive"><?php if(Lang::has(Session::get('admin_lang_file').'.BACK_INACTIVE_DEALS')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_INACTIVE_DEALS')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_INACTIVE_DEALS')); ?> <?php endif; ?></label>
                                     <span class="label label-danger"><?php echo e($inactivedeal_cnt); ?></span>
                                </td> 
                                                              
                              </tr>
                            </tbody></table> </div>
				<?php else: ?> 
					No Deal List.
				<?php endif; ?> 
		    </div>
          
		</div>
                             
		</div>
                            </div>
                    </div>

                 
                </div>
                
            </div>

        </div>
</div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    <!--END FOOTER -->
    <?php if(($admin_users+$fb_users+$website_users)>0): ?>
    <script>
	 $(document).ready(function(){
		
	plot6 = $.jqplot('chart6', [[<?php echo e($admin_users); ?>,<?php echo e($fb_users); ?>,<?php echo e($website_users); ?> ]], { seriesDefaults:{renderer:$.jqplot.PieRenderer}
	} );
		}); 
	</script>
    <?php endif; ?>
    <?php if(($activeproductscnt+$sold_cnt+$inactive_cnt)>0): ?>
      <script>
	$(document).ready(function(){
		
	plot10 = $.jqplot('chart10', [[<?php echo e($activeproductscnt); ?>,<?php echo e($sold_cnt); ?> , <?php echo e($inactive_cnt); ?> ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer} });
		});
	</script>
    <?php endif; ?>
	<?php if(($active_cnt+$archievd_cnt+$inactivedeal_cnt)>0): ?>
	 <script>

	$(document).ready(function(){
		
	plot11 = $.jqplot('chart11', [[<?php echo e($active_cnt); ?> ,<?php echo e($archievd_cnt); ?>, <?php echo e($inactivedeal_cnt); ?> ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer} });
		});
	</script>
    <?php endif; ?>
    <?php if($cus_count!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php echo e($s1 = "[" .$cus_count. "]"); ?>

        var s1 =  <?php echo e($s1); ?>

        var ticks = ['<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JANUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FEBRUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MARCH')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_APRIL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL')); ?><?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MAY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY')); ?> <?php endif; ?>','<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JUNE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JULY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AUGUST')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_OCTOBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER')); ?>  <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_NOVEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DECEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER')); ?> <?php endif; ?>'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickOptions:{
                            formatString:'%b&nbsp;%#d'
                      },
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    <?php endif; ?>  
     <?php if($merchant_chart!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
        
        <?php echo e($s1 = "[" .$merchant_chart. "]"); ?>

        var s1 =  <?php echo e($s1); ?>

        var ticks = ['<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JANUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FEBRUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MARCH')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_APRIL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL')); ?><?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MAY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY')); ?> <?php endif; ?>','<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JUNE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JULY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AUGUST')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_OCTOBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER')); ?>  <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_NOVEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DECEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER')); ?> <?php endif; ?>'];
        
        plot1 = $.jqplot('chart7', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickOptions:{
                            formatString:'%b&nbsp;%#d'
                      },
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart7').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    <?php endif; ?>   
    <?php if($transaction_chart!='0,0,0,0,0,0,0,0,0,0,0,0'): ?>
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true; 
        <?php echo e($s1 = "[" .$transaction_chart. "]"); ?>

        <?php echo e($s2 = "[" .$transaction_credit. "]"); ?>

        var s1 = <?php echo e($s1); ?>

         var s2 = <?php echo e($s2); ?>

       
        //var ticks = ['Jan', 'Feb', 'Mar', 'Apr', 'May','June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
         var ticks = ['<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JANUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_FEBRUARY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MARCH')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_APRIL')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL')); ?><?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_MAY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY')); ?> <?php endif; ?>','<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JUNE')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_JULY')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_AUGUST')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_OCTOBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER')); ?>  <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_NOVEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER')); ?> <?php endif; ?>', '<?php if(Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.BACK_DECEMBER')); ?>  <?php else: ?> <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER')); ?> <?php endif; ?>'];
        
        plot1 = $.jqplot('chart5', [s1,s2], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart5').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
     <?php endif; ?>     
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.jqplot.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/bootstrap-datepicker.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo e(url('')); ?>/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="<?php echo e(url('')); ?>/public/assets/js/bar_chart.js"></script>
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>


</body>

    <!-- END BODY -->
</html>
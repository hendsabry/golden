 <?php $data = app('App\Help'); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
 
<?php $singer_leftmenu =1; ?>
<div class="merchant_vendor cont_add"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu')); ?> <?php endif; ?></h5>
      </header>
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      
      <?php if($getusers->status =='5'): ?>
      <div class="alert alert-info"><?php if(Lang::has(Session::get('mer_lang_file').'.Deny_by_admin')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Deny_by_admin')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Deny_by_admin')); ?> <?php endif; ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
          <div class="box commonbox needrtl">
          <form>
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="english" disabled="" name="customer_name" id="customer_name" maxlength="60" value="<?php echo e($getusers->cus_name); ?>" type="text">
                  <input class="arabic ar" disabled="" name="customer_name_ar" id="customer_name_ar" maxlength="60" value="<?php echo e($getusers->cus_name); ?>" type="text">
                </div>
              </div>
              <div class="form_row_right">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="" disabled="" name="email" id="email" value="<?php echo e($getusers->email); ?>" maxlength="60" type="text">
                </div>
              </div>
            </div>
            <div class="form_row">
              <!--div class="form_row_left">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PHONE')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="small-sel" disabled="" name="phone" id="phone" value="<?php echo e($getusers->cus_phone); ?>" maxlength="10" type="text">
                </div>
              </div-->


              <div class="form_row_right">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Hall')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Hall')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Hall')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="english" disabled="" name="hall" value="<?php echo e($getusers->hall); ?>" id="hall" value="50" maxlength="" type="text">
                  <input class="arabic ar" disabled="" name="hall_ar" value="<?php echo e($getusers->hall); ?>"   id="hall_ar" value="50" maxlength="" type="text">
                </div>
              </div>
            </div>
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Occasion_Type')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="english" disabled="" name="occasion_type" value="<?php echo e($getusers->occasion_type); ?>"  id="occasion_type"   maxlength="50" type="text">
                  <input class="arabic ar" disabled="" name="occasion_type_ar" id="occasion_type_ar" value="<?php echo e($getusers->occasion_type); ?>" maxlength="50" type="text">
                </div>
              </div>
              <div class="form_row_right">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Location')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Location')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Location')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input name="location" disabled="" id="location" maxlength="150" value="<?php echo e($getusers->location); ?>" type="text">
                </div>
              </div>
            </div>
			<?php $getNmae = Helper::getmerchantcity($getusers->city_id); ?>
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?> <?php endif; ?> </label>
                <div class="info100">
				<input name="singer_city" disabled="" id="singer_city" maxlength="150" value="<?php echo e($getNmae->ci_name); ?>" type="text">                  
                </div>
              </div>
              <div class="form_row_right">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Duration')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Duration')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Duration')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="english xs_small" disabled="" name="duration" id="duration" maxlength="10" value="<?php echo e($getusers->duration); ?>" type="text">
                  <input class="arabic ar xs_small" disabled="" name="duration_ar" value="<?php echo e($getusers->duration); ?>" id="duration_ar" maxlength="10" value="" type="text">
                </div>
              </div>
            </div>
            <div class="form_row_right">
              <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_UserComments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UserComments')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UserComments')); ?> <?php endif; ?> </label>
              <div class="info100">
                <div class="english">
                  <textarea class="english" readonly="readonly"><?php echo e(isset($getusers->user_comment) ? $getusers->user_comment : ''); ?></textarea>
                </div>
              </div>
            </div>
            <div class="form_row">
              <!--<div class="form_row_left">
                    <label class="form_label"> 
                   
                   <?php if(Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE')); ?> <?php endif; ?>
                     </label>
                    <div class="info100">
                      <input class="english xs_small cal-t" disabled="" name="request_date" id="request_date" maxlength="" value="<?php echo e(Carbon\Carbon::parse($getusers->
              created_at)->format('F j, Y')); ?>" type="text">
              <input class="arabic ar xs_small cal-t" disabled="" name="request_date_ar" id="request_date_ar" maxlength="<?php echo e(Carbon\Carbon::parse($getusers->created_at)->format('F j, Y')); ?>" value="" type="text">
            </div>
            </div>
            -->
            <div class="form_row_right">
              <div class="dd-time">
                <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Request_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Request_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Request_Date')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="english cal-t" disabled="" name="date" id="date" value="<?php echo e($getusers->date); ?>" type="text">
                  <input class="arabic ar cal-t" name="date_ar" id="date_ar" value="<?php echo e($getusers->date); ?>" type="text">
                </div>
              </div>
              <div class="dd-time ts">
                <label class="form_label"> <span class="english"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Time')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Time')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Time')); ?> <?php endif; ?> </label>
                <div class="info100">
                  <input class="english time-t" disabled="" name="time" id="time" value="<?php echo e($getusers->time); ?>" maxlength="10"  type="text">
                  <input class="arabic ar time-t" disabled="" name="time_ar" value="<?php echo e($getusers->time); ?>" id="time_ar" maxlength="10"  value="" type="text">
                </div>
              </div>
            </div>
            </div>
          </form>
          <form style="padding-top:0" name="form1" method="post" action="<?php echo e(route('store-singer')); ?>" id="form1">
            <?php echo e(csrf_field()); ?>

            <div class="commenthide">
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRICE')); ?> <?php endif; ?> </label>
                  <div class="info100">
                    <input class="small-sel" name="price" id="price" value="<?php echo e(isset($getquote->price) ? $getquote->price : ''); ?>" maxlength="10" type="text">
                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Comments')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Comments')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Comments')); ?> <?php endif; ?> </label>
                  <div class="info100">
                    <div class="english">
                      <textarea class="english" id="comments" maxlength="300" name="comments"><?php echo e(isset($getquote->comment) ? $getquote->comment : ''); ?></textarea>
                    </div>
                    <div class="arabic">
                      <textarea class="arabic ar" id="comments_ar" name="comments_ar"></textarea>
                    </div>
                    <input type="hidden" name="enqueryid" id="enqueryid" value="<?php echo e($getusers->id); ?>">
                    <input type="hidden" name="catid" id="id" value="<?php echo e($id); ?>">
                    <input type="hidden" name="hid" id="" value="<?php echo e($hid); ?>">
                    <input type="hidden" name ="itmid" id="" value="<?php echo e($itmid); ?>">
                    <input type="hidden" name ="autoid" id="autoid" value="<?php echo e(isset($autoid) ? $autoid : ''); ?>">
                    <input type="hidden" name ="userid" id="" value="<?php echo e($getusers->user_id); ?>">
                    <input type="hidden" name ="customername" id="" value="<?php echo e($getusers->cus_name); ?>">
                    <input type="hidden" name ="email" id="" value="<?php echo e($getusers->email); ?>">
                    <input type="hidden" name ="languagetype" id="" value="<?php echo e($getusers->language_type); ?>">
                  </div>
                </div>
              </div>
              <?php if($getusers->status !='4'  && $getusers->status !='5' && $getusers->status !='3'): ?>
              <div class="form-btn-section" style="width:auto;">
                <input type="submit" id="submit" name="addhallpics" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SUBMIT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SUBMIT')); ?> <?php endif; ?>">
              </div>
              <?php endif; ?> </div>
            <?php if($autoid=='' && $getusers->status !='5'): ?>
            <div class="buttonhide">
              <div class="form-btn-section">
                <input type="submit" id="commentsubmit" name="addhallpics" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_CONFIRM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CONFIRM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CONFIRM')); ?> <?php endif; ?>">
              </div>
            </div>
            <?php endif; ?>
            <?php if($getusers->status !='3' && $getusers->status !='4'  && $getusers->status !='5'): ?>
            <div class="buttonhideright" style="float:left; margin-left:10px;">
              <div class="form-btn-section">
                <input type="button" data-id="<?php echo e($getusers->id); ?>" id="reject" name="reject" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_REJECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_REJECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_REJECT')); ?> <?php endif; ?>">
              </div>
            </div>
            <?php endif; ?>
          </form>
        </div>
        <!-- box -->
      </div>
    </div>
  </div>
</div>
<!-- global_area -->
</div>
</div>
</div>
</div>
<script type="text/javascript">     
$("#form1").validate({
                  ignore: [],
                  rules: {
                         
                         price: {
                          required: true,
                          },
                          comments: {
                          required: true,
                          },
                          
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                 
           messages: {
                price: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?>",
                  }, 
                   
                  comments: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_COMMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_COMMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_COMMENT')); ?> <?php endif; ?>",
                  }, 
                   
                   
                },
                invalidHandler: function(e, validation){
                      var valdata=validation.invalid;
                       if (typeof valdata.comments != "undefined" || valdata.price != null) 
                      {
                      $('.english_tab').trigger('click'); 
                       }
                      if (typeof valdata.comments != "undefined" || valdata.comments != null) 
                      {
                      $('.english_tab').trigger('click'); 
                       }
                     

                      },

                submitHandler: function(form) {
                    form.submit();
                }
            });
$(document).ready(function() {
  var autoid =  $('#autoid').val();
  if(autoid!='') {
  $('.commenthide').show();
} else {
    $('.commenthide').hide();
}
  $('#commentsubmit').click(function() {
    $('.commenthide').show();
    $('.buttonhide').hide();

  })  

})
$('#reject').click(function() {
var getID = $(this).data('id');
 
if(getID!=''){
     jQuery.ajax({
        type: "Post",
         headers: {
        'X-CSRF-TOKEN': jQuery('input[name="_token"]').val()
          },
        url: "<?php echo e(route('denyquote')); ?>",
        data: {id:getID},
        success: function(data) {         
            if(data==1){
               location.reload();
            }
      
        }
    });

}

});
 

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>
    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <?php

                                $name = 'name';

                                if(Session::get('lang_file')!='en_lang')

                                {

                                    $name = 'name_ar';

                                }

                               // echo $singerDetails->$name;

                                ?>
                                <h2><?php echo e($singerDetails->$name); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php

                                    $address = 'address';

                                    if(Session::get('lang_file')!='en_lang')

                                    {

                                        $address = 'address_ar';

                                    }

                                    echo $singerDetails->$address;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>
                                <?php

                                        ?>

                                <?php $about='about'?>

                                <?php if(Session::get('lang_file')!='en_lang'): ?>

                                    <?php $about= 'about_ar'; ?>

                                <?php endif; ?>

                                <?php echo nl2br($singerDetails->$about); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        MAP
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php if(isset($userinfo->cus_pic) && $userinfo->cus_pic!=''){echo @$userinfo->cus_pic;} ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php if(isset($userinfo->cus_name) && $userinfo->cus_name!=''){echo @$userinfo->cus_name;}else{echo 'N/A';} ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php if(isset($val->comments) && $val->comments!=''){echo @$val->comments;} ?><</p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                        <?php echo e($allreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe style="width: 100%;height: 100%" class="service-video" src="<?php echo e($singerDetails->video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                    </div>
                </div>
            </div>

            <h3 class="head-title"><?php echo e((Lang::has(Session::get('lang_file').'.SINGER_INFORMATION')!= '')  ?  trans(Session::get('lang_file').'.SINGER_INFORMATION'): trans($OUR_LANGUAGE.'.SINGER_INFORMATION')); ?></h3>
            <div class="box p-15" style="background: #f4f4f4">
                <div class="row">
                    <div class="col-md-3">
                        <div class="user-image"><img src="<?php echo e($singerDetails->image); ?>" alt="" /></div>
                    </div>
                    <div class="col-md-9">
                        <?php echo Form::open(array('url'=>"insert_singer",'class'=>'form','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'singer_frm','id'=>'singer_frm')); ?>

                        <?php echo e(csrf_field()); ?>

                        <h3 class="head-title">
                    <?php

                        $name = 'name';

                        if(Session::get('lang_file')!='en_lang')

                        {

                          $name = 'name_ar';

                        }

                        echo ucfirst($singerDetails->$name);
                                            ?>
                        </h3>
                        <input type="hidden" name="quote_for" value="singer"/>

                        <input type="hidden" name="shop_id" value="<?php echo e($sid); ?>"/>

                        <input type="hidden" name="music_id" value="<?php echo e($lid); ?>"/>

                        <input type="hidden" name="vendor_id" value="<?php echo e($singerDetails->vendor_id); ?>"/>
                        <?php

                        if(Session::has('customerdata.user_id'))

                        {

                            $userid  = Session::get('customerdata.user_id');

                        }

                        ?>

                        <input type="hidden" name="user_id" value="<?php if(isset($userid) && $userid!=''){echo $userid;}?>"/>

                        <input type="hidden" name="singer_name" value="<?php echo e($singerDetails->$name); ?>"/>

                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php if(Lang::has(Session::get('lang_file').'.HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HALL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HALL')); ?> <?php endif; ?></label>
                                        <input  class="t-box form-control" name="hall" id="hall" type="text" maxlength="75" value="<?php echo Input::old('hall'); ?>"/>
                                        <div><?php if($errors->has('hall')): ?><span class="error"><?php echo e($errors->first('hall')); ?></span><?php endif; ?> </div>
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php echo e((Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')); ?> </label>
                                    <select name="occasion" id="occasion" class="form-control">

                                        <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Occasion_Type')); ?> <?php endif; ?></option>

                                        <?php

                                        if(Session::get('lang_file')!='en_lang')

                                        {

                                            $getArrayOfOcc = array( '2'=>'مناسبة الزفاف');

                                        }

                                        else

                                        {

                                            $getArrayOfOcc = array( '2'=>'Wedding Occasion');

                                        }

                                        foreach($getArrayOfOcc as $key=>$ocval){?>

                                        <option value="" disabled="" style="color: #d2cece;"><?php echo e($ocval); ?></option>

                                        <?php

                                        $setOccasion = Helper::getAllOccasionNameList($key);

                                        foreach($setOccasion as $val){ ?>

                                        <option value="<?php echo e($val->id); ?>"<?php if(isset($occasiontype) && $occasiontype==$val->id){ echo 'selected="selected"'; }?>>

                                            <?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>

                                        </option>

                                        <?php } ?>

                                        <?php } ?>

                                    </select>

                                    <div><?php if($errors->has('occasion')): ?><span class="error"><?php echo e($errors->first('occasion')); ?></span><?php endif; ?> </div>

                                </div>
                            </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?></label>
                                        <select class="checkout-small-box form-control" name="city" id="city">

                                            <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></option>



                                            <?php $getC = Helper::getCountry(); ?>

                                            <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                                                <option value="" disabled="" style="color: #d2cece;"><?php echo e($cbval->co_name); ?></option>



                                                <?php $getCity = Helper::getCityb($cbval->co_id); ?>

                                                <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                    <?php if(isset($city_id) && $city_id==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} ?>

                                                    <?php if($selected_lang_code !='en'): ?>

                                                        <?php $ci_name= 'ci_name_ar'; ?>

                                                    <?php else: ?>

                                                        <?php $ci_name= 'ci_name'; ?>

                                                    <?php endif; ?>



                                                    <option value="<?php echo e($val->ci_id); ?>" <?php echo e($selectedcity); ?> ><?php echo e($val->$ci_name); ?></option>



                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



                                        </select>

                                       <div> <?php if($errors->has('city')): ?><span class="error"><?php echo e($errors->first('city')); ?></span><?php endif; ?> </div>

                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php if(Lang::has(Session::get('lang_file').'.LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOCATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOCATION')); ?> <?php endif; ?></label>
                                        <input class="t-box form-control" type="text" name="location" maxlength="75" id="location" value="<?php echo Input::old('location'); ?>">

                                        <div><?php if($errors->has('location')): ?><span class="error"><?php echo e($errors->first('location')); ?></span><?php endif; ?> </div>

                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php if(Lang::has(Session::get('lang_file').'.DURATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DURATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DURATION')); ?> <?php endif; ?></label>
                                        <input class="t-box form-control"  name="duration" id="duration" type="text" maxlength="2" value="<?php echo Input::old('duration'); ?>" style="max-width:125px;" onkeypress="return isNumber(event)" >

                                        <div><?php if($errors->has('duration')): ?><span class="error"><?php echo e($errors->first('duration')); ?></span><?php endif; ?> </div>

                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group data">
                                        <label><?php if(Lang::has(Session::get('lang_file').'.DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DATE')); ?> <?php endif; ?> / <?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TIME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?> <?php endif; ?></label>
                                        <input class="t-box cal-t datetimepicker form-control"  autocomplete="off" name="date" id="date" value="<?php echo Input::old('date'); ?>" type="text" >

                                        <div><?php if($errors->has('date')): ?><span class="error"><?php echo e($errors->first('date')); ?></span><?php endif; ?> </div>
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label><?php if(Lang::has(Session::get('lang_file').'.COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COMMENTS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COMMENTS')); ?> <?php endif; ?> </label>
                                    <textarea class="text-area form-control" style="min-height: 150px" name="comments" id="comments"><?php echo Input::old('comments'); ?></textarea>

                                    <div><?php if($errors->has('comments')): ?><span class="error"><?php echo e($errors->first('comments')); ?></span><?php endif; ?> </div>


                                </div>
                            </div>
                            <center>
                                <input type="submit" name="submit" value="<?php if(Lang::has(Session::get('lang_file').'.ASK_FOR_A_QUOTE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ASK_FOR_A_QUOTE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ASK_FOR_A_QUOTE')); ?> <?php endif; ?>" />
                            </center>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>



    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker();
        });
    </script>


    <script type="text/javascript">

        function isNumber(evt, element) {

            var charCode = (evt.which) ? evt.which : event.keyCode

            if ( charCode > 31 &&

                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.

                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.

                (charCode < 48 || charCode > 57))

            {



                return false;

            }

            return true;

        }

    </script>
    

    <script>



        var checkPastTime = function(inputDateTime) {

            if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {

                var current = new Date();



                //check past year and month

                if (inputDateTime.getFullYear() < current.getFullYear()) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                }



                // 'this' is jquery object datetimepicker

                // check input date equal to todate date

                if (inputDateTime.getDate() == current.getDate()) {

                    if (inputDateTime.getHours() < current.getHours()) {

                        $('.datetimepicker').datetimepicker('reset');

                    }

                    this.setOptions({

                        minTime: current.getHours() + ':00' //here pass current time hour

                    });

                } else {

                    this.setOptions({

                        minTime: false

                    });

                }

            }

        }


        

    <script>



        var checkPastTime = function(inputDateTime) {

            if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {

                var current = new Date();



                //check past year and month

                if (inputDateTime.getFullYear() < current.getFullYear()) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                }



                // 'this' is jquery object datetimepicker

                // check input date equal to todate date

                if (inputDateTime.getDate() == current.getDate()) {

                    if (inputDateTime.getHours() < current.getHours()) {

                        $('.datetimepicker').datetimepicker('reset');

                    }

                    this.setOptions({

                        minTime: current.getHours() + ':00' //here pass current time hour

                    });

                } else {

                    this.setOptions({

                        minTime: false

                    });

                }

            }



            var currentYear = new Date();

            $('.datetimepicker').datetimepicker({

                format:'d M Y g:i A',

                minDate : 0,

                yearStart : currentYear.getFullYear(), // Start value for current Year selector

                onChangeDateTime:checkPastTime,

                onShow:checkPastTime

            });









        /*

       jQuery(window).load(function()

       {

         jQuery("body").on('mouseover', '.datetimepicker', function() {

           var today = '2018-11-09';

             jQuery(this).datetimepicker({

             ampm: true, // FOR AM/PM FORMAT

             format : 'd M Y g:i A',

             minDate : 0,

           minTime: 0

          });

         });

       })

       */

    </script>

    <style type="text/css">

        a.morelink {

            text-decoration:none;

            outline: none;

        }

        .morecontent span {

            display: none;

        }

    </style>

    <script type="text/javascript">

        jQuery(document).ready(function(){

            //jQuery('#date').datepicker({format: "dd/mm/yyyy"});

            jQuery("#singer_frm").validate({

                rules: {

                    "hall" : {

                        required : true

                    },

                    "occasion" : {

                        required : true

                    },

                    "city" : {

                        required : true

                    },

                    "location" : {

                        required : true

                    },

                    "duration" : {

                        required : true,

                        number: true

                    },

                    "date" : {

                        required : true

                    },

                    "time" : {

                        required : true

                    },

                    /*"comments" : {

                      required:true

                    },*/

                },

                messages: {

                    "hall": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_HALL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_HALL')); ?> <?php endif; ?>"

                    },

                    "occasion": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_OCCASION')); ?> <?php endif; ?>"

                    },

                    "city": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_CITY')); ?> <?php endif; ?>"

                    },

                    "location": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_LOCATION')); ?> <?php endif; ?>"

                    },

                    "duration": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DURATION')); ?> <?php endif; ?>"

                    },

                    "date": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DATE')); ?> <?php endif; ?>"

                    },

                    "time": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_TIME')); ?> <?php endif; ?>"

                    },

                    /*"comments": {

                      required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_COMMENTS_QUERIES')); ?> <?php endif; ?>"

          }, */

                }

            });

            jQuery(".btn-info-wisitech").click(function() {

                if(jQuery("#singer_frm").valid()) {

                    jQuery('#singer_frm').submit();

                }

            });

        });

        $(document).ready(function()

        {

            var showChar = 400;

            var ellipsestext = "...";

            var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";

            var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";

            $('.more').each(function()

            {

                var content = $(this).html();

                if(content.length > showChar)

                {

                    var c = content.substr(0, showChar);

                    var h = content.substr(showChar-1, content.length - showChar);

                    var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

                    $(this).html(html);

                }

            });



            $(".morelink").click(function(){

                if($(this).hasClass("less")) {

                    $(this).removeClass("less");

                    $(this).html(moretext);

                } else {

                    $(this).addClass("less");

                    $(this).html(lesstext);

                }

                $(this).parent().prev().toggle();

                $(this).prev().toggle();

                return false;

            });

        });



    </script>

    <div class="action_popup">

        <div class="action_active_popup">

            <div class="action_content"><?php echo e(Session::get('message')); ?></div>

            <div class="action_btnrow">

                <input type="hidden" id="delid" value=""/>

                <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>

        </div>

    </div>

    <?php if(Session::has('message')): ?>

        <script type="text/javascript">

            $(document).ready(function()

            {

                $('.action_popup').fadeIn(500);

                $('.overlay').fadeIn(500);

            });

        </script>

    <?php endif; ?>

    <script type="text/javascript">

        $('.status_yes').click(function()

        {

            $('.overlay, .action_popup').fadeOut(500);

        });

    </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
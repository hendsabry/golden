<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
  $basecategory_ids =Session::get('searchdata.mainselectedvalue');  
 

?>
<?php $productInsuranceamount=0; ?>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="inner_wrap"> 


 
<?php if( $basecategory_ids ==1): ?>

<!-- for bussiness meeting -->

<div class="search-section">
      <div class="modify-search-form-90">
        <div class="search-box search-box1">
 
 
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.TypeofBusiness')!= '')  ?  trans(Session::get('lang_file').'.TypeofBusiness'): trans($OUR_LANGUAGE.'.TypeofBusiness')); ?></div>
<div class="search-box-field">
<?php echo e($otype); ?>

</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')); ?></div>
<div class="search-box-field"><?php echo e(Session::get('searchdata.noofattendees')); ?></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?></div>
<div class="search-box-field"><?php echo e(Session::get('currency')); ?> <?php echo e(Session::get('searchdata.budget')); ?> </div>
</div> <!-- search-box -->

<div class="search-box search-box4">  
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div>
<div class="search-box-field">
 <?php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; ?>
</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.StartDate')!= '')  ?  trans(Session::get('lang_file').'.StartDate'): trans($OUR_LANGUAGE.'.StartDate')); ?></div>
<div class="search-box-field"><?php echo e(Session::get('searchdata.startdate')); ?></div>
</div> <!-- search-box -->


 <div class="search-box search-box6">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.EndDate')!= '')  ?  trans(Session::get('lang_file').'.EndDate'): trans($OUR_LANGUAGE.'.EndDate')); ?></div>
<div class="search-box-field"><?php echo e(Session::get('searchdata.enddate')); ?></div>
</div> <!-- search-box -->


 <!-- search-box -->

 </div>
</div> <!-- search-form -->



<!-- for bussiness meeting end-->



<?php else: ?>




<!-- for wedding -->


    <div class="search-section">
      <div class="event-result-form">
        <div class="search-box search-box1">
          <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')); ?></div>
          <div class="search-noneedit"> <?php echo e($otype); ?></div>
        </div>
        <!-- search-box -->
        <div class="search-box search-box2">
          <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_ATTENDANCE'): trans($OUR_LANGUAGE.'.NO_OF_ATTENDANCE')); ?></div>
          <div class="search-noneedit"><?php echo e(Session::get('searchdata.noofattendees')); ?></div>
        </div>
        <!-- search-box -->
        <div class="search-box search-box3">
          <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')); ?></div>
          <div class="search-noneedit"><?php echo e(Session::get('currency')); ?> <?php echo e(Session::get('searchdata.budget')); ?></div>
        </div>
        <!-- search-box -->
        <!--<div class="search-box search-box4">
<div class="search-box-label">Gender</div>
<div class="search-noneedit">Female</div>
</div>-->
        <!-- search-box -->
        <div class="search-box search-box5">
          <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div>
          <div class="search-noneedit"><?php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; ?></div>
        </div>
        <!-- search-box -->
        <div class="search-box search-box6">
          <div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')); ?></div>
          <div class="search-noneedit"><?php echo e(Session::get('searchdata.occasiondate')); ?></div>
        </div>
        <div class="search-box search-box6">
<div class="search-box-label"><?php echo e((Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')); ?></div>
<div class="search-noneedit">
 <?php if(Session::get('searchdata.gender')=='male'){ 
    if (Lang::has(Session::get('lang_file').'.MALE')!= '') { echo trans(Session::get('lang_file').'.MALE'); } else  { echo trans($OUR_LANGUAGE.'.MALE'); }
}
?>

<?php if(Session::get('searchdata.gender')=='female'){ 
 if (Lang::has(Session::get('lang_file').'.FEMALE')!= '') { echo trans(Session::get('lang_file').'.FEMALE'); } else  { echo trans($OUR_LANGUAGE.'.FEMALE'); }
}
?>
<?php if(Session::get('searchdata.gender')=='Both'){ 
if (Lang::has(Session::get('lang_file').'.BOTH')!= '') { echo trans(Session::get('lang_file').'.BOTH'); } else  { echo trans($OUR_LANGUAGE.'.BOTH'); }
}
?>
</div>
</div>
        <!-- search-box -->
      </div>
 
      <!-- modify-search-form -->
    </div>



<!-- for wedding -->


 <?php endif; ?>




    <?php     
    $ProIds = Session::get( 'ProIds' ); 
    $ExtraQty = Session::get( 'ExtraQty' ); 
    ?>
    <?php if(isset($ProIds) && count($ProIds)>=1): ?>

  <div class="productExtraqty error" style="padding-top: 10px;"> 
      <ul>
      <?php $i=0; ?>
      <?php $__currentLoopData = $ProIds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pid): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php $pname = Helper::getproductNames($pid); ?>
      <li>Max quantity (<?php echo e($pname); ?>) available is <?php echo e($ExtraQty[$i]); ?></li>
      <?php $i=$i+1; ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </ul>
   </div>
    <?php endif; ?>
 

    <div class="tatal-services-area">
      <div class="mobile-filter-line">
        <div class="mobile-search mobile-search1row"><span>Event Result</span></div>
      </div>
      <div class="tatal-services-inner-area">
        <div class="tatal-services-heading"><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_SERVICES')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_SERVICES'): trans($OUR_LANGUAGE.'.TOTAL_SERVICES')); ?></div>
        <div class="table-cart-wrapper">
          <table class="table-cart tatal-services-table">
            <tr class="tr table_heading_tr">
               
              <td class="table_heading td1"><?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?></td>
			  <td class="table_heading td2"><?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?></td>
              <td class="table_heading td3"><?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?></td>
              <td class="table_heading td4"><?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?></td>
            </tr>
			<?php $k=1; $basetotal=0; ?>
            <tr class="tr mobile-first-row">
              <td colspan="4"><div class="demo">
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <?php   
						if(count($productinfo)>0)
						{
                          $xyz=0;
                  foreach($productinfo as $productinfo)
                  {
                    $xyz++;
                  if(isset($productinfo[0]->pro_discount_percentage) && $productinfo[0]->pro_discount_percentage>0){ $hallprice=$productinfo[0]->pro_disprice; }else{ $hallprice=$productinfo[0]->pro_price; }
                           $remainingamount= ($hallprice*75)/100;
                           $remainingamount=currency($remainingamount, 'SAR',$Current_Currency, $format = false);
                         ?>
						 <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingOne">
                       
                          <table width="100%">
                            <tr class="tr hall-cart-tr ">
							<td colspan="4" class="hall-cart-td">
							<table width="100%">
							<tr>
              


							<td class="td td1 cast-hall mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <a href="#" class="cart-delete" data-pro="hall" data-id="<?php echo e($productinfo[0]->pro_id); ?>">&nbsp;</a>  <?php echo e(isset($productinfo[0]->pro_title) ? $productinfo[0]->pro_title : ''); ?></td>


							<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">&nbsp;</td>
							<td class="td td3 hall-sp" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"> <?php $getC = Helper::getparenthallCat($productinfo[0]->pro_mc_id); ?>
                                <?php echo e($getC->mc_name); ?></td>
							<td class="td td4 cast-hall" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e($hallprice); ?></td>
							</tr>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Advance_Payment_for_Hall_Booking')!= '')  ?  trans(Session::get('lang_file').'.Advance_Payment_for_Hall_Booking'): trans($OUR_LANGUAGE.'.Advance_Payment_for_Hall_Booking')); ?></td>
							<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>">
                <?php
                      if($productinfo[0]->Insuranceamount!='')
                                { $productInsuranceamount=$productinfo[0]->Insuranceamount; }
                                else{ $productInsuranceamount=0;}
                 $finalprice=$hallprice;
                                   //$finalprice=currency($hallprice, 'SAR',$Current_Currency, $format = false); 
                                   
                                   $advanceprice=($finalprice*25)/100;
                                   $remainingammount=$finalprice-$advanceprice;

                                ?>
                                <?php $advance_price=currency($advanceprice, 'SAR',$Current_Currency, $format = false); 
                                  $basetotal= $basetotal + $advance_price;
                                ?>
                               <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($advance_price, 2)); ?></td>
							</tr>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')); ?></td>
							<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>">
                  <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($productInsuranceamount,2)); ?>

              </td>
							</tr>
							 <?php $pst=0; if(count($productservices)>0){ ?>
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">
							 <h4 class="panel-title"> 
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo e($productinfo[0]->pro_mc_id); ?><?php echo e($xyz); ?>" aria-expanded="true" aria-controls="collapseOne<?php echo e($productinfo[0]->pro_mc_id); ?><?php echo e($xyz); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Items_Ordered')!= '')  ?  trans(Session::get('lang_file').'.Items_Ordered'): trans($OUR_LANGUAGE.'.Items_Ordered')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></a> </h4></td>
							
							<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>">
                  <?php   



                  if(count($productservices)>0){ 
                          
                  ?>
                              <?php $__currentLoopData = $productservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adopedproductservices): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $adopedproductservices->getProductOptionValue; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $addoptedamount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                               <?php $pst=$pst+currency($addoptedamount->price, 'SAR',$Current_Currency, $format = false); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php } ?>
                  

                      <?php  if(count($internal_foods)>0){ ?>
                            <?php $__currentLoopData = $internal_foods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internalfoodcost): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <?php 
                                 $itemdishselcted_price=currency($internalfoodcost['final_price'], 'SAR',$Current_Currency, $format = false); 
                                  $pst=$pst+$itemdishselcted_price;
                                 ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php } ?>



              <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($pst,2)); ?></td>
							</tr>
              <?php } ?>
							<?php  
              $basetotal=$basetotal+currency($productInsuranceamount, 'SAR',$Current_Currency, $format = false);
               if(count($productservices)>0){ ?>
							<tr><td colspan="4" class="hall-more-less">
                <div id="collapseOne<?php echo e($productinfo[0]->pro_mc_id); ?><?php echo e($xyz); ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<?php echo e($productinfo[0]->pro_mc_id); ?><?php echo e($xyz); ?>">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
                           
                            
                            <?php   if(count($productservices)>0){ 
                            $k=1; $ij=1; $z=0;
                            
                             //echo "<pre>";
                           //print_r($productservices);
                           //die;
                            ?> 
                            <?php $__currentLoopData = $productservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $adopedproductservices): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $adopedproductservices->getProductOptionValue; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $addoptedservices): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                            <?php
                                $ps=currency($addoptedservices->price, 'SAR',$Current_Currency, $format = false);
                               
                                $basetotal=$basetotal+$ps;
                              
                                ?>
                            <tr class="tr">
                              
                              <td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">
                                
                                 <a href="#" data-id="<?php echo e($addoptedservices->id); ?>" data-pro="hall_attr" class="cart-delete">&nbsp;</a>

                                 <?php $orderservices_name = 'option_title'; 
                                      if(Session::get('lang_file')!='en_lang')
                                    {
                                        $orderservices_name = 'option_title_ar'; 
                                      }
                                  ?>

                                <?php echo e($addoptedservices->$orderservices_name); ?>

                                <!-- <a href="#" class="services-edit">&nbsp;</a>--></td>
								<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  </td>
                              <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Hall_Services')!= '')  ?  trans(Session::get('lang_file').'.Hall_Services'): trans($OUR_LANGUAGE.'.Hall_Services')); ?></td>
                              <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>">
                                  <?php $item_price=currency($addoptedservices->price, 'SAR',$Current_Currency, $format = false); ?>
                                <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($item_price, 2)); ?></td>
                            </tr>
                            <?php $k++; $ij++; $z++; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php } 
                           
                             //$basetotal=($basetotal*25)/100;
                               
                            
                            if(count($internal_foods)>0){ 
                              //dd($internal_foods);
                            ?>
                            <?php $__currentLoopData = $internal_foods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internalfood): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="tr">
                             
                              <td class="td td1" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-pro="hall_internal" data-id="<?php echo e($internalfood['dish_id']); ?>"  >&nbsp;</a><?php echo e($internalfood['dish_name']); ?><br />
                                <?php echo e($internalfood['container_title']); ?> X <?php echo e($internalfood['quantity']); ?>

                                <!--<a href="#" class="services-edit">&nbsp;</a>--></td>
							                 <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">


                            <?php
                            $Qran = rand(111,999);
                            
                            $dishID = $internalfood['dish_id'];
                            $IntQty = $internalfood['quantity'];
                            ?>
                            <!--<div class="quantity-box">
                             <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($dishID) ? $dishID : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','internalfood');"></button>


                           <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e($IntQty); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($dishID) ? $dishID : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','internalfood');" type="number">


                            <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($dishID) ? $dishID : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','internalfood');"></button>
                            </div> -->
                              <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($dishID); ?>">
                   <span id="<?php echo e(isset($dishID) ? $dishID : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
                                </td>
                              


                              <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"> </td>


                              <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?>

                                <?php $itemselcted_price=currency($internalfood['final_price'], 'SAR',$Current_Currency, $format = false); ?>
                               <?php echo e(number_format($itemselcted_price, 2)); ?></td>
                            </tr>
                            <?php $basetotal=$basetotal+$itemselcted_price; ?>
                            <?php $k++; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php }  ?>
                          </table>
                        </div>
                      </div></td></tr>
							          <?php }  ?>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Total_Cost')!= '')  ?  trans(Session::get('lang_file').'.Total_Cost'): trans($OUR_LANGUAGE.'.Total_Cost')); ?></td>
							<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>">
                <?php 
                    $totalcast=$finalprice+$productInsuranceamount+$pst;
                 ?>
             <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($totalcast,2)); ?></td>
							</tr>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Payable_Amount')!= '')  ?  trans(Session::get('lang_file').'.Payable_Amount'): trans($OUR_LANGUAGE.'.Payable_Amount')); ?></td>
							<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>">

                 <?php   
                    $totalpayable=$advance_price+$productInsuranceamount+$pst;
                 ?>
              <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($totalpayable,2)); ?></td>
							</tr>
														
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Balance_Payment')!= '')  ?  trans(Session::get('lang_file').'.Balance_Payment'): trans($OUR_LANGUAGE.'.Balance_Payment')); ?></td>
							<td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"> <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($remainingammount,2)); ?></td>
							</tr>
							
							</table>
							</td>
							
                            </tr>
                          </table>
                        
                      </div>
                      <?php $k++; } } ?>
                      
						</div>
                      <!----cart BUFFET food ---->
                      <?php 
                    
                      if(count(@$cart_fooditem) > 0){ 
                      $f=1;
                      $foodtotal=0;

                      ?>
                      <?php if(count($cart_fooditem)>0){
                        

                       ?>
                      <?php $__currentLoopData = $cart_fooditem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartfoodbasictotal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php if(count($cartfoodbasictotal->cart_external_food_dish)>0){

                           ?>
                            <?php $__currentLoopData = $cartfoodbasictotal->cart_external_food_dish; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $extrenalprice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                             <?php $foodtotal=$foodtotal+$extrenalprice->price; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              <?php }  ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php }  ?>
                     
                     


					  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
                            
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">
                         

                                <?php echo e((Lang::has(Session::get('lang_file').'.CARTBUFFET')!= '')  ?  trans(Session::get('lang_file').'.CARTBUFFET'): trans($OUR_LANGUAGE.'.CARTBUFFET')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 hall-info-price mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($foodtotal, 2)); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
                            <?php if(count(@$cart_fooditem)>0){
                           
                            ?>
                            <?php $k=0;   ?>
                            <?php $__currentLoopData = $cart_fooditem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartfoodbasicinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            

                            <?php  
                            if(Session::get('lang_file')=='ar_lang'){ $protitle=$cartfoodbasicinfo->getProduct[0]->pro_title_ar; }else{ $protitle=$cartfoodbasicinfo->getProduct[0]->pro_title; } 
                            
                            if(Session::get('lang_file')=='ar_lang'){
if(count($cartfoodbasicinfo->branch_info)>=1){
                             $promctitle=$cartfoodbasicinfo->branch_info[0]->mc_name_ar;
}else {   $promctitle= ''; }
                              }else{ 
if(count($cartfoodbasicinfo->branch_info)>=1){
                              $promctitle=$cartfoodbasicinfo->branch_info[0]->mc_name; 
                            }else {   $promctitle= ''; }
                            } 
                            $jk=0;
                           
                            ?>
                            
                            <?php $__currentLoopData = $cartfoodbasicinfo->cart_external_food_dish; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderdishinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="tr">
                      
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-pro="buffetdelfood" data-id="<?php echo e(isset($orderdishinfo->id) ? $orderdishinfo->id : 0); ?>" data-baseid="<?php echo e(isset($cartfoodbasicinfo->id) ? $cartfoodbasicinfo->id : ''); ?>">&nbsp;</a>

                                <?php echo e(isset($protitle) ? $protitle : ''); ?><br>

                                <?php echo e(isset($cartfoodbasicinfo->dish_container_info[$k][0]->short_name) ? $cartfoodbasicinfo->dish_container_info[$k][0]->short_name : ''); ?> X <?php echo e(isset($orderdishinfo->quantity) ? $orderdishinfo->quantity : ''); ?>


                                 <?php $k=$k+1; ?>
                                <!-- <a href="#" class="services-edit">&nbsp;</a>--></td>
								 <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">  <?php
              $Qran = rand(111,999);
                    $dishID = $orderdishinfo->id;
                    $IntQty = $orderdishinfo->quantity;
                    $cart_product_root_id=$cartfoodbasicinfo->id;
              ?>    
                     <div class="quantity-box">
                   <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($dishID) ? $dishID : 0); ?>','<?php echo e($cart_product_root_id); ?>','remove','<?php echo e($Qran); ?>','buffetfood');"></button>


                            <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e($IntQty); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($dishID) ? $dishID : 0); ?>','<?php echo e($cart_product_root_id); ?>','pricewithqty','<?php echo e($Qran); ?>','buffetfood');" type="number">


                            <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($dishID) ? $dishID : 0); ?>','<?php echo e($cart_product_root_id); ?>','add','<?php echo e($Qran); ?>','buffetfood');"></button>
                            </div> 
                              <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($dishID); ?>">
                   <span id="<?php echo e(isset($dishID) ? $dishID : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>







                 </td>
                              <td class="td td3" data-title="Service Provider"><?php echo e(isset($promctitle) ? $promctitle : ''); ?></td>
                              <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($orderdishinfo->price, 2)); ?></td>
                            </tr>
                            <?php 

                            $basetotal=$basetotal+$orderdishinfo->price;
                            $jk++; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <?php $f++;
                            
                            ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php }  ?>
                            
                           
                            
                          </table>
                        </div>
                      </div>
                     </div>
					  <?php } ?>
                      <!----end cart BUFFET food ---->


                      <!----start cart Dates food ---->
                        <?php     
                                      
                      if(count(@$cart_fooddateitem) > 0){ 
                        $f=1;
                      $foodtotal=0; ?>
                     <?php if(count($cart_fooddateitem) > 0){ ?>
                      <?php $__currentLoopData = $cart_fooddateitem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartdatefoodbasicinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php $productdateprice=$cartdatefoodbasicinfo->cart_option_value[0]->quantity * $cartdatefoodbasicinfo->cart_option_value[0]->value;
                      $foodtotal=$foodtotal+$productdateprice; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                      
                      <?php }  ?>
                        <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingdates">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsedate" aria-expanded="true" aria-controls="collapsedate">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
                            
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">
                                         <?php echo e((Lang::has(Session::get('lang_file').'.CARTDATES')!= '')  ?  trans(Session::get('lang_file').'.CARTDATES'): trans($OUR_LANGUAGE.'.CARTDATES')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
                <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 hall-info-price mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($foodtotal, 2)); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsedate" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingdates">
                        <div class="panel-body">
                          <table width="100%" class="child-table">                      

                             <?php if(count($cart_fooddateitem)>0){                             
                            ?>
                            <?php $__currentLoopData = $cart_fooddateitem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartdatefoodbasicinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php 
                           $productprice=$cartdatefoodbasicinfo->cart_option_value[0]->quantity * $cartdatefoodbasicinfo->cart_option_value[0]->value;
                          
                            if($cartdatefoodbasicinfo->cart_option_value[0]->product_option_id=='22'){ 
                            if(Lang::has(Session::get('lang_file').'.KG')!= ''){ $wt=trans(Session::get('lang_file').'.KG');}else{ $wt=trans($OUR_LANGUAGE.'.KG'); }
                             }else{ $wt="";} 
                            ?>

                             <?php  
                            if(Session::get('lang_file')=='ar_lang'){ $prodatestitle=$cartdatefoodbasicinfo->getProduct[0]->pro_title_ar;
                            $pmc=$cartdatefoodbasicinfo->shop_info[0]->mc_name_ar;
                             }else{ $prodatestitle=$cartdatefoodbasicinfo->getProduct[0]->pro_title; 
                              $pmc=$cartdatefoodbasicinfo->shop_info[0]->mc_name;
                           } 
                            ?>
                            <tr class="tr">
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">
                                <a href="#" class="cart-delete" data-pro="datesfood" data-id="<?php echo e(isset($cartdatefoodbasicinfo->cart_option_value[0]->product_id) ? $cartdatefoodbasicinfo->cart_option_value[0]->product_id : 0); ?>">&nbsp;</a>
                                <?php echo e(isset($prodatestitle) ? $prodatestitle : ''); ?><br>
                                <?php if($cartdatefoodbasicinfo->cart_option_value[0]->product_option_id=='22'){ ?>
                                <?php echo e(isset($cartdatefoodbasicinfo->cart_option_value[0]->quantity) ? $cartdatefoodbasicinfo->cart_option_value[0]->quantity : ''); ?> <?php echo e(isset($wt) ? $wt : ''); ?>

                                  <?php } ?>
                              </td>
                
                                      <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">  <?php
              $Qran = rand(111,999);
              ?>

                    <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($cartdatefoodbasicinfo->id) ? $cartdatefoodbasicinfo->id : 0); ?>','','remove','<?php echo e($Qran); ?>','food');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($cartdatefoodbasicinfo->cart_option_value[0]->quantity) ? $cartdatefoodbasicinfo->cart_option_value[0]->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($cartdatefoodbasicinfo->id) ? $cartdatefoodbasicinfo->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','food');" type="number" readonly>
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($cartdatefoodbasicinfo->id) ? $cartdatefoodbasicinfo->id : 0); ?>','','add','<?php echo e($Qran); ?>','food');"></button>
                </div> 
                <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($cartdatefoodbasicinfo->cart_option_value[0]->product_id); ?>">
                   <span id="<?php echo e(isset($cartdatefoodbasicinfo->id) ? $cartdatefoodbasicinfo->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>

                 </td>
                   
                              <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($pmc) ? $pmc : ''); ?></td>
                              <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($productprice,2)); ?></td>
                            </tr>
                            <?php $f++;
                           $basetotal=$basetotal+$productprice;
              
                            ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                            <?php }   ?>
                           
                            
                          </table>
                        </div>
                      </div>
                     </div>
                     <?php } ?>
                      <!----end cart Dates food ---->








                         <!----start cart Desserts food ---->
                        <?php                     
                      if(count(@$cart_fooddessertitem) > 0){ 
                        $f=1;
                      $foodtotal=0; ?>
                     <?php  if(count($cart_fooddessertitem)>0){ 

                      ?>
                      <?php $__currentLoopData = $cart_fooddessertitem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartdessertfoodbasicinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
if(isset($cartdessertfoodbasicinfo->cart_option_value) && count($cartdessertfoodbasicinfo->cart_option_value) >=1)
{
  //$productdessertprice=$cartdessertfoodbasicinfo->cart_option_value[0]->quantity * $cartdessertfoodbasicinfo->cart_option_value[0]->value;
   $productdessertprice=$cartdessertfoodbasicinfo->total_price;
}
else
{
  $productdessertprice=1;
}
                    


                      $foodtotal=$foodtotal+$productdessertprice; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php }   ?>
                        <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingdesserts">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsedesserts" aria-expanded="true" aria-controls="collapsedate">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
                            
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">
                                         <?php echo e((Lang::has(Session::get('lang_file').'.CARTDESSERTS')!= '')  ?  trans(Session::get('lang_file').'.CARTDESSERTS'): trans($OUR_LANGUAGE.'.CARTDESSERTS')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
                <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 hall-info-price mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($foodtotal, 2)); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsedesserts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingdesserts">
                        <div class="panel-body">
                          <table width="100%" class="child-table">                      

                                <?php if(count($cart_fooddessertitem)>0){ 
                                  $ijk=0;
                                  
                                ?>
                            <?php $__currentLoopData = $cart_fooddessertitem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartdessertfoodbasicinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php  

                              
                             //$productpricedessert=$cartdessertfoodbasicinfo->cart_option_value[0]->quantity * $cartdessertfoodbasicinfo->cart_option_value[0]->value;


                               $productpricedessert=$cartdessertfoodbasicinfo->total_price;

                            if($cartdessertfoodbasicinfo->cart_option_value[0]->product_option_id=='21'){ 

                             if(Lang::has(Session::get('lang_file').'.KG')!= ''){ $wt3=trans(Session::get('lang_file').'.KG');}else{ $wt3=trans($OUR_LANGUAGE.'.KG'); }
                                    }else{ $wt3="";} 
                            ?>

                             <?php  
                            if(Session::get('lang_file')=='ar_lang'){ $prodessertstitle=$cartdessertfoodbasicinfo->getProduct[0]->pro_title_ar;
                            $pmcdessert=$cartdessertfoodbasicinfo->shop_info[0]->mc_name_ar;
                             }else{ $prodessertstitle=$cartdessertfoodbasicinfo->getProduct[0]->pro_title; 
                              $pmcdessert=$cartdessertfoodbasicinfo->shop_info[0]->mc_name;
                           } 
                            ?>
                            <tr class="tr">
                           
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">
                                <a href="#" class="cart-delete" data-pro="dessertfood" data-id="<?php echo e(isset($cartdessertfoodbasicinfo->cart_option_value[0]->product_id) ? $cartdessertfoodbasicinfo->cart_option_value[0]->product_id : 0); ?>">&nbsp;</a>
                                <?php echo e(isset($prodessertstitle) ? $prodessertstitle : ''); ?><br>
                                <?php if($cartdessertfoodbasicinfo->cart_option_value[0]->product_option_id=='21'){ ?>
                                <?php echo e(isset($cartdessertfoodbasicinfo->cart_option_value[0]->quantity) ? $cartdessertfoodbasicinfo->cart_option_value[0]->quantity : ''); ?> <?php echo e($wt3); ?>

                                <?php } ?>
                              </td>
                 <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">  <?php
              $Qran = rand(111,999);
              ?>


                   <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($cartdessertfoodbasicinfo->id) ? $cartdessertfoodbasicinfo->id : 0); ?>','','remove','<?php echo e($Qran); ?>','food');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($cartdessertfoodbasicinfo->quantity) ? $cartdessertfoodbasicinfo->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($cartdessertfoodbasicinfo->id) ? $cartdessertfoodbasicinfo->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','food');" type="number" readonly>
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($cartdessertfoodbasicinfo->id) ? $cartdessertfoodbasicinfo->id : 0); ?>','','add','<?php echo e($Qran); ?>','food');"></button>
                </div> 
                <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($cartdessertfoodbasicinfo->cart_option_value[0]->product_id); ?>">
                   <span id="<?php echo e(isset($cartdessertfoodbasicinfo->id) ? $cartdessertfoodbasicinfo->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>



                 </td>
                   
                              <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($pmcdessert) ? $pmcdessert : ''); ?></td>
                              <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($productpricedessert,2)); ?></td>
                            </tr>
                            <?php $f++;
                            $ijk++;
                            $basetotal=$basetotal+$productpricedessert;
                            ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                            <?php }   ?>
                           
                            
                          </table>
                        </div>
                      </div>
                     </div>
                     <?php } ?>
                      <!----end cart Desserts food ---->

























					
                      <!-------- Tailor Start------------------>
					  <?php $totalsum = 0; if(count($carttailortitem) > 0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.TAILOR')!= '')  ?  trans(Session::get('lang_file').'.TAILOR'): trans($OUR_LANGUAGE.'.TAILOR')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($carttailortitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($carttailortitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $bodymesurmentdata = Helper::bodymesurment($val->cart_id,$val->product_id);
                $fabric_name_data = Helper::fabricname($val->product_id,$val->fabric_name);
                $shopname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $tailorshopname=$shopname->mc_name;
             }else{
             $tailorshopname=$shopname->mc_name_ar;
           }
               
						   $basetotal = $basetotal + $val->total_price;
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                                <div class="bodymesurment">
                                  <ul>
								  <?php if(isset($fabric_name_data->option_title) && $fabric_name_data->option_title!=''){ ?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.Fabrics')!= '')  ?  trans(Session::get('lang_file').'.Fabrics'): trans($OUR_LANGUAGE.'.Fabrics')); ?> - <?php echo e($fabric_name_data->option_title); ?> </li>
                                  <?php } ?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')); ?>- <?php echo e($bodymesurmentdata->length); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')); ?>- <?php echo e($bodymesurmentdata->chest_size); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')); ?>- <?php echo e($bodymesurmentdata->waistsize); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')); ?>- <?php echo e($bodymesurmentdata->soulders); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')); ?>- <?php echo e($bodymesurmentdata->neck); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')); ?>- <?php echo e($bodymesurmentdata->arm_length); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>

                                     <li><?php echo e((Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')); ?>- <?php echo e($bodymesurmentdata->wrist_diameter); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
                                     <!-- <li><?php echo e((Lang::has(Session::get('lang_file').'.CUTTING')!= '')  ?  trans(Session::get('lang_file').'.CUTTING'): trans($OUR_LANGUAGE.'.CUTTING')); ?>- 

                                          <?php if($bodymesurmentdata->cutting=='Ample'){ ?>
                                            <?php echo e((Lang::has(Session::get('lang_file').'.AMPLE')!= '')  ?  trans(Session::get('lang_file').'.AMPLE'): trans($OUR_LANGUAGE.'.AMPLE')); ?>

                                          <?php } ?>
                                     
                                             <?php if($bodymesurmentdata->cutting=='Normal'){ ?>
                                            <?php echo e((Lang::has(Session::get('lang_file').'.NORMAL')!= '')  ?  trans(Session::get('lang_file').'.NORMAL'): trans($OUR_LANGUAGE.'.NORMAL')); ?>

                                          <?php } ?>

                                           <?php if($bodymesurmentdata->cutting=='Curved'){ ?>
                                           <?php echo e((Lang::has(Session::get('lang_file').'.CURVED')!= '')  ?  trans(Session::get('lang_file').'.CURVED'): trans($OUR_LANGUAGE.'.CURVED')); ?>

                                          <?php } ?>

                                       </li>-->
                                  </ul>

                                </div>
                              </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	
 <?php
              $Qran = rand(111,999);
              ?>
               <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basetailorprice149','remove','<?php echo e($Qran); ?>','tailor');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basetailorprice149','pricewithqty','<?php echo e($Qran); ?>','tailor');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basetailorprice149','add','<?php echo e($Qran); ?>','tailor');"></button>
                </div> 

                 </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> /
                <?php echo e(isset($tailorshopname) ? $tailorshopname : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  </div>
					  <?php } ?>
                      <!-------- Tailor end------------------>					
					
                      <!-------- Electronics Start------------------>
					  <?php $totalsum = 0; if(count($cartinvitationsitem) > 0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingelectronics">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseelectronics" aria-expanded="true" aria-controls="collapseelectronics">
                          <table width="100%">
                          <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.ELECTRONIC_INVITATION')!= '')  ?  trans(Session::get('lang_file').'.ELECTRONIC_INVITATION'): trans($OUR_LANGUAGE.'.ELECTRONIC_INVITATION')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartinvitationsitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseelectronics" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingelectronics">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($cartinvitationsitem as $val)
						  { 
						   $getVendorName = Helper::getbranchmgr($val->merchant_id);
						   $basetotal = $basetotal + $val->total_price;

               $cartinvitationinfo=Helper::getcartinvitationinfo($val->cart_id);

               if(Session::get('lang_file')=='en_lang')
                 {
                   $proname=$cartinvitationinfo->pro_label;
                 }
                 else
                 {
                   $proname=$cartinvitationinfo->pro_label_ar;
                   }    

						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?>

                                  <?php if(isset($cartinvitationinfo->date) && $cartinvitationinfo->date!=''): ?>
                                <br>
                                <?php $newinvi_Date = date("d M Y", strtotime($cartinvitationinfo->date)); ?>
                                  <?php echo e($newinvi_Date); ?> / <?php echo e($cartinvitationinfo->time); ?> 
                                    <?php endif; ?>
                                     <?php if(isset($cartinvitationinfo->occasion_name) && $cartinvitationinfo->occasion_name!=''): ?>
                                  <br>
                                  <?php echo e($proname); ?> - <?php echo e($cartinvitationinfo->occasion_name); ?>

                                      <?php endif; ?>
									  <?php if(isset($cartinvitationinfo->venue) && $cartinvitationinfo->venue!=''): ?>
                                  <br>
                                    <?php echo e((Lang::has(Session::get('lang_file').'.VENUE')!= '')  ?  trans(Session::get('lang_file').'.VENUE'): trans($OUR_LANGUAGE.'.VENUE')); ?>- <?php echo e($cartinvitationinfo->venue); ?>

                                      <?php endif; ?>
									   <?php if(isset($cartinvitationinfo->invitation_msg) && $cartinvitationinfo->invitation_msg!=''): ?>
                                  <br>
                                    <?php echo e((Lang::has(Session::get('lang_file').'.MESSAGE')!= '')  ?  trans(Session::get('lang_file').'.MESSAGE'): trans($OUR_LANGUAGE.'.MESSAGE')); ?>- <?php echo e($cartinvitationinfo->invitation_msg); ?>

                                      <?php endif; ?>
                              </td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  </div>
					  <?php } ?>
                      <!-------- Electronics end------------------>				  
					  
					  <!-------- Abaya Start------------------>
					  <?php $totalsum = 0;  if(count($cartabayatitem) >0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseabaya" aria-expanded="true" aria-controls="collapseabaya">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.ABAYA')!= '')  ?  trans(Session::get('lang_file').'.ABAYA'): trans($OUR_LANGUAGE.'.ABAYA')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartabayatitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseabaya" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingabaya">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($cartabayatitem as $val)
						  { 
						       $getVendorName     = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
							   $bodymesurmentdata = Helper::bodymesurment($val->cart_id,$val->product_id);
							   $fabric_name_data  = Helper::fabricname($val->product_id,$val->fabric_name);
							   $shopname          = Helper::getparentCat($val->shop_id);
							   if(Session::get('lang_file')=='en_lang')
							   {
							     $tailorshopname=$shopname->mc_name;
							   }
							   else
							   {
							     $tailorshopname=$shopname->mc_name_ar;
						       }			   
						       $basetotal = $basetotal + $val->total_price;
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                                <?php echo e(isset($val->product_size) ? $val->product_size : ''); ?>

							  <div class="bodymesurment">
                                  <ul>
								  <?php if(isset($fabric_name_data->option_title) && $fabric_name_data->option_title!=''){ ?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.Fabrics')!= '')  ?  trans(Session::get('lang_file').'.Fabrics'): trans($OUR_LANGUAGE.'.Fabrics')); ?> - <?php echo e($fabric_name_data->option_title); ?></li>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.FABRICS_PRICE')!= '')  ?  trans(Session::get('lang_file').'.FABRICS_PRICE'): trans($OUR_LANGUAGE.'.FABRICS_PRICE')); ?> -  <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($fabric_name_data->price,2)); ?></li>
                                  <?php }if(isset($bodymesurmentdata->length) && $bodymesurmentdata->length!=''){?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')); ?>- <?php echo e($bodymesurmentdata->length); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
									<?php } if(isset($bodymesurmentdata->chest_size) && $bodymesurmentdata->chest_size!=''){?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')); ?>- <?php echo e($bodymesurmentdata->chest_size); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
									<?php } if(isset($bodymesurmentdata->waistsize) && $bodymesurmentdata->waistsize!=''){?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')); ?>- <?php echo e($bodymesurmentdata->waistsize); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
									<?php } if(isset($bodymesurmentdata->soulders) && $bodymesurmentdata->soulders!=''){?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')); ?>- <?php echo e($bodymesurmentdata->soulders); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
									<?php } if(isset($bodymesurmentdata->neck) && $bodymesurmentdata->neck!=''){?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')); ?>- <?php echo e($bodymesurmentdata->neck); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
									<?php } if(isset($bodymesurmentdata->arm_length) && $bodymesurmentdata->arm_length!=''){?>
                                    <li><?php echo e((Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')); ?>- <?php echo e($bodymesurmentdata->arm_length); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
                                    <?php } if(isset($bodymesurmentdata->wrist_diameter) && $bodymesurmentdata->wrist_diameter!=''){?>
                                     <li><?php echo e((Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')); ?>- <?php echo e($bodymesurmentdata->wrist_diameter); ?> <?php echo e((Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')); ?></li>
									 <?php } ?>
                                  </ul>
                                </div>
							  </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 <?php
              $Qran = rand(111,999);

              if($val->product_size!=''){ $prodtype='dress'; }else{ $prodtype='abaya'; }
              ?>
                <div class="quantity-box">

                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','<?php echo e(isset($val->product_size) ? $val->product_size : ''); ?>','remove','<?php echo e($Qran); ?>','<?php echo e($prodtype); ?>');"></button>
                <input name="itemqty[]" id="qty<?php echo e($Qran); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','<?php echo e(isset($val->product_size) ? $val->product_size : ''); ?>','pricewithqty','<?php echo e($Qran); ?>','<?php echo e($prodtype); ?>');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','<?php echo e(isset($val->product_size) ? $val->product_size : ''); ?>','add','<?php echo e($Qran); ?>','<?php echo e($prodtype); ?>');"></button>
                </div> 
                 <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>


              </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($tailorshopname) ? $tailorshopname : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  </div>
					  <?php } ?>
                      <!-------- Abaya end------------------>
					  
					  <!-------- Car Rental Start------------------>
					  <?php $totalsum = 0; if(count($cartcarrentaltitem) >0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsecar_rental" aria-expanded="true" aria-controls="collapsecar_rental">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.CAR_RENTAL')!= '')  ?  trans(Session::get('lang_file').'.CAR_RENTAL'): trans($OUR_LANGUAGE.'.CAR_RENTAL')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartcarrentaltitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsecar_rental" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingcar_rental">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
              
              //print_r($cartcarrentaltitem);
						  foreach($cartcarrentaltitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
              
               $shopname=Helper::getparentCat($val->shop_id);
						   $basetotal = $basetotal + $val->total_price;


                if(Session::get('lang_file')=='en_lang'){
               $shopname=$shopname->mc_name;
               $getVendorName=$getVendorName->mer_fname;
             }else{
              $shopname=$shopname->mc_name_ar;
              $getVendorName=$getVendorName->mer_fname_ar;
           }


						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?>

                                  <?php if(count($val->bookedandrentaldate)>0) { 
                                    $ijk=1;
                                  echo "<br>";
                                    foreach($val->bookedandrentaldate as $bookedreturndate){
                                    ?>
                                        <?php  $newDate = date("d M Y", strtotime($bookedreturndate->rental_date)); 
                                                $newreturn_Date = date("d M Y", strtotime($bookedreturndate->return_date));
                                        ?>
                                       <?php echo e($newDate); ?> - <?php echo e($newreturn_Date); ?> 

                                  <?php $ijk++; } } ?>

                              </td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 <?php
              $Qran = rand(111,999);
              ?>
                 </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName) ? $getVendorName : ''); ?> / <?php echo e(isset($shopname) ? $shopname : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					 </div>
					  <?php } ?>
                      <!-------- Car Rental end------------------>
					 
					  <!-------- Travel Agency Start------------------>
					  <?php $totalsum = 0; if(count($carttraveltitem) > 0){ ?>
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingtravel">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetravel" aria-expanded="true" aria-controls="collapsetravel">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.TRAVEL_AGENCY')!= '')  ?  trans(Session::get('lang_file').'.TRAVEL_AGENCY'): trans($OUR_LANGUAGE.'.TRAVEL_AGENCY')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($carttraveltitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsetravel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtravel">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
              //dd($carttraveltitem);
						  foreach($carttraveltitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
               if(Session::get('lang_file')=='en_lang'){
               $shopname=$shopname->mc_name;
               $getVendorName=$getVendorName->mer_fname;
             }else{
              $shopname=$shopname->mc_name_ar;
              $getVendorName=$getVendorName->mer_fname;
           }

						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                                <?php echo e((Lang::has(Session::get('lang_file').'.NO_OF_PEOPLE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_PEOPLE'): trans($OUR_LANGUAGE.'.NO_OF_PEOPLE')); ?> <?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>


                             <br>
                             <?php
                             //echo "<pre>";
                             //print_r($carttraveltitem); 
                             ?>
 <?php if(isset($carttraveltitem->product_rent->return_date) && $carttraveltitem->product_rent->return_date !=''): ?>

 <?php echo e((Lang::has(Session::get('lang_file').'.PACKAGE_DATE')!= '')  ?  trans(Session::get('lang_file').'.PACKAGE_DATE'): trans($OUR_LANGUAGE.'.PACKAGE_DATE')); ?>



       <?php echo e(Carbon\Carbon::parse($carttraveltitem->product_rent->return_date)->format('h:i A d M Y')); ?>


    <?php endif; ?>                 
                              
                              </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 <?php
              $Qran = rand(111,999);
              ?>
                


                 </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName) ? $getVendorName : ''); ?> / <?php echo e(isset($shopname) ? $shopname : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					 </div>
					  <?php } ?>
                      <!-------- Travel Agency end------------------>
					  
					  <!-------- Kosha Start------------------>
					  <?php $totalsum = 0; if(count($cartcoshaitem) >0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingkosha">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsekosha" aria-expanded="true" aria-controls="collapsekosha">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.KOSHA')!= '')  ?  trans(Session::get('lang_file').'.KOSHA'): trans($OUR_LANGUAGE.'.KOSHA')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							 <td class="td td2 sp-blank"></td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartcoshaitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsekosha" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingkosha">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($cartcoshaitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->shop_vendor_id);

                 $getVendor_Name=$getVendorName->mer_fname;


						   $basetotal = $basetotal + $val->total_price;
                 $shopname=Helper::getparentCat($val->category_id);
                 if(Session::get('lang_file')=='en_lang'){
               $shopname=$shopname->mc_name;               
              
             }else{
              $shopname=$shopname->mc_name_ar;
             
           } 
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->cart_product_id) ? $val->cart_product_id : 0); ?>" data-pro="cosha">&nbsp;</a><?php echo e($val->pro_title); ?></td>
							 

                <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	

              <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->cart_product_id) ? $val->cart_product_id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','kosha');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->cart_product_id) ? $val->cart_product_id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','kosha');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->cart_product_id) ? $val->cart_product_id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','kosha');"></button>
                </div> 

 <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>

  </td>



							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e($getVendor_Name); ?>  / <?php echo e($shopname); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					 </div>
					  <?php } ?>
                      <!-------- Kosha end------------------>
					  
					  <!-------- Photo Graphy Start------------------>
					  <?php 

             $totalsum = 0; if(count($cartphotoitem) >0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingphotography">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsephotography" aria-expanded="true" aria-controls="collapsephotography">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.PhotoGraphy')!= '')  ?  trans(Session::get('lang_file').'.PhotoGraphy'): trans($OUR_LANGUAGE.'.PhotoGraphy')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartphotoitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsephotography" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingphotography">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
            // echo "<pre>";
             // print_r($cartphotoitem);
						  foreach($cartphotoitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
               $getbookinginfor=Helper::getbookinginformation($val->cart_id,$val->product_id);

               if(Session::get('lang_file')=='en_lang'){
               $shopname=$shopname->mc_name;
               $getVendorName=$getVendorName->mer_fname;
             }else{
              $shopname=$shopname->mc_name_ar;
              $getVendorName=$getVendorName->mer_fname;
           }

               
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                        <?php if(isset($getbookinginfor->date) && $getbookinginfor->date!=''): ?>

                        <?php echo e(Carbon\Carbon::parse($getbookinginfor->date)->format('d M Y')); ?><br>
                        <?php endif; ?>
                        <?php echo e(isset($getbookinginfor->time) ? $getbookinginfor->time : ''); ?><br><?php echo e(isset($getbookinginfor->elocation) ? $getbookinginfor->elocation : ''); ?>


                              </td> 
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	

             <?php
              $Qran = rand(111,999);
              ?>
               

  </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName) ? $getVendorName : ''); ?> / <?php echo e(isset($shopname) ? $shopname : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  </div>
					  <?php } ?>
                      <!-------- Photo Graphy end------------------>
					  
					  <!-------- Video Graphy Start------------------>
					  <?php $totalsum = 0; if(count($cartvideographyitem) >0){ ?>
                        <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingvideography">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsevideography" aria-expanded="true" aria-controls="collapsevideography">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.VideoGraphy')!= '')  ?  trans(Session::get('lang_file').'.VideoGraphy'): trans($OUR_LANGUAGE.'.VideoGraphy')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartvideographyitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsevideography" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingvideography">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($cartvideographyitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
               $getbookinginfor=Helper::getbookinginformation($val->cart_id,$val->product_id);
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                               <?php echo e(Carbon\Carbon::parse($getbookinginfor->date)->format('d M Y')); ?><br>
                                  <?php echo e($getbookinginfor->time); ?><br><?php echo e($getbookinginfor->elocation); ?></td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	

        <?php
              $Qran = rand(111,999);
              ?>            


  </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($shopname->mc_name) ? $shopname->mc_name : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					 </div>
					  <?php } ?>
                      <!-------- Video Graphy end------------------>
					  
					  
					  <!-------- Roses Start------------------>
					  <?php $totalsum = 0; if(count($cartrosesitem) >0){ ?>
                        <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingroses">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseroses" aria-expanded="true" aria-controls="collapseroses">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.SpecialRoses')!= '')  ?  trans(Session::get('lang_file').'.SpecialRoses'): trans($OUR_LANGUAGE.'.SpecialRoses')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartrosesitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseroses" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingroses">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($cartrosesitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
                //echo $val->product_id.'------------'.$val->cart_id;
               $getroseotions=Helper::getroseotion($val->product_id,$val->cart_id);

             //print_r($getroseotions);

						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>

                                  <?php /*
                                         if(isset($getroseotions) && count($getroseotions)>0){ 
                                         $jk=0;
                                           

                                           foreach($getroseotions as $prodotions){


                                               if($jk==0){
                                                if(Lang::has(Session::get('lang_file').'.Wrapping_Type')!= ''){ $wraptypedesign= trans(Session::get('lang_file').'.Wrapping_Type');  }else{ $wraptypedesign= trans($OUR_LANGUAGE.'.Wrapping_Type'); }  
                                          }else{
                                                 if(Lang::has(Session::get('lang_file').'.Wrapping_Design')!= ''){ $wraptypedesign= trans(Session::get('lang_file').'.Wrapping_Design');  }else{ $wraptypedesign= trans($OUR_LANGUAGE.'.Wrapping_Design'); } 
                                        }
                                                     echo $wraptypedesign.'- '.$prodotions->option_title."<br>";
                                                 $jk++;  }
                                         }
										 */
                                  ?>
							  <?php 
							   $serviceInfo         = Helper::getProduckInfo($val->product_id);
							   $getPackage          = Helper::getPackage($val->shop_id,$val->product_id);									
								 if(isset($serviceInfo->packege) && $serviceInfo->packege=='yes')
								 {
								  $wrappingType        = Helper::setExtraProductField($val->product_id,$val->merchant_id,$option_id=25);
								  $wrappingDesign      = Helper::setExtraProductField($val->product_id,$val->merchant_id,$option_id=26);
								 ?>
								<?php echo e((Lang::has(Session::get('lang_file').'.WRAPPING_TYPE')!= '')  ?  trans(Session::get('lang_file').'.WRAPPING_TYPE'): trans($OUR_LANGUAGE.'.WRAPPING_TYPE')); ?>:
								<?php if(isset($wrappingType->option_title) && $wrappingType->option_title!=''){echo $wrappingType->option_title;}else{echo 'N/A';} ?>
								<br />
								<?php echo e((Lang::has(Session::get('lang_file').'.WRAPPING_DESIGN')!= '')  ?  trans(Session::get('lang_file').'.WRAPPING_DESIGN'): trans($OUR_LANGUAGE.'.WRAPPING_DESIGN')); ?>:
								<?php if(isset($wrappingDesign->option_title) && $wrappingDesign->option_title!=''){echo $wrappingDesign->option_title;}else{echo 'N/A';} ?>
								
								 <?php 
								} 
								if(isset($serviceInfo->packege) && $serviceInfo->packege=='no')
								{
								 if(isset($getroseotions) && count($getroseotions)>0){ 
                                         $jk=0;
                                           

                                           foreach($getroseotions as $prodotions){


                                               if($jk==0){
                                                if(Lang::has(Session::get('lang_file').'.Wrapping_Type')!= ''){ $wraptypedesign= trans(Session::get('lang_file').'.Wrapping_Type');  }else{ $wraptypedesign= trans($OUR_LANGUAGE.'.Wrapping_Type'); }  
                                          }else{
                                                 if(Lang::has(Session::get('lang_file').'.Wrapping_Design')!= ''){ $wraptypedesign= trans(Session::get('lang_file').'.Wrapping_Design');  }else{ $wraptypedesign= trans($OUR_LANGUAGE.'.Wrapping_Design'); } 
                                        }
                                                     echo $wraptypedesign.'- '.$prodotions->option_title."<br>";
                                                 $jk++;  }
                                         } } ?>
				              </td>
							  
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">

              <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','rose');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','rose');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','rose');"></button>
                </div> 
<input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>

  </td>
<?php 
$mc_name = 'mc_name'; 
if(Session::get('lang_file')!='en_lang')
{
  $mc_name = 'mc_name_ar'; 
}
?>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> /  <?php echo e(isset($shopname->$mc_name) ? $shopname->$mc_name : ''); ?> </td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  
					  </div>
					  <?php } ?>
                      <!-------- Roses end------------------>
					  
					  <!-------- Special Events Start------------------>
					  <?php $totalsum = 0; if(count($carteventsitem) >0){ ?>
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingevents">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseevents" aria-expanded="true" aria-controls="collapseevents">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.special_event')!= '')  ?  trans(Session::get('lang_file').'.special_event'): trans($OUR_LANGUAGE.'.special_event')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($carteventsitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseevents" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingevents">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($carteventsitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?></td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	

                <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','');"></button>
                </div> 
<input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>

  </td>
  <?php 
	$mc_name = 'mc_name';
	if(Session::get('lang_file')!='en_lang')
	{
	$mc_name= 'mc_name_ar'; 
	}
	$catshop=$shopname->$mc_name;
  ?>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($catshop) ? $catshop : ''); ?> </td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					 	</div>
					  <?php } ?>
                      <!-------- Special Events end------------------>
					  
					  <!-------- Reception & Hospitality Start------------------>
					  <?php $totalsum = 0; if(count($carthospitalityitem) >0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headinghospitality">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsehospitality" aria-expanded="true" aria-controls="collapsehospitality">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.Reception_andHospitality')!= '')  ?  trans(Session::get('lang_file').'.Reception_andHospitality'): trans($OUR_LANGUAGE.'.Reception_andHospitality')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> 
                                <?php
                                 $zk=1;
                                 foreach($carthospitalityitem->product_attr as $val){ 
                                  if($zk==1 && $val->attribute_title=='Design Your Package'){ 
                                  $bookinginfo=Helper::getbookinghosinformation($val->cart_id,$val->shop_id,$val->attribute_title);             
               if(isset($bookinginfo->no_of_staff) && $bookinginfo->no_of_staff>0){ $workertotal=$bookinginfo->no_of_staff*$val->worker_price; }else{ $workertotal=0; } 
                                  $twoprice=$val->total_price+$workertotal;
                                  $zk++;
                                }else{
                                $twoprice=$val->total_price;
                              }

                                 $totalsum = $totalsum + $twoprice;
                                  //
                                   } 
                                  echo number_format($totalsum,2); 
                                 ?>
                              </td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsehospitality" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headinghospitality">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
              $z=1;
              $ab=1;
             // dd($carthospitalityitem);
						  foreach($carthospitalityitem->product_attr as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->shop_vendor_id);
               //$getVendorName = '';
               $bookinginfo=Helper::getbookinghosinformation($val->cart_id,$val->shop_id,$val->attribute_title);             
               if(isset($bookinginfo->no_of_staff) && $bookinginfo->no_of_staff>0){ $workertotal=$bookinginfo->no_of_staff*$val->worker_price; }else{ $workertotal=0; } 
               
               if($ab==1 && $val->attribute_title=='Design Your Package'){ $twprice=$val->total_price + $workertotal; }else{ $twprice=$val->total_price;}
						   $basetotal = $basetotal + $twprice;
            $shopname=Helper::getparentCat($val->shop_id);
          
            
             

						  ?>
                           



                          <?php if($val->attribute_title=='Design Your Package' && $ab==1){ ?>
                           <tr>                 
                              <td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">

                                 <?php echo e((Lang::has(Session::get('lang_file').'.Worker')!= '')  ?  trans(Session::get('lang_file').'.Worker'): trans($OUR_LANGUAGE.'.Worker')); ?><br>
                          
                                

<td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> 
   <?php echo e(isset($bookinginfo->no_of_staff) ? $bookinginfo->no_of_staff : ''); ?>

              
  </td>
                <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"></td>
                <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>">


                  <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($workertotal,2)); ?></td>
                            </tr>

                          <?php $ab++; } ?>




                            <tr>							   
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>">

                                <a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>" data-pro="hospitality" >&nbsp;</a>

                                  <?php 
  $pro_name = 'pro_title';
  if(Session::get('lang_file')!='en_lang')
  {
  $pro_name= 'pro_title_ar'; 
  }
  $product_name=$val->$pro_name;
  $getReceptionHospitalityInfo = Helper::getReceptionHospitality($val->cart_id,$val->product_id);
  ?>

                                <?php echo e(isset($product_name) ? $product_name : ''); ?> <br>

                                   <?php if(isset($bookinginfo->date) && $bookinginfo->date!='')
                                   { 
                                   $ordertime=strtotime($bookinginfo->date);
                                   $orderedtime = date("d M Y",$ordertime);
                                   echo $orderedtime;
                                   } 
                                   ?>

                                <br>

                                <?php echo e(isset($getReceptionHospitalityInfo->time) ? $getReceptionHospitalityInfo->time : ''); ?><br>
                                <?php echo e(isset($getReceptionHospitalityInfo->elocation) ? $getReceptionHospitalityInfo->elocation : ''); ?>

                                <input type="hidden" name="qty" value="<?php echo e(isset($val->quantity) ? $val->quantity : 1); ?>"></td>

<td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> 

                <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                  <?php if($val->attribute_title=='Design Your Package'){ ?>
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','hospitality');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','hospitality');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','hospitality');"></button>
              <?php }else{ ?>
                  <?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>

              <?php } ?>
                </div> 
                  <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>

  </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>">

                
                       <?php 
  $mc_name = 'mc_name';
  if(Session::get('lang_file')!='en_lang')
  {
  $mc_name= 'mc_name_ar'; 
  }
  $catshop=$shopname->$mc_name;
  ?>

                

                  <?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($catshop) ? $catshop : ''); ?> </td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>


                       



							<?php $z++; $i++;} ?>

             

                          </table>
                        </div>
                      </div>
					 </div>
					  <?php } ?>
                      <!-------- Reception & Hospitality end------------------>
					  
					  <!-------- Music Start------------------>
					  <?php $totalsum = 0; if(count($cartmusictitem) > 0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingmusic">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsemusic" aria-expanded="true" aria-controls="collapsemusic">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.MUSIC')!= '')  ?  trans(Session::get('lang_file').'.MUSIC'): trans($OUR_LANGUAGE.'.MUSIC')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartmusictitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsemusic" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingmusic">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
              
						  foreach($cartmusictitem as $key=>$val){ 
 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
               $rentinfo=Helper::getProductattrdress($val->product_id,$val->cart_id,$val->id);
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br/>
                                <?php if( isset($rentinfo->rental_date) && $rentinfo->rental_date !=''): ?>
 <?php echo e((Lang::has(Session::get('lang_file').'.Booking_date_time')!= '')  ?  trans(Session::get('lang_file').'.Booking_date_time'): trans($OUR_LANGUAGE.'.Booking_date_time')); ?> :  <?php echo e($rentinfo->rental_date); ?> <?php echo e($rentinfo->rental_time); ?><br/>
 <?php echo e((Lang::has(Session::get('lang_file').'.Return_date_time')!= '')  ?  trans(Session::get('lang_file').'.Return_date_time'): trans($OUR_LANGUAGE.'.Return_date_time')); ?> : <?php echo e($rentinfo->return_date); ?> <?php echo e($rentinfo->return_time); ?> 
     
      <?php endif; ?>

                              </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">
 <?php 
  $mc_name = 'mc_name';
  if(Session::get('lang_file')!='en_lang')
  {
  $mc_name= 'mc_name_ar'; 
  }
  $catshop=$shopname->$mc_name;
  ?>
               <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','rose');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','rose');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','rose');"></button>
                </div> 
<input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>

  </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($catshop) ? $catshop : ''); ?> </td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  </div>
					  <?php } ?>
                      <!-------- Music end------------------>
					  
					  <!-------- Gold & Jewelry Start------------------>
					  <?php $totalsum = 0; if(count($cartgoldtitem) > 0){ ?>
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headinggold">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsegold" aria-expanded="true" aria-controls="collapsegold">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.GOLD_AND_JEWELRY')!= '')  ?  trans(Session::get('lang_file').'.GOLD_AND_JEWELRY'): trans($OUR_LANGUAGE.'.GOLD_AND_JEWELRY')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartgoldtitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsegold" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headinggold">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;

						  foreach($cartgoldtitem as $val){    
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;


$shopname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $makeptodushopname=$shopname->mc_name;
             }else{
             $makeptodushopname=$shopname->mc_name_ar;
           }






						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br/>
                <?php if(isset($val->product_size) && $val->product_size!=''): ?>
                <?php echo e((Lang::has(Session::get('lang_file').'.TITLE')!= '')  ?  trans(Session::get('lang_file').'.TITLE'): trans($OUR_LANGUAGE.'.TITLE')); ?>: <?php echo e($val->product_size); ?>

                <?php endif; ?>
				<?php 
				$getProductAttr = Helper::getAllProductAttribute($val->product_id,$val->merchant_id);
				if(isset($getProductAttr[0]->value) && $getProductAttr[0]->value!=''){
				?>
				<br/>
				<?php echo e((Lang::has(Session::get('lang_file').'.CALIBER')!= '')  ?  trans(Session::get('lang_file').'.CALIBER'): trans($OUR_LANGUAGE.'.CALIBER')); ?>: <?php echo e($getProductAttr[0]->value); ?>

				<?php }  if(isset($getProductAttr[1]->value) && $getProductAttr[1]->value!=''){?>
				<br/>
				<?php echo e((Lang::has(Session::get('lang_file').'.WEIGHT')!= '')  ?  trans(Session::get('lang_file').'.WEIGHT'): trans($OUR_LANGUAGE.'.WEIGHT')); ?>: <?php echo e($getProductAttr[1]->value); ?>

				<?php } ?>
                              </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	

                   <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','');"></button>
                </div> 
<input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
              </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($makeptodushopname) ? $makeptodushopname : ''); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  </div>
					  <?php } ?>
                      <!-------- Gold & Jewelry end------------------>
					  
					  <!-------- Perfume Start------------------>
					  <?php $totalsum = 0; if(count($cartperfumetitem) > 0){ ?>
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperfume" aria-expanded="true" aria-controls="collapseperfume">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.PERFUME')!= '')  ?  trans(Session::get('lang_file').'.PERFUME'): trans($OUR_LANGUAGE.'.PERFUME')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartperfumetitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperfume" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperfume">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($cartperfumetitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;

$shopname=Helper::getparentCat($val->shop_id);
             
               $s=$shopname->mc_name;

						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?></td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	
               

               <?php
              $Qran = rand(111,999);              
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','');"></button>
                </div> 
                  <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
                 </td>
							  <td class="td td3" data-title="Services"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e($s); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					  </div>
					  <?php } ?>
                      <!-------- Perfume end------------------>
					  
					  <!-------- Dress Start------------------>
					  <?php $totalsum = 0; if(count($cartdressitem) > 0){ ?>
                        <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingdress">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsedress" aria-expanded="true" aria-controls="collapsedress">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.DRESSES')!= '')  ?  trans(Session::get('lang_file').'.DRESSES'): trans($OUR_LANGUAGE.'.DRESSES')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartdressitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsedress" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingdress">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  <?php 
						  $i=1;
						  $k++;
						  foreach($cartdressitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
                $productbookingrentinfo=Helper::getProductattrdress($val->product_id,$val->cart_id,$val->id);
               $shopname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $dressshopname=$shopname->mc_name;
             }else{
             $dressshopname=$shopname->mc_name_ar;
           }
						  ?>
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?> <br>
                                <?php
                                $siozeoption=substr($val->product_size, 4, 2);
                                ?>
                                  <?php echo e((Lang::has(Session::get('lang_file').'.SIZE_DESSESS')!= '')  ?  trans(Session::get('lang_file').'.SIZE_DESSESS'): trans($OUR_LANGUAGE.'.SIZE_DESSESS')); ?> <?php echo e($siozeoption); ?>

                                  <?php 
                                    if(isset($productbookingrentinfo->rental_time) && $productbookingrentinfo->rental_time!=''){
                                      echo "<br>";
                                    echo $productbookingrentinfo->rental_time;
                                  }
                                    if(isset($productbookingrentinfo->return_time) && $productbookingrentinfo->return_time!=''){

                                    echo ' - '.$productbookingrentinfo->return_time;
                                  }
                                  ?>
                              </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','<?php echo e(isset($val->product_size) ? $val->product_size : ''); ?>','remove','<?php echo e($Qran); ?>','dress');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','dress');" type="number" readonly>
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','<?php echo e(isset($val->product_size) ? $val->product_size : ''); ?>','add','<?php echo e($Qran); ?>','dress');"></button>
                </div> 
                <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                   <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
              </td>
							  <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e($dressshopname); ?></td>
							  <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
							<?php $i++;} ?>
                          </table>
                        </div>
                      </div>
					 </div>
					  <?php } ?>
                      <!-------- Dress end------------------>




 <!-------- Makeup cosmetic Start------------------>
            <?php $totalsum = 0; if(count($cartmakeupcosmeticitem) > 0){ ?>
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsemakeup" aria-expanded="true" aria-controls="collapseperfume">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.MAKEUPCOSMETIC')!= '')  ?  trans(Session::get('lang_file').'.MAKEUPCOSMETIC'): trans($OUR_LANGUAGE.'.MAKEUPCOSMETIC')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartmakeupcosmeticitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsemakeup" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperfume">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              <?php 
              $i=1;
			  $k++;
              foreach($cartmakeupcosmeticitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
                $shopname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $makeptodushopname=$shopname->mc_name;
             }else{
             $makeptodushopname=$shopname->mc_name_ar;
           }
              ?>
                            <tr>
                
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?> <br>
                                
                              </td>
							  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	

             <?php
              $Qran = rand(111,999);
              ?>
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','remove','<?php echo e($Qran); ?>','');"></button>
                <input name="itemqty[]" id="qty<?php echo e(isset($Qran) ? $Qran : 0); ?>" value="<?php echo e(isset($val->quantity) ? $val->quantity : 0); ?>" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','pricewithqty','<?php echo e($Qran); ?>','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('<?php echo e(isset($val->id) ? $val->id : 0); ?>','basecontainerprice149','add','<?php echo e($Qran); ?>','');"></button>
                </div> 
                  <input type="hidden" name="<?php echo e($Qran); ?>" id="<?php echo e($Qran); ?>" value="<?php echo e($val->product_id); ?>">
                  <span id="<?php echo e(isset($val->id) ? $val->id : 0); ?><?php echo e($Qran); ?>" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
  </td>
                <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($makeptodushopname) ? $makeptodushopname : ''); ?></td>
                <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
              <?php $i++;} ?>
                          </table>
                        </div>
                      </div>
            </div>
			<?php } ?>
                      <!-------- makeupcosmetic end------------------>





<!-------- beauty center Start------------------>
            <?php $totalsum = 0; if(count($cartbeautycenteritem) > 0){ $k++; ?>
                      <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperbeautycenter" aria-expanded="true" aria-controls="collapseperbeautycenter">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.BEAUTY_CENTERS')!= '')  ?  trans(Session::get('lang_file').'.BEAUTY_CENTERS'): trans($OUR_LANGUAGE.'.BEAUTY_CENTERS')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartbeautycenteritem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperbeautycenter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperbeautycenter">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              <?php 
              $i=1;
			  $k++;
        //print_r($cartbeautycenteritem);
              foreach($cartbeautycenteritem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $shopname=Helper::getparentCat($val->shop_id);
               if(Session::get('lang_file')=='en_lang'){
               $beautyshopname=$shopname->mc_name;
               if(isset($val->booked_staffandotherinformation->staff_member_name) && $val->booked_staffandotherinformation->staff_member_name!='')
               {
                $wrakername=$val->booked_staffandotherinformation->staff_member_name;
               }
               else
               {
                $wrakername='';
               }
               
             }else{
              $beautyshopname=$shopname->mc_name_ar;

              if(isset($val->booked_staffandotherinformation->staff_member_name_ar) && $val->booked_staffandotherinformation->staff_member_name_ar!='')
              {

              $wrakername=$val->booked_staffandotherinformation->staff_member_name_ar;
              }
              else
              {
              $wrakername=''; 
              }
              }
               
              

               $basetotal = $basetotal + $val->total_price;
              // echo "<pre>";
               //print_r($val);
              ?>
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>" class="cart-delete">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                  <?php echo e(isset($wrakername) ? $wrakername : ''); ?><br>
                    <?php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             ?>
                  <br> <?php echo e(isset($val->booked_staffandotherinformation->start_time) ? $val->booked_staffandotherinformation->start_time : ''); ?> </td>
				  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 <?php
              $Qran = rand(111,999);
              ?>
              N/A
                  </td>
                <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e($beautyshopname); ?></td>
                <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
              <?php $i++;} ?>
                          </table>
                        </div>
                      </div>
            </div>
			<?php } ?>
                      <!-------- beauty center end------------------>

<!-------- spa center Start------------------>
            <?php $totalsum = 0; if(count($cartspaitem) > 0){ $k++; ?>
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperspacenter" aria-expanded="true" aria-controls="collapseperspacenter">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.SPA_CENTERS')!= '')  ?  trans(Session::get('lang_file').'.SPA_CENTERS'): trans($OUR_LANGUAGE.'.SPA_CENTERS')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartspaitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperspacenter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperspacenter">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              <?php 
              $i=1;
              //echo "<pre>";
              //print_r($cartspaitem);
              foreach($cartspaitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;

                 $shopname=Helper::getparentCat($val->shop_id);
               if(Session::get('lang_file')=='en_lang'){
                $beautyshopname=$shopname->mc_name;
                if(isset($val->booked_staffandotherinformation->staff_member_name) && $val->booked_staffandotherinformation->staff_member_name!='')
                {
                $workername=$val->booked_staffandotherinformation->staff_member_name;
                }
                else
                {
                $workername='';
                }


                }else{
                $beautyshopname=$shopname->mc_name_ar;
                if(isset($val->booked_staffandotherinformation->staff_member_name_ar) && $val->booked_staffandotherinformation->staff_member_name_ar!='')
                {
                $workername=$val->booked_staffandotherinformation->staff_member_name_ar;
                }
                else
                {
                $workername='';
                }
                }


             //echo "<pre>"; 
              //print_r($val);
              ?>
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>" class="cart-delete">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                  <?php echo e(isset($workername) ? $workername : ''); ?><br>
                    <?php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             ?>

                   <?php echo e(isset($val->booked_staffandotherinformation->start_time) ? $val->booked_staffandotherinformation->start_time : ''); ?> </td>
                <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 <?php
              $Qran = rand(111,999);
              ?> N/A
                 </td>
		<td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> /
      <?php echo e(isset($beautyshopname) ? $beautyshopname : ''); ?>

    </td>
				
                <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
              <?php $i++;} ?>
                          </table>
                        </div>
                      </div>
           </div>
		    <?php } ?>
                      <!-------- spa center end------------------>


<!-------- saloon center Start------------------>
            <?php $totalsum = 0; if(count($cartmen_saloonitem) > 0){ $k++; ?>
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingpersallon">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperbarbarcenter" aria-expanded="true" aria-controls="collapseperbarbarcenter">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.BARBAR_CENTERS')!= '')  ?  trans(Session::get('lang_file').'.BARBAR_CENTERS'): trans($OUR_LANGUAGE.'.BARBAR_CENTERS')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartmen_saloonitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperbarbarcenter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperspacenter">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              <?php 
              $i=1;
              foreach($cartmen_saloonitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
             //echo "<pre>";
              //print_r($val);
                $saloonshopdbname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $saloonshopname=$saloonshopdbname->mc_name;
             }else{
              $saloonshopname=$saloonshopdbname->mc_name_ar;
           }
              ?>
                            <tr>
               
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>" class="cart-delete">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                  <?php echo e(isset($val->booked_staffandotherinformation->staff_member_name) ? $val->booked_staffandotherinformation->staff_member_name : ''); ?><br>

                  
                    <?php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             ?>


                  <br> <?php echo e(isset($val->booked_staffandotherinformation->start_time) ? $val->booked_staffandotherinformation->start_time : ''); ?> </td>
				  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">

           <?php
              $Qran = rand(111,999);
              ?>
               N/A

     </td>
                <td class="td td4" data-title="Services"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($saloonshopname) ? $saloonshopname : ''); ?></td>
                <td class="td td4" data-title="Services"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
              <?php $i++;} ?>
                          </table>
                        </div>
                      </div>
           </div>
		    <?php } ?>
                      <!-------- saloon center end------------------>



<!-------- saloon center Start------------------>
            <?php $totalsum = 0; if(count($cartmakeup_artistitem) > 0){ $k++; ?>
			  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingpersallon">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsepermakeup_artist" aria-expanded="true" aria-controls="collapsepermakeup_artist">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.MAKEUP_ARTIST')!= '')  ?  trans(Session::get('lang_file').'.MAKEUP_ARTIST'): trans($OUR_LANGUAGE.'.MAKEUP_ARTIST')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2  sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartmakeup_artistitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsepermakeup_artist" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingpermakeup_artist">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              <?php 
              $i=1;
              foreach($cartmakeup_artistitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
             //echo "<pre>";
              //print_r($val);
                $makupshopdbname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $makupshopname=$makupshopdbname->mc_name;
             }else{
              $makupshopname=$makupshopdbname->mc_name_ar;
           }

              ?>
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>" class="cart-delete">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>
                  <?php echo e(isset($val->booked_staffandotherinformation->staff_member_name) ? $val->booked_staffandotherinformation->staff_member_name : ''); ?><br>

              <?php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             ?>

           <br> <?php echo e(isset($val->booked_staffandotherinformation->start_time) ? $val->booked_staffandotherinformation->start_time : ''); ?> </td>
				  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	
            N/A
             <?php
              $Qran = rand(111,999);
              ?>
                  </td>
                <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e($makupshopname); ?></td>
                <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
              <?php $i++;} ?>
                          </table>
                        </div>
                      </div>
           </div>
		    <?php } ?>
            <!---saloon center end-->



            <!--Cosmetics And Laser Start-->
            <?php $totalsum = 0; if(count($cartclinic_cosmeticitem) > 0){  ?>
			  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingpercosmetic">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsepercosmetic" aria-expanded="true" aria-controls="collapsepercosmetic">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.COSMETICS_AND_LASER')!= '')  ?  trans(Session::get('lang_file').'.COSMETICS_AND_LASER'): trans($OUR_LANGUAGE.'.COSMETICS_AND_LASER')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2  sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartclinic_cosmeticitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsepercosmetic" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingpermakeup_artist">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              <?php 
			  //echo '<pre>';print_r($cartclinic_cosmeticitem);
              $i=1;
              foreach($cartclinic_cosmeticitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);

                 $shopname=Helper::getparentCat($val->shop_id);

                  if(Session::get('lang_file')=='en_lang'){
               $clinclasershopname=$shopname->mc_name;
             }else{
              $clinclasershopname=$shopname->mc_name_ar;
           }

               $basetotal = $basetotal + $val->total_price;
             //echo "<pre>";
              //print_r($val);
              ?>
                            <tr>
                

            
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" class="cart-delete" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>

                  <?php
				     //echo '<pre>';print_r($val);
                      $bookordertime=strtotime($val->booked_staffandotherinformation->booking_date);
                     $bokkingorderedtime = date("d M Y",$bookordertime);             
                  ?>


                  <?php echo e(isset($bokkingorderedtime) ? $bokkingorderedtime : ''); ?>, <?php echo e(isset($val->booked_staffandotherinformation->start_time) ? $val->booked_staffandotherinformation->start_time : ''); ?><br>
     <?php if(isset($val->booked_staffandotherinformation->file_no) && $val->booked_staffandotherinformation->file_no!=''): ?> 

                  <?php echo e((Lang::has(Session::get('lang_file').'.File_No')!= '')  ?  trans(Session::get('lang_file').'.File_No'): trans($OUR_LANGUAGE.'.File_No')); ?> <?php if(isset($val->booked_staffandotherinformation->file_no) && $val->booked_staffandotherinformation->file_no!=''){echo $val->booked_staffandotherinformation->file_no;} ?>


 <?php endif; ?>


                </td>
               


				  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 <?php
              $Qran = rand(111,999);
              ?>
                 </td>
                <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($clinclasershopname) ? $clinclasershopname : ''); ?></td>
                <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
              <?php $i++;} ?>
                          </table>
                        </div>
                      </div>
            </div>
			<?php  $k++; } ?>
            <!-------- Cosmetics And Laser end------------------>


            <!-------- Dental And Dermatology Start------------------>
            <?php $totalsum = 0; if(count($cartclinic_skinitem) > 0){  ?>
			  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingperskin">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperskin" aria-expanded="true" aria-controls="collapseperskin">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"> <?php echo e((Lang::has(Session::get('lang_file').'.DENTAL_AND_DERMATOLOGY')!= '')  ?  trans(Session::get('lang_file').'.DENTAL_AND_DERMATOLOGY'): trans($OUR_LANGUAGE.'.DENTAL_AND_DERMATOLOGY')); ?> <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php foreach($cartclinic_skinitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); ?></td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperskin" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingpermakeup_skin">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              <?php 
              $i=1;
               
              foreach($cartclinic_skinitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
          $shopname=Helper::getparentCat($val->shop_id);

                if(Session::get('lang_file')=='en_lang'){
               $clincshopname=$shopname->mc_name;
             }else{
              $clincshopname=$shopname->mc_name_ar;
           }


              ?>
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')); ?>"><a href="#" data-id="<?php echo e(isset($val->id) ? $val->id : 0); ?>" class="cart-delete">&nbsp;</a><?php echo e($val->getProduct[0]->pro_title); ?><br>

                  <?php
                      $bookordertime=strtotime($val->booked_staffandotherinformation->booking_date);
                     $bokkingorderedtime = date("d M Y",$bookordertime);             
                  ?>
                  
                  <?php echo e(isset($bokkingorderedtime) ? $bokkingorderedtime : ''); ?>, <?php echo e(isset($val->booked_staffandotherinformation->start_time) ? $val->booked_staffandotherinformation->start_time : ''); ?><br>
                  <?php if(isset($val->booked_staffandotherinformation->file_no) && $val->booked_staffandotherinformation->file_no!=''){ ?>
                      <?php echo e((Lang::has(Session::get('lang_file').'.File_No')!= '')  ?  trans(Session::get('lang_file').'.File_No'): trans($OUR_LANGUAGE.'.File_No')); ?>

                   <?php echo $val->booked_staffandotherinformation->file_no;} ?></td>
				  <td class="td td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')); ?>">	 <?php
              $Qran = rand(111,999);
              ?>
                 </td>
                <td class="td td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>"><?php echo e(isset($getVendorName->mer_fname) ? $getVendorName->mer_fname : ''); ?> / <?php echo e(isset($clincshopname) ? $clincshopname : ''); ?></td>
                <td class="td td4" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')); ?>"><?php echo e(Session::get('currency')); ?> <?php echo e(number_format($val->total_price,2)); ?></td>
                            </tr>
              <?php $i++;} ?>
                          </table>
                        </div>
                      </div>
            </div>
			<?php  $k++; } ?>
            <!-------- Dental And Dermatology end------------------>
 
                    </div>
                  </div>
                  <!-- panel-group -->
                </div></td>
            </tr>
            <tr class="tr total-prise">
              <td class="td td4" data-title="Total" colspan="5"><span><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL')!= '')  ?  trans(Session::get('lang_file').'.TOTAL'): trans($OUR_LANGUAGE.'.TOTAL')); ?></span> <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($basetotal, 2)); ?></td>
            </tr>
          </table>
          <!-- table -->
        </div>
        <div class="tatal-services-pay-area">
        <a href="<?php echo e(url('/checkout')); ?>">  <input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.PAY_NOW')!= '')  ?  trans(Session::get('lang_file').'.PAY_NOW'): trans($OUR_LANGUAGE.'.PAY_NOW')); ?>" class="form-btn" /> </a>
        </div>
        <!-- tatal-services-pay-area -->
      </div>
      <!-- tatal-services-inner-area -->
    </div>
  </div>
  <!-- outer_wrapper -->
</div>

<script>
jQuery(document).ready(function(){
    jQuery("button").click(function(){
        jQuery('#halldetails').toggle();
    });
});
</script>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
	
	function toggleIcon(e) {
    jQuery(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);

</script>
<style type="text/css">
	
/*******************************
* Does not work properly if "in" is added after "collapse".
* Get free snippets on bootpen.com
*******************************/
    .panel-group .panel {
        border-radius: 0;
		border:0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
	font-weight:normal;
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 0px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }


</style>
<?php echo $__env->make('includes.deletemessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $MakupArtistbig_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if($autoid==''): ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ADDWORKER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADDWORKER')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADDWORKER')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.UPDATEWORKER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATEWORKER')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATEWORKER')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?>
              <!-- Display Message after submition -->
       
                <form name="form1" id="makeuparistworker" method="post" action="<?php echo e(route('store-worker')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <!-- form_row -->
                  <div class="form_row">
                    <div class="form_row_left">
                      <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_NAME'); ?> </span> </label>
                      <div class="info100">
                        <div class="english">
                          <input type="text" class="english" name="name" maxlength="50" value="<?php echo e(isset($fetchdata->staff_member_name) ? $fetchdata->staff_member_name : ''); ?>" id="name" required="" >
                          <input type="hidden" name="parentid" id="parentid" value="<?php echo e($id); ?>">
                          <input type="hidden" name="sid" id="sid" value="<?php echo e($sid); ?>">
                          <input type="hidden" name="autoid" id="autoid" value="<?php echo e($autoid); ?>">
                        </div>
                        <div class="arabic ar">
                          <input type="text" class="arabic ar" name="name_ar" maxlength="50" value="<?php echo e(isset($fetchdata->staff_member_name_ar) ? $fetchdata->staff_member_name_ar : ''); ?>" id="name_ar" required="" >
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <div class="form_row_left">
                      <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_User_Image'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_User_Image'); ?> </span> </label>
                      <div class="info100">
                        <div class="input-file-area">
                          <label for="company_logo">
                          <div class="file-btn-area">
                            <div id="file_value1" class="file-value"></div>
                            <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                          </div>
                          </label>
                          <input id="company_logo" name="stor_img" class="info-file" type="file" value="" accept="image/*">
                        </div>
                        <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                        <div class="form-upload-img"> <?php if(isset($fetchdata->image) && $fetchdata->image!=''): ?> <img src="<?php echo e(isset($fetchdata->image) ? $fetchdata->image : ''); ?>"> <?php endif; ?> </div>
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <div class="form_row_left">
                      <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.EXPERT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.EXPERT'); ?> </span> </label>
                      <div class="info100">
					  <div class="cat-box-line">
					   <?php $__currentLoopData = $getAttr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catvals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        $getProducts = Helper::getrelateditems($catvals->id,request()->sid); 
                        if($getProducts->count() < 1) {continue; }
                        ?>
                        <!--Loop start-->
                        <div class="cat_box">
                          <div class="save-card-line">
                            <div class="save-card-line">
                              <input type="checkbox" class="cdata " id="attri_<?php echo e($catvals->id); ?>" data-id="<?php echo e($catvals->id); ?>">
                              <label for="attri_<?php echo e($catvals->id); ?>"> <?php if($mer_selected_lang_code =='en'): ?>  <?php echo e(isset($catvals->attribute_title) ? $catvals->attribute_title : ''); ?> <?php else: ?> <?php echo e(isset($catvals->attribute_title_ar) ? $catvals->attribute_title_ar : ''); ?> <?php endif; ?> </span> </label>
                            </div>
                          </div>
                          <div class="category_area" id="childcbox_<?php echo e($catvals->id); ?>"> <?php $__currentLoopData = $getProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proitems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <!--Loop -->
                            <div class="save-card-line">
                              <input type="checkbox" id="men<?php echo e($proitems->pro_id); ?>" <?php if(in_array($proitems->
                              pro_id, $proid)): ?> CHECKED <?php endif; ?> name="productitems[]" value="<?php echo e(isset($proitems->pro_id) ? $proitems->pro_id : ''); ?>">
                              <label for="men<?php echo e($proitems->pro_id); ?>"> <span class="englishd"><?php if($mer_selected_lang_code =='en'): ?>  <?php echo e(isset($proitems->pro_title) ? $proitems->pro_title : ''); ?> <?php else: ?>  <?php echo e(isset($proitems->pro_title_ar) ? $proitems->pro_title_ar : ''); ?>  <?php endif; ?><span class="sar">(SAR <?php echo e(isset($proitems->pro_price) ? $proitems->pro_price : ''); ?>)</span> </span> </label>
                            </div>
                            <!--Loop end -->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></div> <span for="productitems[]" generated="true" class="error"> </span>
                        <!--Loop End -->
                      </div>
                    </div>
                  </div>
                  <div class="form_row common_field">
                    <div class="form_row_left">
                      <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.YEAROFEXP'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.YEAROFEXP'); ?> </span> </label>
                      <div class="info100">
                        <div class="engliskjh">
                          <input type="text" class="xs_small notzero" name="year_of_exp" onkeypress="return isNumber(event)" maxlength="4" value="<?php echo e(isset($fetchdata->experience) ? $fetchdata->experience : ''); ?>" id="year_of_exp" required="" >
                        </div>
                      </div>
                      <!-- form_row -->
                    </div>
                  </div>
                  <div class="form_row">
                    <div class="form_row_left">
                      <div class="english">
                        <input type="submit" name="submit" value="Submit">
                      </div>
                      <!-- form_row -->
                      <div class="arabic ar">
                        <input type="submit" name="submit" value="خضع">
                      </div>
                      <!-- form_row -->
                    </div>
                  </div>
                </form>
       
              <!-- one-call-form -->
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>
      
            $("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
  
$("#makeuparistworker").validate({

                  ignore: [],
                  rules: {
                    name: {
                         required: true,
                        },

                       name_ar: {
                       required: true,
                      },

                      year_of_exp: {
                       required: true,
                      },

                       "productitems[]": {
                       required: true,
                      },
                      
                       <?php if(isset($fetchdata->image)!=''): ?>  
                        stor_img: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       <?php else: ?>
                        stor_img: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      <?php endif; ?>


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
                name: {
                    required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_NAME'); ?>",
 
                      }, 

                    name_ar: {

                       required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_NAME'); ?>",

          
                      },     


                       year_of_exp: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXP')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXP')); ?> <?php endif; ?> ",
                      },

                      "productitems[]": {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXPERTISE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXPERTISE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXPERTISE')); ?> <?php endif; ?> ",
                      },
                    
                  stor_img: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },   

                                                       
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.arabic_tab').trigger('click');
                           

                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.arabic_tab').trigger('click');
                           

                    }
                      
                     
                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      
                     if (typeof valdata.name_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
          <?php else: ?>


             if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                  if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }    
 
            if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      

                    
                <?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

 
 
$(document).ready(function() {
  $('.cdata').click(function() { 
   var childP = $(this).data('id');

    var checked = $(this).prop('checked');
    $('#childcbox_'+childP).find('input:checkbox').prop('checked', checked);
  });
})
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
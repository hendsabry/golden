<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $dessert_inner_leftmenu =1; $attribute_id = '';?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <?php if(request()->autoid==''): ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Add_Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Add_Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Add_Type')); ?> <?php endif; ?></h5>
        <?php else: ?>
  <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Update_Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Update_Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Update_Type')); ?> <?php endif; ?></h5>
        <?php endif; ?>
      </header>
      <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- Display Message after submition -->
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">

                <form name="type" id="type" method="post" action="<?php echo e(route('update-dessart-type')); ?>" enctype="multipart/form-data">
                  <input type="hidden" name="services_id" value="<?php echo e($services_id); ?>">
                  <input type="hidden" name="shopid" value="<?php echo e($shopid); ?>">
                  <input type="hidden" name="branch_id" value="<?php echo e($branch_id); ?>">
                  <input type="hidden" name="proid" value="<?php echo e(isset($product_id) ? $product_id : ''); ?>">
               <?php echo e(csrf_field()); ?>

              <div class="one-call-form">
              <div class="form_row">
			   <div class="form_row_left">
              <label class="form_label">
              <span class="english"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_TITLE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_TITLE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_TITLE')); ?> <?php endif; ?></span>
              <span class="arabic ar">اسم</span>
              </label>
<div class="info100" >
              <div class="english">
              <input class="english" name="title" maxlength="50" value="<?php echo e(isset($getdesart->pro_title) ? $getdesart->pro_title : ''); ?>"   type="text">
              </div>
              <div class="arabic ar">
              <input class="arabic ar" name="title_ar" maxlength="50" value="<?php echo e(isset($getdesart->pro_title_ar) ? $getdesart->pro_title_ar : ''); ?>"   type="text">
              </div>
</div></div>

              </div>

              <div class="form_row common_field">
			   <div class="form_row_left">
              <label class="form_label">
              <span class="english"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_IMAGE')); ?> <?php endif; ?></span>
              <span class="arabic ar">صورة</span>
              </label>
              <div class="info100"> 
              <div class="input-file-area">
              <label for="company_logo">
              <div class="file-btn-area">
              <div id="file_value1" class="file-value"></div>
              <div class="file-btn">
              <span ><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></span>

              </div>
              </div>
              </label>
              <input id="company_logo" name="pro_img" class="info-file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" value="" type="file">
              </div>


              <?php if(isset($getdesart->pro_Img) && $getdesart->pro_Img!=''): ?><div class="form-upload-img">
                
              <img src="<?php echo e(isset($getdesart->pro_Img) ? $getdesart->pro_Img : ''); ?>"> </div>
              <?php endif; ?>
             
              </div>
            </div>     </div>	


 
<!-- Add More product images start -->
        <?php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        ?>

        <?php for($i=0;$i < $GalleryCunt;$i++): ?>
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo<?php echo e($k); ?>">
        <div class="file-btn-area">
        <div id="file_value<?php echo e($J); ?>" class="file-value"></div>
        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
        </div>
        </label>
        <input id="company_logo<?php echo e($k); ?>" name="image[]" class="info-file proImgess" data-lbl="file_value<?php echo e($J); ?>" type="file" value="">
        </div>
        <?php if(isset($productGallery[$i]->image) && $productGallery[$i]->image!=''): ?>
        <div class="form-upload-img product_img_del">
        <img src="<?php echo e(isset($productGallery[$i]->image) ? $productGallery[$i]->image : ''); ?>" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="<?php echo e($productGallery[$i]->id); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value<?php echo e($J); ?>"></span>  
        <?php endif; ?>
        <input type="hidden" name="privius[]" value="<?php echo e($productGallery[$i]->id); ?>">
        </div>

        </div>
        <?php  $J=$J + 1; $k=$k +1; ?>
        <?php endfor; ?>
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>
        <span class="error pictureformat"></span>
        <?php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} ?>                
        <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
        </div>
  <!-- Add More product images end -->







            				  
              <?php
              $getAttr_info ='';
              if(count( $getproductOption) >=1)
              {
              $getAttr_info = $getdesart->attribute_id;
              $discount = '';
              $product_option_value_id ='';
              $product_option_id ='';
              $discount_one ='';
              $product_option_value_id_one ='';
              $product_option_id_one ='';
              $discount_alt = '';
              $product_option_value_id_alt = '';
 
              if($getAttr_info == '45')
              {
              $discount = $getproductOption[0]->discount;
              $product_option_value_id = $getproductOption[0]->value;
              }

              if($getAttr_info == '44')
              {
              if($getproductOption[0]->product_option_id=='21')
              {
              $discount_alt = $getproductOption[0]->discount;
              $product_option_value_id_alt = $getproductOption[0]->value;
              $product_option_id = $getproductOption[0]->product_option_id;
              $discount_one = $getproductOption[1]->discount;
              $product_option_value_id_one = $getproductOption[1]->value;
              $product_option_id_one = $getproductOption[1]->product_option_id;
              }
              else
              {
              $discount_alt = $getproductOption[1]->discount;
              $product_option_value_id_alt = $getproductOption[1]->value;
              $product_option_id = $getproductOption[1]->product_option_id;
              $discount_one = $getproductOption[0]->discount;
              $product_option_value_id_one = $getproductOption[0]->value;
              $product_option_id_one = $getproductOption[0]->product_option_id;  
              }
 
              } 
              }
              ?>

              <div class="form_row common_field" >
			     <div class="form_row_left">
              <label class="form_label">
              <span class="english"><?php if(Lang::has(Session::get('mer_lang_file').'.TYPEOFDISH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.TYPEOFDISH')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.TYPEOFDISH')); ?> <?php endif; ?> </span>
              </label>
              <div class="info100">           
            <div class="save-card-line">
            <input name="attribute_id" value="44" <?php if($getAttr_info=='44'): ?> CHECKED <?php endif; ?> class="attri" id="attri0" data-name="Type" type="radio">
            <label for="attri0"> <span class="cat_face"><?php if(Lang::has(Session::get('mer_lang_file').'.Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Type')); ?> <?php endif; ?></span> </label>
            </div>
            <div class="save-card-line">
            <input name="attribute_id" value="45" <?php if($getAttr_info=='45'): ?> CHECKED <?php endif; ?> class="attri" id="attri1" data-name="Dish" type="radio">
            <label for="attri1"> <span class="cat_face"><?php if(Lang::has(Session::get('mer_lang_file').'.Dish')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Dish')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Dish')); ?> <?php endif; ?></span> </label>
            </div>
            <span for="attribute_id" generated="true" class="error"></span>      
            </div>
              <div class="form_labs type lbs " <?php if(isset($getAttr_info) && $getAttr_info !='44'): ?> style="display: none;" <?php endif; ?>>
 
              <div class="form_tabs"> 

              <div class="per_box">
              <div class="perarea">
              <div class="per_left"><?php if(Lang::has(Session::get('mer_lang_file').'.Per_KG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Per_KG')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Per_KG')); ?> <?php endif; ?></div>
              <div class="per_right"> <input class="perkg notzero" name="type_perkg" onkeypress="return isNumber(event)" maxlength="7" value="<?php echo e(isset($product_option_value_id_alt) ? $product_option_value_id_alt : ''); ?>"  type="text"></div>
              </div>
              <div class="perarea">
              <div class="per_left"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DISCOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT')); ?> <?php endif; ?></div>
              <div class="per_right"> <input class="perkg notzero" onkeypress="return isNumber(event)" name="type_perkg_discount" maxlength="2" value="<?php echo e(isset($discount_alt) ? $discount_alt : ''); ?>"  type="text"></div>       
              </div></div> 


 <div class="per_box" style="display: none;">
              <div class="perarea">
              <div class="per_left"><?php if(Lang::has(Session::get('mer_lang_file').'.Per_Pieces')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Per_Pieces')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Per_Pieces')); ?> <?php endif; ?> </div>
              <div class="per_right"> <input class="perkg notzero" onkeypress="return isNumber(event)" name="type_perpiece" maxlength="7" value="<?php echo e(isset($product_option_value_id_one) ? $product_option_value_id_one : ''); ?>"  type="text"></div>       
              </div>
              <div class="perarea">
              <div class="per_left"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DISCOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT')); ?> <?php endif; ?></div>
              <div class="per_right"> <input class=" perkg notzero" onkeypress="return isNumber(event)" name="type_perpiece_discount" maxlength="2" value="<?php echo e(isset($discount_one) ? $discount_one : ''); ?>"  type="text"></div>
              </div>
              </div> 


              </div> 
              </div> 
              <div class="form_labs dish" <?php if(isset($getAttr_info) &&  $getAttr_info !='45'): ?> style="display: none;" <?php endif; ?>>
               <div class="form_tabs">
              <div class="per_box">

              <div class="perarea">
              <div class="per_left"><?php if(Lang::has(Session::get('mer_lang_file').'.Per_Pieces')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Per_Pieces')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Per_Pieces')); ?> <?php endif; ?> </div>
              <div class="per_right"> <input class="perkg notzero" onkeypress="return isNumber(event)" name="dish_per_qty" maxlength="7" value="<?php echo e(isset($product_option_value_id) ? $product_option_value_id : ''); ?>"   type="text"></div>
 

              </div>



              <div class="perarea">
              <div class="per_left"><?php if(Lang::has(Session::get('mer_lang_file').'.Discount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Discount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Discount')); ?> <?php endif; ?> %</div>
              <div class="per_right"> <input class=" perkg notzero" onkeypress="return isNumber(event)" name="dish_per_qty_discount" maxlength="2" value="<?php echo e(isset($discount) ? $discount : ''); ?>"   type="text"></div>
              </div>
              </div>	
              </div>
              </div>
              <div class="form_labs ">

              <div class="form_tabs">
              <div class="per_box">
              <div class="perarea">
              <div class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.Available_Qty')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Available_Qty')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Available_Qty')); ?> <?php endif; ?> </div>
              <div class="info100"> <input class="perkg notzero" name="aval_qty" onkeypress="return isNumber(event)" maxlength="7" value="<?php echo e(isset($getdesart->pro_qty) ? $getdesart->pro_qty : ''); ?>" required="" type="text"></div> 
              <span for="aval_qty" generated="true" class="error"></span>
              </div>          
              </div>

              </div>
              </div>

              <div class="form_labs ">

              <div class="form_tabs">
              <div class="per_box">
              <div class="perarea">
              <div class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.PRODUCT_WEIGHT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PRODUCT_WEIGHT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PRODUCT_WEIGHT')); ?> <?php endif; ?> </div>
              <div class="info100"> <input class="perkg notzero" name="pro_weight" onkeypress="return isNumberKey(event,this)" maxlength="7" value="<?php echo e(isset($getdesart->pro_weight) ? $getdesart->pro_weight : ''); ?>" required="" type="text"></div> 
             
              </div>          
              </div>

              </div>
              </div>







 </div>	
              </div>	
			  
			  
			  
			   
            <div class="form_row">
			 <div class="form_row_left">
              <label class="form_label">

<span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>

             <div class="info100">
        
              <div class="english">          
              <textarea name="pro_description" maxlength="500"><?php echo e(isset($getdesart->pro_desc) ? $getdesart->pro_desc : ''); ?></textarea>
              </div>
              <div class="arabic ar">
             
              <textarea name="pro_description_ar"  maxlength="500"><?php echo e(isset($getdesart->pro_desc_ar) ? $getdesart->pro_desc_ar : ''); ?></textarea>

              </div>

</div>
              </div> </div>

    <div class="form_row">
	 <div class="form_row_left">
              <div class="english">
              <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
              </div> 
              <div class="arabic ar">
              <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
              </div>	    
     </div>
   </div>
              </div>


              <!-- one-call-form -->
            </div>
            <!-- box -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<script>
  $(function() {
  $('.attri').click(function() {
  var datavalid = $(this).data('name');
  if(datavalid=='Dish')
  {
  $('.dish').show();
  $('.type').hide();
  }
  else
  {
  $('.dish').hide();
  $('.type').show();
  }
  });
  });
</script>


<script type="text/javascript">

$("#type").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       pro_description: {
                       required: true,
                      },
                       pro_description_ar: {
                       required: true,
                      },
 <?php if(isset($getdesart->pro_Img) && $getdesart->pro_Img!=''): ?>
                      pro_img: {                                            
                       required: false,
                       accept:'png|jpeg|gif|jpg',                  
                      },
<?php else: ?>
                    pro_img: {                                            
                    required: true,
                    accept:'png|jpeg|gif|jpg',                  
                    },

<?php endif; ?>
                      aval_qty: {
                       required: true,
                      },
                      attribute_id: {
                       required: true,
                      },
 
                      
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
               required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_TITLE'); ?>",
                      },              
                title_ar: {
             required:  "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_TITLE'); ?>",
                      },
              pro_description: {
               required:  "<?php echo lang::get('mer_en_lang.PLEASE_ENTER_DESCRIPTION'); ?>",
                      },              
                pro_description_ar: {
             required:  "<?php echo lang::get('mer_ar_lang.PLEASE_ENTER_DESCRIPTION'); ?>",
                      },
                        pro_img: {
             required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
             accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                      },
                        aval_qty: {
             required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_QUANTITY')); ?> <?php endif; ?>",
                      },

                     attribute_id: {
             required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_Choose_Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_Choose_Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_Choose_Type')); ?> <?php endif; ?>",
                      },  
                                  
                },
                invalidHandler: function(e, validation){
 
                    var valdata=validation.invalid;
                                  <?php if($mer_selected_lang_code !='en'): ?>

                                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  }                    
                                  if (typeof valdata.pro_description != "undefined" || valdata.pro_description != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.aval_qty != "undefined" || valdata.aval_qty != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.pro_img != "undefined" || valdata.pro_img != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 
if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 


                                  if (typeof valdata.pro_description_ar != "undefined" || valdata.pro_description_ar != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 


                                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                                  {

                                  $('.arabic_tab').trigger('click');     

                                  }
                                  <?php else: ?>



                                  if (typeof valdata.pro_description_ar != "undefined" || valdata.pro_description_ar != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 


                                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                                  {

                                  $('.arabic_tab').trigger('click');     

                                  }
                                  if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 

                                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  }                    
                                  if (typeof valdata.pro_description != "undefined" || valdata.pro_description != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.aval_qty != "undefined" || valdata.aval_qty != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.pro_img != "undefined" || valdata.pro_img != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 


                                  <?php endif; ?>
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
 /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

 function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
</script> 



<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         <?php if(Session::get('lang_file') =='ar_lang'): ?>
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        <?php else: ?>
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        <?php endif; ?>
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-shop-image')); ?>",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->

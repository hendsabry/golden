<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $hall_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_DISH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPDATE_DISH')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPDATE_DISH')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
        <!-- Display Message after submition --> 
            <?php if(Session::has('message')): ?>
            <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
            <?php endif; ?> 
            <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> 
				
              <form name="form1" id="add-container" method="post" action="<?php echo e(route('updatehallmenu')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
                     <span class="english"><?php echo lang::get('mer_en_lang.MER_Select_Menu'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Select_Menu'); ?> </span>
 </label>
                  <div class="info100">
                    <select name="menucategory" id="mencategory">
                      <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Select_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Select_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Select_Menu')); ?> <?php endif; ?></option>
                       <?php $menu_name='menu_name'?>
                       <?php if($mer_selected_lang_code !='en'): ?>
                           <?php $menu_name= 'menu_name_'.$mer_selected_lang_code; ?>
                       <?php endif; ?>
                        <?php $__currentLoopData = $menucat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                      <option value="<?php echo e($val->id); ?>" <?php echo e(isset($val->id) && $menucontainer->internal_food_menu_id ==$val->id ? 'selected' : ''); ?>><?php echo e($val->$menu_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                  </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">
             <span class="english"><?php echo lang::get('mer_en_lang.MER_Dish_Name'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Dish_Name'); ?> </span>
 </label>
                  <div class="info100">
                    <div class="english">
                    <input type="text" class="english" name="dish_name" value="<?php echo e($menucontainer->dish_name); ?>" id="dish_name" maxlength="300" required="" placeholder="Dish Name">
                    </div>
                    <div class="arabic">
                    <input class="arabic ar" maxlength="300" id="dish_name_ar" value="<?php echo e($menucontainer->dish_name_ar); ?>" name="dish_name_ar" placeholder=" " value=""   type="text" >
                    </div>
                  </div>
                  </div>
                </div>
              
              

           
                <div class="form_row common_field">
                   <div class="form_row_left">
                   <label class="form_label disc_label">
                   <span class="english"><?php echo lang::get('mer_en_lang.MER_CONTAINER'); ?></span>
                 <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CONTAINER'); ?> </span>
                    </label>
					<label class="form_label disc_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); ?> %</span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); ?>  %</span> </label>
                    <div class="info100">
                      <?php $containername='title'?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $containername= 'title_'.$mer_selected_lang_code; ?>
                          <?php endif; ?>
                       <?php $__currentLoopData = $containerpackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                      $data =  Helper::getcontainer($val->id,$id);
                        ?>

                      <div class="container_row">
                       
                        <div class="label_fl"><?php echo e($val->$containername); ?></div>
                        <div class="input_flr"><input type="text" class="check notzero" onkeypress="return isNumber(event)" name="containerprice[]" value="<?php echo e(isset($data->container_price) ? $data->container_price : ''); ?>" maxlength="40"></div>
                         <input type="hidden" name="container_id[]" value="<?php echo e($val->id); ?>">
                        <div class="discount_col_2">
            <div class="discount_col">
                    
                    <div class="info100">
            
            <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount[]" onkeypress="return isNumber(event)" maxlength="2" value="<?php echo e(isset($data->discount) ? $data->discount : ''); ?>" id="discount" >
                      </div>
            
                    </div>
                    <!-- form_row -->
                  </div>
          </div>
                      </div>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    </div>
                </div>
             
                <span class="containersd"></span>




                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
                    <span class="english"><?php echo lang::get('mer_en_lang.MER_Dish_Image'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Dish_Image'); ?> </span>

          </label>
                  
                  <div class="input-file-area">
                  
                    <label for="company_logo">
                    
                    <div class="file-btn-area">
                    
                      <div id="file_value1" class="file-value"></div>
                      
                                        
                  <?php if($menucontainer->dish_image !=''): ?>
<span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
<?php endif; ?>
        
                      
                     <div class="form-upload-img">
                      <?php if(isset($menucontainer->dish_image) && $menucontainer->dish_image!=''): ?>
                    <img src="<?php echo e($menucontainer->dish_image); ?>"> <?php endif; ?> </div>
                      <div class="file-btn">
                      

       <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span>

             </div>
                    </div>
                    </label>
                    <input id="company_logo" name="dish_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" type="file">
                  </div>
                  </div>
                </div>
                
                <div class="form_row">
                <div class="form_row_left">
                  <input type="hidden" name="updatemc_img" value="<?php echo e($menucontainer->dish_image); ?>">
                  <input type="hidden" name="id" value="<?php echo e($id); ?>">
                  <input type="hidden" name="hid" value="<?php echo e($hid); ?>">
                  <input type="hidden" name="bid" value="<?php echo e($bid); ?>">

<div class="english">
	<input type="submit" name="submit" value="Update">
</div>
<div class="arabic ar">
 <input type="submit" name="submit" value="تحديث">
</div>
</div>
                </div>
              </form>

            
          </div> <!-- box -->
        </div>
      </div>
    </div>
  </div>
</div> <!-- merchant_vendor -->

<script>      
	$("form").data("validator").settings.ignore = "";
</script>
 
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  menucategory: {
                       required: true,
                      },

                       dish_name: {
                       required: true,
                      },
                       dish_name_ar: {
                       required: true,
                      },
                      price: {
                       required: true,
                      },
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       dish_image: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             menucategory: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SELECT_CATEGORY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SELECT_CATEGORY')); ?> <?php endif; ?>",
                      },  
                dish_name: {
           required:  " <?php echo lang::get('mer_en_lang.MER_VALIDATION_DISH_NAME'); ?>",
                      },

                 dish_name_ar: {
               required:  " <?php echo lang::get('mer_ar_lang.MER_VALIDATION_DISH_NAME_AR'); ?>",
                      }, 

                   price: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?>",
                      }, 

                    about: {
               required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); ?>",
                      }, 

                    about_ar: { 
               required:  "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_AR'); ?>",
                      },
                     dish_image: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

   <?php if($mer_selected_lang_code !='en'): ?>
                   if (typeof valdata.menucategory != "undefined" || valdata.menucategory != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.dish_name != "undefined" || valdata.dish_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.dish_image != "undefined" || valdata.dish_image != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                   if (typeof valdata.dish_name_ar != "undefined" || valdata.dish_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                       if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
                    if (typeof valdata.dish_name_ar != "undefined" || valdata.dish_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                       if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                  if (typeof valdata.menucategory != "undefined" || valdata.menucategory != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.dish_name != "undefined" || valdata.dish_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.dish_image != "undefined" || valdata.dish_image != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }


<?php endif; ?>
                    },

                submitHandler: function(form) {
                    $('.check').each(function() {    
                        if($(this).val() !='')
                        {
                       form.submit();
                        }
                        else
                        {
                         $('.containersd').html('<span class="error" style="text-align:left;">Please enter atleast one container price</span>'); 
                        }
                                  
                    });

                }
            });
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script> 

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
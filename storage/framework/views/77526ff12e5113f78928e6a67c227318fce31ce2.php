<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="UTF-8" />
<title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="_token" content="<?php echo csrf_token(); ?>"/>
<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
<?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
<?php endif; ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/switch.css" />
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
<?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
<!--END GLOBAL STYLES -->
<!-- PAGE LEVEL STYLES -->
<link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
<link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head><!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="padTop53 " >
<!-- MAIN WRAPPER -->
<div id="wrap" >
<!-- HEADER SECTION -->
<?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- END HEADER SECTION -->
<?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--PAGE CONTENT -->
<div class=" col-sm-9 col-xs-12 right-side-bar">
  <div class="right_col" role="main">
    <div id="appUserList">
      <div class="clearfix"></div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	  <?php if(Session::has('message')): ?>
       <div class="alert alert-success no-border">Amount refund successfully </div>
      <?php endif; ?>
        <div class="x_panel">
		<?php echo Form::open(array('url'=>"admin/search_insurance",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'studio_frm','id'=>'search_frm')); ?>

          <div class="page-title">
            <div class="title_left">
              <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE')); ?></h3>
            </div>
			 


          <!--  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hight_overhid">
			<?php if(Session::has('message')): ?>
			 <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
			<?php endif; ?>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <select v-model="branch" name="branch" id="branch" class="form-control">
                  <option value="" selected="selected"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_BRANCH_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BRANCH_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BRANCH_NAME')); ?></option>
                  <?php $__currentLoopData = $branchs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <option  value="<?php echo e($branch->mc_id); ?>"> <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>  <?php echo e($branch->mc_name_ar); ?>  <?php else: ?> <?php echo e($branch->mc_name); ?>  <?php endif; ?> </option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <select v-model="hall" name="hall" id="hall" class="form-control dtmagin">
                  <option value="" selected="selected"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_HALL_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HALL_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HALL_NAME')); ?></option>
                </select>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <button type="submit" :disabled="isProcessing" @click="search" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                <button type="submit" :disabled="isProcessing" @click="clearFilter" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
              </div>
            </div>-->






            <input id="lang" class="form-control" value="<?php echo e(Session::get('admin_lang_code')); ?>" type="hidden">
          </div>
		  <?php echo Form::close(); ?>

          <div class="x_content UsersList">
            <div class="table-responsive">
              <table class="table table-striped jambo_table bulk_action">
                <thead>
                  <tr class="headings">
                    <th class="column-title numrical_2"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> </th>
                    <th class="column-title"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ORDER_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_ID')); ?> </th>
                    <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_VENDOR_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDOR_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDOR_NAME')); ?> </th>
                    <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMER_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CUSTOMER_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMER_NAME')); ?></th>
                    <?php /* ?><th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_HALL_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HALL_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HALL_NAME') }}</span>
                            </th>
                            <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_BRANCH_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BRANCH_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BRANCH_NAME') }}</span>
                            </th>
							<?php */ ?>
                    <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_INSURANCE_AMT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INSURANCE_AMT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INSURANCE_AMT')); ?></span> </th>
					 <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_REFUND_AMT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REFUND_AMT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_REFUND_AMT')); ?></span> </th>
                    <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_BOOKING_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BOOKING_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BOOKING_DATE')); ?></span> </th>					
					 <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NOTES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NOTES') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NOTES')); ?></span> </th>
                    <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION')); ?></span> </th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
						if(count($productdetails) >0)
						{
						  //echo '<pre>';print_r($productdetails);
						  $i=1;
						  foreach($productdetails as $val)
						  { 
						   $getName = Helper::getServiceProviderName($val->merchant_id);
						?>
                  <tr>
                    <td><?php echo e($i); ?></td>
                    <td><?php echo e($val->order_id); ?></td>
                    <td><?php echo e($getName); ?></td>
                    <td><?php echo e($val->payer_name); ?></td>
                    <td>SAR <?php echo e($val->insurance_amount); ?></td>
					<td><?php if(isset($val->refund_amount) && ($val->refund_amount!='' || $val->refund_amount!='0' || $val->refund_amount!='0.00')){ echo 'SAR '.$val->refund_amount; }else{echo '0.00';} ?></td>
                    <td><?php echo date('Y M d',strtotime($val->order_date)); ?></td>
					<td><?php echo $val->refund_message; ?></td>
					<?php if(isset($val->refund_status) && $val->refund_status==1){?>
					<td class="last"><a href="javascript:void(0);" style="color:#00FF00;"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUCCESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUCCESS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUCCESS')); ?></td>
					<?php }else{?> 
                    <?php if(Session::get('access_permission_type')>0): ?>
                    <td class="last">
					<?php if(isset($val->refund_amount) && ($val->refund_amount!='' || $val->refund_amount!='0' || $val->refund_amount!='0.00')){ ?>
					<a href="<?php echo e(url('/admin/update_insurance')); ?>/<?php echo $val->cus_id; ?>/<?php echo $val->refund_amount; ?>/<?php echo $val->id; ?>/<?php echo $val->id; ?>/<?php echo $val->merchant_id; ?>" onClick="return confirm('Do you want to refund insurance amount SAR <?php if($val->refund_amount){ echo $val->refund_amount; }else{echo '0.00';}?>')" class="btn view-delete"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_REFUND_ADMIN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REFUND_ADMIN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_REFUND_ADMIN')); ?></a>
					<?php } else{ ?>
					<a href="javascript:void(0);" onClick="return confirm('No any refund insurance amount')"  class="btn view-delete"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_REFUND_ADMIN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REFUND_ADMIN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_REFUND_ADMIN')); ?></a>
					<?php } ?>
					</td>
                    <?php else: ?>
                    <td class="last">-</td>
                    <?php endif; ?>
					<?php } ?>
					
					
					
					
                  </tr>
                  <?php $i++;}}else{ ?>
                  <tr>
                    <td colspan="6"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php /*?><tbody>
                          <tr v-if="users" v-for="(ud, index) in users">
                            <td>
                          @{{ index+1 }}
                            </td>
                            <td>@{{ ud.get_merchant[0].mer_fname }} @{{ ud.get_merchant[0].mer_lname }}</td>
                            <td>@{{ ud.get_user[0].cus_name }}</td>
                            <td>@if(Session::get('admin_lang_file') == 'admin_ar_lang')
                              <p> @{{ ud.get_product[0].pro_title_ar }}</p>
                            @else <p> @{{ ud.get_product[0].pro_title }}</p> @endif</td>
                              <td>@if(Session::get('admin_lang_file') == 'admin_ar_lang') <p>@{{ ud.get_product[0].get_category[0].mc_name_ar }}</p>@else
                                <p>@{{ ud.get_product[0].get_category[0].mc_name }}</p>
                                @endif
                              </td>
                              <td>@{{ ud.get_product[0].Insuranceamount }}</td>
                            <td>
                              @{{ ud.created_date }}
                            </td>
                      
                            <td class="last">
                            <button  @click="ViewUser(ud.order_id)" class="btn view-delete"><i class="fa fa-eye"></i> </button>
                            </td>
                          </tr>
                          <tr v-if="users ==''">
                            <td colspan="8">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND') }}</td>
                          </tr>
                        </tbody><?php */?>
              </table>
              <div align="center"><?php echo e($productdetails->links()); ?></div>
            </div>
          </div>
          <!--<div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>-->
        </div>
      </div>
    </div>
  </div>
</div>



<!--END MAIN WRAPPER -->
<!-- FOOTER -->
<?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--END FOOTER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
<!-- END GLOBAL SCRIPTS -->
<!-- PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script type="text/javascript">
  $(document).on('change','#branch',function(){
        $.ajax({
          url: "/get_hall",
          data: {'pro_mc_id':$('#branch').val(),'flag':'branch',<?php if (Session::get('admin_lang_file') == 'admin_ar_lang') {?> 'language':'ar'<?php } else ?>'language':'en' <?php ?>},
          cache: false,
          success: function(html){
            setTimeout(function(){     
                $("#hall").html(html);
                }, 500);
          }
        });
    });
	
	
function Prodel(id,amt)
{
   var amt = amt;
   var con=confirm("Do you want to refund insurance amount SAR "+amt);
   if(con==true)
   {
     document.location.href="<?php echo e(url('/admin/hall_insurance/update_insurance')); ?>/?id="+id+"&amt="+amt+"";
   }
   else 
   {
    return false ;
   }   
}
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._users = [<?php echo $users->toJson(); ?>];
</script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/hall_insurance.js"></script>
</body>
<!-- END BODY -->
</html>

<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $reception_hospitality_leftmenu =1;
$cid = request()->cid;
$id = request()->id;
$shopid = request()->shopid;
 ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if(isset($cid) && $cid !=''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.UPDATE_ITEM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATE_ITEM')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATE_ITEM')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.ADD_ITEM')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADD_ITEM')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADD_ITEM')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        
        
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?> 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              
              
                <form name="form1" id ="menucategory" method="post" action="<?php echo e(route('recption-updateitems')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <div class="form_row common_field">
				   <div class="form_row_left">
                    <label class="form_label "> <span class="english"><?php echo lang::get('mer_en_lang.CATEGORY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.CATEGORY'); ?> </span> </label>
                    <?php               
                    $SecondCats=Helper::getAttributes($shopid);
                    ?>
                    <div class="info100">
                      <select name="attribute">
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?></option>                      
                        
                    <?php $__currentLoopData = $SecondCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                  
                        
                        <option value="<?php echo e($val->id); ?>" <?php if($val->id == $getAttr->attribute_id ): ?> selected="selected"  <?php endif; ?>  ><?php echo e($val->attribute_title); ?></option>
                        
                        
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                      
                      </select>
                    </div>
                  </div>
				   </div>
				  
                  <div class="form_row">
				   <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.ITEMNAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.ITEMNAME'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" id="itemname" maxlength="90"  name="itemname"  value="<?php echo e(isset($getAttr->pro_title) ? $getAttr->pro_title : ''); ?>"  required=""  type="text" >
                      </div>
                      <div class="arabic ar">
                        <input class="arabic ar" id="itemname_ar" maxlength="90"  name="itemname_ar"  value="<?php echo e(isset($getAttr->pro_title_ar) ? $getAttr->pro_title_ar : ''); ?>"  required=""  type="text" >
                      </div>
                    </div>
                  </div>    </div>
                  <div class="form_row common_field">
				   <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PRICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PRICE'); ?> </span> </label>
                    <div class="info100">
                      <input id="price" class="xs_small" onkeypress="return isNumber(event);" maxlength="10"  name="price" value="<?php echo e(isset($getAttr->pro_price) ? $getAttr->pro_price : ''); ?>"  required=""  type="text" >
                    </div>
                  </div> </div>

                     <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Discount'); ?> %</span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Discount_For_Sale'); ?>  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="7" value="<?php echo e(isset($getAttr->pro_discount_percentage) ? $getAttr->pro_discount_percentage : ''); ?>" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                     <div class="form_row_left common_field">
            <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.QTYSTOCK'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.QTYSTOCK'); ?> </span> </label>
                  <div class="info100">
                    <div>
                      <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)"  maxlength="5" name="qty" value="<?php echo e(isset($getAttr->pro_qty) ? $getAttr->pro_qty : ''); ?>" id="qty" required="">
                    </div>
                  </div>
          </div>
				  
                  <div class="form_row common_field">
				   <div class="form_row_left">
                    <input type="hidden" name="mc_id" value="">
                   <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.UPLOADIMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.UPLOADIMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="uploadimage"  class="info-file rimage" type="file"  > 
                      
                        <input type="hidden" name="updateimage" value="<?php echo e(isset($getAttr->pro_Img) ? $getAttr->pro_Img : ''); ?>">
                 
                   

  <div class="form-upload-img"><?php if(isset($getAttr->pro_Img) && $getAttr->pro_Img !=''): ?> <img src="<?php echo e($getAttr->pro_Img); ?>" width="150" height="150"> <?php endif; ?></div>

                         </div>
                      
                    </div>
                  </div>
 
                </div>




<!-- Add More product images start -->
        <?php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        ?>

        <?php for($i=0;$i < $GalleryCunt;$i++): ?>
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo<?php echo e($k); ?>">
        <div class="file-btn-area">
        <div id="file_value<?php echo e($J); ?>" class="file-value"></div>
        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
        </div>
        </label>
        <input id="company_logo<?php echo e($k); ?>" name="image[]" class="info-file proImgess" data-lbl="file_value<?php echo e($J); ?>" type="file" value="">
        </div>
        <?php if(isset($productGallery[$i]->image) && $productGallery[$i]->image!=''): ?>
        <div class="form-upload-img product_img_del">
        <img src="<?php echo e(isset($productGallery[$i]->image) ? $productGallery[$i]->image : ''); ?>" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="<?php echo e($productGallery[$i]->id); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value<?php echo e($J); ?>"></span>  
        <?php endif; ?>
        <input type="hidden" name="privius[]" value="<?php echo e($productGallery[$i]->id); ?>">
        </div>

        </div>
        <?php  $J=$J + 1; $k=$k +1; ?>
        <?php endfor; ?>
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>
        <span class="error pictureformat"></span>
        <?php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} ?>                
        <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
        </div>
  <!-- Add More product images end -->





                  
                  <div class="form_row"> 
				   <div class="form_row_left">
                    <input type="hidden" name="cid" value="<?php echo e(isset($cid) ? $cid : ''); ?>">
                    <input type="hidden" name="id" value="<?php echo e(isset($id) ? $id : ''); ?>">
                     <input type="hidden" name="shopid" value="<?php echo e(isset($shopid) ? $shopid : ''); ?>">
                    <?php if($cid==''): ?>
                    <div class="english">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                    </div>
                    <?php else: ?>
                    <div class="english">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="Update">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="تحديث">
                    </div>
                    <?php endif; ?> </div></div>
                </form>
               
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<script type="text/javascript">
 
$("#menucategory").validate({
                  ignore: [],
                  rules: {

                    itemname: {
                       required: true,
                      },
                     itemname_ar: {
                       required: true,
                      },
                     attribute: {
                     required: true,
                    },
                    price: {
                     required: true,
                    }, 

                      qty: {
                     required: true,
                    },                    
                   
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
             messages: {
             itemname: {
               required: "<?php echo e(trans('mer_en_lang.MER_VALIDATION_ITEM_NAME')); ?>",
                      }, 
               itemname_ar: {
               required: "<?php echo e(trans('mer_ar_lang.MER_VALIDATION_ITEM_NAME_AR')); ?>",
                      }, 
               attribute: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ATTRIBUTE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_ATTRIBUTE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ATTRIBUTE')); ?> <?php endif; ?>",
                      },            
               price: {
               required: " <?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?> ",
                      }, 

                       qty: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY')); ?> <?php endif; ?> ",
                      },
                  
              
                                                            
                     
                },
                invalidHandler: function(e, validation){
                    //console.log("invalidHandler : event", e);
                    //console.log("invalidHandler : validation", validation);

                    var valdata=validation.invalid;
                    console.log("invalidHandler : validation", valdata);

                    <?php if($mer_selected_lang_code !='ar'): ?>

                    if (typeof valdata.attribute != "undefined" || valdata.attribute != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                    else if (typeof valdata.itemname != "undefined" || valdata.itemname != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }    
                    else if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  
                    
                     else if (typeof valdata.uploadimage != "undefined" || valdata.uploadimage != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }                    
                  
                    else if (typeof valdata.itemname_ar != "undefined" || valdata.itemname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                    
                  <?php else: ?>




                    if (typeof valdata.attribute != "undefined" || valdata.attribute != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    } 
                    else if (typeof valdata.itemname_ar != "undefined" || valdata.itemname_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }    
                    else if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }  
                    
                     else if (typeof valdata.uploadimage != "undefined" || valdata.uploadimage != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }                    
                  
                    else if (typeof valdata.itemname != "undefined" || valdata.itemname != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                   



                  <?php endif; ?>


                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });


var createValidation = function() {
       
         
<?php if(!isset($getAttr->pro_Img )): ?> 



        $(".rimage").each(function() {         

            $(this).rules('remove');
            $(this).rules('add', {
             required: true,
             accept:'jpg|png|jpeg',
              messages: {
                required: " <?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?> ",                          
               accept  :"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
              }
            });
          });
<?php else: ?>   


$(".rimage").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
           
             accept:'jpg|png|jpeg',
              messages: {
                                       
               accept  :"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
              }
            });
          });    
        <?php endif; ?>


};

$(function() {
   createValidation();
});

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<script type="text/javascript">
  jQuery("#company_logo").change(function(){
  var fake = this.value.replace("C:\\fakepath\\", ""); 
  var fileExtension = ['png','jpeg','jpg','gif'];
  $(".certifications").html('');
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
  jQuery("#file_value1").html('');         
  jQuery(".certifications").html('Please upload valid image');
  }
  else
  {
  jQuery("#file_value2").html(fake); 
  }
  });
</script> 



<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         <?php if(Session::get('lang_file') =='ar_lang'): ?>
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        <?php else: ?>
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        <?php endif; ?>
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-shop-image')); ?>",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->

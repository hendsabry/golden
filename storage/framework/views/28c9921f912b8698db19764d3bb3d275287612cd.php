<?php //echo "<pre>"; print_r($servicecategoryAndservices); ?>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
	$Current_Currency = 'SAR'; 
  } 
  $cururl = request()->segment(count(request()));
?>
<div class="outer_wrapper">
<div class="vendor_header">
	<div class="inner_wrap">               
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($fooddateshopdetails[0]->mc_img); ?>" alt="" /></a></div>                      	
        </div> 
		<div class="vendor_header_right">
        <!-- vendor_header_left -->
        <div class="vendor_welc">
          <?php echo $__env->make("includes.language-changer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div class="vendor_name username"> <?php echo e((Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')); ?> <span>
            <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
            <ul class="vendor_header_navbar">
              <li><a href="<?php echo e(route('my-account-profile')); ?>"<?php if($cururl=='my-account-profile') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-ocassion')); ?>"<?php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-studio')); ?>"<?php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-security')); ?>"<?php if($cururl=='my-account-security') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-wallet')); ?>"<?php if($cururl=='my-account-wallet') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-review')); ?>"<?php if($cururl=='my-account-review') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')); ?></a></li>
              <li><a href="<?php echo e(route('my-request-a-quote')); ?>"<?php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')); ?></a></li>
              <li><a href="<?php echo e(route('change-password')); ?>"<?php if($cururl=='change-password') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')); ?></a></li>
              <li><a href="<?php echo e(url('login-signup/logoutuseraccount')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')); ?></a></li>
            </ul>
            </span></div>
          <?php if(Session::get('customerdata.token')!=''): ?>
          <?php $getcartnoitems = Helper::getNumberOfcart(); ?>
          <?php if($getcartnoitems>0): ?> <a href="<?php echo e(url('mycart')); ?>" class="vendor_cart"><img src="<?php echo e(url('')); ?>/themes/images/basket.png" /><span><?php echo e($getcartnoitems); ?></span></a> <?php endif; ?>
          <?php endif; ?> </div>
        <!-- vendor_header_right -->
        <?php if(count($otherbarnch)>1){ ?>
        <div class="select_catg">

          <div class="select_lbl"><?php if(Lang::has(Session::get('lang_file').'.Other_Branches')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Other_Branches')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Other_Branches')); ?> <?php endif; ?></div>
          <div class="search-box-field">
            <select class="select_drp" id="dynamic_select">
              <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Branch')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Branch')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Branch')); ?> <?php endif; ?></option>           
                         <?php $__currentLoopData = $otherbarnch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $otherbarnches): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e(url('')); ?>/clinicshop/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shop_id); ?>/<?php echo e($otherbarnches->mc_id); ?>"> <?php echo e($otherbarnches->mc_name); ?> </option>

                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
            </select>
          </div>
        </div>
        <?php } ?> </div>
		<!-- vendor_header_left -->
	</div>
</div>
<!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">

       <ul>
        <li><a href="#about_shop" class="active"><?php if(Lang::has(Session::get('lang_file').'.About_Shop')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.About_Shop')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.About_Shop')); ?> <?php endif; ?></a></li>

           <?php if(trim($fooddateshopdetails[0]->mc_video_url!='')): ?>
        <li><a href="#video"><?php if(Lang::has(Session::get('lang_file').'.Video')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Video')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Video')); ?> <?php endif; ?></a></li>
        <?php endif; ?>
		<?php if(count($fooddateshopreview) >=1): ?>
        <li><a href="#our_client"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></a></li>
		<?php endif; ?>
        <li><a href="#choose_package"><?php if(Lang::has(Session::get('lang_file').'.SELECT_DOC')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_DOC')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_DOC')); ?> <?php endif; ?></a></li>
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->




 
<div class="inner_wrap service-wrap diamond_space">
<div class="detail_page">	
<a name="about_shop" class="linking">&nbsp;</a>
    <div class="service_detail_row">
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
		  <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		   <?php 
		   	$im=$shopgallery->image;
		   $gimg=str_replace('thumb_','',$im);
		    ?>
             <li>
  	    	    <img src="<?php echo e($gimg); ?>" />
  	    		</li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  	    		 	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
           <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallerythumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

             <li>
  	    	    <img src="<?php echo e($shopgallerythumb->image); ?>" />
  	    		</li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  	    		
           
          </ul>
        </div>
      		</section>
            
        </div>
        
        <div class="service_detail">
        	<!-- Example DataTables Card-->
	   
        	<div class="detail_title"><?php echo e($fooddateshopdetails[0]->mc_name); ?></div>
            <div class="detail_hall_description"><?php echo e($fooddateshopdetails[0]->address); ?></div>
            <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
            <div class="detail_about_hall">
            	<div class="comment more"><?php echo e($fooddateshopdetails[0]->mc_discription); ?></div>
            </div>
              <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: <span>
			  <?php $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id); 
				$mc_name = 'ci_name'; 
				if(Session::get('lang_file')!='en_lang')
				{
				  $mc_name = 'ci_name_ar'; 
				}
				echo $getcityname->$mc_name; 
			  ?></span></div>

 <div class="detail_hall_dimention"><span class="tm_for"><?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.TIME')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?><?php endif; ?>: </span><span><?php echo e(isset($fooddateshopdetails[0]->opening_time) ? $fooddateshopdetails[0]->opening_time : ''); ?> - <?php echo e(isset($fooddateshopdetails[0]->closing_time) ? $fooddateshopdetails[0]->closing_time : ''); ?> </span></div>



            <?php  $homevisitcharges=$fooddateshopdetails[0]->home_visit_charge; ?>
 <?php if(isset($fooddateshopdetails[0]->google_map_address) && $fooddateshopdetails[0]->google_map_address!=''){
 $lat  = $fooddateshopdetails[0]->latitude;
$long  = $fooddateshopdetails[0]->longitude;
 ?>
		  <div class="detail_hall_dimention" id="map" style="height: 230px!important">  </div>
		  <?php } ?> 
             
        </div> 
       
    </div> <!-- service_detail_row -->
    
    
	      <div class="service-mid-wrapper">
		         	<a name="video" class="linking">&nbsp;</a>
        <?php if(trim($fooddateshopdetails[0]->mc_video_url!='')): ?>
        <div class="service-video-area">
         <div class="service-video-cont"><?php echo e($fooddateshopdetails[0]->mc_video_description); ?></div>
          <div class="service-video-box">
            <iframe class="service-video" src="<?php echo e($fooddateshopdetails[0]->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
		<?php endif; ?>


        <!-- service-video-area -->
        
         
           <?php if(count($fooddateshopreview) >=1): ?>
           <div class="service_list_row service_testimonial">
        	<a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                          <ul class="slides">
						  <?php $__currentLoopData = $fooddateshopreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						  <?php $userinfo = Helper::getuserinfo($customerreview->customer_id); ?>
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description"><?php echo e($customerreview->comments); ?></div>
                                    	<div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                                        <div class="testim_star"><img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png"></div>
                                    </div>
                                </div>
                            </li>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           
                          </ul>
                        </div>
            </section>
          </div>
        </div>  


        <?php endif; ?>  
          </div> <!-- service-mid-wrapper -->
	





<?php $tbl_field='service_id';  ?>

	<div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                 
                <?php    
					
					$k=1; $jk=0;  ?>
                	<?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<?php 
                 
                		if(count($categories->serviceslist)>0){
                		if($k==1){ $cl='select'; }else{ $cl=''; }                     
                	?>
                  <?php if(isset($categories->attribute_title) && $categories->attribute_title !=''): ?>


                <li><a href="#<?php echo e($k); ?>"  class="cat <?php echo e($cl); ?>" data-toggle="tab" <?php if(isset($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id) && $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id!=''): ?> onclick="getbeautyservice('<?php echo e($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');"  <?php endif; ?>>

                  <?php echo e(isset($categories->attribute_title) ? $categories->attribute_title : ''); ?>


                </a></li>



                <?php endif; ?>
                <?php $k++; $jk++; } ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
	
	<div class="service-display-section">
    <a name="choose_package" class="linking">&nbsp;</a>
	<div class="service-display-right">
	
			<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
		
		<?php  $z=1;     ?>
			<?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productservice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php    $k=count($productservice->serviceslist); 
						if($k >0 ){
			 ?>
			  <?php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } ?> 
		<div class="diamond_wrapper_main tab-pane fade <?php echo e($addactcon); ?>"  id="<?php echo e($z); ?>">
			
			
			
		
			<?php if($k < 6){ ?>
			<?php  $i=1;     ?>
			<div class="diamond_wrapper_inner ">
			<?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php
                    $img = str_replace('thumb_','',$getallcats->pro_Img);
                  ?>

      
									<div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row">
					  <a href="#" onclick="getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
						<div class="category_wrapper <?php if($k==3): ?> category_wrapper<?php echo e($k); ?>  <?php endif; ?>" style="background:url(<?php echo e(isset($img) ? $img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?>  </div></div>
						</div>
					</a>
				</div>
				 <?php $i=$i+1; ?>
				  
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div> 
				<!------------ 6th-------------->
				<?php }elseif($k==6){ ?>
				<?php $j=1; ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			 <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
			 <?php if($j==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($j==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
			 <?php if($j==3){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($j==5){ ?> <div class="row_4of5 rows5row"> <?php } ?>
			  <?php if($j==6){ ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					<a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div></div>
						</div>
					</a>
			<?php if($j==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($j==2){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($j==4){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($j==5){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($j==6){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $j=$j+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!------------ 7th-------------->
				<?php }elseif($k==7){ ?>
				<?php $l=1; ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
			
			 <?php if($l==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($l==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
			 <?php if($l==3){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($l==6){ ?> <div class="row_4of5 rows5row"> <?php } ?>
			  <?php if($l==7){ ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					 <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div></div>
						</div>
					</a>
			<?php if($l==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==2){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($l==5){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==6){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($l==7){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $l=$l+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!------------ 8th-------------->
				<?php }elseif($k==8){ ?>
				<?php $l=1; ?>
			
					<div class="diamond_wrapper_inner">
					<?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
				<?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
				<?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
				
			 <?php if($l==1){ $classrd='category_wrapper1'; ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($l==2){ ?>  <div class="row_3of5 rows5row"> <?php } ?> 
			 <?php if($l==4){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($l==6){ ?> <div class="row_3of5 rows5row"> <?php } ?>
			  <?php if($l==8){ $classrd='category_wrapper9'; ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					  <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div></div>
						</div>
					</a>
			<?php if($l==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==3){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($l==5){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==7){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($l==8){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $l=$l+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!---------- 9th ------------------->
		<?php }elseif($k==9){ ?>
		
		
					<div class="diamond_wrapper_inner">
	  			<?php $i=1; ?>
		  <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
		  

		 <?php if($i==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		  <?php if($i==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
		   <?php if($i==4){ ?> <div class="row_3of5 rows5row"> <?php } ?> 
		    <?php if($i==7){ ?>  <div class="row_4of5 rows5row"> <?php } ?> 
			<?php if($i==9){ ?>  <div class="row_5of5 rows5row"> <?php } ?> 
            <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
              <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></span></span>
              </span>
            </a>
		 <?php if($i==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($i==3){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($i==6){ ?> <div class="clear"></div> </div> <?php } ?> 
		    <?php if($i==8){ ?> <div class="clear"></div></div> <?php } ?> 
			<?php if($i==9){ ?>  <div class="clear"></div> </div> <?php } ?> 
		 
		    <?php $i=$i+1; ?>
		   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
	  
		  
        </div>
		
		
		
				
			<?php } ?>
				 	
				
		</div>
		<?php $z=$z+1; }?>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
		
		

		
		
	  </div>
  </div>
  <div class="diamond_shadow"><?php echo e($fooddateshopproducts->links()); ?></span></div>
  <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
	
	</div> <!-- service-display-right -->
<?php //print_r($beautyshopleftproduct); ?>
	 <?php echo Form::open(['url' => '#', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

	<!-- container-section -->
	<div class="service-display-left">
		<span id="selectedproduct">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="<?php echo e($beautyshopleftproduct->pro_Img); ?>" alt="" /></div>
          <div class="service-product-name"><?php echo e($beautyshopleftproduct->pro_title); ?></div>
          <div class="service-beauty-description">

<?php echo e(wordwrap( $beautyshopleftproduct->pro_desc, 10, "\n", true )); ?>

 </div>
          <div class="service-beauty-description"><b><?php echo e((Lang::has(Session::get('lang_file').'.Specialist')!= '')  ?  trans(Session::get('lang_file').'.Specialist'): trans($OUR_LANGUAGE.'.Specialist')); ?> <?php echo e($beautyshopleftproduct->specialistion); ?></b></div>
          <?php if(count($professionalbio)>0){ ?> 
          <div class="service_sidebar_list">
          	<div class="service_sidebar_list_title"><?php echo e((Lang::has(Session::get('lang_file').'.PROFESSIONAL_BIO')!= '')  ?  trans(Session::get('lang_file').'.PROFESSIONAL_BIO'): trans($OUR_LANGUAGE.'.PROFESSIONAL_BIO')); ?></div>
            <ul id="professional">
            	<?php $__currentLoopData = $professionalbio; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $professionalbiodata): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            	<li><?php echo e($professionalbiodata->attribute_title); ?></li>
            	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               
            </ul>
          </div>
           <?php } ?> 
            
     </span>
     <div class="container-section">
            <div class="total_food_cost">
            <div class="total_price"><?php echo e((Lang::has(Session::get('lang_file').'.Consultation_Fees')!= '')  ?  trans(Session::get('lang_file').'.Consultation_Fees'): trans($OUR_LANGUAGE.'.Consultation_Fees')); ?> : <span id="cont_final_price"> <?php echo e(currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)); ?> </span></div>
          </div>
    
        <?php $totalprice=$beautyshopleftproduct->pro_price;
        	if($category_id=='29'){ $cart_sub_type='cosmetic'; }else{ $cart_sub_type='skin'; }
         ?>
       <div class="btn_row">
          	
       		<input type="hidden" name="opening_time" id="opening_time" value="<?php echo e($beautyshopleftproduct->opening_time); ?>">
       		<input type="hidden" name="closing_time" id="closing_time" value="<?php echo e($beautyshopleftproduct->closing_time); ?>">

          	
            
            <div class="btn_row service_skin">
            <input type="button" value="<?php echo e((Lang::has(Session::get('lang_file').'.BOOK_APPOINTMENT')!= '')  ?  trans(Session::get('lang_file').'.BOOK_APPOINTMENT'): trans($OUR_LANGUAGE.'.BOOK_APPOINTMENT')); ?>" class="form-btn addto_cartbtn open_popup">
          </div>
		  <?php if(isset($fooddateshopdetails[0]->terms_conditions) && $fooddateshopdetails[0]->terms_conditions!=''){ ?>
		  <div class="terms_conditions"><a  href="<?php echo e($fooddateshopdetails[0]->terms_conditions); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')); ?></a></div>
		  <?php } ?>
          </div>

          </div>
        
          <!-- container-section -->
        </div>
<a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a>
<?php echo Form::close(); ?>

        
        <!-- service-display-left -->
      </div>
	
	</div> <!-- service-display-left -->
	</div> <!--service-display-section-->


  <!--service-display-section-->
<?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   
	  <!-- other_serviceinc --><!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div>

<div class="common_popup book_appointmentpopup">
	<?php echo Form::open(['url' => 'clinicshop/addcartproduct', 'method' => 'post', 'name'=>'booknowid', 'id'=>'booknowid', 'enctype' => 'multipart/form-data']); ?>

      	<div class="ring_review_popup">      
		<a href="javascript:void(0);" class="close_popup_btn">X</a>
        <div class="appointment_title"> <?php echo e((Lang::has(Session::get('lang_file').'.Appointment_for')!= '')  ?  trans(Session::get('lang_file').'.Appointment_for'): trans($OUR_LANGUAGE.'.Appointment_for')); ?></div>
        <div class="appointment_name"><span id="doct"><?php echo e($beautyshopleftproduct->pro_title); ?></span></div>
        
        <div class="appointment_wrap">
        	<div class="appointment_row app_date ">
            	<div class="checkout-form-cell">
                   <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.DATE')!= '')  ?  trans(Session::get('lang_file').'.DATE'): trans($OUR_LANGUAGE.'.DATE')); ?></div> 

                   <div class="checkout-form-bottom"><input data-selecteddateproduct='<?php echo e($beautyshopleftproduct->pro_id); ?>' data-branchid='<?php echo e($branchid); ?>' type="text" name="bookingdate" id="bookingdate" class="t-box checkout-small-box cal-t input-calendar mytest" onchange="mynewtimeslot();" >
                    
                   </div> 
                </div>
                <div class="checkout-form-cell">
                   <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.TIME')!= '')  ?  trans(Session::get('lang_file').'.TIME'): trans($OUR_LANGUAGE.'.TIME')); ?></div> 
                   <div class="checkout-form-bottom" id="bookingtimebasic">
                   	
                   
                  <input name="bookingtime" type="text" id="bookingtime" autocomplete="off" class="t-box cal-t" />
                   
                   </div> 
                </div>
            </div>
            
            <div class="appointment_row appointment_row_radio">            
            <div class="checkout-form-cell">
               <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.PATIENT_STATUS')!= '')  ?  trans(Session::get('lang_file').'.PATIENT_STATUS'): trans($OUR_LANGUAGE.'.PATIENT_STATUS')); ?></div> 
               <div class="checkout-form-bottom">
               <div class="gender-radio-box"><input  name="patienttype" id="patienttype" type="radio" value="newpatient" checked="checked" onclick="selectpatienttype(this.value);"> <label for="patienttype"><?php echo e((Lang::has(Session::get('lang_file').'.NEW')!= '')  ?  trans(Session::get('lang_file').'.NEW'): trans($OUR_LANGUAGE.'.NEW')); ?></label></div>
               <div class="gender-radio-box"><input  name="patienttype" id="patienttype1" type="radio" value="existpatient" onclick="selectpatienttype(this.value);"> <label for="patienttype1"><?php echo e((Lang::has(Session::get('lang_file').'.EXISTING')!= '')  ?  trans(Session::get('lang_file').'.EXISTING'): trans($OUR_LANGUAGE.'.EXISTING')); ?></label></div>
               </div> 
           </div>
            </div>
            

            	<div class="checkout-form-cell fns" id="fno" style="display: none;">
                   <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.ENTER_FILE_NO')!= '')  ?  trans(Session::get('lang_file').'.ENTER_FILE_NO'): trans($OUR_LANGUAGE.'.ENTER_FILE_NO')); ?></div> 
                   <div class="checkout-form-bottom"><input type="text" name="fileno" id="fileno" class="t-box checkout-small-box">
                   		
                   </div> 
                </div>

        </div>
         <span id="alertmsg"></span>
        <div class="btn_row">

        	  <input type="hidden" name="category_id" value="<?php echo e($subsecondcategoryid); ?>">
          	<input type="hidden" name="subsecondcategoryid" value="<?php echo e($category_id); ?>">
          	<input type="hidden" name="cart_sub_type" value="<?php echo e($cart_sub_type); ?>">
          	<input type="hidden" name="branch_id" value="<?php echo e($branchid); ?>">
          	<input type="hidden" name="shop_id" value="<?php echo e($shop_id); ?>">
          	<input type="hidden" name="vendor_id" value="<?php echo e($beautyshopleftproduct->pro_mr_id); ?>">
          	<input type="hidden" name="product_id" id="product_id" value="<?php echo e($beautyshopleftproduct->pro_id); ?>">
          	<input type="hidden" name="product_price" id="product_price" value="<?php echo e(currency($totalprice, 'SAR',$Current_Currency,$format = false)); ?>">
            <input type="hidden" name="todaydate" id="todaydate" value="<?php echo e(date('jMY')); ?>">
<input type="hidden" name="product_orginal_price" id="product_orginal_price" value="<?php echo e(currency($totalprice, 'SAR',$Current_Currency,$format = false)); ?>">
           
            <input type="submit" class="form-btn big-btn btn-info-wisitech" value="<?php echo e((Lang::has(Session::get('lang_file').'.BOOK_APPOINTMENT')!= '')  ?  trans(Session::get('lang_file').'.BOOK_APPOINTMENT'): trans($OUR_LANGUAGE.'.BOOK_APPOINTMENT')); ?>" />
        </div>
        
      </div> <!-- review_popup -->
      <?php echo Form::close(); ?>

	  </div>



</div> <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>

   

 

jQuery('#booknowid').submit(function()
{
 
 var bdate =  jQuery('#bookingdate').val();
  var btime =  jQuery('#bookingtime').val();
  if(btime == undefined)
  {
   var btime =  jQuery('#bookingtimesecond').val(); 
  }
 
 var productid =  jQuery('#product_id').val();
 
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('chkavailbility')); ?>?bdate="+bdate+"&btime="+btime+"&pid="+productid,
     success:function(res)
     {  
        if(res==1)
        {
        jQuery('#bookingtime').val('');
        jQuery('#bookingtime').val('');
        jQuery('#bookingtimesecond').val('');
        jQuery('#alertmsg').html("<span class='error'><?php echo e((Lang::has(Session::get('lang_file').'.Please_select_other_date')!= '')  ?  trans(Session::get('lang_file').'.Please_select_other_date'): trans($OUR_LANGUAGE.'.Please_select_other_date')); ?></span>");
        return false;
        }
        else
        {
        return true;
        }

     }
   });



});
 


jQuery(function(){
  jQuery('#dynamic_select').on('change', function () {
	  var url = jQuery(this).val(); // get selected value          
	  if (url) { // require a URL
		  window.location = url; // redirect
	  }
	  return false;
  });
});
</script>

<script>
jQuery(document).ready(function(){
 jQuery("#booknowid").validate({
    rules: {          
          "bookingdate" : {
            required : true
          },
          "bookingtime" : {
            required : true
          },
          fileno: { 
     		required: true
    		}
         },
         messages: {
          "bookingdate": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.BOOKINGDATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BOOKINGDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BOOKINGDATE')); ?> <?php endif; ?>"
          },
          "bookingtime": {
            required: "<?php if(Lang::has(Session::get('lang_file').'.BOOKINGTIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BOOKINGTIME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BOOKINGTIME')); ?> <?php endif; ?>"
          },
          "fileno": {
            required: "<?php if(Lang::has(Session::get('lang_file').'.FILE_NO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.FILE_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.FILE_NO')); ?> <?php endif; ?>"
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#booknowid").valid()) {        
    jQuery('#booknowid').submit();
   }
  });
});
</script>




<script type="text/javascript">
function selectpatienttype(ptype)
{

	if(ptype=='newpatient')
	{   
        jQuery('#fno').css('display','none');
     }
     if(ptype=='existpatient')
	{   
        jQuery('#fno').css('display','block');
        
     }
}
</script>








<script language="javascript">

$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});

</script>

<!------------------ tabs----------------->
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
</script>



<script src="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>

<!------------ end tabs------------------>

<!--------- date picker script-------->
 <script type="text/javascript">

function getCurrentTime(date) {
    var hours = date.getHours(),
        minutes = date.getMinutes(),
        ampm = hours >= 12 ? 'pm' : 'am';

  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;

  return hours + ':' + minutes + ' ' + ampm;
}


            // When the document is ready
			var jql = jQuery.noConflict();
            jql(document).ready(function () {


               
                 var currentTime= moment(); 
            var starttime=document.getElementById('opening_time').value;
                var endtime=document.getElementById('closing_time').value;

                 var mstartTime = moment(starttime, "HH:mm a");
                    var mendTime = moment(endtime, "HH:mm a");
                    
                   var amIBetween = currentTime.isBetween(mstartTime , mendTime);
                   if(amIBetween==false){
                       //var pdate= new Date(+new Date() + 86400000);
                       var pdate= new Date();
                   }else{
                      var pdate=new Date();
                   }                
      

              jql('#bookingdate').datepicker({
              //format: "yyyy-mm-dd",
              format: "d M yyyy",
              maxViewMode: 0,
              autoclose: true,
              startDate: pdate,
              }); 
              jql("#bookingdate").datepicker("setDate", pdate);

          
                 if(amIBetween==true){                  
                    
                    var st=getCurrentTime(new Date());
                 }else{
                  var st=starttime
                 }
                //alert('test');
 

				jql('#bookingtime').timepicker({
					'minTime': st,
					'maxTime': endtime,
					'timeFormat': 'g:i A',   
					'showDuration': false
				});

            });
        </script>



////////////// added code 01-mar-2019 ////////
<script type="text/javascript">
  function mynewtimeslot(){
 
jQuery('#bookingtime').timepicker('remove');

      var todaydate=jQuery('#todaydate').val();
var bookingdate=jQuery('#bookingdate').val();
       var starttime=document.getElementById('opening_time').value;
                var endtime=document.getElementById('closing_time').value;

var n= bookingdate.split(" ").slice(0, 3).join('');

if(todaydate==n){
 var timeNow = new Date();

        var currhours   = timeNow.getHours();

         var currmins   = timeNow.getMinutes();

         var hours   = timeNow.getHours()+1;

        var ampm = hours >= 12 ? 'pm' : 'am';

if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;

}else{


         var newstarttime=currhours+':'+'30 '+ampm;
}
}else{
       var newstarttime = starttime;
}



 

  jQuery('#bookingtime').timepicker({
  'minTime': newstarttime,
  'maxTime': endtime,
  'timeFormat': 'g:i A',
  'showDuration': false,
  });



  }
</script>

////////////// end code of 01-mar-2019 ////////






<!--------- end date picker script------>




<script type="text/javascript">


  function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }
 

 function OnKeyDown(event) { event.preventDefault(); };

 


	function getbeautyservice(selecteddateproduct,branchid,tbl){
		var Specialist = "<?php if(Lang::has(Session::get('lang_file').'.Specialist')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Specialist')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Specialist')); ?> <?php endif; ?>";
    var PROFESSIONAL_BIO = "<?php if(Lang::has(Session::get('lang_file').'.PROFESSIONAL_BIO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PROFESSIONAL_BIO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PROFESSIONAL_BIO')); ?> <?php endif; ?>";
    var Dates = jQuery('#bookingdate').val();

if(Dates == '' || Dates == undefined)
{
  Dates = date("Y-m-d");
}

		var dateproductid=selecteddateproduct;

			  if(selecteddateproduct){
        jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('clinicshop/getcartproduct')); ?>?product_id="+dateproductid+"&branchid="+branchid+"&tblfld="+tbl+"&chk="+Dates,
           success:function(res){  
           <?php $Cur = Session::get('currency'); ?>             
            if(res){
            	 var json = JSON.stringify(res);
			var obj = JSON.parse(json);
			//alert(obj);
  
            	
            		length=obj.productsericeinfo.length;
            		//alert(length);
            		
            		if(length>0){
		 for(i=0; i<length; i++)
			{
				if(obj.productprice==obj.productsericeinfo[i].pro_price){

					 TotalPricse = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					 
					var productrealprice='<span><?php echo $Cur; ?> '+TotalPricse+'</span>';
					var productrealpriceamount=TotalPricse;
				}else{
					 TotalPricsse = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					 productprices = getChangedPrice(obj.productprice); 
					var productrealprice='<span class="strike"><?php echo $Cur; ?> '+TotalPricsse+' </span><span> <?php echo $Cur; ?> '+obj.productprice+'</span>';
					var productrealpriceamount=productprices;
				}
				jQuery('#selectedproduct').html('<div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productsericeinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productsericeinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productsericeinfo[i].pro_desc+'</div><div class="service-beauty-description"><b>'+Specialist+' '+obj.productsericeinfo[i].specialistion+'</b></div><div class="service_sidebar_list"><div class="service_sidebar_list_title"><?php echo e((Lang::has(Session::get('lang_file').'.PROFESSIONAL_BIO')!= '')  ?  trans(Session::get('lang_file').'.PROFESSIONAL_BIO'): trans($OUR_LANGUAGE.'.PROFESSIONAL_BIO')); ?>  </div><ul id="professional"></ul></div></div>');
					
				jQuery('#doct').html(''+obj.productsericeinfo[i].pro_title);
				jQuery('[name=product_id]').val(obj.productsericeinfo[i].pro_id);
				jQuery('[name=opening_time]').val(obj.productsericeinfo[i].opening_time);
				jQuery('[name=closing_time]').val(obj.productsericeinfo[i].closing_time);
				
				jQuery('[name=product_price]').val(productrealpriceamount);
				jQuery('[name=product_orginal_price]').val(productrealpriceamount);
				jQuery('#totalprice').html(productrealprice);
				jQuery('[name=itemqty]').val(1);
				document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur; ?> '+productrealpriceamount;

				
				jQuery('#bookingtimebasic').html('');
      

				jQuery('#bookingtimebasic').html('<input name="bookingtime" type="text" id="bookingtimesecond" onkeydown="OnKeyDown(event)" autocomplete="off" class="t-box cal-t" data-pro="'+obj.productsericeinfo[i].pro_id+'" data-shop="'+branchid+'" data-start="'+obj.productsericeinfo[i].opening_time+'" data-end="'+obj.productsericeinfo[i].closing_time+'"   />');
  
 
			}
 
			professionbiolength=obj.expertstaff.length;
			if(professionbiolength>0){
				for(k=0; k<professionbiolength; k++)
			{
				jQuery('#professional').append('<li>'+obj.expertstaff[k].attribute_title+'</li>');
			}

			}

			
			}
           
           }
           }
        });
    }
 $('html, body').animate({
         scrollTop: ($('.service-display-left').offset().top)
      }, 'slow');

		
	}

 jQuery('body').on('mouseover','#bookingtimesecond', function(){
 var t1 = jQuery(this).data('start');
 var t2 = jQuery(this).data('end');
 var pro = jQuery(this).data('pro');
 var shop = jQuery(this).data('shop');
var Dates = jQuery('#bookingdate').val(); 
   
  jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('checkbookedtime')); ?>?product_id="+pro+"&branchid="+shop+"&chk="+Dates,
           success:function(res){  
                workerbookedslots= res.length;
                 workerbookedslot = [];
           
                  if(workerbookedslots>0){
                    for(lk=0; lk<workerbookedslots; lk++)
                    {
                     var inRange = [];                    
                    var tM = res[lk].split('~~');
                      inRange.push(tM[0]);
                      inRange.push(tM[1]);
                      workerbookedslot.push(inRange);
                     }
 

                  }
                  
                  jQuery('#bookingtimesecond').timepicker('remove'); 
                  
         jQuery('#bookingtimesecond').timepicker({
          'minTime': t1,
          'maxTime': t2,
          'timeFormat': 'g:i A',
          'dynamic' : true,
          'disableTimeRanges': workerbookedslot,
          'showDuration': false
        });
 
     
 
}});




 })

</script>




  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">

  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

 

</script>

<?php if(count($servicecategoryAndservices)>=1 && isset($servicecategoryAndservices[0]->serviceslist[0]->pro_id) && $servicecategoryAndservices[0]->serviceslist[0]->pro_id!=''): ?>

<script type="text/javascript"> 
jQuery(window).load(function(){
getbeautyservice('<?php echo $servicecategoryAndservices[0]->serviceslist[0]->pro_id;?>','<?php echo $branchid ?>','service_id');

})
 


</script>
<?php endif; ?>

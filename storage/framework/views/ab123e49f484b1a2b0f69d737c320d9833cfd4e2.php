<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="service_listingrow">
    <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.Assigned_Invitation')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Assigned_Invitation')); ?>  
      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Assigned_Invitation')); ?> <?php endif; ?></h5>
  </div>
  
  <div class="global_area">
    <div class="row">
      <div class="col-lg-12">
        <div class="table_wrap">
		  
          <div class="panel-body panel panel-default">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <?php if($getorderinvitation->count() < 1): ?>
            <div class="no-record-area"> <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?> </div>
            <?php else: ?>
			
            <div class="table dish-table">
              <div class="tr">
                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Order_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Order_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Order_id')); ?> <?php endif; ?></div>
                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></div>
                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Occasion_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Occasion_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Occasion_Name')); ?> <?php endif; ?></div>
                <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Venue')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Venue')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Venue')); ?> <?php endif; ?></div>

                <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Event_DATE_TIME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Event_DATE_TIME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Event_DATE_TIME')); ?> <?php endif; ?></div>

                 <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Number_of_Invitees')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Number_of_Invitees')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Number_of_Invitees')); ?> <?php endif; ?></div>
                  <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Invitations_Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Invitations_Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Invitations_Type')); ?> <?php endif; ?></div>
                  <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Price')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Price')); ?> <?php endif; ?></div>
                  <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIONS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIONS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIONS')); ?> <?php endif; ?></div>
              </div>
              <?php $totalamount =0; ?>
              <?php $__currentLoopData = $getorderinvitation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>              
             <?php $userinfo=Helper::getuserinfo($value->customer_id);
                  $electronicinvitationtproduct=Helper::getorderedelectronicinvitation($value->order_id,'invitations'); 
                  $amo=$electronicinvitationtproduct->total_price; 
                  $vatamonu = Helper::calculatevat($value->order_id,$amo);  
                  $totalamount= $vatamonu+$amo;    
              ?>

             <?php if($value->invitaion_mode==1){ $ocmode='Text Message'; }elseif($value->invitaion_mode==2){ $ocmode='Email'; }else{ $ocmode='Invitations Card';}

              ?>
              <div class="tr">
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Order_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Order_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Order_id')); ?> <?php endif; ?>"><?php echo e(isset($value->order_id) ? $value->order_id : ''); ?></div>
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?>">

                  <?php echo e($userinfo->cus_name); ?></div>
                <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Occasion_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Occasion_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Occasion_Name')); ?> <?php endif; ?>">  <?php echo e($value->occasion_name); ?> </div>
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Venue')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Venue')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Venue')); ?> <?php endif; ?>">   <?php echo e($value->venue); ?> </div>

                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Event_DATE_TIME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Event_DATE_TIME')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Event_DATE_TIME')); ?> <?php endif; ?>">   <?php echo e(Carbon\Carbon::parse($value->date)->format('F j, Y')); ?> / <?php echo e($value->time); ?>  </div>
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Number_of_Invitees')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Number_of_Invitees')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Number_of_Invitees')); ?> <?php endif; ?>">   <?php echo e($value->no_of_invitees); ?> </div>
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Note')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Note')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Note')); ?> <?php endif; ?>">   <?php echo e($ocmode); ?> </div>
                
                  <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Price')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Price')); ?> <?php endif; ?>">SAR <?php echo e(number_format($totalamount,2)); ?> </div>

                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIONS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIONS')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIONS')); ?> <?php endif; ?>"> 
                  <a  href="<?php echo e(url('')); ?>/manage_invitees_list/<?php echo e($value->order_id); ?>">
                          <img src="<?php echo e(url('')); ?>/public/assets/img/view-icon.png" title="<?php if(Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_view_title')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_view_title')); ?> <?php endif; ?>" alt="" /></a>


                        </div>
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
            <?php endif; ?>
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          </div>
          <?php echo e($getorderinvitation->links()); ?> </div>
      </div>
    </div>
  </div>
</div>
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         <?php if($mer_selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        <?php else: ?>
        $('#todata').html("End date should be greater than From date");
        <?php endif; ?>
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
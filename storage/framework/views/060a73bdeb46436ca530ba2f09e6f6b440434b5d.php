 <?php $data = app('App\Help'); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  

<?php $hall_leftmenu =1; ?>
<div class="merchant_vendor cont_add">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_REVIEWANDRATING')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_REVIEWANDRATING')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_REVIEWANDRATING')); ?> <?php endif; ?> </h5>
      </header>
      
      <!-- Display Message after submition --> 
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?> 
              <!-- Display Message after submition --> 
              
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
          
            <div class="table_wrap"> 
              <!-- Display Message after submition --> 
              
              
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!-- Display Message after submition -->
              <div class="panel-body panel panel-default"> <?php if($reviewrating->count() <1): ?>
                <div class="no-record-area"><?php if(Lang::has(Session::get('mer_lang_file').'.You_are_yet_to_get_reviews_or_ratings')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.You_are_yet_to_get_reviews_or_ratings')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.You_are_yet_to_get_reviews_or_ratings')); ?> <?php endif; ?></div>
                <?php else: ?>
                <div class="table revie_table">
                  <div class="tr">
                    <div class="table_heading product_name"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_GIVEN_BY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_GIVEN_BY')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_GIVEN_BY')); ?> <?php endif; ?></div>
                    <div class="table_heading product_name"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_RREVIEW_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_RREVIEW_DATE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_RREVIEW_DATE')); ?> <?php endif; ?></div>  
                    <div class="table_heading prod_comp"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMENT')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMENT')); ?> <?php endif; ?></div>
                    <div class="table_heading prod_rating"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_RATING')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_RATING')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_RATING')); ?> <?php endif; ?></div>
                  </div>
                  <?php $__currentLoopData = $reviewrating; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                  <?php $rating=($row->ratings/5)*100; ?>
                  <div class="tr">
                    <div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_GIVEN_BY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_GIVEN_BY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_GIVEN_BY')); ?> <?php endif; ?>"><?php echo e($data->CustName($row->customer_id)); ?> </div>
                    <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_RREVIEW_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_RREVIEW_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_RREVIEW_DATE')); ?> <?php endif; ?>"><?php echo e(date('d-M-Y',strtotime($row->created_at))); ?></div>
                    <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMENT')); ?> <?php endif; ?>"><?php echo e($row->comments); ?></div>
                    <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_RATING')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_RATING')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_RATING')); ?> <?php endif; ?>">

                      <div class="containerdiv">
                        <div>
                      <!-- <img src="https://image.ibb.co/jpMUXa/stars_blank.png" alt="img">-->
						<img src="<?php echo e(url('')); ?>/public/assets/img/stars_blank.png" alt="" />

                        </div>
                        <div class="cornerimage" style="width:<?php echo e($rating); ?>%;">
						<img src="<?php echo e(url('')); ?>/public/assets/img/stars_full.png" alt="" />

                        <!--<img src="https://image.ibb.co/caxgdF/stars_full.png" alt="">-->
						
                        </div>                     
                      </div>
                     </div> 
                  </div>   
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
              </div>
              <?php endif; ?> 
              
              </div>
                          <?php echo e($reviewrating->links()); ?> 
  
          </div>
        </div>
        
      </div> <!-- global_area -->
      
    </div>
  </div>
</div>
</div>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
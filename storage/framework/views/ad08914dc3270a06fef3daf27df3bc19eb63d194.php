<?php $__env->startSection('content'); ?>
    <?php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');

        if($Current_Currency =='') {
        $Current_Currency = 'SAR';
        }

    ?>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></h2>
                                <h2><?php echo e($vendordetails->address); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php if($lang != 'en_lang'): ?> <?php echo e($vendordetails->mc_discription); ?> <?php else: ?> <?php echo e($vendordetails->mc_discription); ?> <?php endif; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <?php if($vendordetails->google_map_address!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        <?php }  ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">Customer Reviews</div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php echo e($allreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <?php if($vendordetails->mc_video_url !=''): ?>    <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li class="active">
                            <div class="image"><a href="<?php echo e(route('photography-studio-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/photography-01.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('photography-studio-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>" <?php if(request()->type!=2): ?> class="select" <?php endif; ?>><?php echo e((Lang::has(Session::get('lang_file').'.Photography')!= '')  ?  trans(Session::get('lang_file').'.Photography'): trans($OUR_LANGUAGE.'.Photography')); ?> </a></h2>
                        </li>
                        <li >
                            <div class="image"><a href="<?php echo e(route('videography-studio-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/photography-02.png" alt=""></a></div>
                            <h2><a  <?php if(request()->type==2): ?> class="select" <?php endif; ?> href="<?php echo e(route('videography-studio-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Videography')!= '')  ?  trans(Session::get('lang_file').'.Videography'): trans($OUR_LANGUAGE.'.Videography')); ?> </a></h2>
                        </li>
                    </ul>
                </center>

                <br>
                <?php if(count($product) < 1): ?>
                    <?php echo e((Lang::has(Session::get('lang_file').'.No_product_found_in_this_category')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_category'): trans($OUR_LANGUAGE.'.No_product_found_in_this_category')); ?>

                <?php else: ?>

                <div class="slide-cats2 owl-carousel" dir="ltr">
                    <?php $f=1; ?>
                    <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item-cat title-in-img" onclick="redy('table<?php echo e($f); ?>','<?php echo e($pro->pro_id); ?>');">
                            <a href="#o" id="<?php echo e($pro->pro_id); ?>" class="redycls <?php if($f==1): ?> select <?php endif; ?>"><img src="<?php echo e($pro->pro_Img); ?>" alt="">
                                <p><?php echo e($pro->pro_title); ?></p>
                            </a>
                        </div>
                        <?php $f=$f+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                </div>
                <?php endif; ?>
                <?php $h=1; ?>
                <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="box kosha p-15 koha-ready redymades" id="table<?php echo e($h); ?>" <?php if($h!=1): ?> style="display: none;" <?php endif; ?>>

                        <form name="form1" method="post" id="add-kosha" action="<?php echo e(route('photography-add-to-cart')); ?>">
                        <?php echo e(csrf_field()); ?>

                        <div class="row" >
                            <div class="col-md-7">
                                <div class="carousel-owl owl-carousel" dir="ltr">
                                    <img src="<?php echo e($pro->pro_Img); ?>" alt="Image 1" />
                                </div>
                            </div>
                            <div class="col-md-5">
                                <h3>Description</h3>
                                <div class="box p-15">
                                    <p><?php echo nl2br($pro->pro_desc); ?></p>
                                </div>
                            </div>
                            <ul class="list-unstyled">

                                <?php if(count($pro->product_attribute)>=1): ?>
                                <li><?php echo e((Lang::has(Session::get('lang_file').'.Number_of_Cameras')!= '')  ?  trans(Session::get('lang_file').'.Number_of_Cameras'): trans($OUR_LANGUAGE.'.Number_of_Cameras')); ?> : <span class="price"><?php echo e($pro->product_attribute[1]->value); ?></span></li>
                                <li>No of pictures : <span><?php echo e($pro->product_attribute[0]->value); ?></span></li>
                                    <?php endif; ?>
                            </ul>

                        </div>
                        <div class="box p-15" style="margin: 15px 0 0">
                        <div class="row">
                            <div class="col-md-7">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="flex-input">
                                                <label><?php echo e((Lang::has(Session::get('lang_file').'.Date')!= '')  ?  trans(Session::get('lang_file').'.Date'): trans($OUR_LANGUAGE.'.Date')); ?> </label>
                                                <input class="t-box cal-t ncal" type="text" id="bookingdate<?php echo e($pro->pro_id); ?>" readonly="" required name="bookingdate<?php echo e($pro->pro_id); ?>" onmouseout="checkbookingitem('<?php echo e($pro->pro_id); ?>');" onchange="mynewtimeslot();">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="flex-input">
                                                <label><?php echo e((Lang::has(Session::get('lang_file').'.Time')!= '')  ?  trans(Session::get('lang_file').'.Time'): trans($OUR_LANGUAGE.'.Time')); ?></label>
                                                <input class="t-box sel-time ntime" type="text" id="bookingtime<?php echo e($pro->pro_id); ?>" required name="bookingtime<?php echo e($pro->pro_id); ?>" onkeydown="OnKeyDown(event)" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="flex-input">
                                                <label><?php echo e((Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')); ?></label>
                                                <input class="t-box" type="text" id="location<?php echo e($pro->pro_id); ?>" required name="location<?php echo e($pro->pro_id); ?>">
                                            </div>
                                        </div>
                                    </div>

                            </div>
                            <div class="col-md-5">
                                <p class="fw-700"><?php echo e((Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')); ?>:
                                        <?php
                                            $Disprice = $pro->pro_discount_percentage;
                                            $originalP = $pro->pro_price;

                                            if($Disprice==''){  $getAmount = 0; }else { $getAmount = ($originalP * $Disprice)/100; }

                                           $DiscountPricea = $originalP - $getAmount;
                                           $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');
                                        ?>
                                        <?php if($Disprice >=1): ?>
                                            <span class="strike">  <?php echo e(currency($originalP,'SAR',$Current_Currency)); ?>  </span> <span >  <?php echo e(currency($DiscountPrice,'SAR',$Current_Currency)); ?>  </span>
                                        <?php else: ?>
                                            <span > <?php echo e(currency($originalP,'SAR',$Current_Currency)); ?></span>
                                        <?php endif; ?>

                                        </p>
                                <div class="pull-right">
                                    <a href="">Terms and conditions</a>
                                    <input type="hidden" name="product_id" value="<?php echo e($pro->pro_id); ?>">
                                    <input type="hidden" name="cart_type" value="occasion">
                                    <?php if($lang != 'en_lang'): ?> <input type="hidden" name="language_type" value="ar"> <?php else: ?> <input type="hidden" name="language_type" value="en">  <?php endif; ?>
                                    <input type="hidden" name="attribute_id" value="143">
                                    <input type="hidden" name="cart_sub_type" value="video">
                                    <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                                    <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
                                    <input type="hidden" name="vid" value="<?php echo e(request()->vid); ?>">
                                    <input type="hidden" name="service_date" value="<?php echo e(request()->vid); ?>">
                                    <input type="hidden" name="service_time" value="<?php echo e(request()->vid); ?>">

                                    <input type="hidden" name="opening_time" id="opening_time" value="<?php echo e($vendordetails->opening_time); ?>">
                                    <input type="hidden" name="closing_time" id="closing_time" value="<?php echo e($vendordetails->closing_time); ?>">
                                    <input type="hidden" name="todaydate" id="todaydate" value="<?php echo e(date('m/d/Y')); ?>">

                                    <input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn" />

                                </div>
                            </div>
                        </div>
                    </div>
                        </form>
                </div>
                        <?php $h=$h+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


            </div>

            <center>
                <?php echo e($product->links()); ?>

                
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                
            </center>

        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>

    <script type="text/javascript">

        function OnKeyDown(event) { event.preventDefault(); };
    </script>

    <script type="text/javascript">
        // When the document is ready
        //var jql = jQuery;
        jQuery(window).load(function () {
        //     jQuery('.ncal').datepicker({
        //         autoclose: true,
        //         format: "d M yyyy",
        //         startDate: new Date(),
        //     });
        //
        //     jQuery(".ncal").datepicker("setDate", new Date());
        // $( function() {
        //     $( ".ncal" ).datepicker({
        //         format: "d M, yyyy",
        //         endDate: '+14m',
        //         autoclose: true,
        //         startDate: new Date(),
        //     });
        // });

            jQuery('.ncal').datepicker({
                'minTime': newstarttime,
                'maxTime': '11:30 PM',
                'dynamic': true,
                'timeFormat': 'g:i A',
            });

            var starttime=jQuery('#opening_time').val();
            var endtime=jQuery('#closing_time').val();




            d = Date.now();
            d = new Date(d);

            d = (d.getHours() > 12 ? d.getHours() - 12 : d.getHours()) + ':' + d.getMinutes() + ' ' + (d.getHours() >= 12 ? "PM" : "AM");





            var timeNow = new Date();

            var currhours   = timeNow.getHours();

            var currmins   = timeNow.getMinutes();

            var hours   = timeNow.getHours()+1;

            var ampm = hours >= 12 ? 'pm' : 'am';

            if(currmins>30){
                var newstarttime=hours+':'+'00 '+ampm;

            }else{


                var newstarttime=currhours+':'+'30 '+ampm;
            }


            jQuery('.ntime').timepicker('remove');
            jQuery('.ntime').timepicker({
                'minTime': newstarttime,
                'maxTime': '11:30 PM',
                'dynamic': true,
                'timeFormat': 'g:i A',
            });
        });
    </script>

    <script type="text/javascript">
        function checkbookingitem(pid){

            //$('#bookingdate'+pid).css('display','block');
            var endtime=jQuery('#bookingdate'+pid).val();





        }
    </script>

    <script type="text/javascript">
        function mynewtimeslot(){

            jQuery('.ntime').timepicker('remove');

            var todaydate=jQuery('#todaydate').val();
            var bookingdate=jQuery('.ncal').val();

            var n= bookingdate.split(" ").slice(0, 3).join('');
            if(todaydate==n){
                var timeNow = new Date();

                var currhours   = timeNow.getHours();

                var currmins   = timeNow.getMinutes();

                var hours   = timeNow.getHours()+1;

                var ampm = hours >= 12 ? 'pm' : 'am';

                if(currmins>30){
                    var newstarttime=hours+':'+'00 '+ampm;

                }else{


                    var newstarttime=currhours+':'+'30 '+ampm;
                }
            }else{
                var newstarttime = '12:00 AM';
            }





            jQuery('.ntime').timepicker({
                'minTime': newstarttime,
                'maxTime': '11:30 PM',
                'timeFormat': 'g:i A',
                'showDuration': false,
            });



        }
    </script>





    <script type="text/javascript">
        $('.selct').click(function()
        {
            var title =  $(this).data('title');
            var from =  $(this).data('from');
            var price =  $(this).data('price');
            var img =   $(this).data('img');
            var pid =   $(this).data('id');
            var attribute =   $(this).data('attribute');
            $(".kosha-selections-area").show();
            $(".info_"+from).html('');
            $(".info_"+from).append('<div class="selection-box"><div class="selection-img"><img src="'+img+'" alt="" /></div><div class="selection-name">'+title+'<input type="hidden" class="tprice" name="itemprice" value="'+price+'"><input type="hidden" name="product[attribute_id][]" value="'+attribute+'"><input type="hidden" name="product[pro_id][]" value="'+pid+'"></div><div class="selection-prise">SAR '+price+'</div></div>');
            var totalP = 0;
            var getP = 0;
            $( ".tprice" ).each(function() {
                getP =  $(this).val();
                totalP = parseFloat(totalP) + parseFloat(getP);
            });

            totalP = parseFloat(totalP).toFixed(2)

            $("#totalPrice").html('SAR '+ totalP);
        });

        function redy(num,id)
        {
            $('.redymades').css('display','none');
            $('#'+num).css('display','block');
            $('.redycls').removeClass('select');
            $('#'+id).addClass('select');
            $('.flexslider').resize();
        }

        function sela(num,id,ser){
            $('#displayCats').html(num);
            $(".kosha-select-line").hide();
            $(".items_"+ser).show();

            $(".attb").removeClass('select');
            $("#"+id).addClass('select');
        }
    </script>

    <style type="text/css">
        a.morelink {
            text-decoration:none;
            outline: none;
        }
        .morecontent span {
            display: none;
        }
    </style>

    <script type="text/javascript">
        function pricecalculation(act)
        {
            var no=1;
            var product_id      = document.getElementById('product_id').value;
            var product_size    = document.getElementById('product_size').value;
            var currentquantity = document.getElementById('qty').value;
            var unititemprice   = document.getElementById('priceId').value;
            if(act=='add')
            {
                var qty = parseInt(currentquantity)+parseInt(no);
            }
            else
            {
                if(parseInt(currentquantity)==1)
                {
                    var qty=parseInt(currentquantity)
                }
                else
                {
                    var qty=parseInt(currentquantity)-parseInt(no);
                }
            }

            if(product_size)
            {
                $.ajax({
                    type:"GET",
                    url:"<?php echo e(url('getSizeQuantity')); ?>?product_id="+product_id+'&product_size='+product_size+'&qty='+qty,
                    success:function(res)
                    {

                        if(res!='ok')
                        {
                            //alert(res);
                            alert('Not available quantity');
                            var qtyupdated=parseInt(currentquantity);
                            document.getElementById('qty').value=qtyupdated;
                        }
                        else
                        {
                            //alert(res);
                            //alert('er');
                            var producttotal = qty*unititemprice;
                            document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
                            document.getElementById('itemqty').value=qty;
                        }
                    }
                });
            }
            else
            {
                var producttotal = qty*unititemprice;
                document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
                document.getElementById('itemqty').value=qty;
            }

        }






        function showProductDetail(str,vendorId)
        {

            $.ajax({
                type:"GET",
                url:"<?php echo e(url('getShoppingProductInfo')); ?>?product_id="+str+'&vendor_id='+vendorId,
                success:function(res)
                {
                    if(res)
                    {
                        $('html, body').animate({
                            scrollTop: ($('.service-display-left').first().offset().top)
                        },500);
                        var json = JSON.stringify(res);
                        var obj = JSON.parse(json);
                        console.log(obj);
                        length=obj.productdateshopinfo.length;
                        //alert(length);
                        document.getElementById('qty').value=1;
                        if(length>0)
                        {
                            for(i=0; i<length; i++)
                            {
                                $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+obj.productdateshopinfo[i].pro_price+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productdateshopinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');
                                $('#cont_final_price').html('SAR '+obj.productdateshopinfo[i].pro_price);
                            }
                        }
                        if($.trim(obj.productattrsize) !=1)
                        {
                            $('#ptattrsize').html(obj.productattrsize);
                            $('#ptattrsizeenone').css('display','block');
                        }
                        else
                        {
                            $('#ptattrsizeenone').css('display','none');
                        }
                    }
                }
            });
        }
    </script>
    <script>
        $(document).ready(function()
        {
            var showChar = 200;
            var ellipsestext = "...";
            var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
            var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
            $('.more').each(function()
            {
                var content = $(this).html();
                if(content.length > showChar)
                {
                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar-1, content.length - showChar);
                    var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
                    $(this).html(html);
                }
            });

            $(".morelink").click(function(){
                if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>
    <script>
        jQuery(document).ready(function(){
            jQuery("#cartfrm").validate({
                rules: {
                    "product_size" : {
                        required : true
                    },
                },
                messages: {
                    "product_size": {
                        required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php endif; ?>'
                    },
                }
            });
            jQuery(".btn-info-wisitech").click(function() {
                if(jQuery("#cartfrm").valid()) {
                    jQuery('#cartfrm').submit();
                }
            });
        });
    </script>

    <script language="javascript">
        $('.add').click(function () {
            if ($(this).prev().val() < 99) {
                $(this).prev().val(+$(this).prev().val() + 1);
            }
        });
        $('.sub').click(function () {
            if ($(this).next().val() > 1) {
                if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
            }
        });
    </script>

    <?php if(request()->type!=''): ?>
        <script type="text/javascript">
            $(window).load(function(){
                setTimeout( function(){
                    $('.yy').trigger('click');
                }  , 1000 );
            })
        </script>
    <?php endif; ?>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $hall_leftmenu =1; ?>
<div class="merchant_vendor cont_add">
<?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
  <div class="inner">
    <header>
      <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')); ?>  
            <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_CONTAINER')); ?> <?php endif; ?> </h5>
           
      </header>
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <!-- Display Message after submition -->
            <?php if(Session::has('message')): ?>
            <div class="alert alert-info"><?php echo e(Session::get('message')); ?>  </div>
            <?php endif; ?>
            <!-- Display Message after submition -->
              <form name="form1" id="add-container" method="post" action="<?php echo e(route('storecontainerpackage')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                   <div class="form_row_left">
                   <label class="form_label">      
                  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container_Name')); ?>  
                 <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container_Name')); ?> <?php endif; ?>
                   </label>
                   <div class="info100">
                      
                      <select class="small-sel" name="title"  id="title">

                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_SELEST_CONTAINER.'.MER_SELEST_CONTAINER')); ?> <?php endif; ?></option>
                        <?php $__currentLoopData = $letter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                        <option value="<?php echo e($val); ?>" <?php echo e(isset($fetchfirstdata->option_title) && $fetchfirstdata->option_title==$val ? 'selected' : ''); ?>><?php echo e($val); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                      </select>
                    </div>  
                    </div>
                </div>
               
                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
<?php if(Lang::has(Session::get('mer_lang_file').'.MER_No_People')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_No_People')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_No_People')); ?> <?php endif; ?>
 
</label>
                  <div class="info100">
                    <input class="small-sel notzero" onkeypress="return isNumber(event)" maxlength="10" type="text" name="no_people" value="<?php echo Input::old('no_people'); ?>" id="no_people" required="">
                  </div>
                  </div>
                </div>
              
                <div class="form_row" style="display: none;">
                  <div class="form_row_left">
                  <label class="form_label">
                    <?php if(Lang::has(Session::get('mer_lang_file').'.MER_About')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_About')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_About')); ?> <?php endif; ?>

                 
            </label>
                  <div class="info100">
                   <div class="english"> 
                    <textarea  maxlength="500" name="about" id="about" rows="4" cols="50">This is tested </textarea>
                     </div>
                    <div class="arabic ar"> 
                    <textarea  maxlength="500" name="about_ar" id="about_ar " rows="4" cols="50">This is tested</textarea>
                    <input type="hidden" value="<?php echo e($hid); ?>" name="hid">
                     <input type="hidden" value="<?php echo e($bid); ?>" name="bid">
                    </div>
                    </div>
                    </div>
                 </div>
                <!-- form_row -->
                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
                         <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container_Image')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container_Image')); ?> <?php endif; ?>

 
          </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn">
                          <?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?>

 </div>
                      </div>
                      </label>
                      <input id="company_logo" name="img" class="info-file" type="file" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" required="" value="">
                    </div>
                  </div>
                  </div>
                </div>
                <!-- form_row -->
                
                <div class="form_row_left englissh">
                  <input type="submit" name="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SUBMIT')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SUBMIT')); ?> <?php endif; ?>">
                </div>

                   
                <!-- form_row -->
              </form>

            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                      no_people: {
                       required: true,
                      },
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       img: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
        
                 required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_SELECT_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_SELECT_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CONTAINER_SELECT_NAME')); ?> <?php endif; ?>",

                      },  
   
                title_ar: {
               required: " <?php echo e(trans('mer_ar_lang.MER_VALIDATION_CONTAINER_NAME')); ?> ",
                      },
                       no_people: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?>",
                      }, 

                    about: {
            required:  " <?php echo e(trans('mer_en_lang.MER_VALIDATION_ABOUT')); ?> ",
                      }, 
 
                    about_ar: {
               required:  " <?php echo e(trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR')); ?> ",
                      },
                     img: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

  <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                  
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
 if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                  
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }






<?php endif; ?>
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
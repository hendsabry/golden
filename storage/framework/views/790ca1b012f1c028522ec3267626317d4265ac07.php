<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>    
<?php $hall_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALL_PICTURE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALL_PICTURE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALL_PICTURE')); ?> <?php endif; ?> </h5>
         <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
      </header>
      <!-- Display Message after submition --> 
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?> 
              <!-- Display Message after submition -->  
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box commonbox"> 
              
                           
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
               <?php echo Form::open(array('url' => '/add_hallpicture','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'add_hallpicture', 'name' =>'add_hallpicture' )); ?> 

			  	 <div class="hall-top-form common_field"> 
                <div class="hall-top-form-label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD_HALLPICTURTE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD_HALLPICTURTE')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD_HALLPICTURTE')); ?> <?php endif; ?> </div>
                <div class="input-file-area">
                  <label for="company_logo">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
   <div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div>
                  </div>
                  </label>
                  <input id="company_logo" data-lbl="file_value1" name="hallpics[]" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
                </div> <!-- input-file-area -->
                 


                <div id="img_upload"></div>
                <?php if($errors->has('hallpics')): ?>
                <div class="error"> <?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_VALID_IMAGE')); ?> <?php endif; ?> </div>
                <?php endif; ?>
                <div class="img-upload-line">
                  <div id="add_button"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>
                  <span class="error pictureformat"></span>
              

                  <?php if(count($getDbC)>=1){ $Count =  count($getDbC);}else { $Count = 1; } ?> 
                  <input type="hidden" name="pro_id" value="<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>">
                  <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
                 
                </div>
                <!-- img-upload-line --> 
                </div>			
				
                <div class="imgpics-area common_field"> <?php if($getDbC >=1): ?> 
                
                <?php $__currentLoopData = $getDb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="imgpics-box">
                  <div class="imgpics"><img src="<?php echo $vals->image;  ?>" ></div>
                  <div class="imgpics-delet">

 
                    <a data-status="Active" data-id="<?php echo e($vals->image); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>



                  </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?> </div>
				
              	 <div class="hall-top-form">
              <div class="hall-video-section ">

                <div class="hall-video-label posrel common_field"><span><?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALL_UPLOADVIDEO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALL_UPLOADVIDEO')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALL_UPLOADVIDEO')); ?> <?php endif; ?></span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_youtube_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_youtube_add')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_youtube_add')); ?> <?php endif; ?></span></a></div>
                
                <div class="hall-video-form common_field">
                  <input type="url" name="youtubevideo" id="youtubevideo" value="<?php echo e($getVideos->video_url); ?>">
                  <?php if($errors->has('youtubevideo')): ?>
                  <div class="error"> <?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_URL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_URL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_URL')); ?> <?php endif; ?> </div>
                  <?php endif; ?>
                  
                  <input type="hidden" name="pro_id" value="<?php echo e(isset($_REQUEST['hid']) ? $_REQUEST['hid'] : ''); ?>">
                  
                <!-- hall-video-form -->
                 
                <div class="clear"></div>
              
                <div class="ex-show">Ex. https://youtu.be/IY9cIpidnOc</div>
                <div class="hall-video-area"> <?php if(isset($getVideos->video_url) && $getVideos->video_url!=''): ?>
                
                  <iframe class="hall-video-ifrem" src="<?php echo e(isset($getVideos->video_url) ? $getVideos->video_url : ''); ?>"></iframe>
                  <?php endif; ?> </div>
                <!-- hall-video-area --> 
              </div>
              
                
                

                 <div class="hall-video-section marb0">
                  <div class="hall-video-label"> <label class="form_label">
                     <span class="english"><?php echo lang::get('mer_en_lang.mer_about_video'); ?></span>
                     <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.mer_about_video'); ?> </span>
                  </label>
                    </div>
                 
                  <div class="hall-video-form">
                    <div class="english">
                      <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50" onkeyup="limitTextCount('about', 'divcount', 500,'<?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?>');" onkeydown="limitTextCount('about', 'divcount', 500,'<?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?>');"><?php echo e(isset($getVideos->mc_video_description) ? $getVideos->mc_video_description : ''); ?></textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar" rows="4" cols="50" onkeyup="limitTextCount('about_ar', 'divcount_ar', 500,'<?php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); ?>');" onkeydown="limitTextCount('about_ar', 'divcount_ar', 500,'<?php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); ?>');"><?php echo e(isset($getVideos->mc_video_description_ar) ? $getVideos->mc_video_description_ar : ''); ?></textarea>
                    </div>
                      <?php 
                      if(isset($getDb->about) && $getDb->about!='') 
                      {
                         $chkChar = 500 - strlen(trim($getDb->about)); 
                      }
                      else
                      {
                         $chkChar = 500;
                      }
                      if(isset($getDb->about_ar) && $getDb->about_ar!='') 
                      {
                         $chkChar_ar = 500 - mb_strlen(trim($getDb->about_ar),'utf8'); 
                      }
                      else
                      {
                         $chkChar_ar = 500;
                      }
                 
                      ?>
                    <div id="divcount" class="english"><?php echo e(isset($chkChar) ? $chkChar : '500'); ?> <?php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); ?></div>
                      <div id="divcount_ar" class="arabic ar"><?php echo e(isset($chkChar_ar) ? $chkChar_ar : '500'); ?>  <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); ?></div>
                    
                  </div>
                </div>
              </div>
              <!-- hall-video-section --> 

             

			<div class="form_row">
             <div class="form_row_left english">
                  <input type="submit" name="submit" value="Submit">
                </div>
             <div class="form_row_right arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div> 
            </div>
            </div>
                
                </div>
              <!-- hall-top-form --> 
              <?php echo Form::close(); ?>

              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
              
            
          </div>
        </div>
      </div>
      <!--  global_area  --> 
    </div>
</div>   <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script type="text/javascript">
		$(document).ready(function(){
		var maxField = 9;  
		var addButton = $('#add_button');  
		var wrapper = $('#img_upload');
		var x = <?php echo $Count; ?>;

		var y = x+1;  
		$(addButton).click(function(){  
		if(x < maxField){  
		x++; y++; 
		var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="hallpics[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div></div> ';
		$(wrapper).append(fieldHTML);  
		document.getElementById('count').value = parseInt(x);
		}
		});
		$(wrapper).on('click', '#remove_button', function(e){  
		e.preventDefault();
		$(this).parent('div').remove(); 
		x--;  
		document.getElementById('count').value = parseInt(x);
		});
		});
</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
     
    <div class="action_content"></div>
    <div class="action_btnrow">  <a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a> </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('change-status')); ?>",
        data: {activestatus:activestatus,id:id,from:'deletehallpics'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});

jQuery('body').on('change','.info-file', function(){
            $('.pictureformat').html("");


  var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
var gid = $(this).data('lbl');
 $('#'+gid).html('');
             $('.pictureformat').html("Only formats are allowed : 'jpeg', 'jpg', 'png', 'gif', 'bmp'");
        }

<?php if($Count > 9){ ?>;
 var gid = $(this).data('lbl');
$('#'+gid).html('');
$('.info-file').val('');
$('.pictureformat').html("You can only upload 10 images");

  <?php } ?>



        
});
 

</script>  

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
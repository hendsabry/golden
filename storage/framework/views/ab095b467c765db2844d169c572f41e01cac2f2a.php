<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <div class="inner_wrap"> 
<div class="homepage_category">
<?php $categorycount =1; ?>
<?php $__currentLoopData = $getbasecategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <?php
                    
					if($categorycount==1){
                    $mainclass = 'meeting_category';
                    $subclass = '';
                    $link_class = 'bussiness_link';
                    } else {
                    $mainclass = 'wedding_category';
                    $dstatus = 'flr';
                    $link_class = 'wedding_ocaasion';
                    }
                    ?> 


<?php if(Session::get('lang_file')=='ar_lang'){ $linkbtninfo=$val->mc_name_ar; }else{ $linkbtninfo=$val->mc_name;} ?>
		<div class="<?php echo e($mainclass); ?>">
      <div class="homepage_col2 <?php echo e($subclass); ?>">         
            <div class="homepage_img">
              <?php if(Session::get('customerdata.token')==''): ?>
          <a  href="<?php echo e(url('login-signup')); ?>" style="color:#FFFFFF; text-decoration:none;"> <img src="<?php echo e($val->mc_img); ?>" alt="<?php echo e($val->mc_name); ?>"> </a>
        <?php else: ?>
               <a class="<?php echo e($link_class); ?>" href="javascript:void(0);" onclick="postmastercategory(<?php echo e($val->mc_id); ?>);" style="color:#FFFFFF; text-decoration:none;"> <img src="<?php echo e($val->mc_img); ?>" alt="<?php echo e($val->mc_name); ?>"> </a>
         <?php endif; ?>
              
            </div>            
        </div>
        <div class="info_btn <?php echo e($link_class); ?>" onclick="postmastercategory(<?php echo e($val->mc_id); ?>);">
				<?php if(Session::get('customerdata.token')==''): ?>
				  <a  href="<?php echo e(url('login-signup')); ?>" style="color:#FFFFFF; text-decoration:none;"><?php echo e($linkbtninfo); ?> </a>
				<?php else: ?>
               <a class="<?php echo e($link_class); ?>" href="javascript:void(0);" onclick="postmastercategory(<?php echo e($val->mc_id); ?>);" style="color:#FFFFFF; text-decoration:none;"><?php echo e($linkbtninfo); ?> </a>
			   <?php endif; ?>
            </div>
    </div>
<?php $categorycount++; ?>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

            
</div> <!-- homepage_category -->


</div> <!-- innher_wrap -->
<div class="flower_bg"></div>
   
    
</div> <!-- outer_wrapper -->
<?php if(Session::get('customerdata.token')!=''): ?>
<div class="search_popup bussiness_search_popup">
<a href="javascript:void(0);" class="close_search">X</a>
   <div class="form_title"><?php echo e((Lang::has(Session::get('lang_file').'.BusinessMeetings')!= '')  ?  trans(Session::get('lang_file').'.BusinessMeetings'): trans($OUR_LANGUAGE.'.BusinessMeetings')); ?></div>
    <?php echo $__env->make('includes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div> <!-- search_popup -->  

<div class="search_popup wedding_search_popup">
	<a href="javascript:void(0);" class="close_search">X</a>
   <div class="form_title"><?php echo e((Lang::has(Session::get('lang_file').'.WeddingandOccasions')!= '')  ?  trans(Session::get('lang_file').'.WeddingandOccasions'): trans($OUR_LANGUAGE.'.WeddingandOccasions')); ?></div>
    <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div> <!-- search_popup -->  

<?php endif; ?>
<!-- MainBody End -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.js"></script>
<script language="javascript">
function postmastercategory(val){
 			
		
    var categoryID = val;    
    if(categoryID){
        $.ajax({
           type:"GET",
           url:"<?php echo e(url('getbussinessbycategoryid')); ?>?category_id="+categoryID,
           success:function(res){               
            if(res){
					if(categoryID==1){
						$("#bussinesslist").empty();
						$("#bussinesslist").append('<option value=""><?php echo e((Lang::has(Session::get('lang_file').'.Select_Business_Type')!= '')  ?  trans(Session::get('lang_file').'.Select_Business_Type'): trans($OUR_LANGUAGE.'.Select_Business_Type')); ?></option>');
						$.each(res,function(key,value){
								$("#maincategoryid").val(categoryID);
								$(".mysel").val(categoryID);
							
							$("#bussinesslist").append('<option value="'+res[key].id+'">'+res[key].title+'</option>');
						});
           		}
				if(categoryID==2){
						$("#weddingandoccasionlist").empty();
						$("#weddingandoccasionlist").append('<option value=""><?php echo e((Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '')  ?  trans(Session::get('lang_file').'.Select_Occasion_Type'): trans($OUR_LANGUAGE.'.Select_Occasion_Type')); ?></option>');
						$.each(res,function(key,value){
						//alert(res[key].title);
								
								$("#basecategoryid").val(categoryID);
								$(".mysel").val(categoryID);
								
							$("#weddingandoccasionlist").append('<option value="'+res[key].id+'" >'+res[key].title+'</option>');
						});
           		}
            }else{
               $("#bussinesslist").empty();
			    $("#weddingandoccasionlist").empty();
            }
           }
        });
    }else{
        $("#bussinesslist").empty();
		 $("#weddingandoccasionlist").empty();
         }      
   
	 
}

</script>
<?php echo $__env->make('includes.genralpopup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
 function postcitypop(str)
 {
  if(str==2)
  {
 jQuery('.citypop').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
  }


 }

 
 

</script>
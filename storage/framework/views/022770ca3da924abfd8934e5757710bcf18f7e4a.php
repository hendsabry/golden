<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $serviceleftmenu =1; ?>  
<div class="merchant_vendor">     


<div class="profile_box">
		 
			<div class="inner">
			
			<header>
						 
			<h5 class="global_head gds"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_SERVICESS')); ?>  
			<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICESS')); ?> <?php endif; ?> </h5><div class="backs"><a href="<?php echo e(url('sitemerchant_dashboard')); ?>" >
			
			 <?php if(Lang::has(Session::get('mer_lang_file').'.BACK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK')); ?>  
			<?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK')); ?> <?php endif; ?> 
			 


			
			</a></div>
			</header>
                    <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?> 
      <!-- Display Message after submition -->       
			<div class="global_area">	
				<div class="row">
				<div class="col-lg-12">
					<div class="add_service_page">
                    <div class="box commonbox">
			 
			<?php $Checkservics=array(); ?>

			<?php $__currentLoopData = $GetAlreadyServices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php $Checkservics[] = $vals->service_id; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 
			<!--PAGE CONTENT PART WILL COME INSIDE IT START-->

		<?php echo Form::open(array('url' => '/addservices','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'addservices', 'name' =>'addservices' )); ?>


			<?php $__currentLoopData = $getCatlists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catlists): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>		  
			<?php                
			$par_id = $catlists->mc_id;
			if($par_id == '1'){ continue;}
			$SecondCats=Helper::getChildCats($par_id);
			?>

			<?php $__currentLoopData = $SecondCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Seccatlists): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php
			$DataId = $Seccatlists->mc_id;
			?>
			<div class="addservices-row">
  			<div class="addservices-name">
         	<?php if(Config::get('app.locale') == 'ar'): ?> 
        	 <?php echo e($Seccatlists->mc_name_ar); ?>

        	<?php else: ?> 
        	 <?php echo e($Seccatlists->mc_name); ?>  
        	<?php endif; ?> 
 		     </div>

			<?php                
			$par_id = $Seccatlists->mc_id;

			$ThirdCats=Helper::getChildCats($par_id);
			?>
			<?php if(count($ThirdCats) >=1 && $par_id !='37' && $par_id !='871' && $par_id !='87'): ?>
			<?php $__currentLoopData = $ThirdCats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $thicatlists): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php
			$DataIds = $thicatlists->mc_id;
			if($thicatlists->mc_id ==16){ continue;}
			?>
 			<div class="addservices-cell">
		        <input id="<?php echo e($thicatlists->mc_id); ?>" type="checkbox" name="cat[]" <?php if(in_array($DataIds, $Checkservics)){ echo "CHECKED";  echo ' disabled=true'; }  ?>  value="<?php echo e($thicatlists->mc_id); ?>"> 
		       <label for="<?php echo e($thicatlists->mc_id); ?>">
		        	<?php if(Config::get('app.locale') == 'ar'): ?> 
		        	<?php echo e($thicatlists->mc_name_ar); ?>

		        	<?php else: ?> 
		        	<?php echo e($thicatlists->mc_name); ?>

		        	<?php endif; ?> 
		      </label>
		      	</div>

                 
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

 			<?php else: ?>
					<div class="addservices-cell"> 
					<input id="<?php echo e($Seccatlists->mc_id); ?>" type="checkbox" name="cat[]" <?php if(in_array($DataId, $Checkservics)){ echo "CHECKED"; echo ' disabled=true'; }  ?>  value="<?php echo e($Seccatlists->mc_id); ?>"> 
					<label for="<?php echo e($Seccatlists->mc_id); ?>">
					<?php if(Config::get('app.locale') == 'ar'): ?> 
					<?php echo e($Seccatlists->mc_name_ar); ?>

					<?php else: ?> 
					<?php echo e($Seccatlists->mc_name); ?>

					<?php endif; ?> 
					</label>
					</div>


                    <?php endif; ?>

			</div> <!-- addservices-row -->
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 
            <input type="submit" name="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_SERVICES')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICES')); ?> <?php endif; ?>">

        <?php echo Form::close(); ?>



				<!--PAGE CONTENT PART WILL COME INSIDE IT END-->

					</div>
                    </div> <!-- add_service_page -->
				</div>
				</div>
				</div> <!--  global_area  -->
	 		</div>
 
		</div> <!-- right_panel -->
		       
                
</div> <!-- merchant_vendor -->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
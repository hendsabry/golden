<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $MakupArtistbig_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
    
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              
             <form name="form1" method="post" id="addbranch" action="<?php echo e(route('store-makeup-artist-shop-info')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="mc_name" value="<?php echo e(isset($fetchdata->mc_name) ? $fetchdata->mc_name : ''); ?>" maxlength="255" value="<?php echo e(isset($getDb->pro_title) ? $getDb->pro_title : ''); ?>" data-validation-length="max35">
                    
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="mc_name_ar" value=" <?php echo e(isset($fetchdata->mc_name_ar) ? $fetchdata->mc_name_ar : ''); ?> "  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                
                  <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); ?> </span> 
                  <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span></a>
                  </label>
                  <div class="info100">
                    <input type="url" name="google_map_address"  data-validation="length required" 
                    
                     value=" <?php echo e(isset($fetchdata->google_map_address) ? $fetchdata->google_map_address : ''); ?>" data-validation-length="max100">
                     </div>
                 
                </div>

              </div>


<span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->longitude) ? $fetchdata->longitude : ''); ?>" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->latitude) ? $fetchdata->latitude : ''); ?>"  readonly="" name="latitude" />
          </div>
                  </div>

              

              <div class="form_row">
                <div class="form_row_left common_field">
                
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_IMAGE'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value1"></div>
                    </div>
                    </label>
                    <input type="file" name="branchimage" id="company_logo" class="info-file">
                  </div>
                  <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                  <?php if(isset($fetchdata->mc_img) && $fetchdata->mc_img !=''): ?>
                  <div class="form-upload-img"> <img src="<?php echo e($fetchdata->mc_img); ?>" width="150" height="150"> </div>  <?php endif; ?>
                  <?php if($errors->has('branchimage')): ?> <span class="error"> <?php echo e($errors->first('branchimage')); ?> </span> <?php endif; ?>
                   
                   </div>
                
                <!--<div class="form_row_right common_field">
                
                  <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); ?> </span> <a href="javascript:void(0);" class="address_image_tooltip">
               <span class="add_img_tooltip"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_address_img')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_address_img')); ?> <?php endif; ?></span>
              </a>
              </label>
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value2"></div>
                    </div>
                    </label>
                    <input type="file" name="address_image" id="company_logo1" class="info-file">
                  </div>
                  <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                  <?php if(isset($fetchdata->address_image) && $fetchdata->address_image !=''): ?>
                  <div class="form-upload-img">
                   <img src="<?php echo e($fetchdata->address_image); ?>" > </div><?php endif; ?>
                  <?php if($errors->has('address_image')): ?> <span class="error"> <?php echo e($errors->first('address_image')); ?> </span> <?php endif; ?>
                 
                   </div>-->
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.BACK_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.BACK_ADDRESS'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="address" maxlength="235"  data-validation="length required" 
     data-validation-error-msg="Please enter hall name" value="<?php echo e(isset($fetchdata->address) ? $fetchdata->address : ''); ?>" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="address_ar" value="<?php echo e(isset($fetchdata->address_ar) ? $fetchdata->address_ar : ''); ?>"  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CITY'); ?> </span> </label>
                  <div class="info100" >
                    <div class="englidsh">
                     <select name="city_id" id="status" class="city_type">
             <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?></option>
               <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?> <?php echo e($cbval->co_name); ?>  <?php endif; ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php $ci_id = $val->ci_id; ?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php echo e(isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''); ?> ><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
                    </div>
                  </div>
                
                </div>
              </div>
              <div class="form_row common_field">
                 
                <div class="form_row_left">
                 
                  <div class="info100" >
                    <div class="englishs">
                      <div class="small_time ">
             <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.OPENING'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.OPENING'); ?> </span> </label>
                      <select name="opening_time" id="opening_time" >
                        <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?></option> 
                        <option value="12:00 AM"<?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:00 AM' ? 'selected' : ''); ?>> 12:00 AM</option>
                        <option value="00:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='00:30 AM' ? 'selected' : ''); ?>> 00.30 AM</option>
                        <option value="1.00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='1.00 AM' ? 'selected' : ''); ?>> 1:00 AM</option>
                        <option value="1:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='1:30 AM' ? 'selected' : ''); ?>> 1:30 AM</option>
                        <option value="2:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:00 AM' ? 'selected' : ''); ?>> 2:00 AM</option>
                        <option value="2:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:30 AM' ? 'selected' : ''); ?>> 2:30 AM</option>
                        <option value="3:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='3:00 AM' ? 'selected' : ''); ?>> 3:00 AM</option>
                        <option value="3.30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='3.30 AM' ? 'selected' : ''); ?>> 3:30 AM</option>
                        <option value="4.00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='4.00 AM' ? 'selected' : ''); ?>> 4:00 AM</option>
                        <option value="4:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='4:30 AM' ? 'selected' : ''); ?>> 4:30 AM</option>
                        <option value="5:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:00 AM' ? 'selected' : ''); ?>> 5:00 AM</option>
                        <option value="5:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:30 AM' ? 'selected' : ''); ?>>5:30 AM</option>
                        <option value="6:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:00 AM' ? 'selected' : ''); ?>> 6:00 AM</option>
                        <option value="6:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:30 AM' ? 'selected' : ''); ?>> 6:30 AM</option>
                        <option value="7:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:00 AM' ? 'selected' : ''); ?>> 7:00 AM</option>
                        <option value="7:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:30 AM' ? 'selected' : ''); ?> > 7:30 AM</option>
                        <option value="8:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:00 AM' ? 'selected' : ''); ?>> 8:00 AM</option>
                        <option value="8:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:30 AM' ? 'selected' : ''); ?>> 8:30 AM</option>
                        <option value="9:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:00 AM' ? 'selected' : ''); ?>> 9:00 AM</option>
                        <option value="9:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:30 AM' ? 'selected' : ''); ?>> 9:30 AM</option>
                        <option value="10:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:00 AM' ? 'selected' : ''); ?>> 10:00 AM</option>
                        <option value="10:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:30 AM' ? 'selected' : ''); ?>> 10:30 AM</option>
                        <option value="11:00 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:00 AM AM' ? 'selected' : ''); ?>> 11:00 AM</option>
                        <option value="11:30 AM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:30 AM' ? 'selected' : ''); ?>> 11:30 AM</option>


                        <option value="12:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:00 PM' ? 'selected' : ''); ?>> 12:00 PM</option>
                        <option value="12:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:30 PM' ? 'selected' : ''); ?>> 12.30 PM</option>
                        <option value="1.00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='1.00 PM' ? 'selected' : ''); ?>> 1:00 PM</option>
                        <option value="1:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='1:30 PM' ? 'selected' : ''); ?>> 1:30 PM</option>
                        <option value="2:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:00 PM' ? 'selected' : ''); ?>> 2:00 PM</option>
                        <option value="2:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:30 PM' ? 'selected' : ''); ?>> 2:30 PM</option>
                        <option value="3:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='3:00 PM' ? 'selected' : ''); ?>> 3:00 PM</option>
                        <option value="3.30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='3.30 PM' ? 'selected' : ''); ?>> 3:30 PM</option>
                        <option value="4.00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='4.00 PM' ? 'selected' : ''); ?>> 4:00 PM</option>
                        <option value="4:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='4:30 PM' ? 'selected' : ''); ?>> 4:30 PM</option>
                        <option value="5:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:00 PM' ? 'selected' : ''); ?>> 5:00 PM</option>
                        <option value="5:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:30 PM' ? 'selected' : ''); ?>>5:30 PM</option>
                        <option value="6:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:00 PM' ? 'selected' : ''); ?> > 6:00 PM</option>
                        <option value="6:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:30 PM' ? 'selected' : ''); ?>> 6:30 PM</option>
                        <option value="7:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:00 PM' ? 'selected' : ''); ?>> 7:00 PM</option>
                        <option value="7:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:30 PM' ? 'selected' : ''); ?>> 7:30 PM</option>
                        <option value="8:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:00 PM' ? 'selected' : ''); ?>> 8:00 PM</option>
                        <option value="8:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:30 PM' ? 'selected' : ''); ?>> 8:30 PM</option>
                        <option value="9:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:00 PM' ? 'selected' : ''); ?>> 9:00 PM</option>
                        <option value="9:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:30 PM' ? 'selected' : ''); ?>> 9:30 PM</option>
                        <option value="10:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:00 PM' ? 'selected' : ''); ?>> 10:00 PM</option>
                        <option value="10:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:30 PM' ? 'selected' : ''); ?>> 10:30 PM</option>
                        <option value="11:00 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:00 PM' ? 'selected' : ''); ?>> 11:00 PM</option>
                        <option value="11:30 PM" <?php echo e(isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:30 PM' ? 'selected' : ''); ?>> 11:30 PM</option>

                       
                      </select>
                      </div>
                      <div class="small_time">
            <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.ClOSING'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.ClOSING'); ?> </span> </label>
                        <select name="closing_time" id="closing_titme" >
                        <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?></option> 
                        <option value="12:00 AM"<?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:00 AM' ? 'selected' : ''); ?>> 12:00 AM</option>
                        <option value="00:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='00:30 AM' ? 'selected' : ''); ?>> 00.30 AM</option>
                        <option value="1.00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='1.00 AM' ? 'selected' : ''); ?>> 1:00 AM</option>
                        <option value="1:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='1:30 AM' ? 'selected' : ''); ?>> 1:30 AM</option>
                        <option value="2:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:00 AM' ? 'selected' : ''); ?>> 2:00 AM</option>
                        <option value="2:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:30 AM' ? 'selected' : ''); ?>> 2:30 AM</option>
                        <option value="3:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='3:00 AM' ? 'selected' : ''); ?>> 3:00 AM</option>
                        <option value="3.30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='3.30 AM' ? 'selected' : ''); ?>> 3:30 AM</option>
                        <option value="4.00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='4.00 AM' ? 'selected' : ''); ?>> 4:00 AM</option>
                        <option value="4:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='4:30 AM' ? 'selected' : ''); ?>> 4:30 AM</option>
                        <option value="5:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:00 AM' ? 'selected' : ''); ?>> 5:00 AM</option>
                        <option value="5:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:30 AM' ? 'selected' : ''); ?>>5:30 AM</option>
                        <option value="6:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:00 AM' ? 'selected' : ''); ?>> 6:00 AM</option>
                        <option value="6:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:30 AM' ? 'selected' : ''); ?>> 6:30 AM</option>
                        <option value="7:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:00 AM' ? 'selected' : ''); ?>> 7:00 AM</option>
                        <option value="7:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:30 AM' ? 'selected' : ''); ?> > 7:30 AM</option>
                        <option value="8:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:00 AM' ? 'selected' : ''); ?>> 8:00 AM</option>
                        <option value="8:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:30 AM' ? 'selected' : ''); ?>> 8:30 AM</option>
                        <option value="9:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:00 AM' ? 'selected' : ''); ?>> 9:00 AM</option>
                        <option value="9:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:30 AM' ? 'selected' : ''); ?>> 9:30 AM</option>
                        <option value="10:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:00 AM' ? 'selected' : ''); ?>> 10:00 AM</option>
                        <option value="10:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:30 AM' ? 'selected' : ''); ?>> 10:30 AM</option>
                        <option value="11:00 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:00 AM AM' ? 'selected' : ''); ?>> 11:00 AM</option>
                        <option value="11:30 AM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:30 AM' ? 'selected' : ''); ?>> 11:30 AM</option>


                        <option value="12:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:00 PM' ? 'selected' : ''); ?>> 12:00 PM</option>
                        <option value="12:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:30 PM' ? 'selected' : ''); ?>> 12.30 PM</option>
                        <option value="1.00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='1.00 PM' ? 'selected' : ''); ?>> 1:00 PM</option>
                        <option value="1:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='1:30 PM' ? 'selected' : ''); ?>> 1:30 PM</option>
                        <option value="2:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:00 PM' ? 'selected' : ''); ?>> 2:00 PM</option>
                        <option value="2:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:30 PM' ? 'selected' : ''); ?>> 2:30 PM</option>
                        <option value="3:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='3:00 PM' ? 'selected' : ''); ?>> 3:00 PM</option>
                        <option value="3.30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='3.30 PM' ? 'selected' : ''); ?>> 3:30 PM</option>
                        <option value="4.00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='4.00 PM' ? 'selected' : ''); ?>> 4:00 PM</option>
                        <option value="4:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='4:30 PM' ? 'selected' : ''); ?>> 4:30 PM</option>
                        <option value="5:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:00 PM' ? 'selected' : ''); ?>> 5:00 PM</option>
                        <option value="5:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:30 PM' ? 'selected' : ''); ?>>5:30 PM</option>
                        <option value="6:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:00 PM' ? 'selected' : ''); ?> > 6:00 PM</option>
                        <option value="6:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:30 PM' ? 'selected' : ''); ?>> 6:30 PM</option>
                        <option value="7:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:00 PM' ? 'selected' : ''); ?>> 7:00 PM</option>
                        <option value="7:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:30 PM' ? 'selected' : ''); ?>> 7:30 PM</option>
                        <option value="8:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:00 PM' ? 'selected' : ''); ?>> 8:00 PM</option>
                        <option value="8:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:30 PM' ? 'selected' : ''); ?>> 8:30 PM</option>
                        <option value="9:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:00 PM' ? 'selected' : ''); ?>> 9:00 PM</option>
                        <option value="9:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:30 PM' ? 'selected' : ''); ?>> 9:30 PM</option>
                        <option value="10:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:00 PM' ? 'selected' : ''); ?>> 10:00 PM</option>
                        <option value="10:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:30 PM' ? 'selected' : ''); ?>> 10:30 PM</option>
                        <option value="11:00 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:00 PM' ? 'selected' : ''); ?>> 11:00 PM</option>
                        <option value="11:30 PM" <?php echo e(isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:30 PM' ? 'selected' : ''); ?>> 11:30 PM</option>
                       
                      </select>
                      </div>
                    </div>
                   
                  </div>
                    <?php if($mer_selected_lang_code !='en'): ?>
                    <span class="service_prov">24X7 مقدم الخدمة</span>
                    <?php else: ?>
                    <span class="service_prov">24X7 Service Provider</span>
                    <?php endif; ?>
                </div>
                 

              </div>
              <div class="form_row common_field">
        <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.SERVICEAVAIL'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.SERVICEAVAIL'); ?> </span> </label>
                  <div class="info100">
                    <div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" <?php echo e(isset($fetchdata->service_availability) && ($fetchdata->service_availability =='1' or $fetchdata->service_availability =='3')  ? 'checked' : ''); ?> name="service_available[]" value="1" id="type">
                      <label for="type"> <span class="english"><?php echo lang::get('mer_en_lang.HOME'); ?></span> <span class="arabic ar"><?php echo lang::get('mer_ar_lang.HOME'); ?> </span> </label>
                    </div>
                    <div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" <?php echo e(isset($fetchdata->service_availability) && ($fetchdata->service_availability =='2' or $fetchdata->service_availability =='3') ? 'checked' : ''); ?>  data-validation="checkbox_group" name="service_available[]" value="2" id="dish">
                      <label for="dish"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP'); ?></span> <span class="arabic ar"><?php echo lang::get('mer_ar_lang.MER_SHOP'); ?> </span> </label>
                    </div>
                      <span for="service_available[]" generated="true" class="error">  </span>
                  </div>
                </div>
                
                 <div class="homevisit common_field">
                <div class="form_row_right">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.HOMEVISIT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.HOMEVISIT'); ?> </span> </label>
                  <div class="info100" >
                    <div class="englssish">
                      <input type="text" class="englxish small-sel notzero" onkeypress="return isNumber(event)" name="home_visit_charge" maxlength="8"  data-validation="length required" 
                       value="<?php echo e(isset($fetchdata->home_visit_charge) ? $fetchdata->home_visit_charge : ''); ?>" >
                    </div>
                  </div>
                </div>
                </div>

              </div>
              <div class="form_row">
                 
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription) ? $fetchdata->mc_discription : ''); ?> </textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription_ar) ? $fetchdata->mc_discription_ar : ''); ?></textarea>
                    </div>
                  </div>
                </div>
                </div>
                
               <div class="form_row_right english">
                <div class="form_row">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo7">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value6"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc" id="company_logo7" class="info-file">
                  </div>
                  
                    <?php if(isset($fetchdata->terms_conditions)!=''): ?> 
                    <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                   <div class="pdf_icon">
                   <a href="<?php echo e($fetchdata->terms_conditions); ?>" target="_blank"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?></span></a>
                   
                   </div>
                   <input type="hidden" value="<?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?>" name="tmcvalue">
                   
                       <?php endif; ?> 
                </div>
                </div>
              
              <div class="form_row">
                <div class="form_row_left arabic ar">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                    <div class="input-file-area">
                      <label for="company_logo9">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        <div class="file-value" id="file_value8"></div>
                      </div>
                      </label>
                      <input type="file" name="mc_tnc_ar" id="company_logo9" class="info-file">
                    </div>
                    <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                    <?php if(isset($fetchdata->terms_conditions_ar)!=''): ?>
                    <div class="pdf_icon"> <a href="<?php echo e($fetchdata->terms_conditions_ar); ?>" target="_blank"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> </a> </div>
                    <input type="hidden" value="<?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?>" name="tmcvalue_ar">
                    <?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?>

                    <?php endif; ?> </div>
                </div>

              
               <div class="form_row_left common_field">
                    <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?> </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?> </option>

                      <option value="1" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==1){?> SELECTED <?php } ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?> </option>
                     <option value="0" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==0){?> SELECTED <?php } ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?>  </option>
                   </select>
                    
                    </div>
                  </div>

              
              <div class="form_row">
              <div class="form_row_left">
              <div class="form-btn-section english">
                   <input type="hidden" name ="parent_id" value="<?php echo e(isset($id) ? $id : ''); ?>">
                       <input type="hidden" name ="itemid" value="<?php echo e(isset($itemid) ? $itemid : ''); ?>">
                <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
              </div>
              <div class="form-btn-section arabic ar">
                <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
              </div>
              </div>
              </div>
   
            </form>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
</div>
<!-- merchant_vendor --> 


<script>
    
    $(document).ready(function(){
      
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
      if(checkedvalue==1) {  
      $('.homevisit').show();
    } else {
        $('.homevisit').hide();
    }

     $('#type').click(function(){
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
     if(checkedvalue==1) {
       $('.homevisit').show();
       } else {
           $('.homevisit').hide();

       }
     })

     $('#dish').click(function(){
       if(checkedvalue==1 || $('input#type').is(':checked')) {
        $('.homevisit').show();
      } else {
          $('.homevisit').hide();
      }
    
     })

    })

  </script>

<script>
	$("form").data("validator").settings.ignore = "";
</script> 

<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },

                      "service_available[]": {
                       required: true,
                      },

                       manager: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       address: {
                       required: true,
                      },
                       address_ar: {
                       required: true,
                      },

                       about: {
                       required: true,
                      },
                       about_ar: {
                       required: true,
                      },
                       <?php if(isset($fetchdata->mc_img)!=''): ?>  
                        branchimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       <?php else: ?>
                        branchimage: {
                           required:true,
                       accept:"png|jpeg|jpg|gif",        
                      },
                      <?php endif; ?>
                      <?php if(isset($fetchdata->address_image)!=''): ?>  
                        address_image: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       <?php else: ?>
                        address_image: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      <?php endif; ?>
                      
                       <?php if(isset($fetchdata->terms_conditions)!=''): ?> 
                        mc_tnc: {
                           required:false,
                           accept:"pdf",
                      },
                    <?php else: ?>
                         mc_tnc: {
                           required:true,
                           accept:"pdf",
                      },
                      <?php endif; ?>

                      <?php if(isset($fetchdata->terms_conditions_ar)!=''): ?> 
                        mc_tnc_ar: {
                           required:false,
                           accept:"pdf",
                      },
                    <?php else: ?>
                         mc_tnc_ar: {
                           required:true,
                           accept:"pdf",
                      },
                      <?php endif; ?>

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             city_id: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?>",
                      },  

                 "service_available[]": {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SERVICE_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SERVICE_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR')); ?> <?php endif; ?>",
                      },  
                          mc_tnc: {
                                required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                                accept: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                        },

                       mc_tnc_ar: {
                                required: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); ?>",
                                accept: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); ?>",
                      },


                         mc_name: {

                         required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_SHOPNAME'); ?>", 
                      },   

                          mc_name_ar: {
                  required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOPNAME_AR'); ?>",
                      },   

            

                       address: {
            
                         required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
  
                       address_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
                        
                       about: {
             

               required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); ?>",
                      },
  
                       about_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); ?>",
                      },
                  branchimage: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },                                 
                      address_image: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      }, 
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    
                     

                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }



                  if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                     

                    }




                    if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    

                    }

                     if (typeof valdata.address_image != "undefined" || valdata.address_image != null) 
                    {

                        $('.arabic_tab').trigger('click');
                        

                    }
                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                    {

                        $('.arabic_tab').trigger('click');
                        

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
          <?php else: ?>


             if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
 
            if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                  
                     if (typeof valdata.address_image != "undefined" || valdata.address_image != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }

                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                  

<?php endif; ?>

                    },

                submitHandler: function(form) {
                   var mapAdd = jQuery('input[name=google_map_address]').val();
                              if(mapAdd !='')
                              {
                              var long = jQuery('input[name=longitude]').val();
                              var lat  =  jQuery('input[name=latitude]').val();
                              if(long =='' && lat=='')
                              {
                              var allOk = 0;

                              }
                              else
                              {
                              var allOk = 1; 
                              }
                              }
                              else
                              {
                              var allOk = 1;
                              }

                              if(allOk == 1)
                              {
                              form.submit();
                              }
                              else
                              {
                              $('#maperror').html("<span class='error'><?php if(Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.get_Lat_Long_issue')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue')); ?> <?php endif; ?></span>");
                              }
                }
            });

 
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script> 

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
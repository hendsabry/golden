<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
	$Current_Currency = 'SAR'; 
  } 
?>
<div class="outer_wrapper">
  <div class="inner_wrap">
    <div class="vendor_header">
      <div class="inner_wrap">
        <div class="vendor_header_left">
          <div class="vendor_logo" ><a href="javascript:void(0);"><img src="<?php echo e($fooddateshopdetails[0]->mc_img); ?>" alt="" style="max-height: 168px;" /></a></div>
        </div>
        <!-- vendor_header_left -->
        <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </div>
    </div>
    <!-- vemdor_header -->
    <div class="common_navbar">
      <div class="inner_wrap">
        <div id="menu_header" class="content">
          <ul>
            <li><a href="#about_shop" class="active"><?php if(Lang::has(Session::get('lang_file').'.About_Shop')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.About_Shop')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.About_Shop')); ?> <?php endif; ?></a></li>
            <li><a href="#video"><?php if(Lang::has(Session::get('lang_file').'.Video')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Video')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Video')); ?> <?php endif; ?></a></li>
			<?php if(count($fooddateshopreview) > 0){ ?>
            <li><a href="#our_client"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></a></li>
			<?php } ?>
            <li><a href="#choose_package"><?php if(Lang::has(Session::get('lang_file').'.Choose_Package')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Choose_Package')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Choose_Package')); ?> <?php endif; ?></a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- common_navbar -->
    <div class="inner_wrap service-wrap diamond_space">
      <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
        <div class="service_detail_row">
          <div class="gallary_detail">
            <section class="slider">
              <div id="slider" class="flexslider">
                <ul class="slides">
                  <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php 
                  $im=$shopgallery->image;
                  $gimg=str_replace('thumb_','',$im);
                  ?>
                  <li> <img src="<?php echo e($gimg); ?>" /> </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                <ul class="slides">
                  <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallerythumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li> <img src="<?php echo e($shopgallerythumb->image); ?>" /> </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
		  <?php $cid=$fooddateshopdetails[0]->city_id;
        $getCity = Helper::getcity($cid);
        if(Session::get('lang_file') !='en_lang'){
        $city_name= 'ci_name_ar';
        }else{
        $city_name= 'ci_name'; 
        }  
        
        ?>
          <div class="service_detail">
            <!-- Example DataTables Card-->
            <div class="detail_title"><?php echo e($fooddateshopdetails[0]->mc_name); ?></div>
            <div class="detail_hall_description"><?php echo e($fooddateshopdetails[0]->address); ?></div>
            <div class="detail_hall_subtitle"><?php if(Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ABOUT_SHOP')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?> <?php endif; ?></div>
            <div class="detail_about_hall">
              <div class="comment more"><?php echo e($fooddateshopdetails[0]->mc_discription); ?></div>
            </div>
            <div class="detail_hall_dimention"><?php if(Lang::has(Session::get('lang_file').'.CITY')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.CITY')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.CITY')); ?><?php endif; ?>: <span><?php echo e($getCity->$city_name); ?></span></div>
             <?php if($fooddateshopdetails[0]->google_map_address!=''){  $lat='28.5561912';   $long='77.2512172';    ?>
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          <?php }  ?> 
            <?php  $homevisitcharges=$fooddateshopdetails[0]->home_visit_charge; ?> </div>
        </div>
        <!-- service_detail_row -->
        <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
		  <?php if(isset($fooddateshopdetails[0]->mc_video_url) && $fooddateshopdetails[0]->mc_video_url!=''){ ?>
          <div class="service-video-area">
            <div class="service-video-cont"><?php echo e($fooddateshopdetails[0]->mc_video_description); ?>



            </div>
            <div class="service-video-box">
              <iframe class="service-video" src="<?php echo e($fooddateshopdetails[0]->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>
		  <?php } ?>
          <!-- service-video-area -->
		  <?php if(count($fooddateshopreview) > 0){ ?>
          <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
            <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></div>
            <div class="testimonial_slider">
              <section class="slider">
                <div class="flexslider1">
                  <ul class="slides">
                    <?php $__currentLoopData = $fooddateshopreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                      <div class="testimonial_row">
                        <div class="testim_left">
                          <div class="testim_img"><img src="<?php echo e($customerreview->cus_pic); ?>"></div>
                        </div>
                        <div class="testim_right">
                          <div class="testim_description"><?php echo e($customerreview->comments); ?></div>
                          <div class="testim_name"><?php echo e($customerreview->cus_name); ?></div>
                          <div class="testim_star"><img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png"></div>
                        </div>
                      </div>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
                </div>
              </section>
            </div>
          </div>
		  <?php } ?>
        </div>
        <!-- service-mid-wrapper -->
        <?php $tbl_field='service_id'; if(count($servicecategoryAndservices)>0){ ?>
        <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
          <div class="service_line">
            <div class="service_tabs">
              <div id="content-5" class="content">
                <ul>
                  <?php $k=1; ?>
                  <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php 
                  if(count($categories->serviceslist)>0){
                  if($k==1){ $cl='select'; $HH = $categories->serviceslist[0]->pro_id; }else{ $cl=''; } 
                  ?>
                  <li><a href="#<?php echo e($k); ?>" data-tabid="<?php echo e($k); ?>" class="cat <?php echo e($cl); ?>" data-toggle="tab" onclick="getbeautyservice('<?php echo e($categories->serviceslist[0]->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>','<?php echo e($k); ?>');"><?php echo e($categories->attribute_title); ?></a></li>
                  <?php $k++; } ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </div>
            <div class="service_bdr"></div>
          </div>
        </div>
        <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
          <div class="service-display-right">
            <div class="diamond_main_wrapper">
              <div class="diamond_wrapper_outer"> <?php  $z=1;     ?>
                <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productservice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php    $k=count($productservice->serviceslist); 
                if($k >0 ){
                ?>
                <?php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } ?>
                <div class="diamond_wrapper_main tab-pane fade <?php echo e($addactcon); ?>"  id="<?php echo e($z); ?>"> <?php if($k < 6){ ?>
                  <?php  $i=1;     ?>
                  <div class="diamond_wrapper_inner "> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                      if($k<=3)
                  {
                  $bgImg = str_replace('thumb_','',$getallcats->pro_Img);  
                  }
                  else
                  {
                  $bgImg = $getallcats->pro_Img;  
                  }
                    ?>
                    <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
                      <div class="category_wrapper" style="background:url(<?php echo e(isset($bgImg) ? $bgImg : ''); ?>);">
                        <div class="category_title">
                          <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                        </div>
                      </div>
                      </a> </div>
                    <?php $i=$i+1; ?>
                    
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!------------ 6th-------------->
                  <?php }elseif($k==6){ ?>
                  <?php $j=1; ?>
                  <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                    <?php if($j==1){ ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($j==2){ ?>
                      <div class="row_2of5 rows5row"> <?php } ?> 
                        <?php if($j==3){  ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($j==5){ ?>
                          <div class="row_4of5 rows5row"> <?php } ?>
                            <?php if($j==6){ ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
                              <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                                <div class="category_title">
                                  <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                                </div>
                              </div>
                              </a> <?php if($j==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($j==2){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==4){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?>
                      <?php if($j==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php $j=$j+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!------------ 7th-------------->
                  <?php }elseif($k==7){ ?>
                  <?php $l=1; ?>
                  <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                    
                    <?php if($l==1){ ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($l==2){ ?>
                      <div class="row_2of5 rows5row"> <?php } ?> 
                        <?php if($l==3){  ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($l==6){ ?>
                          <div class="row_4of5 rows5row"> <?php } ?>
                            <?php if($l==7){ ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
                              <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                                <div class="category_title">
                                  <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                                </div>
                              </div>
                              </a> <?php if($l==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($l==2){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==5){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?>
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php $l=$l+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!------------ 8th-------------->
                  <?php }elseif($k==8){ ?>
                  <?php $l=1; ?>
                  <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                    <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                    <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                    
                    <?php if($l==1){ $classrd='category_wrapper1'; ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($l==2){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==4){  ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($l==6){ ?>
                          <div class="row_3of5 rows5row"> <?php } ?>
                            <?php if($l==8){ $classrd='category_wrapper9'; ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');">
                              <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                                <div class="category_title">
                                  <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                                </div>
                              </div>
                              </a> <?php if($l==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($l==3){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==5){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==7){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?>
                      <?php if($l==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php $l=$l+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!---------- 9th ------------------->
                  <?php }elseif($k>=9){ ?>
                  <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                    <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($i==1) { $k=9; }else{ $k=$i;} 
                    if($i<=9){
                    ?>
                    
                    
                    <?php if($i==1){ ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($i==2){ ?>
                      <div class="row_2of5 rows5row"> <?php } ?> 
                        <?php if($i==4){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($i==7){ ?>
                          <div class="row_4of5 rows5row"> <?php } ?> 
                            <?php if($i==9){ ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getbeautyservice('<?php echo e($getallcats->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></span></span> </span> </a> <?php if($i==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($i==3){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==6){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==8){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==9){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    
                    <?php $i=$i+1; } ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <?php } ?> </div>
                <?php $z=$z+1; }?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
            </div>
            <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
            <?php  $PP=1; $jk=0; ?>
            <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productservice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php 
            $TotalPage = $productservice->serviceslist->count(); 
            if($TotalPage > 9)
            {
            $paginations = ceil($TotalPage/9);
            }
            else
            {
            $paginations =0;
            }
            if(count($productservice->serviceslist) < 1) {continue; }
            ?>
            
            <?php if($paginations!=0): ?>
            <div class="pagnation-area" id="tab<?php echo e($PP); ?>" style="display: none;">
              <ul class="pagination">
                <li class="disabled"><span>«</span></li>
                <?php for($KO=1;$KO<=$paginations;$KO++): ?>
                <li class="<?php if($KO==1): ?> active <?php endif; ?> paginate" data-page="<?php echo e($KO); ?>" data-attribute_id="<?php echo e($productservice->id); ?>"  data-branchid="<?php echo e($branchid); ?>" data-tblfield="<?php echo e($tbl_field); ?>" data-tabid="<?php echo e($PP); ?>"><span><?php echo e($KO); ?></span></li>
                <?php endfor; ?>
                <li><a href="#?page=<?php echo e($paginations); ?>" rel="next">»</a></li>
              </ul>
            </div>
            <?php endif; ?>
            
            <?php  $PP=$PP+1; $jk++; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
          <!-- service-display-right -->
          <?php echo Form::open(['url' => 'beautycosmeticshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

          <!-- container-section -->
          <div class="service-display-left"> <span id="selectedproduct">
            <div class="service-left-display-img product_gallery">
   <?php    $pro_id = $beautyshopleftproduct->pro_id; ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            

            </div>
            <div class="service-product-name"><?php echo e($beautyshopleftproduct->pro_title); ?></div>
            <div class="service-beauty-description"><?php echo e($beautyshopleftproduct->pro_desc); ?></div>
            <div class="beauty-prise"> <?php if($beautyshopleftproduct->pro_discount_percentage>0){ $productprice= $beautyshopleftproduct->pro_price*(100-$beautyshopleftproduct->pro_discount_percentage)/100;}else{ $productprice='';}
              if($productprice==''){ ?> <span> <?php echo e(currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)); ?> </span> <?php }else{ ?> <span class="strike"><?php echo e(currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)); ?> </span><span> <?php echo e(currency($productprice, 'SAR',$Current_Currency)); ?> </span> <?php  } ?> </div>
            </span>
            <div class="service-radio-line" id="qtyop">
              <div class="service_quantity_box" id="service_quantity_box">
                <div class="service_qunt"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?></div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity" data-title="Total Quality">
                    <div class="quantity">
                      <button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
                      <input type="number" name="itemqty" id="qty" value="1" min="1" max="9" readonly onkeyup="isNumberKey(event); pricecalculation('pricewithqty');" onkeydown="isNumberKey(event); pricecalculation('pricewithqty');" />
                      <button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                  </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
            </div>
            <span id="addtocartprice" >
            <div class="container_total_price foods-totals"> <?php if($productprice!=''){ $tprice=$productprice; }else{ $tprice=$beautyshopleftproduct->pro_price; } ?>
              <?php if(Lang::has(Session::get('lang_file').'.Total_Price')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.Total_Price')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.Total_Price')); ?><?php endif; ?>: <span class="cont_final_price" id="cont_final_price"><?php echo e(currency($tprice, 'SAR',$Current_Currency)); ?> </span></div>
            </span> <?php if($productprice>0){ $totalprice=$productprice;}else{$totalprice=$beautyshopleftproduct->pro_price;} ?>
			
            <div class="btn_row">
              <input type="hidden" name="category_id" value="<?php echo e($subsecondcategoryid); ?>">
              <input type="hidden" name="subsecondcategoryid" value="<?php echo e($category_id); ?>">
              <input type="hidden" name="cart_sub_type" value="makeup">
              <input type="hidden" name="branch_id" value="<?php echo e($branchid); ?>">
              <input type="hidden" name="shop_id" value="<?php echo e($shop_id); ?>">
              <input type="hidden" name="vendor_id" value="<?php echo e($beautyshopleftproduct->pro_mr_id); ?>">
              <input type="hidden" name="product_id" id="product_id" value="<?php echo e($beautyshopleftproduct->pro_id); ?>">
              <input type="hidden" name="pro_qty" id="pro_qty" value="<?php echo e($beautyshopleftproduct->pro_qty); ?>">
              
              <input type="hidden" name="product_price" id="product_price" value="<?php echo e(currency(number_format($totalprice,2), 'SAR',$Current_Currency, $format = false)); ?>">
              <input type="hidden" name="product_orginal_price" id="product_orginal_price" value="<?php echo e(currency($totalprice, 'SAR',$Current_Currency, $format = false)); ?>">
              <input type="submit" name="submit" id="sbtn" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn">
            </div>
			<div class="terms_conditions"><a href="<?php echo e($fooddateshopdetails[0]->terms_conditions); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')); ?><a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a> </div>
          </div>
          <?php } ?>
          <!-- container-section -->
        </div>
        <!-- service-display-left -->
      </div>
      <?php echo Form::close(); ?> </div>
    <!-- service-display-left -->
  </div>
  <!--service-display-section-->
  <!--service-display-section-->
  <?php
  $sesionData = session()->all();
  if(isset($sesionData) && $sesionData!='')
  {
  $url = url('search-results?weddingoccasiontype='.$sesionData['searchdata']['weddingoccasiontype'].'&noofattendees='.$sesionData['searchdata']['noofattendees'].'&budget='.$sesionData['searchdata']['budget'].'&cityid='.$sesionData['searchdata']['cityid'].'&occasiondate='.$sesionData['searchdata']['occasiondate'].'&gender='.$sesionData['searchdata']['gender'].'&basecategoryid='.$sesionData['searchdata']['mainselectedvalue'].'&maincategoryid='.$sesionData['searchdata']['maincategoryid'].'&mainselectedvalue='.$sesionData['searchdata']['mainselectedvalue']); 
  }        
  ?>
  <div class="sticky_other_service"> <a href="<?php echo e($url); ?>">
    <div class="sticky_serivce"><?php if(Lang::has(Session::get('lang_file').'.OTHER_SERVICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OTHER_SERVICE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OTHER_SERVICE')); ?> <?php endif; ?></div>
    <div class="sticku_service_logo"><img src="<?php echo e(url('')); ?>/themes/images/logo.png"></div>
    </a> </div>
  <!-- other_serviceinc -->
  <!-- other_serviceinc -->
</div>
<!-- detail_page -->
</div>
</div>
<!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script language="javascript">
  function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }

$('.add').click(function () {

  var proqty=document.getElementById('pro_qty').value;
  var currentvalue=parseInt($(this).prev().val());
		if (currentvalue < proqty) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}else{
       //jQuery('#maxqty').css("display", "block");
       $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();

    }
});
$('.sub').click(function () {
   //jQuery('#maxqty').css("display", "none");
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});

</script>
<script type="text/javascript">
	 function pricecalculation(act){
	 		
							
							var no=1;
							
							var currentquantity=document.getElementById('qty').value;
							var unititemprice=document.getElementById('product_orginal_price').value;
							var totalproqty=document.getElementById('pro_qty').value;

							if(currentquantity<1){
								document.getElementById('qty').value=1;
								 var qty= parseInt(no);
							}else{
							if(act=='pricewithqty'){
								var qty=parseInt(currentquantity)
							}
							if(act=='add'){
									 var qty= parseInt(currentquantity)+parseInt(no);
								 }
							if(act=='remove'){ 
									if(parseInt(currentquantity)==1){
								      var qty=parseInt(currentquantity)
								   }else{
								      var qty=parseInt(currentquantity)-parseInt(no);
									}

							 }
							}
              if(totalproqty<qty){
                var orderedqty=qty-1;
              }else{
                 var orderedqty=qty;
              }
							var producttotal=orderedqty*unititemprice;
							//alert(producttotal);
							jQuery('[name=product_price]').val(producttotal);
							var fprice=producttotal.toFixed(2);

							 <?php $Cur = Session::get('currency'); ?>
							document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+fprice;
							
							
					
	 }

</script>

<!------------------ tabs----------------->
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
</script>
<script src="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery-clockpicker.min.js"></script>
<link href="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery-clockpicker.min.css" rel="stylesheet" />
<!------------ end tabs------------------>
<script type="text/javascript">
function checkgallery(str)
{
  jQuery.ajax({
     type:"GET",
     url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
     success:function(res)
     { 
      jQuery('.product_gallery').html(res);
     }
   });
 
}

	function getbeautyservice(selecteddateproduct,branchid,tbl,Ks){
		if(Ks!=undefined)
			{
			jQuery('.pagnation-area').hide()
			jQuery('#tab'+Ks).show()
			}
		   var Add_to_Cart = "<?php if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Add_to_Cart')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Add_to_Cart')); ?> <?php endif; ?>";

       var SOLDOUT = "<?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>";
      jQuery('#errorqty').html('');
		var dateproductid=selecteddateproduct;

			  if(selecteddateproduct){
        jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('beautycosmeticshop/getcartproduct')); ?>?product_id="+dateproductid+"&branchid="+branchid+"&tblfld="+tbl,
           success:function(res){               
            if(res){
            	 var json = JSON.stringify(res);
			           var obj = JSON.parse(json);
			//alert(obj);
             <?php $Cur = Session::get('currency'); ?>
            	
            		length=obj.productsericeinfo.length;
            		//alert(length);
            		
            		if(length>0){
		 for(i=0; i<length; i++)
			{
				if(obj.productprice==obj.productsericeinfo[i].pro_price){
					TotalPrices = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					var productrealprice='<span><?php echo $Cur; ?> '+TotalPrices+'</span>';
					var productrealpriceamount= parseFloat(TotalPrices).toFixed(2);
          TotalPricesss = getChangedPrice(obj.TotalPrices); 
				}else{
					TotalPrices = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					TotalPricesss = getChangedPrice(obj.productprice); 
					var productrealprice='<span class="strike"><?php echo $Cur; ?> '+TotalPrices+' </span><span> <?php echo $Cur; ?> '+TotalPricesss+'</span>';
					var productrealpriceamount=parseFloat(TotalPricesss).toFixed(2);
				}
        var qty=parseInt(obj.productsericeinfo[i].pro_qty);

         if(qty<1){
          $("#sbtn").val(SOLDOUT);
          $('#qtyop').css('display','none');
          $(':input[type="submit"]').prop('disabled', true);
         }else{
          $("#sbtn").val(Add_to_Cart);
          $('#qtyop').css('display','block');
          $(':input[type="submit"]').prop('disabled', false);
         }

				jQuery('#selectedproduct').html('<div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productsericeinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productsericeinfo[i].pro_desc+'</div><div class="beauty-prise">'+productrealprice+'</div><span id="productextraimages"></span></div>');
        checkgallery(selecteddateproduct);
				jQuery('[name=product_id]').val(obj.productsericeinfo[i].pro_id);
				jQuery('[name=itemqty]').val(1);
        document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur; ?> '+productrealpriceamount;
        jQuery('[name=pro_qty]').val(qty);
				jQuery('[name=product_price]').val(productrealpriceamount);
				jQuery('[name=product_orginal_price]').val(productrealpriceamount);
				jQuery('#totalprice').html(parseFloat(TotalPricesss).toFixed(2));

				
			}
			
			}

			jQuery('html, body').animate({
       scrollTop: (jQuery('.service-display-left').offset().top)
    }, 'slow');



           }



           }
        });
    }


		
	}

</script>
<script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">

  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

</script>
<script type="text/javascript">
	
jQuery('.paginate').click(function(){
var page = jQuery(this).data('page');
var attribute_id = jQuery(this).data('attribute_id');
var branchid = jQuery(this).data('branchid');
 
if(page!='')
{

    jQuery.ajax({
           type:"GET",
           url:"<?php echo e(url('newbeautycosmaticshop')); ?>?page="+page+"&attribute_id="+attribute_id+"&branchid="+branchid,
           success:function(res){               
            if(res){
				jQuery('.diamond_main_wrapper').html(res);

			  }
			}
			});
            	

}


})


<?php if(isset($HH) && $HH!=''){ ?>
jQuery(window).load(function(){ 
  
  getbeautyservice('<?php echo $HH;?>','<?php echo $branchid; ?>','service_id','1');
})

<?php } ?>
</script>


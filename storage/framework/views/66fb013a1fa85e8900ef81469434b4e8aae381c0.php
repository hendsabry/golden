<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $beautybig_leftmenu =1; ?>

<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
    
      <header>
        <?php if(request()->catitemid==''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_CATEGORY')); ?> <?php endif; ?> </h5>
          <?php else: ?>
 <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPDATE_CATEGORY')); ?> <?php endif; ?> </h5>
          <?php endif; ?>
          <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
           <?php $hid = request()->hid; $id = request()->id; $itemid = request()->itemid; $catitemid = request()->catitemid; ?>

              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->  

                <form name="form1"  id ="menucategory" method="post" action="<?php echo e(route('beauty-updatecategory')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <div class="form_row">
                    <div class="form_row_left">
                     <label class="form_label">
                      <span class="english"><?php echo lang::get('mer_en_lang.MER_CATEGORY_NAME'); ?></span>
                      <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); ?> </span>

              </label>  
                    <div class="info100">
                      <div class="english">
                      <input type="text" value="<?php echo e(isset($getAttr->attribute_title) ? $getAttr->attribute_title : ''); ?>" class="english" name="category_name" maxlength="90" id="category_name" required="" >
                      </div>
                      <div class="arabic ar">
                      <input class="arabic ar" id="category_ar" maxlength="90"  name="category_name_ar"  value="<?php echo e(isset($getAttr->attribute_title_ar) ? $getAttr->attribute_title_ar : ''); ?>"  required=""  type="text" >
                      </div>
                       </div>
                       </div>
                  </div>
                  
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CREATED_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CREATED_IMAGE'); ?> </span> </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="catimage" id="company_logo1" class="info-file">
                    </div>
                      <?php if(isset($getAttr->image) && $getAttr->image !=''): ?> <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span> <?php endif; ?>
                    <?php if(isset($getAttr->image) && $getAttr->image !=''): ?>
                    <div class="form-upload-img"> <img src="<?php echo e($getAttr->image); ?>"> </div>
                     <input type="hidden" value="<?php echo e($getAttr->image); ?>" name="updatecatimage">
                      <?php endif; ?> 
                     </div>


                  <div class="form_row">
      
                  <input type="hidden" name="id" value="<?php echo e(isset($id) ? $id : ''); ?>">
                  <input type="hidden" name="hid" value="<?php echo e(isset($hid) ? $hid : ''); ?>">
                   <input type="hidden" name="itemid" value="<?php echo e(isset($itemid) ? $itemid : ''); ?>">
                   <input type="hidden" name="catitemid" value="<?php echo e(isset($catitemid) ? $catitemid : ''); ?>">
                   
                   <div class="form_row">
                   <div class="form_row_left">
                   <?php if($catitemid==''): ?>
                   <div class="arabic ar">
                <input type="submit" name="submit" value="خضع">
         </div>
          <div class="english">
                <input type="submit" name="submit" value="Submit">
         </div> <!-- form_row -->
                  <?php else: ?> 
                   <div class="arabic ar">
        
                <input type="submit" name="submit" value="تحديث">
         </div>
          <div class="english">
                <input type="submit" name="submit" value="Update">
         </div> <!-- form_row -->
                  <?php endif; ?>
                  </div>
                  </div>
                  </div>
                  
                </form>


              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },

                      <?php if(isset($getAttr->image) && $getAttr->image !=''): ?>
                      catimage: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      },
                      <?php else: ?> 
                       catimage: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>
                     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
           category_name: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CATEGORY_NAME')); ?> <?php endif; ?>",
                      }, 
           category_name_ar: {
               required:  "<?php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME_VALIDATION_AR'); ?>",
                      },
              catimage: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      }, 

                },
                invalidHandler: function(e, validation){
 
                    var valdata=validation.errorList;
                     console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


                    <?php if($mer_selected_lang_code !='en'): ?>
                      if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }

                      if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                      {
                      $('.arabic_tab').trigger('click'); 
                      }

                      if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                    <?php else: ?>  
                      if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                       if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }


                      if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      } 
                    <?php endif; ?>

                    },
 

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
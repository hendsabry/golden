<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
<div class="inner_wrap">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</div> <!-- outer_wrapper -->
</div>
 <div class="header-page-divder">&nbsp;</div>
<div class="inner_wrap">
  <div class="inner_wrap">
 
 <?php (Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ? $sub= trans(Session::get('lang_file').'.SUBMIT'): $sub=trans($OUR_LANGUAGE.'.SUBMIT');

  ?>
  
 <a name="loginfrml"></a>
     <div class="forgot-password-area">  
	 <div class="form_title"><?php echo e((Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD')); ?></div>
   <div class="forgot-password-box">
   
   <?php echo Form::open(['url' => 'member_forgot_pwd_email/update', 'method' => 'post', 'name'=>'loginfrm', 'id'=>'loginfrm', 'enctype' => 'multipart/form-data']); ?>

    <div class="usersign-form">
    	<?php if(session('loginstatus')): ?>
                        <div class="alert error">
                            <?php echo e(session('loginstatus')); ?>

                        </div>
                    <?php endif; ?>
  <div class="usersign-form-line">
  <div class="usersign-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.New_Password')!= '')  ?  trans(Session::get('lang_file').'.New_Password'): trans($OUR_LANGUAGE.'.New_Password')); ?></div>
  <div class="usersign-form-bottom"><input required="" id="password" class="t-box" maxlength="80" name="password" type="password"></div>
  </div>

  <div class="usersign-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Confirm_Password')!= '')  ?  trans(Session::get('lang_file').'.Confirm_Password'): trans($OUR_LANGUAGE.'.Confirm_Password')); ?></div>
  <div class="usersign-form-bottom"><input required="" id="cpassword" class="t-box" maxlength="80" name="cpassword" type="password">
    <input type="hidden" name="userid" id="userid" value="<?php echo e($email_address); ?>">
  </div>
  </div>
  
  <div class="usersign-form-button"><?php echo Form::submit($sub, array('class'=>'form-btn')); ?></div>
  </div>
  <?php echo Form::close(); ?>

   </div>
  
  </div> <!-- forgot-password-area -->
  
  
 

 



  






</div>  <!-- usersign-area -->






</div>





<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 <script type="text/javascript">
 
jQuery("#loginfrm").validate({
                  ignore: [],
                  rules: {         

                       password: {
                       required: true,
             minlength: 8
                      },
                      cpassword: {
                        required: true,
                        minlength: 8,
                        equalTo: "#password"
                    }
                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
             
          password: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($OUR_LANGUAGE.'.USER_PASSWORD'); ?>",
                      },

              cpassword: {
               required: "<?php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($OUR_LANGUAGE.'.USER_PASSWORD'); ?>",
                      },

                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 


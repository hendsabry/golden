<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
?>

<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap ">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($vendordetails->mc_img); ?>" alt="logo" style="max-width: 200px;" /></a></div>
      </div>

      <!-- vendor_header_left -->
      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap ">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
<?php if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !=''): ?>
          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>
      <?php endif; ?>    
       <li><a href="#kosha"><?php echo e((Lang::has(Session::get('lang_file').'.CHOSSE_FLOWER')!= '')  ?  trans(Session::get('lang_file').'.CHOSSE_FLOWER'): trans($OUR_LANGUAGE.'.CHOSSE_FLOWER')); ?></a></li>
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
          <?php } ?>
   
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>
 
  <!-- common_navbar -->
  <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?>
                <li><img src="<?php echo e(str_replace('thumb_','',$vendordetails->image)); ?>" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></div>
          <div class="detail_hall_description"><?php echo e($vendordetails->address); ?></div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
          <div class="detail_about_hall">
            <div class="comment more"><?php if($lang != 'en_lang'): ?> <?php echo e($vendordetails->mc_discription); ?> <?php else: ?> <?php echo e($vendordetails->mc_discription); ?> <?php endif; ?></div>
          </div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

         <?php if($vendordetails->google_map_address!=''): ?> 
 <?php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
   <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
 <?php endif; ?>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>

      

  <?php if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !=''): ?>

        <div class="service-video-area">
         <?php if($vendordetails->mc_video_description!=''): ?> <div class="service-video-cont">
          <?php if($lang != 'en_lang'): ?>  <?php echo e($vendordetails->mc_video_description_ar); ?> <?php else: ?>  <?php echo e($vendordetails->mc_video_description); ?> <?php endif; ?></div><?php endif; ?>
          <div class="service-video-box">
        <?php if($vendordetails->mc_video_url !=''): ?>    <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> <?php endif; ?>
          </div>
        </div>
<?php endif; ?>

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
                   <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
     



  <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                
                <li><a href="<?php echo e(route('roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>" <?php if(request()->type!=2): ?> class="select" <?php endif; ?>><?php echo e((Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')); ?> </a></li>
               
                <li><a  <?php if(request()->type==2): ?> class="select" <?php endif; ?> href="<?php echo e(route('single-roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Single')!= '')  ?  trans(Session::get('lang_file').'.Single'): trans($OUR_LANGUAGE.'.Single')); ?> </a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
 
 <!-- SINGLE -->
<div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
        
 
 <?php  $i=1; $j=1;  $getC = $product->count();     ?>

<?php if($getC >= 1): ?>



<div class="diamond_main_wrapper">
    <div class="diamond_wrapper_outer">
    <div class="diamond_wrapper_main">
      <div class="diamond_wrapper_inner">

<?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 
 
<!-- TILL 5 RECORD -->
  <?php if($getC <=5): ?> 
        <div class="row_<?php echo e($i); ?>of<?php echo e($getC); ?> rows<?php echo e($getC); ?>row">   
        <a href="#">      
            <div data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper <?php if($getC!=5 && $getC!=4  && $getC!=2): ?> category_wrapper<?php echo e($i); ?> <?php endif; ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
              <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div><div class="clear"></div></div>
            </div> 
            </a>         
        </div>
<!-- TILL 6 RECORD -->
  <?php elseif($getC == 6): ?>
 
          <?php if($i != 3 && $i != 4): ?> 
          <?php if($i==5){ $M=4; } elseif($i==6){ $M=5; }else { $M=$i; } ?>
          <div class="row_<?php echo e($M); ?>of5 rows5row">
          <a href="#">
          <div class="category_wrapper" data-pid="<?php echo e($val->pro_id); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="#">
          <span data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper  <?php if($i==3): ?> category_wrapper2 <?php else: ?> category_wrapper3 <?php endif; ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==4): ?>  <div class="clear"></div>   
         </div>   <?php endif; ?>  
  <?php endif; ?>
<!-- TILL 7 RECORD -->
  <?php elseif($getC == 7): ?>

          <?php if($i != 3 && $i != 4 && $i != 5): ?> 
          <?php if($i==6){ $j = 4;} if($i==7){ $j = 5;}  ?>
          <div class="row_<?php echo e($j); ?>of5 rows5row">
          <a href="#">
          <div data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="#">
          <span  data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper  <?php if($i==3): ?> category_wrapper4 <?php elseif($i==4): ?> category_wrapper5 <?php else: ?> category_wrapper6 <?php endif; ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==5): ?>  <div class="clear"></div>   
          </div>   <?php endif; ?>  
  <?php endif; ?>
 <!-- TILL 8 RECORD -->
 <?php elseif($getC == 8): ?>
 
        <?php if($i==1 || $i==8 ): ?>
        <?php if($i==1) { $k = 1;} if($i==8) { $k = 5;}   ?>
       
          <div class="row_<?php echo e($k); ?>of5 rows5row">
          <a href="#">
          <div data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper category_wrapper1" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
          </div>
          </a>   
          </div>
          <?php else: ?>
          <?php if($i==2 ||$i==4 ||$i==6): ?>   <div class="row_3of5 rows5row"> <?php endif; ?>
          <?php if($i==2) { $P = 2;} if($i==3) { $P = 3;}  if($i==4) { $P = 2;}  if($i==5) { $P = 3;}  if($i==6) { $P = 7;}  if($i==7) { $P = 8;}  ?>

          <a href="#">
          <span data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper category_wrapper<?php echo e($P); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
          </span>
          </a>
          <?php if($i==3 ||$i==5 ||$i==7): ?>  
          <div class="clear"></div>
          </div><?php endif; ?>
 <?php endif; ?>
  <!-- TILL 9 RECORD -->
<?php elseif($getC == 9): ?>

        <?php if($i==1 || $i==9 ): ?>
        <?php if($i==1){$k=1; } if($i==9){$k=5;   } ?>
        <div class="row_<?php echo e($k); ?>of5 rows5row">
        <a href="#">
        <div data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
        </div>
        </a>   
        </div>
        <?php elseif($i==2 || $i==3 ): ?>
 
        <?php if($i==2): ?> <div class="row_2of5 rows5row"> <?php endif; ?> 
        <a href="#">
        <span data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==3): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==4 || $i==5 || $i==6 ): ?>


        <?php if($i==4): ?> <div class="row_3of5 rows5row"> <?php endif; ?> 
        <a href="#">
        <span data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==6): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==7 || $i==8 ): ?>

        <?php if($i==7): ?> <div class="row_4of5 rows5row"><?php endif; ?> 
        <a href="#">
        <span data-pid="<?php echo e($val->pro_id); ?>" class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
        </span>
        </a>
       
        <?php if($i==8): ?> <div class="clear"></div>
        </div> <?php endif; ?>            
        <?php endif; ?>

<?php endif; ?>

   <!-- END ALL LOOP -->


<?php $i= $i+1; $j=$j+1; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    </div>
  </div>
 
    
 <div class="diamond_shadow"><img src="<?php echo e(url('/images/shadow.png')); ?>" alt=""></div> 
      <div class="pagnation-area"><?php echo e($product->links()); ?></span></div>   

        </div>
        <!-- service-display-right -->
  

<?php 
  $getproID = $product[0]->pro_id;  ?>
   
 <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img class="proimg" src="<?php echo e($product[0]->pro_Img); ?>" alt="" /></div>
          <div class="service-product-name marB0"><span id="pname"><?php echo e($product[0]->pro_title); ?></span></div>
          <div class="service-beauty-description"><?php echo e($product[0]->pro_desc); ?></div>
      <div class="packge_numb"><?php if(Lang::has(Session::get('lang_file').'.Number_of_Flowers')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Number_of_Flowers')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Number_of_Flowers')); ?> <?php endif; ?>  <span id="noflwrer"><?php echo e(count($product[0]->product_packege)); ?></span></div>

<?php



            if($product[0]->pro_qty>0)
            {
                   if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') 
                   {
                           $cartbtn=trans(Session::get('lang_file').'.Add_to_Cart');  } else{  $cartbtn=trans($OUR_LANGUAGE.'.Add_to_Cart'); 
                    }
                      $btnst='';
                      $avcart='';
                    $avcart='block';
              }
               else
              {

                if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''){ $cartbtn=trans(Session::get('lang_file').'.SOLD_OUT'); }else{ $cartbtn=trans($OUR_LANGUAGE.'.SOLD_OUT');}
                $btnst='disabled="disabled"';
                $avcart='none';
              }

$OriginalP = $product[0]->pro_price; 
$OriginalDis = $product[0]->pro_discount_percentage;
$Disp = $OriginalP - ($OriginalP*$OriginalDis)/100;
$Disp = number_format((float)$Disp, 2, '.', ''); 
?> 

          <div class="beauty-prise">
            <?php if($OriginalDis!=''){ ?>
            <span class="strike" id="oprice"><?php echo e(currency($product[0]->pro_price,'SAR',$Current_Currency)); ?>    </span> &nbsp;&nbsp;
            <?php } ?>

             <span id="disprice"><?php echo e(currency($Disp,'SAR',$Current_Currency)); ?>     </span></div>

         
      

             <div class="thumbnail_wrap">
 
 <span id="updateinfo5">
<?php if(count($product[0]->product_packege) >=1): ?> 
<?php  ?>
<div class="leftbar_title"> <?php if(Lang::has(Session::get('lang_file').'.Flower_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Flower_Type')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Flower_Type')); ?> <?php endif; ?></div>
<div class="choose_staf_box choose_roses">
<?php for($i=0;$i< count($product[0]->product_packege);$i++): ?>



<div class="choose_name">
<div class="choose_img"><img src="<?php echo e($product[0]->product_packege[$i]->getProductPackage->pro_Img); ?>" alt="" /></div>
<div class="choose_text"><?php echo e($product[0]->product_packege[$i]->getProductPackage->pro_title); ?>

</div>
<div class="select_price"><?php echo e(currency($product[0]->product_packege[$i]->getProductPackage->pro_price,'SAR',$Current_Currency)); ?>   </div>
</div>

                    
  <?php endfor; ?>  
  </div>     
   <?php endif; ?> 
 </span>

 </div>
 
 
 
 <span id="updateinfo">
<?php if(count($product[0]->product_option) >=1): ?> 
<?php for($i=0;$i<=1;$i++): ?>
<div class="thumbnail_wrap">
<div class="leftbar_title"><?php echo e($product[0]->product_option[$i]->option_title); ?></div>
<div class="choose_staf_box choose_roses">
<div class="choose_name">
<div class="choose_img"><img src="<?php echo e($product[0]->product_option[$i]->product_option_value[0]->image); ?>" alt="" /></div>
<div class="choose_text"><?php echo e($product[0]->product_option[$i]->product_option_value[0]->option_title); ?>

</div>
<div class="select_price"><?php echo e(currency($product[0]->product_option[$i]->product_option_value[0]->price,'SAR',$Current_Currency)); ?>   </div>
</div>
</div>
    </div>                 
  <?php endfor; ?>       
   <?php endif; ?> 
 </span>



        
     <form name="form1" method="post" action="<?php echo e(route('roses-package-add-to-cart')); ?>"" enctype="multipart/form-data">
      <?php echo e(csrf_field()); ?>


        <div class="service-radio-line recp_spc" id="cartqty" style="display: <?php echo e($avcart); ?>">
              <div class="service_quantity_box" id="service_quantity_box" >
                <div class="service_qunt"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?></div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity recp" data-title="Total Quality">
                    <div class="quantity">
                      <button type="button" id="sub" class="sub" onClick="return checkquantity('remove','<?php echo e($product[0]->pro_id); ?>');"></button>
                      <input type="number" name="itemqty" id="qty" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantity('add','<?php echo e($product[0]->pro_id); ?>');" onkeydown="isNumberKey(event); checkquantity('add','<?php echo e($product[0]->pro_id); ?>');" />
                      <button type="button" id="add" class="add" onClick="return checkquantity('add','<?php echo e($product[0]->pro_id); ?>');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                    </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
            </div>

            <div class="total_food_cost">
            <div class="total_price"><?php if(Lang::has(Session::get('lang_file').'.Total_Price')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Total_Price')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Total_Price')); ?> <?php endif; ?> : <span id="totalp"><?php echo e(currency($Disp,'SAR',$Current_Currency)); ?>  </span></div>
          
        </div>
           
            <input type="hidden" name="netamountforqty_<?php echo e($product[0]->pro_id); ?>" id="netamountforqty_<?php echo e($product[0]->pro_id); ?>" value="<?php echo e(currency($Disp,'SAR',$Current_Currency, $format = false)); ?>">

      <input type="hidden" name="product_id" id="total_product_id" value="<?php echo e($product[0]->pro_id); ?>">
      <input type="hidden" name="cart_type" value="occasion">
      <input type="hidden" name="total_price" id="total_total_price" value="<?php echo e(currency($Disp, 'SAR',$Current_Currency, $format = false)); ?>">
      <input type="hidden" name="language_type" value="en">
      <input type="hidden" name="product_qty" value="1">
      <input type="hidden" name="product_option_value[]">
      <input type="hidden" name="attribute_id" value="<?php echo e(request()->id); ?>">
      <input type="hidden" name="cart_sub_type" value="roses">
 

        <div class="btn_row" id="cartbtn"> <input type="submit" value="<?php echo e($cartbtn); ?>" id="sbtn" class="form-btn addto_cartbtn" <?php echo e($btnst); ?>></div>
           <span id="sold" class="form-btn addto_cartbtn" style="display: none;"><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></span>
      </form>
                <a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a>   
        </div>
       
<?php else: ?>
 
<?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?>

<?php endif; ?>
 </div>

 <!-- Single end -->

 <!--service-display-section-->
     <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <script type="text/javascript">  
 $('.selct').click(function()
 {
  var title =  $(this).data('title');
  var from =  $(this).data('from');
  var price =  $(this).data('price');  
  var img =   $(this).data('img');
  var pid =   $(this).data('id');
  var attribute =   $(this).data('attribute');
  $(".kosha-selections-area").show();  
  $(".info_"+from).html('');
  $(".info_"+from).append('<div class="selection-box"><div class="selection-img"><img src="'+img+'" alt="" /></div><div class="selection-name">'+title+'<input type="hidden" class="tprice" name="itemprice" value="'+price+'"><input type="hidden" name="product[attribute_id][]" value="'+attribute+'"><input type="hidden" name="product[pro_id][]" value="'+pid+'"></div><div class="selection-prise">SAR '+price+'</div></div>');
    var totalP = 0;
    var getP = 0;
    $( ".tprice" ).each(function() {
    getP =  $(this).val();
    totalP = parseFloat(totalP) + parseFloat(getP);
    }); 

   totalP = parseFloat(totalP).toFixed(2)

$("#totalPrice").html('SAR '+ totalP); 
  });

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
 
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
function pricecalculation(act)
{
  var no=1;
  var product_id      = document.getElementById('total_product_id').value;  
  var currentquantity = document.getElementById('qty').value;
  var unititemprice   = document.getElementById('pricecalculation').value;
  if(act=='add')
  {
    var qty = parseInt(currentquantity)+parseInt(no);     
  }
  else
  { 
    if(parseInt(currentquantity)==1)
    {
      var qty=parseInt(currentquantity)
    }
    else
    {
      var qty=parseInt(currentquantity)-parseInt(no);
    }
  }
  
  if(product_size)
  {
   $.ajax({
     type:"GET",
     url:"<?php echo e(url('getSizeQuantity')); ?>?product_id="+product_id+'&product_size='+product_size+'&qty='+qty,
     success:function(res)
     {               
    
     if(res!='ok')
     {
      //alert(res);
       alert('Not available quantity');
       var qtyupdated=parseInt(currentquantity);      
       document.getElementById('qty').value=qtyupdated;
     }
     else
     {
        //alert(res);
        //alert('er');
      var producttotal = qty*unititemprice;
      document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
      document.getElementById('itemqty').value=qty;
     }
     }
   });
  }
  else
  {
    var producttotal = qty*unititemprice;
    document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
    document.getElementById('itemqty').value=qty;
  }
      
}


$('.category_wrapper').click(function(){
var Pro_id = $(this).data('pid');
if(Pro_id !='')
{

$.ajax({
     type:"GET",
     url:"<?php echo e(url('getProductInfo')); ?>?product_id="+Pro_id,
     success:function(res)
          {             
            var Add_to_Cart = "<?php if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Add_to_Cart')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Add_to_Cart')); ?> <?php endif; ?>";
            var Info = res.split("~~~");
            var ProName = Info[0];
            var proCount = Info[1];
            var OriginalP = Info[2];
            var Disp = Info[3];
            var Wrappingtype = Info[4];
            var Wrappingdesign = Info[5];
            var pro_Img = Info[6];
            var avqty = Info[15]; 
            var Type_OptionTitle = Info[7];
            var Type_image = Info[8];
            var Type_price = Info[9];
            var Design_OptionTitle = Info[10];
            var Design_image = Info[11];
            var Design_price = Info[12];
            var pro_id  = Info[13];
            $('#pname').html(ProName);
            $('#noflwrer').html(proCount);
<?php $Cur = Session::get('currency'); ?>
            document.getElementById('qty').value = 1; 
            $('#oprice').html('<?php echo $Cur; ?> '+OriginalP);
            $('#disprice').html('<?php echo $Cur; ?> '+Disp);
            $('#totalp').html('<?php echo $Cur; ?> '+Disp);
            $('.proimg').attr('src',pro_Img);
 

            $('#total_product_id').val(pro_id);
            $('#total_total_price').val(Disp);


              if(avqty<1){
              $('#cartbtn').css('display','none');
              $('#cartqty').css('display','none');
              $('#sold').css('display','block');
            }else{
              $('#cartbtn').css('display','block');
              $('#cartqty').css('display','block');
              $('#sold').css('display','none');
              $("#sbtn").val(Add_to_Cart);
               $(':input[type="submit"]').prop('disabled', false);
            }



  $('#updateinfo').html('<div class="leftbar_title">'+Wrappingtype+'</div><div class="choose_staf_box choose_roses"><div class="choose_name"><div class="choose_img"><img src="'+Type_image+'" alt="" /></div><div class="choose_text">'+Type_OptionTitle+'</div><div class="select_price"><?php echo $Cur; ?> '+Type_price+'</div></div></div>');

  $('#updateinfo').append('<div class="leftbar_title">'+Wrappingdesign+'</div><div class="choose_staf_box choose_roses"><div class="choose_name"><div class="choose_img"><img src="'+Design_image+'" alt="" /></div><div class="choose_text">'+Design_OptionTitle+'</div><div class="select_price"><?php echo $Cur; ?> '+Design_price+'</div></div></div>');
 


          }
  });


}

});



function showProductDetail(str,vendorId)
{
    
     $.ajax({
     type:"GET",
     url:"<?php echo e(url('getShoppingProductInfo')); ?>?product_id="+str+'&vendor_id='+vendorId,
     success:function(res)
     {               
    if(res)
    { 
      $('html, body').animate({
        scrollTop: ($('.service-display-left').first().offset().top)
      },500);
      var json = JSON.stringify(res);
      var obj = JSON.parse(json);
          console.log(obj);
      length=obj.productdateshopinfo.length;
      //alert(length);
      document.getElementById('qty').value=1;
      if(length>0)
      {
           for(i=0; i<length; i++)
         {         
              
              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+obj.productdateshopinfo[i].pro_price+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productdateshopinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');          
               $('#cont_final_price').html('SAR '+obj.productdateshopinfo[i].pro_price); 

            

         }
      }
      if($.trim(obj.productattrsize) !=1)
      {
        $('#ptattrsize').html(obj.productattrsize);
      $('#ptattrsizeenone').css('display','block');
      }
      else
      {
        $('#ptattrsizeenone').css('display','none');
      }   
      }
     }
   });
}
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script>
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {       
          "product_size" : {
            required : true
          },    
         },
         messages: {
          "product_size": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php endif; ?>'
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
</script>
 
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 <?php if(request()->type!=''): ?> 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 <?php endif; ?>


 <script type="text/javascript">
   function checkquantity(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty').value;
         var insuranceamount  = 0;

         var netamountforqty  = document.getElementById('total_total_price').value;
        
          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }
//alert(qty);
   $.ajax({
       type:"GET",
       url:"<?php echo e(url('checkcoshaquantity')); ?>?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty').value = qtyupdated - 1;
        
       }
       else
       {
        
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         //alert(totalcalcamount);
         
         jQuery('#totalp').html('<span><?php echo $Cur; ?></span> <span>'+totalcalcamount.toFixed(2)+'</span>');

         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }


    

 </script>
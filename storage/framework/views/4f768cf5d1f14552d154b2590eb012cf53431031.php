<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $spabig_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      </header>
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
  <?php echo Form::open(array('url' => '/addspabranch','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'addbranch', 'name' =>'addbeautybranch' )); ?>



              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); ?> </span> </label>
                  <div class="info100"> <?php $getShopName = Helper::getshopname(request()->hid); ?>
                    <div class="english"> <?php echo e(isset($getShopName->mc_name) ? $getShopName->mc_name : ''); ?> </div>
                     <div class="arabic ar"> <?php echo e(isset($getShopName->mc_name_ar) ? $getShopName->mc_name_ar : ''); ?> </div>
                  </div>
                </div>
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                        <input type="text" id="branchname" class="english" name="branchname" maxlength="60" value="<?php echo e(isset($getbranchinfo->mc_name) ? $getbranchinfo->mc_name : ''); ?>">
                      </div>
                      <div class="arabic ar">
                        <input type="text" name="branchname_ar" maxlength="60" class="arabic ar" id="ssb_name_ar" value="<?php echo e(isset($getbranchinfo->mc_name_ar) ? $getbranchinfo->mc_name_ar : ''); ?>">
                      </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); ?> </span> 
                  <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span></a>
                  </label>
                  <div class="info100">
                    <input type="url" name="google_map_address"   value="<?php echo e(isset($getbranchinfo->google_map_address) ? $getbranchinfo->google_map_address : ''); ?>" >
                  </div>
                </div>
              </div>


   <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($getbranchinfo->longitude) ? $getbranchinfo->longitude : ''); ?>" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($getbranchinfo->latitude) ? $getbranchinfo->latitude : ''); ?>"  readonly="" name="latitude" />
          </div>
                  </div>


              
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value1"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="mc_imgname" value="<?php echo e(isset($getbranchinfo->mc_img) ? $getbranchinfo->mc_img : ''); ?>">
                        <input type="file" <?php if(!isset($getbranchinfo->mc_img)): ?> required <?php endif; ?> class="info-file" name="mc_img" id="company_logo">
                         <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                        <?php if(isset($getbranchinfo->mc_img) && $getbranchinfo->mc_img!=''): ?>
                        <div class="form-upload-img"><img src="<?php echo e(isset($getbranchinfo->mc_img) ? $getbranchinfo->mc_img : ''); ?>"></div>
                        <?php endif; ?>
                      </div>
                    </div> </div>
                <!--<div class="form_row_right">
                  <label class="form_label posrel"> 
				  <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); ?></span> 
				  <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); ?> </span> 
				  <a href="javascript:void(0);" class="address_image_tooltip">
  					<span class="add_img_tooltip"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_address_img')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_address_img')); ?> <?php endif; ?></span>
				   </a>
</label>
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value2"></div>
                    </div>
                    </label>
                    <input type="file" name="hall_addressimg" id="company_logo1" class="info-file">
                  </div>
                   <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                    <?php if(isset($getbranchinfo->address_image) && $getbranchinfo->address_image !=''): ?>
                  <input type="hidden" name="mc_addimgname" value="<?php echo e($getbranchinfo->address_image); ?>">
                 <div class="form-upload-img"> <img src="<?php echo e(isset($getbranchinfo->address_image) ? $getbranchinfo->address_image : ''); ?>">  </div> <?php endif; ?></div>-->
                  
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.BACK_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.BACK_ADDRESS'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="address" maxlength="55"  value="<?php echo e(isset($getbranchinfo->address) ? $getbranchinfo->address : ''); ?>"  >
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" value="<?php echo e(isset($getbranchinfo->address_ar) ? $getbranchinfo->address_ar : ''); ?>"  name="address_ar"  maxlength="55" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CITY'); ?> </span> </label>
                  <div class="info100" >
                
                    <div class="">
                     <?php $getCitys = Helper::getcitylist();  ?>
                      <select id="city_id" name="city_id" class="small-sel">
                       

                         <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?></option>
               <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"><?php if($mer_selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?> <?php echo e($cbval->co_name); ?> <?php endif; ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $ci_id = $val->ci_id; ?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php if(isset($getbranchinfo->city_id) && ($getbranchinfo->city_id == $val->ci_id)): ?>) SELECTED <?php endif; ?>><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="form_row common_field">
                <div class="form_row_left ">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_MANAGER'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_MANAGER'); ?> </span> </label>
                  <div class="info100" >
                    <div class=" ">
                      <?php $getManagers = Helper::getmanagerlist();  ?>

                      <select id="manager" name="manager" class="small-sel">
                        <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?></option>
                        <?php $__currentLoopData = $getManagers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gms): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($gms->mer_id); ?>" <?php if(isset($getbranchinfo->branch_manager_id) && ($getbranchinfo->branch_manager_id == $gms->mer_id)): ?> SELECTED <?php endif; ?> ><?php echo e(isset($gms->mer_fname) ? $gms->mer_fname : ''); ?> <?php echo e(isset($gms->mer_lname) ? $gms->mer_lname : ''); ?> </option>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </div>
                   
                  </div>
                </div>
                <div class="form_row_right">
        <div class="form_opening">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.OPENING'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.OPENING'); ?> </span> </label>
                  <div class="info100" >
                    <div class="englishs">
                      <div class="small_time "> 
                      <select name="opening_time" id="opening_time" >  
                        <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?></option> 
                        <option value="12:00 AM"<?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='12:00 AM' ? 'selected' : ''); ?>> 12:00 AM</option>
                        <option value="00:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='00:30 AM' ? 'selected' : ''); ?>> 00.30 AM</option>
                        <option value="1.00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='1.00 AM' ? 'selected' : ''); ?>> 1:00 AM</option>
                        <option value="1:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='1:30 AM' ? 'selected' : ''); ?>> 1:30 AM</option>
                        <option value="2:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='2:00 AM' ? 'selected' : ''); ?>> 2:00 AM</option>
                        <option value="2:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='2:30 AM' ? 'selected' : ''); ?>> 2:30 AM</option>
                        <option value="3:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='3:00 AM' ? 'selected' : ''); ?>> 3:00 AM</option>
                        <option value="3.30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='3.30 AM' ? 'selected' : ''); ?>> 3:30 AM</option>
                        <option value="4.00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='4.00 AM' ? 'selected' : ''); ?>> 4:00 AM</option>
                        <option value="4:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='4:30 AM' ? 'selected' : ''); ?>> 4:30 AM</option>
                        <option value="5:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='5:00 AM' ? 'selected' : ''); ?>> 5:00 AM</option>
                        <option value="5:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='5:30 AM' ? 'selected' : ''); ?>>5:30 AM</option>
                        <option value="6:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='6:00 AM' ? 'selected' : ''); ?>> 6:00 AM</option>
                        <option value="6:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='6:30 AM' ? 'selected' : ''); ?>> 6:30 AM</option>
                        <option value="7:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='7:00 AM' ? 'selected' : ''); ?>> 7:00 AM</option>
                        <option value="7:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='7:30 AM' ? 'selected' : ''); ?> > 7:30 AM</option>
                        <option value="8:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='8:00 AM' ? 'selected' : ''); ?>> 8:00 AM</option>
                        <option value="8:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='8:30 AM' ? 'selected' : ''); ?>> 8:30 AM</option>
                        <option value="9:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='9:00 AM' ? 'selected' : ''); ?>> 9:00 AM</option>
                        <option value="9:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='9:30 AM' ? 'selected' : ''); ?>> 9:30 AM</option>
                        <option value="10:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='10:00 AM' ? 'selected' : ''); ?>> 10:00 AM</option>
                        <option value="10:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='10:30 AM' ? 'selected' : ''); ?>> 10:30 AM</option>
                        <option value="11:00 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='11:00 AM AM' ? 'selected' : ''); ?>> 11:00 AM</option>
                        <option value="11:30 AM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='11:30 AM' ? 'selected' : ''); ?>> 11:30 AM</option>
                        <option value="12:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='12:00 PM' ? 'selected' : ''); ?>> 12:00 PM</option>
                        <option value="12:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='12:30 PM' ? 'selected' : ''); ?>> 12.30 PM</option>
                        <option value="1.00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='1.00 PM' ? 'selected' : ''); ?>> 1:00 PM</option>
                        <option value="1:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='1:30 PM' ? 'selected' : ''); ?>> 1:30 PM</option>
                        <option value="2:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='2:00 PM' ? 'selected' : ''); ?>> 2:00 PM</option>
                        <option value="2:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='2:30 PM' ? 'selected' : ''); ?>> 2:30 PM</option>
                        <option value="3:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='3:00 PM' ? 'selected' : ''); ?>> 3:00 PM</option>
                        <option value="3.30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='3.30 PM' ? 'selected' : ''); ?>> 3:30 PM</option>
                        <option value="4.00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='4.00 PM' ? 'selected' : ''); ?>> 4:00 PM</option>
                        <option value="4:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='4:30 PM' ? 'selected' : ''); ?>> 4:30 PM</option>
                        <option value="5:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='5:00 PM' ? 'selected' : ''); ?>> 5:00 PM</option>
                        <option value="5:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='5:30 PM' ? 'selected' : ''); ?>>5:30 PM</option>
                        <option value="6:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='6:00 PM' ? 'selected' : ''); ?> > 6:00 PM</option>
                        <option value="6:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='6:30 PM' ? 'selected' : ''); ?>> 6:30 PM</option>
                        <option value="7:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='7:00 PM' ? 'selected' : ''); ?>> 7:00 PM</option>
                        <option value="7:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='7:30 PM' ? 'selected' : ''); ?>> 7:30 PM</option>
                        <option value="8:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='8:00 PM' ? 'selected' : ''); ?>> 8:00 PM</option>
                        <option value="8:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='8:30 PM' ? 'selected' : ''); ?>> 8:30 PM</option>
                        <option value="9:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='9:00 PM' ? 'selected' : ''); ?>> 9:00 PM</option>
                        <option value="9:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='9:30 PM' ? 'selected' : ''); ?>> 9:30 PM</option>
                        <option value="10:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='10:00 PM' ? 'selected' : ''); ?>> 10:00 PM</option>
                        <option value="10:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='10:30 PM' ? 'selected' : ''); ?>> 10:30 PM</option>
                        <option value="11:00 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='11:00 PM' ? 'selected' : ''); ?>> 11:00 PM</option>
                        <option value="11:30 PM" <?php echo e(isset($getbranchinfo->opening_time) && $getbranchinfo->opening_time =='11:30 PM' ? 'selected' : ''); ?>> 11:30 PM</option>

</select>



                      </div>
                      
                    </div>
                   
                  </div>
          </div>
            <div class="form_opening">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.ClOSING'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.ClOSING'); ?> </span> </label>
                  <div class="info100" >
                    <div class="englishs">
                      <div class="small_time ">
                        
                        <select name="closing_time" id="closing_time" >
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?></option> 
                        <option value="12:00 AM"<?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='12:00 AM' ? 'selected' : ''); ?>> 12:00 AM</option>
                        <option value="00:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='00:30 AM' ? 'selected' : ''); ?>> 00.30 AM</option>
                        <option value="1.00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='1.00 AM' ? 'selected' : ''); ?>> 1:00 AM</option>
                        <option value="1:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='1:30 AM' ? 'selected' : ''); ?>> 1:30 AM</option>
                        <option value="2:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='2:00 AM' ? 'selected' : ''); ?>> 2:00 AM</option>
                        <option value="2:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='2:30 AM' ? 'selected' : ''); ?>> 2:30 AM</option>
                        <option value="3:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='3:00 AM' ? 'selected' : ''); ?>> 3:00 AM</option>
                        <option value="3.30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='3.30 AM' ? 'selected' : ''); ?>> 3:30 AM</option>
                        <option value="4.00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='4.00 AM' ? 'selected' : ''); ?>> 4:00 AM</option>
                        <option value="4:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='4:30 AM' ? 'selected' : ''); ?>> 4:30 AM</option>
                        <option value="5:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='5:00 AM' ? 'selected' : ''); ?>> 5:00 AM</option>
                        <option value="5:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='5:30 AM' ? 'selected' : ''); ?>>5:30 AM</option>
                        <option value="6:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='6:00 AM' ? 'selected' : ''); ?>> 6:00 AM</option>
                        <option value="6:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='6:30 AM' ? 'selected' : ''); ?>> 6:30 AM</option>
                        <option value="7:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='7:00 AM' ? 'selected' : ''); ?>> 7:00 AM</option>
                        <option value="7:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='7:30 AM' ? 'selected' : ''); ?> > 7:30 AM</option>
                        <option value="8:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='8:00 AM' ? 'selected' : ''); ?>> 8:00 AM</option>
                        <option value="8:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='8:30 AM' ? 'selected' : ''); ?>> 8:30 AM</option>
                        <option value="9:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='9:00 AM' ? 'selected' : ''); ?>> 9:00 AM</option>
                        <option value="9:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='9:30 AM' ? 'selected' : ''); ?>> 9:30 AM</option>
                        <option value="10:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='10:00 AM' ? 'selected' : ''); ?>> 10:00 AM</option>
                        <option value="10:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='10:30 AM' ? 'selected' : ''); ?>> 10:30 AM</option>
                        <option value="11:00 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='11:00 AM AM' ? 'selected' : ''); ?>> 11:00 AM</option>
                        <option value="11:30 AM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='11:30 AM' ? 'selected' : ''); ?>> 11:30 AM</option>
                        <option value="12:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='12:00 PM' ? 'selected' : ''); ?>> 12:00 PM</option>
                        <option value="12:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='12:30 PM' ? 'selected' : ''); ?>> 12.30 PM</option>
                        <option value="1.00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='1.00 PM' ? 'selected' : ''); ?>> 1:00 PM</option>
                        <option value="1:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='1:30 PM' ? 'selected' : ''); ?>> 1:30 PM</option>
                        <option value="2:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='2:00 PM' ? 'selected' : ''); ?>> 2:00 PM</option>
                        <option value="2:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='2:30 PM' ? 'selected' : ''); ?>> 2:30 PM</option>
                        <option value="3:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='3:00 PM' ? 'selected' : ''); ?>> 3:00 PM</option>
                        <option value="3.30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='3.30 PM' ? 'selected' : ''); ?>> 3:30 PM</option>
                        <option value="4.00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='4.00 PM' ? 'selected' : ''); ?>> 4:00 PM</option>
                        <option value="4:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='4:30 PM' ? 'selected' : ''); ?>> 4:30 PM</option>
                        <option value="5:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='5:00 PM' ? 'selected' : ''); ?>> 5:00 PM</option>
                        <option value="5:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='5:30 PM' ? 'selected' : ''); ?>>5:30 PM</option>
                        <option value="6:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='6:00 PM' ? 'selected' : ''); ?> > 6:00 PM</option>
                        <option value="6:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='6:30 PM' ? 'selected' : ''); ?>> 6:30 PM</option>
                        <option value="7:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='7:00 PM' ? 'selected' : ''); ?>> 7:00 PM</option>
                        <option value="7:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='7:30 PM' ? 'selected' : ''); ?>> 7:30 PM</option>
                        <option value="8:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='8:00 PM' ? 'selected' : ''); ?>> 8:00 PM</option>
                        <option value="8:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='8:30 PM' ? 'selected' : ''); ?>> 8:30 PM</option>
                        <option value="9:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='9:00 PM' ? 'selected' : ''); ?>> 9:00 PM</option>
                        <option value="9:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='9:30 PM' ? 'selected' : ''); ?>> 9:30 PM</option>
                        <option value="10:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='10:00 PM' ? 'selected' : ''); ?>> 10:00 PM</option>
                        <option value="10:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='10:30 PM' ? 'selected' : ''); ?>> 10:30 PM</option>
                        <option value="11:00 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='11:00 PM' ? 'selected' : ''); ?>> 11:00 PM</option>
                        <option value="11:30 PM" <?php echo e(isset($getbranchinfo->closing_time) && $getbranchinfo->closing_time =='11:30 PM' ? 'selected' : ''); ?>> 11:30 PM</option>
                      </select>
                      </div>
                      
                    </div>
                   
                  </div>
          </div>
                </div>
              </div>
              
              <div class="form_row common_field">

 
        
        <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.SERVICEAVAIL'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.SERVICEAVAIL'); ?> </span> </label>
                  <div class="info100">
                    <div class="save-card-line">
                      <input <?php echo e(isset($getbranchinfo->service_availability) && ($getbranchinfo->service_availability =='1' or $getbranchinfo->service_availability =='3')  ? 'checked' : ''); ?> type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="service_available[]" value="1" id="type">
                      <label for="type"> <span class="english"><?php echo lang::get('mer_en_lang.HOME'); ?></span> <span class="arabic ar"><?php echo lang::get('mer_ar_lang.HOME'); ?> </span> </label>
                    </div>
                    <div class="save-card-line">
                      <input type="checkbox" <?php echo e(isset($getbranchinfo->service_availability) && ($getbranchinfo->service_availability =='2' or $getbranchinfo->service_availability =='3')  ? 'checked' : ''); ?> data-validation-qty="min1" data-validation="checkbox_group" name="service_available[]" value="2" id="dish">
                      <label for="dish"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP'); ?></span> <span class="arabic ar"><?php echo lang::get('mer_ar_lang.MER_SHOP'); ?> </span> </label>
                    </div>
                      <span for="service_available[]" generated="true" class="error">  </span>
                  </div>
                </div>
                
                 <div class="homevisit">
                <div class="form_row_right">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.HOMEVISIT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.HOMEVISIT'); ?> </span> </label>
                  <div class="info100" >
                    <div class="englissh">
                      <input type="text" class="engslish small-sel" name="home_visit_charge" maxlength="8"  data-validation="length required" value="<?php echo e(isset($getbranchinfo->home_visit_charge) ? $getbranchinfo->home_visit_charge : ''); ?>" data-validation-length="max35">
                    </div>
                  </div>
                </div>
                </div>

              
              </div>
              
              <div class="form_row">
               
                <div class="form_row_left ">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>
                  <div class="info100" >
                   <div class="english">
                        <textarea class="english" cols="50" rows="4" id="description" name="description" maxlength="500"><?php echo e(isset($getbranchinfo->mc_discription) ? $getbranchinfo->mc_discription : ''); ?></textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea cols="50" rows="4" id="description Arabic" name="description_ar" class="arabic ar"  maxlength="500"><?php echo e(isset($getbranchinfo->mc_discription_ar) ? $getbranchinfo->mc_discription_ar : ''); ?></textarea>
                      </div>
                  </div>
                </div>

                 <div class="form_row_right english">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo9">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value8"></div>
                    </div>
                    </label>
                    <input type="file" name="tnc" id="company_logo9" class="info-file">
                   <input type="hidden" name="tnc_img" value="<?php echo e(isset($getbranchinfo->terms_conditions) ? $getbranchinfo->terms_conditions : ''); ?>">
                   <input type="hidden" name="tncname" value="<?php echo e(isset($getbranchinfo->terms_condition_name) ? $getbranchinfo->terms_condition_name : ''); ?>">
                   <div class="pdf_msg"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </div>
                      <?php if(isset($getbranchinfo->terms_conditions) && $getbranchinfo->terms_conditions!=''): ?>
                      
                      <a href="<?php echo e(isset($getbranchinfo->terms_conditions) ? $getbranchinfo->terms_conditions : ''); ?>" target="_blank" class="pdf_icon"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($getbranchinfo->terms_condition_name) ? $getbranchinfo->terms_condition_name : ''); ?></span></a>
                        <?php endif; ?>


                  </div>
                </div>

              </div>
              
              <div class="arabic ar">
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo8">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value7"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc_ar" id="company_logo8" class="info-file">
                  <input type="hidden" name="tnc_img_ar" value="<?php echo e(isset($getbranchinfo->terms_conditions_ar) ? $getbranchinfo->terms_conditions_ar : ''); ?>">
                   <input type="hidden" name="tncname_ar" value="<?php echo e(isset($getbranchinfo->terms_condition_name_ar) ? $getbranchinfo->terms_condition_name_ar : ''); ?>">
                   <div class="pdf_msg"> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?></div>
                      <?php if(isset($getbranchinfo->terms_conditions_ar) && $getbranchinfo->terms_conditions_ar!=''): ?>
                      
                      <a href="<?php echo e(isset($getbranchinfo->terms_conditions_ar) ? $getbranchinfo->terms_conditions_ar : ''); ?>" target="_blank" class="pdf_icon"><span><?php echo e(isset($getbranchinfo->terms_condition_name_ar) ? $getbranchinfo->terms_condition_name_ar : ''); ?></span> <img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> </a>
                        <?php endif; ?>


                  </div>
                </div>
              </div>
              </div>
              
              <div class="form-btn-section english">
              <div class="form_row_left">
                        <input type="hidden" name="hid" value="<?php echo e(request()->hid); ?>">
                    <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                    <input type="hidden" name="itemid" value="<?php echo e(request()->itemid); ?>">   
                <input type="submit" id="beautysubmit" name="beautysubmit" value="Submit">
                </div>
              </div>
              <div class="form-btn-section arabic ar">
              <div class="form_row_left">
                <input type="submit" id="beautysubmit" name="beautysubmit" value="خضع">
              </div>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>


<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  'branchname': {
                       required: true,
                      },

                      'branchname_ar': {
                       required: true,
                      },
                 
                      'address': {
                       required: true,
                      },
                      'address_ar': {
                       required: true,
                      },
                      'city_id': {
                       required: true,
                      },
                      /*'manager': {
                       required: true,
                      },*/
                       'opening_time': {
                       required: true,
                      },
                       'closing_time': {
                       required: true,
                      },
                    "service_available[]": {
                       required: true,
                      },
                       
                       'description': {
                       required: true,
                      },
                      'description_ar': {
                       required: true,
                      },

                    <?php if(isset($getbranchinfo->terms_conditions) && $getbranchinfo->terms_conditions!=''): ?>
                    'tnc': {
                    required: false,
                    accept:"pdf",
                    },
                    'mc_img': {
                    required: false,
                    accept:"png|jpe?g|gif",
                    },
                  

                    <?php else: ?>
                    'tnc': {
                    required: true,
                    accept:"pdf",
                    },
                    'mc_img': {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },
                     <?php endif; ?>

                    <?php if(isset($getbranchinfo->terms_conditions_ar)!=''): ?> 
                        mc_tnc_ar: {
                           required:false,
                           accept:"pdf",
                      },
                    <?php else: ?>
                         mc_tnc_ar: {
                           required:true,
                           accept:"pdf",
                      },
                      <?php endif; ?>

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
              
           messages: { 

                    branchname: {
                   required: "<?php echo lang::get('mer_en_lang.PLEASE_ENTER_BRANCH_NAME'); ?>",
                    },  
                    branchname_ar: {
                     required: "<?php echo lang::get('mer_ar_lang.PLEASE_ENTER_BRANCH_NAME'); ?>",
                    },  
                    address: {
                    required: "<?php echo lang::get('mer_en_lang.PLEASE_ENTER_ADDRESS'); ?>",
                    },
                    address_ar: {
                      required: "<?php echo lang::get('mer_ar_lang.PLEASE_ENTER_ADDRESS'); ?>",
                    }, 
                    city_id: {
                      required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_CITY')); ?> <?php endif; ?> ",
                    }, 
                    /*manager: {
                    required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_MANAGER')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_MANAGER')); ?> <?php endif; ?> ",
                    },*/
              "service_available[]": {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SERVICE_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SERVICE_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR')); ?> <?php endif; ?>",
                      },  
                    opening_time: {
                     required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_OPENING_TIME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_OPENING_TIME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_OPENING_TIME')); ?> <?php endif; ?> ",
                    },
                    closing_time: {
                    required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CLOSING_TIME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CLOSING_TIME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_CLOSING_TIME')); ?> <?php endif; ?> ",
                    },

                    description: {
                    required: "<?php echo lang::get('mer_en_lang.PLEASE_ENTER_DESCRIPTION'); ?>",
                    },

                    description_ar: {
                    required: "<?php echo lang::get('mer_ar_lang.PLEASE_ENTER_DESCRIPTION'); ?>",
                    },

                    mc_img: {
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    },    

                    tnc: {
                                required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                                accept: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                        },

                       mc_tnc_ar: {
                                required: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?>",
                                accept: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?>",
                      },                   
         
                     
                },
                      
                invalidHandler: function(e, validation){ 

              
                    var valdata=validation.invalid;

                  <?php if($mer_selected_lang_code !='en'): ?>

                    if (typeof valdata.branchname != "undefined" || valdata.branchname != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                    $('.english_tab').trigger('click');                         
                    }
                    if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                     if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.opening_time != "undefined" || valdata.opening_time != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.closing_time != "undefined" || valdata.closing_time != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.branchname_ar != "undefined" || valdata.branchname_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');
                    }

                    if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }

          <?php else: ?>
                  
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.branchname_ar != "undefined" || valdata.branchname_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');
                    }

                    if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }

                    if (typeof valdata.branchname != "undefined" || valdata.branchname != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                    $('.english_tab').trigger('click');                         
                    }
                    if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                     if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    if (typeof valdata.opening_time != "undefined" || valdata.opening_time != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    if (typeof valdata.closing_time != "undefined" || valdata.closing_time != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {
                    $('.english_tab').trigger('click');     
                    }
                    
                <?php endif; ?>



                    },


                submitHandler: function(form) {
                   var mapAdd = jQuery('input[name=google_map_address]').val();
                              if(mapAdd !='')
                              {
                              var long = jQuery('input[name=longitude]').val();
                              var lat  =  jQuery('input[name=latitude]').val();
                              if(long =='' && lat=='')
                              {
                              var allOk = 0;

                              }
                              else
                              {
                              var allOk = 1; 
                              }
                              }
                              else
                              {
                              var allOk = 1;
                              }

                              if(allOk == 1)
                              {
                              form.submit();
                              }
                              else
                              {
                              $('#maperror').html("<span class='error'><?php if(Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.get_Lat_Long_issue')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue')); ?> <?php endif; ?></span>");
                              }
                }
            });
 
    
   $(document).ready(function(){
      
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
      if(checkedvalue==1) {  
      $('.homevisit').show();
    } else {
        $('.homevisit').hide();
    }

     $('#type').click(function(){
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
     if(checkedvalue==1) {
       $('.homevisit').show();
       } else {
           $('.homevisit').hide();

       }
     })

     $('#dish').click(function(){
       if(checkedvalue==1  || $('input#type').is(':checked')) {
        $('.homevisit').show();
      } else {
          $('.homevisit').hide();
      }
    
     })

    })
</script>




<!-- merchant_vendor --> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
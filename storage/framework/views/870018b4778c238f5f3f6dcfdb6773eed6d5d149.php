<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="UTF-8" />
  <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="<?php echo csrf_token(); ?>"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" />
<!--<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/theme.css" />-->
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
<?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
<?php endif; ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<?php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
<!--END GLOBAL STYLES -->
<script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

  <!-- MAIN WRAPPER -->
  <div id="wrap" >
    <!-- HEADER SECTION -->
    <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- END HEADER SECTION -->
    <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!--PAGE CONTENT -->
    <div class=" col-sm-9 col-xs-12 right-side-bar">
      <div class="right_col" role="main">
        <?php if(Session::has('message')): ?>
        <div class="alert alert-danger no-border">
          <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
          <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo $error; ?></li>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php endif; ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content padduser-block">
              <div class="top_back_btn">
                  <div class="title-block">
                        <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_VENDOR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EDIT_VENDOR') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_VENDOR')); ?></h3>
                        </div>
                        <div class="back-btn-area">
                       <a href="/admin/vendor_management" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a>
                      </div>
                      
                    </div>
              <form action="/admin/vendor_management/edit/<?php echo e($users->mer_id); ?>" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left form-section" enctype="multipart/form-data" method="post">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="form-group">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_FIRST_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FIRST_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FIRST_NAME')); ?> <span class="required">*</span>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <input type="text" id="lead_name" name="mer_fname" required class="form-control col-md-7 col-xs-12" maxlength="20" value="<?php echo e($users->mer_fname); ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_LAST_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LAST_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_NAME')); ?> <span class="required">*</span>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <input type="text" id="lead_name" name="mer_lname" required class="form-control col-md-7 col-xs-12" maxlength="20" value="<?php echo e($users->mer_lname); ?>">
                </div>
              </div>    

              <div class="form-group">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL')); ?> <span class="required">*</span>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <input type="email" id="client_email" name="mer_email" required class="form-control col-md-7 col-xs-12" maxlength="50" value="<?php echo e($users->mer_email); ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_PHONE')); ?><span class="required">*</span>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="row row-area">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        <select name="country_code" class="selectpicker form-control" data-live-search="true" required="required">
                        <option  value=""><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY_CODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY_CODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY_CODE')); ?></option>
                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($country->country_code); ?>" <?php if ($country->country_code == $users->country_code): ?> selected
                          <?php endif ?>><?php echo e($country->country_code); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <input type="text" id="client_ph" name="mer_phone" required class="form-control col-md-7 col-xs-12" maxlength="15" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo e($users->mer_phone); ?>"> 
                  </div>
                  </div>
                </div>
              </div> 
              <div class="form-group">
                  <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SALES_CODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_CODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_CODE')); ?>

                  </label>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <input type="text" id="client_ph" name="sales_rep_code" class="form-control col-md-7 col-xs-12" maxlength="20" value="<?php echo e($users->sales_rep_code); ?>"> 
                  </div>
                </div>
              <div class="form-group gender">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_GENDER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GENDER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GENDER')); ?><span class="required">*</span></label>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">
                  <input type="radio" class="flat" name="gender" id="active_1" value="0" required <?php if($users->gender == 0): ?> checked <?php endif; ?> />
                  <label for="active_1" class="gender-name">
                    <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_MALE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MALE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_MALE')); ?> 
                  </label>
                </div> 
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">                  
                  <input type="radio" class="flat" name="gender" id="inactive_1" value="1" <?php if($users->gender == 1): ?> checked <?php endif; ?> />
                  <label for="inactive_1" class="gender-name">
                  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_FEMALE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FEMALE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FEMALE')); ?></label>
                </div>
              </div> 
              <!-- <div class="form-group">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AGE')); ?><span class="required">*</span>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <input type="text" id="target" name="age" required="required" class="form-control col-md-7 col-xs-12" maxlength="3" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo e($users->age); ?>"> 
                </div>
              </div>   -->                    

              <div class="form-group">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_BIRTH')); ?><span class="required">*</span>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <input type="text" id="demo" name="dob" required class="form-control col-md-7 col-xs-12" value="<?php echo(date('d-m-Y', strtotime($users->dob)));?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?><span class="required"></span>
                </label>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <textarea class="form-control" rows="3"
                  name="mer_address1" id="my_form" placeholder='<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS')); ?>' maxlength="200"><?php echo e($users->mer_address1); ?></textarea>
                </div>
              </div> 
              <div class="form-group gender">
                <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?></label>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">
                  <input type="radio" class="flat" name="mer_staus" id="active" value="1" <?php if($users->mer_staus == 1): ?> checked <?php endif; ?> required />       <label for="active" class="gender-name">
                  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE')); ?> </label>
                </div> 
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 marTop">                 
                  <input type="radio" class="flat" name="mer_staus" id="inactive" value="0" <?php if($users->mer_staus == 0): ?> checked <?php endif; ?>  /> <label for="inactive" class="gender-name">
                  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE')); ?>  </label>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 col-md-offset-4">
                  <a class="btn btn-primary" href="/admin/vendor_management" type="button"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL')); ?></a>
                  <button class="btn btn-primary" type="reset"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
                  <button type="submit" class="btn btn-success"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>

  </div>
  <!--END MAIN WRAPPER -->

  <!-- FOOTER -->
  <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!--END FOOTER -->
  <!-- GLOBAL SCRIPTS -->

  <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <!-- END GLOBAL SCRIPTS -->
  <!-- PAGE LEVEL SCRIPTS -->
  <!-- END PAGE LEVEL SCRIPTS -->
  <script type="text/javascript">
    $.ajaxSetup({
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
  </script>

  <script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script> 
  <script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript">
    $(window).on('load',function(){
      $('#demo').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        endDate: '+0d'   
      });
    });
  </script>

  <script type="text/javascript">
    $(document).on('click', '.day', function () {
      $(".datepicker").css("display", "none");
    });

    $(document).on('click', '#demo', function () {
      $(".datepicker").css("display", "block");
    });
  </script>
</body>

<!-- END BODY -->
</html>
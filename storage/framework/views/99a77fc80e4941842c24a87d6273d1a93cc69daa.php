<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $reception_hospitality_leftmenu =1; ?>
<div class="merchant_vendor cont_add">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_WORKERS_NATIONALTY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_WORKERS_NATIONALTY')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_WORKERS_NATIONALTY')); ?> <?php endif; ?></h5>
      
      </header>
      <style type="text/css">
       .ms-drop ul li label input[type="checkbox"] {
    height: 20px;
    display: inline-block;
    width: 20px;
    position: relative;
    border: 1px solid red;
    opacity: 1;
}


      </style>
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/themes/css/multiple-select.css" />
      <!-- service_listingrow --> 
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?> 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">  

              <div class="box notab">
              
                  <form  id="nationality" name="nationality" method="post" action="<?php echo e(route('storenationality')); ?>" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                     
                    <div class="form_row">
					 <div class="form_row_left">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_NATIONALITY')); ?> <?php endif; ?> </label>
                    <div class="info100">
                      
                      <select name="workers_nationality[]" multiple="" id="workers_nationality" class="english">
                           <?php $co_name='co_name'?>
                           <?php if($mer_selected_lang_code !='en'): ?>
                           <?php $co_name= 'co_name_'.$mer_selected_lang_code; ?>
                           <?php endif; ?>  

                           <?php $__currentLoopData = $countrylist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php

                           $checked = in_array($val->co_id, $natid) ? 'selected' : '';
                          $getWP = Helper::getworkerprice($val->co_id);
                           ?>


                             <option value="<?php echo e($val->$co_name); ?>~<?php echo e($getWP); ?>" <?php echo e($checked); ?>> <?php echo e($val->$co_name); ?></option>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      </select>
                      <?php if($errors->has('workers_nationality')): ?>
                       <span class="help-block text-red error">
                       <?php echo e($errors->first('workers_nationality')); ?>

                       </span>
                   <?php endif; ?>
                      <input type="hidden" name="shopid" value="<?php echo e(request()->shopid); ?>">
                    </div>
					</div>
                    </div>

                  <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.WorkerPrice')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.WorkerPrice')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.WorkerPrice')); ?> <?php endif; ?> </label>
                  <div class="info100">

             <!--  <input type="text" name="worker_price" maxlength="6" required="" value="<?php echo e(isset($getWP->value) ? $getWP->value : ''); ?>" class="xs_small notzero"> -->

 <div id="result" > <?php $i=1; ?> <?php $__currentLoopData = $countrydata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

<?php if($mer_selected_lang_code =='en'): ?>
 <div class="result-prise-line" >Price for <?php echo e(isset($vals->country_name) ? $vals->country_name : ''); ?><br/>
  <?php else: ?>
 <div class="result-prise-line" >السعر ل  <?php echo e(isset($vals->country_name_ar) ? $vals->country_name_ar : ''); ?><br/>
  <?php endif; ?>
<input type="text" name="worker_price<?php echo e($i); ?>" maxlength="6" required="" value="<?php echo e(isset($vals->worker_price) ? $vals->worker_price : ''); ?>" class="xs_small notzero">  <?php $i=$i + 1; ?>  
 </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </div>





                  </div>
                  </div>
                  </div>
                                    
                    <div class="form_row">
					 <div class="form_row_left">
                      <input type="submit" name="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SUBMIT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SUBMIT')); ?> <?php endif; ?>">
                   </div>
				    </div>
                    
                  </form>
             
              </div>

          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor -->

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_popup_title"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Action')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Action')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Action')); ?> <?php endif; ?></div>
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a> <a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_De_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record')); ?> <?php endif; ?>')
 } else {

     jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Activate_Record')); ?> <?php endif; ?>')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('change-status')); ?>",
        data: {activestatus:activestatus,id:id,from:'hotellist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
            $('form[name=test]').submit();
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });
    

    </script> 
     
 <script type="text/javascript">
  
$("#nationality").validate({
                  ignore: [],
                  rules: {
                  worker_price: {
                       required: true,
                      },                        

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
           

                 worker_price: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_WORKER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_WORKER_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_WORKER_PRICE')); ?> <?php endif; ?>",
                      },  
               
                },

                invalidHandler: function(e, validation){                 
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            }); 

</script> 
  
    <script src="<?php echo e(url('')); ?>/themes/js/jquery.min.js"></script>
<script src="<?php echo e(url('')); ?>/themes/js/multiple-select.js"></script>
    <?php if($mer_selected_lang_code !='en'): ?>
<script>
    $(function() {
        $('#workers_nationality,#ms_2,#ms_3,#ms_4,#ms_5,#ms_6,#ms_7,#ms_8,#ms_9,#ms_10').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });

          

    });
</script>
<?php else: ?>

<script>
    $(function() {
        $('#workers_nationality').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });

          

    });
</script>

<?php endif; ?>
 
 
<script type="text/javascript">
  


$(function() {
    $("#workers_nationality").change(function() {
        var multipleValues = $("#workers_nationality").val() || "";
    

        var result = "";
        if (multipleValues != "") {
            var aVal = multipleValues.toString().split(",");

            $.each(aVal, function(i, value) {

              var addd = value.split("~");
 
                
                <?php if($mer_selected_lang_code =='en'): ?>   
                 result += "<div class='clear'>Price for "+addd[0];  
                result += "<input required placeholder='Enter price for "+addd[0]+"' value='"+addd[1]+"' class='notzero' maxlength='6' type='text' name='worker_price" + (parseInt(i) + 1) + "' >";    
                <?php else: ?>
                 result += "<div class='clear'>السعر ل  "+addd[0]; 
            result += "<input required placeholder='أدخل السعر ل  "+addd[0]+"' value='"+addd[1]+"' class='notzero' maxlength='6' type='text' name='worker_price" + (parseInt(i) + 1) + "' >";  
                <?php endif; ?>        
                result += "</div>";
            });


        }
        //Set Result
        $("#result").html(result);

    });
});


</script>

 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
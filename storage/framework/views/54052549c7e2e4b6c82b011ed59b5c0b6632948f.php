<?php $lang = Session::get('lang_file'); ?>
<div class="citypop action_popup">
  <div class="action_active_popup">
    <div class="action_content"><?php echo e((Lang::has(Session::get('lang_file').'.Please_select_city')!= '')  ?  trans(Session::get('lang_file').'.Please_select_city'): trans($OUR_LANGUAGE.'.Please_select_city')); ?> </div>
 
    <div class="action_btnrow" align="center"> 
      <span id="hidemsgab">
 

<a class="action_yes status_no" href="javascript:void(0);"> <?php if(Lang::has($lang.'.OK')!= ''): ?> <?php echo e(trans($lang.'.OK')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.OK')); ?> <?php endif; ?> </a> </span>
 
	 
	  </div>
  </div>
</div>
   
<script type="text/javascript">
jQuery('.status_no').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
 
});
  
</script>


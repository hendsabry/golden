<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Rosestbig_leftmenu =1; ?>

<div class="merchant_vendor">   <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
    
      <header>
        <?php if(request()->autoid==''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.ADDWRAPPINGTYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADDWRAPPINGTYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADDWRAPPINGTYPE')); ?> <?php endif; ?> </h5>
          <?php else: ?>
 <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.UPDATE_WRAPPING_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATE_WRAPPING_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATE_WRAPPING_TYPE')); ?> <?php endif; ?> </h5>
          <?php endif; ?>
          <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
           <?php  $id = request()->id; $itemid = request()->itemid; $autoid = request()->autoid; ?>

              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->  

                <form name="form1"  id ="menucategory" method="post" action="<?php echo e(route('roses-updatewrapping')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <div class="form_row">
                    <div class="form_row_left">
                     <label class="form_label">
                      <span class="english"><?php echo lang::get('mer_en_lang.NAME'); ?></span>
                      <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.NAME'); ?> </span>

              </label>  
                    <div class="info100">
                      <div class="english">
                      <input type="text" value="<?php echo e(isset($getAttr->option_title) ? $getAttr->option_title : ''); ?>" class="english" name="category_name" maxlength="80" id="category_name" required="" >
                      </div>
                      <div class="arabic ar">
                      <input class="arabic ar" id="category_ar" maxlength="80"  name="category_name_ar"  value="<?php echo e(isset($getAttr->option_title_ar) ? $getAttr->option_title_ar : ''); ?>"  required=""  type="text" >
                      </div>
                       </div>
                       </div>
                  </div>



      <div class="form_row common_field">
                    <div class="form_row_left">
                      <label class="form_label">  
 <?php if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_IMAGE')); ?> <?php endif; ?>
 
                       </label>
                      <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" required="" value="">
                      </div>
<span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                  <?php if(isset($getAttr->image) && $getAttr->image!=''): ?>

<div class="form-upload-img"><img src="<?php echo e($getAttr->image); ?>"></div>

                  <?php endif; ?>
                    </div>
                        
                        
                    </div>
                  </div>



                        <div class="form_row common_field">
                    <div class="form_row_left">
                     <label class="form_label">                     
                   <?php if(Lang::has(Session::get('mer_lang_file').'.PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PRICE')); ?> <?php endif; ?> 
                      </label>  
                    <div class="info100">
                      <div class="englishs">
                      <input type="text" onkeypress="return isNumber(event)" value="<?php echo e(isset($getAttr->price) ? $getAttr->price : ''); ?>" class="notzero xs_small" name="price" maxlength="9" id="price" required="" >
                      </div>
                      
                       </div>
                       </div>
                  </div>

                  
                  <div class="form_row">
      
                  <input type="hidden" name="id" value="<?php echo e(isset($id) ? $id : ''); ?>">    
                   <input type="hidden" name="itemid" value="<?php echo e(isset($itemid) ? $itemid : ''); ?>">
                   <input type="hidden" name="autoid" value="<?php echo e($autoid); ?>">
                      <input type="hidden" name="type" value="1">
                   
                   <div class="form_row">
                   <div class="form_row_left">
                   <?php if($autoid==''): ?>
                   <div class="arabic ar">
                <input type="submit" name="submit" value="خضع">
         </div>
          <div class="english">
                <input type="submit" name="submit" value="Submit">
         </div> <!-- form_row -->
                  <?php else: ?> 
                   <div class="arabic ar">
        
                <input type="submit" name="submit" value="تحديث">
         </div>
          <div class="english">
                <input type="submit" name="submit" value="Update">
         </div> <!-- form_row -->
                  <?php endif; ?>
                  </div>
                  </div>
                  </div>
                  
                </form>


              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },
                        price: {
                        required: true,
                        },
               <?php if(isset($getAttr->image) && $getAttr->image!=''): ?>         
                     mc_img: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },
<?php else: ?>
 mc_img: {
                           required:true,
                           accept:"png|jpe?g|gif",
                      },

<?php endif; ?>
                       category_name_ar: {
                       required: true,
                      },

                       
                     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
                  category_name: {
                  required:  "<?php echo lang::get('mer_en_lang.MER_NAME_VALIDATION'); ?>",
                  }, 
                  category_name_ar: {
                  required:  "<?php echo lang::get('mer_ar_lang.MER_NAME_VALIDATION'); ?>",
                  },
                  mc_img: {
                  accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                  required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                  }, 
                  price: {
                  required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE')); ?> <?php endif; ?>",
                  }, 
                },
                invalidHandler: function(e, validation){
 
                    var valdata=validation.errorList;
                     console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


                    <?php if($mer_selected_lang_code !='en'): ?>
                      if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                       if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.arabic_tab').trigger('click'); 
                      }
                       if (typeof valdata.price != "undefined" || valdata.price != null) 
                      {
                      $('.arabic_tab').trigger('click'); 
                      }
                      if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                    <?php else: ?>  
                      if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                        if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                       if (typeof valdata.price != "undefined" || valdata.price != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      } 
                    <?php endif; ?>


                    },
 

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="service_listingrow">
    <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.Invitees_list')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Invitees_list')); ?>  
      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Invitees_list')); ?> <?php endif; ?></h5>

      <div style="float:right;"><a  href="<?php echo e(url('')); ?>/manage_assigned_invitation" class="btn">Back</a></div>
  </div>
  
  <div class="global_area">
    <div class="row">
      <div class="col-lg-12">
        <div class="table_wrap">
		  
          <div class="panel-body panel panel-default">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <?php if($getorderinvitation->count() < 1): ?>
            <div class="no-record-area"> <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?> </div>
            <?php else: ?>
			
            <div class="table dish-table">
              <div class="tr">
                
                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Invitees_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Invitees_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Invitees_Name')); ?> <?php endif; ?></div>
                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Invitees_Email')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Invitees_Email')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Invitees_Email')); ?> <?php endif; ?></div>

                <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Invitees_Address')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Invitees_Address')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Invitees_Address')); ?> <?php endif; ?></div>

                

                 <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?></div>
                
                 
              </div>
              <?php $totalamount =0; ?>
              <?php $__currentLoopData = $getorderinvitation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>              
          
              <?php
                   if($value->status==1){ 
               if (Lang::has(Session::get('mer_lang_file').'.Attended')!= ''){ $atst=trans(Session::get('mer_lang_file').'.Attended'); }else { $atst=trans($MER_OUR_LANGUAGE.'.Attended'); }

               }else{
                  if (Lang::has(Session::get('mer_lang_file').'.NotAttended')!= ''){ $atst=trans(Session::get('mer_lang_file').'.NotAttended'); }else{  $atst=trans($MER_OUR_LANGUAGE.'.NotAttended'); }
              }?>
           
              <div class="tr">
                
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?>">

                  <?php echo e(isset($value->invite_name) ? $value->invite_name : ''); ?></div>
                <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Occasion_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Occasion_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Occasion_Name')); ?> <?php endif; ?>">  <?php echo e(isset($value->invite_email) ? $value->invite_email : ''); ?> </div>
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Invitees_Address')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Invitees_Address')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Invitees_Address')); ?> <?php endif; ?>">   <?php echo e($value->address); ?> </div>
              
                <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?>">   <?php echo e(isset($atst) ? $atst : ''); ?> </div>
               
              </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
            <?php endif; ?>
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          </div>
          <?php echo e($getorderinvitation->links()); ?> </div>
      </div>
    </div>
  </div>
</div>
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         <?php if($mer_selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        <?php else: ?>
        $('#todata').html("End date should be greater than From date");
        <?php endif; ?>
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper"> <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="inner_wrap">
    <!-- search-section -->
    <div class="page-left-right-wrapper not-sp">
      <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
      <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="myaccount_right">
        <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.MyDashboard')!= '')  ?  trans(Session::get('lang_file').'.MyDashboard'): trans($OUR_LANGUAGE.'.MyDashboard')); ?></h1>
        <div class="field_group top_spacing_margin"> <span class="dash_heading"><?php echo e((Lang::has(Session::get('lang_file').'.UPCOMING_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.UPCOMING_OCCASIONS'): trans($OUR_LANGUAGE.'.UPCOMING_OCCASIONS')); ?> </span>
          <div class="main_user">
            <?php 
			//echo '<pre>';print_r($order);die;
			if(!count($order)<1){?>
            <div class="myaccount-table">
              <div class="mytr">
                <div class="mytable_heading order_id"></div>
                <div class="mytable_heading"> <?php echo e((Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')); ?> </div>
                <div class="mytable_heading"><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')); ?> </div>
                <div class="mytable_heading order_id"><?php echo e((Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')); ?></div>
              </div>
              <?php 
             if(!count($order)<1)
      			 {
                     $i = 1;
                     foreach($order as $val)
      			   {               
			          ?>
              <div class="mytr">
                <div class="mytd td1" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.S.NO')!= '')  ?  trans(Session::get('lang_file').'.S.NO'): trans($OUR_LANGUAGE.'.S.NO')); ?> "><?php echo e($i); ?></div>
                <?php 
				if(isset($val->search_occasion_id) && $val->search_occasion_id!='0')
				{
                $setTitle = Helper::getOccasionName($val->search_occasion_id);
                $mc_name='title';
                if(Session::get('lang_file')!='en_lang')
                $mc_name= 'title_ar'; ?>
                <div class="mytd td2" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')); ?>"><a style="text-decoration:none;" href="<?php echo e(route('order-details',['id'=>$val->order_id])); ?>"><?php if(isset($setTitle->$mc_name) && $setTitle->$mc_name!=''){echo $setTitle->$mc_name;}else{echo 'N/A';} ?></a> </div>
				 <?php } else { 
				 if(isset($val->main_occasion_id) && $val->main_occasion_id!='0'){ 
				 if(Session::get('lang_file')!='en_lang')
				 {
				   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
				 }
				 else
				 {
					$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
				 } 
				 foreach($getArrayOfOcc as $key=>$ocval)
				 {
				  if($val->main_occasion_id==$key)
				  {
				   $occasion_name = $ocval;
				  }
				 }				  
				 ?> 
				 <div class="mytd td2" data-title="Hall">
				 <a href="<?php echo e(route('order-details',['id'=>$val->order_id])); ?>"><?php echo $occasion_name; ?></a>
				 </div>
				 <?php  }} ?>                
                <div class="mytd td3" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')); ?>">
				<?php echo e(Carbon\Carbon::parse($val->order_date)->format('d M Y')); ?>			
				</div>
                <div class="mytd order_id" data-title="<?php echo e((Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')); ?>"><a href="<?php echo e(route('order-details',['id'=>$val->order_id])); ?>"><?php echo e($val->order_id); ?></a></div>
              </div>
              <?php            
             $i++; }} ?>
            </div>
            <?php }else{ ?>
            <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div>
            <!-- no-record-area -->
            <?php } ?>
          </div>
        </div>
        <?php if(count($order)>= 5){ //if(!count($order)<1){ ?>
        <div class="all_review"><a href="<?php echo e(route('my-account-ocassion')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.VIEW_ALL')!= '')  ?  trans(Session::get('lang_file').'.VIEW_ALL'): trans($OUR_LANGUAGE.'.VIEW_ALL')); ?></a></div>
        <?php }//} ?>
        <div class="my_photo_area lts">
          <div class="myphoto_left"><span class="dash_heading"><?php echo e((Lang::has(Session::get('lang_file').'.LATEST_PHOTO_GALLERY')!= '')  ?  trans(Session::get('lang_file').'.LATEST_PHOTO_GALLERY'): trans($OUR_LANGUAGE.'.LATEST_PHOTO_GALLERY')); ?> </span>
            <?php if(isset($getImagelist[0]->id) && $getImagelist[0]->id!=''){ ?>
            <?php $setAllImage = Helper::getAllOccasionImage($getImagelist[0]->id); ?>
            <?php if(count($setAllImage) > 0): ?>
            <div class="galls">
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')); ?>:</div>
                <div class="wed_text">
                  <?php if(isset($getImagelist[0]->occasion_name) && $getImagelist[0]->occasion_name!=''){ echo $getImagelist[0]->occasion_name;} ?>
                </div>
              </div>
              <?php if(isset($getImagelist[0]->city_id) && $getImagelist[0]->city_id!=''){ ?>
              <div class="occs">
                <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CTIY')); ?>:</div>
                <?php $city_name = Helper::getcity($getImagelist[0]->city_id); ?>
                <div class="wed_text"><?php echo e($city_name->ci_name); ?></div>
              </div>
              <?php } ?>
            </div>
            <div class="photosmy">
              <ul>
                <?php if(isset($getImagelist[0]->id) && $getImagelist[0]->id!=''){ ?>
                <?php $setAllImage = Helper::getAllOccasionImage($getImagelist[0]->id); ?>
                <?php if(count($setAllImage) > 0): ?>
                <?php $__currentLoopData = $setAllImage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><img src="<?php echo e($val->image_url); ?>" width="125" alt="" /></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <div class="no-record-area" align="center"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div>
                <?php endif; ?>
                <?php } ?>
              </ul>
            </div>
            <div class="photo_view_detail"><a href="<?php echo e(route('my-account-studio')); ?>">View Details</a></div>
            <?php endif; ?>
            <?php } else{ ?>
            <div class="no-record-area" align="center"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div>
            <?php } ?>
          </div>
          <div class="myphoto_left"><span class="dash_heading"><?php echo e((Lang::has(Session::get('lang_file').'.WALLET_BALANCE')!= '')  ?  trans(Session::get('lang_file').'.WALLET_BALANCE'): trans($OUR_LANGUAGE.'.WALLET_BALANCE')); ?> </span>
            <div class="sar_money">
              <div class="sar_text">SAR
                <?php 
			$userid  = Session::get('customerdata.user_id');
			$getInfo = Helper::getuserinfo($userid); echo number_format($getInfo->wallet,2);
			?>
              </div>
              <div class="add_money"><a href="<?php echo e(route('my-account-wallet')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ADD_MONEY')!= '')  ?  trans(Session::get('lang_file').'.ADD_MONEY'): trans($OUR_LANGUAGE.'.ADD_MONEY')); ?></a></div>
            </div>
          </div>
        </div>
        <div class="my_latest_area lts">
          <div class="latest_mix"><span class="dash_heading"><?php echo e((Lang::has(Session::get('lang_file').'.LATEST_REVIEW')!= '')  ?  trans(Session::get('lang_file').'.LATEST_REVIEW'): trans($OUR_LANGUAGE.'.LATEST_REVIEW')); ?> </span>
            <div class="my_latest_revie" >
              <?php 
		     $i =1; 
             if(count($getReview) > 0){ 
			 foreach($getReview as $val){?>
              <div class="btm <?php if($i!=1) echo 'tps'; ?>">
                <div class="occs">
                  <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')); ?>: </div>
                  <div class="wed_text"> <?php $shop_id = $val->shop_id; 
                    $get_show_name = Helper::getCatName($shop_id);
                    echo $get_show_name;
                    ?></div>
                </div>
                <div class="occs">
                  <div class="occus_text"><?php echo e((Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')); ?>: </div>
                  <div class="wed_text"> <?php 
                    $vandor_id = $val->vandor_id; 
                    $get_service_provider = Helper::getServiceProviderName($vandor_id);
                    echo $get_service_provider;
                    ?> </div>
                  <div class="my_star"> <?php 
                    $total_rate = $val->ratings;
                    for($i=1;$i<=5;$i++){ if($i==$total_rate) { ?> <img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($i); ?>.png" alt="" /> <?php }} ?> </div>
                </div>
                <div class="lors_text"><?php echo e($val->comments); ?></div>
              </div>
              <?php }$i++;?>
              <div class="all_review"><a href="<?php echo e(route('my-account-review')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ALL_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.ALL_REVIEWS'): trans($OUR_LANGUAGE.'.ALL_REVIEWS')); ?></a></div>
              <?php } else{?>
              <div class="no-record-area" align="center"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
</div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="<?php echo e(url('/')); ?>/themes/js/height.js"></script>

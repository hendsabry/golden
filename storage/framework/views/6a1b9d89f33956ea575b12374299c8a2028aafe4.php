<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
  $cururl = request()->segment(count(request()));
?>
<div class="outer_wrapper">
  <div class="inner_wrap">
    <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($fooddateshopdetails[0]->mc_img); ?>" alt="" /></a></div>
      </div>
      <div class="vendor_header_right">
        <!-- vendor_header_left -->
        <div class="vendor_welc">

           <?php echo $__env->make("includes.language-changer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div class="vendor_name username"> <?php echo e((Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')); ?> <span>
            <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
            <ul class="vendor_header_navbar">
              <li><a href="<?php echo e(route('my-account-profile')); ?>"<?php if($cururl=='my-account-profile') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-ocassion')); ?>"<?php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-studio')); ?>"<?php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-security')); ?>"<?php if($cururl=='my-account-security') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-wallet')); ?>"<?php if($cururl=='my-account-wallet') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')); ?></a></li>
              <li><a href="<?php echo e(route('my-account-review')); ?>"<?php if($cururl=='my-account-review') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')); ?></a></li>
              <li><a href="<?php echo e(route('my-request-a-quote')); ?>"<?php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')); ?></a></li>
              <li><a href="<?php echo e(route('change-password')); ?>"<?php if($cururl=='change-password') echo 'class="select"'; ?>><?php echo e((Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')); ?></a></li>
              <li><a href="<?php echo e(url('login-signup/logoutuseraccount')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')); ?></a></li>
            </ul>
            </span></div>
          <?php if(Session::get('customerdata.token')!=''): ?>
          <?php $getcartnoitems = Helper::getNumberOfcart(); ?>
          <?php if($getcartnoitems>0): ?> <a href="<?php echo e(url('mycart')); ?>" class="vendor_cart"><img src="<?php echo e(url('')); ?>/themes/images/basket.png" /><span><?php echo e($getcartnoitems); ?></span></a> <?php endif; ?>
          <?php endif; ?> </div>
        <!-- vendor_header_right -->
        <?php if(count($otherbarnch)>1){ ?>
        <div class="select_catg">

          <div class="select_lbl"><?php if(Lang::has(Session::get('lang_file').'.Other_Branches')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Other_Branches')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Other_Branches')); ?> <?php endif; ?></div>
          <div class="search-box-field">
            <select class="select_drp" id="dynamic_select">
              <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Branch')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Branch')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Branch')); ?> <?php endif; ?></option>           
                        <?php $__currentLoopData = $otherbarnch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $otherbarnches): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          
                <option value="<?php echo e(url('')); ?>/dessertshop/<?php echo e($halltype); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($otherbarnches->mc_id); ?>"> <?php echo e($otherbarnches->mc_name); ?> </option>
                

                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>
        </div>
        <?php } ?> </div>
    </div>
  </div>
    <!-- vemdor_header -->
    <div class="common_navbar">
      <div class="inner_wrap">
        <div id="menu_header" class="content">
          <ul>
            <li><a href="#about_shop" class="active"><?php if(Lang::has(Session::get('lang_file').'.About_Shop')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.About_Shop')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.About_Shop')); ?> <?php endif; ?></a></li>
            <li><a href="#video"><?php if(Lang::has(Session::get('lang_file').'.Video')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Video')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Video')); ?> <?php endif; ?></a></li>
            <?php if(count($fooddateshopreview)>0){ ?>
            <li><a href="#our_client"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></a></li>
            <?php } ?>
            <li><a href="#choose_package"><?php if(Lang::has(Session::get('lang_file').'.Choose_Package')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Choose_Package')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Choose_Package')); ?> <?php endif; ?></a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- common_navbar -->
    <div class="inner_wrap service-wrap diamond_space">
      <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
        <div class="service_detail_row">
          <div class="gallary_detail">
            <section class="slider">
              <div id="slider" class="flexslider">
                <ul class="slides">
                  <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php $galleryimg=str_replace('thumb_','',$shopgallery->image) ?>
                  <li> <img src="<?php echo e($galleryimg); ?>" /> </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                <ul class="slides">
                  <?php $__currentLoopData = $fooddateshopgallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shopgallerythumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li> <img src="<?php echo e($shopgallerythumb->image); ?>" /> </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
          <div class="service_detail">
            <div class="detail_title"><?php echo e($fooddateshopdetails[0]->mc_name); ?></div>
            <div class="detail_hall_description"><?php echo e($fooddateshopdetails[0]->address); ?></div>
            <div class="detail_hall_subtitle"><?php if(Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ABOUT_SHOP')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?> <?php endif; ?></div>
            <div class="detail_about_hall">
              <div class="comment more"><?php echo e($fooddateshopdetails[0]->mc_discription); ?></div>
            </div>
            <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: <span>
              <?php
		    $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?>
              </span></div>
              <?php if($fooddateshopdetails[0]->latitude!='' && $fooddateshopdetails[0]->longitude!=''){  $lat=$fooddateshopdetails[0]->latitude;   $long=$fooddateshopdetails[0]->longitude;    ?>


          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          <?php }  ?>
          </div>
        </div>
        <!-- service_detail_row -->
        <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
          <div class="service-video-area">
            <div class="service-video-cont"><?php echo e($fooddateshopdetails[0]->mc_video_description); ?></div>
            <div class="service-video-box">
              <iframe class="service-video" src="<?php echo e($fooddateshopdetails[0]->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>
          <!-- service-video-area -->
          <?php if(count($fooddateshopreview)>0){ ?>
          <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
            <div class="common_title"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></div>
            <div class="testimonial_slider">
              <section class="slider">
                <div class="flexslider1">
                  <ul class="slides">
                    <?php $__currentLoopData = $fooddateshopreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                      <div class="testimonial_row">
                        <div class="testim_left">
                          <div class="testim_img"><img src="<?php echo e($customerreview->cus_pic); ?>"></div>
                        </div>
                        <div class="testim_right">
                          <div class="testim_description"><?php echo e($customerreview->comments); ?></div>
                          <div class="testim_name"><?php echo e($customerreview->cus_name); ?></div>
                          <div class="testim_star"><img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png"></div>
                        </div>
                      </div>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
                </div>
              </section>
            </div>
          </div>
          <?php } ?> </div>
        <!-- service-mid-wrapper -->
        <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
          <div class="service_line">
            <div class="service_tabs">
              <div id="content-5" class="content">
                <ul>
                  <li><a href="<?php echo e(url('/')); ?>/dessertshop/<?php echo e($halltype); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($shop_id); ?>" class="select"><?php if(Lang::has(Session::get('lang_file').'.TYPE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TYPE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.TYPE')); ?> <?php endif; ?></a></li>
                  <li><a href="<?php echo e(url('/')); ?>/dessertdishshop/<?php echo e($halltype); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($shop_id); ?>"><?php if(Lang::has(Session::get('lang_file').'.DISH')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DISH')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.DISH')); ?> <?php endif; ?></a></li>
                </ul>
              </div>
            </div>
            <div class="service_bdr"></div>
          </div>
        </div>
        <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
          <div class="service-display-right">
            <?php $k=0; if(count($fooddateshopproducts)>0){ ?>
            <div class="diamond_main_wrapper">
              <div class="diamond_wrapper_outer">
                <div class="diamond_wrapper_main"> <?php if($category_id==10){ $foodshop='datesshop'; }else{ $foodshop='foodshopbranch'; } ?>
                  <?php  $i=1;     ?>
                  <?php    $k=count($fooddateshopproducts);  ?>
                  <?php if($k<6){ ?>
                  <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $fooddateshopproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                      <div class="category_wrapper" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                        <div class="category_title">
                          <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                        </div>
                      </div>
                      </a> </div>
                    <?php $i=$i+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!------------ 6th-------------->
                  <?php }elseif($k==6){ ?>
                  <?php $j=1; ?>
                  <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $fooddateshopproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                    <?php if($j==1){ ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($j==2){ ?>
                      <div class="row_2of5 rows5row"> <?php } ?> 
                        <?php if($j==3){  ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($j==5){ ?>
                          <div class="row_4of5 rows5row"> <?php } ?>
                            <?php if($j==6){ ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                              <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                                <div class="category_title">
                                  <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                                </div>
                              </div>
                              </a> <?php if($j==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($j==2){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==4){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?>
                      <?php if($j==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php $j=$j+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!------------ 7th-------------->
                  <?php }elseif($k==7){ ?>
                  <?php $l=1; ?>
                  <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $fooddateshopproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                    
                    <?php if($l==1){ ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($l==2){ ?>
                      <div class="row_2of5 rows5row"> <?php } ?> 
                        <?php if($l==3){  ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($l==6){ ?>
                          <div class="row_4of5 rows5row"> <?php } ?>
                            <?php if($l==7){ ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                              <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                                <div class="category_title">
                                  <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                                </div>
                              </div>
                              </a> <?php if($l==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($l==2){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==5){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?>
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php $l=$l+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!------------ 8th-------------->
                  <?php }elseif($k==8){ ?>
                  <?php $l=1; ?>
                  <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $fooddateshopproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                    <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                    <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                    
                    <?php if($l==1){ $classrd='category_wrapper1'; ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($l==2){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==4){  ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($l==6){ ?>
                          <div class="row_3of5 rows5row"> <?php } ?>
                            <?php if($l==8){ $classrd='category_wrapper9'; ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                              <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                                <div class="category_title">
                                  <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                                </div>
                              </div>
                              </a> <?php if($l==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($l==3){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==5){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==7){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?>
                      <?php if($l==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php $l=$l+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <!-- 9th -->
                  <?php }elseif($k==9){ ?>
                  <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                    <?php $__currentLoopData = $fooddateshopproducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
                    
                    
                    <?php if($i==1){ ?>
                    <div class="row_1of5 rows5row"> <?php } ?> 
                      <?php if($i==2){ ?>
                      <div class="row_2of5 rows5row"> <?php } ?> 
                        <?php if($i==4){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?> 
                          <?php if($i==7){ ?>
                          <div class="row_4of5 rows5row"> <?php } ?> 
                            <?php if($i==9){ ?>
                            <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></span></span> </span> </a> <?php if($i==1){ ?>
                              <div class="clear"></div>
                            </div>
                            <?php } ?> 
                            <?php if($i==3){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==6){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==8){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==9){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    
                    <?php $i=$i+1; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                  <?php } ?> </div>
              </div>


                <?php }else{?> <?php echo e((Lang::has(Session::get('lang_file').'.No_product_found_in_this_shop')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_shop'): trans($OUR_LANGUAGE.'.No_product_found_in_this_shop')); ?> <?php }?>






            </div>







            <?php if($k >=2): ?>
            <div class="diamond_shadow"><?php echo e($fooddateshopproducts->links()); ?></span></div>
            <?php endif; ?>
            <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
          </div>

            <?php  
      
      //if(isset($productlist[0]->pro_disprice) && $productlist[0]->pro_disprice!='0'){$getPrice = $productlist[0]->pro_disprice;}else{$getPrice = $productlist[0]->pro_price;} 
      ?>
          <!-- service-display-right -->
          <?php if($k >=1): ?>
          
          <?php echo Form::open(['url' => 'dessertshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

          <div class="service-display-left"><span id="selectedproduct">
            <div class="service-left-display-img">

                <?php    $pro_id = $fooddateshopleftproduct->pro_id; 


                ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       
            </div>
            <div class="service-product-name"><?php echo e($fooddateshopleftproduct->pro_title); ?></div>
            <div class="service-product-description"><?php echo e($fooddateshopleftproduct->pro_desc); ?></div>
            </span>
            <div class="service-radio-line">
              <div class="leftbar_title"><?php if(Lang::has(Session::get('lang_file').'.SHOP_BY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SHOP_BY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SHOP_BY')); ?> <?php endif; ?></div>
              <?php if(count($fooddateshopleftproduct->product_price) ==1): ?>
              <div class="service-radio-box">
                <input type="radio" id="kilo" name="selectedwt" value="22" onclick="selectedweight(this.value);pricecalculation('remove');" />
                <label for="kilo"><?php if(Lang::has(Session::get('lang_file').'.PER_QTY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PER_QTY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PER_QTY')); ?> <?php endif; ?></label>
              </div>
              <?php else: ?>
              <div class="service-radio-box">
                <input type="radio" id="kilo" name="selectedwt" value="21" onclick="selectedweight(this.value);pricecalculation('remove');" />
                <label for="kilo"><?php if(Lang::has(Session::get('lang_file').'.PER_KILO')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PER_KILO')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PER_KILO')); ?> <?php endif; ?></label>
              </div>
              <div class="service-radio-box" style="display: none;">
                <input type="radio" id="piece" name="selectedwt" value="23" onclick="selectedweight(this.value); pricecalculation('remove');"/>
                <label for="piece"><?php if(Lang::has(Session::get('lang_file').'.PER_PIECE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PER_PIECE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PER_PIECE')); ?> <?php endif; ?></label>
              </div>
              <?php endif; ?>
              <div class="service_quantity_box" id="service_quantity_box" style="display: none;">
                <div class="service_qunt"><?php if(Lang::has(Session::get('lang_file').'.Quantity')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Quantity')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Quantity')); ?> <?php endif; ?></div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity" data-title="Total Quality">
                    <div class="quantity">
                      <button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
                      <input type="number" name="itemqty" id="qty" value="1" min="1"  onkeyup="isNumberKey(event); pricecalculation('pricewithqty');" onkeydown="isNumberKey(event); pricecalculation('pricewithqty');" />
                      <button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button>
                    </div>

                    <?php if($fooddateshopleftproduct->product_price[0]->discount>0){ $netprice=$fooddateshopleftproduct->product_price[0]->discount_price; }else{ $netprice=$fooddateshopleftproduct->product_price[0]->product_option_value_id; } ?>
                    <span class="service_qunt_price" id="p22" style="display: none;">SAR <?php echo e(isset($netprice) ? $netprice : '0'); ?> </span>
                     <span class="service_qunt_price" id="p23" style="display: none;">SAR <?php echo e(isset($fooddateshopleftproduct->product_price[1]->product_option_value_id) ? $fooddateshopleftproduct->product_price[1]->product_option_value_id : '0'); ?></span> </div>
                </div>
              </div>
            </div>
            <!-- service-radio-line -->
            <span id="addtocartprice" style="display: none;">
            <div class="container_total_price foods-totals"><?php if(Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TOTAL_PRICE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?> <?php endif; ?>: <span class="cont_final_price" id="cont_final_price">SAR 0.00 </span></div>
            </span>
            <div class="addto_cart"> <span id="selectedproductprices">
              <input type="hidden" id="product_id" name="product_id" value="<?php echo e($fooddateshopleftproduct->pro_id); ?>">
              <input type="hidden" id="21" name="pr21" value="<?php echo e(isset($netprice) ? $netprice : '0'); ?>">
              <input type="hidden" id="23" name="pr23" value="<?php echo e(isset($fooddateshopleftproduct->product_price[1]->product_option_value_id) ? $fooddateshopleftproduct->product_price[1]->product_option_value_id : '0'); ?>">
              <input type="hidden" name="shopby" id="shopby" value="">
              </span>
              <input type="hidden" id="cart_type" name="cart_type" value="food">
              <input type="hidden" id="attribute_id" name="attribute_id" value="34">
              <input type="hidden" id="prodqty" name="prodqty" value="<?php echo e($fooddateshopleftproduct->pro_qty); ?>">
              <input type="hidden" id="shop_id" name="shop_id" value="<?php echo e($shop_id); ?>">
              <input type="hidden" id="category_id" name="category_id" value="<?php echo e($subcategory_id); ?>">
              <input type="hidden" id="subcategory_id" name="subcategory_id" value="<?php echo e($category_id); ?>">
              <input type="hidden" id="branch_id" name="branch_id" value="<?php echo e($branch_id); ?>">
              <span id="error"></span> <span id="addtocart" style="display: none;">               
              <input type="submit" class="form-btn" id="sbtn" value="<?php if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Add_to_Cart')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Add_to_Cart')); ?> <?php endif; ?>">
            </div>
            </span> 

<div class="terms_conditions"> <a href="<?php echo e($fooddateshopdetails[0]->terms_conditions); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')); ?></a><a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a> </div>
          </div>

          <!-- container-section -->
          <?php echo Form::close(); ?>

          
          
          
          <?php endif; ?> 


        </div>
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
    <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
</div>
<!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="trd"><?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
</script>
<script type="text/javascript">
	 function pricecalculation(act){
	 		if (!$('input[name=selectedwt]:checked').val() ) {    
	 		    		document.getElementById('error').innerHTML = 'please select shopby option first';   
	 		    		document.getElementById('qty').value=1; 
					
					}else{
							
							var no=1;
              var product_id      = document.getElementById('product_id').value;
							var selectedshopby=document.getElementById('shopby').value;
							var currentquantity=document.getElementById('qty').value;
							var unititemprice=document.getElementById(''+selectedshopby).value;
							if(currentquantity<1){
								document.getElementById('qty').value=1;
								 var qty= parseInt(no);
							}else{
							if(act=='pricewithqty'){
								var qty=parseInt(currentquantity)
							}
							if(act=='add'){
									 var qty= parseInt(currentquantity)+parseInt(no);
								 }
							if(act=='remove'){ 
									if(parseInt(currentquantity)==1){
								      var qty=parseInt(currentquantity)
								   }else{
								      var qty=parseInt(currentquantity)-parseInt(no);
									}

							 }
							 //alert(qty);


                  if(product_id)
  {
                 $.ajax({
     type:"GET",
     url:"<?php echo e(url('getProductQuantity')); ?>?product_id="+product_id+'&qty='+qty,
    async: false,
     success:function(res)
     { 
         <?php $Cur = Session::get('currency'); ?>              
     if(res!='ok')
     {
      $('.action_popup').fadeIn(500);
       $('.overlay').fadeIn(500);
       $('#showmsg').show();
       $('#hidemsgab').hide();
       $('#showmsgab').show();
             var qtyupdated = parseInt(currentquantity)-1;  
             $('#qty').val(qtyupdated);   
        
     }
     else
     {
        var newqty=document.getElementById('qty').value;
        var producttotal=qty*unititemprice;
              //alert(producttotal);
              document.getElementById('cont_final_price').innerHTML = 'SAR '+producttotal;
     }
     }
   });

  }










							
							}
							
							
					}
	 }

</script>
<script type="text/javascript">
	
	function selectedweight(val){
		//alert(val);
       var Add_to_Cart = "<?php if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Add_to_Cart')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Add_to_Cart')); ?> <?php endif; ?>";

       var SOLDOUT = "<?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>";
    var prodqty=document.getElementById('prodqty').value;

      if(prodqty<1){
          $("#sbtn").val(SOLDOUT);
          $('#qtyop').css('display','none');
          $('#addtocartprice').css('display','none');
          
          $(':input[type="submit"]').prop('disabled', true);
         }else{
          $("#sbtn").val(Add_to_Cart);
          $('#qtyop').css('display','block');
          $('#addtocartprice').css('display','block');
          $(':input[type="submit"]').prop('disabled', false);
         }


		document.getElementById('shopby').value=val;
		document.getElementById('error').innerHTML = ''; 
		document.getElementById('qty').value=1;
		$('#addtocartprice').css('display','');
		$('#addtocart').css('display','');
		$('#service_quantity_box').css('display','');
		
		if(val==22)
		{

				$('#p22').css('display','');
				$('#p23').css('display','none');
		}
		if(val==23){

					$('#p22').css('display','none');
				$('#p23').css('display','');
		}
	}

function checkgallery(str)
{
jQuery.ajax({
type:"GET",
url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
success:function(res)
{ 
jQuery('.product_gallery').html(res);
}
});

}
  
</script>
<script type="text/javascript">
	function getdatedish(selecteddateproduct){
		//alert(selecteddateproduct);
		var dateproductid=selecteddateproduct;

			  if(selecteddateproduct){
        $.ajax({
           type:"GET",
           url:"<?php echo e(url('datesshop/getcartproduct')); ?>?product_id="+dateproductid,
           success:function(res){  
               $('html, body').animate({
    scrollTop: ($('.service-display-left').first().offset().top)
},500);
             
            if(res){
            	 var json = JSON.stringify(res);
			var obj = JSON.parse(json);
			//alert(obj);
console.log(obj);
            	
            		length=obj.productdateshopinfo.length;
            		//alert(length);

            		if(length>0){

                  $('input[name=selectedwt]').attr('checked',false);
        $('#addtocartprice').css('display','none');
    $('#addtocart').css('display','none');
    $('#service_quantity_box').css('display','none');



		 for(i=0; i<length; i++)
			{
        
checkgallery(obj.productdateshopinfo[i].pro_id);
        
        jQuery('[name=prodqty]').val(obj.productdateshopinfo[i].pro_qty);


			$('#selectedproduct').html('<div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-product-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');
			$('#selectedproductprices').html('<input type="hidden" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" name="shopby" id="shopby" value="">');
			}
			
			}

			pricelength=obj.productdateshopprice.length;
            		
            if(pricelength>0){
		 for(k=0; k<pricelength; k++)
			{
        if(obj.productdateshopprice[k].discount>0){
          var netprice=obj.productdateshopprice[k].discount_price;
        }else{
          var netprice=obj.productdateshopprice[k].product_option_value_id;
        }
			
			$('#selectedproductprices').append('<input type="hidden" id="'+obj.productdateshopprice[k].product_option_id+'" name="pr'+obj.productdateshopprice[k].product_option_id+'" value="'+netprice+'">');
			$('#p'+obj.productdateshopprice[k].product_option_id).html('SAR '+netprice);
				}
			
			}

           }
           }
        });
    }


		
	}

</script>
<script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>
<script>

      // bind change event to select
      jQuery('#dynamic_select').on('change', function () {
      
          var url = jQuery(this).val(); // get selected value          
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
  
</script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">

  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

</script>

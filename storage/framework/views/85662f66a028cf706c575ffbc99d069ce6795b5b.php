<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
<div class="inner_wrap">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</div> <!-- outer_wrapper -->
</div>
 <div class="header-page-divder">&nbsp;</div>
<div class="inner_wrap">
  <div class="inner_wrap">
	<?php if(Session::has('loginstatus')): ?>
	  <div class="alert alert-info"><?php echo e(Session::get('loginstatus')); ?></div>
	<?php endif; ?>
 <?php (Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ? $sub= trans(Session::get('lang_file').'.SUBMIT'): $sub=trans($OUR_LANGUAGE.'.SUBMIT');

  ?>
  
 <a name="loginfrml"></a>
     <div class="forgot-password-area">  
	 <div class="form_title"><?php echo e((Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD')); ?></div>
   <div class="forgot-password-box">
   <p><?php echo e((Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD_MSG')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD_MSG'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD_MSG')); ?> <?php echo e((Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD_MSG2')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD_MSG2'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD_MSG2')); ?> </p>
   <?php echo Form::open(['url' => 'userforgotpassword/checkloginaccount', 'method' => 'post', 'name'=>'loginfrm', 'id'=>'loginfrm', 'enctype' => 'multipart/form-data']); ?>

    <div class="usersign-form">
    	
  <div class="usersign-form-line frg">
  <div class="usersign-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.EMAILADDRRESS')!= '')  ?  trans(Session::get('lang_file').'.EMAILADDRRESS'): trans($OUR_LANGUAGE.'.EMAILADDRRESS')); ?></div>
  <div class="usersign-form-bottom"><?php echo Form::text('email', null, array('required','id'=>'emailaddres', 'class'=>'t-box','maxlength'=>'80','onfocusout'=>'checkalreadyregisteredemail(this.value);')); ?><span id="availabilitystatus"></span></div>
  </div>
  
  <div class="usersign-form-button"><?php echo Form::submit($sub, array('class'=>'form-btn')); ?></div>
  </div>
  <?php echo Form::close(); ?>

   </div>
  
  </div> <!-- forgot-password-area -->
  
  
 

 



  






</div>  <!-- usersign-area -->






</div>





<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 <script type="text/javascript">
 
jQuery("#loginfrm").validate({
                  ignore: [],
                  rules: {
             email: {
                       required: true,
             		email: true,
                      },

                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
             email: {
               required:  "<?php echo (Lang::has(Session::get('lang_file').'.USER_EMAIL')!= '')  ?  trans(Session::get('lang_file').'.USER_EMAIL'): trans($OUR_LANGUAGE.'.USER_EMAIL'); ?>", 
                      },  
         
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<script language="javascript">

function checkalreadyregisteredemail(emailaddress) {
	var emailaddress=emailaddress;
	$.ajax({
	url: "<?php echo e(url('userforgotpassword/checkuseraccount')); ?>",
	data: { email: emailaddress, _token: '<?php echo e(csrf_token()); ?>' },
	type: "POST",
	dataType: "text",
	success:function(data){
			if(data=='Error'){
					$("#availabilitystatus").html("");
			}else{
					
					$("#emailaddres").val('');
					$("#availabilitystatus").html("<span class='status-not-available error'>this email address does not registered. please create account.</span>");
			}
			
		
	},
	error:function (){ }
	});

}
</script>
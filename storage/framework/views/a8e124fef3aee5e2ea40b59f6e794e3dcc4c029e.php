<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.css" />
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($vendordetails->mc_img); ?>" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
           <?php if(trim($vendordetails->mc_video_url)!=''): ?>

          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>
          <?php endif; ?>

          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
          <?php } ?>
          <li><a href="#choose_package"><?php echo e((Lang::has(Session::get('lang_file').'.Choose_Dress')!= '')  ?  trans(Session::get('lang_file').'.Choose_Dress'): trans($OUR_LANGUAGE.'.Choose_Dress')); ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?><li><img src="<?php echo e(str_replace('thumb_','',$vendordetails->image)); ?>" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
               <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>               
                    <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"><?php echo e($vendordetails->mc_name); ?></div>
          <div class="detail_hall_description"><?php echo e($vendordetails->address); ?></div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
          <div class="detail_about_hall"><div class="comment more"><?php echo e($vendordetails->mc_discription); ?></div></div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: <span><?php
		    $getcityname = Helper::getcity($vendordetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?></span></div>

       <?php if($vendordetails->latitude!='' && $vendordetails->longitude!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>


          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          <?php }  ?>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
	    <a name="video" class="linking">&nbsp;</a>

      <?php if(trim($vendordetails->mc_video_url)!=''): ?>
        <div class="service-video-area">
          <div class="service-video-cont"><?php echo e($vendordetails->mc_video_description); ?></div>
          <div class="service-video-box">
            <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
<?php endif; ?>

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
				 <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				 <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
				  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>			  
                </ul>
              </div>
            </section>
          </div>
        </div>
		<?php } ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <?php $k=1;?>
                <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php 
                if(count($categories) > 0 ) 
                {
                if($k==1)
                { 
                $cl='select'; 
                }
                else
                { 
                $cl=''; 
                } 
                ?>
				<?php 
				$j = count($categories->cat_attribute);
				if($j > 0){ ?>
                <li><a href="#<?php echo e($k); ?>"  class="cat <?php echo e($cl); ?>" data-toggle="tab" onclick="return showProductDetail('<?php echo e($categories->cat_attribute[0]->pro_id); ?>','<?php echo e($categories->cat_attribute[0]->pro_mr_id); ?>')"><?php echo e($categories->attribute_title); ?></a></li>
                <?php } $k++; } ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  
	  <!-- service_bottom -->
      <div class="service-display-section"> 
	  <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer"> <?php  $z=1;     ?>
              <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productservice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php $k=count($productservice->cat_attribute); 
              if($k >0 ){
              ?>
              <?php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } ?>
              <div class="diamond_wrapper_main tab-pane fade <?php echo e($addactcon); ?>"  id="<?php echo e($z); ?>"> <?php if($k < 6){ ?>
                <?php  $i=1;     ?>
                <div class="diamond_wrapper_inner "> <?php $__currentLoopData = $productservice->cat_attribute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <?php $img = str_replace('thumb_','',$getallcats->pro_Img); ?>
                  <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                    <div class="category_wrapper  <?php if($k==3): ?> category_wrapper<?php echo e($k); ?> <?php endif; ?>" style="background:url(<?php echo e(isset($img) ? $img : ''); ?>);">
                      <div class="category_title">
                        <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                      </div>
                    </div>
                    </a> </div>
                  <?php $i=$i+1; ?>
                  
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 6th-------------->
                <?php }elseif($k==6){ ?>
                <?php $j=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->cat_attribute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                  <?php if($j==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($j==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($j==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($j==6){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($j==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==4){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($j==5){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($j==6){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $j=$j+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 7th-------------->
                <?php }elseif($k==7){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->cat_attribute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                  
                  <?php if($l==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($l==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($l==7){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==7){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 8th-------------->
                <?php }elseif($k==8){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $productservice->cat_attribute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                  <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                  <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                  
                  <?php if($l==1){ $classrd='category_wrapper1'; ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_3of5 rows5row"> <?php } ?> 
                      <?php if($l==4){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?>
                          <?php if($l==8){ $classrd='category_wrapper9'; ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==8){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!---------- 9th ------------------->
                <?php }elseif($k==9){ ?>
                <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                  <?php $__currentLoopData = $productservice->cat_attribute; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
                  
                  
                  <?php if($i==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($i==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($i==4){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($i==7){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?> 
                          <?php if($i==9){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return showProductDetail('<?php echo e($getallcats->pro_id); ?>','<?php echo e($getallcats->pro_mr_id); ?>')"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></span></span> </span> </a> <?php if($i==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    <?php if($i==9){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?> 
                  
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php } ?> </div>
              <?php $z=$z+1; }?> 
			  <div class="diamond_shadow"></span></div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
			  </div>
          </div>
		  <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
		  <div class="" align="center"><?php echo e($productservice->cat_attribute->links()); ?></div>
		  </div>
        <!-- service-display-right -->
		<?php echo Form::open(['url' => 'addtocartofdress', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

		<?php if(isset($productlist[0]->cat_attribute[0]->pro_disprice) && ($productlist[0]->cat_attribute[0]->pro_disprice!='0' || $productlist[0]->cat_attribute[0]->pro_disprice!='0.00')){$getPrice = $productlist[0]->cat_attribute[0]->pro_disprice;}else{$getPrice = $productlist[0]->cat_attribute[0]->pro_price;}?>
        <div class="service-display-left">
        
         <input type="hidden" id="Insuranceamount" name="Insuranceamount" value="<?php echo e($productlist[0]->cat_attribute[0]->Insuranceamount); ?>">	
          <input type="hidden" id="category_id" name="category_id" value="<?php echo e($category_id); ?>">
          <input type="hidden" id="subcat_id" name="subcat_id" value="<?php echo e($subcat_id); ?>">
          <input type="hidden" id="shop_id" name="shop_id" value="<?php echo e($shop_id); ?>">
          <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9" />
          <input type="hidden" name="actiontype" value="dressesdetail">
          <input type="hidden" name="cart_sub_type" value="dress">
          <input type="hidden" name="cart_type" value="shopping">
		  <b><?php echo e(Session::get('status')); ?></b>
		  <span id="selectedproduct">
		  <input type="hidden" id="insuranceamount" name="insuranceamount" value="<?php if(isset($productlist[0]->cat_attribute[0]->Insuranceamount) && $productlist[0]->cat_attribute[0]->Insuranceamount!=''){echo $productlist[0]->cat_attribute[0]->Insuranceamount;}?>">
		  <input type="hidden" id="attribute_id" name="attribute_id" value="<?php if(isset($productlist[0]->cat_attribute[0]->attribute_id) && $productlist[0]->cat_attribute[0]->attribute_id!=''){echo $productlist[0]->cat_attribute[0]->attribute_id;}?>">
          <input type="hidden" id="product_id" name="product_id" value="<?php if(isset($productlist[0]->cat_attribute[0]->pro_id) && $productlist[0]->cat_attribute[0]->pro_id!=''){echo $productlist[0]->cat_attribute[0]->pro_id;}?>">
          <input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>">
          <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->cat_attribute[0]->pro_mr_id) && $productlist[0]->cat_attribute[0]->pro_mr_id!=''){echo $productlist[0]->cat_attribute[0]->pro_mr_id;}?>">

          <div class="service-left-display-img product_gallery">

   <?php    $pro_id = $productlist[0]->cat_attribute[0]->pro_id; ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          </div>
		  <div class="service-product-name marB0"><?php if(isset($productlist[0]->cat_attribute[0]->pro_title) && $productlist[0]->cat_attribute[0]->pro_title!=''){ echo $productlist[0]->cat_attribute[0]->pro_title;}?>
      
      
      </div>
          <div class="service_prod_description"><?php if(isset($productlist[0]->cat_attribute[0]->pro_desc) && $productlist[0]->cat_attribute[0]->pro_desc!=''){ echo nl2br($productlist[0]->cat_attribute[0]->pro_desc);}?></div>
		  </span>
      
		   <?php 
		  if(isset($productlist[0]->cat_attribute[0]->pro_id) && $productlist[0]->cat_attribute[0]->pro_id!='')
		  {
		    $getalldata = Helper::getProduckOptionInfo($productlist[0]->cat_attribute[0]->pro_id,$productlist[0]->cat_attribute[0]->pro_mr_id); 
        $isqtythere=0;
			if(count($getalldata) > 0)
			{
        
			?>
			  <div class="service_selecrrow dress_selecrrow" id="ptattrsizeenone">
				<div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.SIZE_DESSESS')!= '')  ?  trans(Session::get('lang_file').'.SIZE_DESSESS'): trans($OUR_LANGUAGE.'.SIZE_DESSESS')); ?></div>
				<div class="checkout-form-bottom" id="ptattrsize">
      
				  <select class="checkout-small-box" name="product_size" id="product_size" onchange="morefrmoption(this.value,'<?php echo e($productlist[0]->cat_attribute[0]->pro_id); ?>','<?php echo e($productlist[0]->cat_attribute[0]->pro_mr_id); ?>');">
					<option value=""><?php echo e((Lang::has(Session::get('lang_file').'.SELECT')!= '')  ?  trans(Session::get('lang_file').'.SELECT'): trans($OUR_LANGUAGE.'.SELECT')); ?></option>
					<?php  foreach($getalldata as $val){ if($val->value!=''){ 
              $siozeoption=substr($val->option_title, 4, 2);
            ?>
					  <option value="<?php echo $val->option_title;?>">Size <?php echo str_replace('Size','',$val->option_title);?></option>
					<?php }} ?>
				  </select>
				</div>
          <div class="size-chart-line"><a href="#pop" class="try_now_page_one"><?php echo e((Lang::has(Session::get('lang_file').'.Size_Chart')!= '')  ?  trans(Session::get('lang_file').'.Size_Chart'): trans($OUR_LANGUAGE.'.Size_Chart')); ?></a></div>
			  </div>
		  <?php }} ?>
		      <span id="opensel" style="display: none;">
          <div class="service_selecrrow dress_selecrrow" id="outofstock">
		    <div class="buy_rent">
			  <div class="books_male_left">
				<input id="ShopId" name="buy_rent" type="radio" id="buy_rent" value="buy" onclick="selectRentAndBy(this.value);">
				<label for="ShopId"><?php echo e((Lang::has(Session::get('lang_file').'.BUY')!= '')  ?  trans(Session::get('lang_file').'.BUY'): trans($OUR_LANGUAGE.'.BUY')); ?></label>
			  </div>
			   <?php 
			  if(isset($productlist[0]->cat_attribute[0]->pro_id) && $productlist[0]->cat_attribute[0]->pro_id!='')
			  {




				$getalldata = Helper::getProduckOptionCheckRent($productlist[0]->cat_attribute[0]->pro_id,$productlist[0]->cat_attribute[0]->pro_mr_id); 

				

			  ?>
			  <div class="books_male_left" id="rentdata" <?php if(count($getalldata) < 1 ): ?>  style="display:none;"   <?php endif; ?> >
				<input id="RentId" name="buy_rent" id="buy_rent" type="radio" value="rent" onclick="selectRentAndBy(this.value);">
				<label for="RentId"><?php echo e((Lang::has(Session::get('lang_file').'.RENT')!= '')  ?  trans(Session::get('lang_file').'.RENT'): trans($OUR_LANGUAGE.'.RENT')); ?></label>
			  </div>
			  <?php   } //echo $isqtythere;?>
            </div>
          
          </div>
    </span>
      <div> &nbsp;</div>
     <div id="sold" class="form-btn addto_cartbtn" style="display: none;"><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></div>
          
		  <span id="addtocartprice" style="display: none;">
		  <div class="service-radio-line">
            <div class="service_quantity_box">
			 <div class="service_qunt"><?php echo e((Lang::has(Session::get('lang_file').'.QUANTITY')!= '')  ?  trans(Session::get('lang_file').'.QUANTITY'): trans($OUR_LANGUAGE.'.QUANTITY')); ?></div>
              <div class="service_qunatity_row">
                <div class="td td2 quantity food-quantity" data-title="Total Quality">
                  <div class="quantity">
                    <button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
                    <input type="number" name="itemqty" id="qty" value="1" min="1" max="9" onblur="return pricecalculation('add');" />
                    <button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  <!--<div class="packge_numb"><?php echo e((Lang::has(Session::get('lang_file').'.PRICE_FOR_PURCHASE')!= '')  ?  trans(Session::get('lang_file').'.PRICE_FOR_PURCHASE'): trans($OUR_LANGUAGE.'.PRICE_FOR_PURCHASE')); ?></div>--> 
		  <span class="ajaxIdForPrice">
		  <div class="discount_price"><span class="strike newprice"><?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->cat_attribute[0]->pro_price){echo currency($productlist[0]->cat_attribute[0]->pro_price, 'SAR',$Current_Currency);} ?></span> <span id="cont_final_price1">  <?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->cat_attribute[0]->pro_price){echo currency($getPrice, 'SAR',$Current_Currency);}else{echo currency($productlist[0]->cat_attribute[0]->pro_price, 'SAR',$Current_Currency);} ?></span></div>
		  </span>
          <div class="total_food_cost padT0">
            <div class="total_price"><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: 
               
                  <?php $cprice=number_format($getPrice,2,'.','');
                        $ncprice=number_format($productlist[0]->cat_attribute[0]->pro_price,2,'.','');
                   ?>
              <span id="cont_final_price2" class="total_price">  <?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->cat_attribute[0]->pro_price){echo currency($cprice, 'SAR',$Current_Currency);}else{ echo currency($ncprice, 'SAR',$Current_Currency);} ?></span></div>
          </div>
		  </span>
		  <span id="sizetestId">
		    <div id="addtocarbtn" class="equipment-button addtocartClass" style="display: none">
 
   <input type="submit" name="submit" id="submit"  value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>"  class="form-btn btn-info-wisitech">

 


        </div>
            <div id="booknow" class="equipment-button" style="display:none"><a href="javascript:void(0);" class="form-btn big-btn rentpopup_open"><?php echo e((Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '')  ?  trans(Session::get('lang_file').'.BOOK_NOW'): trans($OUR_LANGUAGE.'.BOOK_NOW')); ?></a></div>
			<?php if(isset($vendordetails->terms_conditions) && $vendordetails->terms_conditions!=''){ ?>
			<div class="terms_conditions" style="display:none"> <a href="<?php echo e($vendordetails->terms_conditions); ?>" target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')); ?></a></div>
			<?php } ?>
			
	      </span>
        
        </div>
        <a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a>
		<?php echo Form::close(); ?>

        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
	  <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>   
	  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
<?php echo Form::open(['url' => 'addtocartofdress', 'method' => 'post', 'name'=>'booknowid', 'id'=>'booknowid', 'enctype' => 'multipart/form-data']); ?>

<input type="hidden" name="category_id" value="<?php echo e($category_id); ?>">
<input type="hidden" name="subcat_id" value="<?php echo e($subcat_id); ?>">
<input type="hidden" name="shop_id" value="<?php echo e($shop_id); ?>">
<input type="hidden" name="actiontype" value="dressesdetail">
<input type="hidden" name="cart_sub_type" value="dress">
<input type="hidden" name="cart_type" value="shopping">
<input type="hidden" name="itemqty" id="itemqty_on_popup" value="1"/>
<input type="hidden" name="attribute_id" id="attribute_id_on_popup" value="">
<input type="hidden" name="product_id"  id="product_id_on_popup" value="">
<input type="hidden" name="priceId" id="priceId_on_popup" value="">
<input type="hidden" name="insuranceamount" id="insuranceamount_on_popup" value="">
<input type="hidden" name="vendor_id" id="vendor_id_on_popup" value="">
<input type="hidden" name="buy_rent" id="buy_rent_popup" value="">
<input type="hidden" name="product_size" id="product_size_pop" value="">
<input type="hidden" name="product_ordered" id="product_ordered" value="">
<div class="openpop">
  <span id="todata"></span>
  <div class="rent-pop-up"> <a href="javascript:void(0);" class="close_popup_btn">X</a>
    <div class="main_title"><?php echo e((Lang::has(Session::get('lang_file').'.DRESS_FOR_RENT')!= '')  ?  trans(Session::get('lang_file').'.DRESS_FOR_RENT'): trans($OUR_LANGUAGE.'.DRESS_FOR_RENT')); ?></div>
    <div class="rent-form">
      <div class="rent-form-line">
        <div class="rent-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.OCCASIONS_DATE')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_DATE'): trans($OUR_LANGUAGE.'.OCCASIONS_DATE')); ?></div>
        <div class="rent-form-bottom">
          <input type="text" name="rental_time" id="rental_time" maxlength="40" autocomplete="off" class="t-box cal-t datetimepicker" value=""/>
        </div>
      </div>
      <div class="rent-form-line">
        <div class="rent-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.RETURN_DATE')!= '')  ?  trans(Session::get('lang_file').'.RETURN_DATE'): trans($OUR_LANGUAGE.'.RETURN_DATE')); ?></div>
        <div class="rent-form-bottom">
          <input type="text" name="return_time" id="return_time" autocomplete="off"  maxlength="40" class="t-box cal-t calculate_hour" value="" onchange="testfun();" />
        </div>
      </div>
      <div id="todata"></div>
      <div class="rent-form-line">
        <!--<div class="rent-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')); ?>  <?php echo e(Session::get('currency')); ?> <span id="insuranceamount_on_popup_lb"></span></div>-->
        <div class="rent-form-bottom">
          <div class="insurance_price"></div>
          <div class="insurance_price"><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: <br> <?php echo e(Session::get('currency')); ?>  <span id="priceId_on_popup_lb"></span></div>
        </div>
      </div>
      <div class="rent-form-btn">
        <input type="submit" class="form-btn big-btn btn-info-wisitech" value="<?php echo e((Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '')  ?  trans(Session::get('lang_file').'.BOOK_NOW'): trans($OUR_LANGUAGE.'.BOOK_NOW')); ?>" />
      </div>
    </div>
  </div>
</div>
<?php echo Form::close(); ?>

<div id="try_now_page_one_popup" class="new-popup sizechart-popup" style="display:none;"> 
       <a href="javascript:void(0);" class="b-close">X</a>
        
        <div class="table">
<div class="tr">
<div class="table_heading">US</div>
<div class="table_heading">UK</div>
<div class="table_heading">Germany</div>
<div class="table_heading">France</div>
<div class="table_heading">Italy</div>
<div class="table_heading">Kore</div>
 
</div> <!-- tr -->

<div class="tr">
<div class="td td1" data-title="US">0</div>
<div class="td td2" data-title="UK">4</div>                     
<div class="td td3" data-title="Germany">30</div>
<div class="td td4" data-title="France">32</div>   
<div class="td td5" data-title="Italy">36</div>   
<div class="td td6" data-title="Kore">44</div>   
 
 </div> <!-- tr -->

<div class="tr">
<div class="td td1" data-title="US">2</div>
<div class="td td2" data-title="UK">6</div>                     
<div class="td td3" data-title="Germany">32</div>
<div class="td td4" data-title="France">34</div>   
<div class="td td5" data-title="Italy">38</div>   
<div class="td td6" data-title="Kore">44</div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">4</div>
<div class="td td2" data-title="UK">8</div>                     
<div class="td td3" data-title="Germany">34</div>
<div class="td td4" data-title="France">36</div>   
<div class="td td5" data-title="Italy">40</div>   
<div class="td td6" data-title="Kore">55</div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">6</div>
<div class="td td2" data-title="UK">10</div>                     
<div class="td td3" data-title="Germany">36</div>
<div class="td td4" data-title="France">38</div>   
<div class="td td5" data-title="Italy">42</div>   
<div class="td td6" data-title="Kore">55</div>   
  
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">8</div>
<div class="td td2" data-title="UK">12</div>                     
<div class="td td3" data-title="Germany">38</div>
<div class="td td4" data-title="France">40</div>   
<div class="td td5" data-title="Italy">44</div>   
<div class="td td6" data-title="Kore">66</div>   
  
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">10</div>
<div class="td td2" data-title="UK">14</div>                     
<div class="td td3" data-title="Germany">40</div>
<div class="td td4" data-title="France">42</div>   
<div class="td td5" data-title="Italy">46</div>   
<div class="td td6" data-title="Kore">66</div>   

 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">12</div>
<div class="td td2" data-title="UK">16</div>                     
<div class="td td3" data-title="Germany">42</div>
<div class="td td4" data-title="France">44</div>   
<div class="td td5" data-title="Italy">48</div>   
<div class="td td6" data-title="Kore">77</div>   

 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">14</div>
<div class="td td2" data-title="UK">18</div>                     
<div class="td td3" data-title="Germany">44</div>
<div class="td td4" data-title="France">46</div>   
<div class="td td5" data-title="Italy">50</div>   
<div class="td td6" data-title="Kore">77</div>   
   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">16</div>
<div class="td td2" data-title="UK">20</div>                     
<div class="td td3" data-title="Germany">46</div>
<div class="td td4" data-title="France">48</div>   
<div class="td td5" data-title="Italy">52</div>   
<div class="td td6" data-title="Kore">88</div>   
  
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">18</div>
<div class="td td2" data-title="UK">22</div>                     
<div class="td td3" data-title="Germany">48</div>
<div class="td td4" data-title="France">50</div>   
<div class="td td5" data-title="Italy">54</div>   
<div class="td td6" data-title="Kore">88</div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">20</div>
<div class="td td2" data-title="UK">24</div>                     
<div class="td td3" data-title="Germany">50</div>
<div class="td td4" data-title="France">52</div>   
<div class="td td5" data-title="Italy">56</div>   
<div class="td td6" data-title="Kore"></div>   
  </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">22</div>
<div class="td td2" data-title="UK">26</div>                     
<div class="td td3" data-title="Germany">52</div>
<div class="td td4" data-title="France">54</div>   
<div class="td td5" data-title="Italy">58</div>   
<div class="td td6" data-title="Kore"></div>   
 
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">24</div>
<div class="td td2" data-title="UK">28</div>                     
<div class="td td3" data-title="Germany">54</div>
<div class="td td4" data-title="France">56</div>   
<div class="td td5" data-title="Italy">60</div>   
<div class="td td6" data-title="Kore"></div>   
  </div> <!-- tr -->
           
           </div>
        </div> <!-- new-popup -->
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script src="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>
<script> 
jQuery(window).load(function()
{
  jQuery("body").on('mouseover', '.datetimepicker', function() {  
  jQuery(this).datetimepicker({ ampm: true, // FOR AM/PM FORMAT
    format : 'd M Y g:i A',
    minDate : 0 }); 
  });

  jQuery("body").on('mouseover', '#return_time', function() { 
  var startrentDate =  $('#rental_time').val();  
  jQuery(this).datetimepicker({ ampm: true, // FOR AM/PM FORMAT
    format : 'd M Y g:i A',
    minDate : 0 }); 
  });

})
</script>

<script type="text/javascript">
  function morefrmoption(val,pid,mid){

        var size_id      = val;
        var product_id      = pid;
        var vandor_id      = mid;

        $.ajax({
     type:"GET",
     url:"<?php echo e(url('getdresssize')); ?>?product_id="+product_id+'&product_size='+size_id+'&vandor_id='+vandor_id,
    async: false,
     success:function(res)
     { 
         <?php $Cur = Session::get('currency'); ?>  
if(res =='ok')
     {
$('#sold').css('display','none');
 $('#opensel').css('display','block');
 $('#RentId').attr('disabled',false);
 $('#ShopId').attr('disabled',false);
     }

   else if(res =='rentnotavail')
     {
$('#sold').css('display','none');
 $('#opensel').css('display','block');
 $('#RentId').attr('disabled',true);
$('#ShopId').attr('disabled',false);

     }
     
    else if(res =='buynotavail')
     {
$('#sold').css('display','none');
 $('#opensel').css('display','block');
$('#RentId').attr('disabled',false);
$('#ShopId').attr('disabled',true);

     } 


     else 
     {
      
      
      $('#addtocartprice').css('display','none');
       $('#sold').css('display','block');
      $('#opensel').css('display','none');
      $('#addtocarbtn').css('display','none');
      $('#booknow').css('display','none');
      

     }
      
     }
   });

    
  }

</script>

<script type="text/javascript">



  $('body').mouseover(function()
{
 
 testfun();
});

$(document).ready(function()
{
  var startDate =  $('#rental_time').val();
  var endDate =  $('#return_time').val()  

})

  //$('body').on('change dp.change', '#return_date', function() { 
  function testfun()
  {
 
var startDate =  $('#rental_time').val();
  var endDate =  $('#return_time').val()  
 var BOOK_NOW = "<?php if(Lang::has(Session::get('lang_file').'.BOOK_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BOOK_NOW')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.BOOK_NOW')); ?> <?php endif; ?>";
$('input[type="submit"]').val(BOOK_NOW);
 $('input[type="submit"]').prop('disabled',false);

if(startDate !='' && endDate !='')
{

$('#popitemqty').val($('#qty').val());

 
var timeStart = new Date(startDate).getTime();
var timeEnd = new Date(endDate).getTime();
var hourDiff = timeEnd - timeStart; //in ms
var secDiff = hourDiff / 1000; //in s
var minDiff = hourDiff / 60 / 1000; //in minutes
var hDiff = hourDiff / 3600 / 1000; //in hours
var CalHours =   Math.floor(hDiff);

var PerH = $('#priceId_on_popup').val();
var insuranceamount = $('#insuranceamount_on_popup').val();
var QTY = $('#qty').val();


var TotalAmount = ((CalHours * PerH)+parseFloat(insuranceamount)) * parseInt(QTY);


//alert(TotalAmount);
//rentfinal_price pricePerhour

    if ((Date.parse(startDate) > Date.parse(endDate))) {

      document.getElementById("return_date").value = "";
        <?php if($selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");
        <?php else: ?>
        $('#todata').html("End date should be greater than Start date");
        <?php endif; ?>
      
     
    }
    else
    {
      $('#todata').html("");
      <?php if($selected_lang_code !='en'): ?>
        $('#priceId_on_popup_lb').html(parseFloat(TotalAmount).toFixed(2));
        <?php else: ?>
        $('#priceId_on_popup_lb').html(parseFloat(TotalAmount).toFixed(2));
        <?php endif; ?>
 
    }
//alert(CalHours);
   document.getElementById('product_ordered').value=parseFloat(TotalAmount).toFixed(2);
      
var bookingdate_from =  $('#rental_time').val();
  var bookingdate_to =  $('#return_time').val();
  var product_id =  $('#product_id_on_popup').val();
  var product_size =  $('#product_size').val(); 
console.log(bookingdate_from);
console.log(bookingdate_to);
 

 $.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('checkdressrent')); ?>?product_id="+product_id+"&product_size="+product_size+"&bookingdate_to="+bookingdate_to+"&bookingdate_from="+bookingdate_from,
     success:function(res)
     {  
       if(res==0)
       {
var SOLD_OUT = "<?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>";

 $('input[type="submit"]').val(SOLD_OUT);
 $('input[type="submit"]').prop('disabled',true);
       }
       else
       {
 var BOOK_NOW = "<?php if(Lang::has(Session::get('lang_file').'.BOOK_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BOOK_NOW')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.BOOK_NOW')); ?> <?php endif; ?>";
$('input[type="submit"]').val(BOOK_NOW);
 $('input[type="submit"]').prop('disabled',false);
       }
     }
   });

 
 
}
  };
  
</script>
<script type="text/javascript">
    function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }


function selectRentAndBy(buyrent)
{   
    var product_id         = $('#product_id').val();
    var vendor_id          = $('#vendor_id').val();
	var attribute_id       = $('#attribute_id').val();	
	var insuranceamount    = $('#insuranceamount').val();

	var buy_rent_id        = $('#RentId').val();
	var product_size_id   = $('#product_size').val();
    var insuramount= parseFloat(insuranceamount).toFixed(2);
    var insurancetext = "<?php if(Lang::has(Session::get('lang_file').'.INSURANCE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.INSURANCE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.INSURANCE')); ?> <?php endif; ?>";
	//alert(product_size_id);
	$('#product_size_pop').val(product_size_id);
	$('#product_id_on_popup').val(product_id);
	$('#vendor_id_on_popup').val(vendor_id);
	$('#buy_rent_popup').val(buy_rent_id);
	$('#attribute_id_on_popup').val(attribute_id);
	$('#insuranceamount_on_popup').val(insuranceamount);
	$('#insuranceamount_on_popup_lb').html(insuranceamount);
	
    $('#addtocartprice').css('display','block');
	$('.terms_conditions').css('display','block');
	$('#qty').val(1);
	$('#itemqty').val(1);
	if(buyrent=='buy')
	{   
        $('#addtocarbtn').css('display','block');
        $('#booknow').css('display','none');
		$.ajax({
		   type:"GET",
		   url:"<?php echo e(url('getbuyprice')); ?>?product_id="+product_id+"&vendor_id="+vendor_id,
		   async: false,
		   success:function(res)
		   {             
			 if(res!='')
			 {
			     var result = res.split('-');
				 if(result[1]=='ok')
				 {
				   $('#priceId').val(result[0]);
				   var pamount=parseFloat(result[0]).toFixed(2);
 <?php $Cur = Session::get('currency'); ?>
 pamount = getChangedPrice(pamount);  
				   $('.ajaxIdForPrice').html('<div class="discount_price"><span id="cont_final_price1"><?php echo $Cur;?> '+pamount+'</span></div>');
				   $('#cont_final_price2').html(pamount);
				   			   
				 }
				 else
				 {
				   $('#priceId').val(result[1]);
				   var pamount=parseFloat(result[0]).toFixed(2);
				   var disamount=parseFloat(result[1]).toFixed(2);
            <?php $Cur = Session::get('currency'); ?>
            pamount = getChangedPrice(pamount); 
            disamount = getChangedPrice(disamount); 
				   $('.ajaxIdForPrice').html('<div class="discount_price"><span class="strike newprice"><?php echo $Cur;?> '+pamount+'</span> <span id="cont_final_price1"><?php echo $Cur;?> '+disamount+'</span></div>');
				   $('#cont_final_price2').html(disamount);					   			   
				 }
			 }			 
		   }
	  });    
	}
	if(buyrent=='rent')
	{
      
       $('.disrent').hide();
       $('#addtocarbtn').css('display','none');
       $('#booknow').css('display','block');
	   $.ajax({
		   type:"GET",
		   url:"<?php echo e(url('getrentprice')); ?>?product_id="+product_id+"&vendor_id="+vendor_id,
		   async: false,
		   success:function(res)
		   {             
			 if(res!='')
			 {  //alert(res);
			     var result = res.split('-');
           <?php $Cur = Session::get('currency'); ?>
            if(result[1]=='ok')
				 {
				   $('#priceId').val(result[0]);
				   var pamount=parseFloat(result[0]).toFixed(2);
           var nprice=parseFloat(pamount)+parseFloat(insuramount);
           pamount = getChangedPrice(pamount);  
           nprice = getChangedPrice(nprice);
				   $('.ajaxIdForPrice').html('<div class="discount_price"><span id="cont_final_price1"><?php echo $Cur;?> '+pamount+'</span></div><div class="discount_price"><span id="insuranceamount_price1">'+insurancetext+' <?php echo $Cur;?> '+insuramount+'</span></div>');
				   $('#cont_final_price2').html(nprice.toFixed(2));
				   $('#priceId_on_popup').val(pamount);	
				   $('#priceId_on_popup_lb').html(pamount);				   
				 }
				 else
				 {
				  
				   var pamount=parseFloat(result[0]).toFixed(2);
				   var disamount=parseFloat(result[1]).toFixed(2);
            $('#priceId').val(result[1]);
            if(result[1]>0){
                var realprice=parseFloat(result[1]).toFixed(2);
            }else{
                 var realprice=parseFloat(result[0]).toFixed(2);
            }
           var nprice=parseFloat(realprice);
           nprice = getChangedPrice(nprice);  
           pamount = getChangedPrice(pamount); 
           var fprice=parseFloat(nprice).toFixed(2);
           if(result[1]>0){
            var damount='<div class="discount_price"><?php if(Lang::has(Session::get('lang_file').'.PRICE_PER_HOUR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE_PER_HOUR')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PRICE_PER_HOUR')); ?> <?php endif; ?> <span class="strike newprice"><?php echo $Cur;?> '+pamount+'</span> <span id="cont_final_price1"><?php echo $Cur;?> '+disamount+'</span></div><div class="discount_price"><span id="insuranceamount_price1">'+insurancetext+' <?php echo $Cur;?> '+insuramount+'</span></div>';
           }else{
             var damount='<div class="discount_price"><?php if(Lang::has(Session::get('lang_file').'.PRICE_PER_HOUR')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE_PER_HOUR')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PRICE_PER_HOUR')); ?> <?php endif; ?> <span id="cont_final_price1"><?php echo $Cur;?> '+pamount+'</span></div><div class="discount_price"><span id="insuranceamount_price1">'+insurancetext+' <?php echo $Cur;?> '+insuramount+'</span></div>';
           }
				   $('.ajaxIdForPrice').html(damount);
				   $('#cont_final_price2').html(fprice);	
				   $('#priceId_on_popup').val(fprice);	
				   $('#priceId_on_popup_lb').html(nprice);			   
				 }
			 }			 
		   }
	  });
	}
}
</script>
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});

function pricecalculation(act)
{
    var no=1;
    var ordertype          = $('input[name="buy_rent"]:checked').val();
	var product_id         = document.getElementById('product_id').value;
	var currentquantity    = document.getElementById('qty').value;
	var unititemprice      = document.getElementById('priceId').value;
	var product_size       = document.getElementById('product_size').value;
	var Insuranceamount    = document.getElementById('insuranceamount').value;
    if(ordertype=='rent')
	{
       var totalpriceforcalculation=parseFloat(unititemprice)+parseFloat(Insuranceamount);
    }
	else
	{
       totalpriceforcalculation = parseFloat(unititemprice);
    }
	if(act=='add')
	{
		var qty = parseInt(currentquantity)+parseInt(no);			
	}
	else
	{ 
		if(parseInt(currentquantity)==1)
		{
		  var qty=parseInt(currentquantity)
		}
		else
		{
		  var qty=parseInt(currentquantity)-parseInt(no);
		}
	}
	if(product_size)
	{
	 $.ajax({
	   type:"GET",
	   url:"<?php echo e(url('getSizeQuantity')); ?>?product_id="+product_id+'&product_size='+product_size+'&qty='+qty+'&ordertype='+ordertype,
	   async: false,
	   success:function(res)
	   {   
         <?php $Cur = Session::get('currency'); ?>            
		 if(res!='ok')
		 {
		     //$('#sizetestId').hide();
			 $('.action_popup').fadeIn(500);
			 $('.overlay').fadeIn(500);
			 $('#showmsg').show();
			 $('#hidemsgab').hide();
			 $('#showmsgab').show();
		     var qtyupdated = parseInt(currentquantity);			
		     document.getElementById('qty').value = qtyupdated - 1;
		     document.getElementById('hideid').style.display = "none";
		 }
		 else
		 {
		    $('#sizetestId').show();
			var producttotal = qty*totalpriceforcalculation;
            producttotal = getChangedPrice(producttotal); 
			//document.getElementById('cont_final_price1').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
			document.getElementById('cont_final_price2').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
			document.getElementById('itemqty').value=qty;
			document.getElementById('itemqty_on_popup').value = qty;
			document.getElementById('priceId_on_popup_lb').innerHTML = parseFloat(producttotal).toFixed(2);
			document.getElementById('hideid').style.display = "block";
		 }
	   }
	 });
	}
	else
	{

		var producttotal = qty*totalpriceforcalculation;
         producttotal = getChangedPrice(producttotal); 
		
		document.getElementById('cont_final_price2').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
		document.getElementById('itemqty').value = qty;
		document.getElementById('itemqty_on_popup').value = qty;
		document.getElementById('priceId_on_popup_lb').innerHTML = parseFloat(producttotal).toFixed(2);
	}
}

function showProductDetail(productId,vendorId)
{

      $('#opensel').css('display','none');
       $('.terms_conditions').css('display','none');
	   $.ajax({
	   type:"GET",
	   url:"<?php echo e(url('getShoppingProductDress')); ?>?product_id="+productId+'&vendor_id='+vendorId,
	   success:function(res)
	   {              
		if(res)
		{  
		  $('#addtocartprice').css('display','none');
		  $('#addtocarbtn').css('display','none');
          $('#booknow').css('display','none');
          $('input[name=buy_rent]').attr('checked', false);
		  var json = JSON.stringify(res);
		  var obj = JSON.parse(json);
           <?php $Cur = Session::get('currency'); ?>
		  productlength = obj.productdata.length;
		  if(productlength > 0)
		  {
			for(i=0; i<productlength; i++)
		    {   
			   if(obj.productdata[i].pro_disprice !='0'){var getPrice = obj.productdata[i].pro_disprice;}else{var getPrice = obj.productdata[i].pro_price;}
			   if(getPrice < obj.productdata[i].pro_price)
			   {   
			      var pamount   = parseFloat(getPrice).toFixed(2);
				  var disamount = parseFloat(obj.productdata[i].pro_price).toFixed(2); 
            pamount = getChangedPrice(pamount); 
            disamount = getChangedPrice(disamount); 

				  $('.newprice').html('<del><?php echo $Cur;?> '+disamount+'</del>');	
				  $('#cont_final_price1').html('<?php echo $Cur;?> '+pamount);
				  $('#cont_final_price2').html('<?php echo $Cur;?> '+pamount);
			   }	
			   else
			   {
          pamountx = getChangedPrice(obj.productdata[i].pro_price); 
          disamountx = getChangedPrice(obj.productdata[i].pro_price); 

			     $('#cont_final_price1').html('<?php echo $Cur;?> '+pamountx);
				 $('#cont_final_price2').html('<?php echo $Cur;?> '+disamountx);		
				 $('.newprice').html('');	
			   }
		       $('#selectedproduct').html('<input type="hidden" id="insuranceamount" name="insuranceamount" value="'+obj.productdata[i].Insuranceamount+'"><input type="hidden" id="attribute_id" name="attribute_id" value="'+obj.productdata[i].attribute_id+'"><input type="hidden" id="product_id" name="product_id" value="'+obj.productdata[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+getPrice+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdata[i].pro_mr_id+'"><div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productdata[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdata[i].pro_desc+'</div><div id="gallery></div>'); 
			   
		    }
		  }
		  if($.trim(obj.productattrsize) !=1)
		  {
		    $('#ptattrsize').html(obj.productattrsize);
			$('#ptattrsizeenone').css('display','block');
      $('#outofstock').css('display','block');
      $('#sold').css('display','none');
      
		  }
		  else
		  {
		    $('#ptattrsizeenone').css('display','none');
        $('#outofstock').css('display','none');
        $('#sold').css('display','block');
		  }
		  if($.trim(obj.productattr))
		  {
			$('#rentdata').css('display','block');
		  }
		  else
		  {
		    $('#rentdata').css('display','none');
		  }
 
			checkgallery(productId);
			$('html, body').animate({
			scrollTop: ($('.service-display-left').first().offset().top)
			},500);

	    }
	   }
	 });
}

function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}

  

</script>
<script>
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script>
jQuery(function(){
  jQuery('#dynamic_select').on('change', function () {
	  var url = jQuery(this).val(); // get selected value          
	  if (url) { // require a URL
		  window.location = url; // redirect
	  }
	  return false;
  });
});
</script>
<script>
jQuery(document).ready(function(){

jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
},"<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER__GREATER_OCCASSION_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER__GREATER_OCCASSION_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER__GREATER_OCCASSION_DATE')); ?> <?php endif; ?>");

 jQuery("#booknowid").validate({
    rules: {          
          "rental_time" : {
            required : true
          },
          "return_time" : {
            required : true,
            greaterThan: "#rental_time"
          },
         },
         messages: {
          "rental_time": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_OCCASSION_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_OCCASSION_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_OCCASSION_DATE')); ?> <?php endif; ?>"
          },
          "return_time": {
            required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_RETURN_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_RETURN_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_RETURN_DATE')); ?> <?php endif; ?>"
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#booknowid").valid()) {        
    jQuery('#booknowid').submit();
   }
  });
  
  jQuery("#cartfrm").validate({
    rules: { 
	      "product_size" : {
            required : true
          },         
         },
         messages: {
          "product_size": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php endif; ?>"
          },
         }
 });
 jQuery(".addtocartClass").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
</script>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
</script>
<script type="text/javascript">
  
  function checkqtyavaiablity(dvalue,prodid){

    
      var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('checkqtybysize')); ?>?sizevalue="+dvalue+"&productid="+prodid,
     success:function(res)
     {  
      alert(res);
      if(res==0){
         $('#outofstock').css('display','none');
      }else{
         $('#outofstock').css('display','block');
      }
      
     }
   });
  

  }
  $('.close_popup_btn').click(function(){
$('#rental_time').val('');
$('#return_time').val('');
  })
</script>
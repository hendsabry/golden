<div <?php if($forapp==1): ?> style="display: none" <?php endif; ?>>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
<div class="outer_wrapper">
	<div <?php if($forapp==1): ?> style="display: none" <?php endif; ?>>
 <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 </div>
  <div class="inner_wrap"> 
   
  
   <!-- search-section -->
    <!-- search-section -->
    <div class="page-left-right-wrapper">
     
	 <div class="contact_left">
	 <div class="cont">
	 <h1 class="golden">Goldencages.com</h1>
	 <?php 
	 //print_r($cmscontent);
	 		$mapurl='';
	 if(isset($cmscontent)){
           foreach ($cmscontent as  $value) {
           	# code...
          $mapurl=$value->mapurl;
	 	?>
	 <div class="footer-info-box address-box">
	 		<?php echo $value->content; ?>
</div>
<?php  }} ?>
<div class="cont_form_spc">
<div class="inquiry"><?php echo e((Lang::has(Session::get('lang_file').'.Inquiry_Form')!= '')  ?  trans(Session::get('lang_file').'.Inquiry_Form'): trans($OUR_LANGUAGE.'.Inquiry_Form')); ?> </div>
	  
<?php if(Session::has('message')): ?>
<div class="error">
<?php echo e(Session::get('message')); ?>

</div>
<?php endif; ?>
<form id="cartfrm" name="cartfrm" method="POST" action="<?php echo e(url('/sendcontactenquiry')); ?>">
	 <?php echo e(csrf_field()); ?>

	  <div class="contact_form_box">
	  <div class="contact_form_row">
	  <div class="checkout-form-top"> <?php echo e((Lang::has(Session::get('lang_file').'.Name')!= '')  ?  trans(Session::get('lang_file').'.Name'): trans($OUR_LANGUAGE.'.Name')); ?></div>
	  <div class="checkout-form-bottom">
                  <input type="text" class="t-box" value="" id="Ename" name="Ename" maxlength="70">
                </div>
	  
	  
	  </div>
	  
	  <div class="contact_form_row">
	  <div class="checkout-form-top"> <?php echo e((Lang::has(Session::get('lang_file').'.Email')!= '')  ?  trans(Session::get('lang_file').'.Email'): trans($OUR_LANGUAGE.'.Email')); ?> </div>
	  <div class="checkout-form-bottom">
                  <input type="email" class="t-box" value="" id="Email" name="Email" maxlength="80">
                </div>
	  
	  
	  </div>
	  <div class="contact_form_row">
	  <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Message')!= '')  ?  trans(Session::get('lang_file').'.Message'): trans($OUR_LANGUAGE.'.Message')); ?> </div>
	  <div class="checkout-form-bottom">
                  <textarea name="Message" cols="" rows=""></textarea>
                </div>
	   
	  </div>
	  
	  <div class="contact_form_row">
	  <input type="submit" class="form-btn btn-info-wisitech" value="<?php echo e((Lang::has(Session::get('lang_file').'.Submit')!= '')  ?  trans(Session::get('lang_file').'.Submit'): trans($OUR_LANGUAGE.'.Submit')); ?>" name="submit">
	  
	  
	  </div>
	    </form>
	  </div>

	

</div>

</div>
	 
	 </div>  
	  <div class="contact_right">
	  <?php if(isset($mapurl) && $mapurl!=''): ?>
	  <div class="map_area"><iframe src="<?php echo e($mapurl); ?>" width="100%" height="580" frameborder="0" style="border:0" allowfullscreen></iframe></div>
	  
	  <?php endif; ?>
	  </div> 
    <div>



      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrappercontactus.blade.php -->
  </div>
  <!-- outer_wrapper -->
</div>
 <script type="text/javascript">
 	


jQuery("#cartfrm").validate({
    rules: {       
          "Ename" : {
            required : true,
    
            maxlength: 70
          },  
		  "Email" : {
            required : true,
            maxlength: 80
          },
		  "Message" : {
            required : true,       
            maxlength: 500
          },		  
		    
         },
         messages: {
          "Ename": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NAME')); ?> <?php endif; ?>'
          },
		  "Email": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php endif; ?>' 
          },
		  "Message": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_MESSAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_MESSAGE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_MESSAGE')); ?> <?php endif; ?>' 
          }, 
         },
          submitHandler: function(form) {
                    form.submit();
                }
 });
 

 </script>
 <div <?php if($forapp==1): ?> style="display: none" <?php endif; ?>>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
</div>
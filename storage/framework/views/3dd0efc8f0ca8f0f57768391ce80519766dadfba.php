<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $hallsizetype=$id; ?>
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
 <?php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ ?>
      <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?>
      <?php if(Session::get('searchdata.mainselectedvalue')=='1'){ ?>
      <?php echo $__env->make('includes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?>
</div> <!-- search-section -->

<div class="page-left-right-wrapper">
<?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="page-right-section">
<div class="budget-menu-outer">
<div class="budget-menu">

 

<div class="budget-menu-box"><input type="checkbox" checked="" id="withinbudget" value="withinbudget"/> <label for="withinbudget"><?php echo e((Lang::has(Session::get('lang_file').'.WITHIN_BUDGET')!= '')  ?  trans(Session::get('lang_file').'.WITHIN_BUDGET'): trans($OUR_LANGUAGE.'.WITHIN_BUDGET')); ?></label></div>

<div class="budget-menu-box"><input type="checkbox"  id="abovebudget" value="abovebudget"/> <label for="abovebudget"><?php echo e((Lang::has(Session::get('lang_file').'.ABOVE_BUDGET')!= '')  ?  trans(Session::get('lang_file').'.ABOVE_BUDGET'): trans($OUR_LANGUAGE.'.ABOVE_BUDGET')); ?></label></div>

<div class="budget-menu-box"><input type="checkbox"  id="offers" value="offers"/> <label for="offers"><?php echo e((Lang::has(Session::get('lang_file').'.OFFERS')!= '')  ?  trans(Session::get('lang_file').'.OFFERS'): trans($OUR_LANGUAGE.'.OFFERS')); ?></label></div>

</div>
</div>

<div class="budget-carousel-area">
<div style="clear:both"></div>
<div class="form_title">

	 <?php if($hallsizetype == 4): ?> 
<?php if(Lang::has(Session::get('lang_file').'.HOTEL_HALLS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOTEL_HALLS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.HOTEL_HALLS')); ?> <?php endif; ?>
 <?php endif; ?>

  <?php if($hallsizetype == 5): ?> 
<?php if(Lang::has(Session::get('lang_file').'.Large_Halls')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Large_Halls')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Large_Halls')); ?> <?php endif; ?>
 <?php endif; ?>

 <?php if($hallsizetype == 6): ?> 
<?php if(Lang::has(Session::get('lang_file').'.Small_Halls')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Small_Halls')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Small_Halls')); ?> <?php endif; ?>
 <?php endif; ?>




</div>
<div style="clear:both"></div>
<div class="diamond-area" id="darea">
<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
	  <!------------ 1st to 5th-------------->
		<div class="diamond_wrapper_main">
			
			<?php $i=1; ?>
			<?php $k=count($getbranchhalls); ?>
			<?php if($k<6){ ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $getbranchhalls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row">
					<?php if($k==3){ $cl='category_wrapper3'; }else{ $cl=''; } ?>
					<a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($getallcats->pro_id); ?>">
						<div class="category_wrapper <?php echo e($cl); ?>" style="background:url(<?php echo e(isset($getallcats->pro_img) ? $getallcats->pro_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div></div>
						</div>
					</a>
				</div>
				 <?php $i=$i+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div> 
				<!------------ 6th-------------->
				<?php }elseif($k==6){ ?>
				<?php $j=1; ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $getbranchhalls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			 <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
			 <?php if($j==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($j==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
			 <?php if($j==3){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($j==5){ ?> <div class="row_4of5 rows5row"> <?php } ?>
			  <?php if($j==6){ ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					<a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($getallcats->pro_id); ?>">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_img) ? $getallcats->pro_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div></div>
						</div>
					</a>
			<?php if($j==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($j==2){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($j==4){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($j==5){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($j==6){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $j=$j+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!------------ 7th-------------->
				<?php }elseif($k==7){ ?>
				<?php $l=1; ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $getbranchhalls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
			
			 <?php if($l==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($l==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
			 <?php if($l==3){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($l==6){ ?> <div class="row_4of5 rows5row"> <?php } ?>
			  <?php if($l==7){ ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					<a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($getallcats->pro_id); ?>">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_img) ? $getallcats->pro_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div></div>
						</div>
					</a>
			<?php if($l==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==2){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($l==5){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==6){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($l==7){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $l=$l+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!------------ 8th-------------->
				<?php }elseif($k==8){ ?>
				<?php $l=1; ?>
			
					<div class="diamond_wrapper_inner">
					<?php $__currentLoopData = $getbranchhalls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
				<?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
				<?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
				
			 <?php if($l==1){ $classrd='category_wrapper1'; ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($l==2){ ?>  <div class="row_3of5 rows5row"> <?php } ?> 
			 <?php if($l==4){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($l==6){ ?> <div class="row_3of5 rows5row"> <?php } ?>
			  <?php if($l==8){ $classrd='category_wrapper9'; ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					<a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($getallcats->pro_id); ?>">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_img) ? $getallcats->pro_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?> </div></div>
						</div>
					</a>
			<?php if($l==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==3){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($l==5){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==7){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($l==8){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $l=$l+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!---------- 9th ------------------->
		<?php }elseif($k==9){ ?>
		
		
					<div class="diamond_wrapper_inner">
	  			<?php $i=1; ?>
		  <?php $__currentLoopData = $getbranchhalls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
		  

		 <?php if($i==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		  <?php if($i==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
		   <?php if($i==4){ ?> <div class="row_3of5 rows5row"> <?php } ?> 
		    <?php if($i==7){ ?>  <div class="row_4of5 rows5row"> <?php } ?> 
			<?php if($i==9){ ?>  <div class="row_5of5 rows5row"> <?php } ?> 
            <a href="<?php echo e(url('')); ?>/branchhalldetail/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($shopid); ?>/<?php echo e($branchid); ?>/<?php echo e($getallcats->pro_id); ?>">
              <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_img) ? $getallcats->pro_img : ''); ?>);">
                <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->pro_title) ? $getallcats->pro_title : ''); ?></span></span>
              </span>
            </a>
		 <?php if($i==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($i==3){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($i==6){ ?> <div class="clear"></div> </div> <?php } ?> 
		    <?php if($i==8){ ?> <div class="clear"></div></div> <?php } ?> 
			<?php if($i==9){ ?>  <div class="clear"></div> </div> <?php } ?> 
		 
		    <?php $i=$i+1; ?>
		   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
	  
		  
        </div>
		
		
		
				
			<?php } ?>
				 	
				
		</div>
	  </div>
  </div>
 
<div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
 </div>
<div id="flxslider"></div>


</div> <!--  budget-carousel-area -->

</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->
 
</div> <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  
 
   
<script language="javascript">
jQuery("#abovebudget,#offers,#withinbudget").click(function(){ 	
	jQuery("#darea").hide();
	var w=document.getElementById('withinbudget').checked;
	var a=document.getElementById('abovebudget').checked;
	var o=document.getElementById('offers').checked;

	if(w == true){ var showData_One =1; }else {var showData_One =0; }
	if(a == true){ var showData_Two =1; }else {var showData_Two =0; }
	if(o == true){ var showData_three =1; }else {var showData_three =0; }		

	if((showData_One==0 || showData_One==1) && showData_Two==0 && showData_three==0 )
	{
	jQuery("#darea").show();
	jQuery("#flxslider").html('');

	}

		var hid  = <?php echo request()->hid;?>;
		var id  = <?php echo request()->id;?>;
		var sid  = <?php echo request()->sid;?>;
		var bid  = <?php echo request()->bid;?>; 
		jQuery.ajax({url: "<?php echo e(url('/')); ?>/checkflxslider/"+hid+"/"+id+"/"+sid+"/"+bid+"/"+showData_One+"/"+showData_Two+"/"+showData_three, success: function(result){
			if(showData_Two==0 && showData_three==0 )
			{
			jQuery("#flxslider").html('');
			}
			else
			{
			jQuery("#flxslider").html(result);		
			}	

		jQuery('.flexslider').flexslider({		 
		animation: "slide",
		animationLoop: true,
		animationSpeed: 400,          
		itemWidth: 200,
		itemMargin: 15,
		slideshow: true,
		start: function(slider){
		//console.log('flexlider started');
		}
		});

		}});
	});
 
</script>

<?php if(isset($_REQUEST['isabove']) && $_REQUEST['isabove'] !=''){?>

<script>
jQuery(window).load(function(){


jQuery('#abovebudget').trigger('click');		
 
});

 
</script>

<?php } ?>
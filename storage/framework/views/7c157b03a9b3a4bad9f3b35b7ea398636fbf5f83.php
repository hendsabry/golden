<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
 <?php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ ?>
      <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?>
      <?php if(Session::get('searchdata.mainselectedvalue')=='1'){ ?>
      <?php echo $__env->make('includes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?>
</div> <!-- search-section -->

 

<div class="page-left-right-wrapper">
<?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="page-right-section">

 <!-- budget-menu-outer -->
  
<div class="form_title">
  <?php if($hallsizetype == 4): ?> 
<?php if(Lang::has(Session::get('lang_file').'.HOTEL_HALLS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HOTEL_HALLS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.HOTEL_HALLS')); ?> <?php endif; ?>
 <?php endif; ?>

  <?php if($hallsizetype == 5): ?> 
<?php if(Lang::has(Session::get('lang_file').'.Large_Halls')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Large_Halls')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Large_Halls')); ?> <?php endif; ?>
 <?php endif; ?>

 <?php if($hallsizetype == 6): ?> 
<?php if(Lang::has(Session::get('lang_file').'.Small_Halls')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Small_Halls')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Small_Halls')); ?> <?php endif; ?>
 <?php endif; ?>



 </div>
   



 <div class="budget-menu">
<div class="budget-menu-box"><input onclick="setGetParameter('within', 1)" type="checkbox" <?php if($within==1 || ($within=='' && $above=='' && $offer=='')  ): ?> checked="" <?php endif; ?> id="withinbudget" value="withinbudget"> <label for="withinbudget"><?php if(Lang::has(Session::get('lang_file').'.WITHIN_BUDGET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WITHIN_BUDGET')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.WITHIN_BUDGET')); ?> <?php endif; ?>
</label></div>
<div class="budget-menu-box"><input onclick="setGetParameter('above', 1)"  type="checkbox" <?php if($above==1 ): ?> checked="" <?php endif; ?>  id="abovebudget" value="abovebudget"> <label for="abovebudget"><?php if(Lang::has(Session::get('lang_file').'.ABOVE_BUDGET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ABOVE_BUDGET')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.ABOVE_BUDGET')); ?> <?php endif; ?>
</label></div>
<div class="budget-menu-box"><input onclick="setGetParameter('offer', 1)"  type="checkbox" <?php if($offer==1 ): ?> checked="" <?php endif; ?> id="offers" value="offers"> <label for="offers"><?php if(Lang::has(Session::get('lang_file').'.OFFERS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OFFERS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.OFFERS')); ?> <?php endif; ?>
</label></div>
</div>


<script type="text/javascript">
function setGetParameter(paramName, paramValue) 
{
          var uri = window.location.href;
          var re = new RegExp("([?&])" + paramName + "=1", "i"); 
          var separator = uri.indexOf('?') !== -1 ? "&" : "?";
          if (uri.match(re)) {
          var newUr =  uri.replace(re, '$1');
          var newUr =  newUr.replace('&&&&', '');   
          }
          else {
          var newUr = uri + separator + paramName + "=" + paramValue;
          }
          window.location.href = newUr;
} 
</script> 

 
 


 <?php if($within==1): ?>  

<div class="budget-carousel-area"> 
 
<div class="carousel-row">
    <div class="carousel-heading"><?php if(Lang::has(Session::get('lang_file').'.WITHIN_BUDGET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.WITHIN_BUDGET')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.WITHIN_BUDGET')); ?> <?php endif; ?></div>
    <div class="clear"></div>
        <div class="flexslider carousel">
<?php if(count($shopunderbugetincity)>0): ?> 
          <ul class="slides">
 
  
 <?php $__currentLoopData = $shopunderbugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php
  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
  ?>           
        <li>
        <span class="carousel-product-box">
        <span class="carousel-product-img"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><img src="<?php echo e($bgImg); ?>" /></a></span>
        <span class="carousel-product-cont">
        <span class="carousel-product-name"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></a></span>      
        <span class="carousel-product-view"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><?php if(Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VIEW_DETAILS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.VIEW_DETAILS')); ?> <?php endif; ?> </a></span>
        </span>
        </span>
        </li>     
        
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           
 
          </ul>

          <?php else: ?>
          <div class="no-record"> <?php if(Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.There_is_no_record_available')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.There_is_no_record_available')); ?> <?php endif; ?></div>
          <?php endif; ?>


        </div>
    </div> <!-- carousel-row -->

<?php endif; ?>


 <?php if($above==1 ): ?>

<div class="carousel-row">
<div class="carousel-heading"><?php if(Lang::has(Session::get('lang_file').'.ABOVE_BUDGET')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ABOVE_BUDGET')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.ABOVE_BUDGET')); ?> <?php endif; ?></div>
  <div class="clear"></div>   
    <div class="flexslider carousel">
           <?php if(count($shopabovebugetincity)>0): ?> 
          <ul class="slides">
 
  
 <?php $__currentLoopData = $shopabovebugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php
  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
  ?>           
        <li>
        <span class="carousel-product-box">
        <span class="carousel-product-img"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><img src="<?php echo e($bgImg); ?>" /></a></span>
        <span class="carousel-product-cont">
        <span class="carousel-product-name"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></a></span>      
        <span class="carousel-product-view"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>/?isabovebusget=1"><?php if(Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VIEW_DETAILS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.VIEW_DETAILS')); ?> <?php endif; ?> </a></span>
        </span>
        </span>
        </li>     
        
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           
  
          </ul>
          <?php else: ?>
          <div class="no-record"> <?php if(Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.There_is_no_record_available')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.There_is_no_record_available')); ?> <?php endif; ?> </div>
          <?php endif; ?>
        </div>
  </div>  <!-- carousel-row -->
<?php endif; ?>
 <?php if($offer==1 ): ?> 
<div class="carousel-row">
<div class="carousel-heading"><?php if(Lang::has(Session::get('lang_file').'.OFFERS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.OFFERS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.OFFERS')); ?> <?php endif; ?></div>
  <div class="clear"></div>   
    <div class="flexslider carousel">
          <?php if(count($shopofferincity)>0): ?> 
          <ul class="slides">
 
  
 <?php $__currentLoopData = $shopofferincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php
  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
  ?>           
        <li>
        <span class="carousel-product-box">
        <span class="carousel-product-img"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><img src="<?php echo e($bgImg); ?>" /></a></span>
        <span class="carousel-product-cont">
        <span class="carousel-product-name"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></a></span>      
        <span class="carousel-product-view"><a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>"><?php if(Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.VIEW_DETAILS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.VIEW_DETAILS')); ?> <?php endif; ?> </a></span>
        </span>
        </span>
        </li>     
        
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           
 
          </ul>
         <?php else: ?>
          <div class="no-record"><?php if(Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.There_is_no_record_available')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.There_is_no_record_available')); ?> <?php endif; ?> </div>
          <?php endif; ?>
        </div>
  </div>  <!-- carousel-row -->   
      </div> <!-- budget-carousel-area -->
<?php endif; ?>
 

<?php if($within=='' && $above=='' && $offer==''): ?> 

<div class="diamond-area" id="withinbuget">
<?php if(count($shopunderbugetincity)>0) { ?>

<div class="diamond_main_wrapper">

	  <div class="diamond_wrapper_outer">
		<div class="diamond_wrapper_main">
			<div class="diamond_wrapper_inner">
		 
       <?php  $i=1; $j=1;  $getC = count($shopunderbugetincity); ?>
			<?php $k=count($shopunderbugetincity);    ?>
			<?php $__currentLoopData = $shopunderbugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  

<!-- TILL 5 RECORD -->
  <?php if($getC <=5): ?> 
        <div class="row_<?php echo e($i); ?>of<?php echo e($getC); ?> rows<?php echo e($getC); ?>row">
         <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">

          <?php
                if($getC<=3)
                  {
                  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
                  }
                  else
                  {
                  $bgImg = $getallcats[0]->mc_img;  
                  }

           ?>


            <div class="category_wrapper <?php if($getC!=5 && $getC!=4  && $getC!=2): ?> category_wrapper<?php echo e($i); ?> <?php endif; ?>" style="background:url(<?php echo e(isset($bgImg) ? $bgImg : ''); ?>);">
              <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></div><div class="clear"></div></div>
            </div>
          </a>
        </div>
<!-- TILL 6 RECORD -->
  <?php elseif($getC == 6): ?>
 
          <?php if($i != 3 && $i != 4): ?> 
          <?php if($i==5){ $M=4; } elseif($i==6){ $M=5; }else { $M=$i; } ?>
          <div class="row_<?php echo e($M); ?>of5 rows5row">
          <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
          <div class="category_wrapper" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
          <span class="category_wrapper  <?php if($i==3): ?> category_wrapper2 <?php else: ?> category_wrapper3 <?php endif; ?>" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==4): ?>  <div class="clear"></div>   
         </div>   <?php endif; ?>  
  <?php endif; ?>
<!-- TILL 7 RECORD -->
  <?php elseif($getC == 7): ?>

          <?php if($i != 3 && $i != 4 && $i != 5): ?> 
          <?php if($i==6){ $j = 4;} if($i==7){ $j = 5;}  ?>
          <div class="row_<?php echo e($j); ?>of5 rows5row">
          <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
          <div class="category_wrapper" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
          <span class="category_wrapper  <?php if($i==3): ?> category_wrapper4 <?php elseif($i==4): ?> category_wrapper5 <?php else: ?> category_wrapper6 <?php endif; ?>" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==5): ?>  <div class="clear"></div>   
          </div>   <?php endif; ?>  
  <?php endif; ?>
 <!-- TILL 8 RECORD -->
 <?php elseif($getC == 8): ?>
 
        <?php if($i==1 || $i==8 ): ?>
        <?php if($i==1) { $k = 1;} if($i==8) { $k = 5;}   ?>
       
          <div class="row_<?php echo e($k); ?>of5 rows5row">
          <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
          <div class="category_wrapper category_wrapper1" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></div></div>
          </div>
          </a>   
          </div>
          <?php else: ?>
          <?php if($i==2 ||$i==4 ||$i==6): ?>   <div class="row_3of5 rows5row"> <?php endif; ?>
          <?php if($i==2) { $P = 2;} if($i==3) { $P = 3;}  if($i==4) { $P = 2;}  if($i==5) { $P = 3;}  if($i==6) { $P = 7;}  if($i==7) { $P = 8;}  ?>

          <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
          <span class="category_wrapper category_wrapper<?php echo e($P); ?>" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></span><span class="clear"></span></span>
          </span>
          </a>
          <?php if($i==3 ||$i==5 ||$i==7): ?>  
          <div class="clear"></div>
          </div><?php endif; ?>
 <?php endif; ?>
  <!-- TILL 9 RECORD -->
<?php elseif($getC == 9): ?>

        <?php if($i==1 || $i==9 ): ?>
        <?php if($i==1){$k=1; } if($i==9){$k=5;   } ?>
        <div class="row_<?php echo e($k); ?>of5 rows5row">
        <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
        <div class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
        <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></div></div>
        </div>
        </a>   
        </div>
        <?php elseif($i==2 || $i==3 ): ?>
 
        <?php if($i==2): ?> <div class="row_2of5 rows5row"> <?php endif; ?> 
        <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
        <span class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==3): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==4 || $i==5 || $i==6 ): ?>


        <?php if($i==4): ?> <div class="row_3of5 rows5row"> <?php endif; ?> 
        <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
        <span class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==6): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==7 || $i==8 ): ?>

        <?php if($i==7): ?> <div class="row_4of5 rows5row"><?php endif; ?> 
        <a href="<?php echo e(url('')); ?>/branchlist/<?php echo e($halltype); ?>/<?php echo e($typeofhallid); ?>/<?php echo e($getallcats[0]->mc_id); ?>">
        <span class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats[0]->mc_img) ? $getallcats[0]->mc_img : ''); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats[0]->mc_name) ? $getallcats[0]->mc_name : ''); ?></span><span class="clear"></span></span>
        </span>
        </a>
       
        <?php if($i==8): ?> <div class="clear"></div>
        </div> <?php endif; ?>            
        <?php endif; ?>

<?php endif; ?>

   <!-- END ALL LOOP -->

















 
				 <?php $i=$i+1; $j=$j+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
		</div>
	  </div>
 
  </div>
  
  <?php }else{ ?>	 
	  <div class="diamond_shadow"><strong> <?php echo e((Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= '')  ?  trans(Session::get('lang_file').'.There_is_no_record_available'): trans($OUR_LANGUAGE.'.There_is_no_record_available')); ?> </strong> </div>
<?php } ?>	
<div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>

</div>



<?php endif; ?>





    <!-- budget-carousel-area -->


</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

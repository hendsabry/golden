<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.css" />
<div class="outer_wrapper">

  <div class="vendor_header">

    <div class="inner_wrap">

      <div class="vendor_header_left">

        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($singerDetails->image); ?>" alt="" /></a></div>

      </div>

	  <!-- vendor_header_left -->

      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <!-- vendor_header_right -->

    </div>

  </div>

  <!-- vemdor_header -->

  <div class="common_navbar">

    <div class="inner_wrap">

      <div id="menu_header" class="content">

        <ul>

          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>

  <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>



          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>



          <li><a href="#quotation"><?php echo e((Lang::has(Session::get('lang_file').'.GET_QUOTATION')!= '')  ?  trans(Session::get('lang_file').'.GET_QUOTATION'): trans($OUR_LANGUAGE.'.GET_QUOTATION')); ?></a></li>



        <?php } if(count($allreview) > 0){ ?>

          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>

        <?php } ?>

        </ul>

      </div>

    </div>

  </div>

  <!-- common_navbar -->

  <div class="inner_wrap">

    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>

      <div class="service_detail_row">

        <div class="gallary_detail">

          <section class="slider">

            <div id="slider" class="flexslider">

              <ul class="slides">

                <?php  

                  if(isset($singerDetails->id) && $singerDetails->id!=''){

                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);

                  if(isset($getallimage) && $getallimage!='')

                  {

                    foreach($getallimage as $value){ ?>

                <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>

                <?php } }else{?>

                <li><img src="<?php echo e(str_replace('thumb_','',$singerDetails->image)); ?>" alt=""/></li>

                <?php } } ?>

              </ul>

            </div>

            <div id="carousel" class="flexslider">

              <ul class="slides">

                <?php  

                  if(isset($singerDetails->id) && $singerDetails->id!=''){

                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);

                  foreach($getallimage as $value){ ?>

                <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>

                <?php } } ?>

              </ul>

            </div>

          </section>

        </div>

        <div class="service_detail">

          <div class="detail_title">

            <?php

        $name = 'name'; 

        if(Session::get('lang_file')!='en_lang')

      {

          $name = 'name_ar'; 

        }

        echo $singerDetails->$name;

      ?>

          </div>

          <div class="detail_hall_description">

            <?php

        $address = 'address'; 

        if(Session::get('lang_file')!='en_lang')

      {

          $address = 'address_ar'; 

        }

        echo $singerDetails->$address;

      ?>

          </div>

          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SINGER')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SINGER'): trans($OUR_LANGUAGE.'.ABOUT_SINGER')); ?></div>

          <div class="detail_about_hall">

            <div class="comment more"> <?php

              ?>

              <?php $about='about'?>

              <?php if(Session::get('lang_file')!='en_lang'): ?>

              <?php $about= 'about_ar'; ?>

              <?php endif; ?>

              <?php echo nl2br($singerDetails->$about); ?> </div>

          </div>

          <?php if(isset($singerDetails->city_id) && $singerDetails->city_id!=''){ ?>

          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CTIY')); ?>: <span>

            <?php

        $getcityname = Helper::getcity($singerDetails->city_id); 

        $mc_name = 'ci_name'; 

        if(Session::get('lang_file')!='en_lang')

      {

          $mc_name = 'ci_name_ar'; 

        }

        echo $getcityname->$mc_name; 

      ?>

            </span></div>

          <?php } ?>

        </div>

      </div>

      <!-- service_detail_row -->

      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>

        <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>

		<div class="service-video-area">

          <div class="service-video-cont">

            <?php 

        $mc_name = 'video_description'; 

        if(Session::get('lang_file')!='en_lang')

      {

          $mc_name = 'video_description_ar'; 

        }

        echo $singerDetails->$mc_name; 

      ?>

          </div>

          <div class="service-video-box">

            <iframe class="service-video" src="<?php echo e($singerDetails->video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

          </div>

        </div>

        <!-- service-video-area -->

        <?php } if(count($allreview) > 0){ ?>

        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>

          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>

          <div class="testimonial_slider">

            <section class="slider">

              <div class="flexslider1">

                <ul class="slides">

                  <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                  <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>

                  <li>

                    <div class="testimonial_row">

                      <div class="testim_left">

                        <div class="testim_img"><img src="<?php if(isset($userinfo->cus_pic) && $userinfo->cus_pic!=''){echo @$userinfo->cus_pic;} ?>"></div>

                      </div>

                      <div class="testim_right">

                        <div class="testim_description"><?php if(isset($val->comments) && $val->comments!=''){echo @$val->comments;} ?></div>

                        <div class="testim_name"><?php if(isset($userinfo->cus_name) && $userinfo->cus_name!=''){echo @$userinfo->cus_name;}else{echo 'N/A';} ?></div>

                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>

                      </div>

                    </div>

                  </li>

                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </ul>

              </div>

            </section>

          </div>

        </div>

        <?php } /*else{ ?>

    <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>

    <?php } */?>

        <div class="singer-area singer-area-title">

          <a name="quotation" class="linking">&nbsp;</a>

          <div class="singer-heading"><?php echo e((Lang::has(Session::get('lang_file').'.SINGER_INFORMATION')!= '')  ?  trans(Session::get('lang_file').'.SINGER_INFORMATION'): trans($OUR_LANGUAGE.'.SINGER_INFORMATION')); ?></div>

          <div class="singer-box">

            <div class="singer-image-area"><img src="<?php echo e($singerDetails->image); ?>" alt="" /></div>

            <?php echo Form::open(array('url'=>"insert_singer",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'singer_frm','id'=>'singer_frm')); ?>


            <?php echo e(csrf_field()); ?>


            <div class="singer-form-area">

              <div class="singer-name">

                <?php

        $name = 'name'; 

        if(Session::get('lang_file')!='en_lang')

        {

          $name = 'name_ar'; 

        }

        echo ucfirst($singerDetails->$name);

         ?>

              </div>

              <input type="hidden" name="quote_for" value="singer"/>

              <input type="hidden" name="shop_id" value="<?php echo e($sid); ?>"/>

              <input type="hidden" name="music_id" value="<?php echo e($lid); ?>"/>

              <input type="hidden" name="vendor_id" value="<?php echo e($singerDetails->vendor_id); ?>"/>

              <?php 

        if(Session::has('customerdata.user_id')) 

        {

          $userid  = Session::get('customerdata.user_id');

        }

        ?>

              <input type="hidden" name="user_id" value="<?php if(isset($userid) && $userid!=''){echo $userid;}?>"/>

              <input type="hidden" name="singer_name" value="<?php echo e($singerDetails->$name); ?>"/>

              <div class="checkout-form">

                <div class="checkout-form-row">

                  <div class="checkout-form-cell singer-bottom-space">

                    <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.HALL')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.HALL')); ?> <?php endif; ?></div>

                    <div class="checkout-form-bottom">

                      <input class="t-box" name="hall" id="hall" type="text" maxlength="75" value="<?php echo Input::old('hall'); ?>">

                      <?php if($errors->has('hall')): ?><span class="error"><?php echo e($errors->first('hall')); ?></span><?php endif; ?> </div>

                  </div>

                  <!-- checkout-form-cell -->

                  <div class="checkout-form-cell">

                    <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')); ?></div>

                    <div class="checkout-form-bottom">

                      <select name="occasion" id="occasion">

                        <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Occasion_Type')); ?> <?php endif; ?></option>

                        <?php 

             if(Session::get('lang_file')!='en_lang')

             {

               $getArrayOfOcc = array( '2'=>'مناسبة الزفاف'); 

             }

             else

             {

                $getArrayOfOcc = array( '2'=>'Wedding Occasion'); 

             }             

             foreach($getArrayOfOcc as $key=>$ocval){?>

                        <option value="" disabled="" style="color: #d2cece;"><?php echo e($ocval); ?></option>

                        <?php 

            $setOccasion = Helper::getAllOccasionNameList($key);

            foreach($setOccasion as $val){ ?>

                        <option value="<?php echo e($val->id); ?>"<?php if(isset($occasiontype) && $occasiontype==$val->id){ echo 'selected="selected"'; }?>>

                        <?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>

                        </option>

                        <?php } ?>

                        <?php } ?>

                      </select>

                      <?php if($errors->has('occasion')): ?><span class="error"><?php echo e($errors->first('occasion')); ?></span><?php endif; ?> </div>

                  </div>

                </div>

                <!-- checkout-form-cell -->

              </div>

              <!-- checkout-form-row -->

              <div class="checkout-form-row">

                <div class="checkout-form-cell">

                  <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?></div>

                  <div class="checkout-form-bottom">

                    <select class="checkout-small-box" name="city" id="city">

                      <option value=""><?php if(Lang::has(Session::get('lang_file').'.SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_CITY')); ?> <?php endif; ?></option>

                      

             <?php $getC = Helper::getCountry(); ?>

              <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

              

                      <option value="" disabled="" style="color: #d2cece;"><?php echo e($cbval->co_name); ?></option>

                      

               <?php $getCity = Helper::getCityb($cbval->co_id); ?>              

              <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                 <?php if(isset($city_id) && $city_id==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} ?>

              <?php if($selected_lang_code !='en'): ?>

              <?php $ci_name= 'ci_name_ar'; ?>

              <?php else: ?>

               <?php $ci_name= 'ci_name'; ?>

              <?php endif; ?>   

              

                      <option value="<?php echo e($val->ci_id); ?>" <?php echo e($selectedcity); ?> ><?php echo e($val->$ci_name); ?></option>

                      

               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            

                    </select>

                    <?php if($errors->has('city')): ?><span class="error"><?php echo e($errors->first('city')); ?></span><?php endif; ?> </div>

                </div>

                <!-- checkout-form-cell -->

                <div class="checkout-form-cell">

                  <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LOCATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.LOCATION')); ?> <?php endif; ?></div>

                  <div class="checkout-form-bottom">

                    <input class="t-box" type="text" name="location" maxlength="75" id="location" value="<?php echo Input::old('location'); ?>">

                    <?php if($errors->has('location')): ?><span class="error"><?php echo e($errors->first('location')); ?></span><?php endif; ?> </div>

                </div>

                <!-- checkout-form-cell -->

              </div>

              <!-- checkout-form-row -->

              <div class="checkout-form-row">

                <div class="checkout-form-cell">

                  <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.DURATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DURATION')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DURATION')); ?> <?php endif; ?></div>

                  <div class="checkout-form-bottom">

                    <input class="t-box" name="duration" id="duration" type="text" maxlength="2" value="<?php echo Input::old('duration'); ?>" style="max-width:125px;" onkeypress="return isNumber(event)" >

                    <?php if($errors->has('duration')): ?><span class="error"><?php echo e($errors->first('duration')); ?></span><?php endif; ?> </div>

                </div>

                <!-- checkout-form-cell -->

                <div class="checkout-form-cell">

                  <div class="form-cell-date date-time">

                    <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.DATE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.DATE')); ?> <?php endif; ?> / <?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TIME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?> <?php endif; ?></div>

                    <div class="checkout-form-bottom">

                      <input class="t-box cal-t datetimepicker" autocomplete="off" name="date" id="date" value="<?php echo Input::old('date'); ?>" type="text" readonly="">

                      <?php if($errors->has('date')): ?><span class="error"><?php echo e($errors->first('date')); ?></span><?php endif; ?> </div>

                  </div>

                  <!--<div class="form-cell-time">

                      <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.TIME')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.TIME')); ?> <?php endif; ?></div>

                      <div class="checkout-form-bottom">                        

            <input class="t-box sel-time" name="time" id="times" value="<?php echo Input::old('time'); ?>" type="text">

            <?php if($errors->

                  has('time')): ?><span class="error"><?php echo e($errors->first('time')); ?></span><?php endif; ?> </div>

              </div>

              --> </div>

            <!-- checkout-form-cell -->

          </div>

          <!-- checkout-form-row -->

          <div class="checkout-form-row">

            <div class="checkout-form-cell cell-full-width">

              <div class="checkout-form-top"><?php if(Lang::has(Session::get('lang_file').'.COMMENTS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COMMENTS')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.COMMENTS')); ?> <?php endif; ?> </div>

              <div class="checkout-form-bottom">

                <textarea class="text-area" name="comments" id="comments"><?php echo Input::old('comments'); ?></textarea>

                <?php if($errors->has('comments')): ?><span class="error"><?php echo e($errors->first('comments')); ?></span><?php endif; ?> </div>

            </div>

            <!-- checkout-form-cell -->

          </div>

          <!-- checkout-form-row -->

          <div class="form-button-line">

            <input type="submit" name="submit" class="form-btn" value="<?php if(Lang::has(Session::get('lang_file').'.ASK_FOR_A_QUOTE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ASK_FOR_A_QUOTE')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.ASK_FOR_A_QUOTE')); ?> <?php endif; ?>" />

          </div>

        </div>

        <?php echo Form::close(); ?>


        <!-- checkout-form -->

      </div>

      <!-- singar-form-area -->

    </div>

    <!-- singer-box -->

  </div>

  <!-- singer-area -->

</div>

<!-- service-mid-wrapper -->

<?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- other_serviceinc -->

</div>

<!-- detail_page -->

</div>

<!-- innher_wrap -->

</div>

<!-- outer_wrapper -->

<div class="othrserv_overl"></div>

<script type="text/javascript">

function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if ( charCode > 31 &&

            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.

            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.

            (charCode < 48 || charCode > 57))

        {



            return false;

        }

        return true;

    } 

 </script>

<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script src="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>

<script> 



var checkPastTime = function(inputDateTime) {

    if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {

        var current = new Date();

 

        //check past year and month

        if (inputDateTime.getFullYear() < current.getFullYear()) {

            $('.datetimepicker').datetimepicker('reset');

            alert("Sorry! Past date time not allow.");

        } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {

            $('.datetimepicker').datetimepicker('reset');

            alert("Sorry! Past date time not allow.");

        }

 

        // 'this' is jquery object datetimepicker

        // check input date equal to todate date

        if (inputDateTime.getDate() == current.getDate()) {

            if (inputDateTime.getHours() < current.getHours()) {

                $('.datetimepicker').datetimepicker('reset');

            }

            this.setOptions({

                minTime: current.getHours() + ':00' //here pass current time hour

            });

        } else {

            this.setOptions({

                minTime: false

            });

        }

    }

};

 

var currentYear = new Date();

$('.datetimepicker').datetimepicker({

    format:'d M Y g:i A',

    minDate : 0,

    yearStart : currentYear.getFullYear(), // Start value for current Year selector

    onChangeDateTime:checkPastTime,

    onShow:checkPastTime

});









 /* 

jQuery(window).load(function()

{

  jQuery("body").on('mouseover', '.datetimepicker', function() {  

    var today = '2018-11-09';

	  jQuery(this).datetimepicker({

	  ampm: true, // FOR AM/PM FORMAT

	  format : 'd M Y g:i A',

	  minDate : 0,

    minTime: 0 

   });

  });

})

*/

</script>

<style type="text/css">

a.morelink {

  text-decoration:none;

  outline: none;

}

.morecontent span {

  display: none;

}

</style>

<script type="text/javascript">

jQuery(document).ready(function(){

 //jQuery('#date').datepicker({format: "dd/mm/yyyy"}); 

 jQuery("#singer_frm").validate({

    rules: {          

          "hall" : {

            required : true

          },  

          "occasion" : {

            required : true

          },  

          "city" : {

            required : true

          },

          "location" : {

            required : true

          }, 

      "duration" : {

            required : true,

             number: true

          },  

      "date" : {

            required : true

          },  

      "time" : {

            required : true

          }, 

          /*"comments" : {

            required:true

          },*/     

         },

         messages: {

          "hall": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_HALL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_HALL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_HALL')); ?> <?php endif; ?>"

          },

          "occasion": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_OCCASION')); ?> <?php endif; ?>"

          }, 

          "city": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_CITY')); ?> <?php endif; ?>"

          },

          "location": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_LOCATION')); ?> <?php endif; ?>"

          },

      "duration": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DURATION')); ?> <?php endif; ?>"

          },

      "date": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DATE')); ?> <?php endif; ?>"

          },

      "time": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_TIME')); ?> <?php endif; ?>"

          },

          /*"comments": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_COMMENTS_QUERIES')); ?> <?php endif; ?>"

          }, */     

         }

 });

 jQuery(".btn-info-wisitech").click(function() {

   if(jQuery("#singer_frm").valid()) {        

    jQuery('#singer_frm').submit();

   }

  });

});

$(document).ready(function() 

{

  var showChar = 400;

  var ellipsestext = "...";

  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";

  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";

  $('.more').each(function() 

  {

    var content = $(this).html();

    if(content.length > showChar) 

  {

      var c = content.substr(0, showChar);

      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      $(this).html(html);

    }

  });



  $(".morelink").click(function(){

    if($(this).hasClass("less")) {

      $(this).removeClass("less");

      $(this).html(moretext);

    } else {

      $(this).addClass("less");

      $(this).html(lesstext);

    }

    $(this).parent().prev().toggle();

    $(this).prev().toggle();

    return false;

  });

});

 

</script>

<div class="action_popup">

  <div class="action_active_popup">

    <div class="action_content"><?php echo e(Session::get('message')); ?></div>

    <div class="action_btnrow">

      <input type="hidden" id="delid" value=""/>

      <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>

  </div>

</div>

<?php if(Session::has('message')): ?>

<script type="text/javascript">

$(document).ready(function()

{

 $('.action_popup').fadeIn(500);

 $('.overlay').fadeIn(500);

});

</script>

<?php endif; ?>

<script type="text/javascript">

$('.status_yes').click(function()

{

 $('.overlay, .action_popup').fadeOut(500);

});

</script>


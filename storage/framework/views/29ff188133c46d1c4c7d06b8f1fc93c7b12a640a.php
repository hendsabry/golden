<!doctype html>

<html><head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 



 <meta name="_token" content="<?php echo csrf_token(); ?>"/>

	 <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />

	





<title><?php if(Lang::has(Session::get('lang_file').'.GOLDEN_CAGES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.GOLDEN_CAGES')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.GOLDEN_CAGES')); ?> <?php endif; ?></title>

<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(url('/')); ?>/themes/images/favicon.ico">

<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 

<link href="<?php echo e(url('/')); ?>/themes/css/reset.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/common-style.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/stylesheet.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/demo.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/slider/css/flexslider.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/user-my-account.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/interface.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/arabic.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/interface-media.css" rel="stylesheet" />

<link href="<?php echo e(url('/')); ?>/themes/css/diamond.css" rel="stylesheet" />





<!--  jQuery -->

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>



<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->

<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />





<!--<script src="<?php echo e(url('/')); ?>/themes/js/jquery-lib.js"></script>

<script src="<?php echo e(url('/')); ?>/themes/js/height.js"></script>-->

<script src="<?php echo e(url('/')); ?>/themes/js/jquery.flexslider.js"></script>

<script src="<?php echo e(url('/')); ?>/themes/js/modernizr.js"></script>

<script src="<?php echo e(url('/')); ?>/themes/js/jquery.mousewheel.js"></script>

<script src="<?php echo e(url('/')); ?>/themes/js/demo.js"></script>

<script src="<?php echo e(url('/')); ?>/themes/js/froogaloop.js"></script>

<script src="<?php echo e(url('/')); ?>/themes/js/jquery.easing.js"></script>







<!-- maping js responisve end -->



<!--  Date picker script -->

<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">   -->

<link href="<?php echo e(url('/')); ?>/themes/css/bootstrap_tab.css" rel="stylesheet" />

<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  -->

	<script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script> 

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  





 <script src="<?php echo e(url('/')); ?>/themes/js/bootstrap-datepicker.js"></script>





<!----- end date picker----->

<!------add from validation lib ---->

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<!--<script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/jquery.form-validator.js"></script>-->

<!-----end from validation lib------>

<script language="javascript">

  function isNumberKey(evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {

            return false;

        } else {

            return true;

        }

    }

</script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138258734-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-138258734-1');
</script>

</head>

<?php  if(Session::get('lang_file')=='ar_lang'){ $sitecss="arabic"; }else{ $sitecss=""; } ?>

<body class="<?php echo e($sitecss); ?>">

<div class="popup_overlay"></div>



 <?php

 

$sesionData = session()->all();

 

 if(!isset($sesionData['searchdata']))

 {

    $urlOk = 0;

	$pageid = request()->id;

	//echo $pageid;die;

    if(Request::is('/')) {   $urlOk = 1; } 

    if(Request::is('login-signup')) { $urlOk = 1; } 

    if(Request::is('userforgotpassword')) { $urlOk = 1;  }

    if(Request::is('about-us')) { $urlOk = 1;    }

    if(Request::is('testimonials')) { $urlOk = 1;   }

    if(Request::is('occasions')) { $urlOk = 1;   }

    if(Request::is('contact-us')) { $urlOk = 1;  }

	if(Request::is('elect-barcode')) { $urlOk = 1;  }

    if(Request::is('privacy-policy')) { $urlOk = 1;  }

    if(Request::is('return-policy')) { $urlOk = 1;  }

    $GetIDD = request()->id;



    $cus_id = request()->cus_id;

    $product_type = request()->product_type;

    $product_sub_type = request()->product_sub_type;

    $order_id = request()->order_id;

    $category_id = request()->category_id;

    $product_id = request()->product_id;

    $merchant_id = request()->merchant_id;

    $shop_id = request()->shop_id;



    $shopid = request()->shopid;

    $orderid = request()->orderid;

    $vendorid = request()->vendorid;

 $uidx = request()->uid;

 $evid = request()->id;

$eid = request()->eid; 

  if(Request::is("member_forgot_pwd_email/$eid") ||  Request::is("occasion-details/$uidx") || Request::is('my-account') || Request::is('my-account-profile') || Request::is('my-account-ocassion') || Request::is('my-account-studio') || Request::is('my-account-security') || Request::is('my-account-wallet') || Request::is('my-account-review') || Request::is('my-request-a-quote') || Request::is('change-password') || Request::is("order-details/$GetIDD") || Request::is("serviceorder/$cus_id/$product_type/$product_sub_type/$order_id/$category_id/$product_id/$merchant_id/$shop_id") || Request::is("reviewdisplay/$shopid/$orderid/$vendorid") || Request::is("requestaquoteview/$GetIDD") || Request::is("ocassion-more-image/$GetIDD") || Request::is("elect-barcode/$pageid") || Request::is("invitation-attended-list/$evid")) 

  { 

    $urlOk = 1;  

  }



 

    if($urlOk ==0)

    {

        $url = url('/');

 



header("Location: $url");

die;

    }





 } 



 ?>
<?php $__env->startSection('content'); ?>
    <div class="home-site">
        <div class="container">
            <h3>HALLS</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/city-hall.png" alt="" />
                            </a>
                            <a href="" class="tite">Hotel Halls</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/seats-at-the-hall-1.png" alt="" />
                            </a>
                            <a href="" class="tite">Hotel Halls</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/seats-at-the-hall.png" alt="" />
                            </a>
                            <a href="" class="tite">Hotel Halls</a>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Food</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/tray.png" alt="" />
                            </a>
                            <a href="" class="tite">Buffet</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/cupcake.png" alt="" />
                            </a>
                            <a href="" class="tite">Desserts</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/dates-0.png" alt="" />
                            </a>
                            <a href="" class="tite">Dates</a>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Occasion Coordinator</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/wedding-arch.png" alt="" />
                            </a>
                            <a href="" class="tite">Kosha</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/umbrella.png" alt="" />
                            </a>
                            <a href="" class="tite">Photography</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/receptionist.png" alt="" />
                            </a>
                            <a href="" class="tite">Hospitality</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/bouquet.png" alt="" />
                            </a>
                            <a href="" class="tite">Roses</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/fireworks.png" alt="" />
                            </a>
                            <a href="" class="tite">Specia Events</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/love-letter.png" alt="" />
                            </a>
                            <a href="" class="tite">E-Invitations</a>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Beauty And Elegance</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/beauty-center.png" alt="" />
                            </a>
                            <a href="" class="tite">Beauty Center</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/spa.png" alt="" />
                            </a>
                            <a href="" class="tite">Beauty Center</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/makeup-artist.png" alt="" />
                            </a>
                            <a href="" class="tite">Makeup Artist</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/makeup-products.png" alt="" />
                            </a>
                            <a href="" class="tite">Makeup</a>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Reviving Concerts</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/singers.png" alt="" />
                            </a>
                            <a href="" class="tite">Singers</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/popular-bands.png" alt="" />
                            </a>
                            <a href="" class="tite">Popular Bands</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/sound-system.png" alt="" />
                            </a>
                            <a href="" class="tite">Sound systems</a>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Clinics</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/dental.png" alt="" />
                            </a>
                            <a href="" class="tite">Dental & Skin</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/cosmetics-laser.png" alt="" />
                            </a>
                            <a href="" class="tite">Cosmetics & Laser</a>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Shopping</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/dress.png" alt="" />
                            </a>
                            <a href="" class="tite">Dresses</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/abaya.png" alt="" />
                            </a>
                            <a href="" class="tite">Abaya</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/perfume.png" alt="" />
                            </a>
                            <a href="" class="tite">Perfums</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/jewelry.png" alt="" />
                            </a>
                            <a href="" class="tite">Jewelry</a>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Travel agency</h3>
            <div class="box p-15">
                <div class="display-flex">
                    <div class="">
                        <div class="item">
                            <a href="" class="img">
                                <img src="<?php echo e(url('newWebsite')); ?>/images/travel-agencies.png" alt="" />
                            </a>
                            <a href="" class="tite">Travel Agency</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="head-Occasions">
                <h2>#Occasions</h2>
                <a href="">View All</a>
            </div>
            <div id="Occasions" class="owl-carousel" dir="ltr">
                <div class="item-occasions">
                    <img src="<?php echo e(url('newWebsite')); ?>/images/images-copy.png" alt="" />
                    <div class="title-caption">
                        <p>Name City</p>
                    </div>
                </div>
                <div class="item-occasions">
                    <img src="<?php echo e(url('newWebsite')); ?>/images/images-copy.png" alt="" />
                    <div class="title-caption">
                        <p>Name City</p>
                    </div>
                </div>
                <div class="item-occasions">
                    <img src="<?php echo e(url('newWebsite')); ?>/images/images-copy.png" alt="" />
                    <div class="title-caption">
                        <p>Name City</p>
                    </div>
                </div>
                <div class="item-occasions">
                    <img src="<?php echo e(url('newWebsite')); ?>/images/images-copy.png" alt="" />
                    <div class="title-caption">
                        <p>Name City</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
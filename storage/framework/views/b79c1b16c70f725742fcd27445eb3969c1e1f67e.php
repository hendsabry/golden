<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
            <div class=" col-sm-9 col-xs-12 right-side-bar">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="right_col" role="main">
      <div id="appUser">
        <div class="page-title">
          <div class="mainView-details bordr-bg">
            <div class="row">
            <div class="col-sm-6">
              <div class="title_left">
                <h3 class="padd"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_STATUS_DETAILS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_STATUS_DETAILS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_STATUS_DETAILS')); ?></h3>
                <p><?php echo e($users->cus_name); ?></p>
                <p class="paddNone"><?php echo date('d,M Y', strtotime($users->created_at));?></p>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="user-view-info">
                <div class="userImage marBotm">
                  <figure><?php if(isset($users->cus_pic) && $users->cus_pic != ''): ?>
                      <img src="<?php echo e(url('')); ?><?php echo e($users->cus_pic); ?>">
                    <?php else: ?>
                        <img src="<?php echo e(url('')); ?>/public/assets/adimage/user-dummy.png">
                    <?php endif; ?>  
                  </figure>
                   <a href="/admin/user_management" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a>
                </div>
                <div class="view-userDetails">
                  <div class="view-userDetails-inner">
                    <h3><?php echo e($users->cus_name); ?></h3>
                    <p class="username"><?php echo e($users->email); ?></p>
                    <div class="status">
                      <?php if($users->cus_status == 1): ?>
                      <div>
                      <span class="active"></span>
                      <h2 ><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE')); ?> </h2>
                      
                      </div>
                      <?php else: ?>
                      <div>
                      <span class="Inactive"></span>
                      <h2 ><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE')); ?> </h2>
                      </div>
                      <?php endif; ?>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>          
        </div>
        <div class="clearfix"></div>
        <div class="viewListtab">
            <div class="userinformation bordr-bg paddzero small-width-column2">
                  <ul class="paddleftright">
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_FULL_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FULL_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FULL_NAME')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo e($users->cus_name); ?></p></div>
                    </li>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_E-MAIL_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_E-MAIL_ADDRESS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_E-MAIL_ADDRESS')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo e($users->email); ?></p></div>
                    </li>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php if($users->cus_phone): ?><?php echo e($users->country_code); ?> - <?php echo e($users->cus_phone); ?> <?php else: ?> N/A <?php endif; ?></p></div>
                    </li>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AGE')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo((date('Y') - date('Y',strtotime($users->dob))));?></p></div>
                    </li>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_BIRTH')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo(date('M d, Y', strtotime($users->dob)));?></p></div>
                    </li>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_LOCATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LOCATION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_LOCATION')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?> <?php if(isset($users->getCity[0]->ci_name_ar) && isset($users->getCountry[0]->co_name_ar)): ?> <?php echo e($user->getCity[0]->ci_name_ar); ?> , <?php echo e($users->getCountry[0]->co_name_ar); ?> <?php else: ?> N/A <?php endif; ?> <?php else: ?> <?php if(isset($users->getCity[0]->ci_name) && isset($users->getCountry[0]->co_name)): ?><?php echo e($users->getCity[0]->ci_name); ?> , <?php echo e($users->getCountry[0]->co_name); ?> <?php else: ?> N/A <?php endif; ?> <?php endif; ?></p></div>
                    </li>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_LAST_LOGGED_IN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LAST_LOGGED_IN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_LOGGED_IN')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php if($users->last_log_in != ''): ?><?php echo e($users->last_log_in); ?> <?php else: ?> N/A <?php endif; ?></p></div>
                    </li>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_WALLET_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_WALLET_AMOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_WALLET_AMOUNT')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo e($users->wallet); ?></p></div>
                    </li>
                      <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php if($users->cus_status == 1): ?> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE')); ?> <?php else: ?> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE')); ?> <?php endif; ?></p></div>
                    </li>
                    <?php if(isset($users->cus_pic) && $users->cus_pic !=''): ?>
                    <li class="row">
                      <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NATIONAL_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NATIONAL_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NATIONAL_ID')); ?></label></div>
                      <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><a href="<?php echo e($users->cus_pic); ?>" target="_blank"><img src="<?php echo e($users->cus_pic); ?>" width="300" height="300"> </a> </p></div>
                    </li>
                    <?php endif; ?>
                  </ul>
                  
                </div>
            </div>
      </div>
    </div>
              </div></div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script> 
</body>

    <!-- END BODY -->
</html>
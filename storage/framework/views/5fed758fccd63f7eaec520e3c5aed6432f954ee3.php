<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $buffet_leftmenu =1; ?>
<!--start merachant-->
<div class="merchant_vendor"> <?php
  $id = request()->id;
  $sid = request()->sid;
  $opid = request()->opid;
  $cusid = request()->cusid;
  $oid = request()->oid;
  $hid = request()->hid;
  $itemid=request()->itemid;
  $getCustomer = Helper::getuserinfo($cusid); 
  $shipaddress       = Helper::getgenralinfo($oid);
$cityrecord=Helper::getcity($shipaddress->shipping_city);
  //$getorderedproduct=Helper::getorderedproduct($opid);
  
  $getorderedproductdate=Helper::getorderedproduct($oid);
  $getorderedproduct=Helper::getfoodorderedproducttotal($oid,'buffet');
  
  ?>
  <!--left panel-->
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!--left panel end-->
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order_Detail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order_Detail')); ?> <?php endif; ?> <a href="<?php echo e(url('/')); ?>/buffet-orders/<?php echo e($id); ?>/<?php echo e($sid); ?>/<?php echo e($itemid); ?>" class="order-back-page"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BACK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?> <?php endif; ?></a> </h5>
      </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} else { echo 'N/A'; }?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <?php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){ ?>
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDRESS')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} ?> <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="<?php echo e(url('')); ?>/themes/images/placemarker.png" /></a></div>
                  </div>
                  <?php } ?>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?></label>
                    <div class="info100" > <?php 
                      $ordertime=strtotime($getorderedproductdate->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      ?> </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} ?></div>
                  </div>
				   <?php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ ?>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.transaction_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.transaction_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.transaction_id')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} ?></div>
                  </div>
				    <?php } ?>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD')); ?> <?php endif; ?></label>
                    <div class="info100"> <?php if(isset($shipaddress->shipping_id) && $shipaddress->shipping_id!='')
                      { 
                      $getName = Helper::getShippingMethodName($shipaddress->shipping_id);
                      echo $getName;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      ?> </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE')); ?> <?php endif; ?></label>
                    <div class="info100"> <?php if(isset($shipaddress->shipping_charge) && $shipaddress->shipping_charge!='' && $shipaddress->shipping_charge!='0')
                      {
                      echo $shipaddress->shipping_charge;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      ?> </div>
                    <!-- box -->
                  </div>
                 <?php $getsearchedproduct = Helper::searchorderedDetails($oid);
 if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ ?>
                    <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONDATE')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                      echo date('d M Y', strtotime($getsearchedproduct->occasion_date));
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      ?> </div>
                  </div>
                  <?php } ?>
                </div>
 


            <?php if($hallName!=''): ?>
            <div class="form_row">
            <div class="noneditbox">
            <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.HALL_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HALL_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.HALL_NAME')); ?> <?php endif; ?></label>
            <div class="info100"><?php echo e($hallName); ?></div>
            <!-- box -->
            </div>

            <div class="noneditbox box-right">
            <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.HALL_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HALL_LOCATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.HALL_LOCATION')); ?> <?php endif; ?></label>
            <div class="info100"><?php echo e($hallLocation); ?> </div>
            </div>

            </div>
            <?php endif; ?>

              </div>
              <!-- hall-od-top -->
              <?php if(count($getbuffetfood_item)>0){ ?>
              <div class="style_head"><?php if(Lang::has(Session::get('mer_lang_file').'.PRODUCT_INFORMATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PRODUCT_INFORMATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PRODUCT_INFORMATION')); ?> <?php endif; ?></div>
              <?php $k=0;  $basetotal = 0; ?>
              <?php $i=1; 

              ?>
              <?php $__currentLoopData = $getbuffetfood_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bodymeasurement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php 
             
              $basetotal            = ($basetotal+$bodymeasurement->price);
               $pid=$bodymeasurement->external_food_dish_id;
              $mid=$bodymeasurement->external_food_menu_id;
              $bid=$bodymeasurement->category_id;
              $conid=$bodymeasurement->container_id;
              $getorderedproducttbl=Helper::getorderedfromproducttbl($pid);
              $productproductimage=Helper::getproductimage($pid);
              
              $getcontainerinformation=Helper::getcontainerinfo($conid);
              
              //print_r($getcontainerinformation); die;
              $externalfoodmainmenu=Helper::getexternalfoodmainmenu($mid,$bid);
              
              if(Session::get('mer_lang_code')!='en') {  $ci_name='attribute_title_ar'; } else{ $ci_name='attribute_title';}
              if(Session::get('mer_lang_code')!='en') {  $con_name='option_title_ar'; } else{ $con_name='option_title';}
              ?>
              <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                <div class="style_area">
                  <div class="style_box_type">
                    <div class="sts_box">
                      <div class="style_left">
                        <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_DISH_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DISH_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DISH_NAME')); ?> <?php endif; ?></div>
                        <div class="style_label_text"> <?php echo e(isset($getorderedproducttbl) ? $getorderedproducttbl : ''); ?> </div>
                      </div>
                     
                      <div class="style_left">
                        <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Dish_Image')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Dish_Image')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Dish_Image')); ?> <?php endif; ?></div>
                        <div class="style_label_text"> <?php if($productproductimage->pro_Img): ?> <img src="<?php echo e($productproductimage->pro_Img); ?>" alt="" width="150"> <?php else: ?>
                          No image
                          
                          <?php endif; ?></div>
                      </div>
                       <div class="style_left">
                        <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?></div>
                        <div class="style_label_text">SAR <?php echo e(number_format($bodymeasurement->price,2)); ?> </div>
                      </div>
                    </div>
                    <div class="sts_box">
                      <div class="style_left">
                        <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.CONTAINER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.CONTAINER')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.CONTAINER')); ?> <?php endif; ?></div>
                        <div class="style_label_text"> <?php echo e(isset($getcontainerinformation->$con_name) ? $getcontainerinformation->$con_name : ''); ?> </div>
                      </div>
                      <div class="style_right">
                        <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_QUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_QUANTITY')); ?> <?php endif; ?></div>
                        <div class="style_label_text"><?php echo e(isset($bodymeasurement->quantity) ? $bodymeasurement->quantity : ''); ?></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php $k++; $i++; ?>
              
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <div class="merchant-order-total-area">
                <div class="merchant-order-total-line"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')); ?>: &nbsp;
                  <?php $totalvatprice=$basetotal;
                     $vatamount = Helper::calculatevat($shipaddress->order_id,$totalvatprice);
                     echo 'SAR '.number_format(($vatamount),2);
                ?>
                   </div>
                <!-- merchant-order-total-line -->
                <div class="merchant-order-total-line"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: &nbsp;
                  <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$vatamount),2);
				 } ?>
                </div>
                <!-- merchant-order-total-line -->
              </div>
              <?php } ?> </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
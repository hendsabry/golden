<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Abayabig_leftmenu =1; ?>
<div class="merchant_vendor">
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
  <div class="inner">
     <header>
      <?php if($autoid==''): ?>
      <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ADD_Fabric')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADD_Fabric')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADD_Fabric')); ?> <?php endif; ?> </h5>

        <?php else: ?>
          <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.UPDATE_Fabric')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATE_Fabric')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATE_Fabric')); ?> <?php endif; ?> </h5>

        <?php endif; ?>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
		   <div class="global_area">
      <div class="row">

          <div class="col-lg-12">
            <div class="box">
		
        <form name="form1" id="menucategory" method="post" action="<?php echo e(route('store-abayatailorfabrics')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

				  
                  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
                      <span class="english"><?php echo lang::get('mer_en_lang.name'); ?></span>
                      <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.name'); ?> </span>
                    </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="<?php echo e(isset($categorysave->fabric_name) ? $categorysave->fabric_name : ''); ?>"  class="english"  maxlength="150" />
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" value="<?php echo e(isset($categorysave->fabric_name_ar) ? $categorysave->fabric_name_ar : ''); ?>" class="arabic ar" name="mc_name_ar"  id="mc_name_ar"   type="text"  maxlength="150">
                      </div>
                    </div>
                    </div>
                  </div>
				  
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english"><?php echo lang::get('mer_en_lang.Image'); ?></span>
<span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Image'); ?> </span>
 </label>
                    <div class="info100">
                     
				  <div class="input-file-area">
                  <label for="company_logo">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
   <div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div>
                  </div>
                  </label>
                  <input id="company_logo" data-lbl="file_value1" name="proimage" class="info-file" type="file">
                </div>
                <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                  <?php if(isset($categorysave->image) && $categorysave->image !=''): ?>
                 <div class="form-upload-img">
                       <img src="<?php echo e(isset($categorysave->image) ? $categorysave->image : ''); ?>" >
                     </div>
                     <?php endif; ?> 
                    </div>
                    </div>
                  </div>


    
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english"><?php echo lang::get('mer_en_lang.MER_PRICE'); ?></span>
<span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRICE'); ?> </span>
 </label>
                    <div class="info100">
                      
                        <input type="text" class="notzero xs_small" name="price" id="price" onkeypress="return isNumber(event)" value="<?php echo e(isset($categorysave->price) ? $categorysave->price : ''); ?>"  maxlength="9" />
                        <input type="hidden" name="id" value="<?php echo e(isset($id) ? $id : ''); ?>">
                        <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
                        <input type="hidden" name="autoid" value="<?php echo e(isset($autoid) ? $autoid : ''); ?>">
                     
                    </div>
                    </div>
                  </div>

 
             
				 <!-- <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
                    <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span>
                    <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span>
                     </label>
                    <div class="info100">
        					   <div class="english"><textarea name="description" maxlength="500" id="description"><?php echo e(isset($categorysave->fabric_desc) ? $categorysave->fabric_desc : ''); ?></textarea></div>
                      <div class="arabic ar"><textarea name="description_ar" maxlength="500" id="description_ar"><?php echo e(isset($categorysave->fabric_desc_ar) ? $categorysave->fabric_desc_ar : ''); ?></textarea></div>
                      
                    </div>
                    </div>
               </div>-->
                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  </div></div>
			   </form>
		
			 
			</div></div></div></div>
		

  </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>
 $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#menucategory").validate({

                  ignore: [],
                  rules: {
                
                       mc_name: {
                       required: true,
                      },
                       mc_name_ar: {
                       required: true,
                      },
 
                       price: {
                       required: true,
                      },
                       
    

                       <?php if(isset($categorysave->image)!=''): ?>  
                        proimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       <?php else: ?>
                        proimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      <?php endif; ?>


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             

                  mc_name: {
                     required:  "<?php echo lang::get('mer_en_lang.PLEASE_ENTER_NAME'); ?>",

                      },   

                          mc_name_ar: {
                             required:  "<?php echo lang::get('mer_ar_lang.PLEASE_ENTER_NAME'); ?>",
                 
                      },   

             

                       price: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?> ",
                      },

                        quantity: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY')); ?> <?php endif; ?> ",
                      },

                    
  

                  proimage: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },   


                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  <?php if($mer_selected_lang_code !='en'): ?>
                   
                     if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                       if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                        $('.arabic_tab').trigger('click');
                           

                    }
                     
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                     

                    }
                    
                   
                     
                      if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                    
                    
          <?php else: ?>


                       if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                        $('.arabic_tab').trigger('click');
                           

                    }
                     
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                    
                   
                     
                      if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    
                <?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script> 
 
 

 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>
    <?php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='')
        {
          $Current_Currency = 'SAR';
        }
       // $cururl = request()->segment(count(request()));
    ?>
    <?php $cid=$fooddateshopdetails[0]->city_id;
          $getCity = Helper::getcity($cid);
          if(Session::get('lang_file') !='en_lang'){
          $city_name= 'ci_name_ar';
          }else{
          $city_name= 'ci_name';
          }

    ?>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2>Beauty Shop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php echo e($fooddateshopdetails[0]->mc_name); ?></h2>
                                <h2><?php echo e($fooddateshopdetails[0]->address); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i><?php echo e($getCity->$city_name); ?></p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php echo e($fooddateshopdetails[0]->mc_discription); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <?php if($fooddateshopdetails[0]->google_map_address!=''){  $lat=$fooddateshopdetails[0]->latitude;   $long=$fooddateshopdetails[0]->longitude;    ?>
                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        <?php }  ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $fooddateshopreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($customerreview->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e(isset($customerreview->cus_name) ? $customerreview->cus_name : ''); ?></h2>
                                    <div class="stars">
                                        <img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png">
                                    </div>
                                    <p><?php echo e(isset($customerreview->comments) ? $customerreview->comments : ''); ?></p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                        <?php echo e($fooddateshopreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe class="service-video" src="<?php echo e(isset($fooddateshopdetails[0]->mc_video_url) ? $fooddateshopdetails[0]->mc_video_url : ''); ?>?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="desserts">
                <div class="slide-cats owl-carousel" dir="ltr">
                    <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e($cat->image); ?>" alt="">
                            <p><?php echo e($cat->attribute_title); ?></p>
                        </a>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>


                <div class="box p-15">
                    <div class="row">

                        <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $productservice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $productservice->serviceslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="<?php echo e($getallcats->pro_Img); ?>" alt="" />
                                </div>
                                <div class="caption">
                                    <a href=""><?php echo e($getallcats->pro_title); ?></a>
                                    <p><?php echo e($beautyshopleftproduct->pro_desc); ?></p>
                                    <div class="flex-end">
                                        <div class="price">

                                            <?php if($beautyshopleftproduct->pro_discount_percentage!=''){ $productprice= $beautyshopleftproduct->pro_price*(100-$beautyshopleftproduct->pro_discount_percentage)/100;}else{ $productprice='';}
                                                  if($productprice==''){ ?>  <?php echo e(currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)); ?> <?php }else{ ?> <span class="strike"><?php echo e(currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)); ?> </span><?php echo e(currency($productprice, 'SAR',$Current_Currency)); ?> <?php  } ?> </div>



                                    </div>

                                    <?php echo Form::open(['url' => 'beautyshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>


                                    <?php if($productprice!=''){ $totalprice=$productprice; }else{$totalprice=$beautyshopleftproduct->pro_price;} ?>
                                    <?php if($category_id==21){ $sub_type='spa'; }else{ $sub_type='beauty_centers'; } ?>
                                    <input type="hidden" name="service_duration" id="service_duration" value="<?php echo e($beautyshopleftproduct->service_hour); ?>">
                                    <input type="hidden" name="opening_time" id="opening_time" value="<?php echo e(isset($fooddateshopdetails[0]->opening_time) ? $fooddateshopdetails[0]->opening_time : ''); ?>">
                                    <input type="hidden" name="closing_time" id="closing_time" value="<?php echo e(isset($fooddateshopdetails[0]->closing_time) ? $fooddateshopdetails[0]->closing_time : ''); ?>">
                                    <input type="hidden" name="category_id" value="<?php echo e($subsecondcategoryid); ?>">
                                    <input type="hidden" name="subsecondcategoryid" value="<?php echo e($category_id); ?>">
                                    <input type="hidden" name="cart_sub_type" value="<?php echo e($sub_type); ?>">
                                    <input type="hidden" name="branch_id" value="<?php echo e($branchid); ?>">
                                    <input type="hidden" name="shop_id" value="<?php echo e($shop_id); ?>">
                                    <input type="hidden" name="typeserice" id="typeserice" value="">
                                    <input type="hidden" name="vendor_id" value="<?php echo e($beautyshopleftproduct->pro_mr_id); ?>">
                                    <input type="hidden" name="product_id" id="product_id" value="<?php echo e($beautyshopleftproduct->pro_id); ?>">
                                    <input type="hidden" name="product_price" id="product_price" value="<?php echo e(currency($totalprice, 'SAR',$Current_Currency, $format = false)); ?>">
                                    <input type="hidden" name="todaydate" id="todaydate" value="<?php echo e(date('jMY')); ?>">

                                    <input type="hidden" name="product_orginal_price" id="product_orginal_price" value="<?php echo e(currency($totalprice, 'SAR',$Current_Currency, $format = false)); ?>">

                                    <input type="submit" name="booknow" value="<?php if(Lang::has(Session::get('lang_file').'.booknow')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.booknow')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.booknow')); ?> <?php endif; ?>" class="form-btn addto_cartbtn">




                                    <?php echo Form::close(); ?> </div>

                            </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>

            </div>

            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>

        </div>
    </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>

    <script type="text/javascript">
        function staffavailabilitychart(branchid){
            var bookingdate=jql('#bookingdate').val();
            var product_id=jql('#product_id').val();
            var servicestaffid=jql('#servicestaffid').val();
            // var branch_id=parseInt(branchid);

            if(servicestaffid){
                jQuery.ajax({
                    type:"GET",
                    url:"<?php echo e(url('beautyshop/staffavailabilitychart')); ?>?branchid="+branchid+"&servicestaffid="+servicestaffid+"&bookingdate="+bookingdate+"&product_id="+product_id,
                    success:function(res){
                        if(res){
                            var json = JSON.stringify(res);
                            var obj = JSON.parse(json);

                            console.log(obj);

                            length=obj.bookingopen.length;
                            jql('#bookingdatepopup').html(bookingdate);

                            if(length>0){
                                jql('#staffpopup').html('');
                                for(jk=0; jk<length; jk++)
                                {
                                    bookedslot=obj.bookingopen[jk].bookedslot.length;

                                    jql('#staffpopup').append('<div class="avalb_row"><div class="avalb_tbl_col3"><img src="'+obj.bookingopen[jk].image+'"></div><div class="avalb_tbl_col3">'+obj.bookingopen[jk].staff_member_name+'</div><div class="avalb_tbl_col3"><span id="tslot'+jk+'"></span></div>');
                                    if(bookedslot>0){
                                        for(k=0; k<length; k++){
                                            jql('#tslot'+jk).append('<div class="time_slot">'+obj.bookingopen[jk].bookedslot[k].start_time+'-'+obj.bookingopen[jk].bookedslot[k].end_time+'</div>');
                                        }
                                    }else{
                                        jql('#tslot'+jk).append('<div class="time_slot">-</div>');
                                    }

                                    //jql('#staffpopup').append('</div>');
                                }


                            }else{
                                jql('#staffpopup').html('<div class="avalb_row">No Data Found</div>');
                            }


                        }
                    }
                });
            }






        }

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  

<?php $Coshabig_leftmenu =1; ?> 

 

<!--start merachant-->

<div class="merchant_vendor">

  <!--left panel-->

 <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <!--left panel end-->

  <!--right panel-->

<?php

	$id                = request()->id;

	$sid               = request()->sid;

	$opid              = request()->opid;

	$cusid             = request()->cusid;

	$oid               = request()->oid;

	$getCustomer       = Helper::getuserinfo($cusid);

	$shipaddress       = Helper::getgenralinfo($oid); 

  $ordid  = $oid; 

	$cityrecord        = Helper::getcity($shipaddress->shipping_city);

	$getorderedproduct = Helper::getorderedproduct($oid);

	//echo '<pre>';print_r($shipaddress);die;

?>



  <div class=" right_panel" >

    <div class="inner">

      <!--haeding panel-->

      <div class="service_listingrow">

        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order_Detail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order_Detail')); ?> <?php endif; ?>

		 <a href="<?php echo e(url('/')); ?>/cosha-order/<?php echo e($id); ?>/<?php echo e($sid); ?>" class="order-back-page"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BACK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?> <?php endif; ?></a>

		</h5>

       </div>

      <!-- service_listingrow -->

      <!--haeding panel end-->

      <!-- Display Message after submition -->

      <?php if(Session::has('message')): ?>

      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>

      <?php endif; ?>

      <!-- Display Message after submition -->

      

      <div class="global_area">

        <!--global start-->

        <div class="row">

          <div class="col-lg-12">

            <div class="box hall-od commonbox" style="max-width: 850px;">

              <div class="hall-od-top">

                <div class="form_row">

                  <div class="noneditbox">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></label>

                    <div class="info100" ><?php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} ?></div>

                  </div>

                  <div class="noneditbox box-right">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?></label>

                    <div class="info100" ><?php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} ?></div>

                  </div>

                </div>

                <!-- form_row -->

                <div class="form_row">

                  <div class="noneditbox">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?></label>

                    <div class="info100"><?php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} ?></div>

                  </div>

                  <div class="noneditbox box-right">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?> <?php endif; ?></label>

                    <div class="info100"><?php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} ?></div>

                  </div>

                </div>

                <!-- form_row -->

                <div class="form_row">

                  <div class="noneditbox">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDRESS')); ?> <?php endif; ?></label>

                    <div class="info100"><?php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} ?>  <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="<?php echo e(url('')); ?>/themes/images/placemarker.png" /></a></div>

                  </div>

                  <div class="noneditbox box-right">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?></label>

                    <div class="info100" >

                        <?php 

						$ordertime=strtotime($getorderedproduct->created_at);

                        $orderedtime = date("d M Y",$ordertime);

						if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}

                       ?>

                   </div>

                  </div>

                </div>

				<div class="form_row">

                  <div class="noneditbox">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT')); ?> <?php endif; ?></label>

                    <div class="info100"><?php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} ?></div>

                  </div>

                  <?php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ ?>

                  <div class="noneditbox box-right">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.transaction_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.transaction_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.transaction_id')); ?> <?php endif; ?></label>

                   <div class="info100"><?php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} ?></div>

                  </div>

                  <?php } ?>

                </div>

				<div class="form_row">

                  <div class="noneditbox">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?></label>

                    <div class="info100"><?php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} ?></div>

                  </div>

                  <div class="noneditbox box-right">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD')); ?> <?php endif; ?></label>

                   <div class="info100">

				   <?php if(isset($shipaddress->shipping_id) && $shipaddress->shipping_id!='')

					 { 

					  $getName = Helper::getShippingMethodName($shipaddress->shipping_id);

					  echo $getName;

					 }

					 else

					 {

					  echo 'N/A';

					 }

					?>

				 </div>

                  </div>

                </div>

                <!-- form_row -->

                <div class="form_row">

				  <div class="noneditbox">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE')); ?> <?php endif; ?></label>

                    <div class="info100"> 

					<?php if(isset($shipaddress->shipping_charge) && $shipaddress->shipping_charge!='' && $shipaddress->shipping_charge!='0')

					 {

					  echo $shipaddress->shipping_charge;

					 }

					 else

					 {

					  echo 'N/A';

					 }

					 ?>

				 </div>

                  </div>

                  <div class="noneditbox box-right">

                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONDATE')); ?> <?php endif; ?></label>

                    <div class="info100"> <?php if(isset($shipaddress->occasion_date) && $shipaddress->occasion_date!=''){echo $shipaddress->occasion_date;}else{echo 'N/A';} ?></div>

                  </div>

                  

                </div>

                

                <div class="noneditbox-sub-heading"><?php if(Lang::has(Session::get('mer_lang_file').'.item_information')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.item_information')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.item_information')); ?> <?php endif; ?></div>

              </div>

              <!-- hall-od-top -->

              <div class="table">

                <div class="tr">

                  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.ABAYA_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ABAYA_PRODUCT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ABAYA_PRODUCT')); ?> <?php endif; ?></div>

				  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_IMAGE')); ?> <?php endif; ?></div>

                  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Quantity')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Quantity')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Quantity')); ?> <?php endif; ?></div>

                  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.BACK_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> <?php endif; ?></div>

                  <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Insuranceamount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Insuranceamount')); ?> <?php endif; ?></div>

                  

</div>

               <?php $pro_title='pro_title';   ?>

                   <?php if($mer_selected_lang_code !='en'): ?>

                                         <?php $pro_title='pro_title_'.$mer_selected_lang_code; ?>

                   <?php endif; ?>   

                <?php 

				$basetotal = 0; $couponcode=0; $couponcodeinfo=''; $wisiinsurance = 0;

				foreach($getOrderKosha as $bodymeasurement)

				{

				  $basetotal            = ($basetotal+$bodymeasurement->total_price);

                  $pid                  = $bodymeasurement->product_id;



                  $getorderedproducttbl = Helper::getProduckInfo($pid);

                  $wisiinsurance=$wisiinsurance+$bodymeasurement->insurance_amount;

				  $productproductimage  = Helper::getproductimage($pid);



            $orderedprodattrib=Helper::getproductorderattribute($bodymeasurement->order_id,$pid);





				  if(isset($getorderedproducttbl->pro_disprice) && $getorderedproducttbl->pro_disprice!='0'){$getPrice = $getorderedproducttbl->pro_disprice;}else{$getPrice = $getorderedproducttbl->pro_price;}



           $couponcode=$couponcode+$bodymeasurement->coupon_code_amount;;

        $couponcodeinfo=$bodymeasurement->coupon_code;



				?>

                <div class="tr">

                  <div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.ABAYA_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ABAYA_PRODUCT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ABAYA_PRODUCT')); ?> <?php endif; ?>"><?php if(isset($getorderedproducttbl->pro_title) && $getorderedproducttbl->pro_title!=''){echo $getorderedproducttbl->$pro_title;} ?></div>

				   <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_IMAGE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_IMAGE')); ?> <?php endif; ?>">

                          <?php if($productproductimage->pro_Img): ?>   

                           <img src="<?php echo e($productproductimage->pro_Img); ?>" alt="" width="150">

                          <?php else: ?>

                           No image



                          <?php endif; ?>

                         

                       </div> 

                 <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Quantity')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Quantity')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Quantity')); ?> <?php endif; ?>">



                    <?php if(isset($orderedprodattrib->quantity) && $orderedprodattrib->quantity>0){ $pqty=$orderedprodattrib->quantity; }else{ $pqty=$bodymeasurement->quantity; } ?>

                       <?php if(isset($pqty) && $pqty!=''): ?>

                                            <?php echo e($pqty); ?>


                                            <?php endif; ?> 



 </div>

                 <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?>"><?php echo e(number_format($bodymeasurement->total_price,2)); ?></div>

				 <div class="td td5" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Insuranceamount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Insuranceamount')); ?> <?php endif; ?>"><?php echo e(number_format($bodymeasurement->insurance_amount,2)); ?></div>

                </div>

                <?php } ?>

              </div>  <div class="merchant-order-total-area">

                <div class="merchant-order-total-line"> <?php if(isset($couponcode) && $couponcode>=1): ?>

             <?php echo e((Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')); ?>: 



             <?php if(isset($couponcode) && $couponcode!='')

          {

          echo 'SAR '.number_format($couponcode,2);

          }

          else

          {

          echo 'N/A';

          }

          ?>  

          <br>



          <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')); ?>: <?php echo e($couponcodeinfo); ?>


 <br>

          <?php endif; ?>







            <?php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }

              $calnettotalamount=$basetotal-$couponamount-$wisiinsurance;

             ?>



          <?php echo e((Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')); ?>: 

          <?php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); ?>



          <?php if(isset($vatamount) && $vatamount!='')

          {

          echo 'SAR '.number_format($vatamount,2);

          }

          else

          {

          echo 'N/A';

          }

          ?>

				 </div>

				 <div class="merchant-order-total-line">

				<?php echo e((Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($MER_OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: 

				<?php if(isset($shipaddress) && $shipaddress!=''){

				  echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);

				 } ?>              

				</div></div>

              

            </div>

            <!-- box -->

          </div>

        </div>

        <!--global end-->

      </div>

    </div>

  </div>

  <!--end right panel-->

</div>

<!--end merachant-->

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
 <?php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ ?>
      <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?>
      <?php if(Session::get('searchdata.mainselectedvalue')=='1'){ ?>
      <?php echo $__env->make('includes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?> 
</div> <!-- search-section -->

 

<div class="page-left-right-wrapper">
<?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>





<div class="page-right-section">

 <!-- budget-menu-outer -->
<div class="form_title"><?php echo e($foodmaincategory[0]->mc_name); ?></div>
<div class="diamond-area">
<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
		<div class="diamond_wrapper_main">
			
			<?php  $i=1;     ?>
			<?php if($typeofhallid==9){ $reurl='dessertshop';}else{ $reurl='foodshopbuffetmenu';} ?>
			<?php  $k=count($foodshopunderbugetincity);  ?>
			<?php if($k<6){ ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $foodshopunderbugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row">
					  <a href="<?php echo e(url('')); ?>/<?php echo e($reurl); ?>/<?php echo e($subcategory_id); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($getallcats->mc_id); ?>">
						<div class="category_wrapper" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div></div>
						</div>
					</a>
				</div>
				 <?php $i=$i+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div> 
				<!------------ 6th-------------->
				<?php }elseif($k==6){ ?>
				<?php $j=1; ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $foodshopunderbugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			 <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
			 <?php if($j==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($j==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
			 <?php if($j==3){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($j==5){ ?> <div class="row_4of5 rows5row"> <?php } ?>
			  <?php if($j==6){ ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					<a href="<?php echo e(url('')); ?>/<?php echo e($reurl); ?>/<?php echo e($subcategory_id); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($getallcats->mc_id); ?>">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div></div>
						</div>
					</a>
			<?php if($j==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($j==2){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($j==4){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($j==5){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($j==6){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $j=$j+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!------------ 7th-------------->
				<?php }elseif($k==7){ ?>
				<?php $l=1; ?>
			<div class="diamond_wrapper_inner">
			<?php $__currentLoopData = $foodshopunderbugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
			
			 <?php if($l==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($l==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
			 <?php if($l==3){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($l==6){ ?> <div class="row_4of5 rows5row"> <?php } ?>
			  <?php if($l==7){ ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					  <a href="<?php echo e(url('')); ?>/<?php echo e($reurl); ?>/<?php echo e($subcategory_id); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($getallcats->mc_id); ?>">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div></div>
						</div>
					</a>
			<?php if($l==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==2){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($l==5){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==6){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($l==7){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $l=$l+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!------------ 8th-------------->
				<?php }elseif($k==8){ ?>
				<?php $l=1; ?>
			
					<div class="diamond_wrapper_inner">
					<?php $__currentLoopData = $foodshopunderbugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
				<?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
				<?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
				
			 <?php if($l==1){ $classrd='category_wrapper1'; ?> <div class="row_1of5 rows5row"> <?php } ?> 
		     <?php if($l==2){ ?>  <div class="row_3of5 rows5row"> <?php } ?> 
			 <?php if($l==4){  ?> <div class="row_3of5 rows5row"> <?php } ?> 
			  <?php if($l==6){ ?> <div class="row_3of5 rows5row"> <?php } ?>
			  <?php if($l==8){ $classrd='category_wrapper9'; ?> <div class="row_5of5 rows5row"> <?php } ?> 		
					  <a href="<?php echo e(url('')); ?>/<?php echo e($reurl); ?>/<?php echo e($subcategory_id); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($getallcats->mc_id); ?>">
						<div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
							<div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?> </div></div>
						</div>
					</a>
			<?php if($l==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==3){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($l==5){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($l==7){ ?> <div class="clear"></div> </div> <?php } ?>
		  <?php if($l==8){ ?> <div class="clear"></div> </div> <?php } ?>
				<?php $l=$l+1; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
				<!---------- 9th ------------------->
		<?php }elseif($k==9){ ?>
		
		
					<div class="diamond_wrapper_inner">
	  			<?php $i=1; ?>
		  <?php $__currentLoopData = $foodshopunderbugetincity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
		  

		 <?php if($i==1){ ?> <div class="row_1of5 rows5row"> <?php } ?> 
		  <?php if($i==2){ ?>  <div class="row_2of5 rows5row"> <?php } ?> 
		   <?php if($i==4){ ?> <div class="row_3of5 rows5row"> <?php } ?> 
		    <?php if($i==7){ ?>  <div class="row_4of5 rows5row"> <?php } ?> 
			<?php if($i==9){ ?>  <div class="row_5of5 rows5row"> <?php } ?> 
            <a href="<?php echo e(url('')); ?>/<?php echo e($reurl); ?>/<?php echo e($subcategory_id); ?>/<?php echo e($category_id); ?>/<?php echo e($branch_id); ?>/<?php echo e($getallcats->mc_id); ?>">
              <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
                <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span></span>
              </span>
            </a>
		 <?php if($i==1){ ?> <div class="clear"></div> </div> <?php } ?> 
		  <?php if($i==3){ ?> <div class="clear"></div> </div> <?php } ?> 
		   <?php if($i==6){ ?> <div class="clear"></div> </div> <?php } ?> 
		    <?php if($i==8){ ?> <div class="clear"></div></div> <?php } ?> 
			<?php if($i==9){ ?>  <div class="clear"></div> </div> <?php } ?> 
		 
		    <?php $i=$i+1; ?>
		   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
	  
		  
        </div>
		
		
		
				
			<?php } ?>
				 	
				
		</div> 
	  </div>
	 
  </div>
  <?php //<div class="diamond_shadow">{{ $foodshopunderbugetincity->links() }}</span></div> ?>
<div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>

</div>









    <!-- budget-carousel-area -->


</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

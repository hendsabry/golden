<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>

    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
                    <div class="right_col" role="main">
          <div id="appUserList">
            <div class="clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="page-title">
                          <div class="title_left">
                              <h3 class="elec"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION')); ?></h3>


<span class="invlist vist">
  <a href="<?php echo e(route('admin/manage_electronic')); ?>">
 <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.MANAGE_ELECTRONIC_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.MANAGE_ELECTRONIC_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.MANAGE_ELECTRONIC_INVITATION')); ?></h3>
</a>
</span>


                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hight_overhid controlwidth">
                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                           <div class="mainSeacrh_box">
                    <div class="input-groupSearch clearfix search-form">
                    <input v-model="search_keyword" value="" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH_BY_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SEARCH_BY_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH_BY_NAME')); ?>" autocomplete="off" class="form-control pr-Large20" id="navbar-search-input" type="text" @keyup="search">
                         </div>
                         </div>
                         </div>
                         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">  
                            <input id="from_date"  placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_DATE')); ?>" class="form-control" type="text">
                          </div>                         
                         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
                          <select v-model="status" class="form-control">
                             <option value="" selected="selected"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PLEASE_SELECT_STATUS')); ?></option>
                              <option  value="1"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE')); ?></option>
                              <option  value="0"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE')); ?></option>
                          </select>
                         </div>
                         <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                        <button type="submit" :disabled="isProcessing" @click="search" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                        <button type="submit" :disabled="isProcessing" @click="clearFilter" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
            <?php if(Session::get('access_permission_type')>0): ?>
                        <a type="submit" href="/admin/electronic_invitation/list" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_DESIGNER_CARDS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_DESIGNER_CARDS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_DESIGNER_CARDS')); ?></a>
                        <a type="submit" href="/admin/sms_email/list" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_SMS_MSG_CHARGES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_SMS_MSG_CHARGES') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_SMS_MSG_CHARGES')); ?></a>
                        <?php endif; ?>
                          </div>
                        </div>
                        <input id="lang" class="form-control" value="<?php echo e(Session::get('admin_lang_code')); ?>" type="hidden">
                        </div>
                              <div class="x_content UsersList">
                                <div class="table-responsive">
                                  <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                      <tr class="headings">
                                        <th class="column-title numrical_2"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO')); ?> </th>
										<th class="column-title numrical_2"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ORDER_ID') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_ID')); ?> </th>
                                        <th class="column-title name_2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME')); ?></th>
                                        <th class="column-title occasion_2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.OCCASION_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.OCCASION_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.OCCASION_TYPE')); ?></th>
                                        <th class="column-title no-link  designer_card"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_MODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_MODE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_MODE')); ?></span>
                                        </th>
                                        <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_NO_PACKAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_PACKAGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_PACKAGE')); ?></span>
                                        </th>
                                        <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_BOOKING_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BOOKING_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BOOKING_DATE')); ?></span>
                                        </th>
                                       <!--  <th class="column-title no-link status_2 "><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?></span>
                                        </th> -->
                                         <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION')); ?></span>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr v-if="users" v-for="(ud, index) in users" class="all_content">
                                        <td class="numrical_1"> {{ index+1 }}</td>
										 <td>{{ ud.order_id }} </td>
                                        <td class="numrical_1">{{ ud.get_user.cus_name }}</td>
                                        <td v-if="ud.get_package" v-for="(pk, sn) in ud.get_package">
                                            <p v-if="pk.event_type == 1"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_BUSINESS_MEETING')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_BUSINESS_MEETING') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_BUSINESS_MEETING')); ?></p>
                                            <p v-else><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_WEDDING_&_OTHER_OCASSION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_WEDDING_&_OTHER_OCASSION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_WEDDING_&_OTHER_OCASSION')); ?></p>
                                          </td>
                                              <td v-if="ud.get_package" v-for="(pk, sn) in ud.get_package">
                                             <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
                                             <p>{{pk.pro_title_ar}}</p>
                                             <?php else: ?>
                                              <p>{{pk.pro_title}} <span v-if="pk.pro_title != 'Email' && pk.pro_title != 'Message'"> (<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DESIGNER_CARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESIGNER_CARD') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DESIGNER_CARD')); ?>)</span></p>
                                             <?php endif; ?>

                                             
                                              </td>
                                          
                                          <td>{{ ud.invite_count }} </td>
                                          <td>{{ ud.created }}</td>
                                        <!-- <td>
                                          <div>
                                            <label class="switch" >
                                             <input class="example2" type="checkbox" v-bind:id="ud.id" v-bind:checked="ud.status == 1" @click="statusChanged(ud.id)">
                                              <div class="slider round"></div>
                                            </label>
                                            </div>
                                        </td> -->
                                  
                                        <td class="last">
                                        <button  @click="ViewUser(ud.id)" class="btn view-delete"><i class="fa fa-eye"></i> </button>
                                         <!-- <button  @click="DeleteUser(ud.id)" class="btn view-delete"><i class="fa fa-trash-o"></i> </button> -->
                                        </td>
                                      </tr>
                                      <tr v-if="users ==''">
                                        <td colspan="7"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?></td></tr>
                                    </tbody>
                                  </table>
                                </div>        
                              </div>
                              <div class="paging">
                           <div class="pull-right mt-lg">
                          <ul class="pagination  pagination-separated">
                          <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
                          @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
                          <li v-for="page in pagesNumber"
                          v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
                          @click.prevent="changePage(page)">{{ page }}</a> </li>
                          <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
                          @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
                          </ul>
                          </div>
                          </div>
                            </div>
                          </div>
          </div>
        </div>
          </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
      $('#from_date').datepicker({
        format: "MM dd, yyyy",
        autoclose: true,
        todayHighlight: true    
        });

        $('#from_date').on('change',function(){
          var dated = $(this).val();
          //$('#from_date').val(dated);
          //$('#fill_date').val(dated);
        
        });
      });
    </script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._users = [<?php echo $users->toJson(); ?>];
</script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/electronicinvitation.js"></script>  
</body>

    <!-- END BODY -->
</html>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $oudandperfumes_leftmenu =1; ?>
<div class="merchant_vendor"> 
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.mer_shoplist')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_shoplist')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_shoplist')); ?> <?php endif; ?> </h5>
           <?php if(Session::get('LoginType') !='4'): ?>
        <div class="add"><a  href="<?php echo e(route('oudandperfumes-add-shop',['id' => request()->id])); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_SHOP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_SHOP')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_SHOP')); ?> <?php endif; ?> </a></div>
        <?php endif; ?>
      </div>
      <!-- service_listingrow --> 
 


      <?php $id = request()->id; ?>
      <?php echo Form::open(array('url'=>"oudandperfumes-shop/{$id}",'class'=>'form-horizontal','accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?>


       <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
      <div class="filter_area">
        <div class="filter_left">
          <div class="search_filter"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></div>
          <div class="search-box-field mems "> <?php $statuss = request()->status; $searchh= request()->search; ?>
         <select name="status" id="status">
              <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS')); ?> <?php endif; ?> </option>
              <option value="1" <?php if(isset($statuss) && $statuss=='1'): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?></option>
              <option value="0" <?php if(isset($statuss) && $statuss=='0'): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?> > <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?></option>
            </select>
            <input name="" type="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''): ?><?php echo e(trans(Session::get('mer_lang_file').'.MER_APPLY')); ?><?php else: ?><?php echo e(trans($MER_OUR_LANGUAGE.'.MER_APPLY')); ?><?php endif; ?>" class="applu_bts" />
          </div>
        </div>
        <div class="search_box">
          <div class="search_filter">&nbsp;</div>
          <div class="filter_right">
            <input name="search" type="text" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SEARCH')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?> <?php endif; ?>" value="<?php echo e(isset($searchh) ? $searchh : ''); ?>" />
            <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
          </div>
        </div>
      </div>
      <!-- filter_area -->
 
      <?php echo Form::close(); ?>

      <!-- Display Message after submition -->


  <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>




      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap">
              <div class="panel-body panel panel-default">
                

              <?php if($alldata->count() < 1): ?>
              <div class="no-record-area">
              <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?>
              </div>
              <?php else: ?> 
              <div class="table hotel-list-table">
                          <div class="tr">
                            <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_NAME')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_NAME')); ?> <?php endif; ?></div>
                             <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_LOGO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_LOGO')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_LOGO')); ?> <?php endif; ?>  </div>
                              <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CREATED_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CREATED_DATE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CREATED_DATE')); ?> <?php endif; ?></div>
                                <?php if(Session::get('LoginType') !='4'): ?>
                             <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?></div>
                             <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?></div>
                             <?php endif; ?>
                              <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?></div>
                          </div>


                        <?php $mc_name='mc_name'?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $mc_name= 'mc_name_'.$mer_selected_lang_code; ?>
                        <?php endif; ?>           
                        <?php $__currentLoopData = $alldata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

 
                        <div class="tr">
                        <div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_NAME')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_NAME')); ?> <?php endif; ?>"><?php echo e($val->$mc_name); ?></div>
                        <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_LOGO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_LOGO')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_LOGO')); ?> <?php endif; ?>  ">
                        
                          <img src="<?php echo e(isset($val->mc_img) ? $val->mc_img : ''); ?>" alt="" width="150" />

 

                        </div>
                        <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_CREATED_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CREATED_DATE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CREATED_DATE')); ?> <?php endif; ?>">  <?php echo e(Carbon\Carbon::parse($val->updated_at)->format('F j, Y')); ?> </div>

                  <?php if(Session::get('LoginType') !='4'): ?>
                       <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.STATUS')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.STATUS')); ?> <?php endif; ?>">
                    <?php if($val->mc_status==1): ?>
                   <span  class="status_active  status_active2 cstatus" data-status="Active"  data-id="<?php echo e($val->mc_id); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?> </span> 
                    <?php else: ?>
                    <span class="status_deactive  status_active2 cstatus" data-status="Inactive" data-id="<?php echo e($val->mc_id); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?></span>
                    <?php endif; ?>

                    </div>
 

                        <div class="td td5" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?> "><a href="<?php echo e(route('oudandperfumes-add-shop',['id' => request()->id,'hid' => $val->mc_id])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EDIT')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EDIT')); ?> <?php endif; ?></a></div>
                        <?php endif; ?>
                        <div class="td td6 view_center" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST')); ?> <?php endif; ?>" ><a  href="<?php echo e(route('oudandperfumes-shop-branch',['id' => request()->id,'hid' => $val->mc_id])); ?>">

                          <img src="<?php echo e(url('')); ?>/public/assets/img/view-icon.png" title="<?php if(Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_view_title')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_view_title')); ?> <?php endif; ?>" alt="" /></a>
                        </div>
                        </div>
 
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


        			</div>  
              <?php endif; ?>



                
                <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
                <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
              </div>
                <?php echo e($alldata->links()); ?> 
            </div>
          </div>
        </div>
      </div>
      <!--global_area-->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- <div merchant_vendor -->
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Action')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Action')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Action')); ?> <?php endif; ?></div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a> </div>
  </div>
</div>
<!-- action_popup -->

<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_De_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record')); ?> <?php endif; ?>')
     var activestatus = 1;
 } else {

     jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Activate_Record')); ?> <?php endif; ?>')
      var activestatus = 0;
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
    

     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('change-status')); ?>",
        data: {activestatus:activestatus,id:id,from:'hotellist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script>

<script type="text/javascript">

  $(document).ready(function(){
	$('form[name=test]').submit();
	 $('#submitdata').click(function(){            
	 $('form[name=filter]').submit();
  });
  });

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
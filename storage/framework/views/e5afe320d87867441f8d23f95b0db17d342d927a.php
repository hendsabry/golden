<?php $__env->startSection('content'); ?>
<?php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
?>
 <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></h2>
                                <h2><?php echo e($vendordetails->address); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                	<?php
								        $getcityname = Helper::getcity($vendordetails->city_id); 
								        $mc_name = 'ci_name'; 
								        if(Session::get('lang_file')!='en_lang')
								      {
								          $mc_name = 'ci_name_ar'; 
								        }
								        echo $getcityname->$mc_name; 
								      ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php if($lang != 'en_lang'): ?> <?php echo e($vendordetails->mc_discription); ?> <?php else: ?> <?php echo e($vendordetails->mc_discription); ?> <?php endif; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                     <?php if($vendordetails->google_map_address!=''): ?> 
					 <?php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
					   <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
					 <?php endif; ?>
					</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                        	<?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
                   				<?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                        <?php echo e($allreview->links()); ?>

                        <!-- <ul class="pagenation">
                            <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">10</a></li>
                            <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                    	<iframe width="100%" height="315" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li class="active">
                            <div class="image"><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/roses-04.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>" <?php if(request()->type!=2): ?> class="select" <?php endif; ?>><?php echo e((Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')); ?></a></h2>
                        </li>
                        <li >
                            <div class="image"><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/roses-05.png" alt=""></a></div>
                            <h2><a  <?php if(request()->type==2): ?> class="select" <?php endif; ?> href="<?php echo e(route('single-roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Single')!= '')  ?  trans(Session::get('lang_file').'.Single'): trans($OUR_LANGUAGE.'.Single')); ?></a></h2>
                        </li>
                    </ul>
                </center>

                <br>
                <div class="slide-cats2 owl-carousel" dir="ltr">
                	
                	<?php $__currentLoopData = $catg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="<?php echo e($cat->mc_img); ?>" alt="">
                            <p><?php echo e($cat->mc_name); ?></p>
                        </a>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </div>
<?php 
  $getproID = $product[0]->pro_id;  ?>
  <?php



            if($product[0]->pro_qty>0)
            {
                   if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') 
                   {
                           $cartbtn=trans(Session::get('lang_file').'.Add_to_Cart');  } else{  $cartbtn=trans($OUR_LANGUAGE.'.Add_to_Cart'); 
                    }
                      $btnst='';
                      $avcart='';
                    $avcart='block';
              }
               else
              {

                if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''){ $cartbtn=trans(Session::get('lang_file').'.SOLD_OUT'); }else{ $cartbtn=trans($OUR_LANGUAGE.'.SOLD_OUT');}
                $btnst='disabled="disabled"';
                $avcart='none';
              }

$OriginalP = $product[0]->pro_price; 
$OriginalDis = $product[0]->pro_discount_percentage;
$Disp = $OriginalP - ($OriginalP*$OriginalDis)/100;
$Disp = number_format((float)$Disp, 2, '.', ''); 
?> 

                <div class="box kosha p-15 koha-ready">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="fw-700 h4"><?php echo e($product[0]->pro_title); ?></p>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                            	<?php if($OriginalDis!=''){ ?>
                                <p class="price"><span class="old"><?php echo e(currency($product[0]->pro_price,'SAR',$Current_Currency)); ?></span> <?php } ?><span class="new"> <?php echo e(currency($Disp,'SAR',$Current_Currency)); ?> </span></p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="image-ready">
                                <img src="<?php echo e($product[0]->pro_Img); ?>" alt="Image 1" />
                            </div>
                            <h3>Description</h3>
                            <div class="box p-15">
                                <p><?php echo e($product[0]->pro_desc); ?></p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <h3>Details</h3>
                            <p class="fw-700"><?php if(Lang::has(Session::get('lang_file').'.Number_of_Flowers')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Number_of_Flowers')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Number_of_Flowers')); ?> <?php endif; ?> <span style="color: #000"></span><?php echo e(count($product[0]->product_packege)); ?></p>
                            <br>
                            <h3><?php if(Lang::has(Session::get('lang_file').'.Flower_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Flower_Type')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Flower_Type')); ?> <?php endif; ?></h3>
                            <div class="row">
                            	<?php for($i=0;$i< count($product[0]->product_packege);$i++): ?>
	                                <div class="col-md-4">
	                                    <div class="item-type">
	                                        <img src="<?php echo e($product[0]->product_packege[$i]->getProductPackage->pro_Img); ?>" alt="" />
	                                        <p class="fw-700"><?php echo e($product[0]->product_packege[$i]->getProductPackage->pro_title); ?></p>
	                                    </div>
	                                </div>
                                  <?php endfor; ?> 
                              
                            </div>
                            <br>
                            <div class="row">
                            	   <?php if(count($product[0]->product_option) >=1): ?> 
									<?php for($i=0;$i<=1;$i++): ?>
                                <div class="col-md-6">
                                    <h3><?php echo e($product[0]->product_option[$i]->option_title); ?></h3>
                                 
                                    <div class="item-type">
                                        <img src="<?php echo e($product[0]->product_option[$i]->product_option_value[0]->image); ?>" alt="" />
                                        <p class="fw-700"><?php echo e($product[0]->product_option[$i]->product_option_value[0]->option_title); ?></p>
                                    </div>
                                      
                                </div>
                                <?php endfor; ?>       
  									 <?php endif; ?> 
                                
                            </div>
                            <br>
                            <form name="form1" method="post" action="<?php echo e(route('roses-package-add-to-cart')); ?>"" enctype="multipart/form-data">
      						<?php echo e(csrf_field()); ?>

                            <p class="fw-700"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?> : <input class="quantity" name="itemqty" id="qty" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantity('add','<?php echo e($product[0]->pro_id); ?>');" onkeydown="isNumberKey(event); checkquantity('add','<?php echo e($product[0]->pro_id); ?>');" type="number" /></p>
                            <br>
                            <p class="price fw-700"><span style="color: #000"><?php if(Lang::has(Session::get('lang_file').'.Total_Price')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Total_Price')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Total_Price')); ?> <?php endif; ?>:</span> <?php echo e(currency($Disp,'SAR',$Current_Currency)); ?></p>
                             <input type="hidden" name="netamountforqty_<?php echo e($product[0]->pro_id); ?>" id="netamountforqty_<?php echo e($product[0]->pro_id); ?>" value="<?php echo e(currency($Disp,'SAR',$Current_Currency, $format = false)); ?>">

      <input type="hidden" name="product_id" id="total_product_id" value="<?php echo e($product[0]->pro_id); ?>">
      <input type="hidden" name="cart_type" value="occasion">
      <input type="hidden" name="total_price" id="total_total_price" value="<?php echo e(currency($Disp, 'SAR',$Current_Currency, $format = false)); ?>">
      <input type="hidden" name="language_type" value="en">
      <input type="hidden" name="product_qty" value="1">
      <input type="hidden" name="product_option_value[]">
      <input type="hidden" name="attribute_id" value="<?php echo e(request()->id); ?>">
      <input type="hidden" name="cart_sub_type" value="roses">
                            <div class="form-group">
                                <div class="pull-right">
                                    <a href="">Terms and conditions</a>
                                    <input type="submit" value="<?php echo e($cartbtn); ?>" id="sbtn" class="form-btn addto_cartbtn" <?php echo e($btnst); ?>>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>

            </div>

 <!--            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center> -->

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
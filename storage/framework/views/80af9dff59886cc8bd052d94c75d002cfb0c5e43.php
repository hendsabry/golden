<?php $current_route = Route::getCurrentRoute()->uri(); ?>
<div class="col-sm-3 col-xs-12 left-side-bar">
 <!-- LOGO SECTION -->
<header class="navbar-header">
    <a href="<?php echo url('')?>/siteadmin_dashboard" class="navbar-brand"><img src="<?php echo $SITE_LOGO; ?>" alt="Logo" /></a>
</header>
<div class="user-profile">
<figure class="user-photo">
  <img src="<?php echo e(url('')); ?>/public/assets/adimage/user-dummy.png">
</figure>
<h3><?php if(isset($user->adm_fname)): ?> <?php echo e($user->adm_fname); ?> <?php endif; ?> <?php if(isset($user->adm_lname)): ?> <?php echo e($user->adm_lname); ?> <?php endif; ?></h3>
</div>
  <!-- END LOGO SECTION -->
   <div id="left">
   <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    SUB MENU <i class="icon-align-justify"></i>
                </a>
   <!--<div class="media user-media well-small">
        <div class="media-body">
            <h5 class="media-heading"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCTS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PRODUCTS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCTS')); ?></h5>
            
        </div>
    </div>-->
   <ul id="menu" class="collapse">
    <?php $__currentLoopData = $admin_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($menu->id == 1): ?>
         <li <?php if($current_route == 'admin/user_management' || $current_route == 'admin/user/view/{id}' || $current_route == 'admin/user_management/add' || $current_route == 'admin/user_management/edit/{id}') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-users"></i>
            <a href="<?php echo e(url('/admin/user_management')); ?>">
              <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT')); ?></a>                   
        </li>
        <?php elseif($menu->id == 2): ?>
          <li <?php if($current_route == 'admin/sub_admin' || $current_route == 'admin/subadmin/add' || $current_route == 'admin/sub_admin/view/{id}' || $current_route == 'admin/subadmin/edit/{id}') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
          
          <i class="fa fa-user "></i>
            <a href="<?php echo e(url('/admin/sub_admin')); ?>">
               <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN_MANAGEMENT'): trans($ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN_MANAGEMENT')); ?>

            </a>                   
        </li>
        <?php elseif($menu->id == 3): ?>
            <li <?php if($current_route == 'admin/access_level_management' || $current_route == 'admin/access_level/add' || $current_route == 'admin/access_level/view/{id}' || $current_route == 'admin/access_level/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="<?php echo e(url('/admin/access_level_management')); ?>" >
                <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT'): trans($ADMIN_OUR_LANGUAGE.'.BACK_ACCESS_LEVEL_MANAGEMENT')); ?>

           </a>                   
        </li>
      <?php elseif($menu->id == 18): ?>
         <li <?php if($current_route == 'admin/sales_rep' || $current_route == 'admin/sales_rep/add' || $current_route == 'admin/sales_rep/view/{id}' || $current_route == 'admin/sales_rep/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
        <i class="fa fa-shopping-cart"></i>
            <a href="<?php echo e(url('/admin/sales_rep')); ?>" >
              <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SALES_MANAGER_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_MANAGER_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_MANAGER_MANAGEMENT')); ?>

           </a>                   
        </li>
         <?php elseif($menu->id == 5): ?>
          <li <?php if($current_route == 'admin/vendor_management' || $current_route == 'admin/vendor_management/add' || $current_route == 'admin/vendor_management/view/{id}' || $current_route == 'admin/vendor_management/search' || $current_route == 'admin/vendor_management/delete/{id}' || $current_route == 'admin/vendor_management/edit/{id}' || $current_route == 'admin/vendor_management/broadcasst_message/add' || $current_route == 'admin/vendor_management/broadcasst_message' || $current_route == 'admin/vendor_management/broadcasst_message/list' || $current_route == 'admin/vendor_management/broadcasst_message/view/{id}' || $current_route == 'admin/vendor_management/vendor_payment/{id}' || $current_route == 'admin/vendor_management/vendor_payment/debitview/{id}' || $current_route == 'admin/vendor_management/vendor_payment/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
      <i class="fa fa-shopping-cart"></i>
            <a href="<?php echo e(url('/admin/vendor_management')); ?>" >
               <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_VENDORS_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDORS_MANAGEMENT'): trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDORS_MANAGEMENT')); ?>

           </a>                   
        </li>
        <?php elseif($menu->id == 6): ?>
        <li <?php if( $current_route == "admin/payment_transaction" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
         <i class="fa fa-credit-card"></i>
            <a href="/admin/payment_transaction" >
              <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION')); ?>

           </a>                   
        </li>
        <?php elseif($menu->id == 7): ?>
       <li <?php if($current_route == 'admin/static_content_management' || $current_route == 'admin/static_content_management/add' || $current_route == 'admin/static_content_management/view/{id}' || $current_route == 'admin/static_content_management/broadcasst_message/list' || $current_route == 'admin/static_content_management/broadcasst_message' || $current_route == 'admin/static_content_management/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
          <i class="fa fa-tasks"></i>
            <a href="<?php echo e(url('/admin/static_content_management')); ?>" >
                <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_STATIC_CONTENT_MANAGEMENT')); ?>

           </a>                   
        </li>

        <?php elseif($menu->id == 8): ?>
          <li <?php if( $current_route == "admin/designcard/invitation" || $current_route == 'admin/designcard/view/{id}' || $current_route == 'admin/designcard/invitation/list/{id}/{id2}' || $current_route == 'admin/electronic_invitation/list' || $current_route == 'admin/electronic_invitation/add'|| $current_route == 'admin/electronic_invitation/edit/{pro_id}' || $current_route == 'admin/electronic_invitation/view/{pro_id}') { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
     <i class="fa fa-user-plus"></i>
            <a href="/admin/designcard/invitation">
             <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION')); ?>

           </a>                   
        </li>

         <?php elseif($menu->id == 9): ?>
         <li <?php if($current_route == 'admin/review' || $current_route =='admin/review/view/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
        <i class="fa fa-star"></i>
            <a href="<?php echo e(url('/admin/review')); ?>" >
               <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING') : trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING')); ?>

           </a>                   
        </li>

         <?php elseif($menu->id == 10): ?>
          <li <?php if($current_route == 'admin/subscription' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="/admin/subscription">
               <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBSCRIPTION_MANAGEMENT')); ?>

           </a>                   
        </li>
         <?php elseif($menu->id == 11): ?>
          <li <?php if($current_route == 'admin/occasions' || $current_route == 'admin/occasions/view/{id}') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-calendar"></i>
            <a href="<?php echo e(url('/admin/occasions')); ?>" >
                <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_OCCASION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OCCASION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_OCCASION')); ?>

           </a>                   
        </li>
         <?php elseif($menu->id == 12): ?>
          <li <?php if($current_route == 'admin/delivery' || $current_route == 'admin/delivery/view/{id}' || $current_route == 'admin/delivery/add' || $current_route == 'admin/delivery/edit{id}') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
          <i class="fa fa-tasks"></i>
            <a href="/admin/delivery">
            <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DELIVERY_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_MANAGEMENT')); ?>

           </a>                   
        </li>
        <?php elseif($menu->id == 13): ?>
          <li <?php if($current_route == 'admin/category_management' || $current_route == 'admin/category_management/add' || $current_route == 'admin/category_management/view/{id}' || $current_route == 'admin/category_management/sub_category/list' || $current_route == 'admin/category_management/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="/admin/category_management" >
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT')); ?>

           </a>                   
        </li>
        <?php elseif($menu->id == 16): ?>
          <li <?php if($current_route == 'admin/hall_insurance' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-suitcase"></i>
            <a href="/admin/hall_insurance" >
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE')); ?>

           </a>                   
        </li>
        <li <?php if($current_route == 'admin/ordermanagement'  ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="/admin/ordermanagement" >
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.ORDER_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.ORDER_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.ORDER_MANAGEMENT')); ?>

           </a>                   
        </li>
         <?php elseif($menu->id == 14): ?>


         



          <li <?php if($current_route == 'admin/offer' || $current_route == 'admin/offer/add' || $current_route == 'admin/offer/view/{id}' || $current_route == 'admin/offer/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="/admin/offer" >
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_OFFER_MENU')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OFFER_MENU') : trans($ADMIN_OUR_LANGUAGE.'.BACK_OFFER_MENU')); ?>

           </a>                   
        </li>
         <?php elseif($menu->id == 15): ?>
          <li <?php if($current_route == 'admin/plan' || $current_route == 'admin/plan/add' || $current_route == 'admin/plan/view/{id}' || $current_route == 'admin/plan/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="/admin/plan" >
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PLAN_MENU')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PLAN_MENU') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PLAN_MENU')); ?>

           </a>                   
        </li>
        <?php elseif($menu->id == 4): ?>
         <li <?php if($current_route == 'admin/lead' || $current_route == 'admin/lead/add' || $current_route == 'admin/lead/view/{id}' || $current_route == 'admin/lead/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
        <i class="fa fa-shopping-cart"></i>
            <a href="<?php echo e(url('/admin/lead')); ?>" >
              <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SALES_REP_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_REP_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_REP_MANAGEMENT')); ?>

           </a>                   
        </li>
		<?php if(Session::get('access_group_id') == 6) { ?>
				 <li <?php if($current_route == 'admin/convertedvendor') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
        <i class="fa fa-shopping-cart"></i>
            <a href="<?php echo e(url('/admin/convertedvendor')); ?>" >
              <?php echo e((Lang::has(Session::get('admin_lang_file').'.Subscribed_Vendors')!= '') ?  trans(Session::get('admin_lang_file').'.Subscribed_Vendors') : trans($ADMIN_OUR_LANGUAGE.'.Subscribed_Vendors')); ?>

           </a>                   
        </li>
		<?php } ?>
		
         <?php elseif($menu->id == 17): ?>
          <li <?php if($current_route == 'admin/wallet') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="/admin/wallet" >
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_WALLET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_WALLET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_WALLET')); ?>

           </a>                   
        </li>
         <?php elseif($menu->id == 19): ?>
         <li <?php if($current_route == 'admin/currency' || $current_route == 'admin/currency/add' || $current_route == 'admin/currency/view/{id}' || $current_route == 'admin/currency/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-tasks"></i>
            <a href="/admin/currency" >
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CURRENCY_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CURRENCY_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CURRENCY_MANAGEMENT')); ?>

           </a>                   
        </li>
        <?php elseif($menu->id == 20): ?>
         <li <?php if($current_route == '/admin/setting') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
         <i class="fa fa-cog"></i>
            <a href="/admin/setting">
          <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SETTINGS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS')); ?>

           </a>                   
        </li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
   </div>                
</div>
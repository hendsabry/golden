<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $singer_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.Singer_Info_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Singer_Info_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Singer_Info_Menu')); ?> <?php endif; ?></h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> <?php echo Form::open(array('url' => '/add_singerinfo','method' => 'POST', 'id'=> 'add_singerinfo','enctype' =>'multipart/form-data', 'name' =>'add_singerinfo' )); ?>

            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_Singer_Name'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Singer_Name'); ?> </span> </label>
                <div class="info100">
                  <div class="english">
                    <input name="singer_name" maxlength="70" value="<?php echo e(isset($getSinger->name) ? $getSinger->name : ''); ?>" id="singer_name" required="" type="text">
                  </div>
                  <div  class="arabic ar" >
                    <input class="arabic ar" name="singer_name_ar" maxlength="70" value="<?php echo e(isset($getSinger->name_ar) ? $getSinger->name_ar : ''); ?>" id="singer_name_ar" required="" type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="form_row common_field">
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_Singer_Image'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Singer_Image'); ?> </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div id="file_value2" class="file-value"></div>
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                    </div>
                    <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                    </label>
                    <input type="hidden" name="singer_image_url" value="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>">
                    <?php if(!isset($getSinger->image) ): ?>
                    <input required="" id="company_logo1" name="singer_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" id="singer_image" class="info-file" value="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>" type="file">
                    <?php else: ?>
                    <input  id="company_logo1" name="singer_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" id="singer_image" class="info-file" value="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>" type="file">
                    <?php endif; ?> </div>
                  <div class="error certifications"> </div>
                  <?php if(isset($getSinger->image) && $getSinger->image!='' ): ?> <img src="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>"> <?php endif; ?> </div>
              </div>
            </div>
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_About_Singer'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_About_Singer'); ?> </span> </label>
                <div class="info100">
                  <div class="english">
                    <textarea name="about_singer" id="about_singer" rows="4" cols="50" class="english" maxlength="500"><?php echo e(isset($getSinger->about) ? $getSinger->about : ''); ?></textarea>
                  </div>
                  <div  class="arabic ar" >
                    <textarea class="arabic ar" name="about_singer_ar" id="about_singer_ar" rows="4" cols="50"  maxlength="500"><?php echo e(isset($getSinger->about_ar) ? $getSinger->about_ar : ''); ?></textarea>
                  </div>
                </div>
              </div>
              <div class="form_row_right english">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                    </div>
                    </label>
                    <input type="hidden" name="tnc_url" value="<?php echo e(isset($getSinger->terms_conditions) ? $getSinger->terms_conditions : ''); ?>">
                    <?php if(!isset($getSinger->terms_conditions) ): ?>
                    <input required="" id="company_logo" name="tnc" class="info-file" value="" type="file">
                    <?php else: ?>
                    <input id="company_logo" name="tnc" class="info-file" value="" type="file">
                    <?php endif; ?> </div>
                  <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                  <?php if(isset($getSinger->terms_conditions) && $getSinger->terms_conditions!='' ): ?> <a href="<?php echo e($getSinger->terms_conditions); ?>" target="_blank" class="pdf_icon"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($getSinger->terms_condition_name) ? $getSinger->terms_condition_name : ''); ?></span></a>
                  <input type="hidden" name="tnc_name" value="<?php echo e(isset($getSinger->terms_condition_name) ? $getSinger->terms_condition_name : ''); ?>">
                  <?php endif; ?>
                  <div class="error certifications"> </div>
                </div>
              </div>
              <div class="form_row_right arabic ar">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo11">
                    <div class="file-btn-area">
                      <div id="file_value10" class="file-value"></div>
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                    </div>
                    </label>
                    <input type="hidden" name="tnc_url_ar" value="<?php echo e(isset($getSinger->terms_conditions_ar) ? $getSinger->terms_conditions_ar : ''); ?>">
                    <?php if(!isset($getSinger->terms_conditions_ar) ): ?>
                    <input required="" id="company_logo11" name="tnc_ar" class="info-file" value="" type="file">
                    <?php else: ?>
                    <input id="company_logo11" name="tnc_ar" class="info-file" value="" type="file">
                    <?php endif; ?> </div>
                  <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                  <?php if(isset($getSinger->terms_conditions_ar) && $getSinger->terms_conditions_ar!='' ): ?> <a href="<?php echo e($getSinger->terms_conditions_ar); ?>" target="_blank" class="pdf_icon"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($getSinger->terms_condition_name_ar) ? $getSinger->terms_condition_name_ar : ''); ?></span></a>
                  <input type="hidden" name="tnc_name_ar" value="<?php echo e(isset($getSinger->terms_condition_name_ar) ? $getSinger->terms_condition_name_ar : ''); ?>">
                  <?php endif; ?>
                  <div class="error certifications"> </div>
                </div>
              </div>
            </div>

            <div class="form_row_left common_field">
                    <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?> </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?> </option>

                      <option value="1" <?php  if(isset($getSinger->status) && $getSinger->status==1){?> SELECTED <?php } ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?> </option>
                     <option value="0" <?php if(isset($getSinger->status) && $getSinger->status==0){?> SELECTED <?php } ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?>  </option>
                   </select>
                    
                    </div>
                  </div>
            <div class="form_row">
              <div class="form_row_left">
                <div class="arabic ar">
                  <input type="hidden" name="hid" value="<?php echo e(isset($getSinger->id) ? $getSinger->id : ''); ?>">
                  <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                  <input type="submit" name="submit" value="خضع">
                </div>
                <div class="english">
                  <input type="submit" name="submit" value="Submit">
                </div>
              </div>
              <!-- form_row -->
            </div>
            <?php echo Form::close(); ?>

            <!-- one-call-form -->
            <!-- Display Message after submition -->
            <!-- Display Message after submition -->
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script type="text/javascript">     
$("#add_singerinfo").validate({
                  ignore: [],
                  rules: {
                          singer_name: {
                          required: true,
                          },
                          singer_name_ar: {
                          required: true,
                          },
                           
                        
                                        
                           
                          about_singer: {
                          required: true,
                          },
                          about_singer_ar: {
                          required: true,
                          },
                     <?php if(!isset($getSinger->terms_conditions)): ?>    
                      singer_image: {
                          accept:"png|jpe?g|gif",
                           required: true,
                          },
                                                  
                          tnc: {
                            required: true,
                           accept:"pdf",
                          },  
                                                   
                      <?php else: ?>
                       singer_image: {
                          accept:"png|jpe?g|gif",
                           required: false,
                          },
                             
                      tnc: {
                            required: false,
                           accept:"pdf",
                          },

                      <?php endif; ?>


                       <?php if(!isset($getSinger->terms_conditions_ar)): ?>                         
                          tnc_ar: {
                            required: true,
                           accept:"pdf",
                          },  
                                                   
                      <?php else: ?>
                      tnc_ar: {
                            required: false,
                           accept:"pdf",
                          },

                      <?php endif; ?>       
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                 
           messages: {
                  singer_name: {
                  required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_SINGER_NAME'); ?>",
                  },  
                  singer_name_ar: {
                  required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_SINGER_NAME_ARABIC'); ?>",
                  },

               
                         
                  singer_image: {
                  accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                   required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SINGER_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SINGER_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SINGER_IMAGE')); ?> <?php endif; ?>",
                  }, 
                
                  about_singer: {
                  required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); ?>",
                  },   
                  about_singer_ar: {
                 required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER_ARABIC'); ?> ",
                  },
                   tnc: {
                                required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                                accept: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                        },

                       tnc_ar: {
                                required: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); ?>",
                                accept: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); ?>",
                      },
 
                },
                invalidHandler: function(e, validation){
                      var valdata=validation.invalid;


                   <?php if($mer_selected_lang_code !='en'): ?>

                      if (typeof valdata.singer_name != "undefined" || valdata.singer_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      
                       
                       if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                      {
                      $('.english_tab').trigger('click');    

                      }

                       if (typeof valdata.about_singer != "undefined" || valdata.about_singer != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }
                     
                       if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }

                        if (typeof valdata.singer_name_ar != "undefined" || valdata.singer_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }

                       if (typeof valdata.tnc_ar != "undefined" || valdata.singer_tnc_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }
                    if (typeof valdata.singer_image != "undefined" || valdata.singer_image != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }
                         if (typeof valdata.about_singer_ar != "undefined" || valdata.about_singer_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }


                      <?php else: ?>

                            if (typeof valdata.singer_name_ar != "undefined" || valdata.singer_name_ar != null) 
                            {
                            $('.arabic_tab').trigger('click'); 
                            }
                          

                             if (typeof valdata.tnc_ar != "undefined" || valdata.tnc_ar != null) 
                            {
                            $('.arabic_tab').trigger('click');     

                            }
                              

                             if (typeof valdata.about_singer_ar != "undefined" || valdata.about_singer_ar != null) 
                            {
                            $('.arabic_tab').trigger('click');     

                            }                         

                             if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }
                            if (typeof valdata.singer_image != "undefined" || valdata.singer_image != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }
                             if (typeof valdata.about_singer != "undefined" || valdata.about_singer != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }
                             if (typeof valdata.singer_name != "undefined" || valdata.singer_name != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }



                      <?php endif; ?>

                      },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
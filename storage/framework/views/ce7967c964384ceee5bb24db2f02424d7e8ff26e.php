<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Rosestbig_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SHOP_INFO')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      </header>
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              
             <form name="form1" method="post" id="addbranch" action="<?php echo e(route('store_roses-info')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="mc_name" value="<?php echo e(isset($fetchdata->mc_name) ? $fetchdata->mc_name : ''); ?>" maxlength="255"  
                       value="<?php echo e(isset($getDb->pro_title) ? $getDb->pro_title : ''); ?>" data-validation-length="max35">
                    
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="mc_name_ar" value=" <?php echo e(isset($fetchdata->mc_name_ar) ? $fetchdata->mc_name_ar : ''); ?> "  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">

                  <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); ?> </span> 
                  <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span></a>
                  </label>
                  <div class="info100">
                    <input type="url" name="google_map_address"  data-validation="length required"                     
                     value=" <?php echo e(isset($fetchdata->google_map_address) ? $fetchdata->google_map_address : ''); ?>" data-validation-length="max100">
                     </div>
                </div>

              </div>

              <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->longitude) ? $fetchdata->longitude : ''); ?>" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($fetchdata->latitude) ? $fetchdata->latitude : ''); ?>"  readonly="" name="latitude" />
          </div>
                  </div>
              <div class="form_row">
                <div class="form_row_left common_field">

                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SHOP_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SHOP_IMAGE'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value1"></div>
                    </div>
                    </label>
                    <input type="file" name="branchimage" id="company_logo" class="info-file">
                  </div>
                   <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                  <?php if(isset($fetchdata->mc_img) && $fetchdata->mc_img !=''): ?>
                  <div class="form-upload-img"> <img src="<?php echo e($fetchdata->mc_img); ?>" width="150" height="150">  </div><?php endif; ?>
                  <?php if($errors->has('branchimage')): ?> <span class="error"> <?php echo e($errors->first('branchimage')); ?> </span> <?php endif; ?>
                    </div> 

                
                <!--div class="form_row_right common_field">

                  <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); ?> </span> <a href="javascript:void(0);" class="address_image_tooltip">
               <span class="add_img_tooltip"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_address_img')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_address_img')); ?> <?php endif; ?></span>
              </a>
              </label>
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value2"></div>
                    </div>
                    </label>
                    <input type="file" name="address_image" id="company_logo1" class="info-file">
                  </div>
                   <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                  <div class="form-upload-img"><?php if(isset($fetchdata->address_image) && $fetchdata->address_image !=''): ?>
                   <img src="<?php echo e($fetchdata->address_image); ?>" > <?php endif; ?></div>
                  <?php if($errors->has('address_image')): ?> <span class="error"> <?php echo e($errors->first('address_image')); ?> </span> <?php endif; ?>

                   </div-->
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.BACK_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.BACK_ADDRESS'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="address" maxlength="235"  data-validation="length required" 
     data-validation-error-msg="Please enter hall name" value="<?php echo e(isset($fetchdata->address) ? $fetchdata->address : ''); ?>" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="address_ar" value="<?php echo e(isset($fetchdata->address_ar) ? $fetchdata->address_ar : ''); ?>"  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                 
                  <label class="form_label" > <span class="english"><?php echo lang::get('mer_en_lang.MER_CITY'); ?></span> <span class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CITY'); ?>  </label>
                  <div class="info100" >
                    
                     <select name="city_id" id="status" class="city_type">
             <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?></option>
               <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?> <?php echo e($cbval->co_name); ?> <?php endif; ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <?php $ci_id = $val->ci_id; ?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_ar'; ?>
                        <?php else: ?>
                         <?php $ci_name= 'ci_name'; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php echo e(isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''); ?> ><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
                   
                  </div>

                </div>
              </div>
             
              <div class="form_row">
                 
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription) ? $fetchdata->mc_discription : ''); ?> </textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50"><?php echo e(isset($fetchdata->mc_discription_ar) ? $fetchdata->mc_discription_ar : ''); ?></textarea>
                    </div>
                  </div>
                </div>
                
                <div class="form_row_right english">

                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo7">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value6"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc" id="company_logo7" class="info-file">
                  </div>
                  
                    <?php if(isset($fetchdata->terms_conditions)!=''): ?> 
                    <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                   <div class="pdf_icon">
                   <a href="<?php echo e($fetchdata->terms_conditions); ?>" target="_blank"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?></span></a>
                   
                   </div>
                   <input type="hidden" value="<?php echo e(isset($fetchdata->terms_condition_name) ? $fetchdata->terms_condition_name : ''); ?>" name="tmcvalue">
                   
                       <?php endif; ?> 

                </div>
              </div>
              <div class="arabic ar">
                <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo11">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      <div class="file-value" id="file_value10"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc_ar" id="company_logo11" class="info-file">
                  </div>
                  
                    <?php if(isset($fetchdata->terms_conditions_ar)!=''): ?> 
                    <div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
                   <div class="pdf_icon">
                   <a href="<?php echo e($fetchdata->terms_conditions_ar); ?>" target="_blank"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"><span><?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?></span> </a>
                   
                   </div>
                   <input type="hidden" value="<?php echo e(isset($fetchdata->terms_condition_name_ar) ? $fetchdata->terms_condition_name_ar : ''); ?>" name="tmcvalue_ar">
                   
                       <?php endif; ?> 
                </div>
              </div>
              </div>
               <div class="form_row_left common_field">
                    <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?> </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?> </option>

                      <option value="1" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==1){?> SELECTED <?php } ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?> </option>
                     <option value="0" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==0){?> SELECTED <?php } ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?>  </option>
                   </select>
                    
                    </div>
                  </div>
              <div class="form_row">
              <div class="form_row_left">
              <div class="form-btn-section english">
                   <input type="hidden" name ="parent_id" value="<?php echo e(isset($id) ? $id : ''); ?>">
                       <input type="hidden" name ="itemid" value="<?php echo e(isset($itemid) ? $itemid : ''); ?>">
                <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
              </div>
              <div class="form-btn-section arabic ar">
                <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
              </div>
              </div>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor  --> 
   

<script>
       

 $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },
 
                     
                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       address: {
                       required: true,
                      },
                       address_ar: {
                       required: true,
                      },

                       about: {
                       required: true,
                      },
                       about_ar: {
                       required: true,
                      },
                    address_image: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },

                       <?php if(isset($fetchdata->mc_img) && $fetchdata->mc_img !=''): ?> 
                        branchimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       <?php else: ?>
                        branchimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      <?php endif; ?>
                      
                       <?php if(isset($fetchdata->terms_conditions)!=''): ?> 
                        mc_tnc: {
                           required:false,
                           accept:"pdf",
                      },
                    <?php else: ?>
                         mc_tnc: {
                           required:true,
                           accept:"pdf",
                      },
                      <?php endif; ?>


                       <?php if(isset($fetchdata->terms_conditions_ar)!=''): ?> 
                        mc_tnc_ar: {
                           required:false,
                           accept:"pdf",
                      },
                    <?php else: ?>
                         mc_tnc_ar: {
                           required:true,
                           accept:"pdf",
                      },
                      <?php endif; ?>

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             city_id: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?>",
                      },  

             
                          mc_tnc: {
          
                  accept: "<?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?>",
                 required: "<?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?>",

                      },
           mc_tnc_ar: {
                       required: "<?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file_ar'); ?>",
                         accept: "<?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file_ar'); ?>",
                                 
                      },
                         mc_name: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SHOPNAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SHOPNAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SHOPNAME')); ?> <?php endif; ?>",
                      },   

                          mc_name_ar: {
                  required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOPNAME_AR'); ?>",
                      },   

             

                      address: {
                         required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); ?>",

                       },
                       address_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
                        
                        about: {
                       required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); ?>",  
               
                      },
  
                       about_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); ?>",
                      },
                  branchimage: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },    

                      address_image: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                               


                                    

                      },                               
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                <?php if($mer_selected_lang_code !='en'): ?>
                      if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }
                      if (typeof valdata.address != "undefined" || valdata.address != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.about != "undefined" || valdata.about != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }

                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }
                      if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }
                      

                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                      {

                      $('.arabic_tab').trigger('click');     

                      }
                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');    
                      }
          <?php else: ?>


                      if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');   
                      }
                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                      if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                      if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.description != "undefined" || valdata.description != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.english_tab').trigger('click');
                      }

                      if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                  

<?php endif; ?>

                    },

                submitHandler: function(form) {
                      var mapAdd = jQuery('input[name=google_map_address]').val();
                  if(mapAdd !='')
                  {
                  var long = jQuery('input[name=longitude]').val();
                  var lat  =  jQuery('input[name=latitude]').val();
                  if(long =='' && lat=='')
                  {
                  var allOk = 0;

                  }
                  else
                  {
                  var allOk = 1; 
                  }
                  }
                  else
                  {
                  var allOk = 1;
                  }

                  if(allOk == 1)
                  {
                  form.submit();
                  }
                  else
                  {
                  $('#maperror').html("<span class='error'><?php if(Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.get_Lat_Long_issue')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue')); ?> <?php endif; ?></span>");
                  }
                }
            });

 

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
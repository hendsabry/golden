<?php $__env->startSection('content'); ?>
    <?php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='') {
        $Current_Currency = 'SAR';
        }
    ?>

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2>Hospitality Shop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php echo e($vendordetails->mc_name); ?> </h2>
                                <h2><?php echo e($vendordetails->address); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php echo e($vendordetails->mc_discription); ?> </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                        <?php if($vendordetails->google_map_address!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        <?php }  ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                                <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                        <?php echo e($allreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box"><iframe width="100%" height="315" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <div class="slide-cats owl-carousel" dir="ltr">
                    <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <div class="item-cat">
                        <a href=""><img src="<?php echo e($menus->image); ?>" alt="">
                            <p><?php echo e($menus->attribute_title); ?></p>
                        </a>
                    </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>


                <div class="box p-15">
                    <div class="row">
                        <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($menus->product)  < 1) { continue; } ?>
                                <?php $__currentLoopData = $menus->product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php

                                        if($products->pro_disprice < 1){ $Disprice = $products->pro_price;}else { $Disprice =$products->pro_disprice; }
                                    ?>



                                    <?php

                                        $Disprice = $products->pro_discount_percentage;


                                        $originalP = $products->pro_price;
                                        if($Disprice==''){ $getAmount = 0; }else {$getAmount = ($originalP * $Disprice)/100;}


                                        $DiscountPricea = $originalP - $getAmount;
                                        $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');

                                         if($Disprice >=1) {
                                         $productrealprice=$DiscountPricea;
                                          }else{
                                          $productrealprice=$originalP;
                                          }
                                    ?>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="<?php echo e($products->pro_Img); ?>" alt="" />
                                </div>
                                <div class="caption">
                                    <a href=""><?php echo e($products->pro_title); ?></a>

                                    <p><?php echo e($products->pro_desc); ?></p>
                                    <div class="flex-end">
                                        <div class="price">
                                            <?php if($Disprice >=1): ?>
                                                <?php echo e(currency($products->pro_price,$Current_Currency,'SAR')); ?>

                                                    <?php $resulttotalamount=$DiscountPrice; ?>
                                                    <?php echo e(currency($DiscountPrice,$Current_Currency,'SAR')); ?>

                                            <?php else: ?>
                                                 <?php echo e(currency($products->pro_price,$Current_Currency,'SAR')); ?>

                                                <?php $resulttotalamount=$products->pro_price; ?>
                                            <?php endif; ?>
                                            </div>
                                             <?php echo Form::open(['url' => 'recieption_addtoCart', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

   <input type="hidden" name="product_id" value="<?php echo e(request()->id); ?>">
 <input type="hidden" name="cart_type" value="occasion">
  <input type="hidden" name="language_type" value="en">
   <input type="hidden" name="attribute_id" value="1">
  <input type="hidden" name="cart_sub_type" value="hospitality">
   <input type="hidden" name="tp" id="tp" value="0">
              <input type="hidden" name="werkerP" id="werkerP" value="0">
              <input type="hidden" name="todaydate" id="todaydate" value="<?php echo e(date('jMY')); ?>">
              <input type="hidden" name="product_qty" value="1">
              <input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '')  ?  trans(Session::get('lang_file').'.Add_to_Cart'): trans($OUR_LANGUAGE.'.Add_to_Cart')); ?>" class="form-btn" onclick="return showValidation()"/>
          </form>
                                       <!--  <a href="">Add To Cart</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>

            </div>

            <center>
                <?php echo e($menus->product->links()); ?>

                
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                
            </center>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
<?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
 

<div class="page-left-right-wrapper">
<?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 

<div class="page-right-section">

 <!-- budget-menu-outer -->
<div class="form_title"><?php echo e((Lang::has(Session::get('lang_file').'.KOSHA')!= '')  ?  trans(Session::get('lang_file').'.KOSHA'): trans($OUR_LANGUAGE.'.KOSHA')); ?></div>
<div class="diamond-area">
 
 <?php  $i=1; $j=1;  $getC = $vendor->count(); ?>
 <div class="diamond_main_wrapper">
    <div class="diamond_wrapper_outer">
    <div class="diamond_wrapper_main">
      <div class="diamond_wrapper_inner">

<?php $__currentLoopData = $vendor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php $img = str_replace('thumb_','',$getallcats->mc_img); ?>
<!-- TILL 5 RECORD -->
  <?php if($getC <=5): ?> 
        <div class="row_<?php echo e($i); ?>of<?php echo e($getC); ?> rows<?php echo e($getC); ?>row">
         <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
            <div class="category_wrapper <?php if($getC!=5 && $getC!=4  && $getC!=2): ?> category_wrapper<?php echo e($i); ?> <?php endif; ?>" style="background:url(<?php echo e(isset($img) ? $img : ''); ?>);">
              <div class="category_title <?php if($getC==3): ?> category_title3 <?php endif; ?>"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></div><div class="clear"></div></div>
            </div>
          </a>
        </div>
<!-- TILL 6 RECORD -->
  <?php elseif($getC == 6): ?>
 
          <?php if($i != 3 && $i != 4): ?> 
          <?php if($i==5){ $M=4; } elseif($i==6){ $M=5; }else { $M=$i; } ?>
          <div class="row_<?php echo e($M); ?>of5 rows5row">
          <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
          <div class="category_wrapper" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
          <span class="category_wrapper  <?php if($i==3): ?> category_wrapper2 <?php else: ?> category_wrapper3 <?php endif; ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==4): ?>  <div class="clear"></div>   
         </div>   <?php endif; ?>  
  <?php endif; ?>
<!-- TILL 7 RECORD -->
  <?php elseif($getC == 7): ?>

          <?php if($i != 3 && $i != 4 && $i != 5): ?> 
          <?php if($i==6){ $j = 4;} if($i==7){ $j = 5;}  ?>
          <div class="row_<?php echo e($j); ?>of5 rows5row">
          <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
          <div class="category_wrapper" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
          <span class="category_wrapper  <?php if($i==3): ?> category_wrapper4 <?php elseif($i==4): ?> category_wrapper5 <?php else: ?> category_wrapper6 <?php endif; ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==5): ?>  <div class="clear"></div>   
          </div>   <?php endif; ?>  
  <?php endif; ?>
 <!-- TILL 8 RECORD -->
 <?php elseif($getC == 8): ?>
 
        <?php if($i==1 || $i==8 ): ?>
        <?php if($i==1) { $k = 1;} if($i==8) { $k = 5;}   ?>
       
          <div class="row_<?php echo e($k); ?>of5 rows5row">
          <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
          <div class="category_wrapper category_wrapper1" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></div></div>
          </div>
          </a>   
          </div>
          <?php else: ?>
          <?php if($i==2 ||$i==4 ||$i==6): ?>   <div class="row_3of5 rows5row"> <?php endif; ?>
          <?php if($i==2) { $P = 2;} if($i==3) { $P = 3;}  if($i==4) { $P = 2;}  if($i==5) { $P = 3;}  if($i==6) { $P = 7;}  if($i==7) { $P = 8;}  ?>

          <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
          <span class="category_wrapper category_wrapper<?php echo e($P); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span><span class="clear"></span></span>
          </span>
          </a>
          <?php if($i==3 ||$i==5 ||$i==7): ?>  
          <div class="clear"></div>
          </div><?php endif; ?>
 <?php endif; ?>
  <!-- TILL 9 RECORD -->
<?php elseif($getC == 9): ?>

        <?php if($i==1 || $i==9 ): ?>
        <?php if($i==1){$k=1; } if($i==9){$k=5;   } ?>
        <div class="row_<?php echo e($k); ?>of5 rows5row">
        <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
        <div class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
        <div class="category_title"><div class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></div></div>
        </div>
        </a>   
        </div>
        <?php elseif($i==2 || $i==3 ): ?>
 
        <?php if($i==2): ?> <div class="row_2of5 rows5row"> <?php endif; ?> 
        <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
        <span class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==3): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==4 || $i==5 || $i==6 ): ?>


        <?php if($i==4): ?> <div class="row_3of5 rows5row"> <?php endif; ?> 
        <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
        <span class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==6): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==7 || $i==8 ): ?>

        <?php if($i==7): ?> <div class="row_4of5 rows5row"><?php endif; ?> 
        <a href="<?php echo e(route('kosha-shop-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])); ?>">
        <span class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e(isset($getallcats->mc_img) ? $getallcats->mc_img : ''); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e(isset($getallcats->mc_name) ? $getallcats->mc_name : ''); ?></span><span class="clear"></span></span>
        </span>
        </a>
       
        <?php if($i==8): ?> <div class="clear"></div>
        </div> <?php endif; ?>            
        <?php endif; ?>

<?php endif; ?>

   <!-- END ALL LOOP -->


<?php $i= $i+1; $j=$j+1; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

 </div>
    </div>
    </div>
  </div>
  <div class="diamond_shadow"><img src="<?php echo e(url('/images/shadow.png')); ?>" alt=""></div>





<!-- START PAGINATION -->
 <div class="pagnation-area"><?php echo e($vendor->links()); ?></span></div> 
<!-- END PAGINATION -->

 

</div>

 

 <!-- budget-carousel-area -->


</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

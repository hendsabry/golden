<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Coshabig_leftmenu =1; ?>
<div class="merchant_vendor">
<?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
 <div class="inner">
      <header>
        <?php if(request()->autoid ==''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_CATEGORY')); ?> <?php endif; ?></h5>
          <?php else: ?>
           <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPDATE_CATEGORY')); ?> <?php endif; ?></h5>
          <?php endif; ?>
      </header>
      <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
       <!-- Display Message after submition --> 
            <?php if(Session::has('message')): ?>
            <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
            <?php endif; ?> 
            <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box">             

              <form name="form1" id="menucategory" method="post" action="<?php echo e(route('store-cosha-category')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
				 <div class="form_row_left">
                  
                  <label class="form_label">
                    <span class="english"><?php echo e(lang::get('mer_en_lang.MER_CATEGORY_NAME')); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); ?> </span>
 </label>
                  <div class="info100">
                   <div class="english"> 
                    <input type="text" class="english" name="category_name" maxlength="90" value="<?php echo e(isset($datafirst->attribute_title) ? $datafirst->attribute_title : ''); ?>" id="category_name" required="" >
                     </div>
                    <div class="arabic">
                    <input type="text" class="arabic ar" name="category_name_ar" maxlength="90" value="<?php echo e(isset($datafirst->attribute_title_ar) ? $datafirst->attribute_title_ar : ''); ?>" id="category_name_ar" required="" >
                  </div>
                  </div>
				  </div>
               
          		</div> <!-- form_row -->


 <div class="form_row common_field">
  <div class="form_row_right">
                  
                  <label class="form_label">
                    <?php if(Lang::has(Session::get('mer_lang_file').'.MER_CREATED_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CREATED_IMAGE')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CREATED_IMAGE')); ?> <?php endif; ?>
                </label>
                 <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                      <input id="company_logo" name="mc_img" class="info-file" type="file">
                      <input type="hidden" name="updatemc_img" value="<?php echo e(isset($datafirst->image) ? $datafirst->image : ''); ?>">
                      <?php if(isset($datafirst->image) && $datafirst->image !=''): ?>
                      <div class="form-upload-img">
                       <img src="<?php echo e(isset($datafirst->image) ? $datafirst->image : ''); ?>" width="150" height="150"> 
                      </div>
                       <?php endif; ?>
                    </div>
					</div>
               
              </div> <!-- form_row -->



      <?php if(request()->autoid !=''): ?>
<!-- Add More product images start -->
        <?php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        ?>

        <?php for($i=0;$i < $GalleryCunt;$i++): ?>
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo<?php echo e($k); ?>">
        <div class="file-btn-area">
        <div id="file_value<?php echo e($J); ?>" class="file-value"></div>
        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
        </div>
        </label>
        <input id="company_logo<?php echo e($k); ?>" name="image[]" class="info-file proImgess" data-lbl="file_value<?php echo e($J); ?>" type="file" value="">
        </div>
        <?php if(isset($productGallery[$i]->image) && $productGallery[$i]->image!=''): ?>
        <div class="form-upload-img product_img_del">
        <img src="<?php echo e(isset($productGallery[$i]->image) ? $productGallery[$i]->image : ''); ?>" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="<?php echo e($productGallery[$i]->id); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value<?php echo e($J); ?>"></span>  
        <?php endif; ?>
        <input type="hidden" name="privius[]" value="<?php echo e($productGallery[$i]->id); ?>">
        </div>

        </div>
        <?php  $J=$J + 1; $k=$k +1; ?>
        <?php endfor; ?>
 <?php endif; ?>
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <!--<div id="add_button"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>-->
        <span class="error pictureformat"></span>
        <?php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} ?>                
        <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
        </div>
  <!-- Add More product images end -->



                <div class="form_row">
                <div class="form_row_left">
                <div class="english">
                  <input type="submit" name="submit" value="Submit">
                  <input type="hidden" name="parent_id" value="<?php echo e(isset($id) ? $id : ''); ?>">
                  <input type="hidden" name="sid" value="<?php echo e(isset($sid) ? $sid : ''); ?>">
                  <input type="hidden" name="autoid" value="<?php echo e(isset($autoid) ? $autoid : ''); ?>">
                

                </div>
       			<div class="arabic ar">
                  <input type="submit" name="submit" value="خضع">
             
                </div>
</div></div>

              </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },
                       <?php if(isset($datafirst->image) && $datafirst->image !=''): ?>
                        mc_img: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },
                      <?php else: ?>
                        mc_img: {
                           required:true,
                           accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
                    category_name: {
                    required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_CATEGORY_NAME'); ?>",
                    },  

                    category_name_ar: {
                    required:  "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_CAT_ARABIC_AR'); ?>",
                    },  
                    mc_img: {
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    },    
                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

 <?php if($mer_selected_lang_code !='en'): ?>

                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }    
                    if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }                
                  
                    if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
                  if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }
                  if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  } 
                   if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }   

<?php endif; ?>




                    },

                submitHandler: function(form) {
                  
                    form.submit();
                }
            });

</script>



<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         <?php if(Session::get('lang_file') =='ar_lang'): ?>
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        <?php else: ?>
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        <?php endif; ?>
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1; var kosha = 1;
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-shop-image')); ?>",
        data: {id:id,products:products,kosha:kosha},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
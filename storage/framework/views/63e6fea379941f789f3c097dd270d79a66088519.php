<?php $__env->startSection('content'); ?>
    <?php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='')
        {
        $Current_Currency = 'SAR';
        }
    ?>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2><?php echo e($vendordetails->mc_name); ?></h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php echo e($vendordetails->mc_name); ?></h2>
                                <h2><?php echo e($vendordetails->address); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php echo e($vendordetails->mc_discription); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <?php if($vendordetails->latitude!='' && $vendordetails->longitude!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        <?php }  ?>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                                <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                    </div>

                        <?php echo e($allreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>

                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="desserts">

                <center>
                    <ul class="link">
                        <li class="active">
                            <div class="image"><a href="<?php echo e(url('')); ?>/abayareadymadedetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/abaya-01.png" alt=""></a></div>
                            <h2><a href="<?php echo e(url('')); ?>/abayareadymadedetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.READY_MADE')!= '')  ?  trans(Session::get('lang_file').'.READY_MADE'): trans($OUR_LANGUAGE.'.READY_MADE')); ?></a></h2>
                        </li>
                        <li>
                            <div class="image"><a href="<?php echo e(url('')); ?>/abayatailersdetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/abaya-02.png" alt=""></a></div>
                            <h2><a href="<?php echo e(url('')); ?>/abayatailersdetail/<?php echo e($category_id); ?>/<?php echo e($subcat_id); ?>/<?php echo e($shop_id); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.TAILOR')!= '')  ?  trans(Session::get('lang_file').'.TAILOR'): trans($OUR_LANGUAGE.'.TAILOR')); ?></a></h2>
                        </li>
                    </ul>
                </center>

                <div class="box p-15">
                    <div class="row">
                        <?php $__currentLoopData = $productlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="<?php echo e($getallcats->pro_Img); ?>" alt="" />
                                </div>
                                <div class="caption">
                                    <a href=""><?php echo e($getallcats->pro_title); ?> </a>
                                    <p><?php echo e($getallcats->pro_desc); ?> </p>
                                    <div class="flex-end">
                                        <div class="price">
                                            <?php if(isset($getallcats->pro_disprice) && $getallcats->pro_disprice!='0'){$getPrice = $getallcats->pro_disprice;}else{$getPrice = $getallcats->pro_price;}?>
                                                <?php echo e($getPrice); ?> SAR
                                            </div>
                                        <?php echo Form::open(['url' => 'addtocartforshopping', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>


                                            <input type="hidden" id="category_id" name="category_id" value="<?php echo e($category_id); ?>">
                                            <input type="hidden" id="subcat_id" name="subcat_id" value="<?php echo e($subcat_id); ?>">
                                            <input type="hidden" id="shop_id" name="shop_id" value="<?php echo e($shop_id); ?>">
                                            <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9" />
                                            <input type="hidden" name="actiontype" value="abayareadymadedetail">
                                            <input type="hidden" name="cart_sub_type" value="abaya">
                                            <input type="hidden" name="cart_type" value="shopping">
                                            <input type="hidden" id="product_id" name="product_id" value="<?php if(isset($getallcats->pro_id) && $getallcats->pro_id!=''){echo $getallcats->pro_id;}?>">
                                            <input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>">
                                            <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($getallcats->pro_mr_id) && $getallcats->pro_mr_id!=''){echo $getallcats->pro_mr_id;}?>">
                                           
                                        <input type="submit" name="submit" id="submit"  value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn btn-info-wisitech">

                                        <?php echo Form::close(); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>

            </div>

            <center>
                <?php echo e($productlist->links()); ?>

                
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                
            </center>

        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
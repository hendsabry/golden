<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 

if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 

?>

<script src="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="<?php echo e(url('/')); ?>/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>


<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap ">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($vendordetails->mc_img); ?>" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap ">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
<?php if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !=''): ?>
          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>
      <?php endif; ?>    
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
          <?php } ?>
   
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>

  <!-- common_navbar -->
  <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?>
                <li><img src="<?php echo e(str_replace('thumb_','',$vendordetails->image)); ?>" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></div>
          <div class="detail_hall_description"><?php echo e($vendordetails->address); ?></div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
          <div class="detail_about_hall">
            <div class="comment more"> <?php echo e($vendordetails->mc_discription); ?> </div>
          </div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

<?php if($vendordetails->google_map_address!=''): ?>   
   <div class="detail_hall_dimention"><iframe src="<?php echo e($vendordetails->google_map_address); ?>" width="450" height="230" frameborder="0" style="border:0" allowfullscreen></iframe></div>
 <?php endif; ?>
            
        </div>
      </div>

      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>

      

  <?php if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !=''): ?>

        <div class="service-video-area">
         <?php if($vendordetails->mc_video_description!=''): ?> <div class="service-video-cont">
          <?php if($lang != 'en_lang'): ?>  <?php echo e($vendordetails->mc_video_description_ar); ?> <?php else: ?>  <?php echo e($vendordetails->mc_video_description); ?> <?php endif; ?></div><?php endif; ?>
          <div class="service-video-box">
        <?php if($vendordetails->mc_video_url !=''): ?>    <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> <?php endif; ?>
          </div>
        </div>
<?php endif; ?>

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
                   <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div> 

  <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                
                <li><a href="<?php echo e(route('reception-and-hospitality-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>" <?php if(request()->type!=2): ?> class="select" <?php endif; ?>><?php echo e((Lang::has(Session::get('lang_file').'.Design_your_Package')!= '')  ?  trans(Session::get('lang_file').'.Design_your_Package'): trans($OUR_LANGUAGE.'.Design_your_Package')); ?> </a></li>
               
                <li><a  <?php if(request()->type==2): ?> class="select" <?php endif; ?> href="<?php echo e(route('package-reception-and-hospitality-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')); ?> </a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
         
 
<?php if(count($product) < 1): ?>

<div class="kosha-area">
        <div class="kosha-tab-line">  
          <div id="kosha-tab" class="content">
<?php echo e((Lang::has(Session::get('lang_file').'.No_product_found_in_this_package')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_package'): trans($OUR_LANGUAGE.'.No_product_found_in_this_package')); ?>

          </div> </div> </div>

<?php else: ?>
 
 <div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
       
            <ul>
              <?php $f=1; ?>
              <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li onclick="redy('table<?php echo e($f); ?>','<?php echo e($pro->pro_id); ?>');"><a href="#o" id="<?php echo e($pro->pro_id); ?>" class="redycls <?php if($f==1): ?> select <?php endif; ?>"><?php echo e($pro->pro_title); ?></a></li>
            <?php $f=$f+1; ?>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line --> 
       <div class="rm-kosha-outer">
        <div class="rm-kosha-area">
        <?php $h=1; 
        
        ?> 
		
	  
	<?php
	$D=1;
	?>	
  <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php echo Form::open(['url' => 'recieption_addtoCart', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>	
<?php $nationalityname=Helper::getworkernationalityCountry($pro->staff_nationality) ?>

          <div class="kosha-tab-area redymades" id="table<?php echo e($h); ?>" <?php if($h!=1): ?> style="display: none;" <?php endif; ?>>
            <div class="rm-kosha-name"><?php echo e($pro->pro_title); ?></div>
            <div class="kosha-box">
               <div class="rm-kosha-img"><img src="<?php echo e($pro->pro_Img); ?>" alt="" /></div>
         <div class="rm-kosha-text"><?php echo nl2br($pro->pro_desc); ?></div>
         <div class="rm-kosha-text"><?php echo e((Lang::has(Session::get('lang_file').'.Number_of_Staff')!= '')  ?  trans(Session::get('lang_file').'.Number_of_Staff'): trans($OUR_LANGUAGE.'.Number_of_Staff')); ?> - <?php echo e($pro->no_of_staff); ?>

          <br>
            <?php echo e((Lang::has(Session::get('lang_file').'.Nationality')!= '')  ?  trans(Session::get('lang_file').'.Nationality'): trans($OUR_LANGUAGE.'.Nationality')); ?> - <?php echo e(isset($nationalityname->co_name) ? $nationalityname->co_name : ''); ?> 
         </div>
          <?php if(isset($pro->product_packege) && count($pro->product_packege)>0): ?>
            <div class="rm-kosha-text">
              <div class="packagetitle"> <?php echo e((Lang::has(Session::get('lang_file').'.ITEM(S)')!= '')  ?  trans(Session::get('lang_file').'.ITEM(S)'): trans($OUR_LANGUAGE.'.ITEM(S)')); ?> </div>
              <ul>
                <?php $__currentLoopData = $pro->product_packege; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getpackageitems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php $itemname=Helper::getproducthospitalitytoolNames($getpackageitems->item_id);
                      $pro_name = 'pro_title'; 
                      if(Session::get('lang_file')!='en_lang')
                    {
                        $pro_name = 'pro_title_ar'; 
                      }
                 ?>
                    <li>
                      <?php if(isset($itemname->pro_Img) && $itemname->pro_Img!=''){ ?>
                      <img src="<?php echo e($itemname->pro_Img); ?>" width="50px;">
                      <?php } ?>
                       <?php echo e($itemname->$pro_name); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php if($pro->pro_qty>0): ?>
<div class="package_qty">
 <div id="service_quantity_box" class="service_quantity_box">
      <div class="service_qunt"><?php if(Lang::has(Session::get('lang_file').'.Quantity')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Quantity')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Quantity')); ?> <?php endif; ?> </div>
        <div class="service_qunatity_row">
          
        <div data-title="Total Quality" class="td td2 quantity food-quantity">
        <div class="quantity">
    <button onclick="return pricecalculation('remove','<?php echo e($h); ?>');" class="sub" id="sub" type="button"></button>
    <input type="number" onkeydown="isNumberKey(event); pricecalculation('pricewithqty','<?php echo e($h); ?>');" onkeyup="isNumberKey(event); pricecalculation('pricewithqty','<?php echo e($h); ?>');" max="9" min="1" value="1" id="qty_<?php echo e($h); ?>" name="product_qty">
    <button onclick="return pricecalculation('add','<?php echo e($h); ?>');" class="add" id="add" type="button"></button></div>

 
          
       </div>
      </div>
    </div>

</div>
<?php endif; ?>

         <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise"><?php echo e((Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')); ?>: 
              <?php
                $Disprice = $pro->pro_discount_percentage; 
               $originalP = $pro->pro_price;
                if($Disprice==''){  $getAmount = 0;  }  else   {$getAmount = ($originalP * $Disprice)/100;}
               
              $DiscountPricea = $originalP - $getAmount; 
              $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');



              ?>
              <?php if($Disprice >=1): ?>  
              <input type="hidden" name="orignalP" id="orignalP_<?php echo e($h); ?>" value="<?php echo e(currency($DiscountPrice, 'SAR',$Current_Currency, $format = false)); ?>">
               <span class="strike">  <?php echo e(currency($originalP,'SAR',$Current_Currency)); ?> </span> <span id="tp_<?php echo e($h); ?>"><?php echo e(currency($DiscountPrice,'SAR',$Current_Currency)); ?> </span>
              <?php else: ?>
              <input type="hidden" name="orignalP" id="orignalP_<?php echo e($h); ?>"  value="<?php echo e(currency($originalP, 'SAR',$Current_Currency, $format = false)); ?>">
              <span id="tp_<?php echo e($h); ?>"><?php echo e(currency($originalP,'SAR',$Current_Currency)); ?>  </span>
              <?php endif; ?>
              </div>
                
              <!------new form------>
			  <div class="package_box_top">
                <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Date')!= '')  ?  trans(Session::get('lang_file').'.Date'): trans($OUR_LANGUAGE.'.Date')); ?> </div>
                  <div class="checkout-form-bottom">
                    <input class="t-box cal-t" type="text" id="bookingdate_<?php echo e($D); ?>" name="bookingdate">
					<span id="showerrorbookingdate_<?php echo $D;?>" class="error"></span>
                  </div>
                </div>
                <!-- checkout-form-cell -->
                <div class="checkout-form-cell">
                  <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Time')!= '')  ?  trans(Session::get('lang_file').'.Time'): trans($OUR_LANGUAGE.'.Time')); ?> </div>
                  <div class="checkout-form-bottom">                   
                  <input class="t-box sel-time" type="text" id="bookingtime_<?php echo e($D); ?>" name="bookingtime">
				  <span id="showerrorbookingtime_<?php echo $D;?>" class="error"></span>
                  </div>
                </div>
                <!-- checkout-form-cell -->
              </div>
              <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')); ?> </div>
                  <div class="checkout-form-bottom">
                    <input class="t-box" type="text" id="location_<?php echo $D;?>" name="location">
					<span id="showerrorlocation_<?php echo $D;?>" class="error"></span>
                  </div>
                </div>
                
                
                <!-- checkout-form-cell -->
              </div>
              <!---------- end new form-------->
			  </div>




              <div class="kosha-button-area">
              <input type="hidden" name="product_id" id="product_id" value="<?php echo e($pro->pro_id); ?>"> 
              <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
              <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
              <input type="hidden" name="vid" value="<?php echo e(request()->vid); ?>">
              <input type="hidden" name="cart_type" value="occasion">
              <input type="hidden" name="language_type" value="en">
              <input type="hidden" name="attribute_id" value="1">
              <input type="hidden" name="cart_sub_type" value="hospitality">
              <input type="hidden" name="nationality" value="23">
              <input type="hidden" name="products[]" value="">
              <input type="hidden" name="time" value="">
              <input type="hidden" name="date" value="">
              <input type="hidden" name="p_total_price" value="<?php echo e(currency($pro->pro_price, 'SAR',$Current_Currency, $format = false)); ?>">
 
                <?php if($pro->pro_qty>0): ?>

               <input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn" onclick="return showValidation_<?php echo $D;?>()"/>
               <?php else: ?>
                 <span id="sold" class="form-btn addto_cartbtn" ><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></span>
               <?php endif; ?>
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
       

 
 
 <script>
 
 
 
 
            // When the document is ready
            //var jql = jQuery;
            jQuery(window).load(function () {
            jQuery('#bookingdate_<?php echo $D;?>').datepicker({
            format: "d M yyyy",
            startDate: new Date(),
            }); 
            jQuery("#bookingdate_<?php echo $D;?>").datepicker("setDate", new Date());
 
            var starttime=jQuery('#opening_time').val();     
            var endtime=jQuery('#closing_time').val();
            var service_duration=jQuery('#service_duration_<?php echo $D;?>').val();
            var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A'); 
            jQuery('#bookingtime_<?php echo $D;?>').timepicker({
            'minTime': starttime,
            'maxTime': netime,
            'dynamic': true,
            'timeFormat': 'g:i A',          
            });
            });
			
			
 </script>
  <script type="text/javascript">
 function showValidation_<?php echo $D;?>()
{
  var bookingdate_<?php echo $D;?> = document.getElementById("bookingdate_<?php echo $D;?>").value;
  var bookingtime_<?php echo $D;?> = document.getElementById("bookingtime_<?php echo $D;?>").value;
  var location_<?php echo $D;?> = document.getElementById("location_<?php echo $D;?>").value;
  if(bookingdate_<?php echo $D;?> == '') 
  {
   document.getElementById('showerrorbookingdate_<?php echo $D;?>').innerHTML = '<span style="color:red;"><?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MER_SELECT_NATIONALITY')); ?> <?php endif; ?></span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorbookingdate_<?php echo $D;?>').innerHTML = '';
  }
  if(bookingtime_<?php echo $D;?> == '') 
  {
   document.getElementById('showerrorbookingtime_<?php echo $D;?>').innerHTML = '<span style="color:red;"><?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_TIME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MER_SELECT_NATIONALITY')); ?> <?php endif; ?></span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorbookingtime_<?php echo $D;?>').innerHTML = '';
  }
  if(location_<?php echo $D;?> == '') 
  {
   document.getElementById('showerrorlocation_<?php echo $D;?>').innerHTML = '<span style="color:red;"><?php if(Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_LOCATION')); ?> <?php endif; ?></span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorlocation_<?php echo $D;?>').innerHTML = '';
  }
}
 
        </script>
  <?php $h=$h+1;  	$D=$D+1; ?>
   </form>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div> <!-- rm-kosha-area -->
          </div> <!-- rm-kosha-outer -->
      </div>  <!-- kosha-area -->
<?php endif; ?>
 
  

 <!--service-display-section-->
    <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>




<!------------ end tabs------------------>
<!--------- date picker script-------->


<script type="text/javascript">
  
function pricecalculation(str,ps)
{
  
var productId        = $('#product_id').val();
  if(str =='remove')
  {
   var Qty = parseInt($('#qty_'+ps).val())-1; 
  }
  else
  {
   var Qty = parseInt($('#qty_'+ps).val())+1;  
  }
if(Qty == 0){ return false;}

var currentquantity=$('#qty_'+ps).val();
 
$.ajax({
       type:"GET",
       url:"<?php echo e(url('checkcoshaquantity')); ?>?product_id="+productId+'&qty='+Qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();       
         var qtyupdated = parseInt(currentquantity)-1;
          $('#qty_'+ps).val(qtyupdated);
        
       }
       else
       {
           var  totalp = $('#orignalP_'+ps).val()
           var TP = parseInt(Qty)*parseFloat(totalp);
           TP = parseFloat(TP).toFixed(2);
           <?php $Cur = Session::get('currency'); ?>
          $('#tp_'+ps).html('<?php echo $Cur; ?> '+TP);
        
       }
       }
    });


}

</script>


 <script type="text/javascript">  
 

$('.Checktype').click(function()
 {
var price = $(this).data('price');
var desinp = $('#designprice').val();
var totalp = $('#totalp').val();
 var Qty = $('#qty').val();
var Gtotal = parseInt(Qty) * (parseFloat(price) + parseFloat(desinp) + parseFloat(totalp));
var Gtotals =  parseFloat(price) + parseFloat(desinp) + parseFloat(totalp);
$('#typeprice').val(price);
 Gtotal = parseFloat(Gtotal).toFixed(2);
 <?php $Cur = Session::get('currency'); ?>
 

$('#gtotal').html('<?php echo $Cur; ?> '+Gtotal);
$('#add_total_price').val(Gtotals);
});

$('.Checkdesign').click(function()
 {
var price = $(this).data('price');
var desinp = $('#typeprice').val();
var totalp = $('#totalp').val();
 var Qty = $('#qty').val();
var Gtotal =parseInt(Qty) * (parseFloat(price) + parseFloat(desinp) + parseFloat(totalp));
var Gtotals = parseFloat(price) + parseFloat(desinp) + parseFloat(totalp);
 Gtotal = parseFloat(Gtotal).toFixed(2);
  <?php $Cur = Session::get('currency'); ?>
$('#gtotal').html('<?php echo $Cur; ?> '+Gtotal);
$('#designprice').val(price); 
$('#add_total_price').val(Gtotals);
});




 $('.selct').click(function()
 {
  var title =  $(this).data('title');
  var from =  $(this).data('from');
  var price =  $(this).data('price');  
  var img =   $(this).data('img');
  var pid =   $(this).data('id');
  var attribute =   $(this).data('attribute');
  $(".kosha-selections-area").show();  
  $(".info_"+from).html('');
  $(".info_"+from).append('<div class="selection-box"><div class="selection-img"><img src="'+img+'" alt="" /></div><div class="selection-name">'+title+'<input type="hidden" class="tprice" name="itemprice" value="'+price+'"><input type="hidden" name="product[attribute_id][]" value="'+attribute+'"><input type="hidden" name="product[pro_id][]" value="'+pid+'"></div><div class="selection-prise">SAR '+price+'</div></div>');
    var totalP = 0;
    var getP = 0;
    $( ".tprice" ).each(function() {
    getP =  $(this).val();
    totalP = parseFloat(totalP) + parseFloat(getP);
    }); 

   totalP = parseFloat(totalP).toFixed(2)
 <?php $Cur = Session::get('currency'); ?>
$("#totalPrice").html('<?php echo $Cur; ?> '+ totalP); 
  });

 

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
 
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
 $('.category_wrapper').click(function(){
var Pro_id = $(this).data('pid');

if(Pro_id !='')
{

$.ajax({
     type:"GET",
     url:"<?php echo e(url('getsingleProductInfo')); ?>?product_id="+Pro_id,
     success:function(res)
          {             
            var Info = res.split("~~~");
            var ProName = Info[0];
            var OriginalP = Info[1];
            var Disp = Info[2];
            var pro_Img = Info[3];         
            $('#pname').html(ProName);
            <?php $Cur = Session::get('currency'); ?>
            $('#oprice').html('<?php echo $Cur; ?> '+OriginalP);
            $('#disprice').html('<?php echo $Cur; ?> '+Disp);
            $('.proimg').attr('src',pro_Img);
            $('#totalp').html('<?php echo $Cur; ?> '+Disp);
            $('.Checktype').removeAttr('checked');
            $('.Checkdesign').removeAttr('checked');
            $('#gtotal').html('<?php echo $Cur; ?> '+Disp);
            $('#add_product_id').val(Pro_id);
            $('#add_total_price').val(Disp); 
            $('#DispriceTotal').html('<?php echo $Cur; ?> '+Disp); 
            $('#totalp').val(Disp); 
            $('#typeprice,#designprice').val(0); 
            $('#qty').val(1);
                   
           }
  });


}

});
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
 
 
<script language="javascript">
$('.add').click(function () { 

 

    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 <?php if(request()->type!=''): ?> 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 <?php endif; ?>
 <script type="text/javascript">
   function isNumberKey(evt) {  

        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
       evt.preventDefault();
        } else {
            return true;
        }
    }
 
 </script>
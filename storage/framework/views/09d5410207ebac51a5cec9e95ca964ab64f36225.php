<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="UTF-8" />
<title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="_token" content="<?php echo csrf_token(); ?>"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
<?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
<?php endif; ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/switch.css" />
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
<?php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
<link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>

<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

<!-- MAIN WRAPPER -->
<div id="wrap" >
<!-- HEADER SECTION --> 
<?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!-- END HEADER SECTION --> 
<?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<!--PAGE CONTENT -->
<div class=" col-sm-9 col-xs-12 right-side-bar">
  <div class="right_col" role="main">
    <div id="appElectInviteList">
      <div class="clearfix"></div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="page-title">
          <div class="top_back_btn padd-left-right">
                  <div class="title-block">
                  <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_SMS_MSG_LIST')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_SMS_MSG_LIST') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_SMS_MSG_LIST')); ?></h3>
                  </div>
                  <div class="back-btn-area "> <a href="/admin/designcard/invitation" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a> </div>
              </div>
            <div class="col-md-12 col-sm-12 col-xs-12 hight_overhid">
              <!-- <div class="col-md-3 col-sm-3 col-xs-12">
                <input id="from_date"  placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_DATE')); ?>" class="form-control" type="text">
              </div> -->
             <!--  <div class="col-md-3 col-sm-3 col-xs-12">
                <select v-model="event_type" class="form-control dtmagin">
                  <option value="" selected="selected"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_OCASSION_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_OCASSION_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_OCASSION_TYPE')); ?></option>
                  <option  value="1"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_BUSINESS_MEETING')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_BUSINESS_MEETING') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_BUSINESS_MEETING')); ?></option>
                  <option  value="2"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_WEDDING_&_OTHER_OCASSION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_WEDDING_&_OTHER_OCASSION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_WEDDING_&_OTHER_OCASSION')); ?></option>
                </select>
              </div> -->
              <!-- <div class="col-md-6 col-sm-12 col-xs-3">
                <button type="submit" :disabled="isProcessing" @click="search" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                <button type="reset" :disabled="isProcessing" @click="clearFilter" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
              </div> -->
            </div>
          </div>
          <div class="x_content ElectInviteList"> 
            <!-- <a href="<?php echo e(URL::asset('/admin/electronic_invitation/add')); ?>" class="btn btn-default broadcast-btn theme-default-btn margnright"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_ADD_NEW_DESIGNER_CARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_ADD_NEW_DESIGNER_CARD') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_ADD_NEW_DESIGNER_CARD')); ?></a> -->
            <div class="table-responsive">
              <table class="table table-striped jambo_table bulk_action ">
                <thead >
                  <tr class="headings">
                    <th class="column-title"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_S_No')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_S_No') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_S_No')); ?> </th>
                    <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PACKAGE_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PACKAGE_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_PACKAGE_NAME')); ?> </th>
                    <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_PRICE')); ?></th>
                    <!-- <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE_DISCOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE_DISCOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_PRICE_DISCOUNT')); ?></th>-->
                    <th class="column-title no-link last lastmodified"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_OCASSION_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_OCASSION_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_OCASSION_TYPE')); ?></span> </th>
                    <th class="column-title no-link last "><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_LAST_MODIFIED_ON')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_LAST_MODIFIED_ON') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_LAST_MODIFIED_ON')); ?></span> </th>
                    <th class="column-title no-link last text-center"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?></span> </th>
                    <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_ACTION')); ?></span> </th>
                  </tr>
                  <input id="lang" class="form-control" value="<?php echo e(Session::get('admin_lang_code')); ?>" type="hidden">
                </thead>
                <tbody >
                  <tr v-if="elect_invite" v-for="(ud, index) in elect_invite" class="all_content">
                    <td class="numrical_1">{{ index+1 }}</td>
                    <td class="package_name"> {{ ud.pro_title }}</td>
                    <td class="price_list_1">(SAR) {{ ud.pro_price }}</td>
                    <!-- <td class="price_list_1"> {{ ud.pro_discount }}</td>-->
                    <td class="occasion_type">{{ ud.event_type }}</td>
                    <td >{{ ud.created }}</td>
                    <td class="status_type"><div>
                                <label class="switch" >
                                 <input class="example2" type="checkbox" v-bind:id="ud.pro_id" v-bind:checked="ud.pro_status == 1" @click="statusChanged(ud.pro_id)">
                                  <div class="slider round"></div>
                                </label>
                                </div></td>
                    <td class="last">

                      <!--button  @click="view(ud.pro_id)" class="btn view-delete"><i class="fa fa-eye"></i> </button-->
                    <button  @click="Edit(ud.pro_id)" class="btn view-delete"><i class="fa fa-edit"></i> </button><!-- <button  @click="deletecard(ud.pro_id)" class="btn view-delete"><i class="fa fa-trash-o"></i> </button></td> -->
                  </tr>
                  <tr v-if="elect_invite ==''">
                    <td colspan="7"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="paging">
            <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
                <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
@click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
                <li v-for="page in pagesNumber"
v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
@click.prevent="changePage(page)">{{ page }}</a> </li>
                <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
@click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--END MAIN WRAPPER --> 

<!-- FOOTER --> 
<?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<!--END FOOTER --> 

<!-- GLOBAL SCRIPTS --> 

<script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script> 
<script type="text/javascript">
  $(window).on('load',function(){
    $('#from_date').datepicker({
      format: "MM dd, yyyy",
      autoclose: true,
      todayHighlight: true    
    });

    $('#from_date').on('change',function(){
      var dated = $(this).val();
//$('#from_date').val(dated);
//$('#fill_date').val(dated);

});
  });
</script> 
<!-- END GLOBAL SCRIPTS --> 

<!-- PAGE LEVEL SCRIPTS --> 

<!-- END PAGE LEVEL SCRIPTS --> 
<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script> 
<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script> 
<script type="text/javascript">
  window._elect_invite = [<?php echo $elect_invite->toJson(); ?>];
</script> 
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/sms_mail.js"></script>
</body>

<!-- END BODY -->
</html>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
 
if($Current_Currency =='') { 
  $Current_Currency = 'SAR'; 
 } 
 
  ?>
<div class="outer_wrapper">  
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.css" />
    <div class="vendor_header">
      <div class="inner_wrap">
        <div class="vendor_header_left">
          <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($singerDetails->image); ?>" alt="" /></a></div>
        </div>
        <!-- vendor_header_left -->
        <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- vendor_header_right -->
      </div>
    </div>
    <!-- vemdor_header -->
    <div class="common_navbar">
      <div class="inner_wrap">
        <div id="menu_header" class="content">
          <ul>
            <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
              <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>

            <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>

<li><a href="#choose_package"><?php echo e((Lang::has(Session::get('lang_file').'.choose_items')!= '')  ?  trans(Session::get('lang_file').'.choose_items'): trans($OUR_LANGUAGE.'.choose_items')); ?></a></li>


            <?php } if(count($allreview) > 0){ ?>
            <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
			<?php } ?>
            <!--<li><a href="#choose_package"><?php echo e((Lang::has(Session::get('lang_file').'.Choose_Package')!= '')  ?  trans(Session::get('lang_file').'.Choose_Package'): trans($OUR_LANGUAGE.'.Choose_Package')); ?></a></li>-->
          </ul>
        </div>
      </div>
    </div>
    <!-- common_navbar -->



    <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
	 
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?>
                <li><img src="<?php echo e(str_replace('thumb_','',$singerDetails->image)); ?>" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">		
          <div class="detail_title">
            <?php
		    $name = 'name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $name = 'name_ar'; 
		    }
		    echo $singerDetails->$name;
		  ?>
          </div>
          <div class="detail_hall_description">
            <?php
		    $address = 'address'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $address = 'address_ar'; 
		    }
		    echo $singerDetails->$address;
		  ?>
          </div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_ACOUSTIC')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_ACOUSTIC'): trans($OUR_LANGUAGE.'.ABOUT_ACOUSTIC')); ?></div>
          <div class="detail_about_hall">
            <div class="comment more"> <?php
              ?>
              <?php $about='about'?>
              <?php if(Session::get('lang_file')!='en_lang'): ?>
              <?php $about= 'about_ar'; ?>
              <?php endif; ?>
              <?php echo nl2br($singerDetails->$about); ?> </div>
          </div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CTIY')); ?>: <span>
            <?php
		    $getcityname = Helper::getcity($singerDetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?>
            </span></div>

            <?php 
            $long  = @$singerDetails->longitude;
            $lat = @$singerDetails->latitude;
            ?>
            <?php if($long !='' && $lat!=''): ?>
                <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;"> </div>
            <?php endif; ?>
 
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
	  <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>
        <div class="service-video-area">
          <div class="service-video-cont">
            <?php 
    		    $mc_name = 'video_description'; 
    		    if(Session::get('lang_file')!='en_lang')
    			{
    		      $mc_name = 'video_description_ar'; 
    		    }
    		    echo $singerDetails->$mc_name; 
    		  ?>
          </div>		  
          <div class="service-video-box">
            <iframe class="service-video" src="<?php echo e($singerDetails->video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <!-- service-video-area -->
        <?php } if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } /*else{ ?>
		<div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
		<?php } */?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                 <li><a href="<?php echo e(route('acousticequipment',[$id,$sid,$lid])); ?>" class="select"><?php echo e((Lang::has(Session::get('lang_file').'.EQUIPMENT')!= '')  ?  trans(Session::get('lang_file').'.EQUIPMENT'): trans($OUR_LANGUAGE.'.EQUIPMENT')); ?></a></li>

                <li><a href="<?php echo e(route('acousticrecording',[$id,$sid,$lid])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')); ?></a></li>
               
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
   <?php  $k=count($getAllProduct); if($k>0){ ?>
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> <?php  $i=1; ?>
               
                <?php if($k<6){ ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $getAllProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                      <?php
$Img = str_replace('thumb_','',$getallcats->pro_Img);
                          ?>


                  <div class="row_<?php echo e($i); ?>of<?php echo e($k); ?> rows<?php echo e($k); ?>row"> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                    <div class="category_wrapper <?php if($k==3): ?> category_wrapper<?php echo e($k); ?> <?php endif; ?>" style="background:url(<?php echo e(isset($Img) ? $Img : ''); ?>);">
                      <div class="category_title <?php if($k==3): ?> category_title3 <?php endif; ?>">
                        <div class="category_title_inner">
                          <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                        </div>
                      </div>
                    </div>
                    </a> </div>
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 6th-------------->
                <?php }elseif($k==6){ ?>
                <?php $j=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $getAllProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } ?>
                  <?php if($j==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($j==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($j==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($j==5){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($j==6){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                                </div>
                              </div>
                            </div>
                            </a> <?php if($j==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($j==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($j==4){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($j==5){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($j==6){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $j=$j+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 7th-------------->
                <?php }elseif($k==7){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $getAllProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } ?>
                  
                  <?php if($l==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($l==3){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?>
                          <?php if($l==7){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                                </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==2){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==6){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==7){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <!------------ 8th-------------->
                <?php }elseif($k==8){ ?>
                <?php $l=1; ?>
                <div class="diamond_wrapper_inner"> <?php $__currentLoopData = $getAllProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($l==2 || $l==4){  $classrd='category_wrapper2';  } ?>
                  <?php if($l==3 || $l==5){  $classrd='category_wrapper3';  } ?>
                  <?php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } ?>
                  
                  <?php if($l==1){ $classrd='category_wrapper1'; ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($l==2){ ?>
                    <div class="row_3of5 rows5row"> <?php } ?> 
                      <?php if($l==4){  ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($l==6){ ?>
                        <div class="row_3of5 rows5row"> <?php } ?>
                          <?php if($l==8){ $classrd='category_wrapper9'; ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');">
                            <div class="category_wrapper <?php echo e($classrd); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                                </div>
                              </div>
                            </div>
                            </a> <?php if($l==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($l==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($l==5){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($l==7){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?>
                    <?php if($l==8){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?>
                  <?php $l=$l+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                
                <?php }elseif($k==9){ ?>
                <div class="diamond_wrapper_inner"> <?php $i=1; ?>
                  <?php $__currentLoopData = $getAllProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($i==1) { $k=9; }else{ $k=$i;} ?>
                  
                  
                  <?php if($i==1){ ?>
                  <div class="row_1of5 rows5row"> <?php } ?> 
                    <?php if($i==2){ ?>
                    <div class="row_2of5 rows5row"> <?php } ?> 
                      <?php if($i==4){ ?>
                      <div class="row_3of5 rows5row"> <?php } ?> 
                        <?php if($i==7){ ?>
                        <div class="row_4of5 rows5row"> <?php } ?> 
                          <?php if($i==9){ ?>
                          <div class="row_5of5 rows5row"> <?php } ?> <a href="#" onclick="return getdatedish('<?php echo e($getallcats->pro_id); ?>');"> <span class="category_wrapper category_wrapper<?php echo e($k); ?>" style="background:url(<?php echo e(isset($getallcats->pro_Img) ? $getallcats->pro_Img : ''); ?>);"> <span class="category_title"><span class="category_title_inner">
                            <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                            </span></span> </span> </a> <?php if($i==1){ ?>
                            <div class="clear"></div>
                          </div>
                          <?php } ?> 
                          <?php if($i==3){ ?>
                          <div class="clear"></div>
                        </div>
                        <?php } ?> 
                        <?php if($i==6){ ?>
                        <div class="clear"></div>
                      </div>
                      <?php } ?> 
                      <?php if($i==8){ ?>
                      <div class="clear"></div>
                    </div>
                    <?php } ?> 
                    <?php if($i==9){ ?>
                    <div class="clear"></div>
                  </div>
                  <?php } ?> 
                  
                  <?php $i=$i+1; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                <?php } ?> </div>
            </div>
          </div>
          <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
		  <div align="center"> <?php echo e($getAllProduct->links()); ?></div>
        </div>
   <?php   $rentPrice = Helper::getProductByAttributeValue($getAllProduct[0]->pro_id); ?>
   <?php echo Form::open(['url' => 'addcartsingerproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

   
	<div class="service-display-left"><span id="selectedproduct">
	 <b><?php echo e(Session::get('status')); ?></b>
	<div class="service-left-display-img product_gallery">
 
  <?php    $pro_id = $getAllProduct[0]->pro_id; ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


  </div>
	<div class="service-product-name"><?php echo e($getAllProduct[0]->pro_title); ?> <?php echo e($getAllProduct[0]->pro_qty); ?></div>
	<div class="service-product-description"><?php echo e($getAllProduct[0]->pro_desc); ?></div>
<?php if($getAllProduct[0]->notes!=''): ?>
	<p><em><strong><?php echo e((Lang::has(Session::get('lang_file').'.NOTES')!= '')  ?  trans(Session::get('lang_file').'.NOTES'): trans($OUR_LANGUAGE.'.NOTES')); ?>:</strong>
              <?php $mc_notes='notes';if(Session::get('lang_file')!='en_lang'){$mc_notes= 'notes_ar';}echo $getAllProduct[0]->$mc_notes; ?>
     </em></p>
<?php endif; ?>


	</span>




	<div class="service-radio-line">
    <div class="leftbar_title"><?php echo e((Lang::has(Session::get('lang_file').'.SHOP_BY')!= '')  ?  trans(Session::get('lang_file').'.SHOP_BY'): trans($OUR_LANGUAGE.'.SHOP_BY')); ?></div>
	

  <div class="service-radio-box">
	<input type="radio" id="kilo" rel="buy" name="productoptionval"   <?php if(count($rentPrice) < 1) { ?> CHECKED <?php } ?> value="58" onclick="selectedweight(this.value,'buy');" /> <label for="kilo"><?php echo e((Lang::has(Session::get('lang_file').'.BUY')!= '')  ?  trans(Session::get('lang_file').'.BUY'): trans($OUR_LANGUAGE.'.BUY')); ?></label>
	</div>
   

 <?php if(count($rentPrice) < 1) { ?>
<script type="text/javascript">
   $(document).ready(function(){
selectedweight('58','buy');
   

   })

</script>


 <?php } ?>


 <?php
   //print_r($rentPrice);die;
 ?>
	
	<div class="service-radio-box rentprice" <?php if(count($rentPrice) <1) { ?> style="display:none;" <?php } ?> >
	<input type="radio" rel="rent" id="piece" name="productoptionval" value="<?php echo e($getAllProduct[0]->pro_id); ?>" onclick="selectedweight(this.value,'rent');"/> <label for="piece"><?php echo e((Lang::has(Session::get('lang_file').'.RENT')!= '')  ?  trans(Session::get('lang_file').'.RENT'): trans($OUR_LANGUAGE.'.RENT')); ?></label>
	</div>
 
    
     <!-----case buy------>
 
  <?php
if($getAllProduct[0]->pro_disprice>0){
  $sellingprice=$getAllProduct[0]->pro_disprice;
  
}else{
  $sellingprice=$getAllProduct[0]->pro_price;
 
}
?>
      <div class="service_quantity_box" id="service_quantity_box" style="display: none;">
    	<div class="service_qunt"><?php echo e((Lang::has(Session::get('lang_file').'.QUANTITY')!= '')  ?  trans(Session::get('lang_file').'.QUANTITY'): trans($OUR_LANGUAGE.'.QUANTITY')); ?></div>
        <div class="service_qunatity_row">
        	
        <div class="td td2 quantity food-quantity" data-title="Total Quality">
        <div class="quantity">
		<button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
		<input type="number" name="itemqty" id="qty" value="1" min="1" max="9" readonly />
		<button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button></div>
		<span class="service_qunt_price" id="p58" style="display: none;">
<?php if($getAllProduct[0]->pro_disprice>0){ ?>
      <span class="strike"><?php echo e(currency($getAllProduct[0]->pro_price,'SAR',$Current_Currency)); ?> </span><span>

      <?php echo e(currency($sellingprice,'SAR',$Current_Currency)); ?></span>
<?php } else { ?>
     <span> <?php echo e(currency($sellingprice,'SAR',$Current_Currency)); ?></span>
<?php } ?>
  </span>
            
        	<?php
         
           if(!empty($rentPrice)){
                if($rentPrice[2]->discount_price>0){
                $rentedprice=$rentPrice[2]->discount_price;
  
                  }else{
                     $rentedprice=$rentPrice[0]->price;
 
                  }

            ?>
			<span class="service_qunt_price" id="p59" style="display: none;">
          <?php if($rentPrice[2]->discount_price>0){ ?>
      <span class="strike"><?php echo e(currency($rentPrice[0]->price,'SAR',$Current_Currency)); ?></span><span><?php echo e(currency($rentedprice,'SAR',$Current_Currency)); ?></span>
<?php } else { ?>
     <span>  <?php echo e(currency($rentedprice,'SAR',$Current_Currency)); ?> </span>
<?php } ?>
 

        <?php echo e((Lang::has(Session::get('lang_file').'.PER_OURR')!= '')  ?  trans(Session::get('lang_file').'.PER_OURR'): trans($OUR_LANGUAGE.'.PER_OURR')); ?>

		
		<br /><br />
		
			
			
		
		</span>
            <?php } ?>
			
		</div>
		</div>
		<span id="hiderentinsurence" style="display:none;"><?php if(isset($getAllProduct[0]->Insuranceamount) && $getAllProduct[0]->Insuranceamount!=''){ ?>
			<b><?php echo e((Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')); ?>: </b>
			<span id="showRentIn"><?php echo  currency($getAllProduct[0]->Insuranceamount,'SAR',$Current_Currency); ?></span>
			<?php } ?></span>
    </div>
	</div>	<!-- service-radio-line -->






<div>&nbsp;</div>
    <div id="soldoutnow" class="form-btn addto_cartbtn" style="display:none"><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></div>

	
    <div class="disrent">
    <span id="addtocartprice" style="display: none;"><div class="container_total_price foods-totals"><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: <span class="cont_final_price" id="cont_final_price"><?php echo e(currency($sellingprice,'SAR',$Current_Currency)); ?></span></div></span> </div>
    
    <div class="addto_cart">
    	<span id="selectedproductprices"> 
    		<input type="hidden" id="product_id" name="product_id" value="<?php echo e($getAllProduct[0]->pro_id); ?>">
    		<input type="hidden" id="58" name="pr58" value="<?php echo e(currency($sellingprice, 'SAR',$Current_Currency, $format = false)); ?>">
    		<input type="hidden" id="59" name="pr59" value="<?php if(!empty($rentPrice)){ echo number_format($rentedprice,2);} ?>">
    		<input type="hidden" name="shopby" id="shopby" value="">
    	</span>    	
    	  <input type="hidden" id="category_id" name="category_id" value="<?php echo e($id); ?>">
          <input type="hidden" id="subcategory_id" name="subcategory_id" value="<?php echo e($sid); ?>">
          <input type="hidden" id="vendor_id" name="vendor_id" value="<?php echo e($lid); ?>">
		  <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9" readonly />
		  <input type="hidden" id="attribute_id" name="attribute_id" value="0">
		  <input type="hidden" id="cart_type" name="cart_type" value="music">
    	<span id="error"></span>
    	<span id="addtocart">
       
        <div id="addtocarbtn" class="equipment-button" style="display: none"><input type="submit" class="form-btn" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>"></div>
      <div id="booknow" class="equipment-button" style="display:none"><a href="javascript:void(0);" class="form-btn big-btn try_now_page_one"><?php echo e((Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '')  ?  trans(Session::get('lang_file').'.BOOK_NOW'): trans($OUR_LANGUAGE.'.BOOK_NOW')); ?></a>


      </div>

       

      </span>
	</div> <!-- container-section -->
	<a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a> 
	<?php echo Form::close(); ?>

        <!-- service-display-left -->
		
      </div>
	  <?php } else{ echo "<div class='service-display-section' style='text-align:center;'> No record found </div>";}?>
      <!--service-display-section-->
 <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
	
    <!-- checkout-form-cell -->
  </div>
  <!-- checkout-form-row -->
</div>
</div>
</div>
</div>

<!-- other_serviceinc -->
</div>
</div>
<!-- detail_page -->
</div>
<!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<?php if($k>0){ ?>

<div id="try_now_page_one_popup" class="new-popup measurement-popup-outer" style="display:none;"> 
<?php echo Form::open(['url' => 'addcartsingerproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'booknowid', 'enctype' => 'multipart/form-data']); ?>

				 <a href="javascript:void(0);" class="b-close">X</a>
  <div class="rent-pop-up"> 
    <div class="main_title"><?php echo e((Lang::has(Session::get('lang_file').'.EQUIPMENT_FOR_RENT')!= '')  ?  trans(Session::get('lang_file').'.EQUIPMENT_FOR_RENT'): trans($OUR_LANGUAGE.'.EQUIPMENT_FOR_RENT')); ?></div>
    <div class="rent-form">
      <div class="rent-form-line">
        <div class="rent-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.OCCASIONS_DATE')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_DATE'): trans($OUR_LANGUAGE.'.OCCASIONS_DATE')); ?></div>
        <div class="rent-form-bottom">
          <input type="text" name="occasions_date" id="occasions_date" maxlength="40" class="t-box cal-t datetimepicker" autocomplete="off" value=""/>
        </div>
        <span class="error one"></span>
      </div>
      <div class="rent-form-line">
        <div class="rent-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.RETURN_DATE')!= '')  ?  trans(Session::get('lang_file').'.RETURN_DATE'): trans($OUR_LANGUAGE.'.RETURN_DATE')); ?></div>
        <div class="rent-form-bottom">
          <input type="text" name="return_date" id="return_date" maxlength="40" class="t-box cal-t calculate_hour datetimepicker" autocomplete="off" value=""/>
        </div>
        <span class="error two"></span>
      </div>
      <div id="todata"></div>
      <div class="rent-form-line">
        <div class="rent-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')); ?></div>
        <div class="rent-form-bottom">
          <input type="hidden" name="insurance_amount" id="insurance_amount" readonly class="t-box" value="<?php echo  currency($getAllProduct[0]->Insuranceamount, 'SAR',$Current_Currency, $format = false); ?>"/>

          <div class="insurance_price">  <?php echo currency($getAllProduct[0]->Insuranceamount, 'SAR',$Current_Currency);  ?></div>
 

        </div>
      </div>
      <?php $HH= $getAllProduct[0]->pro_id; ?>
	  <span id="selectedproductpricespop">
    		<input type="hidden" id="product_idpop" name="product_id" value="<?php echo e($getAllProduct[0]->pro_id); ?>">
    		<span id="59pop"></span>
      </span>  
	  <input type="hidden" name="itemqty" id="popitemqty" value="1" min="1" max="9" readonly />
	  <input type="hidden" id="category_idpop" name="category_id" value="<?php echo e($id); ?>">
	  <input type="hidden" id="subcategory_idpop" name="subcategory_id" value="<?php echo e($sid); ?>">
	  <input type="hidden" id="vendor_idpop" name="vendor_id" value="<?php echo e($lid); ?>">
	  <input type="hidden" id="attribute_idpop" name="attribute_id" value="0">
	  <input type="hidden" id="cart_typepop" name="cart_type" value="music">
	  <input type="hidden" id="productoptionvalid" name="productoptionval" value="">
	  <input type="hidden" name="shopby" id="shopbypop" value="59">
<?php if(isset($rentedprice) && $rentedprice!=''): ?>
     <input type="hidden" name="pricePerhour" id="pricePerhour" value="<?php echo e(currency($rentedprice, 'SAR',$Current_Currency, $format = false)); ?>">
     <?php else: ?>
 <input type="hidden" name="pricePerhour" id="pricePerhour" value="0">
 <?php $rentedprice=0; ?>
     <?php endif; ?>
<input type="hidden" name="finalprice" id="finalprice" value="<?php echo e(currency($rentedprice, 'SAR',$Current_Currency, $format = false)); ?>">
 
      <div class="rent-form-prise" id="rentfinal_price"> </div>  
 
      <div class="rent-form-btn">
        <input type="submit" class="form-btn big-btn btn-info-wisitech" value="<?php echo e((Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '')  ?  trans(Session::get('lang_file').'.BOOK_NOW'): trans($OUR_LANGUAGE.'.BOOK_NOW')); ?>" />
      </div>
    </div>
  </div>
  <?php echo Form::close(); ?>

</div>

<?php } ?>
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
 
<script>
/* Mobile Number Validation */
 function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
 }
</script>
<script>
jQuery(function () 
{ 
 jQuery("#booknowid").validate({
    rules: {          
          "occasions_date" : {
            required : true
          },
          "return_date" : {
            required : true
          },
         },
         messages: {
          "occasions_date": {
            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_OCCASSION_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_OCCASSION_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_OCCASSION_DATE')); ?> <?php endif; ?>"
          },
          "return_date": {
            required: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_RETURN_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_RETURN_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_RETURN_DATE')); ?> <?php endif; ?>"
          },
         },
 });
 jQuery(".btn-info-wisitech").click(function() {
   if (jQuery("#booknowid").valid() == true) {
    jQuery('#booknowid').submit();
   }
  });
});
</script>
<script language="javascript">

  function getChangedPrice(price)
  {
	  var UpdatedPrice = '';
	  $.ajax({
		 async: false,
		 type:"GET",
		 url:"<?php echo e(url('getChangedprice')); ?>?price="+price,
		 success:function(res)
		 {  
		  UpdatedPrice =res;
		 }
	   });
	  return UpdatedPrice;
  }

function pricecalculation(act)
{
    var productId        = document.getElementById('product_id').value;
	var currentquantity  = document.getElementById('qty').value;
	var unititemprice    = document.getElementById('58').value;
	var rel_data = $('input[name=productoptionval]:checked').attr("rel");
	//alert(rel_data);
	if(!$('input[name=productoptionval]:checked').val()) 
	{    
		document.getElementById('error').innerHTML = 'please select shopby option first';   
		document.getElementById('qty').value=1; 
	}
	else
	{
	    
		var no=1;	   
	   //alert(productId);
		//alert(currentquantity);		
		//alert(unititemprice);
		//var selectedshopby  = document.getElementById('shopby').value;
		//var unititemprice   = document.getElementById(''+selectedshopby).value;
		
		//alert(currentquantity);
        <?php $Cur = Session::get('currency'); ?>
		if(act=='add')
		{
			var qty= parseInt(currentquantity)+parseInt(no);
		}
		else
		{ 
			if(parseInt(currentquantity)==1)
			{
			  var qty=parseInt(currentquantity)
		    }
			else
			{
			  var qty=parseInt(currentquantity)-parseInt(no);
			}
		}
		//alert(currentquantity);
		$.ajax({
		   type:"GET",
		   url:"<?php echo e(url('checkQuantity')); ?>?product_id="+productId+'&qty='+qty+'&rel='+rel_data,
		   async: false,
		   success:function(res)
		   {   
			//alert(res);
			 if(res!='ok')
			 {
				 $('.action_popup').fadeIn(500);
				 $('.overlay').fadeIn(500);
				 $('#showmsg').show();
				 $('#hidemsgab').hide();
				 $('#showmsgab').show();
				 var qtyupdated = parseInt(currentquantity);			
				 document.getElementById('qty').value = qtyupdated - 1;
				 document.getElementById('hideid').style.display = "none";
			 }
			 else
			 {
			    var producttotal = qty*unititemprice;
				document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
				document.getElementById('itemqty').value=qty;
				document.getElementById('popitemqty').value=qty;
				//document.getElementById('rentfinal_price').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
			 }
		   }
		});
	}
}
</script>
<script type="text/javascript">
function selectedweight(val,buyrent)
{
	
	document.getElementById('productoptionvalid').value=val;
	document.getElementById('shopby').value=val;
	document.getElementById('error').innerHTML = ''; 
	//document.getElementById('qty').value=1;
  //document.getElementById('cont_final_price').innerHTML = '';
  var productId=$('#product_id').val();

	



    $.ajax({
       type:"GET",
       url:"<?php echo e(url('checksoldout')); ?>?product_id="+productId+'&rel='+buyrent,
       async: false,
       success:function(res)
       {   

       if(res=='ok')
       {

         $('#addtocartprice').css('display','block');
           $('#service_quantity_box').css('display','none');
          $('#soldoutnow').css('display','block');
         $('#service_quantity_box').css('display','none');
          $('#hiderentinsurence').css('display','none');
          $('#addtocart').css('display','none');
      

       }
       else
       {
          $('#soldoutnow').css('display','none');
              $('#addtocartprice').css('display','');
  $('#addtocart').css('display','');
  $('#service_quantity_box').css('display','');
  $('#hiderentinsurence').css('display','');
                if(buyrent=='buy')
                  {
                    $('#p58').css('display','');
                    $('#p59').css('display','none');
                      /*$('#addtocart').html('<div class="equipment-button"><input type="submit" class="form-btn" value="Add to Cart"></div>');*/
                        $('#addtocarbtn').css('display','block');
                        $('#booknow').css('display','none'); 
                        $('.disrent').show(); 
                    $('#hiderentinsurence').css('display','none');    
                  }
                  if(buyrent=='rent')
                  {
                     $('#p58').css('display','none');
                     $('#p59').css('display','');
                       $('.disrent').hide();
                     /*$('#addtocart').html('<div class="equipment-button"><a href="javascript:void(0);" class="form-btn big-btn open_popup">Book Now</a></div>');*/
                       $('#addtocarbtn').css('display','none');
                       $('#booknow').css('display','block');
                     $('#hiderentinsurence').css('display','block');
                  }
       }
       }
    });

 
}
</script>

<script type="text/javascript">
  function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}

function getdatedish(selecteddateproduct)
{
	var dateproductid=selecteddateproduct;

    if(selecteddateproduct)
	{
	   $.ajax({
	   type:"GET",
	   url:"<?php echo e(url('getproductdetail')); ?>?product_id="+dateproductid,
	   success:function(res)
	   {         
		if(res)
		{ 
		
		
		$('#soldoutnow').css('display','none');
		
		  var json = JSON.stringify(res);
		  var obj = JSON.parse(json);		 
		  length=obj.productdateshopinfo.length;
	      <?php $Cur = Session::get('currency'); ?>
		  if(length>0)
		  {
	         for(i=0; i<length; i++)
		     {
          if($.trim(obj.productdateshopinfo[i].notes)=='')
          {
          $('#selectedproduct').html('<div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-product-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');
          }
          else
          {
          $('#selectedproduct').html('<div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-product-description">'+obj.productdateshopinfo[i].pro_desc+'</div> <p><em><strong><?php if(Lang::has(Session::get('lang_file').'.NOTES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NOTES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.NOTES')); ?> <?php endif; ?>:</strong> </em>'+obj.productdateshopinfo[i].notes+'</p>');  
          }
          if(obj.productdateshopinfo[i].pro_qty >=1)
          {

 
        // $('#soldoutnow').css('display','none');
        // $('#addtocartprice').css('display','');
        // $('#addtocart').css('display','');
        // $('#service_quantity_box').css('display','');
        // $('#hiderentinsurence').css('display','');
        // $('#addtocarbtn').css('display','');
 
          }

            if(obj.productdateshopinfo[i].pro_disprice>0){
                 var dprice=obj.productdateshopinfo[i].pro_disprice;
                  dprice = getChangedPrice(dprice);
                   dpriceaa = getChangedPrice(obj.productdateshopinfo[i].pro_price);
                 var displayprice='<span class="strike"><?php echo $Cur;?> '+dpriceaa+'</span><br><span> <?php echo $Cur;?> '+dprice+'</span>';
             }else{
              var dprice=obj.productdateshopinfo[i].pro_price;
              dprice = getChangedPrice(dprice);                 
                var displayprice='<?php echo $Cur;?> '+dprice;
            }



		       $('#selectedproductprices').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" name="shopby" id="shopby" value="58"><input type="hidden" id="58" name="pr58" value="'+dprice+'">');
		       $('#p58').html(displayprice);	
               $('#cont_final_price').html('<?php echo $Cur;?> '+dprice);
			   $('#showRentIn').html('<?php echo $Cur;?> '+parseFloat(obj.productdateshopinfo[i].Insuranceamount).toFixed(2));
			   $('#insurance_amount').val(parseFloat(obj.productdateshopinfo[i].Insuranceamount).toFixed(2));	
        

			   $('.insurance_price').html('<?php echo $Cur;?> '+parseFloat(obj.productdateshopinfo[i].Insuranceamount).toFixed(2));	  
		     }
		
		  }


  

		  pricelength=obj.productdateshopprice.length;
      if(pricelength==0)
           { 
        $('.rentprice').hide(); selectedweight('58','buy'); $("#kilo").attr('checked', true);  
            }
		  if(pricelength>0)
		  { $('.rentprice').show(); $('.rentprice').show();  
	       
		      
        if(obj.productdateshopprice[2].discount_price>0){
                 var rentdprice=obj.productdateshopprice[2].discount_price;
                rentdpricaae = getChangedPrice(obj.productdateshopprice[0].price);
                  rentdprice = getChangedPrice(rentdprice);
                 var rentdisplayprice='<span class="strike"><?php echo $Cur;?> '+rentdpricaae+'</span><span> <?php echo $Cur;?> '+rentdprice+'</span>';
             }else{
              var rentdprice=obj.productdateshopprice[0].price; 
              rentdprice = getChangedPrice(rentdprice);
               var rentdisplayprice='<?php echo $Cur;?> '+rentdprice;
            }


              rentdpriace = getChangedPrice(obj.productdateshopprice[0].price);
 
               $('#pricePerhour').val(parseFloat(rentdpriace).toFixed(2));

           $('#selectedproductprices').append('<input type="hidden" id="'+obj.productdateshopprice[0].product_option_id+'" name="pr'+obj.productdateshopprice[0].product_option_id+'" value="'+rentdpriace+'">');

		       $('#p'+obj.productdateshopprice[0].product_option_id).html('<?php echo $Cur;?> '+obj.productdateshopprice[0].price);
		       $('#p59').html(''+rentdisplayprice+'<?php if(Lang::has(Session::get('lang_file').'.PER_OURR')!=''): ?><?php echo e(trans(Session::get('lang_file').'.PER_OURR')); ?><?php else: ?><?php echo e(trans($MER_OUR_LANGUAGE.'.PER_OURR')); ?><?php endif; ?>');
			    $('#59pop').val(rentdprice);			   
			 
		  }
      checkgallery(selecteddateproduct);
	    }
	   }
	 });
   }
}
</script>
<script type="text/javascript">
$(document).ready(function() {
 
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script src="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>
<script>
  
var checkPastTime = function(inputDateTime) {
    if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {
        var current = new Date();
 
        //check past year and month
        if (inputDateTime.getFullYear() < current.getFullYear()) {
            $('.datetimepicker').datetimepicker('reset');
            alert("Sorry! Past date time not allow.");
        } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {
            $('.datetimepicker').datetimepicker('reset');
            alert("Sorry! Past date time not allow.");
        }
 
        // 'this' is jquery object datetimepicker
        // check input date equal to todate date
        if (inputDateTime.getDate() == current.getDate()) {
            if (inputDateTime.getHours() < current.getHours()) {
                $('.datetimepicker').datetimepicker('reset');
            }
            this.setOptions({
                minTime: current.getHours() + ':00' //here pass current time hour
            });
        } else {
            this.setOptions({
                minTime: false
            });
        }
    }
};
 
var currentYear = new Date();
$('.datetimepicker').datetimepicker({
    format:'d M Y g:i A',
    minDate : 0,
    yearStart : currentYear.getFullYear(), // Start value for current Year selector
    onChangeDateTime:checkPastTime,
    onShow:checkPastTime
});
/* 
jQuery(window).load(function()
{
	jQuery("body").on('mouseover', '.datetimepicker', function() {  
	jQuery(this).datetimepicker({ ampm: true, // FOR AM/PM FORMAT
    format : 'd M Y g:i A',
    minDate : 0 });
 
	});

})
*/
</script>
<script type="text/javascript">
/*$('#occasions_date').datepicker({
  format: "d M yyyy",
   startDate: '-0m'
});

$('#return_date').datepicker({
  format: "d M yyyy",
  startDate: '-0m'
});
*/

$('body').mouseover(function()
{
 
 testfun();
});

$(document).ready(function()
{
	var startdate =  $('#occasions_date').val();
	var enddate =  $('#return_date').val()  
})

  //$('body').on('change dp.change', '#return_date', function() { 
  function testfun()
  {
 
var startDate = document.getElementById("occasions_date").value;

var endDate = document.getElementById("return_date").value;
 
if(startDate !='' && endDate !='')
{
 
 
 
$('#popitemqty').val($('#qty').val());
 

var timeStart = new Date(startDate).getTime();
var timeEnd = new Date(endDate).getTime();
var hourDiff = timeEnd - timeStart; //in ms
var secDiff = hourDiff / 1000; //in s
var minDiff = hourDiff / 60 / 1000; //in minutes
var hDiff = hourDiff / 3600 / 1000; //in hours
 
var CalHours =   Math.floor(hDiff);
  

var PerH = $('#pricePerhour').val();
var insuranceamount = $('#insurance_amount').val();
var QTY = $('#qty').val();
var TotalAmount = ((CalHours * PerH)+parseFloat(insuranceamount)) * parseInt(QTY);

//alert(TotalAmount);
//rentfinal_price pricePerhour

    if ((Date.parse(startDate) > Date.parse(endDate))) {

      document.getElementById("return_date").value = "";
        <?php if($selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");
        <?php else: ?>
        $('#todata').html("End date should be greater than Start date");
        <?php endif; ?>
      
     
    }
    else
    {
      $('#todata').html("");
      <?php if($selected_lang_code !='en'): ?>
        $('#rentfinal_price').html('إجمالي المبلغ SAR '+ parseFloat(TotalAmount).toFixed(2));
        <?php else: ?>
        $('#rentfinal_price').html('Total Amount <br/> SAR '+ parseFloat(TotalAmount).toFixed(2));
        <?php endif; ?>
 
    }

    document.getElementById('finalprice').value=TotalAmount;
   

 

var bookingdate_from =  $('#occasions_date').val();
  var bookingdate_to =  $('#return_date').val();
  var product_id =  $('#productoptionvalid').val();
  
console.log(bookingdate_from);
console.log(bookingdate_to);


 $.ajax({
     async: false,
     type:"GET",
     url:"<?php echo e(url('checkacosticsrent')); ?>?product_id="+product_id+"&bookingdate_to="+bookingdate_to+"&bookingdate_from="+bookingdate_from,
     success:function(res)
     {  
       if(res==0)
       {
var SOLD_OUT = "<?php if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SOLD_OUT')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SOLD_OUT')); ?> <?php endif; ?>";

 $('input[type="submit"]').val(SOLD_OUT);
 $('input[type="submit"]').prop('disabled',true);
       }
       else
       {
 var BOOK_NOW = "<?php if(Lang::has(Session::get('lang_file').'.BOOK_NOW')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BOOK_NOW')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.BOOK_NOW')); ?> <?php endif; ?>";
$('input[type="submit"]').val(BOOK_NOW);
 $('input[type="submit"]').prop('disabled',false);
       }
     }
   });








 
}
  };
  
 

</script>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});


<?php if(isset($HH) && $HH!=''){  ?>
jQuery(window).load(function(){
     
   getdatedish('<?php echo $HH;?>')
})

<?php } ?>


</script>


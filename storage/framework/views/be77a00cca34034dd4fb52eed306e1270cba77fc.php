<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
     
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
    <link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " onLoad="return showDropDown('<?php echo $subscri->services_id; ?>');" >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
   <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
                   <div class="right_col" role="main">
            <?php if(Session::has('message')): ?>
                <div class="alert alert-danger no-border">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php endif; ?>
            <?php if(Session::has('warning')): ?>
                <div class="alert alert-danger no-border">
                <button type="button" class="close" data-dismiss="alert" id="warning"><span>×</span><span class="sr-only">Close</span></button>
                    <li><?php echo Session::get('warning'); ?></li>
                </div>
            <?php endif; ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content padduser-block">
                   <div class="top_back_btn">
                  <div class="title-block">
                        <h3 align="center"> <?php echo e($subscri->title); ?></h3>
                        </div>
                        <div class="back-btn-area">
                       <a href="/admin/subscription" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a>
                      </div>                      
                    </div>
                    
                    <form action="/admin/subscription/edit/<?php echo e($subscri->id); ?>" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left form-section" enctype="multipart/form-data" method="post">
                      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
					  <input type="hidden" name="cat_id" value="<?php echo e($subscri->cat_id); ?>">
                      <?php /*?><div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_SERVICE_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICE_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICE_NAME') }} <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <select name="cat_id" class="form-control dtmagin" required="">
                        <option value="" selected="selected">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SERVICE_SELECT_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICE_SELECT_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICE_SELECT_NAME') }}</option>
                        @foreach($main_category as $main)
                        <option @if($main->mc_id == $subscri->cat_id) selected @endif value="{{$main->mc_id}}"> @if(Session::get('admin_lang_file') == 'admin_ar_lang') {{$main->mc_name_ar}} @else  {{$main->mc_name}} @endif</option>
                        @endforeach
                        </select>
                        </div>
                      </div><?php */?>
                      
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TYPE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_TYPE')); ?>  <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <select name="services_id" class="form-control dtmagin" required="required" onChange="return showDropDown(this.value)">
                        <option value="" selected="selected"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_TYPE')); ?></option>
                        <option  value="1" <?php if($subscri->services_id == 1): ?> selected <?php endif; ?>><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_COMMISSION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_COMMISSION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COMMISSION')); ?></option>
                        <option  value="2" <?php if($subscri->services_id == 2): ?> selected <?php endif; ?>><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_FEE_BASED')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FEE_BASED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_FEE_BASED')); ?></option>
                        </select>
                        </div>
                      </div>
					<span  id="p1" style="display:none;">
                       <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PERCENTAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PERCENTAGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PERCENTAGE')); ?><span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="paymentpercentage"  required class="form-control col-md-7 col-xs-12" maxlength="2" max="2" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo e($subscri->payment); ?>">
                        </div>
                      </div>
					  </span>
					 <span  id="p2" style="display:none;">
					   <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?><span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="paymentamount"  maxlength="20" required class="form-control col-md-7 col-xs-12" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo e($subscri->payment); ?>">
                        </div>
                      </div>
					  </span>
					  
					  
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DESCRIPTION_FEATURE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESCRIPTION_FEATURE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DESCRIPTION_FEATURE')); ?><span class="required"></span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <textarea class="form-control" rows="3"
                          name="description" id="my_form" placeholder='<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DESCRIPTION_FEATURE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESCRIPTION_FEATURE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DESCRIPTION_FEATURE')); ?>' maxlength="200"><?php echo e($subscri->description); ?></textarea>
                        </div>
                      </div>
                       <div class="ln_solid"></div>
                       <div class="form-group gender">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS')); ?></label>
                      <div class="col-md-3 col-sm-3 col-xs-6 marTop">
                        <input type="radio" class="flat" name="status" id="active" value="1"  required <?php if($subscri->status == 1): ?> checked <?php endif; ?> />
						            <label for="active" class="gender-name">
                        <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE')); ?> </label>
                       
                        </div> 
                        <div class="col-md-3 col-sm-3 col-xs-6 marTop">
                        <input type="radio" class="flat" name="status" id="inactive" value="0" <?php if($subscri->status == 0): ?> checked <?php endif; ?> />
							         <label for="inactive" class="gender-name"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE')); ?>

                        </label>  
                        
                      </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 col-md-offset-4">
                          <a class="btn btn-primary" type="button" href="/admin/subscription"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL')); ?></a>
                          <button class="btn btn-primary" type="reset"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
                          <button type="submit" class="btn btn-success"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

        </div>

        </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script> 
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  $(document).on('click','#warning',function(){
       $.ajax({
          url: "/session_flush",
          data: {},
          type: "POST",
          cache: false,
          success: function(html){
          }
        });
    });
function showDropDown(str)
{
 if(str==1)
 {
  
   $("#p2").css("display", "none");
  $("#p1").css("display", "block");
   //$('.percentageid').html('<?php if(Lang::has(Session::get('lang_file').'.BACK_PERCENTAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BACK_PERCENTAGE')); ?> <?php else: ?>  <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_PERCENTAGE')); ?> <?php endif; ?>');
 }
 else if(str==2)
 {
  $("#p1").css("display", "none");
  $("#p2").css("display", "block");
   //$('.percentageid').html('<?php if(Lang::has(Session::get('lang_file').'.BACK_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.BACK_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> <?php endif; ?>');
 }
}
</script>
</body>

    <!-- END BODY -->
</html>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper diamond_fullwidth"> <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="inner_wrap">
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
      <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper"><?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="page-right-section">
<div class="diamond_main_wrapper"> <?php if(Session::get('lang_file')!='en_lang'): ?> <img src="<?php echo e(url('')); ?>/themes/images/clinic_ar.jpg" alt="" usemap="#hall-subcategory" hidefocus="true"> <?php else: ?> <img src="<?php echo e(url('')); ?>/themes/images/clinic.jpg" alt="" usemap="#hall-subcategory"> <?php endif; ?>
            <map name="hall-subcategory" id="hall-subcategory">  
			  <area shape="poly" coords="21,458,469,12,917,459,21,460" href="<?php echo e(url('')); ?>/cliniccosmeticsandlaser/28/29/" />
			<area shape="poly" coords="19,464,919,466,471,916,20,464" href="<?php echo e(url('')); ?>/clinicdentalanddermatology/28/30" />
            </map>
          </div>
        <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
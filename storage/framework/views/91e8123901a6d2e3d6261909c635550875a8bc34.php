<?php $__env->startSection('content'); ?>

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2>Sound SystemShop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>
                                    <?php
                                    $name = 'name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $name = 'name_ar';
                                    }
                                    echo $singerDetails->$name;
                                    ?>
                                </h2>
                                <h2>
                                    <?php
                                    $address = 'address';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $address = 'address_ar';
                                    }
                                    echo $singerDetails->$address;
                                    ?>
                                </h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($singerDetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>
                                <?php
                                        ?>
                                <?php $about='about'?>
                                <?php if(Session::get('lang_file')!='en_lang'): ?>
                                    <?php $about= 'about_ar'; ?>
                                <?php endif; ?>
                                <?php echo nl2br($singerDetails->$about); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                    <?php
                        $long  = @$singerDetails->longitude;
                        $lat = @$singerDetails->latitude;
                    ?>


                <?php if($long !='' && $lat!=''): ?>
                            <input type="hidden" id="long" value="<?php echo e($long); ?>">
                            <input type="hidden" id="lat" value="<?php echo e($lat); ?>">
                            <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;"> </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?>
                                        <?php for($i = 0 ; $i < $val->ratings ; $i++ ): ?>
                                        <i class="fa fa-star"></i>
                                         <?php endfor; ?>
                                        <?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        
                            <?php echo e($allreview->links()); ?>


                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <?php
                    $mc_name = 'video_description';
                    if(Session::get('lang_file')!='en_lang')
                    {
                        $mc_name = 'video_description_ar';
                    }
                    echo $singerDetails->$mc_name;
                    ?>
                    <div class="p-15 box"><iframe width="100%" height="315" src="<?php echo e($singerDetails->video_url); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li class="active">
                            <div class="image"><a href="<?php echo e(route('acousticequipment',[$id,$sid,$lid])); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/sound-system.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('acousticequipment',[$id,$sid,$lid])); ?>" class="select"><?php echo e((Lang::has(Session::get('lang_file').'.EQUIPMENT')!= '')  ?  trans(Session::get('lang_file').'.EQUIPMENT'): trans($OUR_LANGUAGE.'.EQUIPMENT')); ?></a></h2>
                        </li>
                        <li>
                            <div class="image"><a href="<?php echo e(route('acousticrecording',[$id,$sid,$lid])); ?>"><img src="<?php echo e(url('newWebsite')); ?>/images/Recording.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('acousticrecording',[$id,$sid,$lid])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')); ?></a></h2>
                        </li>
                    </ul>
                </center>




                <?php
                    global $Current_Currency;
                      $Current_Currency  = Session::get('currency');

                    if($Current_Currency =='') {
                      $Current_Currency = 'SAR';
                     }

                ?>

                <div class="box p-15">
                    <div class="row">

                        <?php $__currentLoopData = $getAllProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $getallcats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php
                                $Img = str_replace('thumb_','',$getallcats->pro_Img);
                            ?>


                            <?php
                                if($getallcats->pro_disprice > 0){
                                  $sellingprice = $getallcats->pro_disprice;

                                }else{
                                  $sellingprice=$getallcats->pro_price;

                                }
                            ?>


                            <?php   $rentPrice = Helper::getProductByAttributeValue($getallcats->pro_id); ?>


                            <?php

                            if(!empty($rentPrice)){
                                if($rentPrice[2]->discount_price>0){
                                    $rentedprice=$rentPrice[2]->discount_price;

                                }else{
                                    $rentedprice=$rentPrice[0]->price;

                                }
                            }
                            ?>

                            <div class="col-md-4">
                                <div class="item-style clearfix">
                                    <div class="tump">
                                        <img src="<?php echo e($Img); ?>" alt="" />
                                    </div>
                                    <div class="caption">
                                        <a href=""><?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?></a>
                                        <p><?php echo e($getallcats->pro_desc); ?></p>
                                        <div class="flex-end">
                                            <div class="price"><?php echo e($getallcats->pro_price); ?> SAR</div>
                                            <?php echo Form::open(['url' => 'addcartsingerproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']); ?>

                                            <span id="selectedproductprices">
                                                <input type="hidden" id="product_id" name="product_id" value="<?php echo e($getallcats->pro_id); ?>">
                                                <input type="hidden" id="58" name="pr58" value="<?php echo e(currency($sellingprice, 'SAR',$Current_Currency, $format = false)); ?>">
                                                <input type="hidden" id="59" name="pr59" value="<?php if(!empty($rentPrice)){ echo number_format($rentedprice,2);} ?>">
                                                <input type="hidden" name="shopby" id="shopby" value="">
                                            </span>
                                            <input type="hidden" id="category_id" name="category_id" value="<?php echo e($id); ?>">
                                            <input type="hidden" id="subcategory_id" name="subcategory_id" value="<?php echo e($sid); ?>">
                                            <input type="hidden" id="vendor_id" name="vendor_id" value="<?php echo e($lid); ?>">
                                            <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9" readonly />
                                            <input type="hidden" id="attribute_id" name="attribute_id" value="0">
                                            <input type="hidden" id="cart_type" name="cart_type" value="music">
                                            <input id="addtocarbtn" type="submit" class="form-btn" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>">
                                            <?php echo Form::close(); ?>

                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>

            </div>

            <center>
                <?php echo e($getAllProduct->links()); ?>

                
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                
            </center>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
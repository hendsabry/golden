<?php $__env->startSection('content'); ?>
    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php echo e($vendordetails->mc_name); ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php echo e($vendordetails->address); ?>

                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php echo e($vendordetails->mc_discription); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        MAP
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $userinfo = Helper::getuserinfo($val->customer_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?><</p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>
                        <?php echo e($allreview->links()); ?>

                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                    </div>
                </div>
            </div>

            <h3 class="head-title"><?php echo e((Lang::has(Session::get('lang_file').'.INVITATION_DETAILS')!= '')  ?  trans(Session::get('lang_file').'.INVITATION_DETAILS'): trans($OUR_LANGUAGE.'.INVITATION_DETAILS')); ?></h3>
            <div class="box p-15">
                <?php echo Form::open(['url' => 'insert_electronic', 'method' => 'post', 'name'=>'addtoelect', 'id'=>'addtoelect', 'class'=>'form','enctype' => 'multipart/form-data']); ?>

                <input type="hidden" id="category_id" name="category_id" value="<?php echo e($category_id); ?>">

                <input type="hidden" id="subcat_id" name="subcat_id" value="<?php echo e($subcat_id); ?>">

                <input type="hidden" name="actiontype" value="electronic-invitations">

                <input type="hidden" name="cart_sub_type" value="invitations">

                <input type="hidden" name="cart_type" value="occasion">

                <input type="hidden" name="vendor_id" value="<?php echo e($vendordetails->vendor_id); ?>">

                <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?php echo e((Lang::has(Session::get('lang_file').'.OCCASION_TYPE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_TYPE'): trans($OUR_LANGUAGE.'.OCCASION_TYPE')); ?></label>
                                <select name="occasion" id="occasion" class="form-control" onchange="getlabel(this.value)">

                                    <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select_Occasion_Type')); ?> <?php endif; ?></option>

                                    <?php

                                    if(Session::get('lang_file')!='en_lang')

                                    {

                                        $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');

                                    }

                                    else

                                    {

                                        $getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding Occasion');

                                    }

                                    foreach($getArrayOfOcc as $key=>$ocval){

                                    if($key==2){

                                    ?>



                                    <option value="" disabled="" style="color: #d2cece;"><?php echo e($ocval); ?></option>

                                    <?php

                                    $setOccasion = Helper::getelectronic($key);



                                    foreach($setOccasion as $val){ ?>

                                    <option value="<?php echo e($val->pro_id); ?>"><?php $title = 'pro_title';if(Session::get('lang_file')!='en_lang'){$title = 'pro_title_ar';}echo $val->$title;?>

                                    </option>

                                    <?php } ?>

                                    <?php } } ?>

                                </select>

                                <?php if($errors->has('occasion')): ?><span class="error"><?php echo e($errors->first('occasion')); ?></span><?php endif; ?>

                            </div>
                        </div>
                    <input type="hidden" name="lblinfo" id="lblinfo">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?php echo e((Lang::has(Session::get('lang_file').'.OCCASSION_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASSION_NAME'): trans($OUR_LANGUAGE.'.OCCASSION_NAME')); ?></label>
                                <input type="text" name="occassion_name" id="occassion_name" class="t-box form-control" value="<?php echo Input::old('occassion_name'); ?>" maxlength="50">

                                <?php if($errors->has('occassion_name')): ?><span class="error"><?php echo e($errors->first('occassion_name')); ?></span><?php endif; ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?php echo e((Lang::has(Session::get('lang_file').'.VENUE')!= '')  ?  trans(Session::get('lang_file').'.VENUE'): trans($OUR_LANGUAGE.'.VENUE')); ?></label>
                                <input type="text" name="venue" id="venue" class="t-box form-control" value="<?php echo Input::old('venue'); ?>" maxlength="75">

                                <?php if($errors->has('venue')): ?><span class="error"><?php echo e($errors->first('venue')); ?></span><?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group data">
                                        <label><?php echo e((Lang::has(Session::get('lang_file').'.DATE')!= '')  ?  trans(Session::get('lang_file').'.DATE'): trans($OUR_LANGUAGE.'.DATE')); ?></label>
                                        <input class="form-control t-box cal-t datetimepicker" autocomplete="off" name="date" id="datebo" value="<?php echo Input::old('date'); ?>" type="text" maxlength="30" onchange="mynewtimeslot();">

                                        <?php if($errors->has('date')): ?><span class="error"><?php echo e($errors->first('date')); ?></span><?php endif; ?>

                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?php echo e((Lang::has(Session::get('lang_file').'.TIME')!= '')  ?  trans(Session::get('lang_file').'.TIME'): trans($OUR_LANGUAGE.'.TIME')); ?></label>
                                        <input class="form-control t-box cal-t checkout-small-box select_time" autocomplete="off" name="time" id="time" value="<?php echo Input::old('time'); ?>" type="text" maxlength="30">

                                        <?php if($errors->has('time')): ?><span class="error"><?php echo e($errors->first('time')); ?></span><?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><?php echo e((Lang::has(Session::get('lang_file').'.NUMBER_OF_INVITATION')!= '')  ?  trans(Session::get('lang_file').'.NUMBER_OF_INVITATION'): trans($OUR_LANGUAGE.'.NUMBER_OF_INVITATION')); ?> </label>
                                <input type="file" name="upload_csv" class="info-file form-control" id="company_logo" accept=".xlsx, .xls, .csv">

                                <a href="<?php echo e(url('/electronic.csv')); ?>" target="_blank">

                                    <span class="checkfile"><?php echo e((Lang::has(Session::get('lang_file').'.CHKFILE')!= '')  ?  trans(Session::get('lang_file').'.CHKFILE'): trans($OUR_LANGUAGE.'.CHKFILE')); ?></span>

                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><?php echo e((Lang::has(Session::get('lang_file').'.INVITATION_TYPE')!= '')  ?  trans(Session::get('lang_file').'.INVITATION_TYPE'): trans($OUR_LANGUAGE.'.INVITATION_TYPE')); ?></label>
                                <div>
                                    <input id="Text-Message" name="invitations_type" type="radio" value="1" onclick="selectInvitationsTypeAndBy(this.value,'message');">

                                    <label for="Text-Message"><?php echo e((Lang::has(Session::get('lang_file').'.TEXT_MESSAGE')!= '')  ?  trans(Session::get('lang_file').'.TEXT_MESSAGE'): trans($OUR_LANGUAGE.'.TEXT_MESSAGE')); ?></label>

                                    <input id="Email" name="invitations_type" type="radio" value="2" onclick="selectInvitationsTypeAndBy(this.value,'email');">

                                    <label for="Email"><?php echo e((Lang::has(Session::get('lang_file').'.Email')!= '')  ?  trans(Session::get('lang_file').'.Email'): trans($OUR_LANGUAGE.'.Email')); ?></label>

                                    <input id="Invitations_Card" name="invitations_type" type="radio" value="3" onclick="selectInvitationsTypeAndByCard(this.value,'<?=$subcat_id?>');">

                                    <label for="Invitations_Card"><?php echo e((Lang::has(Session::get('lang_file').'.INVITATION_CARD')!= '')  ?  trans(Session::get('lang_file').'.INVITATION_CARD'): trans($OUR_LANGUAGE.'.INVITATION_CARD')); ?></label>
                                    <label for="invitations_type" class="error"> </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="total_food_cost" style="display:none;">

                        <div class="total_price"><?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: <span id="price_id"></span></div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Text</label>
                            <textarea class="form_textarea form-control" name="comments" id="comments"><?php echo Input::old('comments'); ?></textarea>

                            <?php if($errors->has('comments')): ?><span class="error"><?php echo e($errors->first('comments')); ?></span><?php endif; ?>
                        </div>
                    </div>

                    <center>
                        <input type="hidden" name="todaydate" id="todaydate" value="<?php echo e(date('m/d/Y')); ?>">

                        <input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn addto_cartbtn btn-info-wisitech">

                    </center>
                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>


    <script>





        function selectInvitationsTypeAndBy(str,texttype)

        {

            $('#addproductwritedata').html('');

            $('.invitaion_cardrow').css('display','none');

            var catId = $('#subcat_id').val();

            $.ajax({

                type:"GET",

                url:"<?php echo e(url('getAllDesignerCard')); ?>?invitation_type="+str+"&texttype="+texttype+"&category_id="+catId,

                async: false,

                success:function(res)

                {

                    if(res!='')

                    {

                        var result = res.split('-');

                        var no_of_atten = $('#no_of_atten').val();

                        if(no_of_atten>=1)

                        {

                            var totalPrice = parseFloat(no_of_atten) * parseFloat(result[1]);

                        }

                        else

                        {

                            var totalPrice =   parseFloat(result[1]);

                        }

                        $('#writedata_main').html('<?php if(Lang::has(Session::get('lang_file').'.PRICE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PRICE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PRICE')); ?> <?php endif; ?>: SAR '+result[1]);



                        $('#writedata').html('<input type="hidden" id="product_id" name="product_id" value="'+result[0]+'"><input type="hidden" id="priceId" name="priceId" value="'+totalPrice+'">');

                        $('#price_id').html('SAR '+totalPrice);







                        $('.total_food_cost').css('display','block');

                        $('.btn_row').css('display','block')

                    }

                }

            });

        }

        function selectInvitationsTypeAndByCard(str,catId)

        {

            var chk = $('#occasion').val();

            $('#writedata_main').html('');

            if(chk!='')

            {

                var crrurl = "<?php echo e(url('/')); ?>";

                $.ajax({

                    type:"GET",

                    url:"<?php echo e(url('getAllDesignerCardAllProduct')); ?>?invitation_type="+str+"&category_id="+catId+"&pid="+chk,

                    async: false,

                    success:function(res)

                    {

                        $('#writedata').html('');

                        if(res!='')

                        {

                            var json = JSON.stringify(res);

                            var obj = JSON.parse(json);

                            productlength = obj.productdata.length;

                            if(productlength > 0)

                            {

                                for(i=0; i<productlength; i++)

                                {

                                    $('#writedata').append('<div class="gender-radio-box "><div class="choose_radio"><input type="hidden" id="card'+(i+1)+'" name="invitations_card" value="'+obj.productdata[i].pro_id+'" onclick="showInvitationsTypeDesign(this.value);"><label for="card'+(i+1)+'"><div class="invitaion_card photographi-left-area"><img width="250" height="100%" src="'+crrurl+obj.productdata[i].pro_Img+'"></div></label></div></div>');

                                    $('.invitaion_cardrow').css('display','block');

                                    $('.total_food_cost').css('display','none');

                                    $('.btn_row').css('display','none');

                                    showInvitationsTypeDesign(obj.productdata[i].pro_id);

                                }

                            }

                        }

                    }

                });

            }

            else

            {

                var choosetext = "<?php if(Lang::has(Session::get('lang_file').'.Please_choose_Occasion_Type')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Please_choose_Occasion_Type')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Please_choose_Occasion_Type')); ?> <?php endif; ?>";

                $('#Invitations_Card').prop('checked', false);

                $('#writedata').html('<span class="error">'+choosetext+'</span>');



            }



        }

        function showInvitationsTypeDesign(pId)

        {

            $.ajax({

                type:"GET",

                url:"<?php echo e(url('getsinglesroduct')); ?>?product_id="+pId,

                async: false,

                success:function(res)

                {

                    if(res!='')

                    {

                        var result = res.split('-');

                        $('#addproductwritedata').html('<input type="hidden" id="product_id" name="product_id" value="'+result[0]+'"><input type="hidden" id="priceId" name="priceId" value="'+result[1]+'">');

                        $('#price_id').html('SAR '+result[1]);

                        $('.total_food_cost').css('display','block');

                        $('.btn_row').css('display','block')

                    }

                }

            });

        }

        jQuery( function() {



            jQuery( "#datebo" ).datepicker({

                format: "d M yyyy",

                minDate: '0'

            });









            d = Date.now();

            d = new Date(d);



            d = (d.getHours() > 12 ? d.getHours() - 12 : d.getHours()) + ':' + d.getMinutes() + ' ' + (d.getHours() >= 12 ? "PM" : "AM");











            var timeNow = new Date();



            var currhours   = timeNow.getHours();



            var currmins   = timeNow.getMinutes();



            var hours   = timeNow.getHours()+1;



            var ampm = hours >= 12 ? 'pm' : 'am';



            if(currmins>30){

                var newstarttime=hours+':'+'00 '+ampm;



            }else{





                var newstarttime=currhours+':'+'30 '+ampm;

            }











            jQuery('#time').timepicker({

                'minTime': newstarttime,

                'maxTime': '11:30 PM',

                'timeFormat': 'g:i A',

                'showDuration': false,

            });

        });

    </script>



    <script type="text/javascript">

        function mynewtimeslot(){



            jQuery('#time').timepicker('remove');



            var todaydate=jQuery('#todaydate').val();

            var bookingdate=jQuery('#datebo').val();



            var n= bookingdate.split(" ").slice(0, 3).join('');

            if(todaydate==n){

                var timeNow = new Date();



                var currhours   = timeNow.getHours();



                var currmins   = timeNow.getMinutes();



                var hours   = timeNow.getHours()+1;



                var ampm = hours >= 12 ? 'pm' : 'am';



                if(currmins>30){

                    var newstarttime=hours+':'+'00 '+ampm;



                }else{





                    var newstarttime=currhours+':'+'30 '+ampm;

                }

            }else{

                var newstarttime = '12:00 AM';

            }











            jQuery('#time').timepicker({

                'minTime': newstarttime,

                'maxTime': '11:30 PM',

                'timeFormat': 'g:i A',

                'showDuration': false,

            });







        }

    </script>



    <style type="text/css">

        a.morelink {

            text-decoration:none;

            outline: none;

        }

        .morecontent span {

            display: none;

        }

    </style>



    <script>

        $(document).ready(function()

        {

            var showChar = 400;

            var ellipsestext = "...";

            var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";

            var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";

            $('.more').each(function()

            {

                var content = $(this).html();

                if(content.length > showChar)

                {

                    var c = content.substr(0, showChar);

                    var h = content.substr(showChar-1, content.length - showChar);

                    var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

                    $(this).html(html);

                }

            });



            $(".morelink").click(function(){

                if($(this).hasClass("less")) {

                    $(this).removeClass("less");

                    $(this).html(moretext);

                } else {

                    $(this).addClass("less");

                    $(this).html(lesstext);

                }

                $(this).parent().prev().toggle();

                $(this).prev().toggle();

                return false;

            });

        });

    </script>



    <?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <script type="text/javascript">

        $('.status_yes').click(function()

        {

            $('.overlay, .action_popup').fadeOut(500);

        });

    </script>

    <script>

        function getlabel(str)

        {



            $('#Invitations_Card').prop('checked', false);

            $('#writedata').html('');



            if(str !=''){

                jQuery.ajax({

                    type: "GET",

                    url: "<?php echo e(route('getlabel')); ?>",

                    data: {id:str},

                    success: function(data) {

                        jQuery('#changelbl').html(data);

                        jQuery('#lblinfo').val(data);

                    }

                });

            }

        }





        jQuery(document).ready(function(){

            jQuery("#addtoelect").validate({

                rules: {

                    "occassion_name" : {

                        required : true

                    },

                    "occasion" : {

                        required : true

                    },

                    "venue" : {

                        required : true

                    },

                    "date" : {

                        required : true

                    },

                    "time" : {

                        required : true

                    },

                    "upload_csv" : {

                        required: true

                        //extension: "csv"

                    },

                    "invitations_type" : {

                        required : true

                    },

                    "comments" : {

                        required:true

                    },

                },

                messages: {

                    "occassion_name": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_OCCASSION_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_OCCASSION_NAME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_OCCASSION_NAME')); ?> <?php endif; ?>"

                    },

                    "occasion": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_YOUR_OCCASION')); ?> <?php endif; ?>"

                    },

                    "venue": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLZ_ENTER_YOUR_VENUE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLZ_ENTER_YOUR_VENUE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLZ_ENTER_YOUR_VENUE')); ?> <?php endif; ?>"

                    },

                    "date": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DATE')); ?> <?php endif; ?>"

                    },

                    "time": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_TIME')); ?> <?php endif; ?>"

                    },

                    "upload_csv": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_UPLOAD_IMAGE')); ?> <?php endif; ?>"

                        //extension: "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_UPLOAD_CSV_FILE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_UPLOAD_CSV_FILE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_UPLOAD_CSV_FILE')); ?> <?php endif; ?>"

                    },

                    "invitations_type": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.PLZ_SELECT_YOUR_INVITATION_TYPE')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PLZ_SELECT_YOUR_INVITATION_TYPE')); ?> <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.PLZ_SELECT_YOUR_INVITATION_TYPE')); ?> <?php endif; ?>"

                    },

                    "comments": {

                        required:  "<?php if(Lang::has(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.ENTER_COMMENTS_QUERIES')); ?> <?php endif; ?>"

                    },

                }

            });

            jQuery(".btn-info-wisitech").click(function() {

                if(jQuery("#addtoelect").valid()) {

                    jQuery('#addtoelect').submit();

                }

            });

        });



        jQuery('body').on('change','#company_logo', function(){

            var fake = this.value.replace("C:\\fakepath\\", "");

            jQuery("#file_value_count").html('Please wait..');

            jQuery("#no_of_atten").val(0);

            jQuery("#file_value").html(fake);

            var file_data = $('#company_logo').prop('files')[0];

            var form_data = new FormData();

            form_data.append('file', file_data);

            jQuery.ajax({

                url: "<?php echo e(route('checkfilentry')); ?>",

                cache: false,

                contentType: false,

                processData: false,

                headers: {

                    'X-CSRF-TOKEN': jQuery('input[name="_token"]').val()

                },

                data: form_data,

                type: 'POST',

                success: function(res){

                    jQuery("#file_value_count").html('['+res+']');

                    jQuery("#no_of_atten").val(res);





                    var Pid = jQuery("#priceId").val();



                    if(res>=1)

                    {

                        var totalPrice = parseFloat(res) * parseFloat(Pid);

                    }

                    else

                    {

                        var totalPrice =   parseFloat(Pid);

                    }



                    jQuery("#priceId").val(totalPrice);

                    jQuery("#price_id").html('SAR '+totalPrice);









                }

            });





        });

    </script>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
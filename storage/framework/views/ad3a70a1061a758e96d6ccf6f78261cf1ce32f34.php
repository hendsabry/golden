<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $buffet_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <?php if($autoid==''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_MENU_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_MENU_CATEGORY')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_MENU_CATEGORY')); ?> <?php endif; ?></h5>
          <?php else: ?>
          <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_MENU_UPDATE_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_MENU_UPDATE_CATEGORY')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_MENU_UPDATE_CATEGORY')); ?> <?php endif; ?></h5>
          <?php endif; ?>
      </header>
      <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
       <!-- Display Message after submition --> 
            <?php if(Session::has('message')): ?>
            <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
            <?php endif; ?> 
            <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box">             

              <form name="form1" id="menucategory" method="post" action="<?php echo e(route('store-buffet-category')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">
                    <span class="english"><?php echo lang::get('mer_en_lang.MER_CATEGORY_NAME'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); ?> </span>
            </label>
                  <div class="info100">
                   <div class="english"> 
                    <input type="text" class="english" name="category_name" maxlength="90" value="<?php echo e(isset($datafirst->attribute_title) ? $datafirst->attribute_title : ''); ?>" id="category_name" required="" >
                     </div>
                    <div class="arabic">
                    <input type="text" class="arabic ar" name="category_name_ar" maxlength="90" value="<?php echo e(isset($datafirst->attribute_title_ar) ? $datafirst->attribute_title_ar : ''); ?>" id="category_name_ar" required="" >
                  </div>
                  </div>
                  </div>
          		</div> <!-- form_row -->

               <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CREATED_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CREATED_IMAGE'); ?> </span> </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="catimage" id="company_logo1" class="info-file">
                    </div>
                     <?php if(!isset($datafirst->image)): ?> <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span> <?php endif; ?>
                    <?php if(isset($datafirst->image) && $datafirst->image !=''): ?>
                    <div class="form-upload-img"> <img src="<?php echo e($datafirst->image); ?>"> </div>
                      <?php endif; ?> 
                     </div>
                <div class="form_row">
                <div class="form_row_left">
                <div class="english">
                  <input type="submit" name="submit" value="Submit">
                  <input type="hidden" name="parent_id" value="<?php echo e(isset($parent_id) ? $parent_id : ''); ?>">
                  <input type="hidden" name="sid" value="<?php echo e(isset($sid) ? $sid : ''); ?>">
                  <input type="hidden" name="itemid" value="<?php echo e(isset($itemid) ? $itemid : ''); ?>">
                   <input type="hidden" name="autoid" value="<?php echo e(isset($autoid) ? $autoid : ''); ?>">

                </div>
                 <div class="arabic ar">
                  <input type="submit" name="submit" value="خضع">
             
                </div>
                </div></div>


              </form>

            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },


                     <?php if(isset($datafirst->image) && $datafirst->image !=''): ?>
                     catimage: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      }, 

                      <?php else: ?>
                         catimage: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },

                      <?php endif; ?>

                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             category_name: {
           required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_CATEGORY_NAME'); ?>",
                      },  
 
                 category_name_ar: {
               required:  "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_CAT_ARABIC_AR'); ?>",
                      }, 

                    catimage: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },       
                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

 <?php if($mer_selected_lang_code !='en'): ?>

                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                    if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }                   
                  
                    if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
                  if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }
                  if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }  

                  if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }                   
                  

<?php endif; ?>




                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
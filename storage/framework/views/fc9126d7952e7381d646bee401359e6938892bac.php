<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />

  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />

  <div class="service_listingrow">

    <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.FUND_REQUEST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.FUND_REQUEST')); ?>  

      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.FUND_REQUEST')); ?> <?php endif; ?></h5>

  </div>

  <?php echo Form::open(array('url'=>"fund-request",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?> <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>

  <div class="filter_area">

    <div class="order-filter-line">

      <div class="filter_left studio-amount">

        <input name="amount" maxlength="6"  placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?>" onkeypress="return isNumber(event)"  type="text" value="<?php echo e(request()->amount); ?>" />

      </div>

      <div class="of-date-box studio-date-box">

        <input name="dateFrom"  class="cal-t"  placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.DateFrom')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DateFrom')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DateFrom')); ?> <?php endif; ?>" id="dateFrom"  type="text"  maxlength="12"   value="<?php echo e(request()->dateFrom); ?>" />

        <input name="dateTo" id="dateTo" type="text" maxlength="12" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.DateTo')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DateTo')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DateTo')); ?> <?php endif; ?>"  class="cal-t studio-date" value="<?php echo e(request()->dateTo); ?>" />

        <span for="dateTo" generated="true" class="error" id="todata"> </span>

        <input name="" value="<?php if(Lang::has(Session::get('mer_lang_file').'.Apply')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Apply')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Apply')); ?> <?php endif; ?>" class="applu_bts" type="submit">

      </div>

    </div>

  </div>

  <!-- filter_area -->

  <?php echo Form::close(); ?>


  <div class="global_area">

    <div class="row">

      <div class="col-lg-12">

        <div class="table_wrap">

		 

     <!-- <div><span><b><?php if(Lang::has(Session::get('mer_lang_file').'.Total_Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Total_Amount')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Total_Amount')); ?> <?php endif; ?>:</b>  

		  <?php 

		  $mer_id   = Session::get('merchantid');

		  $totalAmount  = Helper::totalAmount($mer_id); 

      if(isset($totalAmount) && $totalAmount!=''){

		  echo $totalAmount;

    }

		  ?>

		  </span>&nbsp;&nbsp;<span><b><?php if(Lang::has(Session::get('mer_lang_file').'.MER_TOTAL_BALANCE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_TOTAL_BALANCE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_TOTAL_BALANCE')); ?> <?php endif; ?>:</b> 

		  <?php 

		  $mer_id   = Session::get('merchantid');

		  $balanceAmount  = Helper::totalAmount($mer_id,$requestid=1); 

       if(isset($balanceAmount) && $balanceAmount!=''){

		  echo $balanceAmount;

    }

		  ?>

		  </span></div>-->





                <div class="panel-body panel panel-default">

            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->

            <?php if($getTotalAmount->count() < 1): ?>

            <div class="no-record-area"> <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?> </div>

            <?php else: ?>

			

            <div class="table dish-table">

              <div class="tr">

                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Order_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Order_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Order_id')); ?> <?php endif; ?></div>

                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?></div>

                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Request_date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Request_date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Request_date')); ?> <?php endif; ?></div>

                <!--<div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Note')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Note')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Note')); ?> <?php endif; ?></div>-->

              </div>

              <?php $TotalAmount =0; ?>

              <?php $__currentLoopData = $getTotalAmount; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                



              <?php if(isset($value->payable_amount) && $value->payable_amount>1){ $ht='payable_amount';}else{ $ht='amount';}

              $Amt = $value->$ht; ?>

              <?php $vatamount=Helper::calculatevat($value->order_id,$Amt); 

                $totalamount=$value->$ht;
              ?>

              <div class="tr">

                <div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Order_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Order_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Order_id')); ?> <?php endif; ?>"><?php echo e(isset($value->order_id) ? $value->order_id : ''); ?></div>

                <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?>"><?php echo e(number_format($totalamount,2)); ?></div>

                <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Request_date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Request_date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Request_date')); ?> <?php endif; ?>"> <?php echo e(Carbon\Carbon::parse($value->created_at)->format('F j, Y')); ?> </div>

                <!--<div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Note')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Note')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Note')); ?> <?php endif; ?>"> <?php echo e(isset($value->note) ? $value->note : 'N/A'); ?></div>-->

              </div>

              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>

            <?php endif; ?>

            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->

          </div>

          <?php echo e($getTotalAmount->links()); ?> </div>

      </div>

    </div>

  </div>

</div>

<script>

 $(function() {

$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });

$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});

});



 $(function() {

 $("#dateTo").change(function () {

    var startDate = document.getElementById("dateFrom").value;

    var endDate = document.getElementById("dateTo").value;



    if ((Date.parse(startDate) > Date.parse(endDate))) {

        document.getElementById("dateTo").value = "";

         <?php if($mer_selected_lang_code !='en'): ?>

        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");

        <?php else: ?>

        $('#todata').html("End date should be greater than From date");

        <?php endif; ?>

      

    }

    else

    {

        $('#todata').hide();



    }

});

 });

   

</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $Travel_leftmenu =1; ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.css" />
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if($autoid==''): ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_PACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_PACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_PACKAGE')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_PACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPDATE_PACKAGE')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPDATE_PACKAGE')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box arabic-ltr">
              <!-- Display Message after submition -->
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?>
              <!-- Display Message after submition -->
              <form name="makeupartist" id="makeupartist" method="post" action="<?php echo e(route('store-travel-service')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PACKEGENAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKEGENAME'); ?></span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="model_name" maxlength="60" value="<?php echo e(isset($fetchdata->pro_title) ? $fetchdata->pro_title : ''); ?>" id="service_name"  >
                      </div>
                      <div class="arabic ar ">
                        <input type="text" class="arabic ar" name="model_name_ar" maxlength="60" value="<?php echo e(isset($fetchdata->pro_title_ar) ? $fetchdata->pro_title_ar : ''); ?>" id="model_name_ar" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SERVICE_PRICE'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SERVICE_PRICE'); ?> (%) </span> </label>
                    <div class="info100">
                      <div>
                        <input type="text" class="xs_small notzero" name="price"  onkeypress="return isNumber(event)"  maxlength="12" value="<?php echo e(isset($fetchdata->pro_price) ? $fetchdata->pro_price : ''); ?>" id="price" >
                      </div>
                    </div>
                  </div>

                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); ?> %</span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); ?>  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="<?php echo e(isset($fetchdata->pro_discount_percentage) ? $fetchdata->pro_discount_percentage : ''); ?>" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_User_Image'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_User_Image'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                      <?php if(isset($fetchdata->pro_Img)!=''): ?>
                      <div class="form-upload-img"> <img src="<?php echo e(isset($fetchdata->pro_Img) ? $fetchdata->pro_Img : ''); ?>" width="150" height="150"> </div>
                      <?php endif; ?> </div>
                  </div>

      
<!-- Add More product images start -->
        <?php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        ?>

        <?php for($i=0;$i < $GalleryCunt;$i++): ?>
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo<?php echo e($k); ?>">
        <div class="file-btn-area">
        <div id="file_value<?php echo e($J); ?>" class="file-value"></div>
        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
        </div>
        </label>
        <input id="company_logo<?php echo e($k); ?>" name="image[]" class="info-file proImgess" data-lbl="file_value<?php echo e($J); ?>" type="file" value="">
        </div>
        <?php if(isset($productGallery[$i]->image) && $productGallery[$i]->image!=''): ?>
        <div class="form-upload-img product_img_del">
        <img src="<?php echo e(isset($productGallery[$i]->image) ? $productGallery[$i]->image : ''); ?>" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="<?php echo e($productGallery[$i]->id); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value<?php echo e($J); ?>"></span>  
        <?php endif; ?>
        <input type="hidden" name="privius[]" value="<?php echo e($productGallery[$i]->id); ?>">
        </div>

        </div>
        <?php  $J=$J + 1; $k=$k +1; ?>
        <?php endfor; ?>
 
        <div id="img_upload_pro" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button_pro"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>
        <span class="error pictureformat"></span>
        <?php $Countpro =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Countpro = $GalleryCunt+1;} ?>                
        <input type="hidden" id="Countpro" name="Countpro" value="<?php echo e($Countpro); ?>">
        </div>
  <!-- Add More product images end -->

                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Location'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Location'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class=" " name="location" maxlength="90" value="<?php echo e(isset($Location->value) ? $Location->value : ''); ?>" id="location" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class=" " name="location_ar" maxlength="90" value="<?php echo e(isset($Location->value_ar) ? $Location->value_ar : ''); ?>" id="location_ar" required="" >
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <?php
                  $PDC = 1;
                  if(isset($PackageDate->value) && $PackageDate->value!='')
                  {
                  $getPD = explode(',', $PackageDate->value);
                  $PDC = count($getPD);
                  ?>
                  <div class="form_row_left common_field"> <?php for($i=0; $i< $PDC;$i++): ?>
                    <?php $getPDs = $getPD[$i]; if($getPDs==''){ continue;} ?>
                    <div class="add_row_wrap_new posrel" >
                      <div id="remove_button" class="remove_btn"><a href="javascript:void(0);" title="Remove field" class="status_active  status_active2 cstatus" data-status="Active" data-id="<?php echo e(isset($getPDs) ? $getPDs : ''); ?>" data-proid="<?php echo e(request()->autoid); ?>" style="float:left;">&nbsp;</a></div>
                      <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PackageSchedule'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PackageSchedule'); ?> </span> </label>
                      <div class="info100">
                        <div>
                          <input type="text" class="datetimepicker" name="package_schedule[]" maxlength="20" value="<?php echo e(isset($getPDs) ? $getPDs : ''); ?>" id="datetimepicker" required="" >
                        </div>
                      </div>
                      <!-- info100 -->
                    </div>
                    <?php endfor; ?>
                    <!-- form_row -->
                  </div>
                  <?php } else { ?>
                  <div class="form_row_left common_field posrel">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PackageSchedule'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PackageSchedule'); ?> </span> </label>
                    <div class="info100">
                      <input type="text" class="datetimepicker" name="package_schedule[]" maxlength="20" value="<?php echo e(isset($getPDs) ? $getPDs : ''); ?>" id="datetimepicker" required="" >
                    </div>
                    <!-- info100 -->
                  </div>
                  <?php } ?>
                  <div id="img_upload" class="common_field"></div>
                  <div class="form_row_left common_field">
                    <div id="add_button" class=""><a href="javascript:void(0);" class="form-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SERVICE_ADDMORE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SERVICE_ADDMORE'); ?> </span> </a></div>
                  </div>
                  <?php $Count = 1;?>
                  <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
                  <!-- form_row -->
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PackageDuration'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PackageDuration'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class=" " name="package_duration" maxlength="40" value="<?php echo e(isset($PackageDuration->value) ? $PackageDuration->value : ''); ?>" id="package_duration" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class=" " name="package_duration_ar" maxlength="40" value="<?php echo e(isset($PackageDuration->value_ar) ? $PackageDuration->value_ar : ''); ?>" id="package_duration_ar" required="" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.ExtraService'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.ExtraService'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea  class=" " name="extra_service" maxlength="255" id="extra_service" rows="4" cols="50" required="" ><?php echo e(isset($ExtraService->value) ? $ExtraService->value : ''); ?></textarea>
                      </div>
                      <div  class="arabic ar">

                        <textarea  class=" " name="extra_service_ar" maxlength="255" id="extra_service" rows="4" cols="50" required="" ><?php echo e(isset($ExtraService->value_ar) ? $ExtraService->value_ar : ''); ?></textarea>
                      
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea name="description" class="english" maxlength="500" id="description" rows="4" cols="50"><?php echo e(isset($fetchdata->pro_desc) ? $fetchdata->pro_desc : ''); ?> </textarea>
                      </div>
                      <div  class="arabic ar" >
                        <textarea class="arabic ar" name="description_ar"  maxlength="500" id="description_ar" rows="4" cols="50"><?php echo e(isset($fetchdata->pro_desc_ar) ? $fetchdata->pro_desc_ar : ''); ?></textarea>
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                      <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
                      <input type="hidden" name="autoid" value="<?php echo e(request()->autoid); ?>">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <!-- form_row -->
                    <div class="arabic ar arabic_left">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                    <!-- form_row -->
                  </div>
                </div>
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>  
  $(document).ready(function(){
  var maxField = 5;  
  var addButton = $('#add_button');  
  var wrapper = $('#img_upload');
  var x = <?php echo $Count; ?>; 

  $(addButton).click(function(){  
  if(x < maxField){  
  x++; 
  var main = x;

  var testElement= document.getElementById('english_tab');
  var result = testElement.classList.contains('active')

  var enclass=''
  var arclass='f'


  var fieldHTML = '<div class="add_row_wrap_new main'+x+' "><div id="remove_button" class="remove_btn" onclick="javascript: removemain('+main+')"><a href="javascript:void(0);"  title="Remove field" > &nbsp; </a></div><div class="form_row"><label class="form_label"><span class=" '+enclass+' "><?php if(Lang::has(Session::get('mer_lang_file').'.PackageSchedule')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PackageSchedule')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PackageSchedule')); ?> <?php endif; ?> </span></label><div class="info100"><div class="has-success"><div class="p '+enclass+' "><input type="text" class="datetimepicker" name="package_schedule[]" maxlength="20"> </div></div></div></div>'; 
  $(wrapper).append(fieldHTML);  
  createValidation();
  document.getElementById('count').value = parseInt(x);
  }
  });
   
  });
  
  function removemain(val)
  {
       
      var x = document.getElementById('count').value;
      $('.main'+val).remove();
      x--;  
      document.getElementById('count').value = parseInt(x);

  }

       
 $("form").data("validator").settings.ignore = "";
 </script>
<script>
 $(function() {
 
$( "#datepicker" ).datetimepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});
 
</script>
<script>

$("form").data("validator").settings.ignore = "";
</script>
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a> </div>
  </div>
</div>
<!-- action_popup -->
<script>
/* Action Popup */
jQuery('.cstatus').click(function(){
 var id =jQuery(this).data("id");
  var proid =jQuery(this).data("proid");
 jQuery('.action_yes').attr('data-id',id);
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
 
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

 
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-attribute')); ?>",
        data: {times:id,proids:proid},
        success: function(data) {
         
            if(data==1){
              location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script>
<script>
$("form").data("validator").settings.ignore = "";
</script>
<script type="text/javascript">  
$("#makeupartist").validate({

                  ignore: [],
                  rules: {
                
                       model_name: {
                       required: true,
                      },

                      model_name_ar: {
                       required: true,
                      },
 
                       price: {
                       required: true,
                      },

                      location: {
                       required: true,
                      },
                      "package_schedule[]": {
                       required: true,
                      },
                      package_duration: {
                       required: true,
                      },
                      extra_service: {
                       required: true,
                      },

                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

 
                       <?php if(isset($fetchdata->pro_Img)!=''): ?>  
                        stor_img: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       <?php else: ?>
                        stor_img: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      <?php endif; ?>


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
                model_name: {
                required: "<?php echo lang::get('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME'); ?>",
                },  
                model_name_ar: {
                required: "<?php echo lang::get('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME'); ?>",
                },  

                location: {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_LOCATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_LOCATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_LOCATION')); ?> <?php endif; ?>",
                }, 

                "package_schedule[]": {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_SCHEDULE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_SCHEDULE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PACKAGE_SCHEDULE')); ?> <?php endif; ?>",
                },   

                price: {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?> ",
                },


                package_duration: {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_DURATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_DURATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PACKAGE_DURATION')); ?> <?php endif; ?> ",
                },


                extra_service: {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_EXTRA_SERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_EXTRA_SERVICES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_EXTRA_SERVICES')); ?> <?php endif; ?> ",
                },
                description: {          
                required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); ?>",
                },

                description_ar: {          
                required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_SINGER_AR'); ?>",
                },

                stor_img: {
                accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                },   

                                                  
                     
                },

                invalidHandler: function(e, validation){
       
                    var valdata=validation.invalid;

       <?php if($mer_selected_lang_code !='en'): ?>
              if (typeof valdata.model_name != "undefined" || valdata.model_name != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.location != "undefined" || valdata.location != null) 
              {
              $('.english_tab').trigger('click');
              }
              if (typeof valdata.package_schedule != "undefined" || valdata.package_schedule != null) 
              {
              $('.english_tab').trigger('click');
              }
              if (typeof valdata.package_duration != "undefined" || valdata.package_duration != null) 
              {
              $('.english_tab').trigger('click');
              }
              if (typeof valdata.price != "undefined" || valdata.price != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.extra_service != "undefined" || valdata.extra_service != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.description != "undefined" || valdata.description != null) 
              {
              $('.english_tab').trigger('click');
             }
              if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
              {
              $('.english_tab').trigger('click');    
              }

              if (typeof valdata.model_name_ar != "undefined" || valdata.model_name_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
              if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
          <?php else: ?>

              if (typeof valdata.model_name_ar != "undefined" || valdata.model_name_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
              if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
               if (typeof valdata.extra_service_ar != "undefined" || valdata.extra_service_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
              if (typeof valdata.model_name != "undefined" || valdata.model_name != null) 
              {
              $('.english_tab').trigger('click'); 

              }
              if (typeof valdata.location != "undefined" || valdata.location != null) 
              {
              $('.english_tab').trigger('click');


              }
              if (typeof valdata.package_schedule != "undefined" || valdata.package_schedule != null) 
              {
              $('.english_tab').trigger('click');


              }
              if (typeof valdata.package_duration != "undefined" || valdata.package_duration != null) 
              {
              $('.english_tab').trigger('click');


              }
              if (typeof valdata.price != "undefined" || valdata.price != null) 
              {
              $('.english_tab').trigger('click'); 


              }
              if (typeof valdata.extra_service != "undefined" || valdata.extra_service != null) 
              {
              $('.english_tab').trigger('click'); 


              }
              if (typeof valdata.description != "undefined" || valdata.description != null) 
              {

              $('.english_tab').trigger('click');


              }

              if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
              {

              $('.english_tab').trigger('click');     


              }
                    
                <?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script>
<script src="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>
<script> 
$(window).load(function(){
$("body").on('mouseover', '.datetimepicker', function() {  
$(this).datetimepicker({

ampm: true, // FOR AM/PM FORMAT
    format : 'Y-m-d g:i A' });
 
});

})
 
</script>



<!-- Add More product images start -->
 
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button_pro');  
    var wrapper = $('#img_upload_pro');
    var x = <?php echo $Countpro;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         <?php if(Session::get('lang_file') =='ar_lang'): ?>
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        <?php else: ?>
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        <?php endif; ?>
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-shop-image')); ?>",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->





<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
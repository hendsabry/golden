<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php 

global $Current_Currency;

$Current_Currency  = Session::get('currency'); 

if($Current_Currency =='') { 

$Current_Currency = 'SAR'; 

} 

?>

<div class="outer_wrapper"><?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  <div class="inner_wrap"> 

  <div class="search-section">

<div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>



  <?php $basecategory_ids =Session::get('searchdata.mainselectedvalue');  ?> 



  <?php if($basecategory_ids ==2): ?>

<?php echo $__env->make('includes.checkoutsearch', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php else: ?>

<?php echo $__env->make('includes.checkoutbussinesssearch', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php endif; ?>



</div>

    <!-- search-section -->

    <div class="page-left-right-wrapper main_category not-sp">

   



        <?php if($errors->any()): ?>

        <div class="alert alert-danger">

        <ul>

        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <li><?php echo e($error); ?></li>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </ul>

        </div>

        <?php endif; ?>

      <?php echo Form::open(['route' => 'chkoutinfo', 'method' => 'post', 'name'=>'chekoutinfo', 'id'=>'chekoutinfo']); ?> 

         <div class="checkout-area">

   

   <div class="checkout-left-area">

   <div class="checkout-page-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Personal_Info')!= '')  ?  trans(Session::get('lang_file').'.Personal_Info'): trans($OUR_LANGUAGE.'.Personal_Info')); ?></div>

   <div class="checkout-box">

   <div class="checkout-form">

   <div class="checkout-form-row">

   <div class="checkout-form-cell">

   <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Name')!= '')  ?  trans(Session::get('lang_file').'.Name'): trans($OUR_LANGUAGE.'.Name')); ?></div> 

   <div class="checkout-form-bottom"><input type="text" name="uname" value="<?php if(isset($User->cus_name) && $User->cus_name!=''){echo $User->cus_name;}else{echo $getcustomershippingaddress->ship_name;}?>" maxlength="60" class="t-box" /></div> 

   </div> <!-- checkout-form-cell -->

   

   <div class="checkout-form-cell">

   <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Email')!= '')  ?  trans(Session::get('lang_file').'.Email'): trans($OUR_LANGUAGE.'.Email')); ?></div> 

   <div class="checkout-form-bottom"><input type="email" name="email"  value="<?php echo e($User->email); ?>" maxlength="60" class="t-box" /></div> 

   </div> <!-- checkout-form-cell -->

   <?php if(isset($getcustomershippingaddress->ship_phone) && $getcustomershippingaddress->ship_phone!=''){$rec_array = explode('-',$getcustomershippingaddress->ship_phone);} ?>

   </div> <!-- checkout-form-row --> 

   <div class="checkout-form-row">

   <div class="checkout-form-cell country_row">

   <div class="checkout-form-top">  <?php echo e((Lang::has(Session::get('lang_file').'.Telephone_Number')!= '')  ?  trans(Session::get('lang_file').'.Telephone_Number'): trans($OUR_LANGUAGE.'.Telephone_Number')); ?></div> 

   <div class="checkout-form-bottom">

    <?php 
    $concode=''; 
    if(isset($User->cus_phone) && $User->cus_phone!=''){$cusphone = $User->cus_phone;}else{$cusphone = $getcustomershippingaddress->ship_phone;}
    $getphone = explode('-',$cusphone);   
    ?>

    <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode check_count">

      <?php $__currentLoopData = $getCountry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ccode): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <option value="<?php echo e($Ccode->country_code); ?>" <?php if(isset($getphone[0]) && $getphone[0]==$Ccode->country_code){ ?> selected <?php } ?>>+<?php echo e($Ccode->country_code); ?></option>     

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </select>

  
   
   <input type="text" value="<?php if(isset($getphone[1]) && $getphone[1]!=''){echo $getphone[1];}else{echo $getcustomershippingaddress->ship_phone;} ?>" name="phone" maxlength="10" onkeypress="return isNumber(event)"  class="t-box checkout-small-box check_input" />

   </div> 

   <label for="country_code" class="error"></label>

   <label for="phone" class="error"></label>

   </div> <!-- checkout-form-cell -->

   <div class="checkout-form-cell">

   <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Gender')!= '')  ?  trans(Session::get('lang_file').'.Gender'): trans($OUR_LANGUAGE.'.Gender')); ?></div> 

   <div class="checkout-form-bottom">

   <div class="gender-radio-box"><input id="male" <?php if(isset($User->gender) && $User->gender==0){echo 'Checked';} ?> name="gender" value="0" type="radio"> <label for="male"><?php echo e((Lang::has(Session::get('lang_file').'.Male')!= '')  ?  trans(Session::get('lang_file').'.Male'): trans($OUR_LANGUAGE.'.Male')); ?> </label></div>

   <div class="gender-radio-box"><input id="female" name="gender" <?php if(isset($User->gender) && $User->gender==1){echo 'Checked';} ?> value="1" type="radio"> <label for="female"><?php echo e((Lang::has(Session::get('lang_file').'.Female')!= '')  ?  trans(Session::get('lang_file').'.Female'): trans($OUR_LANGUAGE.'.Female')); ?> </label></div>

   <label for="gender" class="error"></label>

   </div> 

   </div> <!-- checkout-form-cell -->

   </div> <!-- checkout-form-row --> 

   

    <?php   



      $days = '0';

      $mon = '0';

      $yr = '0';

       

      ?>



   <div class="checkout-form-row">

   <?php /*?>

   <div class="checkout-form-cell">

   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Date_of_Birth')!= '')  ?  trans(Session::get('lang_file').'.Date_of_Birth'): trans($OUR_LANGUAGE.'.Date_of_Birth')}}</div> 

   <div class="checkout-form-bottom checkout-dob"> 

    <select name="day">

  <option value="">{{ (Lang::has(Session::get('lang_file').'.DD')!= '')  ?  trans(Session::get('lang_file').'.DD'): trans($OUR_LANGUAGE.'.DD')}}</option>

  @for($i=1;$i<=31;$i++)

  <?php if($i<=9){ $k= '0'.$i; }else{ $k= $i; } ?>  

  <option  value="{{$k}}" @if($days== $k) {{'SELECTED'}}  @endif >{{$k}}</option>

  @endfor

  </select>

  <select name="month">

  <option value="">{{ (Lang::has(Session::get('lang_file').'.MM')!= '')  ?  trans(Session::get('lang_file').'.MM'): trans($OUR_LANGUAGE.'.MM')}}</option>

  @for($i=1;$i<=12;$i++)

  <?php if($i<=9){ $k= '0'.$i; }else{ $k= $i; } ?>  

  <option  value="{{$k}}"  @if($mon== $k) {{'SELECTED'}}  @endif >{{$k}}</option>

  @endfor

  </select>

  <select name="year">

  <option value="">  {{ (Lang::has(Session::get('lang_file').'.YYYY')!= '')  ?  trans(Session::get('lang_file').'.YYYY'): trans($OUR_LANGUAGE.'.YYYY')}}</option>

@for($i=(date("Y")-5);$i >=1960;$i--)

  <option  value="{{$i}}"  @if($yr== $i) {{'SELECTED'}}  @endif >{{$i}}</option>

  @endfor

  </select></div> 

  <label for="day" class="error"></label>  <label for="month" class="error"></label>  <label for="year" class="error"></label>

   </div> <!-- checkout-form-cell -->

   

   <div class="checkout-form-cell">

                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.DATE_OF_BIRTH')!= '')  ?  trans(Session::get('lang_file').'.DATE_OF_BIRTH'): trans($OUR_LANGUAGE.'.DATE_OF_BIRTH')}}</div>

                <div class="checkout-form-bottom">

                <input type="text" class="t-box cal-t checkout-small-box" name="dob" id="dob" value="{{ $User->dob or ''}}" autocomplete="off"/>

                </div>

                @if($errors->has('dob'))<span class="error">{{ $errors->first('dob') }}</span>@endif

              </div><?php */?>

    <div class="checkout-form-cell">

   <div class="checkout-form-top"> <?php echo e((Lang::has(Session::get('lang_file').'.Country')!= '')  ?  trans(Session::get('lang_file').'.Country'): trans($OUR_LANGUAGE.'.Country')); ?></div> 

   <div class="checkout-form-bottom not-edit">



<?php if(Session::get('lang_file')!='en_lang') { $lang='ar'; } else{ $lang='en'; }  ?>

<select class="checkout-small-box" name="country"  id="country" onchange="getCity(this.value,'<?php echo e($lang); ?>');getVat(this.value,'<?php echo e($lang); ?>')">

  <option value="">  <?php echo e((Lang::has(Session::get('lang_file').'.Select')!= '')  ?  trans(Session::get('lang_file').'.Select'): trans($OUR_LANGUAGE.'.Select')); ?> </option>

  <?php $__currentLoopData = $getCountry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ctry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

  <option value="<?php echo e($Ctry->co_id); ?>"><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($Ctry->co_name_ar); ?> <?php else: ?> <?php echo e($Ctry->co_name); ?>   <?php endif; ?></option>

  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  </select>





   </div> 

   </div> <!-- checkout-form-cell -->

   <div class="checkout-form-cell">

   <div class="checkout-form-top"> <?php echo e((Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')); ?></div> 

   <div class="checkout-form-bottom">



<select class="checkout-small-box" id="city" name="city">

<option value=""><?php echo e((Lang::has(Session::get('lang_file').'.Select')!= '')  ?  trans(Session::get('lang_file').'.Select'): trans($OUR_LANGUAGE.'.Select')); ?></option>

 



</select>



</div> 

   </div>

   

   </div><!-- checkout-form-row --> 





   <div class="checkout-form-row">

  

   <div class="checkout-form-cell address-cell">

   <div class="checkout-form-top"><?php echo e((Lang::has(Session::get('lang_file').'.Address')!= '')  ?  trans(Session::get('lang_file').'.Address'): trans($OUR_LANGUAGE.'.Address')); ?></div> 

   <div class="checkout-form-bottom">



<textarea name="address1" id="address1" rows="3" cols="40" maxlength="350"><?php if(isset($User->cus_address1) && $User->cus_address1!=''){echo $User->cus_address1;}else{echo $getcustomershippingaddress->ship_address1;}?></textarea>



 </div> 

   <label for="address1" class="error"></label>

   </div>

    <!-- checkout-form-cell -->

   

   <div class="checkout-form-cell">

   <div class="checkout-form-top"> <?php echo e((Lang::has(Session::get('lang_file').'.Pin_Code')!= '')  ?  trans(Session::get('lang_file').'.Pin_Code'): trans($OUR_LANGUAGE.'.Pin_Code')); ?></div> 

   <div class="checkout-form-bottom"><input type="text" id="pincode" name="pincode" value="<?php if(isset($User->ship_postalcode) && $User->ship_postalcode!=''){echo $User->ship_postalcode;}else{echo $getcustomershippingaddress->ship_postalcode;}?>" maxlength="7" class="t-box checkout-small-box" /></div> 

   </div>

   

   

   </div> <!-- checkout-form-row --> 

   

   <div class="checkout-form-row">

    <!-- checkout-form-cell -->

   







    <!-- checkout-form-cell -->

   </div> <!-- checkout-form-row --> 

   

   </div> <!-- checkout-form -->

   

   </div> <!-- checkout-box -->



 



<div class="checkout-page-heading" id="shipbox"><?php echo e((Lang::has(Session::get('lang_file').'.Select_Shipping_Companies_of_Following_Product')!= '')  ?  trans(Session::get('lang_file').'.Select_Shipping_Companies_of_Following_Product'): trans($OUR_LANGUAGE.'.Select_Shipping_Companies_of_Following_Product')); ?></div>



<div class="checkout-box"  id="shipbox1"> 



<?php $K=1; $ChkShip=0; ?>

<?php $__currentLoopData = $vendorList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php

$Sub = $val->cart_sub_type;

?>

<div class="check_shipping_area">

 <div class="shipping-companies-heading"> 

<?php echo $name = $val->fname.' '.$val->lname; ?>

</div>





<?php $__currentLoopData = $Sub; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $SubProv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php 

$aramxe = $SubProv->products[0]->shipping_methods['aramax'];

  $merchant_id = $SubProv->products[0]->merchant_id;

$picks = $SubProv->products[0]->shipping_methods['pick_logistics'];

if($aramxe ==0 && $picks==0){ continue; }

?>

 

  <div class="shipping-companies-wrapper">

  <div class="shipping-companies-heading">    

  <?php echo ucfirst($SubProv->cart_sub_type); ?>

  </div>     



<?php if($aramxe==1): ?>

  <!-- ARAMEX SHIPPING -->

<?php $ChkShip=$ChkShip+1; ?>

    <div class="shipping-companies-line">

    <div class="scl1">





    <div class="scl1-radio"><input type="radio" data-price="0"  data-ship="picks_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" data-aship="Aramex_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" data-value="1" data-merchant_id="<?php echo e($merchant_id); ?>" data-cart_sub_type="<?php echo e($SubProv->cart_sub_type); ?>" data-productid="<?php echo e($SubProv->products[0]->product_id); ?>" class="shiping" required   name="shipping_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" 

      id="Aramex_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" value="0" /> <label>&nbsp;</label></div>

    <div class="scl1-img"><label for="Aramex_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>"><img src="<?php echo e(url('/')); ?>/public/assets/img/aramex.png" alt="" /></label></div>

    <div class="scl1-name"><label for="Aramex_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Aramex')!= '')  ?  trans(Session::get('lang_file').'.Aramex'): trans($OUR_LANGUAGE.'.Aramex')); ?></label></div>

    </div>





    <span id="Aramex_priceCalc_<?php echo e($SubProv->products[0]->product_id); ?>">

    <div class="scl2"><?php echo e(Session::get('currency')); ?> 0</div> 

    </span>

    

    <label class="error" for="shipping_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" style="margin-top:8px;"></label>



    </div> <!-- shipping-companies-line -->

<?php endif; ?>



<?php if($picks==1): ?>

  <!-- picklogistics SHIPPING -->

   <?php $ChkShip=$ChkShip+1; ?>

   <div class="shipping-companies-line">  

   <div class="scl1 ">

   <div class="scl1-radio"><input type="radio"  data-price="0" data-ship="picks_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" data-aship="Aramex_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" data-value="2"  data-productid="<?php echo e($SubProv->products[0]->product_id); ?>" class="shiping"  data-merchant_id="<?php echo e($merchant_id); ?>" name="shipping_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" required="" id="picks_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>" data-cart_sub_type="<?php echo e($SubProv->cart_sub_type); ?>" value="0" /> <label>&nbsp;</label></div>

   <div class="scl1-img"><label for="picks_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>"><img src="<?php echo e(url('/')); ?>/public/assets/img/picklogistics.png" alt="" /></label></div>

   <div class="scl1-name"><label for="picks_<?php echo e($SubProv->products[0]->product_id); ?>_<?php echo e($K); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Pick_Logistics')!= '')  ?  trans(Session::get('lang_file').'.Pick_Logistics'): trans($OUR_LANGUAGE.'.Pick_Logistics')); ?> </label></div>

   </div>

 <span id="Picks_priceCalc_<?php echo e($SubProv->products[0]->product_id); ?>">

    <div class="scl2"><?php echo e(Session::get('currency')); ?> 0</div> 

    </span>

   </div> <!-- shipping-companies-line -->





<?php endif; ?>



 <label for="shipping_1013" class="error"></label>

   </div> <!-- shipping-companies-wrapper -->

<?php $K=$K + 1; ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

</div>

  



<?php

 $shippingneed =0;

 $shipp = array('music','shopping','occasion');

 $M=1;

 $ishallincart=0;

 foreach($cart_pro as $valss)

 {

  $Ctype = $valss->cart_type;

  if($Ctype=='hall' || $Ctype=='travel'){ $ishallincart=1; }

  $cartsub_type = $valss->cart_sub_type;

  if($cartsub_type=='invitations'){ $ishallincart=1; }

  if($cartsub_type == 'invitations'){ continue;}

  if(!in_array($Ctype,$shipp)){ continue;}else{  $M = $M+1; }

 

 }

 

?>

 



 <div class="checkout-page-heading"> <?php echo e((Lang::has(Session::get('lang_file').'.Payment_Option')!= '')  ?  trans(Session::get('lang_file').'.Payment_Option'): trans($OUR_LANGUAGE.'.Payment_Option')); ?></div>

   <div class="checkout-box po-checkout">

   <div class="payment-option-area">





 <div class="shipping-companies-wrapper">



   <div class="shipping-companies-line rds" id="creditCrd">

   <div class="scl1 notpadding">

   <div class="scl1-radio"><input type="radio" name="payment" id="cc" value="1" /> <label>&nbsp;</label></div> 

   <div class="scl1-name"><label for="cc"><?php echo e((Lang::has(Session::get('lang_file').'.Credit_Debit_Cards')!= '')  ?  trans(Session::get('lang_file').'.Credit_Debit_Cards'): trans($OUR_LANGUAGE.'.Credit_Debit_Cards')); ?> </label></div>

   </div>   

   </div> <!-- shipping-companies-line -->

   



 



   <div class="shipping-companies-line rds" id="wallet">

   <div class="scl1 notpadding">

   <div class="scl1-radio"><input type="radio" name="payment" id="wlt" value="2" /> <label>&nbsp;</label></div> 

   <div class="scl1-name"><label for="wlt"><?php echo e((Lang::has(Session::get('lang_file').'.Mobile_Wallets')!= '')  ?  trans(Session::get('lang_file').'.Mobile_Wallets'): trans($OUR_LANGUAGE.'.Mobile_Wallets')); ?> </label></div>

   </div>   

   </div> <!-- shipping-companies-line -->



<?php if($ChkShip >=1 && $ishallincart<1): ?>

   <div class="shipping-companies-line rds" id="Wiretfr">

   <div class="scl1 notpadding">

   <div class="scl1-radio"><input type="radio" id="wt" name="payment" value="3" /> <label>&nbsp;</label></div> 

   <div class="scl1-name"><label for="wt"><?php echo e((Lang::has(Session::get('lang_file').'.Wire_Transfer')!= '')  ?  trans(Session::get('lang_file').'.Wire_Transfer'): trans($OUR_LANGUAGE.'.Wire_Transfer')); ?> </label></div>

   </div>   

   </div> <!-- shipping-companies-line -->

<?php endif; ?>

   <label for="payment" class="error"></label>

   </div> <!-- shipping-companies-wrapper -->

    





   <div class="payment-option-prise" id="walletdis"><span class="pay_great"><?php echo e((Lang::has(Session::get('lang_file').'.Wallet_Deduct')!= '')  ?  trans(Session::get('lang_file').'.Wallet_Deduct'): trans($OUR_LANGUAGE.'.Wallet_Deduct')); ?> </span><span id="wltddcut"> </span> </div>



   <div class="payment-option-prise" id="total_amtdis"><?php echo e(Session::get('currency')); ?> <span id="oTotal"> </span> </div>







   <div class="payment-option-btn"><input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.Pay_Now')!= '')  ?  trans(Session::get('lang_file').'.Pay_Now'): trans($OUR_LANGUAGE.'.Pay_Now')); ?>" class="form-btn btn-info-wisitech" /></div>

   

   </div> <!-- payment-option-area -->

   </div> <!-- checkout-box -->

   

    </div> <!-- checkout-left-area -->



  <div class="checkout-right-area">



<?php

$iscoupan=0;

foreach($cart_pro as $vals)

  {   

    $Carttype = $vals->cart_type;

    if($Carttype=='singer' || $Carttype=='band'){ $iscoupan=0; }else{ $iscoupan=$iscoupan+1; }



  }

?>

<?php if($iscoupan>0): ?>

<div class="couponcode">

<input type="button" name="addcounpons" value="<?php echo e((Lang::has(Session::get('lang_file').'.APPLY_COUPONS')!= '')  ?  trans(Session::get('lang_file').'.APPLY_COUPONS'): trans($OUR_LANGUAGE.'.APPLY_COUPONS')); ?>" class="form-btn popcoupons">

</div>

<?php endif; ?>



   <div class="checkout-page-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Order_Summary')!= '')  ?  trans(Session::get('lang_file').'.Order_Summary'): trans($OUR_LANGUAGE.'.Order_Summary')); ?></div>

   

 

   <div class="order-summary-box pay-now-os">

    <div class="order-summary-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Pay_Now')!= '')  ?  trans(Session::get('lang_file').'.Pay_Now'): trans($OUR_LANGUAGE.'.Pay_Now')); ?> </div>

  

  <div class="os-row-area">



<?php 

 $TP                   = 0; 

 $jk                   = 0;

 $total_amount_of_cart = 0;

 $cAmount =0;

 $final_insurance_amount=0;

 foreach($cart_pro as $vals)

 {



   $jk++;

   if($vals->cart_type=='hall')

   {



     $dprice = $vals->paid_total_amount;

     $TP =  $vals->paid_total_amount;



   }

   else

   {

     $dprice = $vals->total_price;

     $TP     =  $vals->total_price;

   }

   $final_insurance_amount=$final_insurance_amount+$vals->insurance_amount;

   $total_amount_of_cart = $total_amount_of_cart + $TP;

   $dprice=currency($dprice, 'SAR',$Current_Currency, $format = false);

   $tprice=currency($total_amount_of_cart, 'SAR',$Current_Currency, $format = false);

    if(Session::get('lang_file')=='ar_lang'){ $langtitle=$vals->pro_title_ar; }else{ $langtitle=$vals->pro_title; };

     if($vals->cart_type=='car_rental'){ $carrental='Car rental'; }else{ $carrental=''; }



     if($vals->cart_type=='travel'){ 



     //$travelrent='Travel';



     if(Lang::has(Session::get('lang_file').'.TRAVEL')!= ''){ $travelrent=trans(Session::get('lang_file').'.TRAVEL');}else{ $travelrent=trans($OUR_LANGUAGE.'.TRAVEL');}



      }else{ 

      $travelrent='';

       }



      if($vals->cart_type=='food'){ 



     //$travelrent='Travel';



     if(Lang::has(Session::get('lang_file').'.CHECKFOOD')!= ''){ $food=trans(Session::get('lang_file').'.CHECKFOOD');}else{ $food=trans($OUR_LANGUAGE.'.CHECKFOOD');}



      }else{ 

      $food='';

       }



     //echo $vals->shop_id;

    if(isset($vals->shop_id) && $vals->shop_id!='')

    {

    $shopname=Helper::getparentCat($vals->shop_id);

    if(count($shopname)< 1){  $shname = '';  }else{

        if(Session::get('lang_file')=='ar_lang')

        {

        $shname = $shopname->mc_name_ar;

          }else{

          $shname = $shopname->mc_name;

        }

    }

  }

    else

    {

    $shname = ''; 

    }



    if($vals->cart_type=='singer'){ $shname='Singer'; }elseif($vals->cart_type=='band'){ $shname='Band'; }else{ $shname=$shname; }





    



      if($vals->cart_type=='recording' ){ continue;}

   ?>

    <div class="os-row">

    <div class="os-row-no"> <?php echo e($jk); ?> </div>

    <div class="os-cell1"> <?php if($vals->cart_type=='hall'): ?> Hall  <?php endif; ?> <?php if($vals->pro_title==''): ?> &nbsp; <?php else: ?> <?php echo e($langtitle); ?> <?php endif; ?> <?php echo e(isset($carrental) ? $carrental : ''); ?><?php echo e(isset($travelrent) ? $travelrent : ''); ?> <?php echo e($food); ?></div>

    <div class="os-cell2"> 

      <?php if($vals->cart_type!='singer' && $vals->cart_type!='band'): ?>

     <?php if(count($vals->getProduct)>=1 && $vals->pro_title==''): ?><?php echo e(isset($vals->getProduct[0]->pro_title) ? $vals->getProduct[0]->pro_title : "&nbsp;"); ?> <br/>



      <?php endif; ?> <?php endif; ?> <?php echo e(isset($shname) ? $shname : ''); ?> 





   </div> 

    <div class="os-cell3"> <?php echo e(number_format($dprice, 2)); ?> </div>

  </div> <!-- os-row -->



<?php if(trim($vals->coupon_code)!='' || $vals->coupon_code != NULL): ?>

<?php $cAmount = $vals->coupon_code_amount + $cAmount; ?>

  <div class="os-row">

    <div class="os-row-no"> &nbsp; &nbsp; </div>

    <div class="os-cell1">   <?php echo e($vals->coupon_code); ?> </div>

    <div class="os-cell2"> &nbsp;  &nbsp; &nbsp; </div> 

    <div class="os-cell3">  - <?php echo e(Session::get('currency')); ?> <?php echo e(number_format($vals->coupon_code_amount, 2)); ?></div>

  </div> <!-- os-row -->



<?php endif; ?>

<?php } ?>

 

 

    <input type="hidden" name="couponamount" id="couponamount" value="<?php echo e($cAmount); ?>">

    <?php $tprice= $tprice-$cAmount; ?>

  </div> <!-- os-row-area -->

  

<div class="os-total-line">

  <div class="os-total-left">  <?php echo e((Lang::has(Session::get('lang_file').'.VAT')!= '')  ?  trans(Session::get('lang_file').'.VAT'): trans($OUR_LANGUAGE.'.VAT')); ?></div>

  <div class="os-total-right" id="vat"><?php echo e(Session::get('currency')); ?> 0</div>

  </div>





  <div class="os-total-line">

  <div class="os-total-left"><?php echo e((Lang::has(Session::get('lang_file').'.Total_Shipping')!= '')  ?  trans(Session::get('lang_file').'.Total_Shipping'): trans($OUR_LANGUAGE.'.Total_Shipping')); ?></div>

  <div class="os-total-right" id="totalshipping"><?php echo e(Session::get('currency')); ?> 0</div>

  </div>







  <div class="os-total-line">

  <div class="os-total-left"><?php echo e((Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')); ?></div>

  <div class="os-total-right"><?php echo e(Session::get('currency')); ?> <span id="gtp"><?php echo e(number_format($tprice, 2)); ?></span></div>

   <?php 

  $getvatamount=$tprice-$final_insurance_amount;

  ?>

  <input type="hidden" name="OTP" id="OTP" value="<?php echo e($getvatamount); ?>">





  </div>

  

  



   </div> <!-- order-summary-box -->

   

  

   </div> <!-- checkout-right-area -->

  </div> <!-- checkout-area -->



<input type="hidden" name="wallet" value="<?php echo e($User->wallet); ?>">

<input type="hidden" name="hall_pending_amount" value="0">

<input type="hidden" name="total_amount" id="totalprice" value="<?php echo e($tprice); ?>">

<input type="hidden" name="orgprice" id="orgprice" value="<?php echo e($tprice); ?>">

<input type="hidden" name="wallet_amount_deducted" id="wallet_amount_deducted" value="0">

<input type="hidden" name="vat" id="Gvat" value="0">

<input type="hidden" name="vat_tax" id="vat_tax" value="0">

<input type="hidden" name="vtotalshipping" id="Gtotalshipping" value="0">



<?php echo Form::close(); ?>


  

      <!-- page-right-section   -->

    </div>

    <!-- page-left-right-wrapper -->

  </div>

  <!-- outer_wrapper -->

</div>

<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 <script>

/* Mobile Number Validation */

 function isNumber(evt) {

   evt = (evt) ? evt : window.event;

   var charCode = (evt.which) ? evt.which : evt.keyCode;

   if (charCode > 31 && (charCode < 48 || charCode > 57)) {

   return false;

   }

   return true;

 }

</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

//var jq = $.noConflict();

jQuery( function() {

  jQuery( "#dob" ).datepicker({

    dateFormat: "d M, yy",

    changeMonth: true,

    changeYear: true,

    yearRange: "-80:+0" // last hundred years

  });

} );

</script>

<script>

 <?php $Cur = Session::get('currency'); ?>

 function getCity(cntryid,lan)

 {

     $.ajax({

           type:"GET",

           url:"<?php echo e(url('getcitylist')); ?>?cntry_id="+cntryid+'&lang='+lan,

           success:function(res){ 

 

            $('#city').html(res);

           }

        });

  }

  function getVat(cntryid,lan)

  {  

    if(cntryid=='')

    {

  

            $('#Gvat').html(0);

            var op = $('#orgprice').val();

            var tp = parseFloat(op);         

            var walltamt = <?php echo $User->wallet;?>;

            var couponAmt = 0;

          if(tp > walltamt)

          {

            var PriceAfterWallet = parseFloat(tp)-parseFloat(walltamt)-parseFloat(couponAmt);

            walltamt = parseFloat(walltamt).toFixed(2);

            PriceAfterWallet = parseFloat(PriceAfterWallet).toFixed(2); 

            $('#gtp').html(PriceAfterWallet);

            $('#totalprice').val(PriceAfterWallet);

            $('#vat').html('<?php echo $Cur;?> 0');         

            $('#wallet').hide(); 

            $("#wlt").attr('checked', false);

            $('#creditCrd').show();

            $('#walletdis').hide();

            $('#total_amtdis').show();

            

            $('#Wiretfr').show();

            $('#oTotal').html(PriceAfterWallet);

            $('#wltddcut').html('<?php echo $Cur;?> '+walltamt);

            $('#wallet_amount_deducted').val(walltamt);

            $('#totalprice').val(PriceAfterWallet);

          }

          else

          {

            $('#wallet').show();

            $("#wlt").attr('checked', 'checked');

            $('#creditCrd').hide();

                $('#walletdis').show();

            $('#total_amtdis').hide();

            $('#Wiretfr').hide();

            var PriceAfterWallet = parseFloat(walltamt)-parseFloat(tp)-parseFloat(couponAmt);

            PriceAfterWallet = parseFloat(PriceAfterWallet).toFixed(2);

            $('#oTotal').html(0);

            $('#wltddcut').html('<?php echo $Cur;?> '+PriceAfterWallet);

            $('#wallet_amount_deducted').val(PriceAfterWallet);

            $('#totalprice').val(0);

          }



    }

    else

    {



      var tp =  $('#OTP').val();

        $.ajax({

           type:"GET",

           url:"<?php echo e(url('getVat')); ?>?country_id="+cntryid+'&lang='+lan+'&totalPrice='+tp,

           success:function(res){ 

            

            var values=res.split('~');

            var vatamount=values[0];

            var vatpercent=values[1];

            $('#Gvat').val(vatamount);

            var op = $('#orgprice').val();

            var couponAmt = 0;

            var tp = parseFloat(op) + parseFloat(vatamount) -  parseFloat(couponAmt);

            tp = parseFloat(tp).toFixed(2);

            $('#gtp').html(tp);

            $('#totalprice').val(tp);

            $('#vat_tax').val(vatpercent);

            

            var walltamt = <?php echo $User->wallet;?>;

            if(tp > walltamt)

            {         

            $('#wallet').hide(); 

            $("#wlt").attr('checked', false);

             $('#creditCrd').show();

                 $('#walletdis').hide();

            $('#total_amtdis').show();

            $('#Wiretfr').show();

            var shwamt = tp-walltamt;

            var diductamt = walltamt;

            shwamt = parseFloat(shwamt).toFixed(2);

            diductamt = parseFloat(diductamt).toFixed(2);



            }

            else

            {

            $('#wallet').show();

            $("#wlt").attr('checked', 'checked');

             $('#creditCrd').hide();

                 $('#walletdis').show();

            $('#total_amtdis').hide();

            $('#Wiretfr').hide();

            var diductamt = tp;

            diductamt = parseFloat(diductamt).toFixed(2);

            var shwamt = 0;

            }

            $('#oTotal').html(shwamt);

            $('#wltddcut').html('<?php echo $Cur;?> '+diductamt);

            $('#wallet_amount_deducted').val(diductamt);

            $('#totalprice').val(shwamt);

            $('#vat').html('<?php echo $Cur;?> '+vatamount);

       

           }

        });  

    }

 

  }

  jQuery(document).ready(function(){

jQuery.validator.addMethod("laxEmail", function(value, element) {

  return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);

}, "Please enter valid email address."); 

 jQuery("#chekoutinfo").validate({

    rules: {          

          "uname" : {

            required : true,

            maxlength: 100

          },

          "email" : {

            required : true,

            email: true,

            laxEmail:true

          },

		   "country_code" : {

            required : true

          },

          "phone" : {

            required : true,

            digits: true, 

            minlength: 5,

            maxlength: 13

          },  

          "gender" : {

            required : true

          },                  

          "city" : {

            required : true

          },           

          "pincode" : {

            required : true,

            digits: true,

            maxlength: 6

          },

          "address1" : {

            required : true

          },

		  "shipping" : {

            required : true

          }, 

		  "payment" : {

            required : true

          },                  

         },

         messages: {

          "uname": {

            required:  "<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NAME')); ?> <?php endif; ?>",

            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_NAME_HERE_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_NAME_HERE_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NAME_HERE_MAX')); ?> <?php endif; ?>'

          },

          "email": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php endif; ?>',

            email: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE_VALID')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE_VALID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_HERE_VALID')); ?> <?php endif; ?>'

          },

		   "country_code": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.COUNTRY_CODE_MSG')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.COUNTRY_CODE_MSG')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.COUNTRY_CODE_MSG')); ?> <?php endif; ?>'

          },

          "phone": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_TEEPHONE_NUMBER_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_TEEPHONE_NUMBER_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_TEEPHONE_NUMBER_HERE')); ?> <?php endif; ?>',

            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>', 

            minlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MIN')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MIN')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NUMBER_HERE_MIN')); ?> <?php endif; ?>',

            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NUMBER_HERE_MAX')); ?> <?php endif; ?>'

          },

          "gender": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_GENDER_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_GENDER_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_GENDER_HERE')); ?> <?php endif; ?>'

          },          

          "city": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_CITY_HERE')); ?> <?php endif; ?>'

          },

          "pincode": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_PIN_CODE_HERE')); ?> <?php endif; ?>',

            digits: '<?php if(Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS')); ?> <?php endif; ?>',

            maxlength: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE_MAX')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE_MAX')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_PIN_CODE_HERE_MAX')); ?> <?php endif; ?>'

          },

          "address1": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ADDRESS')); ?> <?php endif; ?>'

          }, 

		  "shipping": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_SHIPPING_METHOD')); ?> <?php endif; ?>'

          }, 

		  "payment": {

            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_PAYMENT_METHOD')); ?> <?php endif; ?>'

          },          

         }

 });

 jQuery(".btn-info-wisitech").click(function() {

   if(jQuery("#chekoutinfo").valid()) {        

    jQuery('#chekoutinfo').submit();

   }

  });

});

 /*

 jQuery("#chekoutinfo").validate({ 

                  ignore: [],

                  rules: {

                          uname: {

                          required: true,

                          },

                          email: {

                          required: true,

                          },

                          phone: {

                          required: true,

                          number:true,

                          },

                          gender: {

                          required: true,

                          },                          

                          dob: {

                          required: true,

                          },

                          city: {

                          required: true,

                          },

                          pincode: {

                          required:true, 

                          number:true,                      

                          },

                          address1: {

                          required:true,

                          }, 

                          shipping: {

                          required:true,

                          },

                          payment: {

                          required:true,

                          },





                      },

                 highlight: function(element) {

                      $(element).removeClass('error');

                },

             

             messages: {

                    uname: {

                       required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NAME')); ?> <?php endif; ?>',

                    },  

                    email: {

                      required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_EMAIL')); ?> <?php endif; ?>',

                    },  

                    phone: {

                              required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PHONE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PHONE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PHONE')); ?> <?php endif; ?>',

                    },                    

                    gender: {

                              required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_GENDER')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_GENDER')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_YOUR_GENDER')); ?> <?php endif; ?>',

                    },

                    dob: {

                            required: '<?php if(Lang::has(Session::get('lang_file').'.ENTER_YOUR_DOB_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.ENTER_YOUR_DOB_HERE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_DOB_HERE')); ?> <?php endif; ?>',

                    },

                    city: {

                    required: '<?php if(Lang::has(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.SELECT_YOUR_CITY_HERE')); ?> <?php endif; ?>',

                    },  



                    address1: {

                    required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ADDRESS')); ?> <?php endif; ?>',

                    }, 



                    pincode: {

                    required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PINCODE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PINCODE')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PINCODE')); ?> <?php endif; ?>',

                    },  

                      shipping: {

                    required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_SHIPPING_METHOD')); ?> <?php endif; ?>',

                    },

                    payment: {

                    required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_PAYMENT_METHOD')); ?> <?php endif; ?>',

                    },

                                         

                     

                },



                invalidHandler: function(e, validation){

                    },



                submitHandler: function(form) {

                    form.submit();

                }

            });*/

			

$(document).ready(function(){

          var op = $('#orgprice').val();           

          tp = parseFloat(op).toFixed(2);         

          var walltamt = <?php echo $User->wallet;?>;

          var couponAmt = 0;

          if(tp > walltamt)

          {

          $('#wallet').hide(); 

          $("#wlt").attr('checked', false);

          $('#creditCrd').show();

              $('#walletdis').hide();

            $('#total_amtdis').show();

          $('#Wiretfr').show();

          var shwamt = tp-walltamt-couponAmt;

          shwamt = parseFloat(shwamt).toFixed(2);

            var diductamt = walltamt;

            diductamt = parseFloat(diductamt).toFixed(2);

          }

          else

          {

          $('#wallet').show();

         

          $("#wlt").attr('checked', 'checked');

          $('#creditCrd').hide();

              $('#walletdis').show();

            $('#total_amtdis').hide();

          $('#Wiretfr').hide();

          var diductamt = tp;

          diductamt = parseFloat(diductamt).toFixed(2);

          var shwamt = 0;

          }



          $('#oTotal').html(shwamt);

          $('#wltddcut').html('<?php echo $Cur;?> '+diductamt);

          $('#wallet_amount_deducted').val(diductamt);

          $('#totalprice').val(shwamt);

})



  

$('.shiping').click(function(){



var method = $(this).data('value');

 var shipids = $(this).data('ship');

 var ashipids = $(this).data('aship');

 





var address1 = $('#address1').val();

var address2 = $('#address2').val();

var city = $('#city').val();

var country = $('#country').val();

var address = address1+ ' ' +address2;



//var Oldprice = $(this).data('price');



var Oldprice=jQuery(this).attr("data-price");

 



var productid = $(this).data('productid');

 var ship =  $('#Gtotalshipping').val();

 var shippingprice = $(this).data('price');



 //  if(method ==2 && shippingprice != undefined)

 // {

  

 

 //  var pickprice = parseFloat(ship)-parseFloat(shippingprice);

 



 //  $('#totalshipping').html('SAR '+pickprice);

  



 //      var op = $('#orgprice').val();           

 //      tp = parseFloat(op).toFixed(2); 

 //      var t = $('#Gtotalshipping').val();

 //      $('#Gtotalshipping').val(pickprice);

 //      var Gvat = $('#Gvat').val();

 //     var rem =  parseFloat(t)-parseFloat(shippingprice);

 //      var tt = parseFloat(tp) + parseFloat(rem) + parseFloat(Gvat);



 //      $('#gtp').html(tt);









 // $(this).attr('disabled', true);

  

 

 // }

 

  

 

//url:"<?php echo e(url('getaramexrates.php')); ?>?countryCode="+country+'&address='+address+'&city='+city+'&productid='+productid,

 var merchant_id = $(this).data('merchant_id');

 var cart_sub_type = $(this).data('cart_sub_type');



        $.ajax({

           type:"GET",

           url:"<?php echo e(url('getshippingrate')); ?>?shipping_type="+method+'&merchant_id='+merchant_id+'&cart_sub_type='+cart_sub_type,

          success:function(res){  

       var result;   

    

        result = parseFloat(res ) - parseFloat(Oldprice);

          $('#'+ashipids).attr('data-price', res);

          $('#'+shipids).attr('data-price', res);

             if(method ==1)

            {

              $('#'+ashipids).attr('disabled', true);

              $('#'+shipids).attr('disabled', false);

            $('#Aramex_priceCalc_'+productid).html('SAR '+res);

            $('#Picks_priceCalc_'+productid).html('SAR 0.00');

            }

            else

            {

            $('#'+ashipids).attr('disabled', false);

            $('#'+shipids).attr('disabled', true);

            $('#Aramex_priceCalc_'+productid).html('SAR 0.00');

            $('#Picks_priceCalc_'+productid).html('SAR '+res);

            }

 

  





          var newPrice = parseFloat(ship)+parseFloat(result);





          $('#totalshipping').html(newPrice);

          $('#Gtotalshipping').val(newPrice);  



       



            

          var op = $('#orgprice').val();           

          tp = parseFloat(op).toFixed(2); 

          var t = $('#Gtotalshipping').val();

          var Gvat = $('#Gvat').val();

          var tt = parseFloat(tp) + parseFloat(t) + parseFloat(Gvat);          

            $('#totalprice').val(tt);

           $('#gtp').html(tt);

          $('#oTotal').html(tt);

           }

        });



  

  

}) 



  </script>





 

 <!--

  <script language="JavaScript">



 

    document.onkeypress = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

           //alert('No F-12');

            return false;

        }

    }

    document.onmousedown = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

            //alert('No F-keys');

            return false;

        }

    }

document.onkeydown = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

            //alert('No F-keys');

            return false;

        }

    }

 



 

var message="Sorry, right-click has been disabled";

function clickIE() {if (document.all) {(message);return false;}}

function clickNS(e) {if

(document.layers||(document.getElementById&&!document.all)) {

if (e.which==2||e.which==3) {(message);return false;}}}

if (document.layers)

{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}

else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}

document.oncontextmenu=new Function("return false")



function disableCtrlKeyCombination(e)

{

var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j' , 'w');

var key;

var isCtrl;

if(window.event)

{

key = window.event.keyCode;     //IE

if(window.event.ctrlKey)

isCtrl = true;

else

isCtrl = false;

}

else

{

key = e.which;     //firefox

if(e.ctrlKey)

isCtrl = true;

else

isCtrl = false;

}

if(isCtrl)

{

for(i=0; i<forbiddenKeys.length; i++)

{

//case-insensitive comparation

if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())

{

alert('Key combination CTRL + '+String.fromCharCode(key) +' has been disabled.');

return false;

}

}

}

return true;

}

</script>

 -->





<!-- Coupon Code start -->



<div class="action_popup coupon-popup-area">

<div class="action_active_popup"> 

<div class="coupon-heading"><?php echo e((Lang::has(Session::get('lang_file').'.APPLY_COUPONS')!= '')  ?  trans(Session::get('lang_file').'.APPLY_COUPONS'): trans($OUR_LANGUAGE.'.APPLY_COUPONS')); ?> <span class="applied closecpn" title="Close">X</span></div>

<div class="coupon_apply">   

<input type="text" name="coupon_code" id="coupon_code" maxlength="45" class="t-box"> 

<input type="button" name="apply" class="insapplied" value="<?php echo e((Lang::has(Session::get('lang_file').'.APPLY')!= '')  ?  trans(Session::get('lang_file').'.APPLY'): trans($OUR_LANGUAGE.'.APPLY')); ?>">

<span class="errors" id="couponerror"></span>

</div>

<div class="afterredeem" <?php if(count($getCartcoupon) < 1): ?> style="display: none;" <?php endif; ?>>

<div class="appliedcoupons"><?php echo e((Lang::has(Session::get('lang_file').'.APPLIED_COUPONS')!= '')  ?  trans(Session::get('lang_file').'.APPLIED_COUPONS'): trans($OUR_LANGUAGE.'.APPLIED_COUPONS')); ?> </div>





<div class="couponlisting mCustomScrollbar" id="content-1" style="max-height:220px;">

<ul class="couponlistinga">



 

   <?php if(count($getCartcoupon) >= 1): ?>

 <?php $__currentLoopData = $getCartcoupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $couponcodes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

   <?php $rand = rand(1111,9999); ?>

<li class="<?php echo e($rand); ?>"><div class="coupon-beauty"><?php echo e($couponcodes->cart_type); ?></div><div class="coupon-beauty"><span><?php echo e($couponcodes->coupon_code); ?></span></div><div class="coupon-beauty coupon-sar">SAR <?php echo e(number_format($couponcodes->coupon_code_amount, 2)); ?><span class="coupon-remove" data-rmd="<?php echo e($rand); ?>" data-ids="<?php echo e($couponcodes->coupon_code); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Remove')!= '')  ?  trans(Session::get('lang_file').'.Remove'): trans($OUR_LANGUAGE.'.Remove')); ?></span></div></li>

 

 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php endif; ?>

</ul>

</div>

<div class="coupon-popup-apply">

<input type="button" name="apply" class="applied form-btn" value="<?php echo e((Lang::has(Session::get('lang_file').'.DONE')!= '')  ?  trans(Session::get('lang_file').'.DONE'): trans($OUR_LANGUAGE.'.DONE')); ?>">

</div>



</div>



</div>

</div>



<!-- Coupon Code end -->





<script type="text/javascript">  

$('body').on('click','.coupon-remove',function(){

    var getCode = $(this).data('ids');

    var rmd = $(this).data('rmd');

    var del = 1;

     $.ajax({

    type:"GET",

    url:"<?php echo e(url('checkcouponcode')); ?>?couopncode="+getCode+"&del="+del,

    success:function(res){

    $('.'+rmd).fadeOut(500);

    }

    });

    });



$('.insapplied').click(function(){

var getCode = $('#coupon_code').val();

    if($.trim(getCode)=='')

    {

    var error = "<?php echo e((Lang::has(Session::get('lang_file').'.Pleaseenter_couponcode')!= '')  ?  trans(Session::get('lang_file').'.Pleaseenter_couponcode'): trans($OUR_LANGUAGE.'.Pleaseenter_couponcode')); ?>";

    $('#couponerror').html(error);

    }

    else

    {

    $.ajax({

               type:"GET",

               url:"<?php echo e(url('checkcouponcode')); ?>?couopncode="+getCode,

               success:function(res){ 

                if(res!='Invaild coupon code'){

                  

              if(res=='Already applied')

              { 

                  var error = "<?php echo e((Lang::has(Session::get('lang_file').'.CouponAlready_applied')!= '')  ?  trans(Session::get('lang_file').'.CouponAlready_applied'): trans($OUR_LANGUAGE.'.CouponAlready_applied')); ?>";

                 $('#couponerror').html(error); 

              }

              else

              {

              $('.afterredeem').show();

              $('#coupon_code').val('');

              $('#couponerror').html('');

              var Remove = "<?php echo e((Lang::has(Session::get('lang_file').'.Remove')!= '')  ?  trans(Session::get('lang_file').'.Remove'): trans($OUR_LANGUAGE.'.Remove')); ?>";

              var ran = Math.floor((Math.random() * 100000000) + 1);

              var code = res.split('~~');

              $('.couponlistinga').append('<li class="'+ran+'"><div class="coupon-beauty">'+code[0]+'</div><div class="coupon-beauty"><span>'+code[1]+'</span></div><div class="coupon-beauty coupon-sar">SAR '+code[2]+'<span class="coupon-remove" data-rmd="'+ran+'" data-ids="'+code[1]+'">'+Remove+'</span></div></li>');

              $("#content-1").mCustomScrollbar({

              scrollButtons:{

              enable:true

              }  

              });



              }

      }

      else

      {

    var error = "<?php echo e((Lang::has(Session::get('lang_file').'.Pleaseenter_validcouponcode')!= '')  ?  trans(Session::get('lang_file').'.Pleaseenter_validcouponcode'): trans($OUR_LANGUAGE.'.Pleaseenter_validcouponcode')); ?>";

    $('#couponerror').html(error);



      }

           }

        });

 

}

});

  



  $('.applied').click(function(){

  $('.action_popup').fadeOut(500);

  $('.overlay').fadeOut(500);

  location.reload();

  });

  $('.popcoupons').click(function(){

  $('.action_popup').fadeIn(500);

  $('.overlay').fadeIn(500);

   $('#coupon_code').val('');

   $('#couponerror').html('');

  });





 

 

  

</script>

 

 <?php if($ChkShip ==0): ?>

<script type="text/javascript">

  $(window).ready(function(){

     $('#shipbox,#shipbox1').hide();

   }); 

</script>

 <?php endif; ?>







 
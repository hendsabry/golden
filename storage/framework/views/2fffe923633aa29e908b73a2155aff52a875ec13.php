<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $wallet_inner_leftmenu =1; 
 
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MyWallet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MyWallet')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MyWallet')); ?> <?php endif; ?></h5>


        <div class="add"><a href="<?php echo e(route('wallet-addamount')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_MyWallet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_MyWallet')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_MyWallet')); ?> <?php endif; ?></a></div>



      </div>
  
 

      <?php echo Form::open(array('url'=>"my-wallet",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?> 

        <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
        <div class="filter_area">
        <div class="order-filter-line">
        <div class="filter_left studio-amount">
          <input name="amount" maxlength="6"  placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?>" onkeypress="return isNumber(event)"  type="text" value="<?php echo e(request()->amount); ?>" /> 
      
        </div>
        <div class="of-date-box studio-date-box">
           <input name="dateFrom"  class="cal-t"  placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.DateFrom')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DateFrom')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DateFrom')); ?> <?php endif; ?>" id="dateFrom"  type="text"  maxlength="12"   value="<?php echo e(request()->dateFrom); ?>" /> 
       
         <input name="dateTo" id="dateTo" type="text" maxlength="12" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.DateTo')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DateTo')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DateTo')); ?> <?php endif; ?>"  class="cal-t studio-date" value="<?php echo e(request()->dateTo); ?>" />
         <span for="dateTo" generated="true" class="error" id="todata"> </span>

         <input name="" value="<?php if(Lang::has(Session::get('mer_lang_file').'.Apply')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Apply')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Apply')); ?> <?php endif; ?>" class="applu_bts" type="submit">
        </div>
        </div>
        </div>
      <!-- filter_area -->   
      <?php echo Form::close(); ?>




      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap">
              <div class="panel-body panel panel-default">
                <?php if($getTotalAmount->count() < 1): ?>
                <div class="no-record-area"> <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?> </div>
                <?php else: ?>
                <div class="table dish-table">
                  <div class="tr">            

                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Amount_Paid')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount_Paid')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount_Paid')); ?> <?php endif; ?></div>
                   <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Payment_date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Payment_date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Payment_date')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Mode')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Mode')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Mode')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Total_Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Total_Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Total_Amount')); ?> <?php endif; ?></div>
                    <div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.Note')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Note')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Note')); ?> <?php endif; ?></div>

                  </div>
                <?php $TotalAmount =0; ?>
                 <?php $__currentLoopData = $getTotalAmount; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <?php $Amt = $value->amount; ?>
                    <div class="tr">
                    <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Amount_Paid')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount_Paid')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount_Paid')); ?> <?php endif; ?>"><?php echo e(isset($value->amount) ? $value->amount : ''); ?></div>
                    <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Payment_date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Payment_date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Payment_date')); ?> <?php endif; ?>"> <?php echo e(Carbon\Carbon::parse($value->created_at)->format('F j, Y')); ?> </div>
                    
                    <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Mode')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Mode')); ?>  
                    <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Mode')); ?> <?php endif; ?>"> <span class="   status_active2 cstatus" data-status=" "data-id=" "><?php $Type = $value->transaction_type; $status = $value->status; ?> <?php if($Type==1): ?> <?php if(Lang::has(Session::get('mer_lang_file').'.Credit')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Credit')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Credit')); ?> <?php endif; ?>   <?php if($status==1): ?>
                    <span class="error"><?php if(Lang::has(Session::get('mer_lang_file').'.Failed')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Failed')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Failed')); ?> <?php endif; ?></span> <?php else: ?> <span class="error"><?php if(Lang::has(Session::get('mer_lang_file').'.Sucess')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Sucess')); ?>    <?php $TotalAmount =$TotalAmount + $Amt; ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Sucess')); ?> <?php endif; ?>  </span>  <?php endif; ?> <?php else: ?> <?php if(Lang::has(Session::get('mer_lang_file').'.Debit')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Debit')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Debit')); ?> <?php endif; ?> <?php $TotalAmount =$TotalAmount - $Amt; ?>  <?php endif; ?></span></div>

                    <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Total_Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Total_Amount')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Total_Amount')); ?> <?php endif; ?>">
                    <?php echo e(isset($TotalAmount) ? $TotalAmount : '0'); ?></div>
                    <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Note')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Note')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Note')); ?> <?php endif; ?>">
                    <?php echo e(isset($value->notes) ? $value->notes : 'N/A'); ?></div>
                    </div>
 
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
 
                </div>

               <?php endif; ?>
 
              </div>
               <?php echo e($getTotalAmount->links()); ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 <script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         <?php if($mer_selected_lang_code !='en'): ?>
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        <?php else: ?>
        $('#todata').html("End date should be greater than From date");
        <?php endif; ?>
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>     
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
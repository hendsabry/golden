<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $listmanagerblade =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_MANAGER')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_MANAGER')); ?> <?php endif; ?> </h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!-- Display Message after submition --> 
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?> 
              <!-- Display Message after submition -->
              
              <form name="form1" id="addmanager" method="post" action="<?php echo e(route('storemanager')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_FIRST_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_FIRST_NAME'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="mer_fname" maxlength="40" value="<?php echo Input::old('mer_fname'); ?>" id="mer_fname" required="" >
                      </div>
                      <div class="arabic ar" >
                        <input type="text" class="arabic ar" name="mer_fname_ar" maxlength="40" value="<?php echo Input::old('mer_fname_ar'); ?>" id="mer_fname_ar" required="" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_LAST_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_LAST_NAME'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="mer_lname" maxlength="40" value="<?php echo Input::old('mer_lname'); ?>" id="mer_lname" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="arabic ar" name="mer_lname_ar" maxlength="40" value="<?php echo Input::old('mer_lname_ar'); ?>" id="mer_lname_ar" required="" >
                      </div>
                    </div>
                  </div>
                </div>
                <!-- form_row --> 
                
                <!-- form_row -->
                <div class="form_row">
                	<div class="form_row_left common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_Email_Address'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Email_Address'); ?> </span> </label>
                  <div class="info100">
                    <input maxlength="60"  type="email" name="mer_email" value="" id="email_id" onchange="check_email();" required="" >
                    <div id="email_id_error_msg"  style="color:#F00;font-weight:800"> </div>
                  </div>
                  </div>
                  
                  <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CUSTOMERMOBILE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CUSTOMERMOBILE'); ?> </span> </label>
                  <div class="info100">
                    <input class="small-sel" maxlength="12"  type="text" name="mobile" value="<?php echo Input::old('mobile'); ?>" id="mobile"  required="" >
                  </div>
                  </div>
                  
                </div>
                <!-- form_row -->
                
                
                <div class="form_row">
                  <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CITY'); ?> </span> </label>
                  <div class="info100">
                    



<select class="small-sel" name="city"  id="city">
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_CITY')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY')); ?> <?php endif; ?></option>

                        <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> <?php if($mer_selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?> <?php echo e($cbval->co_name); ?> <?php endif; ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $ci_name='ci_name'?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_'.$mer_selected_lang_code; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>"><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                      </select>








                  </div>
                  </div>
                  
                  <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_PASSWORD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PASSWORD'); ?> </span> </label>
                  <div class="info100">
                    <input type="password" maxlength="20"  class="small-sel" name="mer_password" value="" id="mer_password" required="" >
                  </div>
                  </div>
                  
                </div>
                <!-- form_row -->
                
                
                
                <div class="form_row">
                <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_User_Image'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_User_Image'); ?> </span> </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                    </div>
                  </div>
                  </div>
                  
                  <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.UPLOADCERTIFICATE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.UPLOADCERTIFICATE'); ?> </span> </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div id="file_value2" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <input id="company_logo1" name="storcertifiction_img" class="info-file" type="file" value="">
                    </div>
                    <div class="error certifications"> </div>
                  </div>
                  </div>
                  
                </div>
                <!-- form_row -->
                
                
                <!-- form_row -->
                
                <div class="form_row english">
                  <input type="submit" name="submit" value="Submit">
                </div>
                <!-- form_row -->
                <div class="form_row arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                <!-- form_row -->
              </form>
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addmanager").validate({
              ignore: [],
               rules: {
                  mer_fname: {
                       required: true,
                      },

                       mer_lname: {
                       required: true,
                      },
                      mer_fname_ar: {
                       required: true,
                      },

                       mer_lname_ar: {
                       required: true,
                      },

                       mer_email: {
                       required: true,
                      },

                      mobile: {
                       required: true,
                      },
                       city: {
                       required: true,
                      },
                    
                       mer_password: {
                       required: true,
                      },

                      stor_img: {
                        required: true,
                           accept:"png|jpe?g|gif",
                      },
                 
                

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                
           messages: {
             mer_fname: {
               required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_FIRST_NAME'); ?>",
                      },  

                 mer_lname: {
               required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_LAST_NAME'); ?>",
                      },  


          mer_fname_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_FIRST_NAME_AR'); ?>",
                      },  

                 mer_lname_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_LAST_NAME_AR'); ?>",
                      }, 


                mer_email: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EMAIL')); ?> <?php endif; ?>",
                      },   

               mobile: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MOBILE')); ?> <?php endif; ?>",
                      },   


                       city: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY_TEXT')); ?> <?php endif; ?> ",
                      },

                   mer_password: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PASSWORD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PASSWORD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PASSWORD')); ?> <?php endif; ?> ",
                      },
     

                         stor_img: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                        required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?> ",
                      },   
                  


                                        
                     
                },             
          invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                     console.log("invalidHandler : validation", valdata);

 
 <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.mer_fname != "undefined" || valdata.mer_fname != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.mer_lname != "undefined" || valdata.mer_lname != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.mer_email != "undefined" || valdata.mer_email != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.mobile != "undefined" || valdata.mobile != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.city != "undefined" || valdata.city != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.mer_password != "undefined" || valdata.mer_password != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                    if (typeof valdata.storcertifiction_img != "undefined" || valdata.storcertifiction_img != null)
                    {
                    $('.english_tab').trigger('click');
                    }

                    if (typeof valdata.mer_fname_ar != "undefined" || valdata.mer_fname_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');  
                    }
                    if (typeof valdata.mer_lname_ar != "undefined" || valdata.mer_lname_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     


                    }
                      
<?php else: ?>
                    if (typeof valdata.mer_fname_ar != "undefined" || valdata.mer_fname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                   if (typeof valdata.mer_lname_ar != "undefined" || valdata.mer_lname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                  if (typeof valdata.mer_fname != "undefined" || valdata.mer_fname != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.mer_lname != "undefined" || valdata.mer_lname != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.mer_email != "undefined" || valdata.mer_email != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.mobile != "undefined" || valdata.mobile != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.city != "undefined" || valdata.city != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                      if (typeof valdata.mer_password != "undefined" || valdata.mer_password != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                      if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }

                       if (typeof valdata.storcertifiction_img != "undefined" || valdata.storcertifiction_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    
<?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
  $("#company_logo1").change(function () {
        var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   $("#file_value2").html('');         
$(".certifications").html('');

         }
    });


</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
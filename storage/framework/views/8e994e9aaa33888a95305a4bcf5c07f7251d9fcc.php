<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $acoustics_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.Acoustics_Info_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Acoustics_Info_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Acoustics_Info_Menu')); ?> <?php endif; ?></h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?> 
      <!-- Display Message after submition -->
      
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> <?php echo Form::open(array('url' => '/add_acousticsinfo','method' => 'POST', 'id'=> 'add_singerinfo','enctype' =>'multipart/form-data', 'name' =>'add_singerinfo' )); ?>

            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_NAME'); ?> </span> </label>
                <div class="info100">
                  <div class="english">
                    <input name="singer_name" maxlength="70" value="<?php echo e(isset($getSinger->name) ? $getSinger->name : ''); ?>" id="singer_name" required="" type="text">
                  </div>
                  <div  class="arabic ar" >
                    <input class="arabic ar" name="singer_name_ar" maxlength="70" value="<?php echo e(isset($getSinger->name_ar) ? $getSinger->name_ar : ''); ?>" id="singer_name_ar" required="" type="text">
                  </div>
                </div>
              </div>
              
              <div class="form_row_right common_field">
                <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_URL'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_URL'); ?> </span> 
                <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span></a>
                </label>
                
                <div class="info100">
                  <div>
                   <input type="url" name="google_map_url" value="<?php echo e(isset($getSinger->google_map_url) ? $getSinger->google_map_url : ''); ?>">
                  </div>
                </div>
              </div>
              
              
            </div>




              <span id="maperror"></span>
              <div class="form_row_left common_field">
              <div class="lat_left">
              <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>
              <input type="text" class="form-control" value="<?php echo e(isset($getSinger->longitude) ? $getSinger->longitude : ''); ?>" readonly="" name="longitude" />
              </div>
              <div class="lat_right">
              <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>
              <input type="text" class="form-control" value="<?php echo e(isset($getSinger->latitude) ? $getSinger->latitude : ''); ?>"  readonly="" name="latitude" />
              </div>
              </div>




            
            <div class="form_row">
              
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESS'); ?> </span> </label>
                <div class="info100">
                  <div class="english">
                    <input name="address" maxlength="250" value="<?php echo e(isset($getSinger->address) ? $getSinger->address : ''); ?>" id="address"  type="text">
                  </div>
                  <div  class="arabic ar" >
                    <input class="arabic ar" name="address_ar" maxlength="250" value="<?php echo e(isset($getSinger->address_ar) ? $getSinger->address_ar : ''); ?>" id="address_ar"  type="text">
                  </div>
                </div>
              </div>

              <div class="form_row_right  common_field">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CITY'); ?> </span> </label>
                <div class="info100">
                  <select class="small-sel" id="singer_city" name="singer_city">
                    


<option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_CITY')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY')); ?> <?php endif; ?></option>

                        <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"><?php echo e($cbval->co_name); ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $ci_name='ci_name'?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_'.$mer_selected_lang_code; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php if(isset($getSinger->city_id) && ($getSinger->city_id == $val->ci_id)): ?> SELECTED <?php endif; ?>><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>





                  </select>
                </div>
              </div>
            </div>
            
            <div class="form_row common_field">
              
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_IMAGE'); ?> </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div id="file_value2" class="file-value"></div>
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                    </div>
                    <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                    </label>
                    <input type="hidden" name="singer_image_url" value="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>">
                    <?php if(!isset($getSinger->image) ): ?>
                    <input required="" id="company_logo1" name="singer_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" id="singer_image" class="info-file" value="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>" type="file">
                    <?php else: ?>
                    <input  id="company_logo1" name="singer_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" id="singer_image" class="info-file" value="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>" type="file">
                    <?php endif; ?> </div>
                  <div class="error certifications"> </div>
                  <?php if(isset($getSinger->image) && $getSinger->image!='' ): ?> <img src="<?php echo e(isset($getSinger->image) ? $getSinger->image : ''); ?>"> <?php endif; ?> </div>
              </div>
              
              <!--div class="form_row_right">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESS_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESS_IMAGE'); ?> </span><a href="javascript:void(0);" class="address_image_tooltip"> <span class="add_img_tooltip"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_office_address_img')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_office_address_img')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_office_address_img')); ?> <?php endif; ?></span> </a> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo9">
                    <div class="file-btn-area">
                      <div id="file_value8" class="file-value"></div>
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                    </div>
                    </label>
                    <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                    <input type="hidden" name="address_image_name" value="<?php echo e(isset($getSinger->address_image) ? $getSinger->address_image : ''); ?>">
                    <?php if(!isset($getSinger->address_image) ): ?>
                    <input  id="company_logo9" name="address_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" value="<?php echo e(isset($getSinger->address_image) ? $getSinger->address_image : ''); ?>" type="file">
                    <?php else: ?>
                    <input  id="company_logo9" name="address_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" value="<?php echo e(isset($getSinger->address_image) ? $getSinger->address_image : ''); ?>" type="file">
                    <?php endif; ?> </div>
                  <div class="error certifications"> </div>
                  <?php if(isset($getSinger->address_image) && $getSinger->address_image!='' ): ?> <img src="<?php echo e(isset($getSinger->address_image) ? $getSinger->address_image : ''); ?>"> <?php endif; ?> </div>
              </div-->
            </div>
      
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_About_Singer'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_About_Singer'); ?> </span> </label>
                <div class="info100">
                  <div class="english">
                    <textarea name="about_singer" id="about_singer" rows="4" cols="50" class="english" maxlength="500"><?php echo e(isset($getSinger->about) ? $getSinger->about : ''); ?></textarea>
                  </div>
                  <div  class="arabic ar" >
                    <textarea class="arabic ar" name="about_singer_ar" id="about_singer_ar" rows="4" cols="50"  maxlength="500"><?php echo e(isset($getSinger->about_ar) ? $getSinger->about_ar : ''); ?></textarea>
                  </div>
                </div>
              </div>
              <div class="form_row_right english">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                    </div>
                    </label>
                    <input type="hidden" name="tnc_url" value="<?php echo e(isset($getSinger->terms_conditions) ? $getSinger->terms_conditions : ''); ?>">
                    <?php if(!isset($getSinger->terms_conditions) ): ?>
                    <input required="" id="company_logo" name="tnc" class="info-file" value="" type="file">
                    <?php else: ?>
                    <input id="company_logo" name="tnc" class="info-file" value="" type="file">
                    <?php endif; ?> </div>

<div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
<?php if(isset($getSinger->terms_conditions) && $getSinger->terms_conditions!='' ): ?>

              <a href="<?php echo e($getSinger->terms_conditions); ?>" target="_blank" class="pdf_icon"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($getSinger->terms_condition_name) ? $getSinger->terms_condition_name : ''); ?></span></a>
                  <input type="hidden" name="tnc_name" value="<?php echo e(isset($getSinger->terms_condition_name) ? $getSinger->terms_condition_name : ''); ?>">
                  <?php endif; ?>
                  <div class="error certifications"> </div>
                </div>
              </div>
            </div>
            
            <div class="arabic ar">
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); ?> </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo11">
                    <div class="file-btn-area">
                      <div id="file_value10" class="file-value"></div>
                      <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                    </div>
                    </label>
                    <input type="hidden" name="tnc_url_ar" value="<?php echo e(isset($getSinger->terms_conditions_ar) ? $getSinger->terms_conditions_ar : ''); ?>">
                    <?php if(!isset($getSinger->terms_conditions_ar) ): ?>
                    <input required="" id="company_logo11" name="tnc_ar" class="info-file" value="" type="file">
                    <?php else: ?>
                    <input id="company_logo11" name="tnc_ar" class="info-file" value="" type="file">
                    <?php endif; ?> </div>
<div class="pdf_msg"><span class="english"><?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?> </span></div>
<?php if(isset($getSinger->terms_conditions) && $getSinger->terms_conditions!='' ): ?>

              <a href="<?php echo e($getSinger->terms_conditions_ar); ?>" target="_blank" class="pdf_icon"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($getSinger->terms_condition_name_ar) ? $getSinger->terms_condition_name_ar : ''); ?></span></a>
                  <input type="hidden" name="tnc_name_ar" value="<?php echo e(isset($getSinger->terms_condition_name_ar) ? $getSinger->terms_condition_name_ar : ''); ?>">
                  <?php endif; ?>
                  <div class="error certifications"> </div>
                </div>
              </div>
            </div>
            </div>
            <div class="form_row_left common_field">
                    <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?> </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?> </option>

                      <option value="1" <?php  if(isset($getSinger->status) && $getSinger->status==1){?> SELECTED <?php } ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?> </option>
                     <option value="0" <?php if(isset($getSinger->status) && $getSinger->status==0){?> SELECTED <?php } ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?>  </option>
                   </select>
                    
                    </div>
                  </div>
            <div class="form_row">
              <div class="form_row_left">
              <div class="arabic ar">
                <input type="hidden" name="hid" value="<?php echo e(isset($getSinger->id) ? $getSinger->id : ''); ?>">
                <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                 <input type="hidden" name="Cid" value="<?php echo e(request()->Cid); ?>">
                <input type="submit" name="submit" value="خضع">
              </div>
              <div class="english">
                <input type="submit" name="submit" value="Submit">
              </div>
              </div>
              
              <!-- form_row --> 
            </div>
            <?php echo Form::close(); ?> 
            <!-- one-call-form --> 
            <!-- Display Message after submition --> 
            
            <!-- Display Message after submition --> 
            <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script type="text/javascript">     
$("#add_singerinfo").validate({
                  ignore: [],
                  rules: {
                          singer_name: {
                          required: true,
                          },
                          singer_name_ar: {
                          required: true,
                          },
                           address_image: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                          address: {
                          required: true,
                          },
                          singer_image: {
                          accept:"png|jpe?g|gif",
                          },
                           address_image: {
                          accept:"png|jpe?g|gif",
                          },                 
                          singer_city: {
                          required: true,
                          },
                          about_singer: {
                          required: true,
                          },
                          about_singer_ar: {
                          required: true,
                          },



                     <?php if(!isset($getSinger->terms_conditions)): ?>                         
                          tnc: {
                            required: true,
                           accept:"pdf",
                          }, 
                          tnc_ar: {
                            required: true,
                           accept:"pdf",
                          },                         
                      <?php else: ?>
                      tnc: {
                            required: false,
                           accept:"pdf",
                          },
                           tnc_ar: {
                            required: false,
                           accept:"pdf",
                          }, 

                      <?php endif; ?>     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                 
           messages: {
                  singer_name: {
                  required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_SINGER_NAME'); ?>",
                  },  
                  singer_name_ar: {
                  required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_SINGER_NAME_ARABIC'); ?>",
                  },

                    address: {
               required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
  
                       address_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },

                         
                  singer_image: {

                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                  
                  }, 
                  singer_city: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?>",
                  },  
                  about_singer: {
                  required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); ?>",
                  },   
                  about_singer_ar: {
                 required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER_ARABIC'); ?> ",
                  },
                   address_image: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      }, 
                  tnc: {
                                required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                                accept: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); ?>",
                        },

                       tnc_ar: {
                                required: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); ?>",
                                accept: "<?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); ?>",
                      },  
 
                },
                invalidHandler: function(e, validation){
                      var valdata=validation.invalid;
                   <?php if($mer_selected_lang_code !='en'): ?>

                      if (typeof valdata.singer_name != "undefined" || valdata.singer_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                        if (typeof valdata.singer_image != "undefined" || valdata.singer_image != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }
                        if (typeof valdata.singer_city != "undefined" || valdata.singer_city != null) 
                      {
                      $('.english_tab').trigger('click');    

                      }

                        if (typeof valdata.about_singer != "undefined" || valdata.about_singer != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }
                     
                        if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }

                         if (typeof valdata.singer_name_ar != "undefined" || valdata.singer_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }

                      if (typeof valdata.about_singer_ar != "undefined" || valdata.about_singer_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }
                      if (typeof valdata.tnc_ar != "undefined" || valdata.tnc_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }


                      <?php else: ?>

                            if (typeof valdata.singer_name_ar != "undefined" || valdata.singer_name_ar != null) 
                            {
                            $('.arabic_tab').trigger('click'); 
                            }
                            
                            

                             

                              if (typeof valdata.about_singer_ar != "undefined" || valdata.about_singer_ar != null) 
                            {
                            $('.arabic_tab').trigger('click');     

                            }                         
                          if (typeof valdata.tnc_ar != "undefined" || valdata.tnc_ar != null) 
                            {
                            $('.arabic_tab').trigger('click');     

                            }
                              if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }
                            if (typeof valdata.singer_city != "undefined" || valdata.singer_city != null) 
                            {
                            $('.english_tab').trigger('click');    

                            }

                              if (typeof valdata.singer_image != "undefined" || valdata.singer_image != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }


                              if (typeof valdata.about_singer != "undefined" || valdata.about_singer != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }
                              if (typeof valdata.singer_name != "undefined" || valdata.singer_name != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }



                      <?php endif; ?>

                      },

                submitHandler: function(form) {
                     var mapAdd = jQuery('input[name=google_map_address]').val();
                              if(mapAdd !='')
                              {
                              var long = jQuery('input[name=longitude]').val();
                              var lat  =  jQuery('input[name=latitude]').val();
                              if(long =='' && lat=='')
                              {
                              var allOk = 0;

                              }
                              else
                              {
                              var allOk = 1; 
                              }
                              }
                              else
                              {
                              var allOk = 1;
                              }

                              if(allOk == 1)
                              {
                              form.submit();
                              }
                              else
                              {
                              $('#maperror').html("<span class='error'><?php if(Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.get_Lat_Long_issue')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue')); ?> <?php endif; ?></span>");
                              }
                }
            });

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $photographybig_leftmenu =1; ?>
<div class="merchant_vendor">   <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if(request()->autoid !=''): ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATEPACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADDPACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADDPACKAGE')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        
        
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?> </div>
              <?php endif; ?>
              <!-- Display Message after submition -->
              <form name="form1" id="add-container" method="post" action="<?php echo e(route('store-photography-package')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PACKEGENAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKEGENAME'); ?> </span></label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" maxlength="100" type="text" name="title" value="<?php echo e(isset($fetchfirstdata->pro_title) ? $fetchfirstdata->pro_title : ''); ?>" id="title" required="">
                      </div>
                      <div class="arabic">
                        <input class="arabic ar" maxlength="100" id="title_ar"  name="title_ar" value="<?php echo e(isset($fetchfirstdata->pro_title_ar) ? $fetchfirstdata->pro_title_ar : ''); ?>"  type="text" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.packagefor'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.packagefor'); ?></span> </label>
                    <div class="info100">
                      <div class="englisSh"> <?php 
                        if(isset($fetchfirstdata->attribute_id) && $fetchfirstdata->attribute_id!='')
                        {
                        $attID =  $fetchfirstdata->attribute_id;
                        }
                        else
                        {
                        $attID =  0; 
                        }
                        ?>
                        <select class="small-sel" name="package_for" onchange="checkinfo(this.value);">
                          <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT')); ?> <?php endif; ?></option>
                          <option value="142" <?php if($attID==142): ?> <?php echo e("SELECTED"); ?> <?php endif; ?>>Photography</option>
                          <option value="143" <?php if($attID==143): ?> <?php echo e("SELECTED"); ?> <?php endif; ?>>Videography</option>
                        </select>
                      </div>
                    </div>
                  </div>

                   <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.numberof_cameras'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.numberof_cameras'); ?> </span> </label>
                    <div class="info100" >
                      <div>
                        <input class="xs_small notzero" maxlength="2" type="text" onkeypress="return isNumber(event)" name="no_of_camra" value="<?php echo e(isset($CameraNumber->value) ? $CameraNumber->value : ''); ?>" id="no_of_camra">
                      </div>
</div> 

                  <div class="form_row_left common_field forpicture" <?php if($attID !=142): ?> style="display: none;" <?php endif; ?>>
                    
                      <div class="video_dur"  style="margin-top:30px; float:left; width:100%;">
                        <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.numberof_picture'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.numberof_picture'); ?> </span> </label>
                        <div class="info100">
                          <div>
                            <input class="xs_small notzero" maxlength="5" type="text" onkeypress="return isNumber(event)" name="no_of_picture" value="<?php echo e(isset($PictureNumber->value) ? $PictureNumber->value : ''); ?>" id="no_of_picture">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field forvideo" <?php if($attID !=143): ?> style="display: none;" <?php endif; ?> >
                    <div class="numb_pics" >
                      <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.duration_video'); ?> <span class="msg_img_replace">(Hrs)</span></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.duration_video'); ?> <span class="msg_img_replace">(Hrs)</span></span> </label>
                      <div class="info100" >
                        <div>
                          <input class="xs_small notzero" maxlength="4" type="text"  onkeypress="return isNumber(event)" name="duration" value="<?php echo e(isset($VideoDuration->value) ? $VideoDuration->value : ''); ?>" id="duration">
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_PRICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRICE'); ?> </span> </label>
                    <div class="info100">
                      <input class="xs_small notzero" maxlength="9" type="text"  onkeypress="return isNumber(event)" name="price" value="<?php echo e(isset($fetchfirstdata->pro_price) ? $fetchfirstdata->pro_price : ''); ?>" id="price" required="">
                    </div>
                  </div>

  <div class="form_row" style="display: none;">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english"><?php echo lang::get('mer_en_lang.QTYSTOCK'); ?></span> 
                        <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.QTYSTOCK'); ?> </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="hidden" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="9999"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>

                  
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DISCOUNT'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DISCOUNT'); ?> </span> </label>
                    <div class="info100">
                      <div class="englishd">
                        <input type="text" class="xs_small notzero" name="discount"  onkeypress="return isNumber(event)" maxlength="2" value="<?php echo e(isset($fetchfirstdata->pro_discount_percentage) ? $fetchfirstdata->pro_discount_percentage : ''); ?>" id="discount"  >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_IMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="proimage" class="info-file" type="file"   value="">
                          <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                        <?php if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!=''): ?>
                        <div class="form-upload-img"><img src="<?php echo e(isset($fetchfirstdata->pro_Img) ? $fetchfirstdata->pro_Img : ''); ?>"></div>
                        <?php endif; ?> </div>
                    </div>
                  </div>


   
<!-- Add More product images start -->
        <?php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        ?>

        <?php for($i=0;$i < $GalleryCunt;$i++): ?>
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo<?php echo e($k); ?>">
        <div class="file-btn-area">
        <div id="file_value<?php echo e($J); ?>" class="file-value"></div>
        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
        </div>
        </label>
        <input id="company_logo<?php echo e($k); ?>" name="image[]" class="info-file proImgess" data-lbl="file_value<?php echo e($J); ?>" type="file" value="">
        </div>
        <?php if(isset($productGallery[$i]->image) && $productGallery[$i]->image!=''): ?>
        <div class="form-upload-img product_img_del">
        <img src="<?php echo e(isset($productGallery[$i]->image) ? $productGallery[$i]->image : ''); ?>" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="<?php echo e($productGallery[$i]->id); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value<?php echo e($J); ?>"></span>  
        <?php endif; ?>
        <input type="hidden" name="privius[]" value="<?php echo e($productGallery[$i]->id); ?>">
        </div>

        </div>
        <?php  $J=$J + 1; $k=$k +1; ?>
        <?php endfor; ?>
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>
        <span class="error pictureformat"></span>
        <?php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} ?>                
        <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
        </div>
  <!-- Add More product images end -->



                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea name="description" maxlength="500" class="english" required=""  id="description" rows="4" cols="50"><?php echo e(isset($fetchfirstdata->pro_desc) ? $fetchfirstdata->pro_desc : ''); ?></textarea>
                      </div>
                      <div  class="arabic ar" >
                        <textarea class="arabic ar" maxlength="500"  name="description_ar" required=""  id="description_ar" rows="4" cols="50"><?php echo e(isset($fetchfirstdata->pro_desc_ar) ? $fetchfirstdata->pro_desc_ar : ''); ?> </textarea>
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                      <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
                      <input type="hidden" name="autoid" value="<?php echo e(request()->autoid); ?>">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- global_area -->
      </div>
    </div>
    <!-- right_panel -->
  </div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                      title: {
                       required: true,
                      },
                       title_ar: {
                       required: true,
                      },
                       package_for: {
                       required: true,
                      },
                     
                       price: {
                       required: true,
                      },                   
                      description: {
                       required: true,
                      },
                      description_ar: {
                       required: true,
                      },
              <?php if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!=''): ?>
                      proimage: {
                       required:false,
                       accept:"png|jpe?g|gif",
                      },
                      <?php else: ?>
                      proimage: {
                       required:true,
                       accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>
                    
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {

                    title: {
                    required:  " <?php echo e(trans('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                    }, 
                   
                    price: {
                    required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE')); ?> <?php endif; ?>",
                    },
                    package_for: {
                    required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.Please_enter_package_for')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Please_enter_package_for')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Please_enter_package_for')); ?> <?php endif; ?>",
                    }, 
                    no_of_camra: {
                    required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.Please_enter_no_of_camera')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Please_enter_no_of_camera')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Please_enter_no_of_camera')); ?> <?php endif; ?>",
                    }, 
                    duration: {
                    required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.Please_enter_duration')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Please_enter_duration')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Please_enter_duration')); ?> <?php endif; ?>",
                    }, 
                    no_of_picture: {
                    required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.Please_enter_no_of_picture')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Please_enter_no_of_picture')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Please_enter_no_of_picture')); ?> <?php endif; ?>",
                    }, 
                    price: {
                    required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE')); ?> <?php endif; ?>",
                    }, 
                    description: {
                    required:  " <?php echo e(trans('mer_en_lang.MER_VALIDATION_ABOUT')); ?> ",
                    },
                     title_ar: {
                    required:  " <?php echo e(trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                    },
                    description_ar: {
                    required:  " <?php echo e(trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR')); ?> ",
                    },
                    proimage: {
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                     },  
                                       
                     
                },
                invalidHandler: function(e, validation){
                   
                    var valdata=validation.invalid;
    <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');    
                    }

                     if (typeof valdata.package_for != "undefined" || valdata.package_for != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }

<?php else: ?>
                      if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');    
                    }
                     if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.package_for != "undefined" || valdata.package_for != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }

<?php endif; ?>


                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

function checkinfo(str)
{
  if(str == 142)
  {
  $('.forpicture').show();
  $('.forvideo').hide();
  $('.forpicture').removeClass('ar');
  $('.forvideo').addClass('ar');
  $('#no_of_camra').attr("required", true);
  $('#no_of_picture').attr("required", true);
  $('#duration').removeAttr('required');


  }
  if(str == 143)
  {


  $('.forpicture').hide();
  $('.forvideo').show();
 $('.forpicture').addClass('ar');
  $('.forvideo').removeClass('ar');
  $('#no_of_camra').removeAttr('required');
  $('#no_of_picture').removeAttr('required');
  $('#duration').attr("required", true);

  }
 

}
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script>


<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         <?php if(Session::get('lang_file') =='ar_lang'): ?>
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        <?php else: ?>
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        <?php endif; ?>
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-shop-image')); ?>",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $buffet_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
       <?php if($autoid==''): ?>
        <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_DISH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_DISH')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_DISH')); ?> <?php endif; ?> </h5>
          <?php else: ?>
          <h5 class="global_head"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_DISH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPDATE_DISH')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPDATE_DISH')); ?> <?php endif; ?> </h5>
          <?php endif; ?>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> 
            <!-- Display Message after submition --> 
            <?php if(Session::has('message')): ?>
            <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
            <?php endif; ?> 
            <!-- Display Message after submition -->

            <form name="form1" id="add-container" method="post" action="<?php echo e(route('store-buffet-dish')); ?>" enctype="multipart/form-data">
              <?php echo e(csrf_field()); ?>

             
               <div class="form_row common_field">
              <div class="form_row_left">
              <label class="form_label">
                <span class="english"><?php echo lang::get('mer_en_lang.MER_Select_Menu'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Select_Menu'); ?> </span>

          </label>
              <div class="info100">
              <select class="small-sel" name="menucategory" id="mencategory">
                <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Select_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Select_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Select_Menu')); ?> <?php endif; ?></option>
                  <?php $menu_name='attribute_title'?>
                 <?php if($mer_selected_lang_code !='en'): ?>
            <?php $menu_name= 'attribute_title_'.$mer_selected_lang_code; ?>
            <?php endif; ?>
                   <?php $__currentLoopData = $menucat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($val->id); ?>" <?php echo e(isset($fetchfirstdata->attribute_id) && $fetchfirstdata->attribute_id ==$val->id ? 'selected' : ''); ?>><?php echo e($val->$menu_name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
              </select>
              </div></div></div>
              <div class="form_row">
              <div class="form_row_left">
              <label class="form_label">
                 <span class="english"><?php echo lang::get('mer_en_lang.MER_Dish_Name'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Dish_Name'); ?> </span> </label>
              <div class="info100">
              <div class="english">
              <input type="text" class="english" name="dish_name" maxlength="100" value="<?php echo e(isset($fetchfirstdata->pro_title) ? $fetchfirstdata->pro_title : ''); ?>" id="dish_name" required="" >
                 </div>     
              <div class="arabic">
              <input class="arabic ar" id="dish_name_ar" maxlength="100" value="<?php echo e(isset($fetchfirstdata->pro_title_ar) ? $fetchfirstdata->pro_title_ar : ''); ?>"  name="dish_name_ar"   type="text" >
              </div>
              </div>
              </div>
              </div>

              
              <div class="form_row">
              <div class="form_row_left">
              <label class="form_label">
                <span class="english"><?php echo lang::get('mer_en_lang.MER_About'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_About'); ?> </span>
 </label>
 
 
 
 
 
              <div class="info100">
               <div class="english">
              <textarea class="" maxlength="500" name="about" id="about" rows="4" cols="50"> <?php echo e(isset($fetchfirstdata->pro_desc) ? $fetchfirstdata->pro_desc : ''); ?> </textarea>
			  
               </div>

                  <div class="arabic ar">
              <textarea class="" maxlength="500" name="about_ar" id="about_ar" rows="4" cols="50"> <?php echo e(isset($fetchfirstdata->pro_desc_ar) ? $fetchfirstdata->pro_desc_ar : ''); ?> </textarea>
               </div>
              </div>
              </div>
              </div>
             
                <?php if($containerpackage!=''): ?>
                <div class="form_row common_field">
                  <div class="form_row_left">

                  <label class="form_label disc_label">
                   <span class="english"><?php echo lang::get('mer_en_lang.MER_CONTAINER'); ?></span>
                 <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_CONTAINER'); ?> </span>
                    </label>
					
					<label class="form_label disc_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); ?> %</span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); ?>  %</span> </label>
					
                    <div class="info100">
                   
                     <?php $containername='option_title'?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php //$containername= 'option_title_ar'; ?>
                          <?php endif; ?>
                       <?php $__currentLoopData = $containerpackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                       if($autoid!=''){
                       $data =  Helper::getcontainerPrice($fetchfirstdata->pro_id,$val->id);
                       }
                        ?>
				  <div class="container_row">
                       
                        <div class="label_fl"><?php echo e(isset($val->$containername) ? $val->$containername : ''); ?></div>
                        <div class="input_flr"><input type="text" onkeypress="return isNumber(event)" class="check notzero" name="containerprice[]" value="<?php echo e(isset($data->price) ? $data->price : ''); ?>"  maxlength="8"></div>
                         <input type="hidden" name="container_id[]" value="<?php echo e(isset($val->id) ? $val->id : ''); ?>">
                       
                      <div class="discount_col_2">
            <div class="discount_col">
                    
                    <div class="info100">
            
            <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount[]" onkeypress="return isNumber(event)" maxlength="2" value="<?php echo e(isset($data->discount) ? $data->discount : ''); ?>" id="discount" >
                      </div>
            
                    </div>
                    <!-- form_row -->
                  </div>
          </div>
				  
                      </div>
                        
                        
          
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

					
					
                    </div>
                    
                </div>
                <?php endif; ?>
<span class="containersd"></span>

               
               <div class="form_row common_field">
              <div class="form_row_left">
              <label class="form_label">
                   <span class="english"><?php echo lang::get('mer_en_lang.MER_Dish_Image'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Dish_Image'); ?> </span>

          </label>
              <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn">
                         <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span>
 </div>
                    </div>
                    </label>
                    <input id="company_logo" name="dish_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" type="file">
                  </div>
                    <?php if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!=''): ?>
                <div class="form-upload-img">
                       <img src="<?php echo e(isset($fetchfirstdata->pro_Img) ? $fetchfirstdata->pro_Img : ''); ?>" width="150" height="150">
                     </div> 
                     <?php endif; ?>
                  </div>                
                 
                 </div> 
               <div class="form_row">
               <div class="form_row_left">
               <div class="english">   
              <input type="hidden" name="parent_id" value="<?php echo e(isset($parent_id) ? $parent_id : ''); ?>">
              <input type="hidden" name="sid" value="<?php echo e(isset($sid) ? $sid : ''); ?>">
              <input type="hidden" name="itemid" value="<?php echo e(isset($itemid) ? $itemid : ''); ?>">
              <input type="hidden" name="autoid" value="<?php echo e(isset($autoid) ? $autoid : ''); ?>">
              <input type="submit" name="submit" value="Submit">
              </div>

               <div class="arabic ar">   
              <input type="submit" name="submit" value="خضع">
              </div>
              </div></div>
              
            </form>

          
          <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
          
          <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
          </div>
        </div>
      </div>
    </div>
    <!-- inner --> 
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  menucategory: {
                       required: true,
                      },

                       dish_name: {
                       required: true,
                      },
                       dish_name_ar: {
                       required: true,
                      },
                      
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       
                       <?php if(!isset($fetchfirstdata->pro_Img)): ?> 
                       dish_image: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
                      <?php else: ?>
                       dish_image: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>
     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                            
           messages: {
             menucategory: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_SELECT_CATEGORY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SELECT_CATEGORY')); ?> <?php endif; ?>",
                      },  
     
                dish_name: {
            required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_DISH_NAME'); ?>",
                      },
                 dish_name_ar: {
               required:  "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_DISH_NAME_AR'); ?>",
                      }, 

                    about: {
           required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); ?>",
                      }, 
 
                    about_ar: {
               required:  "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_AR'); ?>",
                      },
                  



                     dish_image: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid; 
            <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.menucategory != "undefined" || valdata.menucategory != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.dish_name != "undefined" || valdata.dish_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                   
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.containerprice != "undefined" || valdata.containerprice != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.dish_image != "undefined" || valdata.dish_image != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.dish_name_ar != "undefined" || valdata.dish_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>

                if (typeof valdata.dish_name_ar != "undefined" || valdata.dish_name_ar != null) 
                {

                $('.arabic_tab').trigger('click');     

                }

                if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                {

                $('.arabic_tab').trigger('click');     

                }
                if (typeof valdata.menucategory != "undefined" || valdata.menucategory != null) 
                {
                $('.english_tab').trigger('click'); 
                }

                if (typeof valdata.dish_name != "undefined" || valdata.dish_name != null) 
                {
                $('.english_tab').trigger('click'); 
                }

                if (typeof valdata.about != "undefined" || valdata.about != null) 
                {
                $('.english_tab').trigger('click'); 
                }
                if (typeof valdata.containerprice != "undefined" || valdata.containerprice != null) 
                {
                $('.english_tab').trigger('click'); 
                }
                if (typeof valdata.dish_image != "undefined" || valdata.dish_image != null) 
                {
                $('.english_tab').trigger('click'); 
                }


<?php endif; ?>

                    },

                submitHandler: function(form) {
 
              
                    $('.check').each(function() {    
                        if($(this).val() !='')
                        {
                       form.submit();
                        }
                        else
                        {
                         $('.containersd').html('<span class="error" style="text-align:left;">Please enter atleast one container price</span>'); 
                        }
                                  
                    });


                    //
                }
            });

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php //print_r(Session::all()); exit();?>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

    <meta charset="UTF-8" />

    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>

     <meta content="width=device-width, initial-scale=1.0" name="viewport" />

	<meta content="" name="description" />

	<meta content="" name="author" />

    <meta name="_token" content="<?php echo csrf_token(); ?>"/>

     <!--[if IE]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <![endif]-->

    <!-- GLOBAL STYLES -->

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />

     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />

    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>

     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />

      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />

     <?php endif; ?>

    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />

    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />

    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />

         <?php 

     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">

<?php endif; ?>

    <!--END GLOBAL STYLES -->



    <!-- PAGE LEVEL STYLES -->

    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />

    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />

    <link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />

     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>

    <!-- END PAGE LEVEL  STYLES -->

     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

</head>



    <!-- END HEAD -->



    <!-- BEGIN BODY -->

<body class="padTop53 " >



    <!-- MAIN WRAPPER -->

    <div id="wrap" >

 <!-- HEADER SECTION -->

	 <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- END HEADER SECTION -->

       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



       <!--PAGE CONTENT -->

        <div class=" col-sm-9 col-xs-12 right-side-bar">

         <div class="col-md-12 col-sm-12 col-xs-12">

    <div class="right_col" role="main">

  <div id="appUser">

    <div class="page-title">

      <div class="mainView-details bordr-bg">

        <div class="row">

        <div class="col-sm-6">

          <div class="title_left">

            <h3 class="padd"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_OCCASION_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OCCASION_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_OCCASION_DETAIL')); ?></h3>

            <p><?php echo e($occasion->occasion_name); ?></p>

            <p class="paddNone"><?php echo date('d ,M Y', strtotime($occasion->created_at));?></p>

          </div>

        </div>

        <div class="col-sm-6">

          <div class="user-view-info">

            <div class="userImage marBotm">

              <figure>



                <?php if($occasion->images != null): ?> <img src="<?php echo e(url('')); ?><?php echo e($occasion->images); ?>"> <?php else: ?><img src="<?php echo e(url('')); ?>/public/assets/adimage/user-dummy.png"><?php endif; ?>

              </figure>

              <a href="/admin/occasions" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a>

            </div>

            <div class="view-userDetails">

              <div class="view-userDetails-inner">

                <h3><?php echo e($occasion->occasion_name); ?></h3>

                <p class="username"><?php echo e($occasion->occasion_venue); ?></p>

                

              </div>

            </div>

          </div>

        </div>

      </div>

      </div>

      

    </div>

    <div class="clearfix"></div>

    <div class="viewListtab">

        <div class="userinformation bordr-bg paddzero small-width-column2">

              <ul class="paddleftright">

                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_POSTED_BY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_POSTED_BY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_POSTED_BY')); ?></label></div>

                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php if(isset($occasion->getUser)): ?> <?php echo e($occasion->getUser->cus_name); ?> <?php endif; ?></p></div>

                </li>

                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL')); ?></label></div>

                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php if(isset($occasion->getUser)): ?> <?php echo e($occasion->getUser->email); ?> <?php endif; ?></p></div>

                </li>

                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO')); ?></label></div>

                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>

                  <?php if(isset($occasion->getUser) && $occasion->getUser->cus_phone != ''): ?><?php echo e($occasion->getUser->country_code); ?>  <?php echo e($occasion->getUser->cus_phone); ?> <?php else: ?> N/A <?php endif; ?></p></div>

                </li>
                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CITY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY')); ?></label></div>
                  
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>

                  <?php 
                  $getCity = Helper::getcity($occasion->getUser->cus_city);
                  if(isset($occasion->getUser->cus_city) && $occasion->getUser->cus_city != ''){ echo $getCity->ci_name;} else{echo 'N/A';} ?></p></div>

                </li>
                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_OCCASION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OCCASION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_OCCASION')); ?></label></div>

                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p> <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?> <?php echo e($occasion->occasion_name_ar); ?> <?php else: ?>  <?php echo e($occasion->occasion_name); ?> <?php endif; ?> </p></div>

                </li>

                 <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_VENUE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENUE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_VENUE')); ?></label></div>

                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo e($occasion->occasion_venue); ?></p></div>

                </li>

                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_OCCASION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_OCCASION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_OCCASION')); ?></label></div>

                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo date('d,M Y', strtotime($occasion->occasion_date ));?></p></div>

                </li>

                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESCRIPTION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DESCRIPTION')); ?></label></div>

                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php if($occasion->description != ''): ?><?php echo e($occasion->description); ?> <?php else: ?> N/A <?php endif; ?></p></div>

                </li>

                <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_HASHTAGS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HASHTAGS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_HASHTAGS')); ?></label></div>

                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>

                  <?php if($occasion->hastags != ''): ?><?php echo e($occasion->hastags); ?> <?php else: ?> N/A <?php endif; ?></p></div>

                </li>

                  <li class="row">

                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IMAGES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IMAGES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGES')); ?></label></div>

                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>

                     <img src="<?php echo e($occasion->images); ?>" class="maxwidth_img">

                     <?php if(isset($occasionImage) && count($occasionImage)>0){ ?>

                      <?php $__currentLoopData = $occasionImage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                     <img src="<?php echo e($img->image_url); ?>" class="maxwidth_img">

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

                    <?php } ?>

                  </p></div>

                </li>

    

              </ul>

              

            </div>

        </div>

  </div>

</div>

</div>

</div>

    <!--END MAIN WRAPPER -->



    <!-- FOOTER -->

     <?php echo $__env->make('siteadmin.includes.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!--END FOOTER -->

 

   

    



    <!-- GLOBAL SCRIPTS -->

  

     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- END GLOBAL SCRIPTS -->



    <!-- PAGE LEVEL SCRIPTS -->

   

 

  

    <!-- END PAGE LEVEL SCRIPTS -->

    <script type="text/javascript">

   $.ajaxSetup({

       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

   });

</script>



<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script> 

</body>



    <!-- END BODY -->

</html>
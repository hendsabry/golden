<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $makeup_leftmenu =1; ?>
<div class="merchant_vendor">
<?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
  <div class="inner">
     <header>
      <?php if($autoid==''): ?>
      <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCT')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCT')); ?> <?php endif; ?> </h5>

        <?php else: ?>
          <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_PRODUCT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPDATE_PRODUCT')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPDATE_PRODUCT')); ?> <?php endif; ?> </h5>

        <?php endif; ?>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
		   <div class="global_area">
      <div class="row">

          <div class="col-lg-12">
            <div class="box">
		
        <form name="form1" id="menucategory" method="post" action="<?php echo e(route('store-makeup-product')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

				  
                 <div class="form_row common_field">
              <div class="form_row_left">
                    <label class="form_label">
                      <span class="english"><?php echo lang::get('mer_en_lang.SELECT_CATEGORY'); ?></span>
                     <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.SELECT_CATEGORY'); ?> </span>
                    </label>
              <div class="info100"> 
              <select class="small-sel" name="category" id="category">
                  <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SELECT_CATEGORY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY')); ?> <?php endif; ?></option>
                    <?php $menu_name='attribute_title'?>
                   <?php if($mer_selected_lang_code !='en'): ?>
                   <?php $menu_name= 'attribute_title_'.$mer_selected_lang_code; ?>
                    <?php endif; ?>
                     <?php $__currentLoopData = $productcat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($val->id); ?>" <?php echo e(isset($fetchfirstdata->attribute_id) && $fetchfirstdata->attribute_id ==$val->id ? 'selected' : ''); ?>><?php echo e($val->$menu_name); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
              </select>
              </div>
              </div>
              </div>
                  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
                      <span class="english"><?php echo lang::get('mer_en_lang.MER_PRODUCT_NAME'); ?></span>
                      <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRODUCT_NAME'); ?> </span>
                    </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="<?php echo e(isset($fetchfirstdata->pro_title) ? $fetchfirstdata->pro_title : ''); ?>"  class="english"  maxlength="150" />
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" value="<?php echo e(isset($fetchfirstdata->pro_title_ar) ? $fetchfirstdata->pro_title_ar : ''); ?>" class="arabic ar" name="mc_name_ar"  id="mc_name_ar"   type="text"  maxlength="150">
                      </div>
                    </div>
                    </div>
                  </div>
				  
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english"><?php echo lang::get('mer_en_lang.MER_PRODUCT_IMAGE'); ?></span>
<span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRODUCT_IMAGE'); ?> </span>
 </label>
                    <div class="info100">
                     
				  <div class="input-file-area">
                  <label for="company_logo">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
   <div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div>
                  </div>
                  </label>
                  <input id="company_logo" data-lbl="file_value1" name="proimage" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
                </div>

                <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                 <?php if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img !=''): ?>
                  <div class="form-upload-img">
                       <img src="<?php echo e(isset($fetchfirstdata->pro_Img) ? $fetchfirstdata->pro_Img : ''); ?>" >
                     </div>
                     <?php endif; ?>

                    </div>
                    </div>



      
<!-- Add More product images start -->
        <?php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        ?>

        <?php for($i=0;$i < $GalleryCunt;$i++): ?>
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo<?php echo e($k); ?>">
        <div class="file-btn-area">
        <div id="file_value<?php echo e($J); ?>" class="file-value"></div>
        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
        </div>
        </label>
        <input id="company_logo<?php echo e($k); ?>" name="image[]" class="info-file proImgess" data-lbl="file_value<?php echo e($J); ?>" type="file" value="">
        </div>
        <?php if(isset($productGallery[$i]->image) && $productGallery[$i]->image!=''): ?>
        <div class="form-upload-img product_img_del">
        <img src="<?php echo e(isset($productGallery[$i]->image) ? $productGallery[$i]->image : ''); ?>" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="<?php echo e($productGallery[$i]->id); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value<?php echo e($J); ?>"></span>  
        <?php endif; ?>
        <input type="hidden" name="privius[]" value="<?php echo e($productGallery[$i]->id); ?>">
        </div>

        </div>
        <?php  $J=$J + 1; $k=$k +1; ?>
        <?php endfor; ?>
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>
        <span class="error pictureformat"></span>
        <?php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} ?>                
        <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
        </div>
  <!-- Add More product images end -->








                    
                  </div>
                  
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english"><?php echo lang::get('mer_en_lang.MER_PRICE'); ?></span>
<span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRICE'); ?> </span>
 </label>
                    <div class="info100">
                      
                        <input type="text" class="notzero" name="price" id="price" onkeypress="return isNumber(event)" value="<?php echo e(isset($fetchfirstdata->pro_price) ? $fetchfirstdata->pro_price : ''); ?>"  maxlength="9" />
                     
                    </div>
                    </div>
                  </div>

                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); ?> %</span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); ?>  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="<?php echo e(isset($fetchfirstdata->pro_discount_percentage) ? $fetchfirstdata->pro_discount_percentage : ''); ?>" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>


                   <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
                    <span class="english"><?php echo lang::get('mer_en_lang.QTYSTOCK'); ?></span>
                    <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.QTYSTOCK'); ?> </span>
                     </label>
                    <div class="info100">
                        <input type="text" class="notzero" name="quantity" id="quantity" onkeypress="return isNumber(event)" value="<?php echo e(isset($fetchfirstdata->pro_qty) ? $fetchfirstdata->pro_qty : ''); ?>" id="quantity"  maxlength="4" />
                        <input type="hidden" name="itemid" value="<?php echo e($itemid); ?>">
                         <input type="hidden" name="id" value="<?php echo e($id); ?>">
                         <input type="hidden" name="autoid" value="<?php echo e($autoid); ?>">
                     
                    </div>
                    </div>
                  </div>
				  
				  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english"><?php echo lang::get('mer_en_lang.MER_DESCRIPTION'); ?></span>
<span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); ?> </span>
 </label>
                    <div class="info100">
					   <div class="english"><textarea name="description" maxlength="500" id="description"><?php echo e(isset($fetchfirstdata->pro_desc) ? $fetchfirstdata->pro_desc : ''); ?></textarea></div>
					    
              <div class="arabic ar"><textarea name="description_ar" maxlength="500" id="description_ar"><?php echo e(isset($fetchfirstdata->pro_desc_ar) ? $fetchfirstdata->pro_desc_ar : ''); ?></textarea></div>
                      
                    </div>
                    </div>
                  </div>
				  
                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  </div></div>
			   </form>
		
			 
			</div></div></div></div>
		

  </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>
 $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#menucategory").validate({

                  ignore: [],
                  rules: {
                  category: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },
                       mc_name_ar: {
                       required: true,
                      },

                      service_name_ar: {
                       required: true,
                      },

                       duration: {
                       required: true,
                      },
                       price: {
                       required: true,
                      },
                       quantity: {
                       required: true,
                      },

                      year_of_exp: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                       <?php if(isset($fetchfirstdata->pro_Img)!=''): ?>  
                        proimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       <?php else: ?>
                        proimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      <?php endif; ?>


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             category: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY')); ?> <?php endif; ?>",
                      },  

                  mc_name: {
                     required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_PRODUCTNAME'); ?>",

                      },   

                          mc_name_ar: {
                             required:  "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_PRODUCTNAME'); ?>",
                 
                      },   

             

                       price: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')); ?> <?php endif; ?> ",
                      },

                        quantity: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY')); ?> <?php endif; ?> ",
                      },


                       year_of_exp: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXP')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXP')); ?> <?php endif; ?> ",
                      },
                     description: {
                       required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); ?>",
                      },

                       description_ar: {
                         required:  "<?php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER_AR'); ?>",
                      },

                  proimage: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },   


                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  <?php if($mer_selected_lang_code !='en'): ?>
                  
                    
                    
                     if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.image != "undefined" || valdata.image != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }
          <?php else: ?>

                if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
             
                     if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }  

                    if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }
                    
                     if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.image != "undefined" || valdata.image != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }
                      
                    
                <?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script> 
 
<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         <?php if(Session::get('lang_file') =='ar_lang'): ?>
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        <?php else: ?>
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        <?php endif; ?>
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-shop-image')); ?>",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
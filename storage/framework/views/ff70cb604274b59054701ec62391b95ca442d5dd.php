<?php $__env->startSection('content'); ?>

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2><?php echo e($fooddateshopdetails[0]->mc_name); ?></h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php echo e($fooddateshopdetails[0]->mc_name); ?></h2>
                                <h2><?php echo e($fooddateshopdetails[0]->address); ?>.</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                     <?php $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id); 
                                        $mc_name = 'ci_name'; 
                                        if(Session::get('lang_file')!='en_lang')
                                        {
                                          $mc_name = 'ci_name_ar'; 
                                        }
                                        echo $getcityname->$mc_name; 
                                      ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php echo e($fooddateshopdetails[0]->mc_discription); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                                   <?php  $homevisitcharges=$fooddateshopdetails[0]->home_visit_charge; ?>
 <?php if(isset($fooddateshopdetails[0]->google_map_address) && $fooddateshopdetails[0]->google_map_address!=''){
 $lat  = $fooddateshopdetails[0]->latitude;
$long  = $fooddateshopdetails[0]->longitude;
 ?>
          <div class="detail_hall_dimention" id="map" style="height: 230px!important">  </div>
          <?php } ?> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">
                            <?php if(Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.What_Our_Client_Says')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.What_Our_Client_Says')); ?> <?php endif; ?>
                        </div>
                        <div class="items-reviews">
                            <?php $__currentLoopData = $fooddateshopreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customerreview): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php $userinfo = Helper::getuserinfo($customerreview->customer_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <img src="<?php echo e(url('/')); ?>/themes/images/star<?php echo e($customerreview->ratings); ?>.png"/>
                                    </div>
                                    <p><?php echo e($customerreview->comments); ?></p>
                                </div>
                            </div>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php echo e($fooddateshopreview->links()); ?>

                        <!-- <ul class="pagenation">
                            <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">10</a></li>
                            <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                            <iframe class="service-video" src="<?php echo e($fooddateshopdetails[0]->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
<?php $tbl_field='service_id';  ?>
            <div class="desserts">
                <center>
                    <ul class="link">
                             <?php    
                    
                    $k=1; $jk=0;  ?>
                    <?php $__currentLoopData = $servicecategoryAndservices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categories): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                 
                        if(count($categories->serviceslist)>0){
                        if($k==1){ $cl='select'; }else{ $cl=''; }                     
                    ?>
                  <?php if(isset($categories->attribute_title) && $categories->attribute_title !=''): ?>
                        <li class="active">
                            <div class="image"><a href="#<?php echo e($k); ?>"  class="cat <?php echo e($cl); ?>" data-toggle="tab" <?php if(isset($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id) && $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id!=''): ?> onclick="getbeautyservice('<?php echo e($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');"  <?php endif; ?>><img src="<?php echo e($categories->image); ?>" alt=""></a></div>
                            <h2><a href="#<?php echo e($k); ?>"  class="cat <?php echo e($cl); ?>" data-toggle="tab" <?php if(isset($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id) && $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id!=''): ?> onclick="getbeautyservice('<?php echo e($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id); ?>','<?php echo e($branchid); ?>','<?php echo e($tbl_field); ?>');"  <?php endif; ?>><?php echo e(isset($categories->attribute_title) ? $categories->attribute_title : ''); ?></a></h2>
                        </li>
                        
                      <!--   <li>
                            <div class="image"><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/beaut-elegance.png" alt=""></a></div>
                            <h2><a href="">Laser</a></h2>
                        </li> -->
                             <?php endif; ?>
                <?php $k++; $jk++; } ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </center>

                <div class="box p-15">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>

        </div>
    </div>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
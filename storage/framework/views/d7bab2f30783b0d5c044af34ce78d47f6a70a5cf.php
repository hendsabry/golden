<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="UTF-8" />
<title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="_token" content="<?php echo csrf_token(); ?>"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
<?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
<?php endif; ?>
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
<?php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
<!--END GLOBAL STYLES -->
<!-- PAGE LEVEL STYLES -->
<link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
<link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head><!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="padTop53 " >
<!-- MAIN WRAPPER -->
<div id="wrap" >
<!-- HEADER SECTION -->
<?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- END HEADER SECTION -->
<?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--PAGE CONTENT -->
<div class=" col-sm-9 col-xs-12 right-side-bar">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="right_col" role="main">
      <div class="" id="appUser">
        <div class="page-title">
          <div class="mainView-details bordr-bg align-head">
            <div class="title_left title-block arabic-right-align">
              <h3 class="maxwith"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_VENDOR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDOR') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDOR')); ?></h3>
              <div class="back-btn-area"> <a href="/admin/vendor_management" class="back-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK')); ?></a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="viewListtab">
        <div class="userinformation bordr-bg paddzero small-width-column2">
         <?php if(Session::has('message')): ?>
          <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
         <?php endif; ?>
		  <?php echo Form::open(['url' => 'admin/vendor_management/updateserviceofmerchant', 'method' => 'post', 'name'=>'service_form', 'id'=>'service_form', 'enctype' => 'multipart/form-data']); ?>

		  <input type="hidden" name="merchantid" value="<?=$id?>">
          <ul class="paddleftright">
            <li class="row">
              <div class="col-lg-2 col-sm-3 col-xs-12">
                <label><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SERVICES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICES')); ?> </label>
              </div>
              <?php if(count($getServicelist)>0): ?>
              <div class="col-sm-9"> <?php $__currentLoopData = $getServicelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <p><input type="checkbox" name="active_and_deactive[<?=$service->id?>]" value="1" <?php if(isset($service->status) && $service->status==1){echo 'Checked="Checked"';}?>> <?php $getname = Helper::getCatName($service->service_id); echo $getname; ?>
				</p>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<br>
				<input type="submit" name="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.BACK_SUBMIT')!= '')  ?  trans(Session::get('lang_file').'.BACK_SUBMIT'): trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?>" class="form-btn btn-info-wisitech"> 
				</div>
              <?php else: ?>
			  <p><br><br>
              <div class="col-sm-9">
                <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?>

              </div>
			  </p>
              <?php endif; ?> 
			 </li>
          </ul>
		  <?php echo Form::close(); ?>

        </div>



       
         
       
     



      </div>
<div class="table-responsive paddnone"><table class="table table-striped jambo_table bulk_action"><thead><tr class="headings"><th class="column-title numrical_2"> S.No </th> <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_ID')); ?> </th>
<th class="column-title"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SERVICES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICES')); ?> </th>
 <th class="column-title no-link"><span class="nobr"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT')); ?> </span></th> <th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE')); ?></span></th><th class="column-title no-link"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.STATUS')); ?></span></th></tr></thead>
 <tbody>
  <?php $k=0; 
 if(count($walletamount)>0){
  ?>
  <?php $__currentLoopData = $walletamount; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $wallet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php $k++; ?>
  <tr><td> <?php echo e($k); ?> </td> <td><?php echo e($wallet->transaction_id); ?></td> <td> <?php echo e($wallet->category_name); ?> </td> <td><?php echo e(number_format($wallet->subscription_amount,2)); ?></td> <td><?php echo e(Carbon\Carbon::parse($wallet->created_at)->format('F j, Y')); ?></td> <td> 
<?php if($wallet->status==1){ $st='Failed';} else { $st='Complete'; } ?>
    <?php echo e($st); ?> </td></tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <tr><td colspan="6"> <?php echo e($walletamount->links()); ?> </td></tr>

                    <?php }else{ 
                   
                    ?>
 <tr><td colspan="6"> <?php if(Lang::has(Session::get('admin_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('admin_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans(Session::get('admin_lang_file').'.NO_RECORD_FOUND')); ?> <?php endif; ?> </td></tr>
                    <?php } ?>
                          </tbody></table>
                          </div>
    </div>

  </div>

    





</div>
<?php
//echo "<pre>";
  //print_r($walletamount);
?>
<!--END MAIN WRAPPER -->
<!-- FOOTER -->
<?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--END FOOTER -->
<!-- GLOBAL SCRIPTS -->
<script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- END GLOBAL SCRIPTS -->
<!-- PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
        $.ajaxSetup({
          headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
      </script>
<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>

</body>
<!-- END BODY -->
</html>

<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#"><?php echo e((Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')); ?></a></div>
    <?php echo $__env->make('includes.left_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="myaccount_right">
      <h1 class="dashborad_heading"><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')); ?></h1>
      
      <?php echo Form::open(array('url'=>"my-request-a-quote",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'filter')); ?>

      <div class="dash_select">
        <div class="search-box-field">
          <select name="search_key" required id="search_key" onChange="document.filter.submit();">
		    <option value=""><?php echo e((Lang::has(Session::get('lang_file').'.SELECT_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.SELECT_QUOTE'): trans($OUR_LANGUAGE.'.SELECT_QUOTE')); ?></option>
            <option value="singer"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='singer'){echo 'Selected="Selected"';}?>><?php echo e((Lang::has(Session::get('lang_file').'.SINGER')!= '')  ?  trans(Session::get('lang_file').'.SINGER'): trans($OUR_LANGUAGE.'.SINGER')); ?></option>
            <option value="band"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='band'){echo 'Selected="Selected"';}?>><?php echo e((Lang::has(Session::get('lang_file').'.POPULAR_BAND')!= '')  ?  trans(Session::get('lang_file').'.POPULAR_BAND'): trans($OUR_LANGUAGE.'.POPULAR_BAND')); ?></option>
			<option value="recording"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='recording'){echo 'Selected="Selected"';}?>><?php echo e((Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')); ?></option>
          </select>
        </div>
      </div>
      <?php echo Form::close(); ?>


      <div class="field_group top_spacing_margin_occas">
	  <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>
        <div class="main_user">
		 <?php if(count($getallinquiry) >0): ?>
          <div class="myaccount-table">
            <div class="mytr">
              <div class="mytable_heading"></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('lang_file').'.QUOTE_FOR')!= '')  ?  trans(Session::get('lang_file').'.QUOTE_FOR'): trans($OUR_LANGUAGE.'.QUOTE_FOR')); ?></div>

              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('lang_file').'.HallOccasion')!= '')  ?  trans(Session::get('lang_file').'.HallOccasion'): trans($OUR_LANGUAGE.'.HallOccasion')); ?></div>

              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('lang_file').'.REQUEST_DATE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_DATE'): trans($OUR_LANGUAGE.'.REQUEST_DATE')); ?></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('lang_file').'.OccasionDateDuration')!= '')  ?  trans(Session::get('lang_file').'.OccasionDateDuration'): trans($OUR_LANGUAGE.'.OccasionDateDuration')); ?></div>
              <div class="mytable_heading order_id "><?php echo e((Lang::has(Session::get('lang_file').'.REPLY_BY')!= '')  ?  trans(Session::get('lang_file').'.REPLY_BY'): trans($OUR_LANGUAGE.'.REPLY_BY')); ?></div>
            </div>
            <?php $i = 1; ?>
            <?php if(count($getallinquiry) >0): ?>
            <?php $__currentLoopData = $getallinquiry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php
                $cityname=Helper::getcity($val->city_id);
            ?>
            <div class="mytr">
              <div class="mytd " data-title="Sl. No."><?php echo e($i); ?></div>

              <div class="mytd " data-title="Occasions Name">
<?php echo e((Lang::has(Session::get('lang_file').".$val->quote_for")!= '')  ?  trans(Session::get('lang_file').".$val->quote_for"): trans($OUR_LANGUAGE.".$val->quote_for")); ?> - <?php echo e($val->singer_name); ?> <br> <?php echo e($val->occasion_type); ?> 
 </div>
              <div class="mytd " data-title="Hall Occasion"><?php echo e(isset($val->hall) ? $val->hall : ''); ?> <br> <?php echo e($val->location); ?> - <?php echo e(isset($cityname->ci_name) ? $cityname->ci_name : ''); ?></div>
              <div class="mytd " data-title="Date"><?php echo e(date("d M Y", strtotime($val->created_at))); ?></div>
              <div class="mytd " data-title="Occasion Date"><?php echo e(date("d M Y", strtotime($val->date))); ?> <?php echo e(isset($val->time) ? $val->time : ''); ?> <br> <?php echo e($val->duration); ?> Hrs</div>

              <div class="mytd order_id" data-title="Order ID"><?php if($val->status==1){?><?php echo e((Lang::has(Session::get('lang_file').'.Process')!= '')  ?  trans(Session::get('lang_file').'.Process'): trans($OUR_LANGUAGE.'.Process')); ?><?php }elseif($val->status==2){?> <a href="<?php echo e(route('requestaquoteview',[$val->id])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.REPLY_BY_MERCHANT')!= '')  ?  trans(Session::get('lang_file').'.REPLY_BY_MERCHANT'): trans($OUR_LANGUAGE.'.REPLY_BY_MERCHANT')); ?></a><?php }elseif($val->status==3){ ?><a href="<?php echo e(route('requestaquoteview',[$val->id])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.CONFIRMED')!= '')  ?  trans(Session::get('lang_file').'.CONFIRMED'): trans($OUR_LANGUAGE.'.CONFIRMED')); ?></a><?php }elseif($val->status==4){ ?>
                <?php echo e((Lang::has(Session::get('lang_file').'.DENYBYCUSTOMER')!= '')  ?  trans(Session::get('lang_file').'.DENYBYCUSTOMER'): trans($OUR_LANGUAGE.'.DENYBYCUSTOMER')); ?>

              <?php }else{ ?><?php echo e((Lang::has(Session::get('lang_file').'.DENY')!= '')  ?  trans(Session::get('lang_file').'.DENY'): trans($OUR_LANGUAGE.'.DENY')); ?><?php } ?></div>
            </div>
            <?php $i++; ?> 
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
            <?php endif; ?>
         </div>
		 <?php else: ?>
		 <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
		 <?php endif; ?>
        </div> 		       
      </div>
	 <div class="paging_quote">  <?php echo e($getallinquiry->links()); ?></div>
    </div>    
    <!-- page-right-section -->
  </div>

  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<script type="text/javascript">
    /*function SearchData(str)
    {
     $.ajax({
     type: 'get',
     data: 'search_key='+str,
     url: '<?php echo url('my-account-ocassion-withajax'); ?>',
     success: function(responseText){  
      alert(responseText);
      $('#email_id_error_msg').html(responseText);  
      if(responseText!=''){
        $("#email_id").css('border', '1px solid red'); 
        $("#email_id").focus();
      }
      else
        $("#email_id").css('border', '1px solid #ccc');  
     }    
     });  
   }*/
</script>
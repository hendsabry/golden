<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
?>
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($vendordetails->mc_img); ?>" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
<?php if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !=''): ?>
          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>
      <?php endif; ?>  
      <li><a href="#kosha2"><?php echo e((Lang::has(Session::get('lang_file').'.choose_items')!= '')  ?  trans(Session::get('lang_file').'.choose_items'): trans($OUR_LANGUAGE.'.choose_items')); ?></a></li>  
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
          <?php } ?>
   
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>
 
 
  <!-- common_navbar -->
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?>
                <li><img src="<?php echo e(str_replace('thumb_','',$vendordetails->image)); ?>" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></div>
          <div class="detail_hall_description"><?php echo e($vendordetails->address); ?></div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
          <div class="detail_about_hall">
            <div class="comment more"><?php if($lang != 'en_lang'): ?> <?php echo e($vendordetails->mc_discription); ?> <?php else: ?> <?php echo e($vendordetails->mc_discription); ?> <?php endif; ?></div>
          </div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

                 <?php if($vendordetails->google_map_address!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          <?php }  ?>
        </div>
      </div>

      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>



  <?php if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !=''): ?>

        <div class="service-video-area">
         <?php if($vendordetails->mc_video_description!=''): ?> <div class="service-video-cont">
          <?php if($lang != 'en_lang'): ?>  <?php echo e($vendordetails->mc_video_description_ar); ?> <?php else: ?>  <?php echo e($vendordetails->mc_video_description); ?> <?php endif; ?></div><?php endif; ?>
          <div class="service-video-box">
        <?php if($vendordetails->mc_video_url !=''): ?>    <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> <?php endif; ?>
          </div>
        </div>
<?php endif; ?>

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
                   <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
     



  <div class="service_bottom"><a name="kosha2" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                <?php $i=1; ?>
                <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                <li><a href="javascript:void(0);" data-tabid="<?php echo e($i); ?>" data-id="block_<?php echo e($i); ?>" class="maintab <?php if($i==1): ?> select <?php endif; ?>"  > <?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($cats->attribute_title_ar); ?> <?php else: ?> <?php echo e($cats->attribute_title); ?>   <?php endif; ?> </a></li>
               <?php $i=$i+1; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
 <script type="text/javascript">
   
$('.maintab').click(function(){
var tabid = $(this).data('id');
$('.maintab').removeClass('select');
$('.service-display-section').hide();
$('#'+tabid).show();
$(this).addClass('select');
})

 </script>
 <!-- SINGLE -->
 
<?php $m=1; ?>
<?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div id="block_<?php echo e($m); ?>" class="service-display-section" <?php if($m!=1): ?> style="display:none;"  <?php endif; ?>> 


  <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
         

<?php 
$i=1; $j=1;  
$getC = $menus->product->count(); 
if($getC >= 9)
{
$pagination = ceil($getC/9); 
$getC = 9;

}
else
{ 
$pagination =1;
}


?>  
<?php if($getC >=1): ?>

 

<div class="diamond_main_wrapper">
    <div class="diamond_wrapper_outer">
    <div class="diamond_wrapper_main">
      <div class="diamond_wrapper_inner">
       
<?php $__currentLoopData = $menus->product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <span id="<?php echo e($m); ?>">
<!-- TILL 5 RECORD -->
  <?php if($getC <=5): ?> 
   <?php $img = str_replace('thumb_','',$val->pro_Img);  ?>
        <div class="row_<?php echo e($i); ?>of<?php echo e($getC); ?> rows<?php echo e($getC); ?>row">
         <a href="#">
            <div data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper <?php if($getC!=5 && $getC!=4  && $getC!=2): ?> category_wrapper<?php echo e($i); ?> <?php endif; ?>" style="background:url(<?php echo e(isset($img) ? $img : ''); ?>);">
              <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div><div class="clear"></div></div>
            </div>
          </a>
        </div>
<!-- TILL 6 RECORD -->
  <?php elseif($getC == 6): ?>
 
          <?php if($i != 3 && $i != 4): ?> 
          <?php if($i==5){ $M=4; } elseif($i==6){ $M=5; }else { $M=$i; } ?>
          <div class="row_<?php echo e($M); ?>of5 rows5row">
          <a href="#">
          <div data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="#">
          <span data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper  <?php if($i==3): ?> category_wrapper2 <?php else: ?> category_wrapper3 <?php endif; ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==4): ?>  <div class="clear"></div>   
         </div>   <?php endif; ?>  
  <?php endif; ?>
<!-- TILL 7 RECORD -->
  <?php elseif($getC == 7): ?>

          <?php if($i != 3 && $i != 4 && $i != 5): ?> 
          <?php if($i==6){ $j = 4;} if($i==7){ $j = 5;}  ?>
          <div class="row_<?php echo e($j); ?>of5 rows5row">
          <a href="#">
          <div data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
          </div>
          </a>
          </div>  
          <?php else: ?>
          <?php if($i==3): ?> <div class="row_3of5 rows5row">  <?php endif; ?>
          <a href="#">
          <span data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper  <?php if($i==3): ?> category_wrapper4 <?php elseif($i==4): ?> category_wrapper5 <?php else: ?> category_wrapper6 <?php endif; ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
          </span>
          </a>

          <?php if($i==5): ?>  <div class="clear"></div>   
          </div>   <?php endif; ?>  
  <?php endif; ?>
 <!-- TILL 8 RECORD -->
 <?php elseif($getC == 8): ?>
 
        <?php if($i==1 || $i==8 ): ?>
        <?php if($i==1) { $k = 1;} if($i==8) { $k = 5;}   ?>
       
          <div class="row_<?php echo e($k); ?>of5 rows5row">
          <a href="#">
          <div data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper category_wrapper1" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
          </div>
          </a>   
          </div>
          <?php else: ?>
          <?php if($i==2 ||$i==4 ||$i==6): ?>   <div class="row_3of5 rows5row"> <?php endif; ?>
          <?php if($i==2) { $P = 2;} if($i==3) { $P = 3;}  if($i==4) { $P = 2;}  if($i==5) { $P = 3;}  if($i==6) { $P = 7;}  if($i==7) { $P = 8;}  ?>

          <a href="#">
          <span data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper category_wrapper<?php echo e($P); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
          <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
          </span>
          </a>
          <?php if($i==3 ||$i==5 ||$i==7): ?>  
          <div class="clear"></div>
          </div><?php endif; ?>
 <?php endif; ?>
  <!-- TILL 9 RECORD -->
<?php elseif($getC == 9): ?>

        <?php if($i==1 || $i==9 ): ?>
        <?php if($i==1){$k=1; } if($i==9){$k=5;   } ?>
        <div class="row_<?php echo e($k); ?>of5 rows5row">
        <a href="#">
        <div data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>" class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <div class="category_title"><div class="category_title_inner"><?php echo e($val->pro_title); ?></div></div>
        </div>
        </a>   
        </div>
        <?php elseif($i==2 || $i==3 ): ?>
 
        <?php if($i==2): ?> <div class="row_2of5 rows5row"> <?php endif; ?> 
        <a href="#">
        <span data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==3): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==4 || $i==5 || $i==6 ): ?>


        <?php if($i==4): ?> <div class="row_3of5 rows5row"> <?php endif; ?> 
        <a href="#">
        <span data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"  class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
        </span>
        </a>

        <?php if($i==6): ?>    <div class="clear"></div>
        </div>
        <?php endif; ?> 

        <?php elseif($i==7 || $i==8 ): ?>

        <?php if($i==7): ?> <div class="row_4of5 rows5row"><?php endif; ?> 
        <a href="#">
        <span data-pid="<?php echo e($val->pro_id); ?>" data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>" class="category_wrapper category_wrapper<?php echo e($i); ?>" style="background:url(<?php echo e($val->pro_Img); ?>);">
        <span class="category_title"><span class="category_title_inner"><?php echo e($val->pro_title); ?></span><span class="clear"></span></span>
        </span>
        </a>
       
        <?php if($i==8): ?> <div class="clear"></div>
        </div> <?php endif; ?>            
        <?php endif; ?>

<?php endif; ?>

   <!-- END ALL LOOP -->
<?php $i= $i+1; $j=$j+1; ?>
</span>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



        </div>
    </div>
    </div>


  </div>
 
 <div class="diamond_shadow"><img src="<?php echo e(url('/images/shadow.png')); ?>" alt=""></div> 
      
     


    <?php if($pagination !=0): ?>
    <div class="pagnation-area">
    <ul class="pagination">  
    <li class="disabled"><span>«</span></li>
     
    <?php for($KO=1;$KO<=$pagination;$KO++): ?>
    <li class="<?php if($KO==1): ?> active <?php endif; ?> paginate" data-page="<?php echo e($KO); ?>" data-attribute_id="<?php echo e($menus->id); ?>"  data-pro_mc_id="126"  data-tabid="<?php echo e($m); ?>" data-att_id="<?php echo e($menus->id); ?>"><span><?php echo e($KO); ?></span></li>
    <?php endfor; ?>
    <li><a href="#?page=<?php echo e($pagination); ?>" rel="next">»</a></li>
    </ul>
    </div>
    <?php endif; ?>
 
      </div>    
     

          <form name="form1" method="post" action="<?php echo e(route('specialevent-add-to-cart')); ?>"" enctype="multipart/form-data">
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="product_gallery"> <?php    $pro_id = $menus->product[0]->pro_id; ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </div>
           
          <div class="service-product-name marB0" id="pname_<?php echo e($m); ?>"><?php echo e($menus->product[0]->pro_title); ?></div>
        <div class="kosha-select-description"  id="pdes_<?php echo e($m); ?>" ><?php echo e($menus->product[0]->pro_desc); ?></div>

          <div class="service_quantity_box">


          <?php


             if($menus->product[0]->pro_qty>0)
            {
                   if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') 
                   {
                           $cartbtn=trans(Session::get('lang_file').'.Add_to_Cart');  } else{  $cartbtn=trans($OUR_LANGUAGE.'.Add_to_Cart'); 
                    }
                      $btnst='';
                      $avcart='';
                    $avcart='block';
              }
               else
              {

                if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''){ $cartbtn=trans(Session::get('lang_file').'.SOLD_OUT'); }else{ $cartbtn=trans($OUR_LANGUAGE.'.SOLD_OUT');}
                $btnst='disabled="disabled"';
                $avcart='none';
              }





          $OriginalP = $menus->product[0]->pro_price; 
            $OriginalDis = $menus->product[0]->pro_discount_percentage; 
            if($OriginalDis!=''){
          $Disp = $OriginalP - ($OriginalP*$OriginalDis)/100;
        }
        else
        {
           $Disp = $OriginalP;
        }
          $Disp = number_format((float)$Disp, 2, '.', '');
    
          ?> 


            <div class="service-radio-line"  id="cartqty" style="display: <?php echo e($avcart); ?>">
    
    
      <div id="service_quantity_box" class="service_quantity_box">
      <div class="service_qunt"><?php if(Lang::has(Session::get('lang_file').'.Quantity')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Quantity')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Quantity')); ?> <?php endif; ?> </div>
        <div class="service_qunatity_row">
          
        <div data-title="Total Quality" class="td td2 quantity food-quantity">
        <div class="quantity">
    <button onclick="return pricecalculation('remove','<?php echo e($m); ?>');" class="sub" id="sub" type="button"></button>
    <input type="text" onkeypress="isNumberKey(event);" onkeyup="isNumberKey(event); pricecalculation('pricewithqty','<?php echo e($m); ?>');"  maxlength="3" min="1" value="1" id="qty_<?php echo e($m); ?>" name="product_qty">
    <button onclick="return pricecalculation('add','<?php echo e($m); ?>');" class="add" id="add" type="button"></button></div>
 
          
       </div>
      </div>
    </div>
  </div>


          </div>

          

            <div class="total_price"><?php if(Lang::has(Session::get('lang_file').'.Total_Price')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Total_Price')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Total_Price')); ?> <?php endif; ?> :
              <?php if($OriginalDis!=''){ ?>
                <span class="strike"><?php echo e(currency($menus->product[0]->pro_price,'SAR',$Current_Currency)); ?>    </span>
              <?php } ?>
             <span id="gtotal_<?php echo e($m); ?>"><?php echo e(currency($Disp,'SAR',$Current_Currency)); ?>  </span>
          
          <input type="hidden" name="totalp" id="totalp" value="<?php echo e(currency($Disp,'SAR',$Current_Currency, $format = false)); ?>">

            </div>  
         
         
          <div class="btn_row">
           <?php echo e(csrf_field()); ?>

           <input type="hidden" name="product_id" id="add_product_id_<?php echo e($m); ?>" value="<?php echo e($menus->product[0]->pro_id); ?>">
          <input type="hidden" name="cart_type" value="occasion">
          <input type="hidden" name="total_price" id="add_total_price_<?php echo e($m); ?>" value="<?php echo e(currency($Disp,'SAR',$Current_Currency, $format = false)); ?>">
          <input type="hidden" name="language_type" value="en">    
          
          <input type="hidden" name="attribute_id" id="attr_id_<?php echo e($m); ?>" value="<?php echo e($menus->id); ?>">
          <input type="hidden" name="cart_sub_type" value="events">
 
          <input type="hidden" name="product_type" value="sepeevent">

           <div class="btn_row" id="cartbtn">  <input type="submit" value="<?php echo e($cartbtn); ?>" id="sbtn" class="form-btn addto_cartbtn" <?php echo e($btnst); ?>></div>
             <span id="sold" class="form-btn addto_cartbtn" style="display: none;"><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></span>
          </div>
          <a class="diamond-ancar-btn" href="#choose_package"><img src="<?php echo e(url('/themes/images/service-up-arrow.png')); ?>" alt=""></a> 
        </form>
        </div>
        <?php else: ?> 
        <?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?>
        <?php endif; ?>       
      </div>

 <!-- Single end -->
<?php $m=$m+1; ?>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

 <!--service-display-section-->
     <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script type="text/javascript">
  
function pricecalculation(str,nm)
{

 var currentquantity = document.getElementById('qty_'+nm).value;

var product_id      = document.getElementById('add_product_id_'+nm).value;

  if(str =='remove')
  {
   var Qty = parseInt($('#qty_'+nm).val())-1; 
  }
  else if(str =='pricewithqty')
  {
  var Qty = parseInt($('#qty_'+nm).val());  
  }
  else
  {
   var Qty = parseInt($('#qty_'+nm).val())+1;  
  }
 
 if(product_id)
  {
   $.ajax({
     type:"GET",
     url:"<?php echo e(url('getProductQuantity')); ?>?product_id="+product_id+'&qty='+Qty,
    async: false,
     success:function(res)
     { 
         <?php $Cur = Session::get('currency'); ?>              
     if(res!='ok')
     {
      $('.action_popup').fadeIn(500);
       $('.overlay').fadeIn(500);
       $('#showmsg').show();
       $('#hidemsgab').hide();
       $('#showmsgab').show();
             var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_1').value = qtyupdated - 1;
        

     }
     else
     {
                if(Qty == 0){ return false;}
          var  totalp = $('#add_total_price_'+nm).val()
           var TP = parseInt(Qty)*parseFloat(totalp);
           TP = parseFloat(TP).toFixed(2);

           <?php $Cur = Session::get('currency'); ?>
          $('#gtotal_'+nm).html('<?php echo $Cur;?> '+TP);
     }
     }
   });
  }
  else
  {
 
        if(Qty == 0){ return false;}
        var  totalp = $('#add_total_price_'+nm).val()
         var TP = parseInt(Qty)*parseFloat(totalp);
         TP = parseFloat(TP).toFixed(2);

         <?php $Cur = Session::get('currency'); ?>
        $('#gtotal_'+nm).html('<?php echo $Cur;?> '+TP);



}










}

</script>


 <script type="text/javascript">  
 
 

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
 
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
 function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"<?php echo e(url('getmultipleImages')); ?>?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}

  $('body').on('click', '.category_wrapper', function (e) {
 
var Pro_id = $(this).data('pid'); 
var tabid = $(this).data('tabid');
 var attribute_id = $(this).data('att_id');
if(Pro_id !='')
{
 
$.ajax({
     type:"GET",
     url:"<?php echo e(url('getsingleProductInfo')); ?>?product_id="+Pro_id,
     success:function(res)
          {   
            var Add_to_Cart = "<?php if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Add_to_Cart')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Add_to_Cart')); ?> <?php endif; ?>";
            $('input[name="product_qty"]').val(1);
            var Info = res.split("~~~");
            var ProName = Info[0];

            var OriginalP = Info[1];
            var Disp = Info[2];
            var pro_Img = Info[3];  
            var pro_description = Info[4]; 
             var avqty = Info[5]; 
            $('#pname_'+tabid).html(ProName);
            $('#pdes_'+tabid).html(pro_description);
          <?php $Cur = Session::get('currency'); ?>

            $('#gtotal_'+tabid).html('<?php echo $Cur;?> '+Disp);         
            $('.proimg_'+tabid).attr('src',pro_Img);      
            $('#totalp').val(Disp); 
            $('#qty').val(1);
            $('#add_product_id_'+tabid).val(Pro_id); 
              checkgallery(Pro_id);
             $('#add_total_price_'+tabid).val(Disp); 
             if($.trim(attribute_id)!='')
             {
              $('#attr_id_'+tabid).val(attribute_id); 
             }


              if(avqty<1){
              $('#cartbtn').css('display','none');
              $('#cartqty').css('display','none');
              $('#sold').css('display','block');
            }else{
              $('#cartbtn').css('display','block');
              $('#cartqty').css('display','block');
              $('#sold').css('display','none');
              $("#sbtn").val(Add_to_Cart);
               $(':input[type="submit"]').prop('disabled', false);
            }
                    
           }
  });


}

});
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
 
 
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 <?php if(request()->type!=''): ?> 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 <?php endif; ?>


 <script type="text/javascript">
  $('.paginate').click(function(){
  var getPage = $(this).data('page');
  var tabid = $(this).data('tabid');
  var attribute_id = $(this).data('attribute_id');
  var pro_mc_id = $(this).data('pro_mc_id');
 $('.paginate').removeClass('active');

 $(this).addClass('active');
  $.ajax({
  type:"GET",
  url:"<?php echo e(url('getpagination')); ?>?page="+getPage+"&tabid="+tabid+"&attribute_id="+attribute_id+"&pro_mc_id="+pro_mc_id,
  success:function(res)
  {           
  $('#'+tabid).html(res);       
  }
  });
  })
 
  function isNumberKey(evt) {  

        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
       evt.preventDefault();
        } else {
            return true;
        }
    }
 





</script>

<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript" src="<?php echo e(url('/themes/js/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
<link href="<?php echo e(url('/themes/js/magnific-popup/magnific-popup.min.css')); ?>" rel="stylesheet" />
<script src="<?php echo e(url('/themes/js/height.js')); ?>"></script>
<div class="outer_wrapper">
  <div class="inner_wrap"> <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="hashtag-occasions-wrapper">
      <div class="form_title"><?php if(Lang::has(Session::get('lang_file').'.Occasionss')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Occasionss')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.Occasionss')); ?> <?php endif; ?> </div>
     
        <?php if(count($infy)>=1): ?>
        <?php $i=0;    ?>
        <?php $__currentLoopData = $infy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="hashtag-occasions-box <?php echo e($i); ?>">     
        <?php for($k=1;$k<=count($val);$k++): ?>
        <?php $J = $k-1;     ?>    
        <?php if($k!=1): ?> <div class="hashtag-multi-occasions"> <?php endif; ?>
        <div class="hashtag-info-section">
        <?php if($k==1): ?> 
        <div class="hashtag-info-left-section">
        <div class="hashtag-info-img"><img src="<?php echo e(url('/themes/images/user-default.jpg')); ?>" alt="" /></div>
        <div class="hashtag-info-name"><?php echo e(isset($infy[$i][0]->posted_by_name) ? $infy[$i][0]->posted_by_name : ''); ?></div>
        </div>
        <?php endif; ?> 
          <!-- hashtag-info-left-section --> 
          <div class="hashtag-info-right-section">
            <div class="hashtag-occasion-name"><?php echo e(isset($infy[$i][$J]->occasion_name) ? $infy[$i][$J]->occasion_name : ''); ?> (<?php echo e(isset($infy[$i][$J]->posted_by_name) ? $infy[$i][$J]->posted_by_name : ''); ?>)</div>
            <div class="hashtag-occasion-det"><?php echo e(Carbon\Carbon::parse($infy[$i][$J]->occasion_date)->format('d M Y')); ?>, <?php echo e(isset($infy[$i][$J]->occasion_venue) ? $infy[$i][$J]->occasion_venue : ''); ?></div> 
          </div>
          <!-- hashtag-info-right-section --> 
        </div>
        <!-- hashtag-info-section -->
        <div class="hashtag-occasions-gallery"> 
          <ul class="slides test">
            <!--li><a class="popup-link" href="<?php echo e(str_replace('thumb-','',$infy[$i][$J]->images)); ?>"><img src="<?php echo e(isset($infy[$i][$J]->images) ? $infy[$i][$J]->images : ''); ?>" alt="" /></a></li-->
          <?php $__currentLoopData = $infy[$i][$J]->image_url; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li><a class="popup-link_<?php echo e($i); ?>" href="<?php echo e(str_replace('thumb-','',$vals)); ?>"><img src="<?php echo e(isset($vals) ? $vals : ''); ?>" alt="" /></a></li>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul>
        </div>
        <script type="text/javascript">    
        jQuery('.popup-link_<?php echo $i;?>').magnificPopup({
        type: 'image',
        gallery:{enabled:true}
        }); 
        </script>
 <?php if($k!=1): ?> </div> <?php endif; ?>
   <?php endfor; ?>

 
     </div>
       <?php $i=$i+1; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php else: ?>
<div class="norecord">  <?php echo e((Lang::has(Session::get('lang_file').'.NO_RESULT_FOUND')!= '')  ?  trans(Session::get('lang_file').'.NO_RESULT_FOUND'): trans($OUR_LANGUAGE.'.NO_RESULT_FOUND')); ?>  </div>
<?php endif; ?>
    </div>
    <div class="paginate"> 
<?php echo e($getOccsaions->links()); ?> 
</div>
    <!-- occasions-wrapper -->
  </div>
  <!-- inner_wrap -->
</div>
<!-- outer_wrapper -->

<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
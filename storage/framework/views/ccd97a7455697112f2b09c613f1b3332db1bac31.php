<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title><?php if(Lang::has($lang.'.MAIL_EMAIL_TEMPLATE')!= ''): ?> <?php echo e(trans($lang.'.MAIL_EMAIL_TEMPLATE')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.MAIL_EMAIL_TEMPLATE')); ?> <?php endif; ?></title>
</head>
<body>
<?php 
$SITE_LOGO = 'http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png';
$logo = $SITE_LOGO;
if(file_exists($SITE_LOGO))
$logo = $SITE_LOGO;
$getFbLinks        = Helper::getfblinks();
$gettwitterLinks   = Helper::gettwitterlinks();
$getgoogleLinks    = Helper::getgooglelinks();
$getinstagramLinks = Helper::getinstagramlinks();
$getpinterestLinks = Helper::getpinterestlinks();
$getyoutubeLinks   = Helper::getyoutubelinks();
?>
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right  ">مرحبًا  <?php echo e($name); ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373;  text-align:right  ">نشكرا على التعبير عن الاهتمام في خدمتي. لقد ملأت التفاصيل أدناه</td> </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right;  ">مشرف</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; text-align:right; ">أقفاص ذهبية</td>
  </tr>
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
	<?php if($getFbLinks !=''): ?>
	<a target="_blank" title="Facebook" href="<?php echo e($getFbLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-facebook.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($gettwitterLinks !=''): ?>
	<a target="_blank" title="Twitter" href="<?php echo e($gettwitterLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-twitter.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getgoogleLinks !=''): ?>
	<a target="_blank" title="Google+" href="<?php echo e($getgoogleLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-googleplus.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getinstagramLinks !=''): ?>
	<a target="_blank" title="Instagram" href="<?php echo e($getinstagramLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/share-instagram.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getpinterestLinks !=''): ?>
	<a target="_blank" title="Pinterest" href="<?php echo e($getpinterestLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-pint.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getyoutubeLinks !=''): ?>
	<a target="_blank" title="Youtube" href=" <?php echo e($getyoutubeLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-youtube.png" /></a>
	<?php endif; ?>
	</td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">Hi <?php echo e($name); ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; "><?php if(Lang::has($lang.'.HEADINGONE')!= ''): ?> <?php echo e(trans($lang.'.HEADINGONE')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.HEADINGONE')); ?> <?php endif; ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; "><?php if(Lang::has($lang.'.HEADINGTWO')!= ''): ?> <?php echo e(trans($lang.'.HEADINGTWO')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.HEADINGTWO')); ?> <?php endif; ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td style="padding-top:20px; padding-bottom:20px"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.HALL')!= ''): ?> <?php echo e(trans($lang.'.HALL')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.HALL')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($hall); ?></a></td>
        </tr>
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.MYOCCASIONS')!= ''): ?> <?php echo e(trans($lang.'.MYOCCASIONS')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.MYOCCASIONS')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($occasion_type); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.CITY')!= ''): ?> <?php echo e(trans($lang.'.CITY')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.CITY')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($city); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.LOCATION')!= ''): ?> <?php echo e(trans($lang.'.LOCATION')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.LOCATION')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($location); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.DURATION')!= ''): ?> <?php echo e(trans($lang.'.DURATION')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.DURATION')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($duration); ?></a></td>
        </tr>
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.DATE')!= ''): ?> <?php echo e(trans($lang.'.DATE')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.DATE')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($date); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.TIME')!= ''): ?> <?php echo e(trans($lang.'.TIME')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.TIME')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($time); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.COMMENTS')!= ''): ?> <?php echo e(trans($lang.'.COMMENTS')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.COMMENTS')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($comments); ?></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; "><?php if(Lang::has($lang.'.LATEME_KNOW')!= ''): ?> <?php echo e(trans($lang.'.LATEME_KNOW')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.LATEME_KNOW')); ?> <?php endif; ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; "><?php if(Lang::has($lang.'.Golden_Cages')!= ''): ?> <?php echo e(trans($lang.'.Golden_Cages')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.Golden_Cages')); ?> <?php endif; ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
</body>
</html>

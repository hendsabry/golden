<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
        <div class="col-sm-9 col-xs-12 right-side-bar">
         <div class="right_col" role="main">
          <div id="appUserList">
            <div class="clearfix"></div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="page-title">
              <div class="title_left">
                  <h3><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT')); ?></h3>
              </div>             
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hight_overhid"> 
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
                    <div class="mainSeacrh_box">
                    <div class="input-groupSearch clearfix search-form">
                    <input v-model="search_keyword" value="" placeholder="<?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH_BY_NAME_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SEARCH_BY_NAME_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH_BY_NAME_EMAIL')); ?>" autocomplete="off" class="form-control pr-Large20" id="navbar-search-input" type="text" @keyup="search">
                    </div>
                    </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
                    <select v-model="status" class="form-control">
                    <option value="" selected="selected"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PLEASE_SELECT_STATUS')); ?></option>
                    <option  value="1"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE')); ?></option>
                    <option  value="0"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE')); ?></option>
                    </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
                    <button type="submit" :disabled="isProcessing" @click="search" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                    <button type="submit" :disabled="isProcessing" @click="clearFilter" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
                    <button type="submit" :disabled="isProcessing" @click="downloadCsvFilter" class="btn theme-default-btn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DOWNLOAD_CSV')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DOWNLOAD_CSV') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DOWNLOAD_CSV')); ?></button>
                    <?php if(Session::get('access_permission_type')>0): ?>
                    <button type="submit" :disabled="isProcessing" @click="addUser" class="btn theme-default-btn margleftbtn"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_ADD_A_NEW_USER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADD_A_NEW_USER') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_A_NEW_USER')); ?></button>
                    <?php endif; ?>
                    <input id="lang" class="form-control" value="<?php echo e(Session::get('admin_lang_code')); ?>" type="hidden">
                    </div>
                </div>           
            </div>
                  <div class="x_content UsersList">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"> <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_NAME')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_PHONE')); ?></th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_LAST_LOGGED')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_LAST_LOGGED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_LAST_LOGGED')); ?> </th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_WALLET_AMT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_WALLET_AMT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_WALLET_AMT')); ?></th>
                            <th class="column-title"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_STATUS')); ?></th>
                            <th class="column-title no-link last"><span class="nobr"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION')); ?></span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-if="users" v-for="ud in users">
                            <td>
                          {{ ud.cus_name }}
                            </td>
                            <td> {{ ud.email }}</td>
                            <td><p v-if="ud.cus_phone != ''">{{ ud.cus_phone }}</p><p v-else> N/A</p></td>
                            <td>            

                            {{ud.last_log_in}}</td>
                            <td>{{ ud.wallet }}</td>
                            <td>
                              <div>
                                <label class="switch" >
                                 <input class="example2" type="checkbox" v-bind:id="ud.cus_id" v-bind:checked="ud.cus_status == 1" @click="statusChanged(ud.cus_id)">
                                  <div class="slider round"></div>
                                </label>
                                </div>
                            </td>
                      
                            <td class="last">
                            <button  @click="ViewUser(ud.cus_id)" class="btn view-delete"><i class="fa fa-eye"></i> </button>
                            <?php if(Session::get('access_permission_type')>0): ?>
                            <button @click="EditUser(ud.cus_id)" class="btn view-delete"><i class="fa fa-edit"></i> </button>
                             <button  @click="DeleteUser(ud.cus_id)" class="btn view-delete"><i class="fa fa-trash-o"></i> </button>
                             <?php endif; ?>
                            </td>
                          </tr>
                           <tr v-if="users ==''">
                            <td colspan="7"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND')); ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>        
                  </div>
                  <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
                </div>
              </div>
          </div>
        </div>
       </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._users = [<?php echo $users->toJson(); ?>];
</script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/userlist.js"></script>  
</body>

    <!-- END BODY -->
</html>
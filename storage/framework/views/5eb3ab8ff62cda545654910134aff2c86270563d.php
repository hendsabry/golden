 <?php $data = app('App\Help'); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
 
<?php $singer_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu')); ?> <?php endif; ?></h5>
      </div>
       
 <?php $id = request()->id; $hid= request()->hid; ?>
          <?php echo Form::open(array('url'=>"singer-quoted-requested-list/{$id}/{$hid}",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?>

        <?php $statuss = request()->status; $user_acceptance= request()->acceptance;  $searchh= request()->search; ?>
        <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
        <div class="filter_area">
          <div class="filter_left">
            <div class="search_filter"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></div>
            <div class="search-box-field mems">
               <select name="acceptance" id="acceptance">
              <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_ACCEPT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_ACCEPT')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_ACCEPT')); ?> <?php endif; ?> </option>
              <option value="3" <?php if(isset($user_acceptance) && $user_acceptance=='3'): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.YES')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.YES')); ?> <?php endif; ?></option>
              <option value="2" <?php if(isset($user_acceptance) && $user_acceptance=='2'): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?> > <?php if(Lang::has(Session::get('mer_lang_file').'.NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.NO')); ?> <?php endif; ?></option>
            </select>
              <input name="" type="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''): ?><?php echo e(trans(Session::get('mer_lang_file').'.MER_APPLY')); ?><?php else: ?><?php echo e(trans($MER_OUR_LANGUAGE.'.MER_APPLY')); ?><?php endif; ?>" class="applu_bts" />
            </div>
          </div>
          <div class="search_box">
            <div class="search_filter">&nbsp;</div>
            <div class="filter_right">
              <input name="search" type="text" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SEARCH')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?> <?php endif; ?>" value="<?php echo e(isset($searchh) ? $searchh : ''); ?>" />
              <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
            </div>
          </div>
        </div>
      <!-- filter_area --> 
       <?php echo Form::close(); ?>





      <!-- Display Message after submition --> 
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?> 
              <!-- Display Message after submition --> 
              
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
          
            <div class="table_wrap"> 
              <!-- Display Message after submition --> 
              
              <!-- Display Message after submition -->
              <div class="panel-body panel panel-default">
             


<?php if($getusers->count() <1): ?>


<div class="no-record-area">
<?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?>

</div>

<?php else: ?>




                <div class="table">
                  <div class="tr">
<div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></div> 
<div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?></div> 
<!--<div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PHONE')); ?> <?php endif; ?></div> -->

<div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Request_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Request_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Request_Date')); ?> <?php endif; ?></div>

<div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.Language_Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Language_Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Language_Type')); ?> <?php endif; ?></div>


<div class="table_heading view_center"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?></div>
                  </div>
                 
        <?php $__currentLoopData = $getusers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

          <?php
                               if($vals->id!='') $details=Helper::detailid($vals->id);
                                ?>

<div class="tr">
<div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?>"><?php echo e($vals->cus_name); ?></div>
<div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?>"><?php echo e($vals->email); ?></div>
<!--<div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PHONE')); ?> <?php endif; ?>"><?php echo e($vals->cus_phone); ?></div>-->
<div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Request_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Request_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Request_Date')); ?> <?php endif; ?>">   <?php echo e(Carbon\Carbon::parse($vals->created_at)->format('d M Y')); ?>  </div>
<div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.Language_Type')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Language_Type')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Language_Type')); ?> <?php endif; ?>"> 
      <?php if($vals->language_type=='1'): ?>
          English
       <?php elseif($vals->language_type=='2'): ?>
          Arabic
        <?php endif; ?>  
               
    </div>
<div class="td td5 view_center" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?>">

 <?php if($vals->status==1): ?>
<a href="<?php echo e(route('singer-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itemid' => $vals->id])); ?>"><img src="<?php echo e(url('')); ?>/public/assets/img/view-icon.png" title="<?php if(Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_view_title')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_view_title')); ?> <?php endif; ?>" alt="" /></a>

 <?php elseif($vals->status==2): ?> 
 <a href="<?php echo e(route('singer-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itemid' => $vals->id,'autoid'=>$details->id])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Replied')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Replied')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Replied')); ?> <?php endif; ?></a>

<?php elseif($vals->status==3): ?> 
<?php if(isset($details->id) && $details->id!='') { $Dids = $details->id;}else{  $Dids ='';} ?>
 <a href="<?php echo e(route('singer-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itemid' => $vals->id,'autoid'=>$Dids])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.Confirmed_by_customer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Confirmed_by_customer')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Confirmed_by_customer')); ?> <?php endif; ?></a>
 

<?php elseif($vals->status==5): ?> 
 
 <a href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.Deny_by_admin')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Deny_by_admin')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Deny_by_admin')); ?> <?php endif; ?></a>

<?php elseif($vals->status==4): ?> 
 
 <a href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.Deny_by_user')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Deny_by_user')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Deny_by_user')); ?> <?php endif; ?></a>


<?php endif; ?>



</div>

</div>
 
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>         
                
                  
                  
                  </div>


                       <?php endif; ?>
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
                
     
               </div>
            <!-- table_wrap --> 
              <?php echo e($getusers->links()); ?> </div>
          </div>
             
              
              </div>
                          
  
          </div>
        </div>
        
      </div> <!-- global_area -->
      
    </div>
  </div>
</div>
</div>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
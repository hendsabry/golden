<?php $__env->startSection('content'); ?>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="<?php echo e(url('newWebsite')); ?>/images/shop-logo.png" alt="" />
                        <h2>Kosha Design</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></h2>
                                <h2><?php if($lang == 'en_lang'): ?>  <?php echo e($vendordetails->address); ?> <?php else: ?>   <?php echo e($vendordetails->address_ar); ?> <?php endif; ?></h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p><?php if($lang != 'en_lang'): ?> <?php echo e($vendordetails->mc_discription_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_discription); ?> <?php endif; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <?php if($vendordetails->google_map_address!=''): ?>
                            <?php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
                            <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
                        <div class="items-reviews">
                            <?php if(count($allreview) > 0){ ?>
                            <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                            <div class="item">
                                <div class="tump">
                                    <img src="<?php echo e($userinfo->cus_pic); ?>" alt="">
                                </div>
                                <div class="caption">
                                    <h2><?php echo e($userinfo->cus_name); ?></h2>
                                    <div class="stars">
                                        <?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?>
                                    </div>
                                    <p><?php echo e($val->comments); ?></p>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php } ?>
                        </div>
                        
                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                        <?php if($vendordetails->mc_video_description!=''): ?> <div class="service-video-cont">
                            <?php if($lang != 'en_lang'): ?>  <?php echo e($vendordetails->mc_video_description_ar); ?> <?php else: ?>  <?php echo e($vendordetails->mc_video_description); ?> <?php endif; ?></div><?php endif; ?>
                        <div class="service-video-box">
                            <?php if($vendordetails->mc_video_url !=''): ?>    <iframe style="width: 100%" class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li class="active">
                            <div class="image"><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/desykosha.png" alt=""></a></div>
                            <h2><a href="<?php echo e(route('kosha-shop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>" <?php if(request()->type!=2): ?> class="select" <?php endif; ?>><?php echo e((Lang::has(Session::get('lang_file').'.Design_Your_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Design_Your_Kosha'): trans($OUR_LANGUAGE.'.Design_Your_Kosha')); ?> </a></h2>
                        </li>
                        <li>
                            <div class="image"><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/wedding-arch.png" alt=""></a></div>
                            <h2><a  <?php if(request()->type==2): ?> class="select" <?php endif; ?> href="<?php echo e(route('kosha-redymadeshop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Ready_Made_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Ready_Made_Kosha'): trans($OUR_LANGUAGE.'.Ready_Made_Kosha')); ?> </a></h2>
                        </li>
                    </ul>
                </center>

                <br>
                <div class="slide-cats owl-carousel" dir="ltr">
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                </div>

                <div class="box kosha p-15">
                    <div class="row">
                        <div class="col-md-4">
                            <h3>Select Category 3</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="item-style clearfix">
                                        <div class="tump">
                                            <img src="<?php echo e(url('newWebsite')); ?>/images/img-losha1.png" alt="" />
                                        </div>
                                        <div class="caption">
                                            <a href="">Hall 1</a>
                                            <p>Lorem Ipsum is simply dummy
                                                text of the printing and .....</p>
                                            <div class="flex-end">
                                                <div class="price">2500 SAR</div>
                                                <a href="">Selected</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="item-style clearfix">
                                        <div class="tump">
                                            <img src="<?php echo e(url('newWebsite')); ?>/images/img-losha2.png" alt="" />
                                        </div>
                                        <div class="caption">
                                            <a href="">Hall 1</a>
                                            <p>Lorem Ipsum is simply dummy
                                                text of the printing and .....</p>
                                            <div class="flex-end">
                                                <div class="price">2500 SAR</div>
                                                <a href="">Select</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="item-style clearfix">
                                        <div class="tump">
                                            <img src="<?php echo e(url('newWebsite')); ?>/images/img-losha1.png" alt="" />
                                        </div>
                                        <div class="caption">
                                            <a href="">Hall 1</a>
                                            <p>Lorem Ipsum is simply dummy
                                                text of the printing and .....</p>
                                            <div class="flex-end">
                                                <div class="price">2500 SAR</div>
                                                <a href="">Select</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h3>Your Selection</h3>
                            <div class="box p-15">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="item-kosha">
                                            <div class="image">
                                                <img src="<?php echo e(url('newWebsite')); ?>/images/img-losha1.png" alt="" />
                                            </div>
                                            <div class="caption">
                                                <h2 class="fw-700">item name 1</h2>
                                                <p class="fw-700">Quantity : 1</p>
                                                <p class="fw-700 price">500 SAR</p>
                                                <a class="remove" href=""><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="item-kosha">
                                            <div class="image">
                                                <img src="<?php echo e(url('newWebsite')); ?>/images/img-losha2.png" alt="" />
                                            </div>
                                            <div class="caption">
                                                <h2 class="fw-700">item name 1</h2>
                                                <p class="fw-700">Quantity : 1</p>
                                                <p class="fw-700 price">500 SAR</p>
                                                <a class="remove" href=""><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('newWebsite.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
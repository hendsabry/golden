<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $dressesbig_leftmenu =1; ?>
<!--start merachant-->
<div class="merchant_vendor">
  <!--left panel-->
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!--left panel end-->
  <!--right panel-->
<?php
 
	$id                = $id;
	$sid               = $sid;
	$autoid            = $autoid;
	$proid             = $proid;
	$ordid             = $ordid;
	$cusid             = $cusid;
	$productid         = $productid;
	$getCustomer       = Helper::getuserinfo($cusid);
	$shipaddress       = Helper::getgenralinfo($ordid); 
    $oid = $ordid;
	$cityrecord        = Helper::getcity($shipaddress->shipping_city);
	$getorderedproduct = Helper::getorderedproduct($ordid);
?>
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order_Detail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order_Detail')); ?> <?php endif; ?> <a href="<?php echo e(url('/')); ?>/dresses-order/<?php echo e($id); ?>/<?php echo e($sid); ?>" class="order-back-page"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BACK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?> <?php endif; ?></a></h5>
       </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDRESS')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} ?> <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="<?php echo e(url('')); ?>/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?></label>
                    <div class="info100" > <?php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      ?> </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} ?></div>
                  </div>
                   <?php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ ?>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.transaction_id')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.transaction_id')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.transaction_id')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} ?></div>
                  </div>
                    <?php } ?>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD')); ?> <?php endif; ?></label>
                    <div class="info100"> 
 
 <?php echo e($productdata[0]->shippingMethod); ?>

 
                    </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE')); ?> <?php endif; ?></label>
                    <div class="info100">SAR  <?php echo e($productdata[0]->shippingPrice); ?></div>
                    <!-- box -->
                  </div>

                     <?php $getsearchedproduct = Helper::searchorderedDetails($oid);
 if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ ?>
                    <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONDATE')); ?> <?php endif; ?></label>
                    <div class="info100"> <?php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                     echo date('d M Y', strtotime($getsearchedproduct->occasion_date));
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      ?> </div>
                  </div>
                  <?php } ?>


                </div>
                <!--global end-->
              </div>
			   <?php 
			  $basetotal = 0;  $couponcode=0; $couponcodeinfo='';
			  $i = 1;
			  if(count($productdata) > 0){
			  foreach($productdata as $val)
			  {
			  $basetotal            = ($basetotal+$val->total_price);
			  $pid                  = $val->product_id;
			  $serviceInfo          = Helper::getProduckInfo($val->product_id);
			  $getAttributeInfo     = Helper::getAllProductAttribute($val->product_id,$val->merchant_id);
			  $getPrice=0;
        $newbprice=0;

         $couponcode=$couponcode+$val->coupon_code_amount;;
        $couponcodeinfo=$val->coupon_code;
        if(strtolower($val->buy_rent)=='rent'){
       if(isset($val->bookingdetail->return_time) && isset($val->bookingdetail->rental_time)){
        $time1=str_replace(',','',$val->bookingdetail->return_time);
        $time2=str_replace(',','',$val->bookingdetail->rental_time);
         $hourdiff = round((strtotime($time1) - strtotime($time2))/3600, 1);
        if($hourdiff>0){
          $bprice=$val->total_price-$val->bookingdetail->insurance_amount; 
          $newbprice=$bprice;      
        $getPrice=$bprice/$hourdiff;   }  else{ $getPrice=0;}  
        } 
      }else{

      if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
    }



			  ?>
      <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
        <div class="style_area">
          <?php if($i==1){?>
          <div class="style_head"><?php if(Lang::has(Session::get('mer_lang_file').'.item_information')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.item_information')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.item_information')); ?> <?php endif; ?></div>
          <?php } ?>
          <div class="style_box_type">
            <div class="sts_box">
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.DRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DRESS')); ?> <?php endif; ?></div>
                <div class="style_label_text"> <?php 
                            $protitle = 'pro_title'; 
                            if(Session::get('mer_lang_file')!='mer_en_lang')
                            {
                            $protitle = 'pro_title_ar'; 
                            }
                            echo $serviceInfo->$protitle;
                             ?></div>
              </div>
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_QUANTITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_QUANTITY')); ?> <?php endif; ?></div>
                <div class="style_label_text"><?php echo e($val->quantity); ?> </div>
              </div>
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Amount')); ?> <?php endif; ?></div>
                <div class="style_label_text">SAR <?php echo e(number_format($val->total_price,2)); ?></div>
              </div>
            </div>
            <div class="sts_box">
			  <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Price')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Price')); ?> <?php endif; ?></div>
                <div class="style_label_text">SAR 
                      <?php if($newbprice>0){?>  <?php echo number_format($newbprice,2); ?> <?php }else{?>
                  <?php echo number_format($getPrice,2); ?>
                    
                    <?php } ?>
                  </div>
              </div>
			  <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.SIZE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SIZE')); ?> <?php endif; ?></div>
                <div class="style_label_text"><?php if(isset($val->product_size) && $val->product_size!=''){ $size = str_replace('Size','',$val->product_size); echo $size;}?></div>
              </div>
			  <?php if(isset($val->buy_rent) && $val->buy_rent!=''){?>
			  <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FOR')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FOR')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FOR')); ?> <?php endif; ?></div>
                  <div class="style_label_text">
                    <?php if(Lang::has(Session::get('mer_lang_file').'.MER_RENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_RENT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_RENT')); ?> <?php endif; ?><?php //echo ucfirst($val->buy_rent); ?>                    
                  </div>
              </div>
			  <?php } ?>
            </div>
			
			  <?php 
			  if(isset($val->buy_rent) && $val->buy_rent=='rent'){           

           ?><div class="sts_box">
			  <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.RENT_CHARGES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.RENT_CHARGES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.RENT_CHARGES')); ?> <?php endif; ?></div>
                <div class="style_label_text">SAR <?php echo number_format($getPrice,2); ?></div>
              </div>
			  <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BOOKING_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BOOKING_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BOOKING_DATE')); ?> <?php endif; ?></div>
                <div class="style_label_text"><?php echo e(isset($val->bookingdetail->rental_time) ? $val->bookingdetail->rental_time : ''); ?></div>
              </div>
			  <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Returning_Date')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Returning_Date')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Returning_Date')); ?> <?php endif; ?></div>
                <div class="style_label_text"><?php echo e(isset($val->bookingdetail->return_time) ? $val->bookingdetail->return_time : ''); ?></div>
              </div>
			 </div> 
			 <?php }
			  //echo '<pre>';print_r($val);
			  ?> 
			  
			  <div class="sts_box">
			  <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.INSURANCEAMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.INSURANCEAMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.INSURANCEAMOUNT')); ?> <?php endif; ?></div>
                <div class="style_label_text">SAR <?php if(isset($val->bookingdetail->insurance_amount) && $val->bookingdetail->insurance_amount!=''){echo $val->bookingdetail->insurance_amount;}else{echo 'N/A';} ?></div>
              </div>
              <div class="style_left">
                <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_IMAGE')); ?> <?php endif; ?></div>
                <div class="style_label_text"> <?php if($serviceInfo->pro_Img): ?> <img src="<?php echo e($serviceInfo->pro_Img); ?>" alt="" width="150"> <?php else: ?>
                  No image
                  <?php endif; ?> </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <?php $i++;} ?>
      <div class="merchant-order-total-area">
                <div class="merchant-order-total-line">
          <?php if(isset($couponcode) && $couponcode>=1): ?>
             <?php echo e((Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')); ?>: 

             <?php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          ?>  
          <br>

          <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')); ?>: <?php echo e($couponcodeinfo); ?>

 <br>
          <?php endif; ?>



            <?php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             ?>

          <?php echo e((Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')); ?>: 
          <?php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); ?>

          <?php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          ?> 
 </div>
		  <div class="merchant-order-total-line">
        <?php if(Lang::has(Session::get('mer_lang_file').'.Total_Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Total_Price')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Total_Price')); ?> <?php endif; ?>:
        <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);
				 } ?>
      </div></div>
      <?php } ?>
            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
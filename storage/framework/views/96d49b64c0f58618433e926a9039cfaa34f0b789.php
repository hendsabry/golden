<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 
<div class="merchant_vendor">  
  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  <div class="profile_box">
  <div class="profile_area">
<div class="inner">
<div class="row">
<div class="col-lg-12">
	 <header>
	 <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ACCOUNT_DETAILS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ACCOUNT_DETAILS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ACCOUNT_DETAILS')); ?> <?php endif; ?></h5>
<!-- Display Message after submition --> 
<?php if(Session::has('message')): ?>
<div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
<?php endif; ?> 
<!-- Display Message after submition -->
</header>
	  
	 <div class="box commonbox"> 

<?php echo Form::open(array('url' => '/store-bank-info','method' => 'POST', 'id'=> 'store_bank_info', 'name' =>'store_bank_info' )); ?>


	  <div class="global_area">
	  <div class="form_row">
	  <div class="form_row_left">
	  <label class="form_label" for="text1"><?php if(Lang::has(Session::get('mer_lang_file').'.Bank_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Bank_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Bank_Name')); ?> <?php endif; ?> </label>
	  <div class="info100"> 
	  <input class="form-control" id="bank_name" value="<?php echo e(isset($getTotalAmount->bank_name) ? $getTotalAmount->bank_name : ''); ?>" maxlength="90" name="bank_name"  type="text"> </div>
	  </div>
	  <div class="form_row_right">
	  <label class="form_label" for="text1"><?php if(Lang::has(Session::get('mer_lang_file').'.Account_number')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Account_number')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Account_number')); ?> <?php endif; ?> </label>
	  <div class="info100"> <input class="form-control"  value="<?php echo e(isset($getTotalAmount->account_number) ? $getTotalAmount->account_number : ''); ?>" onkeypress="return isNumber(event)" id="ac_number" maxlength="18" name="ac_number" type="text"> </div>
	  </div>
	  </div> 
	  <div class="form_row">
	  <div class="form_row_right">
	  <label class="form_label" for="text1"><?php if(Lang::has(Session::get('mer_lang_file').'.Note')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Note')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Note')); ?> <?php endif; ?> </label>
	  <div class="info100"> <input class="form-control" value="<?php echo e(isset($getTotalAmount->note) ? $getTotalAmount->note : ''); ?>" id="note" maxlength="200" name="note"  type="text"> </div>
	  </div> 
	  </div>  
	  <div class="form_row">
	  	<div class="form_row_left">
	  <button class="form_button" type="submit" id="submit"><?php if(Lang::has(Session::get('mer_lang_file').'.UPDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATE')); ?> <?php endif; ?></button>
	  </div>
	  </div>

	  </div>
	  </form> 

	</div>
</div>
</div>
</div>
</div>
</div>
 
</div>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script type="text/javascript">

$("#store_bank_info").validate({
                  ignore: [],
                  rules: {
                  bank_name: {
                       required: true,
                      },

                       ac_number: {
                       required: true,
                      },
                        
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
           
            bank_name: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_BANK_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_BANK_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_BANK_NAME')); ?> <?php endif; ?>",
                      }, 
             
                 ac_number: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_ACCOUNT_NUMBER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_ACCOUNT_NUMBER')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_ACCOUNT_NUMBER')); ?> <?php endif; ?>",
                      }, 

                   
                },
                invalidHandler: function(e, validation){
                     },

                submitHandler: function(form) {
                    form.submit();
                }
            });
 


</script> 
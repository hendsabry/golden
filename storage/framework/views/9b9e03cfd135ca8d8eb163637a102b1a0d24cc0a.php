<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $hall_leftmenu =1; ?> 
 
<!--start merachant-->
<div class="merchant_vendor">
  <!--left panel-->
 <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php
	$hid         = request()->hid;
	$bid         = request()->bid;
	$opid        = request()->opid;
	 $cusid       = request()->cust_id;
	$oid         = request()->oid;
	$orderid     = request()->orderid;
	$shipaddress       = Helper::getgenralinfo($orderid);
	$getCustomer = Helper::getuserinfoOrderdeatils($cusid,$orderid); 
	$cityrecord  = Helper::getcity($shipaddress->shipping_city);
	$getorderedproduct=Helper::getorderedproductdetailbasicdetails($orderid,'hall','hall',$hid);

  $productinformation=Helper::getProduckInfo($getorderedproduct->product_id);


?>
  <!--left panel end-->
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Order_Detail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Order_Detail')); ?> <?php endif; ?><a href="<?php echo e(url('/')); ?>/hall-order?hid=<?php echo e($_REQUEST['hid']); ?>&bid=<?php echo e($_REQUEST['bid']); ?>" class="order-back-page"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BACK')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BACK')); ?> <?php endif; ?></a></h5>
        </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Customer_Name')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Customer_Name')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($getCustomer->payer_name) && $getCustomer->payer_name!=''){ echo $getCustomer->payer_name; } ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EMAIL')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?> <?php endif; ?></label>
                    <div class="info100" ><?php if(isset($getCustomer->payer_email) && $getCustomer->payer_email!=''){ echo $getCustomer->payer_email; } ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.BACK_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.BACK_PHONE')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($getCustomer->payer_phone) && $getCustomer->payer_phone!=''){ echo $getCustomer->payer_phone; }else{ echo 'N/A';} ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CITY')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){ echo $cityrecord->ci_name; }else{ echo 'N/A';} ?></div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDRESS')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($getCustomer->order_shipping_add) && $getCustomer->order_shipping_add!=''){echo $getCustomer->order_shipping_add; ?> <a target="_blank" href="https://www.google.com/maps/place/<?=$getCustomer->order_shipping_add?>"><img width="30" src="<?php echo e(url('')); ?>/themes/images/placemarker.png" /></a>
                      <?php }else{ echo 'N/A';} ?>
                    </div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ORDER_DATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')); ?> <?php endif; ?></label>
                    <div class="info100" > <?php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      ?> </div>
                  </div>
                </div>




                <!-- form_row -->
                <div class="form_row">
                 <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_TRANSACTION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION')); ?> <?php endif; ?> #</label>
                    <div class="info100"> 

                      <?php echo e($shipaddress->transaction_id); ?>

                    </div>
                  </div>
                  
                     <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDER_ID')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDER_ID')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($getorderedproduct->order_id) && $getorderedproduct->order_id!=''){echo $getorderedproduct->order_id;
                        
                    } ?></div>
                  </div>

                </div>

                <div class="form_row">
                 <div class="noneditbox-sub-heading"> <?php if(Lang::has(Session::get('mer_lang_file').'.PAYMENTS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PAYMENTS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PAYMENTS')); ?> <?php endif; ?></div>
 <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Total_Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Total_Price')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Total_Price')); ?> <?php endif; ?></label>
                    <!-- <div class="info100" >SAR <?php echo e(number_format((float)$getorderedproduct->total_price, 2, '.', '')); ?></div> -->

                    <div class="info100" >SAR <span id="calTotal"></span></div>


                  </div>
                    <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Insuranceamount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Insuranceamount')); ?> <?php endif; ?></label>
                    <div class="info100" > <?php echo e(number_format((float)$getorderedproduct->insurance_amount, 2, '.', '')); ?></div>
                  </div>
                </div>



                  <?php $INSU = $getorderedproduct->insurance_amount;
                    $couponamountapplied=0;
                  if(isset($getorderedproduct->coupon_code) && $getorderedproduct->coupon_code!=''){

                    $couponamountapplied=$getorderedproduct->coupon_code_amount;
                   ?>
                  <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COUPON_CODE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')); ?> <?php endif; ?></label>
                    <div class="info100"><?php if(isset($getorderedproduct->coupon_code) && $getorderedproduct->coupon_code!=''){echo $getorderedproduct->coupon_code;
                        
                    } ?></div>
                  </div>



                    <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_COUPON_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COUPON_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COUPON_AMOUNT')); ?> <?php endif; ?></label>
                    <div class="info100" >SAR <?php echo e(number_format((float)$getorderedproduct->coupon_code_amount, 2, '.', '')); ?></div>
                  </div>
                </div>

                  <?php } ?>
 

                <div class="form_row">

                    <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Hall_Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Hall_Price')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Hall_Price')); ?> <?php endif; ?></label>
                    <div class="info100">SAR <?php if(isset($productinformation->pro_price) && $productinformation->pro_price!=''){
                        if(isset($productinformation->pro_disprice) && $productinformation->pro_disprice>0){
                           echo number_format($productinformation->pro_disprice,2);
                           $pp=$productinformation->pro_disprice;
                      }else{

                         echo number_format($productinformation->pro_price,2);
                         $pp=$productinformation->pro_price;
                      }
                     
                        
                    } ?></div>
                  </div>

                   <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.REMAINING_AMOUNT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.REMAINING_AMOUNT')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.REMAINING_AMOUNT')); ?> <?php endif; ?></label>
                    <div class="info100" >
                    <?php
                     $totalprice                  = $getorderedproduct->total_price;
                     $paid_total_amount           = $getorderedproduct->paid_total_amount;
                     $remainingamount = $totalprice-$paid_total_amount; 
                     $paid_total_insurance_amount  = $getorderedproduct->insurance_amount;
                     $rp=$pp*75/100;
                     ?>

                     SAR <?php echo e(number_format((float)$rp, 2, '.', '')); ?></div>
                  </div>



                </div>

 
                <div class="form_row">

                    <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_AND_FOOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_AND_FOOD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_AND_FOOD')); ?> <?php endif; ?></label>
                    <div class="info100">SAR <span id="serviceprice"></span> </div>
                  </div>

                    <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.Credited')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Credited')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Credited')); ?> <?php endif; ?></label>
                    <div class="info100">SAR <span id="credited"></span> </div>
                  </div>
 
                </div> 

                <div class="form_row"> 

                        <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.VAT_CHARGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')); ?> <?php endif; ?></label>
                    <div class="info100">

                       <?php

                       $discouponamount=$paid_total_amount-$couponamountapplied;
                       $vtamou=$discouponamount-$paid_total_insurance_amount;
                           $vatamonu = Helper::calculatevat($getorderedproduct->order_id,$vtamou);
                        $tppaid=$discouponamount+$vatamonu;
                      ?>
                      
                    SAR <?php echo e(number_format((float)$vatamonu, 2, '.', '')); ?>


                </div>
                  </div>


                   <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Advance_Amount')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Advance_Amount')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Advance_Amount')); ?> <?php endif; ?></label>
                    <div class="info100" >SAR <?php echo e(number_format((float)$tppaid, 2, '.', '')); ?></div>
                  </div>
                  

                </div>

                 <div class="form_row">

                    <div class="noneditbox">
                    <label class="form_label">25% of <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Hall_Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Hall_Price')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Hall_Price')); ?> <?php endif; ?></label>
                    <div class="info100"> <?php 
                       $hp25=$pp*25/100;

                     ?>

                      SAR <?php echo e(number_format((float)$hp25, 2, '.', '')); ?>

                   </div>
                  </div>

                   


                </div>



                <!-- form_row -->
               <?php if(count($getoccasiondetail)>0){ ?>
                    
                 <div class="form_row">
                  <div class="noneditbox-sub-heading"> <?php if(Lang::has(Session::get('mer_lang_file').'.OCCASION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASION')); ?> <?php endif; ?></div>
                   <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONDATE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONDATE')); ?> <?php endif; ?></label>
                    <div class="info100">
                      <?php  $ocdate=strtotime($getoccasiondetail->occasion_date);
                      $ocdatef = date("d M Y",$ocdate);
                      ?>
                      <?php echo e($ocdatef); ?>   
<?php if($getoccasiondetail->occasion_id=='-1'): ?>
                        - 
                      <?php  $ocdate=strtotime($getoccasiondetail->end_date);
                      $ocdatef = date("d M Y",$ocdate);
                      ?>
                      <?php echo e($ocdatef); ?> 

<?php endif; ?>
                    </div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONBUDGET')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONBUDGET')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONBUDGET')); ?> <?php endif; ?></label>
                    <div class="info100" >SAR <?php echo e(number_format((float)$getoccasiondetail->budget, 2, '.', '')); ?></div>
                  </div>

                  <div class="noneditbox">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.total_member')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.total_member')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.total_member')); ?> <?php endif; ?></label>
                    <div class="info100"><?php echo e($getoccasiondetail->total_member); ?></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label"><?php if(Lang::has(Session::get('mer_lang_file').'.OCCASIONTYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.OCCASIONTYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.OCCASIONTYPE')); ?> <?php endif; ?></label>
                    <div class="info100">
                        <?php $getocc = Helper::business_occasion_type($getoccasiondetail->occasion_id);
                                                    if(Session::get('mer_lang_file')=='mer_ar_lang'){ $t='title_ar';}else{ $t='title';}
                         ?>
                       <?php echo e($getocc->$t); ?></div>
                  </div>
                </div>



             <?php } ?>
                <!-- form_row -->
                 <?php $AllServices = 0; ?>
                <!-- form_row -->
                <?php if(count($gethall_ordered_services)>0){ ?>
                <div class="paid-ser-area">
                  <div class="noneditbox-sub-heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Paid_Services')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Paid_Services')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Paid_Services')); ?> <?php endif; ?></div>
                  <div class="paid-ser-line">
                  
                    <?php $__currentLoopData = $gethall_ordered_services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                     if(Session::get('mer_lang_file')=='mer_en_lang'){ $servicesfield='option_value_title'; }else{ $servicesfield='option_value_title_ar';}
                     $AllServices = $AllServices + $order_services->option_value_price;
                     ?>
                    <div class="paid-ser-box"><?php echo e($order_services->$servicesfield); ?><span>SAR <?php echo e(number_format($order_services->option_value_price,2)); ?></span></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                  </div>
                </div>
                <?php } 

                  if(count($gethall_internal_food_services)>0){
                ?>

               </div>
              <!-- hall-od-top -->


                <div class="tailor_product">
                 <div class="style_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLDishes')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLDishes')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLDishes')); ?> <?php endif; ?></div>
                       
                            
           
           
                <?php $i = 1;  $basetotal = 0; $basetotal=0; ?>
                <?php $__currentLoopData = $gethall_internal_food_services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $internalfood): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php  if(Session::get('mer_lang_file')=='mer_en_lang'){ $internalfoodfield='internal_dish_name'; $internalfoodcontanierfield='container_title'; }else{ $internalfoodfield='internal_dish_name_ar'; $internalfoodcontanierfield='container_title_ar';} 
                $AllServices = $AllServices + $internalfood->price;
                $basetotal=$basetotal+$internalfood->price;
                ?>
                


                    <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                  <div class="style_area">
                   
                    <div class="style_box_type">
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_DISH_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DISH_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DISH_NAME')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <?php echo e($internalfood->$internalfoodfield); ?> </div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Dish_Image')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Dish_Image')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Dish_Image')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <img src="<?=$internalfood->dish_image?>" width="80" height="80"/></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container_Qty')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container_Qty')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container_Qty')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> <?php echo e($internalfood->quantity); ?></div>
                        </div>
                      </div>
            
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_CONTAINER_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_CONTAINER_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_CONTAINER_NAME')); ?> <?php endif; ?></div>
                          <div class="style_label_text"><?php echo e($internalfood->$internalfoodcontanierfield); ?></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container_Image')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container_Image')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> 
                            <div class="style_label_text"> <img src="<?=$internalfood->container_image?>" width="80" height="80"/></div>
              </div>
                        </div>

                        <div class="style_left">
                          <div class="style_label"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Price')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Price')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Price')); ?> <?php endif; ?></div>
                          <div class="style_label_text"> 
                            <div class="style_label_text"> <?php echo e(number_format($internalfood->price,2)); ?> </div>
              </div>
                        </div>
                      </div>

                      
                    </div>
                  </div>
                  
                </div> 




                  <?php $i++; ?>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                   
               <!-- <div class="merchant-order-total-area">
                <div class="merchant-order-total-line"> <?php echo e((Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')); ?>: &nbsp;
                  
                  <?php if(isset($shipaddress->order_taxAmt) && $shipaddress->order_taxAmt!='')
                  {
                  echo 'SAR '.number_format($shipaddress->order_taxAmt,2);
                  }
                  else
                  {
                  echo 'N/A';
                  }
                  ?> </div>
                 merchant-order-total-line 
                <div class="merchant-order-total-line"> <?php echo e((Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')); ?>: &nbsp;
                  <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$shipaddress->order_taxAmt),2);
				 } ?>
                </div>-->                <!-- merchant-order-total-line -->
              </div>
                <?php } ?>
                
              </div>
            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
<script type="text/javascript">
  
$(document).ready(function(){ 
  $('#serviceprice').html('<?php echo $AllServices; ?>');
    $('#calTotal').html('<?php echo ($AllServices + $pp+$vatamonu + $INSU); ?>');
$('#credited').html('<?php echo ($tppaid - $INSU); ?>');
})
 

</script>
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- HALL SECTION LEFT MENU START-->
<?php if(isset($hall_leftmenu) && $hall_leftmenu==1): ?>
<?php
$hallmenu = '';
$ids = request()->id;
 if(Request::is('list-hall')) {     $listhall = 'active';  } 
if(Request::is('sitemerchant_dashboard')) {     $sitemerchantdashboard = 'active'; } 
if(Request::is('hall-review-comments')) {     $hallreviewcomments = 'active'; $hallmenu = 'hall_riview';} 
if(Request::is('hall-order')) {     $hallorder = 'active'; } 
if(Request::is('list-hall-offer') || Request::is('add-hall-offer') || Request::is("edit-hall-offer/$ids")) {     $listhalloffer = 'active'; } 
if(Request::is('list-container-package')) {     $listcontainerpackage = 'active'; } 
if(Request::is('list-hall-menu') || Request::is('add-hall-menu') || Request::is("edit-hall-menu/$ids")) {     $listhallmenu = 'active'; }
if(Request::is('editmenucategroy')) {     $menulist = 'active'; } 
if(Request::is('categorymenu')) {     $menulist = 'active'; } 
if(Request::is('menulist')) {     $menulist = 'active'; } 
if(Request::is("edit-container-package/$ids")) {     $listcontainerpackage = 'active'; }
if(Request::is('add-container-package')) {     $listcontainerpackage = 'active'; } 
if(Request::is('hall-paid-services')) {     $hallpaidservices = 'active'; } 
if(Request::is('hall-free-services')) {     $hallfreeservices = 'active'; }
if(Request::is('hall-picture')) {     $hallpicture = 'active'; $hallmenu = 'menuright';} 
if(Request::is('hall-info')) {     $hallinfo = 'active'; } 
?>

<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span>

<?php     
	$putY = Session::get('menuss');
	if($mer_selected_lang_code !='en' && $putY==5) 
	{
	$hotalHall ='كبير'; 
	}
	elseif($mer_selected_lang_code !='ar' && $putY==5) 
	{
	$hotalHall ='Large';
	}
	elseif($mer_selected_lang_code !='en' && $putY==6) 
	{
	$hotalHall ='صغير';
	}
	elseif($mer_selected_lang_code !='ar' && $putY==6) 
	{
	$hotalHall ='Small';
	}
	else
	{
	$hotalHall ='';
	}
 ?>

	<span><?php echo e($hotalHall); ?> <?php if(Lang::has(Session::get('mer_lang_file').'.HOTELS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOTELS')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOTELS')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->

<?php endif; ?>
<!-- HALL SECTION LEFT MENU END-->



<?php if(isset($serviceleftmenu) && $serviceleftmenu==1): ?>
 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.HOTELS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOTELS')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOTELS')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>


<?php if(isset($wallet_inner_leftmenu) && $wallet_inner_leftmenu==1): ?>
  <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.MyWallet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MyWallet')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MyWallet')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- RECEPTION AND HOSPITALITY--> 

<?php if(isset($reception_hospitality_leftmenu) && $reception_hospitality_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$ShopId = request()->shopid;
$itemid =  request()->itemid;
$cid = request()->cid;
if(Request::is("reception-hospitality/$idd/$ShopId"))            {     $receptionhospitality = 'active';        } 
if(Request::is("reception-shop-picture/$idd/$ShopId"))           {     $receptionshoppicture = 'active';        } 
if(Request::is("reception-list-attributes/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId/$itemid"))        {     $receptionlistattributes = 'active';     } 
if(Request::is("reception-list-informations/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId/$cid"))      {     $receptionlistinformations = 'active';   } 
if(Request::is("reception-list-workers/$idd/$ShopId"))           {     $receptionlistworkers = 'active';        } 
if(Request::is("reception-list-packages/$idd/$ShopId") || Request::is("reception-add-packege/$idd/$ShopId")  || Request::is("reception-add-packege/$idd/$ShopId/$cid"))          {     $receptionlistpackages = 'active';       } 
if(Request::is("reception-list-orders/$idd/$ShopId"))            {     $receptionlistorders = 'active';         } 
if(Request::is("reception-list-reviewcomments/$idd/$ShopId"))    {     $receptionlistreviewcomments = 'active'; }  
if(Request::is("reception-shop-list/$idd/$ShopId"))          {     $receptionshoplist = 'active'; }   
?>
 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.reception_hospitality')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.reception_hospitality')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.reception_hospitality')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- Start Singer--> 

<?php if(isset($singer_leftmenu) && $singer_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;
if(Request::is("manage-singer/$idd/$hid"))            {     $managesinger = 'active';        } 
if(Request::is("singer-photo-video/$idd/$hid"))           {     $singerphotovideo = 'active';        } 
if(Request::is("singer-comments-review/$idd/$hid"))        {     $singercommentsreview = 'active';     } 
if(Request::is("singer-quoted-requested-list/$idd/$hid"))      {     $singerrequestedlist = 'active';   } 
 ?>
 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.singer')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.singer')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.singer')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>


<!-- End Singer--> 
 

<!-- Start Acoustics--> 

<?php if(isset($acoustics_leftmenu) && $acoustics_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;
$itmid = request()->itmid;
if(Request::is("acoustics/$idd/$hid")) {$managesinger = 'active';} 
if(Request::is("acoustics-photo-video/$idd/$hid")) {$singerphotovideo = 'active';} 
if(Request::is("acoustics-comments-review/$idd/$hid")) {$singercommentsreview = 'active';} 
if(Request::is("acoustics-quoted-requested-list/$idd/$hid") || Request::is("acoustics-quoted-requested-view/$idd/$hid/$itmid")) {$singerrequestedlist = 'active';} 


 ?>
 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.acoustics')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.acoustics')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.acoustics')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>


<!-- End Acoustics--> 


<!-- Start Popular Band--> 

<?php if(isset($popular_band_leftmenu) && $popular_band_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;

if(Request::is("popular-band-info/$idd/$hid"))                      {    $popularbandinfo = 'active';        } 
if(Request::is("popular-band-photo-video/$idd/$hid"))               {    $popularbandphotovideo = 'active';        } 
if(Request::is("popular-band-comments-review/$idd/$hid"))           {   $popularbandcommentsreview = 'active';     } 
if(Request::is("popular-band-quoted-requested-list/$idd/$hid"))     {   $popularbandquotedrequestedlist = 'active';   } 

?>
 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.popular_band')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.popular_band')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.popular_band')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>


<!-- End Singer--> 

 
<!-- Buffet Menu Start ajit--> 

<?php if(isset($buffet_leftmenu) && $buffet_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$item = request()->itemid;
$autoid = request()->autoid;
if(Request::is("buffet-info/$idd/$sid") ||  Request::is("buffet-info/$idd/$sid/$item"))            {     $buffetinfo      = 'active';  }   
if(Request::is("buffet-picture/$idd/$sid/$item"))            {     $buffetpicture         = 'active';  } 
if(Request::is("buffet-menu-list/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item/$autoid"))      {     $buffetmenlist         = 'active';  }

 
if(Request::is("buffet-orders/$idd/$sid/$item"))  {     $buffetorder         = 'active';  } 
if(Request::is("buffet-list-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item/$autoid"))            {     $buffetmenlistcontainer         = 'active';  }
 
if(Request::is("buffet-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item/$autoid"))            {     $buffetdish        = 'active';  }
if(Request::is("buffet-reviews-and-comments/$idd/$sid/$item"))            {     $buffetreviewcomment        = 'active';  }      

?>

 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.buffet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.buffet')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.buffet')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->




<?php endif; ?>
<!-- Buffet Menu End ajit--> 




<?php if(isset($listmanagerblade) && $listmanagerblade==1): ?>
 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MANAGER')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MANAGER')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->

<?php endif; ?>

	<?php if(isset($hotel_leftmenu) && $hotel_leftmenu==1): ?>
	<?php 
	$id = request()->id;

	$putY = Session::get('menuss');  

	if($mer_selected_lang_code !='en' && $putY==5) 
	{
	$hotalHall ='كبير'; 
	}
	elseif($mer_selected_lang_code !='ar' && $putY==5) 
	{
	$hotalHall ='Large';
	}
	elseif($mer_selected_lang_code !='en' && $putY==6) 
	{
	$hotalHall ='صغير';
	}
	elseif($mer_selected_lang_code !='ar' && $putY==6) 
	{
	$hotalHall ='Small';
	}
	else
	{
	$hotalHall ='';
	}

	?>

 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php echo e($hotalHall); ?> <?php if(Lang::has(Session::get('mer_lang_file').'.HOTELS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOTELS')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOTELS')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- Buffet Branch 3 links--> 

<?php if(isset($buffeth_leftmenu) && $buffeth_leftmenu==1): ?>  
<?php 
$ids = request()->id; 
$sid = request()->sid; 
?>
 <div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.buffet')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.buffet')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.buffet')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>


<!-- Beauty Branch 3 links--> 

<?php if(isset($beauty_leftmenu) && $beauty_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
 
if(Request::is("beauty-shop/$id") || Request::is("beauty-add-shop/$id") || Request::is("beauty-add-shop/$id/$hid")) {$beautyshop = 'active';} 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.beauty')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.beauty')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.beauty')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>


<!-- Start For Beauty Big Menu -->

<?php if(isset($beautybig_leftmenu) && $beautybig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid  = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
$workerid =  request()->workerid;
 
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.beauty')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.beauty')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.beauty')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>
<!-- End For Beauty Big Menu -->



<?php if(isset($oudandperfumes_leftmenu) && $oudandperfumes_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("oudandperfumes-shop/$id") || Request::is("oudandperfumes-add-shop/$id")  || Request::is("oudandperfumes-add-shop/$id/$hid")) { $oudandperfumesshop = 'active';}  
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.oudandperfumes')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.oudandperfumes')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.oudandperfumes')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- Start For oudandperfumes Big Menu  -->
<?php if(isset($oudandperfumesbig_leftmenu) && $oudandperfumesbig_leftmenu==1): ?> 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.oudandperfumes')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.oudandperfumes')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.oudandperfumes')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For oudandperfumes Big Menu -->

<?php if(isset($goldandjewelry_leftmenu) && $goldandjewelry_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("goldandjewelry-shop/$id") || Request::is("goldandjewelry-add-shop/$id")  || Request::is("goldandjewelry-add-shop/$id/$hid")) { $goldandjewelryshop = 'active';}  
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.goldandjewelry')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.goldandjewelry')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.goldandjewelry')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- Start For goldandjewelry Big Menu  -->
<?php if(isset($goldandjewelrybig_leftmenu) && $goldandjewelrybig_leftmenu==1): ?> 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
 

 ?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.goldandjewelry')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.goldandjewelry')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.goldandjewelry')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For goldandjewelry Big Menu -->

<!-- Beauty Branch 3 links--> 

<?php if(isset($spa_leftmenu) && $spa_leftmenu==1): ?>
<?php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("spa-shop/$id") || Request::is("spa-add-shop/$id")  || Request::is("spa-add-shop/$id/$hid")) {$spashop = 'active';}  
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.spa')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.spa')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.spa')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>



<!-- Start For Spa Big Menu  -->

<?php if(isset($spabig_leftmenu) && $spabig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.spa')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.spa')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.spa')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For spa Big Menu -->
 
<!-- Start For Makup Artist Big Menu -->

<?php if(isset($MakupArtistbig_leftmenu) && $MakupArtistbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid= request()->autoid;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.makeup_artist')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.makeup_artist')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.makeup_artist')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Makup Artist Big Menu -->
 
<!-- Start tailor Big Menu -->

<?php if(isset($Tailorbig_leftmenu) && $Tailorbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
 
?> 
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.tailor')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.tailor')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.tailor')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For tailor Big Menu -->



<!-- Start For Photography Big Menu -->

<?php if(isset($photographybig_leftmenu) && $photographybig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$itemid = request()->sid;
$autoid = request()->autoid;
 

?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.photography')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.photography')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.photography')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Photography Big Menu -->

<!-- Start For abaya Big Menu -->

<?php if(isset($Abayabig_leftmenu) && $Abayabig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.abaya')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.abaya')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.abaya')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For abaya Big Menu -->
 
<!-- Start For dresses Big Menu -->

<?php if(isset($dressesbig_leftmenu) && $dressesbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
 
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.dresses')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.dresses')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.dresses')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For dresses Big Menu -->

<!-- Start For Cosha Big Menu -->

<?php if(isset($Coshabig_leftmenu) && $Coshabig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.cosha')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.cosha')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.cosha')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Cosha Big Menu -->

<!-- Start For Special Event Big Menu -->

<?php if(isset($Eventbig_leftmenu) && $Eventbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$shopid = request()->shopid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
 

?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Special_Events')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Special_Events')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Special_Events')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Special Event Big Menu -->

 

<!-- Start For Car rental Menu -->

<?php if(isset($Carrental_leftmenu) && $Carrental_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.car-rental')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.car-rental')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.car-rental')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>




<!-- Start For Car rental Menu -->

<?php if(isset($Travel_leftmenu) && $Travel_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;

 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Travel_Agency')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Travel_Agency')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Travel_Agency')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Special Event Big Menu -->
 
<!-- Start For Makup Artist Big Menu -->

<?php if(isset($Rosestbig_leftmenu) && $Rosestbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
 

?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Roses')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Roses')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Roses')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Makup Artist Big Menu -->




<!-- Start For Men's Barber Big Menu -->

<?php if(isset($mensbig_leftmenu) && $mensbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;

 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Men_Barber_Saloon')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Men_Barber_Saloon')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Men_Barber_Saloon')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- Start For Men's Barber Big Menu End -->





<!-- Sratr For Dessert Front Menu -->

<?php if(isset($dessert_leftmenu) && $dessert_leftmenu==1): ?>
<?php 

$id= request()->id;
$services_id= request()->services_id;
$shopid= request()->shopid;
 
 
if(Request::is("dessert-shop/$id") || Request::is("dessert-add-shop/$services_id"))    {     $buffetresturant = 'active'; } 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Dessert')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Dessert')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Dessert')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>
<!-- End For Dessert Front Menu -->

<!-- Sratr For Dessert Inner Menu -->
<?php if(isset($dessert_inner_leftmenu) && $dessert_inner_leftmenu==1): ?>
 
<?php
$idd = request()->id;

$services_id = request()->services_id;
$shopid = request()->shopid;
$itemid = request()->itemid;
 $autoid = request()->autoid;
$offer_id = request()->offer_id;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Dessert')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Dessert')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Dessert')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>
<!-- End For Dessert Inner Menu -->


<!-- Sratr For Dates Inner Menu -->
<?php if(isset($dates_inner_leftmenu) && $dates_inner_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$offerid  = request()->offerid;
 
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Dates')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Dates')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Dates')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>
<!-- End For Dates Inner Menu -->



<!-- Laser Cosmetic 3 links--> 

<?php if(isset($laser_cosmetic_leftmenu) && $laser_cosmetic_leftmenu==1): ?>
<?php if(Request::is('/laser-cosmetic-shop/4')) {$lasershop = 'active';} 
if(Request::is('laser-cosmetic-shop-branch/4')) {$laserbranch = 'active';} 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Cosmetic_and_Laser')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Cosmetic_and_Laser')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Cosmetic_and_Laser')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>



<!-- Start For Laser Cosmetic Big Menu -->

<?php if(isset($laserbig_leftmenu) && $laserbig_leftmenu==1): ?>
 
<?php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$incid = request()->incid;
 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.Cosmetic_and_Laser')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Cosmetic_and_Laser')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.Cosmetic_and_Laser')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Laser Cosmetic Big Menu -->



<!-- Skin & Teeth 3 links--> 

<?php if(isset($skin_teeth_leftmenu) && $skin_teeth_leftmenu==1): ?>
<?php if(Request::is('/skin-teeth-shop/4')) {$skinshop = 'active';} 
if(Request::is('skin-teeth-shop-branch/4')) {$skinbranch = 'active';} 
?>
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.SKIN_AND_TEETH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SKIN_AND_TEETH')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.SKIN_AND_TEETH')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>



<!-- Start For Skin & Teeth Big Menu -->

<?php if(isset($SkinTeethbig_leftmenu) && $SkinTeethbig_leftmenu==1): ?>
   
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span><?php if(Lang::has(Session::get('mer_lang_file').'.SKIN_AND_TEETH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SKIN_AND_TEETH')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.SKIN_AND_TEETH')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
<?php endif; ?>

<!-- End For Skin & Teeth Big Menu -->
 

<!-- Start For Makeup Menu -->

<?php if(isset($makeup_leftmenu) && $makeup_leftmenu==1): ?>
 
<div class="breadcrumb"><a href="<?php echo e(url('/vendor-dashboard')); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.HOME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HOME')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.HOME')); ?> <?php endif; ?></a> <span class="breadcrumb-divder">&gt;</span><span> <?php if(Lang::has(Session::get('mer_lang_file').'.MAKEUP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MAKEUP')); ?> <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MAKEUP')); ?> <?php endif; ?></span></div> <!-- breadcrumb -->
 

 
<?php endif; ?>
<!-- End For Makeup Menu -->
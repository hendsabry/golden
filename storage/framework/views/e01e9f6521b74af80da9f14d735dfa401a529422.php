 <?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
<?php $hall_leftmenu =1; ?> 

<!-- MAIN WRAPPER -->
<div class="merchant_vendor">
  <div class="profile_box"> 
    
    <!--PAGE CONTENT -->
    <div class="profile_area">
      <div class="inner">
        <div class="row">
          <div class="col-lg-12">
            <header>
              <h5 class="global_head"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_MERCHANT_ACCOUNT')); ?></h5>
            </header>
            <?php if($errors->any()): ?> <br>
            <ul style="color:red;">
              <div class="alert alert-danger alert-dismissable"><?php echo implode('', $errors->all(':message<br>
                ')); ?>

                <?php echo e(Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ])); ?> </div>
            </ul>
            <?php endif; ?>
            <?php if(Session::has('mail_exist')): ?>
            <div class="alert alert-warning alert-dismissable" ><?php echo Session::get('mail_exist'); ?>

              <?php echo e(Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ])); ?> </div>
            <?php endif; ?>
            <?php if(Session::has('result')): ?>
            <div class="alert alert-warning alert-dismissable alert_suc" ><?php echo Session::get('result'); ?>

              <?php echo e(Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ])); ?> </div>
            <?php endif; ?>
            <div class="box commonbox"> <?php $__currentLoopData = $merchant_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fetch_mer_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php echo Form::open(array('url'=>'edit_merchant_account_submit','class'=>'form-horizontal', 'id'=>'editaccount','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')); ?>

              <div class="global_area">
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_FIRST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FIRST_NAME') : trans($MER_OUR_LANGUAGE.'.MER_FIRST_NAME')); ?></label>
                    <div class="info100"> <?php echo e(Form::hidden('mer_id',$fetch_mer_details->mer_id)); ?>

                      <?php echo e(Form::text('first_name',$fetch_mer_details->mer_fname,['class' => 'form-control','id' => 'first_name','maxlength' => 40])); ?> </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_LAST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LAST_NAME') : trans($MER_OUR_LANGUAGE.'.MER_LAST_NAME')); ?></label>
                    <div class="info100"> <?php echo e(Form::text('last_name',$fetch_mer_details->mer_lname,['class' => 'form-control','id' => 'last_name','maxlength' => 40])); ?> </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL')); ?></label>
                    <div class="info100"> <?php echo e(Form::text('email_id',$fetch_mer_details->mer_email,['class' => 'form-control','id' => 'email_id','maxlength' => 50])); ?> </div>
                  </div>
				  <?php if(isset($fetch_mer_details->mer_phone) && $fetch_mer_details->mer_phone!=''){$rec_array = explode('-',$fetch_mer_details->mer_phone);} ?>
                  <div class="form_row_right country_row">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PHONE') : trans($MER_OUR_LANGUAGE.'.MER_PHONE')); ?></label>
                    <div class="info100 meredit-ac">
					 <!--<input type="text" name="country_code" id="country_code" maxlength="5" value="<?php if(isset($rec_array[0]) && $rec_array[0]!=''){ echo ltrim($rec_array[0],'+'); }?>" onkeypress='return isNumberKey(event);' class="t-box countrycode"/>-->


             <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode" required>      
      <?php $__currentLoopData = $getCountry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ccode): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($Ccode->country_code); ?>" <?php if(isset($rec_array[1]) && $rec_array[1]==$Ccode->country_code){ echo 'selected'; } ?>>+<?php echo e($Ccode->country_code); ?></option>        
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>

					  <input class="form-control small-sel" id="phone_no" maxlength="12" name="phone_no" type="text" value="<?php if(isset($rec_array[1]) && $rec_array[1]!=''){ echo ltrim($rec_array[1],'+'); }?>">

				    </div>
					<span for="country_code" generated="true" class="error"></span>
					<span for="phone_no" generated="true" class="error"></span>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY')); ?></label>
                    <div class="info100">
                      <select class="form-control" name="select_mer_city" id="select_mer_city" >
                        <option value=""><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?> </option>
                      </select>
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY')!= '')   ? trans(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_COUNTRY')); ?></label>
                    <div class="info100">
                      <select class="form-control" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" >
                        <option value=""> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT'): trans($MER_OUR_LANGUAGE.'.MER_SELECT')); ?> </option>
                        
                          <?php $__currentLoopData = $country_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country_fetch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          				 
                        <option value="<?php echo e($country_fetch->co_id); ?>"  <?php if($fetch_mer_details->mer_co_id == $country_fetch->co_id): ?> <?php echo e('selected'); ?> <?php endif; ?> ><?php echo e($country_fetch->co_name); ?></option>
                        
           				   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       					 
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1')); ?></label>
                    <div class="info100"> <?php echo e(Form::text('addreess_one',$fetch_mer_details->mer_address1,['class' => 'form-control','id' => 'addreess_one','maxlength' => 200])); ?> </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS2') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS2')); ?></label>
                    <div class="info100"> <?php echo e(Form::text('address_two',$fetch_mer_details->mer_address2,['class' => 'form-control','id' => 'address_two','maxlength' => 200])); ?> </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.ZIP_CODE')!= '') ?  trans(Session::get('mer_lang_file').'.ZIP_CODE') : trans($MER_OUR_LANGUAGE.'.ZIP_CODE')); ?></label>
                    <div class="info100"> <?php echo e(Form::text('zipcode',$fetch_mer_details->zipcode,['class' => 'form-control zip','id' => 'zipcode','maxlength' => 7])); ?> </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.USERIMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.USERIMAGE') : trans($MER_OUR_LANGUAGE.'.USERIMAGE')); ?></label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo1">
                        <div class="file-btn-area">
                          <div id="file_value2" class="file-value"></div>
                          <div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div>
                        </div>
                        </label>
                        <input id="company_logo1" name="stor_img" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
                      </div>
                      <!-- input-file-area --> 
                      
                      <?php if($fetch_mer_details->stor_img!=''): ?> <img src="<?php echo e($fetch_mer_details->stor_img); ?>" width="100" height="100" class="profile_img"> <?php else: ?>
                     
                      <?php endif; ?> </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.CertificateChamber')!= '') ?  trans(Session::get('mer_lang_file').'.CertificateChamber') : trans($MER_OUR_LANGUAGE.'.CertificateChamber')); ?></label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"><?php if(Lang::has(Session::get('lang_file').'.UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.UPLOAD')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.UPLOAD')); ?> <?php endif; ?></div>
                        </div>
                        </label>
                        <input class="info-file"  accept="pdf" type="file" id="company_logo" name="company_logo">
                      </div>
 
                    </div>
                    <?php if($fetch_mer_details->certificates !=''): ?>
                    <input type="hidden" name="certificates" value="<?php echo e($fetch_mer_details->certificates); ?>">
                    <div class="profile-pdf-line"> <a href="<?php echo e($fetch_mer_details->certificates); ?>" target="_blank"> <img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> </a>
                      <input type="hidden" name="certificatesname" value="<?php echo e(isset($fetch_mer_details->certificates_name) ? $fetch_mer_details->certificates_name : ''); ?>">
                      <?php echo e(isset($fetch_mer_details->certificates_name) ? $fetch_mer_details->certificates_name : ''); ?> </div>
                    <?php endif; ?> </div>
                     <div class="form_row_right">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.Shop_link_in_maroof')!= '') ?  trans(Session::get('mer_lang_file').'.Shop_link_in_maroof') : trans($MER_OUR_LANGUAGE.'.Shop_link_in_maroof')); ?></label>
                    <div class="info100"> <?php echo e(Form::text('shop_link_in_maroof',$fetch_mer_details->shop_link_in_maroof,['class' => 'form-control','id' => 'shop_link_in_maroof','maxlength' => 255])); ?> </div>
                  </div>
                </div>

<div class="form_row">
                <div class="form_row_right">
                    <label class="form_label" for="text1"><?php echo e((Lang::has(Session::get('mer_lang_file').'.sales_rep_code')!= '') ?  trans(Session::get('mer_lang_file').'.sales_rep_code') : trans($MER_OUR_LANGUAGE.'.sales_rep_code')); ?>

 </label>
                    <div class="info100"> <?php echo e(isset($fetch_mer_details->sales_rep_code) ? $fetch_mer_details->sales_rep_code : 'N/A'); ?> </div>
                  </div>
            </div>  
                  <div class="form_row">
                    <button class="form_button" type="submit" id="submit" ><?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE'): trans($MER_OUR_LANGUAGE.'.MER_UPDATE')); ?></button>
                  </div>
             
              </div>
              <?php echo e(Form::close()); ?> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--END PAGE CONTENT --> 

<!--END MAIN WRAPPER --> 

<!-- GLOBAL SCRIPTS --> 

<script>
	$( document ).ready(function() {
	
	$('#submit').click(function() {
    var file		 	 = $('#file');
	var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      	if(file.val() == "")
 		{
 		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else
		{
		file.css('border', ''); 				
		}
	});
	
	 var passData = 'city_id_ajax=<?php echo $fetch_mer_details->mer_ci_id; ?>&country_id_ajax=<?php echo $fetch_mer_details->mer_co_id; ?>' ;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_edit_merchant'); ?>',
				  success: function(responseText){  
				  //alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
			
	});
	</script> 
<script>
	

	
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_merchant'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
	}
	</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#editaccount").validate({
                  ignore: [],
                  rules: {
                  first_name: {
                       required: true,
                      },

                       last_name: {
                       required: true,
                      },

                       email_id: {
                       required: true,
                      },

                      select_mer_country: {
                       required: true,
                      },
                       select_mer_city: {
                       required: true,
                      },
					   country_code: {
                       required: true,
                      },
                       phone_no: {
                       required: true,
                      },
                      addreess_one: {
                       required: true,
                      },
                      zipcode: {
                       required: true,
                       number: true
                      },

                      stor_img: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },

                      company_logo: {
                           required:false,
                           accept:"pdf",
                      },

                      
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             first_name: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_FIRSTNAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_FIRSTNAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_FIRSTNAME')); ?> <?php endif; ?>",
                      },  

                 last_name: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_LASTNAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_LASTNAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_LASTNAME')); ?> <?php endif; ?>",
                      },  

                         email_id: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EMAILADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_EMAILADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EMAILADDRESS')); ?> <?php endif; ?>",
                      },   

                          select_mer_country: {
                required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_COUNTRY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_COUNTRY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_COUNTRY')); ?> <?php endif; ?>",
                      },   


                       select_mer_city: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?> ",
                      },
                        country_code: {
                 required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_COUNTRY_CODE_MSG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COUNTRY_CODE_MSG')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COUNTRY_CODE_MSG')); ?> <?php endif; ?> ",
                      },
                       phone_no: {
                 required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PHONE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_PHONE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PHONE')); ?> <?php endif; ?> ",
                      },
                            addreess_one: {
                 required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ADDRESS')); ?> <?php endif; ?> ",
                      },
                            zipcode: {
           required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ZIP')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_ZIP')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ZIP')); ?> <?php endif; ?> ",
                      },
                        stor_img: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      }, 

            company_logo: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_FILE')); ?> <?php endif; ?>",
                                    


                                    

                      },               



                                          
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                     

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
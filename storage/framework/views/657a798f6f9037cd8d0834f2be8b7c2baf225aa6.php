<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/datepicker3.css"/>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
<?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
<?php endif; ?>
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
<link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
<?php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
<!--END GLOBAL STYLES -->
<!-- PAGE LEVEL STYLES -->
<link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
<link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="<?php echo e(url('')); ?> /public/assets/js/chart/jquery.min.js"></script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/tinymce/tinymce.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="padTop53 " >
    <!-- MAIN WRAPPER -->
    <div id="wrap" >
        <!-- HEADER SECTION -->
        <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
        <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
            <div class="right_col" role="main">
                <?php if(Session::has('message')): ?>
                <div class="alert alert-danger no-border">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo $error; ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <?php endif; ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="add-manage-details">
                        <ul class="nav nav-tabs">
                            <li id="en" class="active"><a data-toggle="tab" href="#home">English</a></li>
                            <li id="ar"><a data-toggle="tab" href="#menu1">عربى</a></li>
                        </ul>
                        <form action="/admin/category_management/sub_categroy/main_categroy/edit/<?php echo e($static_content->mc_id); ?>" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left sigle_form_left" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="tab-content">
                                <div id="home" class="tab-pane bordr-bg fade in active english-sec">
                                <div class="paddmar-block"> 
                                    <div class="top_back_btn"> 
                                        <label class="title-block minus-mar-left" for="page-name">
                                            <h3 class="margpadd">Edit Sub Category</h3>
                                            <div  id="error"></div>
                                        </label>
                                        <div class="back-btn-area">
                                            <a href="/admin/category_management/main_category/<?php echo e($static_content->parent_id); ?>" class="back-btn">Back</a>
                                        </div>
                                    </div>
                                    <div class="form-group clearboth">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name">Parent Category </label>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <select name="parent_id" id="parent_id" class="form-control">
                                                <option value="0">Select Parent Category</option>
                                                <?php $__currentLoopData = $static_content_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoryManagements): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($categoryManagements->mc_id); ?>"  <?php if( $categoryManagements->mc_id == $static_content->parent_id): ?> selected <?php endif; ?>><?php echo e($categoryManagements->mc_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name">Category Name <span class="required">*</span>
                                        </label>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="mc_name" name="mc_name" class="form-control col-md-7 col-xs-12" value="<?php echo e($static_content->mc_name); ?>" maxlength="20">
                                        </div>
                                    </div>
                                       <?php if($static_content->mc_id == 16): ?>
                                    <div class="form-group">
                                    <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12">Image</label>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <?php if($static_content->mc_img != ''): ?>
                                    <div class="upload_image"><img src="<?php echo e(url('/')); ?><?php echo e($static_content->mc_img); ?>"></div>
                                    <?php endif; ?>
                                    <div class="custom-file">
                                    <label for="image" class="custom-file-upload">
                                    <div class="browse-text"><i class="fa fa-cloud-upload"></i> Browse</div>
                                    <span class="commit">No file selected</span>
                                    </label>
                                    <input type="file" name="mc_img" id="image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                                    </div>

                                    </div>
                                    </div>  
                                    <?php endif; ?>     
                                    <!-- <div class="form-group">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12">Description<span class="required"></span>
                                        </label>
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3"
                                            name="mc_discription" id="my_form1" placeholder='Description' maxlength="200"><?php echo e($static_content->mc_discription); ?></textarea>
                                        </div>
                                    </div> -->
                                     <div class="form-group">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12">Budget<span class="required"></span>
                                        </label>
                                       
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="checkbox" id="myCheck" name="within_budget" onclick="myFunction()" value="<?php echo e($static_content->within_budget); ?>" <?php if($static_content->within_budget=='1'): ?> checked <?php endif; ?>>
                                            <div class="budget-area">
                                                  <input type="text" id="text" name="budget" style="display:none" value="<?php echo e($static_content->budget); ?>"><i id="percentage" class="fa fa-percent"></i>
                                                <p id="budget" style='color:red'></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12 hidden-xs"></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                                            <button type="button" onClick="validateForm()" class="btn btn-success"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div id="menu1" class="tab-pane bordr-bg fade arabic_sec">
                                <div class="paddmar-block"> 
                                    <div class="top_back_btn">
                                       <div class="back-btn-area">
                                            <a href="/admin/category_management/main_category/<?php echo e($static_content->parent_id); ?>" class="back-btn">الى الخلف</a>
                                        </div>
                                    <div class="title-block minus-mar-left">       
                                        <h3 class="AR-paddmarg">تحرير الفئة</h3>
                                    </div>
                                 </div>
                                    <div class="form-group clearboth">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name">القسم الرئيسي</label>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <select name="parent_id" id="parent_id" class="form-control">
                                                <option value="0">اختر الفئة الرئيسية</option>
                                                <?php $__currentLoopData = $static_content_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoryManagements): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($categoryManagements->mc_id); ?>" <?php if( $categoryManagements->mc_id == $static_content->parent_id): ?> selected <?php endif; ?>><?php echo e($categoryManagements->mc_name_ar); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name"><span class="required">*</span>اسم التصنيف</label>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="mc_name_ar" name="mc_name_ar" required class="form-control col-md-7 col-xs-12" value="<?php echo e($static_content->mc_name_ar); ?>" maxlength="20">
                                        </div>
                                    </div>
                                     <?php if($static_content->mc_id == 16): ?>
                                    <div class="form-group">
                                    <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12">صورة</label>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <?php if($static_content->mc_img_ar != ''): ?>
                                    <div class="upload_image"><img src="<?php echo e(url('/')); ?><?php echo e($static_content->mc_img_ar); ?>"></div>
                                    <?php endif; ?>
                                    <div class="custom-file">
                                    <label for="image" class="custom-file-upload">
                                    <div class="browse-text"><i class="fa fa-cloud-upload"></i>  تصفح</div>
                                    <span class="commit">لم يتم اختيار اي ملف</span>
                                    </label>
                                    <input type="file" name="mc_img_ar" id="image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                                    </div>

                                    </div>
                                    </div>
                                    <?php endif; ?> 
                                    <!-- <div class="form-group">
                                        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12"><span class="required"></span>وصف</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3"
                                            name="mc_discription_ar" id="my_form2" placeholder='Description' maxlength="200"><?php echo e($static_content->mc_discription_ar); ?></textarea>
                                        </div>
                                    </div>  -->      
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                    <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12 hidden-xs"></label>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <button class="btn btn-primary" onClick="firstpage()" type="button"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL')); ?></button>
                                    <button class="btn btn-primary" id="reset" onClick="firstpage()" type="reset"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET')); ?></button>
                                    <button type="submit" class="btn btn-success"><?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT')); ?></button>
                                        </div>
                                    </div>
                                </div>
                                 </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->

    <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/bootstrap-datepicker.js"></script> 
    <script src="https://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script type="text/javascript"> 
        tinymce.init({
            selector: 'textarea',
            plugins: ["image"],
            image_advtab: true,
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code image | forecolor backcolor emoticons",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
});
</script>
<script>
$(function() {
$('input[name=budget]').on('input', function() {
this.value = this.value.match(/\d{0,3}(\.\d{0,2})?/)[0];
});
});
</script>
<script>
function myFunction() {
    var checkBox = document.getElementById("myCheck");
    var text = document.getElementById("text");
    if (checkBox.checked == true){
        text.style.display = "block";
         $('#percentage').removeClass('hidden');
    } else {
       text.style.display = "none";
       $('#percentage').addClass('hidden');
    }
}
window.onload = function() {
  var checkBox = document.getElementById("myCheck");
    var text = document.getElementById("text");
    if (checkBox.checked == true){
        text.style.display = "block";
         $('#percentage').removeClass('hidden');
    } else {
       text.style.display = "none";
       $('#percentage').addClass('hidden');
    }
};
</script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
    </script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script> 
    <script>
        $( function() {
            $( "#demo" ).datepicker({ format: 'dd-mm-yyyy'});
        } );
    </script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/admin/categoryManagementlist.js"></script>
    <script type="text/javascript">
        function validateForm()
        {
            var a = document.getElementsByName("mc_name")[0].value;
             var b = document.getElementsByName("budget")[0].value;
            var c = +b + +<?php echo("$data"); ?>; 
            var d = +100 - +<?php echo("$data"); ?>;
           // alert(d);    
            if (c >= 100) 
                 {
                document.getElementById("budget").innerHTML="Please Fill Less than 100 in budget";
                return false;
            } else{
                 $( "#home" ).removeClass("active");
                $( "#home" ).addClass("fade");
                $( "#menu1" ).addClass("active");
                $( "#menu1" ).removeClass("fade");
                $( "#en" ).removeClass("active");
                $( "#ar" ).addClass("active");
                $( "#error" ).remove();
            } 
            if (a==null || a=="")
            {
                $( "#error" ).append( "<p style='color:red'>Please Fill All Required Field</p>" );; 
                return false;
            }else
            {
                $( "#home" ).removeClass("active");
                $( "#home" ).addClass("fade");
                $( "#menu1" ).addClass("active");
                $( "#menu1" ).removeClass("fade");
                $( "#en" ).removeClass("active");
                $( "#ar" ).addClass("active");
                $( "#error" ).remove();
            }
        }
        function firstpage()
        { 
            $( "#home" ).removeClass("fade");
            $( "#home" ).addClass("active");
            $( "#menu1" ).addClass("fade");
            $( "#menu1" ).removeClass("active");
            $( "#ar" ).removeClass("active");
            $( "#en" ).addClass("active");
        }
</script>
<script type="text/javascript">
  jQuery("input#image").change(function () {
  var imag =  jQuery(this).val();
  var output = imag.split("\\").pop();
  $('.commit').text(output);
});
</script>
<script type="text/javascript">
  $("#reset").click(function(e) {
    $('.commit').text('<?php echo( (Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE')); ?>');
});
</script>
</body>
<!-- END BODY -->
</html>
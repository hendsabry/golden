<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="merchant_vendor" >
  <div class="inner">

      <!-- filter area start -->
      <?php if(Session::get('LoginType') !='4'): ?>
        <div class="service_heading_area no-margin">
          <h1 class="global_head" > <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICES')); ?>  
            <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICES')); ?> <?php endif; ?> </h1>
          <div class="add"><a href="<?php echo e(url('/addservices')); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDMORE')); ?>  
            <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDMORE')); ?> <?php endif; ?></a> </div>
          
        </div>
        <?php endif; ?>
        <!-- service_heading_area --> 
        
        <?php echo Form::open(array('url'=>'sitemerchant_dashboard','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?>

        <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
        
        <?php echo Form::close(); ?> </div>
           <?php echo Form::open(array('url'=>'sitemerchant_dashboard','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?>

      <div class="filter_area">
          <div class="filter_left">
            <div class="search_filter"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></div>
            <div class="search-box-field mems ">
              <select name="subscription" id="subscription">
                <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_MEMBERSHIP_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_MEMBERSHIP_TYPE')); ?>  
                <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_MEMBERSHIP_TYPE')); ?> <?php endif; ?> </option>
                <option value="0" <?php if($subscription=='0'): ?> selected="selected" <?php endif; ?> > <?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMISSION')); ?>  
                <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMISSION')); ?> <?php endif; ?></option>
                <option value="1" <?php if($subscription=='1'): ?> selected="selected" <?php endif; ?> > <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SUBSCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SUBSCRIPTION')); ?>  
                <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SUBSCRIPTION')); ?> <?php endif; ?></option>
              </select>
              <input name="" type="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''): ?><?php echo e(trans(Session::get('mer_lang_file').'.MER_APPLY')); ?><?php else: ?><?php echo e(trans($MER_OUR_LANGUAGE.'.MER_APPLY')); ?><?php endif; ?>" class="applu_bts" />
            </div>
          </div>
          <div class="search_box">
            <div class="search_filter">&nbsp;</div>
            <div class="filter_right">
              <input name="search" type="text" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SEARCH')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?> <?php endif; ?>" value="<?php echo e($search); ?>" />
              <input type="button" class="icon_sch" id="submitdata" />
            </div>
          </div>
        </div>
        <?php echo Form::close(); ?>

      <!-- service list  area start -->
  
        <div class="panel-body panel panel-default"> <?php if($categoryname->count() < 1): ?>
          <div class="no-record-area"> <?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?> </div>
          <?php else: ?>
          <div class="table">
            <div class="tr">
               <div class="table_heading">  </div>

              <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_NAME')); ?>  
                <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_NAME')); ?> <?php endif; ?> </div>
              <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_MEMBERSHIP_TYPE')); ?> <?php endif; ?> </div>
              <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?>  
                <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?> </div>

              <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.JOIN_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.JOIN_DATE')); ?>  
                <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.JOIN_DATE')); ?> <?php endif; ?></div>
                <?php if(Session::get('LoginType') !='4'): ?>
                <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  
                <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?></div>
                
              <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PAYMENT')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PAYMENT')); ?> <?php endif; ?></div>
              <?php endif; ?>
            </div>
               <?php $mc_name = 'mc_name';  $i=1;              
               ?>
            <?php $__currentLoopData = $categoryname; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  

            <?php
                $getsubtype=Helper::subscription_plans_type($val->mc_id);
             ?>           
         
            <?php if($mer_selected_lang_code =='ar'): ?>
            <?php $mc_name= 'mc_name_ar'; ?>
            <?php endif; ?>

            <?php if(Session::get('LoginType') =='4' && ( $val->cat_route =='makeup-artist-shop-info' || $val->cat_route =='roses-shop-info'  || $val->cat_route =='dates-info' || $val->cat_route =='car-rental' || $val->cat_route =='travel-info' || $val->cat_route =='abaya-shop-info'|| $val->cat_route =='mens-shop-info' || $val->cat_route =='dresses-shop-info'  || $val->cat_route =='photography-shop-info' || $val->cat_route =='event-shop-info' || $val->cat_route =='manage-singer' || $val->cat_route=='popular-band-info' || $val->cat_route=='acoustics' || $val->cat_route=='reception-shop-list' || $val->cat_route=='cosha-shop-info'  || $val->cat_route=='makeup-shop-info')): ?>
            <?php continue; ?> 
            <?php endif; ?>
            <?php   
            if(Session::get('LoginType') =='4')
            {
            $merchantManagerid = Session::get('merchant_managerid');
            $GetT = Helper::getAccess($merchantManagerid); 
            $getInfoID = $val->mc_id;
            if (!in_array($getInfoID, $GetT))
            {
            continue;
            }
            }  
            ?>
            <div class="tr">  

      <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.SN')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.SN')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.SN')); ?> <?php endif; ?>"><?php echo e($i); ?> &nbsp;&nbsp;</div>
     <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_NAME')); ?> <?php endif; ?>"> 

<?php if($getsubtype->services_id == '2' && $val->status == 0) {  $link =0; } else { $link =1; } ?>

    <?php if($val->status==1 || $val->status==0){ ?>        
      <?php if($val->cat_route!=''): ?>
      <?php if($val->cat_route=='acoustics'): ?>
      <?php  $getSinger = Helper::getsingerid($val->mc_id); $getSingerCid = Helper::getsingercid($val->mc_id); ?>
      <a class="main-underscore"  <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?> href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger,'Cid' => $getSingerCid ])); ?>" <?php endif; ?> > 
      <?php elseif($val->cat_route =='manage-singer' || $val->cat_route=='popular-band-info' || $val->cat_route=='acoustics'): ?>
      <?php  $getSinger = Helper::getsingerid($val->mc_id); ?>
      <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger ])); ?>" <?php endif; ?>> 
      <?php elseif($val->cat_route =='makeup-artist-shop-info' || $val->cat_route =='roses-shop-info'  || $val->cat_route =='dates-info' || $val->cat_route =='car-rental' || $val->cat_route =='travel-info' || $val->cat_route =='abaya-shop-info'|| $val->cat_route =='mens-shop-info' || $val->cat_route =='dresses-shop-info'  || $val->cat_route =='photography-shop-info' || $val->cat_route =='event-shop-info' ): ?>  
      <?php  $itemid = Helper::getmakeupArtist($val->mc_id); ?>
      <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>

      <?php elseif($val->cat_route =='makeup-shop-info'): ?>
      <?php  $itemid = Helper::getmakeup($val->mc_id); ?>
      <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>

      <?php elseif($val->cat_route =='cosha-shop-info'): ?>
      <?php  $itemid = Helper::getCosha($val->mc_id); ?>

      <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>

      <?php elseif($val->cat_route =='tailor-shop-info'): ?>
      <?php  $itemid = Helper::getTailor($val->mc_id); ?>

      <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>


        <?php else: ?>
        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id ])); ?>" <?php endif; ?>>
        <?php endif; ?>
        <?php echo e(isset($val->$mc_name) ? $val->$mc_name : ''); ?></a>
        <?php else: ?>                
        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route('hotelnamelist',['id' => $val->mc_id])); ?>" <?php endif; ?>><?php echo e(isset($val->$mc_name) ? $val->$mc_name : ''); ?></a>
        <?php endif; ?>

        <?php }else{ ?> <?php echo e(isset($val->$mc_name) ? $val->$mc_name : ''); ?> <?php } ?>
        </div>
        <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_MEMBERSHIP_TYPE')); ?> <?php endif; ?>"><?php if($getsubtype->services_id==1): ?> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_COMMISSION')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_COMMISSION')); ?> <?php endif; ?> <?php else: ?> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SUBSCRIPTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SUBSCRIPTION')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SUBSCRIPTION')); ?> <?php endif; ?> <?php endif; ?> </div>
      <?php if(Session::get('LoginType') !='4'): ?>
        <div class="td td4 " data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_STATUS')); ?>   <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_STATUS')); ?> <?php endif; ?>"> <?php if($val->status==1): ?>  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?>  
        
       <!--  <span class="status_tooltip">
        	<span>Click to deactivate</span>
        </span> -->
        
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?>  <?php else: ?> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_INACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_INACTIVE')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_INACTIVE')); ?> <?php endif; ?> <?php endif; ?></div>
 
      <?php endif; ?>
 
        <div class="td td4 " data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.JOIN_DATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.JOIN_DATE')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.JOIN_DATE')); ?> <?php endif; ?>"> <?php echo e(Carbon\Carbon::parse($val->UUN)->format('F j, Y')); ?> </div>

        <div class="td td6" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTION')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTION')); ?> <?php endif; ?>">
        <?php if($val->status==1 || $val->status==0){ ?>  

        <?php if($val->cat_route!=''): ?>



 <?php if($val->cat_route=='acoustics'): ?>
      <?php  $getSinger = Helper::getsingerid($val->mc_id); $getSingerCid = Helper::getsingercid($val->mc_id); ?>
      <a class="main-underscore"  <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger,'Cid' => $getSingerCid ])); ?>" <?php endif; ?>> 
 
        <?php elseif($val->cat_route =='manage-singer' || $val->cat_route=='popular-band-info'): ?>
        <?php  $getSinger = Helper::getsingerid($val->mc_id); ?>
        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?> href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger ])); ?>" <?php endif; ?>>
        <?php elseif($val->cat_route =='makeup-artist-shop-info'   || $val->cat_route =='roses-shop-info'  || $val->cat_route =='dates-info'  || $val->cat_route =='car-rental'  || $val->cat_route =='travel-info' || $val->cat_route =='abaya-shop-info' || $val->cat_route =='mens-shop-info' || $val->cat_route =='dresses-shop-info' || $val->cat_route =='photography-shop-info' || $val->cat_route =='event-shop-info' ): ?>
        <?php  $itemid = Helper::getmakeupArtist($val->mc_id); ?>

        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>

        <?php elseif($val->cat_route =='makeup-shop-info'): ?>
        <?php  $itemid = Helper::getmakeup($val->mc_id); ?>
        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>

        <?php elseif($val->cat_route =='cosha-shop-info'): ?>
        <?php  $itemid = Helper::getCosha($val->mc_id); ?>
        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>

        <?php elseif($val->cat_route =='tailor-shop-info'): ?>
        <?php  $itemid = Helper::getTailor($val->mc_id); ?>
        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ])); ?>" <?php endif; ?>>
        <?php else: ?>
        <a class="main-underscore"  <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route("$val->cat_route",['id' => $val->mc_id ])); ?>" <?php endif; ?>>
        <?php endif; ?>

        <?php if(Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VIEW')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VIEW')); ?> <?php endif; ?>  <?php echo e($val->$mc_name); ?></a>

        <?php else: ?>
 
        <a class="main-underscore" <?php if($link==0): ?> href="javascript:void(0);" <?php else: ?>  href="<?php echo e(route('hotelnamelist',['id' => $val->mc_id])); ?>" <?php endif; ?>><?php if(Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VIEW')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VIEW')); ?> <?php endif; ?>  <?php echo e($val->$mc_name); ?></a>
        <?php endif; ?>

        <?php }else { ?> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VIEW')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VIEW')); ?> <?php endif; ?>  <?php echo e($val->$mc_name); ?> <?php } ?>
        </div>

    <?php if(Session::get('LoginType') !='4'): ?>
  <div class="td td6" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PAYMENT')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PAYMENT')); ?> <?php endif; ?>">

 <?php if($getsubtype->services_id==2 && $val->status==0): ?>
  <a href="<?php echo e(route("pay-now",['id' => $val->mc_id])); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.PAYNOW')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PAYNOW')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PAYNOW')); ?> <?php endif; ?> </a>
  <?php else: ?>
  N/A
  <?php endif; ?> 

  </div>
  <?php endif; ?>

            </div>
            <?php $i = $i+1; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
          <?php endif; ?> 
          
          <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
          
          <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
          
        </div>
       <?php if(Session::get('LoginType') !='4'): ?>  <?php echo e($categoryname->links()); ?>  <?php endif; ?>
      <!-- service list  area start--> 
 
</div>

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Action')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Action')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Action')); ?> <?php endif; ?></div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a><a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_De_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record')); ?> <?php endif; ?>')
 } else {

     jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Activate_Record')); ?> <?php endif; ?>')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('change-status')); ?>",
        data: {activestatus:activestatus,id:id,from:'servicelist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $buffet_leftmenu =1; ?>
<div class="merchant_vendor cont_add">
<?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="right_panel">
  <div class="inner">
    <header>
     <?php if($autoid==''): ?>
      <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')); ?>  
            <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADD_CONTAINER')); ?> <?php endif; ?> </h5>
            <?php else: ?>
            <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_EDIT_CONTAINER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_EDIT_CONTAINER')); ?>  
            <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_EDIT_CONTAINER')); ?> <?php endif; ?> </h5>
            <?php endif; ?>
         </header>
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box notab">
            <!-- Display Message after submition -->
            <?php if(Session::has('message')): ?>
            <div class="alert alert-info"><?php echo e(Session::get('message')); ?>  </div>
            <?php endif; ?>
            <!-- Display Message after submition -->

              <form name="form1" id="add-container" method="post" action="<?php echo e(route('store-buffet-container')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">      
                  <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container_Name')); ?>  
                 <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container_Name')); ?> <?php endif; ?>
                   </label>
                   <div class="info100">
                      
                      <select class="small-sel" name="title"  id="title">

                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_SELEST_CONTAINER.'.MER_SELEST_CONTAINER')); ?> <?php endif; ?></option>
                        <?php $__currentLoopData = $letter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                        <option value="<?php echo e($val); ?>" <?php echo e(isset($fetchfirstdata->option_title) && $fetchfirstdata->option_title==$val ? 'selected' : ''); ?>><?php echo e($val); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                      </select>
                    </div>  
                    </div>
                </div>
               
                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
                      <?php if(Lang::has(Session::get('mer_lang_file').'.MER_No_People')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_No_People')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_No_People')); ?> <?php endif; ?>
                  </label>
                  <div class="info100">
                   <div>
                    <input class="xs_small notzero" maxlength="5" onkeypress="return isNumber(event)" type="text" name="no_people" value="<?php echo e(isset($fetchfirstdata->no_person) ? $fetchfirstdata->no_person : ''); ?>" id="no_people" required="">
                    <input type="hidden" name="autoid" name="<?php echo e($autoid); ?>">
                    </div>
                  </div>
                  </div>
                </div>
              
                <div class="form_row" style="display: none;">
                  <div class="form_row_left">
                  <label class="form_label">

                   <span class="english"><?php echo lang::get('mer_en_lang.MER_About'); ?></span>
            <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_About'); ?> </span> 
            </label>
                  <div class="info100">
                   <div class="english"> 
                    <textarea  maxlength="500" name="about" id="about" rows="4" cols="50">This is tested </textarea>
                     </div>
                    <div class="arabic"> 
                    <textarea class="arabic ar" maxlength="500" name="about_ar" id="about_ar " rows="4" cols="50">This is tested</textarea>
                    <input type="hidden" value="<?php echo e($parent_id); ?>" name="parent_id">
                    <input type="hidden" value="<?php echo e($sid); ?>" name="sid">
                    <input type="hidden" value="<?php echo e($itemid); ?>" name="itemid">
                     <input type="hidden" value="<?php echo e($autoid); ?>" name="autoid">
                    </div>
                    </div>
                    </div>
                 </div>
                <!-- form_row -->
                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
                    <?php if(Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Container_Image')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Container_Image')); ?> <?php endif; ?>
                     
          </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn">
                 <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SUBMIT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MER_SUBMIT')); ?> <?php endif; ?>
 </div>
         
                      </div>
                      </label>
                      <input id="company_logo" name="img" class="info-file" type="file" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" required="" value="">
                    </div>
                    <?php if(isset($fetchfirstdata->image) && $fetchfirstdata->image!=''): ?>
                    <div class="form-upload-img">
                       <img src="<?php echo e(isset($fetchfirstdata->image) ? $fetchfirstdata->image : ''); ?>">
                     </div>
                     <?php endif; ?>
                  </div>
                  </div>
                </div>
                <!-- form_row -->


                <div class="form_row">
                <div class="form_row_left">
                <div class="english">
                  <input type="submit" name="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SUBMIT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MER_SUBMIT')); ?> <?php endif; ?>">
                  </div>
                  </div>
                </div>

                <!-- form_row -->
              </form>

          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       no_people: {
                       required: true,
                      },
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       <?php if(isset($fetchfirstdata->image)!=''): ?>
                       img: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      },
                      <?php else: ?>
                        img: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
             required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_SELECT_NAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_SELECT_NAME')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CONTAINER_SELECT_NAME')); ?> <?php endif; ?>",
                      },  
   
                
                       no_people: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_No_VALIDATION_PEOPLE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_No_VALIDATION_PEOPLE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_No_VALIDATION_PEOPLE')); ?> <?php endif; ?>",
                      }, 

                    about: {
            required:  " <?php echo e(trans('mer_en_lang.MER_VALIDATION_ABOUT')); ?> ",
                      }, 
 
                    about_ar: {
               required:  " <?php echo e(trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR')); ?> ",
                      },
                     img: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                  
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
 if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                  
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }






<?php endif; ?>
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php $data = app('App\Help'); ?>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $hotel_leftmenu =1; ?>
<div class="merchant_vendor">  <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
	<div class="service_listingrow">
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_HallLIST')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HallLIST')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HallLIST')); ?> <?php endif; ?> </h5>
        <div class="add"><a  href="<?php echo e(url('')); ?>/hall-info?bid=<?php echo e(isset($id) ? $id : ''); ?>"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_ADDHALL')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ADDHALL')); ?>  
        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ADDHALL')); ?> <?php endif; ?></a> </div>
        </div> <!-- service_listingrow -->

        
 <?php $hID = request()->id;?>
          <?php echo Form::open(array('url'=>"list-hall/{$hID}",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')); ?>

        <?php $statuss = request()->status; $searchh= request()->search; ?>
        <a href="javascript:void(0);" class="filter_mobile"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></a>
        <div class="filter_area">
          
          <div class="filter_left">
            <div class="search_filter"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_FILTERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_FILTERS')); ?> <?php endif; ?></div>
            <div class="search-box-field mems">
               <select name="status" id="status">
              <option value=""> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS')); ?> <?php endif; ?> </option>
              <option value="1" <?php if(isset($statuss) && $statuss=='1'): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?>> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?></option>
              <option value="0" <?php if(isset($statuss) && $statuss=='0'): ?> <?php echo e("SELECTED"); ?>  <?php endif; ?> > <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?>  
              <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?></option>
            </select>
              <input name="" type="submit" value="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''): ?><?php echo e(trans(Session::get('mer_lang_file').'.MER_APPLY')); ?><?php else: ?><?php echo e(trans($MER_OUR_LANGUAGE.'.MER_APPLY')); ?><?php endif; ?>" class="applu_bts" />
            </div>
          </div>
          
          <div class="search_box">
            <div class="search_filter">&nbsp;</div>
            <div class="filter_right">
              <input name="search" type="text" placeholder="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SEARCH')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SEARCH')); ?> <?php endif; ?>" value="<?php echo e(isset($searchh) ? $searchh : ''); ?>" />
              <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
            </div>
          </div>
        </div>
      <!-- filter_area --> 
       <?php echo Form::close(); ?>


 <!-- Display Message after submition --> 
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
              <?php endif; ?> 
              <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap"> 
              
             

 

              <div class="panel-body panel panel-default">


<?php if($productdata->count() <1): ?>


<div class="no-record-area">
<?php if(Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?>

</div>

<?php else: ?>





                <div class="table hl-table">
                  <div class="tr">
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLSNAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLSNAME')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLSNAME')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLIMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLIMAGE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLIMAGE')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.FOOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.FOOD')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.FOOD')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.HALLTYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HALLTYPE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.HALLTYPE')); ?> <?php endif; ?></div>

                       <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRICE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRICE')); ?> <?php endif; ?></div>                            
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.LASTMODDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LASTMODDATE')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LASTMODDATE')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.STATUS')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.STATUS')); ?> <?php endif; ?></div>
                    <div class="table_heading"><?php if(Lang::has(Session::get('mer_lang_file').'.HALL_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HALL_INFO')); ?>  
                      <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.HALL_INFO')); ?> <?php endif; ?></div>
                  </div>
                  <?php if($productdata->count()): ?>
                  <?php $__currentLoopData = $productdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($val->food_type=='both'){
                  if(Session::get('mer_lang_file')=='mer_ar_lang'){
                  $foodtype = 'لدينا القائمة / الغذاء من الخارج'; 
                }else{
                  $foodtype = 'Our Menu/Food From Outside'; 
                }

                }
                   elseif($val->food_type=='external') { 
                    if(Session::get('mer_lang_file')=='mer_ar_lang'){
                      $foodtype = 'الغذاء من الخارج'; 
                  }else{
                   $foodtype = 'Food From Outside'; 
                  }
                 }else { 
                 if(Session::get('mer_lang_file')=='mer_ar_lang'){
                 $foodtype = 'القائمة لدينا';
               }else{
                 $foodtype = 'Our Menu';
                 }
                  }



                  if($val->hall_type=='both'){ 
                   if(Session::get('mer_lang_file')=='mer_ar_lang'){
                  $halltype = 'الرجال النساء'; 
                }else{
                 $halltype = 'Men/Women'; 
              }
                } else { 

                $halltype = $val->hall_type;
                if(Session::get('mer_lang_file')=='mer_ar_lang' && $halltype=='men'){
                $halltype='رجالي';
              }
                if(Session::get('mer_lang_file')=='mer_ar_lang' && $halltype=='women'){
                $halltype='نساء';
              }
                 }
                  if($val->pro_status ==1){ $Status = 'Active'; }else { $Status = 'Deactive'; }
                  ?>
                  <div class="tr">
                    <div class="td td1" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLSNAME')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLSNAME')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLSNAME')); ?> <?php endif; ?>">
                  <?php if(Session::get('mer_lang_file')=='mer_ar_lang'){ $t='pro_title_ar';}else{ $t='pro_title';} ?>
                  <?php echo e($val->$t); ?></div>
                    <div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_HALLIMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_HALLIMAGE')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_HALLIMAGE')); ?> <?php endif; ?>"> <?php if($val->pro_Img!=''): ?> <img width="150" height="150" src="<?php echo e($val->pro_Img); ?>"><?php else: ?> N/A <?php endif; ?> </div>
                    <div class="td td3" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.FOOD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.FOOD')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.FOOD')); ?> <?php endif; ?>"><?php echo e($foodtype); ?></div>
                    <div class="td td4" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.HALLTYPE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HALLTYPE')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.HALLTYPE')); ?> <?php endif; ?>"><?php echo e($halltype); ?></div>

<div class="td td2" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.MER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_PRICE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_PRICE')); ?> <?php endif; ?>"><?php echo e($val->pro_price); ?></div>
                  
                    <div class="td td5" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.LASTMODDATE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LASTMODDATE')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LASTMODDATE')); ?> <?php endif; ?>"><?php echo e(Carbon\Carbon::parse($val->updated_at)->format('F j, Y')); ?> </div>



                       <div class="td td6" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.STATUS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.STATUS')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.STATUS')); ?> <?php endif; ?>">
                    <?php if($val->pro_status==1): ?>
                   <span  class="status_active  status_active2 cstatus" data-status="Active"  data-id="<?php echo e($val->pro_id); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_ACTIVE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')); ?> <?php endif; ?> </span> 
                    <?php else: ?>
                    <span class="status_deactive  status_active2 cstatus" data-status="Inactive" data-id="<?php echo e($val->pro_id); ?>"> <?php if(Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_DEACTIVE')); ?>  <?php else: ?> <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')); ?> <?php endif; ?></span>
                    <?php endif; ?>

                    </div>




                    <div class="td td7" data-title="<?php if(Lang::has(Session::get('mer_lang_file').'.HALL_INFO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.HALL_INFO')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.HALL_INFO')); ?> <?php endif; ?>"><a href="<?php echo e(url('')); ?>/hall-info?bid=<?php echo e($id); ?>&hid=<?php echo e($val->pro_id); ?>"><img src="<?php echo e(url('')); ?>/public/assets/img/view-icon.png" title="<?php if(Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_view_title')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_view_title')); ?> <?php endif; ?>" alt="" /></a></div>
                  </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php else: ?>
                  <?php if(Lang::has(Session::get('mer_lang_file').'.NORECORDFOUND')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NORECORDFOUND')); ?>  
                  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.NORECORDFOUND')); ?> <?php endif; ?>
                  <?php endif; ?> </div>
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
                   <?php endif; ?>
              </div>
              <!-- PAGINATION --> 
              <?php echo e($productdata->links()); ?> 
              <!-- PAGINATION --> 
            
            </div>
            <!-- table_wrap --> 
           
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
</div>
<!-- merchant_vendor --> 



<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_Action')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Action')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Action')); ?> <?php endif; ?></div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a><a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_De_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record')); ?> <?php endif; ?>')
 } else {

     jQuery('.action_content').html('<?php if(Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_Activate_Record')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_Activate_Record')); ?> <?php endif; ?>')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('change-status')); ?>",
        data: {activestatus:activestatus,id:id,from:'halllist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 


<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="<?php echo e($vendordetails->mc_img); ?>" alt="logo" /></a></div>
      </div>
   
      <!-- vendor_header_left -->
      <?php echo $__env->make('includes.vendor_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 

if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 

  ?>
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active"><?php echo e((Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')); ?></a></li>
<?php if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !=''): ?>
          <li><a href="#video"><?php echo e((Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')); ?></a></li>
      <?php endif; ?>    

   <li><a href="#kosha"><?php echo e((Lang::has(Session::get('lang_file').'.Choose_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Choose_Kosha'): trans($OUR_LANGUAGE.'.Choose_Kosha')); ?></a></li>

          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></a></li>
          <?php } ?>
   
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>

  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e(str_replace('thumb_','',$value->image)); ?>" alt="" /> </li>
                <?php } }else{?>
                <li><img src="<?php echo e(str_replace('thumb_','',$vendordetails->image)); ?>" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="<?php echo e($value->image); ?>" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"><?php if(Session::get('lang_file')!='en_lang'): ?> <?php echo e($vendordetails->mc_name_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_name); ?> <?php endif; ?></div>
          <div class="detail_hall_description"><?php if($lang == 'en_lang'): ?>  <?php echo e($vendordetails->address); ?> <?php else: ?>   <?php echo e($vendordetails->address_ar); ?> <?php endif; ?></div>
          <div class="detail_hall_subtitle"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')); ?></div>
          <div class="detail_about_hall">
            <div class="comment more"> <?php if($lang != 'en_lang'): ?> <?php echo e($vendordetails->mc_discription_ar); ?> <?php else: ?> <?php echo e($vendordetails->mc_discription); ?> <?php endif; ?></div>
          </div>
          <div class="detail_hall_dimention"><?php echo e((Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')); ?>: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

 

<?php if($vendordetails->google_map_address!=''): ?> 
 <?php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    ?>
   <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
 <?php endif; ?>
 

        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>
  <?php if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !=''): ?>

        <div class="service-video-area">
         <?php if($vendordetails->mc_video_description!=''): ?> <div class="service-video-cont">
          <?php if($lang != 'en_lang'): ?>  <?php echo e($vendordetails->mc_video_description_ar); ?> <?php else: ?>  <?php echo e($vendordetails->mc_video_description); ?> <?php endif; ?></div><?php endif; ?>
          <div class="service-video-box">
        <?php if($vendordetails->mc_video_url !=''): ?>    <iframe class="service-video" src="<?php echo e($vendordetails->mc_video_url); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> <?php endif; ?>
          </div>
        </div>
<?php endif; ?>

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title"><?php echo e((Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')); ?></div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <?php $__currentLoopData = $allreview; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
                   <?php $userinfo = Helper::getuserinfo($val->cus_id); ?>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="<?php echo e($userinfo->cus_pic); ?>"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description"><?php echo e($val->comments); ?></div>
                        <div class="testim_name"><?php echo e($userinfo->cus_name); ?></div>
                        <div class="testim_star"><?php if($val->ratings): ?><img src="<?php echo e(url('')); ?>/themes/images/star<?php echo e($val->ratings); ?>.png"><?php endif; ?></div>
                      </div>
                    </div>
                  </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
   <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                <li><a href="<?php echo e(route('kosha-shop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1])); ?>" <?php if(request()->type!=2): ?> class="select" <?php endif; ?>><?php echo e((Lang::has(Session::get('lang_file').'.Design_Your_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Design_Your_Kosha'): trans($OUR_LANGUAGE.'.Design_Your_Kosha')); ?> </a></li>
                <li><a  <?php if(request()->type==2): ?> class="select" <?php endif; ?> href="<?php echo e(route('kosha-redymadeshop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2])); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Ready_Made_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Ready_Made_Kosha'): trans($OUR_LANGUAGE.'.Ready_Made_Kosha')); ?> </a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
 

<?php if(request()->type!=2): ?> 

<div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
          <ul>
          <?php for($i=0;$i< count($menu[0]->sub_menu); $i++): ?>
          <?php $k = $i+1; ?>
           <li onclick="sela('<?php echo e(addslashes($menu[0]->sub_menu[$i]['attribute_title'])); ?>','<?php echo e($menu[0]->sub_menu[$i]['id']); ?>','<?php echo e($k); ?>');"><a href="#o" id="<?php echo e($menu[0]->sub_menu[$i]['id']); ?>"  class="attb <?php if($k==1): ?> select <?php endif; ?>" ><?php echo e($menu[0]->sub_menu[$i]['attribute_title']); ?> </a></li>     
          <?php endfor; ?>
          </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line -->
 
             

<?php if(count($menu[0]->sub_menu) >=1): ?>
  <div class="kosha-select-area"> 
          <div class="kosha-tab-area" id="table1">
            <div class="kosha-heading"><?php echo e((Lang::has(Session::get('lang_file').'.SELECT')!= '')  ?  trans(Session::get('lang_file').'.SELECT'): trans($OUR_LANGUAGE.'.SELECT')); ?> <span id="displayCats"><?php echo e($menu[0]->sub_menu[0]['attribute_title']); ?></span></div>
            <div class="kosha-box"> 
            <?php for($ks=0;$ks < count($menu[0]->sub_menu); $ks++): ?>
            <?php for($i=0;$i< count($menu[0]->sub_menu[0]['product']); $i++): ?>
            <?php if($menu[0]->sub_menu[$ks]['product'][$i]['pro_title']==''){ continue;} $ksp = $ks+1; ?>
            <div class="kosha-select-line items_<?php echo e($ksp); ?>" <?php if($ksp!=1): ?> style="display:none;" <?php endif; ?>> 
                <?php    $pro_id = $menu[0]->sub_menu[$ks]['product'][$i]['pro_id']; ?>
                 <div class="kosha-select-img">
           
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 

                </div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name"><?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_title']); ?> </div>
                  <div class="kosha-select-description"><?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_desc']); ?></div>
                  <?php if($menu[0]->sub_menu[$ks]['product'][$i]['pro_qty']>0): ?>
                    <div class="service-radio-line">
              <div class="service_quantity_box" id="service_quantity_box">
                <div class="service_qunt"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?></div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity" data-title="Total Quality">
                    <div class="quantity">
                      <button type="button" id="sub" class="sub"></button>
                      <input type="number" name="itemqty" id="qty_<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantity('add','<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>');" onkeydown="isNumberKey(event); checkquantity('add','<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>');" />
                      <button type="button" id="add" class="add" onClick="return checkquantity('add','<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                  </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
            </div>
            <?php endif; ?>
                  <?php 
                       
                       $Insuranceamount = $menu[0]->sub_menu[$ks]['product'][$i]['Insuranceamount']; 
                    
                  ?>

                  <?php if(isset($Insuranceamount) && $Insuranceamount!=''): ?>
                  <div class="kosha-select-prise"> <?php echo e((Lang::has(Session::get('lang_file').'.INSURANCE')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE'): trans($OUR_LANGUAGE.'.INSURANCE')); ?> <?php echo e(currency($Insuranceamount,'SAR',$Current_Currency)); ?></div>
                   <?php endif; ?>
                  
                <?php  
 
                $Disprice = $menu[0]->sub_menu[$ks]['product'][$i]['pro_discount_percentage']; 

                
                $originalP = $menu[0]->sub_menu[$ks]['product'][$i]['pro_price'];
                if($Disprice==''){ $getAmount = 0; }else {$getAmount = ($originalP * $Disprice)/100;}
                

                $DiscountPricea = $originalP - $getAmount; 
                $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');
                ?>


  <?php if($Disprice >=1): ?> 
   <div class="kosha-select-prise "> <span class="strike"><?php echo e(currency($menu[0]->sub_menu[$ks]['product'][$i]['pro_price'],'SAR',$Current_Currency)); ?>    </span> 


        <?php $resulttotalamount=$DiscountPrice; ?>

      <?php echo e(currency($DiscountPrice,'SAR',$Current_Currency)); ?>  </div>
 
<?php else: ?>
<div class="kosha-select-prise">  <?php echo e(currency($menu[0]->sub_menu[$ks]['product'][$i]['pro_price'],'SAR',$Current_Currency)); ?></div>
 <?php $resulttotalamount=$menu[0]->sub_menu[$ks]['product'][$i]['pro_price']; ?>
<?php endif; ?>
        <?php if(isset($Insuranceamount) && $Insuranceamount!=''){ $iamo=$Insuranceamount; }else { $iamo=0;} ?>
       <input type="hidden" name="insuranceamount_<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>" id="insuranceamount_<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>" value="<?php echo e(currency($iamo, 'SAR',$Current_Currency, $format = false)); ?>">
       <input type="hidden" name="netamountforqty_<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>" id="netamountforqty_<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>" value="<?php echo e(currency($resulttotalamount, 'SAR',$Current_Currency, $format = false)); ?>">
<span id="grandtotal_<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>"></span>
                <?php if($menu[0]->sub_menu[$ks]['product'][$i]['pro_qty']>0): ?>
                  <div class="kosha-select-radieo">
                  
                    <input type="radio" name="re<?php echo e($ksp); ?>" class="selct" data-attribute="<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['attribute_id']); ?>" data-from="<?php echo e($ksp); ?>" data-title="<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_title']); ?>" data-currency="<?php echo e(Session::get('currency')); ?>"  data-price="<?php echo e(currency($DiscountPrice, 'SAR',$Current_Currency, $format = false)); ?>" data-insurance="<?php echo e(currency($Insuranceamount, 'SAR',$Current_Currency, $format = false)); ?>" data-img="<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_Img']); ?>" data-id="<?php echo e($menu[0]->sub_menu[$ks]['product'][$i]['pro_id']); ?>" />
                    <label>&nbsp;</label>
                  </div>
                  <?php else: ?>
                    <span id="sold" class="form-btn addto_cartbtn" ><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></span>
                  <?php endif; ?>
                </div>
              </div> <!-- kosha-select-line -->
            <?php endfor; ?> 
       <?php endfor; ?>         
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
              <!-- kosha-tab-area -->
        </div>
        <!-- kosha-select-area -->
          <?php else: ?>

 
        <div class="no-record-area">  
    


          <?php echo e((Lang::has(Session::get('lang_file').'.No_product_found_in_this_category')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_category'): trans($OUR_LANGUAGE.'.No_product_found_in_this_category')); ?>

</div> 



          <?php endif; ?>


      

 <form name="form1" method="post" id="add-container" action="<?php echo e(route('kosha-add-to-cart')); ?>"" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

          <input type="hidden" name="cart_type" value="occasion">
          <input type="hidden" name="cart_sub_type" value="cosha">
          <input type="hidden" name="language_type" value="en">
          <input type="hidden" name="attribute_id" value="47">
          <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
          <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
          <input type="hidden" name="vid" value="<?php echo e(request()->vid); ?>">
          <input type="hidden" name="product_id" value="<?php echo e(request()->id); ?>">
          <div class="kosha-selections-area" style="display: none;">
  
          <div class="kosha-heading"><?php echo e((Lang::has(Session::get('lang_file').'.Your_Selections')!= '')  ?  trans(Session::get('lang_file').'.Your_Selections'): trans($OUR_LANGUAGE.'.Your_Selections')); ?> </div>
          <div class="kosha-box">
            <div class="selection-box-area">             
            <?php for($i=1;$i<= count($menu[0]->sub_menu); $i++): ?>
            <span class="info_<?php echo e($i); ?>"></span>
            <?php endfor; ?>
            </div>
            <!-- selection-box-area -->
            <div class="kosha-tolat-area" >
              <div class="kosha-tolat-prise"><?php echo e((Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')); ?> : <span id="totalPrice">SAR 0</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn" />
              </div>
            </div>


            <!-- kosha-tolat-area -->
          </div>
          <!-- kosha-box -->
        </div>
  </form>
        <!-- kosha-selections-area -->
      </div>  <!-- kosha-area -->

<?php else: ?>
    
 
<?php if(count($product) < 1): ?>

<div class="kosha-area">
        <div class="kosha-tab-line">  
          <div id="kosha-tab" class="no-record-area">
<?php echo e((Lang::has(Session::get('lang_file').'.No_product_found_in_this_category')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_category'): trans($OUR_LANGUAGE.'.No_product_found_in_this_category')); ?>

          </div> </div> </div>

<?php else: ?>
 
 <div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
       
            <ul>
              <?php $f=1; ?>
              <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li onclick="redy('table<?php echo e($f); ?>','<?php echo e($pro->pro_id); ?>');"><a href="#o" id="<?php echo e($pro->pro_id); ?>" class="redycls <?php if($f==1): ?> select <?php endif; ?>"><?php echo e($pro->pro_title); ?></a></li>
            <?php $f=$f+1; ?>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line --> 
       <div class="rm-kosha-outer">
        <div class="rm-kosha-area">
        <?php $h=1; ?> 
  <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <form name="form1" method="post" id="add-kosha" action="<?php echo e(route('kosha-add-to-cart')); ?>"">
                <?php echo e(csrf_field()); ?>

          <div class="kosha-tab-area redymades" id="table<?php echo e($h); ?>" <?php if($h!=1): ?> style="display: none;" <?php endif; ?>>
            <div class="rm-kosha-name"><?php echo e($pro->pro_title); ?>  </div>
            <div class="kosha-box">
               <div class="rm-kosha-img">
<?php    $pro_id = $pro->pro_id; ?>
             <?php echo $__env->make('includes/product_multiimages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
           </div>
         <div class="rm-kosha-text"><?php echo e($pro->pro_desc); ?>





         </div>






         <div class="kosha-tolat-area">



               <?php if($pro->pro_qty>0): ?>
                    <div class="service-radio-line">
              <div class="service_quantity_box" id="service_quantity_box">
                <div class="service_qunt"><?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.QUANTITY')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.QUANTITY')); ?> <?php endif; ?></div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity" data-title="Total Quality">
                    <div class="quantity koshaqty">
                      <button type="button" id="sub" class="sub" onClick="return checkquantityforreadymade('remove','<?php echo e($pro->pro_id); ?>');"></button>
                      <input type="number" name="itemqty" id="qty_<?php echo e($pro->pro_id); ?>" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantityforreadymade('add','<?php echo e($pro->pro_id); ?>');" onkeydown="isNumberKey(event); checkquantityforreadymade('add','<?php echo e($pro->pro_id); ?>');" />
                      <button type="button" id="add" class="add" onClick="return checkquantityforreadymade('add','<?php echo e($pro->pro_id); ?>');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                  </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;"><?php if(Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''): ?><?php echo e(trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php else: ?><?php echo e(trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')); ?><?php endif; ?></span>
            </div>
            <?php endif; ?>


              <div class="kosha-tolat-prise kos_prc"> 
                  <?php if($pro->Insuranceamount>0){ ?>
                 <span><?php echo e((Lang::has(Session::get('lang_file').'.INSURANCE')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE'): trans($OUR_LANGUAGE.'.INSURANCE')); ?> <?php echo e(currency($pro->Insuranceamount,'SAR',$Current_Currency)); ?>  </span><br>

                 <?php } ?>
              <?php
                $Disprice = $pro->pro_discount_percentage; 
               $originalP = $pro->pro_price;
                if($Disprice==''){  $getAmount = 0;  }  else   {$getAmount = ($originalP * $Disprice)/100;}
               
              $DiscountPricea = $originalP - $getAmount; 
              $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');
              ?>

              <?php if($Disprice >=1): ?> 

              <span class="strike"><?php echo e(currency($originalP,'SAR',$Current_Currency)); ?>   </span> <span ><?php echo e(currency($DiscountPrice,'SAR',$Current_Currency)); ?> </span> 
              <?php else: ?>
              <span><?php echo e(currency($originalP,'SAR',$Current_Currency)); ?>  </span>

              

              <?php endif; ?>
<?php if($Disprice >=1) 
    $nprice=$DiscountPrice;
 else
    $nprice=$originalP;
  ?>
              <br>
              <?php $realprice=$nprice+$pro->Insuranceamount; ?>
                <?php echo e((Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')); ?>:
              <span id="grandtotal_<?php echo e($pro->pro_id); ?>"><?php echo e(currency($realprice,'SAR',$Current_Currency)); ?>  </span>
              </div>
 

              <div class="kosha-button-area">
                <input type="hidden" name="insuranceamount_<?php echo e($pro->pro_id); ?>" id="insuranceamount_<?php echo e($pro->pro_id); ?>" value="<?php echo e($pro->Insuranceamount); ?>">
                <input type="hidden" name="netamountforqty_<?php echo e($pro->pro_id); ?>" id="netamountforqty_<?php echo e($pro->pro_id); ?>" value="<?php echo e($nprice); ?>">
                <input type="hidden" name="totalpaidamount_<?php echo e($pro->pro_id); ?>" id="totalpaidamount_<?php echo e($pro->pro_id); ?>" value="<?php echo e($realprice); ?>">
              <input type="hidden" name="product_id" value="<?php echo e($pro->pro_id); ?>">
              <input type="hidden" name="cart_type" value="occasion">
              <input type="hidden" name="language_type" value="en">
              <input type="hidden" name="attribute_id" value="46">
              <input type="hidden" name="cart_sub_type" value="cosha">
              <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
              <input type="hidden" name="sid" value="<?php echo e(request()->sid); ?>">
              <input type="hidden" name="vid" value="<?php echo e(request()->vid); ?>">
              <?php if($pro->pro_qty>0): ?>
               <input type="submit" value="<?php echo e((Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')); ?>" class="form-btn" />
               <?php else: ?>
               <span id="sold" class="form-btn addto_cartbtn" ><?php echo e((Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')); ?></span>
               <?php endif; ?>
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
        </form>
 <?php $h=$h+1; ?>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div> <!-- rm-kosha-area -->
          </div> <!-- rm-kosha-outer -->
      </div>  <!-- kosha-area -->
<?php endif; ?>
<?php endif; ?>

 <!--service-display-section-->
    <?php echo $__env->make('includes.other_services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('includes.popupmessage', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

 <script type="text/javascript"> 

/* Remove Items from selection list */
$('body').on('click','.remove', function(){ 
        var title =  $(this).data('rid');
        var unchk =  $(this).data('unchk');
        $('.'+title).html('');
        $( "input[name='"+unchk+"']" ).prop('checked',false);
        var totalP = 0;
        var getP = 0;
        $( ".tprice" ).each(function() {
        getP =  $(this).val();
        totalP = parseFloat(totalP) + parseFloat(getP);
        }); 
        totalP = parseFloat(totalP).toFixed(2);
        $("#totalPrice").html('SAR '+ totalP); 

        if(totalP == 0)
        {
        $(".kosha-selections-area").hide();  
        }
});
/* Remove Items from selection list end */
 

 $('.selct').click(function()
 {
  var title =  $(this).data('title');
  var from =  $(this).data('from');
  var price =  $(this).data('price');  
  var img =   $(this).data('img');
  var pid =   $(this).data('id');
  var pinsurance =   $(this).data('insurance');
  var attribute =   $(this).data('attribute');
  var qty=$('#qty_'+pid).val();

  var currency =  $(this).data('currency'); 
  var totalprice=parseFloat(price) + parseFloat(pinsurance);
  var totalpriceaccordingqty=qty*totalprice;
  var totaldprice=totalpriceaccordingqty.toFixed(2);

  <?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') { $qtytext=trans(Session::get('lang_file').'.QUANTITY');} else { $qtytext=trans($OUR_LANGUAGE.'.QUANTITY');} ?>

  $(".kosha-selections-area").show();  
  $(".info_"+from).html('');
  $(".info_"+from).append('<div class="selection-box"><div class="selection-img"><img src="'+img+'" alt="" /></div><div class="selection-name">'+title+' <small class="small_oty"> <?php echo $qtytext;?>: '+qty+' </small><input type="hidden" class="tprice" name="itemprice[]" value="'+totaldprice+'"><input type="hidden" name="itemqty'+pid+'" value="'+qty+'"><input type="hidden" name="product[attribute_id][]" value="'+attribute+'"><input type="hidden" name="product[pro_id][]" value="'+pid+'"></div><div class="selection-prise">'+currency+' '+totaldprice+'</div><span class="remove" data-unchk="re'+from+'" data-rid="info_'+from+'"><img src="<?php echo e(url('')); ?>/themes/images/delete.png"/></span></div>');
    var totalP = 0;
    var getP = 0;
    $( ".tprice" ).each(function() {
    getP =  $(this).val();
    totalP = parseFloat(totalP) + parseFloat(getP);
    }); 

   totalP = parseFloat(totalP).toFixed(2)

$("#totalPrice").html(currency+' '+totalP); 
  });

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
  $('.flexslider').resize(); // this is it
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
function pricecalculation(act)
{
  var no=1;
  var product_id      = document.getElementById('product_id').value;
  var product_size    = document.getElementById('product_size').value;
  var currentquantity = document.getElementById('qty').value;
  var unititemprice   = document.getElementById('priceId').value;
  if(act=='add')
  {
    var qty = parseInt(currentquantity)+parseInt(no);     
  }
  else
  { 
    if(parseInt(currentquantity)==1)
    {
      var qty=parseInt(currentquantity)
    }
    else
    {
      var qty=parseInt(currentquantity)-parseInt(no);
    }
  }
  
  if(product_size)
  {
   $.ajax({
     type:"GET",
     url:"<?php echo e(url('getSizeQuantity')); ?>?product_id="+product_id+'&product_size='+product_size+'&qty='+qty,
     success:function(res)
     {               
    
     if(res!='ok')
     {
      //alert(res);
       alert('Not available quantity');
       var qtyupdated=parseInt(currentquantity);      
       document.getElementById('qty').value=qtyupdated;
     }
     else
     {
        //alert(res);
        //alert('er');
      var producttotal = qty*unititemprice;
      document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
      document.getElementById('itemqty').value=qty;
     }
     }
   });
  }
  else
  {
    var producttotal = qty*unititemprice;
    document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
    document.getElementById('itemqty').value=qty;
  }
      
}






function showProductDetail(str,vendorId)
{
    
     $.ajax({
     type:"GET",
     url:"<?php echo e(url('getShoppingProductInfo')); ?>?product_id="+str+'&vendor_id='+vendorId,
     success:function(res)
     {               
    if(res)
    { 
      $('html, body').animate({
        scrollTop: ($('.service-display-left').first().offset().top)
      },500);
      var json = JSON.stringify(res);
      var obj = JSON.parse(json);
          console.log(obj);
      length=obj.productdateshopinfo.length;
      //alert(length);
      document.getElementById('qty').value=1;
      if(length>0)
      {
           for(i=0; i<length; i++)
         {         
              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+obj.productdateshopinfo[i].pro_price+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productdateshopinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');          
               $('#cont_final_price').html('SAR '+obj.productdateshopinfo[i].pro_price);      
         }
      }
      if($.trim(obj.productattrsize) !=1)
      {
        $('#ptattrsize').html(obj.productattrsize);
      $('#ptattrsizeenone').css('display','block');
      }
      else
      {
        $('#ptattrsizeenone').css('display','none');
      }   
      }
     }
   });
}
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "<?php if(Lang::has(Session::get('lang_file').'.MORE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MORE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MORE')); ?> <?php endif; ?>";
  var lesstext = "<?php if(Lang::has(Session::get('lang_file').'.LESS')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.LESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.LESS')); ?> <?php endif; ?>";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script>
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {       
          "product_size" : {
            required : true
          },    
         },
         messages: {
          "product_size": {
            required: '<?php if(Lang::has(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_YOUR_SIZE')); ?> <?php endif; ?>'
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
</script>
 
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 <?php if(request()->type!=''): ?> 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 <?php endif; ?>
 <script type="text/javascript">
   function checkquantity(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty_'+prodid).value;
         var insuranceamount  = document.getElementById('insuranceamount_'+prodid).value;
         var netamountforqty  = document.getElementById('netamountforqty_'+prodid).value;
          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }

   $.ajax({
       type:"GET",
       url:"<?php echo e(url('checkcoshaquantity')); ?>?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_'+productId).value = qtyupdated - 1;
        
       }
       else
       {
         var totalinsurance=parseFloat(insuranceamount)*parseInt(qty);
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         var grandtotal=parseFloat(totalinsurance)+parseFloat(totalcalcamount);
         jQuery('#grandtotal_'+productId).html('<div class="kosha-tolat-prise" style="margin-top:10px;"><span><?php echo $Total_Price; ?></span> <span><?php echo $Cur; ?></span> <span>'+grandtotal.toFixed(2)+'</span></div>');

         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }
jQuery(document).ready(function(){
jQuery('.slideTwo').css('height', '100%');

});

    

 </script>


<script type="text/javascript">
   function checkquantityforreadymade(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty_'+prodid).value;
         var insuranceamount  = document.getElementById('insuranceamount_'+prodid).value;
         var netamountforqty  = document.getElementById('netamountforqty_'+prodid).value;         


          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }

   $.ajax({
       type:"GET",
       url:"<?php echo e(url('checkcoshaquantity')); ?>?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_'+productId).value = qtyupdated - 1;
        
       }
       else
       {
         var totalinsurance=parseFloat(insuranceamount)*parseInt(qty);
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         var grandtotal=parseFloat(totalinsurance)+parseFloat(totalcalcamount);
         jQuery('#grandtotal_'+productId).html('<div class="kosha-tolat-prise" style="margin-top:10px;"><span><?php echo $Total_Price; ?></span> <span><?php echo $Cur; ?></span> <span>'+grandtotal.toFixed(2)+'</span></div>');
         document.getElementById('totalpaidamount_'+productId).value = grandtotal;
         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }
jQuery(document).ready(function(){
jQuery('.slideTwo').css('height', '100%');

});

    

 </script>
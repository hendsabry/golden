    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img class="img-responsive" src="<?php echo e(url('newWebsite')); ?>/images/gc-logo.png" alt="" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Wedding & Occasions</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Halls</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Hotel Halls</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Large Halls</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Smal Halls</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Food</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Buffets</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Desserts</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Dates</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-sm-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Occasion coordinator</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Kosha</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Photography</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Hospitality</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Roses</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Special Events</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">E-invitations</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Beauty & Elegance</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Beauty center</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Spa</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Artist</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Makeup</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Reviving Concerts</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Singers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Popular bands</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Sound systems</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Clinics</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Dental & Dermatology</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Cosmetics & Laser</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-sm-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Shopping</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Dresses</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Abaya</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Perfumes</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Jewelry</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Travel Agencies</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Active</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Link item</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Link item</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="">Business Meeting</a></li>
                    <li><a href="">City</a></li>
                </ul>
                <div class="navbar-right">
                    <ul class="list-inline list-link">
                        <li><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/cart-icon.png"> Cart</a></li>
                        <li><a href=""><img src="<?php echo e(url('newWebsite')); ?>/images/login-icon.png"> Login</a></li>
                        <li><a href="">عربي</a></li>
                        <li><a class="joinnow" href="">Join Now +</a></li>
                    </ul>
                </div>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="slider-home">
        <div class="slide-pages owl-carousel" dir="ltr">
            <div class="item">
                <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
            </div>
            <div class="item">
                <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
            </div>
            <div class="item">
                <img src="<?php echo e(url('newWebsite')); ?>/images/img-slider.png" alt="" />
            </div>
        </div>

        <?php
            $currentpage=Route::getCurrentRoute()->uri();
            if(Session::get('searchdata')){
                $issession= count(Session::get('searchdata')) ;
            }else{
                $issession= 0 ;
            }
            if($currentpage == '/'){ $page='home'; }else{ $page='inner';}
            if($issession>0 && $page!='home'){ $emodify='modify-extra';  }else{ $emodify='';}
        ?>

        <?php $setOccasion = Helper::getAllOccasionNameList(2); ?>

        <form action="<?php echo e(route('search-results')); ?>" class="search" name="searchweddingoccasion2" id="searchweddingoccasion2">

            <div class="input-form">
                <select name="cityid" id="cityid">
                    <option>Select your city</option>
                    <option value=""><?php if(Lang::has(Session::get('lang_file').'.MER_SELECT_CITY')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.MER_SELECT_CITY')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.MER_SELECT_CITY')); ?> <?php endif; ?>
                    </option>
                    <?php $getC = Helper::getCountry(); ?>
                    <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" ><?php if($selected_lang_code !='en'): ?> <?php echo e($cbval->co_name_ar); ?> <?php else: ?>  <?php echo e($cbval->co_name); ?>   <?php endif; ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php

                                if(Session::get('searchdata.cityid')!=''){
                                $user_currentcity   = Session::get('searchdata.cityid');
                                }else {
                                $user_currentcity   = Session::get('user_currentcity');
                                }

                              $cityID = $val->ci_id;


                            ?>
                            <?php if($selected_lang_code !='en'): ?>
                                <?php $ci_name= 'ci_name_ar'; ?>
                            <?php else: ?>
                                <?php $ci_name= 'ci_name'; ?>
                            <?php endif; ?>
                            <option value="<?php echo e($val->ci_id); ?>" <?php if($user_currentcity == $cityID){  echo "SELECTED";}  ?>><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </select>
                <img src="<?php echo e(url('newWebsite')); ?>/images/detect-loaction-icon.png" alt="" />
            </div>
            <div class="input-form">
                <input type="text" placeholder="Occasion Date" class="search-t-box cal-t" name="occasiondate" id="DA" value="<?php echo e(Session::get('searchdata.occasiondate')); ?>" readonly />
                <img src="<?php echo e(url('newWebsite')); ?>/images/date-icon.png" alt="" />
            </div>
            <div class="input-form">
                <select id="weddingandoccasionlist" name="weddingoccasiontype">
                    <option>All Services</option>
                    <option value=""><?php if(Lang::has(Session::get('lang_file').'.Select')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.Select')); ?> <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.Select')); ?> <?php endif; ?></option>
                    <?php foreach($setOccasion as $woevents){ ?>
                    <?php if(Session::get('searchdata.weddingoccasiontype')==$woevents->id) { $occasionselected='selected'; }else{ $occasionselected='';}
                      if(Session::get('lang_file') !='en_lang')
                     {
                       $otitle_name= 'title_ar';
                     }
                     else
                     {
                        $otitle_name= 'title';
                     }
                    ?>
                    <option value="<?php echo e($woevents->id); ?>" <?php echo e($occasionselected); ?>  ><?php echo e($woevents->$otitle_name); ?> </option>
                    <?php } ?>

                </select>
            </div>
            <div class="input-form">
                <input type="submit" value="Go" />
            </div>
        </form>
    </div>
<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $beautybig_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header> <?php if(request()->serviceid==''): ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ADDPACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ADDPACKAGE')); ?> <?php endif; ?> </h5>
        <?php else: ?>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.UPDATEPACKAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE')); ?> <?php endif; ?> </h5>
        <?php endif; ?>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              <?php if(Session::has('message')): ?>
              <div class="alert alert-info"><?php echo e(Session::get('message')); ?> </div>
              <?php endif; ?>
              <!-- Display Message after submition -->
              <form name="form1" id="addmanager" method="post" action="<?php echo e(route('storebeautypackageservice')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"><span class="english"><?php echo lang::get('mer_en_lang.PACKEGENAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKEGENAME'); ?> </span></label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" maxlength="60" type="text" name="title" value="<?php echo e(isset($getservice->pro_title) ? $getservice->pro_title : ''); ?>" id="title" required="">
                      </div>
                      <div class="arabic">
                        <input class="arabic ar" maxlength="60" id="title_ar"  name="title_ar" value="<?php echo e(isset($getservice->pro_title_ar) ? $getservice->pro_title_ar : ''); ?>"  type="text" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.PACKAGEIMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.PACKAGEIMAGE'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="img" class="info-file" type="file">
                        <?php if(isset($getservice->pro_Img) && $getservice->pro_Img !=''): ?>
                        <div class="form-upload-img"><img src="<?php echo e(isset($getservice->pro_Img) ? $getservice->pro_Img : ''); ?>"></div>
                        <?php endif; ?> </div>
                    </div>
                  </div>
                </div>
                <!-- Add More product images start -->
                <?php
                $GalleryCunt = $productGallery->count();
                $J=2; $k=3;
                ?>
                
                <?php for($i=0;$i < $GalleryCunt;$i++): ?>
                <div class="form_row_right common_field">
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo<?php echo e($k); ?>">
                      <div class="file-btn-area">
                        <div id="file_value<?php echo e($J); ?>" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <input id="company_logo<?php echo e($k); ?>" name="image[]" class="info-file proImgess" data-lbl="file_value<?php echo e($J); ?>" type="file" value="">
                    </div>
                    <?php if(isset($productGallery[$i]->image) && $productGallery[$i]->image!=''): ?>
                    <div class="form-upload-img product_img_del"> <img src="<?php echo e(isset($productGallery[$i]->image) ? $productGallery[$i]->image : ''); ?>" >
                      <div class="imgpics-delet"> <a data-status="Active" data-id="<?php echo e($productGallery[$i]->id); ?>" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a> </div>
                    </div>
                    <span class="error file_value<?php echo e($J); ?>"></span> <?php endif; ?>
                    <input type="hidden" name="privius[]" value="<?php echo e($productGallery[$i]->id); ?>">
                  </div>
                </div>
                <?php  $J=$J + 1; $k=$k +1; ?>
                <?php endfor; ?>
                <div id="img_upload" class="common_field"></div>
                <div class="img-upload-line common_field">
                  <div id="add_button"><a href="javascript:void(0);" class="form-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')); ?><?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE')); ?> <?php endif; ?></a></div>
                  <span class="error pictureformat"></span> <?php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} ?>
                  <input type="hidden" id="count" name="count" value="<?php echo e($Count); ?>">
                </div>
                <!-- Add More product images end -->
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_PRICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_PRICE'); ?> </span> </label>
                    <div class="info100">
                      <div>
                        <input class="small-sel notzero" maxlength="9" onkeypress="return isNumber(event)" type="text" name="pro_price"  value="<?php echo e(isset($getservice->pro_price) ? $getservice->pro_price : ''); ?>" id="pro_price">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_DISCOUNT'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_DISCOUNT'); ?> </span> </label>
                    <div class="info100">
                      <input type="text" class="small-sel notzero" onkeypress="return isNumber(event)"  name="discount" maxlength="7" value="<?php echo e(isset($getservice->pro_discount_percentage) ? $getservice->pro_discount_percentage : ''); ?>" id="discount">
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_Duration'); ?> </span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_Duration'); ?> </span> </label>
                    <div class="info100">
                      <div>
                        <input class="small-sel notzero" onkeypress="return isNumber(event)" type="text" name="duration" maxlength="5" value="<?php echo e(isset($getservice->service_hour) ? $getservice->service_hour : ''); ?>"  id="mer_duration"   required="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row ">
                <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.SELECTSERVICE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.SELECTSERVICE'); ?> </span> </label>
                  <div class="info100"> <?php $__currentLoopData = $getAttr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catvals): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php  $Itemsarray = array();
                    $getProducts = Helper::getrelateditems($catvals->id,request()->itemid); 
                    if($getProducts->count() < 1) {continue; }
                    ?>
                    <!--Loop start-->
                    <div class="save-card-line">
                      <label for="men"> <?php if($mer_selected_lang_code =='en'): ?>  <?php echo e(isset($catvals->attribute_title) ? $catvals->attribute_title : ''); ?> <?php else: ?> <?php echo e(isset($catvals->attribute_title_ar) ? $catvals->attribute_title_ar : ''); ?> <?php endif; ?> </span> </label>
                    </div>
                    <div class="category_area"> <?php $__currentLoopData = $getProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proitems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <!--Loop -->
                      <div class="save-card-line"> <?php
                        $pID = $proitems->pro_id;
                        foreach($getPkg as $valp)
                        {
                        $Itemsarray[] = $valp->item_id;
                        }
                        ?>
                        <input type="checkbox" id="men<?php echo e($proitems->pro_id); ?>" <?php if(in_array($pID, $Itemsarray)): ?> CHECKED <?php endif; ?>  name="productitems[]" value="<?php echo e(isset($proitems->pro_id) ? $proitems->pro_id : ''); ?>">
                        <label for="men<?php echo e($proitems->pro_id); ?>"> <span class="englishd"><?php if($mer_selected_lang_code =='en'): ?>  <?php echo e(isset($proitems->pro_title) ? $proitems->pro_title : ''); ?> <?php else: ?>  <?php echo e(isset($proitems->pro_title_ar) ? $proitems->pro_title_ar : ''); ?>  <?php endif; ?><span class="sar">(SAR <?php echo e(isset($proitems->pro_price) ? $proitems->pro_price : ''); ?>)</span> </span> </label>
                      </div>
                      <!--Loop end -->
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <!--Loop End -->
                    <span for="productitems[]" generated="true" class="error"> </span> </div>
                  </label>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="<?php echo e(request()->id); ?>">
                      <input type="hidden" name="hid" value="<?php echo e(request()->hid); ?>">
                      <input type="hidden" name="itemid" value="<?php echo e(request()->itemid); ?>">
                      <input type="hidden" name="serviceid" value="<?php echo e(request()->serviceid); ?>">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                </div>
                <!-- form_row -->
              </form>
            </div>
          </div>
        </div>
        <!-- global_area -->
      </div>
    </div>
    <!-- right_panel -->
  </div>
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">
$("#addmanager").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       pro_price: {
                       required: true,
                      },
                      duration: {
                       required: true,
                      },
                      "productitems[]": {
                       required: true,
                      },

                      <?php if(!isset($getservice->pro_Img)): ?>     
                      img: {
                      required: true,
                      accept:"png|jpe?g|gif",
                      },
                      <?php else: ?>
                      img: {
                      accept:"png|jpe?g|gif",
                      },
                      <?php endif; ?>
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
            
               required:  " <?php echo e(trans('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                      },  

                title_ar: {
               required:  " <?php echo e(trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME')); ?> ",
                      },
                      "productitems[]": {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_SERVICE')); ?> <?php endif; ?> ",
                      },
                   pro_price: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE')); ?> <?php endif; ?>",
                      },
                       duration: {
               required:  "<?php if(Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_DURATION')); ?> <?php endif; ?>",
                      },                   
                     img: {
                  required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",
                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    var valdata=validation.invalid;


             <?php if($mer_selected_lang_code !='en'): ?>
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                     if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                     
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                    if (typeof valdata.pro_price != "undefined" || valdata.pro_price != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                    if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }
if (typeof valdata.pro_price != "undefined" || valdata.pro_price != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }


<?php endif; ?>
                    
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script>
<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_UPLOAD')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_UPLOAD')); ?> <?php endif; ?></div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         <?php if(Session::get('lang_file') =='ar_lang'): ?>
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        <?php else: ?>
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        <?php endif; ?>
        $(this).val('');
         $('.file-value').html('');
         }

});
</script>
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_NO')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_NO')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_NO')); ?> <?php endif; ?></a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_YES')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_YES')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_YES')); ?> <?php endif; ?></a> </div>
  </div>
</div>
<!-- action_popup -->
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
<?php if($mer_selected_lang_code !='en'): ?>
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
<?php else: ?>
jQuery('.action_content').html('Do you want to delete this record?');
<?php endif; ?>

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "<?php echo e(route('delete-shop-image')); ?>",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>
<!-- Add More product images end -->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
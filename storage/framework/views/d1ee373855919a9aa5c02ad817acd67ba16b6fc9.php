<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title><?php if(Lang::has($lang.'.MAIL_EMAIL_TEMPLATE')!= ''): ?> <?php echo e(trans($lang.'.MAIL_EMAIL_TEMPLATE')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.MAIL_EMAIL_TEMPLATE')); ?> <?php endif; ?></title>
</head>
<body>
<?php 
$SITE_LOGO = 'http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png';
$logo = $SITE_LOGO;
if(file_exists($SITE_LOGO))
$logo = $SITE_LOGO;
$getFbLinks        = Helper::getfblinks();
$gettwitterLinks   = Helper::gettwitterlinks();
$getgoogleLinks    = Helper::getgooglelinks();
$getinstagramLinks = Helper::getinstagramlinks();
$getpinterestLinks = Helper::getpinterestlinks();
$getyoutubeLinks   = Helper::getyoutubelinks();
?>
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right  ">مرحبًا  <?php echo e($vendor_name); ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373;  text-align:right  ">نشكرا على التعبير عن الاهتمام في خدمتي. يرجى التحقق من الأسعار وغيرها من التفاصيل أدناه.</td>
  </tr>  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right;  ">مشرف</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; text-align:right; ">أقفاص ذهبية</td>
  </tr>
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
	<?php if($getFbLinks !=''): ?>
	<a target="_blank" title="Facebook" href="<?php echo e($getFbLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-facebook.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($gettwitterLinks !=''): ?>
	<a target="_blank" title="Twitter" href="<?php echo e($gettwitterLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-twitter.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getgoogleLinks !=''): ?>
	<a target="_blank" title="Google+" href="<?php echo e($getgoogleLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-googleplus.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getinstagramLinks !=''): ?>
	<a target="_blank" title="Instagram" href="<?php echo e($getinstagramLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/share-instagram.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getpinterestLinks !=''): ?>
	<a target="_blank" title="Pinterest" href="<?php echo e($getpinterestLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-pint.png" /></a>&nbsp;
	<?php endif; ?>
    <?php if($getyoutubeLinks !=''): ?>
	<a target="_blank" title="Youtube" href=" <?php echo e($getyoutubeLinks); ?>"><img src="<?php echo e(url('')); ?>/themes/images/social-youtube.png" /></a>
	<?php endif; ?>
	</td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">Hi <?php echo e($vendor_name); ?></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Thanks for expressing interest in my service. Please check the pricing and other details below.</td>
  </tr>
  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td style="padding-top:20px; padding-bottom:20px"><table border="0" cellpadding="0" cellspacing="0" width="100%">
	    <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.THE_GROOM_NAME')!= ''): ?> <?php echo e(trans($lang.'.THE_GROOM_NAME')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.THE_GROOM_NAME')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($groom_name); ?></a></td>
        </tr>
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.HALL')!= ''): ?> <?php echo e(trans($lang.'.HALL')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.HALL')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($hall); ?></a></td>
        </tr>
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.MY_ACCOUNT_OCCASIONS')!= ''): ?> <?php echo e(trans($lang.'.MY_ACCOUNT_OCCASIONS')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.MY_ACCOUNT_OCCASIONS')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($occasion_type); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.DATE')!= ''): ?> <?php echo e(trans($lang.'.DATE')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.DATE')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($date); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.TIME')!= ''): ?> <?php echo e(trans($lang.'.TIME')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.TIME')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($time); ?></a></td>
        </tr>
		
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.RECORDING_SECTION')!= ''): ?> <?php echo e(trans($lang.'.RECORDING_SECTION')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.RECORDING_SECTION')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($recording_section); ?></a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.MUSIC_TYPE')!= ''): ?> <?php echo e(trans($lang.'.MUSIC_TYPE')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.MUSIC_TYPE')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($music_type); ?></a></td>
        </tr>
		
          
        
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><?php if(Lang::has($lang.'.SONG_NAME')!= ''): ?> <?php echo e(trans($lang.'.SONG_NAME')); ?> <?php else: ?> <?php echo e(trans($LANUAGE.'.SONG_NAME')); ?> <?php endif; ?>: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo e($song_name); ?></a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">Let us know if you have any further query.</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
</body>
</html>

<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title><?php echo e($SITENAME); ?> |  <?php echo e((Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD')); ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/main.css" />
    <?php if(Session::get('admin_lang_file') == 'admin_ar_lang'): ?>
     <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/custom-ar.css" />
     <?php endif; ?>
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" <?php echo e(url('')); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/public/assets/css/font-awesome/css/font-awesome.min.css" />
         <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?> <?php if(count($favi)>0): ?>  <?php $__currentLoopData = $favi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <link rel="shortcut icon" href="<?php echo e(url('')); ?>/public/assets/favicon/<?php echo e($fav->imgs_name); ?>">
<?php endif; ?>
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="<?php echo e(url('')); ?>/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo e(url('')); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
       <link href="<?php echo e(url('')); ?>/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link rel="<?php echo e(url('')); ?>/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="<?php echo e(url('')); ?>/public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 <?php echo $__env->make('admin.common.admin_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END HEADER SECTION -->
       <?php echo $__env->make('admin.common.left_menu_common', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       <!--PAGE CONTENT -->
        <div class="col-sm-9 col-xs-12 right-side-bar">
            <div class="right_col" role="main">
              <div id="appUserList">
                <div class="clearfix"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    
                                <div class="x_content UsersList">
								
                             
                                   <div class="page-left-right-wrapper not-sp">
    
    <div class="title_left">
      <h3 ><?php echo e((Lang::has(Session::get('admin_lang_file').'.ORDER_MANAGEMENT')!= '')  ?  trans(Session::get('admin_lang_file').'.ORDER_MANAGEMENT'): trans($OUR_LANGUAGE.'.ORDER_MANAGEMENT')); ?></h3>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hight_overhid">

             <?php echo Form::open(array('url'=>"admin/ordermanagement",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'filter')); ?>

             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><div class="mainSeacrh_box"><div class="input-groupSearch clearfix search-form">
          <input value="" placeholder="Order ID" autocomplete="off" id="navbar-search-input" name="orderid" type="text" class="form-control pr-Large20" onkeypress="return isNumberKey(event);">
        </div>
      </div></div>      
          
           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><button type="submit" class="btn theme-default-btn">Submit</button> </div>
         </div>
 <?php echo Form::close(); ?>

 <?php echo Form::open(array('url'=>"admin/ordermanagement",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'filter')); ?>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">  
                <input id="from_date"  placeholder="From Date" name="from_date" class="form-control" type="text">
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">  
                <input id="to_date"  placeholder="To Date" name="to_date" class="form-control" type="text">
              </div>
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><button type="submit" class="btn theme-default-btn">Submit</button> </div>
</div>

 <?php echo Form::close(); ?>

         </div>


      <div>
     
           
         
      </div>
     
      <div class="field_group top_spacing_margin_occas">
	    <?php if(Session::has('message')): ?>
         <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
        <?php endif; ?>
       <div class="page_table">
		 <?php if(count($setOrder) > 0): ?>
          <div class="myaccount-table">
            <div class="mytr">
              <div class="mytable_heading"></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('admin_lang_file').'.ORDERED_CUSTOMER_NAME')!= '')  ?  trans(Session::get('admin_lang_file').'.ORDERED_CUSTOMER_NAME'): trans($OUR_LANGUAGE.'.ORDERED_CUSTOMER_NAME')); ?></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('admin_lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('admin_lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')); ?></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('admin_lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('admin_lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')); ?></div>
              <div class="mytable_heading"><?php echo e((Lang::has(Session::get('admin_lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('admin_lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')); ?></div>
              <div class="mytable_heading order_id "><?php echo e((Lang::has(Session::get('admin_lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('admin_lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')); ?></div>
            </div>
            <?php $i = 1; ?>
            <?php if(count($setOrder) >0): ?>
            <?php $__currentLoopData = $setOrder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php 
              $customerinformation=Helper::getuserinfo($val->order_cus_id);
            ?>           
            <div class="mytr">
              <div class="mytd " data-title="Sl. No."><?php echo e($i); ?></div>
              <div class="mytd " data-title="Name"><?php echo e($customerinformation->cus_name); ?></div>
              <div class="mytd " data-title="Occasions Name">
			  <?php
			  if(isset($val->search_occasion_id) && $val->search_occasion_id!='0'){
              $setTitle = Helper::getOccasionName($val->search_occasion_id);
              ?>
              <?php $mc_name='title'?>
              <?php if(Session::get('lang_file')!='en_lang'): ?>
              <?php $mc_name= 'title_ar'; ?>
              <?php endif; ?>
               <a href="<?php echo e(url('')); ?>/admin/adminorderdetail/<?php echo e($val->order_id); ?>"><?php echo e($setTitle->$mc_name); ?></a>
			   <?php } else{
			   if(isset($val->main_occasion_id) && $val->main_occasion_id!='0'){ 
				 if(Session::get('lang_file')!='en_lang')
				 {
				   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
				 }
				 else
				 {
					$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
				 } 
				 foreach($getArrayOfOcc as $key=>$ocval)
				 {
				  if($val->main_occasion_id==$key)
				  {
				   $occasion_name = $ocval;
				  }
				 }	
				 ?>
				 <a href="<?php echo e(route('order-details',['id'=>$val->order_id])); ?>"> <?php echo $occasion_name; ?> </a>			   
			   <?php }} ?>
            </div>
              <div class="mytd " data-title="Date"><?php echo e(Carbon\Carbon::parse($val->order_date)->format('d M Y')); ?></div>
              <div class="mytd " data-title="Amount">SAR <?php echo e(number_format($val->order_amt,2)); ?></div>
              <div class="mytd order_id" data-title="Order ID">
			  
			  <a href="<?php echo e(url('')); ?>/admin/adminorderdetail/<?php echo e($val->order_id); ?>"><?php echo e($val->order_id); ?></a></div>
            </div>
            <?php $i++; ?> 
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
            <?php endif; ?>
         </div>
		 <?php else: ?>
		 <div class="no-record-area"><?php if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= ''): ?> <?php echo e(trans(Session::get('lang_file').'.NO_RECORD_FOUND')); ?>  <?php else: ?> <?php echo e(trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')); ?> <?php endif; ?></div> <!-- no-record-area -->
		 <?php endif; ?>
        </div>        
      </div>
      
    </div>
   <div class="pull-right mt-lg "><?php echo e($setOrder->links()); ?></div>
    <!-- page-right-section -->
  </div>  
									   
									   
									   
									
									   
									   
									   
									   
									   
									   
									 
									
									 
									 
									 
									 
									 
                                     
                                  </div>
                                  
                                </div>
                              </div>
              </div>
            </div>
       </div>
	   
	   </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     <?php echo $__env->make('admin.common.admin_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>


      <!-- GLOBAL SCRIPTS -->
  
     <script src="<?php echo e(url('')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="<?php echo e(url('')); ?>/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
      $('#from_date').datepicker({
        format: "MM dd, yyyy",
        autoclose: true,
        todayHighlight: true    
        });

        $('#from_date').on('change',function(){
          var dated = $(this).val();
        });

        $('#to_date').datepicker({
        format: "MM dd, yyyy",
        autoclose: true,
        todayHighlight: true    
        });

        $('#to_date').on('change',function(){
          var dated = $(this).val();
        });

      });
    </script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="<?php echo e(url('')); ?>/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">

</script>
<script src="<?php echo e(url('')); ?>/public/assets/js/admin/offerlist.js"></script>  
</body>

    <!-- END BODY -->
</html>

<script language="javascript">
  function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
            return false;
        } else {
            return true;
        }
    }
</script>

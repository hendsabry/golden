<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php $hotel_leftmenu =1; ?>
<div class="merchant_vendor"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.MER_BRANCH_EDIT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_BRANCH_EDIT')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_BRANCH_EDIT')); ?> <?php endif; ?></h5>
        <?php echo $__env->make('sitemerchant.includes.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </header>
      <!-- Display Message after submition --> 
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?> 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
                <form name="form1" method="post"   id="editbranch" action="<?php echo e(route('updatebranch')); ?>" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <div class="form_row ">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_HOTEL_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_HOTEL_NAME'); ?> </span> </label>
                    <div class="info100"> <?php                
                      $par_id = $alldata->parent_id;
                      $SecondCats=Helper::getparentCat($par_id);
                      $Hname = $SecondCats->mc_name;
                      $Hnames = $SecondCats->mc_name_ar;
                      ?>
                      <input type="text" class="english" readonly="" name="hotel1" value="<?php echo e(isset($Hname) ? $Hname : ''); ?>" id="hotal">
                      <input type="text" class="arabic ar" readonly="" name="hotel1" value="<?php echo e(isset($Hnames) ? $Hnames : ''); ?>" id="hotal">
                      <input type="hidden" name="hotel" value="<?php echo e($alldata->parent_id); ?>" id="hotal">
                    </div>
                    </div>
                    
                    <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SELEST_CITY'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); ?> </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="city_id"  id="city_id">
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_CITY')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY')); ?> <?php endif; ?></option>
            

                     <?php $getC = Helper::getCountry(); ?>
                        <?php $__currentLoopData = $getC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cbval): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"><?php echo e($cbval->co_name); ?></option>
                        <?php $getCity = Helper::getCityb($cbval->co_id); ?>              
                        <?php $__currentLoopData = $getCity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $ci_name='ci_name'?>
                        <?php if($mer_selected_lang_code !='en'): ?>
                        <?php $ci_name= 'ci_name_'.$mer_selected_lang_code; ?>
                        <?php endif; ?>   
                        <option value="<?php echo e($val->ci_id); ?>" <?php echo e(isset($val->ci_id) && $allid->city_id ==$val->ci_id ? 'selected' : ''); ?>><?php echo e($val->$ci_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    



                    
                      </select>
                    </div>
                    </div>
                    
                  </div>
                  <!-- form_row -->

                  <div class="form_row">



 <div class="form_row_left">
                        <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); ?> </span> </label>
                        <div class="info100"> 

                     
                            <input maxlength="50" type="text" name="mc_name" class="english" value="<?php echo e($allid->mc_name); ?>" id="hotal_name" >
                      
                      
                            <input id="ssb_name_ar" class="arabic ar" maxlength="50" value="<?php echo e($allid->mc_name_ar); ?>"  name="mc_name_ar"   type="text" onChange="check();">
                      
                          <div id="title_ar_error_msg"  style="color:#F00;font-weight:800"  > <?php if($errors->has('Hotel_Name_ar')): ?> <?php echo e($errors->first('Hotel_Name_ar')); ?><?php endif; ?> </div>
                        </div>
                    </div>


                  <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); ?> </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="manager" id="manager">
                        <option value=""><?php if(Lang::has(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')); ?>  
                        <?php else: ?>  <?php echo e(trans($MER_SELEST_MANAGER.'.MER_SELEST_MANAGER')); ?> <?php endif; ?></option>
                        
                      <?php $__currentLoopData = $manager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      
                        <option value="<?php echo e($val->mer_id); ?>" <?php echo e(isset($val->mer_id) && $allid->branch_manager_id ==$val->mer_id ? 'selected' : ''); ?>><?php echo e($val->mer_fname); ?></option>
                        
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    
                      </select>
                    </div>
                    </div>
                    
                   
                    
                  </div>
                  <!-- form_row -->

                  <div class="form_row">
                  <div class="form_row_left"> 
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESS'); ?> </span> </label>
                    <div class="info100">
                          <div class="english">
                            <input maxlength="150" type="text" name="address" class="english" value="<?php echo e($allid->address); ?>" id="hotal_name" >
                          </div>
                          <div  class="arabic ar" >
                            <input id="ssb_name_ar" class="arabic ar" maxlength="150" value="<?php echo e($allid->address_ar); ?>"  name="address_ar"   type="text" onChange="check();">
                          </div>
                          <div id="title_ar_error_msg"  style="color:#F00;font-weight:800"  > <?php if($errors->has('Hotel_Name_ar')): ?> <?php echo e($errors->first('Hotel_Name_ar')); ?><?php endif; ?> </div>
                        </div>
                    </div>
                    <!--div class="form_row_right common_field">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.Insuranceamount'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.Insuranceamount'); ?> </span> </label>
                    <div class="ensglish">
                      <input type="text" class="small-sel" name="Insuranceamount"  maxlength="15"   data-validation="length required" 
                    data-validation-error-msg="يرجى إدخال اسم القاعة" value="<?php echo e(isset($allid->insurance_amount) ? $allid->insurance_amount : ''); ?>" data-validation-length="max35">
                    </div>
                    <?php if($errors->has('Insuranceamount')): ?> <span class="error"> <?php echo e($errors->first('Insuranceamount')); ?> </span> <?php endif; ?> 
                    </div-->
                    </div>
                  <!-- form_row -->  
                                   
                  <div class="form_row common_field">
                    
                  <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label posrel"> 
                    <span class="english"><?php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); ?></span> 
                    <span class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); ?> </span> 
                    <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_google_add')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_google_add')); ?> <?php endif; ?></span></a>
                    </label>
                    <div class="info100">
                      <input type="url" name="google_map_address"  data-validation="length required" 
     data-validation-error-msg="Please enter google map address"  value="<?php echo e(isset($allid->google_map_address) ? $allid->google_map_address : ''); ?>" data-validation-length="max100">
                    </div>
                    </div>



 <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LONG')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LONG')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LONG')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($allid->longitude) ? $allid->longitude : ''); ?>" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> <?php if(Lang::has(Session::get('mer_lang_file').'.LAT')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.LAT')); ?>  <?php else: ?>  <?php echo e(trans($OUR_LANGUAGE.'.LAT')); ?> <?php endif; ?> </label>
                  <input type="text" class="form-control" value="<?php echo e(isset($allid->latitude) ? $allid->latitude : ''); ?>"  readonly="" name="latitude" />
          </div>
                </div>


                    
                  </div>
                  
                  <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); ?> </span> </label>
                    
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                      </div>
                      </label>
                      <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                      <input id="company_logo" name="mc_img" class="info-file" type="file">
                      <input type="hidden" name="updatemc_img" value="<?php echo e($allid->mc_img); ?>">
                      <input type="hidden" name="id" value="<?php echo e($id); ?>">
                      <div class="form-upload-img"><?php if($allid->mc_img !=''): ?>
                       <img src="<?php echo e(isset($allid->mc_img) ? $allid->mc_img : ''); ?>" width="150" height="150"> 
                       <?php endif; ?></div>
                    </div>
                    
                    </div>
                   <!--
                   <div class="form_row_right common_field">
                   <label class="form_label posrel"> <span class="english"><?php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); ?> </span> <a href="javascript:void(0);" class="address_image_tooltip"><span class="add_img_tooltip"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_address_img')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_address_img')); ?> <?php endif; ?></span></a>                   	
                   </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="hall_addressimg" id="company_logo1" class="info-file">
                    </div>
                    <span class="msg_img_replace"><?php if(Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.mer_img_replace')); ?>  <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.mer_img_replace')); ?> <?php endif; ?></span>
                    <div class="form-upload-img"><?php if(isset($allid->address_image) && $allid->address_image !=''): ?> <img src="<?php echo e($allid->address_image); ?>" width="150" height="150"> <?php endif; ?></div>
                    <?php if($errors->has('hall_addressimg')): ?> <span class="error"> <?php echo e($errors->first('hall_addressimg')); ?> </span> <?php endif; ?>
                    <input type="hidden" name="Address_Img" value="<?php echo e($allid->address_image); ?>">
                    
                   </div>-->
                    
                  </div>
                  <!-- form_row -->
                  </div>
                  <!-- form_row -->                  
                                                      
                  <div class="form_row english">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.TERMSANDCONDITIONS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.TERMSANDCONDITIONS'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo2">
                        <div class="file-btn-area">
                          <div id="file_value3" class="file-value"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="updatemc_tnc" value="<?php echo e($allid->terms_conditions); ?>">
                        <input id="company_logo2" name="mc_tnc" class="info-file" type="file"  value="">
                      </div>
                      <div class="pdf_msg"> 
                        
                        <?php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); ?>
                        
                        </div>
                      <?php if($allid->terms_conditions!=''): ?> 
                      
                      <a href="<?php echo e($allid->terms_conditions); ?>" target="_blank" class="pdf_icon"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($allid->terms_condition_name) ? $allid->terms_condition_name : ''); ?></span></a>
 
                       <?php endif; ?> 
          <input type="hidden" name="termsconditionname" value="<?php echo e(isset($allid->terms_condition_name) ? $allid->terms_condition_name : ''); ?>">
                    </div>
                      </div>
                  </div>
                  <!-- form_row -->

                          



   <div class="form_row arabic ar">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english"><?php echo lang::get('mer_en_lang.TERMSANDCONDITIONS'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.TERMSANDCONDITIONS'); ?> </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo11">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value10"></div>
                          <div class="file-btn"> <span class="english"><?php echo lang::get('mer_en_lang.MER_UPLOAD'); ?></span> <span  class="arabic ar"> <?php echo lang::get('mer_ar_lang.MER_UPLOAD'); ?> </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="updatemc_tnc_ar" value="<?php echo e($allid->terms_conditions_ar); ?>">
                        <input id="company_logo11" name="mc_tnc_ar" class="info-file" type="file"  value="">
                      </div>
                      <div class="pdf_msg"> 
                        
                        <?php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); ?>
                        
                        </div>
                      <?php if($allid->terms_conditions_ar!=''): ?> 
                      
                      <a href="<?php echo e($allid->terms_conditions_ar); ?>" target="_blank" class="pdf_icon"><img src="<?php echo e(url('/themes/images/pdf.png')); ?>"> <span><?php echo e(isset($allid->terms_condition_name_ar) ? $allid->terms_condition_name_ar : ''); ?></span></a>
 
                       <?php endif; ?> 
          <input type="hidden" name="termsconditionname_ar" value="<?php echo e(isset($allid->terms_condition_name_ar) ? $allid->terms_condition_name_ar : ''); ?>">
                    </div>
                      </div>
                  </div>
                  <!-- form_row -->











                  <div class="form_row"> 
                  
                  <?php 
                    if(preg_match('/listbranch/',$_SERVER['HTTP_REFERER'])){
                    $previouspage =1;
                    } else {
                    $previouspage =0;  
                    }
                    
                    ?>
                    <input type="hidden" name="redirctto" value="<?php echo e($previouspage); ?>">
                    <div class="form_row_left">
                    <div class="english">
                      <input type="submit"   name="submit" value="Update">
                    </div>
                    <div class="arabic ar">
                      <input   type="submit" name="submit" value="تحديث">
                    </div>
                    </div>
                    
                    
                    
                  </div>
                  <!-- form_row -->
                  
                </form>
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>       
$("form").data("validator").settings.ignore = "";
 </script> 
 
<script type="text/javascript">
  
$("#editbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },

                       manager: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                      mc_img: {
                           accept:"png|jpe?g|gif",
                      },

                       hall_addressimg: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },
                   mc_tnc: {
                           accept:"pdf",
                      },
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             city_id: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY')); ?> <?php endif; ?>",
                      },  
 
                 manager: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER')); ?> <?php endif; ?>",
                      },  

                         mc_name: {
               required: "<?php echo lang::get('mer_en_lang.MER_VALIDATION_BRANCH'); ?>",
                      },   

                          mc_name_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_BRANCH_AR'); ?>",
                      },   


                       description: {
               required: "<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ADDRESS')); ?> <?php endif; ?> ",
                      },

                       description_ar: {
               required: "<?php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); ?>",
                      },
                        mc_img: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>"
                      },

                       hall_addressimg: {
                                    accept:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')); ?> <?php endif; ?>",
                                    required:"<?php if(Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')); ?> <?php endif; ?>",


                                    

                      },                         
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

<?php if($mer_selected_lang_code !='en'): ?>

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
<?php else: ?>
  if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                   

<?php endif; ?>

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 

<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
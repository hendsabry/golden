<footer>
    <div class="container">
        <ul class="link-footer">
            <li>
                <a href="<?php echo e(url('/')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.HOME')!= '')  ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')); ?></a>
            </li>
            <li>
                <a href="<?php echo e(url('/about-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUTUS')!= '')  ?  trans(Session::get('lang_file').'.ABOUTUS'): trans($OUR_LANGUAGE.'.ABOUTUS')); ?></a>
            </li>
            <li><a href="">Certificates and recommendations</a></li>
            <li>
                <a href="<?php echo e(url('/occasions')); ?>">#<?php echo e((Lang::has(Session::get('lang_file').'.Occasions')!= '')  ?  trans(Session::get('lang_file').'.Occasions'): trans($OUR_LANGUAGE.'.Occasions')); ?> </a>
            </li>
            <li>
                <a href="<?php echo e(url('/privacy-policy')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.PrivacyPolicy')!= '')  ?  trans(Session::get('lang_file').'.PrivacyPolicy'): trans($OUR_LANGUAGE.'.PrivacyPolicy')); ?> </a>
            </li>
            <li>
                <a href="<?php echo e(url('/return-policy')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ReturnPolicy')!= '')  ?  trans(Session::get('lang_file').'.ReturnPolicy'): trans($OUR_LANGUAGE.'.ReturnPolicy')); ?> </a>
            </li>
            <li>
                <a href="<?php echo e(url('/contact-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ContactUs')!= '')  ?  trans(Session::get('lang_file').'.ContactUs'): trans($OUR_LANGUAGE.'.ContactUs')); ?> </a>
            </li>
            <li>
                <a href="<?php echo e(url('/sitemerchant')); ?>"
                   target="_blank"><?php echo e((Lang::has(Session::get('lang_file').'.Merchant_Login')!= '')  ?  trans(Session::get('lang_file').'.Merchant_Login'): trans($OUR_LANGUAGE.'.Merchant_Login')); ?>  </a>
            </li>
        </ul>


        <?php

            $getFbLinks        = Helper::getfblinks();

            $gettwitterLinks   = Helper::gettwitterlinks();

            $getgoogleLinks    = Helper::getgooglelinks();

            $getinstagramLinks = Helper::getinstagramlinks();

            $getpinterestLinks = Helper::getpinterestlinks();

            $getyoutubeLinks   = Helper::getyoutubelinks();

            $getphone          =  Helper::getphone();

            $getSettingaddress =  Helper::getSettingaddress();

            $getSettingemail   =  Helper::getSettingemail();

        ?>


        <div class="row">
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li><i class="fa fa-map-marker"></i> <?php echo e($getSettingaddress); ?></li>
                    <li><i class="fa fa-phone"></i> <?php echo e(isset($getphone) ? $getphone : ''); ?></li>
                    <li><i class="fa fa-envelope"></i> <a
                                href="mailto:<?php echo e(isset($getSettingemail) ? $getSettingemail : ''); ?>"><?php echo e(isset($getSettingemail) ? $getSettingemail : ''); ?></a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h2><?php echo e((Lang::has(Session::get('lang_file').'.FollowUS')!= '')  ?  trans(Session::get('lang_file').'.FollowUS'): trans($OUR_LANGUAGE.'.FollowUS')); ?></h2>
                <ul class="follow-us">

                    <?php if($getinstagramLinks !=''): ?>

                        <li><a href="<?php echo e($getinstagramLinks); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <?php endif; ?>

                    <?php if($getinstagramLinks !=''): ?>

                        <li><a href="<?php echo e($getinstagramLinks); ?>" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                        </li>
                    <?php endif; ?>

                    <?php if($getyoutubeLinks !=''): ?>

                        <li><a href="<?php echo e($getyoutubeLinks); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                    <?php endif; ?>


                    <?php if($getFbLinks !=''): ?>
                        <li><a href="<?php echo e($getFbLinks); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <?php endif; ?>
                        </li>
                        <?php if($getgoogleLinks !=''): ?>

                            <li><a href="<?php echo e($getgoogleLinks); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                            </li>

                        <?php endif; ?>

                        <?php if($gettwitterLinks !=''): ?>

                            <li><a href="<?php echo e($gettwitterLinks); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <?php endif; ?>

                </ul>
            </div>
            <div class="col-md-2">
                <h2><?php echo e((Lang::has(Session::get('lang_file').'.SecureShopping')!= '')  ?  trans(Session::get('lang_file').'.SecureShopping'): trans($OUR_LANGUAGE.'.SecureShopping')); ?></h2>
                <img class="safe" src="<?php echo e(url('newWebsite')); ?>/images/norten.png" alt=""/>
            </div>
            <div class="col-md-3">
                <h2><?php echo e((Lang::has(Session::get('lang_file').'.PaymentOptions')!= '')  ?  trans(Session::get('lang_file').'.PaymentOptions'): trans($OUR_LANGUAGE.'.PaymentOptions')); ?> </h2>
                <div class="dis-flex">
                    <img src="<?php echo e(url('newWebsite')); ?>/images/master.png" alt=""/>
                    <img src="<?php echo e(url('newWebsite')); ?>/images/visa.png" alt=""/>
                    <img src="<?php echo e(url('newWebsite')); ?>/images/master.png" alt=""/>

                </div>
            </div>
        </div>
        <hr>
        <div class="copyright">
            <p class="text-center">&copy; <?php echo date("Y"); ?> Goldencages.com <?php echo e((Lang::has(Session::get('lang_file').'.AllRightsReserved')!= '')  ?  trans(Session::get('lang_file').'.AllRightsReserved'): trans($OUR_LANGUAGE.'.AllRightsReserved')); ?>.</p>
        </div>
    </div>
</footer>



<?php echo $__env->make('includes.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="outer_wrapper diamond_fullwidth"> <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="inner_wrap">
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="<?php echo e(url('')); ?>/themes/images/back-arrow.png" alt="" /></div>
      <?php  
      if(Session::get('searchdata.mainselectedvalue')=='2'){ ?>
      <?php echo $__env->make('includes.searchweddingandoccasions', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?>
      <?php if(Session::get('searchdata.mainselectedvalue')=='1'){ ?>
      <?php echo $__env->make('includes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php } ?>


    </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper main_subcategory"> <?php echo $__env->make('includes.mobile-modify', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="diamond_box">
        <div class="page-right-section">
          <div class="diamond_main_wrapper"> <?php if(Session::get('lang_file')!='en_lang'): ?> <img src="<?php echo e(url('')); ?>/themes/images/Hall_ar-3.jpg" alt="" usemap="#hall-subcategory" hidefocus="true"> <?php else: ?> <img src="<?php echo e(url('')); ?>/themes/images/Hall-3.jpg" alt="" usemap="#hall-subcategory"> <?php endif; ?>
            <map name="hall-subcategory" id="hall-subcategory">
              <area shape="poly" coords="572,112,469,12,299,178,292,188,171,307,768,310" href="<?php echo e(url('')); ?>/vendorlistbycity/3/4" />
              <area shape="poly" coords="776,616,166,614,280,732,290,741,384,833,470,920" href="<?php echo e(url('')); ?>/vendorlistbycity/3/6" />
              <area shape="poly" coords="773,314,166,312,13,463,163,611,777,613,923,464" href="<?php echo e(url('')); ?>/vendorlistbycity/3/5" />
            </map>
          </div>
          <div class="diamond_shadow"><img src="<?php echo e(url('')); ?>/themes/images/diamond/shadow.png" alt=""></div>
        </div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
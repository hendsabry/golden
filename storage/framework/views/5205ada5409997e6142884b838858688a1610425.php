<?php  $ishomepage=substr(Route::currentRouteAction(), 0, (strpos(Route::currentRouteAction(), '@') -1) );
if($ishomepage=='App\Http\Controllers\FrontendControlle'){  $homeclass='homepading';}elseif($ishomepage=='App\Http\Controllers\SigninSignupControlle'){ $homeclass='loginpading'; }else{ $homeclass=''; }
 ?>
<?php $Current_Currency = Session::get('currency');
if($Current_Currency =='') { $Current_Currency = 'SAR';  }
 
 
?>
<div class="nav_row <?php echo e($homeclass); ?>">
  
    <div class="heamburger">
        <span></span>
        <span></span>
        <span></span>
    </div><!-- heamburger -->
 
	<div class="curns">
	<div class="curns_area">
	<div class="currency_text"><?php echo e((Lang::has(Session::get('lang_file').'.Currency')!= '')  ?  trans(Session::get('lang_file').'.Currency'): trans($OUR_LANGUAGE.'.Currency')); ?> </div>
		
		<div class="currency_select">
			<select name="currency" class="cc_currency">		
			<option value="sar" <?php if($Current_Currency == 'SAR'){ echo "SELECTED"; }  ?> >Riyal (SAR)</option>
			<option value="usd" <?php if($Current_Currency == 'USD'){ echo "SELECTED"; }  ?>>Dollar (USD)</option>
			<option value="inr" <?php if($Current_Currency == 'INR'){ echo "SELECTED"; }  ?>>Rupee (INR)</option>
		</select>

 
	</div>
	</div>
	
	
	</div> 
 
        <div class="left_nav">
            <ul>
                <li><a href="<?php echo e(url('/about-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ABOUTUS')!= '')  ?  trans(Session::get('lang_file').'.ABOUTUS'): trans($OUR_LANGUAGE.'.ABOUTUS')); ?></a></li>
                <li><a href="<?php echo e(url('/testimonials')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.Testimonials')!= '')  ?  trans(Session::get('lang_file').'.Testimonials'): trans($OUR_LANGUAGE.'.Testimonials')); ?></a></li>
                <li><a href="<?php echo e(url('/occasions')); ?>">#<?php echo e((Lang::has(Session::get('lang_file').'.Occasions')!= '')  ?  trans(Session::get('lang_file').'.Occasions'): trans($OUR_LANGUAGE.'.Occasions')); ?></a></li>
				<?php if(Session::get('customerdata.token')!=''): ?>
                <span class="mobile_fields"><li><a href="<?php echo e(url('login-signup/logoutuseraccount')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')); ?></a></li></span>
				<?php endif; ?>
            </ul>
        </div>
        <div class="middle_nav">
            <a class="brand" href="<?php echo e(url('/')); ?>">
            <img src="<?php echo e(url('')); ?>/themes/images/logo.png" />
            </a>
        </div>
        <div class="right_nav">
		
		
            <span class="mobile_fields">
 
				<?php if(Session::get('lang_file') == 'ar_lang'): ?>
                <a class="mob_lang" href="javascript:void(0);" data-val="en"  onclick="Lang_change('en')">English</a>
				<?php else: ?>    
				<a class="mob_lang" href="javascript:void(0);" data-val="ar"  onclick="Lang_change('ar')"><?php echo e((Lang::has(Session::get('lang_file').'.ARABIC')!= '')  ?  trans(Session::get('lang_file').'.ARABIC'): trans($OUR_LANGUAGE.'.ARABIC')); ?></a>
				<?php endif; ?>
				
                <?php if(Session::get('customerdata.token')==''): ?>
				<a href="<?php echo e(url('login-signup')); ?>" <?php if(Route::getCurrentRoute()->uri() == 'index' || Route::getCurrentRoute()->uri() == '/') { ?> class="active" <?php } ?>>
				<?php else: ?>
				<a href="<?php echo e(url('my-account')); ?>" <?php if(Route::getCurrentRoute()->uri() == 'index' || Route::getCurrentRoute()->uri() == '/') { ?> class="active" <?php } ?>>
				<?php endif; ?>
                   <?php if(Session::get('customerdata.token')!=''): ?>
                    <img src="<?php echo e(url('')); ?>/themes/images/login.png" />
					<?php else: ?>
					 <img src="<?php echo e(url('')); ?>/themes/images/user.png"  class="lgs"/>
					<?php endif; ?>
                </a>
            </span>
			<?php if(Session::get('customerdata.token')!=''): ?>
			<?php $getcartnoitems = Helper::getNumberOfcart(); ?>
			<?php if($getcartnoitems>0): ?>

            <?php $cid=Session::get('searchdata.cityid');   ?>
			
			<?php if($cid==''): ?>

			<a  href="javascript:void(0);" onclick="postcitypop(2);" class="basket_icon">   <span class="count"><?php echo e($getcartnoitems); ?></span></a>
			<?php else: ?>
			<a href="<?php echo e(url('mycart')); ?>" class="basket_icon"><span class="count"><?php echo e($getcartnoitems); ?></span></a>
 			<?php endif; ?>
 


            <?php endif; ?>
			<?php endif; ?>
            <ul>                   
					<?php if(Session::get('customerdata.token')==''): ?>
                <li><a href="<?php echo e(url('login-signup')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.SignIn')!= '')  ?  trans(Session::get('lang_file').'.SignIn'): trans($OUR_LANGUAGE.'.SignIn')); ?> / <?php echo e((Lang::has(Session::get('lang_file').'.SignUp')!= '')  ?  trans(Session::get('lang_file').'.SignUp'): trans($OUR_LANGUAGE.'.SignUp')); ?></a></li>
				<?php else: ?>
                <li><a href="<?php echo e(url('login-signup/logoutuseraccount')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')); ?></a></li>
				<?php endif; ?>
               
                <?php if(Session::get('lang_file') == 'ar_lang'): ?>
				 <li class="lang_select" data-val="en"  onclick="Lang_change('en')"> 
                <a href="#" class="lang_select" data-val="en"  onclick="Lang_change('en')">
                <div class="lang_select" data-val="en"  onclick="Lang_change('en')">English</div>     
                <?php else: ?>        
				 <li  class="lang_select" data-val="ar" onclick="Lang_change('ar')"> 
                <a href="#"  class="lang_select" data-val="ar" onclick="Lang_change('ar')">
                <div class="lang_select" data-val="ar" onclick="Lang_change('ar')"><?php echo e((Lang::has(Session::get('lang_file').'.ARABIC')!= '')  ?  trans(Session::get('lang_file').'.ARABIC'): trans($OUR_LANGUAGE.'.ARABIC')); ?></div>
                <?php endif; ?> 
                </li></a>
                
                <li><a href="<?php echo e(url('/contact-us')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.ContactUs')!= '')  ?  trans(Session::get('lang_file').'.ContactUs'): trans($OUR_LANGUAGE.'.ContactUs')); ?></a></li>
                <?php if(Session::get('customerdata.token')!=''): ?>
                <li><a href="<?php echo e(route('my-account')); ?>"><?php echo e((Lang::has(Session::get('lang_file').'.MyAccount')!= '')  ?  trans(Session::get('lang_file').'.MyAccount'): trans($OUR_LANGUAGE.'.MyAccount')); ?></a></li>
                <?php endif; ?>
            </ul>
            <div class="welcome_message well_desk">
			<?php 
			if(Session::has('customerdata.user_id')) 
	        { 
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				?>
				<?php echo e((Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')); ?>  <span>
				 
				<?php if(isset($getInfo->cus_name) && $getInfo->cus_name !=''): ?>
				<?php echo e(isset($getInfo->cus_name) ? $getInfo->cus_name : ''); ?>

				<?php else: ?>
				Guest
				<?php endif; ?>


				</span>
			<?php }	?>
			 </div>
            
        </div>
		
		<div class="wee_mobile"><div class="welcome_message">
			<?php 
			if(Session::has('customerdata.user_id')) 
	        { 
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				?>
				<?php echo e((Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')); ?>  <span>
				<?php
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
				?>
				</span>
			<?php }	?>
			 </div></div>
    </div><!-- nav_row -->
	
	<div class="mobile_logo_g">
	
            <a class="brand" href="<?php echo e(url('/')); ?>">
            <img src="<?php echo e(url('')); ?>/themes/images/logo.png" />
            </a>
        
	
	
	</div>

<script type="text/javascript">
 
jQuery(document).ready(function(){
jQuery('.cc_currency').on('change',function(){
var str = jQuery(this).val();
var url      = window.location.href; 

 if(window.location.href.indexOf("?") > -1) {
   window.location.href=url+'&currency='+str;
    }
    else
    {
    window.location.href=url+'?currency='+str;	
    }




})
});
 

</script>


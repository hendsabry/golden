<?php echo $__env->make('sitemerchant.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="merchant_vendor cont_add"> <?php echo $__env->make('sitemerchant.includes.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->make('sitemerchant.includes.left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"><?php if(Lang::has(Session::get('mer_lang_file').'.DASHBOARD')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.DASHBOARD')); ?>  
          <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.DASHBOARD')); ?> <?php endif; ?></h5>
      </header>
      <?php $merchantid=Session::get('merchantid'); ?>
      <!-- service_listingrow -->
      <!-- Display Message after submition -->
      <?php if(Session::has('message')): ?>
      <div class="alert alert-info"><?php echo e(Session::get('message')); ?></div>
      <?php endif; ?>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="dsah"> 
                    <?php if(Session::get('LoginType')!=4): ?>
              <a href="<?php echo e(url('listmanager')); ?>">
              <div class="dash_box">
                <div class="manager_img"><img src="<?php echo e(url('/public/assets/img/manager.jpg')); ?>" /></div>
                <div class="manger_text"> <?php echo e((Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGER') : trans($MER_OUR_LANGUAGE.'.MER_MANAGER')); ?></div>
              </div>
              </a>
              <?php endif; ?>

               <a href="<?php echo e(url('edit_merchant_info',['id' =>$merchantid])); ?>">
              <div class="dash_box">
                <div class="manager_img"><img src="<?php echo e(url('/public/assets/img/vendor-info.jpg')); ?>" /></div>
                <div class="manger_text"><?php echo e((Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ? trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS')); ?></div>
              </div>
              </a> <a href="<?php echo e(url('fund-request')); ?>">
              <div class="dash_box">
                <div class="manager_img"><img src="<?php echo e(url('/public/assets/img/fund.jpg')); ?>" /></div>
                <div class="manger_text"><?php echo e((Lang::has(Session::get('mer_lang_file').'.FUND_REQUESTS')!= '') ? trans(Session::get('mer_lang_file').'.FUND_REQUESTS') : trans($MER_OUR_LANGUAGE.'.FUND_REQUESTS')); ?></div>
              </div>
              </a> <a href="<?php echo e(url('sitemerchant_dashboard')); ?>">
              <div class="dash_box">
                <div class="manager_img"><img src="<?php echo e(url('/public/assets/img/services.jpg')); ?>" /></div>
                <div class="manger_text"><?php echo e((Lang::has(Session::get('mer_lang_file').'.Services')!= '') ? trans(Session::get('mer_lang_file').'.Services') : trans($MER_OUR_LANGUAGE.'.Services')); ?></div>
              </div>
              </a> 
                <?php if(Session::get('LoginType')!=4): ?>
              <a href="<?php echo e(url('account-details')); ?>">
              <div class="dash_box">
                <div class="manager_img"><img src="<?php echo e(url('/public/assets/img/account.jpg')); ?>" /></div>
                <div class="manger_text"><?php if(Lang::has(Session::get('mer_lang_file').'.AccountDetail')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.AccountDetail')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.AccountDetail')); ?> <?php endif; ?> </div>
              </div>
              </a> 
                 <?php endif; ?>
              <a href="<?php echo e(url('recieve-payment')); ?>">
              <div class="dash_box">
                <div class="manager_img"><img src="<?php echo e(url('/public/assets/img/payment.jpg')); ?>" /></div>
                <div class="manger_text"><?php if(Lang::has(Session::get('mer_lang_file').'.Payment')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.Payment')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.Payment')); ?> <?php endif; ?></div>
              </div>
              </a> <a href="<?php echo e(url('recieve_vendor_orders')); ?>">
              <div class="dash_box">
                <div class="manager_img"><img src="<?php echo e(url('/public/assets/img/basket_50x50.jpg')); ?>" />  <?php if(isset($vendorcountorder) && $vendorcountorder>0){echo '<span class="note-icon">'.$vendorcountorder.'</span>';} ?></div>
                <div class="manger_text"><?php if(Lang::has(Session::get('mer_lang_file').'.ORDERS')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.ORDERS')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.ORDERS')); ?> <?php endif; ?></div>
              </div>
              </a> <a href="<?php echo e(url('notification_vendor')); ?>">
              <div class="dash_box">
                <div class="manager_img"><img width="52" height="52" src="<?php echo e(url('/public/assets/img/notification.png')); ?>" />  <?php if(isset($totalcount) && $totalcount>0){echo '<span class="note-icon">'.$totalcount.'</span>';} ?></div>
                <div class="manger_text"><?php if(Lang::has(Session::get('mer_lang_file').'.NOTIFICATION')!= ''): ?> <?php echo e(trans(Session::get('mer_lang_file').'.NOTIFICATION')); ?> <?php else: ?>  <?php echo e(trans($MER_OUR_LANGUAGE.'.NOTIFICATION')); ?> <?php endif; ?></div>
              </div>
              </a> </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<?php echo $__env->make('sitemerchant.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php
	$params = array(
		'ClientInfo'  			=> array(
									'AccountCountryCode'	=> 'SA',
									'AccountEntity'		 	=> 'DHA',
									'AccountNumber'		 	=> '149022',
									'AccountPin'		 	=> '116216',
									'UserName'			 	=> 'ceo.founder@goldencages.com',
									'Password'			 	=> 'Golden-800',
									'Version'			 	=> 'v1.0'
								),
								
		'Transaction' 			=> array(
									'Reference1'			=> '001' 
								),
								
		'OriginAddress' 	 	=> array(
									'City'					=> 'Dhahran',
									'CountryCode'				=> 'SA'
								),
								
		'DestinationAddress' 	=> array(
									'City'					=> 'Dhahran',
									'CountryCode'			=> 'SA'
								),
		'ShipmentDetails'		=> array(
									'PaymentType'			 => 'P',
									'ProductGroup'			 => 'EXP',
									'ProductType'			 => 'PPX',
									'ActualWeight' 			 => array('Value' => 1, 'Unit' => 'KG'),
									'ChargeableWeight' 	     => array('Value' => 1, 'Unit' => 'KG'),
									'NumberOfPieces'		 => 1
								)
	);
	 
	$soapClient = new SoapClient('aramex-rates-calculator-wsdl.wsdl', array('trace' => 1));
	$results = $soapClient->CalculateRate($params);	
	
	echo '<pre>';
	print_r($results);
	die();
?>

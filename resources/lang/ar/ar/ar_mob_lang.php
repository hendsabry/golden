<?php
	return 
	[

/*** Mobile Session Message ends  ***/

'LOGIN_SUCCESS'			=> 		'تم تسجيل دخول المستخدم بنجاح',
'INVALID_CREDENTIAL'	=> 		'بيانات الاعتماد غير صالحة',
'INVALID_TOKEN_ERROR'	=> 		'لا يمكن إنشاء الرمز المميز',
'ALREADY_REGISTERD'		=> 		'أنت بالفعل عضو في أقفاص ذهبية. الرجاء تسجيل الدخول.',
'REGISTER_SUCCESS'		=> 		'مرحبا بكم في أقفاص الذهبية',
'INVALID_INPUT'			=> 		'مدخل غير صالح',
'EMAIL_NOT_FOUND'		=> 		'عرف البريد الإلكتروني غير موجود',
 'SUCESS_FORGOT' 		=>		'كلمة المرور الجديدة ترسل إلى عناوين البريد الإلكتروني' ,
 'PASS_SUCESS_CHANGE'   =>		'تم تغيير الرقم السري بنجاح',
 'PASS_ERROR_CHANGE'    =>     'آسف! كلمة المرور القديمة غير متطابقة. أدخله مرة أخرى.',
 'PROFILE_UPDATE_SUCESS' =>   'تحديث الملف الشخصي بنجاح',
 'PROFILE_UPDATE_ERROR' =>   'المستخدم ليس موجود',





/***********************************Wisi lang*************************************/

	];
?>
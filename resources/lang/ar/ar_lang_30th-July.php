<?php
	return 
	[
		'CATEGORIES' => 'الاقسام',
		'GOLDEN_CAGES'=>'أقفاص ذهبية',
		'MORE_CATEGORIES' => 'المزيد من الفئات',
		'NEW_PRODUCTS' => 'منتجات جديدة',
		'COMPARE' => 'قارن',
		'UPTO' => 'يصل إلى',
		'MER_UPDATE'=> 'تحديث ',
		'SOLD' => 'تم البيع',
		'ADD_TO_CART' => 'أضف إلى السلة',
		'NO_PRODUCTS_AVAILABLE' => 'لا توجد منتجات متوفرة',
		'CLOSE_COMPARISON_OVERLAY' => 'إغلاق تراكب المقارنة',
		'REQUEST_FOR_ADVERTISEMENT' => 'طلب للإعلان',
		'AD_TITLE' => 'عنوان الاعلان',
		'ADS_POSITION' => 'موضع الإعلانات',
		'SELECT_POSITION' => 'حدد الموقف',
		'HEADER_RIGHT' => 'رأس اليمين',
		'LEFT_SIDE_BAR' => 'شريط الجانب الأيسر',
		'BOTTOM_FOOTER' => 'تذييل أسفل',
		'PAGES' => 'صفحات',
		'SELECT_ANY_PAGE' => 'حدد أي صفحة',
		'HOME' => 'الصفحة الرئيسية',
		'UPLOADPIC' => 'تحميل الصورة',
		'SPORTS' => 'رياضات',
		'ELECTRONICS' => 'إلكترونيات',
		'FLOWER_POT' => 'اناء للزهور',
		'HEALTH' => 'الصحة',
		'BEAUTY' => 'جمال',
		'REDIRECT_URL' => 'إعادة توجيه عنوان ورل',
		'UPLOAD_IMAGES' => 'تحميل الصور',
		'UPLOAD' => 'تحميل',
		'CLOSE' => 'قريب',
		'CONTACT' => 'اتصل بنا',
		'CONTACT_US' => 'اتصل بنا',
		'ABOUT_COMPANY' => 'عن الشركة',
		'BLOG' => 'مدونة',
		'ABOUT_US' => 'معلومات عنا',
		'MERCHANT_LOGIN' => 'ميرتشانت تسجيل الدخول',
		'REGISTER' => 'تسجيل',
		'LOGIN' => 'تسجيل الدخول',
		'SOCIAL_MEDIA' => 'وسائل الإعلام الاجتماعية',
		'NEWS_LETTER_SUBSCRIPTION' => 'الاشتراك في النشرة الإخبارية',
		'SUBSCRIBE_TO_RECEIVE_THE_LATEST_NEWS_STRAIGHT_TO_YOUR_INBOX._BY_SUBSCRIBING_YOU_WILL_BE_ABLE_TO' => 'اشترك للحصول على آخر الأخبار مباشرة إلى صندوق البريد الوارد. عن طريق الاشتراك سوف تكون قادرة على','SUBSCRIBE_HERE' => 'اشترك هنا',
		'PAYMENT_METHOD' => 'طريقة الدفع او السداد',
		'OUR_SERVICES' => 'خدماتنا',
		'TERMS_&_CONDITIONS' => 'البنود و الظروف',
		'Thank_you_for_signing' =>'شكرا لك على التسجيل. ستتلقى رسالة تأكيد بالبريد الإلكتروني أيضًا.  .<br/><br/> <a href="sitemerchant" style="text-decoration:underline"> الرجاء الضغط هنا للدخول   </a> ',
		'HELP' => 'مساعدة',
		'THANK_YOU_TO_MERCHANT_SIGN_UP' => ' شكرا لك على التسجيل ',
		'ALL_RIGHTS_RESERVED' => 'كل الحقوق محفوظة',
		'SUCCESSFULLY_SUBSCRIBED' => 'تم الاشتراك بنجاح',
		'ITEMS_IN_YOUR_CART' => 'الوحدات الموجودة فى سلة التسوق الخاصة بك',
		'SELECT_CATEGORY' => 'اختر الفئة',
		'PRODUCTS' => 'منتجات',
		'DEALS' => 'صفقات',
		'SOLD_OUT' => 'بيعت كلها',
		'STORES' => 'مخازن',
		'Shop_link_in_maroof'=> 'متجر للتسوق في maroof',
		'NEAR_BY_STORE' => 'بالقرب من المتجر',
		'SEARCH_PRODUCT_NAME' => 'البحث عن اسم المنتج',
		'CUSTOMER_SUPPORT' => 'دعم العملاء',
		'FORGOT_PASSWORD' => 'هل نسيت كلمة المرور',
		'CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP' => 'ربط حساب الفيسبوك الخاص بك للتسجيل','DONT_HAVE_AN_ACCOUNT_YET' => 'لا تملك حسابا حتى الآن',
		'SIGN_UP' => 'سجل',
		'E-MAIL' => 'البريد الإلكتروني',
		'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO' => 'تسجيل الدخول باستخدام حساب الفيسبوك الخاص بك: قم بتوصيل حساب الفيسبوك الخاص بك لتسجيل الدخول إلى',
		'SIGN_UP_WITH_FACEBOOK' => 'اشترك عبر حساب فايسبوك',
		'ALREADY_A_MEMBER' => 'عضوا فعلا',
		'RESET_PASSWORD' => 'إعادة تعيين كلمة المرور',
		'NEW_PASSWORD' => 'كلمة السر الجديدة',
		'CONFIRM_PASSWORD' => 'تأكيد كلمة المرور',
		'USER_CONFIRM_PASSWORD'=>'تأكيد حقل كلمة المرور يجب أن يكون نفس كلمة المرور',
		'NO_HALL_FOUND'  	=> 	'لا هول المدرجة في هذا الفرع',
		'NO_SERVICES_FOUND'   => 'لا توجد خدمة مدرجة في هذا الفرع',
		'NAME' => 'اسم',
		'EMAIL' => 'البريد الإلكتروني',
		'PASSWORD' => 'كلمه السر',
		'SELECT_COUNTRY' => 'حدد الدولة',
		'SELECT_CITY' => 'اختر مدينة',
		'BY_CLICKING_SIGN_UP_YOU_AGREE_TO' => 'بالنقر على "تسجيل" فإنك توافق على',
		'TERMS_AND_CONDITIONS' => 'الأحكام والشروط',
		'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO_LARAVEL_ECOMMERCE_MULTIVENDOR' => 'تسجيل الدخول باستخدام حساب الفيسبوك الخاص بك: قم بتوصيل حساب الفيسبوك الخاص بك لتسجيل الدخول إلى لارافيل التجارة الإلكترونية مولتيفندو',
		'LOG_IN_WITH_FACEBOOK' => 'تسجيل الدخول باستخدام الفيسبوك',
		'SIGN_IN' => 'تسجيل الدخول',
		'HELPER::CUSTOMER_SUPPORT_NUMBER' => 'المساعد :: عدد دعم العملاء',
		'MIMIMUM_6_CHARACTERS' => 'ميميموم 6 أحرف',
		'ENTER_YOUR_EMAIL' => 'أدخل بريدك الإلكتروني',
		'ENTER_YOUR_NEW_PASSWORD' => 'أدخل كلمة المرور الجديدة',
		'CONFIRM_YOUR_NEW_PASSWORD' => 'أكد كلمة المرور الجديدة',
		'ENTER_YOUR_NAME_HERE' => 'أدخل اسمك هنا',
		'ENTER_YOUR_EMAIL_HERE' => 'أدخل عنوان بريدك الإلكتروني هنا',
		'SUBMIT' => 'عرض',
		'CANCEL' => 'إلغاء',
		'LOGIN_SUCCESSFULLY' => 'تسجيل الدخول بنجاح',
		'INVALID_LOGIN_CREDENTIALS' => 'اعتماد تسجيل الدخول غير صالح',
		'ALREADY_EMAIL_EXISTS' => 'البريد الإلكتروني موجود بالفعل',
		'PLEASE_CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTIONS' => 'يرجى التحقق من بريدك الإلكتروني للحصول على مزيد من الإرشادات',
		'EMAIL_ID_DOES_NOT_EXIST' => 'معرف البريد الإلكتروني غير موجود',
		'PASSWORD_CHANGED_SUCCESS' => 'تم تغيير كلمة المرور بنجاح',
		'INVALID_USER' => 'مستخدم غير صالح',
		'ModifySearch'=>'تعديل البحث',
		'Sign_In_Sign_Up_With' =>'تسجيل الدخول / الاشتراك مع',
		'OR'=>'أو',
		'THANKYOUHEADDING'=>'شكرا لكم',
		'Forgot_Password'=>'Forgot Password',
	/*Start login navbar*/	
		 'WELCOME' => 'أهلا بك',
		 'MY_WISHLIST' => 'قائمة امنياتي',
		 'LOG_OUT' => 'الخروج',
		 'ALL_CATEGORIES' => 'جميع الفئات', 
	     'WELCOME_TO_MERCHANT_SIGN_UP'=>'مرحبًا بك في التاجر',
	     'CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE'=>'قم بإنشاء متجر شخصي خاص بك على الإنترنت',
	     'CREATE_YOUR_STORE'=>'قم بإنشاء المتجر الخاص بك',
	     'MERCHANT_SIGN_UP'=>'تاجر مشترون',
	     'ENTER_YOUR_STORE_LOCATION'=>'أدخل موقع تخزينك',
	     'TYPE_YOUR_LOCATION_HERE'=>'اكتب موقعك هنا',
	     'SEARCH'=>'بحث',
		 'SelectServices' => 'اختر الخدمات',
	     'LATITUDE'=>'خط العرض',
	     'LONGTITUDE'=>'خط الطول',
	     'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS'=>'أدخل تفاصيل حسابك الدفع',
	     'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY'=>'أدخل تفاصيل حسابك الدفع Payfort Key',
	     'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT'=>'أدخل تفاصيل حسابك الدفع Payfort SALT ',
	     'ENTER_YOUR_LAST_NAME'=>'أدخل اسمك الأخير ',
		'STORE_NAME'=>'اسم المتجر',
		'PHONE'=>'هاتف',
		'PHONE'=>'هاتف',
		'ADDRESS1'=>'العنوان 1',
		'ADDRESS2'=>'العنوان 2',
		'COUNTRY'=>'بلد',
		'ENTER_YOUR_STORE_NAME'=>'أدخل اسم المتجر الخاص بك',
		'ENTER_YOUR_CONTACT_NUMBER'=>'أدخل رقم جهة الاتصال الخاصة بك',
		'ENTER_YOUR_ADDRESS1'=>'أدخل العنوان الخاص بك 2',
		'ENTER_YOUR_ADDRESS2'=>'أدخل العنوان الخاص بك 2',
  		'ZIPCODE'=>'الرمز البريدي',
  		'CITY'=>'مدينة',
		'WEBSITE'=>'موقع الكتروني',
		'SERVICES'=>'موقع الكتروني',
		'ENTER_YOUR_ZIPCODE'=>'أدخل رمزك البريدي',
		'ENTER_YOUR_STORE_WEBSITE'=>'أدخل موقع المتجر الخاص بك',
		'PERSONAL_DETAILS'=>'تفاصيل شخصية',
		'FORGOT_PASSWORD'=>'هل نسيت كلمة المرور',
		'FIRST_NAME'=>'الاسم الاول',
		'ENTER_YOUR_FIRST_NAME'=>'أدخل اسمك الأول',
		'LAST_NAME'=>'الكنية',
		'ENTER_YOUR_EMAIL_ID'=>'أدخل معرف البريد الإلكتروني الخاص بك',
		'CONTACT_NUMBER'=>'رقم الاتصال',
		'PAYMENT_EMAIL'=>'دفع البريد الإلكتروني',
		'PAYMENT_EMAIL_KEY'=>'دفع البريد الإلكتروني مفتاح',
		'PAYMENT_EMAIL_SALT'=>'دفع البريد الإلكتروني الملح',
		'FORGOT_PASSWORD_MSG'=>'الرجاء إدخال عنوان البريد الإلكتروني الخاص بك.',
		'FORGOT_PASSWORD_MSG2'=>'ستتلقى رابطًا لإنشاء كلمة مرور جديدة عبر البريد الإلكتروني.',
		'New_Password'=>'كلمة السر الجديدة',
		'Confirm_Password'=>'تأكيد كلمة المرور',

'WITHIN_BUDGET'			=> 	'ضمن الميزانية',
		'ABOVE_BUDGET'			=> 	'فوق الميزانية',
		'OFFERS'			=> 	'عروض',
		'Hotel_Halls'=>'فندق القاعات',
		'TOTAL_PRICE'   => 'السعر الكلي',
		'TOTAL_SERVICES' => 'مجموع الخدمات',
		'MER_MAIL_SEND_SUCCESSFULLY'=>'إرسال البريد بنجاح',
		 
	/*End login navbar*/
	/*Start About Us.php page*/
		
	/*End About Us.php page*/
	/*Start App.php page*/
		'LARAVEL' => 'لارافل',
		'TOGGLE_NAVIGATION' => 'تبديل الملاحة',
	/*End App.php page*/
	/*Start Action blade.php*/
		'SPECIALS' => 'العروض الخاصة',
		'LAST_BIDDER' => 'آخر عطاء',
		'BIT_AMOUNT' => 'مبلغ بت',
		'BID_NOW' => 'Bid now',
		'MORE_FILTERS' => 'عرض الأسعار الآن',
		'RESET' => 'إعادة تعيين',
		'PER_PAGE' => 'لكل صفحة',
		'VIEW_ALL' => 'عرض الكل',
		'LIKES_ASC' => 'يحب أسك',
		'SORT_BY' => 'ترتيب حسب',
		'PRICE_LOW' => 'سعر منخفض',
		'HIGH' => 'متوسط',
		'PRICE_HIGH' => 'السعر مرتفع',
		'LOW' => 'منخفض',
		'TITLE' => 'عنوان',
		'A' => 'ا',
		'Z' => 'ض',
		'DESCRIPTION' => 'وصف',
		'PAGE' => 'صفحة',
		'OF' => 'من',
		'NO_RESULTS_FOUND' => 'لا توجد نتائج',
		'LIST_ACTIONS' => 'قائمة الإجراءات',
		'LIKES_DESC' => 'مرات الإعجاب',
		'DATE_ASC' => 'تاريخ أسك',
		'DATE_DESC' => 'تاريخ ديسك',
		'STYLE_SELECTOR' => 'محدد النمط',
		'OREGIONAL_SKIN' => 'الجلد الأصلي',
		'BOOTSWATCH_SKINS' => 'بوتسواتش جلود',
		'THESE_ARE_JUST' => 'هذه مجرد أمثلة ويمكنك بناء نظام الألوان الخاصة بك في الخلفية',
		'FILTER_BY_TITLE' => 'تصفية حسب العنوان',
		
		'EMAILADDRRESS' => 'العنوان العنوان',
		'PASSWORD' => 'العنوان',
		'MER_FORGOT_PASSWORD'=> 'العنوان',
		'MER_BACK_TO_LOGIN'=> 'العنوان   ال عنوان',
		'MER_ENTER_YOUR_VALID_E-MAIL'=> 'العنوان   ال عنوان',
		'MER_SIGN_IN'=> 'العنوان',
		'NEWMEMBER'=> 'العنوان',
		'SIGNUP'=> 'العنوان' ,
		

'PLEASE_ENTER_YOUR_FIRST_NAME'=> 'ويرجى ادخال الاسم الاول',
'PLEASE_ENTER_LAST_NAME'=> 'ويرجى إدخال اسمك الأخير',
'PLEASE_ENTER_AN_EMAIL_ADDRESS'=> 'والرجاء إدخال عنوان البريد الإلكتروني الخاص بك',
'PLEASE_ENTER_PHONE_NO'=> ' يرجى إدخال رقم الهاتف الخاص بك ',

'PLEASE_SELECT_COUNTRY'=> 'ويرجى اختيار الدولة ',
'PLEASE_ENTER_CITY'=> 'يرجى اختيار المدينة ',


'PLEASE_ENTER_ADDRESS1_FIELD'=> 'والرجاء إدخال عنوانك ',
'PLEASE_ENTER_ZIPCODE'=> 'والرجاء إدخال الرمز البريدي الخاص بك ',
'PLEASE_CHOOSE_YOUR_UPLOAD_FILE'=> 'ويرجى تحميل صورتك ',

// Wisitech language
'VALIDFILE' => 'Please upload valid file',
'CertificateChamber' => 'غرفة الشهادة',
'HOME'	=> 'الصفحة الرئيسية',
'ABOUTUS'	=> 'معلومات عنا',
'Testimonials'	=> 'الشهادات - التوصيات',
'Occasions'	=> 'مناسبات',
'PrivacyPolicy'	=> 'سياسة الخصوصية',
'ReturnPolicy'	=> 'سياسة العائدات',
'ContactUs'	=> 'اتصل بنا',
'Address'	=> 'عنوان',
'PaymentOptions'	=> 'خيارات الدفع',
'SecureShopping'	=> 'التسوق الآمن',
'FollowUS'	=> 'تابعنا',
'PrivacyPolicy'	=> 'سياسة الخصوصية',
'ReturnPolicy'	=> 'سياسة سياسة',
'AllRightsReserved'	=> 'كل الحقوق محفوظة',
'SignIn'	=> 'تسجيل الدخول',
'SignUp'	=> 'سجل',
'SignOut'	=> 'خروج',
'BusinessMeetings'	=> 'اجتماعات عمل',
'WeddingandOccasions'	=> 'عرس ومناسبات',
'VALIDFILEPDF' =>'خروج',
'VALIDSHOPLINK' => 'Please enter shop link in maroof',
'TypeofBusiness'	=> 'نوع من الاعمال',
'NumberofAttendees'	=> 'عدد الحضور',
'Budget'	=> 'ميزانية',
'City'	=> 'مدينة',
'StartDate'	=> 'تاريخ البدء',
'EndDate'	=> 'تاريخ الانتهاء',
'OccasionDate'	=> 'تاريخ المناسبة',
'OccasionType'	=> 'نوع المناسبة',
'MER_SELECT_CITY'=>'اختر مدينة',
'About_Hall'=>'حول القاعة',
'Hall_Dimension'=>'هول البعد',
'currency'=>'SAR',
'Facilities_Offered_Here'=>'التسهيلات المقدمة هنا',
'Prepaid_Services'=>'Prepaid Services',
'Food'=>'أضف طعام',
'EXFood'=>'طعام',
'Services'=>'أضف خدمات',
'What_Our_Client_Says'=>"ماذا يقول عميلنا",
'noofattendees'=>'يرجى إدخال no من الحضور',
'budget'=>'يرجى إدخال الميزانية',
'occasiondate'=>'يرجى إدخال تاريخ المناسبة',
'weddingoccasiontype'=>'الرجاء إدخال نوع المناسبة',
'ENTER_YOUR_CITY' => 'أدخل مدينتك',
'startdate'=>'يرجى إدخال تاريخ البدء',
'enddate'=>'يرجى إدخال تاريخ الانتهاء',
'Reserve_Now'=>'احجز الان',
'USER_NAME'=>'من فضلك أدخل إسمك',
'USER_EMAIL'=>'الرجاء إدخال عنوان البريد الإلكتروني',
'USER_MOBILE'=>'يرجى إدخال اسم الجوال',
'USER_PASSWORD'=>'يرجى إدخال كلمة المرور',
'USER_NEME_LEVEL'=>'اسم',
'USER_MOBILE_LEVEL'=>'رقم الهاتف المحمول',
'External_Food'	=>'الغذاء الخارجي',
'Internal_Food'	=>'الغذاء الداخلي',
'THANKYOU'=>'شكرا لك على التسجيل. ستتلقى تأكيدًا عبر البريد الإلكتروني أيضًا.',
'BACK_MEMBER_ACCOUNT_CREATED_SUCCESSFULLY'  => 'Member account created successfully',
'DISH_NAME_CONTAINER_TYPE'  => 'اسم الصحن / نوع الحاوية',
		'TOTAL_QUALITY'  => 'الجودة الشاملة',
		'PRICE'  => 'السعر',
		'CONTAINER_LIST'  => 'قائمة الحاويات',
		'ADD_TO_CART'  => 'أضف إلى السلة',
		'ORDER_SUMMARY'  => 'ملخص الطلب',
	    'TOTAL'	=> 	'مجموع',
		'CONTINUE'=>'استمر',
		'Select_Business_Type'=>'حدد نوع العمل',
'Select_Occasion_Type'=>'اختر نوع المناسبة',
'HOTEL_HALLS'=>'فندق هال',

'About_Shop'=>'عن شوب',
'Video'=>'فيديو',
'Choose_Package'=>'اختر الحزمة',

//Wisitech language
 
 /*start Ajit*/
     'ADDRESS'	                             => 'عنوان',
     'MyAccount'	                         => 'حسابي',
     'MyDashboard'	                         => 'لوحة القيادة',
     'MyProfile'	                         => 'ملفي',
     'TelephoneNumber'	                     => 'رقم هاتف',
     'GENDER'	                             => 'جنس',
     'DATE_OF_BIRTH'	                     => 'تاريخ الولادة',
     'MY_PAYMENT_METHOD'	                 => 'طريقة الدفع او السداد',
     'PIN_CODE'	                             => 'الرقم السري',
     'UPLOAD_YOUR_NATIONAL_ID'	             => 'الصوره الشخصيه',
     'SAVE'	                                 => 'حفظ',
     'MALE'	                                 => 'الذكر',
     'FEMALE'	                             => 'إناثا',
	 'BOTH'	                             => 'على حد سواء',
     'PLEASE_ENTER_YOUR_NAME'                => 'من فضلك أدخل إسمك',
     'ENTER_YOUR_NAME_HERE_MAX'              => 'الرجاء إدخال ما لا يزيد عن 100 حرف',
     'PLEASE_ENTER_YOUR_EMAIL'               => 'رجاءا أدخل بريدك الإلكتروني.',
	 'ENTER_YOUR_EMAIL_HERE_VALID'           => 'الرجاء إدخال عنوان بريد إلكتروني صالح.',
	 'ENTER_YOUR_TEEPHONE_NUMBER_HERE'       => 'يرجى إدخال رقم الهاتف الخاص بك.',
	 'SELECT_YOUR_GENDER_HERE'               => 'يرجى تحديد جنسك.',
	 'ALLOW_ONLY_DIGITS'                     => 'اسمح فقط للأرقام.',
	 'ENTER_YOUR_NUMBER_HERE_MIN'            => 'يرجى إدخال 5 أرقام على الأقل.',
	 'ENTER_YOUR_NUMBER_HERE_MAX'            => 'يرجى التحقق من رقم هاتفك.',
	 'ENTER_YOUR_DOB_HERE'                   => 'يرجى اختيار dob الخاص بك.',
	 'SELECT_YOUR_CITY_HERE'                 => 'يرجى اختيار مدينتك.',
	 'ENTER_YOUR_PIN_CODE_HERE'              => 'يرجى إدخال رمز التعريف الشخصي الخاص بك.',
	 'ENTER_YOUR_PIN_CODE_HERE_MAX'          => 'يرجى التحقق من الرقم السري الخاص بك.',
	 'PLEASE_ENTER_YOUR_ADDRESS'             => 'الرجاء إدخال عنوانك.',
	 'WELCOME'                               => 'أهلا بك',
	 'ChangePassword'                        => 'تغيير كلمة السر',
	 'OLD_PASSWORD'                          => 'كلمة المرور القديمة',
	 'NEW_PASSWORD'                          => 'كلمة السر الجديدة',
	 'CONFIRM_PASSWORD'                      => 'تأكيد كلمة المرور',
	 'PLEASE_ENTER_YOUR_OLD_PASSWORD'        => 'يرجى إدخال كلمة المرور القديمة.',
	 'PLEASE_ENTER_YOUR_NEW_PASSWORD'        => 'يرجى إدخال كلمة المرور الجديدة.',
	 'PLEASE_ENTER_YOUR_CONFIRM_PASSWORD'    => 'يرجى إدخال كلمة المرور الخاصة بك.',
	 'PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN'    => 'يرجى إدخال 8 أحرف على الأقل.',
	 'PLEASE_ENTER_YOUR_PASSWORD_MATCH'      => 'من فظلك ادخل نفس القيمة مرة أخرى.',
	 'MY_ACCOUNT_OCCASIONS'                  => 'مناسبات',
	 'MY_ACCOUNT_STUDIO'                     => 'تفاصيل الاستوديو',
	 'MY_ACCOUNT_SECURITY'                   => 'أموال الأمن',
	 'MY_WALLET'                             => 'محفظة نقود',
	 'MY_ACCOUNT_REVIEWS'                    => 'التعليقات',
	 'INSURANCE'                             => 'تأمين',
	 'Sl_No'                                 => 'و. لا.',
	 'OCCASIONS_NAME'                        => 'اسم المناسبة',
	 'ORDER_ID'                              => 'رقم التعريف الخاص بالطلب',
	 'DATE'                                  => 'تاريخ',
	 'ALL'                                   => 'الكل',
	 'UPLOAD_YOUR_PHOTO'                     => 'رفع صورتك',
	 'VENUE'                                 => 'مكان',
	 'UPLOAD_YOUR_PIC'                       => 'تحميل الصورة الخاصة بك',
	 'DELETES'                               => 'حذف',
	 'OCCASION_TYPE'                         => 'نوع المناسبة',
	 'AMOUNT'                                => 'كمية',
	 'STATUS'                                => 'الحالة',
	 'ADD_SAR'                               => 'إضافة مبلغ المحفظة',
	 'WALLET_BALANCE'                        => 'رصيد المحفظة',
	 'CHOOSE_AN_AMOUNT'                      => 'اختر مبلغًا',
	 'ENTER_AMOUNT'                          => 'أدخل المبلغ',
	 'CREDIT_CARD'                           => 'بطاقة ائتمان',
	 'DEBIT_CARD'                            => 'بطاقة ائتمان',
	 'NET_BANKING'                           => 'صافي المصرفية',
	 'CHOOSE_AN_OPTION'                      => 'إختر خيار',
	 'CONTINUE'                              => 'استمر',	 
	 'TYPE_OF_OCCASION'                      => 'نوع المناسبة',
	 'OCCASION_DATE'                         => 'تاريخ المناسبة',
     'SERVICES_PROVIDER'                     => 'مقدم الخدمة',
     'REVIEW' 						         => 'إعادة النظر',
     'WRITEYOUR_REVIEW' 					 => 'اكتب مراجعتك',
     'UPCOMING_OCCASIONS' 					 => 'المناسبات القادمة',
	 'LATEST_PHOTO_GALLERY' 				 => 'أحدث معرض الصور',
     'ADD_MONEY' 				             => 'إضافة المال',
     'LATEST_REVIEW' 				         => 'آخر مراجعات',
     'ALL_REVIEWS' 				             => 'آخر مراجعات...',
     'NO_OF_ATTENDANCE' 				     => 'عدد الحضور',
     'LIST_OF_SERVICES' 				     => 'قائمة الخدمات',
     'DELIVERY_DATE' 				         => 'تاريخ التسليم او الوصول',
     'TOTAL_PRICE' 				             => 'السعر الكلي',
     'BACK_TO_STUDIO_LIST' 				     => 'العودة إلى صفحة إدراج استوديو.',
     'BACK_TO_OCCASION_LIST' 				 => 'العودة إلى صفحة قائمة المناسبات',
     'PLEASE_ENTER_YOUR_COMMENT'             => 'يرجى إدخال رأيك',
	 'Other_Branches'                        => 'الفروع الاخرى',
	 'Select_Branch'                         => 'اختر الفرع',
	 'ENTER_YOUR_STUDIO_DATE'                => 'أدخل تاريخك',
	 'SELECT_YOUR_OCCASION'                  => 'يرجى تحديد مناسباتك.',
     'ENTER_YOUR_VENUE'                      => 'أدخل المكان الخاص بك',
     'PLEASE_UPLOAD_IMAGE'                   => 'يرجى تحميل الصورة.',
     'VALIDATION_VALID_IMAGE'                => 'يرجى تحميل صورة صالحة.',
     'MER_SERVICE_ADDMORE'                   => 'أضف المزيد',
     'VIEW_ALL'                              => 'عرض الكل',
     'NO_RECORD_FOUND'                       => 'لا يوجد سجلات',
     'ABOUT_SINGER'                          => 'حول سنجر',
     'MORE'                                  => 'أكثر من',
     'LESS'                                  => 'أقل',
     'WHAT_OUR_CLIENT'                       => "ماذا يقول عميلنا",     
     'SINGER_INFORMATION'                    => "معلومات سنجر",     
     'LOCATION'                              => "موقعك",
     'HALL'                                  => "صالة",
     'DURATION'                              => "مدة (ساعة)",     
     'TIME'                                  => 'زمن',     
     'SELECT_TIME'                           => 'اختر الوقت',     
	 'COMMENTS'                              => 'تعليقات',
	 'ASK_FOR_A_QUOTE'                       => 'تعليقات',
     'ENTER_YOUR_HALL'                       => 'أدخل ولايتك',
     'PLEASE_YOUR_ENTER_LOCATION' 		     => 'يرجى إدخال الموقع',
     'PLEASE_ENTER_YOUR_DURATION'            => 'يرجى إدخال مدته.',
     'PLEASE_ENTER_YOUR_DATE'                => 'يرجى إدخال تاريخك.',
     'PLEASE_ENTER_YOUR_TIME'                => 'يرجى إدخال وقتك.',
     'ENTER_COMMENTS_QUERIES' 		         => 'أدخل تعليقاتك',
     'ABOUT_BAND'                            => 'حول الفرقة الشعبية',
     'BAND_INFORMATION'                      => 'شعبية فرقة المعلومات',
     'ENQUIRY_SUCCESSFULLY'                  => 'تحقيق',     
     'ABOUT_ACOUSTIC'                        => 'حول الصوتية',
     'RECORDING'                             => 'تسجيل',
     'EQUIPMENT'                             => 'الرجعية',
     'THE_GROOM_NAME'                        => "اسم العريس",
     'RECORDING_SECTION'                     => 'قسم التسجيل',
     'MUSIC_TYPE'                            => 'نوع الموسيقى',
     'SINGER_NAME'                           => 'اسم المغني',
     'SONG_NAME'                             => 'إسم الأغنية',     
     'PLEASE_YOUR_ENTER_GROOM_NAME' 		 => "يرجى إدخال اسم العريس.",
     'PLEASE_YOUR_ENTER_RECOR_SEC' 		     => 'يرجى إدخال قسم التسجيل.',
     'PLEASE_YOUR_ENTER_MUSIC_TIPE' 		 => 'يرجى إدخال نوع الموسيقى.',
     'PLEASE_YOUR_ENTER_SINGER_NAME' 		 => 'يرجى إدخال اسم المغني.',
     'PLEASE_YOUR_ENTER_SONG_NAME' 		     => 'الرجاء إدخال اسم الأغنية.',     
     'GET_A_QUOTE' 		                     => 'إقتبس',     
     'PLEASE_ENTER_RATING' 		             => 'يرجى إدخال التصنيف.',
     'NOTES' 		                         => 'ملاحظات',
     'RENT' 		                         => 'تأجير',
     'BUY' 		                             => 'يشترى',
     'ADD_TO_CART'                           => 'أضف إلى السلة',
     'REQUEST_A_QUOTE' 		                 => 'اطلب اقتباس',
     'QUOTE_FOR' 		                     => 'اقتباس ل',
     'REQUEST_DATE' 		                 => 'تاريخ الطلب',
     'REPLY_BY' 		                     => 'الحالة',
     'ARABIC' 		                         => 'الإنجليزية',
     'PLEASE_ENTER_AMOUNT'                   => 'يرجى إدخال المبلغ.',     
     'SELECT_QUOTE'                          => 'اختر اقتباس',
     'SINGER'                                => 'مطرب',
     'POPULAR_BAND'                          => 'فرقة شعبية',
     'RECORDING'                             => 'تسجيل',
     'REQUEST_A_QUOTE_DETAIL'                => 'اطلب تفاصيل اقتباس',     
     'CONFIRMED'                             => 'تم تأكيد',
     'DENY'                                  => 'أنكر',
     'BACK_TO_REQUEST_QUOTE'                 => 'العودة إلى طلب صفحة اقتباس',
     'WAITING'                               => 'انتظار',
     'REPLY_BY_MERCHANT'                     => 'رد من التاجر',     
     'EQUIPMENT_FOR_RENT'                    => 'معدات للايجار',
     'OCCASIONS_DATE'                        => 'تاريخ مناسبات',
     'RETURN_DATE'                           => 'تاريخ العودة',
     'INSURANCE_AMOUNT'                      => 'مبلغ التأمين',
     'BOOK_NOW'                              => 'احجز الآن',
     'QUANTITY'                              => 'كمية',
     'SHOP_BY'                               => 'تسوق من قبل',
     'PER_OURR' 				             => 'في الساعة',
     'PLEASE_ENTER_OCCASSION_DATE' 		     => 'يرجى إدخال تاريخ المناسبات.',
     'PLEASE_ENTER_RETURN_DATE' 		     => 'يرجى إدخال تاريخ العودة.',
     'PLEASE_ENTER_INSUR_AMOUNT' 		     => 'الرجاء إدخال مبلغ التأمين.',
     'ABOUT_CENTER' 		                 => 'حول المركز',
     'PACKAGE_SECHEDULE' 		             => 'جدول الباقة',  
     'PACKAGE_DURATION' 		             => 'مدة الحزم',
     'SERVICE_DAY' 		                     => 'أيام الخدمة',
     'EXTRS_SERVICE' 		                 => 'خدمة إضافية',
     'PLEASE_SELECT_PACKAGE_SECHEDULE'       => 'يرجى تحديد جدول الحزمة الخاصة بك.',
     'OTHER_SERVICE'                         => 'خدمات أخرى',
     'PRICE_PER_KM' 		                 => 'سعر الكيل',
     'CAR_MODEL' 		                     => 'طراز السيارة',
     'SERVICE_DATE' 		                 => 'تاريخ الخدمة',
     'ABOUT_SHOP'                            => 'حول المحل',
     'BODY_MEASUREMENT'                      => 'قياس الجسم',
     'LENGTH'                                => 'الطول',
     'CHEST_SIZE'                            => 'حجم الصدر',
     'WAIST_SIZE'                            => 'مقاس الخصر',
     'SHOULDERS'                             => 'أكتاف',
     'NECK'                                  => 'العنق',
     'ARM_LENGTH'                            => 'طول الذراع',
     'WRIST_DIAMETER'                        => 'قطر المعصم',
     'CUTTING'                               => 'قطع',
     'NORMAL'                                => 'عادي',
     'AMPLE'                                 => 'فسيح',
     'CURVED'                                => 'منحن',
     'CM'                                    => 'سم',
     'CONTINUE_WITH_THESE_MEASURES'          => 'تابع مع هذه التدابير',
     'SIZE_DESSESS'                          => 'بحجم',
     'PRICE_FOR_PURCHASE'                    => 'سعر الشراء',
     'SELECT'  		 				         => 'تحديد',
     'CALIBER'                               => 'قالب',
     'WEIGHT'                                => 'وزن',
     'GRAM'                                  => 'غرام',
     'WRITE_ON_YOUR_RING'                    => 'اكتب على خاتمك',     
     'PLEASE_ENTER_YOUR_LENGTH'              => 'يرجى إدخال طولك.',
     'ENTER_YOUR_MAX'                        => 'يرجى إدخال الطول الأقصى الخاص بك.',
     'PLEASE_ENTER_YOUR_CHEST_SIZE'          => 'يرجى إدخال حجم صدرك.',
     'PLEASE_ENTER_YOUR_WAIST_SIZE'          => 'يرجى إدخال حجم الخصر.',
     'PLEASE_ENTER_YOUR_SHOULDERS'           => 'يرجى إدخال كتفيك.',
     'PLEASE_ENTER_YOUR_NECK'                => 'يرجى إدخال رقبتك.',
     'PLEASE_ENTER_YOUR_ARM_LENGTH'          => 'يرجى إدخال طول ذراعك.',
     'PLEASE_ENTER_YOUR_WRIST_DIA'           => 'يرجى إدخال قطر معصمك.',
     'PLEASE_SELECT_YOUR_SIZE'               => 'يرجى تحديد حجم الخاص بك.',
     'PLEASE_SELECT_REDIO_BUTTON'            => 'يرجى اختيار القطع الخاص بك.',
     'READY_MADE'                            => 'جاهز',
     'TAILOR'                                => 'خياط',
     'SORRY_QTY_PRODUCT'                     => 'آسف! كمية مختارة غير متوفرة لهذا المنتج',
     'SHOPPING'                              => 'التسوق',
     'ABAYA'                                 => 'العباءة',
     'CAR_RENTAL'                            => 'تاجير سيارة',
     'TRAVEL_AGENCY'                         => 'وكالة سفر',
     'MUSIC'                                 => 'موسيقى',
     'GOLD_AND_JEWELRY'                      => 'مجوهرات ذهبيه',
     'PERFUME'                               => 'عطر',
     /*End Ajit*/

     'View_Details' => 'عرض التفاصيل',
    'Within_Budget' => 'ضمن الميزانية',
    'Above_Budget' => 'فوق الميزانية',
    'Offers' => 'عروض',
'sales_rep_code' =>'كود الإحالة',
'KOSHA' =>'Kosha',
'PHOTOGRAPHY_STUDIO'=>'استوديو تصوير',
'RECEPTION_AND_HOSPITALITY'=>'الاستقبال والضيافة',
'special_event'=>'حدث مميز',
'Design_Your_Kosha'=>'تصميم كوشا الخاص بك',
'Ready_Made_Kosha'=>'استعداد كوشا',
'No_product_found_in_this_category' =>'لا يوجد منتج في هذه الفئة',
'Your_Selections' =>'اختياراتك',
'Total_Price' =>'السعر الكلي',
'Add_to_Cart'=>'السعر الكلي',
'Photography'=>'التصوير',
'Videography'=>'بالفيديو',
'Duration_of_Video'=>'مدة الفيديو (ساعات)',
'Number_of_Cameras'=>'عدد الكاميرات',
'Number_of_Pictures'=>'عدد الصور',
'ROSES'=>'الورود',
'Package'=>'حزمه',
'Single'=>'واحد',


'Personal_Info'=>'معلومات شخصية',
'Order_Summary'=>'ملخص الطلب',
'Name'=>'اسم',
'Email'=>'البريد الإلكتروني',
'Telephone_Number'=>'رقم هاتف',
'Gender'=>'جنس',
'Date_of_Birth'=>'تاريخ الولادة',
'DD'=>'DD',
'MM'=>'MM',
'YYYY'=>'YYYY',
'Country'=>'بلد',
'Pin_Code'=>'الرقم السري',
'Order_Summary'=>'ملخص الطلب',
'Cash_on_Delivery_of_following_products_and_services'=>'النقد عند تسليم المنتجات والخدمات التالية',
'Select_Shipping_Companies_of_Following_Product'=>'حدد شركات الشحن للمنتجات التالية',
'Payment_Option'=>'خيار الدفع',
'Mobile_Wallets'=>'محافظ المحمول',
'Wire_Transfer'=>'تحويلة كهربية بالسلك',
'Male'=>'الذكر', 
'Female'=>'إناثا', 
'Credit_Debit_Cards'=>'بطاقات الائتمان / الخصم', 
'Total_Price'=>'السعر الكلي', 
'Pay_Now'=>'ادفع الآن', 
'PLEASE_ENTER_YOUR_PHONE' =>'يرجى إدخال رقم الهاتف الخاص بك',
'PLEASE_CHOOSE_YOUR_GENDER' =>'يرجى اختيار جنسك',
'PLEASE_SELECT_DAY' =>'يرجى اختيار اليوم',
'PLEASE_SELECT_MONTH' =>'يرجى اختيار الشهر', 
'PLEASE_SELECT_YEAR' =>'يرجى اختيار السنة', 
'PLEASE_ENTER_YOUR_PINCODE'=>'يرجى إدخال الرمز السري الخاص بك',
'PLEASE_CHOOSE_SHIPPING_METHOD' =>'يرجى اختيار طريقة الشحن',
'PLEASE_CHOOSE_PAYMENT_METHOD'=>'يرجى اختيار طريقة الدفع',
'BOOKINGFOR'=>'يرجى تحديد موعد ل',
'BOOKINGDATE'=>'يرجى اختيار تاريخ الحجز',
'BOOKINGTIME'=>'يرجى اختيار وقت الحجز',
'checkout_thank_you'=>'شكرا لطلبك',
'Select'=>'تحديد',
'PLEASE_SELECT_COUNTRY'=>'يرجى اختيار بلدك',
'Aramex'=>'أرامكس',
'Pick_Logistics'=>'انتقاء اللوجستيات',
'Pay_Now'=>'ادفع الآن',
'VAT'=>'ضريبة',
'Wallet_Deduct'=>'مبلغ المحفظة',
	];
?>
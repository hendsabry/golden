<?php

return

[

/*sratr back end static pages*/

/*start merchant_header.blade.php*/

'MER_HOME' 										=> 			'الصفحة الرئيسية',

'MER_SHOP' 					    => 			'متجر',

'DETAIL_INFO'=>'تفاصيل',

'LOGO' 							=> 			'الشعار',
'MER_VALID_IMAGE'					=> 			'قبول فقط JPG ، بابوا نيو غينيا ، JPEG ، تنسيق GIF.',

'MER_RENT' => 'تأجير',

'HI' 							=> 			'مرحباً',

'PROFILE' 						=> 			'الملف الشخصي',

'SETTINGS' 						=> 			'بيانات التاجر',

'LOGOUT' 						=> 			'تسجيل الخروج',

'MER_SENT_ON'                   =>          'أرسلت في',

'EXTRS_SERVICE' 		                 => 'خدمات إضافية',

'DASHBOARD' 					=> 			'لوحة التحكم',

'PRODUCTS' 						=> 			'المنتجات',

'NOTIFICATION' 		                 => 'الإشعارات',

'MER_TOTAL_BALANCE'									=>		'الرصيد الإجمالي (ر.س)',

'PACKAGE_DATE' 		                 => 'تاريخ الباقة',

'TRANSACTIONS' 					=> 			'المعاملات',

'FUND_REQUESTS' 				=> 			'طلبات الدفع',

'CLINIC_NAME'                           => 'اسم العيادة',

'BACK_SUBSCRIPTION_MANAGEMENT'	     		=> 	'إدارة الإشتراك',

'DEPARTMENT'                            => 'القسم',

'DR_NAME'                               => 'اسم الطبيب',

'REQUEST_FOR_AMT_RETURN'	=> 'طلب إرجاع التأمين',

'REQUEST_FOR_AMT_RETURN_INSUR'	=> 'المبلغ المودع',

'DEPOSITED_AMOUNT'	=> 'المبلغ المودع',

'RETURN_AMOUNT'	=> 'مبلغ الإرجاع',

'MER_REFUND'											=> 	'إرجاع المبلغ',

'MER_PR' 		                   => 'السعر',

'INSURANCEAMOUNT' => 'مبلغ التأمين',

'ALL' 						    => 			'الكل',

'MER_COMMSSION_BALANCE'									=>		'العمولة',

'NO_OF_DAY'                               => 'عدد الأيام',

'STORES' 						=> 			'المتاجر',

'WAIT_FOR_ORDER_COMPLETE'                  => 'بإنتظار إكتمال الطلب',

'VAT_CHARGE'                            => 'ضريبة',

'Total_Price' =>'السعر الكلي',

'TOTAL_PRICE' =>'السعر الكلي',

'FLOWER_TYPE'                           => 'نوع الورد',

'RETURNING_DATE'     			         => 'تاريخ الإرجاع',

'WRAPPING_TYPE'                         => 'نوع التغليف',

'WRAPPING_DESIGN'                       => 'تصميم التغليف',

'LOCATION'                              => "الموقع",

'Approx_deleivery_date_after' =>' أيام',

'Single'                        =>          'ورد مفرد',

'DEALS' 						=> 			'صفقات',

'MER_SERVICES' 					=> 			'الخدمات',

'MER_DURATION' 				    => 			'المدة',

'CAR_MODEL'                     => 'طراز السيارة',

'WRITEMESSAGE'                  =>'أدخل النص',

'PLEASE_ENTER_URL' => 'الرجاء إدخال رابط الفيديو',

'MER_GOLD_PRICE' 					=> 			'سعر الذهب',

'PRICE_OF_WORKMANSHIP'=>'سعر المصنعية',

'Complete_all_the_price'=>'أكمل جميع حقول السعر',



'STAFF_CHAGES'   			=> 	'رسوم العامل',

'Price'                                  => 'السعر',

'PRODUCT_WEIGHT'                => 'وزن المنتج',



/*end merchant_left_menu_action.blade.php*/

/*start merchant_left_menu_deals.blade.php*/

'DEALS1' 						=> 			'صفقات',

'ADD_DEALS' 					=> 			'أضف الصفقات',

'MANAGE_DEALS' 					=> 			'إدارة الصفقات',

'EXPIRED_DEALS' 				=> 			'الصفقات المنتهية',

'MER_MUSIC_TYPE' 			    => 		    'نوع الموسيقى',

'MER_SONG_NAME' 			    => 		    'إسم الأغنية',

/*end merchant_left_menu_deals.blade.php*/

/*start merchant_left_menu_fund.blade.php*/

'FUND_REQUEST_REPORT' 			=> 			'تقرير طلبات الأموال',

'WITHDRAW_FUND_REQUEST' 		=> 			'طلب الدفع',

/*end merchant_left_menu_fund.blade.php*/

/*start merchant_left_menu_product.blade.php*/

'PRODUCTS1' 					=> 			'المنتجات',

'ADD_PRODUCTS' 					=> 			'إضافة منتجات',

'MANAGE_PRODUCTS' 				=> 			'إدارة المنتجات',

'SOLD_PRODUCTS' 				=> 			'المنتجات المباعة',

'SHIPPING_AND_DELIVERY' 		=> 			'الشحن والتسليم',

'CASH_ON_DELIVERY' 				=> 			'الدفع عن الاستلام',

'MER_Price'						=> 			'السعر',

/*end merchant_left_menu_product.blade.php*/

/*start merchant_left_menu_settings.blade.php*/

'SETTINGS1' 					=> 			'الإعدادات',

'EDIT_ACCOUNT' 					=> 			'تعديل معلومات الحساب',

'CHANGE_PASSWORD' 				=> 			'تغيير كلمة المرور',

'ATTRIBUTES_MANAGEMENT' 		=> 			'إدارة الخصائص',

'ADD_COLOR' 					=> 			'أضف لون',

'MANAGE_COLORS' 				=> 			'إدارة الألوان',

'MR_SHIPPING_METHOD'=>'طريقة الشحن',

'MR_SHIPPING_CHARGE'=>'رسوم الشحن',

'Quantity'=>'الكمية',

'ADD_SIZE' 						=> 			'إضافة مقاس',

'MANAGE_SIZES' 					=> 			'إدارة المقاسات',

/*end merchant_left_menu_settings.blade.php*/

/*start merchant_left_menu_shop.blade.php*/

'MANAGE_STORES' 				=> 			'إدارة المتاجر',

'ADD_STORES' 					=> 			'إضافة متاجر',

/*end merchant_left_menu_shop.blade.php*/

/*start merchant_left_menu_transaction.blade.php*/

'TRANSACTIONS1' 				=> 			'المعاملات',

'DEALS_TRANSACTION' 			=> 			'معاملات الصفقات',

'ALL_ORDERS' 					=> 			'جميع الطلبات',

'SUCCESS_ORDERS' 				=> 			'الطلبات الناجحة',

'COMPLETED_ORDERS' 				=> 			'الطلبات المكتملة',

'HOLD_ORDERS' 					=> 			'الطلبات المعلقة',

'FAILED_ORDERS' 				=> 			'الطلبات الفاشلة',

'DEALS_COD' 					=> 			'صفقات الدفع عند الاستلام',

'PRODUCTS_TRANSACTION' 			=> 			'معاملات المنتجات',

'PRODUCTS_COD' 					=> 			'منتجات الدفع عند الاستلام',

'MER_PRODUCT_BULK_UPLOAD'       =>          'رفع مجموعة منتجات',

'PAYU_SHIPPING_DELIVERY'  		=> 			'Payfort الشحن التسليم',

'MER_S.NO'						=>			'متسلسل',

'MER_COMMISSION_PAID'			=>			'العمولة المدفوعة',

'MER_MERCHANT_NAME'				=>			'مجموع الطلب',

'MER_TOTAL_ORDER_AMOUNT'		=>			'قائمة العمولات',

'MER_TRANSACTION_ID'			=>			'رقم المعاملة',

'MER_COMMISSION_LISTING'		=>			'قائمة العمولات',		

'MER_AMOUNT'					=>			'المبلغ',

'MER_PAID_AMOUNT' 				=> 		'المبلغ المدفوع',

'MER_EDIT_MERCHANT_STORE_ACCOUNT' =>'تعديل حساب المتجر',

'MER_TRANSACTION_ID'			=>			'رقم المعاملة',

'MER_TRANSACTION_ID'			=>			'رقم المعاملة',

'MER_TRANSACTION_ID'			=>			'رقم المعاملة',

'MER_TRANSACTION_ID'			=>			'رقم المعاملة',

'MER_PAYMENT_TYPE'				=>			'نوع الدفع',

'MER_DATE'						=>			'التاريخ',

'MER_STATUS'					=>			'الحالة',

'MER_COMMSSION_TO_PAID'			=>			'العمولة المدفوعة',

'MER_PENDING'					=>			 'قيد الانتظار',

'MER_ACTIVE'					=>			 'نشط',

'MER_MERCHANT_DASHBOARD'		=>			'لوحة تحكم التاجر',

'MER_MONTH_WISE_TRANSACTIONS'	=>		'المعاملات الشهرية',

'SERVICES_NAME'					=>		'اسم الخدمة',

'PAYMENT_STATUS'				=>		'حالة الدفع',

'FILE_NO'						=>		'رقم الملف',

'MER_ORDER_DATE'						=>	'تاريخ الطلب',

'MER_TRANSACTION'						=>	'المعاملة',

/* ARUn Start*/

'STYLE_AND_FABRIC_INFO'                 => 'معلومات التصميم والقماش',

'STYLE'                                 => 'التصميم',

'STYLE_PRICE'                           => 'سعر التصميم',

'STYLE_IMAGE'                           => 'صورة التصميم',

'FABRICS_NAME'                          => 'اسم القماش',

'FABRICS_PRICE'                         => 'سعر القماش',

'FABRICS_IMAGE'                         => 'صورة القماش',

'QUANTITY'                              => 'الكمية',

'DELIVERY_DATE'                         => 'تاريخ التسليم',

'BODY_MEASUREMENT'                      => 'القياسات',

'LENGTH'                                => 'الطول',

'CHEST_SIZE'                            => 'قياس الصدر',

'SIZE'									=>'المقاس',

'DRESS'                                 =>'الفساتين',

'RENT_CHARGES'                          => 'رسوم الإيجار / في اليوم',

'WAIST_SIZE'                            => 'قياس الخصر',

'SHOULDERS'                             => 'قياس الأكتاف',

'NECK'                                  => 'قياس الرقبة',

'ARM_LENGTH'                            => 'طول الذراع',

'WRIST_DIAMETER'                        => 'فتحة الكم',

'CUTTING'                               => 'شكل القصة',

'NORMAL'                                => 'عادي',

'AMPLE'                                 => 'واسع',

'CURVED'                                => 'منحني',

'CM'                                    => 'سم',

'Photography'=>'التصوير الفوتغرافي',

'Videography'=>'الفيديو',

'Duration_of_Video'=>'مدة الفيديو (ساعات)',

'Number_of_Cameras'=>'عدد الكاميرات',

'Number_of_Pictures'=>'عدد الصور',		

'Dashboard'	=> 'لوحة التحكم',

'Vendor_Info'	=> 'معلومات التاجر',

'Services'	=> 'الخدمات',

'Shipping_Option'=> 'خيارات الشحن',

'MER_Services' =>'الخدمات',

'MER_Shipping_Options' => 'خيارات الشحن',

'SHIPPING_NAME' => 'شركة الشحن',

'MER_LIST_SERVICES'=> 'قائمة الخدمات',

'MER_ACTION' => 'إجراء',

'MER_ADD_SERVICES'=> 'إضافة خدمات',

'MER_ADD_HALLSERVICE'=>'إضافة خدمات القاعة',

'MER_SERVICE_NOTES'=>'ملاحظات',

'MER_SERVICE_NAME'=>'اسم الخدمة',

'MER_FREESERVICE'=>'الخدمات المجانية',

'MER_ADD_HALLPAIDSERVICE'=>'الخدمات المدفوعة',

'MER_SERVICE_PRICE'=>'السعر (ر.س)',

'MER_SERVICE_ADDMORE'=>'إضافة المزيد',

'MER_HALL_PICTURE'=>'صور وفيديو القاعة',

'MER_UPLOAD_HALLPICTURTE'=>'رفع صور القاعة',

'MER_HALL_UPLOADVIDEO'=>'أدخل رابط الفيديو على اليوتيوب',

'MER_HALL_INFO'=>'معلومات القاعة',

'MER_HALL_NAME'=>'اسم القاعة',

'MER_HALL_IMAGE'=>'صورة القاعة',

'MER_GOOGLE_MAP_ADDRESS'=>'رابط الموقع في قوقل',

'MER_HALLDIM'=>'ابعاد القاعة',

'MER_FOOD'=>'الطعام',

'MER_HALL_TYPE'=>'نوع القاعة',

'MER_HALL_PRICE'=>'سعر القاعة (ر.س)',

'MER_HALLarea'=>'المساحة',

'MER_HALLWIDTH'=>'العرض',

'MER_HALLLENGTH'=>'الطول',

'MER_HALLINTERNAL'=>'قائمتنا',

'MER_HALLEXTERNAL'=>'طعام خارجي',

'MER_WOMEN'=>'نساء',

'MER_MEN'=>'رجال',

'MER_ADDRESSIMG'=>'صورة الموقع',

'MER_HALLPICTUREANDVIDEO'=>'صور وفيديو القاعة',

'MER_HALLSERVICES'=>'خدمات القاعة',

'MER_HALLMENUCATEGORY'=>'تصنيفات القائمة',

'MER_HALLMENU'=>'قائمة الطعام',

'MER_HALLDishes'=>'الأطباق',



'MER_Offer'=>'العروض',

'MER_Order'=>'الطلبات',

'MER_Order_Detail'=>'تفاصيل الطلب',

'MER_Reviewandcomments'=>'التقييم والتعليقات', 

'MER_SUBMIT'=>'إرسال',

'MER_Delete'=>'حذف',

'MER_CUSTOMERNAMEEMAIL'=>'اسم العميل/البريد الإلكتروني',

'MER_CUSTOMERMOBILE'=>'رقم الجوال',

'MER_ORDERSTATUS'=>'حالة الطلب',

'MER_BOOKINGDATE'=>'تاريخ الحجز',

'MER_AMOUNT'=>'القيمة (ر.س)',

'MER_PAYMENTSTATUS'=>'حالة الدفع',

'MER_ACTION'=>'إجراء',

'MER_REVIEWANDRATING' => 'المراجعة والتقييم',

'MER_OFFER'=>'العروض',

'MER_DEACTIVE' =>'تعطيل',

'MER_LIST_OF_SERVICES'=>'قائمة الخدمات',

'MER_HallLIST'=>'قائمة القاعات',

'MER_ADDHALL'=>'إضافة قاعة',

'MER_HALLSNAME'=>'اسم القاعة',

'NORECORDFOUND'=>'لا يوجد سجلات,',

'ACTION'=>'إجراء',

'MER_HOTELS'=>'الفنادق',

'MER_HALLIMAGE'	   => 		'صورة القاعة',

'FOOD' => 'الطعام',

'HALLTYPE' => 'نوع القاعة',

'LASTMODDATE'=> 'تاريخ آخر تعديل',

'STATUS'=> 'الحالة',

'HALL_INFO'=> 'معلومات القاعة',

'MER_HALL_BRANCHES'=> 'الفروع',

'MER_BRANCHES'=> 'قائمة الفروع',

'MER_LISTMANAGER'=> 'قائمة المدراء',

'MER_PAYMENT'=> 'الدفع',

'MER_STATUS'=> 'الحالة', 

'MER_EDITHOTALNAME'=>'تعديل الفندق',

'MER_ADD_MANAGER' => 'إضافة مدير',

'MER_UPDATE_MANAGER'=> 'تعديل المدراء',

'PLEASE_ENTER_VALID_IMAGE' => 'الرجاء رفع صورة صالحة',

'MER_ADD_HALL_FREE_SERVICE' => 'إضافة خدمات مجانية للقاعة ',

'MER_ADD_HALL_PAID_SERVICE' => 'إضافة خدمات مدفوعة للقاعة',

'NO_RECORD_FOUND'=> 'لا توجد سجلات',

'MER_DISH' => 'الأطباق',

'Thank_you_for_signing' =>'شكراً للتسجيل معنا. ستتلقى رسالة تأكيد عبر البريد الإلكتروني.',

'MER_ADD_CATEGORY' => 'إضافة تصنيف',

'MER_CATEGORY_LIST'=> 'قائمة التصنيفات',

'MER_CATEGORY_DESCRIPTION'=> 'وصف التصنيف',

'CATEGORY' => 'التصنيفات',

'ITEMINFORMATIONLIST' => 'قائمة معلومات الصنف',

'MER_IMAGE' => 'الصور',

'PRICE' => 'السعر (ر.س)',

'ADD_ITEM' => 'إضافة صنف',

'MER_Time' => 'الوقت',

'ITEMNAME'  => 'اسم الصنف',

'UPLOADIMAGE' => 'رفع صورة',

'DESCRIPTION' => 'الوصف',



'UPDATE_ITEM' => 'تعديل الصنف',

'UPDATE' => 'تحديث',

'SELECT' => 'إختيار',

'USERIMAGE' => 'الصورة الشخصية',

'MER_VALIDATION_VALID_PDF_FILE' => 'pdf يرجى رفع ملف', 

'UPLOADCERTIFICATE' => 'رفع الشهادة (إختياري)', 

'MER_VALIDATION_FIRSTNAME' => 'يرجى إدخال الاسم الأول', 

'MER_VALIDATION_LASTNAME' => 'يرجى إدخال الاسم الأخير', 

'MER_VALIDATION_EMAILADDRESS' => 'يرجى إدخال البريد الإلكتروني', 

'MER_VALIDATION_COUNTRY' => 'يرجى إختيار الدولة', 

'MER_VALIDATION_CITY' => 'يرجى إختيار المدينة', 

'MER_VALIDATION_PHONE' => 'يرجى إدخال رقم الهاتف', 

'MER_COUNTRY_CODE_MSG' => 'يرجى إدخال رمز الدولة',

'MER_VALIDATION_ADDRESS' => 'يرجى إدخال العنوان', 

'MER_VALIDATION_ZIP' => 'يرجى إدخال الرمز البريدي', 

'MER_VALIDATION_IMAGE' => 'يرجى إختيار صورة صالحة', 

'EMAILADDRRESS' => 'البريد الإلكتروني',

'MER_VALIDATION_SINGER_NAME' =>'يرجى إدخال اسم المطرب',

'DOWNLOAD' =>'تنزيل',

'MER_VALIDATION_FIRST_NAME_AR'  =>'يرجى إدخال الاسم الأول',

'MER_VALIDATION_LAST_NAME_AR'=> 'يرجى إدخال الاسم الأخير',

'CertificateChamber' =>'السجل التجاري ',

'MER_VALIDATION_TNC' => ' pdf يرجى رفع ملف',

'TERMSANDCONDITIONS' => 'الشروط والأحكام',

'COUPON' => 'كوبون',

'MER_VALIDATION_VALID_FILE'=> 'pdf يرجى رفع ملف',

'MER_VALIDATION_COUPON' => 'يرجى إدخال رقم الكوبون',

'MER_VALIDATION_POPULAR_BAND_NAME' =>'يرجى إدخال اسم الفرقة',

'UPLOAD' => 			'رفع',

'Payment' 	 => 			'الدفع',

'AccountDetail' 	 => 			'معلومات الحساب',

'OrderReport' 	 => 			'تقرير الطلبات',

'ReceivedPayements' 	 =>	'الدفعات المستلمة',

'MER_VALIDATION_COMMENT'=> 'الرجاء كتابة تعليق', 

'MER_VALIDATION_CONTAINER_PRICE'   =>    'يرجى إدخال سعر الحافظة', 

'MER_VALIDATION_CONTAINER_NAME' 		      => 'الرجاء إدخال اسم الحافظة',

'MER_VALIDATION_CONTAINER_NAME_AR' 		   => 'الرجاء إدخال اسم الحافظة بالعربي',  

'PACKEGENAME'=>'اسم الباقة',

'PACKEGINFORMATIONLIST' => 'قائمة الباقات',

'ADD_PACKAGE' => 'إضافة باقة',

'NO_STAFF'=>'عدد العاملين',

'MER_RECEPTION_PICTURE'=>'صور وفيديو المتجر',

'MER_CATEGORY_DESCRIPTION_VALIDATION' => 'يرجى إدخال وصف التصنيف',

'MER_CATEGORY_DESCRIPTION_VALIDATION_AR' => 'يرجى إدخال وصف التصنيف',

'MER_SELECT_NATIONALITY' => 'إختر الجنسية',

'MER_BUFFET_LIST'	  => 	'قائمة الفروع',

'MER_BUFFET_VIDEO'  => 'صور وفيديو البوفيه',

'MER_BRANCHLIST'  => 'قائمة الفروع',

'MER_RESTURANT'=> 'المطعم',

'MER_ADD_RESTURANT'=> 'إضافة مطعم',

'MER_RESTURANT_NAME'=> 'اسم المطعم',

'MER_SERVICE_CATEGORY'=> 'تصنيفات الخدمة',

'LANG'=> 'إختر لغه',

'OPENING'=>  'أوقات الدوام',

'HOMEVISIT'=>  'رسوم الخدمة في المنزل',

'SERVICEAVAIL'=>  'الخدمة متاحة في',

'HOME'=>  'المنزل', 

'MER_SELECT_MANAGER' 						=> 			'إختيار مدير',

'MER_CONFIRM'	  => 	'تأكيد',

/*new check*/

'Design_your_Package'=> 'صمم باقتك',

'Number_of_Staff'=>'عدد العاملين',

'Nationality'=>'الجنسية',

'MER_EDIT' => 'تعديل',

'Add_subscriptionAmount' => 'رسوم الإشتراك',

'MER_UPDATE_CATEGORY'=> 'تعديل التصنيف',



'MER_VALIDATION_HALL_CAPACITY'=> 'يرجى إدخال سعة القاعة',

'People'=>'عدد الاشخاص ',

'CAPACITY' => 'السعة', 

'MER_VALIDATION_LAST_NAME_AR'=> 'الاسم الأخير',

'MER_CATEGORY_NAME_VALIDATION_AR' => 'يرجى إدخال اسم التصنيف بالعربي',

'MER_CATEGORY_NAME_VALIDATION' => 'يرجى إدخال اسم التصنيف',



'MER_CATEGORY_DESCRIPTION_VALIDATION' => 'الرجاء إدخال وصف التصنيف',

'MER_CATEGORY_DESCRIPTION_VALIDATION_AR' => 'الرجاء إدخال وصف التصنيف بالعربي',

'MER_VALIDATION_SHOP_NAME_AR' =>'يرجى إدخال اسم المتجر',



'MER_VALIDATION_TNC' => 'يرجى تحميل ملف الشروط والأحكام',

'CertificateChamber' => 'السجل التجاري',

'TERMSANDCONDITIONS' => 'الشروط والأحكام',

'TERMSANDCONDITION' => 'الشروط والأحكام',

'MER_SHOP_ADDRES' => 'عنوان المتجر',

'MER_VALIDATION_COUPON'=> 'يرجى إدخال رمز كوبون فريد',

'COUPON' => 'كوبون',



'MER_VALIDATION_VALID_FILE'=> 'يرجى رفع ملف صحيح',



'MER_VALIDATION_FIRST_NAME_AR'=> 'الرجاء إدخال الاسم الأول',



'MER_MENU_CATEGORY'=> 'تصنيفات القائمة',

'MER_UPDATE' 	 	=> 			'تعديل',

'Payment' 	 => 	 'الدفع',

'AccountDetail' 	 => 	 'معلومات الحساب',

'OrderReport' 	 => 		 'تقرير الطلبات',

'ReceivedPayements' 	 => 	 'الدفعات المستلمة',

'MER_VALIDATION_END_DATE' => 'الرجاء تحديد تاريخ الإنتهاء',



'MER_VALIDATION_COUPON' => 'يرجى إدخال رمز كوبون فريد',

'MER_VALIDATION_LAST_NAME_AR' => 'الرجاء إدخال الاسم الاخير',

'MER_VALIDATION_ADDRESS' => 'الرجاء إدخال العنوان',

'MER_UPDATE_BEAUTYSHOP'=> 'تعديل بيانات المشغل',



'MER_GOOGLE_MAP_URL'=> 'رابط الموقع على خرائط قوقل',

'MER_ADDRESS_IMAGE'=> 'صورة العنوان',

'MER_GOOGLE_MAP_URL' => 'رابط الموقع على خرائط قوقل',

'MER_ADDRESS_IMAGE'=> 'صورة الموقع على الخريطة',

'MER_SHOP_ADDRES'=> 'عنوان المتجر',

'MER_UPDATE_OFFER' => 'تعديل العرض',





'PLEASE_ENTER_SERVICE_NAME'=> 'أدخل اسم الخدمة',

'PLEASE_SELECT_CATEGORY'=> 'إختر تصنيف',

'PLEASE_ENTER_DURATION'=> 'أدخل المدة',

'PLEASE_ENTER_AMOUNT'=> 'أدخل المبلغ',

'PLEASE_ENTER_DISCOUNT'=> 'أدخل الخصم',

'PLEASE_ENTER_DISCRIPTION'=> 'أدخل الوصف',



'PLEASE_ENTER_PACKAGE_NAME'=> 'أدخل اسم الباقة',

'PLEASE_ENTER_PRICE'=> 'أدخل السعر',

'PLEASE_ENTER_DURATION'=> 'أدخل المدة',



'You_are_yet_to_get_reviews_or_ratings' => 'لم تحصل بعد على أي تقييم',



'MER_UPLOAD_SHOP_PICTURTE'=>'رفع صور المتجر',

'MER_Dish_Image' => 'صورة الطبق',

'Shop_link_in_maroof'=> 'رابط المتجر في موقع معروف',

'UPDATEWORKER' => 'تعديل بيانات العامل',

'VIEW' => ' عرض ',





'PLEASE_ENTER_BRANCH_NAME' => 'يرجى إدخال اسم الفرع',

'PLEASE_ENTER_ADDRESS' => 'يرجى إدخال العنوان',

'PLEASE_CHOOSE_CITY' => 'يرجى إختيار المدينة',

'PLEASE_CHOOSE_MANAGER' => 'يرجى إختيار مدير',

'PLEASE_CHOOSE_OPENING_TIME' => 'يرجى تحديد موعد بداية الدوام',

'PLEASE_CHOOSE_CLOSING_TIME' => 'يرجى تحديد موعد إنتهاء الدوام',

'PLEASE_ENTER_DESCRIPTION' => 'يرجى إدخال الوصف',

'PLEASE_UPLOAD_TERMSANDCONDITON'=> 'يرجى تحميل الشروط والأحكام ',

'PLEASE_SELECT_SERVICE' => 'يرجى إختيار الخدمات',

'MER_UPDATE_SERVICES' => 'تحديث الخدمة ',



/* ARUn Closed*/





// Ashwini Start



'MER_COMMISSION'					=>		'عمولة',

'MER_SUBSCRIPTION'					=>		'رسوم إشتراك',

'MER_SELECT_MEMBERSHIP_TYPE'		=>      'حدد نوع الإشتراك',

'MER_SELECT_MEMBERSHIP_TYPE'		=>      'حدد نوع الإشتراك',

'MER_SERVICE_NAME'					=>      'اسم الخدمة',

'MER_MEMBERSHIP_TYPE'				=>      'نوع الإشتراك',

'MER_SUBSCRIPTION'					=>      'إشتراك',

'MER_LIST_OF_SERVICES'				=>      'قائمة الخدمات',

'MER_SEARCH'						=>'بحث',

'MER_APPLY'						=>'تطبيق',

'MER_SERVICES'					=>  'خدمات',

'MER_FILTERS'						=>      'مرشحات',

'MER_ADDMORE'						=>      'أضف خدمات',

'MER_BRANCH'							=>'قائمة الفروع',

'MER_MANAGER'							=>'المدراء',

'MER_INACTIVE'							=>'غير نشط',

'MER_VIEW'							=>'عرض',

'MER_COMMENT'							=>'تعليق',

'MER_PRODUCTNAME'							=>'اسم المنتج',

'MER_RATING'							=>'تقييم',





'MER_HOTEL_NAME'							=>'اسم الفندق',

'MER_HOTEL_IMAGE'							=>'شعار الفندق',

'MER_CREATED_DATE'							=>'تاريخ الإنشاء',

'MER_EDIT_HOTEL'							=>'تعديل فندق',

'MER_LIST_BRANCH'							=> 'قائمة الفروع ',

'MER_HOTEL_LIST'						=> 		'قائمة الفنادق',

'MER_ADD_HOTEL'						=> 		'إضافة فندق',

'MER_LIST_BRANCH'					=> 		'قائمة الفروع ',

'MER_BRANCH'						=> 		'الفروع',

'MER_CITY'							=> 		'المدينة',

'MER_MANAGER'						=> 		'المدير',

'MER_IMAGE'							=> 		'صورة',

'MER_LIST_OF_HALL'					=>		'قائمة القاعات',

'MER_HALL_LIST'						=>		'قائمة القاعات',

'MER_EDIT'							=>		'تعديل',

'MER_LIST_OF_BRANCH'				=>	    'قائمة الفروع',

'MER_ADD_BRANCH'					=>		'إضافة فرع',

'MER_SELEST_CITY'				    => 		'إختر مدينة',

'MER_SELEST_MANAGER'			    => 		'حدد مدير الفرع',

'MER_BRANCH_NAME'				    => 		'اسم الفرع',

'MER_DESCRIPTION'				    => 		'الوصف',

'MER_BRANCH_IMAGE'				    => 		'شعار الفرع',

'MER_BRANCH_EDIT'				    => 		'تعديل الفرع',

'MER_UPLOAD'				 	    => 		'رفع ملف',

'MER_TYPE'				 	    => 		'النوع',



'MER_LIST_DISH' 		            => 'قائمة الأطباق', 

'MER_PER_KG'=> 'لكل كغ',





'PACKEGENAME'=>'اسم الباقة;',

'NO_STAFF'=>'عدد العاملين',

'MER_PER_PIECE'=>'بالقطعة',









'MER_VALIDATION_ITEM_NAME' => 'الرجاء إدخال اسم الصنف ',

'MER_VALIDATION_ATTRIBUTE' => 'يرجى تحديد التصنيف',

'MER_VALIDATION_ITEM_NAME_AR' => 'الرجاء إدخال اسم الصنف بالعربي',

'PACKEGINFORMATIONLIST' => 'قائمة الباقات',

'ADD_PACKAGE' => 'إضافة باقة',







/* Start Prabhakar */

'MER_SELECT_CITY'		=>      'إختر مدينة',

'MER_LISTBRANCH' => 'عرض الفروع',

'MER_Container_Name' => 'اسم الحافظة',

'MER_No_People' => 'عدد الاشخاص',

'MER_Price' => 'السعر (ر.س)',

'MER_Type' => 'النوع',

'MER_About' => 'حول',

'MER_Container_Image' => 'الصورة',

'MER_Action' => 'إجراء',

'MER_Short_Code' => 'ادخل الرمز',

'MER_Dish_Name' => 'اسم الطبق',

'MER_Select_Menu' => 'إختر قائمة ',

'MER_Select_Menu_Dish' => 'إختر طبق',

'MER_Email_Address' => 'البريد الإلكتروني',

'MER_User_Image' => 'الصورة',

'MER_FT' => '(متر مربع)',

'MER_SQFT' => '(متر مربع)',

'MER_De_Activate_Record' => 'هل تريد الغاء تنشيط السجل؟',		

'MER_Activate_Record' => 'هل تريد  تنشيط السجل؟',	

'MER_Want_Delete' => 'هل تريد مسح السجل?',

'Singer_Info_Menu' => 'معلومات المطرب',

'Singer_Photo_Video_Menu' => 'صور و فيديو المطرب',

'Review_Comments_Menu' => 'التقييم والتعليقات',

'Quoted_Requested_List_Menu' => 'قائمة عروض الاسعار المطلوبة',

'MER_Singer_Name' => 'اسم المطرب',

'MER_Singer_Image' => 'صورة المطرب',	

'MER_Upload_Picture' => 'رفع صورة',

'MER_Customer_Name' => 'اسم العميل',

'MER_Request_Date' => 'تاريخ الطلب',	

'MER_Request_Date' => 'تاريخ الطلب',	

'MER_Hall' => 'القاعة',	

'MER_Occasion_Type' => 'نوع المناسبة',

'MER_Location' => 'الموقع',

'MER_Duration' => 'المدة',

'MER_Comments' => 'التعليقات',

'MER_Time' => 'الوقت',

'MER_Booking_Date' => 'تاريخ الحجز',

'MER_Returning_Date' => 'تاريخ الإرجاع',

'MER_Total_Price' => 'السعر الإجمالي',

'MER_Paid_Services_Price' => 'سعر الخدمات المدفوعة',

'MER_Total_Price' => 'السعر الإجمالي',

'MER_Advance_Amount' => 'دفعة مقدمة',

'MER_Due_Patment' => 'المبلغ مستحق الدفع',

'MER_Hall_Price' => 'سعر القاعة',

'MER_Paid_Services' => 'الخدمات المدفوعة',

'MER_Dish_Image' => 'صورة الطبق',

'MER_Container_Image' => 'صورة الحافظة',

'MER_Container_Qty' => 'كافية لعدد',

'MER_Pictures_Video'=>'الصور والفيديو',

'MER_Type_Dishes'=>'الأنواع والأطباق',

'MER_Add_Type_Dish'=>'إضافة نوع & طبق',

'MER_Types'=>'أنواع / أطباق',

'MER_Add_Type'=>'إضافة نوع', 



'MER_EMAIL'=> 'البريد الإلكتروني',

'MER_SELECT_COUNTRY'=> 'إختر الدولة',

'MER_PHONE'=> 'رقم الجوال',

'USERIMAGE'=> 'صورة المستخدم',

'MER_EDIT_MERCHANT_ACCOUNT' => 'تعديل الحساب',

'MER_ADDRESS1' => 'العنوان 1',

'MER_ADDRESS2' => 'العنوان 2',

'ZIP_CODE'=> 'الرمز البريدي',



'MER_PASSWORD' => 'كلمة المرور',





'MER_YES' => 'نعم',

'MER_NO' => 'لا',



'MER_About_Singer' => 'حول المطرب',		



/* End Prabhakar */



'BACK_PHONE' => 'رقم الجوال',

'MER_BACK' => 'رجوع',	





'MER_COMMISSION_PAYMENT' => 'طريقة الدفع او السداد',

//Himanshu Start for language Menu Category



//Himanshu Start for language Menu Category



'MER_ADD_MENU_CATEGORY'=> 'إضافة تصنيف للقائمة',

'MER_ADD_MENU_UPDATE_CATEGORY'=> 'تعديل تصنيفات القائمة',

'MER_SELECT_STATUS'		=>      'إختر حالة',

'MER_ACTIVE'		=> 'نشط',

'MER_INACTIVE'   => 'غير نشط',

'MER_CATEGORY_NAME'   => 'اسم التصنيف',

'MER_LAST_MODIFIED_DATE'   => 'تاريخ آخر تعديل',

'MER_STATUS'   => 'الحالة',

'MER_ACTION'   => 'إجراء',

'MER_EDIT'   => 'تعديل',

'MER_VALIDATION_CATEGORY_NAME'   => 'يرجى إدخال اسم التصنيف ' ,

'MER_VALIDATION_CAT_ARABIC_AR'=> 'يرجى إدخال اسم التصنيف ' ,





//Dish validation

'MER_VALIDATION_SELECT_CATEGORY'   => 'يرجى إختيار تصنيف',

'MER_VALIDATION_DISH_NAME'   =>        'يرجى إدخال اسم الطبق',

'MER_VALIDATION_DISH_NAME_AR'   =>      'يرجى إدخال اسم الطبق',

'MER_VALIDATION_PRICE'   =>              'يرجى إدخال السعر',

'MER_VALIDATION_ABOUT'   =>              'يرجى إدخال وصف',

'MER_VALIDATION_ABOUT_AR'   =>           'يرجى إدخال وصف',









//Dish

'MER_ADD_DISH' 		                => 			'إضافة طبق',

'MER_UPDATE_DISH' 		                => 		'تعديل طبق',

'MER_ADD_NEW' 		                => 			'إضافة جديد',

'MER_CATEGORY' 		                => 			'التصنيف',

'MER_DISH_NAME' 		            => 	        'اسم الطبق',

'MER_PRICE' 		                => 	        'السعر (ر.س)',

'MER_IMAGE' 		                => 	        'الصورة',

'MER_RECORD' 		                => 	        'لا يوجد سجلات',

//Container





'MER_ADD_CONTAINER' 		            => 'إضافة حافظة',

'MER_EDIT_CONTAINER' 		            => 'تعديل حافظة',

'MER_CONTAINER' 		                => 'الحافظة',

'MER_CONTAINER_NAME' 		                => 'اسم الحافظة',

'MER_CONTAINER_PRICE' 		                => 'السعر (ر.س)',   





'MER_VALIDATION_HOTEL_NAME' => 'يرجى إدخال اسم الفندق',

'MER_VALIDATION_DESCRIPTION' => 'يرجى إدخال الوصف',

'MER_VALIDATION_IMAGE' 		  => 'يرجى تحميل الصورة ',

'MER_VALIDATION_HOTEL_NAME_AR' => 'يرجى إدخال اسم الفندق',

'MER_VALIDATION_DESCRIPTION_AR' => 'الرجاء إدخال الوصف',

'MER_VALIDATION_VALID_IMAGE' => 'يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة ',

// Container validaton

'MER_VALIDATION_CONTAINER_NAME' 		      => 'يرجى إدخال اسم الحافظة',

'MER_VALIDATION_CONTAINER_SELECT_NAME'   =>   'يرجى تحديد اسم الحافظة',

'MER_VALIDATION_CONTAINER_NAME_AR'  => 'يرجى إدخال اسم الحافظة',

'MER_No_VALIDATION_PEOPLE'  => 'يرجى إدخال عدد الاشخاص',





//Offer validation

'MER_OFFER_NAME' 		            => 'اسم العرض',

'MER_OFFER_DISCOUNT' 		            => 'نسبة الخصم',

'MER_START_DATE' 		            => 'تاريخ البدء',

'MER_END_DATE' 		            => 'تاريخ الإنتهاء',

'MER_LIST_OFFER' 		            => ' قائمة العروض',

'MER_CREATE_OFFER' 		            => 'انشاء عرض',

'MER_ADD_OFFER' 		            => 'إضافة عرض',

'MER_OFFER_TITLE' 		            => 'عنوان العرض',

'MER_DISCOUNT' 		            => '% خصم',

'MER_VALIDATION_OFFER_TITLE'      =>'يرجى إدخال عنوان العرض',

'MER_VALIDATION_OFFER_TITLE_AR'      =>'يرجى إدخال عنوان العرض',

'MER_VALIDATION_DISCOUNT'      =>'يرجى إدخال الخصم',

'MER_VALIDATION_START_DATE'      =>'يرجى إختيار وقت البدء',

'MER_VALIDATION_END_DATE'      =>'يرجى إختيار وقت الإنتهاء', 

'MER_OFFER_UPDATE' 		            => 'تعديل العرض',







'MER_VALIDATION_HOTEL_NAME' => 'يرجى إدخال اسم الفندق',

'MER_VALIDATION_DESCRIPTION' => 'يرجى إدخال الوصف',

'MER_VALIDATION_IMAGE' 		  => 'يرجى تحميل الصورة ',

'MER_VALIDATION_HOTEL_NAME_AR' => ' يرجى أدخل اسم الفندق',

'MER_VALIDATION_DESCRIPTION_AR' => 'الرجاء إدخال الوصف ',

'MER_VALIDATION_VALID_IMAGE' => ' (JPG ، JPEG ، PNG)يرجى تحميل صورة صالحة ',

 'VAT_CHARGE'                            => 'ضريبة القيمة المضافة',



'MER_VALIDATION_CITY' => 'يرجى إختيار المدينة',

'MER_VALIDATION_MANAGER' => 'يرجى إختيار مدير',

'MER_VALIDATION_BRANCH' => 'يرجى إدخال اسم الفرع',

'MER_VALIDATION_BRANCH_AR' => 'يرجى ادخال اسم الفرع',

'MER_PRODUCT_DETAILS'				=> 			'تفاصيل المنتج',

'MER_VALIDATION_FIRST_NAME' => 'يرجى إدخال الاسم الأول',

'MER_VALIDATION_LAST_NAME' => 'الرجاء إدخال الاسم الأخير',

'MER_VALIDATION_EMAIL' => 'يرجى إدخال البريد الإلكتروني',

'MER_VALIDATION_VALID' => 'يرجى إدخال البريد الإلكتروني بشكل صحيح',			

'MER_VALIDATION_MOBILE' => 'الرجاء إدخال رقم الجوال',

'MER_VALIDATION_CITY_TEXT' => 'يرجى إدخال اسم المدينة',

'MER_VALIDATION_PASSWORD' => 'يرجى إدخال كلمة المرور',







'MER_VALIDATION_HALL_NAME' => 'يرجى إدخال اسم القاعة',

'MER_VALIDATION_HALL_NAME_AR' => 'يرجى إدخال اسم القاعة',

'MER_VALIDATION_MAP_ADDRESS' => 'يرجى إدخال رابط الموقع على خرائط قوقل',

'MER_VALIDATION_HALL_LENTH' => 'الرجاء إدخال طول القاعة',

'MER_VALIDATION_HALL_WIDTH' => 'الرجاء إدخال عرض القاعة',

'MER_VALIDATION_HALL_AREA' => 'الرجاء إدخال مساحة القاعة',

'MER_VALIDATION_HALL_FOOD_MENMU' => 'يرجى التحقق من قائمة الطعام في القاعة',

'MER_VALIDATION_HALL_PRICE' => 'الرجاء إدخال سعر القاعة',

'MER_VALIDATION_HALL_TYPE' => 'يرجى إدخال نوع القاعة',



'MER_VALIDATION_FREE_SERVICES_NOTES' => 'يرجى إدخال ملاحظات الخدمة',

'MER_VALIDATION_FREE_SERVICES_NAME' => 'يرجى إدخال اسم الخدمة',

'MER_VALIDATION_FREE_SERVICES_NOTES_AR' => 'الرجاء إدخال ملاحظات الخدمة',

'MER_VALIDATION_FREE_SERVICES_NAME_AR' => 'الرجاء إدخال اسم الخدمة',



'MER_VALIDATION_PAID_SERVICES_NOTES' => 'يرجى إدخال ملاحظات الخدمة',

'MER_VALIDATION_PAID_SERVICES_NAME' => 'يرجى إدخال اسم الخدمة',

'MER_VALIDATION_PAID_SERVICES_NOTES_AR' => 'الرجاء إدخال ملاحظات الخدمة ',

'MER_VALIDATION_PAID_SERVICES_NAME_AR' => 'الرجاء إدخال اسم الخدمة',

'MER_VALIDATION_PAID_PRICE' => 'يرجى إدخال السعر',		



'Insuranceamount' => 'مبلغ التأمين (ر.س)', 



//Reception and Hospitality



'MER_SHOP_INFO' =>'معلومات المتجر',

'MER_SHOP_IMAGE' =>'الشعار',

'MER_SHOP_ATTRIBUTES' =>'إضافة خصائص',

'MER_SHOP_ATTRIBUTES_NAME' =>'اسم الخاصية',

'MER_SHOP_SELECT_PARENT_NAME' =>'إختر الخاصية الرئيسية',

'MER_IMAGE' =>'الصورة',

'MER_SHOP_NAME' =>'اسم المتجر',        

'SHOPINFO' => 'معلومات المتجر',

'SHOPPICTUREANDVIDEO' => 'صور وفيديو المتجر',

'SHOPATTRIBUTES'=> 'الخصائص',

'ITEAMINFORMATION'=> 'معلومات الاصناف',

'WORKERSNATIONLITY' => 'جنسيات العمال',

'PACKAGES'=> 'الباقات',

'REVIEWANDCOMMENTS' => 'المراجعة والتعليقات',

'ORDERS' => 'الطلبات',

'SHOPLIST' => 'قائمة المتاجر',

'MER_ADD_SHOP' => 'إضافة متجر',

'MER_NAME' => 'الاسم',

'MER_SHOP_IMAGE' =>'الشعار',

'MER_GOOGLE_MAP_ADDRESS_URL' =>'رابط الموقع في قوقل',

'MER_VALIDATION_VIEW' =>'عرض',





//Validation message

'MER_VALIDATION_SHOP_NAME' =>'يرجى إدخال اسم المتجر',

'MER_VALIDATION_SHOP_NAME_AR' =>'يرجى إدخال اسم المتجر',

'MER_VALIDATION_SHOP_URL' =>'يرجى إدخال رابط الموقع على خرائط قوقل',

'MER_VALIDATION_SHOP_URL_AR' =>'يرجى إدخال رابط الموقع على خرائط قوقل',

'MER_CONTAINER' =>'سعر الحافظات (ر.س)',







'MER_VALIDATION_PACKEGE_ITEMS' => 'يرجى تحديد عناصر الباقة,',

'MER_VALIDATION_PACKEG_NAME' => 'يرجى إدخال اسم الباقة ',

'MER_VALIDATION_NO_STAFF' => 'يرجى إدخال عدد الموظفين',

'MER_VALIDATION_PACKEG_NAME_AR' => 'يرجى إدخال اسم الباقة',









'MER_SELECT_NATIONALITY' => 'إختر الجنسية',



//Buffet Ajit 

'MER_WORKERS_NATIONALTY'					=> 		'جنسية العمال',

'MER_BUFFET_DASHBOARD'					=> 		'البوفيه',

'MER_BUFFET_INFO'					        => 		'معلومات الفرع',

'MER_GIVEN_BY'							=> 		'الاسم',

'MER_RREVIEW_DATE'						=> 		'تاريخ المراجعة',

'MER_BUFFET_MENU'			            => 'قائمة الطعام',

'MER_BUFFET_APPETIZERS'					=> 'المقبلات',

'MER_BUFFET_MAIN_COURSE'				=> 'الأطباق الرئيسية',

'MER_BUFFET_DESERT'					    => 'الحلى',

'MER_BUFFET_ORDERS'					    => 'الطلبات',

'MER_BUFFET_REVIEWS_AND_COMMENTS'		=> 'التقييم والتعليقات',

'EMAILADDRRESS' => 'البريد الإلكتروني',

'PASSWORD' => 'كلمة المرور',

'MER_VALIDATION_RESTURNANT_NAME_AR' => 'يرجى إدخال اسم المطعم',







//Harray Popular band info

'MER_POPULAR_BAND_INFO'	  => 	'معلومات الفرقة',

'MER_BAND_NAME'	  => 	'اسم الفرقة',

'MER_BAND_IMAGE'	  => 	'صورة الفرقة',

'MER_TERMSCONDITION'	  => 	'الشروط والأحكام',

'MER_About_Band'	  => 	'عن الفرقة',

'Popupr_Band_Photo_video'	  => 	'صور وفيديو الفرقة',

'popular_band_title'	  => 	'صور وفيديو الفرقة',

'popular_band_comments_review'	  => 	'المراجعة والتعليقات',			



//Harry Beauty Module



'mer_shoplist'	  => 	'قائمة المتاجر',   

'MER_ADD_SHOP'	  => 	'إضافة متجر',

'MER_SHOP_NAME'	  => 	'اسم المتجر',   

'MER_ADD_BEAUTYSHOP'	  => 'إضافة متجر',   

'mer_service_catogory'	  => 'تصنيفات الخدمة',   	

'mer_category_name'	  => 'اسم التصنيف',   	

'mer_add_category'	  => 'إضافة تصنيف',

'mer_services'	  => 'الخدمات',

'mer_service_name'	  => 'اسم الخدمة',

'mer_google_add'	  => 'انسخ / ألصق عنوان الويب الموجود في شريط عنوان المتصفح في خرائط قوقل',

'mer_view_title'	  => 'عرض',

'mer_address_img'	  => 'قم بتحميل صورة لموقع المتجر',	

'mer_img_replace'	  => 'سيؤدي تحميل الصورة إلى استبدال الصورة السابقة.',		

'mer_workerview'	  => 'تقييمات العاملين',		

'mens_barbe_info'	  => "معلومات صالون الحلاقة",	

'mer_about_video'	  => "حول الفيديو",	

'mer_makeupartistinfo'	  => "معلومات خبيرة التجميل",	

'mer_category_list'	  => "قائمة التصنيفات",	

'mer_category_item'	  => "تصنيفات المنتجات",			

'cosha_item'	  => "عناصر الكوش",

'add_item'	  => "إضافة عنصر",

'add_item_category'	  => "إضافة تصنيف",		

'packges_name'	  => "اسم الباقة",

'numberof_cameras'	  => "عدد الكاميرات",							

'duration_video'	  => "مدة الفيديو",

'numberof_picture'	  => "عدد الصور",

'packagefor'	  => "باقة ",	

'product_list'	  => "قائمة المنتجات",				

'special_event_info'	  => "معلومات المناسبات الخاصة",

'event_image'	  => "الشعار",	

'item_information'	  => "معلومات الصنف",	

'mer_office_address_img'	  => 'قم بتحميل صورة لموقع المتجر',			

'reception_hospitality'	  => "الإستقبال والضيافة",





//Himanshu Makeup and Artist

'MER_VALIDATION_SERVICE_TYPE' => 'يرجى تحديد مكان توفر الخدمة الخاصة بك',



'MER_VALIDATION_SERVICENAME_AR' =>'يرجى إدخال اسم الخدمة',



'MER_VALIDATION_DURATION_AR' =>'يرجى إدخال المدة',

'MER_VALIDATION_EXP' =>'يرجى إدخال سنوات الخبرة',

'MER_VALIDATION_ABOUT_SINGER' =>'يرجى إدخال الوصف',



'MER_VALIDATION_SINGER_NAME' =>'يرجى إدخال اسم المطرب',

'MER_VALIDATION_SINGER_NAME_ARABIC' =>'يرجى إدخال اسم المطرب',

'MER_VALIDATION_ABOUT_SINGER_AR' =>'يرجى إدخال الوصف',

'MER_VALIDATION_EXPERTISES_AR' => 'يرجى إدخال الخبرة',

'MER_VALIDATION_EXPERTISE' => 'يرجى إدخال الخبرة',



'MER_VALIDATION_NAME_AR' => 'يرجى إدخال الاسم',

'MER_UPDATE_RESTURANT'=> 'تعديل البوفية',



/*New Language*/

'MER_CATEGORIES'	  => "الأقسام",

'Please_upload_only_PDF_file' => "يرجى تحميل ملف PDF فقط",

'MER_ADD_NEW_CATEGORY'=> 'إضافة تصنيف جديد',



//Home work

'MER_QUANTITY'=> 'الكمية',

'MER_PRODUCT_NAME'=> 'اسم المنتج',

'MER_PRODUCT_IMAGE'=> 'صورة المنتج',

'MER_PRODUCT_QUANTITY'=> 'الكمية',

'MER_VALIDATION_PRODUCTNAMES_AR'=>'يرجى تحميل ملف PDF فقط',



'MER_VALIDATION_PRODUCTQUANTITY_AR'=>'يرجى إدخال الكمية',

'MER_SHOP_IMAGE' =>'الشعار', 

'MER_Dish_Image' => 'صورة الطبق',

'MER_VALIDATION_SHOPNAME_AR'=>'يرجى إدخال اسم المتجر', 

'MER_con_title'=>'الحافظة',		

'EDIT' => 'تعديل',

'DRLIST' => 'قائمة الاطباء',

'mer_specialist' => 'اخصائي',

'MER_VALIDATION_NAME_AR' =>'يرجى إدخال الاسم بالعربي',

'mer_consultation' => 'رسوم الاستشارة',

'mer_add_dr_list' => 'إضافة الطبيب',

'Professional_Bio' => 'التخصص',

'Consulation_fees' => 'رسوم الاستشارة',

'Acoustics_Info_Menu'  => 'معلومات المتجر',

'MER_VALIDATION_VALID_FILE'=>'يرجى تحميل ملف pdf',

'mer_clinic_list' =>'قائمة العيادات', 

'mer_add_clinic' =>'أضف عيادة',

'JOIN_DATE' =>'تاريخ الانضمام',

'UPDATEROSESTYPE'=>'تعديل الورود',

'ADDROSESTYPE'=>'إضافة نوع ورد',

'QTYSTOCK' => 'الكمية',

'ROSESTYPE' => 'نوع الورد', 

/*start merchant_left_menu_transaction.blade.php*/

/*end back end static pages*/



'List_the_food_choices_available_with_your_hall'=>'خيارات الطعام المتاحة في القاعة',

'Do_you_allow_food_from_outside'=>'هل تسمح بالطعام من الخارج؟',

'SELECT_CATEGORY'  => 			'إختر التصنيف',



//Clinic Name

'MER_CLINIC_NAME' =>'اسم العيادة',

'MER_VALIDATION_CLINIC_NAME' =>'يرجى إدخال اسم العيادة',  



'FlOWERTYPE' =>'نوع الورد',  

'WRAPTYPE' =>'نوع التغليف',  

'WRAPDESIGN' =>'تصميم التغليف',  

'QTYSTOCK'=>'الكمية ',  

'PLEASE_SELECT_FLOWER_TYPE' => 'الرجاء إختيار نوع الورد',  

'PLEASE_SELECT_WRAP_TYPE' => 'الرجاء تحديد نوع التغليف', 

'PLEASE_SELECT_WRAP_DESIGN' => 'الرجاء تحديد تصميم التغليف', 

'UPDATEPACKAGE' => 'تعديل الباقة',

'MER_VALIDATION_NAME' => 'يرجى إدخال الاسم ',

'MER_VALIDATION_PRODUCTQUANTITY' => 'يرجى إدخال الكمية ',

'Consulation_fees'=> 'رسوم الاستشارة',

'Please_Enter_Specilist_AR'=> 'الرجاء إدخال تخصص الطبيب',

'MER_VALIDATION_VALID_FILE_AR'=> 'يرجى تحميل ملف pdf', 

'work' => 'عمل',

'SELECT_YOUR_CHOICE'=>'تفصيل / جاهزة',

'SELECT_YOUR_CHOICE_KOSHA'=>'صمم كوشتك / الكوش الجاهزة',

'Ready_Made_Cosha'=>'الكوش الجاهزة',

'Designer_Cosha'=>'تصميم الكوشة',



'Please_upload_only_PDF_file_ar'=> 'يرجى تحميل ملف PDF فقط',



'Clinic'=>'العيادات',

'MER_ADD_DOCTOR' =>'أضف طبيب',

'mer_update_clinic' =>'تعديل العيادة',

'CarRentalInfo' =>'معلومات المتجر',

'Product' =>'المنتج ',

'Model_Name'=>'اسم الطراز ',

'Model_year'=>'سنة الصنع ',

'Service_Date'=>'تاريخ الخدمة ',

'Price_Per_KM' =>'السعر في اليوم',

'PLEASE_ENTER_MODEL_NAME'=>'الرجاء إدخال اسم الطراز',

'PLEASE_ENTER_MODEL_YEAR'=>'الرجاء إدخال سنة الصنع',

'PLEASE_ENTER_SERVICE_DATE'=>'يرجى إدخال تاريخ الخدمة',

'MER_ADD_PRODUCT'=>'أضف منتج',

'Car_type'=>'نوع السيارة ',

'MER_Dish_Image'=> 'صورة الطبق',

'MER_ADD_Car_type' =>'أضف نوع السيارة ',

'MER_Update_Car_type' =>'تعديل نوع السيارة',

'TravelInfo'  =>'معلومات المكتب',

'MER_ADD_PACKAGE'=>'إضافة باقة',

'Location'=>'الموقع',

'PackageSchedule'=>'جدول الباقة',

'PackageDuration'=>'مدة الباقة',

'ExtraService'=>'خدمات اضافية',

'PLEASE_ENTER_LOCATION'=>'يرجى إدخال الموقع',

'PLEASE_ENTER_PACKAGE_SCHEDULE'=>'الرجاء إدخال مواعيد الباقة',

'PLEASE_ENTER_PACKAGE_DURATION'=>'يرجى إدخال المدة',

'PLEASE_ENTER_EXTRA_SERVICES'=>'يرجى إدخال الخدمات الاضافية',

'MER_UPDATE_PACKAGE'=>'تحديث الباقة',

'Caliber' =>'العيار ',

'Weight' =>'الوزن (غرام)',

'abaya_item' =>'العبايات',

'Ready_Made_abaya' =>'جاهز',

'Designer_abaya' =>'تفصيل',

'SIZE'=>'المقاس',

'WorkerPrice' => 'سعر العامل ',

'PLEASE_ENTER_WORKER_PRICE' => 'الرجاء إدخال سعر العامل',

'SN'=>'ر.م',

'WRAPPINGTYPE' =>'نوع التغليف',

'WRAPPINGDESIGN' =>'تصميم التغليف',

'ADDWRAPPINGDESIGN' =>'إضافة تصميم التغليف',

'ADDWRAPPINGTYPE' =>'إضافة نوع التغليف',

'NAME'=>'الاسم',

'MER_NAME_VALIDATION'=> 'يرجى إدخال الاسم',

'Type'=>'النوع ',

'Dish'=>'الصحون ',

'MER_DISCOUNT'=>'(%)خصم ',

'Per_Pieces'=>'لكل قطعة',

'Per_KG'=>'لكل كجم ',

'Available_Qty'=>'الكمية المتوفرة ',

'Discount'=>'الخصم ',

'Per_Quantity'=>'لكل قطعة',

'MER_VALIDATION_Choose_Type'=>'يرجى إختيار نوع الطبق',

'MER_DESCRIPTION_TXT'=>'حرف متبقي',

'Please_Enter_Consulation_fees' =>'يرجى إدخال رسوم الاستشارة',

'MER_VALIDATION_BIOGRAPHY' =>'الرجاء إدخال التخصصات',

'NUMBERFLOWER' =>'عدد الورد',

'dresses_item'=> 'المنتج',

'MER_ADD_NEW_SIZE'=> 'إضافة مقاس',

'MER_VALIDATION_SIZE_NAME' =>'يرجى إدخال المقاس',

'UPDATE_PRODUCT'=> 'تعديل المنتج',

'Avilable_on_rent'=> 'متاح للإيجار',

'Rent_price'=> 'إيجار / ساعة',

'Yes'=> 'نعم',

'No'=> 'لا',

'MER_IMG'=> 'صورة',

'MER_VALIDATION_PRODUCTNAME' => 'يرجى إدخال اسم المنتج',

'MAKEUP'=> 'مستحضرات التجميل',

'HOME'=> 'المنزل',

'HOTELS' => 'الفنادق',

'reception_hospitality' => 'الإستقبال والضيافة',

'singer'=> 'المطربين',

'acoustics'=> 'الصوتيات',

'popular_band'=> 'الفرق الشعبية',

'buffet'=> 'البوفيه',

'MANAGER'=> 'المدراء',

'beauty'=> 'المشاغل النسائية',

'oudandperfumes' =>'العود والعطور',

'goldandjewelry'=>'المجوهرات',

'spa'=> 'المنتجعات الصحية',

'makeup_artist'=> 'خبيرة التجميل',

'tailor'=> 'الخياطين',

'photography'=> 'استوديو تصوير',

'abaya'=> 'العبايات',

'dresses'=> 'الفساتين',

'cosha'=> 'الكوش',

'Special_Events'=> 'المناسبات الخاصة',

'car-rental'=> 'تأجير السيارات',

'Travel_Agency'=> 'المكاتب السياحية',

'Roses'=> 'الورود',

'Men_Barber_Saloon'=> 'صالونات الحلاقة',

'Dessert'=> 'الحلويات',

'Dates'=> 'التمور والرطب',

'Cosmetic_and_Laser'=> 'عيادات التجميل والليزر',

'SKIN_AND_TEETH'=> 'عيادات الجلدية والأسنان',

'MAKEUP'=> 'خبيرات التجميل',

'MER_SELECT'=>'إختيار',

'MER_VALIDATION_ADDRESS_AR' => 'الرجاء إدخال العنوان',

'PAYNOW'=> 'ادفع الآن',

'PAYMENT_SUCCESS'=> 'نجت العملية',

'PAYMENT_ERROR'=> 'خطأ',



'MER_PLEASE_SELECT_TAILOR' => 'يرجى التحديد تفصيل / جاهز',

'MER_FOR' => 'لـ',

'MER_PRODUCT_CATEGORY'=> 'فئات الأصناف',

'Acousticsinfo' =>'معلومات متجر الصوتيات',

'Acoustics_Info_Menu'=>'معلومات متجر الصوتيات',



'MER_PRODUCTS' => 'الأصناف',

'MER_ADD_PRODUCT' => 'أضف منتج',



'BODY_MEASUREMENT'=>'القياسات',

'LENGTH'=>'الطول',

'CHEST_SIZE'=>'الصدر',

'ABAYA_PRODUCT'=>'العباية',

'WAIST_SIZE' =>'الخصر',

'SHULDERS' =>'الأكتاف',

'NECK' =>'الرقبة',

'ARM' =>'الذراع',

'WRIST_DIAMETER'=>'فتحة الكم',

'CUTTING'  => 'شكل القصة',



'This_is_to_inform_that_we_have_received_your_payment'=> 'نود اعلامك بأننا استلمنا دفعتك بنجاح. شكراً!',

'We_look_forward_to_build_a_mutually-beneficial_long-term_association_with_you' =>'نحن نتطلع إلى بناء علاقة طويلة الأمد معك',



'MyWallet'=> 'المحفظة',

'MER_MyWallet'=> 'أضف مبلغ',

'SNO'=> 'لا',

'Amount_Paid'=> 'المبلغ المدفوع',

'Payment_date'=> 'تاريخ الدفع',

'Mode'=> 'الحالة',

'Total_Amount'=> 'المبلغ الإجمالي',

'Pay_Now'=> 'ادفع الآن',



'Credit'=> 'دائن',

'Sucess'=> 'نجاح',

'Failed'=> 'فشل',

'Debit'=> 'مدين',

'Amount'=> 'المبلغ',

'DateFrom'=> 'من',

'DateTo'=> 'الى',

'Apply'=> 'تطبيق',

'Service_provider'=> 'متوفر 24 ساعة طوال الاسبوع',

'MER_ADD_SERVICESS' => 'أضف خدمات',

'Stitch_Designs'=>'التصاميم',

'ADD_Stitch_Designs'=>'إضافة تصميم ',

'UPDATE_Stitch_Designs'=>'تعديل تصميم',

'ADD_Stitch_Design'=>'إضافة تصميم',

'ADD_Stitch_Design'=>'اسم التصميم',

'FUND_REQUEST'=>'طلب الدفع',

'ACCOUNT_DETAILS'=>'تفاصيل الحساب',

'RECIEVED_PAYMENT'=>'الدفعات المستلمة',

'ORDER_REPORTING'=>'تقارير الطلبات',



'ADD_Stitch_Design_Image'=> 'صورة التصميم',



'Quantity_For_Sale'     =>  'الكمية (للبيع)',



'Quantity_For_Rent'    =>   'الكمية (للايجار)',



'Order_id' => 'رقم الطلب',

'ORDER_ID' => 'رقم الطلب',

'Request_date' => 'تاريخ الطلب',

'transaction_id' => 'رقم المعاملة',

'Bank_Name'=> 'اسم البنك',

'Account_number'=> 'رقم الحساب',

'PLEASE_ENTER_BANK_NAME'=> 'يرجى إدخال اسم البنك',

'PLEASE_ENTER_ACCOUNT_NUMBER'=> 'الرجاء إدخال رقم الحساب',

'Note'=> 'ملحوظة',

'MER_CREATED_IMAGE'=> 'صورة التصنيف',

'MER_PLEASE_SELECT_SIZE'=> 'الرجاء إختيار مقاس',



'YES'=> 'تم قبول العرض من العميل',

'NO'=> 'عرض السعر من قبل التاجر',



'MER_SHOP_LOGO' =>'الشعار',





'MER_CALIBER' =>'الرجاء إدخال العيار',



'MER_WEIGHT' =>'يرجى إدخال الوزن (جرام)',



'PACKAGE_LIST' => 'قائمة الباقات',

'PACKAGE_IMAGE' =>'صورة الباقة',



'SELECT_ORDERS' =>'إختر طلبا',

'Last_10_Days' =>'آخر 10 أيام',

'Last_1_Month' =>'آخر شهر',

'Last_6_Months' =>'آخر 6 أشهر',

'Last_1_Years' =>'آخر سنة',

'Start_Date' =>'تاريخ البدء',

'End_Date' =>'تاريخ الإنتهاء',

'Discount_For_Sale'   =>    'خصم (للبيع)',

'Discount_For_Rent'  =>     'الخصم (للإيجار)',

'MER_ORDER_STATUS' =>'حالة الطلب',

'PLEASE_ENTER_DELIVER_DATE' =>'يرجى ادخال تاريخ التسليم',

'sales_rep_code' =>'كود المندوب',

'MER_ADD_PRODUCTS' =>'أضف منتج',

'VALIDFILEPDF' => 'يرجى تحميل ملف PDF',

'special_event' =>'المناسبات الخاصة',



'FROM' =>'من',

'TO' =>'إلى',

'WORKING_HOURS' =>'ساعات العمل',

'Paid'=>'دفع',

'Select_Category'=>'إختر التصنيف',

'IN_PROCESS'		=> 'قيد التنفيذ',

'COMPLETE'   => 'مكتمل',

'REQUEST' => 'طلب',

'RENTAL_DATE_TIME' => 'تاريخ ووقت الحجز',

'QUANTITY' => 'الكمية',

'RETURN_DATE_TIME' => 'تاريخ ووقت الإرجاع',

'PRODUCT_INFORMATION' => 'معلومات المنتج',



'Price_Per_KM' => 'السعر لكل كيلومتر',

'Car_Renta_Info' =>'معلومات تأجير السيارات',



'Car_Renta_Name' =>'اسم تأجير السيارات',



'Please_enter_carrental' =>'يرجى إدخال اسم تأجير السيارات',



'Please_enter_carrental_ar' =>'يرجى إدخال اسم تأجير السيارات',

'Car_Models' => 'موديلات السيارات',

'Add_Car_Models' => 'إضافة نماذج السيارات',

'FILE_NUMBER'	                         => 'رقم الملف',

'Update_Car_Models' => 'تحديث نماذج السيارات',

'DURATION'                              => "المدة (ساعات)",

'BOOKING_PLACE'                         => 'مكان تقديم الخدمة',

'HOME_CHARGES'=>'رسوم الزيارة المنزلية',

'Travel_Agency_name' => 'اسم وكالة السفر',

'Travel_Agency_image' => 'صورة وكالة السفر',

'Location' =>'الموقع',

'Package_Duration' => 'مدة الباقة',

'Extra_Service' => 'الخدمات الاضافية',



'Service' => 'الخدمات',

'BRANCH' => 'قائمة الفروع',

'STAFF' => 'العاملين',



'BOOKING_DATE' => 'تاريخ الحجز',

'STARTTIME_ENDTIME' => 'وقت البدء / الإنتهاء',

'UNIT'=>'وحدة',

'Dress_Type' => 'نوع الفستان',



'Size' => 'الحجم',

'MENU' => 'قائمة طعام',

'DISH' => 'طبق',

'CONTAINER' => 'الحافظة',

'QUANTITY'=>'الكمية',

'BOOKING_TIME' => 'وقت الحجز',

'BOOKING_DATE_TIME' => 'تاريخ ووقت الحجز',

'RETURN_DATE_TIME' => 'تاريخ ووقت الإرجاع',

'Language_Type' =>'اللغة',

'OCCASIONDATE'=>'تاريخ المناسبة',

'Write_On_jewellery' =>'اكتب على المجوهرات',

'OCCASIONBUDGET'=>'ميزانية المناسبة',

'total_member'=>'عدد الحضور',

'OCCASIONTYPE'=>'مناسبات',

'Fabric'=>'الاقمشة',

'name'=>'الاسم',

'ADD_Fabric'=>'إضافة قماش',

'UPDATE_Fabric'=>'تعديل القماش',

'Image'=>'صورة',

'PLEASE_ENTER_NAME'=>'يرجى إدخال الاسم',

'Choose_Fabrics'=>'إختيار القماش',

'EDIT'=>'تعديل',

'Package_Schedule'=>'جدول الباقة',

'SIZECHART'=>'دليل المقاسات',

'MANAGE_SHIPPING'=>'إدارة الشحن',

'CUSTOMER'=>'العميل',

'BOOKINGDATE'=>'تاريخ الحجز',

'BOOKINGDURATION'=>'وقت الحجز',

'mer_workerbooking'=>'حجز العامل',

'BACK' 					=> 'رجوع',

'BOOKINGSLOT' => 'وقت الحجز',

'NO_OF_PEOPLE'=>'عدد الاشخاص',



'VAT'=>'VAT',

'Please_enter_valid_url' =>'الرجاء إدخال رابط صحيح',

'Replied' =>'تم الرد',

'Confirmed_by_customer'=>'تم التأكيد من قبل العميل',

'MER_UserComments'=>'تعليقات المستخدم',

'MER_REJECT'=>'رفض',

'Update_return_qty'=>'تعديل الكمية',

'REVIEW_ON'=>'مراجعة',

'MER_SHOP'=>'المتجر',

'MER_WORKER'=>'العاملين',

'MER_FILTER'=>'كل المراجعات',

'Deny_by_admin'=>'مرفوض من قبل البائع',

'Deny_by_user'=>'مرفوض من قبل المستخدم',

'Assigned_Invitation'=>'الدعوات المسندة',

'Occasion_Name'=>'اسم المناسبة',

'Venue'=>'مكان المناسبة',

'Event_DATE_TIME'=>'الوقت / التاريخ',

'Number_of_Invitees'=>'عدد المدعوين',

'Invitations_Type'=>'نوع الدعوات',

'Invitees_list'=>'قائمة المدعوين',

'Invitees_Name'=>'اسم المدعو',

'Invitees_Email'=>'ايميل المدعو',

'Invitees_Address'=>'العنوان',

'Attended'=>'حضر',

'NotAttended'=>'لم يحضر',

'MER_ACTIONS'=>'إجراء',

'ORDERFOR'=>'طلب من أجل',

'NATIONALID'=>'الهوية الوطنية',

'Credited'=>'دائن',

'MER_SERVICE_AND_FOOD'=>'الخدمات والطعام',

'PAYMENTS'=>'الدفعات',

'PACKAGE_FOR' =>'باقة لـ',

'HALL_NAME' => 'اسم القاعة',

'HALL_LOCATION' => 'موقع القاعة',

'TOTAL_STAFF_CHARGES' => 'مجموع تكاليف العاملين',

 'OCCASION'=>'تفاصيل المناسبة',
 'ADD_DRESSES'           => 'إضافة فساتين',
 'UPDATE_DRESSES'           => 'تحديث فساتين',

];





?>
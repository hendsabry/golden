<?php
	return 
	[
/*sratr back end static pages*/
	/*start merchant_header.blade.php*/
		'LOGO' 							=> 			'Logo',
		'HI' 							=> 			'Hi',
		'PROFILE' 						=> 			'Profile',
		'SETTINGS' 						=> 			'Vendor Info',
		'LOGOUT' 						=> 			'Logout',
		'DASHBOARD' 					=> 			'Dashboard',
		'PRODUCTS' 						=> 			'Products',
		'TRANSACTIONS' 					=> 			'Transactions',
		'FUND_REQUESTS' 				=> 			'Fund Requests',
		'STORES' 						=> 			'Stores',
		'DEALS' 						=> 			'Deals',
		'MER_WALLET' 					=> 			'Wallet',
		'MER_DURATION' 				    => 			'Duration',
		'MER_COUPON_AMOUNT' 			=> 			'Coupon Amount',
		'COUPON_AMOUNT' 			    => 			'Coupon Amount',
		'MER_ORDER_PLACED' 				=> 			'Order Placed',
		'MER_ORDER_PACKED' 				=> 			'Order Packed',
		'MER_DISPATCHED' 				=> 			'Dispatched',
		'CAR_MODEL'                             => 'Car Model',
		'MER_DELIVERED' 				=> 			'Delivered',
		'MER_PAYMENT_STATUS' 			=> 		    'Payment Status',
		'MER_MERCHANT' 					=> 		    'Merchant',
		'MER_AMOUNT_TO_PAY' 			=> 		    'Amount To Pay',
		'MER_STATE' 					=> 		    'State',
		'MER_MUSIC_TYPE' 			    => 		    'Music Type',
		'MER_SONG_NAME' 			    => 		    'Song Name',
		'MER_DEAL_NAME' 				=> 		    'Deal Name',
		'BACK_PAYPAL'					=> 	'Paypal',
		'BACK_TAX_INVOICE'   				=> 	'TAX INVOICE ',
		'BACK_SHIPPING_DELIVERY'   			=> 	'Shipping And Delivery',
		'BACK_SHIPPING_DETAILS'   		  	=> 	'Shipping Details',
		'BACK_SHIPPING_AND_DELIVERY'   		=> 	'Shipping and Delivery',
		'BACK_AMOUNT_PAID'   				=> 	'Amount Paid',
		'BACK_NAME' 						=> 	'Name',
		'BACK_ADDRESS'   					=> 	'Address',
		'BACK_STATE'						=> 'State',
		'BACK_INCLUSIVE_OF_ALL_CHARGES'   	=> 	'inclusive of all charges',
		'BACK_ZIPCODE'   					=> 	'Zipcode',
		'BACK_PHONE'   						=> 	'Phone',
		'BACK_ORDER_DATE'   				=> 	'Order Date',
		'BACK_INVOICE_DETAILS'   			=> 	'Invoice Details',
		'BACK_SHIPMENT_CONTAINS_ITEMS'   	=> 	'This shipment contains following items',
		'BACK_DEAL_NAME'					=> 'Deal Name',
		'BACK_QUANTITY'						=> 'Quantity',
		'BACK_ORIGINAL_PRICE'   			=> 	'Original Price',
		'BACK_SUB_TOTAL'   					=> 	'Sub Total',
		'BACK_PAYMENT_STATUS'				=> 'Payment Status',
		'BACK_SHIPMENT_VALUE'   			=> 	'Shipment Value',
		'BACK_TAX'			                => 	'Tax',
		'BACK_AMOUNT'			            => 	'Amount (SAR)',
		'BACK_CLOSE'   						=> 	'Close',
		'BACK_AMOUNT_TO_PAY'				=> 'Amount to Pay',
		'FILE_NO'					=>		'File No',
	/*end merchant_header.blade.php*/
	/*start merchant_left_menu_action.blade.php*/
		'AUCTION1' 						=> 			'AUCTION',
		'ADD_AUCTION_PRODUCTS' 			=> 			'Add Auction Products',
		'MANAGE_AUCTION_PRODUCTS' 		=> 			'Manage Auction Products',
		'ARCHIVE_AUCTION_PRODUCTS' 		=> 			'Archive Auction Products',
		'AUCTION_WINNERS' 				=> 			'Auction Winners',
		'AUCTION_COD' 					=> 			'Auction COD',
	/*end merchant_left_menu_action.blade.php*/
	/*start merchant_left_menu_deals.blade.php*/
		'DEALS1' 						=> 			'DEALS',
		'ADD_DEALS' 					=> 			'Add Deals',
		'MANAGE_DEALS' 					=> 			'Manage Deals',
		'EXPIRED_DEALS' 				=> 			'Expired Deals',
		'SOLD_DEALS'					=>			'Sold Deals',
	/*end merchant_left_menu_deals.blade.php*/
	/*start merchant_left_menu_fund.blade.php*/
		'FUND_REQUEST_REPORT' 			=> 			'Fund Request Report',
		'WITHDRAW_FUND_REQUEST' 		=> 			'Withdraw Fund Request',
	/*end merchant_left_menu_fund.blade.php*/
	/*start merchant_left_menu_product.blade.php*/
		'PRODUCTS1' 					=> 			'PRODUCTS',
		'ADD_PRODUCTS' 					=> 			'Add Products',
		'MANAGE_PRODUCTS' 				=> 			'Manage Products',
		'SOLD_PRODUCTS' 				=> 			'Sold Products',
		'SHIPPING_AND_DELIVERY' 		=> 			'Shipping And Delivery',
		'PAYU_SHIPPING_DELIVERY' 		=> 			'PayUMoney Shipping Delivery',
		'CASH_ON_DELIVERY' 				=> 			'Cash On Delivery',
	/*end merchant_left_menu_product.blade.php*/
	/*start merchant_left_menu_settings.blade.php*/
		'SETTINGS1' 					=> 			'SETTINGS',
		'EDIT_ACCOUNT' 					=> 			'Edit Account',
		'CHANGE_PASSWORD' 				=> 			'Change Password',
		'ATTRIBUTES_MANAGEMENT' 		=> 			'Attributes Management',
		'ADD_COLOR' 					=> 			'Add Color',
		'MANAGE_COLORS' 				=> 			'Manage Colors',
		'ADD_SIZE' 						=> 			'Add Size',
		'MANAGE_SIZES' 					=> 			'Manage Sizes',
	/*end merchant_left_menu_settings.blade.php*/
	/*start merchant_left_menu_shop.blade.php*/
		'MANAGE_STORES' 				=> 			'Manage Stores',
		'ADD_STORES' 					=> 			'Add Stores',
		'MER_SHIPPING_DETAILS' 			=> 	        'Shipping Details',
	/*end merchant_left_menu_shop.blade.php*/
	/*start merchant_left_menu_transaction.blade.php*/
		'TRANSACTIONS1' 				=> 			'TRANSACTIONS',
		'DEALS_TRANSACTION' 			=> 			'Deals Transaction',
		'DEALS_PAYUMONEY' 			=> 			    'Deals PayUMoney',
		'ALL_ORDERS' 					=> 			'All Orders',
		'SUCCESS_ORDERS' 				=> 			'Success Orders',
		'COMPLETED_ORDERS' 				=> 			'Completed Orders',
		'HOLD_ORDERS' 					=> 			'Hold Orders',
		'FAILED_ORDERS' 				=> 			'Failed Orders',
		'DEALS_COD' 					=> 			'Deals COD',
		'PRODUCTS_TRANSACTION' 			=> 			'Products Transaction',
		'PRODUCTS_PAYUMONEY' 			=> 			'Products PayUMoney',
		'PRODUCTS_COD' 					=> 			'Products COD',
		
	/*start merchant_left_menu_transaction.blade.php*/
	
	/*Start add deals.blade.php*/
		'MER_PRODUCT_BULK_UPLOAD' 				=> 			'Product Bulk Upload',
		'MER_ADD_DEALS' 						=> 			'Add Deals',
		'MER_TITLE' 							=> 			'Title',
		'MER_TOP_CATEGORY' 						=> 			'Top Category',
		'MER_SELECT' 							=> 			'Select',
		'MER_SELECT_MAIN_CATEGORY' 				=> 			'Select Main Category',
		'MER_SELECT_SUB_CATEGORY' 				=> 			'Select Sub Category',
		'MER_SELECT_SECOND_SUB_CATEGORY' 		=> 			'Select Second Sub Category',
		'MER_ORIGINAL_PRICE' 					=> 			'Original Price',
		'MER_NUMBERS_ONLY' 						=> 			'Numbers Only',
		'MER_DISCOUNTED_PRICE' 					=> 			'Discounted Price',
		'MER_START_DATE' 						=> 			'Start Date',
		'MER_END_DATE' 							=> 			'End Date',
		'MER_DESCRIPTION' 						=> 			'Description',
		'MER_SELECT_STORE' 						=> 			'Select Store',
		'MER_META_KEYWORDS' 					=> 			'Meta keywords',
		'MER_META_DESCRIPTION' 					=> 			'Meta Description',
		'MER_MINIMUM_DEAL_LIMIT' 				=> 			'Minimum Deal Limit',
		'MER_MAXIMUM_DEAL_LIMIT' 				=> 			'User Limit',
		'MER_MAXIMUM_USER_LIMIT' 				=> 			'Maximum User Limit',
		'MER_IMAGE_SIZE_MUST_BE' 				=> 			'image size must be',
		'MER_PIXELS' 							=> 			'Pixels',
		'MER_DEAL_IMAGE' 						=> 			'Deal Image',
		'MER_MAX' 								=> 			'max',
		'ORDER_ID' => 'Order Id',
		'MER_ADD' 								=> 			'Add',
		'MER_RESET' 							=> 			'Reset',
		'MER_THIS_DEAL_TITLE_ALREADY_EXIST_IN_THIS_STORE' 	=> 			'This Deal Title Already Exist in this_Store',
		'MER_THIS_PRODUCT_TITLE_ALREADY_EXIST' 				=> 			'This Product Title Already Exist in this Store',
		'MER_PLEASE_ENTER_TITLE' 				=> 			'Please Enter Title',
		'MER_PLEASE_SELECT_CATEGORY' 			=> 			'Please Select Category',
		'MER_NUMBERS_ONLY_ALLOWED' 				=> 			'Numbers Only Allowed',
		'MER_DISCOUNT_PRICE_SHOLUD' 			=> 			'Discount price sholud be less than original price',
		'MER_PLEASE_SELECT_DEAL_START_DATE' 	=> 			'Please Select Deal Start Date',
		'MER_PLEASE_SELECT_DEAL_END_DATE' 		=> 			'Please Select Deal End Date',
		'MER_END_DATE_SHOLUD_BE_GREATER_THAN_START_DATE' 		=> 			'End Date sholud be greater than start date',
		'MER_PLEASE_SELECT_EXPIRY_DATE' 		=> 			'Please Select Expiry Date',
		'MER_EXPIRY_DATE_SHOLUD_BE_GREATER_THAN_START_DATE' 	=> 			'Expiry Date sholud be greater than start date',
		'MER_PLEASE_SELECT_SHOP' 				=> 			'Please Select Shop',
		'MER_PLEASE_ENTER_DIFFERENT_DEAL_TITLE' 				=> 			'Please Enter Different Deal Title, This Already Exist in this Store',
		'MER_PLEASE_ENTER_METAKEYWORD' 			=> 			'Please Enter Metakeyword',
		'MER_PLEASE_ENTER_METADESCRIPTION' 		=> 			'Please Enter Metadescription',
		'MER_PLEASE_ENTER_MINIMUM_LIMIT' 		=> 			'Please Enter Minimum Limit',
		'MER_PLEASE_ENTER_MAXIMUM_LIMIT' 		=> 			'Please Enter Maximum Limit',
		'MER_MAXIMUM_VALUE_SHOULD_GREATER_THAN_MINIMUM_VALUE' 	=> 			'Maximum value should greater than minimum value',

	/*End add deals.blade.php*/
	/*start add shop.blade.php*/
		'MER_ADD_STORE_DETAILS' 				=> 			'Add Store Details',
		'MER_STORE_DETAILS' 					=> 			'Store Details',
		'MER_ADD_STORE' 						=> 			'Add Store',
		'MER_STORE_NAME' 						=> 			'Store Name',
		'MER_ADDRESS1' 							=> 			'Address One',
		'MER_ADDRESS2' 							=> 			'Address two',
		'ZIP_CODE' 							    => 			'Zip Code',
		'PAYFORT_EMAIL' 					    =>           'Payfort Email',
		'SHOP_IMAGE' 					    =>               'Shop Image',
		'MER_SELECT_COUNTRY' 					=> 			'Select Country',
		'MER_SELECT_CITY' 						=> 			'Select City',
		
		'MER_ZIPCODE' 							=> 			'Zipcode',
		'MER_WEBSITE' 							=> 			'Website',
		'MAP_SEARCH_LOCATION' 					=> 			'Map Search Location',
		'MER_TYPE_YOUR_LOCATION_HERE' 			=> 			'Type your location here',
		'MER_AUTO-COMPLETE' 					=> 			'Auto-complete',
		'MER_DRAG_MARKER_TO_GET_LATITUDE_&_LONGITUDE' 			=> 			'Drag Marker to get latitude & longitude',
		'MER_LATITUDE' 							=> 			'Latitude',
		'MER_LONGITUDE' 						=> 			'Longitude',
		'MER_STORE_IMAGE' 						=> 			'Store Image',
		'MER_IMAGE_UPLOAD_SIZE' 				=> 			'Image upload size',
		'MER_SUBMIT' 							=> 			'Submit',
		'MER_PLEASE_ENTER_STORE_NAME' 			=> 			'Please Enter Store Name',
		'MER_PLEASE_ENTER_CONTACT_NUMBER' 		=> 			'Please Enter Contact Number',
		'MER_PLEASE_ENTER_STORE_ADDRESS' 		=> 			'Please Enter Store Address',
		'MER_PLEASE_ENTER_STORE_ADDRESS_2' 		=> 			'Please Enter Store Address 2',
		'MER_PLEASE_SELECT_COUNTRY' 			=> 			'Please Select Country',
		'MER_PLEASE_SELECT_CITY' 				=> 			'Please Select City',
		'MER_PLEASE_SELECT_ZIP_CODE' 			=> 			'Please Select Zip Code',
		'MER_PLEASE_SELECT_META_KEYWORD' 		=> 			'Please Select Meta Keyword',
		'MER_PLEASE_SELECT_META_DESCRIPTION'	=> 			'Please Select Meta Description',
		'MER_PLEASE_ENTER_YOUR_LOCATION' 		=> 			'Please Enter Your Location',
		'MER_PLEASE_CHOOSE_IMAGE' 				=> 			'Please Choose Image',
		
	/*End add shop.blade.php*/
	/*Start attributes_addcolor.blade.php*/
		'MER_ADD_COLOR' 						=> 			'Add Color',
		'MER_APPLE' 							=> 			'Apple',
		'MER_APPROX' 							=> 			'approx',
		'MER_SELECT_A_COLOR' 					=> 			'Select a Color',
		'MER_YOUR_COLOR' 						=> 			'Your Color',
		'MER_RGB' 								=> 			'RGB',
	/*End attributes_addcolor.blade.php*/
	/*start attributes_addsize.blade.php*/
		'MER_ATTRIBUTES_ADD_SIZE' 				=> 			'Attributes Add Size',
		'MER_ADD_SIZE' 							=> 			'Add Size',
		'MER_SAVE' 								=> 			'Save',
	/*End attributes_addsize.blade.php*/
	/*start attributes_editcolor.blade.blade.php*/
		'MER_EDIT_COLOR' 						=> 			'Edit Color',
	/*End attributes_editcolor.blade.blade.php*/
	/*Start attributes_editsize.blade.php*/
		'MER_ATTRIBUTES_EDIT_SIZE' 				=> 			'Attributes Edit Size',
		'MER_EDIT_SIZE' 						=> 			'Edit Size',
	/*End attributes_editsize.blade.php*/
	
	/*End attributes_manage_color.blade.php*/
		'MER_MANAGE_COLOR' 						=> 			'Manage Color',
		'MER_S.NO' 								=> 			'S.No',
		'MER_COLOR_CODE' 						=> 			'Color Code',
		'MER_COLOR_NAME' 						=> 			'Color Name',
		'MER_EDIT' 								=> 			'Edit',
		'MER_DELETE' 							=> 			'Delete',
	/*End attributes_manage_color.blade.php*/
	/*start attributes_manage_sizes.blade.php*/
		'MER_ATTRIBUTES_MANAGE_SIZES' 			=> 			'Attributes Manage Sizes',
		'MER_MANAGE_SIZES' 						=> 			'Manage Sizes',
	/*End attributes_manage_sizes.blade.php*/
	/*start cod_list.blade.php*/
	
		'MER_CASH_ON_DELIVERY' 					=> 			'Cash On Delivery',
		'MER_EMAIL' 							=> 			'Email',
		'MER_EXAMPLE_BLOCK-LEVEL_HELP_TEXT_HERE' => 			'Example block-level help text here',
		'MER_ENTER_COUPEN_CODE' 				=> 			'Enter coupen code',
		'MER_CASH_ON_DELIVERY1' 				=> 			'CASH ON DELIVERY',
		'MER_TAX' 								=> 			'Tax',
		'MER_AMOUNT' 							=> 			'Remaining Amount',
		'MER_PAID_AMOUNT' 						=> 		'Paid Amount',
	/*end cod_list.blade.php*/
	/*start deal_details.blade.php*/
		
		'MER_SERVICES' 						    => 			   'Services',
		'MER_DEAL_TITLE' 						=> 			'Deal Title',
		'MER_MAIN_CATEGORY' 					=> 			'Main Category',
		'MER_DEAL_DESCRIPTION' 					=> 			'Deal Description',
		'MER_DEALS_PRICE' 						=> 			'Deals Price',
		'MER_DISCOUNT_PERCENTAGE' 				=> 			'Discount Percentage',
		'MER_SAVINGS' 							=> 			'Savings',
		'MER_PURCHASE_COUNT' 					=> 			'Purchase Count',
		'MER_MERCHANT_NAME' 					=> 			'Merchant Name',
		'MER_SHOP_NAME' 						=> 			'Shop Name',
		'MER_DEALS_IMAGE' 						=> 			'Deals Image',
		'MER_TRANSACTION_DETAILS' 				=> 			'Transaction Details',
		'MER_NO_DEALS_FOUND' 					=> 			'No Deals Found',
		'MER_WAITNIGN_FOR_TABLE' 				=> 			'Waitnign For Table',
	/*end deal_details.blade.php*/
	/*start deals_allorders.blade.php*/
		'MER_DEALS_ALL_ORDERS' 					=> 			'Deals All Orders',
		'MER_CUSTOMERS' 						=> 			'Customers',
		'MER_TRANSACTION_DATE' 					=> 			'Transaction Date',
		'MER_TRANSACTION_TYPE' 					=> 			'Transaction Type',
			
	/*end deals_allorders.blade.php*/
	/*start deals_completed_orders.blade.php*/
		'MER_DEALS_COMPLETED_ORDERS' 			=> 			'Deals Completed Orders',
			
	/*end deals_completed_orders.blade.php*/
		
	/*start deals_failed_orders.blade.php*/
		'MER_DEALS_FAILED_ORDERS' 				=> 			'Deals Failed Orders',
	/*end deals_failed_orders.blade.php*/
		
	/*start deals_hold_orders.blade.php*/
		'MER_DEALS_HOLD_ORDERS' 				=> 			'Deals Hold Orders',
	/*end deals_hold_orders.blade.php*/
		
	/*start deals_success_orders.blade.php*/
		'MER_DEALS_SUCCESS_ORDERS' 				=> 			'Deals Success Orders',
	/*end deals_success_orders.blade.php*/
		
	/*start dealscod_allorders.blade.php*/
		'MER_DEALS_COD_ALL_ORDERS' 				=> 			'Deals COD All Orders',
		'MER_COD_STATUS_ACTION' 				=> 			'COD Status Action',
	/*end dealscod_allorders.blade.php*/
		
	/*start dealscod_completed_orders.blade.php*/
		'MER_DEALS_COD_SUCCESS_ORDERS' 			=> 			'Deals COD Success Orders',
	/*end dealscod_completed_orders.blade.php*/
		
	/*start dealscod_failed_orders.blade.php*/
		'MER_DEALS_COD_FAILED_ORDERS' 			=> 			'Deals COD Failed Orders',
	/*end dealscod_failed_orders.blade.php*/
		
	/*start dealscod_hold_orders.blade.php*/
		'MER_DEALS_COD_HOLD_ORDERS' 			=> 			'Deals COD Hold Orders',
	/*end dealscod_hold_orders.blade.php*/
		
	/*start expired_deals.blade.blade.php*/
		'MER_MANAGE_EXPIRED_DEALS' 				=> 			'Manage Expired Deals',
		'MER_MANAGE_SOLD_DEALS'					=>			'Manage Sold Deals',	
		'MER_DEALS_NAME' 						=> 			'Deals Name',
		'MER_CITY' 								=> 			'City',
	/*end expired_deals.blade.blade.php*/
	
	/*start forgot_pwd_mail.blade.php*/
		'MER_ADMIN_DASHBOARD_TEMPLATE' 			=> 			'Admin Dashboard Template',
		'MER_FORGOT_PASSWORD_PAGE' 				=> 			'Forgot Password Page',
		'MER_MERCHANT_PASSWORD_RESET1' 			=> 			'MERCHANT PASSWORD RESET',
		'MER_NEW_PASSWORD' 						=> 			'New Password',
		'MER_CONFIRM_PASSWORD' 					=> 			'Confirm Password',
		'MER_BACK_TO_LOGIN' 					=> 			'Back To Login',
		'MER_PLEASE_PROVIDE_NEW_PASSWORD' 		=> 			'Please enter new password',
		'MER_PLEASE_PROVIDE_CONFIRM_PASSWORD' 	=> 			'Please enter confirm password',
		'MER_BOTH_PASSWORDS_DO_NOT_MATCH' 		=> 			'Both Passwords do not match',
		
	/*end forgot_pwd_mail.blade.php*/
	/*start fund_request.blade.php*/
		'MER_FUND_REQUEST_REPORT' 				=> 			'Fund Request Report',
		'MER_DATATABLES_ADVANCED_TABLES' 		=> 			'DataTables Advanced Tables',
		'MER_TRANSACTION_ID' 					=> 			'Transaction ID',
		
	/*end fund_request.blade.php*/
	/*start manage_deals.blade.php*/
		'MER_MANAGE_DEALS' 						=> 			'Manage Deals',
		'MER_ACTIONS' 							=> 			'Actions',
		
	/*end manage_deals.blade.php*/
	/*start manage_product.blade.php*/
		'MER_MANAGE_PRODUCTS' 					=> 			'Manage Products',
		'MER_PRODUCT_NAME' 						=> 			'Product Name',
		
	/*end manage_product.blade.php*/
	/*start manage_shop.blade.php*/
		'MER_MANAGE_MERCHANT_STORES' 			=> 			'Manage Merchant Stores',
		'MER_BLOCK' 							=> 			'Block',
		'MER_UNBLOCK' 							=> 			'Unblock',
		
	/*end manage_shop.blade.php*/
	/*start merchant_change_password.blade.php*/
		'MER_CHANGE_PASSWORD' 					=> 			'Change Password',
		'MER_OLD_PASSWORD' 						=> 			'Old Password',
		'MER_PLEASE_PROVIDE_OLD_PASSWORD' 		=> 			'Please enter old password',
		
		
	/*end merchant_change_password.blade.php*/
	
	/*start merchant_login.blade.blade.php*/
		'MER_MERCHANT_DASHBOARD_TEMPLATE' 		=> 			'Merchant Dashboard Template',
		'MER_LOGIN_PAGE' 						=> 			'Login Page',
		'MER_LOGO' 								=> 			'Logo',
		'MER_MERCHANT_LOGIN' 					=> 			'MERCHANT LOGIN',
		'MER_SIGN_IN' 							=> 			'Login',
		'MER_PASSWORD' 							=> 			'Password',
		'MER_ENTER_YOUR_VALID_E-MAIL' 			=> 			'Enter your valid e-mail',
		'MER_RECOVER_PASSWORD' 					=> 			'Recover Password',
		'MER_PLEASE_FILL_DETAILS_TO_REGISTER' 	=> 			'Please Fill Details To Register',
		'MER_REGISTER' 							=> 			'Register',
		'MER_FORGOT_PASSWORD' 					=> 			'Forgot Password?',
		'MER_ENTER_YOUR_EMAIL' 					=> 			'Enter your email',
		'MER_ENTER_YOUR_VALID_EMAIL' 			=> 			'Enter your valid Email',
		'MER_YOUR_E-MAIL' 						=> 			'Your E-mail',
		'MER_FIRST_NAME' 						=> 			'First Name',
		'MER_LAST_NAME' 						=> 			'Last Name',
		'MER_USERNAME' 							=> 			'Username',
		'MER_RE_TYPE_PASSWORD' 					=> 			'Re type password',
		
	/*end merchant_login.blade.blade.php*/
	
	/*start merchant_profile.blade.php*/
		'MER_MERCHANT_PROFILE' 					=> 			'Merchant profile',
		'MER_EMAIL-ID' 							=> 			'Email-id',
		'MER_PHONE_NUMBER' 						=> 			'Phone Number',
		'MER_ADDRESS_ONE' 						=> 			'Address 1',
		'MER_ADDRESS_TWO' 						=> 			'Address 2',
		'MER_COUNTRY' 							=> 			'Country',
	/*end merchant_profile.blade.php*/
	/*start merchant_settings.blade.php*/
		'MER_MERCHANT_SETTINGS' 				=> 			'Merchant Settings',
		'MER_EDIT_INFORMATION' 					=> 			'Edit Information',
	/*end merchant_settings.blade.php*/
	/*start merchant_dashboard.blade.php*/
		'MER_DASHBOARD' 						=> 			'Dashboard',
		'MER_MERCHANT_DASHBOARD' 				=> 			'Merchant Dashboard',
		'MER_ACTIVE_PRODUCTS' 					=> 			'Active Products',
		'MER_ACTIVE_DEALS' 						=> 			'Active Deals',
		'MER_ARCHIVE_DEALS' 					=> 			'Archive Deals',
		'MER_EXPIRED_DEALS' 					=> 			'Expired Deals',
		'MER_STORES' 							=> 			'Stores',
		'MER_GO_TO_LIVE' 						=> 			'Go to Live',
		'MER_ADD_SERVICESS' 		                => 			'Add Services',
		'MER_MONTH_WISE_TRANSACTIONS' 			=> 			'Month Wise Transactions',
		'MER_PRODUCT_TRANSACTION' 				=> 			'Product Transaction',
		'MER_DEALS_TRANSACTION' 				=> 			'Deals Transaction',
		'MER_JAN' 								=> 			'Jan',
		'MER_FEB' 								=> 			'Feb',
		'MER_MAR' 								=> 			'Mar',
		'MER_APR' 								=> 			'Apr',
		'MER_MAY' 								=> 			'May',
		'MER_JUNE' 								=> 			'June',
		'MER_JULY' 								=> 			'July',
		'MER_AUG' 								=> 			'Aug',
		'MER_SEP' 								=> 			'Sep',
		'MER_OCT' 								=> 			'Oct',
		'MER_NOV' 								=> 			'Nov',
		'MER_DEC' 								=> 			'Dec',
	/*end merchant_dashboard.blade.php*/
	/*start edit_deals.blade.php*/
		'MER_EDIT_DEALS' 						=> 			'Edit Deals',
	/*end edit_deals.blade.php*/
	
	/*start edit_shop.blade.php*/
		'MER_EDIT_MERCHANT_ACCOUNT' 			=> 			'Edit Merchant Account',
		'MER_EDIT_MERCHANT_STORE_ACCOUNT' 		=> 			'Edit Merchant Store Account',
		'MER_FRUIT_BALL' 						=> 			'Fruit ball',
	/*end edit_shop.blade.php*/
	

	/*start with_fund_request.blade.php */
	'MER_HOME' 										=> 			'Home',
	'MER_WITHDRAW_FUND_REQUEST'						=> 			'Withdraw Fund Request',
	'MER_TOTAL_PRODUCT_AMOUNT'						=> 			'Total Product amount',
	'MER_ADMIN_COMMISION_PERCENTAGE'				=> 			'Admin Commision percentage',
	'MER_WITHDRAW_ACCOUNT_BALANCE_IS'				=> 			'Withdraw Account Balance is',
	'MER_FUND_AMOUNT_RECEIVED'						=> 			'Fund Amount Received',
	'MER_BALANCE_WITHDRAW_AMOUNT'					=> 			'Balance Withdraw Amount',
	'MER_TOTAL_DEAL_AMOUNT'							=> 			'Total Deal amount',
	'MER_SUBMIT'									=> 			'Submit',
	'MER_PLEASE_ENTER_AMOUNT'						=> 			'Please Enter Amount',
	'MER_PLEASE_ENTER_VALID_AMOUNT'					=> 			'Please Enter Valid Amount',
	'MER_LARAVEL_ECOMMERCE_MULTIVENDOR_ADMIN_WITHDRAW_FUND_REQUEST'		=> 			'Withdraw Fund Request',
	/*close with_fund_request.blade.php */
	/*start add_product.blade.php */
	'MER_MERCHANT_ADD_PRODUCTS'						=> 			'Merchant | Add Products',
	'MER_ADD_PRODUCTS'								=> 			'Add Products',
	'MER_PRODUCT_TITLE'								=> 			'Product Title',
	'MER_TOP_CATEGORY'								=> 			'Top Category',
	'MER_SELECT'									=> 			'Select',
	'MER_SUB_CATEGORY'								=> 			'Sub Category',
	'MER_SECOND_SUB_CATEGORY'						=> 			'Second Sub Category',
	'MER_PRODUCT_QUANTITY'							=> 			'Product Quantity',
	'MER_ENTER_QUANTITY_OF_PRODUCT'					=> 			'Enter Quantity of Product',
	'MER_INCLUDING_TAX_AMOUNT'						=> 			'Including tax Percentage',
	'MER_SHIPPING_AMOUNT'							=> 			'Shipping Amount',
	'MER_FREE'										=> 			'Free',
	'MER_WANT_TO_ADD_SPECIFICATION'					=> 			'Want to add specification',
	'MER_YES'										=> 			'Yes',
	'MER_NO'										=> 			'No',
	'MER_Price'										=> 			'Price',
	'MER_TEXT'										=> 			'Text',
	'MER_SPECIFICATION'								=> 			'Specification',
	'MER_SELECT_SPECIFICATION'						=> 			'select specification',
	'MER_MORE_CUSTOM_SPECIFICATION'					=> 			'More custom specification',
	'MER_ADD_PRODUCT_SIZE'							=> 			'Add Product Size',
	'MER_ADD_MORE'									=> 			'Add more',
	'MER_PRODUCT_SIZE'								=> 			'Product Size',
	'MER_MORE_CUSTOM_SIZES'							=> 			'More custom sizes ',
	'MER_YOUR_SELECT_SIZE'							=> 			'Your Select size ',
	'MER_ADD_COLOR_FIELD'							=> 			'Add Color Field ',
	'MER_PRODUCT_COLOR'								=> 			'Product Color ',
	'MER_SELECT_PRODUCT_COLOR'						=> 			'Select product color ',
	'MER_MORE_CUSTOM_COLORS'						=> 			'More custom colors',
	'MER_YOUR_SELECT_COLOR'							=> 			'Your Select Color',
	'MER_DELIVERY_DAYS'								=> 			'Delivery Within',
	'MER_EG'										=> 			'Eg : ( 2 - 5 ) ',
	'MER_IMAGE_SIZE_MUST_BE_250_200_PIXELS'			=> 			'image size must be',
	'MER_ADD_PRODUCT'					=> 			'Add Product',
	'MER_MAXIMUM_LIMIT_REACHED'			=> 			'Maximum limit reached ',
	'MER_ALREADY_COLOR_IS_CHOOSED'		=> 			'Already color is choosed ',
	'MER_SOMETHING_WENT_WRONG'			=> 			'something went wrong ',
	'MER_ALREADY_SIZE_IS_CHOOSED'		=> 			'Already size is choosed ',
	'MER_PLEASE_SELECT_SPECIFICATION_AND_PROVIDE_TEXT_THEN_CLICK_ADD_MORE'					=> 			'Please select specification and provide text then click Add more ',
	'MER_THIS_PRODUCT_TITLE_ALREADY_EXIST_IN_THIS_STORE'					=> 			'This Product Title Already Exist in this Store',
	'MER_NUMBERS_ONLY_ALLOWED'			=> 			'Numbers Only Allowed',
	'MER_PLEASE_SELECT_CATEGORY'		=> 			'Please Select Category',
	'MER_PLEASE_SELECT_MAIN_CATEGORY'	=> 			'Please Select Main Category',
	'MER_PLEASE_ENTER_PRODUCT_QUANTITY'	=> 			'Please Enter Product Quantity',
	'MER_PLEASE_ENTER_ORIGINAL_PRICE'	=> 			'Please Enter Original Price',
	'MER_PLEASE_ENTER_DISCOUNT_PRICE'	=> 			'Please Enter Discount Price',
	'MER_DISCOUNT_PRICE_SHOLUD_BE_LESS_THAN_ORIGINAL_PRICE'	=> 			'Discount price sholud be less than original price',
	'MER_PLEASE_PROVIDE_SHIPPING_AMOUNT'=> 			'Please Provide Shipping Amount',
	'MER_PLEASE_ENTER_DESCRIPTION'		=> 			'Please Enter Description',
	'MER_PLEASE_SELECT_SPECIFICATION_AND_GIVE_TEXT'	=> 			'Please Select specification and give text',
	'MER_PLEASE_SELECT_PRODUCT_SIZE'	=> 			'Please Select Product size',
	'MER_PLEASE_SELECT_COLOR'			=> 			'Please select color',
	'MER_PLEASE_ENTER_DELIVERY_DAYS'	=> 			'Please Enter Delivery Days',
	'MER_PLEASE_ENTER_DIFFERENT_PRODUCT_TITLE_THIS_ALREADY_EXIST_IN_THIS_STORE'	=> 			'Please Enter Different Product Title, This Already Exist in this Store',
	'MER_PLEASE_ENTER_META_DESCRIPTION'	=> 			'Please Enter Meta Description',
	'MER_PLEASE_CHOOSE_VALID_IMAGE'		=> 			'Please choose valid image',
	'MER_ENTER_YOUR_PRODUCT_TITLE'		=> 			'Enter Your Product Title',
	/*Close add_product.blade.php */
	
	/*Start validate_coupon.blade.php */
	'MER_VALIDATE_COUPON_CODE'			=> 			'Validate Coupon Code',
	'MER_CODE'							=> 			'Code',
	'MER_UPDATE'						=> 			'Update',
	'MER_CANCEL'						=> 			'Cancel',
	'MER_ADMIN_VALIDATE_COUPON_CODE'	=> 			'Admin |Validate Coupon Code',
	/*close validate_coupon.blade.php */
	
	/*Start sold_products.blade.php */
	'MER_MERCHANT_SOLD_PRODUCTS'		=> 			'Merchant | Sold Products',
	'MER_SOLD_PRODUCTS'					=> 			'Sold Products',
	'MER_FROM_DATE'						=> 			'From Date',
	'MER_TO_DATE'						=> 			'To Date',
	'MER_SEARCH'						=> 			'Search',
	'MER_VIEW'							=> 			'Manage',
	'MER_DIP_MULTIVENDOR_2014'			=> 			'Dip Multivendor 2014',
	'MER_CLICK_FOR_PREVIOUS_MONTHS'		=> 			'click for previous months',
	'MER_CLICK_FOR_NEXT_MONTHS'			=> 			'click for next months',
	/*close sold_products.blade.php */
	
	/*start shipping_list.blade.php */
	'MER_MERCHANT_SHIPPING_AND_DELIVERY'=> 			'Merchant |Shipping and Delivery',
	'MER_SHIPPING_AND_DELIVERY'			=> 			'Shipping and Delivery',
	'MER_S_No'							=> 			'S.No',
	'MER_PRODUCTS_NAME'					=> 			'Products Name',
	'MER_NAME'							=> 			'Name',
	'MER_DATE'							=> 			'Date',
	'MER_ADDRESS'						=> 			'Address',
	'MER_ADDRESS1'						=> 			'Address 1',
	'MER_ADDRESS2'						=> 			'Address 2',
	'MER_DETAILS'						=> 			'Details',
	'MER_DELIVERY_STATUS'				=> 			'Delivery Status',
	'MER_VIEW_DETAILS'					=> 			'View details',
	'MER_EXAMPLE_BLOCK_LEVEL_HELP_TEXT_HERE'					=> 			'Example block-level help text here',
	'MER_MESSAGE'						=> 			'Message',
	'MER_ASDSA_CALIFORNIA_CANADIAN'		=> 			'asdsa,California,Canadian',
	'MER_CLOSE'							=> 			'Close',
	'MER_SEND'							=> 			'Send',
	'MER_TAX_INVOICE'					=> 			'TAX INVOICE',
	'MER_AMOUNT_PAID'					=> 			'Amount Paid ',
	'MER_INCLUSIVE_OF_ALL_CHARGES'		=> 			'inclusive of all charges',
	'MER_ORDER_DATE'					=> 			'Order Date',
	'MER_ORDER_ID'					    => 			'Order Id',
	'MER_SHIPPING_ADDRESS'				=> 			'Shipping Address',
	'MER_PHONE'							=> 			'Phone',
	'MER_INVOICE_DETAILS'				=> 			'Invoice Details',
	'MER_SUCCESS'						=> 			'Success',
	'MER_COMPLETED'						=> 			'Completed',
	'MER_HOLD'							=> 			'Hold',
	'MER_FAILED'						=> 			'failed',
	'MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS'							=> 			'This shipment contains following items',
	'MER_COLOR'							=> 			'Color',
	'MER_SIZE'							=> 			'Size',
	'MER_QUANTITY'						=> 			'Quantity',
	'MER_SUB_TOTAL'						=> 			'Sub Total',
	'MER_SHIPMENT_VALUE'				=> 			'Shipment Value',
	/*close shipping_list.blade.php */
	
	/*start redeem_coupon.blade.php */
	'MER_ADMIN_REDEEM_COUPON_LIST'		=> 			'Admin |Redeem Coupon List',
	'MER_REDEEM_COUPON_LIST'			=> 			'Redeem Coupon List',
	'MER_SEARCH_DEALS'					=> 			'Search Deals',
	'MER_COUPON_CODE'					=> 			'Coupon Code',
	'MER_DEALS_NAME_STORE_NAME'			=> 			'Deals Name, Store Name',
	/*close redeem_coupon.blade.php */
	
	/*start productcod_hold_orders.blade.php */
	'MER_MERCHANT_PRODUCTS_HOLD_ORDERS'	=> 			'Merchant|Products Hold Orders',
	'MER_COD_HOLD_ORDERS'				=> 			'COD Hold Orders',
	'MER_AMOUNT_S'						=> 			'Amount (SAR)',
	'MER_TAX_S'							=> 			'Tax',
	'MER_STATUS'						=> 			'Status',
	'MER_COD'							=> 			'COD',
	'MER_PAYPAL'						=> 			'Paypal',
	
	'MER_PAYUMONEY'						=> 			'PayUMoney',
	'BACK_DELIVERY_STATUS'				=> 			'Delivery Status',
	/*close productcod_hold_orders.blade.php */
	
	/*start productcod_failed_orders.blade.php */
	'MER_ADMIN_COD_FAILED_ORDERS'		=> 			'Admin |COD Failed Orders',
	'MER_COD_FAILED_ORDERS'				=> 			'COD Failed Orders',
	/*close productcod_failed_orders.blade.php */
	
	/*start productcod_completed_orders.php */
	'MER_ADMIN_COD_SUCCESS_ORDERS'		=> 			'Admin |COD Success Orders',
	'MER_COD_SUCCESS_ORDERS'			=> 			'COD Success Orders',
	/*close productcod_completed_orders.php */
	
	/*start productcod_all_orders.blade.php */
	'MER_ADMIN_COD_ALL_ORDERS'			=> 			'Admin |COD All Orders',
	'MER_COD_ALL_ORDERS'				=> 			'COD All Orders',
	/*close productcod_all_orders.blade.php */
	
	/*start product_success_orders.blade.php */
	'MER_ADMIN_PRODUCTS_SUCCESS_ORDERS'	=> 			'Admin |Products Success Orders',
	'MER_PRODUCTS_SUCCESS_ORDERS'		=> 			'Products Success Orders',
	/*close product_success_orders.blade.php */
	
	/*start product_shipping_details.blade.php */
	'MER_ADMIN_SHIPPING_AND_DELIVERY'	=> 			'Admin | Shipping And Delivery',
	'MER_NAME_EMAIL_COUPON_CODE'		=> 			'Name,Email,Coupon Code',
	'MER_NO_DATA_FOUND'					=> 			'No Data Found',
	/*close product_shipping_details.blade.php */
	
	/*start product_hold_orders.blade.php */
	'MER_MERCHANT_PRODUCTS_HOLD_ORDERS'	=> 			'Merchant | Products Hold Orders',
	'MER_PRODUCTS_HOLD_ORDERS'			=> 			'Products Hold Orders',
	/*close product_hold_orders.blade.php */
	
	/*start product_failed_orders.blade.php */
	'MER_ADMIN_PRODUCTS_FAILED_ORDERS'	=> 			'Admin |Products Failed Orders',
	'MER_PRODUCTS_FAILED_ORDERS'		=> 			'Products Failed Orders',
	/*close product_failed_orders.blade.php */
	
	/*start product_details.blade.php */
	'MER_MERCHANT_PRODUCT_DETAILS'		=> 			'Merchant | Product details',
	'MER_PRODUCT_DETAILS'				=> 			'Product details',
	'MER_PRODUCT_DESCRIPTION'			=> 			'Product Description',
	'MER_PRODUCT_PRICE'					=> 			'Product Price',
	'MER_DISCOUNT_PRICE'				=> 			'Discount Price',
	'MER_PRODUCT_IMAGE'					=> 			'Product Image',
	'MER_BACK'							=> 			'Back',
	'MER_ORDERED_AMOUNT'				=> 	'Ordered Amount',
	
	/*close product_details.blade.php */
	
	
	
	/*start product_details.blade.php */
	'MER_ADMIN_PRODUCTS_COMPLETED_ORDERS'							=> 			'Admin |Products Completed Orders',
	'MER_PRODUCTS_COMPLETED_ORDERS'		=> 			'Products Completed Orders',
	/*close product_details.blade.php */
	
	/*start product_cashondelivery.blade.php */
	'MER_MERCHANT_CASH_ON_DELIVERY'		=> 			'Merchant | Cash On Delivery',
	/*close product_cashondelivery.blade.php */
	
	/*start product_allorders.blade.php */
	'MER_ADMIN_PRODUCTS_ALL_ORDERS'		=> 			'Admin |Products All Orders',
	'MER_PRODUCTS_ALL_ORDERS'			=> 			'Products All Orders',
	/*close product_allorders.blade.php */
	
	/*start merchant_transactiondashboard.blade.php */
	'MER_ADMIN_TRANSACTION_DASHBOARD'	=> 			'Admin | Transaction Dashboard',
	'MER_TRANSACTION'					=> 			'Transaction',
	'MER_TRANSACTION_DASHBOARD'			=> 			'Transaction Dashboard',
	'MER_TOTAL_TRANSACTIONS_COUNT'		=> 			'Total Transactions Count',
	'MER_PRODUCTS'						=> 			'Products',
	'MER_DEALS'							=> 			'Deals',
	'MER_AUCTIONS'						=> 			'Auctions',
	'MER_MARKETING_RESPONSE_RATE'		=> 			'MARKETING RESPONSE RATE',
	'MER_DEALS_TRANSACTION_DETAILS'		=> 			'Deals Transaction Details',
	'MER_TRANSACTIONS'					=> 			'Transactions',
	'MER_COUNT'							=> 			'Count',
	'MER_TODAY'							=> 			'Today',
	'MER_LAST_7_DAYS'					=> 			'Last 7 days',
	'MER_LAST_30_DAYS'					=> 			'Last 30 days',
	'MER_PRODUCTS_TRANSACTION_DETAILS'	=> 			'Products Transaction Details',
	'MER_AUCTION_TRANSACTION_DETAILS'	=> 			'Auction Transaction Details',
	'MER_STATISTICS'					=> 			'Statistics',
	'MER_LAST_ONE_YEAR_PRODUCT_TRANSACTION'			=> 			'Last One Year Product Transaction',
	'MER_LAST_ONE_YEAR_DEALS_TRANSACTION'			=> 			'Last One Year Deals Transaction',
	'MER_LAST_ONE_YEAR_AUCTION_TRANSACTION'			=> 			'Last One Year Auction Transaction',
	/*close merchant_transactiondashboard.blade.php */
	
	/* start edit_product.blade.php */
	'MER_MERCHANT_EDIT_PRODUCTS'		=> 			'Merchant | Edit Products',
	'MER_EDIT_PRODUCTS'					=> 			'Edit Products',
	'MER_BRAND'							=> 			'Brand',
	'MER_SELECT_BRAND'					=> 			'Select Brand',
	'MER_SELECT_SIZE'					=> 			'Select Size',
	'MER_MAX_5'							=> 			'max 5',
	'MER_UPDATE_PRODUCT'				=> 			'Update Product',
	'MER_PLEASE_SELECT_BRAND'			=> 			'Please Select Brand',
	'MER_IMAGE_REMOVED_SUCCESSFULLY'    =>          'Product Image Removed Successfully',
	'BACK_REMOVE'                       =>          'Remove',
	'BACK_ADD'							=>           'Add',
	/* close edit_product.blade.php */
	
	/* start edit_merchant_account.blade.php */
	'MER_ADMIN_ADD_MERCHANT_ACCOUNT'	=> 			'Admin |Add Merchant Account',
	'MER_EDIT_MERCHANT_ACCOUNT'			=> 			'Edit Merchant Account',
	'MER_MERCHANT_ACCOUNT'				=> 			'Merchant Account',
	'MER_SELECT_COUNTRY'				=> 			'Select Country',
	'MER_SELECT'						=> 			'Select',
	'MER_SELECT_CITY'					=> 			'Select City',
	'MER_ADDRESS1'						=> 			'Address 1',
	'MER_ADDRESS2'						=> 			'Address 2',
	'MER_PAYFORT_KEY'				=> 			     'Payfort Key',
	'MER_PAYFORT_SALT_KEY'				=> 			 'Payfort Salt Key',
	'MER_COMMISSION'					=> 			'Commission',
	/* close edit_merchant_account.blade.php */
	
	
	
	
	
##################################################### START CONTROLLER WORK #######################################
	
	
	/* Start MerchantTransactionController.php */
	'MER_TRANSACTION '										=> 			'transaction',
	'MER_YOUR_ORDER_STATUS_DETAILS '						=> 			'Your Order Status Details',
	/* close MerchantTransactionController.php */
	
	/* Start MerchantSettingsController.php */
	'MER_SETTINGS'											=> 			'settings',
	'MER_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY'				=> 			'Merchant Account Created Successfully',
	'MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY'				=> 			'Merchant Account Created', 

	'MER_OLD_PASSWORD_DONOT_MATCH'							=> 			'Old passwords are not matching',
	'MER_PASSWORD_CHANGED_SUCCESSFULLY'						=> 			'Password Changed Successfully',
	'MER_BOTH_PASSWORDS_DONOT_MATCH'						=> 			'Both passwords are not matching',
	'MER_RECORD_UPDATED_SUCCESSFULLY'						=> 			'Record Updated Successfully',
	'MER_MERCHANT_EMAIL_EXIST'								=> 			'Merchant Email Exist',
	'MER_YOUR_ORDER_STATUS_DETAILS '						=> 			'Your Order Status Details',
	
	/* close MerchantSettingsController.php */
	
	/* Start MerchantSettingsController.php */
	'MER_RECORD_INSERTED_SUCCESSFULLY'						=> 			'Record Inserted Successfully',
	
	/* close MerchantSettingsController.php */
	
	/* Start MerchantshopController.php */
	'MER_SHOP'												=> 			'Shop',
	'MER_STORE_BLOCKED_SUCCESSFULLY'						=> 			'Store Blocked Successfully',
	'MER_STORE_ACTIVATED_SUCCESSFULLY'						=> 			'Store Activated Successfully',
	'MER_STORE_ADDED_SUCCESSFULLY'							=> 			'Store Added Successfully',
	'MER_STORE_UPDATED_SUCCESSFULLY'						=> 			'Store Updated Successfully',
	
	/* close MerchantshopController.php */
		
	/*Start FundController.php*/
	
		'MER_FUNDS' 										=> 		'funds',
		'MER_RECORD_UPDATED_SUCCESSFULLY' 					=> 		'Record Updated Successfully',
		
	/*End FundController.php*/
	
	/*Start MerchantattributeController.php*/

		'MER_SETTINGS' 										=> 		'settings',
		'MER_SIZE_NAME_EXIST' 								=> 		'Size Name Exist',
		'MER_RECORD_INSERTED_SUCCESSFULLY' 					=> 		'Record Inserted Successfully',
		'MER_RECORD_DELETED_SUCCESSFULLY' 					=> 		'Record Deleted Successfully',
		
	/*End MerchantattributeController.php*/	
	/*Start DashboardController.php*/

		'MER_BLOGCMTCOUNT' 								=> 		'blogcmtcount',
		'MER_NEWSUBSCRIBERSCOUNT' 						=> 		'newsubscriberscount',
		'MER_INQUIRIESCNT' 								=> 		'inquiriescnt',
		'MER_ADREQUESTCNT' 								=> 		'adrequestcnt',
		'MER_ADREQUESTCNT' 								=> 		'adrequestcnt',
		
	/*End DashboardController.php*/
	
	/*Start MerchantdealsController.php*/
		'MER_DEAL_UPDATED_SUCCESSFULLY' 					=> 		'Deal Updated Successfully',
		'MER_DEAL_ACTIVATED_SUCCESSFULLY' 					=> 		'Deal Unblocked Successfully',
		'MER_DEAL_BLOCKED_SUCCESSFULLY' 					=> 		'Deal Blocked Successfully',
		'MER_DEAL_DELETED_SUCCESSFULLY' 					=> 		'Deal Deleted Successfully',
	/*End MerchantdealsController.php*/
	/*Start MerchantLoginController.php*/

		'MER_LOGIN_SUCCESS' 								=> 		'Login Success',
		'MER_LOGOUT_SUCCESS' 								=> 		'Successfully Logout',
		'MER_INVALID_USERNAME_AND_PASSWORD' 				=> 		'Invalid Username and Password',
		'MER_PASSWORD_RECOVERY_DETAILS' 					=> 		'Password Recovery Details',
		'MER_MAIL_SEND_SUCCESSFULLY' 						=> 		'Email send successfully',
		'MER_INVALID_EMAIL' 								=> 		'Invalid Email',
		'MER_PASSWORD_CHANGED_SUCCESSFULLY' 				=> 		'Password Changed Successfully',
		
	/*End MerchantLoginController.php*/
	/*Start MerchantServiceController.php*/

		'MER_SERVICES' 										=> 		'Services',	
		'MER_DELETED_SUCCESSFULLY' 							=> 		'Deleted Successfully',
		'MER_SERVICE_DELETED_SUCCESSFULLY' 					=> 		'Service Deleted Successfully',
		'MER_RECORD_UPDATION_FAILED' 						=> 		'Record Updation Failed',
		'MER_SERVICES_UNBLOCKED' 							=> 		'Services unblocked',
		'MER_SERVICES_BLOCKED' 								=> 		'Services Blocked',
		
		
	/*End MerchantServiceController.php*/	
	/*Start MerchantproductController.php*/

		'MER_PRODUCTS' 										=> 		'products',	
		'MER_PRODUCT_ADDED_SUCCESSFULLY' 					=> 		'Product Added Successfully',
		'MER_PRODUCT_UPDATED_SUCCESSFULLY' 					=> 		'Product Updated Successfully',
		'MER_SELECT_MAIN_CATEGORY' 							=> 		'Select Main Category',	
		'MER_NO_LIST_AVAILABLE_IN_THE_CATEGORY' 			=> 		'No list available in the category',	
		'MER_SELECT_SUB_CATEGORY' 							=> 		'Select sub Category',
		'MER_SELECT_SECOND_SUB_CATEGORY' 					=> 		'Select Second Sub Category',
		'MER_PRODUCT_UNBLOCKED' 							=> 		'Product unblocked',	
		'MER_PRODUCT_BLOCKED' 								=> 		'Product Blocked',
		'MER_PRODUCT_DELETED_SUCCESSFULLY' 					=> 		'Product Deleted Successfully',	
		
	/*End MerchantproductController.php*/
##################################################### END CONTROLLER WORK #######################################

################################################# MERCHANT EMAIL TEMPLATE #####################################
	/*Start merchantmail.blade.php*/
			'BACK_WALLET'						=> 'Wallet',

		'MAIL_EMAIL_TEMPLATE'     							=> 		'Email Template',
		'MAIL_HI'     										=> 		'Hi',
		'MAIL_YOUR_LOGIN_CREDENTIALS_ARE'     				=> 		'Your Login Credentials Are',
		'MAIL_USER_NAME'     								=> 		'User Name',
		'MAIL_PASSWORD'     								=> 		'Password',
		'MAIL_LOGIN_YOUR_ACCOUNT'     						=> 		'Login Your Account',
		'MOBILE_YOUR_MERCHANT_ACCOUNT_WAS_CREATED_SUCCESSFULLY'    => 		'Your Merchant Account Was Created Successfully',
		
	/*End merchantmail.blade.php*/
	/*start merchant_passwordrecoverymail.blade.php*/
		'MER_PASSWORD_RECOVERY_DETAILS_FOR_MERCHANT' 		=> 		'Password Recovery Details For Merchant',
		'MER_YOUR_LOGIN_CREDENTIALS_ARE' 					=> 		'Your Login Credentials Are',
		'MER_USER_NAME' 									=> 		'User Name',
		'MER_EMAIL_LINK' 									=> 		'Reset Password',
		'MER_PLEASE_CLICK_THE_LINK_TO_RESET_YOUR_PASSWORD' 	=> 		'Please click the link to reset your password',
		
		
	/*end merchant_passwordrecoverymail.blade.php*/

	/* FUND REQUEST NOTIFICATION  */
	'MER_MERCHANT_UNABLE_TO_REQUEST_FUNDS'    				=>		'Sorry, Merchant unable to request fund. Because Merchant holding admin amount!',

	/* MERCHNAT COMMISSION LISTING */
	'MER_COMMISSION'										=>		'commissions',
	'MER_COMMISSION_LISTING'								=>		'Commission Listing',
	'MER_COMMISSION_PAID'									=>		'Commission Paid',
	'MER_TOTAL_COD_ORDER_AMOUNT'							=>		'Total COD Order Amount',
	'MER_TOTAL_ORDER_AMOUNT'								=>		'Total Order Amount',
	'MER_TOTAL_COD_USED_COUPON_AMOUNT'						=>		'Total Coupon Amount',
	'MER_COMMSSION_TO_PAY'									=>		'Commission need to pay',
	'MER_COMMSSION_TO_PAID'									=>		'Commission Paid',
	'MER_COMMSSION_BALANCE'									=>		'Balance',
	'MER_COMMISSION_PAYMENT'								=>		'Payment Method',
	'MER_COMMISSION_PAY_OFFLINE'							=>		'Pay Offline',
	'MER_COMMISSION_PAY_ONLINE'								=>		'Pay Online',
	'MER_PAYMENT_TYPE'										=>		'Payment Method',
	'MER_MERCHANT_NO_NEED_TO_PAY_COMMISSION'				=>		'Merchant no need to pay commission.Because Admin holding the larger amount than merchant commision',
	'MERCHANT_NO_NEED_TO_PAY_COMMISSION'				=>		'Merchant no need to pay commission.Because no transaction has been made',
	'MER_COMMISSION_PAYMENT_COMPLETED_SUCCESSFULLY'			=>		'Commision Payment Completed Successfully',
	'MER_COMMISSION_PAYMENT_CANCELLED'						=>		'Commision Payment Cancelled',
	'MER_COMMSSION_OFFLINE_PAY'								=>		'Commission offline pay',
	'MER_TOTAL_COMMISSION_AMOUNT'							=>		'Total Commission Amount',
	
################################################# END MERCHANT EMAIL TEMPLATE #####################################
	/* Invoice ORder ID */
	'MER_ORDERID'											=> 		'Order ID',
	'MER_PRICE'												=> 		'Price',
	/* Missed  */
	'MER_SHIPPING_DELIVERY'										=>		'Shipping Delivery',
'MER_PAYU_SHIPPING_DELIVERY'										=>		'PayUMoney Shipping Delivery',
	// Merchant withdraw page
	'MER_ADMIN_COMMISION_AMOUNT'							=> 'Admin Commision Amount',
	'MER_ADMIN_COMMISION_PAID'							=> 'Admin Commision Paid',
	'ADMIN_COMMISION_PAID_BY_MER'							=> 'Admin Commision Paid By Merchant',
	/* Image Edit success message */
	'MER_IMAGE_UPDATED_SUCCESSFULLY'						=> 		'Image Updated Successfully',
	//policy applicable to product
	'BACK_APPLY_CANCEL'										=>  'Apply Cancellation Policy',
	'BACK_APPLY_RETURN'										=>  'Apply Return/Refund Policy',
	'BACK_APPLY_REPLACEMENT'								=>  'Apply Replacement Policy',
	'BACK_CANCEL_DESCRIPTION'								=>  'Cancellation Policy',
	'BACK_RETURN_DESCRIPTION'								=>  'Return Policy',
	'BACK_REPLACE_DESCRIPTION'								=>  'Replacement Policy',
	'BACK_ENTER_CANCEL_DESCRIPTION'							=>  'Enter Cancellation Policy',
	'BACK_ENTER_RETURN_DESCRIPTION'							=>  'Enter Return Policy',
	'BACK_ENTER_REPLACE_DESCRIPTION'						=>  'Enter Replacement Policy',
	'BACK_DAYS_CANCEL_DESCRIPTION'							=>  'No of days Cancellation Applicable',
	'BACK_DAYS_RETURN_DESCRIPTION'							=>  'No of days Return Applicable',
	'BACK_DAYS_REPLACE_DESCRIPTION'							=>  'No of days Replacement Applicable',
	//Cancel,return and replace starts
	'MER_CANCELLED'											=> 	'Cancelled',
	'MER_RETURNED'											=> 	'Returned',
	'MER_REPALCED'											=> 	'Replaced',
	'MER_COD_RETURN_ORDERS'									=> 	'COD Return Orders',
	'MER_RETURN_ORDERS'										=> 	'Return Orders',
	'MER_COD_REPLACE_ORDERS'								=> 	'COD Replacement Orders',
	'MER_REPLACE_ORDERS'									=> 	'Replacement Orders',
	'MER_CANCEL_ORDERS'										=> 	'Cancel Orders',
	'MER_APPROVE_CANCEL'									=> 	'Approve Orders Cancellation',
	'MER_APPROVE_RETURN'									=> 	'Approve Orders Return',
	'MER_APPROVE_REPLACEMENT'								=> 	'Approve Orders Replacement',
	'MER_APPROVED_SUCCESSFULLY'								=> 	'Approved Successfully',
	'MER_DISAPPROVED_SUCCESSFULLY'							=> 	'Dispproved Successfully',
	'MER_APPROVE_DISAPPROVED_NOTE'							=> 	'Approve/Disapprove Reason',
	'MER_CANCEL_NOTE'										=> 	'Cancellation Reason',
	'MER_RETURN_NOTE'										=> 	'Cancellation Reason',
	'MER_REPLACEMENT_NOTE'									=> 	'Replacement Reason',
	'MER_CANCEL_APPLIED_ON'									=> 	'Cancellation Applied On',
	'MER_RETURN_APPLIED_ON'									=> 	'Return Applied On',
	'MER_REPLACE_APPLIED_ON'								=> 	'Replacement Applied On',
	'MER_CANCEL_PROCESSED_ON'								=> 	'Cancellation Processed On',
	'MER_RETURN_PROCESSED_ON'								=> 	'Return Processed On',
	'MER_REPLACE_PROCESSED_ON'								=> 	'Replacement Processed On',
	'MER_CANCEL_STATUS'										=> 	'Cancellation Status',
	'MER_RETURN_STATUS'										=> 	'Return Status',
	'MER_REPLACE_STATUS'									=> 	'Replacement Status',

	/* Arun Start */
	'Dashboard'	=> 'Dashboard',
	'Vendor_Info'	=> 'Vendor Info',
	'Services'	=> 'Services',
	'Shipping_Option'=> 'Shipping Option',
	'MER_Services' =>'Services',
	'MER_Shipping_Options' => 'Shipping Options',
	'SHIPPING_NAME' => 'Shipping Name',
	'MER_LIST_SERVICES'=> 'Service Listing',
	'MER_ACTION' => 'Manage',
	'MER_ADD_SERVICES'=> 'Add Service',
	'MER_ADD_HALLSERVICE'=>'Add Hall Service',
	'MER_SERVICE_NOTES'=>'Notes',
	'MER_SERVICE_NAME'=>'Service',
	'MER_FREESERVICE'=>'Free Service',
	'MER_ADD_HALLPAIDSERVICE'=>'Paid Service',
	'MER_SERVICE_PRICE'=>'Price (SAR)',
	'MER_SERVICE_ADDMORE'=>'Add More',
	'MER_HALL_PICTURE'=>'Hall Pictures and Video',
	'MER_UPLOAD_HALLPICTURTE'=>'Upload Picture',
	'MER_HALL_UPLOADVIDEO'=>'Enter YouTube Video URL',
	'MER_HALL_INFO'=>'Hall Info',
	'MER_HALL_NAME'=>'Hall Name',
	'MER_HALL_IMAGE'=>'Hall Image',
	'MER_GOOGLE_MAP_ADDRESS'=>'Google Map Address URL',
	'MER_HALLDIM'=>'Hall Dimension',
	'MER_FOOD'=>'Food',
	'MER_HALL_TYPE'=>'Hall Type',
	'MER_HALL_PRICE'=>'Hall Price (SAR)',
	'MER_HALLarea'=>'Area',
	'MER_HALLWIDTH'=>'Width',
	'MER_HALLLENGTH'=>'Length',
	'MER_HALLINTERNAL'=>'Our Menu',
	'MER_HALLEXTERNAL'=>'Food From Outside',
	'MER_WOMEN'=>'Women',
	'MER_MEN'=>'Men',
	'MER_ADDRESSIMG'=>'Address Image',
	'MER_HALLPICTUREANDVIDEO'=>'Hall Pictures and Video',
	'MER_HALLSERVICES'=>'Hall Services',
	'MER_HALLMENUCATEGORY'=>'Menu Category',
	'MER_HALLMENU'=>'Menu',
	'MER_HALLDishes'=>'Dishes',
	'MER_Container'=>'Container',
	'MER_Offer'=>'Offers',
	'MER_Order'=>'Order',
	'MER_Order_Detail'=>'Order Detail',
	'MER_Reviewandcomments'=>'Reviews and Ratings', 
	'MER_SUBMIT'=>'Submit',
	'MER_Delete'=>'Delete',
	'MER_CUSTOMERNAMEEMAIL'=>'Customer Name/Email',
	'MER_CUSTOMERMOBILE'=>'Mobile',
	'MER_ORDERSTATUS'=>'Order Status',
	'MER_BOOKINGDATE'=>'Booking Date',
	'MER_AMOUNT'=>'Remaining Amount',
	'MER_PAYMENTSTATUS'=>'Payment Status',
	'MER_ACTION'=>'Action',
	'MER_REVIEWANDRATING' => 'Reviews and Ratings',
	'MER_OFFER'=>'Offers',
	'MER_DEACTIVE' =>'Inactive',
	'MER_LIST_OF_SERVICES'=>'Services List',
	'MER_HallLIST'=>'Hall Lists',
	'MER_ADDHALL'=>'Add Hall',
	'MER_HALLSNAME'=>'Hall Name',
	'NORECORDFOUND'=>'No record found',
	'ACTION'=>'Action',
	'MER_HOTELS'=>'Hotels',
	'MER_HALLIMAGE'	   => 		'Hall Image',
	'FOOD' => 'Food',
	'HALLTYPE' => 'Hall type',
	'LASTMODDATE'=> 'Last Modified',
	'STATUS'=> 'Status',
	'HALL_INFO'=> 'Hall Info',
	'MER_HALL_BRANCHES'=> 'Branches',
	'MER_BRANCHES'=> 'Branch List',
	'MER_LISTMANAGER'=> 'Manager List',
	'MER_PAYMENT'=> 'Payment',
	'MER_STATUS'=> 'Status', 
	'MER_EDITHOTALNAME'=>'Update Hotel',
	'MER_ADD_MANAGER' => 'Add Manager',
	'MER_UPDATE_MANAGER'=> 'Update Manager',
	'PLEASE_ENTER_VALID_IMAGE' => 'Please upload valid (JPG,JPEG,PNG) image',
	'MER_ADD_HALL_FREE_SERVICE' => 'Add Hall Free Service',
	'MER_ADD_HALL_PAID_SERVICE' => 'Add Hall Paid Service',
	'NO_RECORD_FOUND'=> 'No record found',
	'MER_DISH' => 'Dish',
	'Thank_you_for_signing' =>'Thank you for signing up. You are to receive an email confirmation as well.',
	'MER_ADD_CATEGORY' => 'Add Category',
	'MER_CATEGORY_LIST'=> 'Category List',
	'MER_CATEGORY_DESCRIPTION'=> 'Category Description',
	'CATEGORY' => 'Category',
	'ITEMINFORMATIONLIST' => 'Item Information List',
	'MER_IMAGE' => 'Images',
	'PRICE' => 'Price (SAR)',
	'ADD_ITEM' => 'Add Item',
	'ITEMNAME'  => 'Item Name',
	'UPLOADIMAGE' => 'Upload Image',
	'DESCRIPTION' => 'Description',

'UPDATE_ITEM' => 'Update Item',
'UPDATE' => 'Update',
'SELECT' => 'Select',
'USERIMAGE' => 'Profile Picture',

'MER_VALIDATION_VALID_PDF_FILE' => 'Please upload valid pdf file', 
'UPLOADCERTIFICATE' => 'Upload Certificate', 
'CAPACITY' => 'Capacity', 
'Insuranceamount' => 'Insurance Amount (SAR)', 
'People'=>'No. of People',

'MER_VALIDATION_FIRSTNAME' => 'Please enter first name', 
'MER_VALIDATION_LASTNAME' => 'Please enter last name', 
'MER_VALIDATION_EMAILADDRESS' => 'Please enter email address', 
'MER_VALIDATION_COUNTRY' => 'Please select country', 
'MER_VALIDATION_CITY' => 'Please select city', 
'MER_VALIDATION_PHONE' => 'Please enter phone number', 
'MER_COUNTRY_CODE_MSG' => 'Please enter country code', 
'MER_VALIDATION_ADDRESS' => 'Please enter address', 
'MER_VALIDATION_ZIP' => 'Please enter zip code', 
'MER_VALIDATION_IMAGE' => 'Please choose valid image', 
'EMAILADDRRESS' => 'Email Address',
'PASSWORD' => 'Password',
'MER_UPDATE_CATEGORY'=> 'Update Category',

'MER_VALIDATION_HALL_CAPACITY'=> 'Please enter capacity',
'MER_About_Singer' =>'About',
'MER_VALIDATION_SINGER_NAME' =>'Please enter singer name',
'MER_VALIDATION_SINGER_NAME' =>'Please enter singer name',
'DOWNLOAD' =>'Download',

'MER_VALIDATION_ABOUT_SINGER' =>'Please enter description',
'MER_VALIDATION_SINGER_NAME_ARABIC' =>'ححريحريحريحري رير',
'MER_VALIDATION_ABOUT_SINGER_ARABIC' =>'ححريحريحريحري رير',
'MER_VALIDATION_FIRST_NAME_AR'=> 'حرير حرير حرير حرير حرير',
'MER_VALIDATION_LAST_NAME_AR'=> 'حرير حرير حرير حرير حرير',

'MER_VALIDATION_ABOUT_SINGER_AR' =>'يرجى إدخال الوصف',

'CertificateChamber' =>'Chamber Certificate ',
'MER_VALIDATION_TNC' => 'Please upload pdf file',
'TERMSANDCONDITIONS' => 'Terms and Conditions',
'TERMSANDCONDITION' => 'Terms and Conditions',
'MER_SHOP_ADDRES' => 'Address',
'COUPON' => 'Coupon Code',

'MER_VALIDATION_VALID_FILE'=> 'Please upload pdf file',

'MER_VALIDATION_COUPON' => 'Please enter coupon code',
//Ppular band validation Himanshu

'MER_VALIDATION_POPULAR_BAND_NAME' =>'Please enter band name',

//End Popular band information
'UPLOAD' => 			'Upload',

'Payment' 	 => 			'Payment',
'AccountDetail' 	 => 			'Account Detail',
'OrderReport' 	 => 			'Order Report',
'ReceivedPayements' 	 =>	'Received Payments',
'MER_VALIDATION_COMMENT'=> 'Please enter comment',
'MER_UPDATE_BEAUTYSHOP'=> 'Update Shop',
'MER_GOOGLE_MAP_URL'=> 'Google Map Address URL',
'MER_ADDRESS_IMAGE'=> 'Address Image',
'MER_SELECT_ACCEPT'=> 'Select',
'YES'=> 'Quote Accepted by Customer',
'NO'=> 'Quote Given by Vendor',
'MER_VALIDATION_SINGER_IMAGE'=> 'Please upload (PNG, GIF, JPEG, JPG) image',
'UPDATEPACKAGE' => 'Update Package',
'MER_UPDATE_OFFER' => 'Update Offer',

'PLEASE_ENTER_SERVICE_NAME'=> 'Please enter service name',
'PLEASE_SELECT_CATEGORY'=> 'Please select category',
'PLEASE_ENTER_DURATION'=> 'Please enter duration',
'PLEASE_ENTER_AMOUNT'=> 'Please enter amount',
'PLEASE_ENTER_DISCOUNT'=> 'Please enter discount',
'PLEASE_ENTER_DISCRIPTION'=> 'Please enter discription',


'PLEASE_ENTER_PACKAGE_NAME'=> 'Please enter package name',
'PLEASE_ENTER_PRICE'=> 'Please enter price',
'PLEASE_ENTER_DURATION'=> 'Please enter duration',

'You_are_yet_to_get_reviews_or_ratings' => 'You are yet to get reviews or ratings',
'VALIDFILEPDF' => 'Please upload a PDF file',
'VALIDSHOPLINK' => 'Please enter shop link in maroof',
'MER_Dish_Image' => 'Dish Image',
'Shop_link_in_maroof' => 'Shop link in maroof',
'UPDATEWORKER' => 'Update Worker',
'VIEW' => 'View',

'PLEASE_ENTER_BRANCH_NAME' => 'Please enter branch name',
'PLEASE_ENTER_ADDRESS' => 'Please enter address',
'PLEASE_CHOOSE_CITY' => 'Please select city',
'PLEASE_CHOOSE_MANAGER' => 'Please select manager',
'PLEASE_CHOOSE_OPENING_TIME' => 'Please select opening time',
'PLEASE_CHOOSE_CLOSING_TIME' => 'Please select closing time',
'PLEASE_ENTER_DESCRIPTION' => 'Please enter description',
'PLEASE_UPLOAD_TERMSANDCONDITON' => 'Please upload terms and conditions',
'PLEASE_SELECT_SERVICE' => 'Please select services',
'MER_UPDATE_SERVICES' => 'Update Service',
'PLEASE_ENTER_SHOP_NAME' => 'Please enter shop name',
'MER_UPDATE_SHOP' => 'Update Shop',
'MER_DESART_VIDEO' => 'Pictures and Videos',


	/* Arun end */

 'MER_RETURN_APPROVE_DISAPPROVED_SUBJECT'				=> 	'Order return Approve/Disapprove Notification',

	'MER_ORDER_CANCELLATION_REQUEST'					=> 'Order Cancellation request status',
	'MER_YOU_ARE_SEND_A_CANCEL_REQUEST_FOR_THE_FOLLOWING_PRODUCT'	=> 'You are send a order cancel request for the following product. ',
	'ADMIN_DELIVERY_STATUS_PRODUCT_PACKED'	=> 'Your Product was packed and will be shipped soon',
	
	'MER_DELIVERY_STATUS_PRODUCT_PACKED'	=> 'Your Product was packed and will be shipped soon',
	'ORDER_PACKED'	=> 'Order Packed',
	'MER_DELIVERY_STATUS_PRODUCT_DISPATCHED'	=> 'Your Product was Dispatched and will be Delivered Soon',
	'ORDER_DISPATCHED'	=> 'Order Dispatched',
	'ADMIN_DELIVERY_STATUS_PRODUCT_DELIVERED'=>'Your Product was Delivered',
	'MER_DELIVERY_STATUS_PRODUCT_DELIVERED'	=> 'Your Product was Delivered',
	'ORDER_DELIVERED'	=> 'Order Delivered',
	'MER_DELIVERY_STATUS_PRODUCT_CANCELLED'	=> 'Your Product was Cancelled By Admin',
	'ORDER_CANCELLED'	=> 'Order Cancelled',
	'MER_DELIVERY_STATUS_PRODUCT_RETURNED'	=> 'Your Product was Returned',
	'ORDER_RETURNED'	=> 'Order Returned',
	'MER_DELIVERY_STATUS_PRODUCT_REPLACED'	=> 'Your Product was Replaced',
	'ORDER_REPLACED'	=> 'Order Replaced',
	'DATE'	=> 'Date',
	    'MER_PAYMENT_TYPE'				=>			'Payment type',
		'MER_DATE'						=>			'Date',
		'MER_STATUS'					=>			'Status',
		'MER_COMMSSION_TO_PAID'			=>			'Commssion to paid',
		'MER_PENDING'					=>			 'Pending',
		'MER_ACTIVE'					=>			 'Active',
		'MER_MERCHANT_DASHBOARD'		=>			'Merchant dashboard',
		'MER_MONTH_WISE_TRANSACTIONS'	=>		'Month wise transactions',
		'SERVICES_NAME'					=>		'Service name',
		'PAYMENT_STATUS'					=>		'Payment_status',

	//Cancel,return and replace ends
	'MER_SRY_SND_RQT' => 'Sorry, You unable to send request,Because your previous request yet in pending .Please contact admin for further details.',
/*end back end static pages*/	


		'MER_COMMISSION'					=>		'Commission',
		'MER_SUBSCRIPTION'					=>		'Sunscription',
		'MER_SELECT_MEMBERSHIP_TYPE'		=>      'Membership Type',
		'MER_SERVICE_NAME'					=>      'Service',
		'MER_MEMBERSHIP_TYPE'				=>      'Membership Type',
		'MER_SUBSCRIPTION'					=>      'Subscription',
		'MER_FILTERS'						=>      'Filter',
		'MER_SERVICES'						=>      'Services',
		'MER_ADDMORE'						=>      'Add Services',
		'MER_APPLY'							=>		'Apply',
		'MER_BRANCH'						=>		'Branch',
		'MER_MANAGER'						=>		'Manager',
		'MER_INACTIVE'						=>		'Inactive',
		'MER_VIEW'							=>		'Manage',
		'MER_PRODUCTNAME'					=>		'View',
		'MER_COMMENT'						=>		'Comment',
		'MER_PRODUCTNAME'					=>		'Product Name',
		'MER_RATING'						=>		'Rating',

		'MER_HOTEL_NAME'					=>		'Hotel Name',
		'MER_BRANCH_INFO'                   =>		'Branch Info',
		'MER_HOTEL_IMAGE'					=> 		'Hotel Logo',
		'MER_CREATED_DATE'					=> 		'Last Modified',
		'MER_EDIT_HOTEL'					=> 		'Edit Hotel',
		'MER_LIST_BRANCH'					=> 		'List Branch ',
		'MER_HOTEL_LIST'					=> 		'Hotel List',
		'MER_ADD_HOTEL'						=> 		'Add Hotel',
		'MER_RATING'						=> 		'Rating',
		'MER_LIST_OF_HALL'					=>		'Hall List',
		'MER_LIST_OF_BRANCH'				=>	    'List of Branch',

		'MER_BRANCH'						=> 		'Branch',
		'MER_CITY'							=> 		'City',
		'MER_MANAGER'						=> 		'Manager',
		'MER_IMAGE'							=> 		'Image',
		'MER_HALL_LIST'						=>		'Hall List',
		'MER_EDIT'							=>		'Edit',
		'MER_ADD_BRANCH'							=>'Add Branch',
		'MER_SELEST_CITY'				   => 		'Select City',
		'MER_SELEST_MANAGER'			   => 		'Select  Branch Manager',
		'MER_SELEST_CONTAINER'			   => 		'Select  Container',
		'MER_BRANCH_NAME'				   => 		'Branch Name',
		'MER_DESCRIPTION'				   => 		'Description',
		'MER_BRANCH_IMAGE'				   => 		'Branch Image',
		'MER_BRANCH_EDIT'				   => 		'Edit Branch',
		'MER_UPLOAD'				 	  => 		'Upload',
		'MER_TYPE'    => 		'Type',
		'MER_PER_KG'=> 'Per Kg',
 	    'MER_PER_PIECE'=>'Per Piece',
 	    



		
  //Himanshu Menu Category

	'MER_ADD_MENU_CATEGORY'=> 'Add Menu Category',
	'MER_ADD_MENU_UPDATE_CATEGORY'=> 'Update Menu Category',
	'MER_MENU_CATEGORY'=> 'Menu Category',
	'MER_SELECT_STATUS'		=>      'All Status',
	'MER_SELECT_CITY'		=>      'Select City',
	'MER_ACTIVE'		=> 'Active',
	'MER_INACTIVE'   => 'Inactive',
	'MER_CATEGORY_NAME'   => 'Category Name',
	'MER_LAST_MODIFIED_DATE'   => 'Last Modified',
	'MER_STATUS'   => 'Status',
	'MER_ACTION'   => 'Action',
	'MER_EDIT'   => 'Edit',
	'MER_VALIDATION_CATEGORY_NAME'   => 'Please enter category name ',
	'MER_VALIDATION_CAT_ARABIC_AR'=> 'Please enter category name arabic',
    
    //Dish menu validation
   
   'MER_VALIDATION_SELECT_CATEGORY'   => 'Please select category',
   'MER_VALIDATION_DISH_NAME'   =>        'Please enter dish name',
   'MER_VALIDATION_DISH_NAME_AR'   =>      'Please enter dish name arabic',
   'MER_VALIDATION_PRICE'   =>              'Please enter price',

   'MER_VALIDATION_ABOUT'   =>              'Please enter description',
   'MER_VALIDATION_ABOUT_AR'   =>           'Please enter description arabic',
    'MER_VALIDATION_CONTAINER_PRICE'   =>    'Please enter container price', 
   
	//Dish
	'MER_ADD_DISH' 		                => 			'Add Dish',
	'MER_UPDATE_DISH' 		                => 		'Update Dish',
	'MER_ADD_NEW' 		                => 			'Add New',
	'MER_CATEGORY' 		                => 			'Category',
	'MER_DISH_NAME' 		            => 	        'Dish Name',
	'MER_PRICE' 		                => 	        'Price (SAR)',
	'MER_IMAGE' 		                => 	        'Image',
	'MER_RECORD' 		                => 	        'No record found',

    //Container
    'MER_ADD_CONTAINER' 		            => 'Add Container',
    'MER_EDIT_CONTAINER' 		            => 'Update Container',
    'MER_CONTAINER' 		                => 'Container',
    'MER_CONTAINER_NAME' 		                => 'Container Name',
    'MER_CONTAINER_PRICE' 		                => 'Price (SAR)',

   // CONTAINER VALIDATION
   'MER_VALIDATION_CONTAINER_NAME' 		      =>  'Please enter container name',
   'MER_VALIDATION_CONTAINER_SELECT_NAME'   =>   'Please select container name',
   'MER_VALIDATION_CONTAINER_NAME_AR' 		   => 'Please enter container name arabic',  

   //START OFFER
  'MER_OFFER_NAME' 		            => 'Offer Name',
  'MER_OFFER_DISCOUNT' 		            => 'Discount',
  'MER_START_DATE' 		            => 'Start Date',
  'MER_END_DATE' 		            => ' End Date',
  'MER_LIST_OFFER' 		            => ' Offers List',
  'MER_CREATE_OFFER' 		            => 'Add Offer',
  'MER_ADD_OFFER' 		            => 'Add Offer',
  'MER_OFFER_TITLE' 		            => 'Offer Title',
  'MER_DISCOUNT' 		            => 'Discount (%)',
 
  'MER_VALIDATION_OFFER_TITLE'      =>'Please enter offer title',
  'MER_VALIDATION_OFFER_TITLE_AR'      =>'Please enter offer title arabic',
  'MER_VALIDATION_DISCOUNT'      =>'Please enter discount',
  'MER_VALIDATION_START_DATE'      =>'Please select start date',
  'MER_VALIDATION_END_DATE'      =>'Please select end date', 
  'MER_OFFER_UPDATE' 		            => 'Update offer',
  'MER_LIST_DISH' 		            => 'List of Dishes', 

  

/* aSHWINI Start */	

/*  Start Prabhakar */		
'MER_LISTBRANCH' => 'View Branch',
'MER_Container_Name' => 'Container Name',
'MER_No_People' => 'No. of people',
'MER_No_VALIDATION_PEOPLE' => 'Please enter number of people',
'MER_Price' => 'Price (SAR)',
'MER_Type' => 'Type',
'MER_About' => 'About',
'MER_Container_Image' => 'Image',
'MER_Action' => 'Action',
'MER_Short_Code' => 'Short Code',
'MER_Dish_Name' => 'Dish Name',
'MER_Select_Menu' => 'Select Menu',
'MER_Select_Menu_Dish' => 'Select Dish',
'MER_Email_Address' => 'Email Address',
'MER_User_Image' => 'Image',
'MER_FT' => '(Sq.Meter)',
'MER_SQFT' => '(Sq.Meter)',
'MER_De_Activate_Record' => 'Are you sure you want to deactivate this record?',		
'MER_Activate_Record' => 'Are you sure you want to activate this record?',	
'MER_Want_Delete' => 'Do you want to delete this record?',
'Singer_Info_Menu' => 'Singer Info',
'Singer_Photo_Video_Menu' => 'Pictures and Video',
'Review_Comments_Menu' => 'Reviews and Ratings',
'Quoted_Requested_List_Menu' => 'Quote Requested',
'Acousticsinfo' => 'Sound Systems Info',

'MER_Singer_Name' => 'Singer Name',
'MER_Singer_Image' => 'Singer Image',		
'MER_Upload_Picture' => 'Upload Picture',
'MER_Customer_Name' => 'Customer Name',
'MER_Request_Date' => 'Request Date',	
		
'MER_Hall' => 'Hall',	
'MER_Occasion_Type' => 'Occasion Type',
'MER_Location' => 'Location',
'MER_Duration' => 'Duration (Hrs)',
'MER_Comments' => 'Comments',
'MER_Time' => 'Time',
'MER_Booking_Date' => 'Booking Date',
'MER_Returning_Date' => 'Returning Date',
'MER_Total_Price' => 'Total Price',
'MER_Paid_Services_Price' => 'Paid Services Price',
'MER_Total_Price' => 'Total Price',
'MER_Advance_Amount' => 'Advance Amount',
'MER_Due_Patment' => 'Due Patment',
'MER_Hall_Price' => 'Hall Price',

'MER_Paid_Services' => 'Paid Services',
'MER_Dish_Image' => 'Dish Image',
'MER_Container_Image' => 'Container Image',
'MER_Container_Qty' => 'Container Qty',
'MER_Pictures_Video'=>'Pictures and Video',
'MER_Type_Dishes'=>'Types & Dishes',
'MER_Add_Type_Dish'=>'Add Type & Dish',

'MER_Types'=>'Types',
'MER_Add_Type'=>'Add Type', 
/* End Prabhakar */

'PACKEGENAME'=>'Package Name',
'PACKEGINFORMATIONLIST' => 'Packages List',
'ADD_PACKAGE' => 'Add Package',
'NO_STAFF'=>'Number of Staff',


		
		

			// validatio start

			'MER_VALIDATION_HOTEL_NAME' => 'Please enter hotel name',
			'MER_VALIDATION_DESCRIPTION' => 'Please enter description',
			'MER_VALIDATION_IMAGE' 		  => 'Please upload (PNG, GIF, JPEG, JPG) image ',
			'MER_VALIDATION_HOTEL_NAME_AR' => 'Please enter hotel name in arabic',
			'MER_VALIDATION_DESCRIPTION_AR' => 'Please enter description in arabic',
			'MER_VALIDATION_VALID_IMAGE' => 'Please upload valid (JPG, JPEG, PNG) image',


			'MER_VALIDATION_CITY' => 'Please select city',
			'MER_VALIDATION_MANAGER' => 'Please select manager',
			'MER_VALIDATION_BRANCH' => 'Please enter branch name',
			'MER_VALIDATION_BRANCH_AR' => 'Please enter branch name arabic',
           
			

			'MER_VALIDATION_FIRST_NAME' => 'Please enter first name',
			'MER_VALIDATION_LAST_NAME' => 'Please enter last name',
			'MER_VALIDATION_EMAIL' => 'Please enter email address',
			'MER_VALIDATION_VALID' => 'Please enter valid email',			
			'MER_VALIDATION_MOBILE' => 'Please enter mobile number',
			'MER_VALIDATION_CITY_TEXT' => 'Please enter city',
			'MER_VALIDATION_PASSWORD' => 'Please enter password',

			'PLEASE_ENTER_URL' => 'Please enter video url',

			'Add_subscriptionAmount' => 'Add Subscription Amount',
			'MER_VALIDATION_HALL_NAME' => 'Please enter hall name',
			'MER_VALIDATION_HALL_NAME_AR' => 'Please enter hall name arabic',
			'MER_VALIDATION_MAP_ADDRESS' => 'Please enter map address',
			'MER_VALIDATION_HALL_LENTH' => 'Please enter hall lenth',
			'MER_VALIDATION_HALL_WIDTH' => 'Please enter hall width',
			'MER_VALIDATION_HALL_AREA' => 'Please enter hall area',
			'MER_VALIDATION_HALL_FOOD_MENMU' => 'Please check hall food menu',
			'MER_VALIDATION_HALL_PRICE' => 'Please enter hall price',
			'MER_VALIDATION_HALL_TYPE' => 'Please enter hall type',
			'MER_VALIDATION_FREE_SERVICES_NOTES' => 'Please enter service notes',
			'MER_VALIDATION_FREE_SERVICES_NAME' => 'Please enter service name',
			'MER_VALIDATION_FREE_SERVICES_NOTES_AR' => 'Please enter service notes Arabic',
			'MER_VALIDATION_FREE_SERVICES_NAME_AR' => 'Please enter service name Arabic',
			
			'MER_VALIDATION_PAID_SERVICES_NOTES' => 'Please enter service notes',
			'MER_VALIDATION_PAID_SERVICES_NAME' => 'Please enter service name',
			'MER_VALIDATION_PAID_SERVICES_NOTES_AR' => 'Please enter service notes Arabic',
			'MER_VALIDATION_PAID_SERVICES_NAME_AR' => 'Please enter service name Arabic',
			'MER_VALIDATION_PAID_PRICE' => 'Please enter price',
			'MER_VALIDATION_ITEM_NAME' => 'Please enter item name ',
			'MER_VALIDATION_ATTRIBUTE' => 'Please select category',
			'MER_VALIDATION_ITEM_NAME_AR' => 'Please item name arabic ',

			'MER_VALIDATION_PACKEGE_ITEMS' => 'Please select package items',
			'MER_VALIDATION_PACKEG_NAME' => 'Please enter package Name ',
			'MER_VALIDATION_NO_STAFF' => 'Please enter number of staff',
			'MER_VALIDATION_PACKEG_NAME_AR' => 'Please enter package Name Arabic ',



			 

 //Reception And Hospatility Himanshu
      'MER_SHOP_INFO' =>'Shop Info',
      'MER_SHOP_IMAGE' =>'Shop Image',
      
      'MER_SHOP_ATTRIBUTES' =>'Add Attribute',
      'MER_SHOP_ATTRIBUTES_NAME' =>'Attribute Name',
      'MER_SHOP_SELECT_PARENT_NAME' =>'Select parent attribute',
      'MER_IMAGE' =>'Image',
      'MER_SHOP_NAME' =>'Shop Name',        
      'SHOPINFO' => 'Shop Info',
      'SHOPPICTUREANDVIDEO' => 'Shop Picture and Video',
      'SHOPATTRIBUTES'=> 'Attribute',
      'ITEAMINFORMATION'=> 'Item Information',
      'WORKERSNATIONLITY' => 'Workers Nationality',
      'PACKAGES'=> 'Packages',
	  'REVIEWANDCOMMENTS' => 'Reviews and Rating',
	  'ORDERS' => 'Orders',
	  'SHOPLIST' => 'List Shop',
      'MER_ADD_SHOP' => 'Add Shop',
      'MER_NAME' => 'Name',
      'MER_SHOP_IMAGE' =>'Shop Image',
      'MER_GOOGLE_MAP_ADDRESS_URL' =>'Google Map Address URL',
      'MER_VALIDATION_VIEW' =>'View',

      //Validation Message
      'MER_VALIDATION_SHOP_NAME' =>'Please enter shop name',
      'MER_VALIDATION_SHOP_NAME_AR' =>'Please enter shop name ',
      'MER_VALIDATION_SHOP_URL' =>'Please enter google map address url',
      'MER_VALIDATION_SHOP_URL_AR' =>'Please enter google map address URL arabic ',
      'MER_CONTAINER' =>'Container Price (SAR)',
	  'MER_CONTAINER_PRICE' =>'Container Price (SAR)',
      'MER_RECEPTION_PICTURE'=>'Shop Pictures and Video',
      'MER_UPLOAD_SHOP_PICTURTE'=>'Upload Shop Picture',
      
      //Category validation

      'MER_CATEGORY_DESCRIPTION_VALIDATION' => 'Please enter category description',
      'MER_CATEGORY_DESCRIPTION_VALIDATION_AR' => 'Please enter category description',
      'MER_SELECT_NATIONALITY' => 'Select Nationality',


      // Buffet Ajit
      "MER_WORKERS_NATIONALTY"					=> 		"Workers' Nationalty",
      'MER_BUFFET_DASHBOARD'					=> 		'Buffet',
      'MER_BUFFET_INFO'					        => 		'Branch Info',
      'MER_GIVEN_BY'							=> 		'Name',
      'MER_RREVIEW_DATE'						=> 		'Review Date',
	  
	  //Buffet nidhi 
	  
	   'MER_BUFFET_LIST'	  => 	'Branch List',
	   'MER_BUFFET_VIDEO'  => 'Buffet Pictures and Video',
	   'MER_BRANCHLIST'  => 'Branch List',
	   'MER_RESTURANT'=> 'Restaurant',
	   'MER_ADD_RESTURANT'=> 'Add Restaurant',

	   'MER_RESTURANT_NAME'=> 'Restaurant',
	   'MER_SERVICE_CATEGORY'=> 'Service  Categories',
	   'LANG'=> 'Select Language',
	  	'OPENING'=>  'Opening Time',
		'ClOSING'=>  ' Closing Time',
	  	'HOMEVISIT'=>  'Home Visit Charge (SAR)',
		'SERVICEAVAIL'=>  'Service Available at',
		'HOME'=>  'Home', 
		'MER_SELECT_MANAGER' 						=> 			'Select Manager',
		'WORKERS' 						=> 			'Workers',
		'mer_youtube_add'=> 			'Click the share icon below the YouTube video and copy/paste the URL that appears in the pop-up window ',
		'MER_CREATED_IMAGE'=> 			'Category Image',
		'AVAILABLE'=> 			'Available at ',
		'PACKAGE'=> 			'Package ',
		'ADDPACKAGE'=> 			'Add Package ',
		'PACKAGEIMAGE'=> 			'Package Image ',
		'SERVICEIMAGE'=> 			'Services Images ',
		'HRS'=> 			'Hrs',
		'ADDWORKER'=> 			'Add Worker',
		'SELECTWORKER'=> 			'Select Worker',
		'BOOKING'=> 			'Booking',
		'REVIEW'=> 			'Reviews',
		'YEAROFEXP'=> 			'Years of Experience',
		'PAYMENTREQUEST'=> 			'Payment Request',
			'TYPEOFDISH'=> 			'Type of Dish',
			'MER_ADD_SERVICE' 		                => 			'Add Service',
			'EXPERT' 		                => 			'Expert in Services',
			'SELECT_CATEGORY'  => 			'All Categories',
			'MER_ORDER_STATUS'=> 			'Order Status',
			'work'=> 			'Worker',
			'Years'=> 			'Years Of Experience',
			'SELECTSERVICE' => 			'Select Services',
			'SERVICEIMAGES' => 			'Service Images',
			'NUMBERFLOWER' => 			'No. of Flower',
			'WRAPDESIGN' => 			'Wrapping Design',
			'WRAPTYPE' => 			'Wrapping Type',
			'FlOWERTYPE' => 			'Flower Type',
			'QTYSTOCK' => 			'Qty In Stock',
			'ROSESTYPE' => 			'Roses Type',
			'ADDROSESTYPE' => 			'Add Rose Type',
		
//Buffet Validation

	'MER_VALIDATION_RESTURNANT_NAME' => 'Please enter Restaurant Name',		
		
//Himanshu Makeup and Artist
	 'MER_VALIDATION_SERVICE_TYPE' => 'Please select where your service is available',
	 'MER_VALIDATION_TITLE' => 'Please enter title',


	 'MER_VALIDATION_QUANTITY' => 'Please enter Quantity',

	 'MER_VALIDATION_NAME' => 'Please enter name',

	 'MER_VALIDATION_EXPERTISE' => 'Please choose expertise',
	 'MER_VALIDATION_EXPERTISES_AR' => 'يرجى اختيار الخبرة',

	 'MER_UPDATE_RESTURANT'=> 'Update Restaurant',

	
	
	// Harry Start From here Popular Band
	 
    'MER_POPULAR_BAND_INFO'	  => 	'Popular Band Info',
    'MER_BAND_NAME'	  => 	'Band Name',
    'MER_BAND_IMAGE'	  => 	'Band Image',
	'MER_TERMSCONDITION'	  => 	'Terms & Conditions',
	'MER_About_Band'	  => 	'About Band',
	'Popupr_Band_Photo_video'	  => 	'Pictures and Video',
	'popular_band_title'	  => 	'Pictures and Video',
	'popular_band_comments_review'	  => 	'Reviews and Ratings',

	'MER_CONFIRM'	  => 	'Confirm',


	//Harry Beauty Module 
	'mer_shoplist'	  => 	'Shop List',   
	'MER_ADD_SHOP'	  => 	'Add Shop',
	'MER_SHOP_NAME'	  => 	'Shop Name',   
	'MER_ADD_BEAUTYSHOP'	  => 'Add Shop',   
	'mer_service_catogory'	  => 'Service Categories',   	
	'mer_category_name'	  => ' Name',   	
	'mer_add_category'	  => 'Add Category',
	'mer_services'	  => 'Services',
	'mer_service_name'	  => 'Service',
	'mer_google_add'	  => 'Copy/paste the web address that comes in the browser address bar in Google Maps',
	'mer_view_title'	  => 'View',
	'mer_address_img'	  => 'Upload a photo of your venue',
	'mer_office_address_img'	  => 'Upload a photo of your Office/Home',	
	'mer_img_replace'	  => 'Uploading image will replace the current image.',		
	'mer_workerview'	  => 'Worker Review',
	'mens_barbe_info'	  => "Men's Barber Info",		
	'mer_about_video'	  => "About Video",
	'mer_makeupartistinfo'	  => "Makeup Artist Info",
	'mer_category_list'	  => "Categories List",	
	'mer_category_item'	  => "Category Item",
	'mer_category_item'	  => "Category Item",
	'cosha_item'	  => "Kosha Item",
	'add_item'	  => "Add Item",	
	'add_item_category'	  => "Add Item Category",
	'packges_name'	  => "Package Name",		
	'numberof_cameras'	  => "Number of Cameras",
	'duration_video'	  => "Duration of Video",	
	'numberof_picture'	  => "Number of Pictures",
	'packagefor'	  => "Package For",
	'product_list'	  => "Product List",
	'special_event_info'	  => "Special Event Info",	
	'event_image'	  => "Event Image",	
	'item_information'	  => "Item Information",
	'reception_hospitality'	  => "Reception & Hospitality",	

	//Makeup artist validation himanshi

	'MER_VALIDATION_SERVICENAME' =>'Please enter service name',
	'MER_VALIDATION_DURATION' =>'Please enter duration',
	'MER_VALIDATION_EXP' =>'Please enter years of experience',
	
 /*New Language*/
'MER_CATEGORIES'	  => "Categories",
'Per_Pieces' => "Per Piece",
'Per_KG' => "Per Kg",
'Available_Qty' => "Available Qty",
'Per_Quantity' => "Per Qty",
'Discount' => "Discount", 
'Type'=> "Type",
'Dish'=> "Dish",
'MER_Update_Type'=> "Update Type",
'Please_upload_only_PDF_file' => "Please upload only PDF file",

'MER_ADD_NEW_CATEGORY'=> 'Add New Category',

//Home work
'MER_QUANTITY'=> 'Quantity',
'MER_PRODUCT_NAME'=> 'Product Name',
'MER_PRODUCT_IMAGE'=> 'Product Image',
'MER_PRODUCT_QUANTITY'=> 'Quantity',
'Please_upload_only_PDF_file'=> 'Please upload only PDF file',
'Please_upload_only_PDF_file_ar'=> 'يرجى تحميل ملف PDF فقط',

'MER_VALIDATION_PRODUCTNAME'=>'Please enter product name',
'MER_VALIDATION_PRODUCTNAMES_AR'=>'Please enter product name',
'MER_VALIDATION_PRODUCTQUANTITY_AR'=>'Please enter quantity',
'MER_VALIDATION_PRODUCTQUANTITY'=>'Please enter quantity',
'MER_VALIDATION_SHOPNAME'=>'Please enter shop name', 
'MER_con_title'=>'Container',    
'EDIT' => 'Edit',
'DRLIST' => 'Doctor List',
'mer_specialist' => 'Specialist',
'mer_consultation' => 'Consultation Fees',
'mer_add_dr_list' => 'Add Doctor List',
'Professional_Bio' => 'Professional Bio',
'Acoustics_Info_Menu'  => 'Sound Systems Info',
'MER_VALIDATION_NAME' =>'Please enter name',
'mer_clinic_list' =>'Clinic List',
'mer_add_clinic' =>'Add Clinic',
'JOIN_DATE' =>'Start Date',
'UPDATEROSESTYPE'=>'Update Roses Type ',
'MER_VALIDATION_Choose_Type'=>'Please choose type of dish',
'List_the_food_choices_available_with_your_hall'=>'List the food choices available with your hall',
'Do_you_allow_food_from_outside'=>'Do you allow food from outside?',


//Clinic Teeth

'MER_CLINIC_NAME' =>'Clinic Name',
'Clinic' =>'Clinic',
'MER_VALIDATION_CLINIC_NAME' =>'Please enter clinic name',
'List_the_food_choices_available_with_your_hall.?' =>'List the food choices available with your hall',
'Do_you_allow_food_from_outside?' =>'Do you allow food from outside?',
'PLEASE_SELECT_FLOWER_TYPE' => 'Please select flower type ',  
'PLEASE_SELECT_WRAP_TYPE' => 'Please select wrap type', 
'PLEASE_SELECT_WRAP_DESIGN' => 'Please select wrap design', 
'Please_enter_package_for'=> 'Please select package for', 
'Please_enter_no_of_camera'=> 'Please enter no of camera', 
'Please_enter_duration'=> 'Please enter duration', 
'Please_enter_no_of_picture'=> 'Please enter  no of picture',
'Consulation_fees'=> 'Consulation Fees',
'Please_Enter_Consulation_fees'=> 'Please enter consulation fees', 
'Please_Enter_Specilist'=> 'Please enter your specialization', 
'MER_VALIDATION_BIOGRAPHY' =>'Please enter professional bio',
'SELECT_YOUR_CHOICE'=>'For Tailor/Ready Made',
'Ready_Made_Cosha'=>'Ready Made Kosha',
'Designer_Cosha'=>'Designer Kosha',
'special_event'=>'Special Event',

'MER_ADD_DOCTOR' =>'Add Doctor',

'mer_update_clinic' =>'Update Clinic',
'CarRentalInfo' =>'Car Rental Info',
'Product' =>'Product',
'Model_Name'=>'Model Name',
'Model_year'=>'Model Year',
'Service_Date'=>'Service Date',
'Price_Per_KM' =>'Price Per Day',
'PLEASE_ENTER_MODEL_NAME'=>'Please enter model name',
'PLEASE_ENTER_MODEL_YEAR'=>'Please enter model year',
'PLEASE_ENTER_SERVICE_DATE'=>'Please enter service date',
'Car_type'=>'Car Type',
 'MER_ADD_Car_type' =>'Add Car Type',
 'MER_Update_Car_type' =>'Update Car Type',
 'TravelInfo'  =>'Travel Info',
 'MER_ADD_PACKAGE'=>'Add Package',
'Location'=>'Location',
'PackageSchedule'=>'Package Schedule',
'PackageDuration'=>'Package Duration',
'ExtraService'=>'Extra Service',
'PLEASE_ENTER_LOCATION'=>'Please enter location',
'PLEASE_ENTER_PACKAGE_SCHEDULE'=>'Please enter package schedule',
'PLEASE_ENTER_PACKAGE_DURATION'=>'Please enter duration',
'PLEASE_ENTER_EXTRA_SERVICES'=>'Please enter extra services',
'MER_UPDATE_PACKAGE'=>'Update Package',
'Caliber' =>'Caliber',
'Weight' =>'Weight (Gram)',
'abaya_item' =>'Items',
'Ready_Made_abaya' =>'Ready Made',
'Designer_abaya' =>'Tailor',
'SIZE'=>'Size',
'WorkerPrice' => 'Worker Price(SAR)',
'PLEASE_ENTER_WORKER_PRICE' => 'Please enter worker price',
'SN'=>'S.No.',
'WRAPPINGTYPE' =>'Wrapping Type',
'WRAPPINGDESIGN' =>'Wrapping Design',
'ADDWRAPPINGDESIGN' =>'Add Wrapping Design',
'ADDWRAPPINGTYPE' =>'Add Wrapping Type',
 'UPDATE_WRAPPING_TYPE' =>'Update Wrapping Type',
 'NAME'=>'Name',
 'MER_NAME_VALIDATION'=> 'Please enter name',
 'UPDATE_WRAPPING_DESIGN'=>'Update Wrapping Design',
 'MER_DESCRIPTION_TXT'=>'Character(s) to go',
 'dresses_item'=> 'Products',
 'MER_ADD_NEW_SIZE'=> 'Add Size',
 'MER_VALIDATION_SIZE_NAME' =>'Please enter size',
 'MER_UPDATE_SIZE'=> 'Update Size',
 'add_product'=> 'Add Product',
  'UPDATE_PRODUCT'=> 'Update Product',
 'Avilable_on_rent'=> 'Avilable for rent',
 'Rent_price'=> 'Rent/Hour (SAR)',
 'Yes'=> 'Yes',
 'No'=> 'No',
 'MER_IMG'=> 'Image',
 'MAKEUP'=> 'Makeup',
  'HOME'=> 'Home',
  'HOTELS' => 'Hotels',
 'reception_hospitality' => 'Reception Hospitality',
'singer'=> 'Singer',
'acoustics'=> 'Sound Systems',
'popular_band'=> 'Popular Band',
'buffet'=> 'Buffet',
'MANAGER'=> 'Manager',
'beauty'=> 'Beauty Centers',
'oudandperfumes' =>'Oud & Perfumes',
'goldandjewelry'=>'Gold & Jewelry',
'spa'=> 'Spa',
'makeup_artist'=> 'Makeup Artist',
'tailor'=> 'Tailor',
'photography'=> 'Photography Studio',
'abaya'=> 'Abaya',
'dresses'=> 'Dresses',
'cosha'=> 'Kosha',
'Special_Events'=> 'Special Events',
'car-rental'=> 'Car Rental',
'Travel_Agency'=> 'Travel Agency',
'Roses'=> 'Roses',
'Men_Barber_Saloon'=> 'Men Barber Salon',
'Dessert'=> 'Dessert',
'Dates'=> 'Dates',
'Cosmetic_and_Laser'=> 'Cosmetic and Laser',
'SKIN_AND_TEETH'=> 'Skin and Teeth',
'MAKEUP'=> 'Makeup',
'PAYNOW'=> 'Pay Now',
'PAYMENT_SUCCESS'=> 'Success',
'PAYMENT_ERROR'=> 'Error',

'BODY_MEASUREMENT'=>'Body Measurement',
'LENGTH'=>'Length',
'CHEST_SIZE'=>'Chest Size',
'ABAYA_PRODUCT'=>'Product',
'WAIST_SIZE' =>'Waist Size',
'SHULDERS' =>'Shoulder',
'NECK' =>'Neck',
'ARM' =>'Arm',
'WRIST_DIAMETER'=>'Wrist Diameter',
'CUTTING'=>'Cutting',

'MER_PLEASE_SELECT_TAILOR' => 'Please select for tailor/ready made',

'MER_FOR' => 'For',

'MER_PRODUCT_CATEGORY'=> 'Product Categories',

'MER_PRODUCTS' => 'Products',

'MER_ADD_PRODUCT' => 'Add Product',
'This_is_to_inform_that_we_have_received_your_payment'=> 'This is to inform that we have received your payment',
'We_look_forward_to_build_a_mutually-beneficial_long-term_association_with_you' =>'We look forward to build a mutually-beneficial long-term association with you',
'MyWallet'=> 'My Wallet',
'MER_MyWallet'=> 'Add Amount',
'SNO'=> 'S.No',
'Amount_Paid'=> 'Amount Paid(SAR)',
'Payment_date'=> 'Payment date',
'Mode'=> 'Mode',
'Total_Amount'=> 'Total Amount(SAR)',
'Pay_Now'=> 'Pay Now',
'Note'=> 'Note',
'Credit'=> 'Credit',
'Sucess'=> 'Success',
'Failed'=> 'Failed',
'Debit'=> 'Debit',
'Amount'=> 'Amount',
'DateFrom'=> 'Date From',
'DateTo'=> 'Date To',
'Apply'=> 'Apply',
'Service_provider'=> '24 X 7 Service Provider',

'Stitch_Designs'=>'Stitch Designs',

'ADD_Stitch_Designs'=>'Add Stitch Design',

'UPDATE_Stitch_Designs'=>'Update Stitch Design',
'ADD_Stitch_Design'=>'Add Stitch Design',

'ADD_Stitch_Design'=>'Stitch Design Name',

'ADD_Stitch_Design_Image'=>'Stitch Design Image',


//New language

'FUND_REQUEST'=>'Fund Request',
'ACCOUNT_DETAILS'=>'Account Details',
'RECIEVED_PAYMENT'=>'Recieved Payment',
 'ORDER_REPORTING'=>'Order Reporting',

 'Quantity_For_Sale'   =>    'Qty In Stock (For Sale)',

 'Quantity_For_Rent'  =>     'Qty In Stock (For Rent)',
'Order_id' => 'Order Id',
'Request_date' => 'Request date',
'transaction_id' => 'Transaction Id',
'Bank_Name'=> 'Bank Name',
'Account_number'=> 'A/C Number',
 'PLEASE_ENTER_BANK_NAME'=> 'Please enter bank name',
 'PLEASE_ENTER_ACCOUNT_NUMBER'=> 'Please enter a/c number',
 'MER_PLEASE_SELECT_SIZE'=> 'Please select size',

  'MER_SHOP_LOGO' =>'Logo',
  
  'MER_CALIBER' =>'Please enter caliber',

  'MER_WEIGHT' =>'Please enter weight (gram)',

  'PACKAGE_LIST' => 'Package list',

  'PACKAGE_IMAGE' =>'Package image',
'SELECT_ORDERS' =>'Select order',
'Last_10_Days' =>'Last 10 Days',
'Last_1_Month' =>'Last 1 Month',
'Last_6_Months' =>'Last 6 Months',
'Last_1_Years' =>'Last 1 Years',
 'Start_Date' =>'Start Date',
 'End_Date' =>'End Date',
'Discount_For_Sale'   =>    'Discount (For Sale)',
'Discount_For_Rent'  =>     'Discount (For Rent)',

'Deliver_date' => 'Deliver day',

'PLEASE_ENTER_DELIVER_DATE' =>'Please enter deliver day',
'sales_rep_code' =>'Referral Code',

 'FROM' =>'From',
 'TO' =>'To',
 'WORKING_HOURS' =>'Working hours',
 'Paid'=>'Paid',
 'Select_Category'=>'Select Category',
 'IN_PROCESS'		=> 'In Process',
	'COMPLETE'   => 'Completed',
	'REQUEST' => 'Request',
 	'RENTAL_DATE_TIME' => 'Rental Date/Time',
	'QUANTITY' => 'Quantity',
	'RETURN_DATE_TIME' => 'Return Date / Time',
	'PRODUCT_INFORMATION' => 'Product Information',

	'Price_Per_KM' => 'Price Per Day',
	'Car_Renta_Info' =>'Car Renta Info',
	'Car_Renta_Name' =>'Car rental name',

	'Please_enter_carrental' =>'Please enter car rental name',

	'Car_Models' => 'Car Models',

   'Add_Car_Models' => 'Add Car Models',

   'Update_Car_Models' => 'Update Car Models',

   'Travel_Agency' => 'Travel Agency Info',

   'Travel_Agency_name' => 'Travel agency name',

   'Travel_Agency_image' => 'Travel agency image',
   'Location' =>'Location',
   'Package_Duration' => 'Package Duration',
   'Extra_Service' => 'Extra Service',
   
	'Service' => 'Service',
	'BRANCH' => 'Branch',
	'STAFF' => 'Staff',
	'BOOKING_PLACE' => 'Booking Place',
	'BOOKING_DATE' => 'Booking Date',
	'STARTTIME_ENDTIME' => 'Start / End Time',
	'UNIT'=>'Unit',
	 'Dress_Type' => 'Dress Type',
	 'Size' => 'Size',
    'MENU' => 'Menu',
	 'DISH' => 'Dish',
	  'CONTAINER' => 'Container',
	  'QUANTITY'=>'Quantity',
	  
	  'BOOKING_DATE_TIME' => 'Booking Date / Time',
	  'RETURN_DATE_TIME' => 'Return Date / Time',
    'Language_Type' =>'Language Type',
 'OCCASION'=>'Occasion Detail',
 'OCCASIONDATE'=>'Occasion Date',
    'Write_On_jewellery' =>'Write on Jewellery',
	'OCCASIONBUDGET'=>'Occasion Budget',
	'total_member'=>'Number of Attendees',
	'OCCASIONTYPE'=>'Occasion',
	'Fabric'=>'Fabric',
 	'WRITEMESSAGE'=>'Write on Message',
	'name'=>'Name',
	'ADD_Fabric'=>'Add Fabric',
	'UPDATE_Fabric'=>'Update Fabric',
	'Image'=>'Image',
	'PLEASE_ENTER_NAME'=>'Please enter name',
	'Choose_Fabrics'=>'Choose Fabrics',
	'Package_Schedule'=>'Package Schedule',
	'SIZECHART'=>'Size Chart',
	'MANAGE_SHIPPING'=>'Manage Shipping',
	'CUSTOMER'=>'Customer',
	'BOOKINGDATE'=>'Booking Date',
	'BOOKINGDURATION'=>'Booking Slot',
	'mer_workerbooking'=>'Worker Booking',
	'BACK' 					=> 'Back',
	'BOOKINGSLOT' => 'Booking slot',
	 'NO_OF_PEOPLE'=>'No of people',
	 'VAT'=>'VAT',
	 'LAT' =>' Latitude',
	 'LONG' =>' Longitude',
	 'get_Lat_Long'=>'Get Latitude and Longitude',
	  'get_Lat_Long_issue'=>'Please enter valid google map url',
	   'Please_enter_valid_url' =>'Please enter valid url',
	    'MER_orderid' =>'Order Id',
		 'You_need_to_add_worker_to_activate_service' =>'You need to add worker to activate service',
		 'Hall_Type' =>'Hall Type',
		 'Replied' =>'Replied',
		 'Confirmed_by_customer'=>'Confirmed by customer',
		 'SHOPORDER'=>'Shop',
		 'MER_UserComments'=>'User Comments',
		 'MER_REJECT'=>'Deny',
	];
?>
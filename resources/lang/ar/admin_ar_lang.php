<?php
return 
[
/*Admin Header*/
'BACK' 					=> 'رجوع',
'MER_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY'				=> 			'تم إنشاء حساب التاجر بنجاح', 
'BACK_PAYABLE'				        => 'المبلغ المستحق',
'BACK_MY_PROFILE' 					=> 'ملفي الشخصي',
'BACK_LOGOUT' 						=> 'تسجيل الخروج',
'BACK_DASHBOARD' 					=> 'لوحة التحكم',
'BACK_DEALS' 						=> 'الصفقات',
'BACK_PRODUCTS' 					=> 'المنتجات',
'BACK_CUSTOMERS' 					=> 'العملاء',
'BACK_MERCHANTS' 					=> 'البائعين',
'BACK_CAT_LIST_ACTION' 				=> 'قائمة الخدمات',
'BACK_TRANSACTION' 					=> 'المعاملات',
'BACK_BLOGS' 						=> 'المدونات',
'BACK_SETTINGS'						=> 'الإعدادات',
'BACK_PAYMENT_STATUS'				=> 'حالة الدفع',
'BACK_COUPON_AMOUNT'				=> 'قيمة خصم الكوبون',
'BACK_WALLET'						=> 'المحفظة',
'BACK_ENTER_PASSWORD'				=> 'أدخل كلمة المرور (6 خانات كحد أدنى)',
'BACK_DELIVERED'					=> 'تم التسليم',
'BACK_DISPATCHED'					=> 'تم الشحن',
'BACK_ORDER_PACKED'					=> 'جاهز للشحن',
'BACK_ORDER_PLACED'					=> 'تم تقديم الطلب',
'BACK_DEAL_NAME'					=> 'اسم الصفقة',
'BACK_STATE'						=> 'المدينة',
'BACK_SELECT_VENDOR_IN'				=> 'إختر مزود خدمة في',
'BACK_AMOUNT_TO_PAY'				=> 'المبلغ المستحق',
'BACK_UPLOAD_LOGO_IMAGE'			=> 'رفع شعار',
'BACK_UPLOAD_FAVICON_IMAGE'			=> 'رفع أيقونة',
'BACK_UPLOAD_NO_IMAGE'				=> 'رفع صورة (لاتوجد صورة)',
'BANK_NAME'                          => 'اسم البنك',
'ACCOUNT_NO'                            => 'رقم الحساب',
'MAP_URL'                               => 'رابط الموقع على خرائط قوقل',
'ADDRESS_IMAGE'                         => 'صورة الموقع',
'PLEASE_ENTER_YOUR_BANK_NAME'           => 'يرجى ادخال اسم البنك.',
'PLEASE_ENTER_YOUR_ACCOUNT_NO'          => 'يرجى ادخال رقم الحساب البنكي.',

'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY'  	=> 		'Enter Your Payumoney Payment Key',
'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT'  	=> 		'Enter Your Payumoney Payment Salt',
'PAYMENT_EMAIL_KEY' 				=> 		'PayUMoney key',
'PAYMENT_EMAIL_SALT' 				=> 		'PayUMoney Salt',

/*Admin Dashboard*/
'BACK_ACTIVE_PRODUCTS' 					=> 'المنتجات النشطة',
'BACK_SOLD_PRODUCTS' 					=> 'المنتجات المباعة',
'BACK_ACTIVE_DEALS' 					=> 'الصفقات النشطة',
'BACK_EXPIRED_DEALS' 					=> 'الصفقات المنتهية',
'BACK_SOLD_DEALS'						=> 'الصفقات المباعة',
'BACK_STORES' 							=> 'المتاجر',
'BACK_CLIENT_ENQUIRY' 					=> 'استعلام العميل',
'BACK_GO_TO_LIVE' 						=> 'Go to Live',
'BACK_NEW_CUSTOMERS_MONTH_WISE' 		=> 'العملاء المسجلين (بالشهر)',
'BACK_TOTAL_CUSTOMER_AND_PRODUCT_COUNT_DEAL_COUNT' => 'مجموع العملاء ، المنتجات وعدد الصفقات',
'BACK_WEBSITE_USER' 					=> 'مستخدم الموقع',
'BACK_FACEBOOK_USER' 					=> 'مستخدم فيسبوك',
'BACK_ADMIN_USER' 						=> 'مستخدم اداري',
'BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT' 	=> 'تقرير المعاملات لآخر سنة',
'VAT_MANAGEMENT' 						=> 	'إدارة ضريبة القيمة المضافة',
/*------------- order management-----------*/
'VIEW_DETAIL'                           => 'عرض التفاصيل',
'VAT'                                   =>'ضريبة القيمة المضافة',
'TOTAL_PRICE' 				          => 'السعر الإجمالي',
'VAT_CHARGE'                            => 'ضريبة القيمة المضافة',
'QTY'								      => 'الكمية',
'SIZES'                                 => 'المقاس',
'Fabrics'                               => 'القماش',
'Pending'                               => 'قيد الإنتظار',
'Process'                               => 'قيد التنفيذ',
'Delivered'                             => 'مكتمل',
'Hold'                                  => 'معلق',
'Failed'                                => 'فشل',
'MY_PAYMENT'	                          => 'الدفع',
'SHIPPING_METHOD'                       => 'طريقة الشحن',
'ORDER_DETAILS'                         => 'تفاصيل الطلب',
'SERVICE_NAME'                          => 'الخدمة',
'HOME_VISIT_CHARGE'                     => 'رسوم الزيارة المنزلية',
'BOOKING_PLACE'                         => 'مكان تقديم الخدمة',
'BOOKING_DATE_AND_TIME'                 => 'تاريخ ووقت الحجز',
'START_TIME'                            => 'وقت البدء',
'BOOKING_TIME'                          => 'وقت الحجز',
'END_TIME'                              => 'وقت الانتهاء',
'STAFF_NAME'                            => 'العامل',
'BOOKING_TIME'                          => 'وقت الحجز',
'SHOP_NAME'                             => 'اسم المتجر',
'FOR'                                   => 'لـ',
'RENT_CHARGES'                          => 'رسوم الإيجار/باليوم',  
'ADDRESS'							      => 'العنوان',
'DRESS'                                 => 'الفساتين',
'BOOKING_DATE'     			          => 'تاريخ الحجز',
'RETURNING_DATE'     			          => 'تاريخ الإرجاع',
'PRODUCT_IMAGE'                         => 'الصورة',
'RENT_CHARGES_HR'                       => 'رسوم الإيجار/بالساعة',
'IMAGE'                                 => 'الصورة',
'TRANSACTION_ID'  				      => 'رقم المعاملة',
'Package'                               => 'الباقة',
'Photography'                           => 'التصوير الفوتغرافي',
'Videography'                           => 'الفيديو',
'Duration_of_Video'                     => 'مدة الفيديو (ساعة)',
'Number_of_Cameras'                     => 'عدد الكاميرات',
'Number_of_Pictures'                    => 'عدد الصور',
'DATE'                                  => 'التاريخ',
'LOCATION'                              => "الموقع",
'DURATION'                              => "المدة (ساعة)",
'TIME'                                  => 'الوقت',
'Date'                                  => 'التاريخ',
'Time'                                  => 'الوقت',
'PRODUCT_NAME'						  => 'اسم المنتج',
'TYPE'                                  => 'النوع',
'DISH'                                  => 'الصحون',
'PER_KILO'                              => 'للكيلو',
'PER_PIECE'                             => 'للقطعة',
'PER_QTY'                               => 'للكمية',
'Price'                                 => 'السعر',
'CREDIT_DEBIT'                          => 'بطاقة الخصم / الائتمان',
'AGENCY_NAME'                           => 'اسم المكتب',
'NO_OF_PEOPLE'                          => 'عدد الاشخاص',
'PACKAGE_DATE' 		                  => 'تاريخ الباقة',
'PACKAGE_DURATION' 		              => 'مدة الباقة',
'EXTRS_SERVICE' 		                  => 'الخدمات الإضافية',
'AUCTION'                               => 'المزاد',
'DAYS'                                  => 'أيام',
'HOURS'                                 => 'ساعات',
'MIN'                                   => 'دقيقة',
'SEC'                                   => 'ثانية',
'CAR_MODEL'                             => 'طراز السيارة',
'PRICE_PER_DAY'                         => 'السعر في اليوم',
'CLINIC_NAME'                           => 'اسم العيادة',
'DR_NAME'                               => 'اسم الطبيب',
'DEPARTMENT'                            => 'القسم',
'FILE_NUMBER'	                          => 'رقم الملف',
'STYLE_AND_FABRIC_INFO'                 => 'معلومات التصميم والقماش',
'STYLE'                                 => 'التصميم',
'STYLE_PRICE'                           => 'سعر التصميم',
'STYLE_IMAGE'                           => 'صورة التصميم',
'FABRICS_NAME'                          => 'اسم القماش',
'FABRICS_PRICE'                         => 'سعر القماش',
'FABRICS_IMAGE'                         => 'صورة القماش',
'PAID_SERVICE'                          => 'الخدمات المدفوعة',		      
'FLOWER_TYPE'                           => 'نوع الورد',
'WRAPPING_TYPE'                         => 'نوع التغليف',
'WRAPPING_DESIGN'                       => 'تصميم التغليف',
'DELIVERY_DATE' 				          => 'تاريخ التسليم',
'BODY_MEASUREMENT'                      => 'قياسات الجسم',
'LENGTH'                                => 'الطول',
'CHEST_SIZE'                            => 'قياس الصدر',
'WAIST_SIZE'                            => 'قياس الخصر',
'SHOULDERS'                             => 'قياس الاكتاف',
'NECK'                                  => 'قياس الرقبة',
'Width'                                 => 'العرض',
'ARM_LENGTH'                            => 'طول الذراع',
'WRIST_DIAMETER'                        => 'قياس فتحة الكم',
'CUTTING'                               => 'شكل القصة',
'NORMAL'                                => 'عادي',
'AMPLE'                                 => 'واسع',
'CURVED'                                => 'منحني',
'CM'                                    => 'سم',
'Area'                                  => 'المساحة',
'WRITE_ON_YOUR_RING'                    => 'اكتب على',
'GM'                                    => 'غم',
'WEIGHT'                                => 'الوزن',
'GRAM'                                  => 'غرام',
'DESIGN'                                => 'التصميم',
'CONFIRMED'                             => 'تم التأكيد',
'NAME'								  => 'الاسم',
'Name'                                  => 'الاسم',
'ADMIN'                                 => 'المدير',
'INVITATION_TYPE'                       => 'نوع الدعوات',
'OCCASION_NAME'                         => 'اسم المناسبة',
'INSURANCE_AMOUNT'                      => 'مبلغ التأمين',
'REMAINING_AMOUNT'                      => 'المبلغ المتبقي',
'Remove'                                => 'حذف',
'couponAmt'                             => 'مبلغ خصم الكوبون',
'coupon_code'                           => 'رمز الكوبون',
'COUPON'                                => 'خصم الكوبون',
'COUPON_CODE'                           => 'رمز الكوبون',
'Number_of_Staff'                       => 'عدد العاملين',
'Nationality'                           => 'الجنسية',
'Single'                                => 'مفرد',
'HOTAL_NAME'                            => 'اسم الفندق',
'HALL'                                  => "القاعة",
'OTHER_HALL'                            => "القاعات الأخرى",
'HALL_PRICE'                            => 'سعر القاعة',
'FOOD_ORDERED'                          => 'الطعام المطلوب',
'DISHT_TYPE'                            => 'نوع الطبق',
'DISHT_NAME'                            => 'اسم الطبق',
'CONTAINER'                             => 'الحافظة',
'DISHT_IMAGES'                          => 'صورة الطبق',
'CONTAINER_IMAGES'                      => 'صورة الحافظة',
'BOOKING_TIME'                          => 'تاريخ الحجز',
'ADVANCE_PAYMENT'                       => '25% دفعة مقدمة',
'PRICE'                                 => 'السعر ',
'QUANTITY'                              => 'الكمية',
'OUR_MENU'                              => 'قائمتنا',




'BACK_ORDER_MENU' 					=> 'إدارة الطلبات',
'ORDER_MANAGEMENT' 					=> 'إدارة الطلبات',
'OCCASIONS_NAME' 					=> 'المناسبات',
'ORDER_DATE' 					=> 'تاريخ الطلب',
'ORDER_ID' 					=> 'رقم الطلب',

'ORDERED_CUSTOMER_NAME'                      => 'الاسم',
'TYPE_OF_OCCASION'                      => 'نوع المناسبة',
'OCCASION_DATE'                         => 'تاريخ المناسبة',
'ORDER_ID'										=> 'رقم الطلب',
'Budget'	=> 'الميزانية',
'NO_OF_ATTENDANCE' 				     => 'عدد الحضور',
'LIST_OF_SERVICES' 				     => 'قائمة الخدمات',
'SERVICES' => 'الخدمات',
'SERVICES_PROVIDER'                     => 'مزود الخدمة / المتجر',
'ORDER_DATE' => 'تاريخ الطلب',
'STATUS' => 'الحالة',
'AMOUNT' => 'القيمة',
'VIEW_DETAILS'                          => 'عرض التفاصيل',
'TOTAL_PRICE' 				             => 'السعر الإجمالي',

/*-------Settings - Side Bar ----------*/
'BACK_GENERAL_SETTINGS' 								=> 'الإعدادات العامة',
'BACK_PRODUCT_BULK_UPLOAD' 								=> 'Product Add Bulk Upload',
'BACK_PRODUCT_BULK_UPLOAD_EDIT' 						=> 'Product Edit Bulk Upload',
'BACK_EMAIL_CONTACT_SETTINGS' 							=> 'اعدادات الاتصال والبريد',
'BACK_SOCIAL_MEDIA_PAGE_SETTINGS' 						=> 'اعدادات صفحات التواصل الإجتماعي',
'BACK_PAYMENT_SETTINGS' 								=> 'اعدادات الدفع',
'BACK_IMAGE_SETTINGS' 									=> 'اعدادات الصور',
'BACK_LOGO_SETTINGS' 									=> 'اعدادات الشعار',
'BACK_FAVICON_SETTINGS' 								=> 'اعدادات الأيقونة',
'BACK_NO_IMAGE_SETTINGS' 								=> 'اعدادت عدم رفع صورة',
'BACK_BANNER_IMAGE_SETTINGS' 							=> 'اعدادت صورة البنر',
'BACK_ADD_BANNER_IMAGE' 								=> 'إضافة صورة بنر',
'BACK_MANAGE_BANNER_IMAGES' 							=> 'إدارة صور البنر',
'BACK_CATEGORY_ADVERTISEMENT' 							=> 'إعلانات التصنيفات',
'BACK_ADD_ADVERTISEMENT' 								=> 'إضافة إعلان',
'BACK_MANAGE_ADVERTISEMENT' 							=> 'إدارة الإعلانات',
'BACK_CATEGORY_BANNER_SETTING' 							=> 'اعدادات بنر الأقسام',
'BACK_ADD_CATEGORY_BANNER' 								=> 'إضافة بنر قسم',
'BACK_MANAGE_CATEGORY_BANNER' 							=> 'إدارة بنرات الأقسام',
'BACK_ATTRIBUTES_MANAGEMENT' 							=> 'إدارة الخواص',
'BACK_ADD_COLOR' 										=> 'إضافة لون',
'BACK_MANAGE_COLORS' 									=> 'إدارة الالوان',
'BACK_ADD_SIZE' 										=> 'إضافة مقاس',
'BACK_MANAGE_SIZES' 									=> 'إدارة المقاسات',
'BACK_SPECIFICATION_MANAGEMENT' 						=> 'إدارة المواصفات',
'BACK_ADD_SPECIFICATION_GROUP' 							=> 'إضافة مجموعة مواصفات',
'BACK_MANAGE_SPECIFICATION_GROUP' 						=> 'إدارة مجموعة المواصفات',
'BACK_ADD_SPECIFICATION' 								=> 'إضافة مواصفات',
'BACK_MANAGE_SPECIFICATION' 							=> 'إدارة المواصفات',
'BACK_COUNTRIES_MANAGEMENT' 							=> 'إدارة الدول',
'BACK_ADD_COUNTRY' 										=> 'إضافة دولة',
'BACK_MANAGE_COUNTRY' 									=> 'إدارة الدول',
'BACK_CITIES_MANAGEMENT' 								=> 'إدارة المدن',
'BACK_ADD_CITY' 										=> 'إضافة مدينة',
'BACK_MANAGE_CITIES' 									=> 'إدارة المدن',
'BACK_CATEGORY_MANAGEMENT' 								=> 'إدارة الأقسام',
'BACK_ADD_CATEGORY' 									=> 'إضافة قسم',
'BACK_MANAGE_CATEGORIES' 								=> 'إدارة الأقسام',
'BACK_CMS_MANAGEMENT' 									=> 'إدارة صفحات المحتوى',
'BACK_ADD_PAGE' 										=> 'إضافة صفحة',
'BACK_MANAGE_CMS_PAGES' 								=> 'إدارة صفحات المحتوى الثابت',
'BACK_ABOUT_US' 										=> 'حول',
'BACK_TERMS_CONDITIONS' 								=> 'الشروط والأحكام',
'BACK_ADS_MANAGEMENT' 									=> 'إدارة الإعلانات',
'BACK_ADD_ADS' 											=> 'إضافة إعلان',
'BACK_MANAGE_ADS' 										=> 'إدارة الإعلانات',
'BACK_FAQ' 												=> 'الأسئلة الشائعة',
'BACK_ADD_FAQ' 											=> 'إضافة أسئلة شائعة',
'BACK_MANAGE_FAQ' 										=> 'إدارة الأسئلة الشائعة',
'BACK_NEWS_LETTER' 										=> 'النشرة البريدية',
'BACK_SEND_NEWSLETTER' 									=> 'إرسال النشرة البريدية',
'BACK_MANAGE_SUBSCRIBED_USERS' 							=> 'إدارة المستخدمين المشتركين',
'BACK_COUPON' 											=> 'الكوبون',
'BACK_MANAGE_COUPON' 									=> 'إدارة الكوبونات',
'BACK_ADD_COUPON' 										=> 'إضافة كوبون',
'BACK_MOBILE_USE' 										=> 'استخدام للجوال',

/*-------Settings - End - Side Bar ----------*/

/*-------Settings - General Settings ----------*/
'BACK_SITE_NAME' 										=> 'اسم الموقع',
'BACK_META_TITLE' 										=> 'العنوان الوصفي',
'BACK_META_KEYWORDS' 									=> 'الكلمات الوصفية',
'BACK_META_DESCRIPTION' 								=> 'وصف البيانات الوصفية',
'BACK_ENABLE_DISABLE_COD' 								=> 'تشغيل / تعطيل الدفع عند الإستلام',
'BACK_COD' 												=> 'الدفع عند الإستلام',
'BACK_ENABLE_DISABLE_PAYPAL' 							=> 'تفعيل / إلغاء تفعيل PAYPAL',
'BACK_ENABLE_DISABLE_PAYUMONEY' 						=> 'تفعيل / إلغاء تفعيل PAYUMONEY',
'BACK_UPDATE' 											=> 'تحديث',
'BACK_CANCEL' 											=> 'إلغاء',
'BACK_CANCEL_1' 											=> 'إلغاء',
'BACK_RESET' 											=> 'إعادة تعيين',
'BACK_RESET_1' 											=> 'إعادة تعيين',
/*-------Settings - Email Settings ----------*/		
'BACK_EMAIL_SETTINGS' 									=> 'اعدادات البريد الإلكتروني',
'BACK_CONTACT_NAME' 									=> 'اسم جهة الاتصال',
'BACK_CONTACT_E_MAIL' 									=> 'البريد اللكتروني',
'BACK_WEBMASTER_E_MAIL' 								=> 'البريد الإلكتروني لمشرف الموقع',
'BACK_SITE_NO_REPLY_E_MAIL' 							=> 'Site no-reply E-Mail',
'BACK_CONTACT_PHONE_1' 									=> 'رقم الجوال 1',
'BACK_CONTACT_PHONE_2' 									=> 'رقم الجوال 2',
'BACK_EXAMPLE' 											=> 'مثال',
'BACK_LATITUDE' 										=> 'خط العرض',
'BACK_LONGITUDE' 										=> 'خط الطول',
/*-------Settings - Social Media Pages ----------*/	

'BACK_SOCIAL_MEDIA_PAGE' 								=> 'صفحات التواصل الإجتماعي',
'BACK_FACEBOOK_APP_ID' 									=> 'Facebook App ID',
'BACK_FACEBOOK_SECRECT_KEY' 							=> 'Facebook Secrect Key',
'BACK_FACEBOOK_PAGE_URL' 								=> 'رابط صفحة الفيسبوك',
'BACK_FACEBOOK_LIKE_BOX_URL' 							=> 'FaceBook Like box URL',
'BACK_TWITTER_PAGE_URL' 								=> 'رابط صفحة تويتر',
'BACK_TWITTER_APP_ID' 									=> 'Twitter App ID',
'BACK_TWITTER_SECRECT_KEY' 								=> 'Twitter Secrect Key',
'BACK_LINKEDIN_PAGE_URL' 								=> 'رابط صفحة لينكد إن',
'BACK_YOUTUBE_URL' 										=> 'رابط قناة اليوتيوب',
'BACK_GMAP_APP_KEY' 									=> 'Gmap App Key',
'BACK_ANALYTICS_CODE' 									=> 'Analytics Code',
/*-------Settings - Payment Settings ----------*/			
'BACK_COUNTRY_CURRENCY' 								=> 'الدولة / العملة',
'BACK_COUNTRY_NAME' 									=> 'اسم الدولة',
'BACK_SELECT' 											=> 'اختيار',
'BACK_COUNTRY_CODE' 									=> 'رمز الدولة',
'BACK_CURRENCY_SYMBOL' 									=> 'رمز العملة',
'BACK_CURRENCY_CODE' 									=> 'كود العملة',
'BACK_PAYMENT_ACCOUNT' 									=> 'حساب الدفع',
'BACK_PAYPAL_ACCOUNT' 									=> 'Paypal حساب',
'BACK_PAYPAL_API_PASSWORD' 								=> 'Paypal API كلمة المرور',
'BACK_PAYPAL_API_SIGNATURE' 							=> 'Paypal API Signature',
'BACK_PAYUMONEY_KEY' 							         => 'PayUmoney Key',
'BACK_PAYUMONEY_SALT' 									=> 'PayUmoney Salt',
'BACK_PAYMENT_MODE' 									=> 'طريقة الدفع',
'BACK_TEST_ACCOUNT' 									=> 'حساب تجريبي',
'BACK_LIVE_ACCOUNT' 									=> 'حساب حقيقي',
/*-------Settings - Logo Settings ----------*/		
'BACK_CURRENT_LOGO' 									=> 'الشعار الحالي',
'BACK_UPLOAD_BANNER_IMAGE' 								=> 'رفع صورة بنر',
'BACK_BANNER_IMAGE_SIZE' 								=> '',
/*-------Settings - Favicon Settings ----------*/
'BACK_CURRENT_FAVICON' 									=> 'أيقونة العملة',
/*------------------Settings - Noimage Settings ----------------------*/		
'BACK_CURRENT_NOIMAGE' 									=> 'بلا صورة الحالية',
/*------------------Settings - Add Banner Image ----------------------*/		
'BACK_IMAGE_TITLE' 										=> 'عنوان الصورة',
'BACK_MAIN_CAT_TITLE' 									=> 'اسم التصنيف الرئيسي',
'BACK_BANNER_TITLE' 									=> 'عنوان البنر',
'BACK_REDIRECT_URL' 									=> 'رابط إعادة التوجية',
/*------------------Settings - Manage Banner Image ----------------------*/		
'BACK_MANAGE_BANNER' 									=> 'إدارة البنر',
/*------------------Settings - Edit Banner Image ----------------------*/		
'BACK_EDIT_BANNER_IMAGE' 								=> 'تعديل صورة البنر',

/*------------------Settings - Add Advertisement ----------------------*/	
'BACK_SUB_CATEGORY' 									=> 'الأقسام الفرعية',
'BACK_ADVERTISEMENT_IMAGE' 								=> 'صورة الإعلان',
'BACK_ADD' 												=> 'إضافة',
'BACK_MAXIMUM' 											=> 'الحد الأقصى',
/*------------------Settings - Manage Advertisement Banner ----------------------*/			
'BACK_MANAGE_ADVERTISEMENT_BANNER' 						=> 'إدارة بنر الإعلان',
/*------------------Settings - Edit Advertisement Banner ----------------------*/
'BACK_EDIT_ADVERTISEMENT' 								=> 'تعديل الإعلان',
'BACK_IMAGE_SIZE_MUST_BE' 								=> 'حجم الصورة يجب أن يكون',
'BACK_PIXELS' 											=> 'بكسل',
/*------------------Settings - Add Category Banner ----------------------*/		
'BACK_REMOVE' 											=> 'حذف',
'BACK_BANNER_IMAGE' 									=> 'صورة البنر',
'BACK_ADD_BANNER' 										=> 'إضافة بنر',
/*------------------Settings - Manage Category Banner ----------------------*/
'BACK_EDIT_CATEGORY_BANNER' 							=> 'Edit Category Banner',
'BACK_UPDATE_BANNER' 									=> 'Update Banner',
/*------------------Settings - Color ----------------------*/
'BACK_SELECT_COLOR' 									=> 'إختر لون',
'BACK_YOUR_COLOR' 										=> 'اللون المحدد',
'BACK_RGB' 												=> 'RGB',

'BACK_COLOR_CODE' 										=> 'Color Code',
'BACK_COLOR_NAME' 										=> 'Color Name',
'BACK_EDIT_COLOR' 										=> 'Edit Color',
'BACK_RECORD_ALREADY_EXISTS'							=> 'Color Code Already Exists',
/*------------------Settings - Size ----------------------*/		
'BACK_ATTRIBUTES_ADD_SIZE' 								=> 'Attributes Add Size',
'BACK_ADD_SIZE' 										=> 'Add Size',
'BACK_SIZE' 											=> 'Size',
'BACK_ATTRIBUTES_MANAGE_SIZES' 							=> 'Attributes Manage Sizes',
'BACK_ATTRIBUTES_EDIT_SIZE' 							=> 'Attributes Edit Size',
'BACK_EDIT_SIZE' 										=> 'Edit Size',
/*------------------Settings - Specification ----------------------*/		
'BACK_SPECIFICATION_NAME' 								=> 'Specification name',
'BACK_SPECIFICATION_GROUP_NAME' 						=> 'Specification group name',
'BACK_SORT_ORDER' 										=> 'Sort order',
'BACK_NUMERIC_VALUES_ONLY' 								=> 'Numeric Values Only',
'BACK_EDIT_SPECIFICATION' 								=> 'Edit specification',
/*------------------Settings - Specification Group ----------------------*/		
'BACK_ADD_SPECIFICATION_GROUP' 							=> 'Add specification Group',
'BACK_MANAGE_SPECIFICATION_GROUP' 						=> 'Manage specification Group',
'BACK_EDIT_SPECIFICATION_GROUP' 						=> 'Edit specification group',
'BACK_ETC' 												=> 'etc',
/*------------------Settings - Country Management ----------------------*/		
'BACK_MANAGE_COUNTRIES' 								=> 'إدارة الدول', 											
'BACK_COUNTRY' 											=> 'الدولة',
'BACK_EDIT_COUNTRY' 									=> 'تعديل الدولة',
/*------------------Settings - City Management ----------------------*/			
'BACK_CHOOSE_A_COUNTRY' 								=> 'إختر الدولة',
'BACK_CITY_NAME' 										=> 'اسم المدينة',
'BACK_CITY_LATITUDE' 									=> 'خطوط عرض المدينة',
'BACK_CITY_LONGITUDE' 									=> 'خطوط طول المدينة',
'BACK_CITY' 											=> 'المدينة',
'BACK_DEFAULT' 											=> 'تلقائي',
'BACK_EDIT_CITY' 										=> 'تعديل المدينة',
/*------------------Settings - Category Management ----------------------*/			
'BACK_TOP_CATEGORY_NAME' 								=> 'اسم القسم الأعلى',
'BACK_ENTER_CATEGORY_NAME' 								=> 'أدخل اسم القسم',
'BACK_CATEGORY_STATUS' 									=> 'حالة القسم',
'BACK_SUBSCRIPTION_TYPE' 								=> 'نوع الاشتراك',
'PAYMENT_TYPE' 									        => 'نوع الدفع',
'BACK_ACTIVE' 											=> 'مفعل',
'BACK_DEACTIVE'											=> 'غير مفعل',
'BACK_COMMISSION_TYPE' 									=> 'العمولة',
'BACK_SUBSCRIBE_TYPE'									=> 'الرسوم',
'BACK_TOP_CATEGORY_IMAGE'								=> 'صورة القسم الأعلى',
'BACK_CHOOSE_IMAGE'										=> 'إختر صورة',
'BACK_ADD_MAIN_CATEGORY'								=> 'إضافة قسم رئيسي',
'BACK_MANAGE_MAIN_CATEGORY'								=> 'إدارة القسم الرئيسي',
'BACK_CATEGORIES'										=> 'الأقسام',
'BACK_EDIT_TOP_CATEGORY'								=> 'تحديد القسم الأعلى',
'BACK_LISTING_CATEGORY_IMAGE'							=> 'صورة قائمة الأقسام',
'BACK_LISTING_SEC_SUBCATEGORY_IMAGE'					=> 'صورة الجوال للقسم الفرعي',
'BACK_MAIN_CATEGORY_NAME'								=> 'اسم القسم الرئيسي',
'BACK_MANAGE_MAIN_CATEGORIES'							=> 'إدارة الأقسام الرئيسية',
'BACK_ADD_SUB_CATEGORY'									=> 'إضافة قسم فرعي',
'BACK_MANAGE_SUB_CATEGORY'								=> 'إدارة الأقسام الفرعية',
'BACK_NO_DATAS_AVAILABLE'								=> 'لا توجد بيانات',
'BACK_EDIT_MAIN_CATEGORY'								=> 'تعديل القسم الرئيسي',
'BACK_SUB_CATEGORY_NAME'								=> 'اسم القسم الفرعي',
/*------------------Settings - Service Type Management ----------------------*/
'BACK_ADD_SERVICE_TYPE'									=> 'أضف نوع الخدمة',
'BACK_SERVICE_TYPE_NAME'								=> 'اسم النوع',
'BACK_ENTER_SERVICE_TYPE'								=> 'أدخل نوع الخدمة',
'BACK_SERVICE_TYPE_STATUS'								=> 'حالة نوع الخدمة',
'BACK_MANAGE_SERVICE_TYPE'								=> 'إدارة نوع الخدمة',
'BACK_EDIT_SERVICE_TYPE'								=> 'تعديل نوع الخدمة',
'BACK_ENTER_BUSINESS_CATEGORY'							=> 'أدخل القسم',
'BACK_BUSINESS_CATEGORY_STATUS'							=> 'حالة القسم',

/*------------------Settings - Service Time ----------------------*/
'BACK_SERVICE_TIME'										=> 'وقت الخدمة',
'BACK_ADD_SERVICE_TIME'									=> 'إضافة وقت الخدمة',
'BACK_ENTER_SERVICE_TIME'								=> 'أدخل وقت الخدمة',
'BACK_SERVICE_TIME_STATUS'								=> 'حالة وقت الخدمة',
'BACK_MANAGE_SERVICE_TIME'								=> 'إدارة وقت الخدمة',
'BACK_EDIT_SERVICE_TIME'								=> 'تعديل وقت الخدمة',
/*------------------Settings - CMS MANAGEMENT ----------------------*/		
'BACK_ADD_CMS_PAGE'										=> 'إضافة صفحة محتوى ثابت',
'BACK_PAGE_TITLE'										=> 'عنوان الصفحة',
'BACK_PAGE_DESCRIPTION'									=> 'وصف الصفحة',
'BACK_MANAGE_CMS_PAGE'									=> 'إدارة صفحة المحتوى',
'BACK_EDIT_CMS_PAGE'									=> 'تعديل صفحة المحتوى',
'BACK_CMS_ABOUT_US'										=> 'صفحة حول',
'BACK_ADD_ABOUT_US_PAGE_DATA'							=> 'إضافة بيانات لصفحة حول',
'BACK_TERMS_PAGE'										=> 'صفحة الشروط',
'BACK_ADD_TERMS_PAGE_DATA'								=> 'إضافة بيانات لصفحة الشروط',
/*------------------Settings - FAQ ----------------------*/
'BACK_ENTER_YOUR_QUESTION'								=> 'أدخل سؤالك',
'BACK_QUESTION'											=> 'السؤال',
'BACK_ANSWER'											=> 'الإجابة',
'BACK_EDIT_FAQ'											=> 'تعديل الأسئلة الشائعة',
'BACK_ENTER_YOUR_ANSWER'								=> 'أدخل اجابتك',
/*------------------Settings - ADS MANAGEMENT ----------------------*/
'BACK_ADS_ADD'						=> 'Ads Add',
'BACK_AD_TITLE'						=> 'Ad Title',
'BACK_ADS_POSITION'					=> 'Ads Position',
'BACK_SELECT_POSITION'				=> 'select position',
'BACK_MIDDLE_PAGE'					=> 'Middle Page',
'BACK_UPLOAD_IMAGE'					=> 'Upload Image',
'BACK_MANAGE_ADS'					=> 'Manage Ads',
'BACK_AD_ID'						=> 'Ad Id',
'BACK_HEADER_RIGHT'					=> 'Right Position',
'BACK_LEFT_SIDE_BAR'				=> 'Left Position',
'BACK_BOTTOM_FOOTER'				=> 'Middle Position',
'BACK_SELECT_ANY_PAGE'				=> 'select any page',
'BACK_AUCTION'						=> 'Auction',
'BACK_EDIT_ADS'						=> 'Edit Ads',
'NO_RECORD_FOUND'                       => 'لا توجد سجلات متاحة',

/*------------------Settings - NEWS LETTER ----------------------*/
'BACK_SUBJECT'						=> 'الموضوع',
'BACK_MESSAGE'						=> 'الرسالة',
'BACK_SEND'							=> 'إرسال',
'BACK_MANAGE_SUBSCRIBERS'			=> 'إدارة المشتركين',
'BACK_SUBSCRIBE'					=> 'اشتراك',
'BACK_UNSUBSCRIBE'					=> 'إلغاء الاشتراك',
/*------------------Settings - COUPON CODE ----------------------*/		
'BACK_AUTO_GENERATE_COUPON_CODE'	=> 'Auto Generate Coupon Code',
'BACK_CUSTOM_COUPON_CODE'			=> 'Custom Coupon Code',
'BACK_COUPON_CODE'					=> 'Coupon Code',
'BACK_COUPON_NAME'					=> 'Coupon Name',
'BACK_TYPE'							=> 'Type',
'BACK_FLAT'							=> 'Flat',
'BACK_PERCENTAGE'					=> 'Percentage',
'BACK_VALUE'						=> 'Value',
'BACK_START_DATE'					=> 'Start Date',
'BACK_END_DATE'						=> 'End Date',
'BACK_TYPE_OF_COUPON'				=> 'Type of Coupon',
'BACK_PRODUCT_COUPON'				=> 'Product Coupon',
'BACK_USER_COUPON'					=> 'User Coupon',
'BACK_USER_COUPON_BY_CITY'			=> 'User Coupon By City',
'BACK_PRODUCT'						=> 'Product',
'BACK_COUPON_PER_PRODUCT'			=> 'Coupon per Product',
'BACK_COUPON_PER_USER'				=> 'Coupon per User',
'BACK_QUANTITY'						=> 'Quantity',
'BACK_CART_TOTAL_AMOUNT'			=> 'Cart Total Amount',
'BACK_USER'							=> 'User',
'BACK_CHOOSE_CITY'					=> 'Choose City',
'BACK_COUPON_CODE_ALREADY_AVAILABLE'		=> 'Coupon code already available',
'BACK_PLEASE_CHOOSE_CITY_TO_LIST_USERS'		=> 'Please Choose City to list Users',
'BACK_NO_USER_AVAILABLE_IN_CHOOSEN_CITY'	=> 'No User Available in Choosen City!',
'BACK_EDIT_COUPON'							=> 'Edit Coupon',
'BACK_COUPON_CODE'					=> 'Coupon Code',
'BACK_GENERATE'						=> 'generate',





/*-------BLOGS - Manage Publish Blog ----------*/

'BACK_MANAGE_PUBLISH' 				=> 	'Manage Publish Blog',
'BACK_HOME' 						=> 	'Home',
'BACK_SNO' 							=> 	'الرقم التسلسلي',
'BACK_BLOG_TITLE' 					=> 	'Blog Title',
'BACK_CATEGORY' 					=> 	'التصنيف',
'BACK_DATE' 						=> 	'التاريخ',
'BACK_IMAGE' 						=> 	'Image',
'BACK_EDIT' 						=> 	'Edit',
'BACK_STATUS' 						=> 	'Status',
'BACK_DELETE' 						=> 	'Delete',
'BACK_BLOCK' 						=> 	'Click to Block',
'BACK_BLOG_DETAILS' 				=> 	'Blog details',
'BACK_VIEW_DETAILS' 				=> 	'View details',
/* -------BLOGS - Manage Publish Blog End--------*/


/*-------BLOGS - Add Blog ----------*/
'BACK_ADD_BLOG' 					=> 	'Add Blog',
'BACK_TITLE' 						=> 	'Title',
'BACK_DESCRIPTION' 					=> 	'Description',
'BACK_SELECT' 						=> 	'Select',
'BACK_SELECT_SNAPSHOT' 				=> 	'Select a snapshot (png,jpg,jpeg less than 1MB and',
'BACK_SELECT_IMAGE' 				=> 	'image size must be 320 x 190 pixels(Formats:jpeg,gif,png,jpg)',
'BACK_META_TITLE' 					=> 	'عنوان البيانات الوصفية',
'BACK_META_DESCRIPTION' 			=> 	'وصف البيانات الوصفية',
'BACK_META_KEYWORDS' 				=> 	'مفاتيح الكلمات الوصفية',
'BACK_TAGS' 						=> 	'Tags',
'BACK_ALLOW_COMMENTS' 				=> 	'السماح بالتعليقات',
'BACK_PUBLISH' 			   			=> 	'نشر',
'BACK_DRAFT' 			   			=> 	'مسودة',
'BACK_SUBMIT' 			   			=> 	'حفظ',
'BACK_SUBMIT_1' 			   		=> 	'إرسال',
'BACK_RESET' 			   			=> 	'إعادة تعيين',
'BACK_UPLOAD_IMAGE' 	 			=> 	'Please upload Blog Image',
'BACK_UPLOAD_AD_IMAGE' 	 			=> 	'Upload Ads Image',
'BACK_UNBLOCKED_SUCC' 	 			=> 	'Record Unblocked Successfully',
'BACK_BLOCKED_SUCC' 	 			=> 	'Record Blocked Successfully',
'BACK_BLOG_EXISTS'	 	 			=> 	'Already Blog Title Exists',
'BACK_REQUIRED'	 	 				=> 	'required',
/*-------BLOGS - Add Blog End----------*/

/*-----------------BLOGS - Edit Blog --------------------*/
'BACK_EDIT_BLOG' 					=> 	'Edit Blog',
/*-----------------BLOGS - Edit Blog End-----------------*/


/*-------BLOGS - Manage Drafted Blog ----------*/
'BACK_DRAFTED_BLOG' 				=> 	'Manage Drafted Blog',
/*-------BLOGS - Manage Drafted Blog End----------*/

/*-------BLOGS - Blog Settings ----------*/
'BACK_BLOG_SETTIGNS' 				=> 	'Blog Settings',
'BACK_ALLOW_COMMENTS' 				=> 	'Allow Comments Posting In Blog',
'BACK_YES' 							=> 	'Yes',
'BACK_NO' 							=> 	'No',
'BACK_ADMIN_APPROVAL' 				=> 	'Require Admin Approval For Every Comment',
'BACK_POST_PER_PAGE'				=> 	'Posts Per Page',
'BACK_UPDATE' 						=> 	'تحديث',
'BACK_CANCEL' 						=> 	'إلغاء',		
/*-------BLOGS - Blog Settings End----------*/


/*-------BLOGS - Manage Blog comments ----------*/
'BACK_BLOG_COMMENTS' 				=> 	'Manage Blog Comments',
'BACK_NAME' 						=> 	'الاسم',
'BACK_EMAIL' 						=> 	'البريد الالكتروني',
'BACK_WEBSITE' 						=> 	'الموقع الالكتروني',
'BACK_COMMENTS' 					=> 	'التعليقات',
'BACK_APPROVE' 						=> 	'موافقة',
'BACK_UNAPPROVE' 					=> 	'عدم موافقة',
'BACK_COMMENT_REPLY' 				=> 	'الرد على التعليقات',
'BACK_REPLY' 						=> 	'رد',
'BACK_REJECTED' 					=> 	'مرفوض',
/*-------BLOGS - Manage Blog comments End----------*/


/*-------BLOGS - Side bar ----------*/
'BACK_PUBLISHED_BLOGS' 				=> 	'Manage Published Blogs',
'BACK_DRAFTED_BLOGS' 				=> 	'Manage Drafted Blogs',	
/*-------BLOGS - Side bar End----------*/

/*-------TRANSACTION - Deals All Orders ----------*/
'BACK_MULTIVENDOR' 					=> 	'Ecommerce Multivendor',
'BACK_DEALS_ALL_ORDERS'				=> 	'Deals All Orders',
'BACK_FROM_DATE'				    => 	'From Date',
'BACK_TO_DATE'				        => 	'To Date',
'BACK_SEARCH'				        => 	'Search',
'BACK_DEAL_TITLE'			        => 	'Deal Title',
'BACK_AMOUNT'			            => 	'Amount',
'BACK_TAX'			                => 	'Tax',
'BACK_TRANSACTION_DATE'             => 	'Transaction Date',
'BACK_TRANSACTION_TYPE'             => 	'Transaction Type',
'BACK_TRANSACTION_TYPE_WALLET'             => 	'Transaction Type',
'BACK_COD_RETURN_ORDERS'			=> 	'Return Orders',
'BACK_REPLACE_ORDERS'				=> 	'Replacement Orders',
/*-------TRANSACTION - Deals All Orders End ----------*/

/*-------TRANSACTION - Deals Success Orders ----------*/
'BACK_LARAVEL'               		=> 	'Laravel',
'BACK_SUCCESS_ORDERS'          		=> 	'Deals Success Orders',
/*-------TRANSACTION - Deals Success Orders End ----------*/


/*-------TRANSACTION - Deals Hold Orders ----------*/
'BACK_HOLD_ORDERS'             			=> 	'Deals Hold Orders',
/*-------TRANSACTION - Deals Hold Orders End----------*/


/*-------TRANSACTION - Deals Deals Failed Orders ----------*/
'BACK_FAILED_ORDERS'             		=> 	'Deals Failed Orders',
/*-------TRANSACTION - Deals Deals Failed Orders End----------*/

/*-------TRANSACTION - DealsCOD All Orders ----------*/
'BACK_COD_ALL_ORDERS'             	=> 	'Deals COD All Orders',


/*-------TRANSACTION - DealsCOD All Orders End -----------*/	



/*-------TRANSACTION - Deals COD Hold Orders ----------*/
'BACK_COD_HOLD_ORDERS'             	=> 	'صفقات الدفع عند الإستلام المعلقة',
'BACK_COD_STATUS_ACTION'           	=> 	'إجراءات حالة الدفع عند الإستلام',

/*-------TRANSACTION - Deals COD Hold Orders End -----------*/


/*-------TRANSACTION - Deals COD Failed Orders ----------*/
'BACK_COD_FAILED_ORDERS'           	=> 	'صفقات الدفع عند الإستلام الفاشلة',
'BACK_SUCCESS'            		 	=> 	'ناجح',
'BACK_HOLD'            			 	=> 	'معلق',
'BACK_FAILED'            		 	=> 	'فشل',
'BACK_COMPLETED'           		 	=> 	'مكتمل',
'BACK_PAYPAL'           		 	=> 	'Paypal',
'BACK_COD'           		 		=> 	'الدفع عند الإستلام',
'BACK_PAYUMONEY'      		 		=> 	'PayUmoney',


/*-------TRANSACTION - Deals COD Failed Orders End -----------*/

/*-------TRANSACTION - Products All Orders ----------*/
'BACK_PRODUCTS_ALL_ORDERS'         	=> 	'منتجات جميع الطلبات',
'BACK_PRODUCT_TITLE'       		  	=> 	'عنوان المنتج',
/*-------TRANSACTION - Products All Orders End -----------*/


/*-------TRANSACTION - Products Success Orders ----------*/
'BACK_PRODUCTS_SUCCESS_ORDERS'     	=> 	'منتجات الطلبات الناجحة',
/*-------TRANSACTION - Products Success Orders End -----------*/

/*-------TRANSACTION - Products Hold Orders ----------*/
'BACK_PRODUCTS_HOLD_ORDERS'     	=> 	'منتجات الطلبات المعلقة',
/*-------TRANSACTION - Products Hold Orders End -------*/

/*-------TRANSACTION - Products Failed Orders ------------*/
'BACK_PRODUCTS_FAILED_ORDERS'     	=> 	'منتجات الطلبات الفاشلة',
/*-------TRANSACTION - Products Hold Failed End ------------*/


/*-------TRANSACTION - COD All Orders ---------------*/
'BACK_COD_ALL_ORDERS'   		  	=> 	'جميع طلبات الدفع عند الإستلام',
'BACK_SHIPPING_DETAILS'   		  	=> 	'تفاصيل الشحن',
/*-------TRANSACTION - COD All Orders End ------------*/

/*-------TRANSACTION - COD Hold Orders ---------------*/
//'BACK_COD_ALL_ORDERS'   		  	=> 	'Products Hold Orders',
'BACK_COD_HOLD_ORDERS'   		  	=> 	'طلبات الدفع عند الإستلام المعلقة',
/*-------TRANSACTION - COD Hold Orders End ------------*/

/*-------TRANSACTION - COD Failed Orders ---------------*/
'BACK_COD_FAILED_ORDERS'   		  	=> 	'طلبات الدفع عند الإستلام الفاشلة',
/*-------TRANSACTION - COD Failed Orders End ------------*/


/*-------TRANSACTION - Side Bar ---------------*/
'BACK_TRANSACTIONS'   		  		=> 	'المعاملات',
'BACK_DEALS_TRANSACTIONS'  	  		=> 	'معاملات الصفقات',
'BACK_DEALS_PAYUMONEY_TRANSACTIONS' => 	'معاملات PayUmoney',
'BACK_ALL_ORDERS'   		  		=> 	'جميع الطلبات',
'BACK_SUCCESS_ORDERS'   	  		=> 	'الطلبات الناجحة',
'BACK_HOLD_ORDERS'   		  		=> 	'الطلبات المعلقة',
'BACK_FAILED_ORDERS'   		  		=> 	'الطلبات الفاشلة',
'BACK_DEALS_COD'   			  		=> 	'صفقات الدفع عند الإستلام',		
'BACK_PRODUCTS_TRANSACTION' 	 	=> 	'معاملات المنتجات',
'BACK_PRODUCTS_COD' 	 			=> 	'منتجات الدفع عند الإستلام',
'BACK_PRODUCTS_PAYUMONEY' 	 	    => 	'منتجات PayUmoney',
/*-------TRANSACTION - Side Bar End ------------*/


/*-------TRANSACTION [FUND REQUESTS] - Manage Inquiries ---------------*/
'BACK_MANAGE_INQUIRIES'   		  	=> 	'إدارة الطلبات',
'BACK_MERCHANT_NAME'   			  	=> 	'اسم التاجر',
'BACK_MERCHANT_EMAIL'   		  	=> 	'بريد التاجر الإلكتروني',
'BACK_TOTAL_WITHDRAW_AMOUNT'   	  	=> 	'مجموع مبلغ السحب',
'BACK_PAID_WITHDRAW_AMOUNT'   	  	=> 	'مبلغ السحب المدفوع',
'BACK_REQUESTED_WITHDRAW_AMT'  	  	=> 	'مبلغ السحب المطلوب',
'BACK_ACTION'				  	  	=> 	'إجراء',
'BACK_PAY_NOW'				  	  	=> 	'الدفع الآن',
'BACK_PAY_NOW_PAYPAL'				  	  	=> 	'الدفع الان Paypal',
'BACK_PAY_NOW_PAYUMONEY'				  	  	=> 	'الدفع الان PayUmoney',
'BACK_NIL'			    	  	  	=> 	'صفر',
'BACK_NO_PAYPAL_EMAIL'		 	  	=> 	'لا يوجد ايميل دفع لهذا التاجر',
/*-------TRANSACTION [FUND REQUESTS] - Manage Inquiries End ------------*/

/*-------TRANSACTION [FUND REQUESTS] - Success Fund Request ---------------*/
'BACK_SUCCESS_FUND'   			  	=> 	'طلبات الدفع الناجحة',
'BACK_TRANSACTION_ID'   			=> 	'رقم المعاملة',
'BACK_PAID_AMOUNT'   				=> 	'المبلغ المدفوع',
'BACK_PENDING_FUND_REQUEST'   		=> 	'طلبات الدفع المعلقة',	
/*-------TRANSACTION [FUND REQUESTS] - Success Fund Request End ------------*/

/*-------TRANSACTION [FUND REQUESTS] - Failed Fund Request ---------------*/
'BACK_FAILED_FUND_REQUEST'   		=> 	'طلبات الدفع الفاشلة',
/*-------TRANSACTION [FUND REQUESTS] - Failed Fund Request End ------------*/

/*-------TRANSACTION [FUND REQUESTS] - Side Bar ---------------*/
'BACK_FUND_REQUEST'   				=> 	'طلبات الدفع',
'BACK_ALL_FUND_REQUEST'   			=> 	'جميع طلبات الدفع',
'BACK_SUCCESS_FUND_REQUEST'   		=> 	'طلبات الدفع الناجحة',
'BACK_PENDING_FUND_REQUEST'   		=> 	'طلبات الدفع المعلقة',
'BACK_FAILED_FUND_REQUESTS'   		=> 	'طلبات الدفع الفاشلة',
/*-------TRANSACTION [FUND REQUESTS] - Side Bar End ------------*/

/*-------MERCHANTS - Manage Merchant Accounts ---------------*/
'BACK_MANAGE_MERCHANT_ACCOUNTS'   	=> 	'إدارة حسابات مزودين الخدمة',
'BACK_EMAIL_ID'   					=> 	'البريد الإلكتروني',
'BACK_CITY'   						=> 	'المدينة',
'BACK_ADD_BRANCH'   				=> 	'إضافة فرع',
'BACK_MANAGE_BRANCH'   				=> 	'إدارة فرع',
'BACK_CREATED_MERCHANT'   		    => 	'تاريخ الإنشاء',
'BACK_UNBLOCK_MERCHANTS'   			=> 	'إلغاء حظر مزودين الخدمات',
'BACK_UNBLOCK_PRODUCTS'   			=> 	'إلغاء حظر المنتجات',
'BACK_ADD_BRANCHES'   				=> 	'إضافة فروع',
'BACK_BRANCHES'   					=> 	'الفروع',
'BACK_UNBLOCK'   					=> 	'اضغط لإلغاء الحطر',
'BACK_LOGIN_TYPE'   				=> 	'نوع تسجيل الدخول',
'BACK_MANAGE_STORE_REVIEW'   		=> 	'إدارة تقييمات المتاجر',

/*-------MERCHANTS - Manage Merchant Accounts End ------------*/


/*-------MERCHANTS - Add Merchant Store ---------------*/
'BACK_ADD_MERCHANT_STORE'   	  	=> 	'إضافة متجر',
'BACK_STORE_NAME'   				=> 	'اسم المتجر',
'BACK_PHONE'   						=> 	'رقم الجوال',
'BACK_STORE_DETAILS'   				=> 	'تفاصيل المتجر ',
'BACK_ADDRESS1'   					=> 	'العنوان 1',
'BACK_ADDRESS2'   					=> 	'العنوان 2',
'BACK_SELECT_COUNTRY'   			=> 	'إختر الدولة',
'BACK_SELECT_COUNTRY_CODE'   	 	=>  'إختر رمز الدولة',
'BACK_SELECT_CITY'   				=> 	'إختر المدينة',
'BACK_ZIPCODE'   					=> 	'الرمز البريدي',
'BACK_SEARCH_LOCATION'   			=> 	'موقع المتجر على خرائط قوقل',
'BACK_DRAG_MARKER'   				=> 	'اسحب العلامة لتحصل على خطوط العرض',
'BACK_LONGITUDE'   					=> 	'خطوط الطول',
'BACK_LATITUDE'   					=> 	'خطوط العرض',
'BACK_STORE_IMAGE'   				=> 	'صورة المتجر',
'BACK_UPLOAD_SIZE'   				=> 	'حجم رفع الصورة',
'BACK_TYPE_YOUR_LOCATION'   		=> 	'أدخل الموقع هنا',
'BACK_AUTO_COMPLETE'   	 			=> 	'الاكمال التلقائي',
'BACK_WEBSITE_MERCHANT'   	 		=> 	'موقع مزود الخدمة',
'BACK_FACEBOOK_MERCHANT'   	 		=> 	'حساب الفيسبوك',

/*-------MERCHANTS - Add Merchant Store End ------------*/


/*-------MERCHANTS - Edit Merchant Account ---------------*/
'BACK_EDIT_MERCHANT'   				=> 	'تعديل حساب مزود الخدمة',
'BACK_MERCHANT_ACCOUNT'   			=> 	'حساب مزود الخدمة',
'BACK_FIRST_NAME'   				=> 	'الاسم الأول',
'BACK_LAST_NAME'   					=> 	'الاسم الأخير',
'BACK_PAYMENT_ACCOUNT'   			=> 	'حساب الدفع',
'BACK_PAYUMONEY_KEY'   				=> 	'PayUmoney Key',
'BACK_PAYUMONEY_SALT'   			=> 	'PayUmoney Salt',
'BACK_COMMISSION'   				=> 	'العمولة',
'BACK_PAYUMONEY_ACCOUNT'   			=> 	'PayUmoney Account',
/*-------MERCHANTS - Edit Merchant Account End ------------*/

/*-------MERCHANTS - Add Merchant Account ---------------*/
'BACK_ADD_MERCHANT'   				=> 	'إضافة حساب مزود الخدمة',
'BACK_PAYMENT_ACCOUNT_DETAILS'   	=> 	'تفاصيل حساب الدفع',
'BACK_PAYUMONEY_ACCOUNT_DETAILS'   	=> 	'PayUmoney تفاصيل حساب',
'BACK_STORES_IMAGE'   				=> 	'صورة المتاجر',
'BACK_OR'   						=> 	'أو',
'BACK_PAYMENT_BANK_DETAILS'   		=> 	'تفاصيل الدفع البنكي',	
'BACK_PAYUMONEY_DETAILS'   		=> 	'PayUmoney Details',	
/*-------MERCHANTS - Add Merchant Account End ------------*/

/*-------MERCHANTS - Merchant Dashboard ---------------*/
'BACK_MERCHANT_DASHBOARD'   		=> 	'لوحة تحكم التاجر',
'BACK_TOTAL_MERCHANTS'   			=> 	'اجمالي التجار',
'BACK_TOTAL_STORES'   				=> 	'اجمالي المتاجر',
'BACK_ADMIN_ADD_STORES'   			=> 	'إضافة متاجر-المدير',
'BACK_WEBSITE_MERCHANT'   			=> 	'تاجر الموقع',
'BACK_ADMIN_MERCHANT'   			=> 	'إضافة تاجر-المدير',
'BACK_ACTIVE_MERCHANT'   			=> 	'التجار النشطين',
'BACK_INACTIVE_MERCHANT'   			=> 	'التجار الغير نشطين',
'BACK_MERCHANT_ADD_STORES'   		=> 	'إضافة متاجر -التاجر',
'BACK_MERCHANT_DETAILS'   			=> 	'تفاصيل التاجر',
'BACK_STORES_DETAILS'   			=> 	'المتاجر الخاصة بالتاجر',
'BACK_COMPLETE'   					=> 	'مكتمل',
'BACK_TOTAL_STORES'   				=> 	'اجمالي المتاجر',
'BACK_MERCHANT_REG_DETAILS'  		=> 	'تفاصيل تسجيل التاجر',
'BACK_WEBSITE_MERCHANTS'   			=> 	'بيانات دخول التاجر',
'BACK_COUNT'   						=> 	'عدد',
'BACK_LAST'   						=> 	'آخر',
'BACK_TODAY'   						=> 	'اليوم',
'BACK_DAYS'   						=> 	'أيام',
'BACK_MONTHS'   					=> 	'شهور',
'BACK_STATISTICS'   				=> 	'إحصائيات',
'BACK_LAST_ONE_YEAR_TRANS'   		=> 	'تفاصيل التاجر لمدة عام واحد',
'BACK_SEE_ALL_TRANSACTION'   		=> 	'عرص جميع المعاملات',
'BACK_SEE_ALL_PRODUCT_TRANSACTION'  => 	'عرض جميع منتجات معاملات باي بال',
'BACK_SEE_ALL_PRODUCT_COD_TRANSACTION'   	=> 	'عرض جميع منتجات الدفع عند الإستلام',
'BACK_SEE_ALL_PRODUCT_PAYU_TRANSACTION'   	=> 	'انظر جميع منتجات معاملات PayUmoney ',
'BACK_SEE_ALL_DEAL_TRANSACTION'  => 	'عرض جميع صفقات معاملات Paypal',
'BACK_SEE_ALL_DEAL_COD_TRANSACTION'   	=> 	'عرض جميع صفقات معاملات الدفع عند الإستلام',
'BACK_SEE_ALL_DEAL_PAYU_TRANSACTION'   	=> 	'عرض جميع صفقات معاملات PayUmoney',

'BACK_JANUARY'   					=> 	'يناير',
'BACK_FEBRUARY'   					=> 	'فبراير',
'BACK_MARCH'   						=> 	'مارس',
'BACK_APRIL'   						=> 	'أبريل',
'BACK_MAY'   						=> 	'مايو',
'BACK_JUNE'   						=> 	'يونيو',
'BACK_JULY'   						=> 	'يوليو',
'BACK_AUGUST'   					=> 	'أغسطس',
'BACK_SEPTEMBER'   					=> 	'سبتمبر',
'BACK_OCTOBER'   					=> 	'أكتوبر',
'BACK_NOVEMBER'   					=> 	'نوفمبر',
'BACK_DECEMBER'   					=> 	'ديسمبر',
/*-------MERCHANTS - Merchant Dashboard End ------------*/

/*-------MERCHANTS - Side bar ---------------*/
'BACK_MERCHANTS'   					=> 	'مزودين الخدمات',
'BACK_MERCHANTS_DASHBOARD'   		=> 	'لوحة تحكم مزود الخدمة',
/*-------MERCHANTS - Side bar End ------------*/

/*-------CUSTOMERS - Customer Dashboard ---------------*/
'BACK_CUSTOMER_DASHBOARD'   		=> 	'لوحة تحكم العميل',
'BACK_TOTAL_CUSTOMERS'   			=> 	'اجمالي العملاء',
'BACK_CUSTOMER_DETAILS'   			=> 	'تفاصيل العميل',
'BACK_WEBSITE_CUSTOMERS'   			=> 	'تفاصيل أيام تسجيل دخول العملاء',
'BACK_WEBSITE_CUSTOMER'   			=> 	'عملاء الموقع',
'BACK_ADMIN_CUSTOMERS'   			=> 	'عملاء اداريين',
'BACK_FACEBOOK_CUSTOMERS'   		=> 	'عملاء فيسبوك',
'BACK_CUSTOMER_LOGIN_DETAILS'   	=> 	'بيانات دخول العميل',
'BACK_TODAY'   						=> 	'اليوم',
'BACK_LAST_ONE_YEAR_WEB_CUST'   	=> 	'عملاء الموقع في آخر سنة',
/*-------CUSTOMERS - Customer Dashboard End ------------*/

/*-------CUSTOMERS -Add Customer  ---------------*/
'BACK_ADD_CUSTOMER'   				=> 	'إضافة عميل',
'BACK_ENTER_CUSTOMER_NAME'   		=> 	'أدخل اسم العميل',
'BACK_ENTER_EMAIL_ID'   			=> 	'أدخل البريد الإلكتروني',
'BACK_ENTER_CONTACT_NUMBER'   		=> 	'أدخل رقم الجوال',
'BACK_ENTER_ADDRESS_ONE'   			=> 	'أدخل العنوان 1',
'BACK_ENTER_ADDRESS_TWO'   			=> 	'أدخل العنوان 2',
'BACK_ENTER_INFO_PASSWORD'   		=> 	'6 خانات كحد أدنى',
/*-------CUSTOMERS - Add Customer  End ------------*/

/*-------CUSTOMERS - Manage Customer  ---------------*/
'BACK_MANAGE_CUSTOMERS'   			=> 	'إدارة العملاء',
'BACK_JOINED_DATE'   				=> 	'تاريخ ووقت الإنشاء',
'BACK_LOGIN_TYPE'   				=> 	'نوع الدخول',
'BACK_EDIT_CUSTOMER'   				=> 	'تعديل العميل',
/*-------CUSTOMERS - Manage Customer  End ------------*/

/*-------CUSTOMERS - Manage Inquiries  ---------------*/
'BACK_PHONE_NUMBER'   				=> 	'رقم الجوال',
'BACK_MESSAGE'   					=> 	'الرسالة',
/*-------CUSTOMERS - Manage Inquiries  End ------------*/

/*-------CUSTOMERS - Side Bar ---------------*/
'BACK_DASHBOARD'   					=> 	'لوحة التحكم',
/*-------CUSTOMERS - Side Bar End ------------*/

/*-------SERVICES - Service Dashboard ---------------*/
'BACK_SERVICE_DASHBOARD'   			=> 	'لوحة تحكم الخدمات',
'BACK_SERVICE'   					=> 	'الخدمات',
'BACK_TOTAL_SERVICE_COUNT'   		=> 	'اجمالي الخدمات',
'BACK_ACTIVE_SERVICE'   			=> 	'الخدمات النشطة',
'BACK_INACTIVE_SERVICE'   			=> 	'الخدمات الموقفة',
'BACK_MARKET_RESPONSE_RATE'   		=> 	'معدل استجابة السوق',
'BACK_ACTIVE_SERVICE_COUNT'   		=> 	'عدد الخدمات النشطة',
'BACK_INACTIVE_SERVICE_COUNT'   	=> 	'عدد الخدمات الموقفة',
'BACK_SERVICE_TRANSACTION_DETAILS'  => 	'تفاصيل معاملة الخدمة',
'BACK_TRANSACTIONS'   				=> 	'المعاملات',
'BACK_AMOUNT'   					=> 	'القيمة',
/*-------SERVICES  - Service Dashboard End ------------*/


/*-------SERVICES  - Add Service ------------*/
'BACK_ADD_SERVICES'   				=> 	'إضافة خدمات',
'BACK_SERVICE_NAME'   				=> 	'اسم الخدمة',
'BACK_SERVICE_TYPE'   				=> 	'نوع الخدمة',
'BACK_SELECT_SERVICE_TYPE'   		=> 	'إختر نوع الخدمة',
'BACK_MORE_CUSTOM_SERVICE_TYPE'   	=> 	'إضافة نوع خجمة مخصص',
'BACK_ADD'   						=> 	'إضافة',
'BACK_SERVICE_TIMING'   			=> 	'وقت الخدمة',
'BACK_MINUTES'   					=> 	'دقائق',
'BACK_HOURS'   						=> 	'ساعات',
'BACK_DAYS'   						=> 	'أيام',
'BACK_WEEKS'   						=> 	'اسابيع',
'BACK_MONTH' 						=> 	'شهر',
'BACK_YEAR'   						=> 	'سنة',
'BACK_ORIGINAL_PRICE'   			=> 	'السعر الاصلي',
'BACK_DISCOUNTED_PRICE'   			=> 	'السعر بعد الخصم',
'BACK_CALENDER_OPTION'   			=> 	'خيار التقويم',
'BACK_ADD_FIELD'   					=> 	'إضافة حقل',
'BACK_ENTER_SERVICE_NAME'   		=> 	'أدخل اسم الخدمة',
'BACK_NUMBERS_ONLY'   				=> 	'ارقام فقط',
'BACK_ENTER_KEYWORD'   				=> 	'أدخل كلمة مفتاحية',
'BACK_ENTER_DESCRIPTION'   			=> 	'أدخل الوصف',
/*-------SERVICES - Add Services End ---------------*/

/*-------SERVICES - Manage Services  ---------------*/
'BACK_MANAGE_SERVICES'   			=> 	'إدارة الخدمات',
'BACK_SERVICE_DURATION'   			=> 	'مدة الخدمة',
/*-------SERVICES  - Manage Services  End ------------*/

/*-------SERVICES - Edit Services  ---------------*/
'BACK_EDIT_SERVICES'   				=> 	'تعديل الخدمات',
'BACK_SERVICE_DURATION'   			=> 	'مدة الخدمة',
/*-------SERVICES  - Edit Services  End ------------*/

/*-------PRODUCTS - Product Dashboard  ---------------*/
'BACK_PRODUCT_DASHBOARD'   			=> 	'لوحة تحكم المنتجات',
'BACK_DEALS'   						=> 	'الصفقات',
'BACK_TOTAL_PRODUCTS_COUNT'   		=> 	'عدد المنتجات',
'BACK_ACTIVE_PRODUCTS_COUNT'   		=> 	'عدد المنتجات النشطة',
'BACK_SOLD_PRODUCTS_COUNT'   		=> 	'عدد المنتجات المباعة',
'BACK_INACTIVE_PRODUCTS_COUNT'   	=> 	'عدد المنتجات الغير نشطة',
'BACK_PRODUCTS_TRANS_DETAILS'   	=> 	'تفاصيل المعاملات',
/*-------PRODUCTS  - Product Dashboard  End ------------*/

/*-------PRODUCTS - Add Products  ---------------*/
'BACK_ADD_PRODUCTS'   				=> 	'أضف منتجات',
'BACK_TOP_CATEGORY'   				=> 	'القسم الاعلى',
'BACK_MAIN_CATEGORY'   				=> 	'القسم الرئيسي',
'BACK_SUB_CATEGORY'   				=> 	'القسم الفرعي',
'BACK_SECOND_SUB_CATEGORY'   		=> 	'القسم الفرعي الثاني',
'BACK_BRAND'   						=> 	'العلامة التجارية',
'BACK_SELECT_PRODUCT_BRAND'   		=> 	'إختر علامة المنتج التجارية',
'BACK_PRODUCT_QUANTITY'   			=> 	'كمية المنتج',
'BACK_SHIPPING_AMOUNT'   			=> 	'رسوم الشحن',
'BACK_FREE'   						=> 	'مجاني',
'BACK_WANT_TO_ADD_SPECIFICATION'   	=> 	'هل تريد إضافة مواصفات',
'BACK_SPECIFICATION'   				=> 	'المواصفات',
'BACK_TEXT'   						=> 	'نص',
'BACK_MORE_CUSTOM_SPECIFICATION'   	=> 	'المزيد من المواصفات الخاصة',
'BACK_SELECT_SPECIFICATION'   		=> 	'إختر المواصفات',
'BACK_ADD_PRODUCT_SIZE'   			=> 	'أضف مقاسات المنتج',
'BACK_ADD_MORE'   					=> 	'إضافة المزيد',
'BACK_PRODUCT_SIZE'   				=> 	'مقاس المنتج',
'BACK_MORE_CUSTOM_SIZES'   			=> 	'المزيد من المقاسات المخصصة',
'BACK_YOUR_SELECT_SIZE'   			=> 	'المقاس المختار',
'BACK_QUANTITY'   					=> 	'الكمية',
'BACK_ADD_COLOR_FIELD'   			=> 	'حقل إضافة اللون',
'BACK_PRODUCT_COLOR'   				=> 	'لون المنتج',
'BACK_SELECT_PRODUCT_COLOR'   		=> 	'إختر لون المنتج',
'BACK_SELECTED_COLOR'   			=> 	'اللون المختار',
'BACK_MORE_CUSTOM_COLORS'   		=> 	'المزيد من الالوان المخصصة',
'BACK_DELIVERY_DAYS'   				=> 	'التسليم خلال',
'BACK_EG'   						=> 	'مثال',
'BACK_SELECT_MERCHANT'   			=> 	'إختر تاجر',
'BACK_CASH_BACK'   					=> 	'استرداد النقود الثابتة (بغض النظر عن الكمية)',
'BACK_PRODUCT_IMAGE'   				=> 	'صورة المنتج',
'BACK_PRODUCT_DATE'   				=> 	'تاريخ الإضافة',
'BACK_IMAGE_SIZE_MUST_BE'   		=> 	'حجم الصورة يجب أن يكون',
'BACK_PIXELS'   					=> 	'بيكسل',
'BACK_S_ADD'   						=> 	'إضافة',
'BACK_ADD_PRODUCT'   				=> 	'إضافة منتج',
'BACK_ENTER_YOUR_PRODUCT_NAME'   	=> 	'أدخل اسم المنتج',
'BACK_ENTER_QTY_OF_PRODUCT'   		=> 	'أدخل كمية المنتج',
'BACK_INCLUDING_TAX_AMOUNT'   		=> 	'يشمل الضرائب',
'BACK_ALLOWED_CHARACTERS'   		=> 	'الحد الاقصى المسموح 250 حرف',
/*-------PRODUCTS  - Add Products  End ------------*/

/*-------PRODUCTS -  Manage Products  ---------------*/
'BACK_MANAGE_PRODUCTS'   			=> 	'إدارة المنتجات',
'BACK_PRODUCT_NAME'   				=> 	'اسم المنتج',
'BACK_ACTIONS'   					=> 	'إجراء',
'BACK_PRODUCT_DETAILS'   			=> 	'تفاصيل المنتج',
'BACK_STORE_NAME'   				=> 	'اسم المتجر',
/*-------PRODUCTS  -  Manage Products  End ------------*/


/*-------PRODUCTS -  Sold Products  ---------------*/
'BACK_VIEW'   						=> 	'عرض',
/*-------PRODUCTS  -  Sold Products  End ------------*/

/*-------PRODUCTS -  Edit Products  ---------------*/
'BACK_EDIT_PRODUCTS'   				=> 	'تعديل المنتجات',
'BACK_SELECT_BRAND'   				=> 	'إختر علامة تجارية',
'BACK_SELECT_SIZE'   				=> 	'إختر المقاس',
'BACK_YOUR_SELECTED_COLOR'   		=> 	'إختر اللون',
'BACK_MAX'   						=> 	'الحد الاقصى',
'BACK_UPDATE_PRODUCT'   			=> 	'تحديث المنتج',
/*-------PRODUCTS  -  Edit Products  End ------------*/

/*-------PRODUCTS -  Shipping Delivery---------------*/
'BACK_SHIPPING_AND_DELIVERY'   		=> 	'الشحن والتوصيلy',
'BACK_ADDRESS'   					=> 	'العنوان',
'BACK_ADDRESS1'   					=> 	'العنوان 1',
'BACK_ADDRESS2'   					=> 	'العنوان 2',
'BACK_DETAILS'   					=> 	'التفاصيل',
'BACK_DELIVERY_STATUS'   			=> 	'حالة التوصيل',
'BACK_BLOCK_LEVEL_HELP'   			=> 	'مثال على نص المساعدة على مستوى المجموعة هنا.',
'BACK_CLOSE'   						=> 	'اغلاق',
'BACK_SEND'   						=> 	'إرسال',
'BACK_TAX_INVOICE'   				=> 	'الفاتورة الضريبية ',
'BACK_AMOUNT_PAID'   				=> 	'المبلغ المدفوع',
'BACK_INCLUSIVE_OF_ALL_CHARGES'   	=> 	'شامل جميع التكاليف',
'BACK_ORDER_DATE'   				=> 	'تاريخ الطلب',
'BACK_SHIPPING_ADDRESS'   			=> 	'عنوان الشحن',
'BACK_INVOICE_DETAILS'   			=> 	'تفاصيل الفاتورة',
'BACK_SHIPMENT_CONTAINS_ITEMS'   	=> 	'هذه الشحنة تحتوي على الاصناف التالية',
'BACK_COLOR'   						=> 	'اللون',
'BACK_SUB_TOTAL'   					=> 	'المجموع الفرعي',
'BACK_SHIPMENT_VALUE'   			=> 	'قيمة الشحن',
'BACK_CANCEL_ORDERS'				=> 	'إلغاء الطلبات',
'BACK_RETURN_ORDERS'				=> 	'إرجاع الطلبات',
'BACK_COD_RETURN_ORDERS'			=> 	'إرجاعطلبات الدفع عند الإستلام',
'BACK_COD_REPLACE_ORDERS'			=> 	'إستبدال طلبات الدفع عند الإستلام',
'BACK_REPLACE_ORDERS'				=> 	'إستبدال الطلبات',
'BACK_CANCELLED'					=> 	'تم إلغاء الطلب',
'BACK_RETURNED'						=> 	'تم إرجاع الطلب',
'BACK_REPALCED'						=> 	'تم إستبدال الطلب',
'BACK_COD'							=> 	'الدفع عند الإستلام',
'BACK_PAYPAL'						=> 	'Paypal',
'BACK_ORDER_ID'						=> 	'رقم الطلب',

/*-------PRODUCTS  - Shipping Delivery End ------------*/

/*-------PRODUCTS -  Cash on Delivery---------------*/
'BACK_CASH_ON_DELIVERY'   			=> 	'الدفع عند الإستلام',
'BACK_ENTER_COUPEN_CODE'   			=> 	'أدخل كود الكوبون',
'STAFF_CHAGES'   			=> 	'رسوم العامل',
/*-------PRODUCTS  - Cash on Delivery End ------------*/

/*-------PRODUCTS -  Manage Product Reviews ---------------*/
'BACK_MANAGE_PRODUCT_REVIEWS'   	=> 	'إدارة تقييمات المنتجات',
'BACK_MANAGE_MERCHANT_REVIEWS'   	=> 	'إدارة تقييمات المتاجر',
'BACK_REVIEW_TITLE'   				=> 	'عنوان التقييم',
'BACK_CUSTOMER_NAME'   				=> 	'اسم العميل',
/*-------PRODUCTS  - Manage Product Reviews End ------------*/

/*-------PRODUCTS -  Edit Review ---------------*/
'BACK_EDIT_REVIEW'   				=> 	'تعديل التقييمات',
'BACK_REVIEW_TITLE_FIELD_IS_EMPTY'  => 	'حقل عنوان التقييم فارغ!',
'BACK_REVIEW_DESCRIPTION_FIELD_IS_EMPTY'  => 	'حقل الوصف فارغ!',
'BACK_REVIEW_DESCRIPTION'   		=> 	'وصف التقييم',
/*-------PRODUCTS  - Edit Review End ------------*/

/*-------PRODUCTS -  Side Bar ---------------*/
'BACK_PRODUCTS'   					=> 	'المنتجات',
'BACK_PRODUCTS_DASHBOARD'   		=> 	'لوحة تحكم المنتجات',
'BACK_SHIPPING_DELIVERY'   			=> 	'الشحن والتوصيل',
/*-------PRODUCTS  - Side Bar End ------------*/

/*-------SETTINGS -  Edit Coupon ---------------*/
'BACK_EDIT_COUPON'   				=> 	'تعديل الكوبون',
'BACK_VALUE'   						=> 	'القيمة',
'BACK_START_DATE'   				=> 	'تاريخ البدء',
'BACK_END_DATE'   					=> 	'تاريخ الانتهاء',
'BACK_TERMS_CONDITION'   			=> 	'الشروط والاحكام',
'BACK_CHOOSEN_CITY'   				=> 	'المدينة المختارة',
/*-------SETTINGS  - Edit Coupon End ------------*/

/*-------SETTINGS -  Side bar ---------------*/
'BACK_SERVICE_TYPE_MANAGEMENT'   	=> 	'إدارة نوع الخدمات',
/*-------SETTINGS  - Side bar End ------------*/

/*-------TOP HEADER -  Admin profile ---------------*/
'BACK_ADMIN_PROFILE'   				=> 	'الملف الشخصي للمدير',
'BACK_BACK'   						=> 	'رجوع',
/*-------TOP HEADER  - Admin profile End ------------*/

/*-------TOP HEADER -  Admin Settings ---------------*/
'BACK_ADMIN_SETTINGS'   			=> 	'اعدادت المدير',
'BACK_OLD_PASSWORD'   				=> 	'كلمة المرور القديمة',
'BACK_NEW_PASSWORD'   				=> 	'كلمة المرور الجديدة',
'BACK_ADMIN'   						=> 	'المدير',
/*-------TOP HEADER  - Admin Settings End ------------*/


/*-------LOGIN -  Admin Settings ---------------*/
'BACK_ADMIN_LOGIN'   				=> 	'صفحة تسجيل الدخول',
'BACK_C_ADMIN_LOGIN'   				=> 	'تسجيل دخول المدير',
'BACK_SIGN_IN'   					=> 	'تسجيل الدخول',
'BACK_ENTER_VALID_EMAIL'   			=> 	'أدخل بريدك الإلكتروني الصحيح',
'BACK_RECOVER_PASSWORD'   			=> 	'إعادة تعيين كلمة المرور',
'BACK_FILL_DETAILS_TO_REGISTER'   	=> 	'الرجاء ملء البيانات للتسجيل',
'BACK_REGISTER'   					=> 	'تسجيل',
'BACK_BACK_TO_LOGIN'   				=> 	'العودة الى صفحة الدخول',
'BACK_FORGOT_PASSWORD'   			=> 	'نسيت كلمة المرور',
'BACK_YOUR_EMAIL'   				=> 	'بريدك الإلكتروني',
'EMAIL_SUCESS'   				=> 	'تم إرسال بريد إلكتروني بنجاح',
/*-------LOGIN  - Admin Settings End ------------*/

/*-------DEALS -  Deals Dashboard-----------------*/
'BACK_DEALS_DASHBOARD'   			=> 	'لوحة تحكم الصفقات',
'BACK_TOTAL_DEALS_COUNT'   			=> 	'عدد جميع الصفقات',
'BACK_MARKET_RESPONSE_RATE'   		=> 	'نعدل استجابة السوق',
'BACK_EXPIRED_DEALS_COUNT'   		=> 	'عدد الصفقات المنتهية',
'BACK_DEALS_TRANSACTION_DETAILS'   	=> 	'تفاصيل المعاملات',
'BACK_LAST_ONE_YEAR'   				=> 	'آخر سنة',
/*-------DEALS  -Deals Dashboard End-------------*/


/*-------DEALS -  Add Deals -----------------*/
'BACK_ADD_DEALS'   					=> 	'إضافة صفقات',
'BACK_USER_LIMIT'   				=> 	'حد المستخدم',
'BACK_DEAL_IMAGE'   				=> 	'صورة الصفقة',

'BACK_ENTER_DEAL_TITLE'   			=> 	'أدخل عنوان الصفقة',
'BACK_ENTER_START_DATE'   			=> 	'أدخل تاريخ البدء',
'BACK_ENTER_END_DATE'   			=> 	'أدخل تاريخ الانتهاء',
'BACK_ENTER_META_KEYWORDS'   		=> 	'أدخل الكلمات الوصفية',
'BACK_ENTER_META_DESCRIPTION'   	=> 	'أدخل وصف الكلمات الوصفية',
/*-------DEALS  -Add Deals  End-------------*/

/*-------DEALS -   Side bar -----------------*/
'BACK_DEALS'   						=> 	'الصفقات',		
/*-------DEALS  - Side bar  End-------------*/

/*-------DEALS -   Edit deals -----------------*/
'BACK_EDIT_DEALS'   				=> 	'تعديل الصفقات',	
'BACK_UPDATE_DEALS'   				=> 	'تحديث الصفقات',	
/*-------DEALS  - Edit deals  End-------------*/



/*-------Deals -  Side Bar ---------------*/
'BACK_MANAGE_REVIEWS'   			=> 	'إدارة القييمات ',
'BACK_MANAGE_DEAL_REVIEWS'   		=> 	'إدارة تقييمات الصفقات',
'BACK_CLICK_FOR_PREVIOUS_MONTHS'   => 	'انقر لعرض الشهر الماضي',
'BACK_CLICK_FOR_NEXT_MONTHS'   	=> 	'انقر لعرض الشهر التالي',
/*-------Deals  - Side Bar End ------------*/

/*-------Deals -  Shipping and delivery ---------------*/
'BACK_LARAVEL_ECOMMERCE_MULTIVENDOR_SHIPPING_DELIVERY'  => 	'الشحن والتوصيل',
'BACK_SHIPPING_DELIVERY'   			=> 	'الشحن والتوصيل',
'BACK_PRODUCTS_NAME'   				=> 	'اسم الصفقات',
'BACK_CANCELLED_ORDER'   			=> 	'إلغاء الطلبات',
'BACK_PAYUMONEY_SHIPPING_DELIVERY'  => 	'PayUmoney الشحن والتوصيل',

/*-------Deals  - Shipping and delivery End ------------*/

/*-------Deals -  expired_deals.blade ---------------*/
'BACK_MANAGE_DEALS'   					=> 	'إدارة الصفقات',
'BACK_DEALS_NAME'   					=> 	'اسم الصفقة',
'BACK_SAVINGS'   						=> 	'توفير',
'BACK_BACK'   							=> 	'رجوع',
'BACK_MINIMUM_DEAL_LIMIT'   			=> 	'الحد الأدنى للصفقة',
'BACK_DEALS_PRICE'   					=> 	'سعر الصفقة',
'BACK_SHOP_NAME'   						=> 	'اسم المتجر',
'BACK_DEALS_IMAGE'   					=> 	'صور الصفقة',
'BACK_PURCHASE_COUNT'   				=> 	'عدد الشراء',
'BACK_MAXIMUM_DEAL_LIMIT'   			=> 	'الحد الأعلى للصفقة',
'BACK_DISCOUNT_PRICE'   				=> 	'سعر الخصم',
'BACK_DEAL_IMAGE'   					=> 	'صورة الصفقة',
'BACK_DISCOUNT_PERCENTAGE'   			=> 	'نسبة الخصم',
'BACK_DEAL_DESCRIPTION'   				=> 	'وصف الصفقة',
'BACK_DEAL_DETAILS'   					=> 	'تفاصيل الصفقة',
'BACK_DEAL_ACTION'						=>  'إجراء',
'BACK_DEAL_IS_INPROGRESS'   			=> 	'الصفقة قيد التقدم',
'BACK_OPEN'   							=> 	'مفتوح',
/*-------Deals  - expired_deals.blader End ------------*/


######################################## Controller work #######################################################
/*Start Controller*/
/*Start AdController.php*/
'BACK_AD_TITLE_ALREADY_EXISTS'     					=> 		'عنوان الإعلان موجود مسبقاً',
'BACK_ERROR_:_IMAGE_SIZE_MUST_BE_380_X_215_PIXELS'  => 		'خطأ، حجم الصورة يجب أن يكون  380 x 250 pixels',
'BACK_IMAGE_FIELD_REQUIRED'     				 	=> 		'حقل الصورة مطلوب',
/*End AdController.php*/
/*Start AdminController.php*/
'BACK_NULL'     									=> 		'فارغ',
'BACK_LOGIN_SUCCESS'     							=> 		'تم تسجيل الدخول بنجاح',
'BACK_MAIL_SEND_SUCCESSFULLY'     					=> 		'تم إرسال الرسالة بنجاح',
'BACK_INVALID_EMAIL'     							=> 		'البريد الإلكتروني غير صحيح',
'BACK_LOGOUT_SUCCESS'     							=> 		'تم تسجيل الخروج بنجاح',
'BACK_PASSWORD_RECOVERY_DETAILS'     				=> 		'تفاصيل استعادة كلمة المرور',
'BACK_INVALID_USERNAME_AND_PASSWORD'     			=> 		'اسم المستخدم وكلمة المرور غير صحيحة',

/*End AdminController.php*/

/*Start MerchantController.php*/

'BACK_MERCHANT' 									=> 		'التاجر',
'BACK_SELECT' 										=> 		'اختيار',
'BACK_NO_DATAS_FOUND' 								=> 		'لا توجد بيانات',
'BACK_MERCHANT_EMAIL_EXIST' 						=> 		'بريد التاجر موجود',
'BACK_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY' 		=> 		'تم إنشاء حساب التاجر بنجاح',
'BACK_RECORD_ADDED_SUCCESSFULLY' 					=> 		'تم إضافة السجل بنجاح',
'BACK_MERCHANT_ACTIVATED_SUCCESSFULLY' 				=> 		'تم تفعيل التاجر بنجاح',
'BACK_MERCHANT_BLOCKED_SUCCESSFULLY' 				=> 		'تم حظر التاجر بنجاح',
'BACK_MERCHANT_PRODUCT_ACTIVATED_SUCCESSFULLY' 		=> 		'تم تفعيل منتجات التاجر بنجاح',
'BACK_MERCHANT_PRODUCT_BLOCKED_SUCCESSFULLY' 		=> 		'تم حظر منتجات التاجر بنجاح',
'BACK_STORE_ACTIVATED_SUCCESSFULLY' 				=> 		'تم تفعيل المتجر بنجاح',
'BACK_STORE_BLOCKED_SUCCESSFULLY' 					=> 		'تم حظر المتجر بنجاح',
'BACK_MERCHANT_EMAIL_ID_ALREADY_EXISTS' 			=> 		'البريد الإلكتروني موجود مسبقاً',
'BACK_MERCHANT_EMAIL_ID_ALREADY_EXISTS_BRANCH' 			=>  'بريد المدير موجود مسبقاً',


/*End MerchantController.php*/

/*Start AdvertisementController.php*/

'BACK_ALREADY_EXIST_FOR_THIS_SUB_CATEGORY'     		=> 		'موجود مسبقاً لهذا القسم الفرعي',
'BACK_INSERTION_FAILED_DUE_TO_SOME_ERROR'     		=> 		'فشل الادخال بسبب بعض الأخطاء',
'BACK_IMAGE_REMOVED_SUCCESSFULLY'     				=> 		'تم حذف الصورة بنجاح',
'BACK_ALREADY_EXIST_FOR_THIS_CATEGORY'     			=> 		'موجود مسبقاً لهذا القسم',



/*End AdvertisementController.php*/
/*Start AttributeController.php*/

'BACK_SIZE_NAME_EXIST'     							=> 		'المقاس موجود',


/*End AttributeController.php*/
/*Start BannerController.php*/

'BACK_ERROR_:_IMAGE_SIZE_MUST_BE_870_X_350_PIXELS' 	=> 		'خطأ: حجم الصورة يجب أن يكون  870 x 350 pixels',
'BACK_ERROR_:_IMAGE_SIZE_MUST_BE_380_X_250_PIXELS'  => 		'خطأ: حجم الصورة يجب أن يكون  380 x 250 pixels',



/*End BannerController.php*/
/*Start BlogController.php*/

'BACK_BLOG' 										=> 		'المدونة',
'BACK_COMMENTS_APPROVED_SUCCESSFULLY' 				=> 		'تمت الموافقة على التعليقات بنجاح',
'BACK_COMMENTS_UNAPPROVED_SUCCESSFULLY' 			=> 		'تم رفض التعليقات بنجاح',
'BACK_REPLY_SAVED_SUCCESSFULLY' 					=> 		'تم حفظ الرد بنجاح',
'BACK_ALREADY_BLOG_TITLE_EXISTS' 					=> 		'عنوان المدونة موجود مسبقاً',
'BACK_PLEASE_UPLOAD_BLOG_IMAGE' 					=> 		'يرجى تحميل صورة للمدونة',

/*End BlogController.php*/
/*Start BrandController.php*/


/*End BrandController.php*/
/*Start CategoryBannerController.php*/



/*End CategoryBannerController.php*/
/*Start CategoryController.php*/

'BACK_THIS_CATEGORY_NAME_ALREADY_EXISTS' 			=> 		'اسم القسم موجود مسبقاً',

/*End CategoryController.php*/
/*Start CityController.php*/

'BACK_CITIES_DELETED_SUCCESSFULLY' 					=> 		'تم حذف المدن بنجاح',
'BACK_CITY_ALREADY_EXISTS' 							=> 		'المدينة موجودة بالفعل',

/*End CityController.php*/
/*Start CmsController.php*/

'BACK_TITLE_ALREADY_EXIST' 							=> 		'العنوان موجود بالفعل',
'BACK_PAGE_ACTIVATED' 								=> 		'تم تفعيل الصفحة',
'BACK_PAGE_BLOCKED' 								=> 		'تم حظر الصفحة',

/*End CmsController.php*/
/*Start CountryController.php*/

'BACK_COUNTRY_DELETED_SUCCESSFULLY' 				=> 		'تم حذف الدولة بنجاح',
'BACK_COUNTRY_ALREADY_EXISTS' 						=> 		'الدولة موجودة بالفعل',
'BACK_COUNTRY_CODE_ALREADY_EXISTS' 					=> 		'كود الدولة موجود بالفعل',


/*End CountryController.php*/
/*Start CouponController.php*/

'BACK_COUPON_CODE_ALREADY_AVAILABLE_FOR_THAT_PRODUCT' => 	'كود الكوبون موجود مسبقاً لهذا المنتج',
'BACK_COUPON_CODE_PRODUCT_NOT_EXISTS' 				=> 		'يرجى اختيار اسم منتج صحيح',
'BACK_NEW_COUPON_CODE_AVAILABLE' 					=> 		'كود الكوبون الجديد متاح!!',
'BACK_NOT_ADDED_CHECK_COUPON_WITH_DATES' 			=> 		'لم يتم الإضافة. تحقق من الكوبون مع التاريخ',
'BACK_THIS_COUPON_CODE_ONLY_FOR_YOU' 				=> 		'هذا الكوبون لك فقط!!',
'BACK_THE_COUPON_ALREADY_EXIST' 					=> 		'الكوبون موجود مسبقاً',
'BACK_COUPON_DELETED_SUCCESSFULLY' 					=> 		'تم حذف الكوبون بنجاح',
'BACK_COUPON_IS_ACTIVATED' 							=> 		'الكوبون مفعل. لا يمكنك حذف الكوبون. ولكن يمكنك تغيير تاريخ الانتهاء.',

/*End CouponController.php*/
/*Start CustomerController.php*/

'BACK_CUSTOMER' 									=> 		'العميل',
'BACK_EMAIL_SUCCESSFULLY_SENT' 						=> 		'تم إرسال البريد الإلكتروني بنجاح',
'BACK_ALREADY_EMAILID_EXISTS' 						=> 		'البريد الإلكتروني موجود مسبقاً',
'BACK_YOUR_ACCOUNT_WAS_CREATED_SUCCESSFULLY' 		=> 		'تم إنشاء حسابك بنجاح',
'BACK_CUSTOMER_HAS_BEEN_CREATED_SUCCESSFULLY' 		=> 		'تم إنشاء حساب العميل بنجاح',

/*End CustomerController.php*/
/*Start CustomerprofileController.php*/



/*End CustomerprofileController.php*/

/*Start DealsController.php*/

'BACK_DEALS' 										=> 		'الصفقات',
'BACK_DEAL_INSERTED_SUCCESSFULLY' 					=> 		'تم إضافة الصفقة بنجاح',
'BACK_DEAL_UPDATED_SUCCESSFULLY' 					=> 		'تم تحديث الصفقة بنجاح',
'BACK_SELECT' 										=> 		'اختيار',
'BACK_DEAL_ACTIVATED' 								=> 		'تم إلغاء حظر الصفقة بنجاح',
'BACK_DEAL_BLOCKED' 								=> 		'تم حظر الصفقة بنجاح',
'BACK_DEAL_DELETED_SUCCESSFULLY' 					=> 		'تم حذف الصفقة بنجاح',
'BACK_DEAL_REVIEW_UNBLOCKED' 						=> 		'تم إلغاء حظر التقييم بنجاح',
'BACK_DEAL_REVIEW_BLOCKED' 							=> 		'تم حظر التقييم بنجاح',


/*End DealsController.php*/
/*Start ImagesettingsController.php*/

'BACK_LOGO_UPDATED' 								=> 		'تم تحديث الشعار',
'BACK_FAVICON_UPDATED' 								=> 		'تم تحديث الأيقونة',
'BACK_NOIMAGE_UPDATED' 								=> 		'تم تحديث بلا صورة',
'LOGO' 												=> 		'الشعار',

/*End ImagesettingsController.php*/
/* Start TransactionController.php */
'BACK_PAYMENT_COMPLETED_SUCCESSFULLY '					=> 			'تمت عملية الدفع بنجاح',
'BACK_PAYMENT_CANCELLED'								=> 			'تم إلغاء عملية الدفع',
'BACK_YOU_ALREADY_CHOOSEN_WINNER_FOR_THIS_AUCTION '		=> 			'لقد إخترت الفائز بالفعل في هذا المزاد',
/* close TransactionController.php */

/* Start SpecificationController.php */
'BACK_RECORD_DELETED_SUCCESSFULLY '						=> 			'تم حذف السجل بنجاح',
'BACK_GROUP_NAME_EXIST'									=> 			'اسم المجموعة موجود',
'BACK_SORT_ORDER_EXIST'									=> 			'ترتيب الفرز موجود',
'BACK_RECORD_INSERTED_SUCCESSFULLY'						=> 			'تم إضافة سجل بنجاح',
'BACK_SPECIFICATION_NAME_EXIST'						=> 			'اسم المواصفات موجود',

/* close SpecificationController.php */

/* Start SettingsController.php */
'BACK_SOMETHING_WENT_WRONG'								=> 			'حدث خطأ ما',
'BACK_SUCCESSFULLY_UPDATED'								=> 			'تم التحديث بنجاح',
'BACK_RECORD_BLOCKED_SUCCESSFULLY'						=> 			'تم حظر السجل بنجاح',
'BACK_RECORD_UNBLOCKED_SUCCESSFULLY'					=> 			'تم إلغاء حظر السجل بنجاح',

/* close SettingsController.php */

/* Start ServiceTypeController.php */
'BACK_STATUS_UPDATED_SUCCESSFULLY '						=> 			'تم تحديث الحالة بنجاح',

/* close ServiceTypeController.php */

/* Start ServiceTimeController.php */
'BACK_TIME_IS_ALREADY_PRESENTS '						=> 			'تم عرض الوقت بالفعل',

/* close ServiceTimeController.php */
/* Start ServiceController.php */
'BACK_SERVICES_BLOCKED'									=> 			'تم حظر الخدمات',
'BACK_DELETED_SUCCESSFULLY'								=> 			'تم الحذف بنجاح',
'BACK_SERVICES_UNBLOCKED'								=> 			'تم إلغاء حظر الخدمات',
'BACK_SERVICES'											=> 			'الخدمات',
'BACK_RECORD_UPDATION_FAILED'							=> 			'فشل تحديث السجل',
'BACK_SERVICE_DELETED_SUCCESSFULLY'						=> 			'تم حذف الخدمة بنجاح',
/* close ServiceController.php */
/* Start Homecontroller.php */
'BACK_PRODUCTS'											=> 			'المنتجات',
'BACK_SERVICES'											=> 			'الخدمات',
'BACK_YOUR_DEAL_STORE_REVIEW_POST_SUCCESSFULLY'			=> 			'تم نشر تعليقك بنجاح',
'BACK_YOUR_DEAL_PRODUCT_REVIEW_POST_SUCCESSFULLY'		=> 			'تم نشر تعليقك عن المنتج بنجاح',
'BACK_YOUR_PRODUCT_REVIEW_POST_SUCCESSFULLY'			=> 			'تم نشر تعليقك عن المنتج بنجاح',
'BACK_YOUR_EMAIL_SUBSCRIBED_SUCCESSFULLY'				=> 			'تم تسجيل بريدك الإلكتروني بنجاح',
'BACK_EMAIL_HAS_BEEN_SUBSCRIPTION_SUCCESSFULLY'			=> 			'تم تسجيل بريدك الإلكتروني بنجاح',
'BACK_ALREADY_USE_EMAIL_EXIST'							=> 			'هذا البريد مستخدم من قبل',
'BACK_REGISTERED_SUCCESSFULLY'							=> 			'تم التسجيل بنجاح',
'BACK_REGISTER_ACCOUNT_CREATED_SUCCESSFULLY'			=> 			'تم إنشاء الحساب بنجاح',
'BACK_ALREADY_EMAIL_EXIST'								=> 			'البريد الإلكتروني مسجل مسبقاً',
'BACK_ERROR_ALREADY_AUCTION_HAS_BID_TRY_WITH_NEW_AMOUNT!'					=> 			'خطأ!  تم عرض القيمة مسبقاً. حأول بقيمة جديدة!',
'BACK_YOUR_PAYMENT_HAS_BEEN_CANCELLED'					=> 			'تم إلغاء عملية الدفع',
'BACK_SOME_ERROR_OCCURED_DURING_PAYMENT'				=> 			'حدث خطأ ما أثناء عملية الدفع',
'BACK_YOUR_PAYMENT_HAS_BEEN_FAILED'						=> 			'فشلت عملية الدفع',
'BACK_PAYMENT_ACKNOWLEDGEMENT'							=> 			'إشعار الدفع .....',
'BACK_YOUR_PAYMENT_SUCCESSFULLY_COMPLETED'				=> 			'عملية الدفع اكتملت بنجاح .....',
'BACK_YOUR_PAYMENT_HAS_BEEN_COMPLETED_SUCCESSFULLY'		=> 			'لقد اكتمل الدفع الخاص بك بنجاح',
'BACK_PAYMENT_RECEIVED_YOUR_PRODUCT_WILL_BE_SENT_TO_YOU_VERY_SOON'					=> 			'تم إستلام دفعتك! سيتم إرسال المنتج لك قريبا!',
'BACK_HI_MERCHANT_YOUR_PRODUCT_PURCHASED'				=> 			'مرحبًا عزيزي التاجر! تم شراء منتجك!!',
'BACK_COD_ACKNOWLEDGEMENT'								=> 			'إشعار الدفع عند الإستلام .....',
'BACK_YOUR_COD_SUCCESSFULLY_REGISTERED'					=> 			'الدفع عند الإستلام مسجل بنجاح.....',
'BACK_NO_RESULT_FOUND'									=> 			'لم يتم العثور على نتائج..',
'BACK_YOUR_COD_PAYMENT_IS_SUCCESS'						=> 			'تم بنجاح الدفع عند الإستلام',
/* Start Homecontroller.php */

/*Admin User Management */
'BACK_USER_MANAGEMENT'						=> 	'إدارة المستخدمين',
'BACK_USER_MANAGEMENT_ID'					=> 	'رقم المستخدم',
'BACK_USER_MANAGEMENT_NAME'					=> 	'الاسم',
'BACK_USER_MANAGEMENT_EMAIL'				=> 	'البريد الإلكتروني',
'BACK_USER_MANAGEMENT_PHONE'				=> 	'رقم الجوال',
'BACK_USER_MANAGEMENT_NEW_PASSWORD'				=> 	'كلمة المرور الجديدة',
'BACK_USER_MANAGEMENT_CONFIRM_PASSWORD'				=> 	'تأكيد كلمة المرور',
'BACK_USER_MANAGEMENT_LAST_LOGGED'			=> 	'آخر تسجيل دخول',
'BACK_USER_MANAGEMENT_WALLET_AMT'			=> 	'رصيد المحفظة',
'BACK_USER_MANAGEMENT_STATUS'			    => 	'الحالة',
'BACK_USER_MANAGEMENT_ACTION'	     		=> 	'إجراء',
//static content management
'BACK_USER_MANAGEMENT_LAST_EDITED_ON'       => 'آخر تعديل في',
'BACK_USER_MANAGEMENT_PAGE_NAME'            => 'اسم الصفحة',
'BACK_USER_MANAGEMENT_MESSAGE'              => 'رسالة',
'BACK_USER_MANAGEMENT_SENT_ON'              => 'تم الإرسال في',
//sub admin 
'BACK_SUB_ADMIN_MANAGEMENT'	     			=> 	'إدارة المدراء الفرعيين',
'BACK_SUB_ADMIN'	     					=> 	'المدير الفرعي',
//Acess level management
'BACK_ACCESS_LEVEL_MANAGEMENT'	     		=> 	'إدارة صلاحيات الوصول',
//Sales rep management
'BACK_SALES_REP_MANAGEMENT'	     			=> 	'إدارة العملاء المحتملين',
//Vendors management
'BACK_VENDORS_MANAGEMENT'	     			=> 	'إدارة مزودين الخدمات',

//Payment & Transaction
'BACK_PAYMENT_TRANSACTION'	     			=> 	'الدفع والمعاملات',

//Static Content Management
'BACK_STATIC_CONTENT_MANAGEMENT'	     	=> 	'إدارة المحتوى الثابت',
//Electronic Invitation
'BACK_ELECTRONIC_INVITATION'	     		=> 	'الدعوات الإلكترونية',
//Review & Rating
'BACK_REVIEW_RATING'	     				=> 	'التقييم والتعليقات',
//Subscription Management
'BACK_SUBSCRIPTION_MANAGEMENT'	     		=> 	'رسوم الاشتراك و العمولات',
//Occasion
'BACK_OCCASION'	     						=> 	'المناسبات',
//Delivery management
'BACK_DELIVERY_MANAGEMENT'	     			=> 	'إدارة التوصيل',
//sub admin list role
'BACK_SUBADMIN_ROLE'	     				=> 	'الادوار',

'BACK_USER_MANAGEMENT' 						=> 	'إدارة المستخدمين',

'BACK_SUBMIT' 								=>'تطبيق',
'BACK_RESET' 								=>'إعادة تعيين',
'BACK_DOWNLOAD_CSV' 						=>'تنزيل ملف CSV',
'BACK_ADD_A_NEW_USER' 						=>'إضافة مستخدم جديد',
'BACK_PLEASE_SELECT_STATUS' 				=>'الرجاء اختيار حالة',
'BACK_ACTIVE' 								=>'نشط',
'BACK_IN_ACTIVE' 							=>'غير نشط',
'BACK_PLEASE_SELECT_STATUS' 				=>'الرجاء اختيار حالة',
'BACK_USER_STATUS_DETAILS' 					=>'تفاصيل حالة المستخدم',
'BACK_FULL_NAME' 							=>'الاسم الكامل',
'BACK_E-MAIL_ADDRESS' 						=>'البريد الإلكتروني',
'BACK_PHONE_NO' 							=>'رقم الجوال',
'BACK_AGE' 									=>'العمر',
'BACK_DATE_OF_BIRTH' 						=>'تاريخ الميلاد',
'BACK_LOCATION' 							=>'الموقع',
'BACK_LAST_LOGGED_IN' 						=>'آخر تسجيل دخول في',
'BACK_WALLET_AMOUNT' 						=>'رصيد المحفظة',
'BACK_STATUS' 								=>'الحالة',
'BACK_ADMIN_BACK' 							=>'رجوع',
'BACK_ADD_USER'								=>'إضافة مستخدم',
'BACK_NAME' 								=>'الاسم',
'BACK_EMAIL' 								=>'البريد الإلكتروني',
'BACK_PHONE_NO' 							=>'رقم الجوال',
'BACK_GENDER' 								=>'الجنس',
'BACK_DATE_OF_BIRTH' 						=>'تاريخ الميلاد',
'BACK_ADDRESS' 								=>'العنوان',
'BACK_COUNTRY' 								=>'الدولة',
'BACK_CITY' 								=>'المدينة',
'BACK_IMAGE' 								=>'الصورة',
'BACK_MALE' 								=>'ذكر',
'BACK_FEMALE' 								=>'انثى',
'BACK_DESCRIPTION' 							=>'الوصف',
'BACK_BROWSE' 								=>'استعراض',
'BACK_NO_FILES_SELECTED' 					=>'لم يتم اختيار ملف',
'BACK_CANCEL' 								=>'إلغاء',
'BACK_EDIT_USER' 							=>'تعديل المستخدم',
'BACK_SUB_ADMIN_LIST' 						=>'قائمة المدراء الفرعيين',
'BACK_ADD_A_NEW_SUB_ADMIN' 					=>'إضافة مدير فرعي',
'BACK_ADD_SUB_ADMIN' 						=>'إضافة مدير فرعي',
'BACK_EDIT_SUB_ADMIN' 						=>'تعديل مدير فرعي',
'BACK_FIRST_NAME' 							=>'الاسم الأول',
'BACK_LAST_NAME' 							=>'الاسم الأخير',
'BACK_SEARCH_BY_ROLE' 						=>'بحث بواسطة الدور',
'BACK_SUB_ADMIN_STATUS' 					=>'تفاصيل حالة المدير الفرعي',
'BACK_STATIC_CONTENT_MANAGEMENT' 			=>'إدارة المحتوى الثابت',
'BACK_BROADCAST_MESSAGE' 					=>'رسالة جماعية',
'BACK_APPLY' 								=>'تطبيق',
'BACK_RESET' 								=>'إعادة تعيين',
'BACK_ADD_A_NEW_PAGE' 						=>'إضافة صفحة جديدة',
'BACK_CMS_DETAIL_PAGE' 		                =>'صفحة تفاصيل المحتوى الثابت',
'BACK_BROADCAST_LIST' 						=>'قائمة الرسائل الجماعية',
'BACK_NEW_MESSAGE' 							=>'رسالة جديدة',
'BACK_MESSAGE' 								=>'رسالة',
'BACK_PERSON_SELECT' 						=>'إختر شخص',
'BACK_OCCASION' 							=>'المناسبات',
'BACK_VENUE'								=>'مكان المناسبة',
'BACK_DATE_OF_OCCASION' 					=>'تاريخ المناسبة',
'BACK_DESCRIPTION' 							=>'الوصف',
'BACK_HASHTAGS' 							=>'هاشتاق',
'BACK_IMAGES' 								=>'الصور',
'BACK_OCCASION_DETAIL' 						=>'تفاصيل المناسبة',
'BACK_ADD_ROLE' 							=>'إضافة دور',
'BACK_ROLE_NAME' 							=>'اسم الدور',
'BACK_TOTAL_ACCESS' 						=>'عدد الاشخاص',
'BACK_TOTAL_USER' 							=>'عدد المستخدمين',
'BACK_ACCESS' 								=>'الوصول',
'BACK_EDIT_ROLE' 							=>'تعديل دور',
'BACK_VIEW_ROLE' 							=>'تفاصيل دور',
'BACK_RATING' 								=>'التقييم',
'BACK_GIVEN_BY' 							=>'بواسطة',
'BACK_GIVEN_TO' 							=>'الى',
'BACK_GIVEN_FOR' 							=>'تقييم لـ',
'BACK_DATE_TIME' 							=>'التاريخ',
'BACK_REVIEWS' 								=>'التعليق',
'BACK_SELECT_RATING' 						=>'إختر تقييم',
'BACK_SELECT_VENDOR' 						=>'إختر مندوب مبيعات',
'BACK_SELECT_CUSTOMER'						=>'إختر عميل',
'OCCASION_TYPE'								=>'نوع المناسبة',
'BACK_DESIGNER_CARD'						=>'بطاقة الدعوة',
'BACK_NO_PACKAGE'							=>'المدعوين',
'BACK_BOOKING_DATE'							=>'تاريخ الحجز',
'BACK_INVITATION_BOOK_ID'					=>'رقم حجز الدعوة',
'BACK_INVITATION_BOOKED_BY'					=>'تم الحجز بواسطة',
'BACK_INVITATION_BOOKED_ON'					=>'تم الحجز في',
'BACK_INVITATION_MODE'						=>'طريقة الدعوة',
'BACK_INVITATION_SELECTED_DESIGN'			=>'البطاقة المختارة',
'BACK_INVITATION_VENU'			 			=>'مكان المناسبة',
'BACK_INVITATION_MESSAGE'			 		=>'رسالة الدعوة',
'BACK_INVITATION_BOOKING_AMOUNT'			=>'المبلغ',
'BACK_INVITATION_DATE_OF_OCCASION'			=>'تاريخ المناسبة',
'BACK_INVITATION_LIST'						=>'قائمة المدعوين',
'BACK_INVITATION_STATUS'					=>'الحالة',
'BACK_INVITATION_UPDATE'					=>'تحديث',
'BACK_INVITATION_CANSEL'					=>'إلغاء',
'BACK_INVITATION_CONTACT_NUMBER'			=>'رقم الجوال',
'BACK_INVITATION_ASSIGN_VENDOR'				=>'اسناد الى مزود خدمة',
'BACK_RECORD_NOT_FOUND'	     		   		=>'لا توجد سجلات !',
'BACK_RECORD_NOT_FOUND_USER'				=>'لم يتم تقديم اي سجلات من قبل المستخدم!',
'BACK_DELIVERY_LIST'	    				=>'إدارة التوصيل',
'BACK_NEW_MEMBER'	    	    			=>'إضافة عضو جديد',
'BACK_EDIT_MEMBER'	    	    			=>'تعديل عضو',
'BACK_STATUS_MEMBER'	    				=>'حالة عضو التوصيل',
'BACK_PAYMENT_TYPE'						=>  'نوع الدفع',
'BACK_SOURCE_ACCOUNT_HOLDER_NAME'	=>  'اسم حساب المصدر',
'BACK_DESTINATION_ACCOUNT_HOLDER_NAME' =>  'اسم حساب الوجهه',

/* Start MerchantSettingsController.php */
'BACK_SPECIAL_PRODUCTS'									=> 			'المنتجات الخاصة',
'BACK_ONLY_3_PRODUCTS_ALLOWED_TO_COMPARE_CLEAR_COMPARE_LIST'					=> 			'يسمح بـ 3 منتجات فقط للمقارنة ، مسح قائمة المقارنة',
'BACK_YOU_CAN_ONLY_COMPARE_SIMILAR_PRODUCTS_SO_CLEAR_LIST_TO_COMPARE'					=> 			'يمكنك فقط مقارنة منتجات مماثلة ، امسح قائمة للمقارنة',
'BACK_PRODUCT_ADDED_TO_COMPARE'							=> 			'تم إضافة المنتج للمقارنة',
'BACK_THIS_PRODUCT_ALREADY_EXISTS_IN_COMPARE'			=> 			'هذا المنتج موجود بالفعل في قائمة المقارنة',
'BACK_REVIEW_BLOCKED'									=> 			'تم حظر التعليق بنجاح',
'BACK_REVIEW_UNBLOCKED'									=> 			'تم إلغاء حظر التعليق بنجاح',
'BACK_REVIEW_DELETED_SUCCESSFULLY'						=> 			'تم حذف التعليق بنجاح',
'BACK_RECORD_ACTIVATED_SUCCESSFULLY'					=> 			'تم تنشيط السجل بنجاح',
'BACK_RECORD_UPDATED_SUCCESSFULLY'						=> 			'تم تحديث السجل بنجاح',
'BACK_THE_RANGE_OVERLAPS'								=> 			'المدى يتداخل',
'BACK_END_CODE_ALREADY_EXIST'							=> 			'كود الانتهاء موجود مسبقاً',
'BACK_START_CODE_ALREADY_EXIST'							=> 			'كود البدء موجود مسبقاً',
'BACK_PRODUCT_BLOCKED'									=> 			'تم حظر المنتج',
'BACK_PRODUCT_UNBLOCKED'								=> 			'تم إلغاء حظر المنتج',
'BACK_SELECT_STORE'										=> 			'إختر متجر',
'BACK_NO_DATAS_FOUND'									=> 			'لم يتم العثور على بيانات',
'BACK_SELECT_SECOND_SUB_CATEGORY'						=> 			'إختر قسم فرعي ثاني',
'BACK_SELECT_SUB_CATEGORY'								=> 			'إختر القسم الفرعي',
'BACK_NO_LIST_AVAILABLE_IN_THE_CATEGORY'				=> 			'لا توجد قائمة في هذا القسم',
'BACK_SELECT_MAIN_CATEGORY'								=> 			'إختر القسم الرئيسي',
'BACK_PRODUCT_UPDATED_SUCCESSFULLY'						=> 			'تم تحديث المنتج بنجاح',
'BACK_THE_PRODUCT_ALREADY_EXIST_IN_THE_STORE'			=> 			'المنتج موجود في المتجر بالفعل',
'BACK_PRODUCT_DELETED_SUCCESSFULLY'						=> 			'تم حذف المنتج بنجاح',
'BACK_RESET_LINK_SEND_SUCCESS'	=>'تم إرسال رابط تعيين كلمة المرور الى بريدك الإلكتروني ',
'BACK_RESET_ALREADY_LINK_SEND_SUCCESS'	=>'تم بالفعل إرسال رابط إعادة تعيين كلمة المرور الى بريدك الإلكتروني المسجل لدينا!',
'BACK_RESET_PASSWORD'			=>'إعادة تعيين كلمة المرور',
'BACK_CONFIRM_PASSWORD'			=>'تأكيد كلمة المرور',
'BACK_SUBMIT_PASSWORD'			=>'حفظ',
'BACK_USER_NOT_EXIT'			=>'المستخدم غير موجود!',
'BACK_CONFIRM_PASSWORD_NOT_MATCH' =>'كلمة المرور وتأكيد كلمة المرور لا يتطابقان!',
'BACK_PASSWORD_SUCCESSFULLY_UPDATE' =>'تم تحديث كلمة المرور ',
'BACK_PASSWORD_LINK_EXP' 			=>'انتهت صلاحية الرابط',
'BACK_THIS_EMAIL_ALREADY_REGISTER'  => 'هذا البريد الإلكتروني مسجل بالفعل مع مدير آخر',
'BACK_ENTER_EMAIL' 					=> 'عنوان البريد الإلكتروني',
'BACK_PASSWORD' 						=> 'كلمة المرور',
'BACK_SELECT_TYPE'					=>  'إختر نوع',
'BACK_TRANSACTION_TYPE'				=>  'رقم المعاملة',
'BACK_PAYER_TYPE'					=>  'اسم المشتري',
'BACK_PAYMENT_AGAINST'				=>  'الدفع مقابل طلب',
'BACK_AMOUNT'						=>  'القيمة',
'BACK_ASSOCIATED_VENDOR'			=>  'مزود الخدمة',
'BACK_PAYMENT_MODE'					=>  'طريقة الدفع',
'BACK_CREDITED'						=>  'دائن',
'BACK_DEBITED'						=>  'مدين',
'BACK_HALL_INSURANCE' 				=>  'التأمين',
'BACK_VENDOR_NAME' 					=>  'اسم مزود الخدمة',
'BACK_CUSTOMER_NAME' 				=>  'اسم العميل',
'BACK_HALL_NAME' 					=>  'اسم القاعة',
'BACK_BRANCH_NAME' 					=>  'اسم الفرع',
'BACK_INSURANCE_AMT' 				=>  'مبلغ التأمين',
'BACK_BOOKING_DATE' 				=>  'تاريخ الحجز',
'BACK_FULL_REFUND' 					=>  'إرجاع بالكامل',
'BACK_PARTIAL_REFUND' 				=>  'إرجاع جزئي',
'BACK_SERVICE_NAME' 				=>  'اسم الخدمة',
'BACK_TYPE' 						=>  'النوع',
'BACK_AMOUNT_PERCENTAGE' 			=>  'القيمة / بالمئة',
'BACK_AMOUNT' 			            =>  'القيمة',
'BACK_PERCENTAGE' 			        =>  'بالمئة',
'BACK_LAST_MODIFIED_ON' 			=>  'آخر تعديل في',
'BACK_SELECT_TYPE' 					=>  'إختر النوع',
'BACK_COMMISSION' 					=>  'عمولة',
'BACK_FEE_BASED' 					=>  'رسوم',
'BACK_ADD_NEW_SUBSCRIPTION' 		=>  'إضافة اشتراك جديد',
'BACK_ADD_SUBSCRIPTION' 			=>  'إضافة اشتراك',
'BACK_DESCRIPTION_FEATURE' 			=>  'الوصف / الخصائص',
'BACK_Hall' 						=>  'القاعات',
'BACK_DATES' 						=>  'التمور والرطب',
'BACK_BUFFET' 						=>  'البوفيهات',
'BACK_SEND_INVITATION' 				=>  'إرسال الدعوات',
'BACK_SELECT_INVITATION_MODE' 				=>  'يرجى اختيار طريقة الدعوة',
'BACK_TEXT_MESSAGE' 				=>  'رسالة نصية',
'BACK_EMAIL' 				=>  'البريد الإلكتروني',
'BACK_DESIGNER_CARD' 				=>  'بطاقة الدعوة',
'BACK_DEDUCT_BALANCE' 				=>  'خصم الرصيد',
'BACK_DEDUCT_BALANCE_1' 				=>  'خصم',


/* close MerchantSettingsController.php */
########################################  END CONTROLLER WORK ################################################

#############################################  START EMAIL TEMPLATE ###########################################
/*Start merchantmail.blade.php*/
'MAIL_EMAIL_TEMPLATE'     							=> 		'قالب البريد الإلكتروني',
'MAIL_HI'     										=> 		'مرحباً ',
'MAIL_YOUR_LOGIN_CREDENTIALS_ARE'     				=> 		'بيانات تسجيل دخولك هي: ',
'MAIL_USER_NAME'     								=> 		'اسم المستخدم',
'MAIL_PASSWORD'     								=> 		'كلمة المرور',
'MAIL_LOGIN_YOUR_ACCOUNT'     						=> 		'سجل الدخول الى حسابك',
'MOBILE_YOUR_MERCHANT_ACCOUNT_WAS_CREATED_SUCCESSFULLY'    => 		'تم إنشاء حساب التاجر الخاص بك بنجاح',

/*End merchantmail.blade.php*/
/*Strat admin_passwordrecoverymail.blade.php*/

'BACK_PASSWORD_RECOVERY_DETAILS_FOR_ADMIN'     		=> 		'تفاصيل استعادة كلمة المرور للمدير',
'BACK_YOUR_LOGIN_CREDENTIALS_ARE'     				=> 		'بيانات تسجيل دخولك هي:',
'BACK_USER_NAME'     								=> 		'اسم المستخدم',
'BACK_PASSWORD'     								=> 		'كلمة المرور',

/*End admin_passwordrecoverymail.blade.php*/
/*start coupon.blade.php*/
'BACK_EMAIL_TEMPLATE'     							=> 		'قالب رسالة البريد الإلكتروني',
'BACK_USE_CODE'     								=> 		'استخدم كود',
'BACK_FROM'     									=> 		'من',
'BACK_TO'     										=> 		'الى',
'BACK_TERMS_&_CONDITIONS' 							=> 		'الشروط والأحكام',
/*End customeraccountcreation.php*/
/*Start customeraccountcreation.php*/
'BACK_HELLO'     									=> 		'مرحباً',
'BACK_THANK_YOU_FOR_REGISTERING'     				=> 		'شكراً للتسجيل معنا',
'BACK_ENJOY_YOUR_SHOPPING'     						=> 		'استمتع بالتسوق معنا',
'BACK_USER_CREDENTIALS'     						=> 		'بيانات تسجيل دخول المستخدم',
'BACK_USERID'     									=> 		'رقم تعريف المستخدم',

/*End customeraccountcreation.php*/
#############################################  END EMAIL TEMPLATE ###########################################	
/* Image Edit success message */
'BACK_IMAGE_UPDATED_SUCCESSFULLY'						=> 		'تم تحديث الصورة بنجاح',

/* DEAL CASH ON Delivery */
'BACK_ASDSA_CALIFORNIA_CANADIAN'						=> 		'Asdsa california candian',
/* Invoice ORder ID */
'BACK_ORDERID'											=> 		'رقم الطلب',
'BACK_PRICE'											=> 		'السعر',
/* no image for category */
'BACK_CATEGORY_IMAGE'									=> 		'صورة القسم',
/* General settings - requeird minimum one payment method */
'BACK_PLEASE_SELECT_MINIMUM_PAYMENT_METHOD'				=> 'يرجى اختيار طريقة واحدة للدفع.',
/* delete category banner */
'BACK_RECORD_DELETED_SUCCESSFULLY'						=> 'تم الحذف بنجاح',
// Merchant Store Edit
'BACK_EDIT_MERCHANT_STORE'								=> 'تعديل متجر مزود الخدمة',

//Commission Listing
'BACK_COMMISSION_LISTING'								=> 'قائمة العمولات',
'BACK_ALL_COMMISSION_LISTING'							=> 'كل العمولات',
'BACK_TOTAL_ORDER_AMOUNT'								=> 'اجمالي قيمة الطلبات',
'BACK_TOTAL_COD_USED_COUPON_AMOUNT'						=> 'اجمالي قيمة الكوبونات',
'BACK_TOTAL_COD_USED_COUPON_AMOUNT_I'						=> 'اجمالي قيمة الكوبون اوفلاين/ اونلاين',
'BACK_COMMSSION_TO_PAY'									=> 'العمولة مستحقة الدفع',
'BACK_COMMSSION_TO_PAY_LIABLE'							=> 'عمولة التاجر المدفوعة بالكامل',

'BACK_COMMSSION_TO_PAID'								=> 'العمولة المدفوعة',

'BACK_COMMSSION_TO_PAID_ENTIRE'								=> 'عمولة التاجر المدفوعة بالكامل ',

'BACK_COMMSSION_BALANCE'								=> 'رصيد العمولات',
'BACK_COMMISSION_PAYMENT'								=> 'حالة الدفع',
'BACK_COMMISSION_PAYMENT'								=> 'حالة الدفع',

'BACK_COMMISSION_NEED_TO_PAY'							=> 'للدفع',
'BACK_COMMISSION_NEED_TO_PAY'							=> 'للدفع',
'BACK_COMMISSION_NEED_TO_PAY_'							=> 'يجب على مزود الخدمة دفع العمولة للإدارة!',
'BACK_COMMISSION_NEED_TO_PAY_PAID'							=> 'تم دفع عمولة مزود الخدمة!',
'BACK_COMMISSION_PAID'									=> 'مدفوع',

//policy applicable to product
'BACK_APPLY_CANCEL'										=>  'تطبيق سياسة الإلغاء',
'BACK_APPLY_RETURN'										=>  'تطبيق سياسة الإرجاع',
'BACK_APPLY_REPLACEMENT'								=>  'تطبيق سياسة الإستبدال',
'BACK_CANCEL_DESCRIPTION'								=>  'سياسة الإلغاء',
'BACK_RETURN_DESCRIPTION'								=>  'سياسة الإرجاع',
'BACK_REPLACE_DESCRIPTION'								=>  'سياسة الإستبدال',
'BACK_ENTER_CANCEL_DESCRIPTION'							=>  'أدخل سياسة الإلغاء',
'BACK_ENTER_RETURN_DESCRIPTION'							=>  'أدخل سياسة الإرجاع',
'BACK_ENTER_REPLACE_DESCRIPTION'						=>  'أدخل سياسة الإستبدال',
'BACK_DAYS_CANCEL_DESCRIPTION'							=>  'عدد أيام الإلغاء',
'BACK_DAYS_RETURN_DESCRIPTION'							=>  'عدد أيام الإرجاع',
'BACK_DAYS_REPLACE_DESCRIPTION'							=>  'عدد أيام الإستبدال',
'BACK_APPROVE_DISAPPROVED_NOTE'							=> 	'سبب الموافقة / الرفض',
'BACK_TRANSACTION '										=> 	'المعاملات',
'BACK_CANCEL_NOTE'										=> 	'سبب الإلغاء',
'BACK_CANCEL_APPLIED_ON'								=> 	'تم الإلغاء في',
'BACK_CANCEL_PROCESSED_ON'								=> 	'تمت معالجة الإلغاء في',
'BACK_CANCEL_STATUS'									=> 	'حالة الإلغاء',
'MER_APPROVED_SUCCESSFULLY'								=> 	'تمت الموافقة بنجاح',
'MER_DISAPPROVED_SUCCESSFULLY'							=> 	'تم الرفض بنجاح',
'MER_RETURN_APPROVE_DISAPPROVED_SUBJECT'				=> 	'اشعار قبول / رفض طلب الإرجاع',
'MER_APPROVE_REPLACEMENT'								=> 	'قبول إستبدال الطلبات',

'BACK_NO_NEWSLETTER_SUBSCRIPTION'						=>	'لا يوجد مشتركين لإرسال رسالة',
'BACK_INACTIVE_DEALS'									=>	'الصفقات الغير نشطة',
'BACK_INACTIVE_PRODUCTS'								=>	'المنتجات الغير نشطة',
'BACK_APPROVE_REPLACEMENT'								=> 	'قبول إستبدال الطلبات',	
'BACK_APPROVE_REPLACEMENT'	=> 'قبول إستبدال الطلبات',        
'BACK_REDIRECT_URL_FORMAT'								=>	'Example:http://www.google.com',

'ADMIN_DELIVERY_STATUS_PRODUCT_PACKED'	=> 'تم تجهيز منتجك وسيتم شحنه قريبًا',
'ORDER_PACKED'	=> 'Order Packed',
'ADMIN_DELIVERY_STATUS_PRODUCT_DISPATCHED'	=> 'تم إرسال منتجك وسيتم تسليمه قريبًا',
'ORDER_DISPATCHED'	=> 'Order Dispatched',
'ADMIN_DELIVERY_STATUS_PRODUCT_DELIVERED'	=> 'تم تسليم المنتج الخاص بك',
'ORDER_DELIVERED'	=> 'Order Delivered',
'ADMIN_DELIVERY_STATUS_PRODUCT_CANCELLED'	=> 'تم إلغاء المنتج الخاص بك بواسطة المسؤول',
'ORDER_CANCELLED'	=> 'Order Cancelled',
'ADMIN_DELIVERY_STATUS_PRODUCT_REPLACED'	=> 'تم إستبدال منتجك',
'ORDER_REPLACED'	=> 'Order Replaced',
'ADMIN_DELIVERY_STATUS_PRODUCT_RETURNED'	=> 'تم إرجاع المنتج الخاص بك',
'ORDER_RETURNED'							=> 'تم إرجاع الطلب',
'DATE'										=> 'التاريخ',
//Coupon email
'BACK_MINIMUM_CART_VALUE'					=> 'الحد الأدنى لاجمالي السلة',
'BACK_COUPON_DETAILS'						=> 'تفاصيل الكوبون',
'BACK_PRODUCT_DETAILS'						=> 'تفاصيل المنتجات',
'BACK_NO_OF_COUPON_PER_PRODUCT'				=> 'عدد الكوبونات لكل منتج',
'BACK_NO_OF_COUPON_PER_USER'				=> 'عدد الكوبونات لكل مستخدم',
'BACK_PRODUCT_QUANTITY_FOR_COUPON'			=> 'كمية المنتج يجب ان تكون ',
'BACK_PRODUCT_LOCATION'			=> 'موقع المنتج ',
'BACK_PRODUCT_PRICE'			=> 'سعر المنتج',
'BACK_NO_TRANSACTION'			=> 'لا توجد معاملات',
'BACK_OFFLINE_COD'			=> 'تم الشراء بالكامل اوفلاين أي الدفع عند الإستلام',
'BACK_ONLINE_ALL_PRO'			=> 'تم الشراء اونلاين أي دفع إلكتروني',

//occasion listing
'BACK_ID'			                                         => 'الرقم',
'BACK_POSTED_BY'			                                 => 'تم النشر بواسطة',	
'BACK_VENUE'			                                     => 'مكان المناسبة',

// Categories management
'BACK_CATEGORIES_MANAGEMENT'                             => 'إدارة الأقسام',
'BACK_CATEGORIES_MANAGEMENT_CATEGORY_LISTING'            => 'قائمة الأقسام',
'BACK_CATEGORIES_MANAGEMENT_EXPORT_AS_CSV'               => 'تصدير كملف CSV',
'BACK_CATEGORIES_MANAGEMENT_ADD_A_NEW_CATEGORY'          => 'إضافة قسم جديد',
'BACK_CATEGORIES_MANAGEMENT_SERVICE_NAME'                => 'اسم الخدمة',
'BACK_CATEGORIES_MANAGEMENT_CATEGORY'                    => 'القطاع',
'BACK_CATEGORIES_MANAGEMENT_ACTIVE_VENDORS'              => 'مزودين الخدمات النشطين',
'BACK_CATEGORIES_MANAGEMENT_ACTION'                      => 'إجراء',
'BACK_CATEGORIES_MANAGEMENT_ADD_CATEGORY_/_SUB-CATEGORY' => 'إضافة قسم رئيسي / فرعي',
'BACK_CATEGORIES_MANAGEMENT_PARENT_CATEGORY'             => 'القسم الرئيسي',
'BACK_CATEGORIES_MANAGEMENT_SUB_CATEGORY'                => 'القسم الفرعي',
'BACK_CATEGORIES_MANAGEMENT_CATEGORY_NAME'               => 'اسم القسم',
'BACK_CATEGORIES_MANAGEMENT_IMAGE'                       => 'الصورة',
'BACK_CATEGORIES_MANAGEMENT_DESCRIPTION'                 => 'الوصف',
'BACK_CATEGORIES_MANAGEMENT_SAVE'                        => 'حفظ',
'BACK_CATEGORIES_MANAGEMENT_CANCEL'                      => 'إلغاء',
'BACK_CATEGORIES_MANAGEMENT_SUB_CATEGORY_NAME'           => 'اسم القسم الفرعي',
'BACK_CATEGORIES_MANAGEMENT_ADD_MORE'                    => 'إضافة المزيد',
'BACK_CATEGORIES_MANAGEMENT_CATEGORY_DETAIL'             => 'تفاصيل الأقسام',
'BACK_CATEGORIES_MANAGEMENT_SUB-CATEGORY_LIST'           => 'قائمة الأقسام الفرعية',
'BACK_CATEGORIES_MANAGEMENT_TOTAL_NO_OF_ACTIVE_VENDOR'   => 'إجمالي عدد مزودين الخدمات النشطين',
'BACK_CATEGORIES_MANAGEMENT_ACTIVE_VENDOR_LIST'          => 'قائمة مزودي الخدمات',
'BACK_CATEGORIES_MANAGEMENT_LIST_OF_ASSOCIATED_VENDORS'  => 'قائمة مزودي خدمات القسم',
'BACK_CATEGORIES_MANAGEMENT_EMAIL'                       => 'البريد الإلكتروني',
'BACK_CATEGORIES_MANAGEMENT_NAME'                        => 'الاسم',
'BACK_CATEGORIES_MANAGEMENT_PHONE_NUMBER'                => 'رقم الجوال',
'BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN'              => 'آخر تسجيل دخول',
'BACK_CATEGORIES_MANAGEMENT_SEARCH_BY_SERVICE_NAME'      => 'البحث حسب اسم الخدمة',
// electronic invitation
'BACK_ELECTRONIC_INVITATION_ADD_NEW_DESIGNER_CARD'	     		    => 	'إضافة تصميم بطاقة جديد',
'BACK_ELECTRONIC_INVITATION_PACKAGE_NAME'	     		            => 	'اسم الباقة',
'BACK_ELECTRONIC_INVITATION_DESIGNER_CARDS'	     		            => 	'بطاقات الدعوات',
'BACK_ELECTRONIC_INVITATION_S_No'	     		                    => 	'ر.م',
'BACK_ELECTRONIC_INVITATION_PRICE'	     		                    => 	'سعر الدعوة',
'BACK_ELECTRONIC_INVITATION_PRICE_DISCOUNT'	     		                    => 	'الخصم لكل دعوة',
'BACK_ELECTRONIC_INVITATION_OCASSION_TYPE'	     		            => 	'نوع المناسبة',
'BACK_ELECTRONIC_INVITATION_LAST_MODIFIED_ON'	     	            => 	'تاريخ آخر تعديل',
'BACK_ELECTRONIC_INVITATION_STATUS '	     		                => 	'الحالة ',
'BACK_ELECTRONIC_INVITATION_ACTION'	     		                    => 	'إجراء',
'BACK_ELECTRONIC_INVITATION_DESIGNER_CARD_LIST'                     => 'قائمة بطاقات الدعوة',
'BACK_ELECTRONIC_INVITATION_SMS_MSG_LIST'  => 'قائمة الرسائل والبريد',
'BACK_ELECTRONIC_INVITATION_EVENT_TYPE'	     		                => 	'نوع المناسبة',
'BACK_ELECTRONIC_INVITATION_EVENT_SUB_TYPE'	     		            => 	'نوع المناسبة الفرعية',
'BACK_ELECTRONIC_INVITATION_DESIGNER_CARD_DETAIL'	     		    => 	'تفاصيل بطاقة الدعوة',
'BACK_ELECTRONIC_SMS_MAIL_CARD_DETAIL'	     		    => 	'تفاصيل الرسائل والبريد',
'BACK_INVITATION_DETAIL'	     		                            =>   'تفاصيل الدعوات',
'BACK_ELECTRONIC_INVITATION_BUSINESS_MEETING'	     		        => 	'إجتماعات الاعمال',
'BACK_ELECTRONIC_INVITATION_WEDDING_&_OTHER_OCASSION'	     	    => 	'الأفراح والمناسبات',
'BACK_SEARCH_BY_NAME'	     		                                =>  'البحث بالاسم',
'BACK_SEARCH_BY_NAME_EMAIL'	     		                            =>  'البحث بالاسم / البريد الإلكتروني',
'BACK_SEARCH_BY_PAGE_NAME'	     		                            =>  'البحث حسب اسم الصفحة ',
'BACK_SELECT_DATE'	     		                                    =>  'إختر تاريخ',
'BACK_SEARCH_OCCASION'	     		                                =>  'البحث حسب المناسبة',    
'BACK_SELECT_ROLE'	     		                                    =>  'إختر دور',
'BACK_NONE_SELECTED'	     		                                =>  'لم يتم الاختيار',
'BACK_TARGET'	     		                                        =>  'الهدف (شهري)',
'BACK_AGE'	     		                                        =>  'العمر',
'BACK_ADD_SALES_REP'	     		                            =>  'إضافة مندوب مبيعات',
'BACK_SALES_MANAGER_MANAGEMENT'                                 =>  'إدارة مناديب المبيعات',

'BACK_SALES_HEAD'                                               =>  'مدير المبيعات',
'BACK_SALES_EXECUTIVE'                                          =>  'مندوب المبيعات',
'BACK_SALES_REP'                                                =>  'تفاصيل مندوب المبيعات',
'BACK_LEAD_NAME'                                                =>  'اسم العميل المحتمل',
'BACK_SERVICE_CATEGORY'                                         =>  'أقسام الخدمات الرئيسية / الفرعية',
'BACK_CLIENT_EMAIL'                                             =>  'ايميل العميل',
'BACK_CLIENT_PHONE'                                             =>  'رقم الجوال.',        
'BACK_NEW_LEAD'                                                 =>  'مندو المبيعات - إضافة عميل محتمل',
'BACK_LEAD_DETAIL'                                              =>  'مندوب المبيعات - تفاصيل العملاء المحتملين',
'BACK_SEARCH_BY_OCCASION'                                       =>  'البحث حسب المناسبة',
'BACK_DATE_OF_GENE'                                             =>  'تاريخ الإنشاء',
'BACK_BALL_PARK_AMOUNT'                                         =>  'المبلغ التقديري',
'BACK_CARD_NAME'                                               =>  'اسم البطاقة',
'BACK_CARD_IMAGE'                                              =>  'صورة البطاقة',
'BACK_CARD_IMAGE'                                              =>  'صورة البطاقة',
'BACK_MESSAGE_DETAIL'   			                            => 	'تفاصيل الرسالة الجماعية',
'BACK_LEAD_ID'                                                 =>  'Lead Id',
'BACK_ADD_OFFER'                                               =>  'إضافة عرض',
'BACK_EDIT_OFFER'                                              =>  'تعديل عرض',
'BACK_VIEW_OFFER'                                              =>  'تفاصيل العرض',
'BACK_OFFER_NAME'                                              =>  'اسم العرض',
'BACK_VENDOR_LIST'                                             =>  'قائمة مندوبي المبيعات',
'BACK_OFFER_DESC'                                              =>  'وصف العرض',
'BACK_OFFER'                                                   =>  'قائمة العروض',
'BACK_ADD_LEAD'                                                =>  'إضافة بيانات عملاء محتملين',
'BACK_ADD_PLAN'                                                =>  'إضافة خطة',
'BACK_EDIT_PLAN'                                               =>  'تعديل الخطة',
'BACK_VIEW_PLAN'                                               =>  'تفاصيل الخطة',
'BACK_PLAN_NAME'                                               =>  'اسم الخطة',
'BACK_VENDOR_LIST'                                             =>  'قائمة مندوبي المبيعات',
'BACK_PLAN_DESC'                                               =>  'وصف الخطة',
'BACK_PLAN'                                                    =>  'قائمة الخطط',
'BACK_PLAN_MENU'                                               =>  'الخطط',
'BACK_OFFER_MENU'                                              =>  'العروض',
'BACK_ADD_VENDOR'                                              =>  'إضافة مزود خدمة',        
'BACK_EDIT_VENDOR'                                             =>  'تعديل مزود خدمة',
'BACK_AVAIL_PLAN'                                              =>  'الخطط المتاحة',
'BACK_VENDOR_DETAIL'                                           =>  'معلومات مزود الخدمة',
'BACK_NOTES'                                                   =>  'الملاحظات',
'BACK_PAYMENT_DETAIL'                                          =>  'معلومات الدفع',
'BACK_VENDOR'                                                  =>  'مزودين الخدمات',
'BACK_USERS'                                                   =>  'المستخدمين',
'BACK_AVAIL_BALANCE'                                           =>  'الرصيد المتوفر', 
'BACK_PAYMENT_REQUEST'                                           =>  'طلب دفع',   
'BACK_SALES_MANAGER'				=>'مدير المبيعات',
'BACK_SALES_REPRESENTATIVE'			=>'مندوب المبيعات',  
'BACK_SALES_CODE'					=>'كود المندوب',
'BACK_TARGET_ACHIEVE'				=>'الهدف المحقق (الشهر الحالي)',
'BACK_MAIN_CATEGORIES'				=>'الأقسام الرئيسية',
'BACK_MAIN_CATEGORIES_NAME'		=>'اسم القسم الرئيسي',
'BACK_SUB_CATEGORY_DETAIL' 		=>'تفاصيل الأقسام الفرعية',
'BACK_MAIN_CATEGORY_DETAIL' 		=>'تفاصيل القسم الرئيسي',
'BACK_SERVICE_SELECT_NAME'   				=> 	'إختر اسم الخدمة',
'BACK_SUB_ADMIN'                                           =>  'مدير فرعي',
'BACK_NEW_MERCHANTS_MONTH_WISE' 		=> 'مزودين الخدمات المشتركين (بالشهر)',
'BACK_SELECT_CATEGORY' 		=>'إختر قسم',
'BACK_SELECT_REP' 						=>'إختر مندوب المبيعات',
'BACK_ENTER_TRANSACTION_ID'	=>'الرجاء ادخال رقم المعاملة',
'BACK_CONVERSION_RATE'                                           =>  'معدل التحويل',
'BACK_DEFAULT'                                           =>  'تلقائي',
'BACK_LAST_UPDATE'                                           =>  'آخر تحديث',

'BACK_ADD_CURRENCY'                                           =>  'إضافة عملة',
'BACK_EDIT_CURRENCY'                                           =>  'تعديل العملة',
'BACK_DETAIL_CURRENECY'                                           =>  'تفاصيل العملات',
'BACK_CURRENCY'                                           =>  'العملات',
'BACK_CURRENCY_MANAGEMENT'                                           =>  'إدارة العملات',
'BACK_EDIT_SALES_REP'                                           =>  'تعديل بيانات مندوب',
'BACK_BROWSE'	=>'استعراض',
'BACK_NO_FILE'	=>'لم يتم تحديد ملف',
'BACK_SELECT_LANGUAGE' 						=>'إختر لغة',
'BACK_NATIONAL_ID' 							=>'الهوية الوطنية',
'BACK_ELECTRONIC_INVITATION_SMS_MSG_CHARGES' 							=>'رسوم الدعوات',
'BACK_URL1'									=>'رابط صفحة فيسبوك',
'BACK_URL2'									=>'رابط حساب تويتر',
'BACK_URL3'									=>'رابط حساب قوقل بلس',
'BACK_URL4'									=>'رابط حساب الانستقرام',
'BACK_URL5'									=>'رابط صفحة بينترست',
'BACK_URL6'									=>'رابط قناة اليوتيوب',
'MANAGE_ELECTRONIC_INVITATION'  =>'إدارة الدعوات الإلكترونية',

'PERMISSIONTYPE' 						=>'الصلاحيات',
'WRITEPERMISSION' 						 =>'قراءة/كتابة',
'READPERMISSION' 						 =>'قراءة فقط',
'BACK_REFUND_AMT' 						 =>'مبلغ الإرجاع',
'BACK_REFUND_ADMIN' 						 =>'إرجاع',
'BACK_USER_LEAD_STATUS' 					 =>'حالة العميل ',
'Bank_details' =>'تفاصيل البنك',
'Bank_name' =>'اسم',
'Bank_acc_no' => 'رقم الحساب',
'Bank_notes' =>'ملاحظات',
'no_record_found' => 'لا يوجد سجلات',
'PAID_AMT' =>'المبلغ المدفوع',
'Associated_Vendors' =>'الباعة المرتبطون',
'BACK_SEARCH_BY_SHOPNAME' =>'البحث عن طريق اسم المحل',
];
?>
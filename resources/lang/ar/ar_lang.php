﻿<?php

return 

[

/*Front end static pages*/

/*Home page started*/

'LOGO' 			 => 		'الشعار',

'GOLDEN_CAGES'=>'قولدن كيجز',

'CATEGORIES' => 'الاقسام',

'MER_UPDATE'=> 'تحديث',

'MORE_CATEGORIES' => 'اقسام اخرى',

'DISH_NAME_CONTAINER_TYPE'  => 'اسم الصحن / نوع الحاوية',

'NEW_PRODUCTS' => 'نتائج البحث',

'COMPARE' => 'مقارنة',

'SOLD' => 'بيعت',

'GET_QUOTATION' => 'الحصول على عرض سعر',

'NO_OF_DAY'   => 'عدد الايام',

'GM' => 'غرام',

'ADD_TO_CART' => 'إضافة ',

'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_KEY'  	=> 		'أدخل مفتاح بوابة الدفع -بايفورت',

'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS_PAYU_SALT'  	=> 		'Enter Your Payfort Payment Salt',

//'NO_PRODUCTS_AVAILABLE' => 'No products available search',

'NO_PRODUCTS_AVAILABLE' => 'لا يوجد منتجات متاحة بمعايير البحث الحالية',

'CLOSE_COMPARISON_OVERLAY' => 'إغلاق تراكب المقارنة',

'Shop_link_in_maroof'=> 'رابط المتجر في معروف',

'HI' => 'مرحبا',

'PAYMENT_EMAIL_KEY' 				=> 		'Payfort key',

'PAYMENT_EMAIL_SALT' 				=> 		'Payfort Salt',

'PAYMENT_STATUS' => 'حالة الدفع',

'MY_DEAL_COD' => 'My Deal COD',

'MY_DEAL_PAYPAL' => 'My Deal PayPal',

'MY_PRODUCT_PAYPAL' => 'My Product PayPal',

'MY_PRODUCT_COD' => 'My Product COD',

'DEAL_TITLE' => 'عنوان الصفقة',

'IMAGE' => 'الصورة',

'MY_PRODUCT_PAYUMONEY'=>'My Product PayUMoney',

'MY_DEAL_PAYUMONEY'=>'My Deal PayUMoney',

'NO_PRODUCTS_INCOMPARE'=>'No Products in Compare',

'DEALS_OF_THE_DAY' => 'صفقة اليوم',

'TOP_OFFERS' => 'أقوى العروض',

'MOST_POPULAR_PRODUCTS'=>'المنتجات الأكثر شعبية',

'REVIEWS_AND_RATINGS' => 'التقييم والتعليقات',

'NO_RATING_FOR_THIS_PRODUCT'=>'لا توجد تقييمات لهذا المنتج',

'AVAILABLE_STOCK'=> 'الكمية المتوفرة',

'CLICK_HERE' => 'اضغط هنا', 

'NO_WHISLIST'=>'لا توجد منتجات في قائمة المفضلة',

'NO_SHIPPING_ADDRESS'=>'',

'BACK_MEMBER_ACCOUNT_CREATED_SUCCESSFULLY'  => 'مرحباً بك في قولدن كيجز !',

'MER_MAIL_SEND_SUCCESSFULLY'=>'تم إرسال البريد الإلكتروني بنجاح',

'FORGOT_PASSWORD_MSG'=>'الرجاء إدخال عنوان بريدك الإلكتروني',

'FORGOT_PASSWORD_MSG2'=>'سيتم إرسال رابط لاعادة تعيين كلمة المرور',

'New_Password'=>'كلمة المرور الجديدة',

'Confirm_Password'=>'تأكيد كلمة المرور',

'USER_CONFIRM_PASSWORD'=>'تأكيد كلمة المرور يجب ان يكون مطابق لكلمة المرور المدخلة',

/*End Home page*/

/*Footer page start*/

'REQUEST_FOR_ADVERTISEMENT' => 'طلب إعلان',

'AD_TITLE' => 'عنوان الإعلان',

'UPTO' => 'حتى',

'DOWNLOAD_OUR_APP' => 'قم بتنزيل تطبيقاتنا',

'ADS_POSITION' => 'مكان الإعلان',

'SELECT_POSITION' => 'إختر مكان',

'HEADER_RIGHT' => 'الشريط العلوي',

'LEFT_SIDE_BAR' => 'شريط الجانب الأيسر',

'BOTTOM_FOOTER' => 'الشريط السفلي',

'PAGES' => 'صفحات',

'SELECT_ANY_PAGE' => 'إختر صفحة',

'HOME' => 'الصفحة الرئيسية',

'SPORTS' => 'Sports',

'ELECTRONICS' => 'Electronics',

'FLOWER_POT' => 'Flower pot',

'HEALTH' => 'Health',

'PRICE_PER_HOUR' => 'السعر في الساعة',

'BEAUTY' => 'الجمال والاناقة',

'REDIRECT_URL' => 'رابط إعادة التوجية',

'UPLOAD_IMAGES' => 'رفع',

'UPLOAD' => 'رفع',

'COUPON_AMOUNT' => 'قيمة الكوبون',

'CLOSE' => 'اغلاق',

'CONTACT' => 'تواصل معنا',

'CONTACT_US' => 'تواصل معنا',

'ABOUT_COMPANY' => 'حول الشركة',

'BLOG' => 'المدونة',

'ABOUT_US' => 'حول',

'MERCHANT_LOGIN' => 'دخول التاجر',

'REGISTER' => 'تسجيل',

'LOGIN' => 'تسجيل الدخول',

'SOCIAL_MEDIA' => 'التواصل الإجتماعي',

'NEWS_LETTER_SUBSCRIPTION' => 'الإشتراك بالنشرة البريدية',

'SUBSCRIBE_TO_RECEIVE_THE_LATEST_NEWS_STRAIGHT_TO_YOUR_INBOX._BY_SUBSCRIBING_YOU_WILL_BE_ABLE_TO' => 'إشترك لتلقي آخر الأخبار مباشرة إلى صندوق البريد الوارد الخاص بك. عن طريق الإشتراك سوف تكون قادر على',

'SUBSCRIBE_HERE' => 'إشترك هنا',

'PAYMENT_METHOD' => 'طرق الدفع',

'OUR_SERVICES' => 'خدماتنا',

'TERMS_&_CONDITIONS' => 'الشروط والأحكام',

'HELP' => 'مساعدة',

'ALL_RIGHTS_RESERVED' => 'جميع الحقوق محفوظة',

'SUCCESSFULLY_SUBSCRIBED' => 'تم الإشتراك بنجاح',

'ITEMS_IN_YOUR_CART' => 'أصناف في السلة',

'SELECT_CATEGORY' => 'إختر قسم',

'DEALS' => 'Deals',

'SOLD_OUT' => 'بيعت كاملاً',

'STORES' => 'المتاجر',

'NEAR_BY_STORE' => 'بالقرب من المتجر',

'SEARCH_PRODUCT_NAME' => 'ابحث عن منتج /خدمة بالاسم',

'CUSTOMER_SUPPORT' => 'خدمة العملاء',

'OK'=>'موافق',

'sqmeter'=>'متر مربع',

/*End footer page*/	

/*Navbar started*/

'FORGOT_PASSWORD' => 'رابط إعادة تعيين كلمة المرور',

'CONNECT_YOUR_FACEBOOK_ACCOUNT_FOR_SIGN_UP' => 'الاتصال بحساب فيسبوك للتسجيل',

'DONT_HAVE_AN_ACCOUNT_YET' => 'ليس لديك حساب حتى الآن؟',

'SIGN_UP' => 'تسجيل',

'E-MAIL' => 'البريد الإلكتروني',

'E-MAIL_ID' => 'معرف البريد الإلكتروني',

'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO' => 'سجل الدخول باستخدام حساب الفيسبوك. قم بالاتصال بالفيسبوك لتسجيل الدخول',

'SIGN_UP_WITH_FACEBOOK' => 'التسجيل عبر فيسبوك',

'ALREADY_A_MEMBER' => 'عضو بالفعل',

'RESET_PASSWORD' => 'إعادة تعيين كلمة المرور',

'NEW_PASSWORD' => 'كلمة المرور الجديدة',

'CONFIRM_PASSWORD' => 'تأكيد كلمة المرور',

'SELECT_COUNTRY' => 'إختر دولة',

'SELECT_CITY' => 'إختر مدينة',

'BY_CLICKING_SIGN_UP_YOU_AGREE_TO' => 'بالضغط على تسجيل انت توافق على',

'TERMS_AND_CONDITIONS' => 'الشروط والأحكام',

'SIGN_IN_WITH_YOUR_FACEBOOK_ACCOUNT:_CONNECT_YOUR_FACEBOOK_ACCOUNT_TO_SIGN_IN_TO_LARAVEL_ECOMMERCE_MULTIVENDOR' => 'تسجيل الدخول باستخدام حساب فيسبوك الخاص بك: قم بالاتصال بحساب الفيسبوك الخاص بك لتسجيل الدخول إلى لوحة تحكم التاجر',

'LOG_IN_WITH_FACEBOOK' => 'تسجيل الدخول باستخدام فيسبوك',

'SIGN_IN' => 'تسجيل الدخول',

'HELPER::CUSTOMER_SUPPORT_NUMBER' => 'Helper::customer_support_number',

'MIMIMUM_6_CHARACTERS' => '6 خانات على الأقل',

'ENTER_YOUR_EMAIL' => 'أدخل بريدك الإلكتروني',

'ENTER_YOUR_NEW_PASSWORD' => 'أدخل كلمة المرور الجديدة',

'CONFIRM_YOUR_NEW_PASSWORD' => 'تأكيد كلمة المرور الجديدة',

'ENTER_YOUR_NAME_HERE' => 'أدخل اسمك هنا',

'ENTER_YOUR_EMAIL_HERE' => 'أدخل البريد الإلكتروني',

'SUBMIT' => 'إرسال',

'CANCEL' => 'إلغاء',

'LOGIN_SUCCESSFULLY' => 'تم تسجيل الدخول بنجاح',

'INVALID_LOGIN_CREDENTIALS' => 'بيانات تسجيل الدخول غير صحيحة',

'ALREADY_EMAIL_EXISTS' => 'البريد الإلكتروني مسجل من قبل',

'PLEASE_CHECK_YOUR_EMAIL_FOR_FURTHER_INSTRUCTIONS' => 'لمزيد من المعلومات يرجى تفقد بريدك الإلكتروني',

'EMAIL_ID_DOES_NOT_EXIST' => 'البريد الإلكتروني غير موجود',

'PASSWORD_CHANGED_SUCCESS' => 'تم تغيير كلمة المرور بنجاح',

'INVALID_USER' => 'مستخدم غير موجود',

'Sign_In_Sign_Up_With' =>'تسجيل الدخول / التسجيل بـ',

/*Navbar started*/

/*Start loginnav bar*/	

'WELCOME' => 'مرحباً',

'MY_WISHLIST' => 'قائمة التفضيلات',

'LOG_OUT' => 'تسجيل الخروج',

/*End lagin Nav bar*/

/*Start About Us.php page*/



/*End About Us.php page*/

/*Start App.php page*/

'LARAVEL' => 'لارافل',

'TOGGLE_NAVIGATION' => 'تبديل التصفح',

'TOTAL_SERVICES' => 'كافة الخدمات',

/*End App.php page*/

/*Start Action blade.php*/

'SPECIALS' => 'العروض الخاصة',

'LAST_BIDDER' => 'آخر مزايد',

'BIT_AMOUNT' => 'قيمة العرض',

'BID_NOW' => 'المزايدة الآن',

'MORE_FILTERS' => 'المزيد من الفلاتر',

'RESET' => 'إعادة تعيين',

'PER_PAGE' => 'لكل صفحة',

'VIEW_ALL' => 'عرض الكل',

'LIKES_ASC' => 'الأقل إعجاباً',

'SORT_BY' => 'فرز حسب',

'PRICE_LOW' => 'السعر الأقل',

'HIGH' => 'اعلى',

'PRICE_HIGH' => 'السعر الاعلى',

'LOW' => 'أقل',

'TITLE' => 'العنوان',

'A' => 'A',

'Z' => 'Z',

'DESCRIPTION' => 'الوصف',

'PAGE' => 'صفحة',

'OF' => 'من',

'NO_RESULTS_FOUND' => 'لم يتم العثور على نتائج',

'LIST_ACTIONS' => 'قائمة الإجراءات',

'LIKES_DESC' => 'الأكثر إعجاباً',

'DATE_ASC' => 'حسب التاريخ تصاعدي',

'DATE_DESC' => 'حسب التاريخ تنازلي',

'STYLE_SELECTOR' => 'إختيار نمط',

'OREGIONAL_SKIN' => 'النمط الاصلي',

'BOOTSWATCH_SKINS' => 'Bootswatch Skins',

'THESE_ARE_JUST' => 'These are just examples and you can build your own color scheme in the backend',

'FILTER_BY_TITLE' => 'تصفية حسب العنوان',

/*End Action blade.php*/

/*Start Actionview blade.php*/

'AUCTION' => 'مزاد علني',

'DAYS' => 'أيام',

'HOURS' => 'ساعات',

'MIN' => 'أنا',

'SEC' => 'ثانية',

'BID_NOW' => 'المزايدة الآن',

'BID_FROM' => 'محاولة من',

'BID_INCREMENT' => 'زيادة العطاء',

'RETAIL_PRICE' => 'سعر التجزئة',

'BID_HISTORIES' => 'تاريخ العروض',

'LATEST_BIDDER(S)' => 'أحدث مزايد (عروض)',

'NOT_YET_BID' => 'ليس بعد',

'PRODUCT_DETAILS' => 'تفاصيل المنتج',

'RELATED_PRODUCTS' => 'منتجات ذات صله',

'PRODUCT_INFORMATION' => 'وصف المنتج',

'PRODUCT_SPECIFICATION'=> 'مواصفات المنتج',

'CATEGORY' => 'التصنيف',

'FEATURES' => 'المميزات',

'NEW' => 'جديد',

'AVAILABLE' => 'متاح',

'VIEW_DETAILS' => 'عرض التفاصيل',

'YOUR_BID_AMOUNT_IS_NO_LONGER_ENOUGHTO_WIN' => 'مبلغ المزايدة الخاص بك لم يعد كافيا للفوز',

'PLEASE_RE-ENTER_YOUR_BID' => 'يرجى إعادة إدخال العرض الخاص بك',

'NEW_MAX_BID' => 'جديد ماكس مزايدة',

'OR_MORE' => 'or More',

'TWITTER_TWEET_BUTTON' => 'تويتر زر سقسقة',

/*End Actionview blade.php */

/*start bid_payment blade.php*/	

'BID_AMOUNT' => 'قيمة المزايدة',

'BID_SUMMARY' => 'ملخص العطاء',

'YOUR_BID_AMOUNT' => 'لديك مبلغ العرض',

'SHIPPING_AMOUNT' => 'كمية الشحن',

'YOUR_SHIPPING_ADDRESS' => 'عنوان الشحن الخاص بك',

'GOOD_NEWS' => 'Good News',

'A_PRODUCT_OF_NEXEMERCHANT' => 'A product of NexEmerchant. selling speciality products to Indians living in Australia',

'BACKGROUND_PATTERNS' => 'Background Patterns',

'AMELIA' => 'Amelia',

'SPRUCE' => 'Spruce',

'SUPERHERO' => 'Superhero',

/*End bid_payment blade.php*/

/*start blog.blade.php*/

'POSTED_BY' => 'تم النشر بواسطة',

'ADMIN' => 'المدير',

'COMMENTS' => 'ملاحظات أخرى',

'LATEME_KNOW' => 'إذا كان لديك أي استفسار لا تتردد بالتواصل معنا ',

'Golden_Cages' => 'إدارة قولدن كيجز',

'CONTINUE_READING' => 'مواصلة القراءة',

'NO_BLOGS_AVAILABLE' => 'لا توجد منشورات',

'POPULAR_POSTS' => 'منشورات شائعة',

'CONTINUE'=>'استمرار',

/*End blog.blade.php*/

/*Start blogcomment.blade.php*/

'YOU_HAVE_ERRORS_WHILE_PROVIDING_COMMENT' => 'حصلت اخطاء أثناء إرسال تعليقك',

'CUSTOMER_COMMENTS' => 'تعليقات العملاء',

'LEAVE_A_REPLY' => 'اترك رد',

'WEBSITE' => 'الموقع',

'SERVICES' => 'الخدمات',

'ENTER_YOUR_NAME' => 'أدخل اسمك',

'ENTER_YOUR_EMAIL_ID' => 'أدخل بريدك الإلكتروني',

'ENTER_YOUR_LINK' => 'أدخل الرابط',

'ENTER_YOUR_MESSAGE' => 'إكتب رسالتك',

'OR'=>'أو',

'KG'=>'كغم',



/*End blogcomment.blade.php*/

/*Start blog view.blade.php*/



/*End blog view.blade.php*/

/*Start cart.blade.php*/

'SHOPPING_CART' => 'السلة',

'ITEM(S)' => 'صنف',

'CONTINUE_SHOPPING' => 'مواصلة التسوق',

'VIEW_CART' => 'عرض السلة',

'USER_COUPON' => 'كوبون العميل',

'PRODUCT_COUPON' => 'كوبون المنتج',

'S.NO' => 'م',

'PRODUCT' => 'المنتجات',

'COLOR' => 'الألوان المتوفرة',

'SIZE' => 'المقاسات المتوفرة',

'QUANTITY' => 'الكمية',

'UPDATE' => 'تحديث',

'REMOVE' => 'حذف',

'PRICE' => 'السعر',

'SUB_TOTAL' => 'المجموع',

'COUPON_CODE' => 'رمز الكوبون',

'N' => 'N',

'ESTIMATE_YOUR_SHIPPING' => 'تقدير الشحن الخاص بك',

'CHECK_PRODUCT_AVAILABILITY_AT_YOUR_LOCATION' => 'تحقق من توفر المنتج في منطقتك',

'POST_CODE' => 'الرمز البريدي',

'ZIPCODE' => 'الرمز البريدي',

'SelectServices' => 'إختر خدمات',

'UPLOADPIC' => 'رفع صور',

'VERIFY' => 'تحقق',

'NO_ITEMS_IN_CART' => 'لا توجد أصناف في السلة',

'PROCEED_TO_CHECKOUT' => 'الذهاب للدفع',

'SPECIAL_COUPON_CODE' => 'رمز الكوبون الخاص',

'PLEASE_FILL_ALL_FIELDS' => 'الرجاء ملئ جميع الحقول',

'ENTER_CORRECT_COUPON_CODE' => 'أدخل رمز كوبون صحيح',

'APLICAPLE_ONLY' => 'ينطبق فقط',

'QUANTITY_OF_PRODUCT' => 'كمية المنتجات',

'USE_THIS_COUPON_ONLY_FOR' => 'إستخدم هذا الكوبون فقط لـ',

'COUPON_IS_NOT_VALID' => 'الكوبون غير صحيح',

'COUPON_DATE_NOT_STARTED' => 'تاريخ الكوبون لم يبدأ بعد',

'COUPON_DATE_EXPIRED' => 'انتهت صلاحية الكوبون',

'COUPON_CODE_ALREADY_EXIST' => 'رمز الكوبون موجود مسبقاً',

'ALL_COUPONS_ARE_PURCHASED' => 'تم شراء جميع الكوبونات',

'YOUR_COUPON_LIMIT_EXIT' => 'حد الكوبون الخاص بك',

'CANNOT_APPLY_MULTIPLE_PRODUCT_COUPON_IN_SAME_CART' => 'لا يمكنك تطبيق أكثر من كوبون للمنتج الواحد في السله',

'PLEASE_ENTER_VALID_COUPON_CODE' => 'الرجاء إدخال رمز كوبون صحيح',

'PLEASE_TRY_AGAIN_ITS_NOT_VALID_COUPON_CODE_APPLIED_FOR_YOU' => 'حاول مرة اخرى. رمز الكوبون غير صالح للتنفيذ',

'NOT_HAVE_SUFFICIENT_CART' => 'السعر الإجمالي في السلة ليس كافياً لتطبيق كوبون الخصم',

'COUPON_CODE_ALREADY_EXSIST' => 'رمز الكوبون موجود مسبقاًً',

'YOUR_COUPON_NOT_STARTED' => 'كوبون الخصم لم يبدء بعد',

'APPLY' => 'تطبيق',

'CANCEL_USER_COUPON' => 'إلغاء كوبون العميل',

'USER_COUPON1' => 'كوبون العميل',

'PRODUCT_COUPON1' => 'كوبون المنتج',

/*End cart.blade.php*/

/*start categorylist.blade.php*/

'MOST_VISITED_PRODUCT' => 'المنتج الأكثر زيارة',

/*End categorylist.blade.php*/

/*start category list all.blade.php*/

'ALL_CATEGORIES' => 'جميع الاقسام', 

/*End category list all.blade.php*/

/*Start Checkout.blade.php*/

'CHECKOUT' => 'الدفع',

'SHIPPING_ADDRESS' => 'عنوان الشحن',

'LOAD_SHIPPING_ADDRESS' => 'تحميل عنوان الشحن',

'YES' => 'نعم',

'NO' => 'لا',

'NAME_FIELD_IS_REQUIRED' => 'حقل الاسم إلزامي',

'ADDRESS_LINE1' => 'العنوان 1',

'ADDRESS_FIELD_IS_REQUIRED' => 'العنوان إلزامي',

'ADDRESS_LINE2' => 'العنوان 2',

'CITY' => 'المدينة',

'STATE' => 'State',

'COUNTRY' => 'الدولة',

'PHONE_NUMBER' => 'رقم الجوال',

'PHONE_NUMBER_FIELD_IS_REQUIRED' => 'رقم الجوال إلزامي',

'SELECT_PAYMENT_METHOD' => 'إختر طريقة الدفع',

'PAYPAL' => 'باي بال',

'PAYUMONEY' => 'PayUMoney ',

'CASH_ON_DELIVERY' => 'الدفع عند الإستلام',

'ORDER_SUMMARY' => 'ملخص الطلب',

'SHIPMENT' => 'الشحنة',

'ESTIMATED_DELIVERY' => 'التوصيل المتوقع',

'SHIPPING' => 'الشحن',

'TYPE_OF_COUPON' => 'نوع الكوبون',

'VALUE' => 'القيمة',

'DEAL_DETAILS' => 'تفاصيل الصفقة',

'USE_WALLET' => 'إستخدم المحفظة',

'ORDER_SUBTOTAL' => 'مجموع الطلب',

'ORDER_SHIPPING' => 'شحن الطلب',

'ORDER_TAX' => 'الضريبة',

'ORDER_TOTAL' => 'إجمالي الطلب',

'PLACE_ORDER' => 'تقديم الطلب',

'NO_ORDERS_PLACED' => 'لا توجد طلبات مرسلة',

'ENTER_FIRST_NAME' => 'أدخل الاسم الاول',

'ENTER_LAST_NAME' => 'أدخل الاسم الاخير',

'ENTER_ADDRESS_LINE1' => 'أدخل العنوان 1',

'ENTER_ADDRESS_LINE2' => 'أدخل العنوان 2',

'ENTER_YOUR_CITY' => 'الرجاء إختيار المدينة',

'ENTER_YOUR_STATE' => 'أدخل المدينة',

'ENTER_YOUR_PHONE_NO' => 'أدخل رقم الجوال',

'ENTER_YOUR_VALID_PHONE_NO' => 'أدخل رقم جوال صحيح',

'ENTER_YOUR_ZIPCODE' => 'أدخل الرمز البريدي',

'ENTER_YOUR_VALID_ZIPCODE' => 'أدخل الرمز البريدي',

'ENTER_YOUR_FIRST_NAME' => 'أدخل اسمك الاول',

'ENTER_YOUR_LAST_NAME' => 'أدخل اسمك الاخير',

'ENTER_YOUR_ADDRESS_LINE1' => 'أدخل العنوان 1',

'ENTER_YOUR_ADDRESS_LINE2' => 'أدخل العنوان 2',

'ENTER_YOUR_COUNTRY' => 'إختر الدولة',

'ENTER_YOUR_PHONENUMBER' => 'أدخل رقم الجوال',

'ENTER_ZIP_CODE' => 'أدخل الرمز البريدي',

/*End Checkout.blade.php*/

/*start cms blade.php*/

'NO_DATA_FOUND' => 'لم يتم العثور على بيانات',

'NO_CATEGORY_FOUND' => 'لا توجد اقسام',

/*End cms blade.php*/

/*start compare.blade.php*/



/*end compare.blade.php*/

/*start compare_product.blade.php*/

'COMPARE_PRODUCTS' => 'مقارنة المنتجات',

'ORIGIANL_PRICE' => 'السعر قبل الخصم',

'DISCOUNT' => 'الخصم',

'RATING' => 'التقييم',

'RATINGS' => 'التقييمات',

'DELIVERY_WITH_IN' => 'التسليم خلال',

'SIZES' => 'المقاسات',

'DESC' => 'الوصف',

'SPEC' => 'المواصفات',

'STORE_NAME' => 'اسم المتجر',

'CLEAR_LIST' => 'تصفية القائمة',

/*start compare_product.blade.php*/

/*start contactus.blade.php*/

'CONTACT_DETAILS' => 'معلومات الاتصال',

'EMAIL_US' => 'راسلنا',

'PLEASE_ENTER_YOUR_FIRSTNAME' => 'الرجاء إدخال الاسم الاول',

'PLEASE_ENTER_YOUR_LASTNAME' => 'الرجاء إدخال الاسم الاخير',

'PLEASE_PROVIDE_A_PASSWORD' => 'الرجاء كتابة كلمة المرور',

'OUR_PASSWORD_MUST_BE_AT_LEAST_5_CHARACTERS_LONG' => 'كلمة المرور يجب ان تكون 5 خانات كحد أدنى',

'PLEASE_ENTER_THE_VALID_EMAIL_ADDRESS' => 'الرجاء إدخال بريد إلكتروني صحيح',

'PLEASE_ACCEPT_OUR_POLICY' => 'الرجاء الموافقة على السياسات والخصوصية',

'ENTER_YOUR_NAME' => 'أدخل اسمك',

'ENTER_YOUR_VALID_EMAIL_ID' => 'أدخل بريدك الإلكتروني الصحيح',

'ENTER_YOUR_PHONE_NUMBER' => 'أدخل رقم الجوال',

'ENTER_QUERIES' => 'إكتب إستفسارك',

'SEND_MESSAGE' => 'إرسال رسالة',

/*End contactus.blade.php*/

/*Start customer_profile.blade.php*/

'MY_PROFILE' => 'الملف الشخصي',

'MY_ACCOUNT' => 'حسابي',

'MY_COD_DETAILS' => 'تفاصيل الدفع عند الإستلام',

'MY_BUYS' => 'قائمة مشترياتي',

'MY_WISH_LIST' => 'قائمة المفضلة',

'MY_SHIPPING_ADDRESS' => 'عنوان الشحن',

'EDIT' => 'تعديل',

'PROFILE_IMAGES' => 'صورة الملف الشخصي',

'CANCEL' => 'إلغاء',

'SELECT_COUNTRY' => 'إختر الدولة',

'TOTAL_WALLET_BALANCE_AMOUNT' => 'إجمالي المبلغ في المحفظة',

'ORDERID' => 'رقم الطلب',

'TOT._PRODUCT' => 'إجمالي المنتجات',

'TOTAL_ORDER_AMOUNT' => 'إجمالي السعر',

'ORDER_DATE' => 'تاريخ الطلب',

'USED_WALLET' => 'إستخدام المحفظة',

'STATUS' => 'الحالة',

'INVOICE' => 'الفاتورة',

'TAX_INVOICE' => 'فاتورة ضريبية',

'CASH_ON_DELIVERY' => 'الدفع عند الإستلام',

'AMOUNT_PAID' => 'المبلغ المدفوع',

'INCLUSIVE_OF_ALL_CHARGES' => 'تشمل جميع الرسوم',

'SHIPPING_ADDRESS' => 'عنوان الشحن',

'INVOICE_DETAILS' => 'تفاصيل الفاتورة',

'THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS' => 'هذه الشحنة تحتوي الأصناف التالية',

'ORIGINAL_PRICE' => 'السعر قبل الخصم',

'SHIPMENT_VALUE' => 'قيمة الشحن',

'WALLET' => 'المحفظة',

'AMOUNT' => 'المبلغ',

'PRODUCT_NAMES' => 'اسم المنتج',

'PRODUCT_IMAGE' => 'صورة المنتج',

'ACTION' => 'إجراءات',

'ALL_FIELDS_ARE_MANDATORY' => 'جميع الحقول إلزامية',

'FULL_NAME' => 'الاسم الكامل',

'ADDRESS2' => 'العنوان 2',

'DEALS_NAMES' => 'اسماء الصفقات',

'CHANGE_PROFILE_PICTURE' => 'تغيير صورة الملف الشخصي',

'IMAGE_UPLOAD_SIZE_1' => 'رفع صورة حجم 1 ميجابايت',

'PLEASE_PROVIDE_PHONENUMBER' => 'الرجاء إدخال رقم الجوال',

'PHONENUMBER_CHANGED_SUCCESSFULLY' => 'تم تغيير رقم الجوال بنجاح',

'PLEASE_PROVIDE_ANY_ONE_OF_THE_ADDRESS_FIELDS' => 'الرجاء إدخال العنوان',

'ADDRESS_CHANGED_SUCCESSFULLY' => 'تم تغيير العنوان بنجاح',

'CITY_AND_COUNTRY_CHANGED_SUCCESSFULLY' => 'تم تغيير المدينو بنجاح',

'PLEASE_PROVIDE_VALID_PHONE_NUMBER' => 'الرجاء إدخال رقم جوال صحيح',

'SHIPPING_DETAILS_UPDATED_SUCCESSFULLY' => 'تم تحديث معلومات الشحن بنجاح',

'COUNTRY_CHANGED_SUCCESSFULLY' => 'تم تغيير الدولة بنجاح',

'ENTER_YOUR_OLD_PASSWORD' => 'أدخل كلمة المرور القديمة',

'ENTER_YOUR_NEW_PASSWORD' => 'أدخل كلمة المرور الجديدة',

'ENTER_CONFIRM_PASSWORD' => 'تأكيد كلمة المرور',

'FRUIT_BALL' => 'Fruit ball',

'ENTER_YOUR_PHONE_NUMBER' => 'أدخل رقم الجوال',

'PROVIDE_ADDRESS1' => 'أدخل العنوان 1',

'PROVIDE_ADDRESS2' => 'أدخل العنوان 2',

'ENTER_YOUR_ADDRESS' => 'أدخل العنوان',

'ENTER_YOUR_MOBILE_NUMBER' => 'أدخل رقم الجوال الخاص بك',

'ENTER_YOUR_EMAIL_ID' => 'أدخل بريدك الإلكتروني',

'ENTER_YOUR_ZIP_CODE' => 'أدخل الرمز البريدي',

'UPDATE' => 'تحديث',

'IMAGE_FIELD_REQUIRED' => 'حقل الصورة مطلوب',

'NAME_UPDATED_SUCCESSFULLY' => 'تم تحديث الاسم بنجاح',

'PASSWORD_CHANGED_SUCCESSFULLY' => 'تم تغيير كلمة المرور بنجاح',

'BOTH_PASSWORDS_DO_NO_MATCH' => 'كلمتا المرور غير متطابقة',

'OLD_PASSWORD_DOES_NOT_MATCH' => 'كلمة المرور القديمة غير صحيحة ',

'MOBILE' => 'رقم الجوال',

/*End customer_profile.blade.php*/

/*Start deals.blade.php*/

'MOST_VISITED_DEALS' => 'الصفقة الأكثر زيارة',

'NO_DEALS_AVAILABLE' => 'لا توجد صفقات متوفره حالياً',

'SHIPPING_DELIVERY' => 'الشحن و التسليم',

/*End deals.blade.php*/

/*start deals_shipping_details.blade.php*/

'PRODUCTS_NAME' => 'اسم المنتج',

'DATE' => 'التاريخ',

'DETAILS' => 'التفاصيل',

'DELIVERY_STATUS' => 'حالة الشحن',

'EXAMPLE_BLOCK-LEVEL_HELP_TEXT_HERE' => 'مثال على نص المساعدة',

'SEND' => 'إرسال',

'SENNEX_ECARTD' => 'Nex eCart',

/*end Deals_shipping_details.blade.php*/

/*Start enquirey.blade.php*/

'ENTER_SUBJECT' => 'أدخل الموضوع',

/*end enquirey.blade.php*/

/*Start dealview.blade.php*/

'RELATED_DEALS'      			=> 		'Related Deals',

'OUT_OF_STOCK' 					=> 		'نفذت الكمية',

'OFFER' 						=> 		'عرض',

'DICOUNT' 						=> 		'خصم',

'YOU_SAVE' 						=> 		'You Save',

'STORE_DETAILS' 				=> 		'معلومات المتجر',

'WRITE_A_POST_COMMENTS' 		=> 		'Write a post comments',



'ENTER_COMMENT_TITLE' 			=> 		'Enter Comment Title',

'STAR' 							=> 		'Star',

'REVIEWS' 						=> 		'التعليقات',

'NO_REVIEW_RATINGS' 			=> 		'No Review Ratings',

'WRITE_A_REVIEW' 				=> 		'Write a Review',

'ENTER_COMMENTS_QUERIES' 		=> 		'Please enter your comments.',

'VIEW_STORE' 					=> 		'عرض المتاجر',

'PRATEEK' 						=> 		'Prateek',

/*end dealview.blade.php*/

/*start faq.blade.php*/

'FAQ' 							=> 		'الاسئلة الشائعة',

/*End faq.blade.php*/

/*start forgptpassword.blade.php*/

'HELLO' 						=> 		'مرحباً',

'PASSWORD_RESET_LINK' 			=> 		'رابط إعادة تعيين كلمة المرور',

'PASSWORD_LINK' 				=> 		'رابط كلمة المرور',

'USERNAME' 						=> 		'اسم المستخدم',

'PASSWORD_LINK' 				=> 		'رابط كلمة المرور',

'PLEASE_CLICK_THIS_LINK_TO_RESET_YOUR_PASSWORD' 				=> 		'الرجاء النقر على الرابط لاعادة تعيين كلمة المرور',



/*end forgptpassword.blade.php*/

/*start help.blade.php*/



/*end help.blade.php*/

/*start manage_deal_review.blade.php*/

'MANAGE_REVIEWS' 				=> 		'إدارة التعليقات',

'MANAGE_PRODUCTS' 				=> 		'إدارة المنتجات',

'REVIW_TITLE' 	 				=> 		'عنوان التعليق',

'CUSTOMER_NAME' 	 			=> 		'اسم العميل',

'ACTIONS' 	 					=> 		'الإجراءات',

'CLICK_FOR_PREVIOUS_MONTHS' 	=> 		'الذهاب للشهر السابق',

'CLICK_FOR_NEXT_MONTHS' 		=> 		'الذهاب للشهر التالي',

/*End manage_deal_review.blade.php*/

/*start manage_dealcason_delivery_details.blade.php*/



/*end manage_dealcason_delivery_details.blade.php*/

/*start merchant_signup.blade.php*/

'MERCHANT_SIGN_UP' 				=> 		'تسجيل التاجر',

'WELCOME_TO_MERCHANT_SIGN_UP' 	=> 		'مرحباً بك في قائمة التسجيل',

'CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE' 	=>'إمتلك متجرك الإلكتروني في عالم المناسبات!',

'CREATE_YOUR_STORE' 			=> 		'انشئ متجرك',

'ADDRESS1' 						=> 		'العنوان 1',

'ADDRESS2' 						=> 		'العنوان 2',

'ZIPCODE' 						=> 		'الرمز البريدي',

'META_KEYWORDS' 				=> 		'الكلمات الوصفية',

'META_DESCRIPTION' 				=> 		'شرح البيانات الوصفية',

'UPLOAD_FILE' 					=> 		'رفع ملف',

'ENTER_YOUR_STORE_LOCATION' 	=> 		'أدخل موقع المتجر',

'GOOGLE_MAPS' 					=> 		'خرائط قوقل',

'LATITUDE' 						=> 		'خط العرض',

'LONGTITUDE' 					=> 		'خط الطول',

'PERSONAL_DETAILS' 				=> 		'المعلومات الشخصية',

'FIRST_NAME' 					=> 		'الاسم الاول',

'LAST_NAME' 					=> 		'الاسم الأخير',

'CONTACT_NUMBER' 				=> 		'رقم الجوال',

'PAYMENT_EMAIL' 				=> 		'ايميل الدفع',

'NUMBERS_ONLY_ALLOWED' 			=> 		'يسمح بالارقام فقط',

'NOT_A_VALID_EMAIL_ADDRESS'     => 		'الرجاء إدخال بريد إلكتروني صحيح',

'PLEASE_ENTER_YOUR_STORE_NAME'  => 		'الرجاء إدخال اسم المتجر',

'PLEASE_ENTER_YOUR_PHONE_NO'  	=> 		'الرجاء إدخال رقم الجوال',

'PLEASE_ENTER_ADDRESS1_FIELD'  	=> 		'الرجاء إدخال العنوان 1',

'PLEASE_ENTER_ADDRESS2_FIELD'  	=> 		'الرجاء إدخال العنوان 2',

'PLEASE_ENTER_ZIPCODE'  		=> 		'الرجاء إدخال الرمز البريدي',

'PLEASE_ENTER_META_KEYWORDS'  	=> 		'الرجاء إدخال الكلمات الوصفية',

'PLEASE_ENTER_META_DESCRIPTION' => 		'الرجاء إدخال شرح البيانات الوصفية',

'PLEASE_ENTER_WEBSITE' 			=> 		'الرجاء إدخال الموقع الإلكتروني',

'PLEASE_ENTER_LOCATION' 		=> 		'الرجاء تحديد الموقع',

'PLEASE_ENTER_COMMISSION' 		=> 		'الرجاء إدخال نسبة العمولة',

'PLEASE_CHOOSE_YOUR_UPLOAD_FILE'=> 		'الرجاء رفع صور',

'PLEASE_SELECT_COUNTRY'         => 		'الرجاء تحديد الدولة',

'PLEASE_SELECT_CITY'         	=> 		'الرجاء تحديد المدينة',

'PLEASE_ENTER_AN_EMAIL_ADDRESS' => 		'الرجاء إدخال البريد الإلكتروني',

'PLEASE_ENTER_YOUR_FIRST_NAME'  => 		'الرجاء إدخال الاسم الاول',

'PLEASE_ENTER_CITY' 		 	=> 		'الرجاء تحديد المدينة',

'PLEASE_ENTER_ADDREESS1_FIELD'  => 		'الرجاء إدخال حقل العنوان 1',

'PLEASE_ENTER_PAYMENT_ACCOUNT'  => 		'الرجاء إدخال رقم حساب الدفع',

'PLEASE_ENTER_LAST_NAME'  		=> 		'الرجاء إدخال الاسم الاخير',

'PLEASE_ENTER_PHONE_NO'  		=> 		'الرجاء إدخال رقم الجوال',

'ENTER_YOUR_STORE_NAME'  		=> 		'أدخل اسم المتجر',

'ENTER_YOUR_CONTACT_NUMBER'  	=> 		'أدخل رقم الجوال',

'ENTER_YOUR_ADDRESS1'  			=> 		'أدخل العنوان 1',

'ENTER_YOUR_ADDRESS2'  			=> 		'أدخل العنوان 2',

'ENTER_YOUR_ZIPCODE'  			=> 		'أدخل الرمز البريدي',

'ENTER_YOUR_META_KEYWORDS'  	=> 		'أدخل الكلمات الوصفية',

'ENTER_YOUR_META_DESCRIPTION'  	=> 		'أدخل شرح البيانات الوصفية',

'ENTER_YOUR_STORE_WEBSITE'  	=> 		'أدخل موقعك الإلكتروني',

'TYPE_YOUR_LOCATION_HERE'  	    => 		'أدخل موقع المتجر هنا',

'ENTER_YOUR_FIRST_NAME'  	    => 		'أدخل الاسم الاول',

'ENTER_YOUR_LAST_NAME'  	    => 		'أدخل الاسم الاخير',

'ENTER_YOUR_PAYMENT_ACCOUNT_DETAILS'  	=> 		'أدخل تفاصيل حساب الدفع',

'SEARCH'  						=> 		'بحث',

'MERCHANT_EMAIL_EXIST'			=> 	'هذا البريد مستخدم بالفعل',



'WITHIN_BUDGET'			=> 	'ضمن الميزانية',

'ABOVE_BUDGET'			=> 	'فوق الميزانية',

'OFFERS'			=> 	'العروض',

'Hotel_Halls'=>'قاعات الفنادق',

'Large_Halls'=>'قاعات الأفراح',

'Small_Halls'=>'صالات الأفراح',



/*end merchant_signup.blade.php*/

/*Start newsletter.blade.php*/

'NEWS_LETTER'  					=> 		'النشرة البريدية',

'SUBSCRIBE_YOUR_MAIL_ID'  		=> 		'إشترك الان بواسطة بريدك الإلكتروني',

'ENTER_YOUR_MAIL_ID_FOR_EMAIL_SUBSCRIPTION'  		=> 		'أدخل بريدك الإلكتروني للإشتراك في القائمة البريدية',

'SUBSCRIBE'  					=> 		'إشتراك',

'WELCOME_TO_SUBSCRIBE_YOUR_NEWS_LETTER_SUBSCRIPTION'  					=> 		'مرحبا بك في القائمة البريدية',

/*end newsletter.blade.php*/

/*start pages.blade.php*/

/*end pages.blade.php*/

/*start paymentresult.blade.php*/

'PAYMENT_RESULT'  				=> 		'نتيجة الدفع',

'YOUR_PAYMENT_PROCESS_SUCCESSFULLY_COMPLETED'  					=> 		'تمت عملية الدفع بنجاح',

'PLEASE_NOTE_THE_TRANSACTION_DETAILS'  							=> 		'يرجى ملاحظة تفاصيل المعاملة',

'YOUR_PAYMENT_PROCESS_FAILED'  	=> 		'فشلت عملية الدفع',

'YOUR_PAYMENT_PROCESS_HAS_BEEN_STOPPED_DUE_TO_SOME_ERROR'  	=> 		'توقفت عملية الدفع نتيجة لخطأ ما!',

'TRANSACTION_DETAILS'  			=> 		'تفاصيل المعاملة',

'THANK_YOU_FOR_SHOPPING_WITH'  	=> 		'شكراً للتسوق مع',

'PAYER_NAME'  					=> 		'اسم العميل',

'TRANSACTIONID'  				=> 		'رقم المعاملة',

'TOKENID'  						=> 		'TokenId',

'PAYER_EMAIL'  					=> 		'البريد الإلكتروني للعميل',

'PAYER_ID'  					=> 		'رقم العميل',

'ACKNOWLEDGEMENT'  				=> 		'اشعار',

'PAYERSTATUS'  					=> 		'حالة العميل',

'RODUCT_DETAILS_FOR_CURRENT_TRANSACTION'  					=> 		'تفاصيل المنتجات للمعاملة الحالية',

'PRODUCT_QUANTITY'  			=> 		'كمية المنتجات',

'PRODUCT_DEAL_QUANTITY'  			=> 		'كمية المنتجات',

/*end paymentresult.blade.php*/

/*Start paymentcod.blade.php*/

'YOUR_ORDER_SUCCESSFULLY_PLACED'  			=> 		'تم تقديم طلبك بنجاح',

'TRANSACTION_ID'  				=> 		'رقم المعاملة',

'PRODUCT_DETAILS_FOR_CURRENT_TRANSACTION'  				=> 		'تفاصيل المنتجات للمعاملة الحالية',

'SUB-TOTAL'  					=> 		'المجموع',

'SHIPPING_TOTAL'  				=> 		'إجمالي رسوم الشحن',

/*end paymentcod.blade.php*/

/*start place_bid_payment.blade.php*/

'CONGRATULATIONS'  				=> 		'تهانينا',

'YOUR_BID_WAS_PLACED_SUCCESSFULLY'  				=> 		'تم تقديم عرضك بنجاح',

'YOU_WILL_RECEIVE_AN_EMAIL_CONFIRMATION_SHORTLY'  	 	=> 		'سوف تستلم بريد إلكتروني للتأكيد قريباً',

'YOUR_BID_AMOUNT'  				=> 		'قيمة العرض',

'ESTIMATED_SHIPPING_CHARGE'  	=> 		'رسوم الشحن التقديرية',

'AUCTION_TIME_REMAINING'    	=> 		'الوقت المتبقي',

'CLOSE_WINDOW'        	    	=> 		'اغلاق النافذة',

'ALTERNATIVE_INFORMATION'       => 		'إذا لم يتم تقديم أي معلومات بديلة خلال 24 ساعة من إغلاق المزاد ، سيتم إستخدام بطاقة الائتمان الافتراضية  وعنوان إرسال الفواتير الخاصة بالمزايد الفائز لشحن الجوائز',

'CREDIT_CARD_WILL_NOT_BE_CHARGED'   => 		'لن يتم تحصيل أي رسوم من بطاقتك الائتمانية حتى إنتهاء المزاد ، وسنقوم فقط بفرض الحد الأدنى للمبلغ اللازم للفوز',

'RANSACTION_FEE' 			  	=> 		'سيتم إضافة رسوم معاملات بنسبة 3٪ بحد أدنى 1.99 دولار أمريكي و 9.99 دولارات أمريكية كحد أقصى لكل طلب في حالة الفوز',

'ORDER_IS_CANCELLED' 			=> 		'إذا تم إلغاء طلبك بسبب رفض بطاقة الائتمان ، فسيتم تقييم رسوم بقيمة 25 دولارًا ',

'APPLICABLE_SALES'	 			=> 		'سيتم إضافة ضريبة المبيعات المطبقة',

'TENNESSEE_ORDERS'	 			=> 		'Tennessee orders',

/*end place_bid_payment.blade.php*/

/*start products.blade.php*/

'MOST_VISITED_PRODUCTS'	 		=> 		'المنتجات الأكثر زيارة',

/*end products.blade.php*/

/*start productview.blade.php*/

'CLICK_TO_VIEW_COUPON_CODE'	 	=> 		'اضغط هنا لعرض كوبون الخصم',

'CASHBACK'	 					=> 		'استرداد النقود الثابتة (بغض النظر عن الكمية)',

'ADD_TO_WISHLIST'	 			=> 		'إضافة الى المفضلة',

'IN_STOCK'  		 			=> 		'متوفر',

'SELECT'  		 				=> 		'إختيار',

'WRITE_A_REVIEW_POST'  		 	=> 		'إكتب تعليق',

'RATE_THIS_PRODUCT'             =>      'قيم هذا المنتج',

'REVIEW_THIS_PRODUCT'             =>      'إكتب تعليق عن المنتح',

'LIMITED_QUANTITY_AVAILABLE'    => 		'الكمية محدودة',

'SELECT_COLOR'   				=> 		'إختر اللون',

'SELECT_SIZE'   				=> 		'إختر المقاس',

'PRODUCT_ADDED_TO_WISHLIST'     => 		'تم إضافة المنتج للمفضلة',

'PRODUCT_ALREADY_EXISTS_IN_YOUR_WISHLIST'     => 		'المنتج موجود بالفعل في المفضلة',

'POST_COMMENTS'     			=> 		'نشر تعليقات',

'SUBMIT_REVIEW'					=>      'إرسال',

'REVIEW_TITLE'                  =>      'عنوان التقييم',

'REVIEW_DESCRIPTION'            =>      'وصف التقييم',

/*end productview.blade.php*/

/*start register.blade.php*/

'WELCOME_TO_USER_REGISTRATION'  => 		'مرحبا بك في تسجيل العملاء',

'CREATE_NEW_USER_ACCOUNT'     	=> 		'إنشاء حساب مستخدم جديد',

'ENTER_YOUR_PASSWORD'    	 	=> 		'أدخل كلمة المرور',

'BY_CLICKING_SIGNUP_I_AGREE_TO' => 		'بالنقر على تسجيل أنا أوافق على',

'THIS_EMAIL_ID_ALREADY_REGISTERED'     => 		'البريد الإلكتروني مسجل مسبقاًً',

'LESSER_MOBILE_NO'    	 		=> 		'رقم الجوال ناقص',

'CORRECT_MOB_NO'     			=> 		'رقم الجوال الصحيح',

'ENTER_10_DIGITS_OF_MOBILE_NO'  => 		'الرجاء إدخال 10 ارقام',

'CORRECT_MOBILE_NO'     		=> 		'رقم الجوال الصحيح',

'PLEASE_ENTER_VALID_MOBILE_NUMBER'     => 		'الرجاء إدخال رقم الجوال بشكل صحيح',

'EMAIL_ID'   					=> 	'البريد الإلكتروني',

/*end register blade.php*/

/*start search.blade.php*/

'SEARCH_FOR_PRODUCTS'     		=> 		'البحث عن منتجات',

'SEARCH_FOR_DEALS'     			=> 		'البحث عن صفقات',

/*end search.blade.php*/

/*start shipping.blade.php*/

'POSTAL_CODE'     				=> 		'الرمز البريدي',

/*end shipping.blade.php*/

/*Start Sold.blade.php*/

'SOLD_PRODUCTS'     			=> 		'المنتجات المباعة',

'Nearbyshop'                    =>      'لم يتم العثور على متاجر في المدينة',

'NO_RECORDS_FOUND_UNDER_PRODUCTS'     			=> 		'لم يتم العثور على سجلات المنتجات',

'SOLD_DEALS'     				=> 		'الصفقات المباعة',

'NO_RECORDS_FOUND_UNDER_DEALS'  => 		'لم يتم العثور على سجلات',

/*end sold.blade.php*/

/*start storview.blade.php*/

'NO_RATING_FOR_THIS_STORE'     	=> 		'لا توجد تقييمات لهذا المتجر بعد',

'MOBILE_NO'     				=> 		'رقم الجوال',

'NO_RECORDS_FOUND_UNDER'     	=> 		'لم يتم العثور على سجلات',

'PRODUCTS'     					=> 		'المنتجات',

'BRANCHES'     					=> 		'الفروع',

'BACK_YOUR_DEAL_STORE_REVIEW_POST_SUCCESSFULLY' => 'تم نشر تعليقك بنجاح',

/*end storview.blade.php*/

/*start submission.blade.php*/

'THANK_YOU_FOR_MERCHANT_REGISTRATION'     	=> 		'شكراً لك على الانضمام الى مزودين الخدمات في قولدن كيجز',

'THANK_YOU_TO_MERCHANT_SIGN_UP'     		=> 		'شكراً للتسجيل معنا',

'YOU_GOT_YOUR_USERNAME_AND_PASSWORD'     	=> 		'تم إرسال اسم المستخدم وكلمة المرور الخاصة بك الى بريدك الإلكتروني ، يرجى الإطلاع والتحقق من ذلك',

'Thank_you_for_signing' =>' ستتلقى تأكيدًا عبر البريد الإلكتروني أيضًا. <br/> <br/> الرجاء <a href="sitemerchant" style="text-decoration:underline"> النقر هنا </a> لتسجيل الدخول',

/*End submission.blade.php*/

/*start stores.blade.php*/

'NO_STORES_FOUND'     			=> 		'لم يتم العثور على متاجر',

/*end stores.php*/

/*Front end static pages*/	

'NO_HALL_FOUND'     			=> 		'لا توجد قاعات مضافة في هذا الفرع',

'NO_SERVICES_FOUND'     			=> 		'لا توجد خدمات مضافة في هذا الفرع',



#############################################  START EMAIL TEMPLATES  ###########################################





/*start admin_passwordrecoverymail.blade.php*/

'CONTACT_INFORMATION'     							=> 		'معلومات التواصل',

/*End admin_passwordrecoverymail.blade.php*/



/*Start merchantmail.blade.php*/

'MAIL_EMAIL_TEMPLATE'     							=> 		'قالب البريد الإلكتروني',

'MAIL_HI'     										=> 		'مرحباً',

'MAIL_YOUR_LOGIN_CREDENTIALS_ARE'     				=> 		'بيانات تسجيل الدخول الخاصة بك',

'MAIL_USER_NAME'     								=> 		'اسم المستخدم',

'MAIL_PASSWORD'     								=> 		'كلمة المرور',

'MAIL_LOGIN_YOUR_ACCOUNT'     						=> 		'سجل الدخول الى حسابك',

'MOBILE_YOUR_MERCHANT_ACCOUNT_WAS_CREATED_SUCCESSFULLY'    => 		'تم إنشاء حساب التاجر الخاص بك بنجاح',



/*End merchantmail.blade.php*/

/*Start ordermail.blade.php*/

'WALLET_APPLIED'     								=> 		'رسوم من المحفظة',

'COUPON_APPLIED'     								=> 		'الكوبون المطبق',

'DISCOUNT_PRICE'     								=> 		'سعر الخصم',

'ACTUAL_PRICE'     									=> 		'السعر الاصلي',

/*End ordermail.blade.php*/



/* Start edit_merchant_account.blade.php */

'PASSWORD_RECOVERY_DETAILS_FOR_USER'=> 			'بيانات استعادة كلمة مرور العميل',

'USER_NAME'							=> 			'اسم المستخدم',

'EMAIL_LINK'						=> 			'رابط البريد الإلكتروني',

'PLEASE_CLICK_THE_LINK_TO_RESET_YOUR_PASSWORD'			=> 			'لاعادة تعيين كلمة المرور يرجى النقر على الرابط',

/* close edit_merchant_account.blade.php */



/* Start edit_merchant_account.blade.php */

'SUBSCRIPTION_EMAIL_CREATED_SUCCESSFULLY_FOR_YOUR_MAIL_ID'			=> 			'تم إشتراكك في القائمة البريدية بنجاح',

'THANK_YOU_CREDENTIALS_ARE'			=> 			'شكراً لك، معلومات تسجيل الدخول',

'HAI_THANK_YOU'						=> 			'شكراً لك',

'SUBSCRIPTION_EMAIL_IN_LARAVEL_ECOMMERCE'			=> 'بريد الإشتراك في قولدن كيجز',

'SUBSCRIPTION_EMAIL_IN'							=> ' للإشتراك في',

'THANK_YOU_FOR_SUBSCRIPTION'      				=>  'شكراً لإشتراكك في النشرة البريدية',

/* close edit_merchant_account.blade.php */



/* Start registermail.blade.php */

'YOUR_REGISTER_ACCOUNT_WAS_CREATED_SUCCESSFULLY'			=> 			'تم إنشاء الحساب بنجاح',

'YOUR_LOGIN_CREDENTIALS_ARE'		=> 			'بيانات تسجيل الدخول الخاصة بك هي',

'PASSWORD'							=> 			'كلمة المرور',

'LOGIN_YOUR_ACCOUNT'				=> 			'سجل الدخول الى حسابك',

'LARAVEL_ECOMMERCE_ALL_RIGHTS_RESERVED'			=> 			'جميع الحقوق محفوظة قولدن كيجز ',

/* close registermail.blade.php */



/* Start product_enquiry.blade.php */

'PRODUCT_ENQUIRY'					=> 			'الإستفسار عن منتج',

'NAME'								=> 			'الاسم',

'PRODUCT_NAME'						=> 			'اسم المنتج',

'PRODUCT_DEAL_NAME'					=>			'اسم المنتج',

'MESSAGE'							=> 			'الرسالة',

/* close product_enquiry.blade.php */



/* Start order-merchantmail.blade.php */

'THANK_YOU_FOR_YOUR_ORDER'			=> 			'شكراً لإختيارك قولدن كيجز. لقد استلمنا طلبك ونعمل عليه. سوف نعود اليك قريبا.',

'YOUR_TRANSACTION_ID'				=> 			'رقم المعاملة',

'VIEW_YOUR_ORDER'					=> 			'عرض الطلب',

'S_NO'								=> 			'التسلسل',

'QTY'								=> 			'الكمية',

'ITEM_PRICE'						=> 			'سعر الصنف',

'TAX'								=> 			'الضريبة',

'SHIP_AMOUNT'						=> 			'رسوم الشحن',

'SUBTOTAL'							=> 			'المجموع',

'TOTAL'								=> 			'الإجمالي',

'2_TO_3_DAYS'						=> 			'2 الى 3 ايام',

'WILL_BE_DELIVERED_BY'				=> 			'سيتم التسليم خلال',

'DELIVERY_ADDRESS'					=> 			'عنوان التسليم',

'ITEM_NAME'							=> 			'اسم الصنف',

'STATE'								=> 			'المدينة',

'ZIP_CODE'							=> 			'الرمز البريدي',

'PHONE'								=> 			'رقم الجوال',

'ADDRESS'							=> 			'العنوان',

'ADDRESS1'							=> 			'العنوان 1',

'ADDRESS2'							=> 			'العنوان 2',

'EMAIL'								=> 			'البريد الإلكتروني',

'ANY_QUESTIONS'						=> 			'أي إستفسارات أخرى',

'GET_IN_TOUCH_WITH_OUR_24X7_CUSTOMER_CARE_TEAM'					=> 			'تواصل مع فريق خدمة العملاء على مدار 24 ساعة طوال أيام الأسبوع',

'MEANWHILE_YOU_CAN_CHECK_THE_STATUS_OF_YOUR_ORDER_ON'				=> 			'في هذه الأثناء ، يمكنك التحقق من حالة الطلب الخاص بك',

'WE_WILL_SEND_YOU_ANOTHER_EMAIL_ONCE_THE_ITEMS_IN_YOUR_ORDER_HAVE_BEEN_SHIPPED'					=> 			'سنرسل لك بريدًا إلكترونيًا آخر بمجرد شحن أصناف طلبك',

/* close order-merchantmail.blade.php */

#############################################  END EMAIL TEMPLATES  ########################################



#################################################### START CONTROLLER WORK ########################################



/* Start RegisterController.php */

'NO_DATAS_FOUND'									=> 		'لا توجد بيانات ',

/* close RegisterController.php */

/* Start ServicelistingController.php */

'ALREADY_EXISTS_IN_CART'							=> 		'موجود بالسلة بالفعل..!!',

'ADDED_TO_CART_SUCCESSFULLY'						=> 		'تمت الاضافة الى السلة بنجاح..!!',

'ADD_TO_CART_FAILED_DUE_TO_SOME_ERROR'				=> 		'حصل خطأ، لم نتمكن من الاضافة الى السلة..!!',

'LOGIN_TO_PROCEED'									=> 		'سجل الدخول للاستمرار..!!',



/* close ServicelistingController.php */

/* Start UserloginController.php */

'LOGIN_SUCCESS'									=> 		'تم تسجيل الدخول بنجاح',

'PASSWORD_RECOVERY_DETAILS'						=> 		'تفاصيل استرداد كلمة المرور',

/* close UserloginController.php */

/*Start FooterController.php*/



'YOUR_COMMENTS_WILL_BE_SHOWN_AFTER_ADMIN_APPROVAL' 	=> 		'سيتم عرض تعليقاتك بعد موافقة المشرف.',

'YOUR_ENQUIRY_DETAILS' 								=> 		'تم إرسال البيانات بنجاح ، سنقوم بالاتصال بك قريبا',

'YOUR_PRODUCT_ENQUIRY' 								=> 		'إستفسار عن المنتج',



/*End FooterController.php*/

#################################################### END CONTROLLER WORK ########################################

/*Home controller merchant mail*/

'BACK_HI_MERCHANT_YOUR_PRODUCT_PURCHASED'        =>        'مرحباً عزيزي البائع! لقد تم شراء منتجك.',

'BACK_HI_CUSTOMER_YOUR_PRODUCT_PURCHASED'        =>        ' تم تقديم بيانات الطلب بنجاح',

/*End Email templates*/

/* order success page - new languages */

'INCLUDING'        	=>        'يشمل',

'TAXES'        		=>        'الضريبة',

/*new*/

'REGISTERED_SUCCESSFULLY' 						=> 'تم التسجيل بنجاح',

'YOUR_COD_SUCCESSFULLY_REGISTERED' 				=> 'تم تسجيل الدفع عند الإستلام بنجاح',

'COD_ACKNOWLEDGEMENT' 							=> 'اشعار الدفع عند الإستلام',

'YOUR_COD_PAYMENT_IS_SUCCESS' 					=> 'تمت عملية الدفع عند الإستلام بنجاح',

'YOUR_ORDER_CONFIRMATION_DETAILS_PLACED_SUCCESSFULLY' 	=> 'تم تقديم بيانات الطلب بنجاح',

'HI_MERCHANT_YOUR_PRODUCT_PURCHASED' 			=> 'مرحباً عزيزي البائع! لقد تم شراء منتجك',



'PAYMENT_RECEIVED_YOUR_PRODUCT_WILL_BE_SENT_TO_YOU_VERY_SOON' 			=> 'تمت عملية الدفع بنجاح. سيتم إرسال المنتج إليك قريباً',

'YOUR_PAYMENT_HAS_BEEN_COMPLETED_SUCCESSFULLY' 	=> 'اكتملت عملية الدفع بنجاح',

'YOUR_PAYMENT_SUCCESSFULLY_COMPLETED' 			=> 'تم الدفع بنجاح',

'PAYMENT_ACKNOWLEDGEMENT' 						=> 'إشعار الدفع',

'YOUR_PAYMENT_HAS_BEEN_FAILED' 					=> 'فشلت عملية الدفع',

'SOME_ERROR_OCCURED_DURING_PAYMENT' 			=> 'حصل خطأ أثناء عملية الدفع',

'YOUR_PAYMENT_HAS_BEEN_CANCELLED' 				=> 'تم إلغاء عملية الدفع',

'ERROR_ALREADY_AUCTION_HAS_BID_TRY_WITH_NEW_AMOUNT' => 'حصل خطأ، أدخل قيمة أخرى',

'ALREADY_EMAIL_EXIST' 							=> 'البريد الإلكتروني موجود مسبقاً',

'ALREADY_USE_EMAIL_EXIST' 						=> ' البريد الإلكتروني المستخدم موجود مسبقاًً',

'EMAIL_HAS_BEEN_SUBSCRIPTION_SUCCESSFULLY' 		=> 'تم تسجيل بريدك الإلكتروني للإشتراك',

'YOUR_EMAIL_SUBSCRIBED_SUCCESSFULLY' 			=> 'تم إشتراك البريد الإلكتروني الخاص بك بنجاح',

'YOUR_PRODUCT_REVIEW_POST_SUCCESSFULLY' 		=> 'تم إرسال تقييمك بنجاح',

'YOUR_DEAL_PRODUCT_REVIEW_POST_SUCCESSFULLY' 	=> 'تم إرسال تقييمك بنجاح',

'YOUR_DEAL_STORE_REVIEW_POST_SUCCESSFULLY' 		=> 'تم إرسال تقييمك بنجاح',

'RATE_FOR_THIS_STORE' 							=> 'الرجاء تقييم المتجر',

'NO_RESULT_FOUND' 								=> 'لم يتم العثور على نتائج',

'MER_COMPLETE'                                  => 'اكتمال',

/* Product page remove wishlist */

'REMOVE_FROM_WISHLIST' 							=> 'الإزالة من المفضلة',	



/* CHECKOUT PAGE  */

'NO_PAYMENT_METHOD_AVAILABLE' 					=> 'لا توجد طرق دفع متاحة. يرجى التواصل بالإدارة للمضي قدما.',	

/* if user not login */

'PLEASE_LOGIN_TO_PROCEED' 						=> 'يرجى تسجيل الدخول للمتابعة', 

'MER_MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY'		=> 'مرحباً بك في قولدن كيجز!', 

'MERCHANT_ACCOUNT_CREATED_SUCCESSFULLY'		    => 'مرحباً بك في قولدن كيجز!', 



/* CArt page */

// Product UnAvailible message 

'PRODUCT_QUANTITY_NOT_AVAILABLE' 				=> 'المعذرة، الكمية المحددة غير متاحة', 

'PRODUCT_QUANTITY_NOT_AVAILABLE_MAX' 				=> 'معذرة، لقد وصلت الحد الاقصى للكمية المتاحة', 



'CART_UPDATED' 									=> 'تم تحديث السلة', 

'PRODUCT_QUANTITY_SHOULD_BE_GREATER_THAN_ZERO'	=> 'يجب ان تكون الكمية اكبر من 0',

'DEAL_QUANTITY_SHOULD_BE_GREATER_THAN_ZERO'		=> 'يجب ان تكون الكمية اكبر من 0',



'DEAL_QUANTITY_NOT_AVAILABLE'					=> 'الكمية غير متاحة',

'PLEASE_SELECT_MINIMUM_PAYMENT_METHOD'			=> 'الرجاء إختيار طريقة دفع',

'PRODUCT_QUANTITY_NOT_AVAILABLE'				=> 'الكمية غير متاحة',

/* Register success message */

'BACK_REGISTER_ACCOUNT_CREATED_SUCCESSFULLY'	=> 	'تم إنشاء الحساب بنجاح',







/* mail add coupon and wallet details */

'UNIT_PRICE'									=>	'سعر الوحدة',

'WALLET_AMOUNT'									=>	'المحفظة',

'GRAND_TOTAL'									=>	'الإجمالي',

//Order merchant mail

'MERCHANT_YOUR_PRODUCT_PURCHASED_FOR_DETAILS'	=>  'تم شراء منتجك. يرجى الإطلاع على التفاصيل. ',

//Policy

'CANCELLATION_ONLY'								=> 'الإلغاء فقط',

'RETURN_ONLY'									=> 'الإرجاع فقط',

'REPLACEMENT_ONLY'								=> 'الإستبدال فقط',

'CANCELLATION'									=> 'الإلغاء',

'RETURN'										=> 'الإرجاع',

'REPLACE'										=> 'الإستبدال',

'ORDER_CANCELLATION'							=> 'إلغاء الطلب',

'ORDER_RETURN'									=> 'إرجاع الطلب',

'ORDER_REPLACE'									=> 'إستبدال الطلب',

'SELECT_ITEM_TO_CANCEL'							=> 'إختر صنف للإلغاء',

'SELECT_ITEM_TO_RETURN'							=> 'إختر صنف للإرجاع',

'SELECT_ITEM_TO_REPLACE'						=> 'إختر صنف للإستبدال',

'REASON_FOR_CANCEL'								=> 'سبب الإلغاء',

'REASON_FOR_RETURN'								=> 'سبب الإرجاع',

'REASON_FOR_REPLACE'							=> 'سبب الإستبدال',

'ENTER_YOUR_REASON_FOR_CANCEL'					=> 'أدخل سبب الإلغاء',

'ENTER_YOUR_REASON_FOR_RETURN'					=> 'أدخل سبب الإرجاع',

'ENTER_YOUR_REASON_FOR_REPLACE'					=> 'أدخل سبب الإستبدال',

'CONFIRM_CANCEL'								=> 'تأكيد الإلغاء',

'CONFIRM_RETURN'								=> 'تأكيد الإرجاع',

'CONFIRM_REPLACE'								=> 'تأكيد الإستبدال',

'ORDER_NOT_VALID'								=> 'الطلب غير صحيح',

'REASON_TO_CANCEL'								=> 'سبب الإلغاء',

'REASON_TO_RETURN'								=> 'سبب الإرجاع',

'REASON_TO_REPLACE'								=> 'سبب الإستبدال',

'ORDER_ID'										=> 'رقم الطلب',

'CANCEL_DATE'									=> 'تاريخ الإلغاء',

'REQUEST_DATE'									=> 'تاريخ الطلب',

'RETURN_DATE'									=> 'تاريخ الإرجاع',

'REPLACE_DATE'									=> 'تاريخ الإستبدال',

'APPROVED_OR_DISAPPROVED_DATE'					=> 'قبول/رفض التاريخ',

'APPROVED_OR_DISAPPROVED_REASON'				=> 'قبول/رفض السبب',

'ORDER_CANCELLATION_REQUEST'					=> 'طلب إلغاء',

'ORDER_RETURN_REQUEST'							=> 'طلب استرجاع',

'ORDER_REPLACE_REQUEST'							=> 'طلب إستبدال',

'YOU_ARE_SEND_A_CANCEL_REQUEST_FOR_THE_FOLLOWING_PRODUCT'	=> 'أنت ترسل طلب إلغاء للمنتج التالي. ستتم معالجة طلبك بعد قليل.',

'YOU_ARE_SEND_A_RETURN_REQUEST_FOR_THE_FOLLOWING_PRODUCT'	=> 'أنت ترسل طلب إرجاع للمنتج التالي. ستتم معالجة طلبك بعد قليل.',

'YOU_ARE_SEND_A_REPLACE_REQUEST_FOR_THE_FOLLOWING_PRODUCT'	=> 'أنت ترسل طلب إستبدال للمنتج التالي. ستتم معالجة طلبك بعد قليل.',



'YOUR_RETURN_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS'	=> 'تتم معالجة طلب إرجاع المنتج. الرجاء الإطلاع على التفاصيل. <br> شكرًا. ',

'YOUR_REPLACE_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS'	=> 'تتم معالجة طلب إستبدال المنتج. الرجاء الإطلاع على التفاصيل. <br> شكرًا. ',

'CUSTOMER_RETURN_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS'	=> 'تتم معالجة طلب إرجاع منتج العميل. الرجاء الإطلاع على التفاصيل. <br> شكرًا. ',

'CUSTOMER_REPLACE_REQUEST_IS_PROCESSED_PLEASE_FIND_THE_DETAILS'	=> 'تتم معالجة طلب إستبدال منتج العميل. الرجاء الإطلاع على التفاصيل. <br> شكرًا. ',



'CUSTOMER_SEND_A_CANCEL_REQUEST_FOR_THE_FOLLOWING_PRODUCT'	=> 'ارسل العميل طلب إلغاء طلبية للمنتج التالي',

'CUSTOMER_SEND_A_RETURN_REQUEST_FOR_THE_FOLLOWING_PRODUCT'	=> 'ارسل العميل طلب إرجاع طلبية للمنتج التالي',

'CUSTOMER_SEND_A_REPLACE_REQUEST_FOR_THE_FOLLOWING_PRODUCT'	=> 'ارسل العميل طلب إستبدال طلبية للمنتج التالي',



'ADMIN_DELIVERY_STATUS_PRODUCT_PACKED'	=> 'تم تجهيز منتجك وسيتم شحنه قريبًا',

'ORDER_PACKED'	=> 'Order Packed',

'ADMIN_DELIVERY_STATUS_PRODUCT_DISPATCHED'	=> 'تم إرسال منتجك وسيتم تسليمه قريبًا',

'ORDER_DISPATCHED'	=> 'Order Dispatched',

'ADMIN_DELIVERY_STATUS_PRODUCT_DELIVERED'	=> 'تم توصيل طلبك',

'ORDER_DELIVERED'	=> 'Order Delivered',

'ADMIN_DELIVERY_STATUS_PRODUCT_CANCELLED'	=> 'تم إلغاء الطلب من قبل المدير',

'ORDER_CANCELLED'	=> 'Order Cancelled',

'ADMIN_DELIVERY_STATUS_PRODUCT_REPLACED'	=> 'تم إستبدال المنتج',

'ORDER_REPLACED'	=> 'Order Replaced',

'ADMIN_DELIVERY_STATUS_PRODUCT_RETURNED'	=> 'تم إرجاع المنتج',

'ORDER_RETURNED'	=> 'Order Returned',



// Wisitech language



'VALIDFILE' => 'الرجاء رفع ملف صحيح',

'VALIDFILEPDF' => 'الرجاء رفع ملف pdf',

'VALIDSHOPLINK' => 'الرجاء إدخال رابط المتجر في معروف',



'CertificateChamber' => 'السجل التجاري ',



'HOME'	=> 'المنزل',

'ABOUTUS'	=> 'حول',

'Testimonials'	=> 'الشهادات والتوصيات',

'Occasions'	=> 'المناسبات',

'PrivacyPolicy'	=> 'سياسة الخصوصية',

'ReturnPolicy'	=> 'سياسة الإرجاع',

'ContactUs'	=> 'تواصل معنا',

'Occasions'	=> 'المناسبات',

'Address'	=> 'العنوان',

'PaymentOptions'	=> 'خيارات الدفع',

'SecureShopping'	=> 'تسوق آمن',

'FollowUS'	=> 'تابعنا',

'PrivacyPolicy'	=> 'سياسة الخصوصية',

'ReturnPolicy'	=> 'سياسة الإرجاع',

'AllRightsReserved'	=> 'جميع الحقوق محفوظة',

'SignIn'	=> 'تسجيل الدخول',

'SignUp'	=> 'التسجيل',

'SignOut'	=> 'تسجيل الخروج',

'BusinessMeetings'	=> 'إجتماعات الأعمال',

'WeddingandOccasions'	=> 'الأفراح والمناسبات',

'TypeofBusiness'	=> 'نوع الإجتماع',

'NumberofAttendees'	=> 'عدد الحضور',

'Budget'	=> 'الميزانية',

'City'	=> 'المدينة',

'StartDate'	=> 'تاريخ البدء',

'EndDate'	=> 'تاريخ الإنتهاء',

'OccasionDate'	=> 'تاريخ المناسبة',

'OccasionType'	=> 'نوع المناسبة',

'MER_SELECT_CITY'=>'إختيار المدينة',

'GENDER'=>'النوع',

'OccasionDateDuration'	=> 'تاريخ المناسبة / المدة',

'HallOccasion'	=> 'القاعة / الموقع',



'About_Hall'=>'حول القاعة',

'Hall_Dimension'=>'مساحة القاعة',

'currency'=>'ر.س',

'Facilities_Offered_Here'=>'الخدمات المجانية',

'Prepaid_Services'=>'الخدمات المدفوعة',

'Food'=>'إضافة طعام',

'EXFood'=>'طعام خارجي',

'CHECKFOOD'=>'الطعام',



'CARTBUFFET'=>'البوفيهات',

'CARTDATES'=>'التمور',

'CARTDESSERTS'=>'الحلويات',

'GET_MY_CURRENT_LOCATION'=>'تحديد موقعي تلقائياً',

'Hall_Services'=>'خدمات القاعة',

'What_Our_Client_Says'=>"أراء العملاء",

'noofattendees'=>'الرجاء إدخال عدد الحضور',

'budget'=>'يجب ان تكون الميزانية اكبر من 10 ر.س',

'occasiondate'=>'الرجاء إدخال تاريخ المناسبة',

'weddingoccasiontype'=>'الرجاء تحديد نوع المناسبة',

'startdate'=>'الرجاء إختيار تاريخ البدء',

'enddate'=>'الرجاء إختيار تاريخ الإنتهاء',

'Select_Business_Type'=>'إختر نوع الإجتماع',

'Select_Occasion_Type'=>'إختر نوع المناسبة',

'HOTEL_HALLS'=>'قاعات الفنادق',

'USER_NAME'=>'الرجاء إدخال الاسم.',

'USER_EMAIL'=>'الرجاء إدخال بريدك الإلكتروني',

'USER_MOBILE'=>'الرجاء إدخال رقم الجوال.',

'COUNTRY_CODE_MSG'=>'الرجاء إدخال كود الدولة',

'COUNTRY_CODE'=>'كود الدولة',

'USER_PASSWORD'=>'الرجاء إدخال كلمة المرور.',

'USER_NEME_LEVEL'=>'الاسم',

'USER_MOBILE_LEVEL'=>'رقم الجوال',

'THANKYOU'=>'شكراً لإشتراكك معنا',

'THANKYOUHEADDING'=>'شكراً',

'Reserve_Now'=>'حجز الآن',

'ModifySearch'=>'تعديل البحث',



'About_Shop'=>'حول',

'BEST_SHOP'=>'افضل متجر',

'Services'=>'إضافة خدمات',

'Video'=>'الفيديو',

'Choose_Package'=>'إختر خدمة',





//Wisitech language



//Coupon error on cart page

'COUPON_EXCEEDS_PRODUCT_PURCHASE_AMOUNT' => 'قيمة خصم الكوبون تتجاوز المبلغ الإجمالي للطلبية. لا يمكن تطبيق الكوبون هذه المرة.',

'CUSTOMER_BLOCKED' => 'يبدو أن حساب المستخدم الخاص بك غير نشط ، يرجى الاتصال بنا للحصول على مزيد من التفاصيل',

'ALL_RIGHTS_RESERVED' => 'جميع الحقوق محفوظة',

'WALLET_USED' => 'تم إستخدام المحفظة',



'EMAILADDRRESS' => 'عنوان البريد الإلكتروني',

'PASSWORD'  => 'كبمة المرور',

'MER_FORGOT_PASSWORD' => 'نسيت كلمة المرور؟',

'MER_BACK_TO_LOGIN' => 'العودة الى شاشة الدخول',

'MER_ENTER_YOUR_VALID_E-MAIL' => 'البريد الإلكتروني',

'MER_SIGN_IN' => 'تسجيل الدخول',

'NEWMEMBER' => 'عضو جديد؟ ',

'SIGNUP' => 'التسجيل',



/*start forgot_pwd_mail.blade.php*/

'MER_ADMIN_DASHBOARD_TEMPLATE' 			=> 			'قالب لوحة تحكم المدير ',

'MER_FORGOT_PASSWORD_PAGE' 				=> 			'صفحة إعادة تعيين كلمة المرور',

'MER_MERCHANT_PASSWORD_RESET1' 			=> 			'إعادة تعيين كلمة المرور',

'MER_NEW_PASSWORD' 						=> 			'كلمة المرور الجديدة',

'MER_CONFIRM_PASSWORD' 					=> 			'تأكيد كلمة المرور',

'MER_BACK_TO_LOGIN' 					=> 			'العودة الى شاشة تسجيل الدخول',

'MER_PLEASE_PROVIDE_NEW_PASSWORD' 		=> 			'الرجاء إدخال كلمة مرور جديدة',

'MER_PLEASE_PROVIDE_CONFIRM_PASSWORD' 	=> 			'الرجاء تأكيد كلمة المرور',

'MER_BOTH_PASSWORDS_DO_NOT_MATCH' 		=> 			'كلمتا المرور غير متطابقة',

/*Start MerchantLoginController.php*/



'MER_LOGIN_SUCCESS' 								=> 		'تم تسجيل الدخول بنجاح',

'MER_LOGOUT_SUCCESS' 								=> 		'تم تسجيل الخروج بنجاح',

'MER_INVALID_USERNAME_AND_PASSWORD' 				=> 		'بيانات الدخول غير صحيحة',

'MER_PASSWORD_RECOVERY_DETAILS' 					=> 		'رابط إعادة تعيين كلمة المرور',

'MER_MAIL_SEND_SUCCESSFULLY' 						=> 		'تم إرسال التعليمات إلى بريدك الإلكتروني. يرجى التحقق من بريدك الإلكتروني لإعادة تعيين كلمة المرور الخاصة بك.',

'MER_INVALID_EMAIL' 								=> 		'يبدو أنك أدخلت بريدًا إلكترونيًا غير صالح. يرجى التحقق وإدخال البريد الإلكتروني المسجل لدينا.',

'MER_PASSWORD_CHANGED_SUCCESSFULLY' 				=> 		'تم تغيير كلمة المرور بنجاح',



/*End MerchantLoginController.php*/	



/*start merchant_login.blade.blade.php*/

'MER_MERCHANT_DASHBOARD_TEMPLATE' 		=> 			'قالب لوحة تحكم التاجر',

'MER_LOGIN_PAGE' 						=> 			'صفحة تسجيل الدخول',

'MER_LOGO' 								=> 			'الشعار',

'MER_MERCHANT_LOGIN' 					=> 			'تسجيل دخول التاجر',



'MER_PASSWORD' 							=> 			'كلمة المرور',

'MER_ENTER_YOUR_VALID_E-MAIL' 			=> 			'أدخل البريد الإلكتروني',

'MER_RECOVER_PASSWORD' 					=> 			'إعادة تعيين كلمة المرور',

'MER_PLEASE_FILL_DETAILS_TO_REGISTER' 	=> 			'يرجى ملء التفاصيل للتسجيل',

'MER_REGISTER' 							=> 			'الانضمام الآن',

'MER_FORGOT_PASSWORD' 					=> 			'نسيت كلمة المرور؟',

'MER_ENTER_YOUR_EMAIL' 					=> 			'أدخل عنوان البريد الإلكتروني',

'MER_ENTER_YOUR_VALID_EMAIL' 			=> 			'أدخل عنوان بريدك الإلكتروني',

'MER_YOUR_E-MAIL' 						=> 			'عنوان بريدك الإلكتروني',

'MER_FIRST_NAME' 						=> 			'الاسم الاول',

'MER_LAST_NAME' 						=> 			'الاسم الاخير',

'MER_USERNAME' 							=> 			'اسم المستخدم',

'MER_RE_TYPE_PASSWORD' 					=> 			'اعد إدخال كلمة المرور',

'MER_EMAIL' 							=> 			'البريد الإلكتروني',

'Other_Branches'=>'الفروع الأخرى',



/*end merchant_login.blade.blade.php*/	

/* start internal food menu*/

'External_Food' 							=> 			'الطعام من الخارج',

'Internal_Food' 							=> 			'قائمتنا',

'TOTAL_QUALITY'  => 'الجودة الشاملة',

'PRICE'  => 'السعر',

'CONTAINER_LIST'  => 'قائمة الحافظات',

'ADD_TO_CART'  => 'اضف الى السلة',

'ORDER_SUMMARY'  => 'ملخص الطلب',

'Select_Branch'=>'إختر فرع',

/* end internal food menu*/

/*Start merchantmail.blade.php*/

'BACK_WALLET'						=> 'المحفظة',



'MAIL_EMAIL_TEMPLATE'     							=> 		'قالب البريد الإلكتروني',

'MAIL_HI'     										=> 		'مرحبا',

'MAIL_YOUR_LOGIN_CREDENTIALS_ARE'     				=> 		'بيانات تسجيل الدخول الخاصة بك :',

'MAIL_USER_NAME'     								=> 		'اسم المستخدم',

'MAIL_PASSWORD'     								=> 		'كلمة المرور',

'MAIL_LOGIN_YOUR_ACCOUNT'     						=> 		'الدخول الى حسابك',

'MOBILE_YOUR_MERCHANT_ACCOUNT_WAS_CREATED_SUCCESSFULLY'    => 		'تم إنشاء حساب التاجر بنجاح',



/*End merchantmail.blade.php*/

/*start merchant_passwordrecoverymail.blade.php*/

'MER_PASSWORD_RECOVERY_DETAILS_FOR_MERCHANT' 		=> 		'تفاصيل استرداد كلمة المرور للتاجر',

'MER_YOUR_LOGIN_CREDENTIALS_ARE' 					=> 		'بيانات تسجيل الدخول الخاصة بك',

'MER_USER_NAME' 									=> 		'اسم المستخدم',

'MER_EMAIL_LINK' 									=> 		'إعادة تعيين كلمة المرور',

'MER_PLEASE_CLICK_THE_LINK_TO_RESET_YOUR_PASSWORD' 	=> 		'لاعادة تعيين كلمة المرور. الرجاء النقر على الرابط',



/*start Ajit*/

'MyAccount'	                         => 'حسابي',

'FILE_NUMBER'	                         => 'رقم الملف',

'MyDashboard'	                         => 'لوحة التحكم',

'MyProfile'	                         => 'الملف الشخصي',

'TelephoneNumber'	                     => 'رقم الجوال',

'GENDER'	                             => 'الجنس',

'DATE_OF_BIRTH'	                     => 'تاريخ الميلاد',

'MY_PAYMENT_METHOD'	                 => 'طرق الدفع',

'MY_PAYMENT'	                         => 'الدفع',

'PIN_CODE'	                             => 'الرمز البريدي',

'UPLOAD_YOUR_NATIONAL_ID'	             => 'رفع صورة الهوية الوطنية',

'SAVE'	                                 => 'حفظ',

'MALE'	                                 => 'ذكر',

'FEMALE'	                             => 'انثى',

'BOTH'	                                 => 'كلاهما',

'PLEASE_ENTER_YOUR_NAME'                => 'الرجاء إدخال الاسم',

'ENTER_YOUR_NAME_HERE_MAX'              => 'الرجاء إدخال ما لا يزيد عن 100 حرف',

'PLEASE_ENTER_YOUR_EMAIL'               => 'الرجاء إدخال بريدك الإلكتروني',

'ENTER_YOUR_EMAIL_HERE_VALID'           => 'الرجاء إدخال عنوان بريد إلكتروني صحيح',

'ENTER_YOUR_TEEPHONE_NUMBER_HERE'       => 'الرجاء إدخال رقم الجوال',

'SELECT_YOUR_GENDER_HERE'               => 'الرجاء تحديد النوع',

'ALLOW_ONLY_DIGITS'                     => 'مسموح بالارقام فقط',

'ENTER_YOUR_NUMBER_HERE_MIN'            => 'الرجاء إدخال 5 أرقام على الأقل',

'ENTER_YOUR_NUMBER_HERE_MAX'            => 'الرجاء التحقق من رقم هاتفك',

'ENTER_YOUR_DOB_HERE'                   => 'الرجاء تحديد تاريخ الميلاد',

'SELECT_YOUR_CITY_HERE'                 => 'الرجاء إختيار مدينتك',

'ENTER_YOUR_PIN_CODE_HERE'              => 'الرجاء إدخال الرمز السري',

'ENTER_YOUR_PIN_CODE_HERE_MAX'          => 'الرجاء التحقق من الرمز السري',

'PLEASE_ENTER_YOUR_ADDRESS'             => 'الرجاء إدخال العنوان',

'WELCOME'                               => 'مرحبا',

'ChangePassword'                        => 'تغيير كلمة المرور',

'OLD_PASSWORD'                          => 'كلمة المرور القديمة',

'NEW_PASSWORD'                          => 'كلمة المرور الجديدة',

'CONFIRM_PASSWORD'                      => 'تأكيد كلمة المرور',

'PLEASE_ENTER_YOUR_OLD_PASSWORD'        => 'الرجاء إدخال كلمة المرور القديمة',

'PLEASE_ENTER_YOUR_NEW_PASSWORD'        => 'الرجاء إدخال كلمة المرور الجديدة',

'PLEASE_ENTER_YOUR_CONFIRM_PASSWORD'    => 'الرجاء تأكيد كلمة المرور',

'PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN'    => 'الرجاء إدخال على الأقل 8 خانات',

'PLEASE_ENTER_YOUR_PASSWORD_MATCH'      => 'الرجاء إدخال نفس القيمة مجدداً',

'MYOCCASIONS'                           => 'نوع المناسبة',

'MY_ACCOUNT_OCCASIONS'                  => 'المناسبات',

'MY_ORDERS'                             => 'طلباتي',

'MY_ACCOUNT_STUDIO'                     => 'تفاصيل الاستوديو',

'MY_ACCOUNT_SECURITY'                   => 'التأمين',

'MY_WALLET'                             => 'المحفظة',

'MY_ACCOUNT_REVIEWS'                    => 'التقييمات',

'INSURANCE'                             => 'التأمين',

'Sl_No'                                 => 'المتسلسل',

'OCCASIONS_NAME'                        => 'اسم المناسبة',

'ALL'                                   => 'الكل',

'UPLOAD_YOUR_PHOTO'                     => 'ارفع صور المناسبة',

'VENUE'                                 => 'مكان المناسبة',

'UPLOAD_YOUR_PIC'                       => 'قم برفع صور المناسبة',

'DELETES'                               => 'حذف',

'OCCASION_TYPE'                         => 'نوع المناسبات',

'ADD_SAR'                               => 'إضافة مبلغ للمحفظة',

'WALLET_BALANCE'                        => 'رصيد المحفظة',

'CHOOSE_AN_AMOUNT'                      => 'إختر مبلغ',

'ENTER_AMOUNT'                          => 'أدخل المبلغ',

'CREDIT_CARD'                           => 'بطاقة الائتمان',

'DEBIT_CARD'                            => 'بطاقة الخصم',

'NET_BANKING'                           => 'حوالة بنكية',

'NODOCTOR'                           => 'لا يتوفر أطباء في هذه العيادة! الرجاء إختيار عيادة أخرى',

'CHOOSE_AN_OPTION'                      => 'حدد خيار',

'CONTINUE'                              => 'استمرار',

'TYPE_OF_OCCASION'                      => 'نوع المناسبة',

'OCCASION_DATE'                         => 'تاريخ المناسبة',

'SERVICES_PROVIDER'                     => 'مزود الخدمة / المتجر',

'REVIEW' 						         => 'تقييم',

'WRITEYOUR_REVIEW' 					 => 'إكتب تعليقك',

'UPCOMING_OCCASIONS' 					 => 'أحدث الطلبات',

'LATEST_PHOTO_GALLERY' 				 => 'أحدث صور المناسبات',

'ADD_MONEY' 				             => 'إضافة مبلغ',

'LATEST_REVIEW' 				         => 'أحدث التقييمات',

'ALL_REVIEWS' 				             => 'كل التقييمات',

'NO_OF_ATTENDANCE' 				     => 'عدد الحضور',

'LIST_OF_SERVICES' 				     => 'قائمة الخدمات',

'DELIVERY_DATE' 				         => 'تاريخ التسليم',

'TOTAL_PRICE' 				             => 'السعر الإجمالي',

'PLEASE_ENTER_YOUR_COMMENT'             => 'الرجاء كتابة تعليق',

'BACK_TO_OCCASION_LIST' 				 => 'العودة الى قائمة الطلبات',

'BACK_TO_STUDIO_LIST' 				     => 'العودة الى قائمة المناسبات',

'ENTER_YOUR_STUDIO_DATE'                => 'الرجاء إختيار تاريخ',

'SELECT_YOUR_OCCASION'                  => 'الرجاء تحديد نوع المناسبة',

'ENTER_YOUR_VENUE'                      => 'الرجاء إدخال مكان المناسبة.',

'PLZ_ENTER_YOUR_VENUE'                  => 'الرجاء إدخال مكان المناسبة.',

'PLZ_SELECT_YOUR_INVITATION_TYPE'       => 'الرجاء إختيار نوع الدعوات',

'PLEASE_UPLOAD_IMAGE'                   => 'الرجاء رفع صورة',

'VALIDATION_VALID_IMAGE'                => 'الرجاء رفع صورة صالحة',

'MER_SERVICE_ADDMORE'                   => 'إضافة المزيد',

'VIEW_ALL'                              => 'عرض الكل',

'NO_RECORD_FOUND'                       => 'لا توجد سجلات متاحة',

'ABOUT_SINGER'                          => 'حول المطرب',

'MORE'                                  => 'المزيد',

'LESS'                                  => 'عرض أقل',

'WHAT_OUR_CLIENT'                       => "أراء العملاء",

'SINGER_INFORMATION'                    => "معلومات المطرب",

'LOCATION'                              => "العنوان",

'HALL'                                  => "القاعة",

'OTHER_HALL'                            => "قاعات أخرى",

'SELECT_HALL'                          => "إختر قاعة",

'DURATION'                              => "المدة",

'HEADINGONE' => 'أنا مهتم بخدماتك. اود ان تشاركني في مناسبتي',

'HEADINGTWO' => 'أرجو منك تزويدي بالأسعار والتفاصيل اللازمة للإداء في المناسبة بحسب تفاصيل المناسبة بالأسفل',

'TIME'                                  => 'الوقت',

'SELECT_TIME'                           => 'إختر الوقت',

'ASK_FOR_A_QUOTE'                       => 'اطلب عرض سعر',

'ENTER_YOUR_HALL'                       => 'الرجاء إدخال اسم القاعة.',

'PLEASE_YOUR_ENTER_LOCATION' 		     => 'الرجاء إدخال العنوان.',

'PLEASE_ENTER_YOUR_DURATION'            => 'الرجاء إدخال المدة.',

'PLEASE_ENTER_YOUR_DATE'                => 'الرجاء تحديد التاريخ',

'PLEASE_ENTER_YOUR_TIME'                => 'الرجاء إختيار الوقت',

'ABOUT_BAND'                            => 'حول الفرقة',

'BAND_INFORMATION'                      => 'معلومات الفرقة',

'ENQUIRY_SUCCESSFULLY'                  => 'طلب عرض سعر',

'ABOUT_ACOUSTIC'                        => 'حول المتجر',

'RECORDING'                             => 'التسجيل',

'EQUIPMENT'                             => 'الأجهزة',

'THE_GROOM_NAME'                        => "الاسم",

'RECORDING_SECTION'                     => 'قسم التسجيل',

'MUSIC_TYPE'                            => 'نوع الموسيقى',

'SINGER_NAME'                           => 'اسم المطرب',

'SONG_NAME'                             => 'اسم الأغنية',

'PLEASE_YOUR_ENTER_GROOM_NAME' 		 => "الرجاء إدخال الاسم",

'PLEASE_YOUR_ENTER_RECOR_SEC' 		     => 'الرجاء تعبئة بيانات قسم التسجيل',

'PLEASE_YOUR_ENTER_MUSIC_TIPE' 		 => 'الرجاء إدخال نوع الموسيقى.',

'PLEASE_YOUR_ENTER_SINGER_NAME' 		 => 'الرجاء إدخال اسم المطرب',

'PLEASE_YOUR_ENTER_SONG_NAME' 		     => 'الرجاء إدخال اسم الاغنية',

'GET_A_QUOTE' 		                     => 'احصل على عرض سعر',

'REQUEST_A_QUOTE' 		                 => 'عروض الأسعار',

'PLEASE_ENTER_RATING' 		             => 'الرجاء إدخال تقييم',

'NOTES' 		                         => 'ملاحظات',

'RENT' 		                         => 'ايجار',

'BUY' 		                             => 'شراء',

'QUOTE_FOR' 		                     => 'عرض سعر لـ',

'REQUEST_DATE' 		                 => 'تاريخ الطلب',

'REPLY_BY' 		                     => 'الحالة',

'NOTIFICATION' 		                 => 'الاشعارات',

'ARABIC' 		                         => 'عربى',

'PLEASE_ENTER_AMOUNT'                   => 'الرجاء إدخال المبلغ.',

'SELECT_QUOTE'                          => 'إختيار عرض سعر',

'SINGER'                                => 'المطرب',

'POPULAR_BAND'                          => 'الفرقة الشعبية',

'RECORDING'                             => 'التسجيل',

'REQUEST_A_QUOTE_DETAIL'                => 'العودة لقائمة العروض',

'CONFIRMED'                             => 'تم التأكيد',

'DENY_BTN'                             => 'رفض',

'DENY'                                  => 'لم يتم قبول طلبك',

'DENYBYCUSTOMER'                                  => 'تم رفض العرض من قبلك',

'BACK_TO_REQUEST_QUOTE'                 => 'العودة الى قائمة العروض',

'WAITING'                               => 'جاري الانتظار',

'Not_available'                               => 'غير متاح',

'REPLY_BY_MERCHANT'                     => 'تم الرد من قبل مزود الخدمة',

'EQUIPMENT_FOR_RENT'                    => 'اجهزة للايجار',

'OCCASIONS_DATE'                        => 'تاريخ المناسبة',

'RETURN_DATE'                           => 'تاريخ الإرجاع',

'INSURANCE_AMOUNT'                      => 'مبلغ التأمين',

'REMAINING_AMOUNT'                      => 'المبلغ المتبقي',

'HALL_PRICE'                            => 'سعر القاعة',

'ADVANCE_PAYMENT'                       => '25% دفعة مقدمة',

'FOOD_FROM_OUTSIDE'                     => 'الطعام من الخارج',

'OUR_MENU'                              => 'قائمتنا',

'BOOK_NOW'                              => 'احجز الآن',

'SHOP_BY'                               => 'شراء حسب',

'PER_OURR' 				             => ' بالساعة',

'PLEASE_ENTER_OCCASSION_DATE' 		     => 'الرجاء إدخال تاريخ المناسبة.',

'PLEASE_ENTER_OCCASSION_NAME' 		     => 'الرجاء إدخال اسم المناسبة.',

'PLEASE_ENTER_RETURN_DATE' 		     => 'الرجاء إدخال تاريخ الإرجاع',

'PLEASE_ENTER_INSUR_AMOUNT' 		     => 'الرجاء إدخال مبلغ التأمين',

'ABOUT_CENTER' 		                 => 'حول المكتب',

'PACKAGE_SECHEDULE' 		             => 'مواعيد الباقة',

'PACKAGE_DATE' 		                 => 'تاريخ الباقة',

'PACKAGE_DURATION' 		             => 'مدة الباقة',

'SELECT_PACKAGE'   					=>'إختر الباقة',

'PRICE_PER_KM' 		                 => 'السعر في اليوم',

'CAR_MODEL' 		                     => 'سنة الصنع',

'SERVICE_DATE' 		                 => 'تاريخ الخدمة',

'SERVICE_DAY' 		                     => 'ايام الخدمة',

'EXTRS_SERVICE' 		                 => 'خدمات اضافية',

'PLEASE_SELECT_PACKAGE_SECHEDULE'       => 'الرجاء تحديد موعد الباقة.',

'OTHER_SERVICE'                         => 'خدمات اخرى',

'ABOUT_SHOP'                            => 'حول المتجر',

'BODY_MEASUREMENT'                      => 'القياسات',

'LENGTH'                                => 'الطول',

'CHEST_SIZE'                            => 'قياس الصدر',

'WAIST_SIZE'                            => 'قياس الخصر',

'SHOULDERS'                             => 'الأكتاف',

'NECK'                                  => 'الرقبة',

'Width'                                  => 'العرض',

'ARM_LENGTH'                            => 'طول الذراع',

'WRIST_DIAMETER'                        => 'فتحة الكم',

'CUTTING'                               => 'شكل القصة',

'NORMAL'                                => 'عادي',

'AMPLE'                                 => 'واسع',

'CURVED'                                => 'منحني',

'CM'                                    => 'سم',

'Area'                                    => 'المساحة',

'CONTINUE_WITH_THESE_MEASURES'          => 'الاستمرار بهذه القياسات',

'SIZE_DESSESS'                          => 'المقاس',

'PRICE_FOR_PURCHASE'                    => 'سعر الشراء',

'CALIBER'                               => 'العيار',

'WEIGHT'                                => 'الوزن',

'GRAM'                                  => 'غرام',

'WRITE_ON_YOUR_RING'                    => 'إكتب على',

'PLEASE_ENTER_YOUR_LENGTH'              => 'الرجاء إدخال الطول',

'ENTER_YOUR_MAX'                        => 'الرجاء إدخال الطول الأقصى',

'PLEASE_ENTER_YOUR_CHEST_SIZE'          => 'الرجاء إدخال قياس الصدر',

'PLEASE_ENTER_YOUR_WAIST_SIZE'          => 'الرجاء إدخال قياس الخصر',

'PLEASE_ENTER_YOUR_SHOULDERS'           => 'الرجاء إدخال عرض الاكتاف',

'PLEASE_ENTER_YOUR_NECK'                => 'الرجاء إدخال قياس الرقبة',

'PLEASE_ENTER_YOUR_ARM_LENGTH'          => 'الرجاء إدخال طول الذراع',

'PLEASE_ENTER_YOUR_WRIST_DIA'           => 'الرجاء إدخال قياس فتحة الكم',

'PLEASE_SELECT_YOUR_SIZE'               => 'الرجاء تحديد المقاس',

'PLEASE_SELECT_REDIO_BUTTON'            => 'الرجاء تحديد نوع القصة',

'READY_MADE'                            => 'جاهزة',

'TAILOR'                                => 'تفصيل',

'SORRY_QTY_PRODUCT'                     => 'المعذرة ! لا تتوفر كمية لهذا المنتج أكثر من الكمية المحددة',

'SHOPPING'                              => 'التسوق',

'ABAYA'                                 => 'العبايات',

'CAR_RENTAL'                            => 'تأجير السيارات',

'TRAVEL_AGENCY'                         => 'المكاتب السياحية',

'TRAVEL'                         => 'المكاتب السياحية',

'MUSIC'                                 => 'الصوتيات',

'GOLD_AND_JEWELRY'                      => 'المجوهرات',

'PERFUME'                               => 'العود والعطور',

'DRESSES'                               => 'الفساتين',

'INVITATION_DETAILS'                    => 'تفاصيل الدعوات',

'OCCASSION_NAME'                        => 'اسم المناسبة',

'INVITATION_TYPE'                       => 'نوع الدعوات',

'TEXT_MESSAGE'                          => 'نص الرسالة',

'INVITATION_CARD'                       => 'كرت الدعوة',

'NUMBER_OF_INVITATION'                  => 'عدد المدعوين',

'ELECTRONIC_INVITATION'                 => 'الدعوات الالكترونية',

'KOSHA'                                 => 'الكوش',

'PhotoGraphy'                           => 'التصوير الفوتغرافي',

'VideoGraphy'                           => 'الفيديو',

'ROSES'                                 => 'الورود',

'SpecialEvents'                         => 'المناسبات الخاصة',

'PLEASE_ENTER_VALID_EMAIL'              => 'الرجاء إدخال عنوان بريد إلكتروني صحيح.',

'PLEASE_ENTER_CHARACTERS'               => 'الرجاء إدخال 8 خانات على الأقل',

'ADD'                                   => 'إضافة',

'PAY_NOW'                               => 'ادفع الآن',

'PLEASE_SELECT_CONTAINER_FIRST'         => 'الرجاء إختيار الحافظة أولاً',

'NO_OF_PEOPLE'                          => 'عدد الاشخاص',

'Golden_Cages'                          => 'قولدن كيجز',

'THIS_ITEM_ALREADY_ADDED_IN_CART'       => 'تم إضافة هذا العنصر الى السلة مسبقاًً', 

'DYANAMIC_ADDRESS'                      => 'الدمام، المملكة العربية السعودية',

'TYPE'                                  => 'النوع',

'DISH'                                  => 'الصحون',

'PER_KILO'                              => 'بالكيلو',

'PER_PIECE'                             => 'بالقطعة',

'PER_QTY'                               => 'بالكمية',

'Reception_andHospitality'              => 'الإستقبال والضيافة',

'NO_SERVICES_AVAILABLE'     			 => 'لا توجد خدمات مدرجة',

'BOOKING_DATE'     			         => 'تاريخ الحجز',

'RETURNING_DATE'     			         => 'تاريخ الإرجاع',

'PLEASE_ENTER_YOUR_BOOKING_DATE'        => 'الرجاء إدخال تاريخ الحجز',

'PLEASE_ENTER_YOUR_RETUNRING_DATE'      => 'الرجاء إدخال تاريخ الإرجاع',

'OCCASION_NAME'                         => 'اسم المناسبة',

'TOTAL_MEMBER'                          => 'عدد الاشخاص',

'TOTAL_BUDGET'                          => 'الميزانية',

'ORDER'                                 => 'الطلب',

'TRANSACTION_ID'                        => 'رقم المعاملة',

'PAYER_NAME'                            => 'اسم العميل',

'TOTAL_ORDER_AMOUNT'                    => 'إجمالي قيمة الطلب',

'TOTAL_TAX_AMOUNT'                      => 'إجمالي الضريبة',

'WALLET_AMOUNT'                         => 'مبلغ المحفظة ',

'HALL_PENDING_AMOUNT'                   => 'متبقي تكلفة القاعة',

'SHIPPING'                              => 'الشحن',

'DELIVERY_TIME'                         => 'وقت التوصيل',

'SHIPPING_IMG'                          => 'صورة الشحن',

'SHIPPING_CHARGE'                       => 'رسوم الشحن',

'PRODUCT_TYPE'                          => 'نوع المنتج',

'PRODUCT_TITLE'                         => 'اسم المنتج',

'PRODUCT_PRICE'                         => 'سعر المنتج',

'INSURANCE_AMOUNT'                      => 'مبلغ التأمين',

'SIZES'                                 => 'المقاس',

'Fabrics'                               => 'القماش',

'Pending'                               => 'قيد الانتظار',

'Process'                               => 'قيد التنفيذ',

'Delivered'                             => 'مكتمل',

'Hold'                                  => 'معلق',

'Failed'                                => 'فشل',

'PLEASE_ENTER_THE_SAME_VALUE'           => 'الرجاء إدخال نفس القيمة مجددا',

'PAYMENT_INFORMATION'                   => 'معلومات الدفع',

'BANK_NAME'                             => 'اسم البنك',

'ACCOUNT_NO'                            => 'رقم الحساب',

'MAP_URL'                               => 'رابط الموقع على الخريطة',

'ADDRESS_IMAGE'                         => 'صورة الموقع',

'PLEASE_ENTER_YOUR_BANK_NAME'           => 'الرجاء إدخال اسم البنك',

'PLEASE_ENTER_YOUR_ACCOUNT_NO'          => 'الرجاء إدخال رقم الحساب.',

'VENDOR_REPLY'                          => 'رد مقدم الخدمة',

'VIEW_DETAILS'                          => 'عرض التفاصيل',

'VIEW_DETAIL'                           => 'عرض التفاصيل',

'REVIEWS_READ_AND_WRITE'                => 'عرض واضافة تقييم',

'BACK_TO_REVIEW'                        => 'العودة الى قائمة التقييمات',

'YOUR_RATING'                           => 'تقييمك',

'APPROVED'                              => 'تمت الموافقة',

'WAITING_FOR_APPROVED'                  => 'بانتظار الموافقة',

'BACK_TO_ORDER_SUMMARY'                 => 'العودة الى ملخص الطلب',

'ORDER_DETAILS'                         => 'تفاصيل الطلب',

'SERVICE_NAME'                          => 'الخدمة',

'HOME_VISIT_CHARGE'                     => 'رسوم الزيارة المنزلية',

'BOOKING_PLACE'                         => 'مكان تقديم الخدمة',

'BOOKING_DATE_AND_TIME'                 => 'تاريخ ووقت الحجز',

'START_TIME'                            => 'وقت البدء',

'BOOKING_TIME'                            => 'وقت الحجز',

'END_TIME'                              => 'وقت الإنتهاء',

'STAFF_NAME'                            => 'العامل',

'BOOKING_TIME'                          => 'وقت الحجز',

'SHOP_NAME'                             => 'اسم المتجر',

'FOR'                                   => 'لـ',

'RENT_CHARGES'                          => 'رسوم الايجار/باليوم',

'RENT_CHARGES_HR'                          => 'رسوم الايجار/بالساعة',

'AGENCY_NAME'                           => 'اسم مكتب السفريات',

'CAR_MODEL'                             => 'سنة الصنع',

'PRICE_PER_DAY'                         => 'السعر في اليوم',

'HOTAL_NAME'                            => 'اسم الفندق',

'FOOD_ORDERED'                          => 'تم طلب الطعام',

'DISHT_TYPE'                            => 'نوع الطبق',

'DISHT_NAME'                            => 'اسم الطبق',

'CONTAINER'                             => 'الحافظة',

'DISHT_IMAGES'                          => 'صورة الطبق',

'CONTAINER_IMAGES'                      => 'صورة الحافظة',

'BOOKING_TIME'                          => 'وقت الحجز',

'PRODUCT_IMAGE'                         => 'الصورة',

'DESIGN'                                => 'التصميم',

'Choose_Design'                         => 'إختر تصميم',

'Choose_Dress'                          => 'إختر فستان',

'Choose_PRODUCT'                        => 'إختر منتج',

'PRODUCT_NAME'                          => 'اسم المنتج',

'DRESS'                                 => 'الفستان',

'COD'                                   => 'الدفع عند الإستلام',

'CREDIT_DEBIT'                          => 'بطاقات الائتمان/الخصم',

'TRANSACTION_ID'                        => 'رقم المعاملة',

'SHIPPING_METHOD'                       => 'طريقة الشحن',

'SHIPPING_CHARGE'                       => 'رسوم الشحن',

'VAT_CHARGE'                            => 'ضريبة القيمة المضافة',

'STYLE_AND_FABRIC_INFO'                 => 'معلومات التصميم والقماش',

'STYLE'                                 => 'التصميم',

'STYLE_PRICE'                           => 'سعر التصميم',

'STYLE_IMAGE'                           => 'صورة التصميم',

'FABRICS_NAME'                          => 'اسم القماش',

'FABRICS_PRICE'                         => 'سعر القماش',

'FABRICS_IMAGE'                         => 'صورة القماش',

'PAID_SERVICE'                          => 'الخدمات المدفوعة',

'CLINIC_NAME'                           => 'اسم العيادة',

'DEPARTMENT'                            => 'القسم',

'DR_NAME'                               => 'اسم الطبيب',

'FLOWER_TYPE'                           => 'نوع الورد',

'WRAPPING_TYPE'                         => 'نوع التغليف',

'WRAPPING_DESIGN'                       => 'تصميم التغليف',

/*End Ajit*/

'View_Details' => 'عرض التفاصيل',

'Within_Budget' => 'ضمن الميزانية',

'Above_Budget' => 'فوق الميزانية',

'Offers' => 'العروض',

'STAFF_CHARGES' => 'رسوم العامل',

'sales_rep_code' =>'كود المندوب',

'KOSHA' =>'الكوش',

'PHOTOGRAPHY_STUDIO'=>'استوديو التصوير',

'RECEPTION_AND_HOSPITALITY'=>'الإستقبال والضيافة',

'special_event'=>'المناسبات الخاصة',



'Design_Your_Kosha'=>'صمم كوشتك',

'Ready_Made_Kosha'=>'الكوش الجاهزة',

'No_product_found_in_this_category' =>'لا توجد منتجات في هذا التصنيف',

'No_product_found_in_this_package' =>'لا توجد منتجات في هذه الباقة',

'No_product_found_in_this_shop' =>'لا توجد منتجات في هذا المتجر',

'Your_Selections' =>'إختياراتك',

'Total_Price' =>'السعر الإجمالي',

'Photography'=>'التصوير الفوتغرافي',

'Videography'=>'الفيديو',

'Duration_of_Video'=>'زمن الفيديو (ساعة)',

'Number_of_Cameras'=>'عدد الكاميرات',

'Number_of_Pictures'=>'عدد الصور',

'ROSES'=>'الورود',

'Package'=>'الباقات',

'Single'=>'مفردة',

'BridalPackage'=>'الباقات',

'Personal_Info'=>'معلومات الشحن',

'Order_Summary'=>'ملخص الطلب',

'Name'=>'الاسم',

'Email'=>'البريد الإلكتروني',

'Telephone_Number'=>'رقم الجوال',

'Gender'=>'النوع',

'Date_of_Birth'=>'تاريخ الميلاد',

'DD'=>'اليوم',

'MM'=>'الشهر',

'YYYY'=>'السنة',

'Country'=>'الدولة',

'Pin_Code'=>'الرمز البريدي',

'Order_Summary'=>'ملخص الطلب',

'Cash_on_Delivery_of_following_products_and_services'=>'الدفع عند الإستلام للمنتجات والخدمات التالية',

'Select_Shipping_Companies_of_Following_Product'=>'الرجاء إختيار شركة الشحن',

'Payment_Option'=>'خيارات الدفع',

'Mobile_Wallets'=>'المحفظة',

'Wire_Transfer'=>'الدفع عند الإستلام', 

'Male'=>'ذكر', 

'Female'=>'انثى', 

'Credit_Debit_Cards'=>'بطاقة الائتمان/الخصم', 

'Total_Price'=>'السعر الإجمالي', 

'Pay_Now'=>'ادفع الآن', 

'PLEASE_ENTER_YOUR_PHONE' =>'الرجاء إدخال رقم الهاتف',

'PLEASE_CHOOSE_YOUR_GENDER' =>'الرجاء تحديد النوع',

'PLEASE_SELECT_DAY' =>'الرجاء تحديد اليوم',

'PLEASE_SELECT_MONTH' =>'الرجاء تحديد الشهر', 

'PLEASE_SELECT_YEAR' =>'الرجاء تحديد السنة', 

'PLEASE_ENTER_YOUR_PINCODE'=>'الرجاء إدخال الرمز البريدي',

'PLEASE_CHOOSE_SHIPPING_METHOD' =>'الرجاء تحديد طريقة الشحن',

'PLEASE_CHOOSE_PAYMENT_METHOD'=>'الرجاء تحديد طريقة الدفع',

'BOOKINGFOR'=>'الرجاء إختيار نوع الموعد',

'BOOKINGDATE'=>'الرجاء إختيار تاريخ الحجز',

'BOOKINGTIME'=>'الرجاء إختيار وقت الحجز',

'checkout_thank_you'=>'شكراً لإختيارك قولدن كيجز. لقد استلمنا طلبك ونعمل عليه. نتواصل معك قريبا.',

'Select'=>'إختيار',

'PLEASE_SELECT_COUNTRY'=>'الرجاء تحديد الدولة',

'Aramex'=>'أرامكس',

'Pick_Logistics'=>'بِك',

'Pay_Now'=>'ادفع الآن',

'VAT'=>'ضريبة القيمة المضافة',

'Wallet_Deduct'=>'رائع! رصيد المحفظة يغطي قيمة الطلب',

'MAKEUPCOSMETIC'=>'مستحضرات التجميل',

'BEAUTY_CENTERS'=>'المشاغل النسائية',

'Number_of_Flowers'=>'عدد الورود',

'SPA_CENTERS'=>'المنتجعات الصحية',

'BARBAR_CENTERS'=>'صالونات الحلاقة',

'MAKEUP_ARTIST'=>'خبيرات التجميل',

'Add_to_Cart' =>'اضف الى السلة',

'BOOK_APPOINTMENT'=>'حجز موعد',

'Wrapping_Type'=>'نوع التغليف',

'Wrapping_Design'=>'تصميم التغليف',

'FILE_NO'=>'الرجاء إدخال رقم الملف',

'PATIENT_STATUS'=>'حالة المريض',

'NEW'=>'جديد',

'EXISTING'=>'موجود',

'EXISTING'=>'موجود',

'ENTER_FILE_NO'=>'أدخل رقم الملف',

'Quantity'=>'الكمية',

'COSMETICS_AND_LASER'=>'عيادات التجميل والليزر',

'DENTAL_AND_DERMATOLOGY'=>'عيادات الجلدية والأسنان',

'PROFESSIONAL_BIO'=>'التخصص',

'Design_your_Package'=> 'صمم باقتك',

'Number_of_Staff'=>'عدد العاملين',

'Nationality'=>'الجنسية',

'Date'=>'التاريخ',

'Time'=>'الوقت',

'SHOP'=>'المتجر',

'Choose_Staff'=>'إختر العامل',

'booknow'=>'احجز الآن',

'Insurance_amount'=>'مبلغ',

'ADD'=>'إضافة',

'OCCASION'=>'تفاصيل المناسبة',

'OCCASIONDATE'=>'تاريخ المناسبة',

'Currency'=>'العملة',

'STOPMESSAGE'=>'الرجاء تحديد نوع المناسبة / الإجتماع ،عدد الحاضرين والميزانية في معايير البحث',

'Consultation_Fees' =>'رسوم الاستشارة',

'Terms_Conditions'=>'الشروط والأحكام',

'Photos' =>'الصور',

'Your_username_or_password_is_incorrect' =>'اسم المستخدم او كلمة المرور غير صحيحة',

'Do_you_want_delete_this_record' =>'هل تريد بالتأكيد حذف هذا السجل؟',

'MAIL_ADDRESS' =>'العنوان',

'MAIL_NAME' =>'الاسم',

'MAIL_PHONE' =>'رقم الجوال',

'MAIL_EMAIL' =>'البريد الإلكتروني',

'Hall_CAPICITY'=>'سعة القاعة',

'Not_found_above_budget'=>'لم نتمكن من العثور على قاعة فوق الميزانية',

'Not_found_offer_budget'=>'لا توجد عروض متوفرة حالياً. تحقق لاحقاً',

'Hall_external_content'=>'إذا كنت ترغب في إضافة الطعام من الخارج ، قم بإضافته من قسم الطعام.',

'external_food_message'=>'تم حجز القاعة. الرجاء إختيار الطعام',

'Total_Shipping'=>'رسوم الشحن',

'STAFFSLECT'=>'الرجاء إختيار العامل',

'Advance_Payment'=>'دفعة مقدمة ',

'Remaining_Payment'=>'المبلغ المتبقي',

'No_shipping_required'=>'لا يحتاج شحن',

'Merchant_Login'=>'تسجيل دخول التاجر',

'Please_select_city'=>'الرجاء إختيار نوع المناسبة',

'Yrs_Exp'=>'سنوات الخبرة',

'Advance_Payment_for_Hall_Booking'=>'25% دفعة مقدمة لتأكيد حجز القاعة',

'Items_Ordered'=>'الخدمات / الأصناف المطلوبة',

'Payable_Amount'=>'المبلغ المستحق',

'Balance_Payment'=>'متبقي من تكلفة القاعة',

'Men_and_Women'=>'رجال ونساء',

'Meter'=>'متر',

'Total_Cost'=>'إجمالي المبلغ',

'File_No'=>'رقم الملف:',

'HOME_CHARGES'=>'رسوم الزيارة المنزلية ',

'Staff_Availibility_Chart'=>'جدول حجوزات العاملين',

'please_click_here_to_view_order'=>'عرض تفاصيل الطلب',

'CONTINUE_PLANNING'=>'مواصلة التخطيط',



'Thanks_heading' => 'شكراً لإختيارك قولدن كيجز',

'Thanks_text' => 'لقد استلمنا طلبك ونعمل عليه. سنتواصل معك قريبا.',

'NO_OF_PEOPLE'=>'عدد الاشخاص',

'PEOPLE'=>'اشخاص',

'FOR'=>'لـ',

'Specialist'=>'اخصائي  ',

'Already_Booked_at'=>'محجوز مسبقاًً',

'Pleaseselectanotherslot'=>'الرجاء إختيار وقت آخر',

'SELECT_DOC' =>'إختر طبيب',

'Appointment_for'=>'موعد لـ',

'Please_select_other_date'=>'الرجاء إختيار تاريخ آخر',

'Book_a_package' =>'احجز باقة',

'Choose_CAR' =>'إختر نوع السيارة',

'Hall_Type' =>'نوع القاعة',

'Booking_date_time'=>'تاريخ ووقت الحجز',

'Return_date_time'=>'تاريخ ووقت الإرجاع',

'recording' =>'التسجيل',

'band' =>'الفرق الشعبية',

'singer' =>'المطرب',

'No_product_added_in_this_section' =>'لا توجد منتجات مضافة في هذا القسم',

'choose_items' =>'إختر أصناف',

'Your_COMMENTS'=>'ملاحظاتك',

'DRESS_FOR_RENT'=>'فستان للايجار',

'Size_Chart'=>'دليل القياسات',

'Style' =>'التصميم',

'Approx_deleivery_date_before' =>'الحد الأدنى للتسليم خلال ',

'Approx_deleivery_date_after' =>' ايام',

'Price'=>'السعر',

'Please_enter_correct_value'=>'الرجاء إدخال قيمة صحيحة',

'Select_items' =>'إختر الأصناف',

'Choose_Kosha'=>'إختر الكوشة',

'Sold_Out'=>'بيعت كاملاً',

'Worker'=>'العامل',

'CHOSSE_FLOWER'=>'إختر الورد',

'SpecialRoses'=>'الورود',

'Confirm_and_pay'=>'التأكيد والدفع',

'Please_choose_Occasion_Type'=>'الرجاء إختيار نوع المناسبة',

'CHKFILE' =>'تنزيل ملف معاينة CSV',

'Occasionss'=>'#مناسبات',

'Short_By' =>'فرز بـ',

'View_All' =>'عرض الكل',

'PLEASE_ENTER_YOUR_NAME' =>'الرجاء إدخال الاسم',

'PLEASE_ENTER_YOUR_EMAIL'=>'الرجاء إدخال عنوان بريدك الإلكتروني',

'PLEASE_ENTER_MESSAGE'=>'الرجاء إدخال رسالة',

'Message'=>'الرسالة',

'Inquiry_Form'=>'نموذج الإستفسار',

'Submit' =>'إرسال',

'There_is_no_record_available'=>'لا توجد سجلات متاحة',

'APPLY_COUPONS' =>'أدخل كوبون الخصم',

'APPLY' =>'تطبيق',

'DONE'=>'تم',

'APPLIED_COUPONS' =>'الكوبونات المستعملة',

'Pleaseenter_couponcode'=>'الرجاء إدخال رمز الكوبون',

'Pleaseenter_validcouponcode' =>'الرجاء إدخال رمز كوبون صحيح',

'CouponAlready_applied'=>'تم تطبيق الكوبون مسبقاً',

'Remove'=>'حذف',

'couponAmt'=>'قيمة الكوبون',

'coupon_code'=>'رمز الكوبون',

'COUPON' =>'كوبون الخصم',

'Flower_Type' =>'نوع الورد',

'No_of_invites' =>'عدد الدعوات',

'No_of_Attended' =>'عدد المدعوين',

'Attended_List' =>'قائمة المدعوين',

'Attended_Name' =>'الاسم',

'Attended_Email' =>'البريد الإلكتروني',

'PLEASE_ENTER__GREATER_OCCASSION_DATE' =>'يجب أن يكون تاريخ العودة أكبر من تاريخ المناسبة',

];









?>
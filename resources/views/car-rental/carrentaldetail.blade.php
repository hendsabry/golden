@include('includes.navbar')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
@endphp
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
              <?php if(isset($vendordetails->mc_video_description) && $vendordetails->mc_video_description!=''){ ?>
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
        <?php } ?>
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
          <li><a href="#choose_package">{{ (Lang::has(Session::get('lang_file').'.Choose_CAR')!= '')  ?  trans(Session::get('lang_file').'.Choose_CAR'): trans($OUR_LANGUAGE.'.Choose_CAR')}}</a></li> 
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap service-wrap diamond_space">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?><li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
               <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>               
                    <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">{{$vendordetails->mc_name}}</div>
          <div class="detail_hall_description">{{$vendordetails->address}}</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_CENTER')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_CENTER'): trans($OUR_LANGUAGE.'.ABOUT_CENTER')}}</div>
          <div class="detail_about_hall"><div class="comment more">{{strip_tags($vendordetails->mc_discription)}}</div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: <span><?php
		    $getcityname = Helper::getcity($vendordetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?></span></div>
		  <?php if(isset($vendordetails->google_map_address) && $vendordetails->google_map_address!=''){ 
        $lat  = $vendordetails->latitude;
$long  = $vendordetails->longitude; ?>
		  <div class="detail_hall_dimention" id="map" style="height: 230px!important"> 

 
 
 
      </div>
		  <?php } ?>
		  </div>
        </div>
      </div>
 
      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
	  <?php if(isset($vendordetails->mc_video_description) && $vendordetails->mc_video_description!=''){ ?>
        <div class="service-video-area">
          <div class="service-video-cont">{{$vendordetails->mc_video_description}}</div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <!-- service-video-area -->
		<?php } if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
				 @foreach($allreview as $val)
				 @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
				  @endforeach			  
                </ul>
              </div>
            </section>
          </div>
        </div>
		<?php } ?>
      </div>
      <!-- service-mid-wrapper -->
      <!--service-display-section-->
	  
      <div class="service-display-section"> 

@if(count($productlist) >=1)

	  <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> @php  $i=1; @endphp
                @php  $k=count($productlist);  @endphp
                @php if($k<6){ @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
<?php
                    $img = str_replace('thumb_','',$getallcats->pro_Img);
                  ?>
                  
                  <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a href="javascript:void(0);" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                    <div class="category_wrapper <?php if($k==3){?> category_wrapper3<?php } ?>" style="background:url({{ $img or '' }});">
                      <div class="category_title category_title">
                        <div class="category_title_inner">
                          {{$getallcats->pro_title}}
                        </div>
                      </div>
                    </div>
                    </a> </div>
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                <!------------ 6th-------------->
                @php }elseif($k==6){ @endphp
                @php $j=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                  @php if($j==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($j==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($j==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($j==6){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="javascript:void(0);" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($j==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==4){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($j==5){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($j==6){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $j=$j+1; @endphp
                  @endforeach </div>
                <!------------ 7th-------------->
                @php }elseif($k==7){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                  
                  @php if($l==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($l==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($l==7){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="javascript:void(0);" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==7){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!------------ 8th-------------->
                @php }elseif($k==8){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                  @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                  @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                  
                  @php if($l==1){ $classrd='category_wrapper1'; @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_3of5 rows5row"> @php } @endphp 
                      @php if($l==4){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp
                          @php if($l==8){ $classrd='category_wrapper9'; @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="javascript:void(0);" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==8){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                
                @php }elseif($k==9){ @endphp
                <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                  @foreach($productlist as $getallcats)
                  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
                  
                  
                  @php if($i==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($i==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($i==4){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($i==7){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp 
                          @php if($i==9){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="javascript:void(0);" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')"> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});"> <span class="category_title"><span class="category_title_inner">
                            {{$getallcats->pro_title}}
                            </span></span> </span> </a> @php if($i==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    @php if($i==9){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp 
                  
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                @php } @endphp 
				
				</div>
          </div>
		  </div>
          <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
		  <div class="" align="center"> {{ $productlist->links() }}</div>
        </div>
        <!-- service-display-right -->
		{!! Form::open(['url' => 'addtocarttravelandcarproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
        <div class="service-display-left">
		<?php if(isset($productlist[0]->pro_disprice) && $productlist[0]->pro_disprice!='' && $productlist[0]->pro_disprice >=1){
      $getPrice = $productlist[0]->pro_disprice;
      //$dprice=
    }else{

      $getPrice = $productlist[0]->pro_price;

    } 
      ?>
		    <input type="hidden" id="category_id" name="category_id" value="{{ $category_id }}">
			<input type="hidden" id="subcat_id" name="subcat_id" value="{{ $subcat_id }}">
			<input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9" readonly />
			<input type="hidden" name="actiontype" value="carrentaldetail">
			<input type="hidden" name="cart_sub_type" value="car">
            <input type="hidden" name="cart_type" value="car_rental">
			 <b class="travel-sucss">{{ Session::get('status') }}</b>
			<span id="selectedproduct">
			<input type="hidden" id="product_id" name="product_id" value="{{ $productlist[0]->pro_id }}">




			<input type="hidden" id="priceId" name="priceId" value="{{currency($getPrice, 'SAR',$Current_Currency, $format = false)}}">
			<input type="hidden" id="vendor_id" name="vendor_id" value="{{$productlist[0]->pro_mr_id}}">
          <div class="service-left-display-img product_gallery">
@php    $pro_id = $productlist[0]->pro_id; @endphp
             @include('includes/product_multiimages')


          </div>
          <div class="service-product-name">{{ $productlist[0]->pro_title }}</div>
          <div class="service-beauty-description">@php echo nl2br($productlist[0]->pro_desc); @endphp</div>
		</span>
          <div class="car_rent_section">
			<?php $get_car_model = Helper::getProductName($productlist[0]->pro_id,$productlist[0]->pro_mr_id,'Car Model'); if(isset($get_car_model->value) && $get_car_model->value!=''){  ?>
            <div class="car_model" id="carmodelnone">
              <div class="car_text">{{(Lang::has(Session::get('lang_file').'.CAR_MODEL')!= '')  ?  trans(Session::get('lang_file').'.CAR_MODEL'): trans($OUR_LANGUAGE.'.CAR_MODEL')}}</div>
              <div class="car_text_type" id="locid"><?php echo $get_car_model->value; ?></div>
            </div>
			<?php } ?>
            <div class="car_model" id="priceperkmnone">
              <div class="car_text"> {{ (Lang::has(Session::get('lang_file').'.PRICE_PER_KM')!= '')  ?  trans(Session::get('lang_file').'.PRICE_PER_KM'): trans($OUR_LANGUAGE.'.PRICE_PER_KM')}}</div>
              <span class="newprice" id="newprice"></span>
              <div class="car_text_type" id="pppriceperkm">

                @php if(isset($productlist[0]->pro_disprice) && $productlist[0]->pro_disprice!='' && $productlist[0]->pro_disprice!='0'){
                  echo   '<del>'.currency($productlist[0]->pro_price, 'SAR',$Current_Currency).'</del>';
                } 
                @endphp
               {{ currency($getPrice, 'SAR',$Current_Currency) }}</div>
            </div>


 



			<?php $get_service_date = Helper::getProductName($productlist[0]->pro_id,$productlist[0]->pro_mr_id,'Service Date'); if(isset($get_service_date->value) && $get_service_date->value!=''){ ?>
            <div class="car_model" id="ppservicedaynone">
              <div class="car_text">{{ (Lang::has(Session::get('lang_file').'.SERVICE_DATE')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_DATE'): trans($OUR_LANGUAGE.'.SERVICE_DATE')}}</div>
              <div class="car_text_type" id="ppserviceday"><?php echo $get_service_date->value; ?></div>
            </div>
			<?php } ?>
			<div class="books_male_area">
              <div class="book_label"> {{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}} </div>
              <div class="book_input">
                <input name="bookingdate" type="text" id="bookingdate" autocomplete="off" class="wisitechCal t-box cal-t valid returningdateCal " readonly="">
              </div>
            </div>
			<div class="books_male_area">
              <div class="book_label"> {{ (Lang::has(Session::get('lang_file').'.RETURNING_DATE')!= '')  ?  trans(Session::get('lang_file').'.RETURNING_DATE'): trans($OUR_LANGUAGE.'.RETURNING_DATE')}} </div>
              <div class="book_input">
                <input name="returningdate" type="text" id="returningdate" autocomplete="off" class="t-box cal-t valid wisitechCal " readonly="">
              </div>
            </div>
		  </div>

		  <span id="show_price_id" style="display:none;">
          <div class="total_food_cost">
		   
            <div class="total_price">{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: <span id="total_price_final"> {{$getPrice}}</span></div>
          </div>
          
          <div class="btn_row">

            <input type="submit" name="submit" id="submit" value="@if (Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '') {{  trans(Session::get('lang_file').'.BOOK_NOW') }} @else  {{ trans($OUR_LANGUAGE.'.BOOK_NOW') }} @endif" class="form-btn addto_cartbtn">
          </div>
		  <?php if(isset($vendordetails->terms_conditions) && $vendordetails->terms_conditions!=''){ ?>
		  <div class="terms_conditions"><a  href="{{ $vendordetails->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}</a></div>
		  <?php } ?>
		  </span>
       <a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a>
          <!-- container-section -->
        </div>
		{!! Form::close() !!}
        <!-- service-display-left -->

@else



@endif


      </div>
 @include('includes.other_services')
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
@include('includes.footer') 
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
 function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }
function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"{{url('getmultipleImages')}}?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}


function showProductDetail(str,vendorId)
{ 
	   $.ajax({
	   type:"GET",
	   url:"{{url('getCarRentalInfo')}}?product_id="+str+'&vendor_id='+vendorId,
	   success:function(res)
	   {               
	
  if(res)
    { 
		  var json = JSON.stringify(res);
		  var obj = JSON.parse(json);
         <?php $Cur = Session::get('currency'); ?>
		  length=obj.productdateshopinfo.length;
		  //alert(length);
		  if(length>0)
		  {
	         for(i=0; i<length; i++)
		     {   
			   if(obj.productdateshopinfo[i].pro_disprice !='' && obj.productdateshopinfo[i].pro_disprice >=1){var getPrice = obj.productdateshopinfo[i].pro_disprice;}else{var getPrice = obj.productdateshopinfo[i].pro_price;}
			 
         if(getPrice < obj.productdateshopinfo[i].pro_price)
			   {   
			      var pamount   = parseFloat(getPrice).toFixed(2);
				  var disamount = parseFloat(obj.productdateshopinfo[i].pro_price).toFixed(2);  

              disamount = getChangedPrice(disamount);
              //getPrice = getChangedPrice(getPrice);
 

				  $('#newprice').html('');	
				  $('#total_price_final').html('<?php echo $Cur; ?> '+getPrice);
				  $('#priceperkmnone').html('<del><?php echo $Cur; ?> '+disamount+'</del> <?php echo $Cur; ?> '+getPrice);	
			   }	
			   else
			   {
 
            getPrice = getChangedPrice(getPrice);
            $('#newprice').html('');  
			     $('#total_price_final').html('<?php echo $Cur; ?> '+getPrice);	
				 $('#priceperkmnone').html('<?php echo $Cur; ?> '+getPrice);	
				 $('.newprice').html('');	
			   }
			       
              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+getPrice+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img product_gallery"> </div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');  

              checkgallery(str);           		  
		     }
		  }	  
		  if($.trim(obj.productatrcarmodel) !=1)
		  {
		   $('#locid').html(obj.productatrcarmodel);
			$('#carmodelnone').css('display','block');
		  }

		$('html, body').animate({
		   scrollTop: ($('.service-display-left').offset().top)
		}, 'slow');
      
	    }
	   }
	 });
}
</script>
<script type="text/javascript">
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
@include('includes.popupmessage') 
<script>
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {          
          "bookingdate" : {
            required : true
          },
          "returningdate" : {
            required : true
          },
         },
         messages: {
          "bookingdate": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_BOOKING_DATE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_BOOKING_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_BOOKING_DATE') }} @endif"
          },
          "returningdate": {
            required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_RETUNRING_DATE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_RETUNRING_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_RETUNRING_DATE') }} @endif"
          },
         }
 });
 jQuery(".addto_cartbtn").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
 }); 
 

  var d = new Date(); 
  d.setDate(d.getDate());
  var currMonth = d.getMonth();
  var currYear = d.getFullYear();
  var currDay = d.getUTCDate();
  var ymMonth = (d.getMonth() + 1)
  var startDate = new Date(currYear, currMonth, currDay);
  var endsDate = new Date(currYear, ymMonth, currDay);

  $('#bookingdate').datepicker({
  //format: "yyyy-mm-dd",  
  format: "d M yyyy",  
  startDate:startDate,
  endDate:endsDate,
  }); 


  $('.returningdateCal').change(function()
  {
 
 	  var bookingdate = $('#bookingdate').val();
    
	  $('#returningdate').datepicker({
			format: "d M yyyy",
			startDate: bookingdate,
 
	  });

 
  });


 
  
  $('.wisitechCal').change(function()
  {
	  var bookingdatenew = $('#bookingdate').val();
	  var returningdate = $('#returningdate').val();
	  var price = $('#priceId').val();
	  
    if(returningdate !='' && bookingdatenew!='')
    {
	  $('#show_price_id').show();
      var date1 = new Date(bookingdatenew);
      var date2 = new Date(returningdate);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      var DateDif = parseInt(diffDays)+1;//alert(DateDif);
      var total_price = parseFloat(price) * parseInt(DateDif);
	  var TotalPrice = parseFloat(total_price).toFixed(2);

var BOOK_NOW = "@if (Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '') {{  trans(Session::get('lang_file').'.BOOK_NOW') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOK_NOW') }} @endif";

var Not_available = "@if (Lang::has(Session::get('lang_file').'.Not_available')!= '') {{  trans(Session::get('lang_file').'.Not_available') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Not_available') }} @endif";
 
 //TotalPrice = getChangedPrice(TotalPrice);   
 <?php $Cur = Session::get('currency'); ?>
      $('#total_price_final').html('<?php echo $Cur; ?> '+TotalPrice); 
	  $('#itemqty').val(DateDif);
  var product_id = $('#product_id').val();
   $.ajax({
     async: false,
     type:"GET",
     url:"{{url('checkcardemo')}}?bookingdate_from="+bookingdatenew+"&bookingdate_to="+returningdate+"&product_id="+product_id,
     success:function(res)
     {  
        if(res == '1')
        {
          $('#submit').val(BOOK_NOW);
         $('#submit').prop('disabled', false);
        }
        else
        {
        $('#submit').val(Not_available);
       $('#submit').prop('disabled', true);
        }
     }
   });




    }
  });  



});
</script>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
</script>


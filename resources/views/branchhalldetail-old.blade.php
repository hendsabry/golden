<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 
<title>Golden-Cages</title>
<link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/themes/images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="{{url('/')}}/themes/css/reset.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/common-style.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/stylesheet.css" rel="stylesheet" />


<link href="{{url('/')}}/themes/slider/css/demo.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/slider/css/flexslider.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/user-my-account.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface-media.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/diamond.css" rel="stylesheet" />
<!-- custome scroll 
<link href="mousewheel/jquery.mCustomScrollbar.css" rel="stylesheet" />
-->
<!--<link href="css/diamond.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />-->

<script src="{{url('/')}}/themes/js/jquery-lib.js"></script>
<script src="{{url('/')}}/themes/js/height.js"></script>
<script src="{{url('/')}}/themes/js/jquery.flexslider.js"></script>
<script src="{{url('/')}}/themes/js/modernizr.js"></script>
<script src="{{url('/')}}/themes/js/jquery.mousewheel.js"></script>
<script src="{{url('/')}}/themes/js/demo.js"></script>
<script src="{{url('/')}}/themes/js/froogaloop.js"></script>
<script src="{{url('/')}}/themes/js/jquery.easing.js"></script>


</head>
<body>
<div class="popup_overlay"></div>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<div class="vendor_header">
	<div class="inner_wrap">                
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{url('/')}}/themes/images/vendor-logo.png" alt="" /></a></div>                      	
        </div> <!-- vendor_header_left -->
        
        <div class="vendor_header_right">
        	<div class="vendor_welc">
            	<div class="vendor_name">Welcome <span>@if(Session::get('customerdata.user_name')=='')
			 Guest 
			 @else 
			 {{Session::get('customerdata.user_name')}}
			 @endif </span></div>
                <a href="#" class="vendor_cart"><img src="{{url('/')}}/themes/images/basket.png" /><span>2</span></a>
            </div>
            
            <div class="select_catg">
            	<div class="select_lbl">Other Branches</div>
                <div class="search-box-field">
                	<select class="select_drp">
                    	<option>select</option>
                    </select>
                </div>
            </div>
        </div> <!-- vendor_header_right -->
	</div>
</div><!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">
      <ul>
        <li><a href="#about_shop" class="active">About Shop</a></li>
        <li><a href="#video">Video</a></li>
        <li><a href="#our_client">What Our Clients Say's</a></li>
        <li><a href="#choose_package">Choose Package</a></li>
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->




<div class="inner_wrap">

<div class="detail_page hall-extra-class">	
    <a name="about"></a>	
    <div class="service_detail_row">
    	
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
                @foreach($shopgalleryimage as $productgalleryimages)
				<li>
  	    	    <img src="{{ $productgalleryimages->image }}" />
  	    		</li>
				@endforeach
  	    		       	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
              @foreach($shopgalleryimage as $productgallerythumbnailimages)
			<li>
  	    	    <img src="{{ $productgallerythumbnailimages->image }}" />
  	    		</li>
			@endforeach	
  	    	
           
          </ul>
        </div>
      		</section>
            
        </div>
        
        <div class="service_detail">
        	<div class="detail_title">{{ $ShophbasicInfo[0]->mc_name }} {{ $ShophallbasicInfo[0]->pro_title}}</div>
            <div class="detail_hall_description">{{ $ShophbasicInfo[0]->address }}</div>
            <div class="detail_hall_subtitle">@if (Lang::has(Session::get('lang_file').'.About_Hall')!= '') {{  trans(Session::get('lang_file').'.About_Hall') }} @else  {{ trans($OUR_LANGUAGE.'.About_Hall') }} @endif: </div>
            <div class="detail_about_hall">{{$ShophallbasicInfo[0]->pro_desc}}</div>
            <div class="detail_hall_dimention">@if (Lang::has(Session::get('lang_file').'.Hall_Dimension')!= '') {{  trans(Session::get('lang_file').'.Hall_Dimension') }} @else  {{ trans($OUR_LANGUAGE.'.Hall_Dimension') }} @endif: <span>{{ $ShophallbasicInfo[0]->hall_dimension }} sqft</span></div>
            <div class="detail_hall_price">@if (Lang::has(Session::get('lang_file').'.currency')!= '') {{  trans(Session::get('lang_file').'.currency') }} @else  {{ trans($OUR_LANGUAGE.'.currency') }} @endif {{ $ShophallbasicInfo[0]->pro_price}}</div>
            <input type="button" value="Reserve Now" class="reserve_now">
            <div class="common_title">@if (Lang::has(Session::get('lang_file').'.Facilities_Offered_Here')!= '') {{  trans(Session::get('lang_file').'.Facilities_Offered_Here') }} @else  {{ trans($OUR_LANGUAGE.'.Facilities_Offered_Here') }} @endif </div>
            <ul class="detail_facilites_list">
				
				 @foreach($shopfacilities as $shophallfacilities)
            	<li>{{ $shophallfacilities-> attribute_title }}</li>
				@endforeach
                
            </ul>
        </div> 
        
    </div> <!-- service_detail_row -->
        
    <div class="service_container">
        <div class="services_detail">
		
		
			 @foreach($shopproductoption as $shopmainoptions)
					<div class="service_list_row">
            	<a name="prepaid"></a>
                <div class="common_title">{{ $shopmainoptions->option_title }}</div>
                <ul class="service_catagory">
						@foreach($shopmainoptions->getEnOptionValue as $shopsuboptions)
                	<li><input name="service[]" type="checkbox" id="service1" value="{{ $shopmainoptions->id }}_{{ $shopsuboptions->id }}"><label for="service1">{{ $shopsuboptions->option_title }}<span class="service_price">SAR {{ $shopsuboptions->price }}</span></label></li>
						 @endforeach
                  
                </ul>
            </div>
			 
			 
			 @endforeach
        	
        
            
            <div class="service_list_row service_testimonial">
            	<a name="client_say"></a>
                <div class="common_title">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
                <div class="testimonial_slider">
                	<section class="slider">
                        <div class="flexslider1">
                          <ul class="slides">
						  @foreach($customerReviewOnProduct as $customerreview)
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="{{ $customerreview->cus_pic }}"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description">{{ $customerreview->comments }}</div>
                                    	<div class="testim_name">{{ $customerreview->cus_name }}</div>
                                        <div class="testim_star"><img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png"></div>
                                    </div>
                                </div>
                            </li>
					 @endforeach
                           
                          </ul>
                        </div>
                      </section>
                </div>
            </div>
            
        </div>
        
        <div class="services_listing">
        	<ul>
            	<li>
                	<span class="service_name">Taj Hall</span>
                    <span class="service_price">SAR 25,000</span>
                </li>
                <li>
                	<span class="service_name">Services 1</span>
                    <span class="service_price">SAR 20</span>
                </li>
                <li>
                	<span class="service_name">Services 2</span>
                    <span class="service_price">SAR 30</span>
                </li>
                <li>
                	<span class="service_name">Services 3</span>
                    <span class="service_price">SAR 30</span>
                </li>
                <li>
                	<span class="service_name">Internal Food</span>
                    <span class="service_price">SAR 200</span>
                </li>
                <li class="total_cost">
                	<span class="service_name">Total Price</span>
                    <span class="service_price">SAR 25,280</span>
                </li>
            </ul>
        </div>
        
    </div> <!-- service_container -->

	<div class="sticky_other_service">

<div class="sticky_serivce">Other Services</div>
<div class="sticku_service_logo"><img src="{{url('/')}}/themes/images/logo.png"></div>

</div>

	<div class="other_serviceinc">
<div class="other_servrow">
	<div class="serv_title">Other Services</div>
    <a href="javascript:void(0);" class="serv_delete">X</a>
</div>
@include('includes.customer-budget-section') 
</div> <!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div> <!-- innher_wrap -->

   
    
</div> <!-- outer_wrapper -->

<div class="othrserv_overl"></div>
@include('includes.footer')


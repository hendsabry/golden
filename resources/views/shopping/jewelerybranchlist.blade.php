@include('includes.navbar')
<div class="outer_wrapper">
    @include('includes.header')
  <div class="inner_wrap"> 

	<div class="search-section">
	<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
	@include('includes.searchweddingandoccasions')
	</div>
	 <!-- search-section -->
    <!-- search-section -->
    <div class="page-left-right-wrapper">
      @include('includes.mobile-modify')


      <div class="page-right-section">
	  @php if(count($vendordetails) > 0 ){ @endphp
        <div class="diamond-area">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> @php if($category_id==10){ $foodshop='datesshop'; }else{ $foodshop='foodshopbranch'; } @endphp
                @php  $i=1;     @endphp
                @php  $k=count($vendordetails);  @endphp
                @php if($k<6){ @endphp
                <div class="diamond_wrapper_inner"> @foreach($vendordetails as $getallcats)
                  @php
$img = str_replace('thumb_','',$getallcats->mc_img);
                  @endphp

                  <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a @php if($getallcats->isproduct!=''){ @endphp href="{{ route('jewelerybranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id]) }}" @php } else{ @endphp onclick="noproductdata();"; @php } @endphp>
                    <div class="category_wrapper @if($k==3) category_wrapper3 @endif" style="background:url({{ $img or '' }});">
                      <div class="category_title">
                        <div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div>
                      </div>
                    </div>
                    </a> </div>
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                <!------------ 6th-------------->
                @php }elseif($k==6){ @endphp
                @php $j=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($vendordetails as $getallcats)
                  @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                  @php if($j==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($j==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($j==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($j==6){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a @php if($getallcats->isproduct!=''){ @endphp href="{{ route('jewelerybranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id]) }}" @php } else{ @endphp onclick="noproductdata();"; @php } @endphp>
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->mc_img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($j==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==4){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($j==5){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($j==6){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $j=$j+1; @endphp
                  @endforeach </div>
                <!------------ 7th-------------->
                @php }elseif($k==7){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($vendordetails as $getallcats)
                  @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                  
                  @php if($l==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($l==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($l==7){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a @php if($getallcats->isproduct!=''){ @endphp href="{{ route('jewelerybranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id]) }}" @php } else{ @endphp onclick="noproductdata();"; @php } @endphp>
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->mc_img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==7){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!------------ 8th-------------->
                @php }elseif($k==8){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($vendordetails as $getallcats)
                  @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                  @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                  @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                  
                  @php if($l==1){ $classrd='category_wrapper1'; @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_3of5 rows5row"> @php } @endphp 
                      @php if($l==4){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp
                          @php if($l==8){ $classrd='category_wrapper9'; @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a @php if($getallcats->isproduct!=''){ @endphp href="{{ route('jewelerybranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id]) }}" @php } else{ @endphp onclick="noproductdata();"; @php } @endphp>
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->mc_img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==8){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!---------- 9th ------------------->
                @php }elseif($k==9){ @endphp
                <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                  @foreach($vendordetails as $getallcats)
                  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp             
                  
                  @php if($i==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($i==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($i==4){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($i==7){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp 
                          @php if($i==9){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a @php if($getallcats->isproduct!=''){ @endphp href="{{ route('jewelerybranchdetail',[$category_id,$subcat_id,$shop_id,$getallcats->mc_id]) }}" @php } else{ @endphp onclick="noproductdata();"; @php } @endphp> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->mc_img or '' }});"> <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span></span> </span> </a> @php if($i==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    @php if($i==9){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp 
                  
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                @php } @endphp </div>
            </div>
          </div>
		  <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
		  <div class="" align="center">{{ $vendordetails->links() }}</div>
		@php }else{ @endphp
        <div class="no-record-area" align="center">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
        @php } @endphp
        <!-- diamond-area -->
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
<div class="oops_popup">
  <div class="oops_title">Oops!</div>
  <div class="ooops_desciption">You have exceeded your budget! Happens. 
    Simply revisit your plan or increase your budget. </div>
  <a href="javascript:void(0)" class="oops_btn">OK</a> </div>
<!-- oops_popup -->
<div class="services_popup">
  <div class="serv_popup_row">
    <div class="service_popup_title">Taj Hall 1</div>
    <a href="javascript:void(0)" class="serv_pop_close">X</a> </div>
  <!-- serv_popup_row -->
  <div class="serv_popupimg"><img src="{{ url('') }}/themes/images/business.png"></div>
  <div class="serv_popup_row">
    <div class="serv_price">SAR 20,000</div>
    <a href="javascript:void(0)" class="serv_detail_link">View Details</a> </div>
  <!-- servicepopup_toprow -->
</div>
<!-- services_popup -->
@include('includes.footer') 
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content">@if (Lang::has(Session::get('lang_file').'.NO_SERVICES_AVAILABLE')!= '') {{  trans(Session::get('lang_file').'.NO_SERVICES_AVAILABLE') }} @else  {{ trans($OUR_LANGUAGE.'.NO_SERVICES_AVAILABLE') }} @endif</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/>
      <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>
  </div>
</div>
<script type="text/javascript">
function noproductdata()
{
 jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
}
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
});
</script>
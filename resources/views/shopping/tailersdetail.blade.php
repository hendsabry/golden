@include('includes.navbar')
@php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
	$Current_Currency = 'SAR'; 
  } 
@endphp
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
          @php if($vendordetails->mc_video_url!=''){ @endphp
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
          @php } @endphp
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
         <li><a href="#choose_design">{{ (Lang::has(Session::get('lang_file').'.Choose_Design')!= '')  ?  trans(Session::get('lang_file').'.Choose_Design'): trans($OUR_LANGUAGE.'.Choose_Design')}}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?><li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
               <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>               
                    <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">{{$vendordetails->mc_name}}</div>
          <div class="detail_hall_description">{{$vendordetails->address}}</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')}}</div>
          <div class="detail_about_hall"><div class="comment more">{{nl2br($vendordetails->mc_discription)}}</div></div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: <span><?php
		    $getcityname = Helper::getcity($vendordetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?></span></div>

         @php if($vendordetails->latitude!='' && $vendordetails->longitude!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp


          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          @php }  @endphp </div>



        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
	    <a name="video" class="linking">&nbsp;</a> 
      @php if($vendordetails->mc_video_url!=''){ @endphp
        <div class="service-video-area">
          <div class="service-video-cont">{{$vendordetails->mc_video_description}}</div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
         @php } @endphp
        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
				 @foreach($allreview as $val)
				 @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
				  @endforeach			  
                </ul>
              </div>
            </section>
          </div>
        </div>
		<?php } ?>
      </div>
 <!--div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="#" class="select">{{ (Lang::has(Session::get('lang_file').'.Style')!= '')  ?  trans(Session::get('lang_file').'.Style'): trans($OUR_LANGUAGE.'.Style')}}</a></li>
               
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div-->

@php if(count($productlist)>0){ @endphp
      <!-- service-mid-wrapper -->
      <div class="service-display-section"> 
	
	  <a name="choose_design" class="linking">&nbsp;</a>
        <div class="service-display-right">
		 <div class="style">{{ (Lang::has(Session::get('lang_file').'.Style')!= '')  ?  trans(Session::get('lang_file').'.Style'): trans($OUR_LANGUAGE.'.Style')}}</div>
          <div class="diamond_main_wrapper dmd">
		   
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> @php  $i=1; @endphp
                @php  $k=count($productlist);  @endphp
                @php if($k<6){ @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)

@php $img = str_replace('thumb_','',$getallcats->pro_Img); @endphp

                  <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                    <div class="category_wrapper @php if($i==3){ echo 'category_wrapper3';}@endphp" style="background:url({{ $img or '' }});">
                      <div class="category_title">
                        <div class="category_title_inner">
                          {{$getallcats->pro_title}}
                        </div>
                      </div>
                    </div>
                    </a> </div>
                  @php $i++; @endphp
                  @endforeach </div>
                <!------------ 6th-------------->
                @php }elseif($k==6){ @endphp
                @php $j=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                  @php if($j==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($j==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($j==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($j==6){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($j==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==4){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($j==5){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($j==6){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $j=$j+1; @endphp
                  @endforeach </div>
                <!------------ 7th-------------->
                @php }elseif($k==7){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                  
                  @php if($l==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($l==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($l==7){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==7){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!------------ 8th-------------->
                @php }elseif($k==8){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                  @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                  @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                  
                  @php if($l==1){ $classrd='category_wrapper1'; @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_3of5 rows5row"> @php } @endphp 
                      @php if($l==4){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp
                          @php if($l==8){ $classrd='category_wrapper9'; @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==8){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                
                @php }elseif($k==9){ @endphp
                <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                  @foreach($productlist as $getallcats)
                  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
                  
                  
                  @php if($i==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($i==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($i==4){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($i==7){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp 
                          @php if($i==9){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')"> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});"> <span class="category_title"><span class="category_title_inner">
                            {{$getallcats->pro_title}}
                            </span></span> </span> </a> @php if($i==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    @php if($i==9){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp 
                  
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                @php }  @endphp 
				
				</div>
          </div>
		  </div>
		  <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
		  <div class="" align="center"> {{ $productlist->links() }}</div>
		  </div>

      @php } @endphp
        <!-- service-display-right -->
	

 

		<?php  

    if(count($productlist)>0){

    if(isset($productlist[0]->pro_disprice) && ($productlist[0]->pro_disprice!='0.00' || $productlist[0]->pro_disprice!='0')){$getPrice = $productlist[0]->pro_disprice;}else{$getPrice = $productlist[0]->pro_price;} ?>
        <div class="service-display-left tailors-l">
		
		 <b>{{ Session::get('status') }}</b>
          <span id="selectedproduct">
		  <input type="hidden" id="product_id" name="product_id" value="<?php if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!=''){echo $productlist[0]->pro_id;}?>">
		  <input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>">
		  <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->pro_mr_id) && $productlist[0]->pro_mr_id!=''){echo $productlist[0]->pro_mr_id;}?>">
		  <div class="service-left-display-img product_gallery">

 
            @php     $pro_id = $productlist[0]->pro_id; @endphp
             @include('includes/product_multiimages')
       

      </div>
          <div class="service-product-name"><?php if(isset($productlist[0]->pro_title) && $productlist[0]->pro_title!=''){echo $productlist[0]->pro_title;}?></div>
		  <div class="service-beauty-description"><?php if(isset($productlist[0]->pro_desc) && $productlist[0]->pro_desc!=''){
      if(Session::get('lang_file') !='en_lang'){
        echo $productlist[0]->pro_desc_ar;
      }else{
        echo $productlist[0]->pro_desc;
      }

      }?></div>
		  </span> 
<div class="deliver_day">
@if(isset($productlist[0]->deliver_day) && $productlist[0]->deliver_day!='')
{{ (Lang::has(Session::get('lang_file').'.Approx_deleivery_date_before')!= '')  ?  trans(Session::get('lang_file').'.Approx_deleivery_date_before'): trans($OUR_LANGUAGE.'.Approx_deleivery_date_before')}}

  {{ $productlist[0]->deliver_day or '' }} 


   {{ (Lang::has(Session::get('lang_file').'.Approx_deleivery_date_after')!= '')  ?  trans(Session::get('lang_file').'.Approx_deleivery_date_after'): trans($OUR_LANGUAGE.'.Approx_deleivery_date_after')}}   
@endif

</div>
		  <?php 
		  if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!='')
		  { 
		    $z = 0;
		    $getalldata = Helper::getProduckOptionInfo($productlist[0]->pro_id,$productlist[0]->pro_mr_id); 

			if(count($getalldata) > 0)
			{
			?>
			   

        
<div class="service_selecrrow dress_selecrrow" id="ptattrsizeenone">
  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Fabrics')!= '')  ?  trans(Session::get('lang_file').'.Fabrics'): trans($OUR_LANGUAGE.'.Fabrics')}}</div>
<div class="checkout-form-bottom" id="ptattrsize">
	<div id="content-1" class="content mCustomScrollbar">
<div class="fabs_op">
<?php foreach($getalldata as $val){ ?>
	<div class="fabric_block">
	<div class="fabric_img_block"> <img src="<?php echo $val->fabric_img;?>"> </div>
	<div class="fabric_title_block">  <?php echo $val->fabric_title;?></div>
	<div class="fabric_price_block">  <?php echo $val->fabric_price;?></div>
  @php if($val->fabric_title!=''){ @endphp
	<div class="choose-line-radio"><input type="radio" value="<?php echo $val->short_name;?>" name="product_fabrics"   onchange="return showPriceDetail(this.value)">
	<label for="name">&nbsp;</label></div>
  @php } @endphp
	</div>
<?php } ?>
</div>
</div>
</div>
</div>
		  <?php  $z++;}} ?>



		  <span id="plzselectf" <?php if($z!=0){ ?>style="display:none;"<?php } ?>>


<div class="service_quantity_box abayaqty">
       <div class="service_qunt">{{ (Lang::has(Session::get('lang_file').'.Quantity')!= '')  ?  trans(Session::get('lang_file').'.Quantity'): trans($OUR_LANGUAGE.'.Quantity')}} </div>
              <div class="service_qunatity_row">
                <div class="td td2 quantity food-quantity" data-title="{{ (Lang::has(Session::get('lang_file').'.Quantity')!= '')  ?  trans(Session::get('lang_file').'.Quantity'): trans($OUR_LANGUAGE.'.Quantity')}}">
                  <div class="quantity">
                    <button type="button" id="sub" class="sub" onclick="return pricecalculation('remove');"></button>
                    <input type="number" name="itemqty" id="qty" value="1" min="1" max="9" onblur="return pricecalculation('add');">
                    <button type="button" id="add" class="add" onclick="return pricecalculation('add');"></button>
                  </div>
                </div>
              </div>
            </div>


          <div class="container_total_price abaya_price"><span class="newprice"><?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->pro_price){echo '<del> '.currency($productlist[0]->pro_price, 'SAR',$Current_Currency).'</del> '.currency($getPrice, 'SAR',$Current_Currency);} ?></span>

            {{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: <span class="cont_final_price total_price">  <?php if(isset($getPrice) && $getPrice!=''){echo currency($getPrice, 'SAR',$Current_Currency);}?></span></div>



          <div class="akaya_bodymesurment" ><a href="#pop" class="try_now_page_one body_btn">{{ (Lang::has(Session::get('lang_file').'.BODY_MEASUREMENT')!= '')  ?  trans(Session::get('lang_file').'.BODY_MEASUREMENT'): trans($OUR_LANGUAGE.'.BODY_MEASUREMENT')}}</a></div>



<?php if(isset($vendordetails->terms_conditions) && $vendordetails->terms_conditions!=''){  ?>
          <div class="terms_conditions"> <a href="{{ $vendordetails->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}</a></div>
<?php } ?>
<a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a>
		  </span>
         


			<div id="try_now_page_one_popup" class="new-popup measurement-popup-outer" style="display:none;"> 
            {!! Form::open(['url' => 'addtocartforshopping', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
			<input type="hidden" id="category_id" name="category_id" value="{{ $category_id }}">
			<input type="hidden" id="subcat_id" name="subcat_id" value="{{ $subcat_id }}">
			<input type="hidden" id="shop_id" name="shop_id" value="{{ $shop_id }}">
			<input type="hidden" name="itemqty" id="itemqty_on_popup" value=""/>
			<input type="hidden" name="actiontype" value="tailersdetail">
			<input type="hidden" name="cart_sub_type" value="tailor">
			<input type="hidden" name="cart_type" value="shopping">
			<input type="hidden" name="fabric_type" value="">
			<input type="hidden" id="mser_product_id" name="product_id" value="<?php if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!=''){echo $productlist[0]->pro_id;}?>">
			<input type="hidden" id="mser_priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>">
			<input type="hidden" id="mser_vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->pro_mr_id) && $productlist[0]->pro_mr_id!=''){echo $productlist[0]->pro_mr_id;}?>">


				 <a href="javascript:void(0);" class="b-close">X</a>
			  <div class="measurement-popup"> 
				<div class="main_title">{{ (Lang::has(Session::get('lang_file').'.BODY_MEASUREMENT')!= '')  ?  trans(Session::get('lang_file').'.BODY_MEASUREMENT'): trans($OUR_LANGUAGE.'.BODY_MEASUREMENT')}}</div>
				<div class="measurement-popup-img"><img src="{{ url('') }}/themes/images/measurement.jpg" alt=""  /></div>
				<div class="measurement-divder">&nbsp;</div>
				<div class="measurement-form">
				  <div class="measurement-form-line">
					<div class="measurement-form-top">{{ (Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')}}</div>
					<div class="measurement-form-bottom">

					  <input type="text" class="t-box" required="required" autocomplete="off" name="length" id="length" onkeyup="return islengh(event,this.value)" onkeydown="return islengh(event,this.value)" maxlength="5"/>
            <span class="error len"></span>
             
					  <div class="parametar">{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div>
					</div>
          <label for="length" class="error"></label>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top">{{ (Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')}}</div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" name="chest_size" id="chest_size"  onkeyup="return islenghs(event,this.value,1)" onkeydown="return islenghs(event,this.value,1)"  onkeypress="return isNumber(event,this.value,'chest_size')" maxlength="6"/>
          
					  <div class="parametar">{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div>
					</div>
          <label for="chest_size" class="error len1"></label>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top">{{ (Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')}}</div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box"  onkeyup="return islenghs(event,this.value,2)" onkeydown="return islenghs(event,this.value,2)"  name="waist_size" id="waist_size" onkeypress="return isNumber(event,this.value,'waist_size')" maxlength="4"/>
          
					  <div class="parametar">{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div>
					</div>
          <label for="waist_size" class="error len2"></label>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top">{{ (Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')}}</div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box"  onkeyup="return islenghs(event,this.value,3)" onkeydown="return islenghs(event,this.value,3)"  name="shoulders" id="shoulders" onkeypress="return isNumber(event,this.value,'shoulders')" maxlength="4"/>

					  <div class="parametar">{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div>
					</div>
          <label for="shoulders" class="error len3"></label>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top">{{ (Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')}}</div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box"  onkeyup="return islenghs(event,this.value,4)" onkeydown="return islenghs(event,this.value,4)"  name="neck" id="neck" onkeypress="return isNumber(event,this.value,'neck')" maxlength="4"/>
          
					  <div class="parametar">{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div>
					</div>
           <label for="neck" class="error len4"></label>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top">{{ (Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')}}</div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box"  onkeyup="return islenghs(event,this.value,5)" onkeydown="return islenghs(event,this.value,5)"  name="arm_length" id="arm_length" onkeypress="return isNumber(event,this.value,'arm_length')" maxlength="4"/>
          
					  <div class="parametar">{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div>
					</div>
           <label for="arm_length" class="error len5"></label>
				  </div>
				  <!-- measurement-form-line -->
				  <div class="measurement-form-line">
					<div class="measurement-form-top">{{ (Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')}}</div>
					<div class="measurement-form-bottom">
					  <input type="text" class="t-box" onkeyup="return islenghs(event,this.value,6)" onkeydown="return islenghs(event,this.value,6)"   name="wrist_diameter" id="wrist_diameter" onkeypress="return isNumber(event,this.value,'wrist_diameter')" maxlength="4"/>
            
					  <div class="parametar">{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div>
					</div>
          <label for="wrist_diameter" class="error len6"></label>
				  </div>
				  <!-- measurement-form-line -->

				  <div class="measurement-form-line" style="display: none;">
					<div class="measurement-form-top" >{{ (Lang::has(Session::get('lang_file').'.CUTTING')!= '')  ?  trans(Session::get('lang_file').'.CUTTING'): trans($OUR_LANGUAGE.'.CUTTING')}}</div>
					<div class="measurement-form-bottom">
					  <div class="measurement-radio-line">
						<input type="radio" name="cutting" id="Normal" value="Normal" />
						<label for="Normal">{{ (Lang::has(Session::get('lang_file').'.NORMAL')!= '')  ?  trans(Session::get('lang_file').'.NORMAL'): trans($OUR_LANGUAGE.'.NORMAL')}}</label>
					  </div>
					  <div class="measurement-radio-line">
						<input type="radio" name="cutting" id="Ample" value="Ample"/>
						<label for="Ample">{{ (Lang::has(Session::get('lang_file').'.AMPLE')!= '')  ?  trans(Session::get('lang_file').'.AMPLE'): trans($OUR_LANGUAGE.'.AMPLE')}}</label>
					  </div>
					  <div class="measurement-radio-line">
						<input type="radio" name="cutting" id="Curved" value="Curved"/>
						<label for="Curved">{{ (Lang::has(Session::get('lang_file').'.CURVED')!= '')  ?  trans(Session::get('lang_file').'.CURVED'): trans($OUR_LANGUAGE.'.CURVED')}}</label>
					  </div>
					  <label for="cutting" class="error"></label>
					</div>
				  </div>
				  <!-- measurement-form-line -->
				</div>
				<!-- measurement-form -->
				<div class="measurement-btn-area">
				  <input type="submit" class="form-btn big-btn btn-info-wisitech" style="margin-top: 60px;" value="{{ (Lang::has(Session::get('lang_file').'.CONTINUE_WITH_THESE_MEASURES')!= '')  ?  trans(Session::get('lang_file').'.CONTINUE_WITH_THESE_MEASURES'): trans($OUR_LANGUAGE.'.CONTINUE_WITH_THESE_MEASURES')}}" />
				</div>
			  </div>
          {!! Form::close() !!} 
			</div>
        </div>
    @php } @endphp
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
	@include('includes.other_services')    
	  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->

</div>
<div class="othrserv_overl"></div>
@include('includes.footer') 
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
function showPriceDetail(act)
{
    var product_id = $('#product_id').val();
    jQuery('[name=fabric_type]').val(act);
	if(act!='')
	{
	   $('#plzselectf').show();
	   $.ajax({
	   type:"GET",
	   url:"{{url('getfabric')}}?product_id="+product_id+'&product_fabric='+act,
	   async: false,
	   success:function(res)
	   { 
		   var totalprice = parseInt(res).toFixed(2);
           totalprice = getChangedPrice(totalprice); 
           <?php $Cur = Session::get('currency'); ?>
		   $('#priceId').val(totalprice);
		   $('#mser_priceId').val(totalprice);
		   $('.total_price').html('<?php echo $Cur;?> '+totalprice);
	   }
	 });
	}
	else
	{
	  $('#plzselectf').hide();
	}
}
function getChangedPrice(price)
{
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
}
function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"{{url('getmultipleImages')}}?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}

function showProductDetail(str,vendorId)
{
	   $.ajax({
	   type:"GET",
	   url:"{{url('getShoppingProductInfoTailor')}}?product_id="+str+'&vendor_id='+vendorId,
	   success:function(res)
	   {               
		if(res)
		{ 
		  var json = JSON.stringify(res);
		  var obj = JSON.parse(json);
          
		  length=obj.productdateshopinfo.length;
		<?php $Cur = Session::get('currency'); ?>
		  if(length>0)
		  {
	         for(i=0; i<length; i++)
		     {    
			  if(obj.productdateshopinfo[i].pro_disprice !='0.00' && obj.productdateshopinfo[i].pro_disprice !=''){var getPrice = parseFloat(obj.productdateshopinfo[i].pro_disprice).toFixed(2); } else {var getPrice = parseFloat(obj.productdateshopinfo[i].pro_price).toFixed(2);}


  
 $('#mser_product_id').val(obj.productdateshopinfo[i].pro_id);
 $('#mser_priceId').val(getPrice);
  $('#mser_vendor_id').val(obj.productdateshopinfo[i].pro_mr_id);


              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+getPrice+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img  product_gallery"> </div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdateshopinfo[i].pro_desc+'</div>'); 
checkgallery(str);



 var prev = "@if (Lang::has(Session::get('lang_file').'.Approx_deleivery_date_before')!= '') {{  trans(Session::get('lang_file').'.Approx_deleivery_date_before') }} @else  {{ trans($OUR_LANGUAGE.'.Approx_deleivery_date_before') }} @endif";

  var aftr = "@if (Lang::has(Session::get('lang_file').'.Approx_deleivery_date_after')!= '') {{  trans(Session::get('lang_file').'.Approx_deleivery_date_after') }} @else  {{ trans($OUR_LANGUAGE.'.Approx_deleivery_date_after') }} @endif";
  
 
$('.deliver_day').html(prev +' '+obj.productdateshopinfo[i].deliver_day+' '+aftr);
 
 
         
			  if(parseFloat(getPrice) < parseFloat(obj.productdateshopinfo[i].pro_price))
			  {  
        

 TotalPrice = getChangedPrice(obj.productdateshopinfo[i].pro_price); 
 getPrice = getChangedPrice(getPrice); 

				  $('.newprice').html('<del><?php echo $Cur;?> '+TotalPrice+'</del> <?php echo $Cur;?> '+getPrice+'<br>');	
				  $('.total_price').html('<?php echo $Cur;?> '+getPrice);
			  }	
			  else
			  {
           TotalPrice = getChangedPrice(obj.productdateshopinfo[i].pro_price); 
			     $('.total_price').html('<?php echo $Cur;?> '+TotalPrice);	
				 $('.newprice').html('');	
			  }  
		     }
		  }	
		  if($.trim(obj.productattrsize) !=1)
		  {
		    $('#ptattrsize').html(obj.productattrsize);
			$('#ptattrsizeenone').css('display','block');
			$('#plzselectf').hide();
		  }
		  else
		  {
		    $('#ptattrsizeenone').css('display','none');
			$('#plzselectf').show();
		  }
		  $('html, body').animate({
				scrollTop: ($('.service-display-left').first().offset().top)
		  },500);	

    $("#content-1").mCustomScrollbar({
        scrollButtons:{
            enable:true
        }  
      });

	    }
	   }
	 });
}
</script>
<script>
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>

<script>
/* Mobile Number Validation */
 function islengh(evt,lenval) {
   $('.len').html('');
   lenval = parseFloat(lenval);
if(lenval >=100 && lenval < 1000)
{
 $('.len').html('');
  
}
else
{

var Please_enter_correct_value = "@if (Lang::has(Session::get('lang_file').'.Please_enter_correct_value')!= '') {{  trans(Session::get('lang_file').'.Please_enter_correct_value') }} @else  {{ trans($OUR_LANGUAGE.'.Please_enter_correct_value') }} @endif";
 
  $('.len').html(Please_enter_correct_value);
$('.btn-info-wisitech').prop('disabled','disabled');

}
var length = parseFloat($('#length').val());
var chest_size = parseFloat($('#chest_size').val());
var waist_size = parseFloat($('#waist_size').val());
var shoulders = parseFloat($('#shoulders').val());
 var arm_length = parseFloat($('#arm_length').val());
var neck = parseFloat($('#neck').val());
var wrist_diameter = parseFloat($('#wrist_diameter').val());
 

    if((chest_size >=10 && chest_size < 100)  && (waist_size >=10 &&  waist_size < 100) && (shoulders >=10  && shoulders < 100) && (arm_length >=10 && arm_length < 100) && (neck >=10 && neck < 100) && (wrist_diameter >=10 && wrist_diameter < 100) && (length >=100 && length < 1000))
    {
 $('.btn-info-wisitech').prop('disabled',false);
      
    }

 
 }

  function islenghs(evt,lenval,pos) {
   $('.len'+pos).html('');
   lenval = parseFloat(lenval);
if(lenval >=10 && lenval < 100)
{
 $('.len'+pos).html('');
 
}
else
{
  var Please_enter_correct_value = "@if (Lang::has(Session::get('lang_file').'.Please_enter_correct_value')!= '') {{  trans(Session::get('lang_file').'.Please_enter_correct_value') }} @else  {{ trans($OUR_LANGUAGE.'.Please_enter_correct_value') }} @endif";
  $('.len'+pos).html(Please_enter_correct_value);
  $('.btn-info-wisitech').prop('disabled','disabled');
}
var length = parseFloat($('#length').val());
var chest_size = parseFloat($('#chest_size').val());
var waist_size = parseFloat($('#waist_size').val());
var shoulders = parseFloat($('#shoulders').val());
 var arm_length = parseFloat($('#arm_length').val());
var neck = parseFloat($('#neck').val());
var wrist_diameter = parseFloat($('#wrist_diameter').val());
 

    if((chest_size >=10 && chest_size < 100)  && (waist_size >=10 &&  waist_size < 100) && (shoulders >=10  && shoulders < 100) && (arm_length >=10 && arm_length < 100) && (neck >=10 && neck < 100) && (wrist_diameter >=10 && wrist_diameter < 100) && (length >=100 && length < 1000))
    {
      
 $('.btn-info-wisitech').prop('disabled',false);

    }


 
 }


</script>


<script>
/* Mobile Number Validation */
 // function isNumber(evt,lenval,fid) {
 //   evt = (evt) ? evt : window.event;
 //   var charCode = (evt.which) ? evt.which : evt.keyCode;
 //   if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {

 //   return false;
 //   }
 //   if(lenval>99){
 //    $('#'+fid).val('');
 //     return false;
 //   }
 //   return true;
 // }
</script>
<script>
 jQuery(".btn-info-wisitech").click(function() {  
 jQuery("#cartfrm").validate({
   ignore: [],
    rules: {       
          "length" : {
            required : true,
             maxlength: 6
          },  
		  "chest_size" : {
            required : true,
            maxlength: 6
          },
		  "waist_size" : {
            required : true,
            maxlength: 6
          },
		  "shoulders" : {
            required : true,
            maxlength: 6
          },
		  "neck" : {
            required : true,
             maxlength: 6
          },  
		  "arm_length" : {
            required : true,
            maxlength: 6
          },  
		  "wrist_diameter" : {
            required : true,
            maxlength: 6
          }, 
		  //"cutting" : {
            //required : true
         // },   
         },
         messages: {
          "length": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_LENGTH')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_LENGTH') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_LENGTH') }} @endif',
            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',
            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MAX') }} @else  {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAX') }} @endif'
          },
		  "chest_size": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CHEST_SIZE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CHEST_SIZE') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_CHEST_SIZE') }} @endif',
            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',
            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MAX') }} @else  {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAX') }} @endif'
          },
		  "waist_size": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WAIST_SIZE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WAIST_SIZE') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_WAIST_SIZE') }} @endif',
            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',
            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MAX') }} @else  {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAX') }} @endif'
          },
		  "shoulders": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_SHOULDERS')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_SHOULDERS') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_SHOULDERS') }} @endif',
            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',
            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MAX') }} @else  {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAX') }} @endif'
          },
		  "neck": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NECK')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NECK') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NECK') }} @endif',
            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',
            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MAX') }} @else  {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAX') }} @endif'
          },		  
		  "arm_length": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ARM_LENGTH')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ARM_LENGTH') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ARM_LENGTH') }} @endif',
            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',
            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MAX') }} @else  {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAX') }} @endif'
          },
		  "wrist_diameter": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WRIST_DIA')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_WRIST_DIA') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_WRIST_DIA') }} @endif',
            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',
            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_MAX') }} @else  {{ trans($OUR_LANGUAGE.'.ENTER_YOUR_MAX') }} @endif'
          },
		  "cutting": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_REDIO_BUTTON')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_REDIO_BUTTON') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_SELECT_REDIO_BUTTON') }} @endif'
          },
         }
 });

});
</script>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
</script>


<script type="text/javascript">
 
function pricecalculation(act)
{
  var no=1;
  var product_id      = document.getElementById('product_id').value;
  var currentquantity = document.getElementById('qty').value;
  var unititemprice   = document.getElementById('priceId').value;
  //alert(product_id);
  //alert(currentquantity);
  //alert(unititemprice);
  if(act=='add')
  {
    var qty = parseInt(currentquantity)+parseInt(no);     
  }
  else
  { 
    if(parseInt(currentquantity)==1)
    {
      var qty=parseInt(currentquantity)
    }
    else
    {
      var qty=parseInt(currentquantity)-parseInt(no);
    }
  }
    <?php $Cur = Session::get('currency'); ?>      
    //var totalprice = $('#mser_priceId').val();
    var Tp =  parseFloat(qty) * parseFloat(unititemprice); 
    $('.total_price').html('<?php echo $Cur;?> '+Tp);
    //$('#priceId').val(Tp);
    //$('#mser_priceId').val(Tp);
    $('#itemqty').val(qty);
	$('#itemqty_on_popup').val(qty);
 
}
</script>


@include('includes.popupmessage') 
@include('includes.navbar')
  @php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
@endphp
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
	
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
         @if(trim($vendordetails->mc_video_url) !='')
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
          @endif
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
          <li><a href="#choose_package">{{ (Lang::has(Session::get('lang_file').'.Choose_PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.Choose_PRODUCT'): trans($OUR_LANGUAGE.'.Choose_PRODUCT')}}</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?><li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
               <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>               
                    <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">{{$vendordetails->mc_name}}</div>
          <div class="detail_hall_description">{{$vendordetails->address}}</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')}}</div>
          <div class="detail_about_hall"><div class="comment more">{{$vendordetails->mc_discription}}</div></div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: <span><?php
		    $getcityname = Helper::getcity($vendordetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?></span></div>

      @php if($vendordetails->latitude!='' && $vendordetails->longitude!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp


          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          @php }  @endphp
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
@if(trim($vendordetails->mc_video_url) !='')
	    <a name="video" class="linking">&nbsp;</a>
        <div class="service-video-area">
          <div class="service-video-cont">{{$vendordetails->mc_video_description}}</div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
@endif


        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
				 @foreach($allreview as $val)
				 @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
				  @endforeach			  
                </ul>
              </div>
            </section>
          </div>
        </div>
		<?php } ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                @php //echo "<pre>";
                   // print_r($productlist);
                   // die;

                $k=1;@endphp
                @foreach($productlist as $categories)
                @php 
                if(count($categories) > 0 ) 
                {
                if($k==1)
                { 
                $cl='select'; 

                $HH = $categories->cat_attribute[0]->pro_id; 
                $MM=$categories->cat_attribute[0]->pro_mr_id;
                }
                else
                { 
                $cl=''; 
                } 
                @endphp
@if(isset($categories->cat_attribute[0]->pro_id) && $categories->cat_attribute[0]->pro_id!='')
                <li><a href="#{{$k}}"  class="cat {{$cl}}" data-toggle="tab" onclick="return showProductDetail('{{ $categories->cat_attribute[0]->pro_id }}','{{ $branch_id }}','{{$categories->cat_attribute[0]->pro_mr_id}}')">{{ $categories->attribute_title }}</a></li>
                @endif
                @php $k++; } @endphp
                @endforeach
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  
	  <!-- service_bottom -->
      <div class="service-display-section"> 
	  <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer"> @php  $z=1;     @endphp
              @foreach($productlist as $productservice)
              @php $k=count($productservice->cat_attribute); 
              if($k >0 ){
              @endphp
              @php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp
              <div class="diamond_wrapper_main tab-pane fade {{ $addactcon }}"  id="{{ $z}}"> @php if($k < 6){ @endphp
                @php  $i=1;     @endphp
                <div class="diamond_wrapper_inner "> @foreach($productservice->cat_attribute as $getallcats)

@php $img = str_replace('thumb_','',$getallcats->pro_Img); @endphp

                  <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{ $branch_id }}','{{$getallcats->pro_mr_id}}')">
                    <div class="category_wrapper @if($k==3) category_wrapper3 @endif" style="background:url({{ $img or '' }});">
                      <div class="category_title">
                        <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                      </div>
                    </div>
                    </a> </div>
                  @php $i=$i+1; @endphp
                  
                  @endforeach </div>
                <!------------ 6th-------------->
                @php }elseif($k==6){ @endphp
                @php $j=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productservice->cat_attribute as $getallcats)
                  @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                  @php if($j==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($j==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($j==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($j==6){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{ $branch_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($j==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==4){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($j==5){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($j==6){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $j=$j+1; @endphp
                  @endforeach </div>
                <!------------ 7th-------------->
                @php }elseif($k==7){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productservice->cat_attribute as $getallcats)
                  @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                  
                  @php if($l==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($l==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($l==7){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{ $branch_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==7){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!------------ 8th-------------->
                @php }elseif($k==8){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productservice->cat_attribute as $getallcats)
                  @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                  @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                  @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                  
                  @php if($l==1){ $classrd='category_wrapper1'; @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_3of5 rows5row"> @php } @endphp 
                      @php if($l==4){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp
                          @php if($l==8){ $classrd='category_wrapper9'; @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{ $branch_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==8){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!---------- 9th ------------------->
                @php }elseif($k==9){ @endphp
                <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                  @foreach($productservice->cat_attribute as $getallcats)
                  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
                  
                  
                  @php if($i==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($i==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($i==4){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($i==7){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp 
                          @php if($i==9){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{ $branch_id }}','{{$getallcats->pro_mr_id}}')"> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});"> <span class="category_title"><span class="category_title_inner">{{ $getallcats->pro_title or ''}}</span></span> </span> </a> @php if($i==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    @php if($i==9){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp 
                  
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                @php } @endphp </div>
              @php $z=$z+1; }@endphp 
              @endforeach 
			  </div>
          </div>
		  <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
		  <div class="" align="center"> {{ $productservice->cat_attribute->links() }}</div>
		  </div>
        <!-- service-display-right -->
		{!! Form::open(['url' => 'addtocartofgoldAndperfume', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
		<?php if(isset($productlist[0]->cat_attribute[0]->pro_disprice) && $productlist[0]->cat_attribute[0]->pro_disprice!='0'){$getPrice = $productlist[0]->cat_attribute[0]->pro_disprice;}else{$getPrice = $productlist[0]->cat_attribute[0]->pro_price;}?>
		<div class="service-display-left">
		<input type="hidden" id="category_id" name="category_id" value="{{ $category_id }}">
          <input type="hidden" id="subcat_id" name="subcat_id" value="{{ $subcat_id }}">
          <input type="hidden" id="shop_id" name="shop_id" value="{{ $shop_id }}">
		  <input type="hidden" id="branch_id" name="branch_id" value="{{ $branch_id }}">
          <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" max="9" />
          <input type="hidden" name="actiontype" value="perfumesbranchdetail">
          <input type="hidden" name="cart_sub_type" value="perfume">
          <input type="hidden" name="cart_type" value="shopping">
		  <b>{{ Session::get('status') }}</b>
		  <span id="selectedproduct">
		  <input type="hidden" name="attribute_id" value="<?php if(isset($productlist[0]->cat_attribute[0]->attribute_id) && $productlist[0]->cat_attribute[0]->attribute_id!=''){echo $productlist[0]->cat_attribute[0]->attribute_id;}?>">
          <input type="hidden" id="product_id" name="product_id" value="<?php if(isset($productlist[0]->cat_attribute[0]->pro_id) && $productlist[0]->cat_attribute[0]->pro_id!=''){echo $productlist[0]->cat_attribute[0]->pro_id;}?>">
          <input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){echo $getPrice;}?>">
          <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->cat_attribute[0]->pro_mr_id) && $productlist[0]->cat_attribute[0]->pro_mr_id!=''){echo $productlist[0]->cat_attribute[0]->pro_mr_id;}?>">
          <div class="service-left-display-img product_gallery">
 
            @php    $pro_id = $productlist[0]->cat_attribute[0]->pro_id; @endphp
             @include('includes/product_multiimages')
     
 </div>
		  <div class="service-product-name marB0"><?php if(isset($productlist[0]->cat_attribute[0]->pro_title) && $productlist[0]->cat_attribute[0]->pro_title!=''){ echo $productlist[0]->cat_attribute[0]->pro_title;}?></div>
          <div class="service_prod_description"><?php if(isset($productlist[0]->cat_attribute[0]->about) && $productlist[0]->cat_attribute[0]->about!=''){ echo $productlist[0]->cat_attribute[0]->about;}?></div>
		  </span>







		  <div class="service-radio-line" id="qtyop">
            <div class="service_quantity_box">
			 <div class="service_qunt">{{ (Lang::has(Session::get('lang_file').'.QUANTITY')!= '')  ?  trans(Session::get('lang_file').'.QUANTITY'): trans($OUR_LANGUAGE.'.QUANTITY')}}</div>
              <div class="service_qunatity_row">
                <div class="td td2 quantity food-quantity" data-title="Total Quality">
                  <div class="quantity">
                    <button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
                    <input type="number" name="itemqty" id="qty" value="1" min="1"  onblur="return pricecalculation('add');" />
                    <button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  
          <div class="container-section">
            <span id="excessqty" style="display: none;">{{ (Lang::has(Session::get('lang_file').'.SORRY_QTY_PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.SORRY_QTY_PRODUCT'): trans($OUR_LANGUAGE.'.SORRY_QTY_PRODUCT')}}</span>
            <div class="total_food_cost">
             

  <div class="total_price"><span class="newprice"><?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->cat_attribute[0]->pro_price){echo '<del> '.currency($productlist[0]->cat_attribute[0]->pro_price, 'SAR',$Current_Currency).'</del> <br />';} ?></span><br/>{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: <span id="cont_final_price" class="total_price">  <?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->cat_attribute[0]->pro_price){echo currency($getPrice, 'SAR',$Current_Currency);}else{echo currency($productlist[0]->cat_attribute[0]->pro_price, 'SAR',$Current_Currency);} ?></span></div>

          </div>		  
          <div class="btn_row" id="hideid">

 
  <input type="submit" name="submit" id="submit"  @if($productlist[0]->cat_attribute[0]->pro_qty < 1) disabled="disabled"  @endif @if($productlist[0]->cat_attribute[0]->pro_qty >= 1) value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" @else value="{{ (Lang::has(Session::get('lang_file').'.Sold_Out')!= '')  ?  trans(Session::get('lang_file').'.Sold_Out'): trans($OUR_LANGUAGE.'.Sold_Out')}}" @endif class="form-btn btn-info-wisitech">

 


          </div>
          <div class="terms_conditions"> <a href="{{ $vendordetails->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}</a></div>
          </div>
        </div>
         <a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a>
		{!! Form::close() !!}
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
	 @include('includes.other_services')      
	  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
@include('includes.footer') 
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
 function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }

function pricecalculation(act)
{
	var no=1;
	var product_id      = document.getElementById('product_id').value;
	var currentquantity = document.getElementById('qty').value;
	var unititemprice   = document.getElementById('priceId').value;
	if(act=='add')
	{
		var qty = parseInt(currentquantity)+parseInt(no);			
	}
	else
	{ 
		if(parseInt(currentquantity)==1)
		{
		  var qty=parseInt(currentquantity)
		}
		else
		{
		  var qty=parseInt(currentquantity)-parseInt(no);
		}
	}
  
	if(product_id)
	{
	 $.ajax({
	   type:"GET",
	   url:"{{url('getProductQuantity')}}?product_id="+product_id+'&qty='+qty,
		async: false,
	   success:function(res)
	   { 
         <?php $Cur = Session::get('currency'); ?>              
		 if(res!='ok')
		 {
			$('.action_popup').fadeIn(500);
			 $('.overlay').fadeIn(500);
			 $('#showmsg').show();
			 $('#hidemsgab').hide();
			 $('#showmsgab').show();
             var qtyupdated = parseInt(currentquantity)-1;	
             $('#qty').val(qtyupdated);		
		    // document.getElementById('qty').value = qtyupdated - 1;
		    // document.getElementById('hideid').style.display = "block";
            // document.getElementById('excessqty').style.display = "block";


          /* var aadd = "@if (Lang::has(Session::get('lang_file').'.Sold_Out')!= '') {{  trans(Session::get('lang_file').'.Sold_Out') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Sold_Out') }} @endif";

           $('#submit').prop('disabled','disabled');
           $('#submit').val(aadd);*/



		 }
		 else
		 {
			var producttotal = qty*unititemprice;
            producttotal = getChangedPrice(producttotal);
			document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
			//document.getElementById('itemqty').value=qty;
			//document.getElementById('hideid').style.display = "block";

         $('#submit').prop('disabled',false);
        var aadd = "@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADD_TO_CART') }} @endif";

        $('#submit').val(aadd);
		 }
	   }
	 });
	}
	else
	{
		var producttotal = qty*unititemprice;
    producttotal = getChangedPrice(producttotal);
		document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+parseFloat(producttotal).toFixed(2);
		document.getElementById('itemqty').value=qty;
	}
}
function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"{{url('getmultipleImages')}}?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}
function showProductDetail(productId,branchId,vendorId)
{
	   $.ajax({
	   type:"GET",
	   url:"{{url('getShoppingProductInfoWithGold')}}?product_id="+productId+'&branch_id='+branchId+'&vendor_id='+vendorId,
	   success:function(res)
	   {              
		if(res)
		{ 
		  $('html, body').animate({
				scrollTop: ($('.service-display-left').first().offset().top)
		  },500);
		  var json = JSON.stringify(res);
		  var obj = JSON.parse(json);
          <?php $Cur = Session::get('currency'); ?>
		  productlength = obj.productdata.length;
		 document.getElementById('qty').value=1;
		  if(productlength > 0)
		  {
			for(i=0; i<productlength; i++)
		    {   
			   if(obj.productdata[i].pro_discount_percentage > 0){var getPrice = obj.productdata[i].pro_disprice;}else{var getPrice = obj.productdata[i].pro_price;}
		       $('#selectedproduct').html('<input type="hidden" name="attribute_id" value="'+obj.productdata[i].attribute_id+'"><input type="hidden" id="product_id" name="product_id" value="'+obj.productdata[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+getPrice+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdata[i].pro_mr_id+'"><div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productdata[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdata[i].about+'</div>');
           checkgallery(productId);
          
			  if(obj.productdata[i].pro_discount_percentage!='' && obj.productdata[i].pro_discount_percentage>0)
			  {        producttotals = getChangedPrice(obj.productdata[i].pro_price);
           getPrice = getChangedPrice(getPrice);
				  $('.newprice').html('<del><?php echo $Cur;?> '+producttotals+'</del>');	
				  $('#cont_final_price').html('<?php echo $Cur;?> '+getPrice);
         
			  }	
			  else
			  { getPrices = getChangedPrice(obj.productdata[i].pro_price); 
			     $('#cont_final_price').html('<?php echo $Cur;?> '+getPrices);	
				 $('.newprice').html('');	
         
			  } 

            if(obj.productdata[i].pro_qty >=1)
            { 
              
              $('#qtyop').css('display','block');
            $('#submit').prop('disabled',false);
            var aadd = "@if (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '') {{  trans(Session::get('lang_file').'.ADD_TO_CART') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADD_TO_CART') }} @endif";

            $('#submit').val(aadd);


            }
            else
            {

              $('#qtyop').css('display','none');
            var aadd = "@if (Lang::has(Session::get('lang_file').'.Sold_Out')!= '') {{  trans(Session::get('lang_file').'.Sold_Out') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Sold_Out') }} @endif";

            $('#submit').prop('disabled','disabled');
            $('#submit').val(aadd);
            }




		    }
		  }
	    }
	   }
	 });
}
</script>
<script>
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
@include('includes.popupmessage')
<script>
jQuery(function(){
  jQuery('#dynamic_select').on('change', function () {
	  var url = jQuery(this).val(); // get selected value          
	  if (url) { // require a URL
		  window.location = url; // redirect
	  }
	  return false;
  });
});
</script>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});



<?php if(isset($HH) && $HH!=''){ ?>
jQuery(window).load(function(){ 
  
  showProductDetail('<?php echo $HH;?>','<?php echo $branch_id; ?>','<?php echo $MM;?>');
})

<?php } ?>
</script>
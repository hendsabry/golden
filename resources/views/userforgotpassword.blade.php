@include('includes.navbar')
<div class="outer_wrapper">
<div class="inner_wrap">
@include('includes.header')

</div> <!-- outer_wrapper -->
</div>
 <div class="header-page-divder">&nbsp;</div>
<div class="inner_wrap">
  <div class="inner_wrap">
	@if (Session::has('loginstatus'))
	  <div class="alert alert-info">{{ Session::get('loginstatus') }}</div>
	@endif
 @php (Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ? $sub= trans(Session::get('lang_file').'.SUBMIT'): $sub=trans($OUR_LANGUAGE.'.SUBMIT');

  @endphp
  
 <a name="loginfrml"></a>
     <div class="forgot-password-area">  
	 <div class="form_title">{{ (Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD')}}</div>
   <div class="forgot-password-box">
   <p>{{ (Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD_MSG')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD_MSG'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD_MSG')}} {{ (Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD_MSG2')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD_MSG2'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD_MSG2')}} </p>
   {!! Form::open(['url' => 'userforgotpassword/checkloginaccount', 'method' => 'post', 'name'=>'loginfrm', 'id'=>'loginfrm', 'enctype' => 'multipart/form-data']) !!}
    <div class="usersign-form">
    	
  <div class="usersign-form-line frg">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.EMAILADDRRESS')!= '')  ?  trans(Session::get('lang_file').'.EMAILADDRRESS'): trans($OUR_LANGUAGE.'.EMAILADDRRESS')}}</div>
  <div class="usersign-form-bottom">{!! Form::text('email', null, array('required','id'=>'emailaddres', 'class'=>'t-box','maxlength'=>'80','onfocusout'=>'checkalreadyregisteredemail(this.value);')) !!}<span id="availabilitystatus"></span></div>
  </div>
  
  <div class="usersign-form-button">{!! Form::submit($sub, array('class'=>'form-btn')) !!}</div>
  </div>
  {!! Form::close() !!}
   </div>
  
  </div> <!-- forgot-password-area -->
  
  
 

 



  






</div>  <!-- usersign-area -->






</div>





@include('includes.footer')

 <script type="text/javascript">
 
jQuery("#loginfrm").validate({
                  ignore: [],
                  rules: {
             email: {
                       required: true,
             		email: true,
                      },

                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
             email: {
               required:  "@php echo (Lang::has(Session::get('lang_file').'.USER_EMAIL')!= '')  ?  trans(Session::get('lang_file').'.USER_EMAIL'): trans($OUR_LANGUAGE.'.USER_EMAIL'); @endphp", 
                      },  
         
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
<script language="javascript">

function checkalreadyregisteredemail(emailaddress) {
	var emailaddress=emailaddress;
	$.ajax({
	url: "{{url('userforgotpassword/checkuseraccount')}}",
	data: { email: emailaddress, _token: '{{csrf_token()}}' },
	type: "POST",
	dataType: "text",
	success:function(data){
			if(data=='Error'){
					$("#availabilitystatus").html("");
			}else{
					
					$("#emailaddres").val('');
					$("#availabilitystatus").html("<span class='status-not-available error'>this email address does not registered. please create account.</span>");
			}
			
		
	},
	error:function (){ }
	});

}
</script>
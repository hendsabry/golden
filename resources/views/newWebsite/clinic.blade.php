@extends('newWebsite.layouts.master')
@section('content')

    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>{{ $fooddateshopdetails[0]->mc_name }}</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>{{ $fooddateshopdetails[0]->mc_name }}</h2>
                                <h2>{{ $fooddateshopdetails[0]->address }}.</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                     <?php $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id); 
                                        $mc_name = 'ci_name'; 
                                        if(Session::get('lang_file')!='en_lang')
                                        {
                                          $mc_name = 'ci_name_ar'; 
                                        }
                                        echo $getcityname->$mc_name; 
                                      ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>{{ $fooddateshopdetails[0]->mc_discription }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                                   @php  $homevisitcharges=$fooddateshopdetails[0]->home_visit_charge; @endphp
 <?php if(isset($fooddateshopdetails[0]->google_map_address) && $fooddateshopdetails[0]->google_map_address!=''){
 $lat  = $fooddateshopdetails[0]->latitude;
$long  = $fooddateshopdetails[0]->longitude;
 ?>
          <div class="detail_hall_dimention" id="map" style="height: 230px!important">  </div>
          <?php } ?> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">
                            @if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif
                        </div>
                        <div class="items-reviews">
                            @foreach($fooddateshopreview as $customerreview)
                          @php $userinfo = Helper::getuserinfo($customerreview->customer_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{ $userinfo->cus_pic }}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{ $userinfo->cus_name }}</h2>
                                    <div class="stars">
                                        <img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png"/>
                                    </div>
                                    <p>{{ $customerreview->comments }}</p>
                                </div>
                            </div>
                         @endforeach
                        </div>
                        {{$fooddateshopreview->links()}}
                        <!-- <ul class="pagenation">
                            <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">10</a></li>
                            <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                            <iframe class="service-video" src="{{ $fooddateshopdetails[0]->mc_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
@php $tbl_field='service_id';  @endphp
            <div class="desserts">
                <center>
                    <ul class="link">
                             @php    
                    
                    $k=1; $jk=0;  @endphp
                    @foreach($servicecategoryAndservices as $categories)
                    @php 
                 
                        if(count($categories->serviceslist)>0){
                        if($k==1){ $cl='select'; }else{ $cl=''; }                     
                    @endphp
                  @if(isset($categories->attribute_title) && $categories->attribute_title !='')
                        <li class="active">
                            <div class="image"><a href="#{{$k}}"  class="cat {{$cl}}" data-toggle="tab" @if(isset($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id) && $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id!='') onclick="getbeautyservice('{{ $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id }}','{{ $branchid }}','{{$tbl_field}}');"  @endif><img src="{{ $categories->image }}" alt=""></a></div>
                            <h2><a href="#{{$k}}"  class="cat {{$cl}}" data-toggle="tab" @if(isset($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id) && $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id!='') onclick="getbeautyservice('{{ $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id }}','{{ $branchid }}','{{$tbl_field}}');"  @endif>{{ $categories->attribute_title or '' }}</a></h2>
                        </li>
                        
                      <!--   <li>
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/beaut-elegance.png" alt=""></a></div>
                            <h2><a href="">Laser</a></h2>
                        </li> -->
                             @endif
                @php $k++; $jk++; } @endphp
                @endforeach
                    </ul>
                </center>

                <div class="box p-15">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>

        </div>
    </div>

    @endsection
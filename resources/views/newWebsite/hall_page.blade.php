@extends('newWebsite.layouts.master')
@section('content')
 <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="images/shop-logo.png" alt="" />
                        <h2>Hotel halls</h2>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <form>
                            <select>
                                <option>Other halls</option>
                                <option>Other halls</option>
                                <option>Other halls</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>Name of hall</h2>
                                <h2>Address</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                            </div>
                            <div class="right">
                                <p>Al BAHA</p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.... <a href="">More</a></p>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="det">
                                        <p><strong>Hall Capacity:</strong> <span>200 Person</span></p>
                                        <p><strong>Hall Type:</strong> <span>Women</span></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="det">
                                        <p><strong>Hall Dimension s</strong></p>
                                        <p><strong>Length:</strong> <span>100</span></p>
                                        <p><strong>Width:</strong> <span>100</span></p>
                                        <p><strong>Area:</strong> <span>100</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <ul class="tabs">
                            <li class="active"><a data-toggle="tab" href="#Free">Free service</a></li>
                            <li><a data-toggle="tab" href="#Paid">Paid service</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="Free" class="tab-pane fade in active">
                                <div class="p-15">
                                    <p>Water supply</p>
                                    <p>Special photography section</p>
                                    <p>Dance show</p>
                                </div>
                            </div>
                            <div id="Paid" class="tab-pane fade">
                                <div class="p-15">
                                    <p>Water supply</p>
                                    <p>Special photography section</p>
                                    <p>Dance show</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <center><a href="" class="add-to-cart">Add to cart</a></center>
            <div class="row">
                <div class="col-md-6">
                    <div class="p-15 box">
                        Video
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">map</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="box p-15">
                        <div class="title-head">Customer Reviews</div>
                        <div class="items-reviews">
                            <div class="item">
                                <div class="tump">
                                    <img src="images/customer-image1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <h2>Customer Names</h2>
                                    <div class="stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the ......</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="tump">
                                    <img src="images/customer-image1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <h2>Customer Names</h2>
                                    <div class="stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the ......</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="tump">
                                    <img src="images/customer-image1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <h2>Customer Names</h2>
                                    <div class="stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry. Lorem Ipsum has been the ......</p>
                                </div>
                            </div>
                        </div>

                        <div class="perivious-next">
                            <a href="">PERIVIOUS</a>
                            <a href="">NEXT</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="box p-15">
                        <p class="fw-700">Order Summary</p>
                        <br>
                        <p class="fw-700">Hall Price</p>
                        <br>
                        <p class="fw-700">Insurance amount</p>
                        <p class="fw-700">PAID SERVICE</p>
                        <br>
                        <ul class="fw-700">
                            <li>- SERVICE 1</li>
                            <li>- SERVICE 2</li>
                            <li>- SERVICE 3</li>
                        </ul>
                        <br>
                        <p class="fw-700">ADD FOOD</p>
                        <div class="flex-end">
                            <a class="btn-site" href="">Our Menue</a>
                            <a class="btn-site" href="">Outside vendor</a>
                        </div>
                        <a class="btn-site block-bn" href="">RESERVE NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
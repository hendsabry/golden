@extends('newWebsite.layouts.master')
@section('content')
    <div class="outer_wrapper">
        <div class="inner_wrap">

        <!-- search-section -->
            <!-- search-section -->
            <div class="page-left-right-wrapper">

                <div>
                    <h1>{{ $cmscontent[0]->title }}</h1>
                    {!! $cmscontent[0]->content !!}

                </div>
                <!-- page-right-section -->
            </div>
            <!-- page-left-right-wrapper -->
        </div>
        <!-- outer_wrapper -->
    </div>
@endsection
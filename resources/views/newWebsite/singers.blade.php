@extends('newWebsite.layouts.master')
@section('content')
    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <?php

                                $name = 'name';

                                if(Session::get('lang_file')!='en_lang')

                                {

                                    $name = 'name_ar';

                                }

                               // echo $singerDetails->$name;

                                ?>
                                <h2>{{$singerDetails->$name}}</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php

                                    $address = 'address';

                                    if(Session::get('lang_file')!='en_lang')

                                    {

                                        $address = 'address_ar';

                                    }

                                    echo $singerDetails->$address;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>
                                @php

                                        @endphp

                                @php $about='about'@endphp

                                @if(Session::get('lang_file')!='en_lang')

                                    @php $about= 'about_ar'; @endphp

                                @endif

                                @php echo nl2br($singerDetails->$about); @endphp</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        MAP
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                            @foreach($allreview as $val)

                                @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="@php if(isset($userinfo->cus_pic) && $userinfo->cus_pic!=''){echo @$userinfo->cus_pic;} @endphp" alt="">
                                </div>
                                <div class="caption">
                                    <h2>@php if(isset($userinfo->cus_name) && $userinfo->cus_name!=''){echo @$userinfo->cus_name;}else{echo 'N/A';} @endphp</h2>
                                    <div class="stars">
                                        @if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif
                                    </div>
                                    <p>@php if(isset($val->comments) && $val->comments!=''){echo @$val->comments;} @endphp<</p>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        {{$allreview->links()}}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe style="width: 100%;height: 100%" class="service-video" src="{{$singerDetails->video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                    </div>
                </div>
            </div>

            <h3 class="head-title">{{ (Lang::has(Session::get('lang_file').'.SINGER_INFORMATION')!= '')  ?  trans(Session::get('lang_file').'.SINGER_INFORMATION'): trans($OUR_LANGUAGE.'.SINGER_INFORMATION')}}</h3>
            <div class="box p-15" style="background: #f4f4f4">
                <div class="row">
                    <div class="col-md-3">
                        <div class="user-image"><img src="{{$singerDetails->image}}" alt="" /></div>
                    </div>
                    <div class="col-md-9">
                        {!! Form::open(array('url'=>"insert_singer",'class'=>'form','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'singer_frm','id'=>'singer_frm')) !!}
                        {{ csrf_field() }}
                        <h3 class="head-title">
                    <?php

                        $name = 'name';

                        if(Session::get('lang_file')!='en_lang')

                        {

                          $name = 'name_ar';

                        }

                        echo ucfirst($singerDetails->$name);
                                            ?>
                        </h3>
                        <input type="hidden" name="quote_for" value="singer"/>

                        <input type="hidden" name="shop_id" value="{{$sid}}"/>

                        <input type="hidden" name="music_id" value="{{$lid}}"/>

                        <input type="hidden" name="vendor_id" value="{{$singerDetails->vendor_id}}"/>
                        <?php

                        if(Session::has('customerdata.user_id'))

                        {

                            $userid  = Session::get('customerdata.user_id');

                        }

                        ?>

                        <input type="hidden" name="user_id" value="<?php if(isset($userid) && $userid!=''){echo $userid;}?>"/>

                        <input type="hidden" name="singer_name" value="{{$singerDetails->$name}}"/>

                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@if(Lang::has(Session::get('lang_file').'.HALL')!= '') {{ trans(Session::get('lang_file').'.HALL')}}  @else {{ trans($OUR_LANGUAGE.'.HALL')}} @endif</label>
                                        <input  class="t-box form-control" name="hall" id="hall" type="text" maxlength="75" value="{!! Input::old('hall') !!}"/>
                                        <div>@if($errors->has('hall'))<span class="error">{{ $errors->first('hall') }}</span>@endif </div>
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ (Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')}} </label>
                                    <select name="occasion" id="occasion" class="form-control">

                                        <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '') {{  trans(Session::get('lang_file').'.Select_Occasion_Type') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Occasion_Type') }} @endif</option>

                                        <?php

                                        if(Session::get('lang_file')!='en_lang')

                                        {

                                            $getArrayOfOcc = array( '2'=>'مناسبة الزفاف');

                                        }

                                        else

                                        {

                                            $getArrayOfOcc = array( '2'=>'Wedding Occasion');

                                        }

                                        foreach($getArrayOfOcc as $key=>$ocval){?>

                                        <option value="" disabled="" style="color: #d2cece;">{{$ocval}}</option>

                                        <?php

                                        $setOccasion = Helper::getAllOccasionNameList($key);

                                        foreach($setOccasion as $val){ ?>

                                        <option value="{{$val->id}}"<?php if(isset($occasiontype) && $occasiontype==$val->id){ echo 'selected="selected"'; }?>>

                                            <?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>

                                        </option>

                                        <?php } ?>

                                        <?php } ?>

                                    </select>

                                    <div>@if($errors->has('occasion'))<span class="error">{{ $errors->first('occasion') }}</span>@endif </div>

                                </div>
                            </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}</label>
                                        <select class="checkout-small-box form-control" name="city" id="city">

                                            <option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_CITY') }} @endif</option>



                                            @php $getC = Helper::getCountry(); @endphp

                                            @foreach($getC as $cbval)



                                                <option value="" disabled="" style="color: #d2cece;">{{$cbval->co_name}}</option>



                                                @php $getCity = Helper::getCityb($cbval->co_id); @endphp

                                                @foreach ($getCity as $val)

                                                    @php if(isset($city_id) && $city_id==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} @endphp

                                                    @if($selected_lang_code !='en')

                                                        @php $ci_name= 'ci_name_ar'; @endphp

                                                    @else

                                                        @php $ci_name= 'ci_name'; @endphp

                                                    @endif



                                                    <option value="{{ $val->ci_id }}" {{ $selectedcity }} >{{ $val->$ci_name }}</option>



                                                @endforeach

                                            @endforeach



                                        </select>

                                       <div> @if($errors->has('city'))<span class="error">{{ $errors->first('city') }}</span>@endif </div>

                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@if(Lang::has(Session::get('lang_file').'.LOCATION')!= '') {{ trans(Session::get('lang_file').'.LOCATION')}}  @else {{ trans($OUR_LANGUAGE.'.LOCATION')}} @endif</label>
                                        <input class="t-box form-control" type="text" name="location" maxlength="75" id="location" value="{!! Input::old('location') !!}">

                                        <div>@if($errors->has('location'))<span class="error">{{ $errors->first('location') }}</span>@endif </div>

                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>@if(Lang::has(Session::get('lang_file').'.DURATION')!= '') {{ trans(Session::get('lang_file').'.DURATION')}}  @else {{ trans($OUR_LANGUAGE.'.DURATION')}} @endif</label>
                                        <input class="t-box form-control"  name="duration" id="duration" type="text" maxlength="2" value="{!! Input::old('duration') !!}" style="max-width:125px;" onkeypress="return isNumber(event)" >

                                        <div>@if($errors->has('duration'))<span class="error">{{ $errors->first('duration') }}</span>@endif </div>

                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group data">
                                        <label>@if(Lang::has(Session::get('lang_file').'.DATE')!= '') {{ trans(Session::get('lang_file').'.DATE')}}  @else {{ trans($OUR_LANGUAGE.'.DATE')}} @endif / @if(Lang::has(Session::get('lang_file').'.TIME')!= '') {{ trans(Session::get('lang_file').'.TIME')}}  @else {{ trans($OUR_LANGUAGE.'.TIME')}} @endif</label>
                                        <input class="t-box cal-t datetimepicker form-control"  autocomplete="off" name="date" id="date" value="{!! Input::old('date') !!}" type="text" >

                                        <div>@if($errors->has('date'))<span class="error">{{ $errors->first('date') }}</span>@endif </div>
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>@if(Lang::has(Session::get('lang_file').'.COMMENTS')!= '') {{ trans(Session::get('lang_file').'.COMMENTS')}}  @else {{ trans($OUR_LANGUAGE.'.COMMENTS')}} @endif </label>
                                    <textarea class="text-area form-control" style="min-height: 150px" name="comments" id="comments">{!! Input::old('comments') !!}</textarea>

                                    <div>@if($errors->has('comments'))<span class="error">{{ $errors->first('comments') }}</span>@endif </div>


                                </div>
                            </div>
                            <center>
                                <input type="submit" name="submit" value="@if(Lang::has(Session::get('lang_file').'.ASK_FOR_A_QUOTE')!= '') {{ trans(Session::get('lang_file').'.ASK_FOR_A_QUOTE')}}  @else {{ trans($OUR_LANGUAGE.'.ASK_FOR_A_QUOTE')}} @endif" />
                            </center>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')



    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker();
        });
    </script>


    <script type="text/javascript">

        function isNumber(evt, element) {

            var charCode = (evt.which) ? evt.which : event.keyCode

            if ( charCode > 31 &&

                (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.

                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.

                (charCode < 48 || charCode > 57))

            {



                return false;

            }

            return true;

        }

    </script>
    {{--<script src="{{ url('')}}/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>--}}

    <script>



        var checkPastTime = function(inputDateTime) {

            if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {

                var current = new Date();



                //check past year and month

                if (inputDateTime.getFullYear() < current.getFullYear()) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                }



                // 'this' is jquery object datetimepicker

                // check input date equal to todate date

                if (inputDateTime.getDate() == current.getDate()) {

                    if (inputDateTime.getHours() < current.getHours()) {

                        $('.datetimepicker').datetimepicker('reset');

                    }

                    this.setOptions({

                        minTime: current.getHours() + ':00' //here pass current time hour

                    });

                } else {

                    this.setOptions({

                        minTime: false

                    });

                }

            }

        }


        {{--<script src="{{ url('')}}/public/assets/timepicker/jquery.datetimepicker.js"></script>--}}

    <script>



        var checkPastTime = function(inputDateTime) {

            if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {

                var current = new Date();



                //check past year and month

                if (inputDateTime.getFullYear() < current.getFullYear()) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {

                    $('.datetimepicker').datetimepicker('reset');

                    alert("Sorry! Past date time not allow.");

                }



                // 'this' is jquery object datetimepicker

                // check input date equal to todate date

                if (inputDateTime.getDate() == current.getDate()) {

                    if (inputDateTime.getHours() < current.getHours()) {

                        $('.datetimepicker').datetimepicker('reset');

                    }

                    this.setOptions({

                        minTime: current.getHours() + ':00' //here pass current time hour

                    });

                } else {

                    this.setOptions({

                        minTime: false

                    });

                }

            }



            var currentYear = new Date();

            $('.datetimepicker').datetimepicker({

                format:'d M Y g:i A',

                minDate : 0,

                yearStart : currentYear.getFullYear(), // Start value for current Year selector

                onChangeDateTime:checkPastTime,

                onShow:checkPastTime

            });









        /*

       jQuery(window).load(function()

       {

         jQuery("body").on('mouseover', '.datetimepicker', function() {

           var today = '2018-11-09';

             jQuery(this).datetimepicker({

             ampm: true, // FOR AM/PM FORMAT

             format : 'd M Y g:i A',

             minDate : 0,

           minTime: 0

          });

         });

       })

       */

    </script>

    <style type="text/css">

        a.morelink {

            text-decoration:none;

            outline: none;

        }

        .morecontent span {

            display: none;

        }

    </style>

    <script type="text/javascript">

        jQuery(document).ready(function(){

            //jQuery('#date').datepicker({format: "dd/mm/yyyy"});

            jQuery("#singer_frm").validate({

                rules: {

                    "hall" : {

                        required : true

                    },

                    "occasion" : {

                        required : true

                    },

                    "city" : {

                        required : true

                    },

                    "location" : {

                        required : true

                    },

                    "duration" : {

                        required : true,

                        number: true

                    },

                    "date" : {

                        required : true

                    },

                    "time" : {

                        required : true

                    },

                    /*"comments" : {

                      required:true

                    },*/

                },

                messages: {

                    "hall": {

                        required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_HALL')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_HALL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_HALL') }} @endif"

                    },

                    "occasion": {

                        required:  "@if (Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= '') {{  trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_OCCASION') }} @endif"

                    },

                    "city": {

                        required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_CITY') }} @endif"

                    },

                    "location": {

                        required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_LOCATION') }} @endif"

                    },

                    "duration": {

                        required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DURATION') }} @endif"

                    },

                    "date": {

                        required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DATE') }} @endif"

                    },

                    "time": {

                        required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_TIME') }} @endif"

                    },

                    /*"comments": {

                      required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')!= '') {{  trans(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_COMMENTS_QUERIES') }} @endif"

          }, */

                }

            });

            jQuery(".btn-info-wisitech").click(function() {

                if(jQuery("#singer_frm").valid()) {

                    jQuery('#singer_frm').submit();

                }

            });

        });

        $(document).ready(function()

        {

            var showChar = 400;

            var ellipsestext = "...";

            var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";

            var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";

            $('.more').each(function()

            {

                var content = $(this).html();

                if(content.length > showChar)

                {

                    var c = content.substr(0, showChar);

                    var h = content.substr(showChar-1, content.length - showChar);

                    var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

                    $(this).html(html);

                }

            });



            $(".morelink").click(function(){

                if($(this).hasClass("less")) {

                    $(this).removeClass("less");

                    $(this).html(moretext);

                } else {

                    $(this).addClass("less");

                    $(this).html(lesstext);

                }

                $(this).parent().prev().toggle();

                $(this).prev().toggle();

                return false;

            });

        });



    </script>

    <div class="action_popup">

        <div class="action_active_popup">

            <div class="action_content">{{ Session::get('message') }}</div>

            <div class="action_btnrow">

                <input type="hidden" id="delid" value=""/>

                <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>

        </div>

    </div>

    @if(Session::has('message'))

        <script type="text/javascript">

            $(document).ready(function()

            {

                $('.action_popup').fadeIn(500);

                $('.overlay').fadeIn(500);

            });

        </script>

    @endif

    <script type="text/javascript">

        $('.status_yes').click(function()

        {

            $('.overlay, .action_popup').fadeOut(500);

        });

    </script>


@endsection
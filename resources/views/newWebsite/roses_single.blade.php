@extends('newWebsite.layouts.master')
@section('style')
<style type="text/css">
	.packages-item{
		display: none;
	}
</style>
@endsection
@section('content')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
@endphp
 <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</h2>
                                <h2>{{$vendordetails->address}}</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                	<?php
							        $getcityname = Helper::getcity($vendordetails->city_id); 
							        $mc_name = 'ci_name'; 
							        if(Session::get('lang_file')!='en_lang')
							      {
							          $mc_name = 'ci_name_ar'; 
							        }
							        echo $getcityname->$mc_name; 
							      ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>
                            	@if($lang != 'en_lang') {{nl2br($vendordetails->mc_discription) }} @else {{nl2br($vendordetails->mc_discription)}} @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                                   @php if($vendordetails->google_map_address!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp
							          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
							         </div>
							          @php }  @endphp
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                        	@foreach($allreview as $val)
           
                   @php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{$userinfo->cus_pic}}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{$userinfo->cus_name}}</h2>
                                    <div class="stars">
                                       @if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif
                                    </div>
                                    <p>{{$val->comments}}</p>
                                </div>
                            </div>
                           @endforeach
                        </div>
                        {{$allreview->links()}}
                        <!-- <ul class="pagenation">
                            <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">10</a></li>
                            <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box"><iframe width="100%" height="315" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <center>
                     <ul class="link">
                        <li>
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/roses-04.png" alt=""></a></div>
                            <h2><a href="{{ route('roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1]) }}" @if(request()->type!=2) class="select" @endif>{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}}</a></h2>
                        </li>
                        <li class="active">
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/roses-05.png" alt=""></a></div>
                            <h2><a  @if(request()->type==2) class="select" @endif href="{{ route('single-roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2]) }}">{{ (Lang::has(Session::get('lang_file').'.Single')!= '')  ?  trans(Session::get('lang_file').'.Single'): trans($OUR_LANGUAGE.'.Single')}}</a></h2>
                        </li>
                    </ul>
                </center>
               
                <div class="row">
                    <div class="col-md-8">
                        <div class="">
                            <div class="row">
                            	@foreach($product as $val)

                                <div class="col-md-6">
                                    <div class="item-style clearfix">
                                        <div class="tump">
                                            <img src="{{$val->pro_Img}}" alt="">
                                        </div>
                                        <div class="caption">
                                            <a href="">{{$val->pro_title}}</a>
                                            <p>{{$val->pro_desc}}</p>
                                            <div class="flex-end">
                                                <div class="price">
                                                	
                                                	{{$val->pro_price }}
                                         
                                            	</div>
                                                <a href="" class="cart" id="{{$val->pro_id}}">Add To Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box p-15">
                            <h3 class="head-title" style="margin-top:0">Your Packages</h3>
                            <div class="all-item">
                            	@foreach($product as $val)
                                <div class="packages-item" id="package_{{$val->pro_id}}">
                                    <div class="img">
                                        <img src="{{$val->pro_Img}}" alt="" />
                                    </div>
                                    <div class="title">
                                        <p>{{$val->pro_title}}</p>
                                        <p>Quantity: 1</p>
                                        <p class="price">{{$val->pro_price}} SAR</p>
                                        <a href="" class="delete" id="{{$val->pro_id}}"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                            <h3 class="head-title">@if(Lang::has(Session::get('lang_file').'.Wrapping_Type')!= '') {{ trans(Session::get('lang_file').'.Wrapping_Type')}}  @else {{ trans($OUR_LANGUAGE.'.Wrapping_Type')}} @endif</h3>
                            <div class="row">
                            	@foreach($product_Option_value_type as $type)
                                <div class="col-md-4">
                                    <div class="item-radio">
                                        <img src="{{$type->image}}" alt="" />
                                        <input id="name" class="Checktype" data-price="{{ currency($type->price,'SAR',$Current_Currency, $format = false) }}" name="product_option_type" type="radio"  value="{{$type->id}}">
                                        <p class="price">{{ currency($type->price,'SAR',$Current_Currency) }} </p>
                                    </div>
                                </div>
                                @endforeach
                        
                            </div>
                            <h3 class="head-title">@if(Lang::has(Session::get('lang_file').'.Wrapping_Design')!= '') {{ trans(Session::get('lang_file').'.Wrapping_Design')}}  @else {{ trans($OUR_LANGUAGE.'.Wrapping_Design')}} @endif</h3>
                            <div class="row">
                            	@foreach($product_Option_value_design as $design)
                                <div class="col-md-4">
                                    <div class="item-radio">
                                        <img src="{{$design->image}}" alt="" />
                                          <input id="name4" class="Checkdesign"  data-price="{{ currency($design->price,'SAR',$Current_Currency, $format = false) }}" name="product_option_design" type="radio" value="{{$design->id}}">
                                        <p class="price">{{ currency($design->price,'SAR',$Current_Currency) }}</p>
                                    </div>
                                </div>
                                @endforeach
                              
                            </div>
                            <br />
                            <p class="fw-700">@if(Lang::has(Session::get('lang_file').'.Total_Price')!= '') {{ trans(Session::get('lang_file').'.Total_Price')}}  @else {{ trans($OUR_LANGUAGE.'.Total_Price')}} @endif : <span class="price">1500 SAR</span></p>
                            <div class="form-group" style="margin-top:15px">
                                <div class="koha-ready">
                                    <center>
                                        <a href="">Terms and conditions</a>
                                        <a href="" class="addtocart">Add To Cart</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <center>
            	{{$product->links()}}
          <!--       <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul> -->
            </center>

        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $(".cart").click(function(e){
  	e.preventDefault();
    var id =$(this).attr('id');
    $("#package_"+id).show();
  });
  $(".delete").click(function(e){
  	e.preventDefault();
    var id =$(this).attr('id');
    $("#package_"+id).hide();
  });
});
</script>
@endsection
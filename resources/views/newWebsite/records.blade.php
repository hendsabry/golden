@extends('newWebsite.layouts.master')
@section('content')
    <?php
    if(Session::has('customerdata.user_id'))
    {
        $nuserid  = Session::get('customerdata.user_id');
        $getnInfo = Helper::getuserinfo($nuserid);
        if($getnInfo->cus_name==''){ $cuname='';}else{ $cuname=$getnInfo->cus_name;}
    }
    ?>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>Sound SystemShop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>
                                    <?php
                                    $name = 'name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $name = 'name_ar';
                                    }
                                    echo $singerDetails->$name;
                                    ?>
                                </h2>
                                <h2>
                                    <?php
                                    $address = 'address';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $address = 'address_ar';
                                    }
                                    echo $singerDetails->$address;
                                    ?>
                                </h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($singerDetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>
                                @php
                                        @endphp
                                @php $about='about'@endphp
                                @if(Session::get('lang_file')!='en_lang')
                                    @php $about= 'about_ar'; @endphp
                                @endif
                                @php echo nl2br($singerDetails->$about); @endphp
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                        @php
                            $long  = @$singerDetails->longitude;
                            $lat = @$singerDetails->latitude;
                        @endphp


                        @if($long !='' && $lat!='')
                            <input type="hidden" id="long" value="{{ $long }}">
                            <input type="hidden" id="lat" value="{{ $lat }}">
                            <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;"> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                            @foreach($allreview as $val)
                                @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{$userinfo->cus_pic}}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{$userinfo->cus_name}}</h2>
                                    <div class="stars">
                                        @if($val->ratings)
                                            @for($i = 0 ; $i < $val->ratings ; $i++ )
                                                <i class="fa fa-star"></i>
                                            @endfor
                                        @endif
                                    </div>
                                    <p>{{$val->comments}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        {{ $allreview->links() }}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                <?php
                    if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){
                    $mc_name = 'video_description';
                        if(Session::get('lang_file')!='en_lang')
                        {
                          $mc_name = 'video_description_ar';
                        }
                        echo $singerDetails->$mc_name;
                        }
                        ?>
                    <div class="p-15 box"><iframe width="100%" height="315" src="{{$singerDetails->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li>
                            <div class="image"><a href="{{ route('acousticequipment',[$id,$sid,$lid]) }}"><img src="{{url('newWebsite')}}/images/sound-system1.png" alt=""></a></div>
                            <h2><a href="{{ route('acousticequipment',[$id,$sid,$lid]) }}">{{ (Lang::has(Session::get('lang_file').'.EQUIPMENT')!= '')  ?  trans(Session::get('lang_file').'.EQUIPMENT'): trans($OUR_LANGUAGE.'.EQUIPMENT')}}</a></h2>
                        </li>
                        <li class="active">
                            <div class="image"><a href="{{ route('acousticrecording',[$id,$sid,$lid]) }}"><img src="{{url('newWebsite')}}/images/Recording1.png" alt=""></a></div>
                            <h2><a href="{{ route('acousticrecording',[$id,$sid,$lid]) }}" class="select">{{ (Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')}}</a></h2>
                        </li>
                    </ul>
                </center>

                <div class="box p-15">
                    {!! Form::open(array('url'=>"insert_acoustic",'class'=>'form','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'acoustic_frm','id'=>'acoustic_frm')) !!}
                    {{ csrf_field() }}
                    <input type="hidden" name="quote_for" value="recording"/>
                    <input type="hidden" name="shop_id" value="{{$sid}}"/>
                    <input type="hidden" name="music_id" value="{{$lid}}"/>
                    <input type="hidden" name="vendor_id" value="{{$singerDetails->vendor_id}}"/>
                    <?php
                    if(Session::has('customerdata.user_id'))
                    {
                        $userid  = Session::get('customerdata.user_id');
                    }
                    ?>
                    <input type="hidden" name="user_id" value="<?php if(isset($userid) && $userid!=''){echo $userid;}?>"/>
                        <h3 class="head-title">{{$singerDetails->$name}}</h3>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>@if(Lang::has(Session::get('lang_file').'.THE_GROOM_NAME')!= '') {{ trans(Session::get('lang_file').'.THE_GROOM_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.THE_GROOM_NAME')}} @endif</label>
                                    <input class="t-box form-control" name="groom_name" maxlength="75" id="groom_name" value="{{ $cuname }}" type="text">
                                    <div>@if($errors->has('groom_name'))<span class="error">{{ $errors->first('groom_name') }}</span>@endif </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ (Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')}}</label>
                                    <select name="occasion" id="occasion" class="t-box form-control">
                                        <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '') {{  trans(Session::get('lang_file').'.Select_Occasion_Type') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Occasion_Type') }} @endif</option>
                                        <?php
                                        if(Session::get('lang_file')!='en_lang')
                                        {
                                            $getArrayOfOcc = array( '2'=>'مناسبة الزفاف');
                                        }
                                        else
                                        {
                                            $getArrayOfOcc = array( '2'=>'Wedding Occasion');
                                        }
                                        foreach($getArrayOfOcc as $key=>$ocval){?>
                                        <option value="" disabled="" style="color: #d2cece;">{{$ocval}}</option>
                                        <?php
                                        $setOccasion = Helper::getAllOccasionNameList($key);
                                        foreach($setOccasion as $val){ ?>
                                        <option value="{{$val->id}}"<?php if(isset($occasiontype) && $occasiontype==$val->id){ echo 'selected="selected"'; }?>>
                                            <?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
                                        </option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <div>@if($errors->has('occasion'))<span class="error">{{ $errors->first('occasion') }}</span>@endif </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@if(Lang::has(Session::get('lang_file').'.HALL')!= '') {{ trans(Session::get('lang_file').'.HALL')}}  @else {{ trans($OUR_LANGUAGE.'.HALL')}} @endif</label>
                                    <input class="t-box form-control" name="hall" id="hall" type="text" maxlength="75" value="{!! Input::old('hall') !!}">
                                    <div>@if($errors->has('hall'))<span class="error">{{ $errors->first('hall') }}</span>@endif </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@if(Lang::has(Session::get('lang_file').'.SONG_NAME')!= '') {{ trans(Session::get('lang_file').'.SONG_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.SONG_NAME')}} @endif</label>
                                    <input type="text" class="t-box form-control" name="song_name" maxlength="75" id="song_name" value="{!! Input::old('song_name') !!}">
                                   <div> @if($errors->has('song_name'))<span class="error">{{ $errors->first('song_name') }}</span>@endif </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>@if(Lang::has(Session::get('lang_file').'.MUSIC_TYPE')!= '') {{ trans(Session::get('lang_file').'.MUSIC_TYPE')}}  @else {{ trans($OUR_LANGUAGE.'.MUSIC_TYPE')}} @endif</label>
                                    <input class="t-box form-control" name="music_type" id="music_type" maxlength="75" value="{!! Input::old('music_type') !!}" type="text">
                                     <div>   @if($errors->has('music_type'))<span class="error">{{ $errors->first('music_type') }}</span>@endif </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}</label>
                                    <select class="checkout-small-box form-control" name="city" id="city">
                                        <option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_CITY') }} @endif</option>
                                        @php $getC = Helper::getCountry(); @endphp
                                        @foreach($getC as $cbval)
                                            <option value="" disabled="" style="color: #d2cece;">{{$cbval->co_name}}</option>
                                            @php $getCity = Helper::getCityb($cbval->co_id); @endphp
                                            @foreach ($getCity as $val)
                                                @php if(isset($city_id) && $city_id==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} @endphp
                                                @if($selected_lang_code !='en')
                                                    @php $ci_name= 'ci_name_ar'; @endphp
                                                @else
                                                    @php $ci_name= 'ci_name'; @endphp
                                                @endif
                                                <option value="{{ $val->ci_id }}" {{ $selectedcity }} >{{ $val->$ci_name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                   <div> @if($errors->has('city'))<span class="error">{{ $errors->first('city') }}</span>@endif
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group data">
                                    <label>@if(Lang::has(Session::get('lang_file').'.DATE')!= '') {{ trans(Session::get('lang_file').'.DATE')}}  @else {{ trans($OUR_LANGUAGE.'.DATE')}} @endif / @if(Lang::has(Session::get('lang_file').'.TIME')!= '') {{ trans(Session::get('lang_file').'.TIME')}}  @else {{ trans($OUR_LANGUAGE.'.TIME')}} @endif</label>
                                    <input class="t-box form-control cal-t datetimepicker" autocomplete="off" name="date" id="date" value="{!! Input::old('date') !!}" type="text" >
                                    <div>@if($errors->has('date'))<span class="error">{{ $errors->first('date') }}</span>@endif </div>
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <center>
                            <input type="submit" name="submit" class="form-btn btn-info-wisitech" value="@if(Lang::has(Session::get('lang_file').'.GET_A_QUOTE')!= '') {{ trans(Session::get('lang_file').'.GET_A_QUOTE')}}  @else {{ trans($OUR_LANGUAGE.'.GET_A_QUOTE')}} @endif" />
                        </center>
                    {!! Form::close() !!}
                </div>

            </div>


        </div>
    </div>
    @endsection
@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker();
        });
    </script>
    @endsection
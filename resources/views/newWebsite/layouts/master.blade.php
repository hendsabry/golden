<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> @yield('title') </title>
    <link href="{{url('newWebsite')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('newWebsite')}}/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('newWebsite')}}/css/owl.carousel.min.css" rel="stylesheet">
    <link href="{{url('newWebsite')}}/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="{{url('newWebsite')}}/css/style.css" rel="stylesheet">
    @yield('style')
</head>

<body>

    @if (Route::getCurrentRoute()->uri() == '/')
        @include('newWebsite.layouts.header')
    @else
        @include('newWebsite.layouts.header1')
    @endif


    @yield('content')


    @include('newWebsite.layouts.footer')


    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--}}

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script src="{{url('newWebsite')}}/js/owl.carousel.min.js"></script>
    <script src="{{url('newWebsite')}}/js/bootstrap.min.js"></script>
    <script src="{{url('newWebsite')}}/js/jquery.waterwheelCarousel.min.js"></script>
    <script src="{{url('newWebsite')}}/js/script.js"></script>

    <script>

        function initMap() {
            var lat = parseInt($('#lat').val());
            var long = parseInt($('#long').val());
            var myLatLng = {lat: lat , lng: long};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
    </script>


    <script type="text/javascript">

        $( function() {
            $( "#DA" ).datepicker({
                format: "d M, yyyy",
                endDate: '+14m',
                autoclose: true,
                startDate: new Date(),
            });
        } );
    </script>


    <script type="text/javascript">

        function Lang_change(str)

        {

            var language_code = str;

            var token =  @php csrf_token(); @endphp

            jQuery.ajax

            ({

                type:'GET',

                url:"<?php echo url('new_change_languages');?>",

                data:{'Language_change':language_code,'csrf-token':token},

                success:function(data)

                {

                    //alert(data);

                    window.location.reload();

                }

            });

        }

    </script>


    @yield('script')
</body>

</html>

    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img class="img-responsive" src="{{url('newWebsite')}}/images/gc-logo.png" alt="" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Wedding & Occasions</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Halls</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Hotel Halls</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Large Halls</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Smal Halls</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Food</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Buffets</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Desserts</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Dates</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-sm-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Occasion coordinator</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Kosha</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Photography</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Hospitality</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Roses</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Special Events</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">E-invitations</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Beauty & Elegance</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Beauty center</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Spa</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Artist</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Makeup</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Reviving Concerts</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Singers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Popular bands</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Sound systems</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Clinics</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Dental & Dermatology</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Cosmetics & Laser</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-sm-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Shopping</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Dresses</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Abaya</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Perfumes</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Jewelry</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                    <div class="col col-xs-6 col-md-3">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <span class="nav-link text-bold">Travel Agencies</span>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#">Active</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Link item</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Link item</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.col  -->
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="">Business Meeting</a></li>
                    <li><a href="">City</a></li>
                </ul>
                <div class="navbar-right">
                    <ul class="list-inline list-link">
                        <li><a href=""><img src="{{url('newWebsite')}}/images/cart-icon.png"> Cart</a></li>
                        <li><a data-toggle="modal" href="#login"><img src="images/login-icon.png"> Login</a></li>
                        <li><a href="">عربي</a></li>
                        <li><a data-toggle="modal" href="#joinnow" class="joinnow" href="">Join Now +</a></li>
                    </ul>
                </div>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <!-- Modal Login -->
    <div class="modal fade" id="login" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>LOG IN</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'login-signup/checkloginaccount', 'method' => 'post', 'name'=>'loginfrm', 'id'=>'loginfrm', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group">
                            <label for="usrname">{{ (Lang::has(Session::get('lang_file').'.EMAILADDRRESS')!= '')  ?  trans(Session::get('lang_file').'.EMAILADDRRESS'): trans($OUR_LANGUAGE.'.EMAILADDRRESS')}}</label>
                            {!! Form::text('email', null, array( 'class'=>'form-control','id'=>'usrname','placeholder'=>'Enter email','maxlength'=>'80')) !!}
                            {{--<input type="text" class="form-control" id="usrname" placeholder="Enter email">--}}
                            <span class="glyphicon glyphicon-user"></span>
                        </div>
                        <div class="form-group">
                            <label for="psw">{{ (Lang::has(Session::get('lang_file').'.PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.PASSWORD'): trans($OUR_LANGUAGE.'.PASSWORD')}}</label>
                            {!! Form::password('password',  array( 'class'=>'form-control','id'=>'psw','placeholder'=>'Enter password','maxlength'=>'20')) !!}
                            {{--<input type="text" class="form-control" id="psw" placeholder="Enter password">--}}
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                        <div class="form-group clearfix">
                            <div class="pull-right">
                                <a href="{{url('')}}/userforgotpassword">{{ (Lang::has(Session::get('lang_file').'.MER_FORGOT_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.MER_FORGOT_PASSWORD'): trans($OUR_LANGUAGE.'.MER_FORGOT_PASSWORD')}} ?</a>
                            </div>
                        </div>
                    {!! Form::submit('login', array('class'=>'btn btn-default btn-success btn-block')) !!}
                        {{--<button type="submit" class="btn btn-default btn-success btn-block">Login</button>--}}
                        {!! Form::close() !!}
                    <div class="or"><span>OR</span></div>
                    <a href="" class="login-google"><img src="images/google-logo.png" alt="" /> Log in with Google</a>
                    <p>Don’t have an account? <a href="">Sign Up</a></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal joinnow -->
    <div class="modal fade" id="joinnow" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>SIGN UP</h4>
                </div>
                <div class="modal-body">

                    {!! Form::open(['url' => 'login-signup/adduseraccount', 'method' => 'post', 'name'=>'regfrm', 'id'=>'regfrm', 'enctype' => 'multipart/form-data']) !!}

                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">{{ (Lang::has(Session::get('lang_file').'.USER_NEME_LEVEL')!= '')  ?  trans(Session::get('lang_file').'.USER_NEME_LEVEL'): trans($OUR_LANGUAGE.'.USER_NEME_LEVEL')}}</label>
                            {!! Form::text('uname', null, array( 'class'=>'form-control','id'=>'name','maxlength'=>'50','required')) !!}
                            {{--<input type="text" class="form-control" id="name">--}}
                        </div>
                        <div class="form-group">
                            <label for="email">{{ (Lang::has(Session::get('lang_file').'.EMAILADDRRESS')!= '')  ?  trans(Session::get('lang_file').'.EMAILADDRRESS'): trans($OUR_LANGUAGE.'.EMAILADDRRESS')}}</label>
                            {!! Form::email('uemail', null, array('required','id'=>'email', 'class'=>'form-control','maxlength'=>'80','onfocusout'=>'checkalreadyregisteredemail(this.value);')) !!}
                            {{--<input type="email" class="form-control" id="email">--}}
                        </div>
                        <div class="form-group">
                            <label for="email">{{ (Lang::has(Session::get('lang_file').'.USER_MOBILE_LEVEL')!= '')  ?  trans(Session::get('lang_file').'.USER_MOBILE_LEVEL'): trans($OUR_LANGUAGE.'.USER_MOBILE_LEVEL')}}</label>
                            {{--<select name="country_code" id="country_code" class="t-box checkout-small-box countrycode" required>--}}

                                {{--@foreach($getCountry as $Ccode)--}}
                                    {{--<option value="{{ $Ccode->country_code}}">+{{ $Ccode->country_code}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            {!! Form::text('umobile', null, array('required', 'class'=>'form-control','maxlength'=>'13','onkeypress'=>'return isNumberKey(event);')) !!}
                            {{--<input type="email" class="form-control" id="email">--}}
                            <span class="menu-down">+966 <span class="glyphicon glyphicon-menu-down"></span></span>
                        </div>
                        <div class="form-group">
                            <label for="psw">{{ (Lang::has(Session::get('lang_file').'.PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.PASSWORD'): trans($OUR_LANGUAGE.'.PASSWORD')}}</label>
                            {!! Form::password('upassword',  array('required', 'maxlength'=>'20', 'class'=>'form-control','id'=>'psw')) !!}
                            {{--<input type="text" class="form-control" id="psw">--}}
                        </div>
                        <div class="form-group">
                            <label for="psw2">{{ (Lang::has(Session::get('lang_file').'.Confirm_Password')!= '')  ?  trans(Session::get('lang_file').'.Confirm_Password'): trans($OUR_LANGUAGE.'.Confirm_Password')}}</label>
                            {!! Form::password('cpassword',  array('required', 'maxlength'=>'20', 'class'=>'form-control')) !!}
                            {{--<input type="text" class="form-control" id="psw2">--}}
                        </div>
                        {{--<div class="form-group clearfix">--}}
                            {{--<div class="pull-right">--}}
                                {{--<a href="">Forget Password ?</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="checkbox">
                            <label><input type="checkbox" value="">I agree to the <a href="">Terms and Conditions</a></label>
                        </div>
                    {!! Form::submit('Sign Up', array('class'=>'btn btn-default btn-success btn-block')) !!}
                        {{--<button type="submit" class="btn btn-default btn-success btn-block">Sign Up</button>--}}
                    {!! Form::close() !!}
                    <div class="or"><span>OR</span></div>
                    <a href="" class="login-google"><img src="images/google-logo.png" alt="" /> Log in with Google</a>
                    <p>Do you have an account? <a href="">Login</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-home">
        <div class="slide-pages owl-carousel" dir="ltr">
            <div class="item">
                <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
            </div>
            <div class="item">
                <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
            </div>
            <div class="item">
                <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
            </div>
        </div>

        @php
            $currentpage=Route::getCurrentRoute()->uri();
            if(Session::get('searchdata')){
                $issession= count(Session::get('searchdata')) ;
            }else{
                $issession= 0 ;
            }
            if($currentpage == '/'){ $page='home'; }else{ $page='inner';}
            if($issession>0 && $page!='home'){ $emodify='modify-extra';  }else{ $emodify='';}
        @endphp

        @php $setOccasion = Helper::getAllOccasionNameList(2); @endphp

        <form action="{{route('search-results')}}" class="search" name="searchweddingoccasion2" id="searchweddingoccasion2">

            <div class="input-form">
                <select name="cityid" id="cityid">
                    <option>Select your city</option>
                    <option value="">@if (Lang::has(Session::get('lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif
                    </option>
                    @php $getC = Helper::getCountry(); @endphp
                    @foreach($getC as $cbval)
                        <option value="" disabled="" >@if($selected_lang_code !='en') {{$cbval->co_name_ar}} @else  {{$cbval->co_name}}   @endif</option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp
                        @foreach ($getCity as $val)
                            @php

                                if(Session::get('searchdata.cityid')!=''){
                                $user_currentcity   = Session::get('searchdata.cityid');
                                }else {
                                $user_currentcity   = Session::get('user_currentcity');
                                }

                              $cityID = $val->ci_id;


                            @endphp
                            @if($selected_lang_code !='en')
                                @php $ci_name= 'ci_name_ar'; @endphp
                            @else
                                @php $ci_name= 'ci_name'; @endphp
                            @endif
                            <option value="{{ $val->ci_id }}" <?php if($user_currentcity == $cityID){  echo "SELECTED";}  ?>>{{ $val->$ci_name }}</option>
                        @endforeach
                    @endforeach

                </select>
                <img src="{{url('newWebsite')}}/images/detect-loaction-icon.png" alt="" />
            </div>
            <div class="input-form">
                <input type="text" placeholder="Occasion Date" class="search-t-box cal-t" name="occasiondate" id="DA" value="{{ Session::get('searchdata.occasiondate') }}" readonly />
                <img src="{{url('newWebsite')}}/images/date-icon.png" alt="" />
            </div>
            <div class="input-form">
                <select id="weddingandoccasionlist" name="weddingoccasiontype">
                    <option>All Services</option>
                    <option value="">@if (Lang::has(Session::get('lang_file').'.Select')!= '') {{  trans(Session::get('lang_file').'.Select') }} @else  {{ trans($OUR_LANGUAGE.'.Select') }} @endif</option>
                    @php foreach($setOccasion as $woevents){ @endphp
                    @php if(Session::get('searchdata.weddingoccasiontype')==$woevents->id) { $occasionselected='selected'; }else{ $occasionselected='';}
                      if(Session::get('lang_file') !='en_lang')
                     {
                       $otitle_name= 'title_ar';
                     }
                     else
                     {
                        $otitle_name= 'title';
                     }
                    @endphp
                    <option value="{{$woevents->id}}" {{ $occasionselected }}  >{{ $woevents->$otitle_name}} </option>
                    @php } @endphp

                </select>
            </div>
            <div class="input-form">
                <input type="submit" value="Go" />
            </div>
        </form>
    </div>
<footer>
    <div class="container">
        <ul class="link-footer">
            <li>
                <a href="{{url('/')}}">{{ (Lang::has(Session::get('lang_file').'.HOME')!= '')  ?  trans(Session::get('lang_file').'.HOME'): trans($OUR_LANGUAGE.'.HOME')}}</a>
            </li>
            <li>
                <a href="{{url('/about-us')}}">{{ (Lang::has(Session::get('lang_file').'.ABOUTUS')!= '')  ?  trans(Session::get('lang_file').'.ABOUTUS'): trans($OUR_LANGUAGE.'.ABOUTUS')}}</a>
            </li>
            <li><a href="">Certificates and recommendations</a></li>
            <li>
                <a href="{{url('/occasions')}}">#{{ (Lang::has(Session::get('lang_file').'.Occasions')!= '')  ?  trans(Session::get('lang_file').'.Occasions'): trans($OUR_LANGUAGE.'.Occasions')}} </a>
            </li>
            <li>
                <a href="{{url('/privacy-policy')}}">{{ (Lang::has(Session::get('lang_file').'.PrivacyPolicy')!= '')  ?  trans(Session::get('lang_file').'.PrivacyPolicy'): trans($OUR_LANGUAGE.'.PrivacyPolicy')}} </a>
            </li>
            <li>
                <a href="{{url('/return-policy')}}">{{ (Lang::has(Session::get('lang_file').'.ReturnPolicy')!= '')  ?  trans(Session::get('lang_file').'.ReturnPolicy'): trans($OUR_LANGUAGE.'.ReturnPolicy')}} </a>
            </li>
            <li>
                <a href="{{url('/contact-us')}}">{{ (Lang::has(Session::get('lang_file').'.ContactUs')!= '')  ?  trans(Session::get('lang_file').'.ContactUs'): trans($OUR_LANGUAGE.'.ContactUs')}} </a>
            </li>
            <li>
                <a href="{{url('/sitemerchant')}}"
                   target="_blank">{{ (Lang::has(Session::get('lang_file').'.Merchant_Login')!= '')  ?  trans(Session::get('lang_file').'.Merchant_Login'): trans($OUR_LANGUAGE.'.Merchant_Login')}}  </a>
            </li>
        </ul>


        @php

            $getFbLinks        = Helper::getfblinks();

            $gettwitterLinks   = Helper::gettwitterlinks();

            $getgoogleLinks    = Helper::getgooglelinks();

            $getinstagramLinks = Helper::getinstagramlinks();

            $getpinterestLinks = Helper::getpinterestlinks();

            $getyoutubeLinks   = Helper::getyoutubelinks();

            $getphone          =  Helper::getphone();

            $getSettingaddress =  Helper::getSettingaddress();

            $getSettingemail   =  Helper::getSettingemail();

        @endphp


        <div class="row">
            <div class="col-md-3">
                <ul class="list-unstyled">
                    <li><i class="fa fa-map-marker"></i> {{ $getSettingaddress }}</li>
                    <li><i class="fa fa-phone"></i> {{$getphone or ''}}</li>
                    <li><i class="fa fa-envelope"></i> <a
                                href="mailto:{{$getSettingemail or ''}}">{{$getSettingemail or ''}}</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h2>{{ (Lang::has(Session::get('lang_file').'.FollowUS')!= '')  ?  trans(Session::get('lang_file').'.FollowUS'): trans($OUR_LANGUAGE.'.FollowUS')}}</h2>
                <ul class="follow-us">

                    @if($getinstagramLinks !='')

                        <li><a href="{{ $getinstagramLinks }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    @endif

                    @if($getinstagramLinks !='')

                        <li><a href="{{ $getinstagramLinks }}" target="_blank"><i class="fa fa-pinterest-p"></i></a>
                        </li>
                    @endif

                    @if($getyoutubeLinks !='')

                        <li><a href="{{ $getyoutubeLinks }}" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                    @endif


                    @if($getFbLinks !='')
                        <li><a href="{{$getFbLinks}}" target="_blank"><i class="fa fa-facebook"></i></a>
                            @endif
                        </li>
                        @if($getgoogleLinks !='')

                            <li><a href="{{ $getgoogleLinks }}" target="_blank"><i class="fa fa-google-plus"></i></a>
                            </li>

                        @endif

                        @if($gettwitterLinks !='')

                            <li><a href="{{ $gettwitterLinks }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        @endif

                </ul>
            </div>
            <div class="col-md-2">
                <h2>{{ (Lang::has(Session::get('lang_file').'.SecureShopping')!= '')  ?  trans(Session::get('lang_file').'.SecureShopping'): trans($OUR_LANGUAGE.'.SecureShopping')}}</h2>
                <img class="safe" src="{{url('newWebsite')}}/images/norten.png" alt=""/>
            </div>
            <div class="col-md-3">
                <h2>{{ (Lang::has(Session::get('lang_file').'.PaymentOptions')!= '')  ?  trans(Session::get('lang_file').'.PaymentOptions'): trans($OUR_LANGUAGE.'.PaymentOptions')}} </h2>
                <div class="dis-flex">
                    <img src="{{url('newWebsite')}}/images/master.png" alt=""/>
                    <img src="{{url('newWebsite')}}/images/visa.png" alt=""/>
                    <img src="{{url('newWebsite')}}/images/master.png" alt=""/>

                </div>
            </div>
        </div>
        <hr>
        <div class="copyright">
            <p class="text-center">&copy; @php echo date("Y"); @endphp Goldencages.com {{ (Lang::has(Session::get('lang_file').'.AllRightsReserved')!= '')  ?  trans(Session::get('lang_file').'.AllRightsReserved'): trans($OUR_LANGUAGE.'.AllRightsReserved')}}.</p>
        </div>
    </div>
</footer>



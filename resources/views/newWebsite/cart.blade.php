@extends('newWebsite.layouts.master')
@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="pull-left">
                        Cart (4 products)
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <a class="planning" href="">Back to Planning</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="content-site cart">
        <div class="container">

            <h2 class="head-cart" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">{{ (Lang::has(Session::get('lang_file').'.CARTBUFFET')!= '')  ?  trans(Session::get('lang_file').'.CARTBUFFET'): trans($OUR_LANGUAGE.'.CARTBUFFET')}}</h2>
            @php $k=1; $basetotal=0; @endphp
            <?php
            if(count($productinfo)>0)
            {
                $xyz=0;
                foreach($productinfo as $productinfo)
                {
                    $xyz++;
                    if(isset($productinfo[0]->pro_discount_percentage) && $productinfo[0]->pro_discount_percentage>0){ $hallprice=$productinfo[0]->pro_disprice; }else{ $hallprice=$productinfo[0]->pro_price; }
                    $remainingamount= ($hallprice*75)/100;
                    $remainingamount=currency($remainingamount, 'SAR',$Current_Currency, $format = false);
                    ?>
            <div class="p-15 box">
                <div class="row">
                    <div class="col-md-8">
                        <div class="item-cart-01 clearfix">
                            <div class="tump">
                                <img src="images/cart-01.png" alt="" />
                            </div>
                            <div class="cart">
                                <h3>     @php
                                        if($productinfo[0]->Insuranceamount!='')
                                                  { $productInsuranceamount=$productinfo[0]->Insuranceamount; }
                                                  else{ $productInsuranceamount=0;}
                                   $finalprice=$hallprice;
                                                     //$finalprice=currency($hallprice, 'SAR',$Current_Currency, $format = false);

                                                     $advanceprice=($finalprice*25)/100;
                                                     $remainingammount=$finalprice-$advanceprice;

                                    @endphp
                                    @php $advance_price=currency($advanceprice, 'SAR',$Current_Currency, $format = false);
                                  $basetotal= $basetotal + $advance_price;
                                    @endphp
                                    {{ Session::get('currency') }} {{ number_format($advance_price, 2) }}</h3>
                                <p>Hall Name</p>
                                <h2>Services: </h2>
                                @php $pst=0; if(count($productservices)>0){ @endphp

                                <ul class="services-cart">
                                    <li class="service-item"><span>      @php



                                                if(count($productservices)>0){

                                            @endphp
                                            @foreach($productservices as $adopedproductservices)
                                                @foreach($adopedproductservices->getProductOptionValue as $addoptedamount)
                                                    @php $pst=$pst+currency($addoptedamount->price, 'SAR',$Current_Currency, $format = false); @endphp
                                                @endforeach
                                            @endforeach
                                            @php } @endphp


                                            @php  if(count($internal_foods)>0){ @endphp
                                            @foreach($internal_foods as $internalfoodcost)
                                                @php
                                                    $itemdishselcted_price=currency($internalfoodcost['final_price'], 'SAR',$Current_Currency, $format = false);
                                                     $pst=$pst+$itemdishselcted_price;
                                                @endphp
                                            @endforeach
                                            @php } @endphp



                                            {{ Session::get('currency') }} {{ number_format($pst,2) }}
                                        </span><span>1200</span></li>
                                    @php } @endphp
                                    @php
                                        $basetotal=$basetotal+currency($productInsuranceamount, 'SAR',$Current_Currency, $format = false);
                                         if(count($productservices)>0){ @endphp
                                    <li class="service-item {{$productinfo[0]->pro_mc_id}}{{$xyz}}"><span>-service</span><span>1200</span></li>
                                </ul>
                                <h2>Food: </h2>
                                <ul class="list-cart">
                                    <li><span>
                            @php   if(count($productservices)>0){
                            $k=1; $ij=1; $z=0;

                             //echo "<pre>";
                           //print_r($productservices);
                           //die;
                            @endphp
                                            @foreach($productservices as $adopedproductservices)
                                                @foreach($adopedproductservices->getProductOptionValue as $addoptedservices)
                                                    @php
                                                        $ps=currency($addoptedservices->price, 'SAR',$Current_Currency, $format = false);

                                                        $basetotal=$basetotal+$ps;

                                                    @endphp
                                        </span><span>A×2</span><span>
                                            @php $orderservices_name = 'option_title';
                                      if(Session::get('lang_file')!='en_lang')
                                    {
                                        $orderservices_name = 'option_title_ar';
                                      }
                                            @endphp

                                            {{ $addoptedservices->$orderservices_name }}</span></li>
                                    <li><span>Dish</span><span>A×2</span><span>500</span></li>
                                    @php $k++; $ij++; $z++; @endphp
                                    @endforeach
                                    @endforeach
                                    @php }

                             //$basetotal=($basetotal*25)/100;


                            if(count($internal_foods)>0){
                              //dd($internal_foods);
                                    @endphp
                                    @foreach($internal_foods as $internalfood)

                                    <li><span>Dish</span><span>A×2</span><span> {{ $internalfood['container_title'] }} X {{ $internalfood['quantity'] }}</span></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="list-ul-cart">
                            <ul>
                                <li><span class="h">
                                                   @php
                                                       $Qran = rand(111,999);

                                                       $dishID = $internalfood['dish_id'];
                                                       $IntQty = $internalfood['quantity'];
                                                   @endphp
                                    </span> <span>1400 SAR</span></li>
                                <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $dishID }}">

                                <li><span class="h">Insurance : </span><span>1400 SAR</span></li>
                                @php $itemselcted_price=currency($internalfood['final_price'], 'SAR',$Current_Currency, $format = false); @endphp
                                {{ number_format($itemselcted_price, 2) }}</td>
                                </tr>
                                @php $basetotal=$basetotal+$itemselcted_price; @endphp
                                @php $k++; @endphp
                                @endforeach
                                @php }  @endphp
                                <li><span class="h">Paid Services : </span><span>1400 SAR</span></li>
                                @php }  @endphp

                                <li><span class="h">Food : </span><span>1400 SAR</span></li>
                                <li><span class="h">Total Cost : </span><span>1400 SAR</span></li>
                                <li><span class="h">  @php
                                            $totalcast=$finalprice+$productInsuranceamount+$pst;
                                        @endphp
                                        {{ Session::get('currency') }} {{ number_format($totalcast,2) }}
                                    </span><span>1400 SAR</span></li>
                            </ul>
                            <div class="pull-right">
                                <span class="pay">  @php
                                        $totalpayable=$advance_price+$productInsuranceamount+$pst;
                                    @endphp
                                    {{ Session::get('currency') }} {{ number_format($totalpayable,2) }}</span>
                                <a class="remove" href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                @php $k++; } } @endphp

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="head-cart">Dessert</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="dessert-item">
                        <div class="items-reviews">
                            <div class="item">
                                <div class="tump">
                                    <img src="images/cart-02.png" alt="">
                                </div>
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h2>Item Name</h2>
                                            <p>Shop name</p>
                                            <p>Quantniy : 40</p>
                                            <p class="price">1400 SAR</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Enter quantity</p>
                                            <input type="number" class="quantity" />
                                            <a class="remove" href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dessert-item">
                        <div class="items-reviews">
                            <div class="item">
                                <div class="tump">
                                    <img src="images/cart-02.png" alt="">
                                </div>
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h2>Item Name</h2>
                                            <p>Shop name</p>
                                            <p>Quantniy : 40</p>
                                            <p class="price">1400 SAR</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Enter quantity</p>
                                            <input type="number" class="quantity" />
                                            <a class="remove" href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <h2 class="head-cart">Photography</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="dessert-item">
                        <div class="items-reviews">
                            <div class="item">
                                <div class="tump">
                                    <img src="images/cart-02.png" alt="">
                                </div>
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2>Package Name</h2>
                                            <p>Shop name</p>
                                            <p>Date / Time</p>
                                            <p>Location</p>
                                            <p class="price">1400 SAR</p>
                                        </div>
                                        <div class="col-md-4">
                                            <a class="remove" href=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="total">Total: 1500</div>
            </div>
            <div class="clearfix"></div>
            <center>
                <a class="continue" href="">Continue</a>
            </center>
        </div>
    </div>
@endsection
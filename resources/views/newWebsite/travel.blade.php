@extends('newWebsite.layouts.master')
@section('content')
    @php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='') {
        $Current_Currency = 'SAR';
        }
    @endphp
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>{{$vendordetails->mc_name}}</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>{{$vendordetails->mc_name}}</h2>
                                <h2>{{$vendordetails->address}}</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                    $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>{{strip_tags($vendordetails->mc_discription)}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <?php if(isset($vendordetails->google_map_address) && $vendordetails->google_map_address!=''){
                        $lat  = $vendordetails->latitude;
                        $long  = $vendordetails->longitude;

                        ?>
                        <div class="detail_hall_dimention" id="map" style="height: 230px!important"> </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                            @foreach($allreview as $val)
                                @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{$userinfo->cus_pic}}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{$userinfo->cus_name}}</h2>
                                    <div class="stars">
                                        <@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{round($val->ratings)}}.png">@endif
                                    </div>
                                    <p>{{$val->comments}}</p>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        {{$allreview->links()}}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="desserts">
                <div class="box p-15">
                    <div class="row">
                        @foreach($productlist as $getallcats)

                            <?php
                            $img = str_replace('thumb_','',$getallcats->pro_Img);
                            ?>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="{{$getallcats->pro_Img}}" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">{{$getallcats->pro_title}}</a>
                                    <p>{{$getallcats->pro_desc}}</p>
                                    <div class="flex-end">
                                         <?php if(isset($productlist[0]->pro_disprice) && $productlist[0]->pro_disprice>=1){$getPrice = $productlist[0]->pro_disprice;}else{$getPrice = $productlist[0]->pro_price;}?>
                                        <div class="price"><?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->pro_price){echo '<del>  '.currency($productlist[0]->pro_price, 'SAR',$Current_Currency).'</del> <br />';} ?>
                                            <?php if(isset($getPrice) && $getPrice!=''){echo currency($getPrice, 'SAR',$Current_Currency);}?>
                                        </div>
                                            {!! Form::open(['url' => 'addtocarttravelandcarproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
                                           
                                                    <input type="hidden" id="category_id" name="category_id" value="{{ $category_id }}">
        <input type="hidden" id="subcat_id" name="subcat_id" value="{{ $subcat_id }}">
        <input type="hidden" name="itemqty" id="itemqty" value="1" min="1" readonly />
        <input type="hidden" name="actiontype" value="travelagencydetail">
        <input type="hidden" name="cart_sub_type" value="travel">
        <input type="hidden" name="cart_type" value="travel">
        <input type="hidden" id="product_id" name="product_id" value="<?php if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!=''){echo $productlist[0]->pro_id;}?>">
        <input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){ echo currency($getPrice, 'SAR',$Current_Currency, $format = false); }?>">

    <input type="hidden" id="product_orginal_price" name="product_orginal_price" value="<?php if(isset($getPrice) && $getPrice!=''){ echo currency($getPrice, 'SAR',$Current_Currency, $format = false); }?>">

        <input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->pro_mr_id) && $productlist[0]->pro_mr_id!=''){echo $productlist[0]->pro_mr_id;} ?>">

                                      <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '')  ?  trans(Session::get('lang_file').'.BOOK_NOW'): trans($OUR_LANGUAGE.'.BOOK_NOW')}}" class="form-btn addto_cartbtn">
                                      {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach

                    </div>
                </div>

            </div>

            <center>
                {{$productlist->links()}}
                {{--<ul class="pagenation">--}}
                    {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                    {{--<li class="active"><a href="">1</a></li>--}}
                    {{--<li><a href="">2</a></li>--}}
                    {{--<li><a href="">3</a></li>--}}
                    {{--<li><a href="">4</a></li>--}}
                    {{--<li><a href="">5</a></li>--}}
                    {{--<li><a href="">6</a></li>--}}
                    {{--<li><a href="">7</a></li>--}}
                    {{--<li><a href="">8</a></li>--}}
                    {{--<li><a href="">9</a></li>--}}
                    {{--<li><a href="">10</a></li>--}}
                    {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                {{--</ul>--}}
            </center>

        </div>
    </div>
    @endsection
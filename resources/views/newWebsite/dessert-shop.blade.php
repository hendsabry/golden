@extends('newWebsite.layouts.master')
@section('content')
    @php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='')
        {
        $Current_Currency = 'SAR';
        }
       // $cururl = request()->segment(count(request()));
    @endphp
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>Dessert Shop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>{{ $fooddateshopdetails[0]->mc_name }}</h2>
                                <h2>{{ $fooddateshopdetails[0]->address }}</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>{{ $fooddateshopdetails[0]->mc_discription }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        @php if($fooddateshopdetails[0]->latitude!='' && $fooddateshopdetails[0]->longitude!=''){  $lat=$fooddateshopdetails[0]->latitude;   $long=$fooddateshopdetails[0]->longitude;    @endphp


                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        @php }  @endphp
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
                        <div class="items-reviews">
                            @foreach($fooddateshopreview as $customerreview)
                                <div class="item">
                                    <div class="tump">
                                        <img src="{{ $customerreview->cus_pic }}" alt="">
                                    </div>
                                    <div class="caption">
                                        <h2>{{ $customerreview->cus_name }}</h2>
                                        <div class="stars">
                                            <img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png">
                                        </div>
                                        <p>{{ $customerreview->comments }}</p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        {{$fooddateshopreview->links()}}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        <iframe class="service-video" src="{{ $fooddateshopdetails[0]->mc_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

                    </div>
                </div>
            </div>

            <div class="desserts">
                <div class="slide-cats owl-carousel" dir="ltr">
                    @foreach($fooddateshopproducts as $getallcats)

                        <div class="item-cat">
                        <a href=""><img src="{{ $getallcats->pro_Img or '' }}" alt="">
                            <p>{{ $getallcats->pro_title or ''}}</p>
                        </a>
                    </div>
                        @endforeach
                </div>


                <div class="box p-15">
                    <div class="row">
                        {{--@foreach($fooddateshopproducts as $getallcats)--}}



                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="{{ $fooddateshopleftproduct->pro_Img or '' }}" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">{{ $fooddateshopleftproduct->pro_title }}</a>
                                    <p>{{ $fooddateshopleftproduct->pro_desc }}</p>
                                    <div class="flex-end">
                                        @php if($fooddateshopleftproduct->product_price[0]->discount>0){ $netprice=$fooddateshopleftproduct->product_price[0]->discount_price; }else{ $netprice=$fooddateshopleftproduct->product_price[0]->product_option_value_id; } @endphp
                                        <div class="price">SAR {{ $netprice or '0'}}</div>
                                        {{--<a href="">Add To Cart</a>--}}
                                        {!! Form::open(['url' => 'dessertshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
                                           <input type="hidden" id="product_id" name="product_id" value="{{ $fooddateshopleftproduct->pro_id }}">
                                           <input type="hidden" id="21" name="pr21" value="{{ $netprice or '0' }}">
                                          <input type="hidden" id="23" name="pr23" value="{{ $fooddateshopleftproduct->product_price[1]->product_option_value_id or '0'}}">
                                         <input type="hidden" name="shopby" id="shopby" value="">
                                                <input type="hidden" id="cart_type" name="cart_type" value="food">
                                                <input type="hidden" id="attribute_id" name="attribute_id" value="34">
                                                <input type="hidden" id="prodqty" name="prodqty" value="{{ $fooddateshopleftproduct->pro_qty }}">
                                                <input type="hidden" id="shop_id" name="shop_id" value="{{ $shop_id }}">
                                                <input type="hidden" id="category_id" name="category_id" value="{{ $subcategory_id }}">
                                                <input type="hidden" id="subcategory_id" name="subcategory_id" value="{{ $category_id }}">
                                                <input type="hidden" id="branch_id" name="branch_id" value="{{ $branch_id }}">
                                                <span id="error"></span>
                                                <input type="submit" class="form-btn" id="sbtn" value="@if (Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') {{  trans(Session::get('lang_file').'.Add_to_Cart') }} @else  {{ trans($OUR_LANGUAGE.'.Add_to_Cart') }} @endif">

                                        <!-- container-section -->
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--@endforeach--}}
                    </div>
                </div>

            </div>

            <center>
                {{ $fooddateshopproducts->links() }}
            </center>

        </div>
    </div>
    @endsection
@extends('newWebsite.layouts.master')
@section('content')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 

if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 

  @endphp
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>Kosha Design</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</h2>
                                <h2>@if($lang == 'en_lang')  {{$vendordetails->address}} @else   {{$vendordetails->address_ar}} @endif</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>@if($lang != 'en_lang') {{$vendordetails->mc_discription_ar}} @else {{$vendordetails->mc_discription}} @endif</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        @if($vendordetails->google_map_address!='')
                            @php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp
                            <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                            <?php if(count($allreview) > 0){ ?>
                            @foreach($allreview as $val)

                                @php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{$userinfo->cus_pic}}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{$userinfo->cus_name}}</h2>
                                    <div class="stars">
                                        @if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif
                                    </div>
                                    <p>{{$val->comments}}</p>
                                </div>
                            </div>
                            @endforeach
                                <?php } ?>
                        </div>
                        {{--{{$allreview->links()}}--}}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                        @if($vendordetails->mc_video_description!='') <div class="service-video-cont">
                            @if($lang != 'en_lang')  {{$vendordetails->mc_video_description_ar}} @else  {{$vendordetails->mc_video_description}} @endif</div>@endif
                        <div class="service-video-box">
                            @if($vendordetails->mc_video_url !='')    <iframe style="width: 100%" class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li class="active">
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/desykosha.png" alt=""></a></div>
                            <h2><a href="{{ route('kosha-shop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1]) }}" @if(request()->type!=2) class="select" @endif>{{ (Lang::has(Session::get('lang_file').'.Design_Your_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Design_Your_Kosha'): trans($OUR_LANGUAGE.'.Design_Your_Kosha')}} </a></h2>
                        </li>
                        <li>
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/wedding-arch.png" alt=""></a></div>
                            <h2><a  @if(request()->type==2) class="select" @endif href="{{ route('kosha-redymadeshop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2]) }}">{{ (Lang::has(Session::get('lang_file').'.Ready_Made_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Ready_Made_Kosha'): trans($OUR_LANGUAGE.'.Ready_Made_Kosha')}} </a></h2>
                        </li>
                    </ul>
                </center>

                <br>
                <div class="slide-cats owl-carousel" dir="ltr">
                   
                @for($i=0;$i< count($menu[0]->sub_menu); $i++)
                     @php $k = $i+1; @endphp
                    <div class="item-cat" onclick="sela('{{addslashes($menu[0]->sub_menu[$i]['attribute_title'])}}','{{$menu[0]->sub_menu[$i]['id']}}','{{$k}}');">
                        <a href="#o" id="{{$menu[0]->sub_menu[$i]['id']}}"  class="attb @if($k==1) select @endif"><img src="{{$menu[0]->sub_menu[$i]['image']}}" alt="">
                            <p>{{$menu[0]->sub_menu[$i]['attribute_title']}}</p>
                        </a>
                    </div>
                    @endfor
                </div>

                <div class="box kosha p-15">
                    <div class="row">
                        <div class="col-md-4">
                            <h3>
                                {{ (Lang::has(Session::get('lang_file').'.SELECT')!= '')  ?  trans(Session::get('lang_file').'.SELECT'): trans($OUR_LANGUAGE.'.SELECT')}}
                                <span id="displayCats">{{$menu[0]->sub_menu[0]['attribute_title']}}</span>
                            </h3>
                            <div class="row">
                                @for($ks=0;$ks < count($menu[0]->sub_menu); $ks++)
                                @for($i=0;$i< count($menu[0]->sub_menu[0]['product']); $i++)
                                @php if($menu[0]->sub_menu[$ks]['product'][$i]['pro_title']==''){ continue;} $ksp = $ks+1; @endphp
                                <div class="col-md-12">
                                    <div class="item-style clearfix">
                                        <div class="tump">
                                            <img src="{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_Img'] }}" alt="" />
                                        </div>

                                        <div class="caption">
                                            <a href="">{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_title'] }}</a>
                                            <p>{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_desc'] }}</p>
                                            <div class="flex-end">
                                                
                                                <div class="price">
                                                    @if($menu[0]->sub_menu[$ks]['product'][$i]['pro_disprice'] > 1)
                                                    <del class="strike">
                                                      {{  $menu[0]->sub_menu[$ks]['product'][$i]['pro_price']}}
                                                    </del>

                                              {{  $menu[0]->sub_menu[$ks]['product'][$i]['pro_disprice']}}
                                                @else
                                                {{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_price']}}
                                                @endif
                                                 SAR</div>
                                                @if($menu[0]->sub_menu[$ks]['product'][$i]['pro_qty']>0)
                  <div class="kosha-select-radieo">
                  
                    <a name="re{{$ksp}}" class="selct" data-attribute="{{ $menu[0]->sub_menu[$ks]['product'][$i]['attribute_id'] }}" data-from="{{$ksp}}" data-title="{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_title'] }}" data-currency="{{ Session::get('currency') }}"  data-price="{{  currency($menu[0]->sub_menu[$ks]['product'][$i]['pro_disprice'], 'SAR',$Current_Currency, $format = false) }}" data-img="{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_Img'] }}" data-id="{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}" > select
                    </a>
                  </div>
                  @else
                    <span id="sold" class="form-btn addto_cartbtn" >{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')}}</span>
                  @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endfor 
                                @endfor 
                       
                            </div>
                        </div>
                        <div class="col-md-8 kosha-selections-area">
                            <h3>Your Selection</h3>
                            <div class="box p-15">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="item-kosha">
                                            <div class="image">
                                                <img src="{{url('newWebsite')}}/images/img-losha1.png" alt="" />
                                            </div>
                                            <div class="caption">
                                                <h2 class="fw-700">item name 1</h2>
                                                <p class="fw-700">Quantity : 1</p>
                                                <p class="fw-700 price">500 SAR</p>
                                                <a class="remove" href=""><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="item-kosha">
                                            <div class="image">
                                                <img src="{{url('newWebsite')}}/images/img-losha2.png" alt="" />
                                            </div>
                                            <div class="caption">
                                                <h2 class="fw-700">item name 1</h2>
                                                <p class="fw-700">Quantity : 1</p>
                                                <p class="fw-700 price">500 SAR</p>
                                                <a class="remove" href=""><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>

        </div>
    </div>
@endsection
@section('script')
 <script type="text/javascript"> 

/* Remove Items from selection list */
$('body').on('click','.remove', function(){ 
        var title =  $(this).data('rid');
        var unchk =  $(this).data('unchk');
        $('.'+title).html('');
        $( "input[name='"+unchk+"']" ).prop('checked',false);
        var totalP = 0;
        var getP = 0;
        $( ".tprice" ).each(function() {
        getP =  $(this).val();
        totalP = parseFloat(totalP) + parseFloat(getP);
        }); 
        totalP = parseFloat(totalP).toFixed(2);
        $("#totalPrice").html('SAR '+ totalP); 

        if(totalP == 0)
        {
        $(".kosha-selections-area").hide();  
        }
});
/* Remove Items from selection list end */
 

 $('.selct').click(function()
 {
  var title =  $(this).data('title');
  var from =  $(this).data('from');
  var price =  $(this).data('price');  
  var img =   $(this).data('img');
  var pid =   $(this).data('id');
  var pinsurance =   $(this).data('insurance');
  var attribute =   $(this).data('attribute');
  var qty=$('#qty_'+pid).val();

  var currency =  $(this).data('currency'); 
  var totalprice=parseFloat(price) + parseFloat(pinsurance);
  var totalpriceaccordingqty=qty*totalprice;
  var totaldprice=totalpriceaccordingqty.toFixed(2);

  <?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') { $qtytext=trans(Session::get('lang_file').'.QUANTITY');} else { $qtytext=trans($OUR_LANGUAGE.'.QUANTITY');} ?>

  $(".kosha-selections-area").show();  
  $(".info_"+from).html('');
  $(".info_"+from).append('<div class="selection-box"><div class="selection-img"><img src="'+img+'" alt="" /></div><div class="selection-name">'+title+' <small class="small_oty"> <?php echo $qtytext;?>: '+qty+' </small><input type="hidden" class="tprice" name="itemprice[]" value="'+totaldprice+'"><input type="hidden" name="itemqty'+pid+'" value="'+qty+'"><input type="hidden" name="product[attribute_id][]" value="'+attribute+'"><input type="hidden" name="product[pro_id][]" value="'+pid+'"></div><div class="selection-prise">'+currency+' '+totaldprice+'</div><span class="remove" data-unchk="re'+from+'" data-rid="info_'+from+'"><img src="{{ url('') }}/themes/images/delete.png"/></span></div>');
    var totalP = 0;
    var getP = 0;
    $( ".tprice" ).each(function() {
    getP =  $(this).val();
    totalP = parseFloat(totalP) + parseFloat(getP);
    }); 

   totalP = parseFloat(totalP).toFixed(2)

$("#totalPrice").html(currency+' '+totalP); 
  });

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
  $('.flexslider').resize(); // this is it
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
function pricecalculation(act)
{
  var no=1;
  var product_id      = document.getElementById('product_id').value;
  var product_size    = document.getElementById('product_size').value;
  var currentquantity = document.getElementById('qty').value;
  var unititemprice   = document.getElementById('priceId').value;
  if(act=='add')
  {
    var qty = parseInt(currentquantity)+parseInt(no);     
  }
  else
  { 
    if(parseInt(currentquantity)==1)
    {
      var qty=parseInt(currentquantity)
    }
    else
    {
      var qty=parseInt(currentquantity)-parseInt(no);
    }
  }
  
  if(product_size)
  {
   $.ajax({
     type:"GET",
     url:"{{url('getSizeQuantity')}}?product_id="+product_id+'&product_size='+product_size+'&qty='+qty,
     success:function(res)
     {               
    
     if(res!='ok')
     {
      //alert(res);
       alert('Not available quantity');
       var qtyupdated=parseInt(currentquantity);      
       document.getElementById('qty').value=qtyupdated;
     }
     else
     {
        //alert(res);
        //alert('er');
      var producttotal = qty*unititemprice;
      document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
      document.getElementById('itemqty').value=qty;
     }
     }
   });
  }
  else
  {
    var producttotal = qty*unititemprice;
    document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
    document.getElementById('itemqty').value=qty;
  }
      
}






function showProductDetail(str,vendorId)
{
    
     $.ajax({
     type:"GET",
     url:"{{url('getShoppingProductInfo')}}?product_id="+str+'&vendor_id='+vendorId,
     success:function(res)
     {               
    if(res)
    { 
      $('html, body').animate({
        scrollTop: ($('.service-display-left').first().offset().top)
      },500);
      var json = JSON.stringify(res);
      var obj = JSON.parse(json);
          console.log(obj);
      length=obj.productdateshopinfo.length;
      //alert(length);
      document.getElementById('qty').value=1;
      if(length>0)
      {
           for(i=0; i<length; i++)
         {         
              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+obj.productdateshopinfo[i].pro_price+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productdateshopinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');          
               $('#cont_final_price').html('SAR '+obj.productdateshopinfo[i].pro_price);      
         }
      }
      if($.trim(obj.productattrsize) !=1)
      {
        $('#ptattrsize').html(obj.productattrsize);
      $('#ptattrsizeenone').css('display','block');
      }
      else
      {
        $('#ptattrsizeenone').css('display','none');
      }   
      }
     }
   });
}
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script>
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {       
          "product_size" : {
            required : true
          },    
         },
         messages: {
          "product_size": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_YOUR_SIZE') }} @endif'
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
</script>
 
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 @if(request()->type!='') 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 @endif
 <script type="text/javascript">
   function checkquantity(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty_'+prodid).value;
         var insuranceamount  = document.getElementById('insuranceamount_'+prodid).value;
         var netamountforqty  = document.getElementById('netamountforqty_'+prodid).value;
          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }

   $.ajax({
       type:"GET",
       url:"{{url('checkcoshaquantity')}}?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_'+productId).value = qtyupdated - 1;
        
       }
       else
       {
         var totalinsurance=parseFloat(insuranceamount)*parseInt(qty);
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         var grandtotal=parseFloat(totalinsurance)+parseFloat(totalcalcamount);
         jQuery('#grandtotal_'+productId).html('<div class="kosha-tolat-prise" style="margin-top:10px;"><span><?php echo $Total_Price; ?></span> <span><?php echo $Cur; ?></span> <span>'+grandtotal.toFixed(2)+'</span></div>');

         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }
jQuery(document).ready(function(){
jQuery('.slideTwo').css('height', '100%');

});

    

 </script>


<script type="text/javascript">
   function checkquantityforreadymade(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty_'+prodid).value;
         var insuranceamount  = document.getElementById('insuranceamount_'+prodid).value;
         var netamountforqty  = document.getElementById('netamountforqty_'+prodid).value;         


          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }

   $.ajax({
       type:"GET",
       url:"{{url('checkcoshaquantity')}}?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_'+productId).value = qtyupdated - 1;
        
       }
       else
       {
         var totalinsurance=parseFloat(insuranceamount)*parseInt(qty);
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         var grandtotal=parseFloat(totalinsurance)+parseFloat(totalcalcamount);
         jQuery('#grandtotal_'+productId).html('<div class="kosha-tolat-prise" style="margin-top:10px;"><span><?php echo $Total_Price; ?></span> <span><?php echo $Cur; ?></span> <span>'+grandtotal.toFixed(2)+'</span></div>');
         document.getElementById('totalpaidamount_'+productId).value = grandtotal;
         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }
jQuery(document).ready(function(){
jQuery('.slideTwo').css('height', '100%');

});

    

 </script>
@endsection
@extends('newWebsite.layouts.master')
@section('content')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
@endphp
 <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</h2>
                                <h2>{{$vendordetails->address}}</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                	<?php
								        $getcityname = Helper::getcity($vendordetails->city_id); 
								        $mc_name = 'ci_name'; 
								        if(Session::get('lang_file')!='en_lang')
								      {
								          $mc_name = 'ci_name_ar'; 
								        }
								        echo $getcityname->$mc_name; 
								      ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>@if($lang != 'en_lang') {{$vendordetails->mc_discription }} @else {{$vendordetails->mc_discription}} @endif</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                     @if($vendordetails->google_map_address!='') 
					 @php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp
					   <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
					 @endif
					</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                        	@foreach($allreview as $val)
           
                   				@php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{$userinfo->cus_pic}}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{$userinfo->cus_name}}</h2>
                                    <div class="stars">
                                        @if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif
                                    </div>
                                    <p>{{$val->comments}}</p>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                        {{$allreview->links()}}
                        <!-- <ul class="pagenation">
                            <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">7</a></li>
                            <li><a href="">8</a></li>
                            <li><a href="">9</a></li>
                            <li><a href="">10</a></li>
                            <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                    	<iframe width="100%" height="315" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li class="active">
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/roses-04.png" alt=""></a></div>
                            <h2><a href="{{ route('roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1]) }}" @if(request()->type!=2) class="select" @endif>{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}}</a></h2>
                        </li>
                        <li >
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/roses-05.png" alt=""></a></div>
                            <h2><a  @if(request()->type==2) class="select" @endif href="{{ route('single-roses-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2]) }}">{{ (Lang::has(Session::get('lang_file').'.Single')!= '')  ?  trans(Session::get('lang_file').'.Single'): trans($OUR_LANGUAGE.'.Single')}}</a></h2>
                        </li>
                    </ul>
                </center>

                <br>
                <div class="slide-cats2 owl-carousel" dir="ltr">
                	
                	@foreach($catg as $cat)
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{$cat->mc_img}}" alt="">
                            <p>{{$cat->mc_name}}</p>
                        </a>
                    </div>
                    @endforeach
                    
                </div>
@php 
  $getproID = $product[0]->pro_id;  @endphp
  @php



            if($product[0]->pro_qty>0)
            {
                   if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') 
                   {
                           $cartbtn=trans(Session::get('lang_file').'.Add_to_Cart');  } else{  $cartbtn=trans($OUR_LANGUAGE.'.Add_to_Cart'); 
                    }
                      $btnst='';
                      $avcart='';
                    $avcart='block';
              }
               else
              {

                if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''){ $cartbtn=trans(Session::get('lang_file').'.SOLD_OUT'); }else{ $cartbtn=trans($OUR_LANGUAGE.'.SOLD_OUT');}
                $btnst='disabled="disabled"';
                $avcart='none';
              }

$OriginalP = $product[0]->pro_price; 
$OriginalDis = $product[0]->pro_discount_percentage;
$Disp = $OriginalP - ($OriginalP*$OriginalDis)/100;
$Disp = number_format((float)$Disp, 2, '.', ''); 
@endphp 

                <div class="box kosha p-15 koha-ready">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="fw-700 h4">{{$product[0]->pro_title}}</p>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                            	@php if($OriginalDis!=''){ @endphp
                                <p class="price"><span class="old">{{ currency($product[0]->pro_price,'SAR',$Current_Currency) }}</span> @php } @endphp<span class="new"> {{ currency($Disp,'SAR',$Current_Currency) }} </span></p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="image-ready">
                                <img src="{{$product[0]->pro_Img}}" alt="Image 1" />
                            </div>
                            <h3>Description</h3>
                            <div class="box p-15">
                                <p>{{ $product[0]->pro_desc }}</p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <h3>Details</h3>
                            <p class="fw-700">@if(Lang::has(Session::get('lang_file').'.Number_of_Flowers')!= '') {{ trans(Session::get('lang_file').'.Number_of_Flowers')}}  @else {{ trans($OUR_LANGUAGE.'.Number_of_Flowers')}} @endif <span style="color: #000"></span>{{ count($product[0]->product_packege)}}</p>
                            <br>
                            <h3>@if(Lang::has(Session::get('lang_file').'.Flower_Type')!= '') {{ trans(Session::get('lang_file').'.Flower_Type')}}  @else {{ trans($OUR_LANGUAGE.'.Flower_Type')}} @endif</h3>
                            <div class="row">
                            	@for($i=0;$i< count($product[0]->product_packege);$i++)
	                                <div class="col-md-4">
	                                    <div class="item-type">
	                                        <img src="{{ $product[0]->product_packege[$i]->getProductPackage->pro_Img }}" alt="" />
	                                        <p class="fw-700">{{ $product[0]->product_packege[$i]->getProductPackage->pro_title }}</p>
	                                    </div>
	                                </div>
                                  @endfor 
                              
                            </div>
                            <br>
                            <div class="row">
                            	   @if(count($product[0]->product_option) >=1) 
									@for($i=0;$i<=1;$i++)
                                <div class="col-md-6">
                                    <h3>{{$product[0]->product_option[$i]->option_title}}</h3>
                                 
                                    <div class="item-type">
                                        <img src="{{ $product[0]->product_option[$i]->product_option_value[0]->image }}" alt="" />
                                        <p class="fw-700">{{ $product[0]->product_option[$i]->product_option_value[0]->option_title }}</p>
                                    </div>
                                      
                                </div>
                                @endfor       
  									 @endif 
                                
                            </div>
                            <br>
                            <form name="form1" method="post" action="{{ route('roses-package-add-to-cart') }}"" enctype="multipart/form-data">
      						{{ csrf_field() }}
                            <p class="fw-700">@if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY')}}  @else {{ trans($OUR_LANGUAGE.'.QUANTITY')}} @endif : <input class="quantity" name="itemqty" id="qty" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantity('add','{{ $product[0]->pro_id }}');" onkeydown="isNumberKey(event); checkquantity('add','{{ $product[0]->pro_id }}');" type="number" /></p>
                            <br>
                            <p class="price fw-700"><span style="color: #000">@if(Lang::has(Session::get('lang_file').'.Total_Price')!= '') {{ trans(Session::get('lang_file').'.Total_Price')}}  @else {{ trans($OUR_LANGUAGE.'.Total_Price')}} @endif:</span> {{ currency($Disp,'SAR',$Current_Currency) }}</p>
                             <input type="hidden" name="netamountforqty_{{ $product[0]->pro_id }}" id="netamountforqty_{{ $product[0]->pro_id }}" value="{{ currency($Disp,'SAR',$Current_Currency, $format = false) }}">

      <input type="hidden" name="product_id" id="total_product_id" value="{{$product[0]->pro_id}}">
      <input type="hidden" name="cart_type" value="occasion">
      <input type="hidden" name="total_price" id="total_total_price" value="{{  currency($Disp, 'SAR',$Current_Currency, $format = false) }}">
      <input type="hidden" name="language_type" value="en">
      <input type="hidden" name="product_qty" value="1">
      <input type="hidden" name="product_option_value[]">
      <input type="hidden" name="attribute_id" value="{{request()->id}}">
      <input type="hidden" name="cart_sub_type" value="roses">
                            <div class="form-group">
                                <div class="pull-right">
                                    <a href="">Terms and conditions</a>
                                    <input type="submit" value="{{ $cartbtn }}" id="sbtn" class="form-btn addto_cartbtn" {{ $btnst }}>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>

            </div>

 <!--            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center> -->

        </div>
    </div>
@endsection
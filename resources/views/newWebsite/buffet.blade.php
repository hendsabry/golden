@extends('newWebsite.layouts.master')
@section('content')
    @php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='')
        {
        $Current_Currency = 'SAR';
        }
       // $cururl = request()->segment(count(request()));
    @endphp
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>Buffet Shop</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            <hr>
        </div>
    </div>
    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>{{ $fooddateshopdetails[0]->mc_name }}</h2>
                                <h2>{{ $fooddateshopdetails[0]->address }}</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>{{ $fooddateshopdetails[0]->mc_discription }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        @php if($fooddateshopdetails[0]->latitude!='' && $fooddateshopdetails[0]->longitude!=''){  $lat=$fooddateshopdetails[0]->latitude;   $long=$fooddateshopdetails[0]->longitude;    @endphp


                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        @php }  @endphp
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
                        <div class="items-reviews">
                            @foreach($fooddateshopreview as $customerreview)
                            <div class="item">
                                <div class="tump">
                                    <img src="{{ $customerreview->cus_pic }}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{ $customerreview->cus_name }}</h2>
                                    <div class="stars">
                                        <img src="{{url('/')}}/themes/images/star{{ round($customerreview->ratings) }}.png">
                                    </div>
                                    <p>{{ $customerreview->comments }}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        {{$fooddateshopreview->links()}}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box"><iframe width="100%" height="315" src="{{ $fooddateshopdetails[0]->mc_video_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>
        </div>
    </div>

    <div class="menue">
        <div class="container">
            <div class="slide-cats owl-carousel" dir="ltr">
                @foreach($mainmenuwithItemAndContainer as $category)
                <div class="item-cat">
                    <a href=""><img src="{{$category->image}}" alt="">
                        <p>{{$category->attribute_title}}</p>
                    </a>
                </div>
                @endforeach
            </div>
            {!! Form::open(['url' => 'foodshopbuffetmenu/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}

            <div class="row">

                @php $j=1; $l=0; @endphp
                @foreach($mainmenuwithItemAndContainer as $mainmenucatgeoryloop)
                    @php $isaddable=''; @endphp
                    @php if($j==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp
                    <div class="col-md-5">
                    <h3>Dishes</h3>
                    <div class="dishes">
                        @php $z=1;  @endphp

                    @foreach($mainmenuwithItemAndContainer[$l]['food_list'] as $menudish)

                        <div class="item-dis clearfix">
                            <div class="tump">
                                <img src="{{ $menudish->pro_Img}}" alt="" />
                                <i class="fa fa-eye"></i>
                            </div>
                            <div class="caption">
                                <p class="fw-700">{{ $menudish->pro_title}} </p>
                                <div class="disflex">


                                    @php $jk=1; @endphp
                                    @foreach($menudish->container_list as $dishcontainer)
                                        @php if(count($dishcontainer->container_price)>0){ $isaddable=1;
				if($jk==1) { $sclass='';  }else{ $sclass=''; }
				$containerid=$dishcontainer->container_price[0]->id;
				$containername=$dishcontainer->container_price[0]->short_name;
        if($dishcontainer->discount_price==0 || $dishcontainer->discount_price==null){ $containernewprice= $dishcontainer->price; }else{ $containernewprice= $dishcontainer->discount_price; }
				$newcontainerprice=number_format($containernewprice,2,'.','');
				 $newcontainerprice=currency($newcontainerprice, 'SAR',$Current_Currency, $format = false);

         $containerrealprice=number_format($dishcontainer->price,2,'.','');
         $realcontainerprice=currency($containerrealprice, 'SAR',$Current_Currency, $format = false);
                                        @endphp

                                        <a href="#" id="{{ $jk }}{{ $menudish->pro_id }}" onClick=" return selectcontainer('basecontainerprice{{ $z}}{{ $menudish->pro_id }}','{{ $menudish->pro_id }}','{{ $containerid }}','{{ $newcontainerprice }}','{{ $jk }}','{{ $containername }}','{{ $realcontainerprice }}','{{ Session::get('currency') }}');" class="containerlisting {{ $sclass }}">{{ $containername }} </a>

                                        @php $jk++; }else{ $isaddable=0; } @endphp

                                    @endforeach

                                </div>
                                <div class="disflex">
                                    <div class="left">
                                        <div class="">
                                            <p>Quantity</p>
                                            <input type="number" name="itemqty[]" id="qty{{ $menudish->pro_id }}" value="1" min="1" max="9" onKeyUp="isNumberKey(event); pricecalculation('{{ $menudish->pro_id }}','basecontainerprice{{ $z}}{{ $menudish->pro_id }}','pricewithqty');"/>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <p class="sal">{{ Session::get('currency') }}

                                            <span id="basecontainerprice{{ $z }}{{ $menudish->pro_id }}">0.00</span><br>
                                            <span id="containerrealdisprice{{ $menudish->pro_id }}"></span>


                                        </p>

                                        <input type="hidden" name="containerid" id="containerid{{ $menudish->pro_id }}" value="">
                                        <input type="hidden" name="itemid" id="itemid{{ $menudish->pro_id }}" value="">

                                        <input type="hidden" name="mainmenuid[]" id="menuid{{ $menudish->pro_id }}" value="{{ $mainmenucatgeoryloop-> id }}">
                                        <input type="hidden" name="menuname[]" id="menuname{{ $menudish->pro_id }}" value="{{ $mainmenucatgeoryloop->attribute_title }}">

                                        <!----------------- Menu Dish Info--------------->
                                        <input type="hidden" name="dishid[]" id="dishid{{ $menudish->pro_id }}" value="{{ $menudish->pro_id }}">
                                        <input type="hidden" name="dishname[]" id="dishname{{ $menudish->pro_id }}" value="{{ $menudish->pro_title}}">
                                        <!-------------- container info ------------------------>


                                        <input type="hidden" name="selectedcontainerid[]" id="selectedcontainerid{{ $menudish->pro_id }}" value="">
                                        <input type="hidden" name="selectedcontainername[]" id="selectedcontainername{{ $menudish->pro_id }}" value="">
                                        <input type="hidden" name="selectedsinglecontainername[]" id="selectedsinglecontainername{{ $menudish->pro_id }}" value="">

                                        <input type="hidden" name="selectedcontainerprice[]" id="selectedcontainerprice{{ $menudish->pro_id }}" value="0.00">
                                        <input type="hidden" name="dishcontainerbaseprice[]" id="dishcontainerbaseprice{{ $menudish->pro_id }}" value="0.00">

                                        <input type="hidden" name="addedmainmenuid[]" id="addedmainmenuid{{ $menudish->pro_id }}" value="">
                                        <input type="hidden" name="addeddishid[]" id="addeddishid{{ $menudish->pro_id }}" value="">
                                        <input type="hidden" name="addeddishcontainerid[]" id="addeddishcontainerid{{ $menudish->pro_id }}" value="">
                                        <input type="hidden" name="addeddishcontainerqty[]" id="addeddishcontainerqty{{ $menudish->pro_id }}" value="">



                                        @php if($isaddable==1){ @endphp
                                        <a href="#" onClick="return addtocart('{{ $menudish->pro_id }}');">{{ (Lang::has(Session::get('lang_file').'.ADD')!= '')  ?  trans(Session::get('lang_file').'.ADD'): trans($OUR_LANGUAGE.'.ADD')}}</a>
                                        @php }else{ @endphp

                                        @php } @endphp


                                        @php $z++; $basecontainerprice='0.00'; @endphp

                                    </div>
                                </div>
                            </div>
                        </div>

                            @php $j++; $l++; @endphp

                        @endforeach

                    </div>
                </div>
                <div class="col-md-3">
                    <h3>Container</h3>
                    <div class="box p-15">
                        @foreach($barnchcontainerlist as $allcontainerlist)
                            @php if($allcontainerlist->no_person < 1){ continue;} @endphp
                        <div class="item-container">
                            <div class="img"><img src="{{ $allcontainerlist->image }}" alt="" /></div>
                            <div>{{ (Lang::has(Session::get('lang_file').'.NO_OF_PEOPLE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_PEOPLE'): trans($OUR_LANGUAGE.'.NO_OF_PEOPLE')}} {{ $allcontainerlist->no_person }}</div>
                            <div>{{ $allcontainerlist->short_name }}</div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <h3>Your Selection</h3>
                    <div class="selection box p-15">
                        <ul>
                            <li>Dish name</li>
                            <li>container</li>
                            <li>Quantity</li>
                            <li>Price</li>
                        </ul>
                        <div class="list-select" id="myContainer">



                        </div>

                        <div class="total_food_cost">
                            @php $basetotal="0.00"; @endphp
                            <div class="total_price">{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: <span>{{ Session::get('currency') }} <span id="pricetotalamount">{{ number_format($basetotal,2,'.','') }}</span></span></div>
                        </div>

                        <div class="btn_row">
                            <input type="hidden" name="totalprice" id="totalprice" value="{{ $basetotal }}">
                            <input type="hidden" name="cartintemid[]" id="cartintemid" value="0">
                            <input type="hidden" name="foodqty[]" id="foodqty" value="0">
                            <input type="hidden" name="cart_type" id="cart_type" value="food">
                            <input type="hidden" name="product_id" id="product_id" value="{{ $productid }}">
                            <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" class="form-btn addto_cartbtn"></div>

                    </div>
                </div>

                 @endforeach
            </div>
            {!! Form::close() !!}

        </div>
    </div>
    @endsection


@section('script')
    <script language="javascript">
        var totalprice = 0;
        var dishidarr=[0];
        //dishidarr.clear();
        var itemquantity=[];
        var seldishid=[];
        function addtocart(menudishid){
            var totalamount=0;
            $('#order_summary_bar').css('display','');
            var menuid=document.getElementById('menuid'+menudishid).value;
            var menuname=document.getElementById('menuname'+menudishid).value;
            var dishid=document.getElementById('dishid'+menudishid).value;
            var dishname=document.getElementById('dishname'+menudishid).value;
            var selectedcontainerid=document.getElementById('selectedcontainerid'+menudishid).value;
            var selectedcontainername=document.getElementById('selectedcontainername'+menudishid).value;
            var selectedsinglecontainername=document.getElementById('selectedsinglecontainername'+menudishid).value;

            var containersectedid = selectedcontainerid.split(",");

            var c= parseInt(containersectedid.length) - 1;
            var cid=containersectedid[c];
            $('#rc'+cid+''+menudishid).css('display','block');

            var itemqty=document.getElementById('qty'+menudishid).value;
            itemquantity.push(itemqty);

            var seleldishid=document.getElementById('dishid'+menudishid).value;
            seldishid.push(seleldishid);


            var cid=document.getElementById('containerid'+menudishid).value;
            var itid=document.getElementById('itemid'+menudishid).value;

            //var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
            var selectedcontainerprices=document.getElementById('dishcontainerbaseprice'+menudishid).value;
            var selectedcontainerprice= itemqty * selectedcontainerprices;



            var dishcontainerbaseprice=document.getElementById('dishcontainerbaseprice'+menudishid).value;
            var totalprice=document.getElementById('totalprice').value;

            var totalamount=parseInt(totalprice)+parseInt(selectedcontainerprice);
            document.getElementById('totalprice').value=totalamount.toFixed(2);
            var isalreadyadded=dishidarr.includes(dishid);
            /*if(isalreadyadded===false)
                    {*/
            dishidarr.push(dishid);
            let ndisharr=dishidarr;
            dishidarr.toString();

            document.getElementById('cartintemid').value=dishidarr;

            //alert(totalamount);
            // var dtotalamount=totalamount;
            //document.getElementById('pricetotalamount').innerHTML = dtotalamount;
            if(selectedcontainerprice==0.00)
            {
                document.getElementById('error'+dishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CONTAINER_FIRST') }} @endif';
                document.getElementById('qty'+dishid).value = 1;
            }

            else

            {
                var div = document.createElement('div');
                jQuery('.'+ menudishid + '_'+selectedsinglecontainername).remove();
                div.className = 'order_sumrow'+ ' '+ menudishid + '_'+selectedsinglecontainername;



                    <?php $Curr = Session::get('currency');?>

                var newslprice=selectedcontainerprice;
                div.innerHTML =
                    '<div class="item-select flex-end"><p class="order_sum_title">'+dishname+'<input type="hidden" name="menuprice" class="disp" value="'+newslprice+'"><input type="hidden" name="menudishid[]" class="disp7" value="'+dishid+'"><input type="hidden" name="newcontainerid[]" class="disp7" value="'+cid+'"><input type="hidden" name="newdishitemid[]" class="disp7" value="'+itid+'"><input type="hidden" name="menudishqty[]" class="disp7" value="'+itemqty+'"><input type="hidden" name="containerid[]" class="proidkk" value="'+selectedcontainerid+'"></p><div class="order_subcol2"> '+selectedsinglecontainername+' x '+itemqty+'</div><div class="order_subcol2 buffet-right"><?php echo $Curr; ?> '+newslprice+'</div><input  type="button" value="X" onclick="removeRow(this,'+menudishid+','+ndisharr+')"></div>';


                document.getElementById('myContainer').appendChild(div);
                document.getElementById('addedmainmenuid'+menudishid).value=menuid;
                document.getElementById('addeddishid'+menudishid).value=menudishid;

                var menuval = jQuery('#addeddishcontainerid'+menudishid).val();

                //if (menuval.indexOf("1210") >= 0){ alert(); }

                document.getElementById('addeddishcontainerid'+menudishid).value=selectedcontainerid;



                document.getElementById('addeddishcontainerqty'+menudishid).value=itemquantity;

                document.getElementById('dishid'+menudishid).value=seldishid;









            }

            /*}else{
                        document.getElementById('alreadyadded'+menudishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.THIS_ITEM_ALREADY_ADDED_IN_CART')!= '') {{  trans(Session::get('lang_file').'.THIS_ITEM_ALREADY_ADDED_IN_CART') }} @else  {{ trans($MER_OUR_LANGUAGE.'.THIS_ITEM_ALREADY_ADDED_IN_CART') }} @endif';
			}*/






            var totalP = 0;
            jQuery( ".disp" ).each(function() {
                getP =  $(this).val();
                totalP = parseFloat(totalP) + parseFloat(getP);
            });
            totalP = parseFloat(totalP).toFixed(2);
            jQuery('#pricetotalamount').html(totalP);

        }


        function removeRow(input,menudishid,disharr) {


            var index = dishidarr.indexOf(menudishid);

            if (index > -1) {
                array.splice(index, 1);
            }
            document.getElementById('myContainer').removeChild(input.parentNode);
            var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
            var totalprice=document.getElementById('totalprice').value;
            var totalamount=parseInt(totalprice)- parseInt(selectedcontainerprice);
            document.getElementById('totalprice').value=totalamount;
            document.getElementById('pricetotalamount').innerHTML = totalamount.toFixed(2);

            var selectedcontainerid=document.getElementById('selectedcontainerid'+menudishid).value;
            var containersectedid = selectedcontainerid.split(",");

            var c= parseInt(containersectedid.length) - 1;
            var cid=containersectedid[c];
            $('#rc'+cid+''+menudishid).css('display','none');

            var totalP = 0;
            jQuery( ".disp" ).each(function() {
                getP =  jQuery(this).val();
                totalP = parseFloat(totalP) + parseFloat(getP);
            });
            totalP = parseFloat(totalP).toFixed(2);
            jQuery('#pricetotalamount').html(totalP);

            var mhjhj='';
            jQuery( ".proidkk" ).each(function() {
                getPv =  jQuery(this).val();
                mhjhj = getPv+','+mhjhj;
            });




            jQuery('#addeddishcontainerid49').val(mhjhj);




        }

    </script>




    <script language="javascript">

        $('.add').click(function () {
            if ($(this).prev().val() < 99) {
                $(this).prev().val(+$(this).prev().val() + 1);
            }
        });
        $('.sub').click(function () {
            if ($(this).next().val() > 1) {
                if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
            }
        });

    </script>



    <script language="javascript">
        function pricecalculation(dishid,k,act){
            //alert(dishid);
            var selectedcontainerprice=document.getElementById('dishcontainerbaseprice'+dishid).value;
            if(selectedcontainerprice==0.00)
            {
                document.getElementById('error'+dishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CONTAINER_FIRST') }} @endif';
                document.getElementById('qty'+dishid).value = 1;
            }

            else

            {

                var did='qty'+dishid;
                var no=1;
                var currentquantity=document.getElementById(did).value;


                if(currentquantity<1){
                    document.getElementById(did).value=1;
                    var qty= parseInt(no);
                }else{
                    if(act=='pricewithqty'){
                        var qty=parseInt(currentquantity)
                    }
                    if(act=='add'){
                        var qty= parseInt(currentquantity)+parseInt(no);
                    }
                    if(act=='remove'){
                        if(parseInt(currentquantity)==1){
                            var qty=parseInt(currentquantity)
                        }else{
                            var qty=parseInt(currentquantity)-parseInt(no);
                        }

                    }
                }



                var finalprice=qty*selectedcontainerprice;

                var inputboxcontanerid='selectedcontainerprice'+dishid;
                document.getElementById(k).innerHTML = finalprice.toFixed(2);
                document.getElementById(inputboxcontanerid).value = finalprice;
            }
        }

    </script>

    <script language="javascript">
        var cid= [];
        var cname=[];
        var cprice=[];
        function selectcontainer(k,dishid,containerid,containerprice,z,containername,realcontainerbaseprice,currs)
        {

            cid.push(containerid);
            cname.push(containername);
            cprice.push(containerprice);

            jQuery('.containerlisting').removeClass('listing_active');

            var classid=''+z+dishid;
            var inputboxcontanerid='selectedcontainerprice'+dishid;
            var dishcontainerbaseprice='dishcontainerbaseprice'+dishid;
            var selectedcontainername='selectedcontainername'+dishid;
            var selectedcontainerid='selectedcontainerid'+dishid;
            var selectedsinglecontainername='selectedsinglecontainername'+dishid;
            var containerrealdisprice='containerrealdisprice'+dishid;
            var containerrealselctedid='containerid'+dishid;
            var selcteditemid='itemid'+dishid;


            var i;
            for (i = 1; i < z; i++) {
                var rid=''+i+dishid;
                document.getElementById(rid).classList.remove("listing_active");
            }
            document.getElementById(selcteditemid).value = dishid;
            document.getElementById(selectedcontainername).value = cname;
            document.getElementById(selectedcontainerid).value = cid;

            document.getElementById(containerrealselctedid).value = containerid;

            document.getElementById(selectedsinglecontainername).value = containername;

            document.getElementById(classid).classList.add("listing_active");
            if(containerprice!=realcontainerbaseprice){
                var displayprice='<del>'+currs+' '+realcontainerbaseprice+'</del><br>'+currs+' '+containerprice;
            }else{
                var displayprice=currs+' '+containerprice;
            }
            document.getElementById(containerrealdisprice).innerHTML = displayprice;
            document.getElementById(k).innerHTML = containerprice;
            document.getElementById(inputboxcontanerid).value = cprice;
            document.getElementById(dishcontainerbaseprice).value = containerprice;
            document.getElementById('error'+dishid).innerHTML='';
            document.getElementById('qty'+dishid).value = 1;

        }

    </script>

    <script>

        // bind change event to select
        jQuery('#dynamic_select').on('change', function () {

            var url = jQuery(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });

    </script>
    <script language="javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
                return false;
            } else {
                return true;
            }
        }
    </script>


@endsection
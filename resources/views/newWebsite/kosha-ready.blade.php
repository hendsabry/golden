@extends('newWebsite.layouts.master')
@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWebsite')}}/images/shop-logo.png" alt="" />
                        <h2>Kosha Ready</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</h2>
                                <h2>@if($lang == 'en_lang')  {{$vendordetails->address}} @else   {{$vendordetails->address_ar}} @endif</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>@if($lang != 'en_lang') {{$vendordetails->mc_discription_ar}} @else {{$vendordetails->mc_discription}} @endif</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        @if($vendordetails->google_map_address!='')
                            @php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp
                            <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                            <?php if(count($allreview) > 0){ ?>
                            @foreach($allreview as $val)

                                @php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{$userinfo->cus_pic}}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{$userinfo->cus_name}}</h2>
                                    <div class="stars">
                                        @if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif
                                    </div>
                                    <p>{{$val->comments}}</p>
                                </div>
                            </div>
                                @endforeach
                                <?php } ?>

                        </div>
                        {{--{{$allreview->links()}}--}}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">

                        @if($vendordetails->mc_video_description!='') <div class="service-video-cont">
                            @if($lang != 'en_lang')  {{$vendordetails->mc_video_description_ar}} @else  {{$vendordetails->mc_video_description}} @endif</div>@endif
                        <div class="service-video-box">
                            @if($vendordetails->mc_video_url !='')    <iframe style="width: 100%" class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="desserts">
                <center>
                    <ul class="link">
                        <li>
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/desykosha-1.png" alt=""></a></div>
                            <h2><a href="{{ route('kosha-shop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1]) }}" @if(request()->type!=2) class="select" @endif>{{ (Lang::has(Session::get('lang_file').'.Design_Your_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Design_Your_Kosha'): trans($OUR_LANGUAGE.'.Design_Your_Kosha')}} </a></h2>
                        </li>
                        <li class="active">
                            <div class="image"><a href=""><img src="{{url('newWebsite')}}/images/wedding-arch-1.png" alt=""></a></div>
                            <h2><a  @if(request()->type==2) class="select" @endif href="{{ route('kosha-redymadeshop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2]) }}">{{ (Lang::has(Session::get('lang_file').'.Ready_Made_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Ready_Made_Kosha'): trans($OUR_LANGUAGE.'.Ready_Made_Kosha')}} </a></h2>
                        </li>
                    </ul>
                </center>

                <br>
                <div class="slide-cats2 owl-carousel" dir="ltr">
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat title-in-img">
                        <a href=""><img src="{{url('newWebsite')}}/images/img-losha1.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                </div>

                <div class="box kosha p-15 koha-ready">
                    <div id="carousel" dir="ltr">
                        <img src="{{url('newWebsite')}}/images/img-lg1.png" alt="Image 1" />
                        <img src="{{url('newWebsite')}}/images/img-lg2.png" alt="Image 1" />
                        <img src="{{url('newWebsite')}}/images/img-lg3.png" alt="Image 1" />
                        <img src="{{url('newWebsite')}}/images/img-lg4.png" alt="Image 1" />
                        <img src="{{url('newWebsite')}}/images/img-lg1.png" alt="Image 1" />
                        <img src="{{url('newWebsite')}}/images/img-lg2.png" alt="Image 1" />
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Description</h3>
                            <div class="box p-15">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.... More</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <ul class="list-unstyled">
                                <li>Insurance: <span class="price">1500 SAR</span></li>
                                <li>Quantity : <input type="number" /></li>
                                <li>TOTAL PRICE :</li>
                            </ul>
                            <div class="pull-right">
                                <a href="">Terms and conditions</a>
                                <a href="" class="addtocart">Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>

        </div>
    </div>
@endsection
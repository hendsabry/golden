@extends('newWebsite.layouts.master')
@section('content')
    @php
        global $Current_Currency;
        $Current_Currency  = Session::get('currency');
        if($Current_Currency =='')
        {
        $Current_Currency = 'SAR';
        }
    @endphp
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <img src="{{url('newWesite')}}/images/shop-logo.png" alt="" />
                        <h2>{{$vendordetails->mc_name}}</h2>
                    </div>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

    <div class="slide-pages owl-carousel" dir="ltr">
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
        <div class="item">
            <img src="{{url('newWebsite')}}/images/img-slider.png" alt="" />
        </div>
    </div>

    <div class="content-site desserts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15">
                        <div class="head-box">
                            <div class="left">
                                <h2>{{$vendordetails->mc_name}}</h2>
                                <h2>{{$vendordetails->address}}</h2>
                                <div class="stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <br>
                            </div>
                            <div class="right">
                                <p><i class="fa fa-map-marker"></i>
                                    <?php
                                    $getcityname = Helper::getcity($vendordetails->city_id);
                                    $mc_name = 'ci_name';
                                    if(Session::get('lang_file')!='en_lang')
                                    {
                                        $mc_name = 'ci_name_ar';
                                    }
                                    echo $getcityname->$mc_name;
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="body-box">
                            <p>{{$vendordetails->mc_discription}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box">
                        @php if($vendordetails->latitude!='' && $vendordetails->longitude!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp


                        <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
                        </div>
                        @php }  @endphp
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box p-15 desserts">
                        <div class="title-head">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
                        <div class="items-reviews">
                            @foreach($allreview as $val)
                                @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                            <div class="item">
                                <div class="tump">
                                    <img src="{{$userinfo->cus_pic}}" alt="">
                                </div>
                                <div class="caption">
                                    <h2>{{$userinfo->cus_name}}</h2>
                                    <div class="stars">
                                        @if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif
                                    </div>
                                    <p>{{$val->comments}}</p>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        {{$allreview->links()}}
                        {{--<ul class="pagenation">--}}
                            {{--<li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>--}}
                            {{--<li class="active"><a href="">1</a></li>--}}
                            {{--<li><a href="">2</a></li>--}}
                            {{--<li><a href="">3</a></li>--}}
                            {{--<li><a href="">4</a></li>--}}
                            {{--<li><a href="">5</a></li>--}}
                            {{--<li><a href="">6</a></li>--}}
                            {{--<li><a href="">7</a></li>--}}
                            {{--<li><a href="">8</a></li>--}}
                            {{--<li><a href="">9</a></li>--}}
                            {{--<li><a href="">10</a></li>--}}
                            {{--<li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-15 box"><iframe width="100%" height="315" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
                </div>
            </div>

            <div class="desserts">
                <div class="slide-cats owl-carousel" dir="ltr">
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                    <div class="item-cat">
                        <a href=""><img src="images/Layer-3.png" alt="">
                            <p>Category</p>
                        </a>
                    </div>
                </div>


                <div class="box p-15">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="item-style clearfix">
                                <div class="tump">
                                    <img src="images/dish1.png" alt="" />
                                </div>
                                <div class="caption">
                                    <a href="">Hall 1</a>
                                    <p>Lorem Ipsum is simply dummy
                                        text of the printing and .....</p>
                                    <div class="flex-end">
                                        <div class="price">2500 SAR</div>
                                        <a href="">Add To Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>

        </div>
    </div>
    @endsection
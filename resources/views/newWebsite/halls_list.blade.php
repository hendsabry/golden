@extends('newWebsite.layouts.master')
@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="logo-title">
                        <h2>Halls</h2>
                    </div>
                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>
    </div>

    <div class="halls-list">
        <div class="container">
            <center>
                <ul class="list-inline cats">
                    <li class="active"><a href=""><img src="images/Hotel-icon.png" alt="" /></a></li>
                    <li><a href=""><img src="images/larg-halls.png" alt="" /></a></li>
                    <li><a href=""><img src="images/small-halls.png" alt="" /></a></li>
                </ul>
                <a class="btn-site-02" href="">Edit Your Search</a>
            </center>
            <div class="row">
                <div class="col-md-6">
                    <h2>Hotel Halls</h2>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <ul class="list-inline options">
                            <li><a class="btn-site-02" href="">Offers</a></li>
                            <li><a class="btn-site-02" href="">within budget</a></li>
                            <li><a class="btn-site-02" href="">above budget</a></li>
                            <li><a class="exchange" href=""><i class="fa fa-exchange fa-rotate-90" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item clearfix">
                        <div class="tump">
                            <img src="images/image-1.png" alt="" />
                        </div>
                        <div class="caption">
                            <a href="">Hall 1</a>
                            <div class="stars">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                            <p>200 Person</p>
                            <div class="price">2500 SAR</div>
                        </div>
                    </div>
                </div>
            </div>
            <center>
                <ul class="pagenation">
                    <li><a href="" class="prev"><i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li><a href="" class="next"><i class="fa fa-angle-left"></i></a></li>
                </ul>
            </center>
        </div>
    </div>
@endsection
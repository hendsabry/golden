@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap occ">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.Attended_List')!= '')  ?  trans(Session::get('lang_file').'.Attended_List'): trans($OUR_LANGUAGE.'.Attended_List')}}</h1>
      
     
      <div class="field_group top_spacing_margin_occas">
	    @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="main_user">
		 @if(count($attenddetails) > 0)
          <div class="myaccount-table">
            <div class="mytr">
              <div class="mytable_heading"></div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.Attended_Name')!= '')  ?  trans(Session::get('lang_file').'.Attended_Name'): trans($OUR_LANGUAGE.'.Attended_Name')}}</div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.Attended_Email')!= '')  ?  trans(Session::get('lang_file').'.Attended_Email'): trans($OUR_LANGUAGE.'.Attended_Email')}}</div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.PHONE_NUMBER')!= '')  ?  trans(Session::get('lang_file').'.PHONE_NUMBER'): trans($OUR_LANGUAGE.'.PHONE_NUMBER')}}</div>

              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}</div>
            </div>
            @php $i = 1; @endphp
            @if(count($attenddetails) >0)
            @foreach($attenddetails as $val)           
            <div class="mytr">
              <div class="mytd " data-title="Sl. No.">{{$i}}</div>
              <div class="mytd " data-title="Name">
			 {{ $val->invite_name }}
            </div>
              <div class="mytd " data-title="Email">  {{ $val->invite_email }} </div>
             <div class="mytd " data-title="Phone">  {{ $val->invite_phone }} </div>
             <div class="mytd " data-title="Invitees"> 
              @php if($val->status==1){ if(Lang::has(Session::get('lang_file').'.Attended_Status')!= ''){ $st=trans(Session::get('lang_file').'.Attended_Status');}else{ $st=trans($OUR_LANGUAGE.'.Attended_Status');} } else{ if(Lang::has(Session::get('lang_file').'.NotAttended_Status')!= ''){ $st=trans(Session::get('lang_file').'.NotAttended_Status');}else{ $st=trans($OUR_LANGUAGE.'.NotAttended_Status');}} @endphp

              {{ $st }} </div>
            </div>
            @php $i++; @endphp 
            @endforeach
            @else
            <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
            @endif
         </div>
		 @else
		 <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
		 @endif
        </div>        
      </div>
      
    </div>
    {{ $attenddetails->links() }}
    <!-- page-right-section -->
  </div>

  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
<script type="text/javascript">
    /*function SearchData(str)
    {
     $.ajax({
     type: 'get',
     data: 'search_key='+str,
     url: '<?php echo url('my-account-ocassion-withajax'); ?>',
     success: function(responseText){  
      alert(responseText);
      $('#email_id_error_msg').html(responseText);  
      if(responseText!=''){
        $("#email_id").css('border', '1px solid red'); 
        $("#email_id").focus();
      }
      else
        $("#email_id").css('border', '1px solid #ccc');  
     }    
     });  
   }*/
</script>
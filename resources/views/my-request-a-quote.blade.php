@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')}}</h1>
      
      {!! Form::open(array('url'=>"my-request-a-quote",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'filter')) !!}
      <div class="dash_select">
        <div class="search-box-field">
          <select name="search_key" required id="search_key" onChange="document.filter.submit();">
		    <option value="">{{ (Lang::has(Session::get('lang_file').'.SELECT_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.SELECT_QUOTE'): trans($OUR_LANGUAGE.'.SELECT_QUOTE')}}</option>
            <option value="singer"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='singer'){echo 'Selected="Selected"';}?>>{{ (Lang::has(Session::get('lang_file').'.SINGER')!= '')  ?  trans(Session::get('lang_file').'.SINGER'): trans($OUR_LANGUAGE.'.SINGER')}}</option>
            <option value="band"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='band'){echo 'Selected="Selected"';}?>>{{ (Lang::has(Session::get('lang_file').'.POPULAR_BAND')!= '')  ?  trans(Session::get('lang_file').'.POPULAR_BAND'): trans($OUR_LANGUAGE.'.POPULAR_BAND')}}</option>
			<option value="recording"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='recording'){echo 'Selected="Selected"';}?>>{{ (Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')}}</option>
          </select>
        </div>
      </div>
      {!! Form::close() !!}

      <div class="field_group top_spacing_margin_occas">
	  @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="main_user">
		 @if(count($getallinquiry) >0)
          <div class="myaccount-table">
            <div class="mytr">
              <div class="mytable_heading"></div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.QUOTE_FOR')!= '')  ?  trans(Session::get('lang_file').'.QUOTE_FOR'): trans($OUR_LANGUAGE.'.QUOTE_FOR')}}</div>

              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.HallOccasion')!= '')  ?  trans(Session::get('lang_file').'.HallOccasion'): trans($OUR_LANGUAGE.'.HallOccasion')}}</div>

              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.REQUEST_DATE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_DATE'): trans($OUR_LANGUAGE.'.REQUEST_DATE')}}</div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.OccasionDateDuration')!= '')  ?  trans(Session::get('lang_file').'.OccasionDateDuration'): trans($OUR_LANGUAGE.'.OccasionDateDuration')}}</div>
              <div class="mytable_heading order_id ">{{ (Lang::has(Session::get('lang_file').'.REPLY_BY')!= '')  ?  trans(Session::get('lang_file').'.REPLY_BY'): trans($OUR_LANGUAGE.'.REPLY_BY')}}</div>
            </div>
            @php $i = 1; @endphp
            @if(count($getallinquiry) >0)
            @foreach($getallinquiry as $val)

            <?php
                $cityname=Helper::getcity($val->city_id);
            ?>
            <div class="mytr">
              <div class="mytd " data-title="Sl. No.">{{$i}}</div>

              <div class="mytd " data-title="Occasions Name">
{{ (Lang::has(Session::get('lang_file').".$val->quote_for")!= '')  ?  trans(Session::get('lang_file').".$val->quote_for"): trans($OUR_LANGUAGE.".$val->quote_for")}} - {{ $val->singer_name }} <br> {{ $val->occasion_type }} 
 </div>
              <div class="mytd " data-title="Hall Occasion">{{ $val->hall or '' }} <br> {{ $val->location }} - {{ $cityname->ci_name or ''}}</div>
              <div class="mytd " data-title="Date">{{date("d M Y", strtotime($val->created_at)) }}</div>
              <div class="mytd " data-title="Occasion Date">{{date("d M Y", strtotime($val->date)) }} {{ $val->time or '' }} <br> {{ $val->duration }} Hrs</div>

              <div class="mytd order_id" data-title="Order ID"><?php if($val->status==1){?>{{ (Lang::has(Session::get('lang_file').'.Process')!= '')  ?  trans(Session::get('lang_file').'.Process'): trans($OUR_LANGUAGE.'.Process')}}<?php }elseif($val->status==2){?> <a href="{{ route('requestaquoteview',[$val->id]) }}">{{ (Lang::has(Session::get('lang_file').'.REPLY_BY_MERCHANT')!= '')  ?  trans(Session::get('lang_file').'.REPLY_BY_MERCHANT'): trans($OUR_LANGUAGE.'.REPLY_BY_MERCHANT')}}</a><?php }elseif($val->status==3){ ?><a href="{{ route('requestaquoteview',[$val->id]) }}">{{ (Lang::has(Session::get('lang_file').'.CONFIRMED')!= '')  ?  trans(Session::get('lang_file').'.CONFIRMED'): trans($OUR_LANGUAGE.'.CONFIRMED')}}</a><?php }elseif($val->status==4){ ?>
                {{ (Lang::has(Session::get('lang_file').'.DENYBYCUSTOMER')!= '')  ?  trans(Session::get('lang_file').'.DENYBYCUSTOMER'): trans($OUR_LANGUAGE.'.DENYBYCUSTOMER')}}
              <?php }else{ ?>{{ (Lang::has(Session::get('lang_file').'.DENY')!= '')  ?  trans(Session::get('lang_file').'.DENY'): trans($OUR_LANGUAGE.'.DENY')}}<?php } ?></div>
            </div>
            @php $i++; @endphp 
            @endforeach
            @else
            <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
            @endif
         </div>
		 @else
		 <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
		 @endif
        </div> 		       
      </div>
	 <div class="paging_quote">  {{ $getallinquiry->links() }}</div>
    </div>    
    <!-- page-right-section -->
  </div>

  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
<script type="text/javascript">
    /*function SearchData(str)
    {
     $.ajax({
     type: 'get',
     data: 'search_key='+str,
     url: '<?php echo url('my-account-ocassion-withajax'); ?>',
     success: function(responseText){  
      alert(responseText);
      $('#email_id_error_msg').html(responseText);  
      if(responseText!=''){
        $("#email_id").css('border', '1px solid red'); 
        $("#email_id").focus();
      }
      else
        $("#email_id").css('border', '1px solid #ccc');  
     }    
     });  
   }*/
</script>
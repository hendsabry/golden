@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')    
    <!-- Display Message after submition -->
    
    <!-- Display Message after submition -->
    <form name="form" id="change_password" method="post" action="{{ route('change-password-update') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}</h1>
      <div class="field_group top_spacing_margin_occas myprofile_page">  
	  @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif      
        <div class="checkout-box">
          <div class="checkout-form">
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.OLD_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.OLD_PASSWORD'): trans($OUR_LANGUAGE.'.OLD_PASSWORD')}}</div>
                <div class="checkout-form-bottom">
                  <input type="password" maxlength="100" name="old_password" id="old_password" value="{!! Input::old('old_password') !!}" class="t-box" />
                </div>
                @if(Session::has('error'))<span class="error">{{ Session::get('error') }}</span>@endif
                @if($errors->has('old_password'))<span class="error">{{ $errors->first('old_password') }}</span>@endif
				<span id="psserrror"></span>
              </div>
            </div>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.NEW_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.NEW_PASSWORD'): trans($OUR_LANGUAGE.'.NEW_PASSWORD')}}</div>
                <div class="checkout-form-bottom">
                  <input type="password" maxlength="150" name="new_password" id="new_password" value="{!! Input::old('new_password') !!}" class="t-box" />
                </div>
                @if($errors->has('new_password'))<span class="error">{{ $errors->first('new_password') }}</span>@endif
              </div>
            </div>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.CONFIRM_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.CONFIRM_PASSWORD'): trans($OUR_LANGUAGE.'.CONFIRM_PASSWORD')}}</div>
                <div class="checkout-form-bottom">
                  <input type="password" maxlength="150" name="confirm_password" id="confirm_password" value="{!! Input::old('confirm_password') !!}" class="t-box" />
                </div>
                @if($errors->has('confirm_password'))<span class="error">{{ $errors->first('confirm_password') }}</span>@endif
              </div>
            </div>
              <!-- checkout-form-cell -->
            </div>
			<div class="myprf_btn">
        <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}" class="form-btn btn-info-wisitech">
      </div>
            <!-- checkout-form-row -->
          </div>
		  
          <!-- checkout-form -->
        </div>      
		
        <!-- checkout-box -->
      </div>    
      
      </form>      
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
<script>
jQuery(document).ready(function(){
jQuery.validator.addMethod("laxEmail", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
}, "Please enter valid email address."); 
 jQuery("#change_password").validate({
    rules: {          
          "old_password" : {
            required : true
          },  
          "new_password" : {
            required : true,
            minlength: 8,
          },
          "confirm_password" : {
            required : true,
            equalTo: "#new_password"
          },              
         },
         messages: {
          "old_password": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_OLD_PASSWORD') }} @endif"
          },
          "new_password": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD') }} @endif",
            minlength:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN') }} @endif"
          },
          "confirm_password": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD') }} @endif",
            equalTo:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PASSWORD_MATCH') }} @endif"
          },          
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#change_password").valid()) {        
    jQuery('#change_password').submit();
   }
  });
  
   jQuery("#old_password").blur(function(){
   var str = jQuery('#old_password').val();
        jQuery.ajax({
		   type: 'get',
		   data: 'password='+str,
		   url: '<?php echo url('change-pass-imagewithajax'); ?>',
		   success: function(responseText)
		   {
			jQuery('#psserrror').html(responseText);
			//location.reload();
		  }       
		});
   });
});
</script>
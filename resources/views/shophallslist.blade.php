@include('includes.navbar')
<div class="outer_wrapper">
@include('includes.header')

@php $hallsizetype=$id; @endphp
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
 @php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ @endphp
      @include('includes.searchweddingandoccasions')
      @php } @endphp
      @php if(Session::get('searchdata.mainselectedvalue')=='1'){ @endphp
      @include('includes.search')
      @php } @endphp
</div> <!-- search-section -->

<div class="page-left-right-wrapper">
@include('includes.mobile-modify')

<div class="page-right-section">
<div class="budget-menu-outer">
<div class="budget-menu">

 

<div class="budget-menu-box"><input type="checkbox" checked="" id="withinbudget" value="withinbudget"/> <label for="withinbudget">{{ (Lang::has(Session::get('lang_file').'.WITHIN_BUDGET')!= '')  ?  trans(Session::get('lang_file').'.WITHIN_BUDGET'): trans($OUR_LANGUAGE.'.WITHIN_BUDGET')}}</label></div>

<div class="budget-menu-box"><input type="checkbox"  id="abovebudget" value="abovebudget"/> <label for="abovebudget">{{ (Lang::has(Session::get('lang_file').'.ABOVE_BUDGET')!= '')  ?  trans(Session::get('lang_file').'.ABOVE_BUDGET'): trans($OUR_LANGUAGE.'.ABOVE_BUDGET')}}</label></div>

<div class="budget-menu-box"><input type="checkbox"  id="offers" value="offers"/> <label for="offers">{{ (Lang::has(Session::get('lang_file').'.OFFERS')!= '')  ?  trans(Session::get('lang_file').'.OFFERS'): trans($OUR_LANGUAGE.'.OFFERS')}}</label></div>

</div>
</div>

<div class="budget-carousel-area">
<div style="clear:both"></div>
<div class="form_title">

	 @if($hallsizetype == 4) 
@if (Lang::has(Session::get('lang_file').'.HOTEL_HALLS')!= '') {{  trans(Session::get('lang_file').'.HOTEL_HALLS') }} @else  {{ trans($OUR_LANGUAGE.'.HOTEL_HALLS') }} @endif
 @endif

  @if($hallsizetype == 5) 
@if (Lang::has(Session::get('lang_file').'.Large_Halls')!= '') {{  trans(Session::get('lang_file').'.Large_Halls') }} @else  {{ trans($OUR_LANGUAGE.'.Large_Halls') }} @endif
 @endif

 @if($hallsizetype == 6) 
@if (Lang::has(Session::get('lang_file').'.Small_Halls')!= '') {{  trans(Session::get('lang_file').'.Small_Halls') }} @else  {{ trans($OUR_LANGUAGE.'.Small_Halls') }} @endif
 @endif




</div>
<div style="clear:both"></div>
<div class="diamond-area" id="darea">
<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
	  <!------------ 1st to 5th-------------->
		<div class="diamond_wrapper_main">
			
			@php $i=1; @endphp
			@php $k=count($getbranchhalls); @endphp
			@php if($k<6){ @endphp
			<div class="diamond_wrapper_inner">
			@foreach($getbranchhalls as $getallcats)
				<div class="row_{{$i}}of{{$k}} rows{{$k}}row">
					@php if($k==3){ $cl='category_wrapper3'; }else{ $cl=''; } @endphp
					<a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$getallcats->pro_id}}">
						<div class="category_wrapper {{ $cl }}" style="background:url({{ $getallcats->pro_img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
				</div>
				 @php $i=$i+1; @endphp
				@endforeach
				</div> 
				<!------------ 6th-------------->
				@php }elseif($k==6){ @endphp
				@php $j=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($getbranchhalls as $getallcats)
			 @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
			 @php if($j==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($j==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($j==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($j==5){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($j==6){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					<a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$getallcats->pro_id}}">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($j==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($j==4){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==5){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($j==6){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $j=$j+1; @endphp
				@endforeach
				</div>
				<!------------ 7th-------------->
				@php }elseif($k==7){ @endphp
				@php $l=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($getbranchhalls as $getallcats)
				@php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
			
			 @php if($l==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($l==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($l==7){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					<a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$getallcats->pro_id}}">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==6){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!------------ 8th-------------->
				@php }elseif($k==8){ @endphp
				@php $l=1; @endphp
			
					<div class="diamond_wrapper_inner">
					@foreach($getbranchhalls as $getallcats)
				@php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
				@php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
				@php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
				
			 @php if($l==1){ $classrd='category_wrapper1'; @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_3of5 rows5row"> @php } @endphp 
			 @php if($l==4){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_3of5 rows5row"> @php } @endphp
			  @php if($l==8){ $classrd='category_wrapper9'; @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					<a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$getallcats->pro_id}}">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==8){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!---------- 9th ------------------->
		@php }elseif($k==9){ @endphp
		
		
					<div class="diamond_wrapper_inner">
	  			@php $i=1; @endphp
		  @foreach($getbranchhalls as $getallcats)
		  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
		  

		 @php if($i==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		  @php if($i==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
		   @php if($i==4){ @endphp <div class="row_3of5 rows5row"> @php } @endphp 
		    @php if($i==7){ @endphp  <div class="row_4of5 rows5row"> @php } @endphp 
			@php if($i==9){ @endphp  <div class="row_5of5 rows5row"> @php } @endphp 
            <a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$getallcats->pro_id}}">
              <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_img or '' }});">
                <span class="category_title"><span class="category_title_inner">{{ $getallcats->pro_title or ''}}</span></span>
              </span>
            </a>
		 @php if($i==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($i==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($i==6){ @endphp <div class="clear"></div> </div> @php } @endphp 
		    @php if($i==8){ @endphp <div class="clear"></div></div> @php } @endphp 
			@php if($i==9){ @endphp  <div class="clear"></div> </div> @php } @endphp 
		 
		    @php $i=$i+1; @endphp
		   @endforeach 
	  
		  
        </div>
		
		
		
				
			@php } @endphp
				 	
				
		</div>
	  </div>
  </div>
 
<div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
 </div>
<div id="flxslider"></div>


</div> <!--  budget-carousel-area -->

</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->
 
</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')
  
 
   
<script language="javascript">
jQuery("#abovebudget,#offers,#withinbudget").click(function(){ 	
	jQuery("#darea").hide();
	var w=document.getElementById('withinbudget').checked;
	var a=document.getElementById('abovebudget').checked;
	var o=document.getElementById('offers').checked;

	if(w == true){ var showData_One =1; }else {var showData_One =0; }
	if(a == true){ var showData_Two =1; }else {var showData_Two =0; }
	if(o == true){ var showData_three =1; }else {var showData_three =0; }		

	if((showData_One==0 || showData_One==1) && showData_Two==0 && showData_three==0 )
	{
	jQuery("#darea").show();
	jQuery("#flxslider").html('');

	}

		var hid  = <?php echo request()->hid;?>;
		var id  = <?php echo request()->id;?>;
		var sid  = <?php echo request()->sid;?>;
		var bid  = <?php echo request()->bid;?>; 
		jQuery.ajax({url: "{{url('/')}}/checkflxslider/"+hid+"/"+id+"/"+sid+"/"+bid+"/"+showData_One+"/"+showData_Two+"/"+showData_three, success: function(result){
			if(showData_Two==0 && showData_three==0 )
			{
			jQuery("#flxslider").html('');
			}
			else
			{
			jQuery("#flxslider").html(result);		
			}	

		jQuery('.flexslider').flexslider({		 
		animation: "slide",
		animationLoop: true,
		animationSpeed: 400,          
		itemWidth: 200,
		itemMargin: 15,
		slideshow: true,
		start: function(slider){
		//console.log('flexlider started');
		}
		});

		}});
	});
 
</script>

<?php if(isset($_REQUEST['isabove']) && $_REQUEST['isabove'] !=''){?>

<script>
jQuery(window).load(function(){


jQuery('#abovebudget').trigger('click');		
 
});

 
</script>

<?php } ?>
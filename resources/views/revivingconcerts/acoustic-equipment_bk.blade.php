@include('includes.navbar')
<div class="outer_wrapper">
  <div class="inner_wrap">
    <div class="vendor_header">
      <div class="inner_wrap">
        <div class="vendor_header_left">
          <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$singerDetails->image}}" alt="" /></a></div>
        </div>
        <!-- vendor_header_left -->
        <div class="vendor_header_right">
          <div class="vendor_welc">
            <div class="vendor_name">{{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}} <span>
              <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
              </span></div>
            <a href="#" class="vendor_cart"><img src="{{ url('') }}/themes/images/basket.png" /><span>2</span></a> </div>
          <!--<div class="select_catg">
            <div class="select_lbl">Other Branches</div>
            <div class="search-box-field">
              <select class="select_drp">
                <option>select</option>
              </select>
            </div>
          </div>-->
        </div>
        <!-- vendor_header_right -->
      </div>
    </div>
    <!-- vemdor_header -->
    <div class="common_navbar">
      <div class="inner_wrap">
        <div id="menu_header" class="content">
          <ul>
            <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
            <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
            <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
            <li><a href="#choose_package">{{ (Lang::has(Session::get('lang_file').'.Choose_Package')!= '')  ?  trans(Session::get('lang_file').'.Choose_Package'): trans($OUR_LANGUAGE.'.Choose_Package')}}</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- common_navbar -->
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?>
                <li><img src="{{str_replace('thumb_','',$singerDetails->image)}}" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">
            <?php
		    $name = 'name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $name = 'name_ar'; 
		    }
		    echo $singerDetails->$name;
		  ?>
          </div>
          <div class="detail_hall_description">
            <?php
		    $address = 'address'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $address = 'address_ar'; 
		    }
		    echo $singerDetails->$address;
		  ?>
          </div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_ACOUSTIC')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_ACOUSTIC'): trans($OUR_LANGUAGE.'.ABOUT_ACOUSTIC')}}</div>
          <div class="detail_about_hall">
            <div class="comment more"> @php
              @endphp
              @php $about='about'@endphp
              @if(Session::get('lang_file')!='en_lang')
              @php $about= 'about_ar'; @endphp
              @endif
              @php echo $singerDetails->$about; @endphp </div>
          </div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CTIY')}}: <span>
            <?php
		    $getcityname = Helper::getcity($singerDetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?>
            </span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
        <div class="service-video-area">
          <div class="service-video-cont">
            <?php 
    		    $mc_name = 'video_description'; 
    		    if(Session::get('lang_file')!='en_lang')
    			{
    		      $mc_name = 'video_description_ar'; 
    		    }
    		    echo $singerDetails->$mc_name; 
    		  ?>
          </div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{$singerDetails->video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <!-- service-video-area -->
        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  @foreach($allreview as $val)
                  @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } /*else{ ?>
		<div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
		<?php } */?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="{{ route('acousticrecording',[$id,$sid,$lid]) }}">{{ (Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')}}</a></li>
                <li><a href="{{ route('acousticequipment',[$id,$sid,$lid]) }}" class="select">{{ (Lang::has(Session::get('lang_file').'.EQUIPMENT')!= '')  ?  trans(Session::get('lang_file').'.EQUIPMENT'): trans($OUR_LANGUAGE.'.EQUIPMENT')}}</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> @php  $i=1; @endphp
                @php  $k=count($getAllProduct);  @endphp
                @php if($k<6){ @endphp
                <div class="diamond_wrapper_inner"> @foreach($getAllProduct as $getallcats)
                  <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a href="#" onclick="return getdatedish('{{ $getallcats->pro_id }}');">
                    <div class="category_wrapper" style="background:url({{ $getallcats->pro_Img or '' }});">
                      <div class="category_title">
                        <div class="category_title_inner">
                          <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                        </div>
                      </div>
                    </div>
                    </a> </div>
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                <!------------ 6th-------------->
                @php }elseif($k==6){ @endphp
                @php $j=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($getAllProduct as $getallcats)
                  @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                  @php if($j==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($j==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($j==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($j==6){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getdatedish('{{ $getallcats->pro_id }}');">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                                </div>
                              </div>
                            </div>
                            </a> @php if($j==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==4){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($j==5){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($j==6){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $j=$j+1; @endphp
                  @endforeach </div>
                <!------------ 7th-------------->
                @php }elseif($k==7){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($getAllProduct as $getallcats)
                  @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                  
                  @php if($l==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($l==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($l==7){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getdatedish('{{ $getallcats->pro_id }}');">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==7){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!------------ 8th-------------->
                @php }elseif($k==8){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($getAllProduct as $getallcats)
                  @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                  @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                  @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                  
                  @php if($l==1){ $classrd='category_wrapper1'; @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_3of5 rows5row"> @php } @endphp 
                      @php if($l==4){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp
                          @php if($l==8){ $classrd='category_wrapper9'; @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getdatedish('{{ $getallcats->pro_id }}');">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==8){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!---------- 9th ------------------->
                @php }elseif($k==9){ @endphp
                <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                  @foreach($getAllProduct as $getallcats)
                  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
                  
                  
                  @php if($i==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($i==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($i==4){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($i==7){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp 
                          @php if($i==9){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getdatedish('{{ $getallcats->pro_id }}');"> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});"> <span class="category_title"><span class="category_title_inner">
                            <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getallcats->$mc_name; ?>
                            </span></span> </span> </a> @php if($i==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    @php if($i==9){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp 
                  
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                @php } @endphp </div>
            </div>
          </div>
          <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
        </div>
        <?php //echo '<pre>';print_r($getAllProduct[0]); ?>
        <!-- service-display-right -->
        {!! Form::open(['url' => 'addcartsingerproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
        <div class="service-display-left" > <span id="selectedproduct">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="{{ $getAllProduct[0]->pro_Img }}" alt="" /></div>
          <div class="service-product-name">
            <?php $mc_name='pro_title';if(Session::get('lang_file')!='en_lang'){$mc_name= 'pro_title_ar';}echo $getAllProduct[0]->$mc_name; ?>
          </div>
          <div class="service-product-description equipment">
            <p>
              <?php $mc_desc='pro_desc';if(Session::get('lang_file')!='en_lang'){$mc_desc= 'pro_desc_ar';}echo $getAllProduct[0]->$mc_desc; ?>
            </p>
            <p><em><strong>{{ (Lang::has(Session::get('lang_file').'.NOTES')!= '')  ?  trans(Session::get('lang_file').'.NOTES'): trans($OUR_LANGUAGE.'.NOTES')}}:</strong>
              <?php $mc_notes='notes';if(Session::get('lang_file')!='en_lang'){$mc_notes= 'notes_ar';}echo $getAllProduct[0]->$mc_notes; ?>
              </em></p>
          </div>
          </span>
			  
		  <div class="equipment-redio-line">
		    <div class="service-radio-box">  
			<span id="buysect">
			<div class="equipment-redio-box">
			  <input type="radio" id="buy" name="buy_rent" value="{{$getAllProduct[0]->pro_price}}" onclick="selectedBuyRent(this.value);pricecalculation('remove');"/>
			  <label for="buy">{{ (Lang::has(Session::get('lang_file').'.BUY')!= '')  ?  trans(Session::get('lang_file').'.BUY'): trans($OUR_LANGUAGE.'.BUY')}}</label>
			</div>
			</span>
			</div>
			<?php $rentPrice = Helper::getProductByAttributeValue($getAllProduct[0]->pro_id); //echo $rentPrice[0]->price; ?>
			<div class="service-radio-box">  
			<span id="rentsect">
			<div class="equipment-redio-box">
			  <input type="radio" id="rent" name="buy_rent" value="<?php echo number_format($rentPrice[0]->price,2); ?>" onclick="selectedBuyRent(this.value);pricecalculation('remove');"/>
			  <label for="rent">{{ (Lang::has(Session::get('lang_file').'.RENT')!= '')  ?  trans(Session::get('lang_file').'.RENT'): trans($OUR_LANGUAGE.'.RENT')}}</label>
			</div>
			</span>
			</div>
			
		  </div>
		  
		  <div class="service-radio-line">
		  	<div class="service_quantity_box" id="service_quantity_box">
    	<div class="service_qunt">Quantity</div>
        <div class="service_qunatity_row">
        <div class="td td2 quantity food-quantity" data-title="Total Quality">
        <div class="quantity">
		<button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
		<input type="number" name="itemqty" id="qty" value="1" min="1" max="9" readonly />
		<button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button></div>
        	<span class="service_qunt_price" id="addtocartprice" style="display: none;">SAR <span class="rentamt" id="rentamt"></span></span>
       </div>
	    </div>
    </div>
		  </div>
			  
			<!-- 
          <span id="addtocartprice" style="display: none;">
             <div class="equipment-prise">S</div>
          </span>
		  --> 

          <input type="hidden" id="category_id" name="category_id" value="{{ $id }}">
          <input type="hidden" id="subcategory_id" name="subcategory_id" value="{{ $sid }}">
          <input type="hidden" id="vendor_id" name="vendor_id" value="{{ $lid }}">
		  <input type="hidden" name="itemqty" id="qty" value="1" min="1" max="9" readonly />
		  <input type="hidden" id="attribute_id" name="attribute_id" value="0">
          <span id="selectedproductprices"> 
		  <input type="hidden" id="product_id" name="product_id" value="{{ $getAllProduct[0]->pro_id }}">
          <input type="hidden" id="shop_id" name="shop_id" value="{{ $sid }}">
		  <input type="text" id="priceId" name="price" value="{{ $getAllProduct[0]->pro_price }}">
		  </span>
		  <span id="addtocart" style="display: none;">
            <input type="submit" class="form-btn big-btn" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" />
          </span> 
		  <span id="addtobook" style="display: none;">
            <input type="submit" class="form-btn big-btn" name="book" value="Book Now" />
          </span>
		  </div>
        {!! Form::close() !!}
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
      <div class="sticky_other_service" style="display:none">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="{{ url('') }}/themes/images/logo.png"></div>
      </div>
    </div>
    <!-- checkout-form-cell -->
  </div>
  <!-- checkout-form-row -->
</div>
</div>
</div>
</div>
<div class="other_serviceinc">
  <div class="other_servrow">
    <div class="serv_title">Other Services</div>
    <a href="javascript:void(0);" class="serv_delete">X</a> </div>
  <div class="customer-budget-section">
    <div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
    <div class="content mCustomScrollbar">
      <div class="customer-budget-box">
        <div class="customer-product-name">Halls</div>
        <div class="customer-budget-line">
          <div class="customer-subproduct-name budgetopen">Hotel Halls</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Wedding Halls</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Wedding Halls (Small)</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="budget-box-divder">&nbsp;</div>
      </div>
      <!-- customer-budget-box -->
      <div class="customer-budget-box">
        <div class="customer-product-name">The Food</div>
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Buffet</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Desert</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Dates</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="budget-box-divder">&nbsp;</div>
      </div>
      <!-- customer-budget-box -->
      <div class="customer-budget-box">
        <div class="customer-product-name">Occasion Co-ordinator</div>
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Photography Studio</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Reception and Hospitality</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Electronic Invitations</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Cosha</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Roses</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Special Events</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="budget-box-divder">&nbsp;</div>
      </div>
      <!-- customer-budget-box -->
      <div class="customer-budget-box">
        <div class="customer-product-name">Car Rentals</div>
        <div class="customer-budget-line">
          <div class="customer-subproduct-name">Car Rentals</div>
          <div class="budget-range-line">
            <div class="budget-range-box1">
              <input type="text" class="budget-t" />
            </div>
            <div class="budget-range-divder">&nbsp;</div>
            <div class="budget-range-box2">
              <input type="text" class="budget-t" />
            </div>
          </div>
        </div>
        <!-- customer-budget-line -->
        <div class="budget-box-divder">&nbsp;</div>
      </div>
      <!-- customer-budget-box -->
    </div>
  </div>
</div>
<!-- other_serviceinc -->
</div>
</div>
<!-- detail_page -->
</div>
<!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
@include('includes.footer')
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
</script>

<script type="text/javascript">
function pricecalculation(act)
{ 
	var no=1;
	var currentquantity=document.getElementById('qty').value;
	var unititemprice=document.getElementById('priceId').value;
	if(act=='add')
	{
	 var qty= parseInt(currentquantity)+parseInt(no);
	}
	else
	{ 
		if(parseInt(currentquantity)==1)
		{
		  var qty=parseInt(currentquantity)
		}
		else
		{
		  var qty=parseInt(currentquantity)-parseInt(no);
		}
	}
	var producttotal=qty*unititemprice;
	document.getElementById('rentamt').innerHTML = producttotal;
	document.getElementById('priceId').value = producttotal;
}
</script>
<script type="text/javascript">
function selectedBuyRent(val)
{
	if(val!='')
	{
		$('#addtocartprice').css('display','');
		$('#addtocart').css('display','');
		$('.rentamt').html(val);
		$('#priceId').val(val);
	}
}
</script>
<script type="text/javascript">
	function getdatedish(selecteddateproduct)
	{
	    if(selecteddateproduct)
		{ 
         $.ajax({
           type:"GET",
           url:"{{url('getproductdetail')}}?product_id="+selecteddateproduct,
           success:function(res){               
            if(res)
			{
            	 var json = JSON.stringify(res);
			     var obj = JSON.parse(json);
			    // alert(obj);
                 console.log(obj);            	
                 length = obj.productdateshopinfo.length;
                 //alert(length);
            	 if(length>0)
				 {
					 for(i=0; i<length; i++)
					 {
			
						$('#selectedproduct').html('<div class="service-left-display-img"><a name="common_linking" lass="linking">&nbsp;</a><img src="'+obj.productdateshopinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-product-description">'+obj.productdateshopinfo[i].pro_desc+'</div><p><em><strong>@if (Lang::has(Session::get('lang_file').'.NOTES')!= '') {{  trans(Session::get('lang_file').'.NOTES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.NOTES') }} @endif:</strong> </em>'+obj.productdateshopinfo[i].notes+'</p>');
						
						$('#buysect').html('<div class="equipment-redio-box"><input type="radio" id="buy" name="buy_rent" value="'+obj.productdateshopinfo[i].pro_price+'" onclick="selectedBuyRent(this.value);"><label for="buy">@if (Lang::has(Session::get('lang_file').'.BUY')!= '') {{  trans(Session::get('lang_file').'.BUY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BUY') }} @endif</label></div>');					
						$('#selectedproductprices').html('<input type="hidden" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" name="shopby" id="shopby" value=""><input type="text" name="price" id="priceId" value="'+obj.productdateshopinfo[i].pro_price+'">');
						$('#addtocartprice').css('display','none');
		                $('#addtocart').css('display','none');
					}			
			     }
			     pricelength=obj.productdateshopprice.length;
				
                 if(pricelength>0)
				 {
					for(k=0; k<pricelength; k++)
					{
					 $('#selectedproductprices').append('<input type="hidden" id="'+obj.productdateshopprice[k].product_option_id+'" name="pr'+obj.productdateshopprice[k].product_option_id+'" value="'+obj.productdateshopprice[k].product_option_value_id+'"><input type="text" name="price" id="priceId" value="'+obj.productdateshopprice[k].product_option_value_id+'">');
					 //$('#p'+obj.productdateshopprice[k].product_option_id).html('SAR '+obj.productdateshopprice[k].product_option_value_id);
					 
					 $('#rentsect').html('<div class="equipment-redio-box"><input type="radio" id="rent" name="buy_rent" value="'+obj.productdateshopprice[k].product_option_value_id+'" onclick="selectedBuyRent(this.value);"><label for="buy">@if (Lang::has(Session::get('lang_file').'.RENT')!= '') {{  trans(Session::get('lang_file').'.RENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.RENT') }} @endif</label></div>');
					 //$('.priceId').val(obj.productdateshopprice[k].product_option_value_id);
					 $('#addtocartprice').css('display','none');
		             $('#addtocart').css('display','none');
					}
				 }
           }
           }
        });
    }
	}

</script>
<script type="text/javascript">
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>

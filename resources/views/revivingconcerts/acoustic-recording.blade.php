@include('includes.navbar')
<link rel="stylesheet" href="{{ url('')}}/public/assets/themes/js/timepicker/jquery.datetimepicker.css" />
<div class="outer_wrapper">
  
    <div class="vendor_header">
      <div class="inner_wrap">
        <div class="vendor_header_left">
          <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$singerDetails->image}}" alt="" /></a></div>
        </div>
        <!-- vendor_header_left -->
        @include('includes.vendor_header')
        <!-- vendor_header_right -->

        <?php 
      if(Session::has('customerdata.user_id')) 
          {
        $nuserid  = Session::get('customerdata.user_id');
        $getnInfo = Helper::getuserinfo($nuserid); 
        if($getnInfo->cus_name==''){ $cuname='';}else{ $cuname=$getnInfo->cus_name;} 
      }
      ?>
      </div>
    </div>
    <!-- vemdor_header -->
    <div class="common_navbar">
      <div class="inner_wrap">
        <div id="menu_header" class="content">
          <ul>
            <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
                <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>
            <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
			<?php } if(count($allreview) > 0){ ?>
            <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
			<?php } ?>
            <!--<li><a href="#choose_package">{{ (Lang::has(Session::get('lang_file').'.Choose_Package')!= '')  ?  trans(Session::get('lang_file').'.Choose_Package'): trans($OUR_LANGUAGE.'.Choose_Package')}}</a></li>-->
          </ul>
        </div>
      </div>
    </div>
    <!-- common_navbar -->
    <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?><li><img src="{{str_replace('thumb_','',$singerDetails->image)}}" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">
            <?php
		    $name = 'name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $name = 'name_ar'; 
		    }
		    echo $singerDetails->$name;
		  ?>
          </div>
          <div class="detail_hall_description">
            <?php
		    $address = 'address'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $address = 'address_ar'; 
		    }
		    echo $singerDetails->$address;
		  ?>
          </div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_ACOUSTIC')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_ACOUSTIC'): trans($OUR_LANGUAGE.'.ABOUT_ACOUSTIC')}}</div>
          <div class="detail_about_hall">
            <div class="comment more"> @php
              @endphp
              @php $about='about'@endphp
              @if(Session::get('lang_file')!='en_lang')
              @php $about= 'about_ar'; @endphp
              @endif
              @php echo nl2br($singerDetails->$about); @endphp </div>
          </div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CTIY')}}: <span>
            <?php
		    $getcityname = Helper::getcity($singerDetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?>
            </span></div>
            @php 
            $long  = @$singerDetails->longitude;
            $lat = @$singerDetails->latitude;
            @endphp
            @if($long !='' && $lat!='')
            		<div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;"> </div>
            @endif
 
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
	  <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>
        <div class="service-video-area">
          <div class="service-video-cont">
            <?php 
		    $mc_name = 'video_description'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'video_description_ar'; 
		    }
		    echo $singerDetails->$mc_name; 
		  ?>
          </div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{$singerDetails->video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <!-- service-video-area -->
        <?php }if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  @foreach($allreview as $val)
                  @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
 
     


      <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                 <li><a href="{{ route('acousticequipment',[$id,$sid,$lid]) }}">{{ (Lang::has(Session::get('lang_file').'.EQUIPMENT')!= '')  ?  trans(Session::get('lang_file').'.EQUIPMENT'): trans($OUR_LANGUAGE.'.EQUIPMENT')}}</a></li>
                <li><a href="{{ route('acousticrecording',[$id,$sid,$lid]) }}" class="select">{{ (Lang::has(Session::get('lang_file').'.RECORDING')!= '')  ?  trans(Session::get('lang_file').'.RECORDING'): trans($OUR_LANGUAGE.'.RECORDING')}}</a></li>
               
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <!-- service_bottom -->
      {!! Form::open(array('url'=>"insert_acoustic",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'acoustic_frm','id'=>'acoustic_frm')) !!}
      {{ csrf_field() }}
      <input type="hidden" name="quote_for" value="recording"/>
      <input type="hidden" name="shop_id" value="{{$sid}}"/>
      <input type="hidden" name="music_id" value="{{$lid}}"/>
        <input type="hidden" name="vendor_id" value="{{$singerDetails->vendor_id}}"/>
      <?php 
		if(Session::has('customerdata.user_id')) 
		{
			$userid  = Session::get('customerdata.user_id');
		}
	  ?>
      <input type="hidden" name="user_id" value="<?php if(isset($userid) && $userid!=''){echo $userid;}?>"/>
      <!--<input type="hidden" name="singer_name" value="{{$singerDetails->
      $name}}"/>-->
      <div class="recording-area">
        <div class="kosha-box">
          
          <div class="checkout-form">
            <div class="checkout-form-row">
              <div class="checkout-form-cell cell-full-width">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.THE_GROOM_NAME')!= '') {{ trans(Session::get('lang_file').'.THE_GROOM_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.THE_GROOM_NAME')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="groom_name" maxlength="75" id="groom_name" value="{{ $cuname }}" type="text">
                  @if($errors->has('groom_name'))<span class="error">{{ $errors->first('groom_name') }}</span>@endif </div>
              </div>
              <!-- checkout-form-cell -->
            </div>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.HALL')!= '') {{ trans(Session::get('lang_file').'.HALL')}}  @else {{ trans($OUR_LANGUAGE.'.HALL')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="hall" id="hall" type="text" maxlength="75" value="{!! Input::old('hall') !!}">
                  @if($errors->has('hall'))<span class="error">{{ $errors->first('hall') }}</span>@endif </div>
              </div>
              <!-- checkout-form-cell -->
              <div class="checkout-form-cell">
                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')}}</div>
                <div class="checkout-form-bottom">
                  <select name="occasion" id="occasion" class="t-box">
                    <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '') {{  trans(Session::get('lang_file').'.Select_Occasion_Type') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Occasion_Type') }} @endif</option>
                    <?php 
					   if(Session::get('lang_file')!='en_lang')
					   {
						  $getArrayOfOcc = array( '2'=>'مناسبة الزفاف'); 
					   }
					   else
					   {
					      $getArrayOfOcc = array( '2'=>'Wedding Occasion'); 
					   }
					   foreach($getArrayOfOcc as $key=>$ocval){?>
                    <option value="" disabled="" style="color: #d2cece;">{{$ocval}}</option>
                    <?php 
						$setOccasion = Helper::getAllOccasionNameList($key);
						foreach($setOccasion as $val){ ?>
                    <option value="{{$val->id}}"<?php if(isset($occasiontype) && $occasiontype==$val->id){ echo 'selected="selected"'; }?>>
                    <?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
                    </option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                  @if($errors->has('occasion'))<span class="error">{{ $errors->first('occasion') }}</span>@endif </div>
              </div>
              <!-- checkout-form-cell -->
            </div>
            <!-- checkout-form-row -->
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="form-cell-date date-time">
                  <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.DATE')!= '') {{ trans(Session::get('lang_file').'.DATE')}}  @else {{ trans($OUR_LANGUAGE.'.DATE')}} @endif / @if(Lang::has(Session::get('lang_file').'.TIME')!= '') {{ trans(Session::get('lang_file').'.TIME')}}  @else {{ trans($OUR_LANGUAGE.'.TIME')}} @endif</div>
                  <div class="checkout-form-bottom">
                    <input class="t-box cal-t datetimepicker" autocomplete="off" name="date" id="date" value="{!! Input::old('date') !!}" type="text" readonly="">
                    @if($errors->has('date'))<span class="error">{{ $errors->first('date') }}</span>@endif </div>
                </div>
				
              <!--<div class="form-cell-time">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.TIME')!= '') {{ trans(Session::get('lang_file').'.TIME')}}  @else {{ trans($OUR_LANGUAGE.'.TIME')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box sel-time" name="time" id="times" value="{!! Input::old('time') !!}" type="text">
                  @if($errors->has('time'))<span class="error">{{ $errors->first('time') }}</span>@endif </div>
              </div>-->
            </div>
			<!-- <div class="checkout-form-cell">
            <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.RECORDING_SECTION')!= '') {{ trans(Session::get('lang_file').'.RECORDING_SECTION')}}  @else {{ trans($OUR_LANGUAGE.'.RECORDING_SECTION')}} @endif</div>
            <div class="checkout-form-bottom">
              <input class="t-box" name="recording_section" maxlength="75" id="recording_section" value="{!! Input::old('recording_section') !!}" type="text">
              @if($errors->has('recording_section'))<span class="error">{{ $errors->first('recording_section') }}</span>@endif </div>
          </div> {{$singerDetails->$name}} -->

          <!-- checkout-form-cell -->

  <div class="checkout-form-cell">
        <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.SINGER_NAME')!= '') {{ trans(Session::get('lang_file').'.SINGER_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.SINGER_NAME')}} @endif</div>
        <div class="checkout-form-bottom">
		  <?php $name = 'name';if(Session::get('lang_file')!='en_lang'){$name = 'name_ar';}?>
          <input class="t-box" name="singer_name" maxlength="75" id="singer_name" value="" type="text">
          @if($errors->has('singer_name'))<span class="error">{{ $errors->first('singer_name') }}</span>@endif </div>
      </div>

          </div><!-- checkout-form-row -->
         <div class="checkout-form-row">
        <div class="checkout-form-cell">
          <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.MUSIC_TYPE')!= '') {{ trans(Session::get('lang_file').'.MUSIC_TYPE')}}  @else {{ trans($OUR_LANGUAGE.'.MUSIC_TYPE')}} @endif</div>
          <div class="checkout-form-bottom">
            <input class="t-box" name="music_type" id="music_type" maxlength="75" value="{!! Input::old('music_type') !!}" type="text">
            @if($errors->has('music_type'))<span class="error">{{ $errors->first('music_type') }}</span>@endif </div>
        </div>
	<div class="checkout-form-cell">
   
                    <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}</div>
                    <div class="checkout-form-bottom">
                      <select class="checkout-small-box" name="city" id="city">
            <option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_CITY') }} @endif</option>
             @php $getC = Helper::getCountry(); @endphp
              @foreach($getC as $cbval)
              <option value="" disabled="" style="color: #d2cece;">{{$cbval->co_name}}</option>
               @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
              @foreach ($getCity as $val)
                 @php if(isset($city_id) && $city_id==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} @endphp
              @if($selected_lang_code !='en')
              @php $ci_name= 'ci_name_ar'; @endphp
              @else
               @php $ci_name= 'ci_name'; @endphp
              @endif   
              <option value="{{ $val->ci_id }}" {{ $selectedcity }} >{{ $val->$ci_name }}</option>
               @endforeach
              @endforeach
            </select>
            @if($errors->has('city'))<span class="error">{{ $errors->first('city') }}</span>@endif
                    </div>
                  </div>
      </div>
	  <div class="checkout-form-row">
        <div class="checkout-form-cell ">
		 
          <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.SONG_NAME')!= '') {{ trans(Session::get('lang_file').'.SONG_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.SONG_NAME')}} @endif</div>
          <div class="checkout-form-bottom">
            <input type="text" class="t-box" name="song_name" maxlength="75" id="song_name" value="{!! Input::old('song_name') !!}">
            @if($errors->has('song_name'))<span class="error">{{ $errors->first('song_name') }}</span>@endif </div>
		
        </div>
		
		  <div class="checkout-form-cell ">
     
          <div class="checkout-form-top"> </div>
          
    
        </div>
      </div>
    
          <div class="acoustic-button-line">
  <input type="submit" name="submit" class="form-btn btn-info-wisitech" value="@if(Lang::has(Session::get('lang_file').'.GET_A_QUOTE')!= '') {{ trans(Session::get('lang_file').'.GET_A_QUOTE')}}  @else {{ trans($OUR_LANGUAGE.'.GET_A_QUOTE')}} @endif" />
</div>
        </div>
        <!-- checkout-form-cell -->
      </div>
      <!-- checkout-form-row -->
      
      <!-- checkout-form-cell -->
      
      
    </div>
    <!-- checkout-form-cell -->
  </div>
  <!-- checkout-form-row -->
</div>

</div>
</div>
</div>
{!! Form::close() !!}
@include('includes.other_services')
</div>
</div>
<!-- detail_page -->
</div>
<!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
@include('includes.footer')
<script src="{{ url('')}}/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>
<script> 
  
var checkPastTime = function(inputDateTime) {
    if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {
        var current = new Date();
 
        //check past year and month
        if (inputDateTime.getFullYear() < current.getFullYear()) {
            $('.datetimepicker').datetimepicker('reset');
            alert("Sorry! Past date time not allow.");
        } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {
            $('.datetimepicker').datetimepicker('reset');
            alert("Sorry! Past date time not allow.");
        }
 
        // 'this' is jquery object datetimepicker
        // check input date equal to todate date
        if (inputDateTime.getDate() == current.getDate()) {
            if (inputDateTime.getHours() < current.getHours()) {
                $('.datetimepicker').datetimepicker('reset');
            }
            this.setOptions({
                minTime: current.getHours() + ':00' //here pass current time hour
            });
        } else {
            this.setOptions({
                minTime: false
            });
        }
    }
};
 
var currentYear = new Date();
$('.datetimepicker').datetimepicker({
    format:'d M Y g:i A',
    minDate : 0,
    yearStart : currentYear.getFullYear(), // Start value for current Year selector
    onChangeDateTime:checkPastTime,
    onShow:checkPastTime
});
/*
jQuery(window).load(function()
{
	jQuery("body").on('mouseover', '.datetimepicker', function() {  
	jQuery(this).datetimepicker({ ampm: true, // FOR AM/PM FORMAT
    format : 'd M Y g:i A',
    useCurrent: true,
    minDate : 0,
    minTime: 0 });
	});
})
*/
  

</script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(){
 jQuery("#acoustic_frm").validate({
    rules: {   
	      "groom_name" : {
            required : true
          },       
          "hall" : {
            required : true
          },  
          "occasion" : {
            required : true
          },           
		  "date" : {
            required : true
          },  
		  "time" : {
            required : true
          }, 
		  "recording_section" : {
            required : true
          },           
		  "music_type" : {
            required : true
          }, 
          "singer_name" : {
            required:true
          }, 
		  "song_name" : {
            required:true
          },  
		  "city" : {
            required : true
          },  
         },
         messages: {
		 "groom_name": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_GROOM_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_GROOM_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_GROOM_NAME') }} @endif"
          },
          "hall": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_HALL')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_HALL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_HALL') }} @endif"
          },
          "occasion": {
            required:  "@if (Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= '') {{  trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_OCCASION') }} @endif"
          },           
		  "date": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DATE') }} @endif"
          },
		  "time": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_TIME') }} @endif"
          },
		  "recording_section": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_RECOR_SEC')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_RECOR_SEC') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_RECOR_SEC') }} @endif"
          },          
		  "music_type": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_MUSIC_TIPE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_MUSIC_TIPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_MUSIC_TIPE') }} @endif"
          },
          "singer_name": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_SINGER_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_SINGER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_SINGER_NAME') }} @endif"
          }, 
		  "song_name": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_SONG_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_SONG_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_SONG_NAME') }} @endif"
          },    
		  "city": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_CITY') }} @endif"
          },  
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#acoustic_frm").valid()) {        
    jQuery('#acoustic_frm').submit();
   }
  });
});
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content">{{ Session::get('message') }}</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/>
      <a class="action_yes status_yes" href="javascript:void(0);">Ok   </a> </div>
  </div>
</div>
       
@if(Session::has('message'))
<script type="text/javascript">
$(document).ready(function()
{
 $('.action_popup').fadeIn(500);
 $('.overlay').fadeIn(500);
});
</script>
@endif
<script type="text/javascript">
$('.status_yes').click(function()
{
 $('.overlay, .action_popup').fadeOut(500);
});
</script>



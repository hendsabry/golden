@include('includes.navbar')
<link rel="stylesheet" href="{{ url('')}}/public/assets/themes/js/timepicker/jquery.datetimepicker.css" />
<div class="outer_wrapper">
  
    <div class="vendor_header">
      <div class="inner_wrap">
        <div class="vendor_header_left">
          <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$singerDetails->image}}" alt="" /></a></div>
        </div>
        <!-- vendor_header_left -->
        @include('includes.vendor_header')
        <!-- vendor_header_right -->
      </div>
    </div>
    <!-- vemdor_header -->
    <div class="common_navbar">
      <div class="inner_wrap">
        <div id="menu_header" class="content">
          <ul>
            <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
              <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>
            <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
           <?php }if(count($allreview) > 0){ ?>
            <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
          <li><a href="#quotation">{{ (Lang::has(Session::get('lang_file').'.GET_QUOTATION')!= '')  ?  trans(Session::get('lang_file').'.GET_QUOTATION'): trans($OUR_LANGUAGE.'.GET_QUOTATION')}}</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- common_navbar -->
    <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?><li><img src="{{str_replace('thumb_','',$singerDetails->image)}}" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
               <?php  
                  if(isset($singerDetails->id) && $singerDetails->id!=''){
                  $getallimage = Helper::getallimagelist($singerDetails->id,$singerDetails->vendor_id);
                  foreach($getallimage as $value){ ?>               
                    <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">
		  <?php
		    $name = 'name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $name = 'name_ar'; 
		    }
		    echo $singerDetails->$name;
		  ?>
		  </div>
          <div class="detail_hall_description">
		  <?php
		    $address = 'address'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $address = 'address_ar'; 
		    }
		    echo $singerDetails->$address;
		  ?>
		  </div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_BAND')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_BAND'): trans($OUR_LANGUAGE.'.ABOUT_BAND')}}</div>
          <div class="detail_about_hall">
            <div class="comment more">
			  @php
			  @endphp
			  @php $about='about'@endphp
			  @if(Session::get('lang_file')!='en_lang')
			  @php $about= 'about_ar'; @endphp
			  @endif
			 @php echo nl2br($singerDetails->$about); @endphp			
			</div>
          </div>
          <!--div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CTIY')}}: <span>
		  <?php
		    $getcityname = Helper::getcity($singerDetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    //echo $getcityname->$mc_name; 
		  ?>
          </span></div-->
		   <div class="detail_hall_dimention"><iframe src="{{ $singerDetails->google_map_url }}" width="450" height="230" frameborder="0" style="border:0" allowfullscreen></iframe></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
	  <?php if(isset($singerDetails->video_url) && $singerDetails->video_url!=''){ ?>
        <div class="service-video-area">
          <div class="service-video-cont">		  
		  <?php 
		    $mc_name = 'video_description'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'video_description_ar'; 
		    }
		    echo $singerDetails->$mc_name; 
		  ?>	  
		  </div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{$singerDetails->video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>		  
        <!-- service-video-area -->
	   <?php }if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
				 @foreach($allreview as $val)
				 @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
				  @endforeach			  
                </ul>
              </div>
            </section>
          </div>
        </div>
		<?php } /*else{ ?>
		<div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
		<?php } */?>
        <div class="singer-area singer-area-title"><a name="quotation" class="linking">&nbsp;</a>
          <div class="singer-heading">{{ (Lang::has(Session::get('lang_file').'.BAND_INFORMATION')!= '')  ?  trans(Session::get('lang_file').'.BAND_INFORMATION'): trans($OUR_LANGUAGE.'.BAND_INFORMATION')}}</div>
          <div class="singer-box">
            <div class="singer-image-area"><img src="{{$singerDetails->image}}" alt="" /></div>
            <div class="singer-form-area">			
              <div class="singer-name">
			   <?php
				$name = 'name'; 
				if(Session::get('lang_file')!='en_lang')
				{
				  $name = 'name_ar'; 
				}
				echo ucfirst($singerDetails->$name);
			   ?>
		      </div>
			  {!! Form::open(array('url'=>"insert_singer",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'singer_frm','id'=>'singer_frm')) !!}
			   {{ csrf_field() }}
        <input type="hidden" name="quote_for" value="band"/>
			  <input type="hidden" name="shop_id" value="{{$sid}}"/>
			  <input type="hidden" name="music_id" value="{{$lid}}"/>
        <input type="hidden" name="vendor_id" value="{{$singerDetails->vendor_id}}"/>
			  <?php 
				if(Session::has('customerdata.user_id')) 
				{
					$userid  = Session::get('customerdata.user_id');
				}
			  ?>
			  <input type="hidden" name="user_id" value="<?php if(isset($userid) && $userid!=''){echo $userid;}?>"/>
			  <input type="hidden" name="singer_name" value="{{$singerDetails->$name}}"/>
              <div class="checkout-form">
                <div class="checkout-form-row">
                  <div class="checkout-form-cell singer-bottom-space">
                    <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.HALL')!= '') {{ trans(Session::get('lang_file').'.HALL')}}  @else {{ trans($OUR_LANGUAGE.'.HALL')}} @endif</div>
                    <div class="checkout-form-bottom">
                      <input class="t-box" name="hall" id="hall" type="text" maxlength="75" value="{!! Input::old('hall') !!}">
					  @if($errors->has('hall'))<span class="error">{{ $errors->first('hall') }}</span>@endif
                    </div>
                  </div>
                  <!-- checkout-form-cell -->
                  <div class="checkout-form-cell">
                    <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')}}</div>
                    <div class="checkout-form-bottom">
                      <select name="occasion" id="occasion">
                    <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '') {{  trans(Session::get('lang_file').'.Select_Occasion_Type') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Occasion_Type') }} @endif</option>
					  <?php 
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $getArrayOfOcc = array( '2'=>'مناسبة الزفاف'); 
					   }
					   else
					   {
					      $getArrayOfOcc = array( '2'=>'Wedding Occasion'); 
					   } 
					   foreach($getArrayOfOcc as $key=>$ocval){?>
					    <option value="" disabled="" style="color: #d2cece;">{{$ocval}}</option>
						<?php 
						$setOccasion = Helper::getAllOccasionNameList($key);
						foreach($setOccasion as $val){ ?>
						<option value="{{$val->id}}"<?php if(isset($occasiontype) && $occasiontype==$val->id){ echo 'selected="selected"'; }?>><?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
					   </option>
						<?php } ?>
					 <?php } ?>
                  </select>
                  @if($errors->has('occasion'))<span class="error">{{ $errors->first('occasion') }}</span>@endif
                </div>
                  @if($errors->has('occasion'))<span class="error">{{ $errors->first('occasion') }}</span>@endif
                    </div>
                  </div>
                  <!-- checkout-form-cell -->
                </div>
                <!-- checkout-form-row -->
                <div class="checkout-form-row">
                  <div class="checkout-form-cell">
                    <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}</div>
                    <div class="checkout-form-bottom">
                      <select class="checkout-small-box" name="city" id="city">
						<option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_CITY') }} @endif</option>
						 @php $getC = Helper::getCountry(); @endphp
							@foreach($getC as $cbval)
							<option value="" disabled="" style="color: #d2cece;">{{$cbval->co_name}}</option>
							 @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
							@foreach ($getCity as $val)
							   @php if(isset($city_id) && $city_id==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} @endphp
							@if($selected_lang_code !='en')
							@php $ci_name= 'ci_name_ar'; @endphp
							@else
							 @php $ci_name= 'ci_name'; @endphp
							@endif   
							<option value="{{ $val->ci_id }}" {{ $selectedcity }} >{{ $val->$ci_name }}</option>
							 @endforeach
							@endforeach
					  </select>
					  @if($errors->has('city'))<span class="error">{{ $errors->first('city') }}</span>@endif
                    </div>
                  </div>
                  <!-- checkout-form-cell -->
                  <div class="checkout-form-cell">
                    <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.LOCATION')!= '') {{ trans(Session::get('lang_file').'.LOCATION')}}  @else {{ trans($OUR_LANGUAGE.'.LOCATION')}} @endif</div>
                    <div class="checkout-form-bottom">
                      <input class="t-box" type="text" maxlength="75" name="location" id="location" value="{!! Input::old('location') !!}">
					  @if($errors->has('location'))<span class="error">{{ $errors->first('location') }}</span>@endif
                    </div>
                  </div>
                  <!-- checkout-form-cell -->
                </div>
                <!-- checkout-form-row -->
                <div class="checkout-form-row">
                  <div class="checkout-form-cell">
                    <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.DURATION')!= '') {{ trans(Session::get('lang_file').'.DURATION')}}  @else {{ trans($OUR_LANGUAGE.'.DURATION')}} @endif</div>
                    <div class="checkout-form-bottom">
                      <input class="t-box" maxlength="2" name="duration" id="duration" type="text" value="{!! Input::old('duration') !!}" style="max-width:125px;">
					  @if($errors->has('duration'))<span class="error">{{ $errors->first('duration') }}</span>@endif
                    </div>
                  </div>
                  <!-- checkout-form-cell -->
                  <div class="checkout-form-cell">
                    <div class="form-cell-date date-time">
                      <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.DATE')!= '') {{ trans(Session::get('lang_file').'.DATE')}}  @else {{ trans($OUR_LANGUAGE.'.DATE')}} @endif / @if(Lang::has(Session::get('lang_file').'.TIME')!= '') {{ trans(Session::get('lang_file').'.TIME')}}  @else {{ trans($OUR_LANGUAGE.'.TIME')}} @endif</div>
                      <div class="checkout-form-bottom">
                        <input class="t-box cal-t datetimepicker" autocomplete="off" name="date" id="date" value="{!! Input::old('date') !!}" type="text" readonly="">
						@if($errors->has('date'))<span class="error">{{ $errors->first('date') }}</span>@endif
                      </div>
                    </div>
                    <!--<div class="form-cell-time">
                      <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.TIME')!= '') {{ trans(Session::get('lang_file').'.TIME')}}  @else {{ trans($OUR_LANGUAGE.'.TIME')}} @endif</div>
                      <div class="checkout-form-bottom">                        
						<input class="t-box sel-time" name="time" id="times" value="{!! Input::old('time') !!}" type="text">
						@if($errors->has('time'))<span class="error">{{ $errors->first('time') }}</span>@endif
                      </div>
                    </div>-->
                  </div>
                  <!-- checkout-form-cell -->
                </div>
                <!-- checkout-form-row -->
                <div class="checkout-form-row">
                  <div class="checkout-form-cell cell-full-width">
                    <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.COMMENTS')!= '') {{ trans(Session::get('lang_file').'.COMMENTS')}}  @else {{ trans($OUR_LANGUAGE.'.COMMENTS')}} @endif </div>
                    <div class="checkout-form-bottom">
                      <textarea class="text-area" name="comments" id="comments">{!! Input::old('comments') !!}</textarea>
					  @if($errors->has('comments'))<span class="error">{{ $errors->first('comments') }}</span>@endif
                    </div>
                  </div>
                  <!-- checkout-form-cell -->
                </div>
                <!-- checkout-form-row -->
                <div class="form-button-line">
                  <input type="submit" name="submit" class="form-btn" value="@if(Lang::has(Session::get('lang_file').'.ASK_FOR_A_QUOTE')!= '') {{ trans(Session::get('lang_file').'.ASK_FOR_A_QUOTE')}}  @else {{ trans($OUR_LANGUAGE.'.ASK_FOR_A_QUOTE')}} @endif" />
                </div>
              </div>
			  {!! Form::close() !!}
              <!-- checkout-form -->
            </div>
            <!-- singar-form-area -->
          </div>
          <!-- singer-box -->
        </div>
        <!-- singer-area -->
      </div>

      <!-- service-mid-wrapper -->
@include('includes.other_services')
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
@include('includes.footer')
<script src="{{ url('')}}/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>
<script>
  
var checkPastTime = function(inputDateTime) {
    if (typeof(inputDateTime) != "undefined" && inputDateTime !== null) {
        var current = new Date();
 
        //check past year and month
        if (inputDateTime.getFullYear() < current.getFullYear()) {
            $('.datetimepicker').datetimepicker('reset');
            alert("Sorry! Past date time not allow.");
        } else if ((inputDateTime.getFullYear() == current.getFullYear()) && (inputDateTime.getMonth() < current.getMonth())) {
            $('.datetimepicker').datetimepicker('reset');
            alert("Sorry! Past date time not allow.");
        }
 
        // 'this' is jquery object datetimepicker
        // check input date equal to todate date
        if (inputDateTime.getDate() == current.getDate()) {
            if (inputDateTime.getHours() < current.getHours()) {
                $('.datetimepicker').datetimepicker('reset');
            }
            this.setOptions({
                minTime: current.getHours() + ':00' //here pass current time hour
            });
        } else {
            this.setOptions({
                minTime: false
            });
        }
    }
};
 
var currentYear = new Date();
$('.datetimepicker').datetimepicker({
    format:'d M Y g:i A',
    minDate : 0,
    yearStart : currentYear.getFullYear(), // Start value for current Year selector
    onChangeDateTime:checkPastTime,
    onShow:checkPastTime
});
/* 
jQuery(window).load(function()
{
	jQuery("body").on('mouseover', '.datetimepicker', function() {  
		jQuery(this).datetimepicker({
		ampm: true, // FOR AM/PM FORMAT
		format : 'd M Y g:i A',
		minDate : 0,
    minTime: 0 
  });
  });
})
*/
</script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(){
 jQuery("#singer_frm").validate({
    rules: {          
          "hall" : {
            required : true
          },  
          "occasion" : {
            required : true
          },  
          "city" : {
            required : true
          },
          "location" : {
            required : true
          }, 
		  "duration" : {
            required : true
          },  
		  "date" : {
            required : true
          },  
		  "time" : {
            required : true
          }, 
          /*"comments" : {
            required:true
          },*/     
         },
         messages: {
          "hall": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_HALL')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_HALL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_HALL') }} @endif"
          },
          "occasion": {
            required:  "@if (Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= '') {{  trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_OCCASION') }} @endif"
          }, 
          "city": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_CITY') }} @endif"
          },
          "location": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_LOCATION') }} @endif"
          },
		  "duration": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DURATION') }} @endif"
          },
		  "date": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_DATE') }} @endif"
          },
		  "time": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_TIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_TIME') }} @endif"
          },
          /*"comments": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES')!= '') {{  trans(Session::get('lang_file').'.ENTER_COMMENTS_QUERIES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_COMMENTS_QUERIES') }} @endif"
          },*/      
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#singer_frm").valid()) {        
    jQuery('#singer_frm').submit();
   }
  });
});
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
$('#duration').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
</script>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content">{{ Session::get('message') }}</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/>
      <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>
  </div>
</div>
       
@if(Session::has('message'))
<script type="text/javascript">
$(document).ready(function()
{
 $('.action_popup').fadeIn(500);
 $('.overlay').fadeIn(500);
});
</script>
@endif
<script type="text/javascript">
$('.status_yes').click(function()
{
 $('.overlay, .action_popup').fadeOut(500);
});
</script>
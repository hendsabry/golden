@include('includes.navbar')
<div class="outer_wrapper diamond_fullwidth">
    @include('includes.header')
  <div class="inner_wrap"> 

	<div class="search-section">
	<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
	@include('includes.searchweddingandoccasions')
	</div>
	 <!-- search-section -->
    <!-- search-section -->
    <div class="page-left-right-wrapper">
      @include('includes.mobile-modify')


      <div class="page-right-section">

		   <div class="diamond_main_wrapper">
				@if(Session::get('lang_file')!='en_lang')
				<img src="{{ url('') }}/themes/images/singer_ar.jpg" alt="" usemap="#hall-subcategory" hidefocus="true">
				@else
				 <img src="{{ url('') }}/themes/images/singer.jpg" alt="" usemap="#hall-subcategory">
				@endif
				  <map name="hall-subcategory" id="hall-subcategory">
					<area shape="poly" coords="572,112,469,12,299,178,292,188,171,307,768,310" href="{{ url('') }}/singers-list/24/25" />
					<area shape="poly" coords="776,616,166,614,280,732,290,741,384,833,470,920" href="{{ url('') }}/acoustic-list/24/27" />	
					<area shape="poly" coords="773,314,166,312,13,463,163,611,777,613,923,464" href="{{ url('') }}/popular-bands-list/24/26" />
				  </map>
			</div>
          <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>

        <!-- diamond-area -->
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
<div class="oops_popup">
  <div class="oops_title">Oops!</div>
  <div class="ooops_desciption">You have exceeded your budget! Happens. 
    Simply revisit your plan or increase your budget. </div>
  <a href="javascript:void(0)" class="oops_btn">OK</a> </div>
<!-- oops_popup -->
<div class="services_popup">
  <div class="serv_popup_row">
    <div class="service_popup_title">Taj Hall 1</div>
    <a href="javascript:void(0)" class="serv_pop_close">X</a> </div>
  <!-- serv_popup_row -->
  <div class="serv_popupimg"><img src="{{ url('') }}/themes/images/business.png"></div>
  <div class="serv_popup_row">
    <div class="serv_price">SAR 20,000</div>
    <a href="javascript:void(0)" class="serv_detail_link">View Details</a> </div>
  <!-- servicepopup_toprow -->
</div>
<!-- services_popup -->
@include('includes.footer') 
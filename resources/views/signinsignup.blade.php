@include('includes.navbar')
<div class="outer_wrapper">

@include('includes.header')

 <!-- outer_wrapper -->
</div>
 <div class="header-page-divder">&nbsp;</div>
 @php (Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ? $sub= trans(Session::get('lang_file').'.SUBMIT'): $sub=trans($OUR_LANGUAGE.'.SUBMIT');

  @endphp
<div class="inner_wrap"><a name="loginfrml"></a>
  <div class="usersign-area">
  <div class="usersign-socal-area">
  <div class="form_title">{{ (Lang::has(Session::get('lang_file').'.Sign_In_Sign_Up_With')!= '')  ?  trans(Session::get('lang_file').'.Sign_In_Sign_Up_With'): trans($OUR_LANGUAGE.'.Sign_In_Sign_Up_With')}}</div>
  <div class="usersign-socal-link">

    <a href="{{route('gplusredirect')}}"><img src="{{url('/')}}/themes/images/google-plus-icon.png" alt="" title="Google+" /></a>



  </div>
  </div> <!-- usersign-socal-area -->
  
  <div class="login-or">{{ (Lang::has(Session::get('lang_file').'.OR')!= '')  ?  trans(Session::get('lang_file').'.OR'): trans($OUR_LANGUAGE.'.OR')}}</div>
  
  <div class="usersign-box-area signup_page">
  <div class="usersign-box">
 
     
  {!! Form::open(['url' => 'login-signup/checkloginaccount', 'method' => 'post', 'name'=>'loginfrm', 'id'=>'loginfrm', 'enctype' => 'multipart/form-data']) !!}

  <div class="usersign-box-heading">{{ (Lang::has(Session::get('lang_file').'.MER_SIGN_IN')!= '')  ?  trans(Session::get('lang_file').'.MER_SIGN_IN'): trans($OUR_LANGUAGE.'.MER_SIGN_IN')}}</div>
  
  <div class="usersign-form">  
 
              @if(isset($_REQUEST['error']) && $_REQUEST['error']!='')
                        <div class="alert error user_not">
                     {{ (Lang::has(Session::get('lang_file').'.Your_username_or_password_is_incorrect')!= '')  ?  trans(Session::get('lang_file').'.Your_username_or_password_is_incorrect'): trans($OUR_LANGUAGE.'.Your_username_or_password_is_incorrect')}} 
                          

                 
                        </div>
                    @endif

					 @if (session('chstatus'))
                        <div class="alert error ">
                            {{ session('chstatus') }}
                        </div>
                    @endif
					
  <div class="usersign-form-line">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.EMAILADDRRESS')!= '')  ?  trans(Session::get('lang_file').'.EMAILADDRRESS'): trans($OUR_LANGUAGE.'.EMAILADDRRESS')}}</div>
  <div class="usersign-form-bottom">{!! Form::text('email', null, array( 'class'=>'t-box','maxlength'=>'80')) !!}</div>
  </div>
  <div class="usersign-form-line">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.PASSWORD'): trans($OUR_LANGUAGE.'.PASSWORD')}}</div>
  <div class="usersign-form-bottom">{!! Form::password('password',  array( 'class'=>'t-box','maxlength'=>'20')) !!}</div>
  </div>
  <div class="usersign-form-button">{!! Form::submit($sub, array('class'=>'form-btn')) !!}<div class="login-rememberme-div">
  <!--<input name="rememberme" id="rememberme" value="forever" type="checkbox"><label for="rememberme">Remember me</label>--></div></div>
  
  <div class="forgot-password"><a href="{{url('')}}/userforgotpassword">{{ (Lang::has(Session::get('lang_file').'.MER_FORGOT_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.MER_FORGOT_PASSWORD'): trans($OUR_LANGUAGE.'.MER_FORGOT_PASSWORD')}}</a></div>
   </div>
   {!! Form::close() !!}
  </div>   <!-- usersign-box -->
  
  
   <div class="sign-clear"></div>
   <div class="usersign-divder">&nbsp;</div>
  
{!! Form::open(['url' => 'login-signup/adduseraccount', 'method' => 'post', 'name'=>'regfrm', 'id'=>'regfrm', 'enctype' => 'multipart/form-data']) !!}

 {{ csrf_field() }}
<div class="usersign-box">

  <div class="usersign-box-heading">{{ (Lang::has(Session::get('lang_file').'.SIGNUP')!= '')  ?  trans(Session::get('lang_file').'.SIGNUP'): trans($OUR_LANGUAGE.'.SIGNUP')}}</div>
  <div class="usersign-form">
    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
  <div class="usersign-form-line">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.USER_NEME_LEVEL')!= '')  ?  trans(Session::get('lang_file').'.USER_NEME_LEVEL'): trans($OUR_LANGUAGE.'.USER_NEME_LEVEL')}}</div>
  <div class="usersign-form-bottom"> {!! Form::text('uname', null, array( 'class'=>'t-box','maxlength'=>'50','required')) !!}</div>
  </div>
  
   <div class="usersign-form-line">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.EMAILADDRRESS')!= '')  ?  trans(Session::get('lang_file').'.EMAILADDRRESS'): trans($OUR_LANGUAGE.'.EMAILADDRRESS')}}</div>
  <div class="usersign-form-bottom"> {!! Form::text('uemail', null, array('required','id'=>'emailaddres', 'class'=>'t-box','maxlength'=>'80','onfocusout'=>'checkalreadyregisteredemail(this.value);')) !!}<span id="availabilitystatus"></span></div>
  </div>

  <div class="usersign-form-line country_row">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.USER_MOBILE_LEVEL')!= '')  ?  trans(Session::get('lang_file').'.USER_MOBILE_LEVEL'): trans($OUR_LANGUAGE.'.USER_MOBILE_LEVEL')}}</div>
  <div class="usersign-form-bottom">
    <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode" required>
      
      @foreach($getCountry as $Ccode)
        <option value="{{ $Ccode->country_code}}">+{{ $Ccode->country_code}}</option>        
        @endforeach
    </select>
    {!! Form::text('umobile', null, array('required', 'class'=>'t-box mobleno','maxlength'=>'13','onkeypress'=>'return isNumberKey(event);')) !!} </div>  
  <label for="country_code" class="error"></label>
    <label for="umobile" class="error"></label>
  </div>
   <div class="usersign-form-line">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.PASSWORD'): trans($OUR_LANGUAGE.'.PASSWORD')}}</div>
  <div class="usersign-form-bottom">{!! Form::password('upassword',  array('required', 'maxlength'=>'20', 'class'=>'t-box','id'=>'upassword')) !!}</div>
  </div>
  
  <div class="usersign-form-line">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.Confirm_Password')!= '')  ?  trans(Session::get('lang_file').'.Confirm_Password'): trans($OUR_LANGUAGE.'.Confirm_Password')}}</div>
  <div class="usersign-form-bottom">{!! Form::password('cpassword',  array('required', 'maxlength'=>'20', 'class'=>'t-box')) !!}</div>
  </div>

  <div class="usersign-form-button">{!! Form::submit($sub, array('class'=>'form-btn')) !!}</div>
  
  </div>
   </div> <!-- usersign-box -->
   
 {!! Form::close() !!} 
  
  </div> <!-- usersign-box-area -->
  
  
    
  
  </div> <!-- usersign-area -->

<script language="javascript">

function checkalreadyregisteredemail(emailaddress) {
	var emailaddress=emailaddress;
	$.ajax({
	url: "{{url('login-signup/checkuseraccount')}}",
	data: { email: emailaddress, _token: '{{csrf_token()}}' },
	type: "POST",
	dataType: "text",
	success:function(data){
			if(data=='Error'){
					$("#emailaddres").val('');
					$("#availabilitystatus").html("<span class='status-not-available error'>This email address already registered. Try with other email or forgot password.</span>");
          
			}else{
					$("#availabilitystatus").html("");
			}
			
		
	},
	error:function (){ }
	});

}
</script>

  






</div>




 <script type="text/javascript">
 
jQuery("#loginfrm").validate({
                  ignore: [],
                  rules: {
                      email: {
                       required: true,
                       email: true,
                      },
                       password: {
                       required: true,
                       minlength: 8
                      },
                  
                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
             email: {
               required:  "@php echo (Lang::has(Session::get('lang_file').'.USER_EMAIL')!= '')  ?  trans(Session::get('lang_file').'.USER_EMAIL'): trans($OUR_LANGUAGE.'.USER_EMAIL'); @endphp", 
			   email:  "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_VALID_EMAIL')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_VALID_EMAIL'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_VALID_EMAIL'); @endphp",
                      },  
          password: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($OUR_LANGUAGE.'.USER_PASSWORD'); @endphp",
			   minlength: "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_CHARACTERS'); @endphp",
                      },
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 



 
 <script type="text/javascript">

jQuery("#regfrm").validate({
                  ignore: [],
                  rules: {
                          uname: {
                          required: true,
 
                          },
                          uemail: {
                          required: true,
                          email: true,
                          },
                          country_code: {
                          required: true,
                 
                          },
                          umobile: {
                          required: true,
                 
                          },
                          upassword: {
                          required: true,
                           minlength: 8
                          },
                           cpassword : {
                             minlength : 8,
                             equalTo : "#upassword"
                           }
                          },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
              jQuery("#availabilitystatus").html("");
                },
             
           messages: {
          uname: {
               required:  "@php echo (Lang::has(Session::get('lang_file').'.USER_NAME')!= '')  ?  trans(Session::get('lang_file').'.USER_NAME'): trans($OUR_LANGUAGE.'.USER_NAME'); @endphp", 
                      },
             uemail: {
               required:  "@php echo (Lang::has(Session::get('lang_file').'.USER_EMAIL')!= '')  ?  trans(Session::get('lang_file').'.USER_EMAIL'): trans($OUR_LANGUAGE.'.USER_EMAIL'); @endphp",
			   email:  "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_VALID_EMAIL')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_VALID_EMAIL'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_VALID_EMAIL'); @endphp", 
                      },  
                   country_code: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.COUNTRY_CODE_MSG')!= '')  ?  trans(Session::get('lang_file').'.COUNTRY_CODE_MSG'): trans($OUR_LANGUAGE.'.COUNTRY_CODE_MSG'); @endphp",
                      },
                 umobile: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_MOBILE')!= '')  ?  trans(Session::get('lang_file').'.USER_MOBILE'): trans($OUR_LANGUAGE.'.USER_MOBILE'); @endphp",
                      },
          upassword: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($OUR_LANGUAGE.'.USER_PASSWORD'); @endphp",
			    minlength: "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_CHARACTERS'); @endphp",
                      },
             cpassword: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_CONFIRM_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_CONFIRM_PASSWORD'): trans($OUR_LANGUAGE.'.USER_CONFIRM_PASSWORD'); @endphp",
			    minlength: "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_CHARACTERS'); @endphp",
				equalTo: "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE'): trans($OUR_LANGUAGE.'.PLEASE_ENTER_THE_SAME_VALUE'); @endphp",
                      },
                     
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 


@include('includes.footer')
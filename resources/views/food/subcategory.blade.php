@include('includes.navbar')
<div class="outer_wrapper">

@include('includes.header')
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
 @php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ @endphp
      @include('includes.searchweddingandoccasions')
      @php } @endphp
      @php if(Session::get('searchdata.mainselectedvalue')=='1'){ @endphp
      @include('includes.search')
      @php } @endphp 
</div> <!-- search-section -->

 

<div class="page-left-right-wrapper">
@include('includes.mobile-modify')





<div class="page-right-section">

 <!-- budget-menu-outer -->
<div class="form_title">{{ $foodmaincategory[0]->mc_name }}</div>
@php if(count($foodshopunderbugetincity) > 0 ){ @endphp
<div class="diamond-area">
<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
		<div class="diamond_wrapper_main">
			@php if($category_id==10){ $foodshop='datesshop'; }else{ $foodshop='foodshopbranch'; } @endphp
			@php  $i=1; $x=0;  @endphp
			@php  $k=count($foodshopunderbugetincity);  @endphp
			@php if($k<6){ @endphp
			<div class="diamond_wrapper_inner">
			@foreach($foodshopunderbugetincity as $getallcats)
			@php if($foodshopunderbugetincity->ischild[$x]>0){ $redirurl=url('')."/".$foodshop."/".$subcategory_id."/".$category_id."/".$getallcats->mc_id; }else{ $redirurl="#";}

			if($k<=6)
                  {
                  $bgImg = str_replace('thumb_','',$getallcats->mc_img);  
                  }
                  else
                  {
                  $bgImg = $getallcats->mc_img;  
                  }


			 @endphp
									<div class="row_{{$i}}of{{$k}} rows{{$k}}row">
					  <a href="{{$redirurl}}" @php if($foodshopunderbugetincity->ischild[$x]==0){ @endphp onclick="noproductdata();" @php } @endphp >
						<div class="category_wrapper" style="background:url({{ $bgImg or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div></div>
						</div>
					</a>
				</div>
				 @php $i=$i+1; $x++; @endphp
				@endforeach
				</div> 
				<!------------ 6th-------------->
				@php }elseif($k==6){ @endphp
				@php $j=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($foodshopunderbugetincity as $getallcats)
			@php if($foodshopunderbugetincity->ischild[$x]>0){ $redirurl=url('')."/".$foodshop."/".$subcategory_id."/".$category_id."/".$getallcats->mc_id; }else{ $redirurl="#";} @endphp
			 @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
			 @php if($j==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($j==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($j==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($j==5){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($j==6){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					<a href="{{$redirurl}}" @php if($foodshopunderbugetincity->ischild[$x]==0){ @endphp onclick="noproductdata();" @php } @endphp >
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->mc_img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div></div>
						</div>
					</a>
			@php if($j==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($j==4){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==5){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($j==6){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $j=$j+1; $x++; @endphp
				@endforeach
				</div>
				<!------------ 7th-------------->
				@php }elseif($k==7){ @endphp
				@php $l=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($foodshopunderbugetincity as $getallcats)
				@php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
			@php if($foodshopunderbugetincity->ischild[$x]>0){ $redirurl=url('')."/".$foodshop."/".$subcategory_id."/".$category_id."/".$getallcats->mc_id; }else{ $redirurl="#";} @endphp
			 @php if($l==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($l==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($l==7){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					  <a href="{{$redirurl}}" @php if($foodshopunderbugetincity->ischild[$x]==0){ @endphp onclick="noproductdata();" @php } @endphp >
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->mc_img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==6){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; $x++; @endphp
				@endforeach
				</div>
				<!------------ 8th-------------->
				@php }elseif($k==8){ @endphp
				@php $l=1; @endphp
			
					<div class="diamond_wrapper_inner">
					@foreach($foodshopunderbugetincity as $getallcats)
					 @php if($foodshopunderbugetincity->ischild[$x]>0){ $redirurl=url('')."/".$foodshop."/".$subcategory_id."/".$category_id."/".$getallcats->mc_id; }else{ $redirurl="#";} @endphp

				@php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
				@php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
				@php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
				
			 @php if($l==1){ $classrd='category_wrapper1'; @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_3of5 rows5row"> @php } @endphp 
			 @php if($l==4){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_3of5 rows5row"> @php } @endphp
			  @php if($l==8){ $classrd='category_wrapper9'; @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					  <a href="{{$redirurl}}" @php if($foodshopunderbugetincity->ischild[$x]==0){ @endphp onclick="noproductdata();" @php } @endphp >
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->mc_img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==8){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; $x++; @endphp
				@endforeach
				</div>
				<!---------- 9th ------------------->
		@php }elseif($k>=9){ @endphp
		
		
					<div class="diamond_wrapper_inner">
	  			@php $i=1;  @endphp
		  @foreach($foodshopunderbugetincity as $getallcats)

		  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
		  @php if($foodshopunderbugetincity->ischild[$x]>0){ $redirurl=url('')."/".$foodshop."/".$subcategory_id."/".$category_id."/".$getallcats->mc_id; }else{ $redirurl="#";} @endphp

		 @php if($i==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		  @php if($i==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
		   @php if($i==4){ @endphp <div class="row_3of5 rows5row"> @php } @endphp 
		    @php if($i==7){ @endphp  <div class="row_4of5 rows5row"> @php } @endphp 
			@php if($i==9){ @endphp  <div class="row_5of5 rows5row"> @php } @endphp 
            <a href="{{$redirurl}}"  @php if($foodshopunderbugetincity->ischild[$x]==0){ @endphp onclick="noproductdata();" @php } @endphp >
              <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->mc_img or '' }});">
                <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span></span>
              </span>
            </a>
		 @php if($i==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($i==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($i==6){ @endphp <div class="clear"></div> </div> @php } @endphp 
		    @php if($i==8){ @endphp <div class="clear"></div></div> @php } @endphp 
			@php if($i==9){ @endphp  <div class="clear"></div> </div> @php } @endphp 
		 
		    @php $i=$i+1; $x++; @endphp
		   @endforeach 
	  
		  
        </div>
		
		
		
				
			@php } @endphp
				 	
				
		</div> 
	  </div>
	 
  </div>
 <div class="diamond_shadow">{{ $foodshopunderbugetincity->links() }}</span></div>  
<div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
@php }else{ @endphp
<div class="no-record-area" align="center">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
@php } @endphp
</div>











    <!-- budget-carousel-area -->


</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')


  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>


<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content">@if (Lang::has(Session::get('lang_file').'.No_product_found_in_this_shop')!= '') {{  trans(Session::get('lang_file').'.No_product_found_in_this_shop') }} @else  {{ trans($OUR_LANGUAGE.'.No_product_found_in_this_shop') }} @endif</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/>
      <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>
  </div>
</div>
  <script type="text/javascript">
function noproductdata()
{
 jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
}
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
});
</script>

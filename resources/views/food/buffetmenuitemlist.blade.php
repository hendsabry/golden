<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 
<title>@if (Lang::has(Session::get('lang_file').'.Golden_Cages')!= '') {{  trans(Session::get('lang_file').'.Golden_Cages') }} @else  {{ trans($OUR_LANGUAGE.'.Golden_Cages') }} @endif</title>
<link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/themes/images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="{{url('/')}}/themes/css/reset.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/common-style.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/stylesheet.css" rel="stylesheet" />


<link href="{{url('/')}}/themes/slider/css/demo.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/slider/css/flexslider.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/user-my-account.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface-media.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/diamond.css" rel="stylesheet" />
<!-- custome scroll 
<link href="mousewheel/jquery.mCustomScrollbar.css" rel="stylesheet" />
-->
<!--<link href="css/diamond.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />-->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script src="{{url('/')}}/themes/js/jquery.flexslider.js"></script>

<script src="{{url('/')}}/themes/js/modernizr.js"></script>
<script src="{{url('/')}}/themes/js/jquery.mousewheel.js"></script>
<script src="{{url('/')}}/themes/js/demo.js"></script>
<script src="{{url('/')}}/themes/js/froogaloop.js"></script>
<script src="{{url('/')}}/themes/js/jquery.easing.js"></script>

<!------------------ tabs----------------->

<link href="{{url('/')}}/themes/css/bootstrap_tab.css" rel="stylesheet" />
<script src="{{url('/')}}/themes/js/bootstrap_tab.js"></script>
<link href="{{url('/')}}/themes/css/arabic.css" rel="stylesheet" />

<!------------ end tabs------------------>

</head>
@php  if(Session::get('lang_file')=='ar_lang'){ $sitecss="arabic"; }else{ $sitecss=""; } @endphp
<body class="{{ $sitecss }}">
@php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
  $cururl = request()->segment(count(request()));
@endphp
<div class="popup_overlay"></div>

<div class="outer_wrapper">
<div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo">
            @php $mainshopinfo = Helper::getparentCat($mainbranchid); @endphp
          <a href="javascript:void(0);"><img src="{{ $mainshopinfo->mc_img }}" alt="" /></a></div>
      </div>
      <div class="vendor_header_right">
        <!-- vendor_header_left -->
        <div class="vendor_welc">
  @include("includes.language-changer")
          
          <div class="vendor_name username"> {{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}} <span>
            <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
            <ul class="vendor_header_navbar">
              <li><a href="{{route('my-account-profile')}}"@php if($cururl=='my-account-profile') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')}}</a></li>
              <li><a href="{{route('my-account-ocassion')}}"@php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')}}</a></li>
              <li><a href="{{route('my-account-studio')}}"@php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')}}</a></li>
              <li><a href="{{route('my-account-security')}}"@php if($cururl=='my-account-security') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')}}</a></li>
              <li><a href="{{route('my-account-wallet')}}"@php if($cururl=='my-account-wallet') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')}}</a></li>
              <li><a href="{{route('my-account-review')}}"@php if($cururl=='my-account-review') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')}}</a></li>
              <li><a href="{{route('my-request-a-quote')}}"@php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')}}</a></li>
              <li><a href="{{route('change-password')}}"@php if($cururl=='change-password') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}</a></li>
              <li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li>
            </ul>
            </span></div>
          @if (Session::get('customerdata.token')!='')
          @php $getcartnoitems = Helper::getNumberOfcart(); @endphp
          @if($getcartnoitems>0) <a href="{{url('mycart')}}" class="vendor_cart"><img src="{{ url('') }}/themes/images/basket.png" /><span>{{ $getcartnoitems }}</span></a> @endif
          @endif </div>
        <!-- vendor_header_right -->
        @php if(count($otherbarnch)>1){ @endphp
        <div class="select_catg">

          <div class="select_lbl">@if (Lang::has(Session::get('lang_file').'.Other_Branches')!= '') {{  trans(Session::get('lang_file').'.Other_Branches') }} @else  {{ trans($OUR_LANGUAGE.'.Other_Branches') }} @endif</div>
          <div class="search-box-field">
            <select class="select_drp" id="dynamic_select">
              <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Branch')!= '') {{  trans(Session::get('lang_file').'.Select_Branch') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Branch') }} @endif</option>           
                        @foreach($otherbarnch as $otherbarnches)
					  <option value="{{url('')}}/foodshopbuffetmenu/{{$rids}}/{{ $otherbarnches->mc_id }}"> {{ $otherbarnches->mc_name }}  </option>

					 @endforeach
            </select>
          </div>
        </div>
        @php } @endphp </div>
    </div>
  </div><!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">
       <ul>
        <li><a href="#about_shop" class="active">@if (Lang::has(Session::get('lang_file').'.About_Shop')!= '') {{  trans(Session::get('lang_file').'.About_Shop') }} @else  {{ trans($OUR_LANGUAGE.'.About_Shop') }} @endif</a></li>
        <li><a href="#video">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif</a></li>
		 @php if(count($fooddateshopreview)>0){ @endphp 
        <li><a href="#our_client">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</a></li>
		@php } @endphp
        <li><a href="#choose_package">@if (Lang::has(Session::get('lang_file').'.Food')!= '') {{  trans(Session::get('lang_file').'.Food') }} @else  {{ trans($OUR_LANGUAGE.'.Food') }} @endif</a></li>
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->
<div class="inner_wrap service-wrap diamond_space">
<div class="detail_page">	
<a name="about_shop" class="linking">&nbsp;</a>

    <div class="service_detail_row">
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
		  @foreach($fooddateshopgallery as $shopgallery)
@php $Gal = str_replace('thumb_','',$shopgallery->image); @endphp

             <li>
  	    	    <img src="{{ $Gal }}" />
  	    		</li>
			@endforeach
  	    		 	    		
          </ul>
        </div> 
        		<div id="carousel" class="flexslider">
          <ul class="slides">
           @foreach($fooddateshopgallery as $shopgallerythumb)
             <li>
  	    	    <img src="{{ $shopgallerythumb->image }}" />
  	    		</li>
			@endforeach
  	    		
           
          </ul>
        </div>
      		</section>
            
        </div>
        
        <div class="service_detail">
        	@if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
        	<div class="detail_title">{{ $fooddateshopdetails[0]->mc_name }}</div>
            <div class="detail_hall_description">{{ $fooddateshopdetails[0]->address }}</div>
            <div class="detail_hall_subtitle">@if (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '') {{  trans(Session::get('lang_file').'.ABOUT_SHOP') }} @else  {{ trans($OUR_LANGUAGE.'.ABOUT_SHOP') }} @endif</div>
            <div class="detail_about_hall"> <div class="comment more">{{ $fooddateshopdetails[0]->mc_discription }}</div></div>
             
              <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: <span><?php
		    $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
		   	{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?></span></div>
   
  @php if($fooddateshopdetails[0]->latitude!='' && $fooddateshopdetails[0]->longitude!=''){  $lat=$fooddateshopdetails[0]->latitude;   $long=$fooddateshopdetails[0]->longitude;    @endphp


          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          @php }  @endphp
             
        </div> 
        
    </div> <!-- service_detail_row -->


    <div class="service-mid-wrapper">
		         	<a name="video" class="linking">&nbsp;</a>
        
        <div class="service-video-area">
          <div class="service-video-cont">{{ $fooddateshopdetails[0]->mc_video_description }}</div>
          <div class="service-video-box">
		  
		  
            <iframe class="service-video" src="{{ $fooddateshopdetails[0]->mc_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <!-- service-video-area -->
        
      @php if(count($fooddateshopreview)>0){ @endphp   
           
           <div class="service_list_row service_testimonial">
        	<a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                          <ul class="slides">
						  @foreach($fooddateshopreview as $customerreview)
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="{{ $customerreview->cus_pic }}"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description">{{ $customerreview->comments }}</div>
                                    	<div class="testim_name">{{ $customerreview->cus_name }}</div>
                                        <div class="testim_star"><img src="{{url('/')}}/themes/images/star{{ round($customerreview->ratings) }}.png"></div>
                                    </div>
                                </div>
                            </li>
					 @endforeach
                           
                          </ul>
                        </div>
            </section>
          </div>
        </div>    
		
		@php } @endphp
		
          </div>


</div>


</div>













<a name="choose_package" class="linking">&nbsp;</a>

<div class="inner_wrap">

<!-- common_navbar -->

<div class="vendor_homepage">

<div class="heading">{{ (Lang::has(Session::get('lang_file').'.Internal_Food')!= '')  ?  trans(Session::get('lang_file').'.Internal_Food'): trans($OUR_LANGUAGE.'.Internal_Food')}}</div>

<div class="vendor_navbar">
	<ul>
	@php $k=1; $lkj=0; 
  

  @endphp
		  @foreach($mainmenuwithItemAndContainer as $mainmenu)
       
		  @php 

      if(count($mainmenuwithItemAndContainer[$lkj]['food_list'])>0){
      if($k==1){ $addact='class="active"'; }else{ $addact=''; } @endphp 

    	<li {{ $addact }}><a href="#{{$k}}" class="venor_active" data-toggle="tab">{{ $mainmenu->attribute_title}} </a></li>
		@php } $k++; 

    $lkj++; @endphp
		 @endforeach
      
    </ul>
</div>

 {!! Form::open(['url' => 'foodshopbuffetmenu/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}


<div class="vendor_container">





<div class="left_container">
 <div class="tab-content">
 	@php $j=1; $l=0; @endphp
@foreach($mainmenuwithItemAndContainer as $mainmenucatgeoryloop)
@php $isaddable=''; @endphp
 @php if($j==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp 
<div id="{{ $j }}" class="tab-pane fade {{ $addactcon }}">
	<Div class="subheading">{{ $mainmenucatgeoryloop->attribute_title}}</Div>

	<div class="table desktp">
     	<div class="tr">
          <div class="table_heading th1">{{ (Lang::has(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE')!= '')  ?  trans(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE'): trans($OUR_LANGUAGE.'.DISH_NAME_CONTAINER_TYPE')}}</div>
          <div class="table_heading th2">{{ (Lang::has(Session::get('lang_file').'.TOTAL_QUALITY')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_QUALITY'): trans($OUR_LANGUAGE.'.TOTAL_QUALITY')}}</div>
          <div class="table_heading th3">{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}</div>
          <div class="table_heading th4"></div>
          
        </div>
	</div>
 <div class="content mCustomScrollbar fds">
   
    <div class="table">
     @php $z=1;  @endphp
    @foreach($mainmenuwithItemAndContainer[$l]['food_list'] as $menudish)   
   
	 <div class="tr">  

	 	<div class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE')!= '')  ?  trans(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE'): trans($OUR_LANGUAGE.'.DISH_NAME_CONTAINER_TYPE')}}">
   <div class="item_title">{{ $menudish->pro_title}} </div>
   <div class="menu_internal_img"><img src="{{ $menudish->pro_Img}}" width="70px;"></div>
        <div class="item_listing ">
		@php $jk=1; @endphp
		 	@foreach($menudish->container_list as $dishcontainer)   
				@php if(count($dishcontainer->container_price)>0){ $isaddable=1;				
				if($jk==1) { $sclass='';  }else{ $sclass=''; }
				$containerid=$dishcontainer->container_price[0]->id;
				$containername=$dishcontainer->container_price[0]->short_name;
        if($dishcontainer->discount_price==0 || $dishcontainer->discount_price==null){ $containernewprice= $dishcontainer->price; }else{ $containernewprice= $dishcontainer->discount_price; }
				$newcontainerprice=number_format($containernewprice,2,'.','');
				 $newcontainerprice=currency($newcontainerprice, 'SAR',$Current_Currency, $format = false);

         $containerrealprice=number_format($dishcontainer->price,2,'.','');
         $realcontainerprice=currency($containerrealprice, 'SAR',$Current_Currency, $format = false);
				@endphp 
				<div class="rigt_cont_tick">
				<a href="#" id="{{ $jk }}{{ $menudish->pro_id }}" onClick=" return selectcontainer('basecontainerprice{{ $z}}{{ $menudish->pro_id }}','{{ $menudish->pro_id }}','{{ $containerid }}','{{ $newcontainerprice }}','{{ $jk }}','{{ $containername }}','{{ $realcontainerprice }}','{{ Session::get('currency') }}');" class="containerlisting {{ $sclass }}">{{ $containername }} </a>
				<div class="right_click" id="rc{{ $containerid }}{{ $menudish->pro_id }}" style="display: none;"><img src="{{url('/')}}/themes/images/right-check.png" alt="" /></div>
       
				</div>
				
				@php $jk++; }else{ $isaddable=0; } @endphp
				
			@endforeach 	
        <!--    <a href="#">B</a>
            <a href="#">C</a>-->
        </div>
        
        <span id="error{{ $menudish->pro_id }}" class="error"></span>
     </div>
	    <div class="td td2 quantity" data-title="{{ (Lang::has(Session::get('lang_file').'.TOTAL_QUALITY')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_QUALITY'): trans($OUR_LANGUAGE.'.TOTAL_QUALITY')}}">
		<div class="quantity">
        <button type="button" id="sub" class="sub" onClick="return pricecalculation('{{ $menudish->pro_id }}','basecontainerprice{{ $z}}{{ $menudish->pro_id }}','remove');"></button>
		<input type="number" name="itemqty[]" id="qty{{ $menudish->pro_id }}" value="1" min="1" max="9" onKeyUp="isNumberKey(event); pricecalculation('{{ $menudish->pro_id }}','basecontainerprice{{ $z}}{{ $menudish->pro_id }}','pricewithqty');"/>
		<button type="button" id="add" class="add" onClick="return pricecalculation('{{ $menudish->pro_id }}','basecontainerprice{{ $z}}{{ $menudish->pro_id }}','add');"></button></div>
	   </div>
	    
        <div class="td td4 catg_price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">
            <div class="dis" style="color:#d3bbba;">{{ Session::get('currency') }}
                <span id="basecontainerprice{{ $z }}{{ $menudish->pro_id }}">0.00</span>
            </div>
            <span id="containerrealdisprice{{ $menudish->pro_id }}"></span>
		
		<!----------------- Menu Info--------------->
     <input type="hidden" name="containerid" id="containerid{{ $menudish->pro_id }}" value="">
     <input type="hidden" name="itemid" id="itemid{{ $menudish->pro_id }}" value="">

		<input type="hidden" name="mainmenuid[]" id="menuid{{ $menudish->pro_id }}" value="{{ $mainmenucatgeoryloop-> id }}">
		<input type="hidden" name="menuname[]" id="menuname{{ $menudish->pro_id }}" value="{{ $mainmenucatgeoryloop->attribute_title }}">
		
		<!----------------- Menu Dish Info--------------->
		<input type="hidden" name="dishid[]" id="dishid{{ $menudish->pro_id }}" value="{{ $menudish->pro_id }}">
		<input type="hidden" name="dishname[]" id="dishname{{ $menudish->pro_id }}" value="{{ $menudish->pro_title}}">
		<!-------------- container info ------------------------>
    

		<input type="hidden" name="selectedcontainerid[]" id="selectedcontainerid{{ $menudish->pro_id }}" value="">
		<input type="hidden" name="selectedcontainername[]" id="selectedcontainername{{ $menudish->pro_id }}" value="">
		<input type="hidden" name="selectedsinglecontainername[]" id="selectedsinglecontainername{{ $menudish->pro_id }}" value="">
		
		<input type="hidden" name="selectedcontainerprice[]" id="selectedcontainerprice{{ $menudish->pro_id }}" value="0.00">
		<input type="hidden" name="dishcontainerbaseprice[]" id="dishcontainerbaseprice{{ $menudish->pro_id }}" value="0.00">
		
		<input type="hidden" name="addedmainmenuid[]" id="addedmainmenuid{{ $menudish->pro_id }}" value="">
		<input type="hidden" name="addeddishid[]" id="addeddishid{{ $menudish->pro_id }}" value="">
		<input type="hidden" name="addeddishcontainerid[]" id="addeddishcontainerid{{ $menudish->pro_id }}" value="">
		<input type="hidden" name="addeddishcontainerqty[]" id="addeddishcontainerqty{{ $menudish->pro_id }}" value="">
		
		</div>
		<div class="td td3 add_categ" data-title="">
		@php if($isaddable==1){ @endphp
		<a href="#" onClick="return addtocart('{{ $menudish->pro_id }}');">{{ (Lang::has(Session::get('lang_file').'.ADD')!= '')  ?  trans(Session::get('lang_file').'.ADD'): trans($OUR_LANGUAGE.'.ADD')}}</a>
		@php }else{ @endphp
			
		@php } @endphp
		
		</div>
	 </div>
	 <span id="alreadyadded{{ $menudish->pro_id }}" class="error itemadded"></span>
	 @php $z++; $basecontainerprice='0.00'; @endphp
	@endforeach 
	 
	 

          	
	</div><!-- table -->
     
	</div>
    
 
    
</div><!-- left_container -->
	@php $j++; $l++; @endphp
@endforeach
 

</div>

</div> 

<div class="right_container">
<Div class="subheading">{{ (Lang::has(Session::get('lang_file').'.CONTAINER_LIST')!= '')  ?  trans(Session::get('lang_file').'.CONTAINER_LIST'): trans($OUR_LANGUAGE.'.CONTAINER_LIST')}}</Div>

<div class="service_inner_wrap">
	
	@foreach($barnchcontainerlist as $allcontainerlist)
	@php if($allcontainerlist->no_person < 1){ continue;} @endphp
		<div class="container_list_row">
    	<div class="food_container_listimg"><img src="{{ $allcontainerlist->image }}"></div>
        <div class="food_container_txt">
        	
            <span>{{ (Lang::has(Session::get('lang_file').'.NO_OF_PEOPLE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_PEOPLE'): trans($OUR_LANGUAGE.'.NO_OF_PEOPLE')}} {{ $allcontainerlist->no_person }}</span>
        </div>
        <div class="fodd_cont_catg">{{ $allcontainerlist->short_name }}</div>
    </div>
   @endforeach 

</div><!-- service_inner_wrap -->

</div><!-- right_container -->

<a name="order_summary1" class="linking">&nbsp;</a>

<div class="order_summary_bar" id="order_summary_bar" style="display: none;">
<Div class="subheading">{{ (Lang::has(Session::get('lang_file').'.ORDER_SUMMARY')!= '')  ?  trans(Session::get('lang_file').'.ORDER_SUMMARY'): trans($OUR_LANGUAGE.'.ORDER_SUMMARY')}}</Div>

<div class="order_sum_list" id="myContainer">


</div>

<div class="total_food_cost">
	@php $basetotal="0.00"; @endphp
	<div class="total_price">{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: <span>{{ Session::get('currency') }} <span id="pricetotalamount">{{ number_format($basetotal,2,'.','') }}</span></span></div>
</div>    

<div class="btn_row">
<input type="hidden" name="totalprice" id="totalprice" value="{{ $basetotal }}">
<input type="hidden" name="cartintemid[]" id="cartintemid" value="0">
<input type="hidden" name="foodqty[]" id="foodqty" value="0">
<input type="hidden" name="cart_type" id="cart_type" value="food">
<input type="hidden" name="product_id" id="product_id" value="{{ $productid }}">
<input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" class="form-btn addto_cartbtn"></div>

   
   <div class="terms_conditions"><a  href="{{ $fooddateshopdetails[0]->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}</a></div>

</div> <!-- order_summary_bar -->


</div><!-- vendor_container -->
{!! Form::close() !!}
</div> <!-- vendor_homepage -->


<!-- service_container -->

 @include('includes.other_services')
 
 <!-- other_serviceinc -->



</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')

<script language="javascript">
var totalprice = 0;
var dishidarr=[0];
 //dishidarr.clear();
var itemquantity=[];
var seldishid=[];
function addtocart(menudishid){
	var totalamount=0;
	$('#order_summary_bar').css('display','');
	var menuid=document.getElementById('menuid'+menudishid).value;
	var menuname=document.getElementById('menuname'+menudishid).value;
	var dishid=document.getElementById('dishid'+menudishid).value;
	var dishname=document.getElementById('dishname'+menudishid).value;
	var selectedcontainerid=document.getElementById('selectedcontainerid'+menudishid).value;
	var selectedcontainername=document.getElementById('selectedcontainername'+menudishid).value;
	var selectedsinglecontainername=document.getElementById('selectedsinglecontainername'+menudishid).value;
  
  var containersectedid = selectedcontainerid.split(",");

  var c= parseInt(containersectedid.length) - 1;
  var cid=containersectedid[c];
    $('#rc'+cid+''+menudishid).css('display','block');

	var itemqty=document.getElementById('qty'+menudishid).value;
	 itemquantity.push(itemqty);

   var seleldishid=document.getElementById('dishid'+menudishid).value;
   seldishid.push(seleldishid);


   var cid=document.getElementById('containerid'+menudishid).value;
    var itid=document.getElementById('itemid'+menudishid).value;

	//var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
	var selectedcontainerprices=document.getElementById('dishcontainerbaseprice'+menudishid).value;
	var selectedcontainerprice= itemqty * selectedcontainerprices;
  
  

	var dishcontainerbaseprice=document.getElementById('dishcontainerbaseprice'+menudishid).value;
	var totalprice=document.getElementById('totalprice').value;
	
	 var totalamount=parseInt(totalprice)+parseInt(selectedcontainerprice);
	 document.getElementById('totalprice').value=totalamount.toFixed(2);
	 var isalreadyadded=dishidarr.includes(dishid);
	 /*if(isalreadyadded===false)
	 		{*/
				 dishidarr.push(dishid);				
				 let ndisharr=dishidarr;
				 dishidarr.toString();
				 
				 document.getElementById('cartintemid').value=dishidarr;
			
				 //alert(totalamount);
        // var dtotalamount=totalamount;
					//document.getElementById('pricetotalamount').innerHTML = dtotalamount;
							if(selectedcontainerprice==0.00)
							{
							document.getElementById('error'+dishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CONTAINER_FIRST') }} @endif';
							document.getElementById('qty'+dishid).value = 1;
							}
						
							else
							
							{
							  var div = document.createElement('div');
							  jQuery('.'+ menudishid + '_'+selectedsinglecontainername).remove();
							div.className = 'order_sumrow'+ ' '+ menudishid + '_'+selectedsinglecontainername;


						 
<?php $Curr = Session::get('currency');?>

							var newslprice=selectedcontainerprice;
								div.innerHTML ='<div class="order_sum_title">'+dishname+'<input type="hidden" name="menuprice" class="disp" value="'+newslprice+'"><input type="hidden" name="menudishid[]" class="disp7" value="'+dishid+'"><input type="hidden" name="newcontainerid[]" class="disp7" value="'+cid+'"><input type="hidden" name="newdishitemid[]" class="disp7" value="'+itid+'"><input type="hidden" name="menudishqty[]" class="disp7" value="'+itemqty+'"><input type="hidden" name="containerid[]" class="proidkk" value="'+selectedcontainerid+'"></div><div class="order_subcol2">'+selectedsinglecontainername+' x '+itemqty+'</div><div class="order_subcol2 buffet-right"><?php echo $Curr; ?> '+newslprice+'</div><input class="buffet-close" type="button" value="X" onclick="removeRow(this,'+menudishid+','+ndisharr+')">';



							 		
								document.getElementById('myContainer').appendChild(div);
								document.getElementById('addedmainmenuid'+menudishid).value=menuid;
								document.getElementById('addeddishid'+menudishid).value=menudishid;

                var menuval = jQuery('#addeddishcontainerid'+menudishid).val();

              //if (menuval.indexOf("1210") >= 0){ alert(); }
 
  document.getElementById('addeddishcontainerid'+menudishid).value=selectedcontainerid;
  

								 
								document.getElementById('addeddishcontainerqty'+menudishid).value=itemquantity;

                document.getElementById('dishid'+menudishid).value=seldishid;








								 
							}
				
			/*}else{
						document.getElementById('alreadyadded'+menudishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.THIS_ITEM_ALREADY_ADDED_IN_CART')!= '') {{  trans(Session::get('lang_file').'.THIS_ITEM_ALREADY_ADDED_IN_CART') }} @else  {{ trans($MER_OUR_LANGUAGE.'.THIS_ITEM_ALREADY_ADDED_IN_CART') }} @endif';
			}*/


 



    		var totalP = 0;
			jQuery( ".disp" ).each(function() {
			getP =  $(this).val();
			totalP = parseFloat(totalP) + parseFloat(getP);
			}); 
			totalP = parseFloat(totalP).toFixed(2); 
			jQuery('#pricetotalamount').html(totalP);

}


function removeRow(input,menudishid,disharr) {

 
  var index = dishidarr.indexOf(menudishid);

	if (index > -1) {
	  array.splice(index, 1);
	}
    document.getElementById('myContainer').removeChild(input.parentNode);
	var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
	var totalprice=document.getElementById('totalprice').value;
	 var totalamount=parseInt(totalprice)- parseInt(selectedcontainerprice);
	 document.getElementById('totalprice').value=totalamount;
	 document.getElementById('pricetotalamount').innerHTML = totalamount.toFixed(2);

    var selectedcontainerid=document.getElementById('selectedcontainerid'+menudishid).value;
   var containersectedid = selectedcontainerid.split(",");

  var c= parseInt(containersectedid.length) - 1;
  var cid=containersectedid[c];
    $('#rc'+cid+''+menudishid).css('display','none');
 
	var totalP = 0;
	jQuery( ".disp" ).each(function() {
	getP =  jQuery(this).val();
	totalP = parseFloat(totalP) + parseFloat(getP);
	}); 
	totalP = parseFloat(totalP).toFixed(2); 
	jQuery('#pricetotalamount').html(totalP);

var mhjhj='';
  jQuery( ".proidkk" ).each(function() {
  getPv =  jQuery(this).val();
  mhjhj = getPv+','+mhjhj;
  }); 
  



  jQuery('#addeddishcontainerid49').val(mhjhj);




}

</script>




<script language="javascript">

$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});

</script>



<script language="javascript">
function pricecalculation(dishid,k,act){
	//alert(dishid);
		var selectedcontainerprice=document.getElementById('dishcontainerbaseprice'+dishid).value;
					if(selectedcontainerprice==0.00)
					{
					document.getElementById('error'+dishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CONTAINER_FIRST') }} @endif';
					document.getElementById('qty'+dishid).value = 1;
					}
				
				else
				
				{
				
		 		var did='qty'+dishid;
				var no=1;
				var currentquantity=document.getElementById(did).value;


      				if(currentquantity<1){
                document.getElementById(did).value=1;
                 var qty= parseInt(no);
              }else{
              if(act=='pricewithqty'){
                var qty=parseInt(currentquantity)
              }
              if(act=='add'){
                   var qty= parseInt(currentquantity)+parseInt(no);
                 }
              if(act=='remove'){ 
                  if(parseInt(currentquantity)==1){
                      var qty=parseInt(currentquantity)
                   }else{
                      var qty=parseInt(currentquantity)-parseInt(no);
                  }

               }
              }



				var finalprice=qty*selectedcontainerprice;

				var inputboxcontanerid='selectedcontainerprice'+dishid;
				document.getElementById(k).innerHTML = finalprice.toFixed(2);
				document.getElementById(inputboxcontanerid).value = finalprice;
			}
}

</script>

<script language="javascript">
	var cid= [];
	var cname=[];
	var cprice=[];
function selectcontainer(k,dishid,containerid,containerprice,z,containername,realcontainerbaseprice,currs)
		{
					
					cid.push(containerid);
				cname.push(containername);
				cprice.push(containerprice);

					jQuery('.containerlisting').removeClass('listing_active');

					var classid=''+z+dishid;
					var inputboxcontanerid='selectedcontainerprice'+dishid;
					var dishcontainerbaseprice='dishcontainerbaseprice'+dishid;
					var selectedcontainername='selectedcontainername'+dishid;
					var selectedcontainerid='selectedcontainerid'+dishid;
					var selectedsinglecontainername='selectedsinglecontainername'+dishid;
          var containerrealdisprice='containerrealdisprice'+dishid;
            var containerrealselctedid='containerid'+dishid;
          var selcteditemid='itemid'+dishid;
            
         
					var i;
						for (i = 1; i < z; i++) { 
						var rid=''+i+dishid;
							document.getElementById(rid).classList.remove("listing_active");
						}
				document.getElementById(selcteditemid).value = dishid;
				document.getElementById(selectedcontainername).value = cname;
				document.getElementById(selectedcontainerid).value = cid;

        document.getElementById(containerrealselctedid).value = containerid;

				document.getElementById(selectedsinglecontainername).value = containername;

						document.getElementById(classid).classList.add("listing_active");
            if(containerprice!=realcontainerbaseprice){
              var displayprice='<del>'+currs+' '+realcontainerbaseprice+'</del><br>'+currs+' '+containerprice;
            }else{
              var displayprice=currs+' '+containerprice;
            }
            document.getElementById(containerrealdisprice).innerHTML = displayprice;
					document.getElementById(k).innerHTML = containerprice;
					document.getElementById(inputboxcontanerid).value = cprice;
					document.getElementById(dishcontainerbaseprice).value = containerprice;
					document.getElementById('error'+dishid).innerHTML='';
					document.getElementById('qty'+dishid).value = 1;
		}

</script>

<script>

      // bind change event to select
      jQuery('#dynamic_select').on('change', function () {
      
          var url = jQuery(this).val(); // get selected value          
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
  
</script>
<script language="javascript">
  function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
            return false;
        } else {
            return true;
        }
    }
</script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function() {
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });
});
</script>
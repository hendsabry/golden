@include('includes.navbar')
<div class="outer_wrapper diamond_fullwidth"> @include('includes.header')
  <div class="inner_wrap">
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
       @php 
	  
      if(Session::get('searchdata.mainselectedvalue')=='2'){ @endphp
      @include('includes.searchweddingandoccasions')
      @php } @endphp
      @php if(Session::get('searchdata.mainselectedvalue')=='1'){ @endphp
      @include('includes.search')
      @php } @endphp  </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper"> @include('includes.mobile-modify')
      <div class="page-right-section">
     @if(Session::has('message')) 
     <div class="message">
{{ (Lang::has(Session::get('lang_file').'.external_food_message')!= '')  ?  trans(Session::get('lang_file').'.external_food_message'): trans($OUR_LANGUAGE.'.external_food_message')}}
</div>

              @endif
        <div class="diamond_main_wrapper"> @if(Session::get('lang_file')!='en_lang') <img src="{{ url('') }}/themes/images/food_ar.jpg" alt="" usemap="#hall-subcategory" hidefocus="true"> @else <img src="{{ url('') }}/themes/images/food.jpg" alt="" usemap="#hall-subcategory"> @endif
            <map name="hall-subcategory" id="hall-subcategory">
              <area shape="poly" coords="572,112,469,12,299,178,292,188,171,307,768,310" href="{{ url('') }}/foodsubcategorybuffets/7/8" />
              <area shape="poly" coords="776,616,166,614,280,732,290,741,384,833,470,920" href="{{ url('') }}/foodsubcategorydates/7/10" />
              <area shape="poly" coords="773,314,166,312,13,463,163,611,777,613,923,464" href="{{ url('') }}/foodsubcategorydesserts/7/9" />
            </map>
          </div>
        <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 
<title>@if (Lang::has(Session::get('lang_file').'.Golden_Cages')!= '') {{  trans(Session::get('lang_file').'.Golden_Cages') }} @else  {{ trans($OUR_LANGUAGE.'.Golden_Cages') }} @endif</title>
<link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/themes/images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="{{url('/')}}/themes/css/reset.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/common-style.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/stylesheet.css" rel="stylesheet" />


<link href="{{url('/')}}/themes/slider/css/demo.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/slider/css/flexslider.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/user-my-account.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface-media.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/arabic.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/diamond.css" rel="stylesheet" />
<!-- custome scroll 
<link href="mousewheel/jquery.mCustomScrollbar.css" rel="stylesheet" />
-->
<!--<link href="css/diamond.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />-->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script src="{{url('/')}}/themes/js/jquery.flexslider.js"></script>

<script src="{{url('/')}}/themes/js/modernizr.js"></script>
<script src="{{url('/')}}/themes/js/jquery.mousewheel.js"></script>
<script src="{{url('/')}}/themes/js/demo.js"></script>
<script src="{{url('/')}}/themes/js/froogaloop.js"></script>
<script src="{{url('/')}}/themes/js/jquery.easing.js"></script>

<!------------------ tabs----------------->

<link href="{{url('/')}}/themes/css/bootstrap_tab.css" rel="stylesheet" />
<script src="{{url('/')}}/themes/js/bootstrap_tab.js"></script>

<!------------ end tabs------------------>

</head>
@php  if(Session::get('lang_file')=='ar_lang'){ $sitecss="arabic"; }else{ $sitecss=""; } @endphp
<body class="{{ $sitecss }}">
@php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
@endphp
 <?php $Cur = Session::get('currency'); ?>
<div class="popup_overlay"></div>

<div class="outer_wrapper">
<div class="vendor_header">
	<div class="inner_wrap">                
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$ShopInfo[0]->mc_img}}" alt="" /></a></div>                      	
        </div> 
		<!-- vendor_header_left -->
          @include('includes.vendor_header')
        <!-- vendor_header_right -->
	</div>
</div><!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">
      <!--<ul>
        <li ><a href="#about_shop" class="active">About Shop</a></li>
        <li><a href="#video">Video</a></li>
        <li><a href="#our_client">What Our Clients Say's</a></li>
        <li><a href="#choose_package">Choose Package</a></li>
      </ul>-->
      </div>
      </div>
    </div>
    <!-- common_navbar -->

 @if(Session::has('message'))
                <div class="error internalerror">
              {{  Session::get('message') }}                
                </div>
                @endif
<div class="inner_wrap">

<!-- common_navbar -->

<div class="vendor_homepage vens">

<div class="heading">{{ (Lang::has(Session::get('lang_file').'.Internal_Food')!= '')  ?  trans(Session::get('lang_file').'.Internal_Food'): trans($OUR_LANGUAGE.'.Internal_Food')}}</div>

<div class="vendor_navbar">
	<ul>
	@php $k=1;   @endphp
		  @foreach($mainmenuitemtype as $mainmenu)
		  @php if($k==1){ $addact='class="active"'; }else{ $addact=''; } @endphp 
    	<li {{ $addact }}><a href="#{{$k}}" class="venor_active" data-toggle="tab">{{ $mainmenu->menu_name}}</a></li>
		@php $k++; @endphp
		 @endforeach
      
    </ul>
</div>

 {!! Form::open(['url' => 'additemstocart', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}


<div class="vendor_container">
 

<div class="left_container">
 <div class="tab-content">
 	@php $j=1; $l=0; @endphp
@foreach($mainmenuwithItemAndContainer as $mainmenucatgeoryloop)

 @php if($j==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp 
<div id="{{ $j }}" class="tab-pane fade {{ $addactcon }}">
	<Div class="subheading">{{ $mainmenucatgeoryloop-> menu_name }}</Div>

	<div class="table desktp">
     	<div class="tr">
          <div class="table_heading th1">{{ (Lang::has(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE')!= '')  ?  trans(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE'): trans($OUR_LANGUAGE.'.DISH_NAME_CONTAINER_TYPE')}}</div>
          <div class="table_heading th2">{{ (Lang::has(Session::get('lang_file').'.TOTAL_QUALITY')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_QUALITY'): trans($OUR_LANGUAGE.'.TOTAL_QUALITY')}}</div>
          <div class="table_heading th3">{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}</div>
          <div class="table_heading th4">&nbsp;</div>
          
        </div>
	</div>
 <div class="content mCustomScrollbar">
   
    <div class="table">
     @php $z=1; @endphp
    @foreach($mainmenuwithItemAndContainer[$l]['food_list'] as $menudish)      
	 <div class="tr">  

	 	<div class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE')!= '')  ?  trans(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE'): trans($OUR_LANGUAGE.'.DISH_NAME_CONTAINER_TYPE')}}">
   <div class="item_title">{{ $menudish->dish_name}} </div>
   <div class="menu_internal_img"><img src="{{ $menudish->dish_image}}" width="70px;"></div>
        <div class="item_listing">
		@php $jk=1; $isaddable=0; @endphp
		 	@foreach($menudish->container_list as $dishcontainer)   
				@php if(count($dishcontainer->container_price)>0){ $isaddable=1;
					$realprice=number_format($dishcontainer->container_price[0]['container_price'],2,'.','');
					$containerid=$dishcontainer->container_price[0]->id;
				if($jk==1) { $sclass=''; 
				if($dishcontainer->container_price[0]['discount_price']==0){
				$basecontainerprice=$dishcontainer->container_price[0]['container_price']; 
					}else{
				$basecontainerprice=number_format($dishcontainer->container_price[0]['discount_price'],2,'.','');
				}
			}else{ $sclass=''; }

			if($dishcontainer->container_price[0]['discount_price']==0){
				$newcontainerprice=number_format($dishcontainer->container_price[0]['container_price'],2,'.','');
			}else{
			$newcontainerprice=number_format($dishcontainer->container_price[0]['discount_price'],2,'.','');
		}
				@endphp 
				<div class="rigt_cont_tick">
				<a href="#" id="{{ $jk }}{{ $menudish->id }}" onClick="return selectcontainer('basecontainerprice{{ $z}}{{ $menudish->id }}','{{ $menudish->id }}','{{$dishcontainer['id'] }}','{{ currency($newcontainerprice, 'SAR',$Current_Currency, $format = false) }}','{{ $jk }}','{{ $dishcontainer['title'] }}','{{ currency($realprice, 'SAR',$Current_Currency, $format = false) }}','{{ $Cur }}');" class="{{ $sclass }} dishh">{{ $dishcontainer['title'] }} </a>
				<div class="right_click" id="rc{{$dishcontainer['id'] }}{{ $menudish->id }}" style="display: none;"><img src="{{url('/')}}/themes/images/right-check.png" alt="" /></div>
			</div>
				@php $jk++; } @endphp
				
			@endforeach 	
      
        </div>
        
        <span id="error{{ $menudish->id }}" class="error"></span>
     </div>
	    <div class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.TOTAL_QUALITY')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_QUALITY'): trans($OUR_LANGUAGE.'.TOTAL_QUALITY')}}">
        	<div class="quantity">
		<button type="button" id="sub" class="sub" onClick="return pricecalculation('{{ $menudish->id }}','basecontainerprice{{ $z}}{{ $menudish->id }}','remove');"></button>
		<input type="number" name="itemqty[]" id="qty{{ $menudish->id }}" value="1" min="1" max="9"  onkeyup="isNumberKey(event); pricecalculation('{{ $menudish->id }}','basecontainerprice{{ $z}}{{ $menudish->id }}','pricewithqty');"/>
		<button type="button" id="add" class="add" onClick="return pricecalculation('{{ $menudish->id }}','basecontainerprice{{ $z}}{{ $menudish->id }}','add');"></button>
		</div>
		</div>
		
        <div class="td td4 catg_price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}"><?php echo $Cur; ?> <span id="basecontainerprice{{ $z }}{{ $menudish->id }}">0.00</span><br><span id="dis{{ $menudish->id }}" ></span>

		
		<!----------------- Menu Info--------------->
		<input type="hidden" name="mainmenuid[]" id="menuid{{ $menudish->id }}" value="{{ $mainmenucatgeoryloop-> id }}">
		<input type="hidden" name="menuname[]" id="menuname{{ $menudish->id }}" value="{{ $mainmenucatgeoryloop-> menu_name }}">
		
		<!----------------- Menu Dish Info--------------->
		<input type="hidden" name="dishid[]" id="dishid{{ $menudish->id }}" value="{{ $menudish->id }}">
		<input type="hidden" name="dishname[]" id="dishname{{ $menudish->id }}" value="{{ $menudish->dish_name}}">
		<!-------------- container info ------------------------>
		<input type="hidden" name="selectedcontainerid[]" id="selectedcontainerid{{ $menudish->id }}" value="">
		<input type="hidden" name="currselectedcontainerid[]" id="currselectedcontainerid{{ $menudish->id }}" value="">


		<input type="hidden" name="selectedcontainername[]" id="selectedcontainername{{ $menudish->id }}" value="">
		<input type="hidden" name="selectedsinglecontainername[]" id="selectedsinglecontainername{{ $menudish->id }}" value="">
		
		
		<input type="hidden" name="selectedcontainerprice[]" id="selectedcontainerprice{{ $menudish->id }}" value="0.00">
		<input type="hidden" name="dishcontainerbaseprice[]" id="dishcontainerbaseprice{{ $menudish->id }}" value="0.00">
		
		<input type="hidden" name="addedmainmenuid[]" id="addedmainmenuid{{ $menudish->id }}" value="">
		<input type="hidden" name="addeddishid[]" id="addeddishid{{ $menudish->id }}" value="">
		<input type="hidden" name="addeddishcontainerid[]" id="addeddishcontainerid{{ $menudish->id }}" value="">
		<input type="hidden" name="addeddishcontainerqty[]" id="addeddishcontainerqty{{ $menudish->id }}" value="">
		<input type="hidden" name="product_id" id="product_id" value="">
		</div>
		
	    <div class="td td3 add_categ" data-title="">
		@php if($isaddable==1){ @endphp
		<a href="#" onClick="return addtocart('{{ $menudish->id }}');">{{ (Lang::has(Session::get('lang_file').'.ADD')!= '')  ?  trans(Session::get('lang_file').'.ADD'): trans($OUR_LANGUAGE.'.ADD')}}</a>
		@php }else{ @endphp
			
		@php } @endphp
		
		</div>
	 </div>
	 <span id="alreadyadded{{ $menudish->id }}" class="error itemadded"></span>
	 @php $z++; $basecontainerprice='0.00'; @endphp
	@endforeach 
	 
	 

          	
	</div><!-- table -->
    
	
	
	
	
	</div>
    
 
    
</div><!-- left_container -->
	@php $j++; $l++; @endphp
@endforeach





</div>

</div>




<div class="right_container">
<Div class="subheading">{{ (Lang::has(Session::get('lang_file').'.CONTAINER_LIST')!= '')  ?  trans(Session::get('lang_file').'.CONTAINER_LIST'): trans($OUR_LANGUAGE.'.CONTAINER_LIST')}}</Div>

<div class="service_inner_wrap">
	
	
	
	@foreach($containerlistitem as $allcontainerlist)
		<div class="container_list_row">
    	<div class="food_container_listimg"><img src="{{ $allcontainerlist->img }}"></div>
        <div class="food_container_txt">
        	
            <span>{{ (Lang::has(Session::get('lang_file').'.NO_OF_PEOPLE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_PEOPLE'): trans($OUR_LANGUAGE.'.NO_OF_PEOPLE')}} {{ $allcontainerlist->no_people }}</span>
        </div>
        <div class="fodd_cont_catg">{{ $allcontainerlist->title }}</div>
    </div>
   @endforeach 

</div><!-- service_inner_wrap -->

</div><!-- right_container -->

<a name="order_summary1" class="linking">&nbsp;</a>

<div class="order_summary_bar">
<Div class="subheading">{{ (Lang::has(Session::get('lang_file').'.ORDER_SUMMARY')!= '')  ?  trans(Session::get('lang_file').'.ORDER_SUMMARY'): trans($OUR_LANGUAGE.'.ORDER_SUMMARY')}}</Div>

<div class="order_sum_list" id="myContainer">

<div class="order_sumrow">
	<div class="order_sum_title">{{ $mkbasecatgory }}</div>
	<div class="order_sum_row_area">
    <div class="order_subcol2 sab">{{ $productinfo[0]->pro_title }}</div>
    <div class="order_subcol2 buffet-right">  {{ currency($newproductprice, 'SAR',$Current_Currency) }}</div>
	</div>
	<div class="order_sum_row_area">
    @if(isset($productinfo[0]->Insuranceamount) && $productinfo[0]->Insuranceamount!='')
    <div class="order_subcol2 sab">@if (Lang::has(Session::get('lang_file').'.Insurance_amount')!= '') {{  trans(Session::get('lang_file').'.Insurance_amount') }} @else  {{ trans($OUR_LANGUAGE.'.Insurance_amount') }} @endif</div>
    <div class="order_subcol2 buffet-right"> 
    	@php $insuprice=$productinfo[0]->Insuranceamount; @endphp
    	   {{ currency($productinfo[0]->Insuranceamount, 'SAR',$Current_Currency)  }} </div>
	@endif
	</div>
</div>
@php if(isset($insuprice) && $insuprice!=''){ $basetotal=$newproductprice + $insuprice; } else { $basetotal=$newproductprice;  } if(count($productservices)>0){ @endphp
@php  @endphp
@foreach($productservices as $adopedproductservices)
  @php $basetotal=$basetotal+$adopedproductservices->price;

   @endphp
  <div class="order_sumrow">
	<div class="order_sum_title"></div>
    <div class="order_subcol2 sab">{{ $adopedproductservices->option_title }}</div>
    <div class="order_subcol2 buffet-close">{{ currency($adopedproductservices->price, 'SAR',$Current_Currency)  }}</div>
</div>   
	 
	 
@endforeach
@php } @endphp
</div>

<div class="total_food_cost">
	<div class="total_price">{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}:  <span id="pricetotalamount">{{ currency($basetotal, 'SAR',$Current_Currency) }}</span> </div>
</div>    

<div class="btn_row">
<input type="hidden" name="totalprice" id="totalprice" value="{{currency($basetotal, 'SAR',$Current_Currency, $format = false) }}">
<input type="hidden" name="orgsprice" id="orgsprice" value="{{currency($basetotal, 'SAR',$Current_Currency, $format = false) }}">
<input type="hidden" name="cartintemid[]" id="cartintemid" value="0">
<input type="hidden" name="foodqty[]" id="foodqty" value="0">

<input type="hidden" name="product_id" id="product_id" value="{{ $productinfo[0]->pro_id }}">
<input type="hidden" name="cart_type" id="cart_type" value="hall">
<input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.ADD')!= '')  ?  trans(Session::get('lang_file').'.ADD'): trans($OUR_LANGUAGE.'.ADD')}}" class="form-btn addto_cartbtn"></div>    
    
</div> <!-- order_summary_bar -->


</div><!-- vendor_container -->
{!! Form::close() !!}
</div> <!-- vendor_homepage -->





</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')

<script language="javascript">
  function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }

var dishidarr=[0];
 //dishidarr.clear();
var itemquantity=[];
function addtocart(menudishid){
	var totalamount=0;
	var menuid=document.getElementById('menuid'+menudishid).value;
	var menuname=document.getElementById('menuname'+menudishid).value;
	var dishid=document.getElementById('dishid'+menudishid).value;

	var dishname=document.getElementById('dishname'+menudishid).value;
	var selectedcontainerid=document.getElementById('selectedcontainerid'+menudishid).value;
	var currselectedcontainerid=document.getElementById('currselectedcontainerid'+menudishid).value;
	var selectedcontainername=document.getElementById('selectedcontainername'+menudishid).value;
	var selectedsinglecontainername=document.getElementById('selectedsinglecontainername'+menudishid).value;

	var containersectedid = selectedcontainerid.split(",");

	var c= parseInt(selectedcontainerid.length) - 1;
     var csid=currselectedcontainerid;  
    $('#rc'+csid+''+menudishid).css('display','block');



	
	var itemqty=document.getElementById('qty'+menudishid).value;
	    
	      itemquantity.push(itemqty);

	var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
	var dishcontainerbaseprice=document.getElementById('dishcontainerbaseprice'+menudishid).value;
	var totalprice=document.getElementById('totalprice').value;
	 <?php $Cur = Session::get('currency'); ?>

	 var totalamount=parseFloat(totalprice)+parseFloat(selectedcontainerprice);


	 document.getElementById('totalprice').value=totalamount.toFixed(2);
	 var isalreadyadded=dishidarr.includes(dishid);
	/*if(isalreadyadded===false)
	 		{*/
				 dishidarr.push(dishid);
				
				 let ndisharr=dishidarr;
				 dishidarr.toString();
				 
				 document.getElementById('cartintemid').value=dishidarr;
			
				 
					document.getElementById('pricetotalamount').innerHTML = totalamount.toFixed(2);
							if(dishcontainerbaseprice==0.00)
							{
							document.getElementById('error'+dishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CONTAINER_FIRST') }} @endif';
							document.getElementById('qty'+dishid).value = 1;
							}
						
							else
							
							{

							dishnames = dishname.replace(/'/g, "");
							var dishnameclass = dishnames.split(' ').join('_');
							$('.'+dishnameclass+'_'+selectedsinglecontainername).remove();
							  var div = document.createElement('div');
							div.className = 'order_sumrow'+' '+dishnameclass+'_'+selectedsinglecontainername;

							var newslprice=itemqty*dishcontainerbaseprice;
 							//newslprice = getChangedPrice(newslprice);  

								document.getElementById('addeddishcontainerid'+menudishid).value='';
					
 								div.innerHTML ='<div class="order_sum_title" id="'+dishnameclass+'_'+selectedsinglecontainername+'">'+dishname+'<input type="hidden" name="disp[]" class="disp" value="'+newslprice+'"><input type="hidden" name="conId[]" class="conId" value="'+currselectedcontainerid+'"><input type="hidden" name="conQid[]" class="conQid" value="'+itemqty+'"><input type="hidden" name="disid[]" class="disid" value="'+menudishid+'"></div><div class="order_subcol2">'+selectedsinglecontainername+' x '+itemqty+'</div><div class="order_subcol2 buffet-right"><?php echo $Cur; ?> '+newslprice+'</div><input class="buffet-close" type="button" value="X" onclick="removeRow(this,'+menudishid+','+ndisharr+')">';




   								document.getElementById('myContainer').appendChild(div);
								document.getElementById('addedmainmenuid'+menudishid).value=menuid;
								document.getElementById('addeddishid'+menudishid).value=menudishid;



								var conIds = '';
								$( ".conId" ).each(function() {
								getPs =  $(this).val();

								conIds = getPs+','+conIds;
								}); 
 
								document.getElementById('addeddishcontainerid'+menudishid).value=conIds;
 



								var conQid = '';
								$( ".conQid" ).each(function() {
								getPs =  $(this).val();
								conQid = getPs+','+conQid;
								}); 
 

								document.getElementById('addeddishcontainerqty'+menudishid).value=conQid;



								var totalP = 0;
								$( ".disp" ).each(function() {
								getP =  $(this).val();
								totalP = parseFloat(totalP) + parseFloat(getP);
								}); 
								totalP = parseFloat(totalP).toFixed(2);

								var totalpricea = $('#orgsprice').val();
								var totalamounta=parseFloat(totalpricea)+parseFloat(totalP);
								document.getElementById('pricetotalamount').innerHTML = totalamounta.toFixed(2);

								
							}
				
			 
}


function removeRow(input,menudishid,disharr) {

 
  var index = dishidarr.indexOf(menudishid);

	if (index > -1) {
	  array.splice(index, 1);
	}
    document.getElementById('myContainer').removeChild(input.parentNode);
	var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
	var totalprice=document.getElementById('totalprice').value;
	 var totalamount=parseInt(totalprice)- parseInt(selectedcontainerprice);
	 totalamount = getChangedPrice(totalamount); 
	 document.getElementById('totalprice').value=totalamount;
	 //document.getElementById('pricetotalamount').innerHTML = totalamount.toFixed(2);

	 	var selectedcontainerid=document.getElementById('selectedcontainerid'+menudishid).value;
   var containersectedid = selectedcontainerid.split(",");

  var c= parseInt(containersectedid.length) - 1;
  var cid=containersectedid[c];
    $('#rc'+cid+''+menudishid).css('display','none');



	var totalP = 0;
	$( ".disp" ).each(function() {
	getP =  $(this).val();
	totalP = parseFloat(totalP) + parseFloat(getP);
	}); 
	totalP = parseFloat(totalP).toFixed(2);
 
	var totalpricea = $('#orgsprice').val();
	var totalamounta=parseInt(totalpricea)+parseInt(totalP);
 
	document.getElementById('pricetotalamount').innerHTML = totalamounta.toFixed(2);




						var conIds = '';
						$( ".conId" ).each(function() {
						getPs =  $(this).val();
						conIds = getPs+','+conIds;
						}); 



						document.getElementById('addeddishcontainerid'+menudishid).value=conIds;



								var conQid = '';
								$( ".conQid" ).each(function() {
								getPs =  $(this).val();
								conQid = getPs+','+conQid;
								}); 
 

								document.getElementById('addeddishcontainerqty'+menudishid).value=conQid;



}

</script>




<script language="javascript">

$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});

</script>



<script language="javascript">
function pricecalculation(dishid,k,act){
		var selectedcontainerprice=document.getElementById('dishcontainerbaseprice'+dishid).value;
					if(selectedcontainerprice==0.00)
					{
					document.getElementById('error'+dishid).innerHTML = '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_CONTAINER_FIRST') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CONTAINER_FIRST') }} @endif';
					document.getElementById('qty'+dishid).value = 1;
					}
				
				else
				
				{
				
		 		var did='qty'+dishid;
				var no=1;
				var currentquantity=document.getElementById(did).value;
				

				if(currentquantity<1){
								document.getElementById(did).value=1;
								 var qty= parseInt(no);
							}else{
							if(act=='pricewithqty'){
								var qty=parseInt(currentquantity)
							}
							if(act=='add'){
									 var qty= parseInt(currentquantity)+parseInt(no);
								 }
							if(act=='remove'){ 
									if(parseInt(currentquantity)==1){
								      var qty=parseInt(currentquantity)
								   }else{
								      var qty=parseInt(currentquantity)-parseInt(no);
									}

							 }
							}
				


				var finalprice=qty*selectedcontainerprice;
				var inputboxcontanerid='selectedcontainerprice'+dishid;
				document.getElementById(k).innerHTML = finalprice.toFixed(2);
				document.getElementById(inputboxcontanerid).value = finalprice;
			}
}

</script>

<script language="javascript">
	var cid= [];
	var cname=[];
	var cprice=[];
function selectcontainer(k,dishid,containerid,containerprice,z,containername,realprice,curr)

		{            
					
				cid.push(containerid);
				cname.push(containername);
				cprice.push(containerprice);

					jQuery('.dishh').removeClass('listing_active');
					var classid=''+z+dishid;
					var inputboxcontanerid='selectedcontainerprice'+dishid;
					var dishcontainerbaseprice='dishcontainerbaseprice'+dishid;
					var selectedcontainername='selectedcontainername'+dishid;
					var selectedcontainerid='selectedcontainerid'+dishid;
var currselectedcontainerid='currselectedcontainerid'+dishid;

					var selectedsinglecontainername='selectedsinglecontainername'+dishid;
					var dispprice='dis'+dishid;

					var i;
						for (i = 1; i < z; i++) { 
						var rid=''+i+dishid;
							document.getElementById(rid).classList.remove("listing_active");
						}
					document.getElementById(selectedcontainerid).value = cid;

					document.getElementById(currselectedcontainerid).value = containerid;


					document.getElementById(selectedcontainername).value = cname;
					document.getElementById(selectedsinglecontainername).value = containername;
				
						document.getElementById(classid).classList.add("listing_active");
						if(realprice==containerprice){
							var displayprice='<span class="brown-price">'+curr+' '+realprice+'</span>';
						}else{
							var displayprice='<del>'+curr+' '+realprice+'</del><br><span class="brown-price">'+curr+' '+containerprice+'</span>';
						}
						document.getElementById(dispprice).innerHTML = displayprice;
					document.getElementById(k).innerHTML = containerprice;
					document.getElementById(inputboxcontanerid).value = cprice;
					document.getElementById(dishcontainerbaseprice).value = containerprice;
					document.getElementById('error'+dishid).innerHTML='';
					document.getElementById('qty'+dishid).value = 1;
		}

</script>

<script language="javascript">
  function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
            return false;
        } else {
            return true;
        }
    }
</script>

@include('includes.navbar')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
@endphp
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
         
     <?php if(isset($vendordetails->mc_video_url) && $vendordetails->mc_video_url!=''){ ?>
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
<?php } ?>
 <li><a href="#Book_a_package">{{ (Lang::has(Session::get('lang_file').'.Book_a_package')!= '')  ?  trans(Session::get('lang_file').'.Book_a_package'): trans($OUR_LANGUAGE.'.Book_a_package')}}</a></li>


          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
          <!--<li><a href="#choose_package">{{ (Lang::has(Session::get('lang_file').'.Choose_Package')!= '')  ?  trans(Session::get('lang_file').'.Choose_Package'): trans($OUR_LANGUAGE.'.Choose_Package')}}</a></li>-->
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap service-wrap diamond_space">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                 <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>               
                    <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?><li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li><?php } } ?>             
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
               <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>               
                    <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">{{$vendordetails->mc_name}}</div>
          <div class="detail_hall_description">{{$vendordetails->address}}</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_CENTER')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_CENTER'): trans($OUR_LANGUAGE.'.ABOUT_CENTER')}}</div>
          <div class="detail_about_hall"><div class="comment more">{{strip_tags($vendordetails->mc_discription)}}</div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: <span><?php
		    $getcityname = Helper::getcity($vendordetails->city_id); 
		    $mc_name = 'ci_name'; 
		    if(Session::get('lang_file')!='en_lang')
			{
		      $mc_name = 'ci_name_ar'; 
		    }
		    echo $getcityname->$mc_name; 
		  ?></span></div>
		  <?php if(isset($vendordetails->google_map_address) && $vendordetails->google_map_address!=''){ 
$lat  = $vendordetails->latitude;
$long  = $vendordetails->longitude;

        ?>
		  <div class="detail_hall_dimention" id="map" style="height: 230px!important"> </div>
		  <?php } ?>
		  </div>
        </div>
      </div>
      <!-- service_detail_row -->
      
	  <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
	  <?php if(isset($vendordetails->mc_video_url) && $vendordetails->mc_video_url!=''){ ?>
        <div class="service-video-area">
          <div class="service-video-cont">{{$vendordetails->mc_video_description or '' }}</div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <!-- service-video-area --> 
		<?php } if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
				 @foreach($allreview as $val)
				 @php $userinfo = Helper::getuserinfo($val->customer_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{round($val->ratings)}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
				  @endforeach			  
                </ul>
              </div>
            </section>
          </div>
        </div>
		<?php } ?>
      </div>
      <!-- service-mid-wrapper -->


<div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                
                <li><a href="#"  class="cat select" data-toggle="tab">{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}}</a></li>
               
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>



      <!--service-display-section-->
	  
      <div class="service-display-section"> 
<a name="Book_a_package" class="linking">&nbsp;</a> 
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer">
              <div class="diamond_wrapper_main"> @php  $i=1; @endphp
                @php  $k=count($productlist);  @endphp
                @php if($k<6){ @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)

                  <?php
                    $img = str_replace('thumb_','',$getallcats->pro_Img);
                  ?>
                  <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                    <div class="category_wrapper" style="background:url({{ $img or '' }});">
                      <div class="category_title">
                        <div class="category_title_inner">
                          {{$getallcats->pro_title}}
                        </div>
                      </div>
                    </div>
                    </a> </div>
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                <!------------ 6th-------------->
                @php }elseif($k==6){ @endphp
                @php $j=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                  @php if($j==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($j==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($j==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($j==6){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($j==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==4){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($j==5){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($j==6){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $j=$j+1; @endphp
                  @endforeach </div>
                <!------------ 7th-------------->
                @php }elseif($k==7){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                  
                  @php if($l==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($l==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($l==7){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==7){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!------------ 8th-------------->
                @php }elseif($k==8){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productlist as $getallcats)
                  @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                  @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                  @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                  
                  @php if($l==1){ $classrd='category_wrapper1'; @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_3of5 rows5row"> @php } @endphp 
                      @php if($l==4){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp
                          @php if($l==8){ $classrd='category_wrapper9'; @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">
                                  {{$getallcats->pro_title}}
                                </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==8){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                
                @php }elseif($k==9){ @endphp
                <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                  @foreach($productlist as $getallcats)
                  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
                  
                  
                  @php if($i==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($i==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($i==4){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($i==7){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp 
                          @php if($i==9){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return showProductDetail('{{ $getallcats->pro_id }}','{{$getallcats->pro_mr_id}}')"> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});"> <span class="category_title"><span class="category_title_inner">
                            {{$getallcats->pro_title}}
                            </span></span> </span> </a> @php if($i==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    @php if($i==9){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp 
                  
                  @php $i=$i+1; @endphp
                  @endforeach </div>
                @php } @endphp 
				
				</div>
          </div>
		  </div>
          <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
		  <div class="" align="center"> {{ $productlist->links() }}</div>
        </div>
        <!-- service-display-right -->
		{!! Form::open(['url' => 'addtocarttravelandcarproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
		<?php if(isset($productlist[0]->pro_disprice) && $productlist[0]->pro_disprice>=1){$getPrice = $productlist[0]->pro_disprice;}else{$getPrice = $productlist[0]->pro_price;}?>
        <div class="service-display-left">
		<input type="hidden" id="category_id" name="category_id" value="{{ $category_id }}">
        <input type="hidden" id="subcat_id" name="subcat_id" value="{{ $subcat_id }}">
		<input type="hidden" name="itemqty" id="itemqty" value="1" min="1" readonly />
		<input type="hidden" name="actiontype" value="travelagencydetail">
		<input type="hidden" name="cart_sub_type" value="travel">
        <input type="hidden" name="cart_type" value="travel">
		<b class="travel-sucss">{{ Session::get('status') }}</b>
		<span id="selectedproduct">
		<input type="hidden" id="product_id" name="product_id" value="<?php if(isset($productlist[0]->pro_id) && $productlist[0]->pro_id!=''){echo $productlist[0]->pro_id;}?>">
		<input type="hidden" id="priceId" name="priceId" value="<?php if(isset($getPrice) && $getPrice!=''){ echo currency($getPrice, 'SAR',$Current_Currency, $format = false); }?>">

    <input type="hidden" id="product_orginal_price" name="product_orginal_price" value="<?php if(isset($getPrice) && $getPrice!=''){ echo currency($getPrice, 'SAR',$Current_Currency, $format = false); }?>">

		<input type="hidden" id="vendor_id" name="vendor_id" value="<?php if(isset($productlist[0]->pro_mr_id) && $productlist[0]->pro_mr_id!=''){echo $productlist[0]->pro_mr_id;} ?>">
          <div class="service-left-display-img product_gallery">

 
            @php    $pro_id = $productlist[0]->pro_id; @endphp
             @include('includes/product_multiimages')
      
 </div>
          <div class="service-product-name"><?php if(isset($productlist[0]->pro_title) && $productlist[0]->pro_title!=''){echo $productlist[0]->pro_title;} ?></div>
          <div class="service-beauty-description"><?php if(isset($productlist[0]->pro_desc) && $productlist[0]->pro_desc!=''){echo nl2br($productlist[0]->pro_desc);} ?></div>
		</span>
		  <?php if(!empty($productlist[0]->pro_id) && !empty($productlist[0]->pro_mr_id)){ ?>
          <div class="car_rent_section">
            <?php $get_location = Helper::getProductName($productlist[0]->pro_id,$productlist[0]->pro_mr_id,'Location'); if(isset($get_location->value) && $get_location->value!=''){ 
                
              ?>
              <div class="car_model" id="pplocationone">
                <div class="car_text">{{ (Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')}}</div>
                <div class="car_text_type" id="locid"><?php echo $get_location->value; ?></div>
              </div>
			<?php } $get_package_date = Helper::getpackagedropdownName($productlist[0]->pro_id,$productlist[0]->pro_mr_id,'Package Date'); if(isset($get_package_date->value) && $get_package_date->value!=''){ 
        $allDate = explode(',',$get_package_date->value); 

        ?>
            <div class="car_model">
              <div class="car_text">{{ (Lang::has(Session::get('lang_file').'.PACKAGE_SECHEDULE')!= '')  ?  trans(Session::get('lang_file').'.PACKAGE_SECHEDULE'): trans($OUR_LANGUAGE.'.PACKAGE_SECHEDULE')}}</div>
              <div class="car_text_type packagedates" id="packagesechedulenone">			  
			  <select name="package_sechedule" id="package_sechedule" required>
			    <option value="">{{ (Lang::has(Session::get('lang_file').'.Select')!= '')  ?  trans(Session::get('lang_file').'.Select'): trans($OUR_LANGUAGE.'.Select')}}</option>
			    <?php 
                $todaydate=date('Y-m-d h:i:s a'); 
            $ftoday=strtotime($todaydate); 
          foreach($allDate as $val){            
          $packagetime=strtotime($val);
          if($packagetime>$ftoday){ 
            ?>
				 <option value="<?php echo $val; ?>"><?php echo date('d M Y h:i A',strtotime($val));  ?></option>
				<?php } } ?>
			  </select>
			  </div>
            </div>
<!--------------No of people------------>
            <div class="service-radio-line">
            <div class="service_quantity_box">
       <div class="service_qunt">{{ (Lang::has(Session::get('lang_file').'.NO_OF_PEOPLE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_PEOPLE'): trans($OUR_LANGUAGE.'.NO_OF_PEOPLE')}}</div>
              <div class="service_qunatity_row">
                <div class="td td2 quantity food-quantity" data-title="Total Quality">
                  <div class="quantity">
                    <button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
                    <input type="number" name="itemqty" id="qty" value="1" min="1"  onkeyup="return pricecalculation('add');" />
                    <button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>

<!--------->


			<?php } $get_package_duration = Helper::getProductName($productlist[0]->pro_id,$productlist[0]->pro_mr_id,'Package Duration'); if(isset($get_package_duration->value) && $get_package_duration->value!=''){ ?>
            <div class="car_model" id="ppdurationnone">
              <div class="car_text">{{(Lang::has(Session::get('lang_file').'.PACKAGE_DURATION')!= '')  ?  trans(Session::get('lang_file').'.PACKAGE_DURATION'): trans($OUR_LANGUAGE.'.PACKAGE_DURATION')}}</div>
              <div class="car_text_type" id="ppduration"><?php echo $get_package_duration->value; ?></div>
            </div>
			<?php } $get_service_days = Helper::getProductName($productlist[0]->pro_id,$productlist[0]->pro_mr_id,'Service Days'); if(isset($get_service_days->value) && $get_service_days->value!=''){ ?>
            <div class="car_model" id="ppservicedaynone">
              <div class="car_text">{{ (Lang::has(Session::get('lang_file').'.SERVICE_DAY')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_DAY'): trans($OUR_LANGUAGE.'.SERVICE_DAY')}}</div>
              <div class="car_text_type" id="ppserviceday"><?php echo $get_service_days->value; ?></div>
            </div>
			<?php } $get_extra_service = Helper::getProductName($productlist[0]->pro_id,$productlist[0]->pro_mr_id,'Extra Service'); if(isset($get_extra_service->value) && $get_extra_service->value!=''){ ?>
            <div class="car_model" id="ppextraservicenone">
              <div class="car_text">{{ (Lang::has(Session::get('lang_file').'.EXTRS_SERVICE')!= '')  ?  trans(Session::get('lang_file').'.EXTRS_SERVICE'): trans($OUR_LANGUAGE.'.SEEXTRS_SERVICERVICE_DAY')}}</div>
              <div class="car_text_type" id="ppextraservice"><?php	echo nl2br($get_extra_service->value); ?></div>
            </div>
			<?php } ?>
		  </div>
		  <?php } ?>
 


          <div class="total_food_cost">
		        <span class="newprice"><?php if(isset($getPrice) && $getPrice!='' && $getPrice < $productlist[0]->pro_price){echo '<del>  '.currency($productlist[0]->pro_price, 'SAR',$Current_Currency).'</del> <br />';} ?></span>

            <div class="total_price">{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: <span id="cont_final_price">  <?php if(isset($getPrice) && $getPrice!=''){echo currency($getPrice, 'SAR',$Current_Currency);}?></span></div>

          </div>
          
          <div class="btn_row">
            <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.BOOK_NOW')!= '')  ?  trans(Session::get('lang_file').'.BOOK_NOW'): trans($OUR_LANGUAGE.'.BOOK_NOW')}}" class="form-btn addto_cartbtn">
          </div>
		  <?php if(isset($vendordetails->terms_conditions) && $vendordetails->terms_conditions!=''){ ?>
		  <div class="terms_conditions"><a href="{{ $vendordetails->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}</div>
		  <?php } ?>
          <!-- container-section -->
         <a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a>
        </div>

		{!! Form::close() !!}
        <!-- service-display-left -->
      </div>
	 @include('includes.other_services')
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
@include('includes.footer') 
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
   function pricecalculation(act){
      
              
              var no=1;
              
              var currentquantity=document.getElementById('qty').value;
              var unititemprice=document.getElementById('product_orginal_price').value;
             

              if(currentquantity<1){
                document.getElementById('qty').value=1;
                 var qty= parseInt(no);
              }else{
              if(act=='pricewithqty'){
                var qty=parseInt(currentquantity)
              }
              if(act=='add'){
                   var qty= parseInt(currentquantity)+parseInt(no);
                 }
              if(act=='remove'){ 
                  if(parseInt(currentquantity)==1){
                      var qty=parseInt(currentquantity)
                   }else{
                      var qty=parseInt(currentquantity)-parseInt(no);
                  }

               }
              }
              
                 var orderedqty=qty;
              
              var producttotal=orderedqty*unititemprice;
              //alert(producttotal);
              //jQuery('[name=priceId]').val(producttotal);
              var fprice=producttotal.toFixed(2);

               <?php $Cur = Session::get('currency'); ?>
              document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+fprice;
              
              
          
   }

</script>





<script type="text/javascript">
 

  function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }
 function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"{{url('getmultipleImages')}}?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}


function showProductDetail(str,vendorId)
{ 
	   $.ajax({
	   type:"GET",
	   url:"{{url('getTravelAgencyInfo')}}?product_id="+str+'&vendor_id='+vendorId,
	   success:function(res)
	   {               
		if(res)
		{ 
		  var json = JSON.stringify(res);
		  var obj = JSON.parse(json);
          console.log(obj);
		  length=obj.productdateshopinfo.length;

        $('html, body').animate({
         scrollTop: ($('#selectedproduct').offset().top)
      }, 'slow');



		  //alert(length);
		  if(length>0)
		  {
	         for(i=0; i<length; i++)
		     {       

        $('#qty').val(1);

			  if(obj.productdateshopinfo[i].pro_discount_percentage >0 && obj.productdateshopinfo[i].pro_discount_percentage !=''){

          var getPrice = parseFloat(obj.productdateshopinfo[i].pro_disprice).toFixed(2); 
          var realprice= parseFloat(obj.productdateshopinfo[i].pro_price).toFixed(2);

        } else {

          var getPrice = parseFloat(obj.productdateshopinfo[i].pro_price).toFixed(2);
          
        }  
        <?php $Cur = Session::get('currency'); ?>
    
       var NewPrice =  getChangedPrice(getPrice);
         var originalprice =  getChangedPrice(realprice);

			  if(getPrice < obj.productdateshopinfo[i].pro_price)
			  {        
				   $('#cont_final_price').html('<?php echo $Cur;?> '+ NewPrice);
				   $('.newprice').html('<del><?php echo $Cur;?> '+originalprice+'</del>');
			  }	
			  else
			  {
			     $('.newprice').hide();
			     $('#cont_final_price').html('<?php echo $Cur;?> '+NewPrice);	
			  } 
			  
              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+NewPrice+'"><input type="hidden" id="product_orginal_price" name="product_orginal_price" value="'+NewPrice+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+nl2br(obj.productdateshopinfo[i].pro_desc)+'</div>');
              checkgallery(str);
		     }
		  }
		  if($.trim(obj.productatrrextraservice) !=5)
		  {
        var ser=obj.productatrrextraservice;
        var servicesContent=ser.replace(/\n/g,"<br>");
		    $('#ppextraservice').html(servicesContent);
			$('#ppextraservicenone').css('display','block');
		  }
		  else
		  {
		    $('#ppextraservicenone').css('display','none');
		  }
		  if($.trim(obj.productatrrpackagedate) !=2)
		  {
		    $('.packagedates').html(obj.productatrrpackagedate);
			$('#packagesechedulenone').css('display','block');
		  }
		  else
		  {
		    $('#packagesechedulenone').css('display','none');
		  }
		  if($.trim(obj.productatrrlocation) !=1)
		  {
		   $('#locid').html(obj.productatrrlocation);
			$('#pplocationone').css('display','block');
		  }
		  else
		  {
		    $('#pplocationone').css('display','none');
		  }
		  if($.trim(obj.productatrrpackageduration) !=3)
		  {
		     $('#ppduration').html(obj.productatrrpackageduration);
			 $('#ppdurationnone').css('display','block');
		  }
		  else
		  {
		    $('#ppdurationnone').css('display','none');
		  }
		  if($.trim(obj.productatrrserviceday) !=4)
		  {
		    $('#ppserviceday').html(obj.productatrrserviceday);
			$('#ppservicedaynone').css('display','block');
		  }
		  else
		  {
		    $('#ppservicedaynone').css('display','none');
		  }
			$('html, body').animate({
			   scrollTop: ($('.service-display-left').offset().top)
			}, 'slow');

	    }
	   }
	 });
}
</script>
<script type="text/javascript">
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {   
	      "package_sechedule" : {
            required : true
          },
         },
         messages: {        
		  "package_sechedule": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_PACKAGE_SECHEDULE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_PACKAGE_SECHEDULE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_PACKAGE_SECHEDULE') }} @endif"
          }, 
         }
 });
 jQuery(".addto_cartbtn").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
	{
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script language="javascript">
$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
</script>
<div class="trd">@include('includes.popupmessage')</div>




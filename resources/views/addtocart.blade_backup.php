@include('includes.navbar')
<div class="outer_wrapper">
<div class="inner_wrap">
@include('includes.header')
@php 
//print_r(Session::get('searchdata'));
//echo "<pre>";
//print_r($hall_data);

@endphp

<div class="search-section">
 <div class="event-result-form">
<div class="search-box search-box1">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')}}</div>
<div class="search-noneedit"> {{ $otype }}</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_ATTENDANCE'): trans($OUR_LANGUAGE.'.NO_OF_ATTENDANCE')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.noofattendees')}}</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}</div>
<div class="search-noneedit">SAR {{ Session::get('searchdata.budget')}}</div>
</div> <!-- search-box -->

<!--<div class="search-box search-box4">
<div class="search-box-label">Gender</div>
<div class="search-noneedit">Female</div>
</div>--> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')}}</div>
<div class="search-noneedit">{{ $hall_data[0]['city'] }}</div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.occasiondate')}}</div>
</div> <!-- search-box -->

 
</div> <!-- modify-search-form -->
 
</div>


<div class="tatal-services-area">
<div class="mobile-filter-line"> 
<div class="mobile-search mobile-search1row"><span>Event Result</span></div>
</div>
<div class="tatal-services-inner-area">
<div class="tatal-services-heading">{{ (Lang::has(Session::get('lang_file').'.TOTAL_SERVICES')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_SERVICES'): trans($OUR_LANGUAGE.'.TOTAL_SERVICES')}}</div>

<div class="table tatal-services-table">
        <div class="tr table_heading_tr">
          <div class="table_heading">Sl. No.</div>
          <div class="table_heading">{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}}</div>
          <div class="table_heading">{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}</div>
          <div class="table_heading">{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}</div>
        </div>       	 
 

<div class="tr">
	 <div class="td td1" data-title="Sl. No.">1<!--<button>Show Detail</button>--></div>
	  <div class="td td2" data-title="Services">{{ $productinfo[0]->pro_title }} <!--<a href="#" class="services-edit">&nbsp;</a>--></div>
	    <div class="td td3" data-title="Service Provider">{{ $mkbasecatgory }} </div>
 <div class="td td4" data-title="Price">SAR 
 @php if($productinfo[0]->pro_disprice !=''){ $hallprice=$productinfo[0]->pro_disprice; }else{ $hallprice=$productinfo[0]->pro_price; }
 $basetotal=$hallprice;
  @endphp
{{ $hallprice }}</div>
	 </div>

	 @php $k=2; if(count($productservices)>0){ @endphp 
@foreach($productservices as $adopedproductservices)
 @php $basetotal=$basetotal+$adopedproductservices->price; @endphp
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">{{ $k }}</div>
	  <div class="td td2" data-title="Services">{{ $adopedproductservices->option_title }}<!-- <a href="#" class="services-edit">&nbsp;</a>--></div>
	    <div class="td td3" data-title="Service Provider">Hall Services</div>
 <div class="td td4" data-title="Price">SAR {{ $adopedproductservices->price }}</div>
	 </div>
	  @php $k++; @endphp
	@endforeach
	  @php } if(count($internal_foods)>0){ @endphp
	@foreach($internal_foods as $internalfood)
	
	<div class="tr">
	 <div class="td td1" data-title="Sl. No.">{{$k}}</div>
	  <div class="td td2" data-title="Services">{{ $internalfood['dish_name'] }}<br />
{{ $internalfood['container_title'] }} X {{ $internalfood['quantity'] }} <!--<a href="#" class="services-edit">&nbsp;</a>--></div>
	    <div class="td td3" data-title="Service Provider">{{ $internalfood['menu_name'] }}</div>
 <div class="td td4" data-title="Price">SAR {{ $internalfood['final_price'] }}</div>
	 </div>
	 @php $basetotal=$basetotal+$internalfood['final_price']; @endphp
	  @php $k++; @endphp
	@endforeach
@php } @endphp
<div class="tr total-prise">
	 <div class="td td1" data-title="Sl. No.">&nbsp;</div>
	  <div class="td td2" data-title="Services">&nbsp;</div>
	    <div class="td td3" data-title="Service Provider">&nbsp;</div>
 <div class="td td4" data-title="Total"><span>{{ (Lang::has(Session::get('lang_file').'.TOTAL')!= '')  ?  trans(Session::get('lang_file').'.TOTAL'): trans($OUR_LANGUAGE.'.TOTAL')}}</span> {{ $basetotal }}</div>
	 </div>	
 </div> <!-- table -->
<div class="tatal-services-pay-area"><input type="submit" value="Pay Now" class="form-btn" /></div> <!-- tatal-services-pay-area -->

</div> <!-- tatal-services-inner-area -->
</div>







</div> <!-- outer_wrapper -->
</div>
 
<script>
jQuery(document).ready(function(){
    jQuery("button").click(function(){
        jQuery('#halldetails').toggle();
    });
});
</script>





@include('includes.footer')
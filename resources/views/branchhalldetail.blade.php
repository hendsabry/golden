<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 
<title>@if (Lang::has(Session::get('lang_file').'.Golden_Cages')!= '') {{  trans(Session::get('lang_file').'.Golden_Cages') }} @else  {{ trans($OUR_LANGUAGE.'.Golden_Cages') }} @endif</title>
<link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/themes/images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="{{url('/')}}/themes/css/reset.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/common-style.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/stylesheet.css" rel="stylesheet" />


<link href="{{url('/')}}/themes/slider/css/demo.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/slider/css/flexslider.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/user-my-account.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/arabic.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface-media.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/diamond.css" rel="stylesheet" />
<!-- custome scroll 
<link href="mousewheel/jquery.mCustomScrollbar.css" rel="stylesheet" />
-->
<!--<link href="css/diamond.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />-->

<script src="{{url('/')}}/themes/js/jquery-lib.js"></script>
<script src="{{url('/')}}/themes/js/height.js"></script>
<script src="{{url('/')}}/themes/js/jquery.flexslider.js"></script>
<script src="{{url('/')}}/themes/js/modernizr.js"></script>
<script src="{{url('/')}}/themes/js/jquery.mousewheel.js"></script>
<script src="{{url('/')}}/themes/js/demo.js"></script>
<script src="{{url('/')}}/themes/js/froogaloop.js"></script>
<script src="{{url('/')}}/themes/js/jquery.easing.js"></script>


</head>
@php  if(Session::get('lang_file')=='ar_lang'){ $sitecss="arabic"; }else{ $sitecss=""; } @endphp
<body class="{{ $sitecss }}">
@php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
  $cururl = request()->segment(count(request()));
@endphp

<div class="outer_wrapper">
<div class="vendor_header">
	<div class="inner_wrap">                
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$ShopInfo[0]->mc_img}}" alt="" /></a></div>                      	
        </div>
		<div class="vendor_header_right"> 
		<!-- vendor_header_left -->
         <div class="vendor_welc">

      @include("includes.language-changer")
    <div class="vendor_name username">  {{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}} <span>
      <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
			
			<ul class="vendor_header_navbar">
				<li><a href="{{route('my-account-profile')}}"@php if($cururl=='my-account-profile') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')}}</a></li>
        <li><a href="{{route('my-account-ocassion')}}"@php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')}}</a></li>
        <li><a href="{{route('my-account-studio')}}"@php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')}}</a></li>
        <li><a href="{{route('my-account-security')}}"@php if($cururl=='my-account-security') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')}}</a></li>
        <li><a href="{{route('my-account-wallet')}}"@php if($cururl=='my-account-wallet') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')}}</a></li>
        <li><a href="{{route('my-account-review')}}"@php if($cururl=='my-account-review') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')}}</a></li>
        <li><a href="{{route('my-request-a-quote')}}"@php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')}}</a></li>
        <li><a href="{{route('change-password')}}"@php if($cururl=='change-password') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}</a></li>
				<li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li>
			</ul>
      </span></div>
	  @if (Session::get('customerdata.token')!='')
			@php $getcartnoitems = Helper::getNumberOfcart(); @endphp
			@if($getcartnoitems>0)
    <a href="{{url('mycart')}}" class="vendor_cart"><img src="{{ url('') }}/themes/images/basket.png" /><span>{{ $getcartnoitems }}</span></a>
	@endif
			@endif
	
	 </div>
        <!-- vendor_header_right -->
		  @php if(isset($otherhalllist) && count($otherhalllist)){  @endphp
            <div class="select_catg">

                    <div class="select_lbl">@if (Lang::has(Session::get('lang_file').'.OTHER_HALL')!= '') {{  trans(Session::get('lang_file').'.OTHER_HALL') }} @else  {{ trans($OUR_LANGUAGE.'.OTHER_HALL') }} @endif</div>

                <div class="search-box-field">
                  <select class="select_drp" id="dynamic_select">
                      <option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_HALL')!= '') {{  trans(Session::get('lang_file').'.SELECT_HALL') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_HALL') }} @endif</option>

                         @foreach($otherhalllist as $otherhalls)
                          <option value="{{url('')}}/branchhalldetail/{{ $halltype }}/{{ $typeofhallid }}/{{ $category_id }}/{{ $otherhalls->pro_mc_id}}/{{ $otherhalls->pro_id}}"> {{ $otherhalls->pro_title or ''}}  </option>

                         @endforeach
                    </select>
                </div>
            </div>

		   @php } else { if(count($otherbarnch)>1){ @endphp
            <div class="select_catg">
            	<div class="select_lbl">@if (Lang::has(Session::get('lang_file').'.Other_Branches')!= '') {{  trans(Session::get('lang_file').'.Other_Branches') }} @else  {{ trans($OUR_LANGUAGE.'.Other_Branches') }} @endif</div>

                <div class="search-box-field">
                	<select class="select_drp" id="dynamic_select">
                    	<option value="">@if (Lang::has(Session::get('lang_file').'.Select_Branch')!= '') {{  trans(Session::get('lang_file').'.Select_Branch') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Branch') }} @endif</option>

                         @foreach($otherbarnch as $otherbarnches)
                          <option value="{{url('')}}/branchlist/{{ $halltype }}/{{ $typeofhallid }}/{{ $category_id }}"> {{ $otherbarnches->mc_name }}  </option>

                         @endforeach
                    </select>
                </div>
            </div>
           @php } } @endphp
		 </div>




	</div>
</div><!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">
      <ul>
	  @php if($ShophallbasicInfo[0]->pro_desc!=''){ @endphp
        <li><a href="#about_shop" class="active">@if (Lang::has(Session::get('lang_file').'.About_Shop')!= '') {{  trans(Session::get('lang_file').'.About_Shop') }} @else  {{ trans($OUR_LANGUAGE.'.About_Shop') }} @endif</a></li>
		@php } @endphp
    @php if($ShophallbasicInfo[0]->video_url!=''){ @endphp
        <li><a href="#video">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif</a></li>
		@php } @endphp
		
		@php if(count($serviceoptionvalue[0])>0){ @endphp
        <li><a href="#prepaid">@if (Lang::has(Session::get('lang_file').'.Services')!= '') {{  trans(Session::get('lang_file').'.Services') }} @else  {{ trans($OUR_LANGUAGE.'.Services') }} @endif</a></li>
		@php } @endphp
		
       <!-- <li><a href="#choose_package">@if (Lang::has(Session::get('lang_file').'.Choose_Package')!= '') {{  trans(Session::get('lang_file').'.Choose_Package') }} @else  {{ trans($OUR_LANGUAGE.'.Choose_Package') }} @endif</a></li>-->
	   @php  if($ShophallbasicInfo[0]->food_type!=''){ @endphp
	    <li><a href="#food">@if (Lang::has(Session::get('lang_file').'.Food')!= '') {{  trans(Session::get('lang_file').'.Food') }} @else  {{ trans($OUR_LANGUAGE.'.Food') }} @endif</a></li>
		@php } @endphp
    @php if(count($customerReviewOnProduct)>0){ @endphp
        <li><a href="#our_client">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</a></li>
    @php } @endphp
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->

 



<div class="inner_wrap">
@if(Session::has('message'))
<div class="alert error">{{ Session::get('message') }}</div>
@endif
<div class="detail_page hall-extra-class">	
    <a name="about_shop"></a>	
    <div class="service_detail_row">
    	
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
                @foreach($shopgalleryimage as $productgalleryimages)
				<li>
  	    	    <img src="{{ str_replace('thumb_','',$productgalleryimages->image) }}" />
  	    		</li>
				@endforeach
  	    		       	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
              @foreach($shopgalleryimage as $productgallerythumbnailimages)
			<li>
  	    	    <img src="{{ $productgallerythumbnailimages->image }}" />
  	    		</li>
			@endforeach	
  	    	
           
          </ul>
        </div>
      		</section>
            
        </div>
        <a name="about_shop"></a>
        <div class="service_detail">
			@php   if( $ShophallbasicInfo[0]->pro_discount_percentage >0 ){ 
      $productrealprice= $ShophallbasicInfo[0]->pro_disprice; 
    } else{
     $productrealprice= $ShophallbasicInfo[0]->pro_price; } @endphp

      @php if( $ShophallbasicInfo[0]->Insuranceamount !='' ){ $productInsuranceamount= $ShophallbasicInfo[0]->Insuranceamount; } else{ $productInsuranceamount= ''; } @endphp
			
        	<div class="detail_title">{{ $ShophallbasicInfo[0]->pro_title}}</div>
            <div class="detail_hall_description">{{ $ShophbasicInfo[0]->address }}</div>
             <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}<span>
            <?php
        $getcityname = Helper::getcity($ShophbasicInfo[0]->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
            </span></div>
            <div class="detail_hall_subtitle">@if (Lang::has(Session::get('lang_file').'.About_Hall')!= '') {{  trans(Session::get('lang_file').'.About_Hall') }} @else  {{ trans($OUR_LANGUAGE.'.About_Hall') }} @endif </div>
            
            <div class="detail_about_hall">
				
              <div class="comment more">{{$ShophallbasicInfo[0]->about}} </div></div>
             

            

            <div class="hall-det-line"><span>@if (Lang::has(Session::get('lang_file').'.Hall_Dimension')!= '') {{trans(Session::get('lang_file').'.Hall_Dimension')}} @else  {{trans($OUR_LANGUAGE.'.Hall_Dimension')}}@endif:</span> 
                @php if(isset($ShophallbasicInfo[0]->hall_dimension) && $ShophallbasicInfo[0]->hall_dimension!=''){
                $d=explode('X',$ShophallbasicInfo[0]->hall_dimension); @endphp<br>
                  @if (Lang::has(Session::get('lang_file').'.LENGTH')!= ''){{trans(Session::get('lang_file').'.LENGTH')}}@else{{ trans($OUR_LANGUAGE.'.LENGTH')}}@endif- {{ $d[0] }} @if(Lang::has(Session::get('lang_file').'.Meter')!= ''){{trans(Session::get('lang_file').'.Meter')}}@else{{ trans($OUR_LANGUAGE.'.Meter')}}@endif,
                   @if (Lang::has(Session::get('lang_file').'.Width')!= '') {{  trans(Session::get('lang_file').'.Width') }} @else  {{ trans($OUR_LANGUAGE.'.Width') }} @endif{{ $d[1] }} @if (Lang::has(Session::get('lang_file').'.Meter')!= ''){{trans(Session::get('lang_file').'.Meter')}}@else{{trans($OUR_LANGUAGE.'.Meter')}}@endif,
                    @if (Lang::has(Session::get('lang_file').'.Area')!= '') {{  trans(Session::get('lang_file').'.Area') }} @else  {{ trans($OUR_LANGUAGE.'.Area') }} @endif{{ $d[2] }} @if (Lang::has(Session::get('lang_file').'.sqmeter')!= ''){{trans(Session::get('lang_file').'.sqmeter')}} @else  {{ trans($OUR_LANGUAGE.'.sqmeter')}} @endif
              @php }@endphp
               </div>


  <div class="hall-det-line"><span>@if (Lang::has(Session::get('lang_file').'.Hall_Type')!= '') {{  trans(Session::get('lang_file').'.Hall_Type') }}: @else  {{ trans($OUR_LANGUAGE.'.Hall_Type') }}: @endif </span>
    @php if($ShophallbasicInfo[0]->hall_type=='both'){     
      if (Lang::has(Session::get('lang_file').'.Men_and_Women')!= ''){ 
      $ht=trans(Session::get('lang_file').'.Men_and_Women'); 
    }else{
      $ht=trans($OUR_LANGUAGE.'.Men_and_Women');
    }
  }else{
      $ht=ucwords($ShophallbasicInfo[0]->hall_type);
} @endphp
  {{ $ht }}
 </div>

             <div class="hall-det-line hall-det-line-last"><span>@if(Lang::has(Session::get('lang_file').'.Hall_CAPICITY')!= ''){{trans(Session::get('lang_file').'.Hall_CAPICITY')}}@else{{trans($OUR_LANGUAGE.'.Hall_CAPICITY')}}@endif: </span> @if (Lang::has(Session::get('lang_file').'.FOR')!= '') {{  trans(Session::get('lang_file').'.FOR') }} @else  {{ trans($OUR_LANGUAGE.'.FOR') }} @endif {{ $ShophallbasicInfo[0]->hallcapicity }} @if (Lang::has(Session::get('lang_file').'.PEOPLE')!= '') {{  trans(Session::get('lang_file').'.PEOPLE') }} @else  {{ trans($OUR_LANGUAGE.'.PEOPLE') }} @endif</div>
            <div class="detail_hall_price">
                     
      @php if( $ShophallbasicInfo[0]->pro_discount_percentage >0){ @endphp   
<strike>  
 {{currency($ShophallbasicInfo[0]->pro_price, 'SAR',$Current_Currency)}}
 
    </strike> 
        
 {{currency($ShophallbasicInfo[0]->pro_disprice, 'SAR',$Current_Currency)}}
    

        @php }else{ @endphp 
     
 {{currency($ShophallbasicInfo[0]->pro_price, 'SAR',$Current_Currency)}}

      @php } @endphp 


    </div>

 
  @php if($ShophbasicInfo[0]->latitude!='' && $ShophbasicInfo[0]->longitude!=''){ @endphp
   @php 
     $lat=$ShophbasicInfo[0]->latitude;  
      $long=$ShophbasicInfo[0]->longitude;  @endphp 

          <div class="hall_map" id="map" width="450" height="230" style="height: 230px!important;"></div>
          @php }  @endphp

    
			 
			@php if(count($shopfacilities)>0){
      //echo "<pre>";
      //print_r($shopfacilities);
           @endphp
            <div class="common_title">@if (Lang::has(Session::get('lang_file').'.Facilities_Offered_Here')!= '') {{  trans(Session::get('lang_file').'.Facilities_Offered_Here') }} @else  {{ trans($OUR_LANGUAGE.'.Facilities_Offered_Here') }} @endif </div>
            <ul class="detail_facilites_list">
				
				 @foreach($shopfacilities as $shophallfacilities)
            	<li>
              {{ $shophallfacilities->option_title or '' }}</li>
				@endforeach
                
            </ul>
			@php } @endphp

       
        </div> 
        
    </div> <!-- service_detail_row -->
        
    <div class="service_container">
        <div class="services_detail">
		<span id="serviceerror" class="error" style="color:#FF0000"></span>
		<a name="prepaid"></a>
    @php $jk=0; @endphp
			 @foreach($shopproductoption as $shopmainoptions)
			 	 @php if(count($serviceoptionvalue[$jk])>0){ @endphp
					<div class="service_list_row">
            	
                <div class="common_title">{{ $shopmainoptions->option_title }}</div>
                
                
                 
                    <ul class="service_catagory">
            @foreach($serviceoptionvalue[$jk] as $shopsuboptions)
            @php $cprice=number_format($shopsuboptions->price,2,'.','') @endphp


            
                  <li><input name="service[]" type="checkbox" id="service{{$shopsuboptions->id}}" value="yes" onClick="return addservices(this.id,this.value,'{{$shopmainoptions->id}}','{{$shopsuboptions->id}}','{{$shopmainoptions->option_title}}','{{$shopsuboptions->option_title}}','{{currency($cprice, 'SAR',$Current_Currency, $format = false)}}');"><label for="service{{$shopsuboptions->id}}">{{ $shopsuboptions->option_title }}<span class="service_price"> {{ currency($shopsuboptions->price, 'SAR',$Current_Currency) }}  </span></label></li>
             @endforeach
                  
                </ul>





            </div>
			 
			  @php $jk++; } @endphp
			 @endforeach
        	@if($ShophallbasicInfo[0]->food_type!='')
        
		 <div class="service_list_row">
            	<a name="food"></a>
                <div class="common_title">@if (Lang::has(Session::get('lang_file').'.Food')!= '') {{  trans(Session::get('lang_file').'.Food') }} @else  {{ trans($OUR_LANGUAGE.'.Food') }} @endif</div>
                <ul class="service_catagory">
					@if($ShophallbasicInfo[0]->food_type=='external' || $ShophallbasicInfo[0]->food_type=='both')
                	<li><input name="foodtype" type="radio" id="foodservice025" value="external" onClick="return foodredirect(this.value);"><label for="foodservice025">@if (Lang::has(Session::get('lang_file').'.External_Food')!= '') {{  trans(Session::get('lang_file').'.External_Food') }} @else  {{ trans($OUR_LANGUAGE.'.External_Food') }} @endif</label></li>
					@endif
          @if($isinternalfood>0)
					@if($ShophallbasicInfo[0]->food_type=='internal' || $ShophallbasicInfo[0]->food_type=='both')
                    <li><input name="foodtype" type="radio" id="foodservice026" value="internal" onClick="return foodredirect(this.value);"><label for="foodservice026">@if (Lang::has(Session::get('lang_file').'.Internal_Food')!= '') {{  trans(Session::get('lang_file').'.Internal_Food') }} @else  {{ trans($OUR_LANGUAGE.'.Internal_Food') }} @endif</label></li>
					@endif
					@endif
                </ul>
            </div>
		@endif
		
            @php if(count($customerReviewOnProduct)>0){ @endphp
            <div class="service_list_row service_testimonial">
            	<a name="our_client"></a>
                <div class="common_title">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
                <div class="testimonial_slider">
                	<section class="slider">
                        <div class="flexslider1">
                          <ul class="slides">
						  @foreach($customerReviewOnProduct as $customerreview)
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="{{ $customerreview->cus_pic }}"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description">{{ $customerreview->comments }}</div>
                                    	<div class="testim_name">{{ $customerreview->cus_name }}</div>
                                        <div class="testim_star"><img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png"></div>
                                    </div>
                                </div>
                            </li>
					 @endforeach
                           
                          </ul>
                        </div>
                      </section>
                </div>
            </div>
            @php } @endphp

@php if($ShophallbasicInfo[0]->video_url!=''){ @endphp
            <div class="service_list_row service_testimonial video_hall">
                <a name="video" class="linking">&nbsp;</a>
        <div class="common_title">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif</div>
        <div class="service-video-area">
         <div class="service-video-cont">{{ $ShophallbasicInfo[0]->mc_video_description }}</div>
          <div class="service-video-box">
      
      
            <iframe class="service-video" src="{{ $ShophallbasicInfo[0]->video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>

</div>

@php } @endphp

        </div>
        
        <div class="services_listing">
        	<ul id="myContainer">
            	<li>
                	<span class="service_name">{{ $ShophallbasicInfo[0]->pro_title}}</span>
                    <span class="service_price"> {{ currency($productrealprice, 'SAR',$Current_Currency) }} </span>
                </li>
                @if($productInsuranceamount!='')
             	<li>
                  <span class="service_name">@if (Lang::has(Session::get('lang_file').'.Insurance_amount')!= '') {{  trans(Session::get('lang_file').'.Insurance_amount') }} @else  {{ trans($OUR_LANGUAGE.'.Insurance_amount') }} @endif</span>
                    <span class="service_price">{{ currency($productInsuranceamount, 'SAR',$Current_Currency) }}</span>
                </li>
                @endif
            </ul>
            @php if(isset($productInsuranceamount) && $productInsuranceamount!='') {$finalprice=$productrealprice+$productInsuranceamount; } else { $finalprice=$productrealprice; } @endphp
			<ul class="total_price_ul">
            	<li class="total_cost">
                	<span class="service_name">@if (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '') {{  trans(Session::get('lang_file').'.TOTAL_PRICE') }} @else  {{ trans($OUR_LANGUAGE.'.TOTAL_PRICE') }} @endif</span>
                    <span class="service_price">
                         @php $fprice=number_format($finalprice,2,'.','') @endphp
                     {{ Session::get('currency') }} <span id="fulltotal">{{ currency($fprice, 'SAR',$Current_Currency, $format = false) }} </span></span>
                </li>
            </ul>
			  {!! Form::open(['url' => 'additemstocart', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
			<div class="reserve_btn_row" id="rnow" style="display: block;">
          
        
      <input type="hidden" name="baseprice" id="baseprice" value="{{currency($productrealprice, 'SAR',$Current_Currency, $format = false)}}">
      <input type="hidden" name="total" id="total" value="{{currency($finalprice, 'SAR',$Current_Currency, $format = false)}}">
      <input type="hidden" name="productInsuranceamount" id="productInsuranceamount" value="{{  currency($productInsuranceamount, 'SAR',$Current_Currency, $format = false)}}">
      <input type="hidden" name="cart_type" id="cart_type" value="hall">
 
      <input type="hidden" name="shop_id" id="shop_id" value="{{request()->sid}}">
      <input type="hidden" name="branch_id" id="branch_id" value="{{request()->bid}}">
      <input type="hidden" name="product_id" id="product_id" value="{{request()->haid}}">
 @if($isbooked<1)

      <input type="hidden" name="food_type" id="food_type" value="internalfood">
            <input type="submit" name="submit" value="@if(Lang::has(Session::get('lang_file').'.Reserve_Now')!= ''){{trans(Session::get('lang_file').'.Reserve_Now')}}@else{{trans($OUR_LANGUAGE.'.Reserve_Now')}}@endif" class="reserve_now">
   @else
    
    <div class="reserve_btn_row" id="aleadybookednowcart">
        <span class="reserve_now">@if(Lang::has(Session::get('lang_file').'.Already_Booked_at')!= ''){{trans(Session::get('lang_file').'.Already_Booked_at')}}@else{{trans($OUR_LANGUAGE.'.Already_Booked_at')}}@endif</span>
    </div>

   @endif

        </div>
   {!! Form::close() !!}


        <div class="reserve_btn_row" id="foodnow" style="display: none;">
       @if($isbooked<1)   

        <a href="{{url('foodmenuitem/')}}/internal" class="reserve_now foodnow">@if(Lang::has(Session::get('lang_file').'.CONTINUE')!= ''){{trans(Session::get('lang_file').'.CONTINUE')}}@else{{trans($OUR_LANGUAGE.'.CONTINUE')}}@endif </a>        



        
          @else
    
   
        <span class="reserve_now">@if(Lang::has(Session::get('lang_file').'.Already_Booked_at')!= ''){{trans(Session::get('lang_file').'.Already_Booked_at')}}@else{{trans($OUR_LANGUAGE.'.Already_Booked_at')}}@endif</span>
   

        @endif
        </div>
<div class="hall_terms_conditions"><a  href="{{ $ShophbasicInfo[0]->terms_conditions or ''}}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}</a></div>


        </div>
        
        
    </div> <!-- service_container -->

	 @include('includes.other_services') <!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div> <!-- innher_wrap -->

   
    
</div> <!-- outer_wrapper -->

<div class="othrserv_overl"></div>
@include('includes.footer')

@php $lang = Session::get('lang_file'); @endphp
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content">{{ Session::get('status') }}<span id="showmsg">{{ (Lang::has(Session::get('lang_file').'.Hall_external_content')!= '')  ?  trans(Session::get('lang_file').'.Hall_external_content'): trans($OUR_LANGUAGE.'.Hall_external_content')}}</span></div>
    <div class="action_btnrow" align="center"> 
    
    <span id="showmsgab"><a class="action_yes status_yes" href="javascript:void(0);"> @if (Lang::has($lang.'.OK')!= '') {{  trans($lang.'.OK') }} @else {{ trans($LANUAGE.'.OK') }} @endif </a></span>
    </div>
  </div>
</div>
       
 
<script type="text/javascript">
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
 jQuery('html, body').animate({
  scrollTop: (jQuery('.service-display-left').first().offset().top)
 },500);
});
</script>



<script language="javascript">
function foodredirect(val){
  if(val=='external')
  {

jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
 
  }
  if (!jQuery("#foodservice026").is(":checked")) {
     document.getElementById("rnow").style.display = "";
     document.getElementById("foodnow").style.display = "none";  
     
  }else{
       document.getElementById("rnow").style.display = "none";
     document.getElementById("foodnow").style.display = "";
     
  }

  if (!jQuery("#foodservice025").is(":checked")) {
     document.getElementById("food_type").value = 'internalfood';
    
  }else{
       document.getElementById("food_type").value = 'extrnalfood';
       document.getElementById("rnow").style.display = "";
     document.getElementById("foodnow").style.display = "none";
  }

  

}
</script>





<script language="javascript">
  jQuery('input:checkbox').removeAttr('checked');
function validate_form()
{
valid = true;

if(jQuery('input[type=checkbox]:checked').length == 0)
{
    //alert ( "ERROR! Please select at least one service" );
//	document.getElementById("serviceerror").innerHTML='ERROR! Please select at least one service';
   // valid = false;
}

return valid;
}

</script>


<script language="javascript">

 function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }



function addservices(idval,val,mainserviceid,subserviceid,mainservicename,subservicename,subserviceprice){
//alert(val);
document.getElementById("serviceerror").innerHTML='';
var checkbox = jQuery( "#"+idval );
checkbox.val( checkbox[0].checked ? "no" : "yes" );
var cid=idval+''+subserviceid;
//var baseprice=document.getElementById('baseprice').value;
var baseprice=document.getElementById('total').value;

var productInsuranceamount=document.getElementById('productInsuranceamount').value;
<?php $Cur = Session::get('currency'); ?>
var formatedsubserviceprice=parseFloat(subserviceprice).toFixed(2);
	if(val=='yes'){
		var newprice=+baseprice + +subserviceprice;
		   var div = document.createElement('div');
    div.className = 'row';
div.innerHTML ='<li id='+cid+'><span class="service_name">'+subservicename+'</span><span class="service_price"><?php echo $Cur; ?> '+formatedsubserviceprice+'</span></li>';
   document.getElementById('myContainer').appendChild(div);
   
    jQuery.ajax({
         url: "{{url('branchhalldetail/setremoveservicessession')}}",
         data: { 'subserviceid': subserviceid,'isadd':'yes' }
    }); 
   
   
   }else{
    var toprice=document.getElementById('total').value+ +productInsuranceamount;
   
   	var newprice=+toprice - +subserviceprice;
     document.getElementById(cid).remove();
	  jQuery.ajax({
         url: "{{url('branchhalldetail/setremoveservicessession')}}",
         data: { 'subserviceid': subserviceid,'isadd':'no' }
    }); 
   }
   document.getElementById('total').value=newprice.toFixed(2);
		document.getElementById("fulltotal").innerHTML=newprice.toFixed(2);
}

function removeRow(input) {
    document.getElementById('myContainer').removeChild(input.parentNode);
}
</script>




<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function() {
  var showChar = 200;
  var ellipsestext = "...";
var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });
});
</script>

<script>
    jQuery(function(){
      // bind change event to select
      jQuery('#dynamic_select').on('change', function () {
          var url = jQuery(this).val(); // get selected value          
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
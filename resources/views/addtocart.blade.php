@include('includes.navbar')
<div class="outer_wrapper">
<div class="inner_wrap">
@include('includes.header')
@php 
//print_r(Session::get('searchdata'));
//echo "<pre>";
//print_r($hall_data);

@endphp

<div class="search-section">
 <div class="event-result-form">
<div class="search-box search-box1">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')}}</div>
<div class="search-noneedit"> {{ $otype }}</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_ATTENDANCE'): trans($OUR_LANGUAGE.'.NO_OF_ATTENDANCE')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.noofattendees')}}</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}</div>
<div class="search-noneedit">SAR {{ Session::get('searchdata.budget')}}</div>
</div> <!-- search-box -->

<!--<div class="search-box search-box4">
<div class="search-box-label">Gender</div>
<div class="search-noneedit">Female</div>
</div>--> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')}}</div>
<div class="search-noneedit">{{ $hall_data[0]['city'] }}</div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.occasiondate')}}</div>
</div> 
<div class="search-box search-box6">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.gender')}}</div>
</div>
<!-- search-box -->

 
</div> <!-- modify-search-form -->
 
</div>


<div class="tatal-services-area">
<div class="mobile-filter-line"> 
<div class="mobile-search mobile-search1row"><span>Event Result</span></div>
</div>
<div class="tatal-services-inner-area">
<div class="tatal-services-heading">{{ (Lang::has(Session::get('lang_file').'.TOTAL_SERVICES')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_SERVICES'): trans($OUR_LANGUAGE.'.TOTAL_SERVICES')}}</div>


<div class="table-cart-wrapper">
<table class="table-cart tatal-services-table">
        <tr class="tr table_heading_tr">
          <td class="table_heading">Sl. No.</td>
          <td class="table_heading">{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}}</td>
          <td class="table_heading">{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}</td>
          <td class="table_heading">{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}</td>
        </tr>       	 
 <tr class="tr"><td colspan="4">

 	<div class="demo">

    
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                       
                        <table width="100%"><tr class="tr">
	 <td class="td td1" data-title="Sl. No.">1<!--<button>Show Detail</button>--></td>
	  <td class="td td2" data-title="Services">{{ $productinfo[0]->pro_title }} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
	    <td class="td td3" data-title="Service Provider">{{ $mkbasecatgory }} </td>
 <td class="td td4" data-title="Price">SAR 
 @php if($productinfo[0]->pro_disprice !=''){ $hallprice=$productinfo[0]->pro_disprice; }else{ $hallprice=$productinfo[0]->pro_price; }
 $basetotal=number_format($hallprice, 2);
  @endphp
{{ number_format($hallprice, 2) }}</td>

	 </tr></table>
                    </a>
                </h4>
            </div>

            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                      <table width="100%" class="child-table">
 @php $k=1; $ij=1; if(count($productservices)>0){ @endphp 
@foreach($productservices as $adopedproductservices)
 @php $basetotal=$basetotal+$adopedproductservices->price; @endphp
<tr class="tr">
  <td class="td td1" data-title="">&nbsp{{ $k }}</td>
  <td class="td td2" data-title="Services">{{ $adopedproductservices->option_title }}<!-- <a href="#" class="services-edit">&nbsp;</a>--></td>
  <td class="td td3" data-title="Service Provider">Hall Services</td>
 <td class="td td4" data-title="Price">SAR {{ number_format($adopedproductservices->price, 2) }}</td>
	 </tr>
	  @php $k++; $ij++; @endphp
	@endforeach
	  @php } if(count($internal_foods)>0){ @endphp
	@foreach($internal_foods as $internalfood)
	
	<tr class="tr">
	 <td class="td td1" data-title="Sl. No.">&nbsp;{{ $k }}</td>
	  <td class="td td2" data-title="Services">{{ $internalfood['dish_name'] }}<br />
{{ $internalfood['container_title'] }} X {{ $internalfood['quantity'] }} <!--<a href="#" class="services-edit">&nbsp;</a>--></td>
	    <td class="td td3" data-title="Service Provider">{{ $internalfood['menu_name'] }}</td>
 <td class="td td4" data-title="Price">SAR {{ number_format($internalfood['final_price'], 2) }}</td>
	 </tr>
	 @php $basetotal=$basetotal+$internalfood['final_price']; @endphp
	  @php $k++; @endphp
	@endforeach
@php } @endphp

</table>
                </div>
            </div>
        </div>

        

    </div><!-- panel-group -->
    
    
</div>
</td>
 </tr>






	







<tr class="tr total-prise">
	 
 <td class="td td4" data-title="Total" colspan="4"><span>{{ (Lang::has(Session::get('lang_file').'.TOTAL')!= '')  ?  trans(Session::get('lang_file').'.TOTAL'): trans($OUR_LANGUAGE.'.TOTAL')}}</span> {{ number_format($basetotal, 2) }}</td>
	 </tr>	
 </table> <!-- table -->
 </div>
 
 





 
 
<div class="tatal-services-pay-area"><a href="" class="form-btn" >Continue</a> <input type="submit" value="Pay Now" class="form-btn" /></div> <!-- tatal-services-pay-area -->

</div> <!-- tatal-services-inner-area -->
</div>







</div> <!-- outer_wrapper -->
</div>
 
<script>
jQuery(document).ready(function(){
    jQuery("button").click(function(){
        jQuery('#halldetails').toggle();
    });
});
</script>





@include('includes.footer')

<script type="text/javascript">
	
	function toggleIcon(e) {
    jQuery(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);

</script>

<style type="text/css">
	
/*******************************
* Does not work properly if "in" is added after "collapse".
* Get free snippets on bootpen.com
*******************************/
    .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 15px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }


</style>
@include('includes.navbar')

<div class="outer_wrapper">
@include('includes.header')
 <div class="inner_wrap"> 
<div class="homepage_category">
@php $categorycount =1; @endphp
@foreach($getbasecategory as $val)
 @php
                    
					if($categorycount==1){
                    $mainclass = 'meeting_category';
                    $subclass = '';
                    $link_class = 'bussiness_link';
                    } else {
                    $mainclass = 'wedding_category';
                    $dstatus = 'flr';
                    $link_class = 'wedding_ocaasion';
                    }
                    @endphp 


@php if(Session::get('lang_file')=='ar_lang'){ $linkbtninfo=$val->mc_name_ar; }else{ $linkbtninfo=$val->mc_name;} @endphp
		<div class="{{ $mainclass }}">
      <div class="homepage_col2 {{ $subclass }}">         
            <div class="homepage_img">
              @if (Session::get('customerdata.token')=='')
          <a  href="{{url('login-signup')}}" style="color:#FFFFFF; text-decoration:none;"> <img src="{{ $val->mc_img}}" alt="{{ $val->mc_name }}"> </a>
        @else
               <a class="{{ $link_class }}" href="javascript:void(0);" onclick="postmastercategory({{ $val->mc_id }});" style="color:#FFFFFF; text-decoration:none;"> <img src="{{ $val->mc_img}}" alt="{{ $val->mc_name }}"> </a>
         @endif
              
            </div>            
        </div>
        <div class="info_btn {{ $link_class }}" onclick="postmastercategory({{ $val->mc_id }});">
				@if (Session::get('customerdata.token')=='')
				  <a  href="{{url('login-signup')}}" style="color:#FFFFFF; text-decoration:none;">{{ $linkbtninfo }} </a>
				@else
               <a class="{{ $link_class }}" href="javascript:void(0);" onclick="postmastercategory({{ $val->mc_id }});" style="color:#FFFFFF; text-decoration:none;">{{ $linkbtninfo }} </a>
			   @endif
            </div>
    </div>
@php $categorycount++; @endphp
 @endforeach 

            
</div> <!-- homepage_category -->


</div> <!-- innher_wrap -->
<div class="flower_bg"></div>
   
    
</div> <!-- outer_wrapper -->
@if (Session::get('customerdata.token')!='')
<div class="search_popup bussiness_search_popup">
<a href="javascript:void(0);" class="close_search">X</a>
   <div class="form_title">{{ (Lang::has(Session::get('lang_file').'.BusinessMeetings')!= '')  ?  trans(Session::get('lang_file').'.BusinessMeetings'): trans($OUR_LANGUAGE.'.BusinessMeetings')}}</div>
    @include('includes.search')
</div> <!-- search_popup -->  

<div class="search_popup wedding_search_popup">
	<a href="javascript:void(0);" class="close_search">X</a>
   <div class="form_title">{{ (Lang::has(Session::get('lang_file').'.WeddingandOccasions')!= '')  ?  trans(Session::get('lang_file').'.WeddingandOccasions'): trans($OUR_LANGUAGE.'.WeddingandOccasions')}}</div>
    @include('includes.searchweddingandoccasions')
</div> <!-- search_popup -->  

@endif
<!-- MainBody End -->
<!-- Footer -->
@include('includes.footer')
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.js"></script>
<script language="javascript">
function postmastercategory(val){
 			
		
    var categoryID = val;    
    if(categoryID){
        $.ajax({
           type:"GET",
           url:"{{url('getbussinessbycategoryid')}}?category_id="+categoryID,
           success:function(res){               
            if(res){
					if(categoryID==1){
						$("#bussinesslist").empty();
						$("#bussinesslist").append('<option value="">{{ (Lang::has(Session::get('lang_file').'.Select_Business_Type')!= '')  ?  trans(Session::get('lang_file').'.Select_Business_Type'): trans($OUR_LANGUAGE.'.Select_Business_Type')}}</option>');
						$.each(res,function(key,value){
								$("#maincategoryid").val(categoryID);
								$(".mysel").val(categoryID);
							
							$("#bussinesslist").append('<option value="'+res[key].id+'">'+res[key].title+'</option>');
						});
           		}
				if(categoryID==2){
						$("#weddingandoccasionlist").empty();
						$("#weddingandoccasionlist").append('<option value="">{{ (Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '')  ?  trans(Session::get('lang_file').'.Select_Occasion_Type'): trans($OUR_LANGUAGE.'.Select_Occasion_Type')}}</option>');
						$.each(res,function(key,value){
						//alert(res[key].title);
								
								$("#basecategoryid").val(categoryID);
								$(".mysel").val(categoryID);
								
							$("#weddingandoccasionlist").append('<option value="'+res[key].id+'" >'+res[key].title+'</option>');
						});
           		}
            }else{
               $("#bussinesslist").empty();
			    $("#weddingandoccasionlist").empty();
            }
           }
        });
    }else{
        $("#bussinesslist").empty();
		 $("#weddingandoccasionlist").empty();
         }      
   
	 
}

</script>
@include('includes.genralpopup')
<script type="text/javascript">
 function postcitypop(str)
 {
  if(str==2)
  {
 jQuery('.citypop').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
  }


 }

 
 

</script>
@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')}}</h1>
      <div class="field_group top_spacing_margin_occas">
        <div class="main_user my_padd">
          <div class="add_sar">{{ (Lang::has(Session::get('lang_file').'.ADD_SAR')!= '')  ?  trans(Session::get('lang_file').'.ADD_SAR'): trans($OUR_LANGUAGE.'.ADD_SAR')}} </div>
          <div class="wallet_balance">{{ (Lang::has(Session::get('lang_file').'.WALLET_BALANCE')!= '')  ?  trans(Session::get('lang_file').'.WALLET_BALANCE'): trans($OUR_LANGUAGE.'.WALLET_BALANCE')}} 
          <span>
           <?php 
            $userid  = Session::get('customerdata.user_id');
            $getInfo = Helper::getuserinfo($userid); echo number_format($getInfo->wallet,2);
            ?>
          </span>
          </div>
          <div class="choose_amt">{{ (Lang::has(Session::get('lang_file').'.CHOOSE_AN_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.CHOOSE_AN_AMOUNT'): trans($OUR_LANGUAGE.'.CHOOSE_AN_AMOUNT')}}</div>
          <div class="amt_box">
            <ul>
              <li>SAR 1,000</li>
              <li>SAR 2,000</li>
              <li>SAR 5,000</li>
              <li>SAR 10,000</li>
            </ul>
          </div>
          <div class="amt_input">
            <input name="amt_input" id="amt_input" type="text" value="" placeholder="{{ (Lang::has(Session::get('lang_file').'.ENTER_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.ENTER_AMOUNT'): trans($OUR_LANGUAGE.'.ENTER_AMOUNT')}}" />
          </div>
          <div class="payment_method_my">
            <div class="pay_head">{{ (Lang::has(Session::get('lang_file').'.MY_PAYMENT_METHOD')!= '')  ?  trans(Session::get('lang_file').'.MY_PAYMENT_METHOD'): trans($OUR_LANGUAGE.'.MY_PAYMENT_METHOD')}}</div>
            <div class="pay_area">
              <div class="cred">
                <input id="male" name="re" type="radio">
                <label for="male" class="crd_text">{{ (Lang::has(Session::get('lang_file').'.CREDIT_CARD')!= '')  ?  trans(Session::get('lang_file').'.CREDIT_CARD'): trans($OUR_LANGUAGE.'.CREDIT_CARD')}}</label>
                <span class="crds"><img src="{{ url('') }}/themes/images/card.jpg" alt="" /></span> </div>
              <div class="cred">
                <input id="DebitCard" name="re" type="radio">
                <label for="DebitCard" class="crd_text">{{ (Lang::has(Session::get('lang_file').'.DEBIT_CARD')!= '')  ?  trans(Session::get('lang_file').'.DEBIT_CARD'): trans($OUR_LANGUAGE.'.DEBIT_CARD')}}</label>
                <span class="crds">
                <select name="debit_card" id="debit_card">
                  <option value="">{{ (Lang::has(Session::get('lang_file').'.CHOOSE_AN_OPTION')!= '')  ?  trans(Session::get('lang_file').'.CHOOSE_AN_OPTION'): trans($OUR_LANGUAGE.'.CHOOSE_AN_OPTION')}}</option>
                </select>
                </span> </div>
              <div class="cred">
                <input id="netbank" name="re" type="radio">
                <label for="netbank" class="crd_text">{{ (Lang::has(Session::get('lang_file').'.NET_BANKING')!= '')  ?  trans(Session::get('lang_file').'.NET_BANKING'): trans($OUR_LANGUAGE.'.NET_BANKING')}}</label>
                <span class="crds">
                <select name="netbank" id="netbank">
                  <option value="">{{ (Lang::has(Session::get('lang_file').'.CHOOSE_AN_OPTION')!= '')  ?  trans(Session::get('lang_file').'.CHOOSE_AN_OPTION'): trans($OUR_LANGUAGE.'.CHOOSE_AN_OPTION')}}</option>
                </select>
                </span> </div>
            </div>
            <div class="cont">
              <input type="submit" name="submit" class="form-btn" value="{{ (Lang::has(Session::get('lang_file').'.CONTINUE')!= '')  ?  trans(Session::get('lang_file').'.CONTINUE'): trans($OUR_LANGUAGE.'.CONTINUE')}}" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
<div class="nav_row">
    <div class="heamburger">
        <span></span>
        <span></span>
        <span></span>
    </div><!-- heamburger -->
 
        <div class="left_nav">
            <ul>
                <li><a href="{{route('about-us')}}">{{ (Lang::has(Session::get('lang_file').'.ABOUTUS')!= '')  ?  trans(Session::get('lang_file').'.ABOUTUS'): trans($OUR_LANGUAGE.'.ABOUTUS')}}</a></li>
                <li><a href="{{route('testimonials')}}">{{ (Lang::has(Session::get('lang_file').'.Testimonials')!= '')  ?  trans(Session::get('lang_file').'.Testimonials'): trans($OUR_LANGUAGE.'.Testimonials')}}</a></li>
                <li><a href="{{route('occasions')}}">#{{ (Lang::has(Session::get('lang_file').'.Occasions')!= '')  ?  trans(Session::get('lang_file').'.Occasions'): trans($OUR_LANGUAGE.'.Occasions')}}</a></li>
                @if (Session::get('customerdata.token')!='')
                <span class="mobile_fields"><li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li></span>
				@endif
            </ul>
        </div>
        <div class="middle_nav">
            <a class="brand" href="{{ url('index') }}">
            <img src="{{ url('') }}/themes/images/logo.png" />
            </a>
        </div>
        <div class="right_nav">
            <span class="mobile_fields">
 

                <a class="mob_lang" href="#">Arabic</a>
				@if (Session::get('customerdata.token')=='')
				<a href="{{url('login-signup')}}" <?php if(Route::getCurrentRoute()->uri() == 'index' || Route::getCurrentRoute()->uri() == '/') { ?> class="active" <?php } ?>>
				@else
				<a href="{{url('my-account')}}" <?php if(Route::getCurrentRoute()->uri() == 'index' || Route::getCurrentRoute()->uri() == '/') { ?> class="active" <?php } ?>>
				@endif
                    <img src="{{ url('') }}/themes/images/login.png" />
                </a>
            </span>
            <a href="#" class="basket_icon"><span class="count">5</span></a>
            <ul>                   
					@if (Session::get('customerdata.token')=='')
                <li><a href="{{url('login-signup')}}">{{ (Lang::has(Session::get('lang_file').'.SignIn')!= '')  ?  trans(Session::get('lang_file').'.SignIn'): trans($OUR_LANGUAGE.'.SignIn')}} / {{ (Lang::has(Session::get('lang_file').'.SignUp')!= '')  ?  trans(Session::get('lang_file').'.SignUp'): trans($OUR_LANGUAGE.'.SignUp')}}</a></li>
				@else
                <li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li>
				@endif
                <li> 
                <a href="#">
                @if(Session::get('lang_file') == 'ar_lang')
                <span class="lang_select" data-val="en"  onclick="Lang_change('en')">English</span>     
                @else        
                <span class="lang_select" data-val="ar" onclick="Lang_change('ar')">{{ (Lang::has(Session::get('lang_file').'.ARABIC')!= '')  ?  trans(Session::get('lang_file').'.ARABIC'): trans($OUR_LANGUAGE.'.ARABIC')}}</span>
                @endif 
                </a>
                </li>
                <li><a href="{{route('contact-us')}}">{{ (Lang::has(Session::get('lang_file').'.ContactUs')!= '')  ?  trans(Session::get('lang_file').'.ContactUs'): trans($OUR_LANGUAGE.'.ContactUs')}}</a></li>
                <li><a href="{{route('my-account')}}">{{ (Lang::has(Session::get('lang_file').'.MyAccount')!= '')  ?  trans(Session::get('lang_file').'.MyAccount'): trans($OUR_LANGUAGE.'.MyAccount')}}</a></li>

            </ul>
            <div class="welcome_message">{{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}}  <span>@if(Session::get('customerdata.user_name')=='')
			 Guest 
			 @else 
			 {{Session::get('customerdata.user_name')}}
			 @endif </span></div>
            
        </div>
    </div><!-- nav_row -->


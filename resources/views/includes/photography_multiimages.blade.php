@php
if(isset($pro_id) && $pro_id!=''){
$getallimage = Helper::getAllimages($pro_id);
$getmainimage = Helper::getmainimages($pro_id);
@endphp
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/jquery.magnific-popup.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.0.0/magnific-popup.min.css" rel="stylesheet" />
	<div class="photographi-left-area" id="slideTwo">
	<ul class="slides test">
	@foreach($getmainimage as $imgsas)
	<li><a class="popup-link" href="{{str_replace('thumb_','',$imgsas->pro_Img)}}"><img src="{{str_replace('thumb_','',$imgsas->pro_Img)}}" alt=""></a></li>
	@endforeach
		
	@foreach($getallimage as $imgsa)
	<li><a class="popup-link" href="{{str_replace('thumb_','',$imgsa->image)}}"><img src="{{str_replace('thumb_','',$imgsa->image)}}" alt=""></a></li>
	@endforeach
	</ul>
	</div>
	<script type="text/javascript">
		
jQuery('.popup-link').magnificPopup({
          type: 'image',
          gallery:{enabled:true}
        });

jQuery('.photographi-left-area').flexslider({
                    animation: "slide",
                    controlNav: true,
                    directionNav: false,
                    animationLoop: true,
                    slideshow: true,
                    touch: true,
                    slideshowSpeed: 3000,     
					animationSpeed: 600, 
               });
 	
	</script>
@php
}
@endphp


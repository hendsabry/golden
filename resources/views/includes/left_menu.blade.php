@php 
$cururl = request()->segment(count(request()));
//echo $cururl;
@endphp
<div class="myaccount_left">
    <ul>
        <li><a href="{{route('my-account-profile')}}"@php if($cururl=='my-account-profile') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')}}</a></li>
        <li><a href="{{route('my-account-ocassion')}}"@php if($cururl=='my-account-ocassion' || $cururl=='order-details' || $cururl=='serviceorder') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ORDERS')!= '')  ?  trans(Session::get('lang_file').'.MY_ORDERS'): trans($OUR_LANGUAGE.'.MY_ORDERS')}}</a></li>
        <li><a href="{{route('my-account-studio')}}"@php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')}}</a></li>
        <li><a href="{{route('my-account-security')}}"@php if($cururl=='my-account-security') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')}}</a></li>
        <li><a href="{{route('my-account-wallet')}}"@php if($cururl=='my-account-wallet') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')}}</a></li>
        <li><a href="{{route('my-account-review')}}"@php if($cururl=='my-account-review' || $cururl=='reviewdisplay') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')}}</a></li>
        <li><a href="{{route('my-request-a-quote')}}"@php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')}}</a></li>
        <li><a href="{{route('change-password')}}"@php if($cururl=='change-password') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}</a></li>        
    </ul>
</div>
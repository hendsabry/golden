@php $lang = Session::get('lang_file'); @endphp
<div class="action_popup">
  <div class="action_active_popup">
  <!--<div class="close_rent">X</div>-->
    <div class="action_content add_pop"><span class="succ_addd">{{ Session::get('status') }}</span><span id="showmsg" style="display:none;">{{ (Lang::has(Session::get('lang_file').'.SORRY_QTY_PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.SORRY_QTY_PRODUCT'): trans($OUR_LANGUAGE.'.SORRY_QTY_PRODUCT')}}</span></div>
    <div class="action_btnrow bdr_top_pop" align="center"><input type="hidden" id="delid" value=""/>
      <span id="hidemsgab"><a class="action_yes status_yes contis" href="javascript:void(0);"> @if (Lang::has($lang.'.CONTINUE_SHOPPING')!= '') {{  trans($lang.'.CONTINUE_SHOPPING') }} @else {{ trans($LANUAGE.'.CONTINUE_SHOPPING') }} @endif </a> <a class="action_yes yes" href="{{url('mycart')}}"> @if (Lang::has($lang.'.VIEW_CART')!= '') {{  trans($lang.'.VIEW_CART') }} @else {{ trans($LANUAGE.'.VIEW_CART') }} @endif</a></span>
	  <span id="showmsgab" style="display:none;"><a class="action_yes status_yes yes" href="javascript:void(0);"> @if (Lang::has($lang.'.OK')!= '') {{  trans($lang.'.OK') }} @else {{ trans($LANUAGE.'.OK') }} @endif </a></span>
	  </div>
  </div>
</div>
       
@if(Session::has('status'))
<script type="text/javascript">
jQuery(document).ready(function()
{
 jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
});
jQuery('.overlay, .close_rent').click(function(){
jQuery('.action_popup').fadeOut(500);
 jQuery('.overlay').fadeOut(500);
 jQuery('.action_popup').hide();
})


</script>
@endif
<script type="text/javascript">
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
 jQuery('html, body').animate({
	scrollTop: (jQuery('.service-display-left').first().offset().top)
 },500);
});
</script>


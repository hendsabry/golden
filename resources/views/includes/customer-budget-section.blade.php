
<div class="customer-budget-section">

<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
<div class="content mCustomScrollbar">
<div class="customer-budget-box">

<div class="customer-product-name">Halls</div>
<div class="customer-budget-line">
<div class="customer-subproduct-name budgetopen">Hotel Halls</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Wedding Halls</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Wedding Halls (Small)</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->
<div class="budget-box-divder">&nbsp;</div>

</div> <!-- customer-budget-box -->

<div class="customer-budget-box">
<div class="customer-product-name">The Food</div>
<div class="customer-budget-line">
<div class="customer-subproduct-name">Buffet</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Desert</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Dates</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->
<div class="budget-box-divder">&nbsp;</div>
</div> <!-- customer-budget-box -->

<div class="customer-budget-box">
<div class="customer-product-name">Occasion Co-ordinator</div>
<div class="customer-budget-line">
<div class="customer-subproduct-name">Photography Studio</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Reception and Hospitality</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Electronic Invitations</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Cosha</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Roses</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->

<div class="customer-budget-line">
<div class="customer-subproduct-name">Special Events</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>
</div> <!-- customer-budget-line -->
<div class="budget-box-divder">&nbsp;</div>
</div> <!-- customer-budget-box -->

<div class="customer-budget-box">
<div class="customer-product-name">Car Rentals</div>
<div class="customer-budget-line">
<div class="customer-subproduct-name">Car Rentals</div>
<div class="budget-range-line">
<div class="budget-range-box1"><input type="text" class="budget-t" /></div>
<div class="budget-range-divder">&nbsp;</div>
<div class="budget-range-box2"><input type="text" class="budget-t" /></div>
</div>

</div> <!-- customer-budget-line -->
<div class="budget-box-divder">&nbsp;</div>
 

</div> <!-- customer-budget-box -->

</div>
</div>

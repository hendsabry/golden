@php 
$currentpage=Route::getCurrentRoute()->uri();
$issession= count(Session::get('searchdata'));
if($currentpage == '/'){ $page='home'; }else{ $page='inner';}
if($issession>0 && $page!='home'){ $emodify='modify-extra';  }else{ $emodify='';}
 @endphp
<div class="after-modify-search {{ $emodify }}" @php if($page=='inner'){ @endphp style="display:none" @php } @endphp> 
<form action="{{route('search-results')}}" name="searchweddingoccasion2" id="searchweddingoccasion2">
<div class="search-form">
<div class="search-box search-box1">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')}}</div>
<div class="search-box-field">
<span>
<?php //$bussinesstype; ?>
@php $setOccasion = Helper::getAllOccasionNameList(2); @endphp
<select id="weddingandoccasionlist" name="weddingoccasiontype">
 <option value="">@if (Lang::has(Session::get('lang_file').'.Select')!= '') {{  trans(Session::get('lang_file').'.Select') }} @else  {{ trans($OUR_LANGUAGE.'.Select') }} @endif</option>
@php foreach($setOccasion as $woevents){ @endphp
@php if(Session::get('searchdata.weddingoccasiontype')==$woevents->id) { $occasionselected='selected'; }else{ $occasionselected='';} 
  if(Session::get('lang_file') !='en_lang')
 {
   $otitle_name= 'title_ar';
 }
 else
 {
    $otitle_name= 'title'; 
 } 
@endphp
<option value="{{$woevents->id}}" {{ $occasionselected }}  >{{ $woevents->$otitle_name}} </option>
 @php } @endphp
</select>
</span>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')}}</div>
<div class="search-box-field"><input type="text" class="search-t-box" name="noofattendees"  value="{{ Session::get('searchdata.noofattendees') }}" id="noofattendees" maxlength="12" onkeypress="return isNumberKey(event)"/></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}</div>
<div class="search-box-field"><input type="text" class="search-t-box" name="budget" value="{{ Session::get('searchdata.budget') }}" id="budget" maxlength="12" onkeypress="return isNumberKey(event);" placeholder="{{ Session::get('currency') }}" /><span id="berror"></span></div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')}}</div>
<div class="search-box-field">
 <select name="cityid" id="cityid">
    <option value="">@if (Lang::has(Session::get('lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif
	</option>
               @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="color: #d2cece;">@if($selected_lang_code !='en') {{$cbval->co_name_ar}} @else  {{$cbval->co_name}}   @endif</option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                         @php  
  
                          if(Session::get('searchdata.cityid')!=''){ 
                          $user_currentcity   = Session::get('searchdata.cityid');  
                          }else {
                          $user_currentcity   = Session::get('user_currentcity');
                          }
                              
                        $cityID = $val->ci_id;
 

                            @endphp
                        @if($selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" <?php if($user_currentcity == $cityID){  echo "SELECTED";}  ?>>{{ $val->$ci_name }}</option>
                        @endforeach
                        @endforeach
                    
              </select>
</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')}}</div>
<div class="search-box-field"><input type="text" class="search-t-box cal-t" name="occasiondate" id="DA" value="{{ Session::get('searchdata.occasiondate') }}" readonly /></div>
</div> <!-- search-box -->



<div class="search-box search-box8">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')}}</div>
<div class="search-box-field">
 <select name="gender">
    <option value="">@if (Lang::has(Session::get('lang_file').'.Select')!= '') {{  trans(Session::get('lang_file').'.Select') }} @else  {{ trans($OUR_LANGUAGE.'.Select') }} @endif</option>
    <option value="male" @php if(Session::get('searchdata.gender')=='male'){ @endphp selected="selected" @php } @endphp>@if (Lang::has(Session::get('lang_file').'.MALE')!= '') {{  trans(Session::get('lang_file').'.MALE') }} @else  {{ trans($OUR_LANGUAGE.'.MALE') }} @endif</option>
	<option value="female" @php if(Session::get('searchdata.gender')=='female'){ @endphp selected="selected" @php } @endphp>@if (Lang::has(Session::get('lang_file').'.FEMALE')!= '') {{  trans(Session::get('lang_file').'.FEMALE') }} @else  {{ trans($OUR_LANGUAGE.'.FEMALE') }} @endif</option>
	<option value="Both" @php if(Session::get('searchdata.gender')=='Both'){ @endphp selected="selected" @php } @endphp>@if (Lang::has(Session::get('lang_file').'.BOTH')!= '') {{  trans(Session::get('lang_file').'.BOTH') }} @else  {{ trans($OUR_LANGUAGE.'.BOTH') }} @endif</option>
	 </select>
</div>
</div>

 <!-- search-box -->

<div class="search-btn">
<input type="hidden" name="basecategoryid" id="basecategoryid" value="{{Session::get('searchdata.basecategoryid')}}" />
<input type="hidden" name="maincategoryid" id="" value="{{Session::get('searchdata.maincategoryid')}}" />
<input type="hidden" name="mainselectedvalue" class="mysel" value="{{Session::get('searchdata.mainselectedvalue')}}"/>
<input type="hidden" name="language_type" id="language_type" value="{{Session::get('lang_file')}}" />
<input type="submit" class="form-btn" value="{{ (Lang::has(Session::get('lang_file').'.SEARCH')!= '')  ?  trans(Session::get('lang_file').'.SEARCH'): trans($OUR_LANGUAGE.'.SEARCH')}}" name="submit" /></div>
</div> <!-- search-form -->

</form>
</div>

@php if($issession>0 && $page!='home'){ @endphp
<div class="modify-search-form">
<div class="search-box search-box1">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.OccasionType')!= '')  ?  trans(Session::get('lang_file').'.OccasionType'): trans($OUR_LANGUAGE.'.OccasionType')}}</div>
<div class="search-noneedit">
<?php 
 $oid = Session::get('searchdata.weddingoccasiontype');
 $getoccasiontype = Helper::business_occasion_type($oid);
 if(count($getoccasiontype) > 0){
 if(Session::get('lang_file') !='en_lang')
 {
   $title_name= 'title_ar';
 }
 else
 {
    $title_name= 'title'; 
 }  
 echo $getoccasiontype->$title_name;
 } ?>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.noofattendees') }}</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.budget') }}</div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')}}</div>
<div class="search-noneedit">
@php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; @endphp </div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')}}</div>
<div class="search-noneedit">{{ Session::get('searchdata.occasiondate') }} </div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')}}</div>
<div class="search-noneedit">
 @php if(Session::get('searchdata.gender')=='male'){ 
		if (Lang::has(Session::get('lang_file').'.MALE')!= '') { echo trans(Session::get('lang_file').'.MALE'); } else  { echo trans($OUR_LANGUAGE.'.MALE'); }
}
@endphp

@php if(Session::get('searchdata.gender')=='female'){ 
 if (Lang::has(Session::get('lang_file').'.FEMALE')!= '') { echo trans(Session::get('lang_file').'.FEMALE'); } else  { echo trans($OUR_LANGUAGE.'.FEMALE'); }
}
@endphp
@php if(Session::get('searchdata.gender')=='Both'){ 
if (Lang::has(Session::get('lang_file').'.BOTH')!= '') { echo trans(Session::get('lang_file').'.BOTH'); } else  { echo trans($OUR_LANGUAGE.'.BOTH'); }
}
@endphp
</div>
</div> <!-- search-box -->
@if(!Request::is('checkout')) 
<div class="search-btn"><a href="javascript:void(0)" class="form-btn" >{{ (Lang::has(Session::get('lang_file').'.ModifySearch')!= '')  ?  trans(Session::get('lang_file').'.ModifySearch'): trans($OUR_LANGUAGE.'.ModifySearch')}}</a></div>
@endif


</div>
@php } @endphp

 <script type="text/javascript">
            // When the document is ready
			var jql = jQuery.noConflict();
            jql(document).ready(function () {
                jql('#DA').datepicker({
                    //format: "dd/mm/yyyy",
					format: "d M, yyyy",
                    endDate: '+14m',
					autoclose: true,
					           startDate: new Date(),
                }); 
                jql("#DA").datepicker("setDate", new Date());
            });
 </script>

@if(!Request::is('checkout')) 

<script>     
       jQuery("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
jQuery("#searchweddingoccasion2").validate({
                  ignore: [],
                  rules: {				  	
					   cityid: {
                       required: true,
                      },					   
                  },
                 highlight: function(element) {
                  jQuery(element).removeClass('error');
                },
             
           messages: {
		   		  cityid: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= '')  ?  trans(Session::get('lang_file').'.ENTER_YOUR_CITY'): trans($OUR_LANGUAGE.'.ENTER_YOUR_CITY'); @endphp",
                      },	  
                     
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@endif


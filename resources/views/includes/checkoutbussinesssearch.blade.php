@php 
$currentpage=Route::getCurrentRoute()->uri();
$issession= count(Session::get('searchdata'));
  $page='inner'; 
  $emodify=''; 
 @endphp
 

<div class="modify-search-form-90">
<div class="search-box search-box1">



 
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.TypeofBusiness')!= '')  ?  trans(Session::get('lang_file').'.TypeofBusiness'): trans($OUR_LANGUAGE.'.TypeofBusiness')}}</div>
<div class="search-box-field">
{{ Session::get('otype')}}
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.NumberofAttendees')!= '')  ?  trans(Session::get('lang_file').'.NumberofAttendees'): trans($OUR_LANGUAGE.'.NumberofAttendees')}}</div>
<div class="search-box-field">{{ Session::get('searchdata.noofattendees')}}</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}</div>
<div class="search-box-field">{{ Session::get('currency') }} {{ Session::get('searchdata.budget')}} </div>
</div> <!-- search-box -->

<div class="search-box search-box4">  
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')}}</div>
<div class="search-box-field">
 @php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; @endphp
</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.StartDate')!= '')  ?  trans(Session::get('lang_file').'.StartDate'): trans($OUR_LANGUAGE.'.StartDate')}}</div>
<div class="search-box-field">{{ Session::get('searchdata.startdate') }}</div>
</div> <!-- search-box -->


 <div class="search-box search-box6">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.EndDate')!= '')  ?  trans(Session::get('lang_file').'.EndDate'): trans($OUR_LANGUAGE.'.EndDate')}}</div>
<div class="search-box-field">{{ Session::get('searchdata.enddate') }}</div>
</div> <!-- search-box -->


 <!-- search-box -->

 
</div> <!-- search-form -->




 
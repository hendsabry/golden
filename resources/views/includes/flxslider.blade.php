<div id="others">
@php
if($showData_One==1)
{
@endphp
 @php if(count($getbranchhalls)>0){ @endphp
<div class="carousel-row" id="withinmybuget">
 		<div class="carousel-heading">{{ (Lang::has(Session::get('lang_file').'.Within_Budget')!= '')  ?  trans(Session::get('lang_file').'.Within_Budget'): trans($OUR_LANGUAGE.'.Within_Budget')}} </div>
		<div class="clear"></div>
        <div class="flexslider carousel" >
          <ul class="slides">		
		  @foreach($getbranchhalls as $withinbudgethalls)	  
         	 
					<li>
					<span class="carousel-product-box">
					<span class="carousel-product-img"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$withinbudgethalls->pro_id}}"><img src="{{ $withinbudgethalls->pro_img or '' }}" /></a></span>
					<span class="carousel-product-cont">
					<span class="carousel-product-name"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$withinbudgethalls->pro_id}}">{{ $withinbudgethalls->pro_title or '' }}</a></span>
					<span class="carousel-product-prise">
						@if(isset($withinbudgethalls->pro_discount_percentage) && $withinbudgethalls->pro_discount_percentage>0)
							<strike>SAR {{ $withinbudgethalls->pro_price or '' }} </strike> <br />
SAR {{ $withinbudgethalls->pro_disprice or '' }}
						@else
							SAR {{ $withinbudgethalls->pro_price or '' }}
						@endif

						</span>
					<span class="carousel-product-view"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$withinbudgethalls->pro_id}}">{{ (Lang::has(Session::get('lang_file').'.View_Details')!= '')  ?  trans(Session::get('lang_file').'.View_Details'): trans($OUR_LANGUAGE.'.View_Details')}}</a></span>
					</span>
					</span>
					</li>	
				@endforeach	 	
				

          </ul>
        </div>
		</div>
		@php }else{ @endphp
		<div class="carousel-row" id="withinmybuget"><div>No hall no available</div></div>
		@php } @endphp

 @php  }   if($showData_Two==1){ @endphp 
<!------------------- above budget--------------------->
 
<div class="carousel-row" id="outofmybuget">
<div class="carousel-heading">{{ (Lang::has(Session::get('lang_file').'.Above_Budget')!= '')  ?  trans(Session::get('lang_file').'.Above_Budget'): trans($OUR_LANGUAGE.'.Above_Budget')}} </div>
	<div class="clear"></div>
	@php if(count($abovecustomerbudget)>0){ @endphp		
		<div class="flexslider carousel">
          <ul class="slides">
        @foreach($abovecustomerbudget as $abovebudgethalls) 	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$abovebudgethalls->pro_id}}"><img src="{{ $abovebudgethalls->pro_img or '' }}" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$abovebudgethalls->pro_id}}">{{ $abovebudgethalls->pro_title or '' }}</a></span>
		   <span class="carousel-product-prise">
		   			@if(isset($abovebudgethalls->pro_discount_percentage) && $abovebudgethalls->pro_discount_percentage>0)
							<strike>SAR {{ $abovebudgethalls->pro_price or '' }} </strike> <br />
SAR {{ $abovebudgethalls->pro_disprice or '' }}
						@else
							SAR {{ $abovebudgethalls->pro_price or '' }}
						@endif

		   	</span>
		   <span class="carousel-product-view"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$abovebudgethalls->pro_id}}">{{ (Lang::has(Session::get('lang_file').'.View_Details')!= '')  ?  trans(Session::get('lang_file').'.View_Details'): trans($OUR_LANGUAGE.'.View_Details')}}</a></span>
		   </span>
		   </span>
  	    		</li>	
		@endforeach	 	 	
				

          </ul>
        </div>
        @php }else{ @endphp
		<div>{{ (Lang::has(Session::get('lang_file').'.Not_found_above_budget')!= '')  ?  trans(Session::get('lang_file').'.Not_found_above_budget'): trans($OUR_LANGUAGE.'.Not_found_above_budget')}}</div>
		@php } @endphp
	</div>

 @php  }   if($showData_three==1){ @endphp 

<!------------------- offer ------------------------>

<div class="carousel-row" id="offermybuget">
<div class="carousel-heading">{{ (Lang::has(Session::get('lang_file').'.Offers')!= '')  ?  trans(Session::get('lang_file').'.Offers'): trans($OUR_LANGUAGE.'.Offers')}} </div>
	<div class="clear"></div>	
	@php if(count($offercustomerbudget)>0){ 

	@endphp

		<div class="flexslider carousel">
          <ul class="slides">
         	 
			  @foreach($offercustomerbudget as $offerbudgethalls) 	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$offerbudgethalls->pro_id}}">
		   	<img src="{{ $offerbudgethalls->pro_Img }}" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$offerbudgethalls->pro_id}}">{{ $offerbudgethalls->pro_title or '' }}</a></span>
		   <span class="carousel-product-prise">
		   	<?php if($offerbudgethalls->pro_disprice>0){?>
		   	<strike>SAR {{ $offerbudgethalls->pro_price or '' }} </strike> <br />
SAR {{ $offerbudgethalls->pro_disprice or '' }}
<?php } else{?>
SAR {{ $offerbudgethalls->pro_price or '' }}
<?php }?>
</span>
		   <span class="carousel-product-view"><a href="{{ url('') }}/branchhalldetail/{{$halltype}}/{{$typeofhallid}}/{{$shopid}}/{{$branchid}}/{{$offerbudgethalls->pro_id}}">
		   {{ (Lang::has(Session::get('lang_file').'.View_Details')!= '')  ?  trans(Session::get('lang_file').'.View_Details'): trans($OUR_LANGUAGE.'.View_Details')}}
		</a></span>
		   </span>
		   </span>
  	    		</li>			 	
			@endforeach	 		

          </ul>
        </div>
        @php }else{ @endphp
		<div>{{ (Lang::has(Session::get('lang_file').'.Not_found_offer_budget')!= '')  ?  trans(Session::get('lang_file').'.Not_found_offer_budget'): trans($OUR_LANGUAGE.'.Not_found_offer_budget')}}</div>
		@php } @endphp
	</div>

 @php  }     @endphp 

</div> 

@php 
$cururl = request()->segment(count(request()));
//echo $cururl;

@endphp

<div class="vendor_header_right">

  <div class="vendor_welc">
     @include("includes.language-changer")
    <div class="vendor_name username"> {{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}} <span>
      <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
			
			<ul class="vendor_header_navbar">
				<li><a href="{{route('my-account-profile')}}"@php if($cururl=='my-account-profile') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')}}</a></li>
        <li><a href="{{route('my-account-ocassion')}}"@php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')}}</a></li>
        <li><a href="{{route('my-account-studio')}}"@php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')}}</a></li>
        <li><a href="{{route('my-account-security')}}"@php if($cururl=='my-account-security') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')}}</a></li>
        <li><a href="{{route('my-account-wallet')}}"@php if($cururl=='my-account-wallet') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')}}</a></li>
        <li><a href="{{route('my-account-review')}}"@php if($cururl=='my-account-review') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')}}</a></li>
        <li><a href="{{route('my-request-a-quote')}}"@php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')}}</a></li>
        <li><a href="{{route('change-password')}}"@php if($cururl=='change-password') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}</a></li>
				<li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li>
			</ul>
			
      </span></div>
	  @if (Session::get('customerdata.token')!='')
			@php $getcartnoitems = Helper::getNumberOfcart(); @endphp
			@if($getcartnoitems>0)
    <a href="{{url('mycart')}}" class="vendor_cart"><img src="{{ url('') }}/themes/images/basket.png" /><span>{{ $getcartnoitems }}</span></a>
	@endif
			@endif
	
	 </div>
  <!--<div class="select_catg">
            <div class="select_lbl">Other Branches</div>
            <div class="search-box-field">
              <select class="select_drp">
                <option>select</option>
              </select>
            </div>
          </div>-->



  @php
 
   if(isset($otherbranch) && count($otherbranch)>1){ @endphp
	   <div class="select_catg">
            	<div class="select_lbl">{{ (Lang::has(Session::get('lang_file').'.Other_Branches')!= '')  ?  trans(Session::get('lang_file').'.Other_Branches'): trans($OUR_LANGUAGE.'.Other_Branches')}}</div>
                <div class="search-box-field">
                	<select class="select_drp" id="dynamic_select">
                    	<option value="">@if (Lang::has(Session::get('lang_file').'.Select_Branch')!= '') {{  trans(Session::get('lang_file').'.Select_Branch') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Branch') }} @endif</option>
						
                         @foreach($otherbranch as $otherbarnches)
						  <?php  
						  if($subcat_id==94)
						  {?>
                          <option value="{{url('jewelerybranchdetail')}}/{{ $category_id }}/{{ $subcat_id }}/{{ $shop_id }}/{{ $otherbarnches->mc_id or '' }}">
						  <?php } if($subcat_id==95){ ?>
						  <option value="{{url('jewelerybranchdetail')}}/{{ $category_id }}/{{ $subcat_id }}/{{ $shop_id }}/{{ $otherbarnches->mc_id or '' }}">
						  <?php } else { ?>
						  <option value="{{url('perfumesbranchdetail')}}/{{ $category_id }}/{{ $subcat_id }}/{{ $shop_id }}/{{ $otherbarnches->mc_id or '' }}">
						  <?php }						  
						    $mc_name = 'mc_name'; 
							if(Session::get('lang_file')!='en_lang')
							{
							  $mc_name = 'mc_name_ar'; 
							}
							echo $otherbarnches->$mc_name; 
						  ?>  
						  </option>
                         @endforeach
                    </select>
                </div>
            </div>
	  @php } @endphp
    </div>







</div>

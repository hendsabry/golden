@php  $ishomepage=substr(Route::currentRouteAction(), 0, (strpos(Route::currentRouteAction(), '@') -1) );
if($ishomepage=='App\Http\Controllers\FrontendControlle'){  $homeclass='homepading';}elseif($ishomepage=='App\Http\Controllers\SigninSignupControlle'){ $homeclass='loginpading'; }else{ $homeclass=''; }
 @endphp
@php $Current_Currency = Session::get('currency');
if($Current_Currency =='') { $Current_Currency = 'SAR';  }
 
 
@endphp
<div class="nav_row {{ $homeclass }}">
  
    <div class="heamburger">
        <span></span>
        <span></span>
        <span></span>
    </div><!-- heamburger -->
 
	<div class="curns">
	<div class="curns_area">
	<div class="currency_text">{{ (Lang::has(Session::get('lang_file').'.Currency')!= '')  ?  trans(Session::get('lang_file').'.Currency'): trans($OUR_LANGUAGE.'.Currency')}} </div>
		
		<div class="currency_select">
			<select name="currency" class="cc_currency">		
			<option value="sar" <?php if($Current_Currency == 'SAR'){ echo "SELECTED"; }  ?> >Riyal (SAR)</option>
			<option value="usd" <?php if($Current_Currency == 'USD'){ echo "SELECTED"; }  ?>>Dollar (USD)</option>
			<option value="inr" <?php if($Current_Currency == 'INR'){ echo "SELECTED"; }  ?>>Rupee (INR)</option>
		</select>

 
	</div>
	</div>
	
	
	</div> 
 
        <div class="left_nav">
            <ul>
                <li><a href="{{url('/about-us')}}">{{ (Lang::has(Session::get('lang_file').'.ABOUTUS')!= '')  ?  trans(Session::get('lang_file').'.ABOUTUS'): trans($OUR_LANGUAGE.'.ABOUTUS')}}</a></li>
                <li><a href="{{url('/testimonials')}}">{{ (Lang::has(Session::get('lang_file').'.Testimonials')!= '')  ?  trans(Session::get('lang_file').'.Testimonials'): trans($OUR_LANGUAGE.'.Testimonials')}}</a></li>
                <li><a href="{{url('/occasions')}}">#{{ (Lang::has(Session::get('lang_file').'.Occasions')!= '')  ?  trans(Session::get('lang_file').'.Occasions'): trans($OUR_LANGUAGE.'.Occasions')}}</a></li>
				@if (Session::get('customerdata.token')!='')
                <span class="mobile_fields"><li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li></span>
				@endif
            </ul>
        </div>
        <div class="middle_nav">
            <a class="brand" href="{{ url('/') }}">
            <img src="{{ url('') }}/themes/images/logo.png" />
            </a>
        </div>
        <div class="right_nav">
		
		
            <span class="mobile_fields">
 
				@if(Session::get('lang_file') == 'ar_lang')
                <a class="mob_lang" href="javascript:void(0);" data-val="en"  onclick="Lang_change('en')">English</a>
				@else    
				<a class="mob_lang" href="javascript:void(0);" data-val="ar"  onclick="Lang_change('ar')">{{ (Lang::has(Session::get('lang_file').'.ARABIC')!= '')  ?  trans(Session::get('lang_file').'.ARABIC'): trans($OUR_LANGUAGE.'.ARABIC')}}</a>
				@endif
				
                @if (Session::get('customerdata.token')=='')
				<a href="{{url('login-signup')}}" <?php if(Route::getCurrentRoute()->uri() == 'index' || Route::getCurrentRoute()->uri() == '/') { ?> class="active" <?php } ?>>
				@else
				<a href="{{url('my-account')}}" <?php if(Route::getCurrentRoute()->uri() == 'index' || Route::getCurrentRoute()->uri() == '/') { ?> class="active" <?php } ?>>
				@endif
                   @if (Session::get('customerdata.token')!='')
                    <img src="{{ url('') }}/themes/images/login.png" />
					@else
					 <img src="{{ url('') }}/themes/images/user.png"  class="lgs"/>
					@endif
                </a>
            </span>
			@if (Session::get('customerdata.token')!='')
			@php $getcartnoitems = Helper::getNumberOfcart(); @endphp
			@if($getcartnoitems>0)

            @php $cid=Session::get('searchdata.cityid');   @endphp
			
			@if($cid=='')

			<a  href="javascript:void(0);" onclick="postcitypop(2);" class="basket_icon">   <span class="count">{{ $getcartnoitems }}</span></a>
			@else
			<a href="{{url('mycart')}}" class="basket_icon"><span class="count">{{ $getcartnoitems }}</span></a>
 			@endif
 


            @endif
			@endif
            <ul>                   
					@if (Session::get('customerdata.token')=='')
                <li><a href="{{url('login-signup')}}">{{ (Lang::has(Session::get('lang_file').'.SignIn')!= '')  ?  trans(Session::get('lang_file').'.SignIn'): trans($OUR_LANGUAGE.'.SignIn')}} / {{ (Lang::has(Session::get('lang_file').'.SignUp')!= '')  ?  trans(Session::get('lang_file').'.SignUp'): trans($OUR_LANGUAGE.'.SignUp')}}</a></li>
				@else
                <li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li>
				@endif
               
                @if(Session::get('lang_file') == 'ar_lang')
				 <li class="lang_select" data-val="en"  onclick="Lang_change('en')"> 
                <a href="#" class="lang_select" data-val="en"  onclick="Lang_change('en')">
                <div class="lang_select" data-val="en"  onclick="Lang_change('en')">English</div>     
                @else        
				 <li  class="lang_select" data-val="ar" onclick="Lang_change('ar')"> 
                <a href="#"  class="lang_select" data-val="ar" onclick="Lang_change('ar')">
                <div class="lang_select" data-val="ar" onclick="Lang_change('ar')">{{ (Lang::has(Session::get('lang_file').'.ARABIC')!= '')  ?  trans(Session::get('lang_file').'.ARABIC'): trans($OUR_LANGUAGE.'.ARABIC')}}</div>
                @endif 
                </li></a>
                
                <li><a href="{{url('/contact-us')}}">{{ (Lang::has(Session::get('lang_file').'.ContactUs')!= '')  ?  trans(Session::get('lang_file').'.ContactUs'): trans($OUR_LANGUAGE.'.ContactUs')}}</a></li>
                @if (Session::get('customerdata.token')!='')
                <li><a href="{{route('my-account')}}">{{ (Lang::has(Session::get('lang_file').'.MyAccount')!= '')  ?  trans(Session::get('lang_file').'.MyAccount'): trans($OUR_LANGUAGE.'.MyAccount')}}</a></li>
                @endif
            </ul>
            <div class="welcome_message well_desk">
			<?php 
			if(Session::has('customerdata.user_id')) 
	        { 
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				?>
				{{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}}  <span>
				 
				@if(isset($getInfo->cus_name) && $getInfo->cus_name !='')
				{{$getInfo->cus_name or ''}}
				@else
				Guest
				@endif


				</span>
			<?php }	?>
			 </div>
            
        </div>
		
		<div class="wee_mobile"><div class="welcome_message">
			<?php 
			if(Session::has('customerdata.user_id')) 
	        { 
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				?>
				{{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}}  <span>
				<?php
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
				?>
				</span>
			<?php }	?>
			 </div></div>
    </div><!-- nav_row -->
	
	<div class="mobile_logo_g">
	
            <a class="brand" href="{{ url('/') }}">
            <img src="{{ url('') }}/themes/images/logo.png" />
            </a>
        
	
	
	</div>

<script type="text/javascript">
 
jQuery(document).ready(function(){
jQuery('.cc_currency').on('change',function(){
var str = jQuery(this).val();
var url      = window.location.href; 

 if(window.location.href.indexOf("?") > -1) {
   window.location.href=url+'&currency='+str;
    }
    else
    {
    window.location.href=url+'?currency='+str;	
    }




})
});
 

</script>


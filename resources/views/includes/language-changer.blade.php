<div class="lang">

        @if(Session::get('lang_file') == 'ar_lang')
        <a class="mob_lang" href="javascript:void(0);" data-val="en"  onclick="Lang_change('en')">English</a>
        @else    
        <a class="mob_lang" href="javascript:void(0);" data-val="ar"  onclick="Lang_change('ar')">{{ (Lang::has(Session::get('lang_file').'.ARABIC')!= '')  ?  trans(Session::get('lang_file').'.ARABIC'): trans($OUR_LANGUAGE.'.ARABIC')}}</a>
        @endif

        </div>
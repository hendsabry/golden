@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
	<div class="dash_select"> <a href="{{route('my-account-review')}}">{{ (Lang::has(Session::get('lang_file').'.BACK_TO_REVIEW')!= '')  ?  trans(Session::get('lang_file').'.BACK_TO_REVIEW'): trans($OUR_LANGUAGE.'.BACK_TO_REVIEW')}}</a> </div>
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.REVIEWS_READ_AND_WRITE')!= '')  ?  trans(Session::get('lang_file').'.REVIEWS_READ_AND_WRITE'): trans($OUR_LANGUAGE.'.REVIEWS_READ_AND_WRITE')}}</h1>
      <div class="field_group top_spacing_margin_occas">
		<div class="main_user"> 
		@if (Session::has('message'))
         <div class="review-submit-msg">{{ Session::get('message') }}</div>
        @endif
		<?php 
		//echo '<pre>';print_r($allreview);

		
            if($getsingleorder->product_sub_type=='band' || $getsingleorder->product_sub_type=='singer'){
            		$singerinfo = Helper::getMusicInfo($getsingleorder->product_id);
              		$catshop=$singerinfo->name;
            }else{

				$shopname = Helper::getparentCat($shopid);

				 $mc_name = 'mc_name';
                       if(Session::get('lang_file')!='en_lang')
					   {
                          $mc_name= 'mc_name_ar'; 
					   }
					   $catshop=$shopname->$mc_name;

				}
		?>

        <form name="reiewfrm" id="reiewfrm" method="post" action="{{ route('insert_review') }}" enctype="multipart/form-data">
		  {{ csrf_field() }}
		  <div class="review_popup dashb_review_popup" style="display:block;">
		  	<div>{{ $catshop or ''}}</div>
	

			 @if(isset($allreview) && count($allreview)<1)
			<div class="review_icon">
				<div class="your_rating"><?php /*?>{{ (Lang::has(Session::get('lang_file').'.YOUR_RATING')!= '')  ?  trans(Session::get('lang_file').'.YOUR_RATING'): trans($OUR_LANGUAGE.'.YOUR_RATING')}}:<?php */?> <span class='starrr star2 updateClass'></span></div>
				<!--  <div class='starrr star2 updateClass'></div> -->
			 
				
				<input type='hidden' name='rating' value='<?php if(isset($allreview[0]->ratings) && $allreview[0]->ratings!=''){ echo $allreview[0]->ratings;} ?>' class='star2_input starinput'/>
				
				<samp id="ratingId" style="color:#FF0000;"></samp>
				
			</div>
			 @else
				  <div><img src="{{url('/')}}/themes/images/star{{ round($allreview[0]->ratings) }}.png"></div>
				  
                 @endif  



			<div class="review_commentbox">
			  <!--<div class="review_label">{{ (Lang::has(Session::get('lang_file').'.WRITEYOUR_REVIEW')!= '')  ?  trans(Session::get('lang_file').'.WRITEYOUR_REVIEW'): trans($OUR_LANGUAGE.'.WRITEYOUR_REVIEW')}}</div>-->
			  <div class="review_comment_row">
				<input type="hidden" name="order_id" id="order_id" class="order_id" value="{{$orderid}}">
				<input type="hidden" name="shop_id" id="shop_id" class="shop_id" value="{{$shopid}}">
				<input type="hidden" name="vendor_id" id="vendor_id" class="vendor_id" value="{{$vendorid}}">
				<input type="hidden" name="product_id" id="product_id" class="product_id" value="<?php if(isset($getsingleorder->product_id) && $getsingleorder->product_id!=''){echo $getsingleorder->product_id;} ?>">
				<?php 
				if(isset($getsingleorder->review_type) && $getsingleorder->review_type=='worker'){ ?>
				<input type="hidden" name="review_type" id="review_type" class="review_type" value="shop">
				<input type="hidden" name="review_type3" id="review_type3" class="review_type3" value="worker">
				<?php }else{?>
				<input type="hidden" name="review_type" id="review_type" class="review_type" value="<?php if(isset($getsingleorder->review_type) && $getsingleorder->review_type!=''){echo $getsingleorder->review_type;} ?>">
				<?php }?>
				<input type="hidden" name="commentId" id="commentId" class="commentId" value="<?php if(isset($allreview[0]->comment_id) && $allreview[0]->comment_id!=''){echo $allreview[0]->comment_id;} ?>">
				<div class="review_txtarea">
				<?php if(isset($allreview[0]->comments) && $allreview[0]->comments!=''){ echo $allreview[0]->comments;}else{ if(count($allreview) > 0){}else{?>
				<textarea name="comment" id="comment" class="comts"></textarea>
				<?php } } ?>
				</div>
				<?php 
				if(isset($getsingleorder->review_type) && $getsingleorder->review_type=='worker'){
	             $staffid = Helper::getstaffOrders($orderid,$getsingleorder->product_id);
				 $staffname = Helper::getStaffNameOne($staffid->staff_id);
				?>	
				<input type='hidden' name='rating3' value='<?php if(isset($allreview[1]->ratings) && $allreview[1]->ratings!=''){echo $allreview[1]->ratings;} ?>' class='star3_input starinput'/>
				
				<input type="hidden" name="worker_id" id="worker_id" class="worker_id" value="<?php if(isset($staffid->staff_id) && $staffid->staff_id!=''){echo $staffid->staff_id;} ?>">
				<div class="review_staffname">{{ (Lang::has(Session::get('lang_file').'.STAFF_NAME')!= '')  ?  trans(Session::get('lang_file').'.STAFF_NAME'): trans($OUR_LANGUAGE.'.STAFF_NAME')}}: <?php echo $staffname->staff_member_name;?></div>
				<div class="your_rating">{{ (Lang::has(Session::get('lang_file').'.YOUR_RATING')!= '')  ?  trans(Session::get('lang_file').'.YOUR_RATING'): trans($OUR_LANGUAGE.'.YOUR_RATING')}}: <span class='starrr star3 updateClass'></span></div>
				<div style=""><samp id="rating3Id" style="color:#FF0000;"></samp></div>
				<div class="review_txtarea">
				<?php if(isset($allreview[1]->comments) && $allreview[1]->comments!=''){ echo $allreview[1]->comments;}else{ if(count($allreview) > 0){}else{?>
				<br />
				<textarea name="wcomment" id="wcomment" class="comts"></textarea>
				<?php } } ?>
				</div>	
				<?php } if(count($allreview) > 0){?><div class="review-status"> <?php if(isset($allreview[0]->status) && $allreview[0]->status==1){?>{{ (Lang::has(Session::get('lang_file').'.APPROVED')!= '')  ?  trans(Session::get('lang_file').'.APPROVED'): trans($OUR_LANGUAGE.'.APPROVED')}} <?php }else{ ?> {{ (Lang::has(Session::get('lang_file').'.WAITING_FOR_APPROVED')!= '')  ?  trans(Session::get('lang_file').'.WAITING_FOR_APPROVED'): trans($OUR_LANGUAGE.'.WAITING_FOR_APPROVED')}}<?php } ?> </div><?php } ?>
				<?php  if(count($allreview) > 0){}else{?>
				<br /><br /><br /><br />
				<input type="submit" name="submit" onclick="return validateForm()" class="form-btn btn-info-wisitech" value="{{ (Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ?  trans(Session::get('lang_file').'.SUBMIT'): trans($OUR_LANGUAGE.'.SUBMIT')}}">
				<?php } ?>
			  </div>
			</div>
		  </div>
		</form>
		</div>
      </div>
	  
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
</div>

<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer')
<script>
function validateForm() 
{
    var x = document.forms["reiewfrm"]["rating"].value;
    if(x == "") 
	{
	    document.getElementById("ratingId").innerHTML = "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_RATING')!= ''){{ trans(Session::get('lang_file').'.PLEASE_ENTER_RATING') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_RATING') }} @endif";
        return false;
    }
	var y = document.forms["reiewfrm"]["rating3"].value;
    if(y == "") 
	{
	    document.getElementById("rating3Id").innerHTML = "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_RATING')!= ''){{ trans(Session::get('lang_file').'.PLEASE_ENTER_RATING') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_RATING') }} @endif";
        return false;
    }
}
</script>

<script src="{{ url('') }}/themes/js/starrr.js"></script>

<script>
var $s2input = $('.star2_input');
$('.star2').starrr({
  max: 5,
  rating: $s2input.val(),
  change: function(e, value){
	$s2input.val(value).trigger('input');
  }
});
var $s3input = $('.star3_input');
$('.star3').starrr({
  max: 5,
  rating: $s3input.val(),
  change: function(e, value){
	$s3input.val(value).trigger('input');
  }
});



</script>
<link rel="stylesheet" href="{{ url('') }}/themes/css/font-awesome-new.css">
<style type='text/css'>
    .starrr {  display: inline-block; }
    .starrr a {
      font-size: 16px;
      padding: 0 1px;
      cursor: pointer;
      color: #FFD119;
      text-decoration: none; 
	  }
    img.ribbon {
      position: fixed;
      z-index: 1;
      top: 0;
      right: 0;
      border: 0;
      cursor: pointer; }

    .container {
      margin-top: 60px;
      text-align: center;
      max-width: 450px; }

    input {
      width: 30px;
      margin: 10px 0;
    }
  </style>
 


@include('includes.navbar')
<script type="text/javascript" src="{{ url('/themes/js/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<link href="{{ url('/themes/js/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" />
<script src="{{ url('/themes/js/height.js') }}"></script>
<div class="outer_wrapper">
  <div class="inner_wrap"> @include('includes.header')
    <div class="hashtag-occasions-wrapper">
      <div class="form_title">@if(Lang::has(Session::get('lang_file').'.Occasionss')!= '') {{ trans(Session::get('lang_file').'.Occasionss')}}  @else {{ trans($OUR_LANGUAGE.'.Occasionss')}} @endif </div>
      <div class="occasions-shorting-line">
   <form action="{{url('/occasions')}}" method="POST" name="filter"> 
    {{ csrf_field() }}
        <select name="occasion" id="occasion" onchange="submit(this.value);">
        <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '') {{  trans(Session::get('lang_file').'.Select_Occasion_Type') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Occasion_Type') }} @endif</option>
        <?php 
        if(Session::get('lang_file')!='en_lang')
        {
        $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف'); 
        }
        else
        {
        $getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding Occasion'); 
        }
        foreach($getArrayOfOcc as $key=>$ocval){?>
        <option value="" disabled="" style="color: #d2cece;">{{$ocval}}</option>
        <?php 
        $setOccasion = Helper::getAllOccasionNameList($key);
        foreach($setOccasion as $val){ ?>
        <option value="{{$val->id}}"<?php if(isset($_REQUEST['occasion']) && $_REQUEST['occasion']==$val->id){ echo 'selected="selected"'; }?>><?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
        </option>
        <?php } ?>
        <?php } ?>
        </select> 
        </form>
        <span>@if(Lang::has(Session::get('lang_file').'.Short_By')!= '') {{ trans(Session::get('lang_file').'.Short_By')}}  @else {{ trans($OUR_LANGUAGE.'.Short_By')}} @endif  </span> </div>
        @if(count($infy)>=1)
        @php $i=0;    @endphp
        @foreach($infy as $val)
        <div class="hashtag-occasions-box {{$i}}">     
        @for($k=1;$k<=count($val);$k++)
        @php $J = $k-1;     @endphp    
        @if($k!=1) <div class="hashtag-multi-occasions"> @endif
        <div class="hashtag-info-section">
        @if($k==1) 
        <div class="hashtag-info-left-section">
        <div class="hashtag-info-img"><img src="{{ url('/themes/images/user-default.jpg') }}" alt="" /></div>
        <div class="hashtag-info-name">{{ $infy[$i][0]->posted_by_name or '' }}</div>
        </div>
        @endif 
          <!-- hashtag-info-left-section --> 
          <div class="hashtag-info-right-section">
            <div class="hashtag-occasion-name">@if(Session::get('lang_file') != 'ar_lang') {{ $infy[$i][$J]->occasion_name or '' }} @else {{ $infy[$i][$J]->occasion_name_ar or '' }} @endif ({{ $infy[$i][$J]->posted_by_name or '' }})</div>
            <div class="hashtag-occasion-det">{{ Carbon\Carbon::parse($infy[$i][$J]->occasion_date)->format('d M Y')}}, {{ $infy[$i][$J]->occasion_venue or '' }}</div> 
          </div>
          <!-- hashtag-info-right-section --> 
        </div>
        <!-- hashtag-info-section -->
        <div class="hashtag-occasions-gallery"> 
          <ul class="slides test">
            <!--li><a class="popup-link" href="{{ str_replace('thumb-','',$infy[$i][$J]->images) }}"><img src="{{ $infy[$i][$J]->images or '' }}" alt="" /></a></li-->
          @foreach($infy[$i][$J]->image_url as $vals)
            <li><a class="popup-link_{{$i}}" href="{{ str_replace('thumb-','',$vals)}}"><img src="{{ $vals or '' }}" alt="" /></a></li>
          @endforeach
          </ul>
        </div>
        <script type="text/javascript">    
        jQuery('.popup-link_<?php echo $i;?>').magnificPopup({
        type: 'image',
        gallery:{enabled:true}
        }); 
        </script>
 @if($k!=1) </div> @endif
   @endfor

   <span class="viewall"><a href="{{url('/occasion-details')}}/{{$infy[$i][0]->posted_by}}"> {{ (Lang::has(Session::get('lang_file').'.View_All')!= '')  ?  trans(Session::get('lang_file').'.View_All'): trans($OUR_LANGUAGE.'.View_All')}} </a></span>
     </div>
       @php $i=$i+1; @endphp
@endforeach
@else
<div class="norecord">  {{ (Lang::has(Session::get('lang_file').'.NO_RESULT_FOUND')!= '')  ?  trans(Session::get('lang_file').'.NO_RESULT_FOUND'): trans($OUR_LANGUAGE.'.NO_RESULT_FOUND')}}  </div>
@endif
    </div>
    <div class="paginate"> 
{{ $getOccsaions->links() }} 
</div>
    <!-- occasions-wrapper -->
  </div>
  <!-- inner_wrap -->
</div>
<!-- outer_wrapper -->

@include('includes.footer') 
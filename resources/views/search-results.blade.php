@include('includes.navbar')
@php 
//print_r(Session::get('searchdata'));
if(Session::get('searchdata.noofattendees')=='' || Session::get('searchdata.budget')=='' ){ $hurl=url('').'/#'; $selclass="required_modify";}else{ $hurl=url('').'/type-of-halls/3'; $selclass=''; }
@endphp
<div class="outer_wrapper diamond_fullwidth">
@include('includes.header')
  <div class="inner_wrap"> 
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
      @php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ @endphp
      @include('includes.searchweddingandoccasions')
      @php } @endphp
      @php if(Session::get('searchdata.mainselectedvalue')=='1'){ @endphp
      @include('includes.search')
      @php } @endphp </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper main_category">
   @include('includes.mobile-modify')
         <div class="diamond_box">
      <div class="page-right-section">
        <div class="diamond_main_wrapper"> 
		
	@if(isset($_REQUEST['mainselectedvalue']) && $_REQUEST['mainselectedvalue']!='' && $_REQUEST['mainselectedvalue']==1)
		 @if(Session::get('lang_file')!='en_lang')
		 <img src="{{ url('') }}/themes/images/diamond-2group-ar.jpg" alt="1" border="0" usemap="#Map"  />
	
		@else
			<img src="{{ url('') }}/themes/images/diamond-2group.jpg" alt="1" border="0" usemap="#Map"  />
		@endif
<map name="Map" id="Map">
  <area shape="poly" coords="469,13,19,462,916,460,468,12" href="{{ $hurl }}" class="{{ $selclass }}" />
<area shape="poly" coords="470,914,19,468,912,468,468,914" href="{{ url('') }}/foodcatgeory/7" />
</map>
		
		@else
		
		
		    @if(Session::get('lang_file')!='en_lang')
			<img src="{{ url('') }}/themes/images/diamond_ar-9.jpg" alt="" usemap="#homeMap" hidefocus="true">
			@else
			<img src="{{ url('') }}/themes/images/diamond-9.jpg" alt="" usemap="#homeMap" hidefocus="true">
			@endif
          <map name="homeMap" id="homeMap">
            <area shape="poly" coords="467,9,406,81,318,155,316,162,282,197,650,197" href="{{ $hurl }}" class="{{ $selclass }}" />
            <area shape="poly" coords="469,198,282,197,177,301,175,308,108,370,469,370" href="{{ url('') }}/foodcatgeory/7" alt="" /> 
            <area shape="poly" coords="654,197,468,198,469,272,469,296,471,369,827,370" href="{{ url('') }}/occasion-co-ordinator/11" />
            <area shape="poly" coords="106,373,306,373,307,547,305,548,97,546,15,464" href="{{ url('') }}/beautyandelegancecatgeory/18" />
            <area shape="poly" coords="308,373,628,371,626,547,572,546,309,544,309,489" href="{{ url('') }}/revivingconcerts/24" />
            <area shape="poly" coords="629,371,831,372,923,462,843,545,631,543,629,423" href="{{ url('') }}/cliniccatgeory/28" />
            <area shape="poly" coords="301,548,468,548,469,718,468,725,277,724,104,550" href="{{ url('') }}/shoppingList/31" />
            <area shape="poly" coords="539,550,837,548,732,652,666,723,471,722,467,547" href="{{ url('') }}/carrental/37" />
            <area shape="poly" coords="441,727,660,727,579,810,470,917,444,892,279,727" href="{{ url('') }}/travelagency/87" />
          </map>
       @endif   
        </div>
        <center>
        </center>
        <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
      </div>
	  </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')

<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content" id="booking">@php if (Lang::has(Session::get('lang_file').'.STOPMESSAGE')!= '') { echo trans(Session::get('lang_file').'.STOPMESSAGE'); } else  { echo trans($OUR_LANGUAGE.'.STOPMESSAGE'); } @endphp</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/>
      <a class="action_yes status_yes" href="javascript:void(0);">  @php if (Lang::has(Session::get('lang_file').'.OK')!= '') { echo trans(Session::get('lang_file').'.OK'); } else  { echo trans($OUR_LANGUAGE.'.OK'); } @endphp</a> </div>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function()
{
 jQuery('body').on('click',".required_modify",function(e){
e.preventDefault();
jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);

 })

});
</script>
<script type="text/javascript">
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
});
</script>
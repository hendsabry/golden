<div @if($forapp==1) style="display: none" @endif>
@include('includes.navbar')
</div>
<div class="outer_wrapper">
	<div @if($forapp==1) style="display: none" @endif>
 @include('includes.header')
 </div>
  <div class="inner_wrap"> 
   
  
   <!-- search-section -->
    <!-- search-section -->
    <div class="page-left-right-wrapper">
     
	 <div class="contact_left">
	 <div class="cont">
	 <h1 class="golden">Goldencages.com</h1>
	 <?php 
	 //print_r($cmscontent);
	 		$mapurl='';
	 if(isset($cmscontent)){
           foreach ($cmscontent as  $value) {
           	# code...
          $mapurl=$value->mapurl;
	 	?>
	 <div class="footer-info-box address-box">
	 		<?php echo $value->content; ?>
</div>
<?php  }} ?>
<div class="cont_form_spc">
<div class="inquiry">{{ (Lang::has(Session::get('lang_file').'.Inquiry_Form')!= '')  ?  trans(Session::get('lang_file').'.Inquiry_Form'): trans($OUR_LANGUAGE.'.Inquiry_Form')}} </div>
	  
@if(Session::has('message'))
<div class="error">
{{  Session::get('message') }}
</div>
@endif
<form id="cartfrm" name="cartfrm" method="POST" action="{{url('/sendcontactenquiry')}}">
	 {{ csrf_field() }}
	  <div class="contact_form_box">
	  <div class="contact_form_row">
	  <div class="checkout-form-top"> {{ (Lang::has(Session::get('lang_file').'.Name')!= '')  ?  trans(Session::get('lang_file').'.Name'): trans($OUR_LANGUAGE.'.Name')}}</div>
	  <div class="checkout-form-bottom">
                  <input type="text" class="t-box" value="" id="Ename" name="Ename" maxlength="70">
                </div>
	  
	  
	  </div>
	  
	  <div class="contact_form_row">
	  <div class="checkout-form-top"> {{ (Lang::has(Session::get('lang_file').'.Email')!= '')  ?  trans(Session::get('lang_file').'.Email'): trans($OUR_LANGUAGE.'.Email')}} </div>
	  <div class="checkout-form-bottom">
                  <input type="email" class="t-box" value="" id="Email" name="Email" maxlength="80">
                </div>
	  
	  
	  </div>
	  <div class="contact_form_row">
	  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Message')!= '')  ?  trans(Session::get('lang_file').'.Message'): trans($OUR_LANGUAGE.'.Message')}} </div>
	  <div class="checkout-form-bottom">
                  <textarea name="Message" cols="" rows=""></textarea>
                </div>
	   
	  </div>
	  
	  <div class="contact_form_row">
	  <input type="submit" class="form-btn btn-info-wisitech" value="{{ (Lang::has(Session::get('lang_file').'.Submit')!= '')  ?  trans(Session::get('lang_file').'.Submit'): trans($OUR_LANGUAGE.'.Submit')}}" name="submit">
	  
	  
	  </div>
	    </form>
	  </div>

	

</div>

</div>
	 
	 </div>  
	  <div class="contact_right">
	  @if(isset($mapurl) && $mapurl!='')
	  <div class="map_area"><iframe src="{{ $mapurl }}" width="100%" height="580" frameborder="0" style="border:0" allowfullscreen></iframe></div>
	  
	  @endif
	  </div> 
    <div>



      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrappercontactus.blade.php -->
  </div>
  <!-- outer_wrapper -->
</div>
 <script type="text/javascript">
 	


jQuery("#cartfrm").validate({
    rules: {       
          "Ename" : {
            required : true,
    
            maxlength: 70
          },  
		  "Email" : {
            required : true,
            maxlength: 80
          },
		  "Message" : {
            required : true,       
            maxlength: 500
          },		  
		    
         },
         messages: {
          "Ename": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NAME') }} @endif'
          },
		  "Email": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_EMAIL') }} @endif' 
          },
		  "Message": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_MESSAGE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_MESSAGE') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_MESSAGE') }} @endif' 
          }, 
         },
          submitHandler: function(form) {
                    form.submit();
                }
 });
 

 </script>
 <div @if($forapp==1) style="display: none" @endif>
@include('includes.footer') 
</div>
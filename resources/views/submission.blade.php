{!! $navbar !!}
<!-- Navbar ================================================== -->

@php $city_det = DB::table('nm_emailsetting')->first(); @endphp
 @inject('data','App\Help')
 <link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />
<!-- Header End====================================================================== -->
<div id="mainBody" class="egis">
	<div class="container">
	<div class="row">
<!-- Sidebar ================================================== -->
	
<!-- Sidebar end=============================================== -->
	<div class="span12">
   
    
        @if ($errors->any())
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>')) !!}
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >×</button>
        </div>
		</ul>	
		@endif
         @if (Session::has('mail_exist'))
		<div class="alert alert-warning alert-dismissable">{!! Session::get('mail_exist') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		@endif
         @if (Session::has('result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('result') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		@endif

				<div class="register_logo"><a href="sitemerchant"><img src="{{url('public/assets/logo/Logo_1517904811_Logo_hfgh.png')}}"></a></div>


	<div class="thank_sub"><div class="thanks_mess"><h3 class="thank-haeding"><?php if (Lang::has(Session::get('lang_file').'.THANK_YOU_TO_MERCHANT_SIGN_UP')!= '') { echo  trans(Session::get('lang_file').'.THANK_YOU_TO_MERCHANT_SIGN_UP');}  else { echo trans($OUR_LANGUAGE.'.THANK_YOU_TO_MERCHANT_SIGN_UP');} ?>!</h3>
  
    <div class="thank_text">
     
 
     <?php if (Lang::has(Session::get('lang_file').'.Thank_you_for_signing')!= '') { echo  trans(Session::get('lang_file').'.Thank_you_for_signing');}  else { echo trans($OUR_LANGUAGE.'.Thank_you_for_signing');} ?>   
 
</div></div></div>

</div>
</div>
</div>
</div>
</div>
 


 


<div class="login_bottom"></div>

</body>
</html>



 

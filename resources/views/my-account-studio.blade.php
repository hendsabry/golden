@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap stud">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')}} </h1>  

      <div class="field_group studio_wrapper">

        @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        <div class="studio_wrap">
          <div class="studio_subtitle">{{ (Lang::has(Session::get('lang_file').'.UPLOAD_YOUR_PHOTO')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD_YOUR_PHOTO'): trans($OUR_LANGUAGE.'.UPLOAD_YOUR_PHOTO')}}</div>
          {!! Form::open(array('url'=>"insert_studio",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'studio_frm','id'=>'studio_frm')) !!}
          <div class="studio_form">
            <div class="common_row">
              <div class="col2">
                <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}</div>
                <div class="search-box-field">
                  <select class="checkout-small-box" name="city" id="city">
                    <option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_CITY') }} @endif</option>
                     @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="color: #d2cece;">{{$cbval->co_name}}</option>
                         @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                           @php if(isset($_REQUEST['city']) && $_REQUEST['city']==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} @endphp
                        @if($selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" {{ $selectedcity }} >{{ $val->$ci_name }}</option>
                         @endforeach
                        @endforeach
                  </select>
                  @if($errors->has('city'))<span class="error">{{ $errors->first('city') }}</span>@endif
                </div>
              </div>
              <div class="col2">
                <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.DATE')!= '')  ?  trans(Session::get('lang_file').'.DATE'): trans($OUR_LANGUAGE.'.DATE')}}</div>
                <div class="search-box-field">
                  <input type="text" class="search-t-box cal-t" name="studio_date" id="studio_date" value="{!! Input::old('studio_date') !!}" readonly="">
                  @if($errors->has('studio_date'))<span class="error">{{ $errors->first('studio_date') }}</span>@endif
                </div>
              </div>
              <div class="col2">
                <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')}}</div>
                <div class="search-box-field">
                  <select name="occasion" id="occasion">
                    <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Occasion_Type')!= '') {{  trans(Session::get('lang_file').'.Select_Occasion_Type') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Occasion_Type') }} @endif</option>
					  <?php 
					     if(Session::get('lang_file')!='en_lang')
               {
                 $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف'); 
               }
               else
               {
                  $getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding Occasion'); 
               }
					     foreach($getArrayOfOcc as $key=>$ocval){?>
					    <option value="" disabled="" style="color: #d2cece;">{{$ocval}}</option>
						<?php 
						$setOccasion = Helper::getAllOccasionNameList($key);
						foreach($setOccasion as $val){ ?>
						<option value="{{$val->id}}"<?php if(isset($_REQUEST['occasion']) && $_REQUEST['occasion']==$val->id){ echo 'selected="selected"'; }?>><?php $title = 'title';if(Session::get('lang_file')!='en_lang'){$title = 'title_ar';}echo $val->$title;?>
					   </option>
						<?php } ?>
					 <?php } ?>
                  </select>
                  @if($errors->has('occasion'))<span class="error">{{ $errors->first('occasion') }}</span>@endif
                </div>
              </div>
              <div class="col2">
                <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.VENUE')!= '')  ?  trans(Session::get('lang_file').'.VENUE'): trans($OUR_LANGUAGE.'.VENUE')}}</div>
                <div class="search-box-field">
                  <input type="text" class="t-box" name="venue" id="venue" value="{!! Input::old('venue') !!}">
                  @if($errors->has('venue'))<span class="error">{{ $errors->first('venue') }}</span>@endif
                </div>
              </div>
              <div class="col2 browse_row" style="height:auto;">
                <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.UPLOAD_YOUR_PIC')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD_YOUR_PIC'): trans($OUR_LANGUAGE.'.UPLOAD_YOUR_PIC')}}</div>
                <div class="myac-file-box">
   
                    <label for="company_logo">
                      <div class="file-btn-area">
                      <div class="file-btn">{{ (Lang::has(Session::get('lang_file').'.UPLOAD')!= '')  ?  trans(Session::get('lang_file').'.UPLOAD'): trans($OUR_LANGUAGE.'.UPLOAD')}}</div>
                      <div  class="file-value" id="file_value"></div>
                    </div>
                    </label>
                
                  <input type="file" name="upload_your_pic" class="info-file" id="company_logo" accept="image/gif, image/jpeg, image/png">                   
                </div>
              </div>
              @if($errors->has('upload_your_pic'))<span class="error">{{ $errors->first('upload_your_pic') }}</span>@endif 
            
             <div id="img_upload"></div>   
             <div class="add-more-btn-line">
                <a id="add_button" class="add-more-link" href="javascript:void(0);">@if (Lang::has(Session::get('lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a> 
                 @php $Count =  1;@endphp
                  <input type="hidden" id="count" name="count" value="{{$Count}}">
                </div>
                 
              </div> 
                   
            </div>
            <div class="common_row">
              <div class="col2">
                <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ?  trans(Session::get('lang_file').'.SUBMIT'): trans($OUR_LANGUAGE.'.SUBMIT')}}" class="form-btn btn-info-wisitech">
              </div>
            </div>
          </div>
          {!! Form::close() !!}
          <!-- studio_form -->
          <div class="studio_gallary">
            {!! Form::open(array('url'=>"my-account-studio",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter','id'=>'filter')) !!}
            <div class="col2">
              <select class="checkout-small-box" name="gallery_city" id="gallery_city" onchange="myFunction()">
                    <option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_CITY') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_CITY') }} @endif</option>
                @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="color: #d2cece;">{{$cbval->co_name}}</option>
                         @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                           @php if(isset($_REQUEST['gallery_city']) && $_REQUEST['gallery_city']==$val->ci_id){ $selectedcity='selected="selected"'; }else{ $selectedcity='';} @endphp
                        @if($selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" {{ $selectedcity }} >{{ $val->$ci_name }}</option>
                         @endforeach
                        @endforeach
                  </select>
            </div>
            {!! Form::close() !!}
            <div class="studio_gallary_row">
              <!--<div class="alert alert-info pictureformat" style="display: none;"></div>-->
              @php if(count($setImagelist) > 0){ @endphp
              @foreach($setImagelist as $value)
              <div class="gallary_col3" id="del{{$value->id}}">
			   <div class="studio_gallary_white_box">
                <div class="studio_gallary_img gallery-img-height"><a href="{{route('ocassion-more-image',['id'=>$value->id])}}"><img src="{{$value->images}}"><span class="total_imgcount"><span class="total_imgcount_plus">+</span> <?php $totalImage = Helper::getAllOccasionImage($value->id); echo count($totalImage);?></span></a></div>                
				<div class="studio_gallary_inner_box">
				<div class="studio-occasions occasions-head"><?php 
				  $getoccasionname = Helper::getOccasionName($value->occasion_type); 
				  $mc_name  = 'title';
				  if(Session::get('lang_file')!='en_lang')
				  {
					 $mc_name = 'title_ar';
				  }
				  
				?><a href="{{route('ocassion-more-image',['id'=>@$value->id])}}"><?php echo @$getoccasionname->$mc_name; ?></a></div>	
				<div class="studio-occasions">{{ucfirst($value->occasion_venue)}}, 
				@php 
				  $getcitynamedata = Helper::getcity($value->city_id); 
				  $mc_name  = 'ci_name';
				  if(Session::get('lang_file')!='en_lang')
				  {
					 $mc_name = 'ci_name_ar';
				  }
				  echo $getcitynamedata->$mc_name;
				@endphp
				</div>				
				<div class="studio-occasions">{{ Carbon\Carbon::parse($value->studio_date)->format('d M Y')}}</div>				
				</div>
				</div>
				<div rel="{{$value->id}}" class="studio_captoion"></div>
              </div>
              @endforeach  
             @php } 
             else
             { @endphp
              @if(Lang::has(Session::get('lang_file').'.NO_RESULTS_FOUND')!= '') {{  trans(Session::get('lang_file').'.NO_RESULTS_FOUND') }} @else  {{ trans($MER_OUR_LANGUAGE.'.NO_RESULTS_FOUND') }} @endif
            @php } @endphp            
            </div>
			
          </div>
          <!-- studio_gallary -->
        </div>
        <!-- studio_wrap -->
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->

<!-- Lightbox- Studio Details  -->
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"> Are you sure to delete image folder?</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/><a class="action_yes status_yes" href="javascript:void(0);"> Yes </a> <a class="action_no" href="javascript:void(0);"> No </a>  </div>
  </div>
</div>

<!-- Lightbox- Studio Details END -->

@include('includes.footer') 

<script type="text/javascript">
jQuery('.studio_captoion').click(function()
{
var str = jQuery(this).attr('rel');
	jQuery('#delid').val(str);	
	jQuery('.action_popup').fadeIn(500);	
	jQuery('.overlay_popup').fadeIn(500);
});

jQuery('.action_no').click(function()
{
	jQuery('.action_popup').fadeOut(500);	
	jQuery('.overlay_popup').fadeOut(500);
});
</script>


  <script type="text/javascript">
    jQuery(document).ready(function(){
    var maxField = 9;  
    var addButton = jQuery('#add_button');  
    var wrapper = jQuery('#img_upload');
    var x = 1;
    var y = x+1;  
    jQuery(addButton).click(function(){
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="myac-file-box"><div id="remove_button"><a href="javascript:void(0);" title="Remove Field">&nbsp;</a></div><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('lang_file').'.UPLOAD')!= '') {{  trans(Session::get('lang_file').'.UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPLOAD') }} @endif</div></div></label><input class="info-file" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="uploadyourpicmore[]" data-info='+x+'></div> ';
    jQuery(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    jQuery(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    jQuery(this).parent('div').remove(); 
    x--;  
    document.getElementById('count').value = parseInt(x);
    });
    });
  </script>
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
	var jq = $.noConflict();
	jq( function() {
	  jq( "#studio_date" ).datepicker({
	    dateFormat: 'd M yy',
		  maxDate: '0'
	  });
	} );
  </script> 
  
  <script type="text/javascript"> 
  jQuery(document).ready(function(){
  jQuery("#studio_frm").validate({
    rules: {          
          "city" : {
            required : true
          },  
          "studio_date" : {
            required : true
          },  
          "occasion" : {
            required : true
          },
          "venue" : {
            required : true
          },  
          "upload_your_pic" : {
            required:true
          },     
         },
         messages: {
          "city": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_CITY')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_CITY') }} @endif"
          },
          "studio_date": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_STUDIO_DATE') }} @endif"
          }, 
          "occasion": {
            required:  "@if (Lang::has(Session::get('lang_file').'.SELECT_YOUR_OCCASION')!= '') {{  trans(Session::get('lang_file').'.SELECT_YOUR_OCCASION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_OCCASION') }} @endif"
          },
          "venue": {
            required:  "@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_VENUE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_VENUE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_VENUE') }} @endif"
          },
          "upload_your_pic": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_UPLOAD_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_UPLOAD_IMAGE') }} @endif"
          },      
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#studio_frm").valid()) {        
    jQuery('#studio_frm').submit();
   }
  });
});

jQuery('body').on('change','#company_logo', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value").html(fake);
});

jQuery('body').on('change','#company_logo3', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value2").html(fake);
});

jQuery('body').on('change','#company_logo4', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value3").html(fake);
});

jQuery('body').on('change','#company_logo5', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value4").html(fake);
});

jQuery('body').on('change','#company_logo6', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value5").html(fake);
});

jQuery('body').on('change','#company_logo7', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value6").html(fake);
});

jQuery('body').on('change','#company_logo8', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value7").html(fake);
});

jQuery('body').on('change','#company_logo9', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value8").html(fake);
});

jQuery('body').on('change','#company_logo10', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value9").html(fake);
});

jQuery('body').on('change','#company_logo11', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value10").html(fake);
});

jQuery('body').on('change','#company_logo12', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value11").html(fake);
});

jQuery('body').on('change','#company_logo13', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value12").html(fake);
});

jQuery('body').on('change','#company_logo14', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value13").html(fake);
});

jQuery('body').on('change','#company_logo15', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value14").html(fake);
});

jQuery('body').on('change','#company_logo16', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value15").html(fake);
});

jQuery('body').on('change','#company_logo17', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value16").html(fake);
});

jQuery('body').on('change','#company_logo18', function(){ 
 var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value17").html(fake);
});


jQuery('.action_yes').click(function()
{ 
	//var str = jQuery(this).attr('rel');
	var str = jQuery('#delid').val();
    jQuery.ajax({
       type: 'get',
       data: 'occasion_id='+str,
       url: '<?php echo url('delete-my-occasion-imagewithajax'); ?>',
       success: function(responseText){
        jQuery('#del'+responseText).hide();
        //jQuery('.pictureformat').text("Deleted successfully.");
        jQuery('.action_popup').fadeOut(500); 
		jQuery('.overlay_popup').fadeOut(500);
		//location.reload();
      }       
    });
});
function myFunction() {
   document.getElementById("filter").submit();
}
/*jQuery(document).ready(function() {
		jQuery('#filter').on('change', function() {
			this.form.submit();
		});
	});*/
</script>


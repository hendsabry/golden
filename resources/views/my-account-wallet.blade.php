@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')}}</h1>
      <div class="field_group top_spacing_margin_occas">
	    <form name="form1" id="updatewallet" method="post" action="{{ route('updatewalletdashboard') }}">
        {{ csrf_field() }}
        <div class="main_user my_padd">
		    @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
          <div class="add_sar">{{ (Lang::has(Session::get('lang_file').'.ADD_SAR')!= '')  ?  trans(Session::get('lang_file').'.ADD_SAR'): trans($OUR_LANGUAGE.'.ADD_SAR')}} </div>
          <div class="wallet_balance">{{ (Lang::has(Session::get('lang_file').'.WALLET_BALANCE')!= '')  ?  trans(Session::get('lang_file').'.WALLET_BALANCE'): trans($OUR_LANGUAGE.'.WALLET_BALANCE')}} 
          <span>SAR 
           <?php 
            $userid  = Session::get('customerdata.user_id');
            $getInfo = Helper::getuserinfo($userid); echo number_format($getInfo->wallet,2);
            ?>
          </span>
          </div>
          <div class="choose_amt">{{ (Lang::has(Session::get('lang_file').'.CHOOSE_AN_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.CHOOSE_AN_AMOUNT'): trans($OUR_LANGUAGE.'.CHOOSE_AN_AMOUNT')}}</div>
          <div class="amt_box">
            <ul id='list'>
              <li id="l1">SAR 1000</li>
              <li id="l2">SAR 2000</li>
              <li id="l3">SAR 5000</li>
              <li id="l4">SAR 10000</li>
            </ul>
          </div>
          <div class="amt_input">
            <input type="text" name="amount" onkeypress="return isNumber(event)" maxlength="9" id="amount" required="" autocomplete="off" placeholder="{{ (Lang::has(Session::get('lang_file').'.ENTER_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.ENTER_AMOUNT'): trans($OUR_LANGUAGE.'.ENTER_AMOUNT')}}">
          </div>
          <div class="payment_method_my">
            <!--<div class="pay_head">{{ (Lang::has(Session::get('lang_file').'.MY_PAYMENT_METHOD')!= '')  ?  trans(Session::get('lang_file').'.MY_PAYMENT_METHOD'): trans($OUR_LANGUAGE.'.MY_PAYMENT_METHOD')}}</div>
            <div class="pay_area">
              <div class="cred">
                <input id="male" name="re" type="radio">
                <label for="male" class="crd_text">{{ (Lang::has(Session::get('lang_file').'.CREDIT_CARD')!= '')  ?  trans(Session::get('lang_file').'.CREDIT_CARD'): trans($OUR_LANGUAGE.'.CREDIT_CARD')}}</label>
                <span class="crds"><img src="{{ url('') }}/themes/images/card.jpg" alt="" /></span> </div>
              <div class="cred">
                <input id="DebitCard" name="re" type="radio">
                <label for="DebitCard" class="crd_text">{{ (Lang::has(Session::get('lang_file').'.DEBIT_CARD')!= '')  ?  trans(Session::get('lang_file').'.DEBIT_CARD'): trans($OUR_LANGUAGE.'.DEBIT_CARD')}}</label>
                <span class="crds">
                <select name="debit_card" id="debit_card">
                  <option value="">{{ (Lang::has(Session::get('lang_file').'.CHOOSE_AN_OPTION')!= '')  ?  trans(Session::get('lang_file').'.CHOOSE_AN_OPTION'): trans($OUR_LANGUAGE.'.CHOOSE_AN_OPTION')}}</option>
                </select>
                </span> </div>
              <div class="cred">
                <input id="netbank" name="re" type="radio">
                <label for="netbank" class="crd_text">{{ (Lang::has(Session::get('lang_file').'.NET_BANKING')!= '')  ?  trans(Session::get('lang_file').'.NET_BANKING'): trans($OUR_LANGUAGE.'.NET_BANKING')}}</label>
                <span class="crds">
                <select name="netbank" id="netbank">
                  <option value="">{{ (Lang::has(Session::get('lang_file').'.CHOOSE_AN_OPTION')!= '')  ?  trans(Session::get('lang_file').'.CHOOSE_AN_OPTION'): trans($OUR_LANGUAGE.'.CHOOSE_AN_OPTION')}}</option>
                </select>
                </span> </div>
            </div>-->
            <div class="cont">
              <input type="submit" name="submit" class="form-btn" value="{{ (Lang::has(Session::get('lang_file').'.CONTINUE')!= '')  ?  trans(Session::get('lang_file').'.CONTINUE'): trans($OUR_LANGUAGE.'.CONTINUE')}}" />
            </div>
          </div>
        </div>
		</form>
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
<script type="text/javascript">

jQuery("#updatewallet").validate({
                  ignore: [],
                  rules: {
                  amount: {
                       required: true,
                      },                       
                 
                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
               
           messages: {
             amount: {
         required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AMOUNT')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_AMOUNT') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_AMOUNT') }} @endif",
                      },  
 
                                                  
                     
                },
                invalidHandler: function(e, validation){
                var valdata=validation.invalid;
                     },

                submitHandler: function(form) {
                    form.submit();
                }
            });
function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
 }
 

jQuery(document).ready(function() 
{
   jQuery('ul[id*=list] li').click(function() {
   var getamt = jQuery(this).text();
   var replaceword = jQuery.trim(getamt.replace('SAR',''));
     jQuery('#amount').val(replaceword); 
   });
});
</script>

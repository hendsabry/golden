@include('includes.navbar')
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
 @include('includes.searchweddingandoccasions')
</div>
 

<div class="page-left-right-wrapper">
@include('includes.mobile-modify')

<div class="page-right-section">

 <!-- budget-menu-outer -->
<div class="form_title">{{ (Lang::has(Session::get('lang_file').'.RECEPTION_AND_HOSPITALITY')!= '')  ?  trans(Session::get('lang_file').'.RECEPTION_AND_HOSPITALITY'): trans($OUR_LANGUAGE.'.RECEPTION_AND_HOSPITALITY')}}</div>
<div class="diamond-area">
 
 @php  $i=1; $j=1;  $getC = $vendor->count(); @endphp
 <div class="diamond_main_wrapper">
    <div class="diamond_wrapper_outer">
    <div class="diamond_wrapper_main">
      <div class="diamond_wrapper_inner">

@foreach($vendor as $getallcats)
<!-- TILL 5 RECORD -->
  @if($getC <=5) 
@php
  if($getC<=4)
                  {
                  $bgImg = str_replace('thumb_','',$getallcats->mc_img);  
                  }
                  else
                  {
                  $bgImg = $getallcats->mc_img;  
                  }

      @endphp
        <div class="row_{{$i}}of{{$getC}} rows{{$getC}}row">
         <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
            <div class="category_wrapper @if($getC!=5 && $getC!=4  && $getC!=2) category_wrapper{{$i}} @endif" style="background:url({{ $bgImg or '' }});">
              <div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}}</div><div class="clear"></div></div>
            </div>
          </a>
        </div>
<!-- TILL 6 RECORD -->
  @elseif($getC == 6)
 
          @if($i != 3 && $i != 4) 
          @php if($i==5){ $M=4; } elseif($i==6){ $M=5; }else { $M=$i; } @endphp
          <div class="row_{{$M}}of5 rows5row">
          <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
          <div class="category_wrapper" style="background:url({{ $getallcats->mc_img or '' }});">
          <div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}}</div></div>
          </div>
          </a>
          </div>  
          @else
          @if($i==3) <div class="row_3of5 rows5row">  @endif
          <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
          <span class="category_wrapper  @if($i==3) category_wrapper2 @else category_wrapper3 @endif" style="background:url({{ $getallcats->mc_img or '' }});">
          <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span><span class="clear"></span></span>
          </span>
          </a>

          @if($i==4)  <div class="clear"></div>   
         </div>   @endif  
  @endif
<!-- TILL 7 RECORD -->
  @elseif($getC == 7)

          @if($i != 3 && $i != 4 && $i != 5) 
          @php if($i==6){ $j = 4;} if($i==7){ $j = 5;}  @endphp
          <div class="row_{{$j}}of5 rows5row">
          <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
          <div class="category_wrapper" style="background:url({{ $getallcats->mc_img or '' }});">
          <div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}}</div></div>
          </div>
          </a>
          </div>  
          @else
          @if($i==3) <div class="row_3of5 rows5row">  @endif
          <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
          <span class="category_wrapper  @if($i==3) category_wrapper4 @elseif($i==4) category_wrapper5 @else category_wrapper6 @endif" style="background:url({{ $getallcats->mc_img or '' }});">
          <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span><span class="clear"></span></span>
          </span>
          </a>

          @if($i==5)  <div class="clear"></div>   
          </div>   @endif  
  @endif
 <!-- TILL 8 RECORD -->
 @elseif($getC == 8)
 
        @if($i==1 || $i==8 )
        @php if($i==1) { $k = 1;} if($i==8) { $k = 5;}   @endphp
       
          <div class="row_{{$k}}of5 rows5row">
          <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
          <div class="category_wrapper category_wrapper1" style="background:url({{ $getallcats->mc_img or '' }});">
          <div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}}</div></div>
          </div>
          </a>   
          </div>
          @else
          @if($i==2 ||$i==4 ||$i==6)   <div class="row_3of5 rows5row"> @endif
          @php if($i==2) { $P = 2;} if($i==3) { $P = 3;}  if($i==4) { $P = 2;}  if($i==5) { $P = 3;}  if($i==6) { $P = 7;}  if($i==7) { $P = 8;}  @endphp

          <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
          <span class="category_wrapper category_wrapper{{$P}}" style="background:url({{ $getallcats->mc_img or '' }});">
          <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span><span class="clear"></span></span>
          </span>
          </a>
          @if($i==3 ||$i==5 ||$i==7)  
          <div class="clear"></div>
          </div>@endif
 @endif
  <!-- TILL 9 RECORD -->
@elseif($getC == 9)

        @if($i==1 || $i==9 )
        @php if($i==1){$k=1; } if($i==9){$k=5;   } @endphp
        <div class="row_{{$k}}of5 rows5row">
        <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
        <div class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats->mc_img or '' }});">
        <div class="category_title"><div class="category_title_inner">{{ $getallcats->mc_name or ''}}</div></div>
        </div>
        </a>   
        </div>
        @elseif($i==2 || $i==3 )
 
        @if($i==2) <div class="row_2of5 rows5row"> @endif 
        <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
        <span class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats->mc_img or '' }});">
        <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span><span class="clear"></span></span>
        </span>
        </a>

        @if($i==3)    <div class="clear"></div>
        </div>
        @endif 

        @elseif($i==4 || $i==5 || $i==6 )


        @if($i==4) <div class="row_3of5 rows5row"> @endif 
        <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
        <span class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats->mc_img or '' }});">
        <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span><span class="clear"></span></span>
        </span>
        </a>

        @if($i==6)    <div class="clear"></div>
        </div>
        @endif 

        @elseif($i==7 || $i==8 )

        @if($i==7) <div class="row_4of5 rows5row">@endif 
        <a href="{{ route('reception-and-hospitality-details',['id'=>$getallcats->mc_id,'sid'=>request()->sid,'vid'=>$getallcats->vendor_id])}}">
        <span class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats->mc_img or '' }});">
        <span class="category_title"><span class="category_title_inner">{{ $getallcats->mc_name or ''}}</span><span class="clear"></span></span>
        </span>
        </a>
       
        @if($i==8) <div class="clear"></div>
        </div> @endif            
        @endif

@endif

   <!-- END ALL LOOP -->


@php $i= $i+1; $j=$j+1; @endphp
@endforeach

 </div>
    </div>
    </div>
  </div>
 



<div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
<!-- START PAGINATION -->
 <div class="pagnation-area">{{ $vendor->links() }}</span></div> 
<!-- END PAGINATION -->
</div>

 

 <!-- budget-carousel-area -->


</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')


  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

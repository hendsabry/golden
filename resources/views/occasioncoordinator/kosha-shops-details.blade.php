@include('includes.navbar')
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
   
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 

if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 

  @endphp
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
@if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !='')
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
      @endif    

   <li><a href="#kosha">{{ (Lang::has(Session::get('lang_file').'.Choose_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Choose_Kosha'): trans($OUR_LANGUAGE.'.Choose_Kosha')}}</a></li>

          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
   
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>

  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?>
                <li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</div>
          <div class="detail_hall_description">@if($lang == 'en_lang')  {{$vendordetails->address}} @else   {{$vendordetails->address_ar}} @endif</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')}}</div>
          <div class="detail_about_hall">
            <div class="comment more"> @if($lang != 'en_lang') {{$vendordetails->mc_discription_ar}} @else {{$vendordetails->mc_discription}} @endif</div>
          </div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

 

@if($vendordetails->google_map_address!='') 
 @php $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp
   <div class="detail_hall_dimention" id="map"  width="450" height="230" style="height: 230px!important;"> </div>
 @endif
 

        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>
  @if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !='')

        <div class="service-video-area">
         @if($vendordetails->mc_video_description!='') <div class="service-video-cont">
          @if($lang != 'en_lang')  {{$vendordetails->mc_video_description_ar}} @else  {{$vendordetails->mc_video_description}} @endif</div>@endif
          <div class="service-video-box">
        @if($vendordetails->mc_video_url !='')    <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> @endif
          </div>
        </div>
@endif

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  @foreach($allreview as $val)
           
                   @php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
   <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                <li><a href="{{ route('kosha-shop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1]) }}" @if(request()->type!=2) class="select" @endif>{{ (Lang::has(Session::get('lang_file').'.Design_Your_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Design_Your_Kosha'): trans($OUR_LANGUAGE.'.Design_Your_Kosha')}} </a></li>
                <li><a  @if(request()->type==2) class="select" @endif href="{{ route('kosha-redymadeshop-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2]) }}">{{ (Lang::has(Session::get('lang_file').'.Ready_Made_Kosha')!= '')  ?  trans(Session::get('lang_file').'.Ready_Made_Kosha'): trans($OUR_LANGUAGE.'.Ready_Made_Kosha')}} </a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
 

@if(request()->type!=2) 

<div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
          <ul>
          @for($i=0;$i< count($menu[0]->sub_menu); $i++)
          @php $k = $i+1; @endphp
           <li onclick="sela('{{addslashes($menu[0]->sub_menu[$i]['attribute_title'])}}','{{$menu[0]->sub_menu[$i]['id']}}','{{$k}}');"><a href="#o" id="{{$menu[0]->sub_menu[$i]['id']}}"  class="attb @if($k==1) select @endif" >{{$menu[0]->sub_menu[$i]['attribute_title']}} </a></li>     
          @endfor
          </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line -->
 
             

@if(count($menu[0]->sub_menu) >=1)
  <div class="kosha-select-area"> 
          <div class="kosha-tab-area" id="table1">
            <div class="kosha-heading">{{ (Lang::has(Session::get('lang_file').'.SELECT')!= '')  ?  trans(Session::get('lang_file').'.SELECT'): trans($OUR_LANGUAGE.'.SELECT')}} <span id="displayCats">{{$menu[0]->sub_menu[0]['attribute_title']}}</span></div>
            <div class="kosha-box"> 
            @for($ks=0;$ks < count($menu[0]->sub_menu); $ks++)
            @for($i=0;$i< count($menu[0]->sub_menu[0]['product']); $i++)
            @php if($menu[0]->sub_menu[$ks]['product'][$i]['pro_title']==''){ continue;} $ksp = $ks+1; @endphp
            <div class="kosha-select-line items_{{$ksp}}" @if($ksp!=1) style="display:none;" @endif> 
                @php    $pro_id = $menu[0]->sub_menu[$ks]['product'][$i]['pro_id']; @endphp
                 <div class="kosha-select-img">
           
             @include('includes/product_multiimages')

 

                </div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_title'] }} </div>
                  <div class="kosha-select-description">{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_desc'] }}</div>
                  @if($menu[0]->sub_menu[$ks]['product'][$i]['pro_qty']>0)
                    <div class="service-radio-line">
              <div class="service_quantity_box" id="service_quantity_box">
                <div class="service_qunt">@if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY')}}  @else {{ trans($OUR_LANGUAGE.'.QUANTITY')}} @endif</div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity" data-title="Total Quality">
                    <div class="quantity">
                      <button type="button" id="sub" class="sub"></button>
                      <input type="number" name="itemqty" id="qty_{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantity('add','{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}');" onkeydown="isNumberKey(event); checkquantity('add','{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}');" />
                      <button type="button" id="add" class="add" onClick="return checkquantity('add','{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                  </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
            </div>
            @endif
                  @php 
                       
                       $Insuranceamount = $menu[0]->sub_menu[$ks]['product'][$i]['Insuranceamount']; 
                    
                  @endphp

                  @if(isset($Insuranceamount) && $Insuranceamount!='')
                  <div class="kosha-select-prise"> {{ (Lang::has(Session::get('lang_file').'.INSURANCE')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE'): trans($OUR_LANGUAGE.'.INSURANCE')}} {{ currency($Insuranceamount,'SAR',$Current_Currency) }}</div>
                   @endif
                  
                @php  
 
                $Disprice = $menu[0]->sub_menu[$ks]['product'][$i]['pro_discount_percentage']; 

                
                $originalP = $menu[0]->sub_menu[$ks]['product'][$i]['pro_price'];
                if($Disprice==''){ $getAmount = 0; }else {$getAmount = ($originalP * $Disprice)/100;}
                

                $DiscountPricea = $originalP - $getAmount; 
                $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');
                @endphp


  @if($Disprice >=1) 
   <div class="kosha-select-prise "> <span class="strike">{{ currency($menu[0]->sub_menu[$ks]['product'][$i]['pro_price'],'SAR',$Current_Currency) }}    </span> 


        @php $resulttotalamount=$DiscountPrice; @endphp

      {{ currency($DiscountPrice,'SAR',$Current_Currency) }}  </div>
 
@else
<div class="kosha-select-prise">  {{ currency($menu[0]->sub_menu[$ks]['product'][$i]['pro_price'],'SAR',$Current_Currency) }}</div>
 @php $resulttotalamount=$menu[0]->sub_menu[$ks]['product'][$i]['pro_price']; @endphp
@endif
        @php if(isset($Insuranceamount) && $Insuranceamount!=''){ $iamo=$Insuranceamount; }else { $iamo=0;} @endphp
       <input type="hidden" name="insuranceamount_{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}" id="insuranceamount_{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}" value="{{ currency($iamo, 'SAR',$Current_Currency, $format = false) }}">
       <input type="hidden" name="netamountforqty_{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}" id="netamountforqty_{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}" value="{{ currency($resulttotalamount, 'SAR',$Current_Currency, $format = false) }}">
<span id="grandtotal_{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}"></span>
                @if($menu[0]->sub_menu[$ks]['product'][$i]['pro_qty']>0)
                  <div class="kosha-select-radieo">
                  
                    <input type="radio" name="re{{$ksp}}" class="selct" data-attribute="{{ $menu[0]->sub_menu[$ks]['product'][$i]['attribute_id'] }}" data-from="{{$ksp}}" data-title="{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_title'] }}" data-currency="{{ Session::get('currency') }}"  data-price="{{  currency($DiscountPrice, 'SAR',$Current_Currency, $format = false) }}" data-insurance="{{  currency($Insuranceamount, 'SAR',$Current_Currency, $format = false) }}" data-img="{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_Img'] }}" data-id="{{ $menu[0]->sub_menu[$ks]['product'][$i]['pro_id'] }}" />
                    <label>&nbsp;</label>
                  </div>
                  @else
                    <span id="sold" class="form-btn addto_cartbtn" >{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')}}</span>
                  @endif
                </div>
              </div> <!-- kosha-select-line -->
            @endfor 
       @endfor         
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
              <!-- kosha-tab-area -->
        </div>
        <!-- kosha-select-area -->
          @else

 
        <div class="no-record-area">  
    


          {{ (Lang::has(Session::get('lang_file').'.No_product_found_in_this_category')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_category'): trans($OUR_LANGUAGE.'.No_product_found_in_this_category')}}
</div> 



          @endif


      

 <form name="form1" method="post" id="add-container" action="{{ route('kosha-add-to-cart') }}"" enctype="multipart/form-data">
                {{ csrf_field() }}
          <input type="hidden" name="cart_type" value="occasion">
          <input type="hidden" name="cart_sub_type" value="cosha">
          <input type="hidden" name="language_type" value="en">
          <input type="hidden" name="attribute_id" value="47">
          <input type="hidden" name="id" value="{{request()->id}}">
          <input type="hidden" name="sid" value="{{request()->sid}}">
          <input type="hidden" name="vid" value="{{request()->vid}}">
          <input type="hidden" name="product_id" value="{{request()->id}}">
          <div class="kosha-selections-area" style="display: none;">
  
          <div class="kosha-heading">{{ (Lang::has(Session::get('lang_file').'.Your_Selections')!= '')  ?  trans(Session::get('lang_file').'.Your_Selections'): trans($OUR_LANGUAGE.'.Your_Selections')}} </div>
          <div class="kosha-box">
            <div class="selection-box-area">             
            @for($i=1;$i<= count($menu[0]->sub_menu); $i++)
            <span class="info_{{$i}}"></span>
            @endfor
            </div>
            <!-- selection-box-area -->
            <div class="kosha-tolat-area" >
              <div class="kosha-tolat-prise">{{ (Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')}} : <span id="totalPrice">SAR 0</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" class="form-btn" />
              </div>
            </div>


            <!-- kosha-tolat-area -->
          </div>
          <!-- kosha-box -->
        </div>
  </form>
        <!-- kosha-selections-area -->
      </div>  <!-- kosha-area -->

@else
    
 
@if(count($product) < 1)

<div class="kosha-area">
        <div class="kosha-tab-line">  
          <div id="kosha-tab" class="no-record-area">
{{ (Lang::has(Session::get('lang_file').'.No_product_found_in_this_category')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_category'): trans($OUR_LANGUAGE.'.No_product_found_in_this_category')}}
          </div> </div> </div>

@else
 
 <div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
       
            <ul>
              @php $f=1; @endphp
              @foreach($product as $pro)
              <li onclick="redy('table{{$f}}','{{ $pro->pro_id }}');"><a href="#o" id="{{$pro->pro_id }}" class="redycls @if($f==1) select @endif">{{$pro->pro_title}}</a></li>
            @php $f=$f+1; @endphp
             @endforeach
            </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line --> 
       <div class="rm-kosha-outer">
        <div class="rm-kosha-area">
        @php $h=1; @endphp 
  @foreach($product as $pro)
 <form name="form1" method="post" id="add-kosha" action="{{ route('kosha-add-to-cart') }}"">
                {{ csrf_field() }}
          <div class="kosha-tab-area redymades" id="table{{$h}}" @if($h!=1) style="display: none;" @endif>
            <div class="rm-kosha-name">{{$pro->pro_title}}  </div>
            <div class="kosha-box">
               <div class="rm-kosha-img">
@php    $pro_id = $pro->pro_id; @endphp
             @include('includes/product_multiimages')
           </div>
         <div class="rm-kosha-text">{{$pro->pro_desc}}




         </div>






         <div class="kosha-tolat-area">



               @if($pro->pro_qty>0)
                    <div class="service-radio-line">
              <div class="service_quantity_box" id="service_quantity_box">
                <div class="service_qunt">@if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY')}}  @else {{ trans($OUR_LANGUAGE.'.QUANTITY')}} @endif</div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity" data-title="Total Quality">
                    <div class="quantity koshaqty">
                      <button type="button" id="sub" class="sub" onClick="return checkquantityforreadymade('remove','{{$pro->pro_id}}');"></button>
                      <input type="number" name="itemqty" id="qty_{{$pro->pro_id}}" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantityforreadymade('add','{{$pro->pro_id}}');" onkeydown="isNumberKey(event); checkquantityforreadymade('add','{{$pro->pro_id}}');" />
                      <button type="button" id="add" class="add" onClick="return checkquantityforreadymade('add','{{$pro->pro_id}}');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                  </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
            </div>
            @endif


              <div class="kosha-tolat-prise kos_prc"> 
                  @php if($pro->Insuranceamount>0){ @endphp
                 <span>{{ (Lang::has(Session::get('lang_file').'.INSURANCE')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE'): trans($OUR_LANGUAGE.'.INSURANCE')}} {{ currency($pro->Insuranceamount,'SAR',$Current_Currency) }}  </span><br>

                 @php } @endphp
              @php
                $Disprice = $pro->pro_discount_percentage; 
               $originalP = $pro->pro_price;
                if($Disprice==''){  $getAmount = 0;  }  else   {$getAmount = ($originalP * $Disprice)/100;}
               
              $DiscountPricea = $originalP - $getAmount; 
              $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');
              @endphp

              @if($Disprice >=1) 

              <span class="strike">{{ currency($originalP,'SAR',$Current_Currency) }}   </span> <span >{{ currency($DiscountPrice,'SAR',$Current_Currency) }} </span> 
              @else
              <span>{{ currency($originalP,'SAR',$Current_Currency) }}  </span>

              

              @endif
@php if($Disprice >=1) 
    $nprice=$DiscountPrice;
 else
    $nprice=$originalP;
  @endphp
              <br>
              @php $realprice=$nprice+$pro->Insuranceamount; @endphp
                {{ (Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')}}:
              <span id="grandtotal_{{$pro->pro_id}}">{{ currency($realprice,'SAR',$Current_Currency) }}  </span>
              </div>
 

              <div class="kosha-button-area">
                <input type="hidden" name="insuranceamount_{{$pro->pro_id}}" id="insuranceamount_{{$pro->pro_id}}" value="{{$pro->Insuranceamount}}">
                <input type="hidden" name="netamountforqty_{{$pro->pro_id}}" id="netamountforqty_{{$pro->pro_id}}" value="{{ $nprice}}">
                <input type="hidden" name="totalpaidamount_{{$pro->pro_id}}" id="totalpaidamount_{{$pro->pro_id}}" value="{{ $realprice }}">
              <input type="hidden" name="product_id" value="{{$pro->pro_id}}">
              <input type="hidden" name="cart_type" value="occasion">
              <input type="hidden" name="language_type" value="en">
              <input type="hidden" name="attribute_id" value="46">
              <input type="hidden" name="cart_sub_type" value="cosha">
              <input type="hidden" name="id" value="{{request()->id}}">
              <input type="hidden" name="sid" value="{{request()->sid}}">
              <input type="hidden" name="vid" value="{{request()->vid}}">
              @if($pro->pro_qty>0)
               <input type="submit" value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" class="form-btn" />
               @else
               <span id="sold" class="form-btn addto_cartbtn" >{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')}}</span>
               @endif
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
        </form>
 @php $h=$h+1; @endphp
 @endforeach
        </div> <!-- rm-kosha-area -->
          </div> <!-- rm-kosha-outer -->
      </div>  <!-- kosha-area -->
@endif
@endif

 <!--service-display-section-->
    @include('includes.other_services')
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
@include('includes.footer')
@include('includes.popupmessage')

 <script type="text/javascript"> 

/* Remove Items from selection list */
$('body').on('click','.remove', function(){ 
        var title =  $(this).data('rid');
        var unchk =  $(this).data('unchk');
        $('.'+title).html('');
        $( "input[name='"+unchk+"']" ).prop('checked',false);
        var totalP = 0;
        var getP = 0;
        $( ".tprice" ).each(function() {
        getP =  $(this).val();
        totalP = parseFloat(totalP) + parseFloat(getP);
        }); 
        totalP = parseFloat(totalP).toFixed(2);
        $("#totalPrice").html('SAR '+ totalP); 

        if(totalP == 0)
        {
        $(".kosha-selections-area").hide();  
        }
});
/* Remove Items from selection list end */
 

 $('.selct').click(function()
 {
  var title =  $(this).data('title');
  var from =  $(this).data('from');
  var price =  $(this).data('price');  
  var img =   $(this).data('img');
  var pid =   $(this).data('id');
  var pinsurance =   $(this).data('insurance');
  var attribute =   $(this).data('attribute');
  var qty=$('#qty_'+pid).val();

  var currency =  $(this).data('currency'); 
  var totalprice=parseFloat(price) + parseFloat(pinsurance);
  var totalpriceaccordingqty=qty*totalprice;
  var totaldprice=totalpriceaccordingqty.toFixed(2);

  <?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') { $qtytext=trans(Session::get('lang_file').'.QUANTITY');} else { $qtytext=trans($OUR_LANGUAGE.'.QUANTITY');} ?>

  $(".kosha-selections-area").show();  
  $(".info_"+from).html('');
  $(".info_"+from).append('<div class="selection-box"><div class="selection-img"><img src="'+img+'" alt="" /></div><div class="selection-name">'+title+' <small class="small_oty"> <?php echo $qtytext;?>: '+qty+' </small><input type="hidden" class="tprice" name="itemprice[]" value="'+totaldprice+'"><input type="hidden" name="itemqty'+pid+'" value="'+qty+'"><input type="hidden" name="product[attribute_id][]" value="'+attribute+'"><input type="hidden" name="product[pro_id][]" value="'+pid+'"></div><div class="selection-prise">'+currency+' '+totaldprice+'</div><span class="remove" data-unchk="re'+from+'" data-rid="info_'+from+'"><img src="{{ url('') }}/themes/images/delete.png"/></span></div>');
    var totalP = 0;
    var getP = 0;
    $( ".tprice" ).each(function() {
    getP =  $(this).val();
    totalP = parseFloat(totalP) + parseFloat(getP);
    }); 

   totalP = parseFloat(totalP).toFixed(2)

$("#totalPrice").html(currency+' '+totalP); 
  });

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
  $('.flexslider').resize(); // this is it
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
function pricecalculation(act)
{
  var no=1;
  var product_id      = document.getElementById('product_id').value;
  var product_size    = document.getElementById('product_size').value;
  var currentquantity = document.getElementById('qty').value;
  var unititemprice   = document.getElementById('priceId').value;
  if(act=='add')
  {
    var qty = parseInt(currentquantity)+parseInt(no);     
  }
  else
  { 
    if(parseInt(currentquantity)==1)
    {
      var qty=parseInt(currentquantity)
    }
    else
    {
      var qty=parseInt(currentquantity)-parseInt(no);
    }
  }
  
  if(product_size)
  {
   $.ajax({
     type:"GET",
     url:"{{url('getSizeQuantity')}}?product_id="+product_id+'&product_size='+product_size+'&qty='+qty,
     success:function(res)
     {               
    
     if(res!='ok')
     {
      //alert(res);
       alert('Not available quantity');
       var qtyupdated=parseInt(currentquantity);      
       document.getElementById('qty').value=qtyupdated;
     }
     else
     {
        //alert(res);
        //alert('er');
      var producttotal = qty*unititemprice;
      document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
      document.getElementById('itemqty').value=qty;
     }
     }
   });
  }
  else
  {
    var producttotal = qty*unititemprice;
    document.getElementById('cont_final_price').innerHTML = 'SAR '+parseFloat(producttotal).toFixed(2);
    document.getElementById('itemqty').value=qty;
  }
      
}






function showProductDetail(str,vendorId)
{
    
     $.ajax({
     type:"GET",
     url:"{{url('getShoppingProductInfo')}}?product_id="+str+'&vendor_id='+vendorId,
     success:function(res)
     {               
    if(res)
    { 
      $('html, body').animate({
        scrollTop: ($('.service-display-left').first().offset().top)
      },500);
      var json = JSON.stringify(res);
      var obj = JSON.parse(json);
          console.log(obj);
      length=obj.productdateshopinfo.length;
      //alert(length);
      document.getElementById('qty').value=1;
      if(length>0)
      {
           for(i=0; i<length; i++)
         {         
              $('#selectedproduct').html('<input type="hidden" id="product_id" name="product_id" value="'+obj.productdateshopinfo[i].pro_id+'"><input type="hidden" id="priceId" name="priceId" value="'+obj.productdateshopinfo[i].pro_price+'"><input type="hidden" id="vendor_id" name="vendor_id" value="'+obj.productdateshopinfo[i].pro_mr_id+'"><div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productdateshopinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productdateshopinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productdateshopinfo[i].pro_desc+'</div>');          
               $('#cont_final_price').html('SAR '+obj.productdateshopinfo[i].pro_price);      
         }
      }
      if($.trim(obj.productattrsize) !=1)
      {
        $('#ptattrsize').html(obj.productattrsize);
      $('#ptattrsizeenone').css('display','block');
      }
      else
      {
        $('#ptattrsizeenone').css('display','none');
      }   
      }
     }
   });
}
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
<script>
jQuery(document).ready(function(){
 jQuery("#cartfrm").validate({
    rules: {       
          "product_size" : {
            required : true
          },    
         },
         messages: {
          "product_size": {
            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_YOUR_SIZE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_YOUR_SIZE') }} @endif'
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#cartfrm").valid()) {        
    jQuery('#cartfrm').submit();
   }
  });
});
</script>
 
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 @if(request()->type!='') 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 @endif
 <script type="text/javascript">
   function checkquantity(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty_'+prodid).value;
         var insuranceamount  = document.getElementById('insuranceamount_'+prodid).value;
         var netamountforqty  = document.getElementById('netamountforqty_'+prodid).value;
          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }

   $.ajax({
       type:"GET",
       url:"{{url('checkcoshaquantity')}}?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_'+productId).value = qtyupdated - 1;
        
       }
       else
       {
         var totalinsurance=parseFloat(insuranceamount)*parseInt(qty);
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         var grandtotal=parseFloat(totalinsurance)+parseFloat(totalcalcamount);
         jQuery('#grandtotal_'+productId).html('<div class="kosha-tolat-prise" style="margin-top:10px;"><span><?php echo $Total_Price; ?></span> <span><?php echo $Cur; ?></span> <span>'+grandtotal.toFixed(2)+'</span></div>');

         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }
jQuery(document).ready(function(){
jQuery('.slideTwo').css('height', '100%');

});

    

 </script>


<script type="text/javascript">
   function checkquantityforreadymade(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty_'+prodid).value;
         var insuranceamount  = document.getElementById('insuranceamount_'+prodid).value;
         var netamountforqty  = document.getElementById('netamountforqty_'+prodid).value;         


          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }

   $.ajax({
       type:"GET",
       url:"{{url('checkcoshaquantity')}}?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_'+productId).value = qtyupdated - 1;
        
       }
       else
       {
         var totalinsurance=parseFloat(insuranceamount)*parseInt(qty);
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         var grandtotal=parseFloat(totalinsurance)+parseFloat(totalcalcamount);
         jQuery('#grandtotal_'+productId).html('<div class="kosha-tolat-prise" style="margin-top:10px;"><span><?php echo $Total_Price; ?></span> <span><?php echo $Cur; ?></span> <span>'+grandtotal.toFixed(2)+'</span></div>');
         document.getElementById('totalpaidamount_'+productId).value = grandtotal;
         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }
jQuery(document).ready(function(){
jQuery('.slideTwo').css('height', '100%');

});

    

 </script>
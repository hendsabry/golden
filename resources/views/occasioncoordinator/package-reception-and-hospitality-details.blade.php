@include('includes.navbar')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 

if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 

@endphp

<script src="{{url('/')}}/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>


<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap ">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap ">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
@if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !='')
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
      @endif    
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
   
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>

  <!-- common_navbar -->
  <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?>
                <li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</div>
          <div class="detail_hall_description">{{$vendordetails->address}}</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')}}</div>
          <div class="detail_about_hall">
            <div class="comment more"> {{$vendordetails->mc_discription }} </div>
          </div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

@if($vendordetails->google_map_address!='')   
   <div class="detail_hall_dimention"><iframe src="{{ $vendordetails->google_map_address }}" width="450" height="230" frameborder="0" style="border:0" allowfullscreen></iframe></div>
 @endif
            
        </div>
      </div>

      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>

      

  @if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !='')

        <div class="service-video-area">
         @if($vendordetails->mc_video_description!='') <div class="service-video-cont">
          @if($lang != 'en_lang')  {{$vendordetails->mc_video_description_ar}} @else  {{$vendordetails->mc_video_description}} @endif</div>@endif
          <div class="service-video-box">
        @if($vendordetails->mc_video_url !='')    <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> @endif
          </div>
        </div>
@endif

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  @foreach($allreview as $val)
           
                   @php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div> 

  <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                
                <li><a href="{{ route('reception-and-hospitality-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1]) }}" @if(request()->type!=2) class="select" @endif>{{ (Lang::has(Session::get('lang_file').'.Design_your_Package')!= '')  ?  trans(Session::get('lang_file').'.Design_your_Package'): trans($OUR_LANGUAGE.'.Design_your_Package')}} </a></li>
               
                <li><a  @if(request()->type==2) class="select" @endif href="{{ route('package-reception-and-hospitality-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2]) }}">{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}} </a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
         
 
@if(count($product) < 1)

<div class="kosha-area">
        <div class="kosha-tab-line">  
          <div id="kosha-tab" class="content">
{{ (Lang::has(Session::get('lang_file').'.No_product_found_in_this_package')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_package'): trans($OUR_LANGUAGE.'.No_product_found_in_this_package')}}
          </div> </div> </div>

@else
 
 <div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
       
            <ul>
              @php $f=1; @endphp
              @foreach($product as $pro)
              <li onclick="redy('table{{$f}}','{{ $pro->pro_id }}');"><a href="#o" id="{{$pro->pro_id }}" class="redycls @if($f==1) select @endif">{{$pro->pro_title}}</a></li>
            @php $f=$f+1; @endphp
             @endforeach
            </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line --> 
       <div class="rm-kosha-outer">
        <div class="rm-kosha-area">
        @php $h=1; 
        
        @endphp 
		
	  
	@php
	$D=1;
	@endphp	
  @foreach($product as $pro)

{!! Form::open(['url' => 'recieption_addtoCart', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}	
@php $nationalityname=Helper::getworkernationalityCountry($pro->staff_nationality) @endphp

          <div class="kosha-tab-area redymades" id="table{{$h}}" @if($h!=1) style="display: none;" @endif>
            <div class="rm-kosha-name">{{$pro->pro_title}}</div>
            <div class="kosha-box">
               <div class="rm-kosha-img"><img src="{{$pro->pro_Img}}" alt="" /></div>
         <div class="rm-kosha-text"><?php echo nl2br($pro->pro_desc); ?></div>
         <div class="rm-kosha-text">{{ (Lang::has(Session::get('lang_file').'.Number_of_Staff')!= '')  ?  trans(Session::get('lang_file').'.Number_of_Staff'): trans($OUR_LANGUAGE.'.Number_of_Staff')}} - {{$pro->no_of_staff}}
          <br>
            {{ (Lang::has(Session::get('lang_file').'.Nationality')!= '')  ?  trans(Session::get('lang_file').'.Nationality'): trans($OUR_LANGUAGE.'.Nationality')}} - {{$nationalityname->co_name or ''}} 
         </div>
          @if(isset($pro->product_packege) && count($pro->product_packege)>0)
            <div class="rm-kosha-text">
              <div class="packagetitle"> {{ (Lang::has(Session::get('lang_file').'.ITEM(S)')!= '')  ?  trans(Session::get('lang_file').'.ITEM(S)'): trans($OUR_LANGUAGE.'.ITEM(S)')}} </div>
              <ul>
                @foreach($pro->product_packege as $getpackageitems)

                @php $itemname=Helper::getproducthospitalitytoolNames($getpackageitems->item_id);
                      $pro_name = 'pro_title'; 
                      if(Session::get('lang_file')!='en_lang')
                    {
                        $pro_name = 'pro_title_ar'; 
                      }
                 @endphp
                    <li>
                      @php if(isset($itemname->pro_Img) && $itemname->pro_Img!=''){ @endphp
                      <img src="{{ $itemname->pro_Img }}" width="50px;">
                      @php } @endphp
                       {{ $itemname->$pro_name }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          @if($pro->pro_qty>0)
<div class="package_qty">
 <div id="service_quantity_box" class="service_quantity_box">
      <div class="service_qunt">@if(Lang::has(Session::get('lang_file').'.Quantity')!= '') {{ trans(Session::get('lang_file').'.Quantity')}}  @else {{ trans($OUR_LANGUAGE.'.Quantity')}} @endif </div>
        <div class="service_qunatity_row">
          
        <div data-title="Total Quality" class="td td2 quantity food-quantity">
        <div class="quantity">
    <button onclick="return pricecalculation('remove','{{$h}}');" class="sub" id="sub" type="button"></button>
    <input type="number" onkeydown="isNumberKey(event); pricecalculation('pricewithqty','{{$h}}');" onkeyup="isNumberKey(event); pricecalculation('pricewithqty','{{$h}}');" max="9" min="1" value="1" id="qty_{{$h}}" name="product_qty">
    <button onclick="return pricecalculation('add','{{$h}}');" class="add" id="add" type="button"></button></div>

 
          
       </div>
      </div>
    </div>

</div>
@endif

         <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">{{ (Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')}}: 
              @php
                $Disprice = $pro->pro_discount_percentage; 
               $originalP = $pro->pro_price;
                if($Disprice==''){  $getAmount = 0;  }  else   {$getAmount = ($originalP * $Disprice)/100;}
               
              $DiscountPricea = $originalP - $getAmount; 
              $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');



              @endphp
              @if($Disprice >=1)  
              <input type="hidden" name="orignalP" id="orignalP_{{$h}}" value="{{currency($DiscountPrice, 'SAR',$Current_Currency, $format = false) }}">
               <span class="strike">  {{ currency($originalP,'SAR',$Current_Currency) }} </span> <span id="tp_{{$h}}">{{ currency($DiscountPrice,'SAR',$Current_Currency) }} </span>
              @else
              <input type="hidden" name="orignalP" id="orignalP_{{$h}}"  value="{{currency($originalP, 'SAR',$Current_Currency, $format = false) }}">
              <span id="tp_{{$h}}">{{ currency($originalP,'SAR',$Current_Currency) }}  </span>
              @endif
              </div>
                
              <!------new form------>
			  <div class="package_box_top">
                <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Date')!= '')  ?  trans(Session::get('lang_file').'.Date'): trans($OUR_LANGUAGE.'.Date')}} </div>
                  <div class="checkout-form-bottom">
                    <input class="t-box cal-t" type="text" id="bookingdate_{{$D}}" name="bookingdate">
					<span id="showerrorbookingdate_<?php echo $D;?>" class="error"></span>
                  </div>
                </div>
                <!-- checkout-form-cell -->
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Time')!= '')  ?  trans(Session::get('lang_file').'.Time'): trans($OUR_LANGUAGE.'.Time')}} </div>
                  <div class="checkout-form-bottom">                   
                  <input class="t-box sel-time" type="text" id="bookingtime_{{$D}}" name="bookingtime">
				  <span id="showerrorbookingtime_<?php echo $D;?>" class="error"></span>
                  </div>
                </div>
                <!-- checkout-form-cell -->
              </div>
              <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')}} </div>
                  <div class="checkout-form-bottom">
                    <input class="t-box" type="text" id="location_<?php echo $D;?>" name="location">
					<span id="showerrorlocation_<?php echo $D;?>" class="error"></span>
                  </div>
                </div>
                
                
                <!-- checkout-form-cell -->
              </div>
              <!---------- end new form-------->
			  </div>




              <div class="kosha-button-area">
              <input type="hidden" name="product_id" id="product_id" value="{{$pro->pro_id}}"> 
              <input type="hidden" name="id" value="{{request()->id}}">
              <input type="hidden" name="sid" value="{{request()->sid}}">
              <input type="hidden" name="vid" value="{{request()->vid}}">
              <input type="hidden" name="cart_type" value="occasion">
              <input type="hidden" name="language_type" value="en">
              <input type="hidden" name="attribute_id" value="1">
              <input type="hidden" name="cart_sub_type" value="hospitality">
              <input type="hidden" name="nationality" value="23">
              <input type="hidden" name="products[]" value="">
              <input type="hidden" name="time" value="">
              <input type="hidden" name="date" value="">
              <input type="hidden" name="p_total_price" value="{{currency($pro->pro_price, 'SAR',$Current_Currency, $format = false)  }}">
 
                @if($pro->pro_qty>0)

               <input type="submit" value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" class="form-btn" onclick="return showValidation_<?php echo $D;?>()"/>
               @else
                 <span id="sold" class="form-btn addto_cartbtn" >{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')}}</span>
               @endif
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
       

 
 
 <script>
 
 
 
 
            // When the document is ready
            //var jql = jQuery;
            jQuery(window).load(function () {
            jQuery('#bookingdate_<?php echo $D;?>').datepicker({
            format: "d M yyyy",
            startDate: new Date(),
            }); 
            jQuery("#bookingdate_<?php echo $D;?>").datepicker("setDate", new Date());
 
            var starttime=jQuery('#opening_time').val();     
            var endtime=jQuery('#closing_time').val();
            var service_duration=jQuery('#service_duration_<?php echo $D;?>').val();
            var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A'); 
            jQuery('#bookingtime_<?php echo $D;?>').timepicker({
            'minTime': starttime,
            'maxTime': netime,
            'dynamic': true,
            'timeFormat': 'g:i A',          
            });
            });
			
			
 </script>
  <script type="text/javascript">
 function showValidation_<?php echo $D;?>()
{
  var bookingdate_<?php echo $D;?> = document.getElementById("bookingdate_<?php echo $D;?>").value;
  var bookingtime_<?php echo $D;?> = document.getElementById("bookingtime_<?php echo $D;?>").value;
  var location_<?php echo $D;?> = document.getElementById("location_<?php echo $D;?>").value;
  if(bookingdate_<?php echo $D;?> == '') 
  {
   document.getElementById('showerrorbookingdate_<?php echo $D;?>').innerHTML = '<span style="color:red;">@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE') }} @else  {{ trans($OUR_LANGUAGE.'.MER_SELECT_NATIONALITY') }} @endif</span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorbookingdate_<?php echo $D;?>').innerHTML = '';
  }
  if(bookingtime_<?php echo $D;?> == '') 
  {
   document.getElementById('showerrorbookingtime_<?php echo $D;?>').innerHTML = '<span style="color:red;">@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_TIME')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_TIME') }} @else  {{ trans($OUR_LANGUAGE.'.MER_SELECT_NATIONALITY') }} @endif</span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorbookingtime_<?php echo $D;?>').innerHTML = '';
  }
  if(location_<?php echo $D;?> == '') 
  {
   document.getElementById('showerrorlocation_<?php echo $D;?>').innerHTML = '<span style="color:red;">@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_LOCATION') }} @endif</span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorlocation_<?php echo $D;?>').innerHTML = '';
  }
}
 
        </script>
  @php $h=$h+1;  	$D=$D+1; @endphp
   </form>
 @endforeach
        </div> <!-- rm-kosha-area -->
          </div> <!-- rm-kosha-outer -->
      </div>  <!-- kosha-area -->
@endif
 
  

 <!--service-display-section-->
    @include('includes.other_services')
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
@include('includes.footer')
@include('includes.popupmessage')




<!------------ end tabs------------------>
<!--------- date picker script-------->


<script type="text/javascript">
  
function pricecalculation(str,ps)
{
  
var productId        = $('#product_id').val();
  if(str =='remove')
  {
   var Qty = parseInt($('#qty_'+ps).val())-1; 
  }
  else
  {
   var Qty = parseInt($('#qty_'+ps).val())+1;  
  }
if(Qty == 0){ return false;}

var currentquantity=$('#qty_'+ps).val();
 
$.ajax({
       type:"GET",
       url:"{{url('checkcoshaquantity')}}?product_id="+productId+'&qty='+Qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();       
         var qtyupdated = parseInt(currentquantity)-1;
          $('#qty_'+ps).val(qtyupdated);
        
       }
       else
       {
           var  totalp = $('#orignalP_'+ps).val()
           var TP = parseInt(Qty)*parseFloat(totalp);
           TP = parseFloat(TP).toFixed(2);
           <?php $Cur = Session::get('currency'); ?>
          $('#tp_'+ps).html('<?php echo $Cur; ?> '+TP);
        
       }
       }
    });


}

</script>


 <script type="text/javascript">  
 

$('.Checktype').click(function()
 {
var price = $(this).data('price');
var desinp = $('#designprice').val();
var totalp = $('#totalp').val();
 var Qty = $('#qty').val();
var Gtotal = parseInt(Qty) * (parseFloat(price) + parseFloat(desinp) + parseFloat(totalp));
var Gtotals =  parseFloat(price) + parseFloat(desinp) + parseFloat(totalp);
$('#typeprice').val(price);
 Gtotal = parseFloat(Gtotal).toFixed(2);
 <?php $Cur = Session::get('currency'); ?>
 

$('#gtotal').html('<?php echo $Cur; ?> '+Gtotal);
$('#add_total_price').val(Gtotals);
});

$('.Checkdesign').click(function()
 {
var price = $(this).data('price');
var desinp = $('#typeprice').val();
var totalp = $('#totalp').val();
 var Qty = $('#qty').val();
var Gtotal =parseInt(Qty) * (parseFloat(price) + parseFloat(desinp) + parseFloat(totalp));
var Gtotals = parseFloat(price) + parseFloat(desinp) + parseFloat(totalp);
 Gtotal = parseFloat(Gtotal).toFixed(2);
  <?php $Cur = Session::get('currency'); ?>
$('#gtotal').html('<?php echo $Cur; ?> '+Gtotal);
$('#designprice').val(price); 
$('#add_total_price').val(Gtotals);
});




 $('.selct').click(function()
 {
  var title =  $(this).data('title');
  var from =  $(this).data('from');
  var price =  $(this).data('price');  
  var img =   $(this).data('img');
  var pid =   $(this).data('id');
  var attribute =   $(this).data('attribute');
  $(".kosha-selections-area").show();  
  $(".info_"+from).html('');
  $(".info_"+from).append('<div class="selection-box"><div class="selection-img"><img src="'+img+'" alt="" /></div><div class="selection-name">'+title+'<input type="hidden" class="tprice" name="itemprice" value="'+price+'"><input type="hidden" name="product[attribute_id][]" value="'+attribute+'"><input type="hidden" name="product[pro_id][]" value="'+pid+'"></div><div class="selection-prise">SAR '+price+'</div></div>');
    var totalP = 0;
    var getP = 0;
    $( ".tprice" ).each(function() {
    getP =  $(this).val();
    totalP = parseFloat(totalP) + parseFloat(getP);
    }); 

   totalP = parseFloat(totalP).toFixed(2)
 <?php $Cur = Session::get('currency'); ?>
$("#totalPrice").html('<?php echo $Cur; ?> '+ totalP); 
  });

 

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
 
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
 $('.category_wrapper').click(function(){
var Pro_id = $(this).data('pid');

if(Pro_id !='')
{

$.ajax({
     type:"GET",
     url:"{{url('getsingleProductInfo')}}?product_id="+Pro_id,
     success:function(res)
          {             
            var Info = res.split("~~~");
            var ProName = Info[0];
            var OriginalP = Info[1];
            var Disp = Info[2];
            var pro_Img = Info[3];         
            $('#pname').html(ProName);
            <?php $Cur = Session::get('currency'); ?>
            $('#oprice').html('<?php echo $Cur; ?> '+OriginalP);
            $('#disprice').html('<?php echo $Cur; ?> '+Disp);
            $('.proimg').attr('src',pro_Img);
            $('#totalp').html('<?php echo $Cur; ?> '+Disp);
            $('.Checktype').removeAttr('checked');
            $('.Checkdesign').removeAttr('checked');
            $('#gtotal').html('<?php echo $Cur; ?> '+Disp);
            $('#add_product_id').val(Pro_id);
            $('#add_total_price').val(Disp); 
            $('#DispriceTotal').html('<?php echo $Cur; ?> '+Disp); 
            $('#totalp').val(Disp); 
            $('#typeprice,#designprice').val(0); 
            $('#qty').val(1);
                   
           }
  });


}

});
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
 
 
<script language="javascript">
$('.add').click(function () { 

 

    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 @if(request()->type!='') 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 @endif
 <script type="text/javascript">
   function isNumberKey(evt) {  

        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
       evt.preventDefault();
        } else {
            return true;
        }
    }
 
 </script>
@include('includes.navbar')
<div class="outer_wrapper">
  <div class="inner_wrap"> @include('includes.header')
  
    <!-- search-section -->
    <div class="page-left-right-wrapper main_category">
      <div class="mobile-filter-line">
        <div class="mobile-budget"><span>Budget Allocation</span></div>
        <div class="mobile-search"><span>Search</span></div>
      </div>
    
      <div class="page-right-section">
        <div class="diamond_main_wrapper"> 
        @if(Session::get('lang_file')!='en_lang')
      <img src="{{ url('') }}/themes/images/occasion_co_ordinator_AR.jpg" alt="" usemap="#homeMap" hidefocus="true">
      @else
      <img src="{{ url('') }}/themes/images/occasion_co_ordinator_EN.jpg" alt="" usemap="#homeMap" hidefocus="true">
      @endif
          <map name="homeMap" id="homeMap">

<area shape="poly" coords="467,9,406,81,318,155,316,162,282,197,650,197" href="{{ url('') }}/kosha/12" />
<area shape="poly" coords="469,198,282,197,177,301,175,308,108,370,469,370" href="{{ url('') }}/photography-studio/127" alt="" /><area shape="poly" coords="654,197,468,198,469,272,469,296,471,369,827,370" href="{{ url('') }}/electronic-invitations/16" />
<area shape="poly" coords="106,373,306,373,307,547,305,548,97,546,15,464" href="{{ url('') }}/reception-and-hospitality/14" />
<area shape="poly" coords="308,373,628,371,626,547,572,546,309,544,309,489" href="{{ url('') }}/roses/15" />         
<area shape="poly" coords="301,548,468,548,469,718,468,725,277,724,104,550" href="{{ url('') }}/special-events/17" />
           
          </map>
          
        </div>
        <center>
        </center>
        <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')
@include('includes.navbar')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
@endphp
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
@if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !='')
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
      @endif  
      <li><a href="#kosha2">{{ (Lang::has(Session::get('lang_file').'.choose_items')!= '')  ?  trans(Session::get('lang_file').'.choose_items'): trans($OUR_LANGUAGE.'.choose_items')}}</a></li>  
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
   
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>
 
 
  <!-- common_navbar -->
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?>
                <li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">@if(Session::get('lang_file')!='en_lang') {{$vendordetails->mc_name_ar}} @else {{$vendordetails->mc_name}} @endif</div>
          <div class="detail_hall_description">{{$vendordetails->address}}</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')}}</div>
          <div class="detail_about_hall">
            <div class="comment more">@if($lang != 'en_lang') {{$vendordetails->mc_discription }} @else {{$vendordetails->mc_discription}} @endif</div>
          </div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

                 @php if($vendordetails->google_map_address!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          @php }  @endphp
        </div>
      </div>

      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>



  @if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !='')

        <div class="service-video-area">
         @if($vendordetails->mc_video_description!='') <div class="service-video-cont">
          @if($lang != 'en_lang')  {{$vendordetails->mc_video_description_ar}} @else  {{$vendordetails->mc_video_description}} @endif</div>@endif
          <div class="service-video-box">
        @if($vendordetails->mc_video_url !='')    <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> @endif
          </div>
        </div>
@endif

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  @foreach($allreview as $val)
           
                   @php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
     



  <div class="service_bottom"><a name="kosha2" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                @php $i=1; @endphp
                @foreach($menu as $cats)  
                <li><a href="javascript:void(0);" data-tabid="{{$i}}" data-id="block_{{$i}}" class="maintab @if($i==1) select @endif"  > @if(Session::get('lang_file')!='en_lang') {{ $cats->attribute_title_ar }} @else {{ $cats->attribute_title }}   @endif </a></li>
               @php $i=$i+1; @endphp
               @endforeach
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
 <script type="text/javascript">
   
$('.maintab').click(function(){
var tabid = $(this).data('id');
$('.maintab').removeClass('select');
$('.service-display-section').hide();
$('#'+tabid).show();
$(this).addClass('select');
})

 </script>
 <!-- SINGLE -->
 
@php $m=1; @endphp
@foreach($menu as $menus)
<div id="block_{{$m}}" class="service-display-section" @if($m!=1) style="display:none;"  @endif> 


  <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
         

@php 
$i=1; $j=1;  
$getC = $menus->product->count(); 
if($getC >= 9)
{
$pagination = ceil($getC/9); 
$getC = 9;

}
else
{ 
$pagination =1;
}


@endphp  
@if($getC >=1)

 

<div class="diamond_main_wrapper">
    <div class="diamond_wrapper_outer">
    <div class="diamond_wrapper_main">
      <div class="diamond_wrapper_inner">
       
@foreach($menus->product as $val)
 <span id="{{$m}}">
<!-- TILL 5 RECORD -->
  @if($getC <=5) 
   @php $img = str_replace('thumb_','',$val->pro_Img);  @endphp
        <div class="row_{{$i}}of{{$getC}} rows{{$getC}}row">
         <a href="#">
            <div data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper @if($getC!=5 && $getC!=4  && $getC!=2) category_wrapper{{$i}} @endif" style="background:url({{$img or ''}});">
              <div class="category_title"><div class="category_title_inner">{{$val->pro_title}}</div><div class="clear"></div></div>
            </div>
          </a>
        </div>
<!-- TILL 6 RECORD -->
  @elseif($getC == 6)
 
          @if($i != 3 && $i != 4) 
          @php if($i==5){ $M=4; } elseif($i==6){ $M=5; }else { $M=$i; } @endphp
          <div class="row_{{$M}}of5 rows5row">
          <a href="#">
          <div data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper" style="background:url({{$val->pro_Img}});">
          <div class="category_title"><div class="category_title_inner">{{$val->pro_title}}</div></div>
          </div>
          </a>
          </div>  
          @else
          @if($i==3) <div class="row_3of5 rows5row">  @endif
          <a href="#">
          <span data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper  @if($i==3) category_wrapper2 @else category_wrapper3 @endif" style="background:url({{$val->pro_Img}});">
          <span class="category_title"><span class="category_title_inner">{{$val->pro_title}}</span><span class="clear"></span></span>
          </span>
          </a>

          @if($i==4)  <div class="clear"></div>   
         </div>   @endif  
  @endif
<!-- TILL 7 RECORD -->
  @elseif($getC == 7)

          @if($i != 3 && $i != 4 && $i != 5) 
          @php if($i==6){ $j = 4;} if($i==7){ $j = 5;}  @endphp
          <div class="row_{{$j}}of5 rows5row">
          <a href="#">
          <div data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper" style="background:url({{$val->pro_Img}});">
          <div class="category_title"><div class="category_title_inner">{{$val->pro_title}}</div></div>
          </div>
          </a>
          </div>  
          @else
          @if($i==3) <div class="row_3of5 rows5row">  @endif
          <a href="#">
          <span data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper  @if($i==3) category_wrapper4 @elseif($i==4) category_wrapper5 @else category_wrapper6 @endif" style="background:url({{$val->pro_Img}});">
          <span class="category_title"><span class="category_title_inner">{{$val->pro_title}}</span><span class="clear"></span></span>
          </span>
          </a>

          @if($i==5)  <div class="clear"></div>   
          </div>   @endif  
  @endif
 <!-- TILL 8 RECORD -->
 @elseif($getC == 8)
 
        @if($i==1 || $i==8 )
        @php if($i==1) { $k = 1;} if($i==8) { $k = 5;}   @endphp
       
          <div class="row_{{$k}}of5 rows5row">
          <a href="#">
          <div data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper category_wrapper1" style="background:url({{$val->pro_Img}});">
          <div class="category_title"><div class="category_title_inner">{{$val->pro_title}}</div></div>
          </div>
          </a>   
          </div>
          @else
          @if($i==2 ||$i==4 ||$i==6)   <div class="row_3of5 rows5row"> @endif
          @php if($i==2) { $P = 2;} if($i==3) { $P = 3;}  if($i==4) { $P = 2;}  if($i==5) { $P = 3;}  if($i==6) { $P = 7;}  if($i==7) { $P = 8;}  @endphp

          <a href="#">
          <span data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper category_wrapper{{$P}}" style="background:url({{$val->pro_Img}});">
          <span class="category_title"><span class="category_title_inner">{{$val->pro_title}}</span><span class="clear"></span></span>
          </span>
          </a>
          @if($i==3 ||$i==5 ||$i==7)  
          <div class="clear"></div>
          </div>@endif
 @endif
  <!-- TILL 9 RECORD -->
@elseif($getC == 9)

        @if($i==1 || $i==9 )
        @php if($i==1){$k=1; } if($i==9){$k=5;   } @endphp
        <div class="row_{{$k}}of5 rows5row">
        <a href="#">
        <div data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}" class="category_wrapper category_wrapper{{$i}}" style="background:url({{$val->pro_Img}});">
        <div class="category_title"><div class="category_title_inner">{{$val->pro_title}}</div></div>
        </div>
        </a>   
        </div>
        @elseif($i==2 || $i==3 )
 
        @if($i==2) <div class="row_2of5 rows5row"> @endif 
        <a href="#">
        <span data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper category_wrapper{{$i}}" style="background:url({{$val->pro_Img}});">
        <span class="category_title"><span class="category_title_inner">{{$val->pro_title}}</span><span class="clear"></span></span>
        </span>
        </a>

        @if($i==3)    <div class="clear"></div>
        </div>
        @endif 

        @elseif($i==4 || $i==5 || $i==6 )


        @if($i==4) <div class="row_3of5 rows5row"> @endif 
        <a href="#">
        <span data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}"  class="category_wrapper category_wrapper{{$i}}" style="background:url({{$val->pro_Img}});">
        <span class="category_title"><span class="category_title_inner">{{$val->pro_title}}</span><span class="clear"></span></span>
        </span>
        </a>

        @if($i==6)    <div class="clear"></div>
        </div>
        @endif 

        @elseif($i==7 || $i==8 )

        @if($i==7) <div class="row_4of5 rows5row">@endif 
        <a href="#">
        <span data-pid="{{$val->pro_id}}" data-tabid="{{$m}}" data-att_id="{{$menus->id}}" class="category_wrapper category_wrapper{{$i}}" style="background:url({{$val->pro_Img}});">
        <span class="category_title"><span class="category_title_inner">{{$val->pro_title}}</span><span class="clear"></span></span>
        </span>
        </a>
       
        @if($i==8) <div class="clear"></div>
        </div> @endif            
        @endif

@endif

   <!-- END ALL LOOP -->
@php $i= $i+1; $j=$j+1; @endphp
</span>
@endforeach



        </div>
    </div>
    </div>


  </div>
 
 <div class="diamond_shadow"><img src="{{url('/images/shadow.png')}}" alt=""></div> 
      
     {{-- <div class="pagnation-area">{{ $menus->product->links() }}   </div> --}}


    @if($pagination !=0)
    <div class="pagnation-area">
    <ul class="pagination">  
    <li class="disabled"><span>«</span></li>
     
    @for($KO=1;$KO<=$pagination;$KO++)
    <li class="@if($KO==1) active @endif paginate" data-page="{{$KO}}" data-attribute_id="{{ $menus->id }}"  data-pro_mc_id="126"  data-tabid="{{$m}}" data-att_id="{{$menus->id}}"><span>{{$KO}}</span></li>
    @endfor
    <li><a href="#?page={{$pagination}}" rel="next">»</a></li>
    </ul>
    </div>
    @endif
 
      </div>    
     

          <form name="form1" method="post" action="{{ route('specialevent-add-to-cart') }}"" enctype="multipart/form-data">
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="product_gallery"> @php    $pro_id = $menus->product[0]->pro_id; @endphp
             @include('includes/product_multiimages') </div>
           
          <div class="service-product-name marB0" id="pname_{{$m}}">{{$menus->product[0]->pro_title}}</div>
        <div class="kosha-select-description"  id="pdes_{{$m}}" >{{$menus->product[0]->pro_desc}}</div>

          <div class="service_quantity_box">


          @php


             if($menus->product[0]->pro_qty>0)
            {
                   if(Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') 
                   {
                           $cartbtn=trans(Session::get('lang_file').'.Add_to_Cart');  } else{  $cartbtn=trans($OUR_LANGUAGE.'.Add_to_Cart'); 
                    }
                      $btnst='';
                      $avcart='';
                    $avcart='block';
              }
               else
              {

                if(Lang::has(Session::get('lang_file').'.SOLD_OUT')!= ''){ $cartbtn=trans(Session::get('lang_file').'.SOLD_OUT'); }else{ $cartbtn=trans($OUR_LANGUAGE.'.SOLD_OUT');}
                $btnst='disabled="disabled"';
                $avcart='none';
              }





          $OriginalP = $menus->product[0]->pro_price; 
            $OriginalDis = $menus->product[0]->pro_discount_percentage; 
            if($OriginalDis!=''){
          $Disp = $OriginalP - ($OriginalP*$OriginalDis)/100;
        }
        else
        {
           $Disp = $OriginalP;
        }
          $Disp = number_format((float)$Disp, 2, '.', '');
    
          @endphp 


            <div class="service-radio-line"  id="cartqty" style="display: {{ $avcart  }}">
    
    
      <div id="service_quantity_box" class="service_quantity_box">
      <div class="service_qunt">@if(Lang::has(Session::get('lang_file').'.Quantity')!= '') {{ trans(Session::get('lang_file').'.Quantity')}}  @else {{ trans($OUR_LANGUAGE.'.Quantity')}} @endif </div>
        <div class="service_qunatity_row">
          
        <div data-title="Total Quality" class="td td2 quantity food-quantity">
        <div class="quantity">
    <button onclick="return pricecalculation('remove','{{$m}}');" class="sub" id="sub" type="button"></button>
    <input type="text" onkeypress="isNumberKey(event);" onkeyup="isNumberKey(event); pricecalculation('pricewithqty','{{$m}}');"  maxlength="3" min="1" value="1" id="qty_{{$m}}" name="product_qty">
    <button onclick="return pricecalculation('add','{{$m}}');" class="add" id="add" type="button"></button></div>
 
          
       </div>
      </div>
    </div>
  </div>


          </div>

          

            <div class="total_price">@if(Lang::has(Session::get('lang_file').'.Total_Price')!= '') {{ trans(Session::get('lang_file').'.Total_Price')}}  @else {{ trans($OUR_LANGUAGE.'.Total_Price')}} @endif :
              @php if($OriginalDis!=''){ @endphp
                <span class="strike">{{ currency($menus->product[0]->pro_price,'SAR',$Current_Currency) }}    </span>
              @php } @endphp
             <span id="gtotal_{{$m}}">{{ currency($Disp,'SAR',$Current_Currency) }}  </span>
          
          <input type="hidden" name="totalp" id="totalp" value="{{ currency($Disp,'SAR',$Current_Currency, $format = false) }}">

            </div>  
         
         
          <div class="btn_row">
           {{ csrf_field() }}
           <input type="hidden" name="product_id" id="add_product_id_{{$m}}" value="{{$menus->product[0]->pro_id}}">
          <input type="hidden" name="cart_type" value="occasion">
          <input type="hidden" name="total_price" id="add_total_price_{{$m}}" value="{{ currency($Disp,'SAR',$Current_Currency, $format = false) }}">
          <input type="hidden" name="language_type" value="en">    
          
          <input type="hidden" name="attribute_id" id="attr_id_{{$m}}" value="{{$menus->id}}">
          <input type="hidden" name="cart_sub_type" value="events">
 
          <input type="hidden" name="product_type" value="sepeevent">

           <div class="btn_row" id="cartbtn">  <input type="submit" value="{{ $cartbtn }}" id="sbtn" class="form-btn addto_cartbtn" {{ $btnst }}></div>
             <span id="sold" class="form-btn addto_cartbtn" style="display: none;">{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')}}</span>
          </div>
          <a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a> 
        </form>
        </div>
        @else 
        @if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif
        @endif       
      </div>

 <!-- Single end -->
@php $m=$m+1; @endphp
 @endforeach

 <!--service-display-section-->
     @include('includes.other_services')
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
@include('includes.footer')
@include('includes.popupmessage')

<script type="text/javascript">
  
function pricecalculation(str,nm)
{

 var currentquantity = document.getElementById('qty_'+nm).value;

var product_id      = document.getElementById('add_product_id_'+nm).value;

  if(str =='remove')
  {
   var Qty = parseInt($('#qty_'+nm).val())-1; 
  }
  else if(str =='pricewithqty')
  {
  var Qty = parseInt($('#qty_'+nm).val());  
  }
  else
  {
   var Qty = parseInt($('#qty_'+nm).val())+1;  
  }
 
 if(product_id)
  {
   $.ajax({
     type:"GET",
     url:"{{url('getProductQuantity')}}?product_id="+product_id+'&qty='+Qty,
    async: false,
     success:function(res)
     { 
         <?php $Cur = Session::get('currency'); ?>              
     if(res!='ok')
     {
      $('.action_popup').fadeIn(500);
       $('.overlay').fadeIn(500);
       $('#showmsg').show();
       $('#hidemsgab').hide();
       $('#showmsgab').show();
             var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_1').value = qtyupdated - 1;
        

     }
     else
     {
                if(Qty == 0){ return false;}
          var  totalp = $('#add_total_price_'+nm).val()
           var TP = parseInt(Qty)*parseFloat(totalp);
           TP = parseFloat(TP).toFixed(2);

           <?php $Cur = Session::get('currency'); ?>
          $('#gtotal_'+nm).html('<?php echo $Cur;?> '+TP);
     }
     }
   });
  }
  else
  {
 
        if(Qty == 0){ return false;}
        var  totalp = $('#add_total_price_'+nm).val()
         var TP = parseInt(Qty)*parseFloat(totalp);
         TP = parseFloat(TP).toFixed(2);

         <?php $Cur = Session::get('currency'); ?>
        $('#gtotal_'+nm).html('<?php echo $Cur;?> '+TP);



}










}

</script>


 <script type="text/javascript">  
 
 

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
 
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
 function checkgallery(str)
{
  $.ajax({
     type:"GET",
     url:"{{url('getmultipleImages')}}?product_id="+str,
     success:function(res)
     { 
      $('.product_gallery').html(res);
     }
   });
 
}

  $('body').on('click', '.category_wrapper', function (e) {
 
var Pro_id = $(this).data('pid'); 
var tabid = $(this).data('tabid');
 var attribute_id = $(this).data('att_id');
if(Pro_id !='')
{
 
$.ajax({
     type:"GET",
     url:"{{url('getsingleProductInfo')}}?product_id="+Pro_id,
     success:function(res)
          {   
            var Add_to_Cart = "@if (Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') {{  trans(Session::get('lang_file').'.Add_to_Cart') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Add_to_Cart') }} @endif";
            $('input[name="product_qty"]').val(1);
            var Info = res.split("~~~");
            var ProName = Info[0];

            var OriginalP = Info[1];
            var Disp = Info[2];
            var pro_Img = Info[3];  
            var pro_description = Info[4]; 
             var avqty = Info[5]; 
            $('#pname_'+tabid).html(ProName);
            $('#pdes_'+tabid).html(pro_description);
          <?php $Cur = Session::get('currency'); ?>

            $('#gtotal_'+tabid).html('<?php echo $Cur;?> '+Disp);         
            $('.proimg_'+tabid).attr('src',pro_Img);      
            $('#totalp').val(Disp); 
            $('#qty').val(1);
            $('#add_product_id_'+tabid).val(Pro_id); 
              checkgallery(Pro_id);
             $('#add_total_price_'+tabid).val(Disp); 
             if($.trim(attribute_id)!='')
             {
              $('#attr_id_'+tabid).val(attribute_id); 
             }


              if(avqty<1){
              $('#cartbtn').css('display','none');
              $('#cartqty').css('display','none');
              $('#sold').css('display','block');
            }else{
              $('#cartbtn').css('display','block');
              $('#cartqty').css('display','block');
              $('#sold').css('display','none');
              $("#sbtn").val(Add_to_Cart);
               $(':input[type="submit"]').prop('disabled', false);
            }
                    
           }
  });


}

});
</script>
<script>
$(document).ready(function() 
{
  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
 
 
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 @if(request()->type!='') 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 @endif


 <script type="text/javascript">
  $('.paginate').click(function(){
  var getPage = $(this).data('page');
  var tabid = $(this).data('tabid');
  var attribute_id = $(this).data('attribute_id');
  var pro_mc_id = $(this).data('pro_mc_id');
 $('.paginate').removeClass('active');

 $(this).addClass('active');
  $.ajax({
  type:"GET",
  url:"{{url('getpagination')}}?page="+getPage+"&tabid="+tabid+"&attribute_id="+attribute_id+"&pro_mc_id="+pro_mc_id,
  success:function(res)
  {           
  $('#'+tabid).html(res);       
  }
  });
  })
 
  function isNumberKey(evt) {  

        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
       evt.preventDefault();
        } else {
            return true;
        }
    }
 





</script>

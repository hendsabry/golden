@include('includes.navbar')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
@endphp
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap ">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{$vendordetails->mc_img}}" alt="logo" /></a></div>
      </div>
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
    </div>
  </div>
  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap ">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">{{ (Lang::has(Session::get('lang_file').'.About_Shop')!= '')  ?  trans(Session::get('lang_file').'.About_Shop'): trans($OUR_LANGUAGE.'.About_Shop')}}</a></li>
@if($vendordetails->mc_video_description!='' || $vendordetails->mc_video_url !='')
          <li><a href="#video">{{ (Lang::has(Session::get('lang_file').'.Video')!= '')  ?  trans(Session::get('lang_file').'.Video'): trans($OUR_LANGUAGE.'.Video')}}</a></li>
      @endif    
          <?php if(count($allreview) > 0){ ?>
          <li><a href="#our_client">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</a></li>
          <?php } ?>
   <li><a href="#kosha">{{ (Lang::has(Session::get('lang_file').'.SELECT_PACKAGE')!= '')  ?  trans(Session::get('lang_file').'.SELECT_PACKAGE'): trans($OUR_LANGUAGE.'.SELECT_PACKAGE')}}</a></li>
        </ul>
      </div>
    </div>
  </div>
 <a href="#kosha"  class="yy"></a>
<!-- @php
print "<pre>";
print_r($menu);
print "</pre>";
@endphp -->
  <!-- common_navbar -->
  <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  if(isset($getallimage) && $getallimage!='')
                  {
                    foreach($getallimage as $value){ ?>
                <li> <img src="{{str_replace('thumb_','',$value->image)}}" alt="" /> </li>
                <?php } }else{?>
                <li><img src="{{str_replace('thumb_','',$vendordetails->image)}}" alt=""/></li>
                <?php } } ?>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <?php  
                  if(isset($vendordetails->mc_id) && $vendordetails->mc_id!=''){
                  $getallimage = Helper::getallimageCatlist($vendordetails->mc_id,$vendordetails->vendor_id);
                  foreach($getallimage as $value){ ?>
                <li> <img src="{{$value->image}}" alt="" /> </li>
                <?php } } ?>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title"> {{$vendordetails->mc_name}} </div>
          <div class="detail_hall_description">{{$vendordetails->address}}</div>
          <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')}}</div>
          <div class="detail_about_hall">
            <div class="comment more"> {{$vendordetails->mc_discription }} </div>
          </div>
          <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: 
           <?php
        $getcityname = Helper::getcity($vendordetails->city_id); 
        $mc_name = 'ci_name'; 
        if(Session::get('lang_file')!='en_lang')
      {
          $mc_name = 'ci_name_ar'; 
        }
        echo $getcityname->$mc_name; 
      ?>
       <span>
         
     
            </span></div>

                  @php if($vendordetails->google_map_address!=''){  $lat=$vendordetails->latitude;   $long=$vendordetails->longitude;    @endphp
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          @php }  @endphp 

        </div>
      </div>
 
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">  
        <a name="video" class="linking">&nbsp;</a>

      

  @if(trim($vendordetails->mc_video_description)!='' || trim($vendordetails->mc_video_url) !='')

        <div class="service-video-area">
         @if($vendordetails->mc_video_description!='') <div class="service-video-cont">
          @if($lang != 'en_lang')  {{$vendordetails->mc_video_description_ar}} @else  {{$vendordetails->mc_video_description}} @endif</div>@endif
          <div class="service-video-box">
        @if($vendordetails->mc_video_url !='')    <iframe class="service-video" src="{{$vendordetails->mc_video_url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> @endif
          </div>
        </div>
@endif

        <?php if(count($allreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">{{ (Lang::has(Session::get('lang_file').'.WHAT_OUR_CLIENT')!= '')  ?  trans(Session::get('lang_file').'.WHAT_OUR_CLIENT'): trans($OUR_LANGUAGE.'.WHAT_OUR_CLIENT')}}</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  @foreach($allreview as $val)
           
                   @php $userinfo = Helper::getuserinfo($val->cus_id); @endphp
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{$userinfo->cus_pic}}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{$val->comments}}</div>
                        <div class="testim_name">{{$userinfo->cus_name}}</div>
                        <div class="testim_star">@if($val->ratings)<img src="{{ url('') }}/themes/images/star{{$val->ratings}}.png">@endif</div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
     



  <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>  
                
                <li><a href="{{ route('reception-and-hospitality-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>1]) }}" @if(request()->type!=2) class="select" @endif>{{ (Lang::has(Session::get('lang_file').'.Design_your_Package')!= '')  ?  trans(Session::get('lang_file').'.Design_your_Package'): trans($OUR_LANGUAGE.'.Design_your_Package')}} </a></li>
               
                <li><a  @if(request()->type==2) class="select" @endif href="{{ route('package-reception-and-hospitality-details',['id'=>request()->id,'sid'=>request()->sid,'vid'=>request()->vid,'type'=>2]) }}">{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}} </a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
          

     <div class="package-row">
      @php   if(count($menu)>0){ @endphp
        <div class="package-section">
          <div class="kosha-heading">{{ (Lang::has(Session::get('lang_file').'.Design_your_Package')!= '')  ?  trans(Session::get('lang_file').'.Design_your_Package'): trans($OUR_LANGUAGE.'.Design_your_Package')}}</div>
          <div class="kosha-box">

 

		@php $KL=1;$KC=1; 
    
    @endphp 
		@foreach($menu as $menus)
		@php if(count($menus->product)  < 1) { continue; } @endphp
		<div class="package-line"> 
		<div class="package-line-heading">{{$menus->attribute_title}}</div>
		@foreach($menus->product as $products)
        @php 
        
       if($products->pro_disprice < 1){ $Disprice = $products->pro_price;}else { $Disprice =$products->pro_disprice; } 
        @endphp



                @php  
 
                $Disprice = $products->pro_discount_percentage; 

                
                $originalP = $products->pro_price;
                if($Disprice==''){ $getAmount = 0; }else {$getAmount = ($originalP * $Disprice)/100;}
                

                $DiscountPricea = $originalP - $getAmount; 
                $DiscountPrice = number_format((float)$DiscountPricea, 2, '.', '');

                 if($Disprice >=1) {
                 $productrealprice=$DiscountPricea;
                  }else{
                  $productrealprice=$originalP;
                  }
                @endphp

		<div class="package-box pck">


@if($products->pro_qty>0)
		<input type="radio" data-contain="{{$KC}}" id="tools{{$KL}}" data-currency="{{ Session::get('currency') }}"  data-price="{{  currency($productrealprice, 'SAR',$Current_Currency, $format = false) }}"  class="chkslection"    data-name="{{$products->pro_title}}"  data-id="{{$products->pro_id}}" data-img="{{$products->pro_Img}}" name="re{{$KC}}" />

  @else
     
  @endif
		<label for="tools{{$KL}}">
      <div class="hospitalityimg">
      <img src='{{$products->pro_Img or ""}}' alt=""  /></div><span class="package-name">{{$products->pro_title or ''}} </span>

         @if($Disprice >=1) 
   <div class="package-price"> <span class="strike">{{ currency($products->pro_price,'SAR',$Current_Currency) }}    </span> 
    <br>
        @php $resulttotalamount=$DiscountPrice; @endphp
      {{ currency($DiscountPrice,'SAR',$Current_Currency) }}  </div> 
@else
<div class="package-price">  {{ currency($products->pro_price,'SAR',$Current_Currency) }}</div>
 @php $resulttotalamount=$products->pro_price; @endphp
@endif

      
@if($products->pro_qty>0)
		<div class="service-radio-line recp_spc">
              <div class="service_quantity_box" id="service_quantity_box">
                <div class="service_qunt">@if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY')}}  @else {{ trans($OUR_LANGUAGE.'.QUANTITY')}} @endif</div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity recp" data-title="Total Quality">
                    <div class="quantity">
                      <button type="button" id="sub" class="sub"></button>
                      <input type="number" name="itemqty" id="qty_{{ $products->pro_id }}" value="1" min="1" max="9" readonly="" onkeyup="isNumberKey(event); checkquantity('add','{{ $products->pro_id }}');" onkeydown="isNumberKey(event); checkquantity('add','{{ $products->pro_id }}');" />
                      <button type="button" id="add" class="add" onClick="return checkquantity('add','{{ $products->pro_id }}');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                    <input type="hidden" name="netamountforqty_{{ $products->pro_id }}" id="netamountforqty_{{ $products->pro_id }}" value="{{ currency($productrealprice,'SAR',$Current_Currency, $format = false) }}">
                  </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
            </div>
            @else
            <span id="sold" class="form-btn addto_cartbtn soldout" >{{ (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '')  ?  trans(Session::get('lang_file').'.SOLD_OUT'): trans($OUR_LANGUAGE.'.SOLD_OUT')}}</span>
          @endif
      <span id="grandtotal_{{ $products->pro_id }}"></span>
    </label>


        

		</div>
		@php  $KL=$KL + 1; @endphp 
		@endforeach
		@php $KC=$KC + 1;   @endphp 
		</div>
		<!-- package-line -->
		@endforeach     
         
          </div>
          <!-- kosha-box -->
        </div>
		 @php }else{ @endphp  

     <div class="kosha-area">
        <div class="kosha-tab-line">  
          <div id="kosha-tab" class="content">
{{ (Lang::has(Session::get('lang_file').'.No_product_found_in_this_package')!= '')  ?  trans(Session::get('lang_file').'.No_product_found_in_this_package'): trans($OUR_LANGUAGE.'.No_product_found_in_this_package')}}
          </div> </div> </div>
     

     @php } @endphp
<script type="text/javascript">
	
$('body').on('click','.remove', function(){
var rem = $(this).data('div');
var uncssk = $(this).data('unck');
$('.'+rem).remove(); 
$( "input[name='"+uncssk+"']" ).prop('checked',false);
var totalP = '0.00';
$(".pp").each(function() {
 totalP = parseFloat(totalP) + parseFloat($(this).val());
});
 totalP = parseFloat(totalP).toFixed(2);
var  toastalp = $('#werkerP').val();
var  qty = $('#qty').val();
if(totalP < 1){
  
  $('.alldatas').hide();
}
var OtherTotal = parseFloat(qty) * parseFloat(toastalp);

totalP = parseFloat(totalP)  +  parseFloat(OtherTotal);

<?php $hh = Session::get('currency');?>
$('#genratePrice').html('<?php echo $hh; ?> '+totalP)
$('#tp').val(totalP)

});

$('.chkslection').click(function(){
var price = $(this).data('price');
var name = $(this).data('name');
var id = $(this).data('id');
var pro_Img = $(this).data('img');
var contain = $(this).data('contain');
var currency = $(this).data('currency');

var qty=$('#qty_'+id).val();
var totalpriceaccordingqty=qty*price;
  var totaldprice=totalpriceaccordingqty.toFixed(2);

   <?php if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') { $qtytext=trans(Session::get('lang_file').'.QUANTITY');} else { $qtytext=trans($OUR_LANGUAGE.'.QUANTITY');} ?>

 $('.chk_'+contain).remove();
 $('.package-section').show();
 $('.getInfos').append('<div class="selection-box chk_'+contain+'"><div class="selection-img"><img src="'+pro_Img+'" alt="" /></div><div class="selection-name">'+name+'<small>( <?php echo $qtytext;?>-'+qty+' )</small></div><div class="selection-prise">'+currency+' '+totaldprice+'</div><input type="hidden" name="price" class="pp" value="'+totaldprice+'"><input type="hidden" name="products[]" value="'+id+'"><input type="hidden" name="qty[]" value="'+qty+'"><span class="remove" data-div="chk_'+contain+'" data-unck="re'+contain+'"><img src="{{ url('') }}/themes/images/delete.png"/></span></div>');

var totalP = '0.00';
$(".pp").each(function() {
 totalP = parseFloat(totalP) + parseFloat($(this).val());
});
 totalP = parseFloat(totalP).toFixed(2);
var  toastalp = $('#werkerP').val();
var  qty = $('#qty').val();

var OtherTotal = parseFloat(qty) * parseFloat(toastalp);

totalP = parseFloat(totalP)  +  parseFloat(OtherTotal);
$('#genratePrice').html(currency+' '+totalP)
$('#tp').val(totalP)

})

</script>



   {!! Form::open(['url' => 'recieption_addtoCart', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
   <input type="hidden" name="product_id" value="{{request()->id}}">
 <input type="hidden" name="cart_type" value="occasion">
  <input type="hidden" name="language_type" value="en">
   <input type="hidden" name="attribute_id" value="1">
  <input type="hidden" name="cart_sub_type" value="hospitality">
 
        <!-- package-section -->
        <div class="package-section alldatas" style="display: none;">
          <div class="kosha-heading">{{ (Lang::has(Session::get('lang_file').'.Your_Selections')!= '')  ?  trans(Session::get('lang_file').'.Your_Selections'): trans($OUR_LANGUAGE.'.Your_Selections')}} </div>
          <div class="kosha-box">
            <div class="selection-box-area reception">
             
				<span class="getInfos"></span>
              



            </div>
            <!-- selection-box-area -->
            <div class="checkout-form package-form recp_oty">
              <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Number_of_Staff')!= '')  ?  trans(Session::get('lang_file').'.Number_of_Staff'): trans($OUR_LANGUAGE.'.Number_of_Staff')}} </div>
                  <div class="checkout-form-bottom">  <div class="quantity">
    <button onclick="return pricecalculation('remove');" class="sub" id="sub" type="button"></button>
    <input type="number" onkeydown="isNumberKey(event); pricecalculation('pricewithqty');" onkeyup="isNumberKey(event); pricecalculation('pricewithqty');" max="9" min="1" value="1" id="qty" name="product_qty">
    <button onclick="return pricecalculation('add');" class="add" id="add" type="button"></button></div></div>
                </div>
                <!-- checkout-form-cell -->
                @php if(count($menu)>0) { @endphp
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Nationality')!= '')  ?  trans(Session::get('lang_file').'.Nationality'): trans($OUR_LANGUAGE.'.Nationality')}} </div>
                  <div class="checkout-form-bottom">
                    <select name="nationality" id="nationality" onchange="getprice(this.value)">

                <option value="">{{ (Lang::has(Session::get('lang_file').'.SELECT')!= '')  ?  trans(Session::get('lang_file').'.SELECT'): trans($OUR_LANGUAGE.'.SELECT')}} </option>
                @foreach($menu[0]->staff_nationality as $menus)
                @php $nationID = $menus->nation_id;
                $Name = Helper::getNationID($nationID);
                 @endphp
               <option value="{{$menus->nation_id  }}">{{ $Name }} </option>
                   @endforeach
                    </select>
					<span id="showerrornationality"></span>
                  </div>
                  <span id="werkerPrice"></span>
                </div>

                @php } @endphp
                <!-- checkout-form-cell -->
              </div>
              <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Date')!= '')  ?  trans(Session::get('lang_file').'.Date'): trans($OUR_LANGUAGE.'.Date')}} </div>
                  <div class="checkout-form-bottom">
                    <input class="t-box cal-t" type="text" id="bookingdate" name="bookingdate" onchange="mynewtimeslot();">
                  </div>
				  <span id="showerrorbookingdate" class="error"></span>
                </div>
                <!-- checkout-form-cell -->
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Time')!= '')  ?  trans(Session::get('lang_file').'.Time'): trans($OUR_LANGUAGE.'.Time')}} </div>
                  <div class="checkout-form-bottom">
                   
                  <input class="t-box sel-time" type="text" id="bookingtime" name="bookingtime" onkeydown="OnKeyDown(event)">
				  <span id="showerrorbookingtime" class="error"></span>
                  </div>
                </div>
                <!-- checkout-form-cell -->
              </div>
              <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')}} </div>
                  <div class="checkout-form-bottom">
                    <input class="t-box" type="text" id="location" name="location">
					<span id="showerrorlocation" class="error"></span>
                  </div>
                </div>
                
                
                <!-- checkout-form-cell -->
              </div>

            </div>
            <!-- checkout-form -->
            <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">{{ (Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')}} : <span id="genratePrice">SAR 0.00</span></div>
              <input type="hidden" name="tp" id="tp" value="0">
              <input type="hidden" name="werkerP" id="werkerP" value="0">
              <input type="hidden" name="todaydate" id="todaydate" value="{{ date('jMY') }}">
              <div class="kosha-button-area">
                <input type="submit" value="{{ (Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '')  ?  trans(Session::get('lang_file').'.Add_to_Cart'): trans($OUR_LANGUAGE.'.Add_to_Cart')}}" class="form-btn" onclick="return showValidation()"/>
              </div>
            </div>
            <!-- kosha-tolat-area -->
          </div>
          <!-- kosha-box -->
        </div>
        </form>
        <!-- package-section -->
      </div>
  

 <!--service-display-section-->
   @include('includes.other_services')
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
</div>
<div class="othrserv_overl"></div>
@include('includes.footer')
@include('includes.popupmessage')
<script src="{{url('/')}}/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
<!------------ end tabs------------------>
<!--------- date picker script-------->
 <script type="text/javascript">
function showValidation()
{
  var nationality = document.getElementById("nationality").value;
  var bookingdate = document.getElementById("bookingdate").value;
  var bookingtime = document.getElementById("bookingtime").value;
  var location = document.getElementById("location").value;
  if(nationality == '') 
  {
   document.getElementById('showerrornationality').innerHTML = '<span style="color:red;">@if (Lang::has(Session::get('lang_file').'.SELECT_NATIONALITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_NATIONALITY') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_NATIONALITY') }} @endif</span>';
   return false;
  }
  else
  {
    document.getElementById('showerrornationality').innerHTML = '';
  }
  if(bookingdate == '') 
  {
   document.getElementById('showerrorbookingdate').innerHTML = '<span style="color:red;">@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_DATE') }} @else  {{ trans($OUR_LANGUAGE.'.MER_SELECT_NATIONALITY') }} @endif</span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorbookingdate').innerHTML = '';
  }
  if(bookingtime == '') 
  {
   document.getElementById('showerrorbookingtime').innerHTML = '<span style="color:red;">@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_STUDIO_TIME')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_STUDIO_TIME') }} @else  {{ trans($OUR_LANGUAGE.'.MER_SELECT_NATIONALITY') }} @endif</span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorbookingtime').innerHTML = '';
  }
  if(location == '') 
  {
   document.getElementById('showerrorlocation').innerHTML = '<span style="color:red;">@if (Lang::has(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_YOUR_ENTER_LOCATION') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_YOUR_ENTER_LOCATION') }} @endif</span>';
   return false;
  }
  else
  {
    document.getElementById('showerrorlocation').innerHTML = '';
  }
}


        function OnKeyDown(event) { event.preventDefault(); };

            // When the document is ready
            //var jql = jQuery;
            jQuery(window).load(function () {
            jQuery('#bookingdate').datepicker({
            format: "d M yyyy",
            startDate: new Date(),
            }); 
            jQuery("#bookingdate").datepicker("setDate", new Date());
 
            var starttime=jQuery('#opening_time').val();     
            var endtime=jQuery('#closing_time').val();
            var service_duration=jQuery('#service_duration').val();
            var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A'); 





 var timeNow = new Date();

        var currhours   = timeNow.getHours();

         var currmins   = timeNow.getMinutes();

         var hours   = timeNow.getHours()+1;

        var ampm = hours >= 12 ? 'pm' : 'am';

if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;

}else{


         var newstarttime=currhours+':'+'30 '+ampm;
}






            jQuery('#bookingtime').timepicker({
            'minTime': newstarttime,
            'maxTime': '11:30 PM',
            'dynamic': true,
            'timeFormat': 'g:i A',          
            });
            });
        </script>
 <script type="text/javascript">
  function mynewtimeslot(){

jQuery('#bookingtime').timepicker('remove');

      var todaydate=jQuery('#todaydate').val();
var bookingdate=jQuery('#bookingdate').val();

var n=bookingdate.split(" ").slice(0, 3).join('');

if(todaydate==n){
 var timeNow = new Date();

        var currhours   = timeNow.getHours();

         var currmins   = timeNow.getMinutes();

         var hours   = timeNow.getHours()+1;

        var ampm = hours >= 12 ? 'pm' : 'am';

if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;

}else{


         var newstarttime=currhours+':'+'30 '+ampm;
}
}else{
       var newstarttime = '12:00 AM';
}



 

  jQuery('#bookingtime').timepicker({
  'minTime': newstarttime,
  'maxTime': '11:30 PM',
  'timeFormat': 'g:i A',
  'showDuration': false,
  });



  }
</script>

<script type="text/javascript">
  
function getprice(str)
{
if(str !='')
{
   var VendorID = '{{request()->vid}}';
$.ajax({
     type:"GET",
     url:"{{url('getcountryWrkerprice')}}?cid="+str+"&vid="+VendorID,
     success:function(res)
          {    
          var qty = $('#qty').val();  
          $('#werkerP').val(res);  
          <?php $Cur = Session::get('currency'); ?>
          @if(Session::get('lang_file')!='en_lang')
          $('#werkerPrice').html('سعر العامل: <?php echo $Cur; ?> '+res); 
          @else

          $('#werkerPrice').html('Worker Price: <?php echo $Cur; ?> '+res); 
          @endif

          var Price =  parseFloat(res)*parseFloat(qty);
          //  var TPrice = $('#tp').val();

          var TPrice = '0.00';
          $( ".getInfos" ).hasClass( "pp" )
          {
          $(".pp").each(function() {
          TPrice = parseFloat(TPrice) + parseFloat($(this).val());
          });
          TPrice = parseFloat(TPrice).toFixed(2);
          } 

          var Total = parseFloat(Price) + parseFloat(TPrice);
          Total = parseFloat(Total).toFixed(2);

          <?php $Cur = Session::get('currency'); ?>
           $('#genratePrice').html('<?php echo $Cur; ?> '+Total);

            }
  });

}
}

function pricecalculation(str)
{  
	  if(str =='remove')
	  {
		 var Qty = parseInt($('#qty').val())-1; 
	  }
	  else
	  {
		 var Qty = parseInt($('#qty').val())+1;  
	  }
	
	  if(Qty == 0)
	  { 
		return false;
	  }
	
	  var  totalp = $('#werkerP').val();
	  var  totalps = $('#tp').val();
	
	
	  var TP = (parseInt(Qty)*parseFloat(totalp)) + parseFloat(totalps);
	
	  TP = parseFloat(TP).toFixed(2);
	  <?php $Cur = Session::get('currency'); ?>
	  $('#genratePrice').html('<?php echo $Cur; ?> '+TP);
}

</script>
<script type="text/javascript">  
$('.Checktype').click(function()
 {
var price = $(this).data('price');
var desinp = $('#designprice').val();
var totalp = $('#totalp').val();
 var Qty = $('#qty').val();
var Gtotal = parseInt(Qty) * (parseFloat(price) + parseFloat(desinp) + parseFloat(totalp));
var Gtotals =  parseFloat(price) + parseFloat(desinp) + parseFloat(totalp);
$('#typeprice').val(price);
 Gtotal = parseFloat(Gtotal).toFixed(2);
 <?php $Cur = Session::get('currency'); ?>
$('#gtotal').html('<?php echo $Cur; ?> '+Gtotal);
$('#add_total_price').val(Gtotals);
});

$('.Checkdesign').click(function()
 {
var price = $(this).data('price');
var desinp = $('#typeprice').val();
var totalp = $('#totalp').val();
 var Qty = $('#qty').val();
var Gtotal =parseInt(Qty) * (parseFloat(price) + parseFloat(desinp) + parseFloat(totalp));
var Gtotals = parseFloat(price) + parseFloat(desinp) + parseFloat(totalp);
 Gtotal = parseFloat(Gtotal).toFixed(2);
 <?php $Cur = Session::get('currency'); ?>
$('#gtotal').html('<?php echo $Cur; ?> '+Gtotal);
$('#designprice').val(price); 
$('#add_total_price').val(Gtotals);
});


 

function redy(num,id)
{
$('.redymades').css('display','none');
$('#'+num).css('display','block');
$('.redycls').removeClass('select');
$('#'+id).addClass('select');
}
 
function sela(num,id,ser){ 
 $('#displayCats').html(num); 
 $(".kosha-select-line").hide();
 $(".items_"+ser).show();
 
  $(".attb").removeClass('select');
  $("#"+id).addClass('select');
}
</script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">
 $('.category_wrapper').click(function(){
var Pro_id = $(this).data('pid');

if(Pro_id !='')
{

$.ajax({
     type:"GET",
     url:"{{url('getsingleProductInfo')}}?product_id="+Pro_id,
     success:function(res)
          {             
            var Info = res.split("~~~");
            var ProName = Info[0];
            var OriginalP = Info[1];
            var Disp = Info[2];
            var pro_Img = Info[3];         
            $('#pname').html(ProName);
             <?php $Cur = Session::get('currency'); ?>
            $('#oprice').html('<?php echo $Cur; ?> '+OriginalP);
            $('#disprice').html('<?php echo $Cur; ?> '+Disp);
            $('.proimg').attr('src',pro_Img);
            $('#totalp').html('<?php echo $Cur; ?> '+Disp);
            $('.Checktype').removeAttr('checked');
            $('.Checkdesign').removeAttr('checked');
            $('#gtotal').html('<?php echo $Cur; ?> '+Disp);
            $('#add_product_id').val(Pro_id);
            $('#add_total_price').val(Disp); 
            $('#DispriceTotal').html('<?php echo $Cur; ?> '+Disp); 
            $('#totalp').val(Disp); 
            $('#typeprice,#designprice').val(0); 
            $('#qty').val(1);
                   
           }
  });


}

});
</script>
<script>
$(document).ready(function() 
{
  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "@if (Lang::has(Session::get('lang_file').'.MORE')!= '') {{  trans(Session::get('lang_file').'.MORE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MORE') }} @endif";
  var lesstext = "@if (Lang::has(Session::get('lang_file').'.LESS')!= '') {{  trans(Session::get('lang_file').'.LESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LESS') }} @endif";
  $('.more').each(function() 
  {
    var content = $(this).html();
    if(content.length > showChar) 
  {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);
      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';
      $(this).html(html);
    }
  });

  $(".morelink").click(function(){
    if($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });
});
</script>
 
 
<script language="javascript">
$('.add').click(function () {
    if ($(this).prev().val() < 99) {
        $(this).prev().val(+$(this).prev().val() + 1);
    }
});
$('.sub').click(function () {
    if ($(this).next().val() > 1) {
      if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
    }
});
</script> 

 @if(request()->type!='') 
 <script type="text/javascript">
 $(window).load(function(){
  setTimeout( function(){ 
   $('.yy').trigger('click');
  }  , 1000 );
 })   
 </script>
 @endif

 <script type="text/javascript">
   function checkquantity(act,prodid){
        var productId        = prodid;
         var currentquantity  = document.getElementById('qty_'+prodid).value;
         var insuranceamount  = 0;

         var netamountforqty  = document.getElementById('netamountforqty_'+prodid).value;
        
          <?php  if (Lang::has(Session::get('lang_file').'.Total_Price')!= '') $Total_Price=  trans(Session::get('lang_file').'.Total_Price'); else  $Total_Price= trans($OUR_LANGUAGE.'.Total_Price'); ?>
          <?php $Cur = Session::get('currency'); ?>
         var no=1;  
       
    if(act=='add')
    {
      var qty= parseInt(currentquantity)+parseInt(no);
    }
    else
    { 
      if(parseInt(currentquantity)==1)
      {
        var qty=parseInt(currentquantity)
        }
      else
      {
        var qty=parseInt(currentquantity)-parseInt(no);
      }
    }

   $.ajax({
       type:"GET",
       url:"{{url('checkcoshaquantity')}}?product_id="+productId+'&qty='+qty,
       async: false,
       success:function(res)
       {   
      //alert(res);
       if(res!='ok')
       {
         $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();
       
         var qtyupdated = parseInt(currentquantity);      
         document.getElementById('qty_'+productId).value = qtyupdated - 1;
        
       }
       else
       {
        
         var totalcalcamount=parseFloat(netamountforqty)*parseInt(qty);
         //alert(totalcalcamount);
         
         jQuery('#grandtotal_'+productId).html('<span><?php echo $Cur; ?></span> <span>'+totalcalcamount.toFixed(2)+'</span>');

         document.getElementById('qty_'+productId).value = currentquantity;
        
       }
       }
    });



   }


    

 </script>
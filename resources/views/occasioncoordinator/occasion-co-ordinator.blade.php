@include('includes.navbar')
<div class="outer_wrapper">
@include('includes.header')
  <div class="inner_wrap"> 
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="{{ url('') }}/themes/{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
      @include('includes.searchweddingandoccasions') </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper main_category">
      @include('includes.mobile-modify')
      <div class="page-right-section">
        @if(Session::has('message'))
<span class="error" align="center" style="font-size: 14px;">{{Session::get('message')}}</span>
        @endif
        <div class="diamond_main_wrapper"> @if(Session::get('lang_file')!='en_lang') <img src="{{ url('') }}/themes/images/occasion_co_ordinator_AR.jpg" alt="" usemap="#homeMap" hidefocus="true"> @else <img src="{{ url('') }}/themes/images/occasion_co_ordinator_EN.jpg" alt="" usemap="#homeMap" hidefocus="true"> @endif
          <map name="homeMap" id="homeMap">
            <area shape="poly" coords="572,112,469,12,299,178,292,188,295,189,648,190" href="{{ url('') }}/kosha-shops/11/12" />
            <area shape="poly" coords="108,372,466,374,469,471,465,547,434,552,103,550,12,465" href="{{ url('') }}/electronic-invitations/11/16" />
            <area shape="poly" coords="283,555,830,555,767,621,654,733,560,733,282,732,105,555" href="{{ url('') }}/roses/11/15" />
            <area shape="poly" coords="652,736,283,734,280,732,290,741,384,833,470,920" href="{{ url('') }}/special-events/11/17" />
            <area shape="poly" coords="649,192,288,192,108,369,109,371,830,371,826,371" href="{{ url('') }}/photography-studio/11/127" />
            <area shape="poly" coords="467,374,829,375,921,464,830,552,467,552" href="{{ url('') }}/reception-and-hospitality/11/14" />
          </map>
        </div>
        <center>
        </center>
        <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')
@include('includes.popupmessage') 
{!! $navbar !!} 
<!-- Navbar ================================================== --> 

@php $city_det = DB::table('nm_emailsetting')->first(); @endphp
 @inject('data','App\Help')
<link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />
<!-- Header End====================================================================== --> 

@if(Session::get('lang_file') =='ar_lang')
<div id="mainBody" class="egis rtl"> @else
  <div id="mainBody" class="egis"> @endif 
    
    <!-- Sidebar ================================================== --> 
    <!-- Sidebar end=============================================== -->
    <div class="register_area">
 
      <ul class="breadcrumb not_here">
        <li><a href="index">@if (Lang::has(Session::get('lang_file').'.HOME')!= '') {{  trans(Session::get('lang_file').'.HOME')}}  @else {{ trans($OUR_LANGUAGE.'.HOME')}} @endif</a> <span class="divider">/</span></li>
        <li class="active">@if (Lang::has(Session::get('lang_file').'.MERCHANT_SIGN_UP')!= '') {{  trans(Session::get('lang_file').'.MERCHANT_SIGN_UP')}}  @else {{ trans($OUR_LANGUAGE.'.MERCHANT_SIGN_UP')}} @endif</li>
      </ul>
      @if (Session::has('result'))
      <div class="alert alert-success alert-dismissable">{!! Session::get('result') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      </div>
      @endif
      <div class="register_logo"><img src="{{url('public/assets/logo/Logo_1517904811_Logo_hfgh.png')}}"></div>

     <div class="form-signin">
<div class="lang">	
<div class="lng_text"> @if(Session::get('lang_file') == 'ar_lang') اختار اللغة  @else  Select Language @endif</div>
<div class="lnag_sel">

<select name="lang"  onchange="Lang_change(this.value)">
	<option value="ar" @if(Session::get('lang_file') == 'ar_lang') selected  @endif>عربى</option>
	<option value="en" @if(Session::get('lang_file') == 'en_lang') selected  @endif>English</option>

</select>

 </div>
</div></div>
      
      <div class="our_text">
        <div class="own">@if (Lang::has(Session::get('lang_file').'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')!= '') {{  trans(Session::get('lang_file').'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')
          }}  @else {{ trans($OUR_LANGUAGE.'.CREATE_YOUR_OWN_PERSONAL_ONLINE_STORE')}} @endif </div>
        <p class="wecome_box">@if (Lang::has(Session::get('lang_file').'.WELCOME_TO_MERCHANT_SIGN_UP')!= '') {{  trans(Session::get('lang_file').'.WELCOME_TO_MERCHANT_SIGN_UP')}}  @else {{ trans($OUR_LANGUAGE.'.WELCOME_TO_MERCHANT_SIGN_UP')}} @endif</p>
		<p style="padding:10px; margin:0px; font-size:11pt;">
@if(Session::get('lang_file') == 'ar_lang')  هل لديك حساب بالفعل ؟  @else  Already have an account? @endif

       <a href="{{url('/sitemerchant')}}" style="color:#95aeb3; text-decoration:underline;">@if(Session::get('lang_file') == 'ar_lang') تسجيل الدخول  @else Sign In @endif </a></p>
      </div>
      @if ($errors->any()) <br>
      <ul class="message_box_error">
        <div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>
          ')) !!}
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true" ></button>
        </div>
      </ul>
      @endif
      
      @if (Session::has('mail_exist'))
      <div class="alert alert-warning alert-dismissable already">{!! Session::get('mail_exist') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
      </div>
      @endif
      <div class="content"> {!! Form::open(array('url'=>'merchant_signup','class'=>'testform ','id'=>'testform','enctype'=>'multipart/form-data','accept-charset' => 'UTF-8')) !!}
        <div class="personal-data signup_arabic">
          <div class="register_form">
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.FIRST_NAME')!= '') {{  trans(Session::get('lang_file').'.FIRST_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.FIRST_NAME')}} @endif</label>
                <input type="text" maxlength="100" id="first_name" name="first_name"  class="t_box" value="{!! Input::old('first_name') !!}" tabindex="1"  >
              </div>
              <div class="row_right">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.LAST_NAME')!= '') {{  trans(Session::get('lang_file').'.LAST_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.LAST_NAME')}} @endif</label>
                <input type="text" id="last_name" maxlength="100" class="t_box" name="last_name" value="{!! Input::old('last_name') !!}"  tabindex="2">
              </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.E-MAIL')!= '') {{  trans(Session::get('lang_file').'.E-MAIL')}}  @else {{ trans($OUR_LANGUAGE.'.E-MAIL')}} @endif</label>
                <input type="email" id="email_id" class="t_box " name="email_id" value="{!! Input::old('email_id') !!}"  tabindex="3" onchange="check_email();">
                <div id="email_id_error_msg"  class="error"> </div>
              </div>
              <div class="row_right">
			  
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.CONTACT_NUMBER')!= '') {{  trans(Session::get('lang_file').'.CONTACT_NUMBER')}}  @else {{ trans($OUR_LANGUAGE.'.CONTACT_NUMBER')}} @endif</label>
				<div class="country_row">
				<div class="signup_rowright">
				<!--<input type="text" id="country_code" maxlength="15" class="t_box contactnumber" name="country_code" value="{!! Input::old('country_code') !!}" onkeypress="return isNumber(event)"  data-error="Country Code">-->
        <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode" required>      
      @foreach($getCountry as $Ccode)
        <option value="{{ $Ccode->country_code}}">+{{ $Ccode->country_code}}</option>        
        @endforeach
    </select>
                <input type="text" id="phone_no" maxlength="15" class="t_box contact contactnumber" name="phone_no" value="{!! Input::old('phone_no') !!}"   tabindex="6" onkeypress="return isNumber(event)"  data-minlength="15" data-maxlength="15" data-error="Less Number">
				</div>
				<label for="country_code" class="error"></label>
				<label for="phone_no" class="error"></label>
				</div>
              </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.COUNTRY')!= '') {{  trans(Session::get('lang_file').'.COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.COUNTRY')}} @endif</label>
                <select class="t_box" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" tabindex="4" >
                  <option value=""> @if (Lang::has(Session::get('lang_file').'.SELECT_COUNTRY')!= '') {{  trans(Session::get('lang_file').'.SELECT_COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.SELECT_COUNTRY')}} @endif</option>
                  
                           @foreach($country_details as $country_fetch) 
                           @if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') 
                           
                  <?php  $co_name = 'co_name'; ?>
                  
                           @else  
                  <?php  $co_name = 'co_name_'.Session::get('lang_code');  ?>                  
                           @endif
                   
                  <option value="{{ $country_fetch->co_id }}" <?php if(Input::old('select_mer_country')==$country_fetch->co_id){ echo "selected"; }?>><?php echo $country_fetch->$co_name; ?></option>
                  
                           @endforeach
                        
                </select>
              </div>
              <div class="row_right">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.CITY')!= '') {{  trans(Session::get('lang_file').'.CITY')}}  @else {{ trans($OUR_LANGUAGE.'.CITY')}} @endif</label>
                @if(Input::old('select_mer_city'))
                <select class="t_box" name="select_mer_city" id="select_mer_city" tabindex="5" >
                  
                           @foreach($get_city as $city)
                           @if(Input::old('select_mer_country')==$city->ci_con_id)
                            @if($city->ci_name!='')
                  <option value="<?php echo $city->ci_id;?>" <?php if(Input::old('select_mer_city')==$city->ci_id){ echo "selected";} ?>><?php echo $city->ci_name; ?></option>
                   @endif
                           @endif
                           @endforeach
                        
                </select>
                @else
                <select class="t_box" name="select_mer_city" id="select_mer_city" tabindex="5" >
                  <option value="">@if (Lang::has(Session::get('lang_file').'.SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.SELECT_CITY')}}  @else {{ trans($OUR_LANGUAGE.'.SELECT_CITY')}} @endif</option>
                </select>
                @endif </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.ADDRESS1')!= '') {{  trans(Session::get('lang_file').'.ADDRESS1')}}  @else {{ trans($OUR_LANGUAGE.'.ADDRESS1')}} @endif</label>
                <input type="text" id="addreess_one" class="t_box" name="addreess_one" value="{!! Input::old('addreess_one') !!}"  tabindex="7">
              </div>
              <div class="row_right">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.ADDRESS2')!= '') {{  trans(Session::get('lang_file').'.ADDRESS2')}}  @else {{ trans($OUR_LANGUAGE.'.ADDRESS2')}} @endif</label>
                <input type="text" id="address_two" name="address_two" class="t_box" value="{!! Input::old('address_two') !!}"   tabindex="8">
              </div>
            </div>
            <div class="row_area">
              <div class="row_left">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.ZIPCODE')!= '') {{  trans(Session::get('lang_file').'.ZIPCODE')}}  @else {{ trans($OUR_LANGUAGE.'.ZIPCODE')}} @endif</label>
                <input type="text" id="zip_code" name="zip_code" maxlength="6"  class="t_box zip" value="{!! Input::old('zip_code') !!}" onkeypress="return isNumber(event)" 
                               data-minlength="6" data-maxlength="6" data-error="Less Number">
                <div class="clear"></div>
                <div id="zip_error_msg"></div>
              </div>
              <div class="row_right">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.UPLOADPIC')!= '') {{  trans(Session::get('lang_file').'.UPLOADPIC')}}  @else {{ trans($OUR_LANGUAGE.'.UPLOADPIC')}} @endif</label>
                <div class="input-file-area">
                  <label for="file">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
                    <div class="file-btn">@if (Lang::has(Session::get('lang_file').'.UPLOAD')!= '') {{  trans(Session::get('lang_file').'.UPLOAD')}}  @else {{ trans($OUR_LANGUAGE.'.UPLOAD')}} @endif</div>
                  </div>
                  </label>
                  <input class="info-file" accept="image/*" type="file" placeholder="{{ $STORE_WIDTH }} x {{ $STORE_HEIGHT }}"  id="file" name="file" value="{!! Input::old('file') !!}" required>
                </div>
              </div>
            </div>
            <div class="services_register signup_arabic">
              <div class="select_service_heading"> @if (Lang::has(Session::get('lang_file').'.SelectServices')!= '') {{  trans(Session::get('lang_file').'.SelectServices')}}  @else {{ trans($OUR_LANGUAGE.'.SelectServices')}} @endif</div>
              <div class="service-section"> @php $j=1;@endphp
                @foreach($getCatlists as $catlists)     
                @php                
                   $par_id = $catlists->mc_id;
                   if($par_id == '1'){ continue;}
                $SecondCats=Helper::getChildCats($par_id);
                @endphp
                
                @foreach($SecondCats as $Seccatlists)
                @php
                $DataId = $Seccatlists->mc_id; $i=1;
                @endphp
                <div class="select_service_section">
                  <div class="meta_area">
                    <div class="meta_block secondlabel">
                      <label class="numer_text"> @if (Config::get('app.locale') == 'ar') 
                        {{$j}} &nbsp;{{$Seccatlists->mc_name_ar}}
                        @else 
                        {{$j}} &nbsp;   {{$Seccatlists->mc_name}}
                        @endif </label>
                    </div>
                    <select id="ms_{{$i}}" class="multicheck" multiple="multiple" required name="services[]">
                 @php   $NotCat = array('87','37');  @endphp
                      @php                
                       $par_id = $Seccatlists->mc_id;
                      $ThirdCats=Helper::getChildCats($par_id);
                      @endphp

                  @if(count($ThirdCats) >=1 && !in_array($par_id, $NotCat))

                      @foreach($ThirdCats as $thicatlists)
                      @php
                        $DataIds = $thicatlists->mc_id;
                      @endphp
                      
                      <option value="{{$thicatlists->mc_id}}">   @if (Config::get('app.locale') == 'ar')  {{$thicatlists->mc_name_ar}} @else {{$thicatlists->mc_name}} @endif </option>
                       
                      @endforeach

                    @else

                    
                      <option value="{{$Seccatlists->mc_id}}">@if (Config::get('app.locale') == 'ar')  {{$Seccatlists->mc_name_ar}} @else {{$Seccatlists->mc_name}} @endif </option>
                       

                    @endif


                      
                    </select>
                  </div>
                </div>
                @php  $i=$i+1;   $j=$j+1; @endphp
                @endforeach
                
                @endforeach </div>
              <div class="row_area">
                <div class="row_left">
                  <label for="text1" class="numer_text">
@if (Lang::has(Session::get('lang_file').'.CertificateChamber')!= '') {{  trans(Session::get('lang_file').'.CertificateChamber')}}  @else {{ trans($OUR_LANGUAGE.'.CertificateChamber')}} @endif
@if(Session::get('lang_file') == 'ar_lang')

<span style="font-size: 10pt; font-weight: normal;">(ملف PDF / الصورة فقط)</span>
@else
<span style="font-size: 10pt; font-weight: normal;">(Only PDF / Image file)</span>
@endif


                  </label>
                  <div class="input-file-area">
                    <label for="file1">
                    <div class="file-btn-area">
                      <div id="file_value2" class="file-value"></div>
                      <div class="file-btn">@if (Lang::has(Session::get('lang_file').'.UPLOAD')!= '') {{  trans(Session::get('lang_file').'.UPLOAD')}}  @else {{ trans($OUR_LANGUAGE.'.UPLOAD')}} @endif</div>
                    </div>
                    </label>
                    <input class="info-file" required="true" type="file" id="file1" name="file1" >
                  </div>
                  <span class="error certifications"></span>
                </div>

                <div class="row_right">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.Shop_link_in_maroof')!= '') {{  trans(Session::get('lang_file').'.Shop_link_in_maroof')}}  @else {{ trans($OUR_LANGUAGE.'.Shop_link_in_maroof')}} @endif</label>
                <div class="input-file-area">
                 <input type="text" id="shop_link_in_maroof" name="shop_link_in_maroof" class="t_box" maxlength="250">
                </div>
              </div>

              </div>

    <div class="row_area">
 
   <div class="row_left">
                <label for="text1" class="label_text">@if (Lang::has(Session::get('lang_file').'.sales_rep_code')!= '') {{  trans(Session::get('lang_file').'.sales_rep_code')}}  @else {{ trans($OUR_LANGUAGE.'.sales_rep_code')}} @endif</label>
                <div class="input-file-area">
                 <input type="text" id="sales_rep_code" name="sales_rep_code" class="t_box" maxlength="80">
                </div>
              </div>

</div>

            </div>
          </div>
        </div>
      </div>
      {!! Form::close() !!} </div>
  </div>
</div>



<!-- Placed at the end of the document so the pages load faster ============================================= --> 
<script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script> 
<script src="<?php //echo url('');?>/themes/js/jquery.steps.js"></script> 
<script src="{{ url('') }}/themes/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="{{ url('') }}/themes/js/bootshop.js"></script> 
<!--    <script src="<?php // echo url(); ?>/themes/js/jquery.lightbox-0.5.js"></script>--> 

<script>
   $( document ).ready(function() {
    $('#zip_code').keypress(function (q){
          if(q.which!=8 && q.which!=0 && q.which!=13 && (q.which<48 || q.which>57))
    {
              originalprice.css('border', '1px solid red'); 
      $('#zip_error_msg').html('<?php if (Lang::has(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED')!= '') { echo  trans(Session::get('lang_file').'.NUMBERS_ONLY_ALLOWED');}  else { echo trans($OUR_LANGUAGE.'.NUMBERS_ONLY_ALLOWED');} ?>');
      originalprice.focus();
      return false;
          }
    else
    {     
              originalprice.css('border', ''); 
      $('#zip_error_msg').html('');         
    }
          });
   
    
    $('.close').click(function() {
      $('.alert').hide();
    });
   $('#submit').click(function() {



      var file       = $('#file');
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
          if(file.val() == "")
      {
      file.focus();
    file.css('border', '1px solid red');    
    return false;
    }     
    else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) {         
    file.focus();
    file.css('border', '1px solid red');    
    return false;
    }     
    else
    {
    file.css('border', '');         
    }
   });
   });
</script> 

<script>
   function select_city_ajax(city_id)
   {
     var passData = 'city_id='+city_id;
     //alert(passData);
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('ajax_select_city'); ?>',
          success: function(responseText){  
         // alert(responseText);
           if(responseText)
           { 
          $('#select_city').html(responseText);            
           }
        }   
      });   
   }
   
   function select_mer_city_ajax(city_id)
   {
     var passData = 'city_id='+city_id;
    // alert(passData);
       $.ajax( {
            type: 'get',
          data: passData,
          url: '<?php echo url('ajax_select_city'); ?>',
          success: function(responseText){  
         // alert(responseText);
           if(responseText)
           { 
          $('#select_mer_city').html(responseText);            
           }
        }   
      }); 
   }
</script> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"></script> 
<script src="{{ url('') }}/themes/js/simpleform.min.js"></script> 

<script type="text/javascript">
   $(".testform").simpleform({
    speed : 500,
    transition : 'fade',
    progressBar : true,
    showProgressText : true,
    validate: true
   });
   
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }
   
   /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

 

   function validateForm(formID, Obj){
   
    switch(formID){
      case 'testform' :
        Obj.validate({
          rules: {

                first_name: {
                required: true
                },
                last_name: {
                required: true
                },

                email_id: {
                required: true
                //email_id: true
                },
				
                country_code: {
                required: true
                },

                phone_no: {
                required: true
                },

                select_mer_country: {
                required: true
                },
                select_mer_city: {
                required: true,

                },       
                addreess_one: {
                required: true
                },        


                zip_code: {
                required: true
                },

                file: {
                required: true
                },       

 
                file1: {
               	 required: function(element){
            		return $("#shop_link_in_maroof").val()=="";
        			}
                },       
			 shop_link_in_maroof: {
               	 required: function(element){
            		return $("#file1").val()=="";
        			}
                },  
            
            
          },
          messages: {
            email: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')}} @endif",
              email: "@if (Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')}} @endif"
            },

        file1: {
              required: "@if (Lang::has(Session::get('lang_file').'.VALIDFILEPDF')!= '') {{  trans(Session::get('lang_file').'.VALIDFILEPDF')}}  @else {{ trans($OUR_LANGUAGE.'.VALIDFILEPDF')}} @endif",
              accept: "@if (Lang::has(Session::get('lang_file').'.VALIDFILEPDF')!= '') {{  trans(Session::get('lang_file').'.VALIDFILEPDF')}}  @else {{ trans($OUR_LANGUAGE.'.VALIDFILEPDF')}} @endif"
            },
		shop_link_in_maroof: {
              required: "@if (Lang::has(Session::get('lang_file').'.VALIDSHOPLINK')!= '') {{  trans(Session::get('lang_file').'.VALIDSHOPLINK')}}  @else {{ trans($OUR_LANGUAGE.'.VALIDSHOPLINK')}} @endif",
              accept: "@if (Lang::has(Session::get('lang_file').'.VALIDSHOPLINK')!= '') {{  trans(Session::get('lang_file').'.VALIDSHOPLINK')}}  @else {{ trans($OUR_LANGUAGE.'.VALIDSHOPLINK')}} @endif"
            },
             
            zip_code: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ZIPCODE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_ZIPCODE')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_ZIPCODE')}} @endif"
            },
             
            website: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_WEBSITE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_WEBSITE')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_WEBSITE')}} @endif"
            },
            location: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LOCATION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_LOCATION')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_LOCATION')}} @endif"
            },
            
            commission: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_COMMISSION')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_COMMISSION')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_COMMISSION')}} @endif"
            },
            file: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_YOUR_UPLOAD_FILE')}} @endif"
            },
            select_country: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')}} @endif"
            },
            select_city: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_CITY')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_CITY')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_SELECT_CITY')}} @endif"
            },
            
            email_id: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')}} @endif",
              email: "@if (Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')}} @endif"
            },
            first_name: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_FIRST_NAME')}} @endif"
            },
            select_mer_city: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CITY')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_CITY')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_CITY')}} @endif",
              
            },
            addreess_one: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')}} @endif"
            },
                   
            last_name: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_LAST_NAME')}} @endif"
            },
            select_mer_country: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')}} @endif"
            },
			country_code: {
              required: "@if (Lang::has(Session::get('lang_file').'.COUNTRY_CODE_MSG')!= '') {{  trans(Session::get('lang_file').'.COUNTRY_CODE_MSG')}}  @else {{ trans($OUR_LANGUAGE.'.COUNTRY_CODE_MSG')}} @endif"
            },
            phone_no: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_PHONE_NO')}} @endif"
            },
            address_two: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')}} @endif"
            }
            
          }
        });
      return Obj.valid();
      break;
   
      case 'testform2' :
        Obj.validate({
          rules: {
            email_id: {
              required: true,
             // email_id: true
            },
            first_name: {
              required: true
            },
            select_mer_city: {
              required: true,
              
            },
            addreess_one: {
              required: true
            },
                     
            last_name: {
              required: true
            },
            select_mer_country: {
              required: true
            },
            phone_no: {
              required: true
            },
            address_two: {
              required: true
            }
          },
          messages: {
            email_id: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_AN_EMAIL_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_AN_EMAIL_ADDRESS')}} @endif",
              email: "@if (Lang::has(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.NOT_A_VALID_EMAIL_ADDRESS')}}  @else {{ trans($OUR_LANGUAGE.'.NOT_A_VALID_EMAIL_ADDRESS')}} @endif"
            },
            first_name: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_FIRST_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_FIRST_NAME')}} @endif"
            },
            select_mer_city: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CITY')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_CITY')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_CITY')}} @endif",
              
            },
            addreess_one: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS1_FIELD')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS1_FIELD')}} @endif"
            },
            payment_account: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_PAYMENT_ACCOUNT')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_PAYMENT_ACCOUNT')}} @endif"
            },
            
            last_name: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_LAST_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_LAST_NAME')}} @endif"
            },
            select_mer_country: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')!= '') {{  trans(Session::get('lang_file').'.PLEASE_SELECT_COUNTRY')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_SELECT_COUNTRY')}} @endif"
            },
            phone_no: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_PHONE_NO')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_PHONE_NO')}} @endif"
            },
            address_two: {
              required: "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_ADDRESS2_FIELD')}}  @else {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_ADDRESS2_FIELD')}} @endif"
            }
          }
        });
      return Obj.valid();
      break;
    }
   }
</script> 

<script type="text/javascript">
   $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script> 

<script src="{{ url('')}}/themes/js/jquery.min.js"></script> 
<script src="{{ url('')}}/themes/js/multiple-select.js"></script> 
@if(Session::get('lang_file') =='ar_lang') 

<script>
    $(function() {
        $('#ms_1,#ms_2,#ms_3,#ms_4,#ms_5,#ms_6,#ms_7,#ms_8,#ms_9,#ms_10').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });

          

    });
</script>
 
@else 
<script>
    $(function() {
        $('#ms_1,#ms_2,#ms_3,#ms_4,#ms_5,#ms_6,#ms_7,#ms_8,#ms_9,#ms_10').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });

          

    });
</script> 
@endif 

 

<script>
jQuery("#file").change(function(){
   var fake = this.value.replace("C:\\fakepath\\", "");   
jQuery("#file_value1").html(fake);
});
jQuery("#file1").change(function(){
   var fake = this.value.replace("C:\\fakepath\\", "");   


 var fileExtension = ['pdf','jpeg', 'jpg', 'png', 'gif', 'bmp'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   jQuery("#file_value2").html('');         
   jQuery("#file1").val('');   
jQuery(".certifications").html('');

         }
         else
         {
         jQuery("#file_value2").html(fake); 
         }



});




</script> 
  
<script type="text/javascript">
  function Lang_change(str) 
  { 
    var language_code = str;
    var token =  @php csrf_token(); @endphp
    jQuery.ajax
    ({
      type:'GET',
            url:"<?php echo url('new_change_languages');?>",
            data:{'Language_change':language_code,'csrf-token':token},
            success:function(data)
      {
        //alert(data);
        window.location.reload();
            }
        });
  }
</script>

@if(Session::get('lang_file') =='ar_lang') 
<script>
  jQuery(window).load(function(){
jQuery("#submit-button").val('خضع');
jQuery(".bottom_sign").html('هل لديك حساب؟    <a href="sitemerchant">تسجيل الدخول   </a>');


  })
</script> 
@endif
<div class="login_bottom"></div>
</body></html>
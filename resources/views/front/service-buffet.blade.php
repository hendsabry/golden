<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">little flora</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      
      
      <!--service-display-section-->
      
     <div class="vendor_homepage">

<div class="heading">Menu</div>

<div class="vendor_navbar">
	<ul>
    	<li><a href="#" class="venor_active">Appetizers</a></li>
        <li><a href="#">Main Course</a></li>
        <li><a href="#">Dessert</a></li>
        <li><a href="#">Soft Beverges</a></li>
    </ul>
</div>

<div class="vendor_container">

<div class="left_container">
	<Div class="subheading">Hot Appetizer</Div>

	<div class="table desktp">
     	<div class="tr">
          <div class="table_heading th1">Dish Name / Container Type</div>
          <div class="table_heading th2">Total Quality</div>
          <div class="table_heading th3">&nbsp;</div>
          <div class="table_heading th4">Price</div>
          
        </div>
	</div>
 <div class="content mCustomScrollbar">
    
    <div class="table">
     
                
	 <div class="tr">  

	 	<div class="td td1" data-title="Dish Name / Container Type">
   <div class="item_title">Kababs</div>
        <div class="item_listing">
        	<a href="#" class="listing_active">A</a>
            <a href="#">B</a>
            <a href="#">C</a>
        </div>
     </div>
	    <div class="td td2 quantity" data-title="Total Quality"><span>2</span></div>
	    <div class="td td3 add_categ" data-title=""><a href="#">ADD</a></div>
        <div class="td td4 catg_price" data-title="Price">SAR 60</div>
		
	 </div>
	 
	 <div class="tr">  

	 	<div class="td td1" data-title="Dish Name / Container Type">
   <div class="item_title">Spring Roll</div>
        <div class="item_listing">
        	<a href="#" class="listing_active">A</a>
            <a href="#">B</a>
            <a href="#">C</a>
        </div>
     </div>
	    <div class="td td2 quantity" data-title="Total Quality"><span>2</span></div>
	    <div class="td td3 add_categ" data-title=""><a href="#order_summary1">ADD</a></div>
        <div class="td td4 catg_price" data-title="Price">SAR 60</div>

	 </div>
     
     <div class="tr">  

	 	<div class="td td1" data-title="Dish Name / Container Type">
   <div class="item_title">Shawarma</div>
        <div class="item_listing">
        	<a href="#" class="listing_active">A</a>
            <a href="#">B</a>
            <a href="#">C</a>
        </div>
     </div>
	    <div class="td td2 quantity" data-title="Total Quality"><span>2</span></div>
	    <div class="td td3 add_categ" data-title=""><a href="#">ADD</a></div>
        <div class="td td4 catg_price" data-title="Price">SAR 60</div>

	 </div>
     
     <div class="tr">  

	 	<div class="td td1" data-title="Dish Name / Container Type">
   <div class="item_title">Chicken Pakora</div>
        <div class="item_listing">
        	<a href="#" class="listing_active">A</a>
            <a href="#">B</a>
            <a href="#">C</a>
        </div>
     </div>
	    <div class="td td2 quantity" data-title="Total Quality"><span>2</span></div>
	    <div class="td td3 add_categ" data-title=""><a href="#">ADD</a></div>
        <div class="td td4 catg_price" data-title="Price">SAR 60</div>

	 </div>
     
     <div class="tr">  

	 	<div class="td td1" data-title="Dish Name / Container Type">
   <div class="item_title">Chicken Pakora</div>
        <div class="item_listing">
        	<a href="#" class="listing_active">A</a>
            <a href="#">B</a>
            <a href="#">C</a>
        </div>
     </div>
	    <div class="td td2 quantity" data-title="Total Quality"><span>2</span></div>
	    <div class="td td3 add_categ" data-title=""><a href="#">ADD</a></div>
        <div class="td td4 catg_price" data-title="Price">SAR 60</div>

	 </div>
     
     <div class="tr">  

	 	<div class="td td1" data-title="Dish Name / Container Type">
   <div class="item_title">Chicken Pakora</div>
        <div class="item_listing">
        	<a href="#" class="listing_active">A</a>
            <a href="#">B</a>
            <a href="#">C</a>
        </div>
     </div>
	    <div class="td td2 quantity" data-title="Total Quality"><span>2</span></div>
	    <div class="td td3 add_categ" data-title=""><a href="#">ADD</a></div>
        <div class="td td4 catg_price" data-title="Price">SAR 60</div>

	 </div>	 	
          	
	</div><!-- table -->
    
	
	
	</div>
    
    <div class="pag_next"><a href="javascript:void(0);" class="food_catg_next">Next></a></div>
    

    
</div><!-- left_container -->


<div class="right_container">
<Div class="subheading">Container List</Div>

<div class="service_inner_wrap">
	<div class="container_list_row">
    	<div class="food_container_listimg"><img src="images/food_container.jpg"></div>
        <div class="food_container_txt">
        	<span>SAR 30</span>
            <span>No. of People 20</span>
        </div>
        <div class="fodd_cont_catg">A</div>
    </div>
    
    <div class="container_list_row">
    	<div class="food_container_listimg"><img src="images/food_container.jpg"></div>
        <div class="food_container_txt">
        	<span>SAR 30</span>
            <span>No. of People 20</span>
        </div>
        <div class="fodd_cont_catg">A</div>
    </div>
    
    <div class="container_list_row">
    	<div class="food_container_listimg"><img src="images/food_container.jpg"></div>
        <div class="food_container_txt">
        	<span>SAR 30</span>
            <span>No. of People 20</span>
        </div>
        <div class="fodd_cont_catg">A</div>
    </div>

</div><!-- service_inner_wrap -->

</div><!-- right_container -->

<a name="order_summary1" class="linking">&nbsp;</a>

<div class="order_summary_bar">
<Div class="subheading">Order Summary</Div>

<div class="order_sum_list">
<div class="order_sumrow">
	<div class="order_sum_title">Kababs</div>
    <div class="order_subcol2">A x 2</div>
    <div class="order_subcol2">SAR 120</div>
</div>

<div class="order_sumrow">
	<div class="order_sum_title">Kababs</div>
    <div class="order_subcol2">A x 2</div>
    <div class="order_subcol2">SAR 120</div>
</div>

<div class="order_sumrow">
	<div class="order_sum_title">Kababs</div>
    <div class="order_subcol2">A x 2</div>
    <div class="order_subcol2">SAR 120</div>
</div>

<div class="order_sumrow">
	<div class="order_sum_title">Kababs</div>
    <div class="order_subcol2">A x 2</div>
    <div class="order_subcol2">SAR 120</div>
</div>

</div>

<div class="total_food_cost">
	<div class="total_price">Total Price: <span>SAR 660</span></div>
</div>    

<div class="btn_row"><input type="button" value="Add to Cart" class="form-btn addto_cartbtn"></div>    
    
</div> <!-- order_summary_bar -->


</div><!-- vendor_container -->

</div>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

<?php include('inc/in-head.php'); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>      
  
  <div class="inner_wrap service-wrap">        
    <div class="detail_page">
    	<a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/halls/hall1.jpg" /> </li>
                <li> <img src="images/halls/hall2.jpg" /> </li>
                <li> <img src="images/halls/hall3.jpg" /> </li>
                <li> <img src="images/halls/hall4.jpg" /> </li>
                <li> <img src="images/halls/hall5.jpg" /> </li>
                <li> <img src="images/halls/hall7.jpg" /> </li>
                <li> <img src="images/halls/hall8.jpg" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/halls/hall1.jpg" /> </li>
                <li> <img src="images/halls/hall2.jpg" /> </li>
                <li> <img src="images/halls/hall3.jpg" /> </li>
                <li> <img src="images/halls/hall4.jpg" /> </li>
                <li> <img src="images/halls/hall5.jpg" /> </li>
                <li> <img src="images/halls/hall7.jpg" /> </li>
                <li> <img src="images/halls/hall8.jpg" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">Amorino</div>
          <div class="detail_hall_description">Ar Rabwah, Riyadh 14215, Arabia Saudí</div>
          <div class="detail_hall_subtitle">About Shop</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      
      
      <div class="service-mid-wrapper">
		   <?php include('inc/service-video.php'); ?>         
           <?php include('inc/what-client-say.php'); ?>      
          </div> <!-- service-mid-wrapper -->
		  
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="service-music-acoustic-recording.php#kosha">Recording</a></li>
                <li><a href="service-music-equipment-buy.php#kosha" class="select">Equipment</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  <!-- service_bottom -->	
	  
      <div class="service-display-section">
      	<a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
        	 
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
          <div class="service-product-name">Violin</div>
          <div class="service-product-description equipment"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
		  <p><em><strong>Notes:</strong> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</em></p>
		  </div>

            <div class="equipment-redio-line">
			<div class="equipment-redio-box"><input type="radio" id="buy" name="re" /><label for="buy">Buy</label></div>
			<div class="equipment-redio-box"><input type="radio" id="rent" name="re" /><label for="rent">Rent</label></div>
			</div>
			<div class="equipment-prise">SAR 200</div>
			<div class="equipment-button"><input type="submit" class="form-btn big-btn" value="Add to Cart" /> </div>
          
        </div>
        <!-- service-display-left --> 
      </div>
      <!--service-display-section-->
      
      <div class="sticky_other_service" style="display:none">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc --> 
      
    </div>
    <!-- detail_page --> 
    
  </div>
  <!-- innher_wrap --> 
  
</div>
<!-- outer_wrapper -->

<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

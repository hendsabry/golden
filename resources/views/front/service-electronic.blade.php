<?php include('inc/in-head.php'); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  

  
  <div class="inner_wrap service-wrap">      
    <div class="detail_page">
    	<a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
               <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
               <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">electronic invitations</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      
      <div class="service-mid-wrapper">
       <?php include('inc/service-video.php'); ?>         
       <?php include('inc/what-client-say.php'); ?>      
      </div> <!-- service-mid-wrapper -->
	  
	
      <div class="service_form_wrap">
      	<div class="form_subtitle">Invitation Details</div>
        
        <div class="service_form">
        	
            <div class="checkout-form-row">
               <div class="checkout-form-cell full_width">
               <div class="checkout-form-top">Occasion Name</div> 
               <div class="checkout-form-bottom"><input type="text" class="t-box"></div> 
               </div> <!-- checkout-form-cell -->
           </div>
                      
            <div class="checkout-form-row">
               <div class="checkout-form-cell">
               <div class="checkout-form-top">Occasion Type</div> 
               <div class="checkout-form-bottom"><input type="text" class="t-box"></div> 
               </div> <!-- checkout-form-cell -->
               
               <div class="checkout-form-cell">
               <div class="checkout-form-top">Venue</div> 
               <div class="checkout-form-bottom"><input type="text" class="t-box"></div> 
               </div> <!-- checkout-form-cell -->
           </div>
           
            <div class="checkout-form-row">
               <div class="checkout-form-cell">
               <div class="checkout-form-top">Date</div> 
               <div class="checkout-form-bottom select_date"><input name="" type="text" class="t-box cal-t"></div> 
               </div> <!-- checkout-form-cell -->
               
               <div class="checkout-form-cell">
               <div class="checkout-form-top">Time</div> 
               <div class="checkout-form-bottom">
               <select class="checkout-small-box select_time">
                <option>01</option>
                <option>02</option>
                </select>
				</div> 
               </div> <!-- checkout-form-cell -->
           </div>
           
            <div class="checkout-form-row">
               <div class="checkout-form-cell full_width">
               	<div class="col2 browse_row">
                    <div class="search-box-label">Upload Your Pic</div>
                    <div class="info-box50 input-file-box">
                        <div class="input-field-name">
                            <label for="company_logo"><div class="file-btn-area"><div class="file-btn">Upload</div><div id="file_value1" class="file-value"></div></div></label>
                        </div><input id="company_logo" class="info-file" type="file"> 
                    </div>
                </div>
               </div> <!-- checkout-form-cell -->
               
               
           </div>
           
            <div class="checkout-form-row">
               <div class="checkout-form-cell full_width">
               <div class="checkout-form-top">Invitations Type</div> 
               
               <div class="checkout-form-bottom">
               		<div class="gender-radio-box"><input id="Text-Message" name="re" type="radio"> <label for="Text-Message">Text Message</label></div>
               		<div class="gender-radio-box"><input id="Email" name="re" type="radio"> <label for="Email">Email</label></div>
                    <div class="gender-radio-box"><input id="Invitations_Card" name="re" type="radio"> <label for="Invitations_Card">Invitations Card</label></div>
               </div>
               
               
               <div class="checkout-form-bottom invitaion_cardrow">
               		<div class="gender-radio-box"><div class="choose_radio"><input id="card1" name="card1" type="radio"><label for="card1"><div class="invitaion_card"><img src="images/BeautyandElegance/invitation_card.jpg"></div></label></div></div>
               		<div class="gender-radio-box"><div class="choose_radio"><input id="card2" name="card1" type="radio"><label for="card2"><div class="invitaion_card"><img src="images/BeautyandElegance/invitation_card.jpg"></div></label></div></div>
                    <div class="gender-radio-box"><div class="choose_radio"><input id="card3" name="card1" type="radio"><label for="card3"><div class="invitaion_card"><img src="images/BeautyandElegance/invitation_card.jpg"></div></label></div></div>
                    <div class="gender-radio-box"><div class="choose_radio"><input id="card4" name="card1" type="radio"><label for="card4"><div class="invitaion_card"><img src="images/BeautyandElegance/invitation_card.jpg"></div></label></div></div>
                    <div class="gender-radio-box"><div class="choose_radio"><input id="card5" name="card1" type="radio"><label for="card5"><div class="invitaion_card"><img src="images/BeautyandElegance/invitation_card.jpg"></div></label></div></div>
               </div>
               
                
               
               <div class="checkout-form-bottom"><textarea class="form_textarea"></textarea></div> 
               </div> <!-- checkout-form-cell -->
               
           </div>
           
            <div class="total_food_cost">
                <div class="total_price">Total Price: <span>SAR 500</span></div>
            </div>
			<div class="btn_row"><input type="button" value="Add to Cart" class="form-btn addto_cartbtn"></div>
	           
        </div><!-- service_form -->
        
      </div> <!-- service_form_wrap -->
      
      <!--service-display-section-->                  
      
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc --> 
      
    </div>
    <!-- detail_page --> 
    
  </div>
  <!-- innher_wrap --> 
  
</div>
<!-- outer_wrapper -->

<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

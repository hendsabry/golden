<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">DIWAN Events</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>  <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>  <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="service-kosha.php#kosha">Design Your Kosha</a></li>
                <li><a href="service-ready-made-kosha.php#kosha" class="select">Ready Made Kosha</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  <!-- service_bottom -->
      <div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
            <ul>
              <li onclick="sel(1);"><a href="#o" id="a1" class="select">Kosha 1</a></li>
              <li onclick="sel(2);"><a href="#o" id="a2">Kosha 2</a></li>
              <li onclick="sel(3);"><a href="#o" id="a3">Kosha 3</a></li>
              <li onclick="sel(4);"><a href="#o" id="a4">Kosha 4</a></li>
            </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line --> 
		   <div class="rm-kosha-outer">
        <div class="rm-kosha-area">
          <div class="kosha-tab-area" id="table1">
            <div class="rm-kosha-name">Kosha 1</div>
            <div class="kosha-box">
               <div class="rm-kosha-img"><img src="images/test-img/kosha-big1.jpg" alt="" /></div>
			   <div class="rm-kosha-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 29,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table2" style="display:none;">
            <div class="rm-kosha-name">Kosha 2</div>
            <div class="kosha-box">
               <div class="rm-kosha-img"><img src="images/test-img/kosha-big2.jpg" alt="" /></div>
			   <div class="rm-kosha-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 29,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table3" style="display:none;">
            <div class="rm-kosha-name">Kosha 3</div>
            <div class="kosha-box">
               <div class="rm-kosha-img"><img src="images/test-img/kosha-big3.jpg" alt="" /></div>
			   <div class="rm-kosha-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 29,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table4" style="display:none;">
            <div class="rm-kosha-name">Kosha 4</div>
            <div class="kosha-box">
               <div class="rm-kosha-img"><img src="images/test-img/kosha-big1.jpg" alt="" /></div>
			   <div class="rm-kosha-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 29,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
            </div> <!-- kosha-box -->
          </div>  <!-- kosha-tab-area -->
        </div> <!-- rm-kosha-area -->
          </div> <!-- rm-kosha-outer -->
      </div>  <!-- kosha-area -->
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>
<script>
function sel(num){
	for(i=1;i<=20;i++){
		if(i==num){
			jQuery('#a'+i).addClass('select');
			jQuery('#s'+i).addClass('select');
			jQuery('#table'+i).show();
		}
		else{
			jQuery('#a'+i).removeClass('select');
			jQuery('#s'+i).removeClass('select');
			jQuery('#table'+i).hide();
		}
	}
}
</script>

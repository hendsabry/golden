<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
		<div class="diamond_wrapper_main">
			<div class="diamond_wrapper_inner">
				<div class="row_1of5 rows5row">
					<a href="#1">
						<div class="category_wrapper" style="background:url(images/diamond/gallery1.jpg);">
							<div class="category_title"><div class="category_title_inner">Hall</div></div>
						</div>
					</a>
				</div>
				<div class="row_2of5 rows5row">
					<a href="#2">
						<div class="category_wrapper" style="background:url(images/diamond/gallery2.jpg);">
							<div class="category_title"><div class="category_title_inner">The Food</div><div class="clear"></div></div>
						</div>
					</a>
				</div>
				<div class="row_3of5 rows5row">
					<a href="#2">
						<div class="category_wrapper" style="background:url(images/diamond/gallery3.jpg);">
							<div class="category_title"><div class="category_title_inner">Occasion Co-ordinator</div><div class="clear"></div></div>
						</div>
					</a>
				</div>
				<div class="row_4of5 rows5row">
					<a href="#2">
						<div class="category_wrapper" style="background:url(images/diamond/gallery4.jpg);">
							<div class="category_title"><div class="category_title_inner">Beauty</div><div class="clear"></div></div>
						</div>
					</a>
				</div>
				<div class="row_5of5 rows5row">
					<a href="#2">
						<div class="category_wrapper" style="background:url(images/diamond/gallery6.jpg);">
							<div class="category_title"><div class="category_title_inner">Shopping</div><div class="clear"></div></div>
						</div>
					</a>
				</div>
				</div>
		</div>
	  </div>
  </div>
<div class="diamond_shadow"><img src="images/diamond/shadow.png" alt=""></div>
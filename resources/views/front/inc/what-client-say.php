<div class="service_list_row service_testimonial">
        	<a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">What Our Client's Says</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="images/testimonial.jpg"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                        <div class="testim_name">Sakeena</div>
                        <div class="testim_star"><img src="images/star.jpg"></div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="images/testimonial.jpg"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                        <div class="testim_name">Sakeena</div>
                        <div class="testim_star"><img src="images/star.jpg"></div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </section>
          </div>
        </div>
<footer class="footer-section">
<div class="footer-wrapper">
<div class="footer-menu"><a href="#">Home</a> <span>|</span> <a href="#">About Us</a> <span>|</span> <a href="#">Testimonials</a> <span>|</span> <a href="#">#Occasions</a> <span>|</span> <a href="#">Privacy Policy</a> <span>|</span> <a href="#">Return Policy</a> <span>|</span> <a href="#">Contact Us</a></div> <!-- footer-menu -->

<div class="footer-info-section">
<div class="footer-info-box address-box">
<div class="footer-info-heading">Address</div>
<div class="foo-address-line foo-address">Dammam, Saudi Arabia</div>
<div class="foo-address-line foo-phone">+966 50 5331 627</div>
<div class="foo-address-line foo-mail"><a href="mailto:info@goldencages.com">info@goldencages.com</a></div>
</div> <!-- footer-info-box -->

<div class="footer-info-box payment-options-box">
<div class="footer-info-heading">Payment Options</div>
<div class="payment-option">&nbsp;</div>
</div> <!-- footer-info-box -->

<div class="footer-info-box secure-shopping-box">
<div class="footer-info-heading">Secure Shopping</div>
<div class="footer-norton"><img src="images/norton.jpg" alt="" /></div>
</div> <!-- footer-info-box -->

<div class="footer-info-box follow-box">
<div class="footer-info-heading">Follow US</div>
<div class="footer-so-line">
<a href="#" target="_blank" title="Facebook" class="so-icon facebook-icon">&nbsp;</a>
<a href="#" target="_blank" title="twitter" class="so-icon twitter-icon">&nbsp;</a>
<a href="#" target="_blank" title="Google+" class="so-icon google-plus-icon">&nbsp;</a>
<a href="#" target="_blank" title="Instagram" class="so-icon instagram-icon">&nbsp;</a>
<a href="#" target="_blank" title="Pinterest" class="so-icon pinterest-icon">&nbsp;</a>
<a href="#" target="_blank" title="YouTube" class="so-icon youtube-icon">&nbsp;</a>
</div>
</div> <!-- footer-info-box -->







</div> <!-- footer-info-section -->
<div class="footer-menu mobile-footer-menu"><a href="#">Privacy Policy</a> <span>|</span> <a href="#">Return Policy</a> </div>
<div class="footer-copyright">&copy; 2018 Goldencages.com All Rights Reserved.</div>


 </div> <!-- footer-wrapper -->

</footer> <!-- footer-section -->

<!-- flex slider -->
<script type="text/javascript">
    /*jQuery(function(){
      SyntaxHighlighter.all();
    });*/
	
	/* slider thumbmail */
    jQuery(window).load(function(){
      jQuery('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 56,
        itemMargin: 10,
        asNavFor: '#slider'
      });

      jQuery('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          jQuery('body').removeClass('loading');
        }
      });
	  	  	  
	/* Testimonial */
	
	  jQuery('.flexslider1').flexslider({
		animation: "slide"
	  });

	  
    });
  </script>

<script>
jQuery(document).ready(function(){
	
/* mobile menu */	
jQuery('.heamburger').click(function(){
	jQuery('.left_nav').slideToggle(500);	
	jQuery('.overlay').css('display','block');
});
jQuery('.overlay').click(function(){
	jQuery('.left_nav').slideUp(500);	
	jQuery('.overlay').css('display','none');
});

/* mobile menu */	
jQuery('.vendor_heamburger').click(function(){
	jQuery('.vendor_navigation').slideToggle(500);	
	jQuery('.popup_overlay').css('display','block');
});
jQuery('.popup_overlay').click(function(){
	jQuery('.vendor_navigation').slideUp(500);	
	jQuery('.popup_overlay').css('display','none');
});

/* Home page search Popup Bussiness*/
jQuery('.bussiness_link').click(function(){
	jQuery('.bussiness_search_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});

jQuery('.popup_overlay').click(function(){
	jQuery('.bussiness_search_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});


/* Home page search Popup Wedding */
jQuery('.wedding_ocaasion').click(function(){
	jQuery('.wedding_search_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay').click(function(){
	jQuery('.wedding_search_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});

/* Search page */
jQuery('.mobile-search').click(function(){
	jQuery('.search-section').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .mobile-back-arrow').click(function(){
	jQuery('.search-section').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});


/* Modify Row  */
jQuery('.form-btn').click(function(){
	jQuery('.modify-search-form').fadeOut(0);
	jQuery('.after-modify-search').fadeIn(0);
});

/* Oops Popup */
jQuery('.budgetopen').click(function(){
	jQuery('.oops_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .oops_btn').click(function(){
	jQuery('.oops_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});


/* Service Lightbox */
jQuery('.serv_lightbox').click(function(){
	jQuery('.services_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.serv_pop_close, .popup_overlay').click(function(){
	jQuery('.services_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});

/* Search page */
jQuery('.mobile-budget').click(function(){
	jQuery('.customer-budget-section').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .mobile-back-arrow').click(function(){
	jQuery('.customer-budget-section').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});

/* Search page */
jQuery('.sticky_serivce').click(function(){
	jQuery('.other_serviceinc').css('right','0');
	jQuery('.othrserv_overl').fadeIn(500);
});
jQuery('.serv_delete, .othrserv_overl').click(function(){
	jQuery('.othrserv_overl').fadeOut(500);
	jQuery('.other_serviceinc').css('right','-300px');
});

/* Review Popup */
jQuery('.review_popup_link').click(function(){
	jQuery('.review_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay').click(function(){
	jQuery('.review_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});


/* Ring Review Popup
jQuery('.write_review a').click(function(){
	jQuery('.ring_review_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .close_popup_btn').click(function(){
	jQuery('.ring_review_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});
 */

/* Checkout Payment Tabs */
jQuery('.paymt_tab li a').click(function(event){
	event.preventDefault();
	jQuery(this).parent().addClass('current-payment');
	jQuery(this).parent().siblings().removeClass('current-payment');		
	var tab = jQuery(this).attr('href');
	jQuery('.payment-option-inner-box').not(tab).hide();
	jQuery(tab).show();
});


/* Common Popup */
jQuery('.open_popup').click(function(){
	jQuery('.common_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .close_popup_btn').click(function(){
	jQuery('.common_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});






});

</script>

<link rel="stylesheet" href="js/scroll/jquery.mCustomScrollbar.css">	
<script src="js/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
jQuery(document).ready(function() { 
jQuery("#content-1").mCustomScrollbar({
	scrollButtons: {
		enable: true
	}
});

jQuery("#content-5").mCustomScrollbar({
	axis:"x",
	theme:"dark-thin",
	autoExpandScrollbar:true,
	advanced:{autoExpandHorizontalScroll:true}
});

jQuery("#menu_header").mCustomScrollbar({
	axis:"x",
	theme:"dark-thin",
	autoExpandScrollbar:true,
	advanced:{autoExpandHorizontalScroll:true}
});

jQuery("#kosha-tab").mCustomScrollbar({
	axis:"x",
	theme:"dark-thin",
	autoExpandScrollbar:true,
	advanced:{autoExpandHorizontalScroll:true}
});

});
</script>

<!-- animate tabs -->
<script type="text/javascript">
jQuery(function(){
jQuery('a[href^="#"]').click(function(e){
var target = jQuery(this).attr('href');
var strip = target.slice(1);
var anchor = jQuery("a[name='" + strip +"']");
e.preventDefault();
jQuery('html, body').animate({
scrollTop: anchor.offset().top - 50
},'slow')
});
});
</script>

<script>
jQuery("#company_logo").change(function(){
	jQuery("#file_value1").html(this.value);
});

</script>
<!-- for tab -->
<script>
function sel(num){
	for(i=1;i<=20;i++){
		if(i==num){
			jQuery('#a'+i).addClass('select');
			jQuery('#s'+i).addClass('select');
			jQuery('#table'+i).show();
		}
		else{
			jQuery('#a'+i).removeClass('select');
			jQuery('#s'+i).removeClass('select');
			jQuery('#table'+i).hide();
		}
	}
}
</script>
</body>
</html>
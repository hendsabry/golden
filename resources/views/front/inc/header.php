<div class="nav_row">
	<div class="heamburger">
    	<span></span>
        <span></span>
        <span></span>
    </div><!-- heamburger -->
    	<div class="left_nav">
        	<ul>
            	<li><a href="#">About Us</a></li>
                <li><a href="#">Testimonials</a></li>
                <li><a href="#">#Occasions</a></li>
                
            </ul>
        </div>
        <div class="middle_nav"><img src="images/logo.png" /></div>
        <div class="right_nav">
        	<span class="mobile_fields">
            	<a class="mob_lang" href="#">Arabic</a>
                <a href="#"><img src="images/login.png" /></a>
            </span>
            <a href="#" class="basket_icon"><span class="count">5</span></a>
        	<ul>
            	            	            	
                <li><a href="#">Sign In / Sign Up</a></li>
				<li><a href="#">Sign Out</a></li>
                <li><a href="#">Arabic</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
			<div class="welcome_message">Welcome  <span>Ali Mohammed</span></div>
            
        </div>
    </div><!-- nav_row -->
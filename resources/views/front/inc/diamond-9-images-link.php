<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
		<div class="diamond_wrapper_main">
			<div class="diamond_wrapper_inner">
					<div class="row_1of5 rows5row">
						<a href="#common_linking">
							<span class="category_wrapper category_wrapper1" style="background:url(images/diamond/gallery1.jpg);">
								<span class="category_title"><span class="category_title_inner">Hall</span></span>
							</span>
						</a>
					</div>
					<div class="row_2of5 rows5row">
						<a href="#common_linking">
							<span class="category_wrapper category_wrapper2" style="background:url(images/diamond/gallery2.jpg);">
								<span class="category_title"><span class="category_title_inner">The Food</span><span class="clear"></span></span>
							</span>
						</a>
						<a href="#common_linking">
							<span class="category_wrapper category_wrapper3" style="background:url(images/diamond/gallery3.jpg);">
								<span class="category_title"><span class="category_title_inner">Occasion Co-ordinator</span><span class="clear"></span></span>
							</span>
						</a>
						<div class="clear"></div>
					</div>
					<div class="row_3of5 rows5row">
						<a href="#4">
							<span class="category_wrapper category_wrapper4" style="background:url(images/diamond/gallery4.jpg);">
								<span class="category_title"><span class="category_title_inner">Beauty & Elegance</span><span class="clear"></span></span>
							</span>
						</a>
						<a href="#5">
							<span class="category_wrapper category_wrapper5" style="background:url(images/diamond/gallery1.jpg);">
								<span class="category_title"><span class="category_title_inner">Reviving Concerts</span><span class="clear"></span></span>
							</span>
						</a>
						<a href="#6">
							<span class="category_wrapper category_wrapper6" style="background:url(images/diamond/gallery5.jpg);">
								<span class="category_title"><span class="category_title_inner">Clinics</span><span class="clear"></span></span>
							</span>
						</a>
						<div class="clear"></div>
					</div>
					<div class="row_4of5 rows5row">
						<a href="#7">
							<span class="category_wrapper category_wrapper7" style="background:url(images/diamond/gallery6.jpg);">
								<span class="category_title"><span class="category_title_inner">Shopping</span><span class="clear"></span></span>
							</span>
						</a>
						<a href="#8">
							<span class="category_wrapper category_wrapper8" style="background:url(images/diamond/gallery7.jpg);">
								<span class="category_title"><span class="category_title_inner">Car Rentals</span><span class="clear"></span></span>
							</span>
						</a>
						<div class="clear"></div>
					</div>
					<div class="row_5of5 rows5row">
						<a href="#9">
							<span class="category_wrapper category_wrapper9" style="background:url(images/diamond/gallery8.jpg);">
								<span class="category_title"><span class="category_title_inner">Travel Angencies</span></span>
							</span>
						</a>
					</div>
				</div>
		</div>
	  </div>
  </div>
  <div class="diamond_shadow"><img src="images/diamond/shadow.png" alt=""></div>
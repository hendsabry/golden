<div class="search-form">
<div class="search-box search-box1">
<div class="search-box-label">Type of Business</div>
<div class="search-box-field">
<select>
<option>Seminar</option>
<option>Seminar 1</option>
</select>
</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">Number of Attendees</div>
<div class="search-box-field"><input type="text" class="search-t-box" /></div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">Budget</div>
<div class="search-box-field"><input type="text" class="search-t-box" /></div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">City</div>
<div class="search-box-field">
<select>
<option>Riyadh</option>
<option>Riyadh 1</option>
</select>
</div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">Start Date</div>
<div class="search-box-field"><input type="text" class="search-t-box cal-t" /></div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">End Date</div>
<div class="search-box-field"><input type="text" class="search-t-box cal-t" /></div>
</div> <!-- search-box -->

<div class="search-btn"><input type="submit" class="form-btn" value="Search" /></div>
</div> <!-- search-form -->
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 
<title>Golden-Cages</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="css/reset.css" rel="stylesheet" />
<link href="css/common-style.css" rel="stylesheet" />
<link href="css/stylesheet.css" rel="stylesheet" />


<link href="slider/css/demo.css" rel="stylesheet" />
<link href="slider/css/flexslider.css" rel="stylesheet" />
<link href="css/user-my-account.css" rel="stylesheet" />
<link href="css/interface.css" rel="stylesheet" />
<link href="css/interface-media.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />
<!-- custome scroll 
<link href="mousewheel/jquery.mCustomScrollbar.css" rel="stylesheet" />
-->
<!--<link href="css/diamond.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />-->

<script src="js/jquery-lib.js"></script>
<script src="js/height.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/demo.js"></script>
<script src="js/froogaloop.js"></script>
<script src="js/jquery.easing.js"></script>


</head>
<body>
<div class="popup_overlay"></div>
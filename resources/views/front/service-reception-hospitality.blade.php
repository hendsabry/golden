<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
  <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">DIWAN Events</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="service-reception-hospitality.php" class="select">Design Your Package</a></li>
                <li><a href="service-reception-hospitality-packages.php">Packages</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <!-- service_bottom -->
      <div class="package-row">
        <div class="package-section">
          <div class="kosha-heading">Design Your Package</div>
          <div class="kosha-box">
            <div class="package-line">
              <div class="package-line-heading">Tools</div>
              <div class="package-box">
                <input type="radio" id="tools1" name="re" />
                <label for="tools1"><img src="images/package3.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 800</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="tools2" name="re" />
                <label for="tools2"><img src="images/package1.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 200</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="tools3" name="re" />
                <label for="tools3"><img src="images/package2.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 700</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="tools4" name="re" />
                <label for="tools4"><img src="images/package1.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 100</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="tools5" name="re" />
                <label for="tools5"><img src="images/package3.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 400</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="tools6" name="re" />
                <label for="tools6"><img src="images/package2.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 500</span></label>
              </div>
              <!-- package-box -->
            </div>
            <!-- package-line -->
            <div class="package-line">
              <div class="package-line-heading">Choose the Outfit of Staff</div>
              <div class="package-box">
                <input type="radio" id="staff1" name="re1" />
                <label for="staff1"><img src="images/staff1.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 800</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="staff2" name="re1" />
                <label for="staff2"><img src="images/staff2.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 200</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="staff3" name="re1" />
                <label for="staff3"><img src="images/staff1.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 700</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="staff4" name="re1" />
                <label for="staff4"><img src="images/staff2.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 100</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="staff5" name="re1" />
                <label for="staff5"><img src="images/staff1.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 400</span></label>
              </div>
              <!-- package-box -->
              <div class="package-box">
                <input type="radio" id="staff6" name="re1" />
                <label for="staff6"><img src="images/staff2.jpg" alt="" /><span class="package-name">Name</span><span class="package-price">SAR 500</span></label>
              </div>
              <!-- package-box -->
            </div>
            <!-- package-line -->
          </div>
          <!-- kosha-box -->
        </div>
        <!-- package-section -->
        <div class="package-section">
          <div class="kosha-heading">Your Selections</div>
          <div class="kosha-box">
            <div class="selection-box-area">
              <div class="selection-box">
                <div class="selection-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <!-- selection-img -->
                <div class="selection-name">The Ritz Carlton</div>
                <!-- selection-name -->
                <div class="selection-prise">SAR 30,000</div>
                <!-- selection-prise -->
              </div>
              <!-- selection-box -->
              <div class="selection-box">
                <div class="selection-img"><img src="images/test-img/kosha3.jpg" alt="" /></div>
                <!-- selection-img -->
                <div class="selection-name">Flashlight</div>
                <!-- selection-name -->
                <div class="selection-prise">SAR 5,000</div>
                <!-- selection-prise -->
              </div>
              <!-- selection-box -->
            </div>
            <!-- selection-box-area -->
            <div class="checkout-form package-form">
              <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">Number of Staff</div>
                  <div class="checkout-form-bottom"><span class="qty-box">10</span></div>
                </div>
                <!-- checkout-form-cell -->
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">Nationality</div>
                  <div class="checkout-form-bottom">
                    <select>
                      <option>Saudi</option>
                      <option>01</option>
                      <option>02</option>
                    </select>
                  </div>
                </div>
                <!-- checkout-form-cell -->
              </div>
              <div class="checkout-form-row">
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">Date</div>
                  <div class="checkout-form-bottom">
                    <input class="t-box cal-t" type="text">
                  </div>
                </div>
                <!-- checkout-form-cell -->
                <div class="checkout-form-cell">
                  <div class="checkout-form-top">Time</div>
                  <div class="checkout-form-bottom">
                    <select class="sel-time">
                      <option></option>
                      <option>01</option>
                      <option>02</option>
                    </select>
                  </div>
                </div>
                <!-- checkout-form-cell -->
              </div>
            </div>
            <!-- checkout-form -->
            <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 29,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div>
            <!-- kosha-tolat-area -->
          </div>
          <!-- kosha-box -->
        </div>
        <!-- package-section -->
      </div>
      <!-- package-row -->
      <div class="sticky_other_service" style="display:none">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

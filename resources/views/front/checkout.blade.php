<?php include('inc/in-head.php'); ?>


<div class="outer_wrapper">
<div class="inner_wrap">
<?php include('inc/header.php'); ?>

<div class="search-section">
 <div class="event-result-form">
<div class="search-box search-box1">
<div class="search-box-label">Type of Occasion</div>
<div class="search-noneedit">Wedding</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">Number of Guests</div>
<div class="search-noneedit">500</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">Budget</div>
<div class="search-noneedit">SAR 50,000</div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">Gender</div>
<div class="search-noneedit">Female</div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">City</div>
<div class="search-noneedit">Riyadh</div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">Booking Date</div>
<div class="search-noneedit">11/04/2018</div>
</div> <!-- search-box -->

 
</div> <!-- modify-search-form -->
 
</div> <!-- search-section -->
  


  <div class="checkout-area">
  <div class="mobile-filter-line"> 
<div class="mobile-search mobile-search1row"><span>Event Result</span></div>
</div>
   <div class="checkout-left-area">
   <div class="checkout-page-heading">Personal Info</div>
   <div class="checkout-box">
   <div class="checkout-form">
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Name</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box" /></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Email</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box" /></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Telephone Number</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box checkout-small-box" /></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Gender</div> 
   <div class="checkout-form-bottom">
   <div class="gender-radio-box"><input id="male" name="re" type="radio"> <label for="male">Male</label></div>
   <div class="gender-radio-box"><input id="female" name="re" type="radio"> <label for="female">Female</label></div>
   </div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Date of Birth</div> 
   <div class="checkout-form-bottom checkout-dob"> 
    <select>
  <option>DD</option>
  <option>01</option>
  <option>02</option>
  </select>
  <select>
  <option>MM</option>
  <option>01</option>
  <option>02</option>
  </select>
  <select>
  <option>YYYY</option>
  <option>1986</option>
  <option>1987</option>
  </select></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Payment Method</div> 
   <div class="checkout-form-bottom"><select class="checkout-small-box">
  <option>Credit Card</option>
  <option>01</option>
  <option>02</option>
  </select></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Country</div> 
   <div class="checkout-form-bottom not-edit">Saudi Arabia</div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">City</div> 
   <div class="checkout-form-bottom"><select class="checkout-small-box">
  <option>Riyadh</option>
  <option>01</option>
  <option>02</option>
  </select></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell address-cell">
   <div class="checkout-form-top">Address</div> 
   <div class="checkout-form-bottom">
   <input type="text" class="t-box" />
   <input type="text" class="t-box" /></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Pin Code</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box checkout-small-box" /></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   </div> <!-- checkout-form -->
   
   </div> <!-- checkout-box -->
   
      <div class="checkout-page-heading">Select Shipping Companies of Following Product</div>
   <div class="checkout-box">
   <div class="shipping-companies-wrapper">
   <div class="shipping-companies-heading">Fashion</div>
   <div class="shipping-companies-line">
   <div class="scl1">
   <div class="scl1-radio"><input type="radio" name="re" /> <label>&nbsp;</label></div>
   <div class="scl1-img"><img src="images/test-img/dhl2.jpg" alt="" /></div>
   <div class="scl1-name">FedEx</div>
   </div>
   <div class="scl2">SAR 50</div>
   <div class="scl3">(1-2 Days)</div>
   </div> <!-- shipping-companies-line -->
   
   <div class="shipping-companies-line">
   <div class="scl1">
   <div class="scl1-radio"><input type="radio" name="re" /> <label>&nbsp;</label></div>
   <div class="scl1-img"><img src="images/test-img/dhl1.jpg" alt="" /></div>
   <div class="scl1-name">MASH Pro</div>
   </div>
   <div class="scl2">SAR 20</div>
   <div class="scl3">(2-4 Days)</div>
   </div> <!-- shipping-companies-line -->
   
   <div class="shipping-companies-line">
   <div class="scl1">
   <div class="scl1-radio"><input type="radio" name="re" /> <label>&nbsp;</label></div>
   <div class="scl1-img"><img src="images/test-img/dhl.jpg" alt="" /></div>
   <div class="scl1-name">DHL</div>
   </div>
   <div class="scl2">SAR 20</div>
   <div class="scl3">(2-4 Days)</div>
   </div> <!-- shipping-companies-line -->  
   </div> <!-- shipping-companies-wrapper -->
   
   <div class="shipping-companies-wrapper">
   <div class="shipping-companies-heading">Shopping</div>
   <div class="shipping-companies-line">
   <div class="scl1">
   <div class="scl1-radio"><input type="radio" name="re" /> <label>&nbsp;</label></div>
   <div class="scl1-img"><img src="images/test-img/dhl2.jpg" alt="" /></div>
   <div class="scl1-name">FedEx</div>
   </div>
   <div class="scl2">SAR 50</div>
   <div class="scl3">(1-2 Days)</div>
   </div> <!-- shipping-companies-line -->
   
   <div class="shipping-companies-line">
   <div class="scl1">
   <div class="scl1-radio"><input type="radio" name="re" /> <label>&nbsp;</label></div>
   <div class="scl1-img"><img src="images/test-img/dhl1.jpg" alt="" /></div>
   <div class="scl1-name">MASH Pro</div>
   </div>
   <div class="scl2">SAR 20</div>
   <div class="scl3">(2-4 Days)</div>
   </div> <!-- shipping-companies-line -->
   
   <div class="shipping-companies-line">
   <div class="scl1">
   <div class="scl1-radio"><input type="radio" name="re" /> <label>&nbsp;</label></div>
   <div class="scl1-img"><img src="images/test-img/dhl.jpg" alt="" /></div>
   <div class="scl1-name">DHL</div>
   </div>
   <div class="scl2">SAR 20</div>
   <div class="scl3">(2-4 Days)</div>
   </div> <!-- shipping-companies-line -->  
   </div> <!-- shipping-companies-wrapper -->
   
    </div> <!-- checkout-box -->
   
   
         <div class="checkout-page-heading">Payment Option</div>
   <div class="checkout-box po-checkout">
   <div class="payment-option-area">
   <div class="payment-option-left">
   <ul class="paymt_tab">
   <li class="current-payment"><a href="#payment_1">Credit/Debit <br />Cards</a></li>
   <li><a href="#payment_2">Mobile <br />Wallets</a></li>
   <li><a href="#payment_3">Wire <br />Transfer</a></li>
   </ul>
   </div>
     <div class="payment-option-right">
	  <div class="payment-option-box">
      <div id="payment_1" class="payment-option-inner-box">
	  <div class="payment-card-line"><img src="images/card-icons.jpg" alt="" /></div>
 <div class="payment-form-line">
  <div class="payment-form-top">Card Number</div>
  <div class="payment-form-bottom"><input type="text" class="t-box" /></div>
 </div>
 <div class="payment-form-line">
  <div class="payment-form-top">Name on the Card</div>
  <div class="payment-form-bottom"><input type="text" class="t-box" /></div>
 </div>
  <div class="payment-form-line payment-expiry">
  <div class="payment-form-top">Expiry Date</div>
  <div class="payment-form-bottom">
  <select>
   <option>MM</option>
  <option>01</option>
   <option>02</option>
  </select>
   <select>
   <option>YYYY</option>
  <option>1990</option>
  <option>1991</option>
  </select>
  </div>
 </div>
  <div class="payment-form-line payment-cvv">
  <div class="payment-form-top">CVV Code</div>
  <div class="payment-form-bottom"><input type="text" class="t-box t-cvv" />
  <div class="what-cvv"><img src="images/cvv-icon.png" alt="" class="cvv-icon" /> 3 digits printed on the back of the card</div>
  
  </div>
 </div>
 
 <div class="save-card-line"><input type="checkbox" id="save-card" /><label for="save-card">Save your card details for faster checkout. CVV is not saved.</label></div>
 </div>
 
  	 <div id="payment_2" class="payment-option-inner-box">Mobile Wallets</div>
 	 <div id="payment_3" class="payment-option-inner-box">Wire Transfer</div>
	  </div>
	 
	 <div class="payment-option-prise">SAR 60,750</div>
	 <div class="payment-option-btn"><input type="submit" value="Pay Now" class="form-btn" /></div>
	 </div>
   
   
   
   </div> <!-- payment-option-area -->
   
   </div> <!-- checkout-box -->
   
   
   
    </div> <!-- checkout-left-area -->
  <div class="checkout-right-area">
   <div class="checkout-page-heading">Order Summary</div>
   
   <div class="order-summary-box">
    <div class="order-summary-heading">Cash on Delivery of following products and services</div>
	
	<div class="os-row-area">
	<div class="os-row">
	<div class="os-row-no">1.</div>
	<div class="os-cell1">Clinics</div>
	<div class="os-cell2">Kaya Skin Clinic</div>
	<div class="os-cell3">SAR 500</div>
	</div> <!-- os-row -->
	
	<div class="os-row">
	<div class="os-row-no">2.</div>
	<div class="os-cell1">Beauty</div>
	<div class="os-cell2">Manna Beauty Center</div>
	<div class="os-cell3">SAR 900</div>
	</div> <!-- os-row -->
	
	<div class="os-row">
	<div class="os-row-no">3.</div>
	<div class="os-cell1">Fashion</div>
	<div class="os-cell2">Kaya Skin Clinic</div>
	<div class="os-cell3">SAR 600</div>
	<div class="os-shipping-fess-row">
	<div class="os-cell1">Shipping Fess</div>
	<div class="os-cell2"><img src="images/test-img/dhl.jpg" alt="" /></div>
	<div class="os-cell3">SAR 60</div>
	</div> <!-- os-shipping-fess-row -->
	</div> <!-- os-row -->
	
	<div class="os-row">
	<div class="os-row-no">4.</div>
	<div class="os-cell1">Shopping</div>
	<div class="os-cell2">Zain</div>
	<div class="os-cell3">SAR 700</div>
	<div class="os-shipping-fess-row">
	<div class="os-cell1">Shipping Fess</div>
	<div class="os-cell2"><img src="images/test-img/dhl2.jpg" alt="" /></div>
	<div class="os-cell3">SAR 60</div>
	</div> <!-- os-shipping-fess-row -->
	</div> <!-- os-row -->
		
	<div class="os-row">
	<div class="os-row-no">5.</div>
	<div class="os-cell1">Car Rental</div>
	<div class="os-cell2">Hertz Car Rental</div>
	<div class="os-cell3">SAR 1,500</div>
	</div> <!-- os-row -->
	</div> <!-- os-row-area -->
	
	<div class="os-total-line">
	<div class="os-total-left">Total Price</div>
	<div class="os-total-right">SAR 4,700</div>
	</div>
	
	
   </div> <!-- order-summary-box -->
   
   
   <div class="order-summary-box pay-now-os">
    <div class="order-summary-heading">Pay Now</div>
	
	<div class="os-row-area">
	<div class="os-row">
	<div class="os-row-no">1.</div>
	<div class="os-cell1">Halls</div>
	<div class="os-cell2">Taj Halls</div>
	<div class="os-cell3">SAR 20,000</div>
	<div class="os-shipping-fess-row">
	<div class="os-down-payment">25% Down Payment of Hall</div>	 
	<div class="os-cell3">SAR 5,000</div>
	</div> <!-- os-shipping-fess-row -->
	</div> <!-- os-row -->
	
	<div class="os-row">
	<div class="os-row-no">2.</div>
	<div class="os-cell1">Food</div>
	<div class="os-cell2">Reza Food Services</div>
	<div class="os-cell3">SAR 15,000</div>
	</div> <!-- os-row -->
	
	<div class="os-row">
	<div class="os-row-no">3.</div>
	<div class="os-cell1">Concert</div>
	<div class="os-cell2">Al Music Concert</div>
	<div class="os-cell3">SAR 600</div>
	 
	</div> <!-- os-row -->
	
	<div class="os-row">
	<div class="os-row-no">4.</div>
	<div class="os-cell1">Tourisim</div>
	<div class="os-cell2">Al Shitaiwi Tours</div>
	<div class="os-cell3">SAR 700</div>
	
	</div> <!-- os-row -->
		
	  
	</div> <!-- os-row-area -->
	
	<div class="os-total-line">
	<div class="os-total-left">Total Price</div>
	<div class="os-total-right">SAR 4,700</div>
	</div>
	
	
   </div> <!-- order-summary-box -->
   
  
   </div> <!-- checkout-right-area -->
  </div> <!-- checkout-area -->




</div> <!-- outer_wrapper -->
</div>
<?php include('inc/footer.php'); ?>
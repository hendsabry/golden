<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  
  <!-- common_navbar -->
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">Karizma Gents Salon</div>
          <div class="detail_hall_description">7731 Al Ulaya, Al Olaya, 2117, Riyadh 12244, Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <!-- service-mid-wrapper -->
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom">
        <div class="service_line">
          <div class="service_tabs">
            <ul>
              <li><a href="service-trim-beard.php" class="select">Trims Beards</a></li>
              <li><a href="service-shaves.php">Shaves</a></li>
              <li><a href="service-hair-cut.php"> Hair Cut</a></li>
            </ul>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
          <div class="service-product-name">Classic Comb Over</div>
          <div class="service-beauty-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
          <div class="container-section">
            <div class="duration">Duration <span>1 Hrs</span></div>
            <div class="beauty-prise"> <span>SAR 500</span></div>
          </div>
          <!-- container-section -->
        </div>
        <div class="service-display-left">
          <div class="leftbar_title">Book an Appointment For</div>
          <div class="books_form">
            <div class="books_male_area">
              <div class="books_male_left">
                <input id="Home" name="re" type="radio">
				<label for="Home">Home</label>
              </div>
              <div class="books_male_left">
                <input id="Shop" name="re" type="radio">
                <label for="Shop">Shop</label>
              </div>
            </div>
            <div class="books_male_area">
              <div class="book_label">Date</div>
              <div class="book_input">
                <input name="" type="text" class="t-box cal-t" />
              </div>
            </div>
            <div class="books_male_area">
              <div class="book_label">Time</div>
              <div class="book_input">
                <select class="checkout-small-box">
                  <option>01</option>
                  <option>02</option>
                </select>
              </div>
            </div>
          </div>
          <div class="staff_area">
            <div class="leftbar_title">Choose Staff</div>
			<div class="choose-line-section">
			<div class="choose-line">
			<div class="choose-line-img"><img src="images/man1.jpg" alt="" /></div> 
			<div class="choose-line-cont">
			<div class="choose-line-neme">Name</div> 
			<div class="choose-line-experience"><span>Experience</span> 8 Years</div>
			<div class="choose-line-review"><img src="images/star.jpg" alt="" /> <span>45 Review</span></div>
			<div class="choose-line-radio"><input id="name" name="re" type="radio">
                  <label for="name">&nbsp;</label></div>
			</div> 
			</div> <!-- choose-line -->
			
			<div class="choose-line">
			<div class="choose-line-img"><img src="images/man2.jpg" alt="" /></div> 
			<div class="choose-line-cont">
			<div class="choose-line-neme">Name</div> 
			<div class="choose-line-experience"><span>Experience</span> 8 Years</div>
			<div class="choose-line-review"><img src="images/star.jpg" alt="" /> <span>45 Review</span></div>
			<div class="choose-line-radio"><input id="name" name="re" type="radio">
                  <label for="name">&nbsp;</label></div>
			</div> 
			</div> <!-- choose-line -->
			
			<div class="choose-line">
			<div class="choose-line-img"><img src="images/man1.jpg" alt="" /></div> 
			<div class="choose-line-cont">
			<div class="choose-line-neme">Name</div> 
			<div class="choose-line-experience"><span>Experience</span> 8 Years</div>
			<div class="choose-line-review"><img src="images/star.jpg" alt="" /> <span>45 Review</span></div>
			<div class="choose-line-radio"><input id="name" name="re" type="radio">
                  <label for="name">&nbsp;</label></div>
			</div> 
			</div> <!-- choose-line -->
			
			</div> <!-- choose-line-section -->
			
             
          </div>
          <div class="total_food_cost">
            <div class="total_price">Total Price: <span>SAR 660</span></div>
          </div>
          <div class="btn_row">
            <input type="button" value="Book Now" class="form-btn addto_cartbtn">
          </div>
          <!-- container-section -->
        </div>
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

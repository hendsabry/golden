<?php include('inc/in-head.php'); ?>


<div class="outer_wrapper">
<div class="inner_wrap">
<?php include('inc/header.php'); ?>

<div class="search-section">
 
 <div class="after-modify-search" style="display:none">  
 <?php include('inc/search.php'); ?></div>
 
 
 
 
 
 <div class="modify-search-form">
<div class="search-box search-box1">
<div class="search-box-label">Type of Business</div>
<div class="search-noneedit">Seminar</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">Number of Attendees</div>
<div class="search-noneedit">500</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">Budget</div>
<div class="search-noneedit">SAR 50,000</div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">City</div>
<div class="search-noneedit">Riyadh</div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">Start Date</div>
<div class="search-noneedit">10/04/2018 </div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">End Date</div>
<div class="search-noneedit">11/04/2018 </div>
</div> <!-- search-box -->

<div class="search-btn"><a href="javascript:void(0)" class="form-btn" >Modify Search</a></div>
</div> <!-- modify-search-form -->
</div> <!-- search-section -->
<div class="page-left-right-wrapper">
<div class="mobile-filter-line">
<div class="mobile-budget"><span>Services</span></div>
<div class="mobile-search"><span>Modify Search</span></div>
</div>

 <?php include('inc/customer-budget-section.php'); ?>
 

<div class="page-right-section">

<div class="budget-menu-outer">
<div class="budget-menu">
<div class="budget-menu-box"><input type="checkbox" id="withinbudget" /> <label for="withinbudget">Within Budget</label></div>
<div class="budget-menu-box"><input type="checkbox" id="abovebudget" /> <label for="abovebudget">Above Budget</label></div>
<div class="budget-menu-box"><input type="checkbox" id="offers" /> <label for="offers">Offers</label></div>
</div>
</div> <!-- budget-menu-outer -->

   <div class="budget-product-area"> 
<div class="form_title">Hotel Halls</div>
 
 <div class="budget-product-box-wrapper">	 
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</div> <!-- budget-product-box -->	 	 	
				
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</div> <!-- budget-product-box -->
         	 
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</div> <!-- budget-product-box -->		 	
				
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</div> <!-- budget-product-box -->		  
         	 
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</div> <!-- budget-product-box -->	 	
				
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</div> <!-- budget-product-box -->
         	 
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</div> <!-- budget-product-box -->		 	
				
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</div> <!-- budget-product-box -->
          	 
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</div> <!-- budget-product-box -->	 	
				
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</div> <!-- budget-product-box -->
         	 
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</div> <!-- budget-product-box -->		 	
				
<div class="budget-product-box">
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall-small1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</div> <!-- budget-product-box -->   
</div> <!-- budget-product-box-wrapper -->      
	  
	  
</div> <!-- budget-product-area -->



</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
<?php include('inc/footer.php'); ?>
  
  

<?php include('inc/in-head.php'); ?>


<div class="outer_wrapper">
<div class="inner_wrap">
<?php include('inc/header.php'); ?>

 <!-- search-section -->


 



<div class="page-left-right-wrapper">
@include('includes.left_menu')
<div class="myaccount_right">
<h1 class="dashborad_heading">Reviews</h1>

<div class="field_group top_spacing_margin_occas">


<div class="main_user">
<div class="types_ocss">
<div class="types_ocs_left">

<div class="occs">
<div class="occus_text">Type of Occasion: </div>
<div class="wed_text">Wedding</div>

</div>

</div>
<div class="types_ocs_right">
<div class="occs">
<div class="occus_text">Occasion Date:</div>
<div class="wed_text">Mar 13, 2018</div>

</div>




</div>

</div>
<div class="list_of_service">


<div class="main_box_table_area">
<div class="myaccount-table mts">
        <div class="mytr">
          <div class="mytable_heading grey">Sl. No.</div>
          <div class="mytable_heading grey">Services</div>
          <div class="mytable_heading grey">Service Provider</div>
		  <div class="mytable_heading order_id grey">&nbsp;</div>		 
        </div>
        	 
	 	 
	 <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">1</div>
         <div class="mytd mies " data-title="Services"><a href="#">Halls</a></div>
         <div class="mytd mies " data-title="Service Provider">Taj Halls</div>
         <div class="mytd mies order_id review_popup_link"><a href="javascript:void(0)">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">2</div>
         <div class="mytd mies " data-title="Services"><a href="#">Food</a></div>
         <div class="mytd mies " data-title="Service Provider">Reza Food Services</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">3</div>
         <div class="mytd mies " data-title="Services"><a href="#">Concert</a></div>
         <div class="mytd mies " data-title="Service Provider">Al Music Concert</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">4</div>
         <div class="mytd mies " data-title="Services"><a href="#">Clinics</a></div>
         <div class="mytd mies " data-title="Service Provider">Kaya Skin Clinic</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">2</div>
         <div class="mytd mies " data-title="Services"><a href="#">Beauty</a></div>
         <div class="mytd mies " data-title="Service Provider">Manna Beauty Center</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">6</div>
         <div class="mytd mies " data-title="Services"><a href="#">Fashion</a></div>
         <div class="mytd mies " data-title="Service Provider">AlYasra Fashi</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">7</div>
         <div class="mytd mies " data-title="Services"><a href="#">Shopping</a></div>
         <div class="mytd mies " data-title="Service Provider">Zain</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">8</div>
         <div class="mytd mies " data-title="Services"><a href="#">Car Rental</a></div>
         <div class="mytd mies " data-title="Service Provider">Hertz Car Rental</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
     
     <div class="mytr mybacg">
         <div class="mytd mies " data-title="Sl. No.">9</div>
         <div class="mytd mies " data-title="Services"><a href="#">Tourisim</a></div>
         <div class="mytd mies " data-title="Service Provider">Al Shitaiwi Tours</div>
         <div class="mytd mies order_id" data-title="&nbsp;"><a href="#">Review</a></div>
	 </div>
	 	 
	
	</div>


<div class="review_popup">
	
    <div class="review_icon"><img src="images/review-icon.png"></div>
    
    <div class="review_commentbox">
    	<div class="review_label">Write your review</div>
        <div class="review_comment_row">
        	<textarea></textarea>
            <input type="submit" class="form-btn" value="SUBMIT">
        </div>
    </div>

</div><!-- review_popup -->

</div>


</div>


</div>

</div>


</div>

 <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->





</div> <!-- outer_wrapper -->
</div>
<?php include('inc/footer.php'); ?>
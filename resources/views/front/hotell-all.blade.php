<?php include('inc/in-head.php'); ?>


<div class="outer_wrapper">
<div class="inner_wrap">
<?php include('inc/header.php'); ?>

<div class="search-section">
 
 <div class="after-modify-search" style="display:none">  
 <?php include('inc/search.php'); ?></div>
 
 
 
 
 
 <div class="modify-search-form">
<div class="search-box search-box1">
<div class="search-box-label">Type of Business</div>
<div class="search-noneedit">Seminar</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">Number of Attendees</div>
<div class="search-noneedit">500</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">Budget</div>
<div class="search-noneedit">SAR 50,000</div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">City</div>
<div class="search-noneedit">Riyadh</div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">Start Date</div>
<div class="search-noneedit">10/04/2018 </div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">End Date</div>
<div class="search-noneedit">11/04/2018 </div>
</div> <!-- search-box -->

<div class="search-btn"><a href="javascript:void(0)" class="form-btn" >Modify Search</a></div>
</div> <!-- modify-search-form -->
</div> <!-- search-section -->
<div class="page-left-right-wrapper">
<div class="mobile-filter-line">
<div class="mobile-budget"><span>Services</span></div>
<div class="mobile-search"><span>Modify Search</span></div>
</div>

 <?php include('inc/customer-budget-section.php'); ?>
 

<div class="page-right-section">

<div class="budget-menu-outer">
<div class="budget-menu">
<div class="budget-menu-box"><input type="checkbox" id="withinbudget" /> <label for="withinbudget">Within Budget</label></div>
<div class="budget-menu-box"><input type="checkbox" id="abovebudget" /> <label for="abovebudget">Above Budget</label></div>
<div class="budget-menu-box"><input type="checkbox" id="offers" /> <label for="offers">Offers</label></div>
</div>
</div> <!-- budget-menu-outer -->

   <div class="budget-carousel-area"> 
<div class="form_title">Hotel Halls</div>
<div class="carousel-row">
 		<div class="carousel-heading">Within Budget</div>
		<div class="clear"></div>
        <div class="flexslider carousel">

          <ul class="slides">
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>		  
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>
								<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>

<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>		  
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
          </ul>
        </div>
		</div> <!-- carousel-row -->
<div class="carousel-row">
<div class="carousel-heading">Above Budget</div>
	<div class="clear"></div>		
		<div class="flexslider carousel">
          <ul class="slides">
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>		  
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
				<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>

<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>		  
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
          </ul>
        </div>
	</div>	<!-- carousel-row -->
<div class="carousel-row">
<div class="carousel-heading">Offers</div>
	<div class="clear"></div>		
		<div class="flexslider carousel">
          <ul class="slides">
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>		  
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>

<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>		  
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
         	 
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall2.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Radisson</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span>
		   </span>
  	    		</li>		 	
				
<li>
		   <span class="carousel-product-box">
		   <span class="carousel-product-img"><a href="#"><img src="images/halls/hall1.jpg" /></a></span>
		   <span class="carousel-product-cont">
		   <span class="carousel-product-name"><a href="#">Taj</a></span>
		   <span class="carousel-product-prise">SAR 20,000</span>
		   <span class="carousel-product-view"><a href="#">View Details</a></span>
		   </span> </span>
  	    		</li>
          </ul>
        </div>
	</div>	<!-- carousel-row -->		
      </div> <!-- budget-carousel-area -->


</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
<?php include('inc/footer.php'); ?>
  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

<?php include('inc/in-head.php'); ?>


<div class="outer_wrapper">
<div class="inner_wrap">
<?php include('inc/header.php'); ?>

<div class="search-section">
 
 <div class="after-modify-search" style="display:none">  
 <?php include('inc/search.php'); ?></div>
 
 
 
 
 
 <div class="modify-search-form">
<div class="search-box search-box1">
<div class="search-box-label">Type of Business</div>
<div class="search-noneedit">Seminar</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">Number of Attendees</div>
<div class="search-noneedit">500</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">Budget</div>
<div class="search-noneedit">SAR 50,000</div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">City</div>
<div class="search-noneedit">Riyadh</div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">Start Date</div>
<div class="search-noneedit">10/04/2018 </div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">End Date</div>
<div class="search-noneedit">11/04/2018 </div>
</div> <!-- search-box -->

<div class="search-btn"><a href="javascript:void(0)" class="form-btn" >Modify Search</a></div>
</div> <!-- modify-search-form -->
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
</div> <!-- search-section -->
<div class="page-left-right-wrapper">
<div class="mobile-filter-line">
<div class="mobile-budget"><span>Services</span></div>
<div class="mobile-search"><span>Modify Search</span></div>
</div>

 <?php include('inc/customer-budget-section.php'); ?>
 

<div class="page-right-section">

<div class="diamond-area">

   
<?php include('inc/diamond-7-images.php'); ?>  
 
</div> <!-- diamond-area -->
</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>


<div class="oops_popup">
	<div class="oops_title">Oops!</div>
    <div class="ooops_desciption">You have exceeded your budget! Happens. 
Simply revisit your plan or increase your budget. </div>
	<a href="javascript:void(0)" class="oops_btn">OK</a>
</div> <!-- oops_popup -->

<div class="services_popup">
<div class="serv_popup_row">
	<div class="service_popup_title">Taj Hall 1</div>
    <a href="javascript:void(0)" class="serv_pop_close">X</a>
</div><!-- serv_popup_row -->

<div class="serv_popupimg"><img src="images/business.png"></div>

<div class="serv_popup_row">
	<div class="serv_price">SAR 20,000</div>
    <a href="javascript:void(0)" class="serv_detail_link">View Details</a>
</div><!-- servicepopup_toprow -->

</div> <!-- services_popup -->


<?php include('inc/footer.php'); ?>
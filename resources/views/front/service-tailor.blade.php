<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/halls/hall1.jpg" /> </li>
                <li> <img src="images/halls/hall2.jpg" /> </li>
                <li> <img src="images/halls/hall3.jpg" /> </li>
                <li> <img src="images/halls/hall4.jpg" /> </li>
                <li> <img src="images/halls/hall5.jpg" /> </li>
                <li> <img src="images/halls/hall7.jpg" /> </li>
                <li> <img src="images/halls/hall8.jpg" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/halls/hall1.jpg" /> </li>
                <li> <img src="images/halls/hall2.jpg" /> </li>
                <li> <img src="images/halls/hall3.jpg" /> </li>
                <li> <img src="images/halls/hall4.jpg" /> </li>
                <li> <img src="images/halls/hall5.jpg" /> </li>
                <li> <img src="images/halls/hall7.jpg" /> </li>
                <li> <img src="images/halls/hall8.jpg" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">BALLAN MEN’S TAILORING</div>
          <div class="detail_hall_description">Hail Street, Al-Ruwais, Jeddah 22231, Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Shop</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
         <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      
	   
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left tailors-l">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/tailors.jpg" alt="" /></div>
          <div class="service-product-name">Saudi</div>
          
		  <div class="container_total_price abaya_price">Total Price: <span class="cont_final_price">SAR 520</span></div>
		  <div class="akaya_bodymesurment"><a href="#pop" class="open_popup">Body Measurement</a></div>
		  <div class="addto_cart"><input class="form-btn" value="Add to Cart" type="submit"></div>
          
		  
           
   
        </div>
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="common_popup measurement-outer"><a class="linking" name="pop">.</a>
  <div class="measurement-popup"> <a href="javascript:void(0);" class="close_popup_btn">X</a>
    <div class="main_title">Body Measurement</div>
    <div class="measurement-popup-img"><img src="images/measurement.jpg" alt=""  /></div><div class="measurement-divder">&nbsp;</div>
	<div class="measurement-form">
	<div class="measurement-form-line">
	<div class="measurement-form-top">Length</div> 
	<div class="measurement-form-bottom"><input type="text" class="t-box" /> <div class="parametar">cm</div></div> 
	</div> <!-- measurement-form-line -->
	
	<div class="measurement-form-line">
	<div class="measurement-form-top">Chest Size</div> 
	<div class="measurement-form-bottom"><input type="text" class="t-box" /> <div class="parametar">cm</div></div> 
	</div> <!-- measurement-form-line -->
	
	<div class="measurement-form-line">
	<div class="measurement-form-top">Waist Size</div> 
	<div class="measurement-form-bottom"><input type="text" class="t-box" /> <div class="parametar">cm</div></div> 
	</div> <!-- measurement-form-line -->
	
	<div class="measurement-form-line">
	<div class="measurement-form-top">Shoulders</div> 
	<div class="measurement-form-bottom"><input type="text" class="t-box" /> <div class="parametar">cm</div></div> 
	</div> <!-- measurement-form-line -->
	
	<div class="measurement-form-line">
	<div class="measurement-form-top">Neck</div> 
	<div class="measurement-form-bottom"><input type="text" class="t-box" /> <div class="parametar">cm</div></div> 
	</div> <!-- measurement-form-line -->
	
	<div class="measurement-form-line">
	<div class="measurement-form-top">Arm Length</div> 
	<div class="measurement-form-bottom"><input type="text" class="t-box" /> <div class="parametar">cm</div></div> 
	</div> <!-- measurement-form-line -->
	
	<div class="measurement-form-line">
	<div class="measurement-form-top">Wrist Diameter</div> 
	<div class="measurement-form-bottom"><input type="text" class="t-box" /> <div class="parametar">cm</div></div> 
	</div> <!-- measurement-form-line -->
	
		<div class="measurement-form-line">
	<div class="measurement-form-top">Cutting</div> 
	<div class="measurement-form-bottom">
	<div class="measurement-radio-line"><input type="radio" name="re" id="Normal" /> <label for="Normal">Normal</label></div>
	<div class="measurement-radio-line"><input type="radio" name="re" id="Ample" /> <label for="Ample">Ample</label></div>
	<div class="measurement-radio-line"><input type="radio" name="re" id="Curved" /> <label for="Curved">Curved</label></div>
	</div> 
	</div> <!-- measurement-form-line -->
	</div> <!-- measurement-form -->
	
	
	
	
	<div class="measurement-btn-area"><input type="submit" class="form-btn big-btn" value="CONTINUE WITH THESE MEASURES" /></div>
	
  </div>
</div>
</div>
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

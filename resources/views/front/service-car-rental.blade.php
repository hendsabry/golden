<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <div class="inner_wrap service-wrap diamond_space">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">little flora</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      
      
      <!--service-display-section-->
      
     <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
          <div class="service-product-name">Mercedes Benz</div>
          <div class="service-beauty-description">The most expensive dates are called Ajwa dates which come from the Madina and are said to have a lot of benefits such as a cure for diabetes and tons of other diseases. They are soft and rich in fiber and help in detoxing the body. </div>
		  <div class="car_rent_section">
          <div class="car_model">
		  <div class="car_text">Car Model</div>
		  <div class="car_text_type">New Car 2018</div>
		  
		  </div>
		  <div class="car_model">
		  <div class="car_text">Price Per KM</div>
		  <div class="car_text_type">SAR 10</div>
		  
		  </div>
		  <div class="car_model">
		  <div class="car_text">Service Date</div>
		  <div class="car_text_type">04.30.2018</div>
		  
		  </div>
		  <div class="car_model">
		  <div class="car_text">Service Date</div>
		  <div class="car_text_type">04.30.2018</div>
		  
		  </div>
		  <div class="car_model">
		  <div class="car_text">Time of Service</div>
		  <div class="car_text_type">4:30 PM</div>
		  <div class="car_small">(Night charges may be applicable depending on the pick-up time.)</div>
		  
		  </div>
          </div>
          
          <div class="total_food_cost">
            <div class="total_price">Total Price: <span>SAR 660</span></div>
          </div>
          <div class="btn_row">
            <input type="button" value="Book a Car" class="form-btn addto_cartbtn">
          </div>
          <!-- container-section -->
        </div>
        
        <!-- service-display-left -->
      </div>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

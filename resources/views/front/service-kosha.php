<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">DIWAN Events</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>  <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>  <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="service-kosha.php#kosha" class="select">Design Your Kosha</a></li>
                <li><a href="service-ready-made-kosha.php#kosha">Ready Made Kosha</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  <!-- service_bottom -->
      <div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
            <ul>
              <li onclick="sel(1);"><a href="#o" id="a1" class="select">Stage</a></li>
              <li onclick="sel(2);"><a href="#o" id="a2">Lightning</a></li>
              <li onclick="sel(3);"><a href="#o" id="a3">Chair</a></li>
              <li onclick="sel(4);"><a href="#o" id="a4">Curtains</a></li>
            </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line -->
        <div class="kosha-select-area">
          <div class="kosha-tab-area" id="table1">
            <div class="kosha-heading">Select Stage</div>
            <div class="kosha-box">
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">The Ritz Carlton</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 20,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div> <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha2.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Intercontinental</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 10,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div> <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Four Seasons</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 30,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div> <!-- kosha-select-line -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table2" style="display:none;">
            <div class="kosha-heading">Select Lightning</div>
            <div class="kosha-box">
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha2.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Intercontinental</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 10,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div>  <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Four Seasons</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 30,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div>
              <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha2.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">The Ritz Carlton</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 20,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div>  <!-- kosha-select-line -->
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table3" style="display:none;">
            <div class="kosha-heading">Select Chair</div>
            <div class="kosha-box">
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">The Ritz Carlton</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 20,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div> <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha2.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Intercontinental</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 10,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div>  <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Four Seasons</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 30,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div>  <!-- kosha-select-line -->
            </div>  <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table4" style="display:none;">
            <div class="kosha-heading">Select Curtains</div>
            <div class="kosha-box">
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha2.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Intercontinental</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 10,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div> <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">Four Seasons</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 30,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div> <!-- kosha-select-line -->
              <div class="kosha-select-line">
                <div class="kosha-select-img"><img src="images/test-img/kosha2.jpg" alt="" /></div>
                <div class="kosha-select-cont">
                  <div class="kosha-select-name">The Ritz Carlton</div>
                  <div class="kosha-select-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</div>
                  <div class="kosha-select-prise">SAR 20,000</div>
                  <div class="kosha-select-radieo">
                    <input type="radio" name="re" />
                    <label>&nbsp;</label>
                  </div>
                </div>
              </div>
              <!-- kosha-select-line -->
            </div>
            <!-- kosha-box -->
          </div>
          <!-- kosha-tab-area -->
        </div>
        <!-- kosha-select-area -->
        <div class="kosha-selections-area">
          <div class="kosha-heading">Your Selections</div>
          <div class="kosha-box">
            <div class="selection-box-area">
              <div class="selection-box">
                <div class="selection-img"><img src="images/test-img/kosha1.jpg" alt="" /></div>
                <!-- selection-img -->
                <div class="selection-name">The Ritz Carlton</div>
                <!-- selection-name -->
                <div class="selection-prise">SAR 30,000</div>
                <!-- selection-prise -->
              </div>
              <!-- selection-box -->
              <div class="selection-box">
                <div class="selection-img"><img src="images/test-img/kosha3.jpg" alt="" /></div>
                <!-- selection-img -->
                <div class="selection-name">Flashlight</div>
                <!-- selection-name -->
                <div class="selection-prise">SAR 5,000</div>
                <!-- selection-prise -->
              </div>
              <!-- selection-box -->
              <div class="selection-box">
                <div class="selection-img"><img src="images/test-img/kosha4.jpg" alt="" /></div>
                <!-- selection-img -->
                <div class="selection-name">The Ritz Carlton</div>
                <!-- selection-name -->
                <div class="selection-prise">Wedding Chair</div>
                <!-- selection-prise -->
              </div>
              <!-- selection-box -->
              <div class="selection-box">
                <div class="selection-img"><img src="images/test-img/kosha5.jpg" alt="" /></div>
                <!-- selection-img -->
                <div class="selection-name">Rose Curtains</div>
                <!-- selection-name -->
                <div class="selection-prise">SAR 30,000</div>
                <!-- selection-prise -->
              </div>
              <!-- selection-box -->
            </div>
            <!-- selection-box-area -->
            <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 29,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div>
            <!-- kosha-tolat-area -->
          </div>
          <!-- kosha-box -->
        </div>
        <!-- kosha-selections-area -->
      </div>  <!-- kosha-area -->
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>
 
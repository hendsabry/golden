<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">Gold in riyadh</div>
          <div class="detail_hall_description">As Suwaidi Al Gharabi, Riyadh 12992, Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Oud & Perfumes</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <!-- service-mid-wrapper -->
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom" id="anch">
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="#" class="select">Yellow Gold</a></li>
                <li><a href="#">White Gold</a></li>
                <li><a href="#">Rose Gold</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
          <div class="service-product-name">Riyadh Jewelry</div>
          <div class="service-beauty-description">The most expensive dates are called Ajwa dates which come from the Madina and are said to have a lot of benefits such as a cure for diabetes and tons of other diseases. They are soft and rich in fiber and help in detoxing the body.</div>
          <div class="container-section">
            <div class="duration">Caliber <span>550</span></div>
            <div class="duration marT15">Weight <span>5 Gram</span></div>
            <div class="total_food_cost">
            <div class="total_price">Total Price: <span>SAR 660</span></div>
          </div>
		  <div class="write_review">
          	<a href="javascript:void(0);" class="open_popup">Write on Your Ring</a>
          </div>
            <div class="btn_row">
            <input type="button" value="Add to cart" class="form-btn addto_cartbtn">
          </div>
          
          </div>
          <!-- container-section --> 
        </div>
        
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc -->
      
      <div class="common_popup">
      <div class="ring_review_popup">      
		<a href="javascript:void(0);" class="close_popup_btn">X</a>
        <div class="main_title">Write on Your Ring</div>
        
        <div class="review_box">
        	<textarea class="text-area"></textarea>
        </div>
        <div class="btn_row">
            <input type="button" value="Submit" class="form-btn addto_cartbtn">
          </div>
      </div> <!-- review_popup -->
	  </div> <!-- common_popup -->
	  
	  
      
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

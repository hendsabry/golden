<?php include('inc/in-head.php'); ?>


<div class="outer_wrapper">
<div class="inner_wrap">
<?php include('inc/header.php'); ?>

<div class="search-section">
 <div class="event-result-form">
<div class="search-box search-box1">
<div class="search-box-label">Type of Occasion</div>
<div class="search-noneedit">Wedding</div>
</div> <!-- search-box -->

<div class="search-box search-box2">
<div class="search-box-label">Number of Guests</div>
<div class="search-noneedit">500</div>
</div> <!-- search-box -->

<div class="search-box search-box3">
<div class="search-box-label">Budget</div>
<div class="search-noneedit">SAR 50,000</div>
</div> <!-- search-box -->

<div class="search-box search-box4">
<div class="search-box-label">Gender</div>
<div class="search-noneedit">Female</div>
</div> <!-- search-box -->

<div class="search-box search-box5">
<div class="search-box-label">City</div>
<div class="search-noneedit">Riyadh</div>
</div> <!-- search-box -->

<div class="search-box search-box6">
<div class="search-box-label">Booking Date</div>
<div class="search-noneedit">11/04/2018</div>
</div> <!-- search-box -->

 
</div> <!-- modify-search-form -->
 
</div> <!-- search-section -->
  


<div class="tatal-services-area">
<div class="mobile-filter-line"> 
<div class="mobile-search mobile-search1row"><span>Event Result</span></div>
</div>
<div class="tatal-services-inner-area">
<div class="tatal-services-heading">Total 9 Services</div>

<div class="table tatal-services-table">
        <div class="tr table_heading_tr">
          <div class="table_heading">Sl. No.</div>
          <div class="table_heading">Services</div>
          <div class="table_heading">Service Provider</div>
          <div class="table_heading">Price</div>
        </div>       	 
 

<div class="tr">
	 <div class="td td1" data-title="Sl. No.">1</div>
	  <div class="td td2" data-title="Services">Halls <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Taj Halls</div>
 <div class="td td4" data-title="Price">SAR 20,000</div>
	 </div>
	 
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">2</div>
	  <div class="td td2" data-title="Services">Food <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Reza Food Services</div>
 <div class="td td4" data-title="Price">SAR 15,000</div>
	 </div>
	
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">3</div>
	  <div class="td td2" data-title="Services">Concert <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Al Music Concert</div>
 <div class="td td4" data-title="Price">SAR 30,000</div>
	 </div>
 
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">4</div>
	  <div class="td td2" data-title="Services">Clinics <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Kaya Skin Clinic</div>
 <div class="td td4" data-title="Price">SAR 500</div>
	 </div>
	 
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">5</div>
	  <div class="td td2" data-title="Services">Beauty <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Manna Beauty Center</div>
 <div class="td td4" data-title="Price">SAR 500</div>
	 </div>
	 
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">6</div>
	  <div class="td td2" data-title="Services">Fashion <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">AlYasra Fashi</div>
 <div class="td td4" data-title="Price">SAR 1,000</div>
	 </div>
 
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">7</div>
	  <div class="td td2" data-title="Services">Shopping <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Zain</div>
 <div class="td td4" data-title="Price">SAR 1,500</div>
	 </div>
	 
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">8</div>
	  <div class="td td2" data-title="Services">Car Rental <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Hertz Car Rental</div>
 <div class="td td4" data-title="Price">SAR 1,100</div>
	 </div>	 	 
 
<div class="tr">
	 <div class="td td1" data-title="Sl. No.">9</div>
	  <div class="td td2" data-title="Services">Tourisim <a href="#" class="services-edit">&nbsp;</a></div>
	    <div class="td td3" data-title="Service Provider">Al Shitaiwi Tours</div>
 <div class="td td4" data-title="Price">SAR 2,000</div>
	 </div>	 	
<div class="tr total-prise">
	 <div class="td td1" data-title="Sl. No.">&nbsp;</div>
	  <div class="td td2" data-title="Services">&nbsp;</div>
	    <div class="td td3" data-title="Service Provider">&nbsp;</div>
 <div class="td td4" data-title="Total"><span>Total</span> 71,600</div>
	 </div>	
 </div> <!-- table -->
<div class="tatal-services-pay-area"><input type="submit" value="Pay Now" class="form-btn" /></div> <!-- tatal-services-pay-area -->

</div> <!-- tatal-services-inner-area -->
</div> <!-- tatal-services-area -->





</div> <!-- outer_wrapper -->
</div>
<?php include('inc/footer.php'); ?>
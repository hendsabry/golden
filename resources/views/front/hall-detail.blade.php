<?php include('inc/in-head.php'); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php include('inc/vendor-header.php'); ?>
<div class="inner_wrap">

<div class="detail_page hall-extra-class">	
    <a name="about"></a>	
    <div class="service_detail_row">
    	
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
            <li>
  	    	    <img src="images/halls/hall1.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall2.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall3.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall4.jpg" />
  	    		</li>
            <li>
  	    	    <img src="images/halls/hall5.jpg" />
  	    		</li>
  	    		
  	    		<li>
  	    	    <img src="images/halls/hall7.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall8.jpg" />
  	    		</li>              	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
            <li>
  	    	    <img src="images/halls/hall1.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall2.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall3.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall4.jpg" />
  	    		</li>
            <li>
  	    	    <img src="images/halls/hall5.jpg" />
  	    		</li>
  	    		
  	    		<li>
  	    	    <img src="images/halls/hall7.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall8.jpg" />
  	    		</li>
           
          </ul>
        </div>
      		</section>
            
        </div>
        
        <div class="service_detail">
        	<div class="detail_title">Taj Halls</div>
            <div class="detail_hall_description">Mohammad Al Maqdimi St. Exit 5, 11352, Riyadh</div>
            <div class="detail_hall_subtitle">About Hall</div>
            <div class="detail_about_hall">Wahat Al Nafil Exit 5 is located in Riyadh and offers self-catering accommodation. Free WiFi access is available... <a href="#">More</a></div>
            <div class="detail_hall_dimention">Hall Dimension: <span>4000 sqft</span></div>
            <div class="detail_hall_price">SAR 25,000</div>
            <input type="button" value="Reserve Now" class="reserve_now">
            <div class="common_title">Facilities Offered Here</div>
            <ul class="detail_facilites_list">
            	<li>A/C</li>
                <li>Lights</li>
                <li>Waiter</li>
            </ul>
        </div> 
        
    </div> <!-- service_detail_row -->
        
    <div class="service_container">
        <div class="services_detail">
        	
        	<div class="service_list_row">
            	<a name="prepaid"></a>
                <div class="common_title">Prepaid Services</div>
                <ul class="service_catagory">
                	<li><input name="service1" type="checkbox" id="service1" value="forever"><label for="service1">Service 1 <span class="service_price">SAR 20</span></label></li>
                    <li><input name="service2" type="checkbox" id="service2" value="forever"><label for="service2">Service 2 <span class="service_price">SAR 30</span></label></li>
                    <li><input name="service3" type="checkbox" id="service3" value="forever"><label for="service3">Service 3 <span class="service_price">SAR 30</span></label></li>
                    <li><input name="service3" type="checkbox" id="service4" value="forever"><label for="service4">Service 4 <span class="service_price">SAR 30</span></label></li>
                </ul>
            </div>
                        
            <div class="service_list_row">
            	<a name="food"></a>
                <div class="common_title">Food</div>
                <ul class="service_catagory">
                	<li><input name="service5" type="checkbox" id="service5" value="forever"><label for="service5">External Food </label></li>
                    <li><input name="service6" type="checkbox" id="service5" value="forever"><label for="service5">Internal Food </label></li>
                </ul>
            </div>
            
            <div class="service_list_row service_testimonial">
            	<a name="client_say"></a>
                <div class="common_title">What Our Client’s Says</div>
                <div class="testimonial_slider">
                	<section class="slider">
                        <div class="flexslider1">
                          <ul class="slides">
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="images/testimonial.jpg"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                                    	<div class="testim_name">Sakeena</div>
                                        <div class="testim_star"><img src="images/star.jpg"></div>
                                    </div>
                                </div>
                            </li>
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="images/testimonial.jpg"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                                    	<div class="testim_name">Sakeena</div>
                                        <div class="testim_star"><img src="images/star.jpg"></div>
                                    </div>
                                </div>
                            </li>
                          </ul>
                        </div>
                      </section>
                </div>
            </div>
            
        </div>
        
        <div class="services_listing">
        	<ul>
            	<li>
                	<span class="service_name">Taj Hall</span>
                    <span class="service_price">SAR 25,000</span>
                </li>
                <li>
                	<span class="service_name">Services 1</span>
                    <span class="service_price">SAR 20</span>
                </li>
                <li>
                	<span class="service_name">Services 2</span>
                    <span class="service_price">SAR 30</span>
                </li>
                <li>
                	<span class="service_name">Services 3</span>
                    <span class="service_price">SAR 30</span>
                </li>
                <li>
                	<span class="service_name">Internal Food</span>
                    <span class="service_price">SAR 200</span>
                </li>
                <li class="total_cost">
                	<span class="service_name">Total Price</span>
                    <span class="service_price">SAR 25,280</span>
                </li>
            </ul>
        </div>
        
    </div> <!-- service_container -->

	<div class="sticky_other_service">

<div class="sticky_serivce">Other Services</div>
<div class="sticku_service_logo"><img src="images/logo.png"></div>

</div>

	<div class="other_serviceinc">
<div class="other_servrow">
	<div class="serv_title">Other Services</div>
    <a href="javascript:void(0);" class="serv_delete">X</a>
</div>
<?php include('inc/customer-budget-section.php'); ?>
</div> <!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div> <!-- innher_wrap -->

   
    
</div> <!-- outer_wrapper -->

<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>


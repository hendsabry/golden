<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">Digital photography</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>  <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>  <!-- service-mid-wrapper -->
      <div class="service_bottom"><a name="photography" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="service-photography-studio.php#photography">Photography</a></li>
                <li><a href="service-video-photography.php#photography" class="select">Video Photography</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  <!-- service_bottom -->
      <div class="kosha-area">
        <div class="kosha-tab-line"> <span class="sat">
          <div id="kosha-tab" class="content">
            <ul>
              <li onclick="sel(1);"><a href="#o" id="a1" class="select">Package 1</a></li>
              <li onclick="sel(2);"><a href="#o" id="a2">Package 2</a></li>
              <li onclick="sel(3);"><a href="#o" id="a3">Package 3</a></li>
              <li onclick="sel(4);"><a href="#o" id="a4">Package 4</a></li>
            </ul>
          </div>
          </span> </div>  <!-- kosha-tab-line --> 
		   <div class="rm-kosha-outer">
        <div class="rm-kosha-area">
          <div class="kosha-tab-area" id="table1">
            <div class="rm-kosha-name photographi-heading">Package 1</div>
            <div class="kosha-box">
               <div class="photographi-area">
			   <div class="photographi-left-area"><img src="images/test-img/photography-img4.jpg"  alt="" /></div> 
			   <div class="photographi-right-area">
			   <div class="photographi-name">Package 1</div> 
			   <div class="photographi-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div> 
			   <div class="photographi-qty-line">Duration of the Video  <span>5 Hrs</span></div> 
			   <div class="photographi-qty-line">Number of Cameras <span>10</span></div> 
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 5,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
			   </div> 
			   
			   
			   </div> <!-- photographi-area -->
			   
			   
			   
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table2" style="display:none;">
            <div class="rm-kosha-name photographi-heading">Package 2</div>
            <div class="kosha-box">
               <div class="photographi-area">
			   <div class="photographi-left-area"><img src="images/test-img/photography-img1.jpg"  alt="" /></div> 
			   <div class="photographi-right-area">
			   <div class="photographi-name">Package 2</div> 
			   <div class="photographi-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div> 
			   <div class="photographi-qty-line">Duration of the Video  <span>150 Hrs</span></div> 
			   <div class="photographi-qty-line">Number of Cameras <span>21</span></div> 
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 45,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
			   </div> 
			   
			   
			   </div> <!-- photographi-area -->
			   
			   
			   
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table3" style="display:none;">
            <div class="rm-kosha-name photographi-heading">Package 3</div>
            <div class="kosha-box">
               <div class="photographi-area">
			   <div class="photographi-left-area"><img src="images/test-img/photography-img2.jpg"  alt="" /></div> 
			   <div class="photographi-right-area">
			   <div class="photographi-name">Package 3</div> 
			   <div class="photographi-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div> 
			   <div class="photographi-qty-line">Duration of the Video  <span>35 Hrs</span></div> 
			   <div class="photographi-qty-line">Number of Cameras <span>30</span></div> 
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 35,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
			   </div> 
			   
			   
			   </div> <!-- photographi-area -->
			   
			   
			   
            </div> <!-- kosha-box -->
          </div> <!-- kosha-tab-area -->
          <div class="kosha-tab-area" id="table4" style="display:none;">
            <div class="rm-kosha-name photographi-heading">Package 4</div>
            <div class="kosha-box">
               <div class="photographi-area">
			   <div class="photographi-left-area"><img src="images/test-img/photography-img3.jpg"  alt="" /></div> 
			   <div class="photographi-right-area">
			   <div class="photographi-name">Package 4</div> 
			   <div class="photographi-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div> 
			   <div class="photographi-qty-line">Duration of the Video  <span>75 Hrs</span></div> 
			   <div class="photographi-qty-line">Number of Cameras <span>40</span></div> 
			   <div class="kosha-tolat-area">
              <div class="kosha-tolat-prise">Total Price: <span>SAR 15,000</span></div>
              <div class="kosha-button-area">
                <input type="submit" value="Add to Cart" class="form-btn" />
              </div>
            </div> <!-- kosha-tolat-area -->
			   </div> 
			   
			   
			   </div> <!-- photographi-area -->
			   
			   
			   
            </div> <!-- kosha-box -->
          </div>  <!-- kosha-tab-area -->
        </div> <!-- rm-kosha-area -->
          </div> <!-- rm-kosha-outer -->
      </div>  <!-- kosha-area -->
      <div class="sticky_other_service" style="display:none;">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>
 

<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
  <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">DIWAN Events</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>  <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>  <!-- service-mid-wrapper -->
      <div class="service_bottom">
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="http://server/golden-cages/golden-HTML/service-abaya-ready-made" class="select">Ready Made</a></li>
                <li><a href="http://server/golden-cages/golden-HTML/service-abaya-tailor">Tailor</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  <!-- service_bottom -->
      
      
      
	  <div class="service-display-section"> 
      <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
          <div class="service-product-name">The Jade Abaya</div>
          <div class="service_selecrrow">
               <div class="checkout-form-top">Size</div> 
               <div class="checkout-form-bottom"><select class="checkout-small-box">
              <option>Select</option>
              <option>01</option>
              <option>02</option>
              </select></div> 
          </div>
          <div class="total_food_cost">
            <div class="total_price">Total Price: <span>SAR 500</span></div>
          </div>
          <div class="btn_row">
            <input type="button" value="Add to Cart" class="form-btn addto_cartbtn">
          </div>
          
        </div>
        
        
        
        
      </div>

      
      <div class="sticky_other_service" style="display:none;">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>
<script>
function sel(num){
	for(i=1;i<=20;i++){
		if(i==num){
			jQuery('#a'+i).addClass('select');
			jQuery('#s'+i).addClass('select');
			jQuery('#table'+i).show();
		}
		else{
			jQuery('#a'+i).removeClass('select');
			jQuery('#s'+i).removeClass('select');
			jQuery('#table'+i).hide();
		}
	}
}
</script>

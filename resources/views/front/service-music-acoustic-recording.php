<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
  <div class="inner_wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">Digital photography</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>  <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      
</div>  <!-- service-mid-wrapper -->

	<div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="service-music-acoustic-recording.php#kosha" class="select">Recording</a></li>
                <li><a href="service-music-equipment-buy.php#kosha">Equipment</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>  <!-- service_bottom -->	
		
		
		<div class="recording-area">
		<div class="kosha-box">
		 
		 <div class="checkout-form">
		    <div class="checkout-form-row">
   <div class="checkout-form-cell cell-full-width">
		    <div class="checkout-form-top">The Groom's Name</div> 
   <div class="checkout-form-bottom"><input class="t-box" type="text"></div> 
   </div> <!-- checkout-form-cell -->
   </div>
		 
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Hall</div> 
   <div class="checkout-form-bottom"><input class="t-box" type="text"></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Occasion Type</div> 
   <div class="checkout-form-bottom"><input class="t-box" type="text"></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="form-cell-date">
   <div class="checkout-form-top">Date</div> 
   <div class="checkout-form-bottom"><input class="t-box cal-t" type="text"></div> 
   </div>
    <div class="form-cell-time">
   <div class="checkout-form-top">Time</div> 
   <div class="checkout-form-bottom"><select class="sel-time">
  <option></option>
  <option>01</option>
  <option>02</option>
  </select></div> 
   </div>
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Recording Section</div> 
   <div class="checkout-form-bottom"><input class="t-box" type="text"></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   
   
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Music Type</div> 
   <div class="checkout-form-bottom"><input class="t-box" type="text"></div> 
   </div> <!-- checkout-form-cell -->
   
    <div class="checkout-form-cell">
   <div class="checkout-form-top">Singer Name</div> 
   <div class="checkout-form-bottom"><input class="t-box" type="text"></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
     <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Song Name</div> 
   <div class="checkout-form-bottom"><input class="t-box" type="text"></div> 
   </div> <!-- checkout-form-cell -->
   
    <div class="checkout-form-cell">
 <div class="add-more-area"><a href="#" class="add-more-btn">Add</a></div>
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   
   <div class="acoustic-total-area">SAR 200</div>
    <div class="acoustic-button-line"><input class="form-btn" value="Order Song" type="submit"></div>
	
	
	
   
   </div>
		 
		 
		</div>
		</div>
		
		
		
		
      <div class="sticky_other_service" style="display:none;">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>  <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>
 

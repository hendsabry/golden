<?php include('inc/in-head.php'); ?>


<div class="outer_wrapper">
<div class="inner_wrap">
<?php include('inc/header.php'); ?>

 <!-- search-section -->


 



<div class="page-left-right-wrapper">

<div class="myaccount_left">
<ul>
<li><a href="{{route('my-account-profile')}}">{{ (Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')}}</a></li>
<li><a href="my-account-ocassion.php" class="select">Occasions </a></li>
<li><a href="#">Studio</a>  </li>
<li><a href="#">Security Money</a></li>
<li><a href="my-wallet.php">Wallet</a> </li>
<li><a href="#">Reviews </a> </li>
<li><a href="{{route('change-password')}}">{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}</a></li>
</ul>


</div>
<div class="myaccount_right">
<h1 class="dashborad_heading">My Profile</h1>

<div class="field_group top_spacing_margin_occas myprofile_page">


<div class="checkout-box">
   <div class="checkout-form">
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Name</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box" /></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Email</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box" /></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Telephone Number</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box checkout-small-box" /></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Gender</div> 
   <div class="checkout-form-bottom">
   <div class="gender-radio-box"><input id="male" name="re" type="radio"> <label for="male">Male</label></div>
   <div class="gender-radio-box"><input id="female" name="re" type="radio"> <label for="female">Female</label></div>
   </div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Date of Birth</div> 
   <div class="checkout-form-bottom checkout-dob"> 
    <select>
  <option>DD</option>
  <option>01</option>
  <option>02</option>
  </select>
  <select>
  <option>MM</option>
  <option>01</option>
  <option>02</option>
  </select>
  <select>
  <option>YYYY</option>
  <option>1986</option>
  <option>1987</option>
  </select></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Payment Method</div> 
   <div class="checkout-form-bottom"><select class="checkout-small-box">
  <option>Credit Card</option>
  <option>01</option>
  <option>02</option>
  </select></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Country</div> 
   <div class="checkout-form-bottom not-edit">Saudi Arabia</div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">City</div> 
   <div class="checkout-form-bottom"><select class="checkout-small-box">
  <option>Riyadh</option>
  <option>01</option>
  <option>02</option>
  </select></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell address-cell">
   <div class="checkout-form-top">Address</div> 
   <div class="checkout-form-bottom">
   <input type="text" class="t-box" />
   <input type="text" class="t-box" /></div> 
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell">
   <div class="checkout-form-top">Pin Code</div> 
   <div class="checkout-form-bottom"><input type="text" class="t-box checkout-small-box" /></div> 
   </div> <!-- checkout-form-cell -->
   </div> <!-- checkout-form-row --> 
   
   <div class="checkout-form-row">
   <div class="checkout-form-cell">
   
   	<div class="col2 browse_row">
    	<div class="search-box-label">Upload Your National ID</div>
        <div class="info-box50 input-file-box">
			<div class="input-field-name">
            	<label for="company_logo"><div class="file-btn-area"><div class="file-btn">Upload</div><div id="file_value1" class="file-value"></div></div></label>
            </div><input id="company_logo" class="info-file" type="file"> 
		</div>
    </div>
   
    
   </div> <!-- checkout-form-cell -->
   
   <div class="checkout-form-cell"></div>
   
   </div> <!-- checkout-form-row --> 
   
   </div> <!-- checkout-form -->
   
   </div> <!-- checkout-box -->

</div>

<div class="myprf_btn"><input type="submit" value="Save" class="form-btn"></div>

</div>



 <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->





</div> <!-- outer_wrapper -->
</div>
<?php include('inc/footer.php'); ?>
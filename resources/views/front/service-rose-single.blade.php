<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">little flora</div>
          <div class="detail_hall_description">Riyadh, 11493 Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Center</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
         <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom">
        <div class="service_line">
          <div class="service_tabs">
            <ul>
              <li><a href="service-roses.php">Package</a></li>
              <li><a href="service-rose-single.php" class="select">Single</a></li>
            </ul>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
          <div class="service-product-name marB0">Red Roses</div>
          <div class="packge_numb">Number of Flowers</div>
          <div class="service_quantity_box">
            <div class="service_qunatity_row"> <span class="service_qunt_txt">3</span><span class="service_qunt_price">SAR 300</span> </div>
          </div>
          <div class="thumbnail_wrap">
            <div class="leftbar_title">Wrapping Type</div>
            <div class="choose_staf_box choose_roses">
              <div class="choose_name">
                <div class="choose_img"><img src="images/BeautyandElegance/roses-thumnail1.jpg" alt="" /></div>
                <div class="choose_text">Name</div>
                <div class="select_price">SAR 50</div>
                <div class="choose_radio">
                  <input id="name" name="re" type="radio">
                  <label for="name">&nbsp;</label>
                </div>
              </div>
              <div class="choose_name">
                <div class="choose_img"><img src="images/BeautyandElegance/roses-thumnail1.jpg" alt="" /></div>
                <div class="choose_text">Name</div>
                <div class="select_price">SAR 50</div>
                <div class="choose_radio">
                  <input id="name1" name="re" type="radio">
                  <label for="name1">&nbsp;</label>
                </div>
              </div>
              <div class="choose_name">
                <div class="choose_img"><img src="images/BeautyandElegance/roses-thumnail1.jpg" alt="" /></div>
                <div class="choose_text">Name</div>
                <div class="select_price">SAR 50</div>
                <div class="choose_radio">
                  <input id="name2" name="re" type="radio">
                  <label for="name2">&nbsp;</label>
                </div>
              </div>
            </div>
            <div class="leftbar_title">Wrapping Design</div>
            <div class="choose_staf_box">
              <div class="choose_name">
                <div class="choose_img"><img src="images/BeautyandElegance/roses-thumnail1.jpg" alt="" /></div>
                <div class="choose_text">Name</div>
                <div class="select_price">SAR 50</div>
                <div class="choose_radio">
                  <input id="name4" name="re" type="radio">
                  <label for="name4">&nbsp;</label>
                </div>
              </div>
              <div class="choose_name">
                <div class="choose_img"><img src="images/BeautyandElegance/roses-thumnail1.jpg" alt="" /></div>
                <div class="choose_text">Name</div>
                <div class="select_price">SAR 50</div>
                <div class="choose_radio">
                  <input id="name5" name="re" type="radio">
                  <label for="name5">&nbsp;</label>
                </div>
              </div>
              <div class="choose_name">
                <div class="choose_img"><img src="images/BeautyandElegance/roses-thumnail1.jpg" alt="" /></div>
                <div class="choose_text">Name</div>
                <div class="select_price">SAR 50</div>
                <div class="choose_radio">
                  <input id="name6" name="re" type="radio">
                  <label for="name6">&nbsp;</label>
                </div>
              </div>
            </div>
          </div>
          <div class="total_food_cost">
            <div class="total_price">Total Price: <span>SAR 660</span></div>
          </div>
          <div class="btn_row">
            <input type="button" value="Add to Cart" class="form-btn addto_cartbtn">
          </div>
        </div>
      </div>
      <!--service-display-section-->
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc -->
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

<?php include('inc/in-head.php'); ?>
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<?php include('inc/vendor-header.php'); ?>

<div class="inner_wrap service-wrap diamond_space">
<div class="detail_page">	
<a name="about_shop" class="linking">&nbsp;</a>
    <div class="service_detail_row">
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
            <li>
  	    	    <img src="images/halls/hall1.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall2.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall3.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall4.jpg" />
  	    		</li>
            <li>
  	    	    <img src="images/halls/hall5.jpg" />
  	    		</li>
  	    		
  	    		<li>
  	    	    <img src="images/halls/hall7.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall8.jpg" />
  	    		</li>              	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
            <li>
  	    	    <img src="images/halls/hall1.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall2.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall3.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall4.jpg" />
  	    		</li>
            <li>
  	    	    <img src="images/halls/hall5.jpg" />
  	    		</li>
  	    		
  	    		<li>
  	    	    <img src="images/halls/hall7.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/halls/hall8.jpg" />
  	    		</li>
           
          </ul>
        </div>
      		</section>
            
        </div>
        
        <div class="service_detail">
        	<div class="detail_title">The Riyadh date</div>
            <div class="detail_hall_description">Mohammad Al Maqdimi St. Exit 5, 11352, Riyadh</div>
            <div class="detail_hall_subtitle">About Shop</div>
            <div class="detail_about_hall">Wahat Al Nafil Exit 5 is located in Riyadh and offers self-catering accommodation. Free WiFi access is available... <a href="#">More</a></div>
             
              <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
   
 
             
        </div> 
        
    </div> <!-- service_detail_row -->
    
    
	      <div class="service-mid-wrapper">
		   <?php include('inc/service-video.php'); ?>         
           <?php include('inc/what-client-say.php'); ?>      
          </div> <!-- service-mid-wrapper -->
	
	
	
	<div class="service-display-section">
    <a name="choose_package" class="linking">&nbsp;</a>
	<div class="service-display-right">
	<?php include('inc/diamond-9-images-link.php'); ?></div> <!-- service-display-right -->
	<div class="service-display-left">
	<div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
	<div class="service-product-name">Ajwa Dates</div>
	<div class="service-product-description">The most expensive dates are called Ajwa dates which come from the Madina and are said to have a lot of benefits such as a cure for diabetes and tons of other diseases. They are soft and rich in fiber and help in detoxing the body.</div>
	<div class="service-radio-line">
    <div class="leftbar_title">Shop By</div>
	<div class="service-radio-box">
	<input type="radio" id="kilo" name="re" /> <label for="kilo">Per Kilo</label>
	</div>
	<div class="service-radio-box">
	<input type="radio" id="piece" name="re" /> <label for="piece">Per Piece</label>
	</div>
    
    <div class="service_quantity_box">
    	<div class="service_qunt">Quantity</div>
        <div class="service_qunatity_row">
        	<span class="service_qunt_txt">1</span><span class="service_qunt_price">SAR 200</span>
        </div>
    </div>
	</div>	<!-- service-radio-line -->
	
	<div class="container-section">
	<div class="leftbar_title">Select Container</div>
	<div class="container-box-line">
	<div class="container-box">
	<input id="container1" type="radio" name="re" />
	<label for="container1"><img src="images/container1.jpg" alt="" /></label>
	
	</div>
	
	<div class="container-box">
	<input id="container2" type="radio" name="re" />
	<label for="container2"><img src="images/container2.jpg" alt="" /></label>
	
	</div>
	
	</div> <!-- container-box-line -->
	
	<div class="container-prise"><span>Container Price:</span> <span>SAR 300</span></div> 
    
    <div class="container_total_price">Total Price: <span class="cont_final_price">SAR 300 </span></div>
    
    <div class="addto_cart"><input type="submit" class="form-btn" value="Add to Cart"></div>
	
	</div> <!-- container-section -->
	
	
	</div> <!-- service-display-left -->
	</div> <!--service-display-section-->
	
	
	
	
	
      

 


 
	<div class="sticky_other_service">

<div class="sticky_serivce">Other Services</div>
<div class="sticku_service_logo"><img src="images/logo.png"></div>

</div>

	<div class="other_serviceinc">
<div class="other_servrow">
	<div class="serv_title">Other Services</div>
    <a href="javascript:void(0);" class="serv_delete">X</a>
</div>
<?php include('inc/customer-budget-section.php'); ?>
</div> <!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div> <!-- innher_wrap -->

   
    
</div> <!-- outer_wrapper -->

<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>


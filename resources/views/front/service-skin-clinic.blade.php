<?php include('inc/in-head.php'); ?>

<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
  <?php include('inc/vendor-header.php'); ?>
  <!-- common_navbar -->
   <div class="inner_wrap service-wrap">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty-large.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty1.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty2.jpg" alt="" /> </li>
                <li> <img src="images/BeautyandElegance/beauty3.jpg" alt="" /> </li>
              </ul>
            </div>
          </section>
        </div>
        <div class="service_detail">
          <div class="detail_title">kaya clinic</div>
          <div class="detail_hall_description">As Suwaidi Al Gharabi, Riyadh 12992, Saudi Arabia</div>
          <div class="detail_hall_subtitle">About Oud & Perfumes</div>
          <div class="detail_about_hall">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.... <a href="#">More</a></div>
          <div class="detail_hall_dimention">City: <span>Riyadh</span></div>
        </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper">
        <?php include('inc/service-video.php'); ?>
        <!-- service-mid-wrapper -->
        <?php include('inc/what-client-say.php'); ?>
      </div>
      <!-- service-mid-wrapper -->
      <div class="service_bottom" id="anch">
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                <li><a href="#" class="select">Dermatology</a></li>
                <li><a href="#">Dental Clinic</a></li>
                <li><a href="#">Orthodontic Clinic</a></li>
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        <div class="service-display-right">
          <?php include('inc/diamond-9-images-link.php'); ?>
        </div>
        <!-- service-display-right -->
        <div class="service-display-left">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="images/diamond/gallery3.jpg" alt="" /></div>
          <div class="service-product-name">Dr. Majd Eldeen Douba</div>
          <div class="service-beauty-description">Specialist Dermatology</div>
          <div class="service_sidebar_list">
          	<div class="service_sidebar_list_title">Professional Bio</div>
            <ul>
            	<li>More than 22 years</li>
                <li>Master Dermatology, Syria</li>
                <li>Arab association of dermatology diseaseindustry's</li>
            </ul>
          </div>
          <div class="container-section">
            <div class="total_food_cost">
            <div class="total_price">Consultation Fees: <span>SAR 400 </span></div>
          </div>
            <div class="btn_row service_skin">
            <input type="button" value="Book Appointment" class="form-btn addto_cartbtn open_popup">
          </div>
          
          </div>
          <!-- container-section --> 
        </div>
        
        <!-- service-display-left -->
      </div>
      <!--service-display-section-->
      <div class="sticky_other_service">
        <div class="sticky_serivce">Other Services</div>
        <div class="sticku_service_logo"><img src="images/logo.png"></div>
      </div>
      <div class="other_serviceinc">
        <div class="other_servrow">
          <div class="serv_title">Other Services</div>
          <a href="javascript:void(0);" class="serv_delete">X</a> </div>
        <?php include('inc/customer-budget-section.php'); ?>
      </div>
      <!-- other_serviceinc -->
      
      
      <div class="common_popup">
      	<div class="ring_review_popup">      
		<a href="javascript:void(0);" class="close_popup_btn">X</a>
        <div class="appointment_title">Write on Your Ring</div>
        <div class="appointment_name">Dr. Majd Eldeen Douba</div>
        
        <div class="appointment_wrap">
        	<div class="appointment_row">
            	<div class="checkout-form-cell">
                   <div class="checkout-form-top">Date</div> 
                   <div class="checkout-form-bottom"><input type="text" class="t-box checkout-small-box cal-t"></div> 
                </div>
                <div class="checkout-form-cell">
                   <div class="checkout-form-top">Time</div> 
                   <div class="checkout-form-bottom">
                   	<select class="sel-time">
                   		<option>Time 1</option>
                        <option>Time 1</option>
                        <option>Time 1</option>
                    </select>
                   
                   </div> 
                </div>
            </div>
            
            <div class="appointment_row appointment_row_radio">            
            <div class="checkout-form-cell">
               <div class="checkout-form-top">Patient Status</div> 
               <div class="checkout-form-bottom">
               <div class="gender-radio-box"><input id="male" name="re" type="radio"> <label for="male">New</label></div>
               <div class="gender-radio-box"><input id="female" name="re" type="radio"> <label for="female">Existing</label></div>
               </div> 
           </div>
            </div>
            
        </div>
        
        <div class="btn_row">
            <input type="button" value="Book Appointment" class="form-btn addto_cartbtn">
        </div>
        
      </div> <!-- review_popup -->
	  </div> <!-- common_popup -->
      
    </div>
    <!-- detail_page -->
  </div>
  <!-- innher_wrap -->
</div>
<!-- outer_wrapper -->
<div class="othrserv_overl"></div>
<?php include('inc/footer.php'); ?>

@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <div class="dash_select"> <a href="{{route('my-account-ocassion')}}">{{ (Lang::has(Session::get('lang_file').'.BACK_TO_OCCASION_LIST')!= '')  ?  trans(Session::get('lang_file').'.BACK_TO_OCCASION_LIST'): trans($OUR_LANGUAGE.'.BACK_TO_OCCASION_LIST')}}</a> </div>
      <div class="field_group top_spacing_margin_occas">
        <div class="main_user">
          <div class="types_ocss">
            <div class="types_ocs_left">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')}}: </div>
                <div class="wed_text">Wedding</div>
              </div>
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.OCCASION_DATE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_DATE'): trans($OUR_LANGUAGE.'.OCCASION_DATE')}}:</div>
                <div class="wed_text">Mar 13, 2018</div>
              </div>
            </div>
            <div class="types_ocs_right">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}:</div>
                <div class="wed_text">SAR1,00,000</div>
              </div>
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_ATTENDANCE'): trans($OUR_LANGUAGE.'.NO_OF_ATTENDANCE')}}:</div>
                <div class="wed_text">2</div>
              </div>
            </div>
          </div>
          <div class="list_of_service">
            <h1 class="list_heading">{{ (Lang::has(Session::get('lang_file').'.LIST_OF_SERVICES')!= '')  ?  trans(Session::get('lang_file').'.LIST_OF_SERVICES'): trans($OUR_LANGUAGE.'.LIST_OF_SERVICES')}}</h1>
            <div class="main_box_table_area">
              <div class="myaccount-table mts">
                <div class="mytr">
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('lang_file').'.Sl_No')!= '')  ?  trans(Session::get('lang_file').'.Sl_No'): trans($OUR_LANGUAGE.'.Sl_No')}}</div>
                  <div class="mytable_heading grey"> {{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} </div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}</div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('lang_file').'.DELIVERY_DATE')!= '')  ?  trans(Session::get('lang_file').'.DELIVERY_DATE'): trans($OUR_LANGUAGE.'.DELIVERY_DATE')}}</div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}</div>
                  <div class="mytable_heading order_id grey">{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}</div>
                </div>
                <div class="mytr mybacg">
                  <div class="mytd mies " data-title="Sl. No.">1</div>
                  <div class="mytd mies " data-title="Occasions Name">Food</div>
                  <div class="mytd mies " data-title="Date">Reza Food Services</div>
                  <div class="mytd mies" data-title="Order ID">March 13, 2018</div>
                  <div class="mytd mies" data-title="Status">Delivered</div>
                  <div class="mytd mies order_id" data-title="Amount">SAR200</div>
                </div>
                <div class="mytr mybacg">
                  <div class="mytd mies " data-title="Sl. No.">1</div>
                  <div class="mytd mies " data-title="Occasions Name">Food</div>
                  <div class="mytd mies " data-title="Date">Reza Food Services</div>
                  <div class="mytd mies" data-title="Order ID">March 13, 2018</div>
                  <div class="mytd mies" data-title="Status">Delivered</div>
                  <div class="mytd order_id mies" data-title="Amount">SAR200</div>
                </div>
                <div class="mytr mybacg">
                  <div class="mytd mies " data-title="Sl. No.">1</div>
                  <div class="mytd mies  " data-title="Occasions Name">Food</div>
                  <div class="mytd mies " data-title="Date">Reza Food Services</div>
                  <div class="mytd mies" data-title="Order ID">March 13, 2018</div>
                  <div class="mytd mies" data-title="Status">Delivered</div>
                  <div class="mytd mies order_id" data-title="Amount">SAR200</div>
                </div>
                <div class="mytr mybacg">
                  <div class="mytd mies " data-title="Sl. No.">1</div>
                  <div class="mytd mies " data-title="Occasions Name">Food</div>
                  <div class="mytd mies " data-title="Date">Reza Food Services</div>
                  <div class="mytd mies" data-title="Order ID">March 13, 2018</div>
                  <div class="mytd mies" data-title="Status">Delivered</div>
                  <div class="mytd mies order_id" data-title="Amount">SAR200</div>
                </div>
                <div class="mytr mybacg">
                  <div class="mytd mies" data-title="Sl. No.">1</div>
                  <div class="mytd mies" data-title="Occasions Name">Food</div>
                  <div class="mytd mies" data-title="Date">Reza Food Services</div>
                  <div class="mytd mies" data-title="Order ID">March 13, 2018</div>
                  <div class="mytd mies" data-title="Status">Delivered</div>
                  <div class="mytd order_id mies" data-title="Amount">SAR200</div>
                </div>
              </div>
              <div class="main_total">{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: SAR1,900</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
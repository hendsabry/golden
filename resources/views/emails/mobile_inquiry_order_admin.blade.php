<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>


<table width="700" border="0" align="center" style="background-color:#fffaee;margin-top:40px;padding-bottom:20px">
  <tbody><tr>
    <td align="center" style="padding-top:40px"><img alt="" src="https://ci5.googleusercontent.com/proxy/WaTlnvJcIdqa7bOj4uV3WZY-9fufp6Q_QcAyOpiiF9njYpkifyBxMNhhLzT1KoCcKOGv9V3FRLq6cryGmm5CfEbVZCoHjAOO3KxjXfLhDvK3cFo6WrsxbeQ=s0-d-e1-ft#http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png" class="CToWUd"></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;font-weight:bold;text-align:right">مرحبًا  Admin</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;text-align:right">قد تلقينا طلبًا الى  من  {{ $user_name }}</td>
  </tr>  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px;font-weight:bold">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;font-weight:bold;text-align:right">مشرف</td>
  </tr>
  
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
  @if($getFbLinks !='')
  <a target="_blank" title="Facebook" href="{{$getFbLinks}}"><img src="https://www.wisitech.in/themes/images/social-facebook.png" /></a>&nbsp;
  @endif
    @if($gettwitterLinks !='')
  <a target="_blank" title="Twitter" href="{{$gettwitterLinks}}"><img src="https://www.wisitech.in/themes/images/social-twitter.png" /></a>&nbsp;
  @endif
    @if($getgoogleLinks !='')
  <a target="_blank" title="Google+" href="{{$getgoogleLinks}}"><img src="https://www.wisitech.in/themes/images/social-googleplus.png" /></a>&nbsp;
  @endif
    @if($getinstagramLinks !='')
  <a target="_blank" title="Instagram" href="{{$getinstagramLinks}}"><img src="https://www.wisitech.in/themes/images/share-instagram.png" /></a>&nbsp;
  @endif
    @if($getpinterestLinks !='')
  <a target="_blank" title="Pinterest" href="{{$getpinterestLinks}}"><img src="https://www.wisitech.in/themes/images/social-pint.png" /></a>&nbsp;
  @endif
    @if($getyoutubeLinks !='')
  <a target="_blank" title="Youtube" href=" {{$getyoutubeLinks}}"><img src="https://www.wisitech.in/themes/images/social-youtube.png" /></a>
  @endif
  </td>
  </tr>
  <!-- social -->
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;font-weight:bold">Hi Admin</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px">You have received an order for you from {{ $user_name }} .</td>
  </tr>

  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px"><table width="100%" cellspacing="0" cellpadding="8" border="0" style="border:solid 1px #faf2e1">
      <tbody>
<tr>
   <td style="padding-top:5px;padding-bottom:5px;font-Size:12pt;color:#fff;background:#c29f54;font-weight:bold" colspan="6"> Order Detail </td>
        </tr>
          <tr>
            
             @if($order !='')
             <td><strong>Order No </strong></td>
              <td colspan="2">{{ $order }}</td>
             @endif 
             @if($order_date !='')          
             <td><strong>Order Date </strong></td>         
              <td colspan="2">{{ $order_date }}</td>
             @endif
            </tr>
          <tr>
            
            @if($order_paytype !='')          
             <td><strong>Payment </strong></td>         
              <td colspan="2">{{ strtoupper($order_paytype) }}</td>
             @endif
                    
             <td><strong>Transcation Id </strong></td>
             @if($order_paytype != 'cod')   
             <td colspan="2">{{ $transaction_id }}</td>
             @else
             <td colspan="2">N/A</td>
             @endif
          </tr>
          <tr>
            @if($order_taxAmt !='')          
             <td><strong>VAT Charge </strong></td>         
             <td colspan="2">SAR {{ $order_taxAmt }}. 00</td>
            @endif
            @if($order_amt !='')          
             <td><strong>Toal Order Amt </strong></td>         
             <td colspan="2">SAR {{ $order_amt }}. 00</td>
            @endif
          </tr>
         <tr>
            <td  colspan="6" height="10"></td>
            </tr>
          <tr>
        <td style="padding-top:5px;padding-bottom:5px;font-Size:12pt;color:#fff;background:#c29f54;font-weight:bold" colspan="6"> Services/Products  </td>

<?php 
$sound_system = DB::table('nm_services_inquiry')->where('id', '=', $inquiry_id)->where('user_id', '=', $user_id)->first();

?>  
      </tr>        
    <!-- Start Popular Band from here -->
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<?php
 
if($sound_system->quote_for == 'singer' || $sound_system->quote_for == 'band')  { 

$city = DB::table('nm_city')->where('ci_id', '=', $sound_system->city_id)->first();

$music = DB::table('nm_music')->where('id', '=', $sound_system->relate_with)->first(); 

 
if($sound_system->quote_for == 'singer')  { 

?>    
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-Size:13pt;"><strong> Singers </strong></td>
<?php } else { ?>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-Size:13pt;"><strong> Popular Band </strong></td>
</tr>
<?php } ?>  
<tr>
<?php  if($sound_system->quote_for == 'singer') { ?>  
<td><strong> Singer Name</strong> </td>
<td colspan="2"><?php if(isset($music->name) && $music->name!='') { echo  $music->name; } ?></td>
<?php } else { ?>
<td><strong> Band Name </strong> </td>
<td colspan="2"><?php if(isset($music->name) && $music->name!='') { echo  $music->name; } ?></td>  
<?php } ?>
<td><strong>Address</strong></td>
<?php if(isset($music->address) && $music->address != '') {?>
<td colspan="2"><?php   echo  $music->address; ?></td>
<td><a href="<?php echo  $music->google_map_url; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td>
<?php } else {?>
<td colspan="2">N/A</td> 
<?php } ?>
</tr>
<tr>
<td><strong> Hall</strong> </td>
<td colspan="2">{{ $sound_system->hall or ''}}</td>
<td><strong> Occasion Type </strong></td>
<td colspan="2">{{ $sound_system->occasion_type or ''}}</td>
</tr>   
<tr>
  <td><strong>City</strong></td>
  <td colspan="2">{{ $city->ci_name or ''}}</td>
  <td><strong>Location</strong></td>
  <td >{{  $sound_system->location or ''}}</td>  
</tr>

<tr>
<td><strong> Duration</strong> </td>
<td colspan="2">{{ $sound_system->duration or ''}} hr </td>
<td><strong> Date </strong></td>
<td colspan="2"><?php echo  date("j M Y", strtotime($sound_system->date)); ?></td>
</tr> 
<tr>
<td><strong> Comment</strong> </td>
<td colspan="2"><?php echo  $sound_system->user_comment; ?></td>
<td><strong> Time </strong></td>
<td colspan="2"><?php echo  date("H:i A", strtotime($sound_system->time)); ?></td>
</tr> 

<tr>
  <td>&nbsp;</td>
  <td colspan="2">&nbsp;</td>
  <td align="right"><strong>Sub Total</strong></td>
  <td  colspan="2"><strong>SAR {{ $order_amt }}. 00</strong></td>
</tr>
<!-- End Popular Band from here -->
<?php } 
if($sound_system->quote_for == 'recording')  { 

$sound = DB::table('nm_category')->where('mc_id', '=', $sound_system->relate_with)->first();   

?>

<!-- Start Sound System from here -->
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-Size:13pt;"><strong> Studio System </strong></td>
</tr>   
<tr>
<td><strong> Studio Name</strong> </td>
<td colspan="2"><?php echo  $sound->mc_name; ?></td>
<td><strong>Studio Address</strong></td>
<td ><?php echo  $sound->address; ?></td>
<td><a href="<?php echo  $sound->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td>
</tr>
<tr>
<td><strong> Name </strong></td>
<?php if($sound_system->the_groooms_name != '') {?>
<td colspan="2"><?php echo  $sound_system->the_groooms_name; ?></td><?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>   
<tr>
  <td><strong>Hall</strong></td>  
<?php if($sound_system->hall != '') {?>
<td colspan="2"><?php echo  $sound_system->hall; ?></td><?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
<td><strong>Occasion Type</strong></td>
<?php if($sound_system->occasion_type != '') {?>
<td colspan="2"><?php echo  $sound_system->occasion_type; ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
  
</tr>

<tr>
<td><strong> Date</strong> </td>
<?php if($sound_system->date != '') {?>
<td colspan="2"><?php echo  date("j M Y", strtotime($sound_system->date)); ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
<td><strong> Singer </strong></td>
<?php if($sound_system->singer_name != '') {?>
<td colspan="2"><?php echo  $sound_system->singer_name; ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr> 

<tr>
<td><strong> Music Type</strong> </td>
<?php if($sound_system->music_type != '') {?>
<td colspan="2"><?php echo  $sound_system->music_type; ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
<td><strong> Song</strong> </td>
<?php if($sound_system->song_name != '') {?>
<td colspan="2"><?php echo  $sound_system->song_name; ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>
<tr>
  <td>&nbsp;</td>
  <td colspan="2">&nbsp;</td>
  <td align="right"><strong>Sub Total</strong></td>
  <td  colspan="2"><strong>SAR {{ $order_amt }}. 00</strong></td>
</tr>
<!-- End Sound System from here -->
<?php } ?>

    <tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
        </tbody></table></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px;font-weight:bold">&nbsp;</td>
  </tr>

  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px;font-weight:bold">Let us know if you have any further query.</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px">&nbsp;</td>
  </tr>
</tbody></table>
</body>
</html>

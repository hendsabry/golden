<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title>@if (Lang::has($lang.'.MAIL_EMAIL_TEMPLATE')!= '') {{  trans($lang.'.MAIL_EMAIL_TEMPLATE') }} @else {{ trans($LANUAGE.'.MAIL_EMAIL_TEMPLATE') }} @endif</title>
</head>
<body>
<?php 
// $logo {{ asset('img/logo.png') }}
//$logo = url('/').'assets/img/logo.png'; 
$SITE_LOGO = 'http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png';
$logo = $SITE_LOGO;
if(file_exists($SITE_LOGO))
$logo = $SITE_LOGO;
?>
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right  ">مرحبًا  Admin</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373;  text-align:right  ">لقد تلقيت طلبًا.</td>
  </tr>  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right;  ">مشرف</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; text-align:right; ">أقفاص ذهبية</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; text-align:right; "><table border="0" cellpadding="0" cellspacing="0" width="100%"> 
      <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.MAIL_NAME')!= '') {{  trans($lang.'.MAIL_NAME') }} @else {{ trans($LANUAGE.'.MAIL_NAME') }} @endif: &nbsp; <?php echo $customername ?>           
          </td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.MAIL_EMAIL')!= '') {{  trans($lang.'.MAIL_EMAIL') }} @else {{ trans($LANUAGE.'.MAIL_EMAIL') }} @endif: &nbsp; <?php echo $customeremail ?>           
          </td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.MAIL_PHONE')!= '') {{  trans($lang.'.MAIL_PHONE') }} @else {{ trans($LANUAGE.'.MAIL_PHONE') }} @endif: &nbsp; <?php echo $customerphone ?>           
          </td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.MAIL_ADDRESS')!= '') {{  trans($lang.'.MAIL_ADDRESS') }} @else {{ trans($LANUAGE.'.MAIL_ADDRESS') }} @endif: &nbsp; <?php echo $customerlocation ?>           
          </td>
        </tr>
     </table>

      <td>
  </tr>
  
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;  "><img src="http://wisitech.in/public/assets/img/icon.png" alt="" /></td>
  </tr>



  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">Hi Admin</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">You have received an order.
</td>
  </tr>  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <?php 
  $occasionName = DB::table('nm_business_occasion_type')->where('id', '=', $occasion)->first();
  $occasionDta = DB::table('nm_search_occasion')->where('order_id', '=', $order)->where('user_id', '=', $user_id)->first(); 
  $shippingDta = DB::table('nm_shipping_descriptions')->where('shipping_id', '=', $shipping)->first();
  $orderDta = DB::table('nm_order')->where('order_id', '=', $order)->first(); 
  $product = DB::table('nm_order_product')->where('order_id', '=', $order)->get(); 
  ?>
  <tr>
    <td style="padding-top:20px; padding-bottom:20px">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">   
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.OCCASION_NAME')!= '') {{  trans($lang.'.OCCASION_NAME') }} @else {{ trans($LANUAGE.'.OCCASION_NAME') }} @endif: &nbsp; <strong><a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $occasionName->title; ?></a></strong></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.TOTAL_MEMBER')!= '') {{  trans($lang.'.TOTAL_MEMBER') }} @else {{ trans($LANUAGE.'.TOTAL_MEMBER') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $occasionDta->total_member; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.TOTAL_BUDGET')!= '') {{  trans($lang.'.TOTAL_BUDGET') }} @else {{ trans($LANUAGE.'.TOTAL_BUDGET') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $occasionDta->budget; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.OCCASION_DATE')!= '') {{  trans($lang.'.OCCASION_DATE') }} @else {{ trans($LANUAGE.'.OCCASION_DATE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  date("d-m-Y", strtotime($occasionDta->occasion_date)); ?></a></td>
        </tr>

        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.ORDER')!= '') {{  trans($lang.'.ORDER') }} @else {{ trans($LANUAGE.'.ORDER') }} @endif: &nbsp;<a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->order_id; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.TRANSACTION_ID')!= '') {{  trans($lang.'.TRANSACTION_ID') }} @else {{ trans($LANUAGE.'.TRANSACTION_ID') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->transaction_id; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.PAYER_NAME')!= '') {{  trans($lang.'.PAYER_NAME') }} @else {{ trans($LANUAGE.'.PAYER_NAME') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->payer_name; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.TOTAL_ORDER_AMOUNT')!= '') {{  trans($lang.'.TOTAL_ORDER_AMOUNT') }} @else {{ trans($LANUAGE.'.TOTAL_ORDER_AMOUNT') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->order_amt; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.TOTAL_TAX_AMOUNT')!= '') {{  trans($lang.'.TOTAL_TAX_AMOUNT') }} @else {{ trans($LANUAGE.'.TOTAL_TAX_AMOUNT') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->order_taxAmt; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.PAYMENT_METHOD')!= '') {{  trans($lang.'.PAYMENT_METHOD') }} @else {{ trans($LANUAGE.'.PAYMENT_METHOD') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->order_paytype; ?></a></td>
        </tr>
        <?php if(!empty($orderDta->wallet_amount)){ ?>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.WALLET_AMOUNT')!= '') {{  trans($lang.'.WALLET_AMOUNT') }} @else {{ trans($LANUAGE.'.WALLET_AMOUNT') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->wallet_amount; ?></a></td>
        </tr>
       <?php }
       if(!empty($orderDta->hall_pending_amount)){ 
       ?>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.HALL_PENDING_AMOUNT')!= '') {{  trans($lang.'.HALL_PENDING_AMOUNT') }} @else {{ trans($LANUAGE.'.HALL_PENDING_AMOUNT') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $orderDta->hall_pending_amount; ?></a></td>
        </tr>
      <?php }
       if(!empty($shippingDta->shipping)){ 
       ?>

        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.SHIPPING')!= '') {{  trans($lang.'.SHIPPING') }} @else {{ trans($LANUAGE.'.SHIPPING') }} @endif: &nbsp; <strong><a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $shippingDta->shipping; ?></a></strong></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.DELIVERY_TIME')!= '') {{  trans($lang.'.DELIVERY_TIME') }} @else {{ trans($LANUAGE.'.DELIVERY_TIME') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $shippingDta->delivery_time; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.SHIPPING_IMG')!= '') {{  trans($lang.'.SHIPPING_IMG') }} @else {{ trans($LANUAGE.'.SHIPPING_IMG') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $shippingDta->image; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.SHIPPING_CHARGE')!= '') {{  trans($lang.'.SHIPPING_CHARGE') }} @else {{ trans($LANUAGE.'.SHIPPING_CHARGE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $shippingDta->shipping_charge; ?></a></td>
        </tr>
       <?php }
       ?>
       <tr>
        <td style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;"><strong>Product Details</strong></td>
       </tr>
        <?php if(!empty($product)){ 
        foreach ($product as $key => $value) { ?>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.PRODUCT_TYPE')!= '') {{  trans($lang.'.PRODUCT_TYPE') }} @else {{ trans($LANUAGE.'.PRODUCT_TYPE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $value->product_type; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.PRODUCT_TITLE')!= '') {{  trans($lang.'.PRODUCT_TITLE') }} @else {{ trans($LANUAGE.'.PRODUCT_TITLE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $value->pro_title; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.PRODUCT_IMG')!= '') {{  trans($lang.'.PRODUCT_IMG') }} @else {{ trans($LANUAGE.'.PRODUCT_IMG') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $value->pro_Img; ?></a></td>
        </tr>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.PRODUCT_PRICE')!= '') {{  trans($lang.'.PRODUCT_PRICE') }} @else {{ trans($LANUAGE.'.PRODUCT_PRICE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $value->pro_price; ?></a></td>
        </tr>
        <?php
       if(!empty($value->insurance_amount)){ 
       ?>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.INSURANCE_AMOUNT')!= '') {{  trans($lang.'.INSURANCE_AMOUNT') }} @else {{ trans($LANUAGE.'.INSURANCE_AMOUNT') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php if(!empty($value->insurance_amount)){ echo  $value->insurance_amount; }  ?></a></td>
        </tr>
      <?php }
       if(!empty($value->quantity)){ 
       ?>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.QUANTITY')!= '') {{  trans($lang.'.QUANTITY') }} @else {{ trans($LANUAGE.'.QUANTITY') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php if(!empty($value->quantity)){ echo  $value->quantity; }  ?></a></td>
        </tr>
      <?php }
       ?>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.TOTAL_PRICE')!= '') {{  trans($lang.'.TOTAL_PRICE') }} @else {{ trans($LANUAGE.'.TOTAL_PRICE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php if(!empty($value->total_price)){ echo  $value->total_price; }  ?></a></td>
        </tr>
        <?php if(!empty($value->product_size))  {?>
        <tr>          
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.SIZES')!= '') {{  trans($lang.'.SIZES') }} @else {{ trans($LANUAGE.'.SIZES') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left"><?php echo  $value->product_size;   ?></a></td>
        </tr>
        <?php }  }  } ?>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">Let us know if you have any further query.</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <?php
  if(isset($OUR_LANGUAGE) && $OUR_LANGUAGE==''){ $OUR_LANGUAGE = 'en'; }else { $OUR_LANGUAGE = 'ar'; }

    ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if (Lang::has(Session::get('lang_file').'.CONTACT_INFORMATION')!= '') { echo  trans(Session::get('lang_file').'.CONTACT_INFORMATION');}  else { echo trans($OUR_LANGUAGE.'.CONTACT_INFORMATION');} ?></title>
</head>

<body>
<table border="0" cellspacing="0" cellpadding="0" width="600" align="center" style="border:1px solid #cccccc;">


  <tr style="background:#f5f5f5;"> 
   <?php 
        $logo = public_path().'/public/assets/default_image/Logo.png'; $logo = $SITE_LOGO;
        if(file_exists($SITE_LOGO))
          $logo = $SITE_LOGO;
        ?>  
          <td align="center"><img src="<?php echo $logo; ?>" alt="Goldencages" style="margin:20px 0px;"/></td>
  </tr>

  <tr>
     <td style="height:30px; text-align:center; font-weight:bold; font-size:16px; font-family:Arial, Helvetica, sans-serif;"><b>

Contact Information
 </b></td>
  </tr>
  <tr>
     <td style=" margin:0 auto; font-size:16px;text-align:left; font-family:Arial, Helvetica, sans-serif; padding:10px 10px 10px;">
        <table  cellspacing="10">
                 <tr>
            <th>Name</th>
             <th>:</th>
              <td >{{ $Ename }}</td>
          </tr>
          <tr>     
          <th>Email Address</th>
             <th>:</th>
              <td >{{$Email}}</td>
            </tr>
           
            <tr>     
          <th>Message</th>
             <th>:</th>
              <td>{{$Message}}</td>
            </tr>
          </table>
     </td>
  </tr>  
<tr>
 
</tr>
  <tr bgcolor="#f5f5f5">
    <td style="height:50px;text-align:center; font-size:14px; background:#f5f5f5;"><a href="#" target="_blank"  style="text-decoration:none;color:#333333;font-weight:800;"> &copy; <?php echo date("Y"); ?>&nbsp; Goldencages </a></td>
  </tr>

 </table>
</body>
</html>

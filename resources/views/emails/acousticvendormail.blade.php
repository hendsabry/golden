<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title>@if (Lang::has($lang.'.MAIL_EMAIL_TEMPLATE')!= '') {{  trans($lang.'.MAIL_EMAIL_TEMPLATE') }} @else {{ trans($LANUAGE.'.MAIL_EMAIL_TEMPLATE') }} @endif</title>
</head>
<body>
<?php 
$SITE_LOGO = 'http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png';
$logo = $SITE_LOGO;
if(file_exists($SITE_LOGO))
$logo = $SITE_LOGO;
$getFbLinks        = Helper::getfblinks();
$gettwitterLinks   = Helper::gettwitterlinks();
$getgoogleLinks    = Helper::getgooglelinks();
$getinstagramLinks = Helper::getinstagramlinks();
$getpinterestLinks = Helper::getpinterestlinks();
$getyoutubeLinks   = Helper::getyoutubelinks();
?>
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right  ">مرحبًا  {{ $vendor_name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373;  text-align:right  ">نشكرا على التعبير عن الاهتمام في خدمتي. يرجى التحقق من الأسعار وغيرها من التفاصيل أدناه.</td>
  </tr>  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right;  ">مشرف</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; text-align:right; ">أقفاص ذهبية</td>
  </tr>
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
	@if($getFbLinks !='')
	<a target="_blank" title="Facebook" href="{{$getFbLinks}}"><img src="{{ url('') }}/themes/images/social-facebook.png" /></a>&nbsp;
	@endif
    @if($gettwitterLinks !='')
	<a target="_blank" title="Twitter" href="{{$gettwitterLinks}}"><img src="{{ url('') }}/themes/images/social-twitter.png" /></a>&nbsp;
	@endif
    @if($getgoogleLinks !='')
	<a target="_blank" title="Google+" href="{{$getgoogleLinks}}"><img src="{{ url('') }}/themes/images/social-googleplus.png" /></a>&nbsp;
	@endif
    @if($getinstagramLinks !='')
	<a target="_blank" title="Instagram" href="{{$getinstagramLinks}}"><img src="{{ url('') }}/themes/images/share-instagram.png" /></a>&nbsp;
	@endif
    @if($getpinterestLinks !='')
	<a target="_blank" title="Pinterest" href="{{$getpinterestLinks}}"><img src="{{ url('') }}/themes/images/social-pint.png" /></a>&nbsp;
	@endif
    @if($getyoutubeLinks !='')
	<a target="_blank" title="Youtube" href=" {{$getyoutubeLinks}}"><img src="{{ url('') }}/themes/images/social-youtube.png" /></a>
	@endif
	</td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">Hi {{ $vendor_name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Thanks for expressing interest in my service. Please check the pricing and other details below.</td>
  </tr>
  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td style="padding-top:20px; padding-bottom:20px"><table border="0" cellpadding="0" cellspacing="0" width="100%">
	    <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.THE_GROOM_NAME')!= '') {{  trans($lang.'.THE_GROOM_NAME') }} @else {{ trans($LANUAGE.'.THE_GROOM_NAME') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $groom_name }}</a></td>
        </tr>
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.HALL')!= '') {{  trans($lang.'.HALL') }} @else {{ trans($LANUAGE.'.HALL') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $hall }}</a></td>
        </tr>
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.MY_ACCOUNT_OCCASIONS')!= '') {{  trans($lang.'.MY_ACCOUNT_OCCASIONS') }} @else {{ trans($LANUAGE.'.MY_ACCOUNT_OCCASIONS') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $occasion_type }}</a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.DATE')!= '') {{  trans($lang.'.DATE') }} @else {{ trans($LANUAGE.'.DATE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $date }}</a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.TIME')!= '') {{  trans($lang.'.TIME') }} @else {{ trans($LANUAGE.'.TIME') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $time }}</a></td>
        </tr>
		
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.RECORDING_SECTION')!= '') {{  trans($lang.'.RECORDING_SECTION') }} @else {{ trans($LANUAGE.'.RECORDING_SECTION') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $recording_section }}</a></td>
        </tr>
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.MUSIC_TYPE')!= '') {{  trans($lang.'.MUSIC_TYPE') }} @else {{ trans($LANUAGE.'.MUSIC_TYPE') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $music_type }}</a></td>
        </tr>
		{{--<tr>--}}
          {{--<td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.SINGER_NAME')!= '') {{  trans($lang.'.SINGER_NAME') }} @else {{ trans($LANUAGE.'.SINGER_NAME') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $singer_name }}</a></td>--}}
        {{--</tr>        --}}
		<tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;  padding-bottom:10px; padding-left:20px;">@if (Lang::has($lang.'.SONG_NAME')!= '') {{  trans($lang.'.SONG_NAME') }} @else {{ trans($LANUAGE.'.SONG_NAME') }} @endif: &nbsp; <a href="javascript:void(0);" style="text-decoration:none;  text-align:left">{{ $song_name }}</a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">Let us know if you have any further query.</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
</body>
</html>

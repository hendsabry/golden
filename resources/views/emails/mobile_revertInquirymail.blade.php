<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title>@if (Lang::has($lang.'.MAIL_EMAIL_TEMPLATE')!= '') {{  trans($lang.'.MAIL_EMAIL_TEMPLATE') }} @else {{ trans($LANUAGE.'.MAIL_EMAIL_TEMPLATE') }} @endif</title>
</head>
<body>
<?php 
// $logo {{ asset('img/logo.png') }}
//$logo = url('/').'assets/img/logo.png'; 
$SITE_LOGO = 'http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png';
$logo = $SITE_LOGO;
if(file_exists($SITE_LOGO))
$logo = $SITE_LOGO;
?>
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right  ">مرحبًا  {{ $name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373;  text-align:right  ">لقد تلقينا رسالتك ونود أن نشكرك على مراسلتنا.</td>
  </tr>  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right;  ">مشرف</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; text-align:right; ">أقفاص ذهبية</td>
  </tr>
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;  "><img src="http://wisitech.in/public/assets/img/icon.png" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">Hi {{ $vendor_name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">We have received your message and would like to thank you for writing to us.</td>
  </tr>
  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr> 
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">We will revert shortly by email on your request.</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
</body>
</html>

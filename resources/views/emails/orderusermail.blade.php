<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>@if (Lang::has($lang.'.MAIL_EMAIL_TEMPLATE')!= '') {{  trans($lang.'.MAIL_EMAIL_TEMPLATE') }} @else {{ trans($LANUAGE.'.MAIL_EMAIL_TEMPLATE') }} @endif</title>
</head>

<body>


<table width="700" border="0" align="center" style="background-color:#fffaee;margin-top:40px;padding-bottom:20px">
  <tbody><tr>
    <td align="center" style="padding-top:40px"><img alt="" src="https://ci5.googleusercontent.com/proxy/WaTlnvJcIdqa7bOj4uV3WZY-9fufp6Q_QcAyOpiiF9njYpkifyBxMNhhLzT1KoCcKOGv9V3FRLq6cryGmm5CfEbVZCoHjAOO3KxjXfLhDvK3cFo6WrsxbeQ=s0-d-e1-ft#http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png" class="CToWUd"></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;font-weight:bold;text-align:right">مرحبًا  {{ $user_name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;text-align:right">لقد استلمنا طلبك ونعمل عليه. سنتواصل معك ونبقيك على علم بذلك. <br>
نحن نتطلع لخدمتك بشكل أفضل.</td>
  </tr>  
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px;font-weight:bold">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;font-weight:bold;text-align:right">مشرف</td>
  </tr>
  
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
  @if($getFbLinks !='')
  <a target="_blank" title="Facebook" href="{{$getFbLinks}}"><img src="https://www.wisitech.in/themes/images/social-facebook.png" /></a>&nbsp;
  @endif
    @if($gettwitterLinks !='')
  <a target="_blank" title="Twitter" href="{{$gettwitterLinks}}"><img src="https://www.wisitech.in/themes/images/social-twitter.png" /></a>&nbsp;
  @endif
    @if($getgoogleLinks !='')
  <a target="_blank" title="Google+" href="{{$getgoogleLinks}}"><img src="https://www.wisitech.in/themes/images/social-googleplus.png" /></a>&nbsp;
  @endif
    @if($getinstagramLinks !='')
  <a target="_blank" title="Instagram" href="{{$getinstagramLinks}}"><img src="https://www.wisitech.in/themes/images/share-instagram.png" /></a>&nbsp;
  @endif
    @if($getpinterestLinks !='')
  <a target="_blank" title="Pinterest" href="{{$getpinterestLinks}}"><img src="https://www.wisitech.in/themes/images/social-pint.png" /></a>&nbsp;
  @endif
    @if($getyoutubeLinks !='')
  <a target="_blank" title="Youtube" href=" {{$getyoutubeLinks}}"><img src="https://www.wisitech.in/themes/images/social-youtube.png" /></a>
  @endif
  </td>
  </tr>
  <!-- social -->
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:12pt;color:#737373;font-weight:bold">Hi {{ $user_name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px">We have received your order and are working on it. We’ll communicate and keep you informed regarding the same.<br>
    We look forward to serve you better.</td>
  </tr> 
  <?php 
  $occasionName = DB::table('nm_business_occasion_type')->where('id',$occasion)->first();
  $occasionDta = DB::table('nm_search_occasion')->where('order_id',$order)->where('user_id',$user_id)->first(); 
  $city = DB::table('nm_city')->where('ci_id',$occasionDta->city_id)->first();
  $orderDta = DB::table('nm_order')->where('order_id',$order)->first(); 
  $product = DB::table('nm_order_product')->where('order_id',$order)->get(); 
  //echo '<pre>';print_r($occasionDta);die;
  $total_sum = 0;
  $total_shipping_charge = 0;
  ?>

  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px"><table width="100%" cellspacing="0" cellpadding="8" border="0" style="border:solid 1px #faf2e1">
      <tbody>
    <?php if(isset($occasionName)) { ?>    
      <tr>
   <td style="padding-top:5px;padding-bottom:5px;font-size:12pt;color:#fff;background:#c29f54;font-weight:bold" colspan="6">Ocassion Searched </td>
        </tr>
      <tr>
        <?php if($occasionDta->main_cat_id == 2) { ?>
        <td width="26%"><strong> Ocassion </strong></td>
        <td width="26%" colspan="2"><?php echo  $occasionName->title; ?></td>
      <?php } else { ?>
    <!--if else condition-->
        <td width="30%"><strong> Type of Business </strong></td>
        <td width="18%" colspan="2"><?php echo $occasionName->title; ?></td>
      <?php } ?>
      </tr>
      <tr>       
        <td><strong> Number of Attendees </strong></td>
        <?php if(isset($occasionDta->total_member) && ($occasionDta->total_member != '' || $occasionDta->total_member)) {?>
        <td colspan="2"><?php echo $occasionDta->total_member; ?></td>
        <?php } else {?>
        <td colspan="2">N/A</td>
        <?php } ?>        
        <td><strong> Budget </strong> </td>
        <?php if(isset($occasionDta->budget) && $occasionDta->budget!='') {?>
        <td colspan="2">SAR <?php echo number_format($occasionDta->budget,2); ?></td>
      <?php } else { ?>
        <td colspan="2">N/A</td>
      <?php } ?>
      </tr>
      <tr>
        <td><strong> City </strong> </td>
        <td colspan="2"><?php echo $city->ci_name; ?></td>
        <td><strong> Ocassion Date </strong> </td>
         <?php if(isset($occasionDta->occasion_date) && $occasionDta->occasion_date != '') {?>
        <td colspan="2"><?php echo  date("j M Y",strtotime($occasionDta->occasion_date)); ?></td>
      <?php } else { ?>
        <td colspan="2">N/A</td>
      <?php } ?>
        
      </tr>
      <tr>
        <td><strong> Gender </strong> </td>
        <?php if(isset($occasionDta->gender) && $occasionDta->gender != '') { ?>
        <td colspan="2"><?php echo $occasionDta->gender; ?></td>
        <?php } else { ?>
          <td colspan="2">N/A</td>
      <?php }  ?>
      </tr>
      <?php if($occasionDta->main_cat_id == 1) { ?>
      <tr>
        <td><strong> Start Date  </strong></td>
        <td colspan="2"><?php echo date("j M Y", strtotime($occasionDta->occasion_date)); ?></td>
        <td><strong> End Date</strong> </td>
        <td colspan="2"><?php echo  date("j M Y", strtotime($occasionDta->end_date)); ?></td>   
      </tr>
     <?php }  } ?> 
    
          <tr>
            <td  colspan="6" height="10"></td>
            </tr>
          <tr>
            <td style="padding-top:5px;padding-bottom:5px;font-size:12pt;color:#fff;background:#c29f54;font-weight:bold" colspan="6">Order Detail</td>
            </tr>
          <tr>
            <td><strong>Order No </strong></td>
            <td colspan="2"><?php if(isset($orderDta->order_id) && $orderDta->order_id!=''){ echo $orderDta->order_id; }?></td>
            <td><strong>Order Date </strong></td>
            <td colspan="2"><?php if(isset($orderDta->order_date) && $orderDta->order_date!=''){ echo date("d M Y", strtotime($orderDta->order_date));} ?></td>
            </tr>
          <tr>
            <td><strong>Payment</strong></td>
              <td colspan="2"><?php if(isset($orderDta->order_paytype) && $orderDta->order_paytype!=''){ echo strtoupper($orderDta->order_paytype);}?></td>
              <td><strong>Transcation Id </strong></td>
              <?php 
			  if($orderDta->order_paytype=='cod' || $orderDta->order_paytype=='COD' || $orderDta->order_paytype=='Wallet') { ?>
              <td colspan="2">N/A</td>
              <?php } else {?>
			  <td colspan="2"><?php echo $orderDta->transaction_id; ?></td>
              <?php } ?>
          </tr>
          <!--<tr>
            <td><strong>VAT Charge </strong></td>
              <td colspan="2">SAR <?php //echo number_format($orderDta->order_taxAmt,2); ?></td>
              <td><strong>Total Order Amt</strong> </td>
              <td colspan="2">SAR <?php //$tlAmt = $orderDta->order_taxAmt; echo number_format($tlAmt,2); ?></td>
          </tr>-->
         <tr>
            <td  colspan="6" height="10"></td>
          </tr>
      <tr>
        <td style="padding-top:5px;padding-bottom:5px;font-size:12pt;color:#fff;background:#c29f54;font-weight:bold" colspan="6"> Services/Product Details  </td>
      </tr>
       <?php
	   //echo '<pre>';print_r($orderDta);die;
	   //echo '<pre>'; print_r($product);
	   if(count($product) > 0){ 
      $discountamount=0; $discountcoupan='';
        foreach ($product as $key => $value) 
		{ 
			$discountamount=$discountamount+$value->coupon_code_amount;
      $discountcoupan=$value->coupon_code;
      $product_sub_type = $value->product_sub_type;
      $product_rent = DB::table('nm_order_product_rent')->where('order_id', '=', $order)->where('product_id',$value->product_id)->first();
			$shop = DB::table('nm_category')->where('mc_id',$value->shop_id)->first();
			//echo '<pre>'; print_r($shop);
$getCShipping = DB::table('nm_shipping')->where('cart_sub_type', '=', $product_sub_type)->where('ship_order_id', '=', $order)->count(); 
          if($getCShipping >=1)
          {
            $getShipping = DB::table('nm_shipping')->where('cart_sub_type', '=', $product_sub_type)->where('ship_order_id', '=', $order)->first(); 
          } 

     		$pro_attr = DB::table('nm_order_product_attribute')->where('order_id',$order)->where('product_id',$value->product_id)->orderBy('attribute_title','ASC')->get();
			$staff = DB::table('nm_order_services_staff')->where('order_id',$order)->where('service_id',$value->product_id)->first();
			$service_attr = DB::table('nm_order_services_attribute')->where('order_id',$order)->where('product_id',$value->product_id)->first();
			$measurment = DB::table('nm_order_body_measurement')->where('order_id',$order)->where('product_id',$value->product_id)->first();
			$optio_value = DB::table('nm_order_option_value')->where('order_id',$order)->where('product_id',$value->product_id)->orderBy('option_value_title','ASC')->get();
			$package = DB::table('nm_order_product_to_package')->select('order_id','product_id','pro_title')->where('order_id',$order)->where('product_id',$value->product_id)->orderBy('pro_title','ASC')->get();
        if($value->product_type == 'car_rental')  
		{
          ?>
          <!-- start Car Rental from here -->
        <tr>
        <td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
        </tr> 
          <tr>
            <td colspan="6" bgcolor="#f7f0e0" style="color:#737373; font-size:13pt; font-weight:bold; text-align:center;">Car Rental</td>
          </tr>
      <?php if(isset($shop) && $shop!='') {?>    
      <tr>
        <td><strong> Agency Name </strong> </td>
        <td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
        <td><strong> Agency Address </strong></td>
        <td><?php if(isset($shop->address) && $shop->address!=''){ echo $shop->address; } ?></td>
        
      </tr> 
      <?php } 
	  $serviceInfo = Helper::getProduckInfo($value->product_id);
	  if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
	  ?> 
	  <tr>
	  <td><strong>Service</strong></td>
	    <td> <?php echo $serviceInfo->pro_title; ?></td>

    <td><strong>No of days</strong></td>
      <td> <?php echo $value->quantity; ?></td>
	  </tr>  
      <tr>
	    
        <td><strong> Car Model </strong> </td>
        <?php 
		$get_car_model = Helper::getProductName($value->product_id,$value->merchant_id,'Car Model');
		if(isset($get_car_model->value) && $get_car_model->value!=''){
		?>
        <td colspan="2"><?php echo $get_car_model->value; ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
        <td><strong> Price per day </strong></td>
        <?php if(isset($getprice) && $getprice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
      </tr>
    <?php 
     if(isset($product_rent)) {  ?>
      <tr> 
        <td><strong> Booking date </strong></td>
        <td colspan="2"><?php echo  date("j M Y", strtotime($product_rent->rental_date)); ?></td>
        <td><strong> Return Date </strong></td>
        <td colspan="2"><?php echo  date("j M Y", strtotime($product_rent->return_date)); ?></td>
      </tr>
    <?php } ?>
    <tr>
       <td><strong>Currency</strong></td>
      <td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
      <td><strong>Total Amount </strong></td>
      <td colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
    </tr>
    <!-- end Car Rental from here -->
<?php  
} 

//echo '<pre>';print_r($value);die;
if($value->product_type == 'travel'){ 

$serviceInfo = Helper::getProduckInfo($value->product_id);
    if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
  ?>
<!-- Start Travel from here -->
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Travel </strong></td>
</tr>   
<tr>
    <td><strong> Agency Name </strong> </td>
    <td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
    <td><strong> Agency Address </strong></td>
    <td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
    <?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr> 
<?php 
$serviceInfo = Helper::getProduckInfo($value->product_id);
if(isset($serviceInfo) && $serviceInfo!='') { ?>
<tr>
<td><strong> Package </strong></td> 
<?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!= '') {?>
<td colspan="2"><?php echo $serviceInfo->pro_title; ?></td>
<?php } else  { ?>
<td colspan="2">N/A</td>
<?php } ?>

<td><strong> Location </strong></td>
<?php 
$get_location = Helper::getProductName($value->product_id,$value->merchant_id,'Location'); if(isset($get_location->value) && $get_location->value!=''){
?>
<td colspan="2"><?php echo $get_location->value; ?></td>
<?php } else  { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Extra Service </strong></td>
<?php 
$get_car_model = Helper::getProductName($value->product_id,$value->merchant_id,'Extra Service');
if(isset($get_car_model->value) && $get_car_model->value!=''){?>
<td  colspan="2"><?php echo $get_car_model->value; ?></td>
<?php } else  { ?>
<td colspan="2">N/A</td>
<?php } ?>

<td><strong>Price</strong></td>
<?php if(isset($getprice) && $getprice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>

</tr> 
<?php } ?>   
<tr>
<td><strong>Package Date</strong></td>
<?php if(date("j M Y", strtotime($product_rent->rental_date)) != '') { ?>
<td colspan="2"><?php echo  date("j M Y", strtotime($product_rent->rental_date)); ?></td>
<?php } else { ?>
  <td colspan="2">N/A</td>
<?php } ?>
<td><strong>No of People</strong> </td>
<td colspan="2"><?php echo  $value->quantity; ?></td>
</tr>
<tr>
 <td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- End Travel from here -->


<!-- start clinic from here -->
<?php } 
if($value->product_type == 'clinic')
{ 
  $serviceInfo = Helper::getProduckInfo($value->product_id);
  $department_name = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
?>
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Clinic </strong></td>
</tr>   
<tr>
<td><strong> Clinic Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Clinic Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>
<tr>
<td><strong> Dr. Name </strong></td>
<td colspan="2"><?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){ echo $serviceInfo->pro_title; ?></td>
  
<td><strong> Department </strong></td>
<?php if(isset($department_name->attribute_title) && $department_name->attribute_title!=''){ ?>
<td colspan="2"><?php echo $department_name->attribute_title; ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>   
<tr>
<td><strong>Booking Date</strong></td>
<td colspan="2"><?php echo  date("j M Y", strtotime($staff->booking_date)); ?></td>
<td><strong>Booking Time </strong></td>
<td  colspan="2"><?php echo  date("H:i A", strtotime($staff->start_time)); ?></td>
</tr>

<tr>

<td><strong>Patient </strong></td>
<?php if($staff->file_no != '')  {?>
<td  colspan="2"><?php echo  'Existing Patient'; ?></td>
<?php } else  {?>
<td  colspan="2"><?php echo  'New Patient'; ?></td>
<?php } ?>
<td><strong>File No. </strong></td>
<?php if($staff->file_no != '')  {?>
<td  colspan="2"><?php echo  $staff->file_no; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php }  }?>
</tr>
<tr>
 <td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>

<!-- end clinic from here -->
<?php } 

if($value->product_sub_type == 'beauty_centers' || $value->product_sub_type == 'spa' || $value->product_sub_type == 'makeup_artists' || $value->product_sub_type == 'men_saloon')   { 
  ?>
<!-- Start Beauty Center from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<?php if($value->product_sub_type == 'beauty_centers') {?> 
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Beauty Center </strong></td>
</tr> 
<?php } 
elseif($value->product_sub_type == 'spa') {
?>
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Spa </strong></td>
</tr>
<?php } 
elseif($value->product_sub_type == 'makeup_artists') {
?>
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Makeup Artist </strong></td>
</tr>
<?php } 
elseif($value->product_sub_type == 'men_saloon') {
?>
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Barber </strong></td>
</tr>
<?php } ?>  
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){  ?><td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>
<tr>
<td><strong> Service </strong></td>
<td colspan="2"><?php if(isset($value->pro_title) && $value->pro_title!=''){echo $value->pro_title;}?></td>
<?php
if(isset($staff->id) && $staff->id!='')
{
    $service_charge = Helper::getshopname($staff->shop_id);
	$serviceInfo = Helper::getProduckInfo($staff->service_id);
	$staffname = Helper::getStaffNameOne($staff->staff_id);
	$getAddresInfo = Helper::getuserinfo($staff->cus_id);
?>
<td><strong> Duration </strong></td>
<?php if(isset($serviceInfo->service_hour) && $serviceInfo->service_hour!=''){ ?>
<td colspan="2"><?php echo $serviceInfo->service_hour. ' hr'; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>   
<tr>
<td><strong>Location Preferred </strong></td>
<?php if(isset($staff->booking_place) && $staff->booking_place!= ''){?>
<td colspan="2"><?php echo ucfirst($staff->booking_place);?></td>
<?php }else{?>
<td colspan="2"> N/A</td>
<?php } ?>

<td><strong>Staff Name </strong></td>
<?php if(isset($staffname->staff_member_name) && $staffname->staff_member_name!=''){?>
<td  colspan="2"><?php echo $staffname->staff_member_name; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>

</tr>
<tr>
<td><strong>Booking Date </strong></td>
<?php if(isset($staff->booking_date) && $staff->booking_date != '')  {?>
<td  colspan="2"><?php echo  date("d M Y", strtotime($staff->booking_date)); ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>

<td><strong>Booking Time</strong></td>
<?php if(isset($staff->start_time) && $staff->start_time != '')  {?>
<td  colspan="2"><?php echo $staff->start_time; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } }?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency; } ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- end Beauty Center from here -->


 <?php } 
 
if($value->product_sub_type == 'makeup')  {
$serviceInfo = Helper::getProduckInfo($value->product_id);
    if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}

 ?>
<!-- Start Make up from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Makeup </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>
<tr>
<td><strong> Product Name </strong></td>
<td colspan="2"><?php echo  $value->pro_title; ?></td>
<td><strong> Product Price </strong></td>
<?php if(isset($getprice) && $getprice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
</tr>   
<tr>
<td><strong>Product Qty </strong></td>
<td colspan="2"><?php echo  $value->quantity; ?></td>
<td><strong>Product Image </strong></td>
<td  colspan="2"><img src="<?php echo  $value->pro_Img; ?>" width="80" /></td>
</tr>

<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>

<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!='') echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; ?></strong></td>
</tr>
 <!-- end Make up from here  -->

<?php } 
 
if($value->product_sub_type == 'tailor') 
{ 
 $serviceInfo = Helper::getProduckInfo($value->product_id);
 if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
?>
<!-- Start Tailor from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Tailor </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>  
<tr>
<td><strong>Design </strong></td>
<td colspan="2"><?php echo $serviceInfo->pro_title; ?></td>
<td><strong>Product Image </strong></td>
<td  colspan="2"><img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" /></td>
</tr>
<tr>
  <td><strong>Qty</strong></td>
  <td colspan="2"><?php echo  $value->quantity; ?></td>
  <td><strong></strong></td>
  <td  colspan="2"></td>
</tr>
<?php if(isset($measurment)) {?>
<tr>
  <td colspan="6"  height="5"></td>
</tr>
<tr>
  <td colspan="6" style="padding-left:10px; color:#c29f54;"><strong>Body Measurement </strong></td>
  </tr>
<tr>
  <td colspan="6"><table width="100%" border="0" cellspacing="0" cellpadding="8">
    
    <tr>
      <td width="22%"><strong>Length</strong></td>
      <td width="16%"><?php echo  $measurment->length . ' CM'; ?></td>
      <td width="15%"><strong>Chest Size </strong></td>
      <td width="13%"><?php echo  $measurment->chest_size. ' CM'; ?></td>
      <td width="20%"><strong>Waist Size </strong></td>
      <td width="14%"><?php echo  $measurment->waistsize. ' CM'; ?> </td>
    </tr>
    <tr>
      <td><strong>Shoulders</strong></td>
      <td><?php echo  $measurment->soulders. ' CM'; ?> </td>
      <td><strong>Neck</strong></td>
      <td><?php echo  $measurment->neck. ' CM'; ?> </td>
      <td><strong>Arm Length </strong></td>
      <td><?php echo  $measurment->arm_length. ' CM'; ?></td>
    </tr>
    <tr>
      <td><strong>Wrist Diameter </strong></td>
      <td><?php echo  $measurment->wrist_diameter. ' CM'; ?></td>
      <td><strong>Cutting</strong></td>
      <?php if($measurment->cutting != '')  {?>
      <td><?php echo  $measurment->cutting; ?></td>
      <?php } else  {?>
      <td> N/A</td>
      <?php } ?>      
      <td><strong>Fabric</strong></td>
      <td><?php echo  $value->fabric_name; ?></td>
    </tr>
  </table></td>
  </tr>
<?php } ?>

<tr>
<td><strong> Fabric Price </strong></td>
<?php if($value->fabric_price != '')  {?>
<td colspan="2"><?php echo  $value->fabric_price; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Fabric Image </strong></td>
<?php if($value->fabric_img != '')  {?>
<td  colspan="2"><img src="<?php echo  $value->fabric_img; ?>" width="80" /></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo  $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php
 $total_shipping_charge = $total_shipping_charge + $getShippings;
 if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price;} ?></strong></td>
</tr>
 <!-- end Tailor from here  --> 

<?php } 
 
if($value->product_sub_type == 'dress'){ 
$serviceInfo = Helper::getProduckInfo($value->product_id);
if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
?>
<!-- Start Dress from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Dresses </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>  

<tr>
<td><strong> Dress </strong></td>
<td colspan="2"><?php echo $serviceInfo->pro_title; ?></td>
<td><strong> Size </strong></td>
<td colspan="2"><?php if(isset($value->product_size) && $value->product_size!=''){ $size = str_replace('Size','',$value->product_size); echo $size; }else{echo 'N/A';}?></td>
</tr>   
<tr>
  <td><strong>Qty</strong></td>
  <td colspan="2"><?php echo  $value->quantity; ?></td>
  <td><strong></strong></td>
  <td  colspan="2"></td>
</tr>
   
<tr>
  <td><strong>For  </strong></td>
  <td colspan="2"><?php echo ucfirst($value->buy_rent); ?></td>
  <?php if(isset($value->buy_rent) && $value->buy_rent== 'rent') { ?>
  <td><strong>Rent Charge/Day </strong></td>
  <td colspan="2">SAR <?php echo $getPrice; ?>    
  </td>
<?php } else {?>
  <td><strong>Price</strong></td>
  <td  colspan="2">SAR <?php echo $getPrice; ?> </td>
<?php } ?>
</tr>
<tr>
<td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" /></td>
<?php
 if(isset($value->buy_rent) && $value->buy_rent== 'rent') {
 if(isset($value->insurance_amount) && $value->insurance_amount != '') { ?>
<td><strong>Insurance Amount</strong></td>
<td colspan="2">SAR <?php echo number_format($value->insurance_amount,2); ?></td>
<?php } } ?>
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php echo  $value->currency; ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price;} ?></strong></td>
</tr>
<!-- end Dress from here  -->
<?php } 
 
if($value->product_sub_type == 'abaya')  { 
 $serviceInfo = Helper::getProduckInfo($value->product_id);
 if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
  ?>
<!-- Start Abaya from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Abaya </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr> 
<!-- If Condion -->
<tr>
  <td><strong>Design</strong></td>
  <td colspan="2"><?php echo  $value->pro_title; ?></td>
  <td><strong>Image</strong></td>
  <td colspan="2"><img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" /></td>
</tr>

<?php if(isset($value->product_size)) {?>
<tr>
   <td align="left"><strong>Size</strong></td>
  <td  colspan="2"><?php echo  $value->product_size; ?></td>  
  <td align="left"><strong>Quantity</strong></td>
  <td  colspan="2"><?php echo  $value->quantity; ?></td>
</tr>
<?php } else{?>
<tr>
  <td><strong>Quantity</strong></td>
  <td colspan="2"><?php echo $value->quantity; ?></td>
</tr>
<?php } ?>
<tr>
  <td><strong>Price</strong></td>
  <?php if(isset($getPrice) && $getPrice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getPrice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
  <td align="left">&nbsp;</td>
  <td  colspan="2">&nbsp;</td>
</tr>
<?php if(isset($measurment)) {?>
<tr>
  <td colspan="6"  height="5"></td>
</tr>
<tr>
  <td colspan="6" style="padding-left:10px; color:#c29f54;"><strong>Body Design Measurment </strong></td>
  </tr>
<tr>
  <td colspan="6"><table width="100%" border="0" cellspacing="0" cellpadding="8">
    
    <tr>
      <td width="22%"><strong>Length</strong></td>
      <td width="16%"><?php echo  $measurment->length . ' CM'; ?></td>
      <td width="15%"><strong>Chest Size </strong></td>
      <td width="13%"><?php echo  $measurment->chest_size. ' CM'; ?></td>
      <td width="20%"><strong>Waist Size </strong></td>
      <td width="14%"><?php echo  $measurment->waistsize. ' CM'; ?> </td>
    </tr>
    <tr>
      <td><strong>Shoulders</strong></td>
      <td><?php echo  $measurment->soulders. ' CM'; ?> </td>
      <td><strong>Neck</strong></td>
      <td><?php echo  $measurment->neck. ' CM'; ?> </td>
      <td><strong>Arm Length </strong></td>
      <td><?php echo  $measurment->arm_length. ' CM'; ?></td>
    </tr>
    <tr>
      <td><strong>Wrist Diameter </strong></td>
      <td><?php echo  $measurment->wrist_diameter. ' CM'; ?></td>
      <td><strong>Cutting</strong></td>
      <?php if($measurment->cutting != '')  {?>
      <td><?php echo  $measurment->cutting; ?></td>
      <?php } else  {?>
      <td> N/A</td>
      <?php } ?>  
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table></td>
  </tr>
<?php } ?>
<tr>
<td><strong> Fabric Price </strong></td>
<?php if($value->fabric_price != '')  {?>
<td colspan="2"><?php echo  $value->fabric_price; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Fabric Image </strong></td>
<?php if($value->fabric_img != '')  {?>
<td  colspan="2"><img src="<?php echo  $value->fabric_img; ?>" width="80" /></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>

<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){echo $value->currency;}?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php
 $total_shipping_charge = $total_shipping_charge + $getShippings;
 if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price;} ?></strong></td>
</tr>
 <!-- end Abaya from here  --> 
 <?php } 
 
if($value->product_sub_type == 'perfume')  { 
$serviceInfo = Helper::getProduckInfo($value->product_id);
    if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
  ?>
<!-- Start Perfumes from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Perfumes </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php  if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>  

<tr>
<td><strong> Product Name </strong></td>
<td colspan="2"><?php echo $value->pro_title; ?></td>
<td><strong>Qty</strong></td>
  <td colspan="2"><?php echo $value->quantity; ?></td>
</tr>   
<tr>  
  
  <td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo  $value->pro_Img; ?>" width="80" /></td>
 <td><strong> Price  </strong></td>

<?php if(isset($getprice) && $getprice!= '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; }?></strong></td>
</tr>
<!-- end Perfumes from here  -->
<?php } 
 
if($value->product_sub_type == 'gold')  { 
$serviceInfo = Helper::getProduckInfo($value->product_id);
	  if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
  ?>
<!-- Start Jewellery from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Jewellery </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>  

<tr>
<td><strong> Product Name </strong></td>
<td colspan="2"><?php if(isset($value->pro_title) && $value->pro_title!=''){ echo $value->pro_title;} ?></td>
<td><strong>Qty</strong></td>
<?php   
if(isset($value->quantity) && $value->quantity != ''){?>
<td colspan="2"><?php echo $value->quantity; ?></td>
<?php } else{ ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>   
<tr>  
  <td><strong>Price</strong></td>
 
<?php if(isset($getprice) && $getprice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
  <td><strong> Image  </strong></td>
<td colspan="2"><?php if(isset($value->pro_Img) && $value->pro_Img!=''){ ?><img src="<?php echo $value->pro_Img; ?>" width="80" /><?php } ?></td>
</tr>
<?php if(count($pro_attr) > 0){ ?>   
  <tr>
    <td><strong> Caliber </strong> </td>
    <td colspan="2"><?php if(isset($pro_attr[0]->value) && $pro_attr[0]->value!=''){ echo $pro_attr[0]->value.' gm';} ?></td>
    <td><strong> Weight </strong></td>
    <td colspan="2"><?php if(isset($pro_attr[1]->value) && $pro_attr[1]->value!=''){ echo $pro_attr[1]->value.' gm';} ?></td>
  </tr>
<?php   } 
if(isset($value->product_size) && $value->product_size != ''){?>
<tr>
  <td><strong>Name on Title </strong></td>
  <td colspan="2"><?php if(isset($value->product_size) && $value->product_size!=''){ echo $value->product_size; } else{echo 'N/A';}?></td>
  <td align="right">&nbsp;</td>
  <td  colspan="2">&nbsp;</td>
</tr>
<?php } ?>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2); $total_sum = $total_sum+$value->total_price;} ?></strong></td>
</tr>
<!-- end Jewellery from here  -->
<?php } 
 
if($value->product_sub_type == 'cosha')  {
  $serviceInfo = Helper::getProduckInfo($value->product_id);
	  if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}

  if($value->showcartproduct==0){
 ?>
<!-- Start Kosha from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Kosha </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; }else{echo 'N/A';} ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr> 
<?php if(isset($service_attr) && $service_attr!='') {?> 
<tr>
<td><strong>Product Name</strong></td>
<td colspan="2"><?php echo  $value->pro_title; ?></td>
<td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo  $value->pro_Img; ?>" width="80" /></td>
</tr>   
<tr> 
<td><strong>Price</strong></td>
<?php if(isset($getprice) && $getprice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
<td align="right"><strong>Quantity</strong></td>
<td  colspan="2">
<?php if(isset($value->quantity) && $value->quantity>0){ echo $value->quantity;} else{ echo "N/A";} ?>
</td>
</tr>

<tr> 
<td><strong>Insurance</strong></td>
<td  colspan="2">
<?php 

if($value->buy_rent=='design'){
  $newinsurance=$value->quantity*$value->insurance_amount;
if(isset($value->insurance_amount) && $value->insurance_amount>0){ echo 'SAR '.number_format($newinsurance,2);} else{ echo "N/A";}

}else{
if(isset($value->insurance_amount) && $value->insurance_amount>0){ echo 'SAR '.number_format($value->insurance_amount,2);} else{ echo "N/A";}
}

 ?>
</td>

<td align="right">&nbsp;</td>
<td  colspan="2">&nbsp;</td>
</tr>


<?php } else {
 $cosha_attr = DB::table('nm_order_services_attribute')->where('order_id',$order)->where('category_id',$value->category_id)->get();
 if(count($cosha_attr) >0) 
 {
  foreach($cosha_attr as $cosha) 
  {   
?>
<tr>
<td><strong> Product Type </strong></td>
<td colspan="2"><?php if(isset($cosha->sub_attribute_title) && $cosha->sub_attribute_title!=''){ echo $cosha->sub_attribute_title;}else{echo 'N/A';}?></td>
<td><strong>Product Name</strong></td>
<td colspan="2"><?php if(isset($cosha->pro_title) && $cosha->pro_title!=''){echo $cosha->pro_title;}else{echo 'N/A';} ?></td>
</tr>   
<tr>  
<td><strong>Price</strong></td>
<td  colspan="2">SAR <?php if(isset($cosha->price) && $cosha->price!=''){ echo number_format($cosha->price,2); }else{echo 'N/A';} ?> </td>
<td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo @$cosha->pro_img; ?>" width="80" /></td>
</tr>
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<?php } } }?>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;}else{echo 'N/A';}  ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!='') echo number_format($value->total_price,2);
$total_sum = $total_sum+$value->total_price;
 ?></strong></td>
</tr>
<!-- end Kosha from here  -->
 <?php } }
 
if($value->product_sub_type == 'video' || $value->product_sub_type == 'photography')  { 
$otherhospitality = DB::table('nm_order_reception_hospitality')->where('order_id', '=', $order)->first();
 $serviceInfo = Helper::getProduckInfo($value->product_id);
    if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
  ?>
<!-- Start Photography  from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Photography </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; }else{echo 'N/A';}  ?></td>
<td><strong>Shop Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>  

<tr>
<?php 
if(isset($service_attr) && $service_attr!=''){
$department_name = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
  ?>   
<td><strong> For </strong></td>
<td colspan="2"><?php if(isset($department_name->attribute_title) && $department_name->attribute_title!=''){echo $department_name->attribute_title; }else{echo 'N/A';} ?></td>
<?php } ?>
<td><strong>Package</strong></td>
  <td colspan="2"><?php if(isset($value->pro_title) && $value->pro_title!=''){ echo $value->pro_title; }else{echo 'N/A';}  ?></td>
</tr> 

<?php if(isset($otherhospitality) && $otherhospitality!=''){?>
<tr>
   
<td><strong> Date / Time </strong></td>
<td colspan="2"><?php if(isset($otherhospitality->date) && $otherhospitality->date!=''){echo $otherhospitality->date.' / '. $otherhospitality->time; }else{echo 'N/A';} ?></td>


<?php 
$extraInfo  = Helper::getExtraInfo($value->product_id,$order,$value->cus_id);
?>
<td><strong>Location</strong></td>
  <td colspan="2"><?php if(isset($extraInfo->elocation) && $extraInfo->elocation!=''){ echo $extraInfo->elocation; }else{echo 'N/A';}  ?></td>
</tr> 
<?php } ?>




<?php if(isset($service_attr->attribute_title) && $service_attr->attribute_title == 'Videography')
{ 
  if(isset($service_attr) && $service_attr!='')
  {
  ?>   
  <tr>
    <td><strong> Number of Cameras </strong> </td>
    <td colspan="2"><?php if(isset($pro_attr[1]->value) && $pro_attr[1]->value!=''){ echo $pro_attr[1]->value; }else{echo 'N/A';} ?></td>

    <td><strong> Duration of Video(hr) </strong></td>
    <td colspan="2"><?php if(isset($pro_attr[0]->value) && $pro_attr[0]->value!=''){ echo $pro_attr[0]->value; }else{echo 'N/A';} ?></td>
  </tr>
<?php   }  } else { 
  if(count($pro_attr) >0){
?> 
  <tr>
    <td><strong> Number of Cameras </strong> </td>
    <td colspan="2"><?php if(isset($pro_attr[0]->value) && $pro_attr[0]->value!=''){ echo $pro_attr[0]->value; }else{echo 'N/A';} ?></td>

    <td><strong> Number of Pic </strong></td>
    <td colspan="2"><?php if(isset($pro_attr[1]->value) && $pro_attr[1]->value!=''){ echo $pro_attr[1]->value; }else{echo 'N/A';} ?></td>
  </tr>
<?php } }?> 
<tr>
<td><strong>Name</strong></td>
<td colspan="2"><?php if(isset($value->pro_title) && $value->pro_title!=''){echo $value->pro_title;}else{echo 'N/A';} ?></td>
<td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo $value->pro_Img; ?>" width="80" /></td>
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;}else{echo 'N/A';}  ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- end Photography  from here  -->
<?php } 
//if(isset($value->rose_package_type) && ($value->rose_package_type == 'package' || $value->rose_package_type == 'single')) 
if(isset($value->product_sub_type) && $value->product_sub_type == 'roses') {
$serviceInfo   = Helper::getProduckInfo($value->product_id);
$getPackage    = Helper::getPackage($value->shop_id,$value->product_id);	
if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
?>
<!-- Start Roses from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Roses </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; }else{echo 'N/A';}  ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; }else{echo 'N/A';}  ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>
<tr>
<td><strong>Name</strong></td>
<td colspan="2"><?php if(isset($value->pro_title) && $value->pro_title!=''){echo $value->pro_title;}else{echo 'N/A';} ?></td>
<td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo $value->pro_Img; ?>" width="80" /></td>
</tr>
<tr> 
<?php if(count($package) >0){?>
<td><strong>Flowers</strong></td>
<td colspan="2">
<?php foreach($package as $pac) {?>
<li><?php if(isset($pac->pro_title) && $pac->pro_title!=''){echo $pac->pro_title; }?></li><br>
<?php }?> 
</td>
<?php } ?>
</tr>
<?php if(isset($serviceInfo->packege) && $serviceInfo->packege=='yes'){ ?>
<tr>
<td>For</td>
<td colspan="2">Package</td>
</tr>
<?php } else{?>
<tr>
<td>For</td>
<td colspan="2">Single</td>
</tr>
<?php } 
if(count($getPackage) >0){
?>
<tr>
<td>Flower Type</td>
<td colspan="2"><?php foreach($getPackage as $pdata){ $getName = Helper::getProduckInfo($pdata->item_id); echo $getName->pro_title.','; } ?></td>
</tr>
<?php } 
if(isset($serviceInfo->packege) && $serviceInfo->packege=='yes')
{
	$wrappingType        = Helper::setExtraProductField($value->product_id,$value->merchant_id,$option_id=25);
	$wrappingDesign      = Helper::setExtraProductField($value->product_id,$value->merchant_id,$option_id=26);
?>
<tr>
<td>Wrapping Type</td>
<td colspan="2"><?php if(isset($wrappingType->option_title) && $wrappingType->option_title!=''){echo $wrappingType->option_title.': SAR '.number_format($wrappingType->price,2);}else{echo 'N/A';} ?></td>
<td>Wrapping Design</td>
<td  colspan="2"><?php if(isset($wrappingDesign->option_title) && $wrappingDesign->option_title!=''){echo $wrappingDesign->option_title.': SAR '.number_format($wrappingDesign->price,2);}else{echo 'N/A';} ?></td>
</tr>

<?php 
} 
if(isset($serviceInfo->packege) && $serviceInfo->packege=='no')
{
	$wrappingTypeOrder = Helper::setMorePackage($value->order_id,$value->product_id,$option_id=25);
	$wrappingDesignOrder = Helper::setMorePackage($value->order_id,$value->product_id,$option_id=26);
?>
<tr>
<td>Wrapping Type</td>
<td colspan="2"><?php echo @$wrappingTypeOrder->option_title.': SAR '.number_format(@$wrappingTypeOrder->value,2); ?></td>
<td>Wrapping Design</td>
<td  colspan="2"><?php echo @$wrappingTypeOrder->option_title.': SAR '.number_format(@$wrappingDesignOrder->value,2); ?></td>
</tr>
<?php } ?>
<tr>
  <td><strong>Price</strong></td>
  <td  colspan="2">SAR <?php echo number_format($getprice,2); ?>  </td>
  <td><strong>Qunatity</strong></td>
  <?php if(isset($value->quantity) && $value->quantity!=''){?>
  <td colspan="2"><?php echo $value->quantity; ?></td>
 <?php } else {?>
  <td colspan="2">N/A</td>
 <?php } ?>
  
</tr>
<?php
$a = 0;
if(count($optio_value) >0) 
{
  foreach($optio_value as $val) 
  {
    if($a % 2 == 0) 
	{?>   
<tr>
  <td><strong><?php if(isset($val->option_value_title) && $val->option_value_title!=''){echo $val->option_value_title; }else{echo 'N/A';}?></strong></td>
  <td colspan="2"><?php if(isset($val->option_value_title) && $val->option_value_title!=''){ echo $val->option_value_title;}else{echo 'N/A';} ?></td>
  <?php } else {?> 
  <td><strong><?php if(isset($val->option_value_title) && $val->option_value_title!=''){echo $val->option_value_title;}else{echo 'N/A';} ?>  </strong></td>
  <td colspan="2"><?php if(isset($val->option_value_title) && $val->option_value_title!=''){ echo $val->option_value_title;}else{echo 'N/A';} ?></td>
</tr>
<?php } $a++; 
} } ?>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price;} ?></strong></td>
</tr>
<!-- end Roses from here  -->
<?php } 
 
if($value->product_sub_type == 'events')  {
$serviceInfo = Helper::getProduckInfo($value->product_id);
	  if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
 ?>
<!-- Start Special Event from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Special Event </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>
<tr>
<td><strong>Product Name</strong></td>
<td colspan="2"><?php echo $value->pro_title; ?></td>
<td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo $value->pro_Img; ?>" width="80" /></td>
</tr>   
<tr> 
<td><strong>Price</strong></td>
<?php if(isset($getprice) && $getprice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>
<td><strong>Quantity</strong></td>
<td  colspan="2"><?php if(isset($value->quantity) && ($value->quantity!='' || $value->quantity!='0')){ echo $value->quantity;}else{echo '0';} ?></td>
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price;} ?></strong></td>
</tr>
<!-- end Special Event from here  -->
<?php } 
 
if($value->product_sub_type == 'hospitality')  { 
$serviceInfo = Helper::getProduckInfo($value->product_id);
	  if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
  ?>
<!-- Start Hospitality from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Hospitality </strong></td>
</tr>   
<tr>
<td><strong> Shop Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr> 

<?php if(isset($service_attr) && $service_attr!='') {?> 
<tr>
<td><strong>Product Name</strong></td>
<td colspan="2"><?php if(isset($value->pro_title) && $value->pro_title!=''){ echo $value->pro_title;}else{echo 'N/A';} ?></td>
<td><strong> Image  </strong></td>
<td colspan="2"><img src="<?php echo $value->pro_Img; ?>" width="80" /></td>
</tr> 
<tr> 
<?php if(count($package) >0)  {?>
<td><strong>Package</strong></td>
<td colspan="2">
<?php foreach($package as $pac){ ?>
<li><?php if(isset($pac->pro_title) && $pac->pro_title!=''){ echo $pac->pro_title; }else{echo 'N/A';}?></li><br>
<?php }?> 
</td>
<?php }?>
<td><strong>Price</strong></td>
<td  colspan="2">SAR <?php number_format($getprice,2) ?> </td>
</tr>
<?php } else {
 $hos_attr = DB::table('nm_order_services_attribute')->where('order_id',$order)->where('category_id',$value->category_id)->get();
 $hospitality = DB::table('nm_order_reception_hospitality')->where('order_id', '=', $order)->where('category_id', '=', $value->category_id)
        ->LeftJoin('nm_country', 'nm_country.co_id', '=', 'nm_order_reception_hospitality.nationality')
        ->first(); 
 if(count($hos_attr) >0) { 

  ?>
<tr>
<td><strong>Location</strong></td>
<td colspan="2"><?php if(isset($extraInfo->elocation) && $extraInfo->elocation!=''){ echo $extraInfo->elocation; }else{echo 'N/A';}  ?></td>
<td><strong></strong></td>
<td colspan="2"></td>
</tr>  

<?php  foreach($hos_attr as $hos) {   
?>
<tr>
<td><strong> Product Type </strong></td>
<?php if(isset($hos->sub_attribute_title) && $hos->sub_attribute_title != '') { ?>
<td colspan="2"><?php echo $hos->sub_attribute_title; ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>

<td><strong>Product Name</strong></td>
<?php if(isset($hos->pro_title) && $hos->pro_title != '') { ?>
<td colspan="2"><?php echo  $hos->pro_title; ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>   
<tr>  
<td><strong>Price</strong></td>
<?php if(isset($hos->price) && $hos->price != '') { ?>
<td  colspan="2">SAR <?php echo number_format($hos->price,2); ?> </td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>

<td><strong> Image  </strong></td>
<?php if(isset($hos->pro_img) && $hos->pro_img != '') { ?>
<td colspan="2"><img src="<?php echo  $hos->pro_img; ?>" width="80" /></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>

</tr>
<?php
  $extraInfo  = Helper::getExtraInfo($value->product_id,$order,$value->cus_id);
  ?>
<tr>
<td><strong>Location</strong></td>
<td colspan="2"><?php if(isset($extraInfo->elocation) && $extraInfo->elocation!=''){ echo $extraInfo->elocation; }else{echo 'N/A';}  ?></td>
<td><strong></strong></td>
<td colspan="2"></td>
</tr>  

<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<?php } } 
if(isset($hospitality) && $hospitality!='') {?>
<tr>
<td><strong> Number of Staff </strong></td>
<td colspan="2"><?php if(isset($hospitality->no_of_staff) && $hospitality->no_of_staff!=''){ echo $hospitality->no_of_staff; }else{echo 'N/A';}?></td>
<td><strong>Nationality</strong></td>
<td colspan="2"><?php if(isset($hospitality->co_name) && $hospitality->co_name!=''){ echo $hospitality->co_name;}else{echo 'N/A';} ?></td>
</tr>
<tr>
<td><strong> Date </strong></td>
<td  colspan="2"><?php echo date("j M Y", strtotime($hospitality->date)); ?></td>
<td><strong> Time</strong></td>
<td  colspan="2"><?php echo date("H:i A", strtotime($hospitality->time)); ?></td>
</tr>
<?php } }?>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($getCShipping >=1)  {?>
<td colspan="2"><?php if($getShipping->shipping_type == 1){ echo "Aramex"; }else { echo "Pick Logistics"; }; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($getCShipping >=1)  { $getShippings = $getShipping->shipping_price;  ?>
<td  colspan="2"><strong>SAR <?php echo  $getShipping->shipping_price; ?></strong></td>
<?php } else  { $getShippings = 0; ?>
<td  colspan="2"> N/A</td>
<?php } ?>
</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php 
$total_shipping_charge = $total_shipping_charge + $getShippings;
if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price;} ?></strong></td>
</tr>
<!-- end Hospitality from here  -->
<?php } 
if($value->product_sub_type == 'invitations')  
{ 
  $invite = DB::table('nm_order_invitation')->where('order_id',$order)->first();
  ?>

<!-- Start Electronic Invitaion Card  from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Electronic Invitaion Card  </strong></td>
</tr>   
<tr>
<td><strong> Occasion Name </strong> </td>
<td colspan="2"><?php if(isset($invite->occasion_name) && $invite->occasion_name!=''){ echo $invite->occasion_name;} ?></td>
<td><strong>Occasion Type</strong></td>
<td>
<?php 
if(isset($invite->occasion_type) && $invite->occasion_type!='')
{
 $get_cat_name = Helper::business_occasion_type($invite->services_id); 
  if(isset($get_cat_name->title) && $get_cat_name->title!=''){echo $get_cat_name->title;}
}
?>
</td>
<td></td>
</tr>
<tr>
<td><strong>Number of Invitees</strong></td>
<td colspan="2"><?php if(isset($invite->total_member_invitation) && $invite->total_member_invitation!=''){ echo $invite->total_member_invitation;} ?></td>
<td><strong> Venue </strong></td>
<td colspan="2"><?php if(isset($invite->venue) && $invite->venue!=''){ echo $invite->venue; } ?></td>
</tr>   
<tr> 
<td><strong>Invitaion Type</strong></td>
<?php 
if(isset($invite->invitaion_mode) && $invite->invitaion_mode == 1) {?>
<td  colspan="2"><?php echo  'Text Message'; ?> </td>
<td><strong>Message</strong></td>
<td  colspan="2"><?php if(isset($invite->invitation_msg) && $invite->invitation_msg!=''){ echo $invite->invitation_msg;}?> </td>
<?php } elseif(isset($invite->invitaion_mode) && $invite->invitaion_mode == 2) { ?>
<td  colspan="2"><?php echo  'Email'; ?> </td>
<td><strong>Message</strong></td>
<td  colspan="2"><?php if(isset($invite->invitation_msg) && $invite->invitation_msg!=''){ echo $invite->invitation_msg; }?> </td>
<?php } elseif(isset($invite->invitaion_mode) && $invite->invitaion_mode == 3) { ?>
<td  colspan="2"><?php echo  'Invitaion Card'; ?> </td>
<?php } ?>
</tr>
<?php if(isset($invite->invitaion_mode) & $invite->invitaion_mode == 3) {?>
<tr> 
<td><strong>Card Design</strong></td>
<td  colspan="2"><?php if(isset($invite->packge_title) && $invite->packge_title!=''){ echo $invite->packge_title; }?> </td>
<?php if(isset($invite->packge_img) && $invite->packge_img!=''){?>
<td><strong>Card Image </strong></td>
<td colspan="2"><img src="<?php echo  url('').$invite->packge_img; ?>" width="80" /></td>
<?php } ?>
</tr>
<?php } ?>
<tr> 
<td><strong>Price </strong></td>
<td  colspan="2">SAR <?php echo number_format($invite->card_price,2); ?></td>
<td align="right"><strong></strong></td>
<td  colspan="2"></td>
</tr>

<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- end Electronic Invitaion Card  from here  -->
<?php } 
 
if(isset($value->product_sub_type) && ($value->product_sub_type == 'singer' || $value->product_sub_type == 'band' || $value->product_sub_type == 'acoustic' || $value->product_sub_type == 'singer'))  {
$singerinfo=array();
if(isset($value->product_sub_type) && ($value->product_sub_type == 'singer' || $value->product_sub_type == 'band')){

 $singerinfo = DB::table('nm_music')->where('id',$value->product_id)->first(); 
}


 ?>
<!-- Start Studio System from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Reviving Concerts </strong></td>
</tr>
@php if(isset($value->product_sub_type) && ($value->product_sub_type != 'singer'  || $value->product_sub_type != 'band')){   @endphp
<tr>
<td><strong> Tiitle </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Studio  Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo $shop->address; }else{ echo 'N/A';} ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?>
  <td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>  
@php } @endphp
<tr>
<td><strong> Name </strong></td>
<td colspan="2"><?php 
if(isset($singerinfo->name) && $singerinfo->name!=''){ echo  $singerinfo->name; }else{
echo  $value->pro_title; } ?></td>
<td><strong> Image  </strong></td>
<td colspan="2">
<?php 
if(isset($singerinfo->image) && $singerinfo->image!=''){ $img=$singerinfo->image; }else{
$img=$value->pro_Img; } ?>
  <img src="<?php echo  $img; ?>" width="80" /></td>   
  @php if(isset($value->quantity) && $value->quantity>0) { @endphp
<tr>
  <td><strong>Qty</strong></td>
  <td colspan="2"><?php echo  $value->quantity; ?></td>
  <td><strong></strong></td>
  <td  colspan="2"></td>
</tr>
@php } @endphp
<?php 
if(count($optio_value) >0) {
foreach($optio_value as $val) {
 ?>   
<tr>
  <td><strong>For  </strong></td>
  <td colspan="2"><?php if($val->option_value_title!=''){ echo  $val->option_value_title ;}else{ echo  $value->buy_rent ; } ?></td>
  <?php if($val->option_value_title == 'Rent') {  ?>
  <td><strong>Rent Charge/Day </strong></td>
  <td colspan="2">SAR <?php echo number_format($val->value,2); ?>    
  </td>
<?php } else { ?>
  <td><strong>Price</strong></td>
  
<?php if(isset($getprice) && $getprice != '') {?>
        <td colspan="2">SAR <?php echo number_format($getprice,2); ?></td>
        <?php } else  { ?>
        <td colspan="2">N/A</td>
        <?php } ?>  
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($value->shipping_id != '')  {?>
<td colspan="2"><?php echo  $value->shipping_id; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($value->shipping_charge != '')  {?>
<td  colspan="2"><strong>SAR <?php echo number_format($value->shipping_charge,2); ?></strong></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } }?>
</tr>
<?php  } }
 if(isset($product_rent)) {  ?>
  <tr> 
    <td><strong> Occasion Date </strong></td>
    <?php if(date("j M Y", strtotime($product_rent->rental_date)) != '1 Jan 1970' || $product_rent->rental_date == '') {?>
    <td colspan="2"><?php echo  date("j M Y", strtotime($product_rent->rental_date)); ?></td>
  <?php } else { ?>
    <td colspan="2">N/A</td>
  <?php }?>
    <td><strong> Return Date </strong></td>
    <?php if(date("j M Y", strtotime($product_rent->return_date)) != '1 Jan 1970' || $product_rent->return_date == '') {?>
    <td colspan="2"><?php echo  date("j M Y", strtotime($product_rent->return_date)); ?></td>
     <?php } else { ?>
    <td colspan="2">N/A</td>
  <?php }?>
  </tr>
<?php } 
if($value->insurance_amount != '') { ?>
  <tr>
<td><strong>Insurance Amount</strong></td>
<td colspan="2">SAR <?php echo number_format($value->insurance_amount,2); ?></td>
<td><strong></strong></td>
<td colspan="2"></td>
</tr>
<?php } ?>

<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- end Studio System from here  -->
<?php } 
 
if($value->product_sub_type == 'hall')  
{ 
  $internal_food = DB::table('nm_order_internal_food_dish')->where('order_id',$order)->where('product_id',$value->product_id)->get();
?>
<!-- Start Hall from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Hall </strong></td>
</tr>   
<tr>
<td><strong> Hotel  Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo $shop->mc_name; } ?></td>
<td><strong>Hotel Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr>  

<tr>
<td><strong> Hall Name </strong></td>
<td colspan="2"><?php echo $value->pro_title; ?></td>
<td><strong> Hall Image  </strong></td>
<td colspan="2"><img src="<?php echo $value->pro_Img; ?>" width="80" /></td>
</tr>
<tr>
<td><strong>Insurance Amount</strong></td>
<?php if($value->insurance_amount != '') { ?>
<td colspan="2">SAR <?php echo number_format($value->insurance_amount,2); ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
<td><strong>Hall Price</strong></td>
<?php if($value->pro_price != '') { ?>
<td colspan="2">SAR <?php echo number_format($value->pro_price,2); ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>

<tr>
<td><strong>Hall Paid  Amount ( 25% Advance)</strong></td>
<?php if($value->pro_price != '') { ?>
<td colspan="2">SAR <?php 
      $paidamu=($value->pro_price)*25/100;
echo number_format($paidamu,2); ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
<td><strong>Hall Pending Amount</strong></td>
<?php if($value->pro_price != '') { ?>
<td colspan="2">SAR <?php 
$unpaidamu=($value->pro_price)*75/100;
echo number_format($unpaidamu,2); ?></td>
<?php } else { ?>
<td colspan="2">N/A</td>
<?php } ?>
</tr>

<?php 
 if(count($optio_value) >0) { ?>
  <tr>
  <td colspan="3" style="text-decoration:underline;"><strong>Service Ordered </strong></td>
  <td align="right">&nbsp;</td>
  <td  colspan="2">&nbsp;</td>
</tr> 
 <?php foreach($optio_value as $val){?>   
<tr>
  <td><strong>Service Name </strong></td>
  <?php if($val->option_value_title != '') { ?>
  <td colspan="2"><?php echo  $val->option_value_title ; ?></td>
  <?php } else { ?>
  <td colspan="2">N/A</td>
  <?php } ?>
  
  <td><strong> Price </strong> </td>
  <?php if($val->value != '') { ?>
  <td  colspan="2">SAR <?php echo number_format($val->value,2); ?></td>
  <?php } else { ?>
  <td colspan="2">N/A</td>
  <?php } ?>
  
</tr>
<?php } }  
if(isset($internal_food)) { ?>
<tr>
<td colspan="3" style="text-decoration:underline"><strong>Food Ordered </strong></td>
<td align="right">&nbsp;</td>
<td  colspan="2">&nbsp;</td>
</tr>
<?php  
foreach($internal_food as $key => $int_food) 
{
?>  
<tr>
  <td><strong>Dish Name </strong></td>
  <td colspan="2"><?php echo  $int_food->internal_dish_name ; ?></td>
  <td><strong>Dish Image </strong> </td>
  <td colspan="2"><img src="<?php echo  $int_food->dish_image; ?>" width="80" /></td>  
</tr>

<tr>
  <td><strong>Container </strong></td>
  <td colspan="2"><?php echo  $int_food->container_title ; ?></td>
  <td><strong>    Container Image  </strong> </td>
  <td colspan="2"><img src="<?php echo  $int_food->container_image; ?>" width="80" /></td>  
</tr>

<tr>
  <td><strong> Quantity </strong></td>
  <td colspan="2"><?php echo  $int_food->quantity ; ?></td>
  <td><strong> Price  </strong> </td>
  <td  colspan="2">SAR <?php echo number_format($int_food->price,2); ?></td>
</tr>
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<?php } } ?>

<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->paid_total_amount) && $value->paid_total_amount!=''){ echo number_format($value->paid_total_amount,2);$total_sum = $total_sum+$value->paid_total_amount; }else{ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- end Hall from here  -->

<?php } 
 
if($value->product_sub_type == 'buffet')  
{ 
  $external_food = DB::table('nm_order_external_food_dish')->where('order_id',$order)->where('category_id',$value->category_id)->where('external_food_dish_id',$value->product_id)->get();
  //echo '<pre>';print_r($external_food);die;
?>
<!-- Start Buffet from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  
  
<tr>
<td><strong> Shop Name</strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php  echo $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr> 

<?php
if(count($external_food) >0) { ?>
<tr>
<td colspan="3" style="text-decoration:underline"><strong>Food Ordered </strong></td>
<td align="right">&nbsp;</td>
<td  colspan="2">&nbsp;</td>
</tr>
<?php  
foreach($external_food as $key => $ext_food)
{
 $setname = Helper::setExtraProductFieldType($ext_food->container_id);
 $dishname = Helper::getProduckInfo($ext_food->external_food_dish_id);
 //echo '<pre>';print_r($ext_food);
?>  
<tr>
  <td><strong>Dish Name </strong></td>
  <td colspan="2"><?php if(isset($dishname->pro_title) && $dishname->pro_title!=''){ echo $dishname->pro_title; } ?></td>
  <?php if(isset($dishname->pro_Img) && $dishname->pro_Img!=''){?>
  <td><strong>Dish Image </strong> </td>
  <td colspan="2"><img src="<?php echo $dishname->pro_Img; ?>" width="80" /></td>  
  <?php } ?>
</tr>
<tr>
  <td><strong>Container </strong></td>
  <td colspan="2"><?php if(isset($setname->option_title) && $setname->option_title!=''){  echo $setname->option_title; }  ?></td>
  <?php if(isset($setname->image) && $setname->image=''){ ?>
  <td><strong>Container Image  </strong> </td>
  <td colspan="2"><img src="<?php echo $setname->image; ?>" width="80" /></td> 
  <?php } ?> 
</tr>

<tr>
  <td><strong> Quantity </strong></td>
  <td colspan="2"><?php echo  $ext_food->quantity ; ?></td>
  <td><strong> Price  </strong> </td>
  <td  colspan="2">SAR <?php echo number_format($ext_food->price,2); ?></td>
</tr>
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<?php } } ?>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($value->shipping_id != '')  {?>
<td colspan="2"><?php echo $value->shipping_id; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($value->shipping_charge != '')  {?>
<td  colspan="2"><strong>SAR <?php echo number_format($value->shipping_charge,2); ?></strong></td>
<?php }else{?>
<td  colspan="2"> N/A</td>
<?php } ?>

</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- end Buffet from here  -->
<?php } 
 
if($value->product_sub_type == 'dessert' || $value->product_sub_type == 'dates')  { 
?>
<!-- Start Dessert and Date from here -->  
<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<?php if($value->product_sub_type == 'dessert')  { 
?>
<tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Dessert </strong></td>
</tr>
<?php } elseif ($value->product_sub_type == 'dates') { ?>
 <tr>
<td colspan="6" bgcolor="#f7f0e0" align="center" style="font-size:13pt;"><strong> Date </strong></td>
</tr>
<?php } ?>  
<tr>
<td><strong> Shop  Name </strong> </td>
<td colspan="2"><?php if(isset($shop->mc_name) && $shop->mc_name!=''){ echo  $shop->mc_name; } ?></td>
<td><strong>Shop   Address</strong></td>
<td><?php if(isset($shop->address) && $shop->address!=''){ echo  $shop->address; } ?></td>
<?php if(isset($shop->google_map_address) && $shop->google_map_address!=''){ ?><td><a href="<?php echo  $shop->google_map_address; ?>"><img src="https://www.wisitech.in/themes/images/mapemail.png" /></a></td><?php } ?>
</tr> 
<tr>
<td colspan="3" style="text-decoration:underline"><strong>Product Details </strong></td>
<td align="right">&nbsp;</td>
<td  colspan="2">&nbsp;</td>
</tr>
<tr>
  <td><strong>Product Name </strong></td>
  <td colspan="2"><?php echo  $value->pro_title ; ?></td>
  <td><strong> Product Image </strong> </td>
  <td colspan="2"><img src="<?php echo  $value->pro_Img; ?>" width="80" /></td>  
</tr>
<?php 
if(count($optio_value) >0) {
foreach($optio_value as $val) {?>   
<tr>
  <td><strong>Shop By </strong></td>
  <td colspan="2"><?php echo  $val->option_value_title ; ?></td>
  <td><strong> Quantity </strong> </td>
  <td  colspan="2"><?php echo  $val->quantity; ?></td>
</tr>
<tr>
  <td><strong>Price </strong></td>
  <td colspan="2">SAR <?php echo number_format($val->value,2); ?></td>
  <td><strong></strong> </td>
  <td colspan="2"></td>  
</tr>
<?php } }?>


<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>
<tr>
<td><strong>Shipping method  </strong></td>
<?php if($value->shipping_id != '')  {?>
<td colspan="2"><?php echo  $value->shipping_id; ?></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>
<td><strong>Shipping Price </strong></td>
<?php if($value->shipping_charge != '')  {?>
<td  colspan="2"><strong>SAR <?php echo number_format($value->shipping_charge,2); ?></strong></td>
<?php } else  {?>
<td  colspan="2"> N/A</td>
<?php } ?>

</tr>
<tr>
<td><strong>Currency</strong></td>
<td colspan="2"><?php if(isset($value->currency) && $value->currency!=''){ echo $value->currency;} ?></td>
<td><strong>Total Amount </strong></td>
<td  colspan="2"><strong>SAR <?php if(isset($value->total_price) && $value->total_price!=''){ echo number_format($value->total_price,2);$total_sum = $total_sum+$value->total_price; } ?></strong></td>
</tr>
<!-- end Dessert and Date from here  -->

<?php } ?>
  



<?php } } ?>


<tr>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="6"></td>
</tr>  


<tr>
  <td colspan="3" style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px"></td>
<td style="border-bottom:1px solid #ebd49c;vertical-align:middle;height:10px" colspan="3">
    <table width="100%">
      

<tr>
  <td><strong>VAT Charge </strong></td>
  <td>
@php 
   $totalvatprice = $total_sum-$discountamount;
   $vatamount     = Helper::calculatevat($orderDta->order_id,$totalvatprice);
   echo 'SAR '.number_format(($vatamount),2);
@endphp
    </td>
  </tr>
<?php if($discountamount>0){ 

if(isset($discountcoupan) && $discountcoupan!=''){
  ?>
 
  <tr>
    <td><strong>Coupan Applied </strong></td>
  <td>@php echo $discountcoupan; @endphp</td>
</tr>
<?php } ?>
<tr>
  <td><strong>Coupan Discount </strong></td>
  <td>@php    echo 'SAR '.number_format(($discountamount),2);
                @endphp
    </td>
  </tr>
  <?php } ?>
  <tr><td><strong>Total Amount </strong></td>
  <td  colspan="2"><strong>SAR <?php $totAmt = $totalvatprice+$vatamount+$total_shipping_charge; echo number_format($totAmt,2); ?></strong></td>
</tr>



    </table>

</td>
</tr>

        </tbody></table></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px;font-weight:bold">&nbsp;</td>
  </tr>

  <tr>
    <td align="left" style="padding:10px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px;font-weight:bold">Let us know if you have any further query.</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px;margin:0px;font-family:Arial,Helvetica,sans-serif;font-size:11pt;color:#737373;line-height:24px">&nbsp;</td>
  </tr>
</tbody></table>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title>@if (Lang::has($lang.'.MAIL_EMAIL_TEMPLATE')!= '') {{  trans($lang.'.MAIL_EMAIL_TEMPLATE') }} @else {{ trans($LANUAGE.'.MAIL_EMAIL_TEMPLATE') }} @endif</title>
</head>
<body>
<?php 
// $logo {{ asset('img/logo.png') }}
//$logo = url('/').'assets/img/logo.png'; 

$logo = 'http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png';
$logo = $SITE_LOGO;
if(file_exists($SITE_LOGO))
$logo = $SITE_LOGO;
$getFbLinks        = Helper::getfblinks();
$gettwitterLinks   = Helper::gettwitterlinks();
$getgoogleLinks    = Helper::getgooglelinks();
$getinstagramLinks = Helper::getinstagramlinks();
$getpinterestLinks = Helper::getpinterestlinks();
$getyoutubeLinks   = Helper::getyoutubelinks();
?>
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right  ">مرحبًا  {{ $name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373;  text-align:right  ">شكراً لتسجيلك معنا في منصة قولدن كيجز الإلكترونية.<br />
سوف نجعلك تصل الى عملائك وتلبي متطلباتهم لإدارة مناسباتهم الخاصة، وكذلك الحصول على مستحقاتك في الوقت المحدد.<br />
يمكنك الآن إضافة خدماتك ومنتجاتك التي يمكنك تقديمها بأفضل صورة للعملاء عبر لوحة إدارة التاجر.
</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ; ">نحن نتطلع إلى بناء علاقة سعيدة وطويلة الأمد معكم.</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:right;  ">مشرف</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; text-align:right; ">أقفاص ذهبية</td>
  </tr>
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
	@if($getFbLinks !='')
	<a target="_blank" title="Facebook" href="{{$getFbLinks}}"><img src="{{ url('') }}/themes/images/social-facebook.png" /></a>&nbsp;
	@endif
    @if($gettwitterLinks !='')
	<a target="_blank" title="Twitter" href="{{$gettwitterLinks}}"><img src="{{ url('') }}/themes/images/social-twitter.png" /></a>&nbsp;
	@endif
    @if($getgoogleLinks !='')
	<a target="_blank" title="Google+" href="{{$getgoogleLinks}}"><img src="{{ url('') }}/themes/images/social-googleplus.png" /></a>&nbsp;
	@endif
    @if($getinstagramLinks !='')
	<a target="_blank" title="Instagram" href="{{$getinstagramLinks}}"><img src="{{ url('') }}/themes/images/share-instagram.png" /></a>&nbsp;
	@endif
    @if($getpinterestLinks !='')
	<a target="_blank" title="Pinterest" href="{{$getpinterestLinks}}"><img src="{{ url('') }}/themes/images/social-pint.png" /></a>&nbsp;
	@endif
    @if($getyoutubeLinks !='')
	<a target="_blank" title="Youtube" href=" {{$getyoutubeLinks}}"><img src="{{ url('') }}/themes/images/social-youtube.png" /></a>
	@endif
	</td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">Hi {{ $name }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Thank you for registering with us. We make you reach out to your prospects or customers, cater to their event management requirements and you will be paid on time.<br />
 You can now add your services and products that you can deliver the best to our elite audience/customers from Merchant Panel. </td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">We look forward to build a happy and long-lasting relationship with you. </td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">&nbsp;</td>
  </tr>
  <tr>
    <td style="padding-top:20px; padding-bottom:20px"><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left; padding-right:10px; text-align:right; padding-bottom:10px">@if (Lang::has($lang.'.MAIL_USER_NAME')!= '') {{  trans($lang.'.MAIL_USER_NAME') }} @else {{ trans($LANUAGE.'.MAIL_USER_NAME') }} @endif : </td>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left; padding-bottom:10px"><a href="javascript:void(0);" style="text-decoration:none; color:blue; text-align:left">{{ $email }}</a></td>
        </tr>
        <tr>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left; padding-right:15px;padding-bottom:15px; text-align:right; vertical-align:middle"> @if (Lang::has($lang.'.MAIL_PASSWORD')!= '') {{  trans($lang.'.MAIL_PASSWORD') }} @else {{ trans($LANUAGE.'.MAIL_PASSWORD') }} @endif : </td>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left;padding-bottom:15px;vertical-align:middle">{{ $password }}</td>
        </tr>
        <tr>
          <td style="padding-right:10px;">&nbsp;</td>
          <td style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#737373;text-align:left; margin:0px; padding:0px;"><a href="<?php echo url(''); ?>/sitemerchant" style="color:#fff; background-color:#c09e9d; display:inline-block; text-decoration:none; padding:8px 15px; text-align:center">@if (Lang::has($lang.'.MAIL_LOGIN_YOUR_ACCOUNT')!= '') {{  trans($lang.'.MAIL_LOGIN_YOUR_ACCOUNT') }} @else {{ trans($LANUAGE.'.MAIL_LOGIN_YOUR_ACCOUNT') }} @endif</a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">Admin</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
</body>
</html>

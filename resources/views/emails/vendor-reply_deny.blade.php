<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title<?php echo $SITENAME;?></title>
</head>
<body>
<?php 
// $logo {{ asset('img/logo.png') }}
//$logo = url('/').'assets/img/logo.png'; 
$getFbLinks        = Helper::getfblinks();
$gettwitterLinks   = Helper::gettwitterlinks();
$getgoogleLinks    = Helper::getgooglelinks();
$getinstagramLinks = Helper::getinstagramlinks();
$getpinterestLinks = Helper::getpinterestlinks();
$getyoutubeLinks   = Helper::getyoutubelinks();

$logo = 'http://wisitech.in/public/assets/logo/Logo_1517904811_Logo_hfgh.png';
$logo = $SITE_LOGO;
if(file_exists($SITE_LOGO))
$logo = $SITE_LOGO;
?>
@if($languagetype=='2')
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
	@if($getFbLinks !='')
	<a target="_blank" title="Facebook" href="{{$getFbLinks}}"><img src="{{ url('') }}/themes/images/social-facebook.png" /></a>&nbsp;
	@endif
    @if($gettwitterLinks !='')
	<a target="_blank" title="Twitter" href="{{$gettwitterLinks}}"><img src="{{ url('') }}/themes/images/social-twitter.png" /></a>&nbsp;
	@endif
    @if($getgoogleLinks !='')
	<a target="_blank" title="Google+" href="{{$getgoogleLinks}}"><img src="{{ url('') }}/themes/images/social-googleplus.png" /></a>&nbsp;
	@endif
    @if($getinstagramLinks !='')
	<a target="_blank" title="Instagram" href="{{$getinstagramLinks}}"><img src="{{ url('') }}/themes/images/share-instagram.png" /></a>&nbsp;
	@endif
    @if($getpinterestLinks !='')
	<a target="_blank" title="Pinterest" href="{{$getpinterestLinks}}"><img src="{{ url('') }}/themes/images/social-pint.png" /></a>&nbsp;
	@endif
    @if($getyoutubeLinks !='')
	<a target="_blank" title="Youtube" href=" {{$getyoutubeLinks}}"><img src="{{ url('') }}/themes/images/social-youtube.png" /></a>
	@endif
	</td>
  </tr>
  <tr>
    <td align="right" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">مرحبا {{ $username }}</td>
  </tr>
  <tr>
    <td align="right" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">شكرا على التعبير عن الاهتمام في خدمتي. يرجى التحقق من الأسعار وغيرها من التفاصيل أدناه... </td>
  </tr>
  <tr>
    <td align="right" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">مشرف</td>
  </tr>
  <tr>
    <td align="right" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">أقفاص ذهبية</td>
  </tr>
  <tr>
    <td align="right" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
@else
<table width="600" border="0" align="center" style="background-color:#fffaee; margin-top:40px; padding-bottom:20px;">
  <tr>
    <td align="center" style="padding-top:40px;"><img src="<?php echo $logo; ?>" alt="" /></td>
  </tr>
  <tr>
    <td align="left" style="padding:35px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold; text-align:center;">
	@if($getFbLinks !='')
	<a target="_blank" title="Facebook" href="{{$getFbLinks}}"><img src="{{ url('') }}/themes/images/social-facebook.png" /></a>&nbsp;
	@endif
    @if($gettwitterLinks !='')
	<a target="_blank" title="Twitter" href="{{$gettwitterLinks}}"><img src="{{ url('') }}/themes/images/social-twitter.png" /></a>&nbsp;
	@endif
    @if($getgoogleLinks !='')
	<a target="_blank" title="Google+" href="{{$getgoogleLinks}}"><img src="{{ url('') }}/themes/images/social-googleplus.png" /></a>&nbsp;
	@endif
    @if($getinstagramLinks !='')
	<a target="_blank" title="Instagram" href="{{$getinstagramLinks}}"><img src="{{ url('') }}/themes/images/share-instagram.png" /></a>&nbsp;
	@endif
    @if($getpinterestLinks !='')
	<a target="_blank" title="Pinterest" href="{{$getpinterestLinks}}"><img src="{{ url('') }}/themes/images/social-pint.png" /></a>&nbsp;
	@endif
    @if($getyoutubeLinks !='')
	<a target="_blank" title="Youtube" href=" {{$getyoutubeLinks}}"><img src="{{ url('') }}/themes/images/social-youtube.png" /></a>
	@endif
	</td>
  </tr>
  <tr>
    <td align="left" style="padding:55px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:12pt; color:#737373; font-weight:bold;  ">Hi {{ $username }}</td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Thanks for expressing interest in my service. Please check the pricing and other details below... </td>
  </tr>
  <tr>
    <td align="left" style="padding:10px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; font-weight:bold; ">Admin</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">Golden Cages</td>
  </tr>
  <tr>
    <td align="left" style="padding:0px 20px 0px 20px; margin:0px; font-family:Arial, Helvetica, sans-serif; font-size:11pt; color:#737373; line-height:24px; ">&nbsp;</td>
  </tr>
</table>
@endif
</body>
</html>

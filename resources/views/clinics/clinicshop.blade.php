@php //echo "<pre>"; print_r($servicecategoryAndservices); @endphp
@include('includes.navbar')
  @php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
	$Current_Currency = 'SAR'; 
  } 
  $cururl = request()->segment(count(request()));
@endphp
<div class="outer_wrapper">
<div class="vendor_header">
	<div class="inner_wrap">               
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{ $fooddateshopdetails[0]->mc_img }}" alt="" /></a></div>                      	
        </div> 
		<div class="vendor_header_right">
        <!-- vendor_header_left -->
        <div class="vendor_welc">
          @include("includes.language-changer")
          <div class="vendor_name username"> {{ (Lang::has(Session::get('lang_file').'.WELCOME')!= '')  ?  trans(Session::get('lang_file').'.WELCOME'): trans($OUR_LANGUAGE.'.WELCOME')}} <span>
            <?php 
			if(Session::has('customerdata.user_id')) 
	        {
				$userid  = Session::get('customerdata.user_id');
				$getInfo = Helper::getuserinfo($userid); 
				if($getInfo->cus_name==''){echo 'Guest';}else{echo $getInfo->cus_name;} 
			}
			?>
            <ul class="vendor_header_navbar">
              <li><a href="{{route('my-account-profile')}}"@php if($cururl=='my-account-profile') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MyProfile')!= '')  ?  trans(Session::get('lang_file').'.MyProfile'): trans($OUR_LANGUAGE.'.MyProfile')}}</a></li>
              <li><a href="{{route('my-account-ocassion')}}"@php if($cururl=='my-account-ocassion' || $cururl=='order-details') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')}}</a></li>
              <li><a href="{{route('my-account-studio')}}"@php if($cururl=='my-account-studio' || $cururl=='ocassion-more-image') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_STUDIO')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_STUDIO'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_STUDIO')}}</a></li>
              <li><a href="{{route('my-account-security')}}"@php if($cururl=='my-account-security') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')}}</a></li>
              <li><a href="{{route('my-account-wallet')}}"@php if($cururl=='my-account-wallet') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_WALLET')!= '')  ?  trans(Session::get('lang_file').'.MY_WALLET'): trans($OUR_LANGUAGE.'.MY_WALLET')}}</a></li>
              <li><a href="{{route('my-account-review')}}"@php if($cururl=='my-account-review') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')}}</a></li>
              <li><a href="{{route('my-request-a-quote')}}"@php if($cururl=='my-request-a-quote' || $cururl=='requestaquoteview') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE')}}</a></li>
              <li><a href="{{route('change-password')}}"@php if($cururl=='change-password') echo 'class="select"'; @endphp>{{ (Lang::has(Session::get('lang_file').'.ChangePassword')!= '')  ?  trans(Session::get('lang_file').'.ChangePassword'): trans($OUR_LANGUAGE.'.ChangePassword')}}</a></li>
              <li><a href="{{url('login-signup/logoutuseraccount')}}">{{ (Lang::has(Session::get('lang_file').'.SignOut')!= '')  ?  trans(Session::get('lang_file').'.SignOut'): trans($OUR_LANGUAGE.'.SignOut')}}</a></li>
            </ul>
            </span></div>
          @if (Session::get('customerdata.token')!='')
          @php $getcartnoitems = Helper::getNumberOfcart(); @endphp
          @if($getcartnoitems>0) <a href="{{url('mycart')}}" class="vendor_cart"><img src="{{ url('') }}/themes/images/basket.png" /><span>{{ $getcartnoitems }}</span></a> @endif
          @endif </div>
        <!-- vendor_header_right -->
        @php if(count($otherbarnch)>1){ @endphp
        <div class="select_catg">

          <div class="select_lbl">@if (Lang::has(Session::get('lang_file').'.Other_Branches')!= '') {{  trans(Session::get('lang_file').'.Other_Branches') }} @else  {{ trans($OUR_LANGUAGE.'.Other_Branches') }} @endif</div>
          <div class="search-box-field">
            <select class="select_drp" id="dynamic_select">
              <option value="">@if (Lang::has(Session::get('lang_file').'.Select_Branch')!= '') {{  trans(Session::get('lang_file').'.Select_Branch') }} @else  {{ trans($OUR_LANGUAGE.'.Select_Branch') }} @endif</option>           
                         @foreach($otherbarnch as $otherbarnches)
              <option value="{{url('')}}/clinicshop/{{ $halltype }}/{{ $typeofhallid }}/{{ $shop_id }}/{{ $otherbarnches->mc_id }}"> {{ $otherbarnches->mc_name }} </option>

                         @endforeach
                    
            </select>
          </div>
        </div>
        @php } @endphp </div>
		<!-- vendor_header_left -->
	</div>
</div>
<!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">

       <ul>
        <li><a href="#about_shop" class="active">@if (Lang::has(Session::get('lang_file').'.About_Shop')!= '') {{  trans(Session::get('lang_file').'.About_Shop') }} @else  {{ trans($OUR_LANGUAGE.'.About_Shop') }} @endif</a></li>

           @if(trim($fooddateshopdetails[0]->mc_video_url!=''))
        <li><a href="#video">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif</a></li>
        @endif
		@if(count($fooddateshopreview) >=1)
        <li><a href="#our_client">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</a></li>
		@endif
        <li><a href="#choose_package">@if (Lang::has(Session::get('lang_file').'.SELECT_DOC')!= '') {{  trans(Session::get('lang_file').'.SELECT_DOC') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_DOC') }} @endif</a></li>
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->




 
<div class="inner_wrap service-wrap diamond_space">
<div class="detail_page">	
<a name="about_shop" class="linking">&nbsp;</a>
    <div class="service_detail_row">
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
		  @foreach($fooddateshopgallery as $shopgallery)
		   @php 
		   	$im=$shopgallery->image;
		   $gimg=str_replace('thumb_','',$im);
		    @endphp
             <li>
  	    	    <img src="{{ $gimg }}" />
  	    		</li>
			@endforeach
  	    		 	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
           @foreach($fooddateshopgallery as $shopgallerythumb)

             <li>
  	    	    <img src="{{ $shopgallerythumb->image }}" />
  	    		</li>
			@endforeach
  	    		
           
          </ul>
        </div>
      		</section>
            
        </div>
        
        <div class="service_detail">
        	<!-- Example DataTables Card-->
	   
        	<div class="detail_title">{{ $fooddateshopdetails[0]->mc_name }}</div>
            <div class="detail_hall_description">{{ $fooddateshopdetails[0]->address }}</div>
            <div class="detail_hall_subtitle">{{ (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '')  ?  trans(Session::get('lang_file').'.ABOUT_SHOP'): trans($OUR_LANGUAGE.'.ABOUT_SHOP')}}</div>
            <div class="detail_about_hall">
            	<div class="comment more">{{ $fooddateshopdetails[0]->mc_discription }}</div>
            </div>
              <div class="detail_hall_dimention">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}: <span>
			  <?php $getcityname = Helper::getcity($fooddateshopdetails[0]->city_id); 
				$mc_name = 'ci_name'; 
				if(Session::get('lang_file')!='en_lang')
				{
				  $mc_name = 'ci_name_ar'; 
				}
				echo $getcityname->$mc_name; 
			  ?></span></div>

 <div class="detail_hall_dimention"><span class="tm_for">@if (Lang::has(Session::get('lang_file').'.TIME')!= ''){{trans(Session::get('lang_file').'.TIME')}}@else{{trans($OUR_LANGUAGE.'.TIME') }}@endif: </span><span>{{ $fooddateshopdetails[0]->opening_time or '' }} - {{ $fooddateshopdetails[0]->closing_time or '' }} </span></div>



            @php  $homevisitcharges=$fooddateshopdetails[0]->home_visit_charge; @endphp
 <?php if(isset($fooddateshopdetails[0]->google_map_address) && $fooddateshopdetails[0]->google_map_address!=''){
 $lat  = $fooddateshopdetails[0]->latitude;
$long  = $fooddateshopdetails[0]->longitude;
 ?>
		  <div class="detail_hall_dimention" id="map" style="height: 230px!important">  </div>
		  <?php } ?> 
             
        </div> 
       
    </div> <!-- service_detail_row -->
    
    
	      <div class="service-mid-wrapper">
		         	<a name="video" class="linking">&nbsp;</a>
        @if(trim($fooddateshopdetails[0]->mc_video_url!=''))
        <div class="service-video-area">
         <div class="service-video-cont">{{ $fooddateshopdetails[0]->mc_video_description }}</div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{ $fooddateshopdetails[0]->mc_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
		@endif


        <!-- service-video-area -->
        
         
           @if(count($fooddateshopreview) >=1)
           <div class="service_list_row service_testimonial">
        	<a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                          <ul class="slides">
						  @foreach($fooddateshopreview as $customerreview)
						  @php $userinfo = Helper::getuserinfo($customerreview->customer_id); @endphp
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="{{ $userinfo->cus_pic }}"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description">{{ $customerreview->comments }}</div>
                                    	<div class="testim_name">{{ $userinfo->cus_name }}</div>
                                        <div class="testim_star"><img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png"></div>
                                    </div>
                                </div>
                            </li>
					 @endforeach
                           
                          </ul>
                        </div>
            </section>
          </div>
        </div>  


        @endif  
          </div> <!-- service-mid-wrapper -->
	





@php $tbl_field='service_id';  @endphp

	<div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                 
                @php    
					
					$k=1; $jk=0;  @endphp
                	@foreach($servicecategoryAndservices as $categories)
                	@php 
                 
                		if(count($categories->serviceslist)>0){
                		if($k==1){ $cl='select'; }else{ $cl=''; }                     
                	@endphp
                  @if(isset($categories->attribute_title) && $categories->attribute_title !='')


                <li><a href="#{{$k}}"  class="cat {{$cl}}" data-toggle="tab" @if(isset($servicecategoryAndservices[$jk]->serviceslist[0]->pro_id) && $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id!='') onclick="getbeautyservice('{{ $servicecategoryAndservices[$jk]->serviceslist[0]->pro_id }}','{{ $branchid }}','{{$tbl_field}}');"  @endif>

                  {{ $categories->attribute_title or '' }}

                </a></li>



                @endif
                @php $k++; $jk++; } @endphp
                @endforeach
              
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
	
	<div class="service-display-section">
    <a name="choose_package" class="linking">&nbsp;</a>
	<div class="service-display-right">
	
			<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
		
		@php  $z=1;     @endphp
			@foreach($servicecategoryAndservices as $productservice)
			@php    $k=count($productservice->serviceslist); 
						if($k >0 ){
			 @endphp
			  @php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp 
		<div class="diamond_wrapper_main tab-pane fade {{ $addactcon }}"  id="{{ $z}}">
			
			
			
		
			@php if($k < 6){ @endphp
			@php  $i=1;     @endphp
			<div class="diamond_wrapper_inner ">
			@foreach($productservice->serviceslist as $getallcats)
<?php
                    $img = str_replace('thumb_','',$getallcats->pro_Img);
                  ?>

      
									<div class="row_{{$i}}of{{$k}} rows{{$k}}row">
					  <a href="#" onclick="getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper @if($k==3) category_wrapper{{$k}}  @endif" style="background:url({{ $img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}}  </div></div>
						</div>
					</a>
				</div>
				 @php $i=$i+1; @endphp
				  
				@endforeach
				</div> 
				<!------------ 6th-------------->
				@php }elseif($k==6){ @endphp
				@php $j=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($productservice->serviceslist as $getallcats)
			 @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
			 @php if($j==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($j==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($j==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($j==5){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($j==6){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					<a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($j==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($j==4){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==5){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($j==6){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $j=$j+1; @endphp
				@endforeach
				</div>
				<!------------ 7th-------------->
				@php }elseif($k==7){ @endphp
				@php $l=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($productservice->serviceslist as $getallcats)
				@php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
			
			 @php if($l==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($l==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($l==7){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					 <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==6){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!------------ 8th-------------->
				@php }elseif($k==8){ @endphp
				@php $l=1; @endphp
			
					<div class="diamond_wrapper_inner">
					@foreach($productservice->serviceslist as $getallcats)
				@php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
				@php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
				@php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
				
			 @php if($l==1){ $classrd='category_wrapper1'; @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_3of5 rows5row"> @php } @endphp 
			 @php if($l==4){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_3of5 rows5row"> @php } @endphp
			  @php if($l==8){ $classrd='category_wrapper9'; @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					  <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==8){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!---------- 9th ------------------->
		@php }elseif($k==9){ @endphp
		
		
					<div class="diamond_wrapper_inner">
	  			@php $i=1; @endphp
		  @foreach($productservice->serviceslist as $getallcats)
		  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
		  

		 @php if($i==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		  @php if($i==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
		   @php if($i==4){ @endphp <div class="row_3of5 rows5row"> @php } @endphp 
		    @php if($i==7){ @endphp  <div class="row_4of5 rows5row"> @php } @endphp 
			@php if($i==9){ @endphp  <div class="row_5of5 rows5row"> @php } @endphp 
            <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
              <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});">
                <span class="category_title"><span class="category_title_inner">{{ $getallcats->pro_title or ''}}</span></span>
              </span>
            </a>
		 @php if($i==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($i==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($i==6){ @endphp <div class="clear"></div> </div> @php } @endphp 
		    @php if($i==8){ @endphp <div class="clear"></div></div> @php } @endphp 
			@php if($i==9){ @endphp  <div class="clear"></div> </div> @php } @endphp 
		 
		    @php $i=$i+1; @endphp
		   @endforeach 
	  
		  
        </div>
		
		
		
				
			@php } @endphp
				 	
				
		</div>
		@php $z=$z+1; }@endphp
		@endforeach 
		
		

		
		
	  </div>
  </div>
  <div class="diamond_shadow">{{ $fooddateshopproducts->links() }}</span></div>
  <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
	
	</div> <!-- service-display-right -->
@php //print_r($beautyshopleftproduct); @endphp
	 {!! Form::open(['url' => '#', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
	<!-- container-section -->
	<div class="service-display-left">
		<span id="selectedproduct">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="{{ $beautyshopleftproduct->pro_Img }}" alt="" /></div>
          <div class="service-product-name">{{ $beautyshopleftproduct->pro_title }}</div>
          <div class="service-beauty-description">

{{ wordwrap( $beautyshopleftproduct->pro_desc, 10, "\n", true ) }}
 </div>
          <div class="service-beauty-description"><b>{{ (Lang::has(Session::get('lang_file').'.Specialist')!= '')  ?  trans(Session::get('lang_file').'.Specialist'): trans($OUR_LANGUAGE.'.Specialist')}} {{ $beautyshopleftproduct->specialistion }}</b></div>
          @php if(count($professionalbio)>0){ @endphp 
          <div class="service_sidebar_list">
          	<div class="service_sidebar_list_title">{{ (Lang::has(Session::get('lang_file').'.PROFESSIONAL_BIO')!= '')  ?  trans(Session::get('lang_file').'.PROFESSIONAL_BIO'): trans($OUR_LANGUAGE.'.PROFESSIONAL_BIO')}}</div>
            <ul id="professional">
            	@foreach($professionalbio as $professionalbiodata)
            	<li>{{ $professionalbiodata->attribute_title}}</li>
            	@endforeach
               
            </ul>
          </div>
           @php } @endphp 
            
     </span>
     <div class="container-section">
            <div class="total_food_cost">
            <div class="total_price">{{ (Lang::has(Session::get('lang_file').'.Consultation_Fees')!= '')  ?  trans(Session::get('lang_file').'.Consultation_Fees'): trans($OUR_LANGUAGE.'.Consultation_Fees')}} : <span id="cont_final_price"> {{currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency)}} </span></div>
          </div>
    
        @php $totalprice=$beautyshopleftproduct->pro_price;
        	if($category_id=='29'){ $cart_sub_type='cosmetic'; }else{ $cart_sub_type='skin'; }
         @endphp
       <div class="btn_row">
          	
       		<input type="hidden" name="opening_time" id="opening_time" value="{{ $beautyshopleftproduct->opening_time }}">
       		<input type="hidden" name="closing_time" id="closing_time" value="{{ $beautyshopleftproduct->closing_time }}">

          	
            
            <div class="btn_row service_skin">
            <input type="button" value="{{ (Lang::has(Session::get('lang_file').'.BOOK_APPOINTMENT')!= '')  ?  trans(Session::get('lang_file').'.BOOK_APPOINTMENT'): trans($OUR_LANGUAGE.'.BOOK_APPOINTMENT')}}" class="form-btn addto_cartbtn open_popup">
          </div>
		  <?php if(isset($fooddateshopdetails[0]->terms_conditions) && $fooddateshopdetails[0]->terms_conditions!=''){ ?>
		  <div class="terms_conditions"><a  href="{{ $fooddateshopdetails[0]->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}</a></div>
		  <?php } ?>
          </div>

          </div>
        
          <!-- container-section -->
        </div>
<a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a>
{!! Form::close() !!}
        
        <!-- service-display-left -->
      </div>
	
	</div> <!-- service-display-left -->
	</div> <!--service-display-section-->


  <!--service-display-section-->
@include('includes.other_services')   
	  <!-- other_serviceinc --><!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div>

<div class="common_popup book_appointmentpopup">
	{!! Form::open(['url' => 'clinicshop/addcartproduct', 'method' => 'post', 'name'=>'booknowid', 'id'=>'booknowid', 'enctype' => 'multipart/form-data']) !!}
      	<div class="ring_review_popup">      
		<a href="javascript:void(0);" class="close_popup_btn">X</a>
        <div class="appointment_title"> {{ (Lang::has(Session::get('lang_file').'.Appointment_for')!= '')  ?  trans(Session::get('lang_file').'.Appointment_for'): trans($OUR_LANGUAGE.'.Appointment_for')}}</div>
        <div class="appointment_name"><span id="doct">{{ $beautyshopleftproduct->pro_title }}</span></div>
        
        <div class="appointment_wrap">
        	<div class="appointment_row app_date ">
            	<div class="checkout-form-cell">
                   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.DATE')!= '')  ?  trans(Session::get('lang_file').'.DATE'): trans($OUR_LANGUAGE.'.DATE')}}</div> 

                   <div class="checkout-form-bottom"><input data-selecteddateproduct='{{ $beautyshopleftproduct->pro_id }}' data-branchid='{{ $branchid }}' type="text" name="bookingdate" id="bookingdate" class="t-box checkout-small-box cal-t input-calendar mytest" onchange="mynewtimeslot();" >
                    
                   </div> 
                </div>
                <div class="checkout-form-cell">
                   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.TIME')!= '')  ?  trans(Session::get('lang_file').'.TIME'): trans($OUR_LANGUAGE.'.TIME')}}</div> 
                   <div class="checkout-form-bottom" id="bookingtimebasic">
                   	
                   
                  <input name="bookingtime" type="text" id="bookingtime" autocomplete="off" class="t-box cal-t" />
                   
                   </div> 
                </div>
            </div>
            
            <div class="appointment_row appointment_row_radio">            
            <div class="checkout-form-cell">
               <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.PATIENT_STATUS')!= '')  ?  trans(Session::get('lang_file').'.PATIENT_STATUS'): trans($OUR_LANGUAGE.'.PATIENT_STATUS')}}</div> 
               <div class="checkout-form-bottom">
               <div class="gender-radio-box"><input  name="patienttype" id="patienttype" type="radio" value="newpatient" checked="checked" onclick="selectpatienttype(this.value);"> <label for="patienttype">{{ (Lang::has(Session::get('lang_file').'.NEW')!= '')  ?  trans(Session::get('lang_file').'.NEW'): trans($OUR_LANGUAGE.'.NEW')}}</label></div>
               <div class="gender-radio-box"><input  name="patienttype" id="patienttype1" type="radio" value="existpatient" onclick="selectpatienttype(this.value);"> <label for="patienttype1">{{ (Lang::has(Session::get('lang_file').'.EXISTING')!= '')  ?  trans(Session::get('lang_file').'.EXISTING'): trans($OUR_LANGUAGE.'.EXISTING')}}</label></div>
               </div> 
           </div>
            </div>
            

            	<div class="checkout-form-cell fns" id="fno" style="display: none;">
                   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.ENTER_FILE_NO')!= '')  ?  trans(Session::get('lang_file').'.ENTER_FILE_NO'): trans($OUR_LANGUAGE.'.ENTER_FILE_NO')}}</div> 
                   <div class="checkout-form-bottom"><input type="text" name="fileno" id="fileno" class="t-box checkout-small-box">
                   		
                   </div> 
                </div>

        </div>
         <span id="alertmsg"></span>
        <div class="btn_row">

        	  <input type="hidden" name="category_id" value="{{ $subsecondcategoryid}}">
          	<input type="hidden" name="subsecondcategoryid" value="{{ $category_id }}">
          	<input type="hidden" name="cart_sub_type" value="{{$cart_sub_type}}">
          	<input type="hidden" name="branch_id" value="{{ $branchid }}">
          	<input type="hidden" name="shop_id" value="{{ $shop_id }}">
          	<input type="hidden" name="vendor_id" value="{{ $beautyshopleftproduct->pro_mr_id }}">
          	<input type="hidden" name="product_id" id="product_id" value="{{ $beautyshopleftproduct->pro_id }}">
          	<input type="hidden" name="product_price" id="product_price" value="{{ currency($totalprice, 'SAR',$Current_Currency,$format = false) }}">
            <input type="hidden" name="todaydate" id="todaydate" value="{{ date('jMY') }}">
<input type="hidden" name="product_orginal_price" id="product_orginal_price" value="{{ currency($totalprice, 'SAR',$Current_Currency,$format = false) }}">
           
            <input type="submit" class="form-btn big-btn btn-info-wisitech" value="{{ (Lang::has(Session::get('lang_file').'.BOOK_APPOINTMENT')!= '')  ?  trans(Session::get('lang_file').'.BOOK_APPOINTMENT'): trans($OUR_LANGUAGE.'.BOOK_APPOINTMENT')}}" />
        </div>
        
      </div> <!-- review_popup -->
      {!! Form::close() !!}
	  </div>



</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')
@include('includes.popupmessage')
<script>

   

 

jQuery('#booknowid').submit(function()
{
 
 var bdate =  jQuery('#bookingdate').val();
  var btime =  jQuery('#bookingtime').val();
  if(btime == undefined)
  {
   var btime =  jQuery('#bookingtimesecond').val(); 
  }
 
 var productid =  jQuery('#product_id').val();
 
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"{{url('chkavailbility')}}?bdate="+bdate+"&btime="+btime+"&pid="+productid,
     success:function(res)
     {  
        if(res==1)
        {
        jQuery('#bookingtime').val('');
        jQuery('#bookingtime').val('');
        jQuery('#bookingtimesecond').val('');
        jQuery('#alertmsg').html("<span class='error'>{{ (Lang::has(Session::get('lang_file').'.Please_select_other_date')!= '')  ?  trans(Session::get('lang_file').'.Please_select_other_date'): trans($OUR_LANGUAGE.'.Please_select_other_date')}}</span>");
        return false;
        }
        else
        {
        return true;
        }

     }
   });



});
 


jQuery(function(){
  jQuery('#dynamic_select').on('change', function () {
	  var url = jQuery(this).val(); // get selected value          
	  if (url) { // require a URL
		  window.location = url; // redirect
	  }
	  return false;
  });
});
</script>

<script>
jQuery(document).ready(function(){
 jQuery("#booknowid").validate({
    rules: {          
          "bookingdate" : {
            required : true
          },
          "bookingtime" : {
            required : true
          },
          fileno: { 
     		required: true
    		}
         },
         messages: {
          "bookingdate": {
            required:  "@if (Lang::has(Session::get('lang_file').'.BOOKINGDATE')!= '') {{  trans(Session::get('lang_file').'.BOOKINGDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKINGDATE') }} @endif"
          },
          "bookingtime": {
            required: "@if (Lang::has(Session::get('lang_file').'.BOOKINGTIME')!= '') {{  trans(Session::get('lang_file').'.BOOKINGTIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKINGTIME') }} @endif"
          },
          "fileno": {
            required: "@if (Lang::has(Session::get('lang_file').'.FILE_NO')!= '') {{  trans(Session::get('lang_file').'.FILE_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.FILE_NO') }} @endif"
          },
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#booknowid").valid()) {        
    jQuery('#booknowid').submit();
   }
  });
});
</script>




<script type="text/javascript">
function selectpatienttype(ptype)
{

	if(ptype=='newpatient')
	{   
        jQuery('#fno').css('display','none');
     }
     if(ptype=='existpatient')
	{   
        jQuery('#fno').css('display','block');
        
     }
}
</script>








<script language="javascript">

$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});

</script>

<!------------------ tabs----------------->
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
</script>



<script src="{{url('/')}}/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>

<!------------ end tabs------------------>

<!--------- date picker script-------->
 <script type="text/javascript">

function getCurrentTime(date) {
    var hours = date.getHours(),
        minutes = date.getMinutes(),
        ampm = hours >= 12 ? 'pm' : 'am';

  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;

  return hours + ':' + minutes + ' ' + ampm;
}


            // When the document is ready
			var jql = jQuery.noConflict();
            jql(document).ready(function () {


               
                 var currentTime= moment(); 
            var starttime=document.getElementById('opening_time').value;
                var endtime=document.getElementById('closing_time').value;

                 var mstartTime = moment(starttime, "HH:mm a");
                    var mendTime = moment(endtime, "HH:mm a");
                    
                   var amIBetween = currentTime.isBetween(mstartTime , mendTime);
                   if(amIBetween==false){
                       //var pdate= new Date(+new Date() + 86400000);
                       var pdate= new Date();
                   }else{
                      var pdate=new Date();
                   }                
      

              jql('#bookingdate').datepicker({
              //format: "yyyy-mm-dd",
              format: "d M yyyy",
              maxViewMode: 0,
              autoclose: true,
              startDate: pdate,
              }); 
              jql("#bookingdate").datepicker("setDate", pdate);

          
                 if(amIBetween==true){                  
                    
                    var st=getCurrentTime(new Date());
                 }else{
                  var st=starttime
                 }
                //alert('test');
 

				jql('#bookingtime').timepicker({
					'minTime': st,
					'maxTime': endtime,
					'timeFormat': 'g:i A',   
					'showDuration': false
				});

            });
        </script>



////////////// added code 01-mar-2019 ////////
<script type="text/javascript">
  function mynewtimeslot(){
 
jQuery('#bookingtime').timepicker('remove');

      var todaydate=jQuery('#todaydate').val();
var bookingdate=jQuery('#bookingdate').val();
       var starttime=document.getElementById('opening_time').value;
                var endtime=document.getElementById('closing_time').value;

var n= bookingdate.split(" ").slice(0, 3).join('');

if(todaydate==n){
 var timeNow = new Date();

        var currhours   = timeNow.getHours();

         var currmins   = timeNow.getMinutes();

         var hours   = timeNow.getHours()+1;

        var ampm = hours >= 12 ? 'pm' : 'am';

if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;

}else{


         var newstarttime=currhours+':'+'30 '+ampm;
}
}else{
       var newstarttime = starttime;
}



 

  jQuery('#bookingtime').timepicker({
  'minTime': newstarttime,
  'maxTime': endtime,
  'timeFormat': 'g:i A',
  'showDuration': false,
  });



  }
</script>

////////////// end code of 01-mar-2019 ////////






<!--------- end date picker script------>




<script type="text/javascript">


  function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }
 

 function OnKeyDown(event) { event.preventDefault(); };

 


	function getbeautyservice(selecteddateproduct,branchid,tbl){
		var Specialist = "@if (Lang::has(Session::get('lang_file').'.Specialist')!= '') {{  trans(Session::get('lang_file').'.Specialist') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Specialist') }} @endif";
    var PROFESSIONAL_BIO = "@if (Lang::has(Session::get('lang_file').'.PROFESSIONAL_BIO')!= '') {{  trans(Session::get('lang_file').'.PROFESSIONAL_BIO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PROFESSIONAL_BIO') }} @endif";
    var Dates = jQuery('#bookingdate').val();

if(Dates == '' || Dates == undefined)
{
  Dates = date("Y-m-d");
}

		var dateproductid=selecteddateproduct;

			  if(selecteddateproduct){
        jQuery.ajax({
           type:"GET",
           url:"{{url('clinicshop/getcartproduct')}}?product_id="+dateproductid+"&branchid="+branchid+"&tblfld="+tbl+"&chk="+Dates,
           success:function(res){  
           <?php $Cur = Session::get('currency'); ?>             
            if(res){
            	 var json = JSON.stringify(res);
			var obj = JSON.parse(json);
			//alert(obj);
  
            	
            		length=obj.productsericeinfo.length;
            		//alert(length);
            		
            		if(length>0){
		 for(i=0; i<length; i++)
			{
				if(obj.productprice==obj.productsericeinfo[i].pro_price){

					 TotalPricse = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					 
					var productrealprice='<span><?php echo $Cur; ?> '+TotalPricse+'</span>';
					var productrealpriceamount=TotalPricse;
				}else{
					 TotalPricsse = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					 productprices = getChangedPrice(obj.productprice); 
					var productrealprice='<span class="strike"><?php echo $Cur; ?> '+TotalPricsse+' </span><span> <?php echo $Cur; ?> '+obj.productprice+'</span>';
					var productrealpriceamount=productprices;
				}
				jQuery('#selectedproduct').html('<div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productsericeinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productsericeinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productsericeinfo[i].pro_desc+'</div><div class="service-beauty-description"><b>'+Specialist+' '+obj.productsericeinfo[i].specialistion+'</b></div><div class="service_sidebar_list"><div class="service_sidebar_list_title">{{ (Lang::has(Session::get('lang_file').'.PROFESSIONAL_BIO')!= '')  ?  trans(Session::get('lang_file').'.PROFESSIONAL_BIO'): trans($OUR_LANGUAGE.'.PROFESSIONAL_BIO')}}  </div><ul id="professional"></ul></div></div>');
					
				jQuery('#doct').html(''+obj.productsericeinfo[i].pro_title);
				jQuery('[name=product_id]').val(obj.productsericeinfo[i].pro_id);
				jQuery('[name=opening_time]').val(obj.productsericeinfo[i].opening_time);
				jQuery('[name=closing_time]').val(obj.productsericeinfo[i].closing_time);
				
				jQuery('[name=product_price]').val(productrealpriceamount);
				jQuery('[name=product_orginal_price]').val(productrealpriceamount);
				jQuery('#totalprice').html(productrealprice);
				jQuery('[name=itemqty]').val(1);
				document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur; ?> '+productrealpriceamount;

				
				jQuery('#bookingtimebasic').html('');
      

				jQuery('#bookingtimebasic').html('<input name="bookingtime" type="text" id="bookingtimesecond" onkeydown="OnKeyDown(event)" autocomplete="off" class="t-box cal-t" data-pro="'+obj.productsericeinfo[i].pro_id+'" data-shop="'+branchid+'" data-start="'+obj.productsericeinfo[i].opening_time+'" data-end="'+obj.productsericeinfo[i].closing_time+'"   />');
  
 
			}
 
			professionbiolength=obj.expertstaff.length;
			if(professionbiolength>0){
				for(k=0; k<professionbiolength; k++)
			{
				jQuery('#professional').append('<li>'+obj.expertstaff[k].attribute_title+'</li>');
			}

			}

			
			}
           
           }
           }
        });
    }
 $('html, body').animate({
         scrollTop: ($('.service-display-left').offset().top)
      }, 'slow');

		
	}

 jQuery('body').on('mouseover','#bookingtimesecond', function(){
 var t1 = jQuery(this).data('start');
 var t2 = jQuery(this).data('end');
 var pro = jQuery(this).data('pro');
 var shop = jQuery(this).data('shop');
var Dates = jQuery('#bookingdate').val(); 
   
  jQuery.ajax({
           type:"GET",
           url:"{{url('checkbookedtime')}}?product_id="+pro+"&branchid="+shop+"&chk="+Dates,
           success:function(res){  
                workerbookedslots= res.length;
                 workerbookedslot = [];
           
                  if(workerbookedslots>0){
                    for(lk=0; lk<workerbookedslots; lk++)
                    {
                     var inRange = [];                    
                    var tM = res[lk].split('~~');
                      inRange.push(tM[0]);
                      inRange.push(tM[1]);
                      workerbookedslot.push(inRange);
                     }
 

                  }
                  
                  jQuery('#bookingtimesecond').timepicker('remove'); 
                  
         jQuery('#bookingtimesecond').timepicker({
          'minTime': t1,
          'maxTime': t2,
          'timeFormat': 'g:i A',
          'dynamic' : true,
          'disableTimeRanges': workerbookedslot,
          'showDuration': false
        });
 
     
 
}});




 })

</script>




  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">

  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

 

</script>

@if(count($servicecategoryAndservices)>=1 && isset($servicecategoryAndservices[0]->serviceslist[0]->pro_id) && $servicecategoryAndservices[0]->serviceslist[0]->pro_id!='')

<script type="text/javascript"> 
jQuery(window).load(function(){
getbeautyservice('<?php echo $servicecategoryAndservices[0]->serviceslist[0]->pro_id;?>','<?php echo $branchid ?>','service_id');

})
 


</script>
@endif

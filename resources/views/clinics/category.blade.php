@include('includes.navbar')
<div class="outer_wrapper diamond_fullwidth"> @include('includes.header')
  <div class="inner_wrap">
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
      @include('includes.searchweddingandoccasions') </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper">@include('includes.mobile-modify')
      <div class="page-right-section">
<div class="diamond_main_wrapper"> @if(Session::get('lang_file')!='en_lang') <img src="{{ url('') }}/themes/images/clinic_ar.jpg" alt="" usemap="#hall-subcategory" hidefocus="true"> @else <img src="{{ url('') }}/themes/images/clinic.jpg" alt="" usemap="#hall-subcategory"> @endif
            <map name="hall-subcategory" id="hall-subcategory">  
			  <area shape="poly" coords="21,458,469,12,917,459,21,460" href="{{ url('') }}/cliniccosmeticsandlaser/28/29/" />
			<area shape="poly" coords="19,464,919,466,471,916,20,464" href="{{ url('') }}/clinicdentalanddermatology/28/30" />
            </map>
          </div>
        <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')
@include('includes.navbar')
<div class="outer_wrapper">
  <div class="inner_wrap"> @include('includes.header')
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
      @php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ @endphp
      @include('includes.searchweddingandoccasions')
      @php } @endphp
      @php if(Session::get('searchdata.mainselectedvalue')=='1'){ @endphp
      @include('includes.search')
      @php } @endphp </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper main_category">
      <div class="mobile-filter-line">
        <div class="mobile-budget"><span>Services</span></div>
        <div class="mobile-search"><span>Search</span></div>
      </div>
      @include('includes.customer-budget-section')
      <div class="page-right-section">
        <div class="diamond_main_wrapper"> <img src="{{ url('') }}/themes/images/diamond-9.jpg" alt="" usemap="#homeMap">
          <map name="homeMap" id="homeMap">
            <area shape="poly" coords="467,9,406,81,318,155,316,162,282,197,650,197" href="{{ url('') }}/type-of-halls/3" />
            <area shape="poly" coords="469,198,282,197,177,301,175,308,108,370,469,370" href="{{ url('') }}/foodcatgeory/7" alt="" />
            <area shape="poly" coords="654,197,468,198,469,272,469,296,471,369,827,370" href="#" />
            <area shape="poly" coords="106,373,306,373,307,547,305,548,97,546,15,464" href="#" />
            <area shape="poly" coords="308,373,628,371,626,547,572,546,309,544,309,489" href="{{ url('') }}/revivingconcerts/24" />
            <area shape="poly" coords="629,371,831,372,923,462,843,545,631,543,629,423" href="#" />
            <area shape="poly" coords="301,548,468,548,469,718,468,725,277,724,104,550" href="#" />
            <area shape="poly" coords="539,550,837,548,732,652,666,723,471,722,467,547" href="{{ url('') }}/travelagency/87" />
            <area shape="poly" coords="441,727,660,727,579,810,470,917,444,892,279,727" href="{{ url('') }}/carrental/37" />
          </map>
        </div>
        <center>
        </center>
        <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')
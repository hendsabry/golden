@include('includes.navbar')
<div class="outer_wrapper">
<div class="inner_wrap">
@include('includes.header')

</div> <!-- outer_wrapper -->
</div>
 <div class="header-page-divder">&nbsp;</div>
<div class="inner_wrap">
  <div class="inner_wrap">
 
 @php (Lang::has(Session::get('lang_file').'.SUBMIT')!= '')  ? $sub= trans(Session::get('lang_file').'.SUBMIT'): $sub=trans($OUR_LANGUAGE.'.SUBMIT');

  @endphp
  
 <a name="loginfrml"></a>
     <div class="forgot-password-area">  
	 <div class="form_title">{{ (Lang::has(Session::get('lang_file').'.FORGOT_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.FORGOT_PASSWORD'): trans($OUR_LANGUAGE.'.FORGOT_PASSWORD')}}</div>
   <div class="forgot-password-box">
   
   {!! Form::open(['url' => 'member_forgot_pwd_email/update', 'method' => 'post', 'name'=>'loginfrm', 'id'=>'loginfrm', 'enctype' => 'multipart/form-data']) !!}
    <div class="usersign-form">
    	@if (session('loginstatus'))
                        <div class="alert error">
                            {{ session('loginstatus') }}
                        </div>
                    @endif
  <div class="usersign-form-line">
  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.New_Password')!= '')  ?  trans(Session::get('lang_file').'.New_Password'): trans($OUR_LANGUAGE.'.New_Password')}}</div>
  <div class="usersign-form-bottom"><input required="" id="password" class="t-box" maxlength="80" name="password" type="password"></div>
  </div>

  <div class="usersign-form-top">{{ (Lang::has(Session::get('lang_file').'.Confirm_Password')!= '')  ?  trans(Session::get('lang_file').'.Confirm_Password'): trans($OUR_LANGUAGE.'.Confirm_Password')}}</div>
  <div class="usersign-form-bottom"><input required="" id="cpassword" class="t-box" maxlength="80" name="cpassword" type="password">
    <input type="hidden" name="userid" id="userid" value="{{ $email_address }}">
  </div>
  </div>
  
  <div class="usersign-form-button">{!! Form::submit($sub, array('class'=>'form-btn')) !!}</div>
  </div>
  {!! Form::close() !!}
   </div>
  
  </div> <!-- forgot-password-area -->
  
  
 

 



  






</div>  <!-- usersign-area -->






</div>





@include('includes.footer')

 <script type="text/javascript">
 
jQuery("#loginfrm").validate({
                  ignore: [],
                  rules: {         

                       password: {
                       required: true,
             minlength: 8
                      },
                      cpassword: {
                        required: true,
                        minlength: 8,
                        equalTo: "#password"
                    }
                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
             
          password: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($OUR_LANGUAGE.'.USER_PASSWORD'); @endphp",
                      },

              cpassword: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans($OUR_LANGUAGE.'.USER_PASSWORD'); @endphp",
                      },

                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 


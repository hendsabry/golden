<!doctype html>
<html><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 

 

<title> Golden Cages | 404 Page not found </title>
<link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/themes/images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="{{url('/')}}/themes/css/reset.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/common-style.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/stylesheet.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/demo.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/slider/css/flexslider.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/user-my-account.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/arabic.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface-media.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/diamond.css" rel="stylesheet" />


<!--  jQuery -->
<script type="text/javascript" src="http://ff.kis.v2.scr.kaspersky-labs.com/9F940E53-12D5-E443-B5A8-3795CF971BE1/main.js" charset="UTF-8"></script><script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />


<!--<script src="{{url('/')}}/themes/js/jquery-lib.js"></script>
<script src="{{url('/')}}/themes/js/height.js"></script>-->
<script src="{{url('/')}}/themes/js/jquery.flexslider.js"></script>
<script src="{{url('/')}}/themes/js/modernizr.js"></script>
<script src="{{url('/')}}/themes/js/jquery.mousewheel.js"></script>
<script src="{{url('/')}}/themes/js/demo.js"></script>
<script src="{{url('/')}}/themes/js/froogaloop.js"></script>
<script src="{{url('/')}}/themes/js/jquery.easing.js"></script>
 
</head>
<body class="">
<div class="popup_overlay"></div>
<div class="outer_wrapper">
<div class="nav_row homepading">
  
 
 
        <div class="left_nav">
            <ul>
                <li><a href="{{url('/')}}/about-us">About Us</a></li>
                <li><a href="{{url('/')}}/testimonials">Testimonials</a></li>
                <li><a href="{{url('/')}}/occasions">#Occasions</a></li>
				                <span class="mobile_fields"><li><a href="{{url('/')}}/login-signup/logoutuseraccount">Sign Out</a></li></span>
				            </ul>
        </div>
        <div class="middle_nav">
            <a class="brand" href="{{url('/')}}">
            <img src="{{url('/')}}/themes/images/logo.png" />
            </a>
        </div>



		 <div class="right_nav">

		 
			<ul>                   
			<li><a href="{{url('/')}}/login-signup/logoutuseraccount">Sign Out</a></li>
			<li> 
			<a href="#">
			<span class="lang_select" data-val="ar" onClick="Lang_change('ar')">عربى</span>
			</a>
			</li>
			<li><a href="{{url('/')}}/contact-us">Contact Us</a></li>
			 
			</ul>

		 </div>
		
		 
    </div><!-- nav_row -->
	
	<div class="mobile_logo_g">
	
            <a class="brand" href="{{url('/')}}">
            <img src="{{url('/')}}/themes/images/logo.png" />
            </a>
        
	
	
	</div>

 <div class="inner_wrap"> 
<div class="homepage_category" align="center">
<div class="ops_area">

<!--<div class="ops_img"><img src="{{url('/')}}/themes/images/cup.png" alt="" /></div>-->
<div class="ops_right_text"><h2 class="ops"><span class="op">Oops!</span> Page Not Found</h2>

<p class="ops_text">Seems one of our pages hasn't returned from its <i>coffee break</i>.
<!--However, we found the below stuff that can be useful to you.--></p></div>
   


</div>

            
</div> <!-- homepage_category -->


</div> <!-- innher_wrap -->
<div class="flower_bg"></div>
   
    
</div> <!-- outer_wrapper -->

 

 
<!-- MainBody End -->
<!-- Footer -->
<footer class="footer-section">  
<div class="footer-wrapper">
<div class="footer-menu"><a href="{{url('/')}}">Home</a> <span>|</span> <a href="{{url('/')}}/about-us">About Us</a> <span>|</span> <a href="{{url('/')}}/testimonials">Testimonials </a> <span>|</span> <a href="{{url('/')}}/occasions">#Occasions </a> <span>|</span> <a href="{{url('/')}}/privacy-policy">Privacy Policy </a> <span>|</span> <a href="{{url('/')}}/return-policy">Return Policy </a> <span>|</span> <a href="{{url('/')}}/contact-us">Contact Us </a></div> <!-- footer-menu -->

<div class="footer-info-section">
<div class="footer-info-box address-box">
<div class="footer-info-heading">Address </div>
<div class="foo-address-line foo-address">Dammam, Saudi Arabia</div>
<div class="foo-address-line foo-phone">+966 50 5331 627</div>
<div class="foo-address-line foo-mail"><a href="mailto:info@goldencages.com">info@goldencages.com</a></div>
</div> <!-- footer-info-box -->

<div class="footer-info-box payment-options-box">
<div class="footer-info-heading">Payment Options </div>
<div class="payment-option">&nbsp;</div>
</div> <!-- footer-info-box -->

<div class="footer-info-box secure-shopping-box">
<div class="footer-info-heading">Secure Shopping </div>
<div class="footer-norton"><img src="{{url('/')}}/themes/images/norton.jpg" alt="" /></div>
</div> <!-- footer-info-box -->

<div class="footer-info-box follow-box">
<div class="footer-info-heading">Follow Us </div>
<div class="footer-so-line">
<a href="#" target="_blank" title="Facebook" class="so-icon facebook-icon">&nbsp;</a>
<a href="#" target="_blank" title="twitter" class="so-icon twitter-icon">&nbsp;</a>
<a href="#" target="_blank" title="Google+" class="so-icon google-plus-icon">&nbsp;</a>
<a href="#" target="_blank" title="Instagram" class="so-icon instagram-icon">&nbsp;</a>
<a href="#" target="_blank" title="Pinterest" class="so-icon pinterest-icon">&nbsp;</a>
<a href="#" target="_blank" title="YouTube" class="so-icon youtube-icon">&nbsp;</a>
</div>
</div> <!-- footer-info-box -->

</div> <!-- footer-info-section -->
<div class="footer-menu mobile-footer-menu"><a href="#">Privacy Policy </a> <span>|</span> <a href="#">Return Policy  </a> </div>
<div class="footer-copyright">&copy; {{date('Y')}} Goldencages.com All Rights Reserved .</div>


 </div> <!-- footer-wrapper -->

</footer> <!-- footer-section -->

<div class="overlay"></div>
<div class="popup_overlay"></div>

 

<script>
jQuery(document).ready(function(){
	
/* mobile menu */	
jQuery('.heamburger').click(function(){
	jQuery('.left_nav').slideToggle(500);	
	jQuery('.overlay').css('display','block');
});
jQuery('.overlay').click(function(){
	jQuery('.left_nav').slideUp(500);	
	jQuery('.overlay').css('display','none');
});

/* mobile menu */	
jQuery('.vendor_heamburger').click(function(){
	jQuery('.vendor_navigation').slideToggle(500);	
	jQuery('.popup_overlay').css('display','block');
});
jQuery('.popup_overlay').click(function(){
	jQuery('.vendor_navigation').slideUp(500);	
	jQuery('.popup_overlay').css('display','none');
});

/* Home page search Popup Bussiness*/
jQuery('.bussiness_link').click(function(){
	jQuery('.bussiness_search_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});

jQuery('.popup_overlay').click(function(){
	jQuery('.bussiness_search_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});


/* Home page search Popup Wedding */
jQuery('.wedding_ocaasion').click(function(){
	jQuery('.search_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .close_search').click(function(){
	jQuery('.search_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});

/* Search page */
jQuery('.mobile-search').click(function(){
	jQuery('.search-section').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .mobile-back-arrow').click(function(){
	jQuery('.search-section').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});


/* Modify Row  */
jQuery('.form-btn').click(function(){
	jQuery('.modify-search-form').fadeOut(0);
	jQuery('.after-modify-search').fadeIn(0);
});

/* Oops Popup 
jQuery('.budgetopen').click(function(){
	jQuery('.oops_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .oops_btn').click(function(){
	jQuery('.oops_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});
*/
/**/


/* Service Lightbox */
jQuery('.serv_lightbox').click(function(){
	jQuery('.services_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.serv_pop_close, .popup_overlay').click(function(){
	jQuery('.services_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});

/* Search page */
jQuery('.mobile-budget').click(function(){
	jQuery('.customer-budget-section').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay, .mobile-back-arrow').click(function(){
	jQuery('.customer-budget-section').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});

/* Search page 
jQuery('.sticky_serivce').click(function(){
	jQuery('.other_serviceinc').css('right','0');
	jQuery('.othrserv_overl').fadeIn(500);
});*/
jQuery('.serv_delete, .othrserv_overl').click(function(){
	jQuery('.othrserv_overl').fadeOut(500);
	jQuery('.other_serviceinc').css('right','-300px');
});

  /* Review Popup */
jQuery('.review_popup_link').click(function(){
	var relData = $(this).attr('rel');
	var ArrData = relData.split('-');
	jQuery('.order_id').val(ArrData[0])
	jQuery('.shop_id').val(ArrData[1])
	jQuery('.vendor_id').val(ArrData[2])
	jQuery('.parentId').val(ArrData[3])
	jQuery('.commentId').val(ArrData[4])
	jQuery('.comts').val(ArrData[5])
	jQuery('.starinput').val(ArrData[6])
	if(ArrData[6]!='')
	{
	 if(ArrData[6]==1)
	 {
	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a>';
	 }
	 if(ArrData[6]==2)
	 {
	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a>'; 
	 }
	 if(ArrData[6]==3)
	 {
	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a><a href="#" class="fa-star-o fa"></a>'; 
	 }
	 if(ArrData[6]==4)
	 {
	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star-o fa"></a>'; 
	 }
	 if(ArrData[6]==5)
	 {
	    var starselect = '<a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a><a href="#" class="fa-star fa"></a>'; 
	 }
	 jQuery('.updateClass').html(starselect);
	}
	jQuery('.review_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);
});
jQuery('.popup_overlay').click(function(){
  jQuery('.review_popup').fadeOut(500);
  jQuery('.popup_overlay').fadeOut(500);
}); 

/* Checkout Payment Tabs */
jQuery('.paymt_tab li a').click(function(event){
	event.preventDefault();
	jQuery(this).parent().addClass('current-payment');
	jQuery(this).parent().siblings().removeClass('current-payment');		
	var tab = jQuery(this).attr('href');
	jQuery('.payment-option-inner-box').not(tab).hide();
	jQuery(tab).show();
});


/* Common Popup */
jQuery('.open_popup').click(function()
{    
	jQuery('.common_popup').fadeIn(500);
	jQuery('.popup_overlay').fadeIn(500);

	jQuery('#occasions_date').val('');
	jQuery('#return_date').val('');
	jQuery('#rentfinal_price').html('');
	
 
});
jQuery('.popup_overlay, .close_popup_btn').click(function(){
	jQuery('.common_popup').fadeOut(500);
	jQuery('.popup_overlay').fadeOut(500);
});




});

</script>

<link rel="stylesheet" href="{{url('/')}}/themes/js/scroll/jquery.mCustomScrollbar.css">	
<script src="{{url('/')}}/themes/js/scroll/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
jQuery(document).ready(function() { 
jQuery("#content-1").mCustomScrollbar({
	scrollButtons: {
		enable: true
	}
});

jQuery("#content-5").mCustomScrollbar({
	axis:"x",
	theme:"dark-thin",
	autoExpandScrollbar:true,
	advanced:{autoExpandHorizontalScroll:true}
});

jQuery("#menu_header").mCustomScrollbar({
	axis:"x",
	theme:"dark-thin",
	autoExpandScrollbar:true,
	advanced:{autoExpandHorizontalScroll:true}
});

jQuery("#kosha-tab").mCustomScrollbar({
	axis:"x",
	theme:"dark-thin",
	autoExpandScrollbar:true,
	advanced:{autoExpandHorizontalScroll:true}
});

});
</script>
 
 

<script type="text/javascript">
	function Lang_change(str) 
	{ 
		var language_code = str;
		var token =  jQuery.ajax
		({
			type:'GET',
            url:"{{url('/')}}/new_change_languages",
            data:{'Language_change':language_code,'csrf-token':token},
            success:function(data)
			{
				//alert(data);
				window.location.reload();
            }
        });
	}
</script>

 

</body>
</html> 
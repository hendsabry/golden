<html>
   <title>Available endpoints</title>
   <style>
   .main_page{margin:0px;}
   .main_page p.post_url{max-width: 880px;width: 100%;word-wrap: break-word;word-break: break-all;padding: 0px 5px;}
   .subheading{margin-left:20px;}
   .margleft{margin-left:20px;}
   .marglefttble{margin-left:50px;}
   .golden_cage{text-align:center;display:block; text-transform: uppercase;}
   .modules{border-bottom:#000 solid 1px;display:block; padding-bottom: 15px;}
   </style>
   <body>
      <style type='text/css'>.tg  {border-collapse:collapse;border-spacing:0;}.tg td{font-family:Courier;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}.tg th{font-family:Arial, sans-serif;font-size:13px;font-weight:bold;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}.tg .tg-yw4l{vertical-align:top}.tg .tg-y9vz{color:#036400}</style>
      <div class="main_page">
      <h2 class="golden_cage">Golden Cage Web API</h2>  
      <h2 class="modules">Module 1 - Hall</h2>	
   
      <h3 class="subheading">1. Home search</h3>
      <p class="subheading">/api/getSearchOption</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getSearchOption</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l '><p class="post_url">http://wisitech.in/api/getSearchOption?top_category=1&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOj
            E1MjUwNzU1MDQsIm5iZiI6MTUyNTA3NTUwNCwianRpIjoiUGRFUmNvbFprMzFjVHduOSIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5Zm
         QxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.vyBa0vhk9npmCit2mVBrQQwjBKxz8I7synji0lLsBT0 </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach option based on parent category(business meeting/wedding and ocassion) </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>top_category</th>
                     <td class='tg-yw4l'>To get top category(business meeting/wedding and ocassion)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get language(ar/en)</td>
                  </tr>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/search_category_option.png" style="height: 500px;"></td>
         </tr>
      </table>      
      <h3 class="subheading">2.  Home search Occasion Category </h3>
      <p class="subheading">/api/getSearchWeddingOccasion </p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getSearchWeddingOccasion </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getSearchWeddingOccasion?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjUxNzA0MDgsIm5iZiI6MTUyNTE3MDQwOCwianRpIjoiR24yMnF1bnpueURsUzl4eSIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.AB_kpNszdVainFt4QwHUrdsody0mMwom0HWEb_x80gE&top_category=2&language_type=en&type_of_occasion_id=1&number_of_guests=100&budget=100000&city_id=1&booking_date=12/10/2018&gender=1</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach Wedding Occasion data based on parent category(business meeting/wedding and ocassion) </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>top_category</th>
                     <td class='tg-yw4l'>To get top category(business meeting/wedding and ocassion)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To search user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>type_of_occasion_id</th>
                     <td class='tg-yw4l'>To search user occasion type</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>number_of_guests</th>
                     <td class='tg-yw4l'>To search  total number of guest</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>budget</th>
                     <td class='tg-yw4l'>To search user budget</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To search user city</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>booking_date</th>
                     <td class='tg-yw4l'>To search user booking_date</td>
                  </tr>
                   <tr>
                     <th class='tg-yw4l'>gender</th>
                     <td class='tg-yw4l'>User Gender</td>
                  </tr>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/wedding_occasion.png" style="height: 500px;"></td>           
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/wedding_occasion1.png" style="height: 500px;"></td>
         </tr>
      </table>      
      <h3 class="subheading">3.Business Meeting</h3>
      <p class="subheading">/api/getSearchBusinessMeeting </p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getSearchBusinessMeeting </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getSearchBusinessMeeting?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjQ4MTE0MjcsIm5iZiI6MTUyNDgxMTQyNywianRpIjoiaVFWMldVYkV1NXE1UTV0YiIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.RM4xi4RmACZtWzZmhCgHW8gDZMVpVC1gIIhmd6FRo2Q&top_category=1&language_type=en&type_of_business_id=1&number_of_attendence=100&budget=100000&city_id=1&start_date=12/10/2018&end_date=13/10/2018</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach business meeting data based on parent category(business meeting/wedding and ocassion) </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>top_category</th>
                     <td class='tg-yw4l'>To get top category(business meeting/wedding and ocassion)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>type_of_business_id</th>
                     <td class='tg-yw4l'>To get user business type</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>number_of_attendence</th>
                     <td class='tg-yw4l'>To get  total number of guest</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>budget</th>
                     <td class='tg-yw4l'>To get user budget</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To get user city</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>start_date</th>
                     <td class='tg-yw4l'>To get start date of business meeting</td>
                  </tr>
                   <tr>
                     <th class='tg-yw4l'>end_date</th>
                     <td class='tg-yw4l'>To get end date of business meeting</td>
                  </tr>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/business_meeting.png" style="height: 500px;"></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/business_meeting1.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">4.  Home search(sub category)</h3>
      <p class="subheading">/api/searchHallSubCategory</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/searchHallSubCategory </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/searchHallSubCategory?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjQ4MjE1OTgsIm5iZiI6MTUyNDgyMTU5OCwianRpIjoib0hiWmtVUG9qTnNyazFGWSIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.ysk6M5-Lmjhm-mlxvsS_vnlMdsm1TWAafOMthi-xdOw&category_id=3&language_type=EN</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach hall sub category data based on parent category(Halls) with pagination </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                  
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/hall_sub_category.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">5.  Home search(hall_vendor_list)</h3>
      <p class="subheading">/api/searchHallVendor</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/searchHallVendor </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/searchHallVendor?category_id=3&subcategory_id=4&city_id=4&language_type=ar&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI0ODEwOTUwLCJuYmYiOjE1MjQ4MTA5NTAsImp0aSI6ImJuOFV1M1hjZXlxWFQ2RVQiLCJzdWIiOjMyNiwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.1r0VDEUNyzm2SuDP7yfR255JLQSx-ONY88vphRrU5_k</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach hall's vendor list based on parent category(Hotel Halls) with pagination</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Hotel Halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To get city_id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                   
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/vendor_list.png" style="height: 500px;"></td>
         </tr>
      </table>      
      <h3 class="subheading">6. Home search(vendor_branch_list->halls)</h3>
      <p class="subheading">/api/searchVendorBranch</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/searchVendorBranch </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/searchVendorBranch?category_id=3&subcategory_id=4&vendor_id=40&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjQ4MjE1OTgsIm5iZiI6MTUyNDgyMTU5OCwianRpIjoib0hiWmtVUG9qTnNyazFGWSIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.ysk6M5-Lmjhm-mlxvsS_vnlMdsm1TWAafOMthi-xdOw&city_id=31</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach vendor's branch list based on parent category(Taj) with pagination</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Hotel Halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor(Taj)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To get City(Delhi)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                   
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/branch_list.png" style="height: 500px;"></td>
         </tr>
      </table>
       <h3 class="subheading">7. getHallList</h3>
      <p class="subheading">/api/getHallList</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getHallList </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getHallList?category_id=3&subcategory_id=4&vendor_id=40&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MzA3NjUyNDUsIm5iZiI6MTUzMDc2NTI0NSwianRpIjoieDF0TTR4c0dEbUd4ZEp2aiIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.e0DP6sus514nUjOqGB0dUhnrEaWzRb22jbsvpjTjv04&branch_id=43&hall_budget=50000&hall_budget_type=3&number_of_guests=500&page=1</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach hall data with pagination </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Hotel Halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor(Taj)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get hall</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>hall_budget</th>
                     <td class='tg-yw4l'>To get hall buget</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>hall_budget_type</th>
                     <td class='tg-yw4l'>To get hall buget type('1' for within budget and '2' for above budget and '3' for offer)</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>   
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                  
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/hall_list.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">8.  getShortHallDetail</h3>
      <p class="subheading">/api/getShortHallDetail</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getShortHallDetail</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getShortHallDetail?category_id=3&subcategory_id=4&vendor_id=40&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjUwNjQzODUsIm5iZiI6MTUyNTA2NDM4NSwianRpIjoiR0c0MWtKU0RkSTdCZnJ0bCIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.Bs1tF0ArSeqlb-RIg2_-KIpIfIpze4AckLCAFlJvOYA&branch_id=43&hall_budget=50000&hall_id=2</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get  short detail of hall.  </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Hotel Halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor(Taj)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get hall branch</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>hall_budget</th>
                     <td class='tg-yw4l'>To get hall buget</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>hall_id</th>
                     <td class='tg-yw4l'>To get hall detail</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/hall_short_detail.png" style="height: 500px;"></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/hall_short_detail1.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">9.  Home search(Full Hall Information)</h3>
      <p class="subheading">/api/getFullHallDetail</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getFullHallDetail</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getFullHallDetail?product_id=1&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjUwNjQzODUsIm5iZiI6MTUyNTA2NDM4NSwianRpIjoiR0c0MWtKU0RkSTdCZnJ0bCIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.Bs1tF0ArSeqlb-RIg2_-KIpIfIpze4AckLCAFlJvOYA&branch_id=43&category_id=3&subcategory_id=4&vendor_id=40</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get  full detail of hall.  </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Hotel Halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor(Taj)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get hall branch</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get hall detail</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/fullhalldetail.png" style="height: 500px;"></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/fullhalldetail1.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">10. Internal Food Menu</h3>
      <p class="subheading">/api/getHallInternalMenu</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getHallInternalMenu</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getHallInternalMenu?category_id=3&subcategory_id=4&vendor_id=40&language_type=ar&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjUwNjQzODUsIm5iZiI6MTUyNTA2NDM4NSwianRpIjoiR0c0MWtKU0RkSTdCZnJ0bCIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.Bs1tF0ArSeqlb-RIg2_-KIpIfIpze4AckLCAFlJvOYA&branch_id=43&hall_budget=50000&hall_id=1</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get  internal food menu.  </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Hotel Halls)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor(Taj)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get hall branch</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>hall_id</th>
                     <td class='tg-yw4l'>To get hall detail</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>hall_budget</th>
                     <td class='tg-yw4l'>To get hall buget</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/internal_food.png" style="height: 500px;"></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/internal_food1.png" style="height: 500px;"></td>
         </tr>
      </table>
       <h3 class="subheading">11. Set Hall Add to Cart</h3>
      <p class="subheading">/api/setHallAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setHallAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setHallAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjYwNDIwNzksIm5iZiI6MTUyNjA0MjA3OSwianRpIjoibWNIS1BSWFYxNXR3QWpTMSIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.RNhehnbhavkZgp8hcY1nBXdix_-y4U2JAVIaJ4A21ZY&user_id=10&product_id=4&cart_type=hall&container[cont_id][]=1&container[cont_id][]=2&product_option_value[]=1&product_option_value[]=2&container[qty][]=5&container[qty][]=10</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get  internal food menu.  </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart type detail(hall)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>container[cont_id][]</th>
                     <td class='tg-yw4l'>To get container detail(In array format)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>container[qty][]</th>
                     <td class='tg-yw4l'>To get container quantity(In array format)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_option_value[]</th>
                     <td class='tg-yw4l'>To get product value(In array format)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>user_id</th>
                     <td class='tg-yw4l'>To get user detail</td>
                  </tr>                 
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data Sucessfully Inserted </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/set_add_to_cart.png" style="height: 500px;"></td>
         </tr>
      </table>
       <h3 class="subheading">12. Get Hall Add to Cart</h3>
      <p class="subheading">/api/getHallAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getHallAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">https://wisitech.in/api/getHallAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjYyODQ1MTEsIm5iZiI6MTUyNjI4NDUxMSwianRpIjoiNDJBZm5hMFZ0Qm9zb3I5YSIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.KbxgNSCyR2u03OP4bKElu38oytSzsdjNTPeA6chER5g&product_id=1&language_type=ar</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get  data from add to cart.  </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_id</th>
                     <td class='tg-yw4l'>To get all user data from add to cart</td>
                  </tr>              
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/get_add_tocart.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h2 class="modules"></h2> 
      <h2 class="modules">Module 2 - Food</h2>
      <h3 class="subheading">1.  Home search(sub category) The Food</h3>
      <p class="subheading">/api/searchSubCategory</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/searchSubCategory </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/searchSubCategory?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjQ4MjE1OTgsIm5iZiI6MTUyNDgyMTU5OCwianRpIjoib0hiWmtVUG9qTnNyazFGWSIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.ysk6M5-Lmjhm-mlxvsS_vnlMdsm1TWAafOMthi-xdOw&category_id=7&language_type=EN</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach  sub category data based on parent category(Food) with pagination </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                  
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/foodsubcatg.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">2.  Home search(sub-sub category) The Food</h3>
      <p class="subheading">/api/searchVendor</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/searchVendor </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/searchVendor?category_id=11&subcategory_id=14&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5OTg5NTk3LCJuYmYiOjE1Mjk5ODk1OTcsImp0aSI6ImNjSnBJV0x4YWZJUEIzQzYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.IBKhXOmmWQ-l_QWQwqt685y2kwnaoR-aZQBT5x51VGA&city_id=31</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach vendor list based on parent category(Dates) with pagination</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Dates)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To get city_id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                   
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/food_vendor.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">3.  Home search(sub-sub category info) Date Shop info for The Food category</h3>
      <p class="subheading">/api/getDateFoodShopInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getDateFoodShopInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getDateFoodShopInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE&category_id=7&language_type=en&subcategory_id=10&vendor_id=72</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Dates Food Shop Info based on parent category(Vendor)</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Dates)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail(Rajasthani Sweets)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                  
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/datesfoodinfo.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">4.  Home search(sub-sub category info) Dessert Shop info for The Food category</h3>
      <p class="subheading">/api/getDessertFoodShopInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getDessertFoodShopInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getDessertFoodShopInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3MjIxNjU0LCJuYmYiOjE1MjcyMjE2NTQsImp0aSI6ImtEUEJJTjVsTjlIMFZTZTgiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.mupjHWJYfd1eiwZFhSqjlapc5EBC_DGlyqRndQYDR7Y&category_id=7&subcategory_id=9&vendor_id=67&language_type=en&branch_id=109</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Dessert Food Shop Info based on parent category(Vendor)</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Dates)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail(Rajasthani Sweets)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                  
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/foodshopinfo.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">4.   Food Menu</h3>
      <p class="subheading">/api/getFoodMenu</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getFoodMenu </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getFoodMenu?category_id=7&subcategory_id=8&vendor_id=62&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3MjIxNjU0LCJuYmYiOjE1MjcyMjE2NTQsImp0aSI6ImtEUEJJTjVsTjlIMFZTZTgiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.mupjHWJYfd1eiwZFhSqjlapc5EBC_DGlyqRndQYDR7Y&branch_id=108</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Shop's Food Menu</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category(Buffet)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail(    Agrawal Sweets)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                  
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/foodmenu.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">5.   FoodAddToCart</h3>
      <p class="subheading">/api/setFoodAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setFoodAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setFoodAddToCart?product_id=63&cart_type=food&attribute_id=44&product_option_value=51&product_qty=50&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTMwMjYyMzUzLCJuYmYiOjE1MzAyNjIzNTMsImp0aSI6ImlZUjJJdnlZMjh3Rkd3a04iLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.OB6XGXz3UdOZQ3z-cCp-FTqmXTRqcrcIKh9yk32XxYA&cart_sub_type=dessert</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To set Food data in add to cart for dates and dessert. </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product(Food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart type(food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type(date/dessert)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_option_value</th>
                     <td class='tg-yw4l'>product_option_value ids(In array format)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>shop_by_qty</th>
                     <td class='tg-yw4l'>To get shop by product quantity(per kilo/per piece)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>container_qty</th>
                     <td class='tg-yw4l'>To get container quantity</td>
                  </tr>                  
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Food Data sucessfully add in cart </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>Issue in cart entery</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/food_addtocart.png" style="height: 500px;"></td>
         </tr>
      </table>   
       <h3 class="subheading">5. External Food Add To Cart</h3>
      <p class="subheading">/api/setExternalFoodAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setExternalFoodAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in//api/setExternalFoodAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI2MzYxMTM3LCJuYmYiOjE1MjYzNjExMzcsImp0aSI6IndYeWNRSXFhdGFtNzdZTUciLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.76U34H9N-ETJnl4yo4cyRDHwc5y7e2jTwKHeutxfXTI&user_id=10&product_id=62&cart_type=food&product_option_value[]=47&container[cont_id][]=24&container[cont_id][]=26&product_option_value[]=48&container[qty][]=4&container[qty][]=5</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To set Food data in add to cart for dates and dessert. </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product(Food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart type(food)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_option_value</th>
                     <td class='tg-yw4l'>product_option_value ids(In array format)</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>container[cont_id]</th>
                     <td class='tg-yw4l'>To get container option(In array format)</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>container[qty]</th>
                     <td class='tg-yw4l'>To get container quantity(In array format)</td>
                  </tr>                 
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>External Food Data sucessfully add in cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>Issue in cart entery</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/external_food_addtocart.PNG" style="height: 500px;"></td>
         </tr>
      </table>  
       <h3 class="subheading">6. Get Food Add To Cart Data</h3>
      <p class="subheading">/api/getFoodAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getFoodAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getFoodAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1Mjc3Mzg3ODcsIm5iZiI6MTUyNzczODc4NywianRpIjoiSlA1ajBoSGJSMkZsbzgwTyIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.oFfxE9g9M6FuTeFySvCJDww60QSSL72QzRJGgVBBUO4&language_type=en&product_id=78</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get language_type(en/ar) </td>
                  </tr>                    
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getmakeup_addtocart.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">7. Delete Food Data From Add To Cart Data</h3>
      <p class="subheading">/api/deleteFoodData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteFoodData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteFoodData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&product_id=78
      </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>                 
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Successfully deleted your cart data</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/delete_beauty.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">8. Delete Buffet Food Data From Add To Cart Data</h3>
      <p class="subheading">/api/deleteBuffetData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteBuffetData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteBuffetData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&product_id=108</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>                 
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Successfully deleted your cart data</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/delete_beauty.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h2 class="modules"></h2> 
      <h2 class="modules">Module 3 - Beauty and Elegance</h2>  
   
      <h3 class="subheading">1.  get Saloon Info</h3>
      <p class="subheading">/api/getSaloonInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getSaloonInfo </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getSaloonInfo?category_id=18&subcategory_id=23&vendor_id=83&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1Mjk2NDQ2MDAsIm5iZiI6MTUyOTY0NDYwMCwianRpIjoiMWEyMmRBck55cGZscVlXNCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.v-Ol6Izh-ULgvpgj5NPKENLVp4k_4kpjzCWoQjL66Q8</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Saloon and Makeup Info based on Vendor detail. Its works for "Beauty Centers (Women concern), Men's Barber Saloon, Makeup (Cosmetics) and Spa."</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Beauty and Elegance)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/saloon.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">2.  Get Makeup Artist Info</h3>
      <p class="subheading">/api/getMakeupArtistInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getMakeupArtistInfo </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getMakeupArtistInfo?category_id=18&subcategory_id=22&vendor_id=82&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Makeup Artist Info based on Vendor detail</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Beauty and Elegance)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/makeup_artist.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">3.  Get Beauty centers and Spa Info</h3>
      <p class="subheading">/api/getBeautyAndSpaInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getBeautyAndSpaInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getBeautyAndSpaInfo?category_id=18&subcategory_id=19&vendor_id=79&language_type=en
            &token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1Mjk2NDQ2MDAsIm5iZiI6MTUyOTY0NDYwMCwianRpIjoiMWEyMmRBck55cGZscVlXNCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.v-Ol6Izh-ULgvpgj5NPKENLVp4k_4kpjzCWoQjL66Q8&branch_id=110</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Spa and Beauty Centers Info based on Branch detail</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Beauty and Elegance)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get branch detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/spa_beauty_info.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">4.  Get Available Worker Info</h3>
      <p class="subheading">/api/getWorkerList</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getWorkerList </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getWorkerList?vendor_id=80&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE&date=17-05-2018&time=11:00&product_id=84</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Available worker list</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>To get booking date by user</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>time</th>
                     <td class='tg-yw4l'>To get booking time by user</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/worker_list.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">5.  Set Book Appointment</h3>
      <p class="subheading">/api/setBookAppointment</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setBookAppointment </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setBookAppointment?vendor_id=80&product_id=95&staff_id=1&date=17-05-2018&time=11:30&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NTAxMTU0LCJuYmYiOjE1Mjk1MDExNTQsImp0aSI6Im90R3lKa2M4WGRaYWVRZDYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.YqzNFWbzDEje0Vcf-JPQ6Foq22_FTJDr5LK4qiR8T_w&cart_type=beauty&attribute_id=16&cart_sub_type=men_saloon&home</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To set data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>staff_id</th>
                     <td class='tg-yw4l'>To get staff detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart type detail(Beauty)</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>To get booking date by user</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>time</th>
                     <td class='tg-yw4l'>To get booking time by user</td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute_id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type detail (makeup_artists/makeup/spa/beauty_centers/men_saloon) </td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>home</th>
                     <td class='tg-yw4l'>To get home visit id</td>
                  </tr>                    
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Your Appoinment Data sucessfully add in cart </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>'Issue in cart entery</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/book_appointment.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">6. Set Makeup Add To Cart Data</h3>
      <p class="subheading">/api/setMakeupAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setMakeupAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setMakeupAddToCart?product_id=93&cart_type=beauty&attribute_id=24&product_qty=3&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NTAxMTU0LCJuYmYiOjE1Mjk1MDExNTQsImp0aSI6Im90R3lKa2M4WGRaYWVRZDYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.YqzNFWbzDEje0Vcf-JPQ6Foq22_FTJDr5LK4qiR8T_w&cart_sub_type=makeup</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart type detail(Beauty)</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute_id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type detail (makeup) </td>
                  </tr>                  
                  <tr>
                     <th class='tg-yw4l'>product_qty</th>
                     <td class='tg-yw4l'>To get product qty</td>
                  </tr>                    
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Makeup Data sucessfully added  in cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>'Issue in cart entery</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/makeup_addtocart.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">7. Get Makeup Add To Cart Data</h3>
      <p class="subheading">/api/getMakeupAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getMakeupAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getMakeupAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1Mjk2NDkxNTgsIm5iZiI6MTUyOTY0OTE1OCwianRpIjoiTXJBU25nNjk4WHI5T01ORCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.3FMOCymqliq_4CD9mO_s_nYMqTrtjDodjgSp_2bxG4Y&language_type=en&product_id=93</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get language_type(en/ar) </td>
                  </tr>                    
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getmakeup_addtocart.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">8. Delete Data From Add To Cart Data</h3>
      <p class="subheading">/api/deleteBeautyData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteBeautyData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteBeautyData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1Mjk2NDkxNTgsIm5iZiI6MTUyOTY0OTE1OCwianRpIjoiTXJBU25nNjk4WHI5T01ORCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.3FMOCymqliq_4CD9mO_s_nYMqTrtjDodjgSp_2bxG4Y&product_id=95</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>                 
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Successfully deleted your cart data</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/delete_beauty.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h2 class="modules"></h2> 
      <h2 class="modules">Module 4 - Clinics</h2> 
      
      <h3 class="subheading">1. Home search(vendor_branch_list)</h3>
      <p class="subheading">/api/searchBranch</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/searchBranch </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/searchBranch?category_id=28&subcategory_id=29&vendor_id=84&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjQ4MjE1OTgsIm5iZiI6MTUyNDgyMTU5OCwianRpIjoib0hiWmtVUG9qTnNyazFGWSIsInN1YiI6MzI2LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.ysk6M5-Lmjhm-mlxvsS_vnlMdsm1TWAafOMthi-xdOw&city_id=31</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach vendor's branch list based on parent category(Clinic) with pagination</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(clinic)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To get City(Delhi)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                   
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/branch_list.png" style="height: 500px;"></td>
         </tr>
      </table> 
   
      <h3 class="subheading">2.  get Clinic Info</h3>
      <p class="subheading">/api/getClinicInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getClinicInfo </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getClinicInfo?category_id=28&subcategory_id=29&vendor_id=84&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE&branch_id=104</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Clinic Info based on Vendor detail.</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Clinic)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get branch detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>It seems no Doctor are available with the clinic category.</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/clinic_info.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">3.  Get Doctor Booking List</h3>
      <p class="subheading">/api/getDoctorBookingList</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getDoctorBookingList</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getDoctorBookingList?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI2OTc5Mjk1LCJuYmYiOjE1MjY5NzkyOTUsImp0aSI6IjEzRkhjVzRSWXpqSGR5d0ciLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.xrB9LDF3nVHDP96aO2P5MZTNyJS4V8lX615ivQInBPM&branch_id=104&product_id=105&attribute_id=27</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get doctor booking list</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get branch detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get service attribute detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>Booking is not available.</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/booked_time.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">4. Booked Doctor's Appointment.</h3>
      <p class="subheading">/api/setBookDoctorAppointment</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setBookDoctorAppointment</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setBookDoctorAppointment?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NjQ0NzIwLCJuYmYiOjE1Mjk2NDQ3MjAsImp0aSI6ImJHUEVlQjJJcVp5SXJiTTIiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.jVBoCUjLVFoWNC4XKw2hIo0Py2Gcr9RWhGPTnb1EsTM&branch_id=104&product_id=105&attribute_id=27&date=25-05-2018&time=13:00&cart_type=Clinic&cart_sub_type=cosmetic&file_no=12345</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To set doctor's appointment data in add to cart. </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get branch detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get service attribute detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>To get booking date</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>time</th>
                     <td class='tg-yw4l'>To get booking time</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart type(Clinic)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type(cosmetic/skin)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>                
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Your Appoinment Data sucessfully added in cart </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>Issue in cart entery.</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setclinic_addtocart.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">5. Get Clinic Add To Cart Data</h3>
      <p class="subheading">/api/getClinicAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getClinicAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getClinicAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NzMyMDY3LCJuYmYiOjE1Mjk3MzIwNjcsImp0aSI6IjB6aUlMRG01bHA0TldXYnkiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.6PZBrc_8NUCj-1hAh-_8gqdqZiSXXsOdyIzS0awmDQY&product_id=105&language_type=en</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get language_type(en/ar) </td>
                  </tr>                    
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getclinic_addtocart.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">6. Delete Data From Add To Cart Data</h3>
      <p class="subheading">/api/deleteClinicData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteClinicData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteClinicData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NzMyMDY3LCJuYmYiOjE1Mjk3MzIwNjcsImp0aSI6IjB6aUlMRG01bHA0TldXYnkiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.6PZBrc_8NUCj-1hAh-_8gqdqZiSXXsOdyIzS0awmDQY&product_id=105</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>                 
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Successfully deleted your cart data</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/delete_clinic.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h2 class="modules"></h2> 
      <h2 class="modules">Module 5 - Reviving Concerts</h2> 
      
      <h3 class="subheading">1. Home search(Music And Band List)</h3>
      <p class="subheading">/api/getMusicAndBandList</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getMusicAndBandList </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getMusicAndBandList?category_id=24&subcategory_id=25&city_id=45&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach music and band list based on parent category(Reviving Concerts) with pagination</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Music)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To get City(Delhi)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                   
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/music_band_list.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">2. Get Music And Band Info</h3>
      <p class="subheading">/api/getMusicAndBandInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getMusicAndBandInfo </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getMusicAndBandInfo?category_id=24&subcategory_id=25&product_id=8&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get serach music and band Info based on parent category(Reviving Concerts) with pagination</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category(Music)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail(singer/band)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>page</th>
                     <td class='tg-yw4l'>To paginate</td>
                  </tr>                   
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/music_band_info.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">3. Service Inquiry of Singer's and Band</h3>
      <p class="subheading">/api/serviceInquiry</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/serviceInquiry </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/serviceInquiry?product_id=8&hall=hall 1&occasion_type=birthday&date=10-08-2018&time=11:00&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3MzA4Mjk3LCJuYmYiOjE1MjczMDgyOTcsImp0aSI6IlpPVnVDSnBpdTMyNmx4c20iLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.UbBDIZZmYg-bSwcyeU25SSNfB5vuu54F5NhbH6sKCqA&language_type=en&city_id=45&location=delhi&duration=2&user_comment=testing</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Service Inquiry  of singer's and Band</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>Input product detail(singer/band)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>hall</th>
                     <td class='tg-yw4l'>Input hall name</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>occasion_type</th>
                     <td class='tg-yw4l'>Input occasion type</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>Input date</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>time</th>
                     <td class='tg-yw4l'>Input time</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>Input city id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>location</th>
                     <td class='tg-yw4l'>Input location</td>
                  </tr>
                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>duration</th>
                     <td class='tg-yw4l'>Input duration</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>user_comment</th>
                     <td class='tg-yw4l'>Input Comments</td>
                  </tr>                   
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/music_band_service_inquiry.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">4. Get Music Acoustic Info</h3>
      <p class="subheading">/api/getMusicAcousticInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getMusicAcousticInfo </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getMusicAcousticInfo?category_id=24&subcategory_id=25&vendor_id=103&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Music Acoustic Info</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To Get category detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>
                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>               
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/music_accoustic_info.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">5. Get Music Acoustic Inquiry</h3>
      <p class="subheading">/api/getMusicAcousticInquiry</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getMusicAcousticInquiry </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getMusicAcousticInquiry?category_id=103&the_groooms_name=xyz&hall=hall 1&occasion_type=birthday&date=10-08-2018&time=11:00&recording_secation=delhi&music_type=xyz&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3MzA4Mjk3LCJuYmYiOjE1MjczMDgyOTcsImp0aSI6IlpPVnVDSnBpdTMyNmx4c20iLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.UbBDIZZmYg-bSwcyeU25SSNfB5vuu54F5NhbH6sKCqA&language_type=ar&singer_name=Himesh</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get Music Acoustic Inquiry</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>Input category detail(singer/band)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>the_groooms_name</th>
                     <td class='tg-yw4l'>Input the groooms name</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>recording_secation</th>
                     <td class='tg-yw4l'>Input the recording secation name</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>hall</th>
                     <td class='tg-yw4l'>Input hall name</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>occasion_type</th>
                     <td class='tg-yw4l'>Input occasion type</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>Input date</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>time</th>
                     <td class='tg-yw4l'>Input time</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>Input city id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>location</th>
                     <td class='tg-yw4l'>Input location</td>
                  </tr>
                  
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>duration</th>
                     <td class='tg-yw4l'>Input duration</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>user_comment</th>
                     <td class='tg-yw4l'>Input Comments</td>
                  </tr>               
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Data found </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/music_accoustic_inquiry.png" style="height: 500px;"></td>
         </tr>
      </table>  
      <h3 class="subheading">6. Set Music Acoustic Add To Cart</h3>
      <p class="subheading">/api/MusicAcousticAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/MusicAcousticAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/MusicAcousticAddToCart?product_id=104&cart_type=music&product_option_value=59&product_qty=5&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE&rental_date=10-6-2018&return_date=12-6-2018&rental_time=12:00&return_time=15:00</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set Music Acoustic data in Add To Cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart type(music)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_option_value</th>
                     <td class='tg-yw4l'>To get product option value</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_qty</th>
                     <td class='tg-yw4l'>To get product quantity</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>rental_date</th>
                     <td class='tg-yw4l'>To get rental date(In case of rent)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>rental_time</th>
                     <td class='tg-yw4l'>To get rental time(In case of rent)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>return_date</th>
                     <td class='tg-yw4l'>To get return date(In case of rent)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>return_time</th>
                     <td class='tg-yw4l'>To get return time(In case of rent)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Successfully data added in cart </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/accoustic_buy.png" style="height: 500px;"></td>
         </tr>
      </table>  
      <h3 class="subheading">7. Get Music Acoustic Add To Cart</h3>
      <p class="subheading">/api/getMusicAcousticAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getMusicAcousticAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">https://wisitech.in/api/getMusicAcousticAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjY3MDQ3NzUsIm5iZiI6MTUyNjcwNDc3NSwianRpIjoiQ3N5TTV2Y1pRQlFtbk9GZCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.dNN_VYkWVtnrmEkQj2fPEG8MKmAS3vpEZgiYAzvpsUE&language_type=ar&product_id=104</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get Music Acoustic data from Add To Cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/accoustic_buy.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">8. Delete Data From Add To Cart Data</h3>
      <p class="subheading">/api/deleteMusicAcousticData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteMusicAcousticData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteMusicAcousticData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1Mjk3Mjk0MzksIm5iZiI6MTUyOTcyOTQzOSwianRpIjoicmJrQ0RYY3FqTXE3OElqaSIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.LYw9BReBCwiro5VQ8Clfm7RsbrCzoiyddJwFhzQmQMM&product_id=104</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>                   
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>                 
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Successfully deleted your cart data</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>service not foud</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/delete_music.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h2 class="modules">Module 6 -Occasion Co-ordinator</h2> 
      <h3 class="subheading">1. Get Cosha Info</h3>
      <p class="subheading">/api/getCoshaInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getCoshaInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getCoshaInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&category_id=11&subcategory_id=12&language_type=en&vendor_id=122</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/cosha_detail.png" style="height: 500px;"></td>
         </tr>
      </table>  
      <h3 class="subheading">2. Set Cosha Add To Cart</h3>
      <p class="subheading">/api/setCoshaAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setCoshaAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setCoshaAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1Mjg2MTI3MTQsIm5iZiI6MTUyODYxMjcxNCwianRpIjoiNWloRmduT2ZYUjM0SEFJRCIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.nWaSJBmddhWmrgBgFZD0UwRLtqRa1ZdyLrcV0ds2kN4&product_id=122&cart_type=occasion&attribute_id=47&product[attribute_id][]=56&product[pro_id][]=159&product[attribute_id][]=57&product[pro_id][]=160&product[attribute_id][]=58&product[pro_id][]=121&product[attribute_id][]=59&product[pro_id][]=158&cart_sub_type=cosha</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product[pro_id]</th>
                     <td class='tg-yw4l'>product[pro_id] should we a array format</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>product[attribute_id]</th>
                     <td class='tg-yw4l'>product[attribute_id] should we a array format</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (occasion)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (cosha)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr>    

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setCosha.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">3. Get Cosha Data From Add To Cart</h3>
      <p class="subheading">/api/getCoshaAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getCoshaAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://dev.goldencage.com/api/getCoshaAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4NjIxMzU1LCJuYmYiOjE1Mjg2MjEzNTUsImp0aSI6ImlWbVJHYUx0blR5N21pYkgiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.lZYoGkTbysq0_oXyUefFQcXfWMPqMZLH2m9tfQRvgjM&product_id=122&language_type=en</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getCosha.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">4. Get Photography And Studio Info</h3>
      <p class="subheading">/api/getPhotographyStudioInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getPhotographyStudioInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getPhotographyStudioInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&category_id=11&subcategory_id=127&language_type=ar&vendor_id=130
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of Photography And Studio</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/photography_detail.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">5. Set Photography Add To Cart</h3>
      <p class="subheading">/api/setPhotographyStudioAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setPhotographyStudioAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://dev.goldencage.com/api/setPhotographyStudioAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&product_id=130&cart_type=occasion&attribute_id=67&cart_sub_type=video</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (occasion)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (cosha)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr>    

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setPhotography.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">6. Get Photography Data From Add To Cart</h3>
      <p class="subheading">/api/getPhotographyAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getPhotographyAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getPhotographyAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4NjIxMzU1LCJuYmYiOjE1Mjg2MjEzNTUsImp0aSI6ImlWbVJHYUx0blR5N21pYkgiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.lZYoGkTbysq0_oXyUefFQcXfWMPqMZLH2m9tfQRvgjM&product_id=130&language_type=en</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getPhotography.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">7. Get Roses And Package Info</h3>
      <p class="subheading">/api/getRosesPackageInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getRosesPackageInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getRosesPackageInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&category_id=11&subcategory_id=15&language_type=ar&vendor_id=124
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of Roses And Package</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/roses.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">8. Set Roses Package Add To Cart</h3>
      <p class="subheading">/api/setRosesPackageAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setRosesPackageAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setRosesPackageAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&product_id=127&cart_type=occasion&attribute_id=50&cart_sub_type=roses&product_option_value[]=64&product_option_value[]=65&product_qty=5</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (occasion)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (cosha)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>product_option_value</th>
                     <td class='tg-yw4l'>product_option_value should be in array format</td>
                  </tr>   

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setRoses.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">9. Get Roses Package Data From Add To Cart</h3>
      <p class="subheading">/api/getRoseAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getRoseAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getRoseAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4NjIxMzU1LCJuYmYiOjE1Mjg2MjEzNTUsImp0aSI6ImlWbVJHYUx0blR5N21pYkgiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.lZYoGkTbysq0_oXyUefFQcXfWMPqMZLH2m9tfQRvgjM&product_id=123&language_type=en
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getRoses.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">10. Get Special Event Info</h3>
      <p class="subheading">/api/getSpecialEventInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getSpecialEventInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getSpecialEventInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&category_id=11&subcategory_id=17&language_type=en&vendor_id=126
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of Special Event</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/special_events.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">11. Set Special Event Add To Cart</h3>
      <p class="subheading">/api/setSpecialEventAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setSpecialEventAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setSpecialEventAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&product_id=125&cart_type=occasion&attribute_id=55&cart_sub_type=events&product_qty=1</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (occasion)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (cosha)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>product_qty</th>
                     <td class='tg-yw4l'>To get product quantity</td>
                  </tr>   

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setEvents.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">11. Get Event Data From Add To Cart</h3>
      <p class="subheading">/api/getEventAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getEventAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getEventAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&language_type=en
            &product_id=125
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getEvents.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">12. Get Reception And Hospitality Info</h3>
      <p class="subheading">/api/getReceptionAndHospitalityInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getReceptionAndHospitalityInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getReceptionAndHospitalityInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTMwNTk3OTQwLCJuYmYiOjE1MzA1OTc5NDAsImp0aSI6InhSeTFFUFpvanlCVUpkVVciLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.4ylmbyBvh_BEuo_dYKnRzLfEEwS2c5JXdJGuQ0W8pFw&category_id=11&subcategory_id=14&language_type=en&vendor_id=123 
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of Special Event</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/hospitality.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">13. Set Hospitality Add To Cart</h3>
      <p class="subheading">/api/setHospitalityAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setHospitalityAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setHospitalityAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4Nzc2ODUyLCJuYmYiOjE1Mjg3NzY4NTMsImp0aSI6ImZjb2Z2UXMwd29hbkhrcmciLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.EAdXoR7HEsV0KLEEb1pBNgde5k8tYuSIDpa8UEhgFhk&product_id=131&cart_type=occasion&attribute_id=49&products[]=&products[]=&cart_sub_type=hospitality&no_of_staff=&nationality=&date=&time=</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (occasion)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (hospitality)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>products</th>
                     <td class='tg-yw4l'>products field should be array format</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>no_of_staff</th>
                     <td class='tg-yw4l'>To get number of staff</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>nationality</th>
                     <td class='tg-yw4l'>To get nationality</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>To get schedule date</td>
                  </tr>   

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/sethopitality.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">14. Get Hospitality Data From Add To Cart</h3>
      <p class="subheading">/api/getHospitalityAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getHospitalityAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getHospitalityAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4Nzc2ODUyLCJuYmYiOjE1Mjg3NzY4NTMsImp0aSI6ImZjb2Z2UXMwd29hbkhrcmciLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.EAdXoR7HEsV0KLEEb1pBNgde5k8tYuSIDpa8UEhgFhk&product_id=131&language_type=en
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getEvents.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">15. Get Electronic Invitations Info</h3>
      <p class="subheading">/api/getElectronicInvitations</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getElectronicInvitations</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getElectronicInvitations?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&category_id=11&subcategory_id=16&language_type=en&vendor_id=125
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of Special Event</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/invitation.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">16. Set Electronic Invitation Add To Cart</h3>
      <p class="subheading">/api/setElectronicInvitationAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setElectronicInvitationAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setElectronicInvitationAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&product_id=16&cart_type=occasion&cart_sub_type=invitations&occasion_name=abc&occasion_type=abc&venue=delhi&date=25-8-2018&time=10:00&invitation_type=1&invitation_data=aban&invitation_card=</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (occasion)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (invitation)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>occasion_name</th>
                     <td class='tg-yw4l'>To get occasion name detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>occasion_type</th>
                     <td class='tg-yw4l'>To get occasion type</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>venue</th>
                     <td class='tg-yw4l'>To get venue detail</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>To get schedule date</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>time</th>
                     <td class='tg-yw4l'>To get schedule time</td>
                  </tr> 

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setInvitation.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">16. Get Invitation Data From Add To Cart</h3>
      <p class="subheading">/api/getInvitationAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getInvitationAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getInvitationAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&language_type=en&product_id=16
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getInvetation.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">16. Get Designer Card Detail</h3>
      <p class="subheading">/api/getDesignerCard</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getDesignerCard</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getDesignerCard?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&product_id=16
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get designer card detail</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/designer_card.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">17. Delete Data From Add To Cart</h3>
      <p class="subheading">/api/deleteOccasionData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteOccasionData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteOccasionData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NDE1ODM2LCJuYmYiOjE1Mjk0MTU4MzYsImp0aSI6Im5DWUZWNHYxTUFmSmZjWlkiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.Zd9ak3e-OygnrQBcIPVDYkDmc_tOzvTkxA5eVXnqLxk&product_id=122
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Delete data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/delete_occasion.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h2 class="modules">Module 7 - Car Rental</h2> 
      <h3 class="subheading">1. Get Car Rental Info</h3>
      <p class="subheading">/api/getCarRentalInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getCarRentalInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getCarRentalInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&category_id=37&subcategory_id=86&language_type=en</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr>            
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/car_rental.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">2. Set Car Rental Add To Cart</h3>
      <p class="subheading">/api/setCarRentalAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setCarRentalAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setCarRentalAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4MjYwNjc3LCJuYmYiOjE1MjgyNjA2NzcsImp0aSI6IkJFRlRlS2VYZHVRYzZwamQiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7MiIBL3sJDtDkH8qL5sQuNlNPJ1e-r3cM2f3P54E6Og&product_id=100&cart_type=car_rental&service_date=15-06-2018&service_time=14:00</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (car_rental)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>service_date</th>
                     <td class='tg-yw4l'>To get service date</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>service_time</th>
                     <td class='tg-yw4l'>To get service time</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setcaraddtocart.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">3. Get Car Rental Data From Add To Cart</h3>
      <p class="subheading">/api/getCarRentalAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getCarRentalAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">https://wisitech.in/api/getCarRentalAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4MjYwNjc3LCJuYmYiOjE1MjgyNjA2NzcsImp0aSI6IkJFRlRlS2VYZHVRYzZwamQiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7MiIBL3sJDtDkH8qL5sQuNlNPJ1e-r3cM2f3P54E6Og&language_type=en&product_id=449

         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getInvetation.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">4. Delete Car Rental Data From Add To Cart</h3>
      <p class="subheading">/api/deleteCarRentalData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteCarRentalData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteCarRentalData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4MTc3NTY5LCJuYmYiOjE1MjgxNzc1NjksImp0aSI6IngyR29QT0VvMWtKbnVtWE4iLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.wMROW3j_tVu0RXEpwRxgFizQVz3f3ajf7X9RJA6gwkc&product_id=100&cart_type=car_rental

         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Delete data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getInvetation.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h2 class="modules">Module 8 - Travel Agencies</h2> 
      <h3 class="subheading">1. Get Travel Agencies Info</h3>
      <p class="subheading">/api/getTravelAgenciesInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getTravelAgenciesInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getTravelAgenciesInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI3NjU0MTkwLCJuYmYiOjE1Mjc2NTQxOTAsImp0aSI6ImJ5SVhyNGpmOTRUMnpoUEYiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7hs5N29Qd_Ni45m5ZNtTKFNkL4zX8ZB1IR7VIj4pkq4&category_id=38&subcategory_id=87&language_type=en</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr>            
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/travel.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">2. Set Travel Agencies Add To Cart</h3>
      <p class="subheading">/api/setTravelAgenciesAddToCart/p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setTravelAgenciesAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setTravelAgenciesAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4MjYwNjc3LCJuYmYiOjE1MjgyNjA2NzcsImp0aSI6IkJFRlRlS2VYZHVRYzZwamQiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7MiIBL3sJDtDkH8qL5sQuNlNPJ1e-r3cM2f3P54E6Og&product_id=101&cart_type=travel&service_date=15-06-2018 10:00</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (travel)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>service_date</th>
                     <td class='tg-yw4l'>To get service date</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>service_time</th>
                     <td class='tg-yw4l'>To get service time</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setInvitation.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">3. Get Travel Agencies Data From Add To Cart</h3>
      <p class="subheading">/api/getTravelAgenciesAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getTravelAgenciesAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">https://wisitech.in/api/getTravelAgenciesAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4MjYwNjc3LCJuYmYiOjE1MjgyNjA2NzcsImp0aSI6IkJFRlRlS2VYZHVRYzZwamQiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.7MiIBL3sJDtDkH8qL5sQuNlNPJ1e-r3cM2f3P54E6Og&language_type=en&product_id=453

         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getInvetation.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">. Delete Travel Data From Add To Cart</h3>
      <p class="subheading">/api/deleteTravelData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteTravelData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteTravelData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4MTc3NTY5LCJuYmYiOjE1MjgxNzc1NjksImp0aSI6IngyR29QT0VvMWtKbnVtWE4iLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.wMROW3j_tVu0RXEpwRxgFizQVz3f3ajf7X9RJA6gwkc&product_id=101&cart_type=travel

         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Delete data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>  

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getInvetation.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h2 class="modules">Module 9 - Shopping</h2> 
      <h3 class="subheading">1. Get Tailors Info</h3>
      <p class="subheading">/api/getTailorsInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getTailorsInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getTailorsInfo?category_id=31&subcategory_id=32&vendor_id=88&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4ODczNzc1LCJuYmYiOjE1Mjg4NzM3NzUsImp0aSI6InJSMjdPMmhVc2cwTWh4TFoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.FvACZNVNI8qcuUM6myuG_uAikqY85FQLrBI4AE_cNTs</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/tailorsInfo.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">2. Set Tailors Add To Cart</h3>
      <p class="subheading">/api/setTailorsAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setTailorsAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setTailorsAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5MzkwODI2LCJuYmYiOjE1MjkzOTA4MjYsImp0aSI6IjY0Rk5PS2RoRmxFZkVDZVoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.unL0n3do3W4IWDpafiSjZY9tAyI4Qma06Qzed9VFfPE&product_id=108&cart_type=shopping&cart_sub_type=tailor&length=&chest_size=&waistsize=&soulders=&neck=&arm_length=&wrist_diameter=&cutting=</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (travel)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (tailor)</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setInvitation.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">3. Get Dresses Info</h3>
      <p class="subheading">/api/getDressesInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getDressesInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getDressesInfo?category_id=31&subcategory_id=34&vendor_id=90&language_type=ar&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4ODczNzc1LCJuYmYiOjE1Mjg4NzM3NzUsImp0aSI6InJSMjdPMmhVc2cwTWh4TFoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.FvACZNVNI8qcuUM6myuG_uAikqY85FQLrBI4AE_cNTs</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/dressesInfo.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">4. Set Dresses Add To Cart</h3>
      <p class="subheading">/api/setDressAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setDressAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setDressAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTMwMTcwMzQxLCJuYmYiOjE1MzAxNzAzNDEsImp0aSI6ImdPSlk2TkFwbWFtZUI2MHIiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.Gxd2pzvr3xFqx7lgohUu7H_DsE6ruH5-eMUa2T4H4BY&product_id=110&cart_type=shopping&cart_sub_type=dress&size=Medium&attribute_id=36&rental_date=25-07-2018&return_date=30-08-2018&product_option_value=141&rental_time=10:00&return_time=13:30&product_qty=2</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (travel)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (tailor)</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>size</th>
                     <td class='tg-yw4l'>To get product size</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>rental_date</th>
                     <td class='tg-yw4l'>To get rental_date</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>rental_time</th>
                     <td class='tg-yw4l'>To get rental_date</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>product_option_value</th>
                     <td class='tg-yw4l'>To get product_option_value(rent/buy)</td>
                  </tr>
                   <tr>
                     <th class='tg-yw4l'>rental_time</th>
                     <td class='tg-yw4l'>To get rental_time</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>return_time</th>
                     <td class='tg-yw4l'>To get return_time</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>product_qty</th>
                     <td class='tg-yw4l'>To get product_qty</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/set_dres_addtocart.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">5. Get Abaya Info</h3>
      <p class="subheading">/api/getAbayaInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getAbayaInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getAbayaInfo?category_id=31&subcategory_id=36&vendor_id=92&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4ODczNzc1LCJuYmYiOjE1Mjg4NzM3NzUsImp0aSI6InJSMjdPMmhVc2cwTWh4TFoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.FvACZNVNI8qcuUM6myuG_uAikqY85FQLrBI4AE_cNTs</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/abhaya.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">6. Set Abaya Add To Cart</h3>
      <p class="subheading">/api/setAbayaAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setAbayaAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setAbayaAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5MzkwODI2LCJuYmYiOjE1MjkzOTA4MjYsImp0aSI6IjY0Rk5PS2RoRmxFZkVDZVoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.unL0n3do3W4IWDpafiSjZY9tAyI4Qma06Qzed9VFfPE&product_id=203&cart_type=shopping&cart_sub_type=abaya&length=40&chest_size=30&waistsize=20&soulders=16&neck=15&arm_length=20&wrist_diameter=32&cutting=normal&attribute_id=42&size</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (travel)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (tailor)</td>
                  </tr>
                  
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setInvitation.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">7. Get Oud And Perfumes Info</h3>
      <p class="subheading">/api/getOudAndPerfumesInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getOudAndPerfumesInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getOudAndPerfumesInfo?category_id=31&subcategory_id=94&vendor_id=98&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4ODczNzc1LCJuYmYiOjE1Mjg4NzM3NzUsImp0aSI6InJSMjdPMmhVc2cwTWh4TFoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.FvACZNVNI8qcuUM6myuG_uAikqY85FQLrBI4AE_cNTs&branch_id=106</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get branch detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/perfumeInfo.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">8. Set Oud And Perfumes Add To Cart</h3>
      <p class="subheading">/api/setOudAndPerfumesAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setOudAndPerfumesAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setOudAndPerfumesAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5MzkwODI2LCJuYmYiOjE1MjkzOTA4MjYsImp0aSI6IjY0Rk5PS2RoRmxFZkVDZVoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.unL0n3do3W4IWDpafiSjZY9tAyI4Qma06Qzed9VFfPE&product_id=117&cart_type=shopping&cart_sub_type=perfume&attribute_id=96</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (travel)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (tailor)</td>
                  </tr>
                  
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setInvitation.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">9. Get Gold And Jewelry Info</h3>
      <p class="subheading">/api/getGoldAndJewelryInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getGoldAndJewelryInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getGoldAndJewelryInfo?category_id=31&subcategory_id=95&vendor_id=99&language_type=en&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI4ODczNzc1LCJuYmYiOjE1Mjg4NzM3NzUsImp0aSI6InJSMjdPMmhVc2cwTWh4TFoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.FvACZNVNI8qcuUM6myuG_uAikqY85FQLrBI4AE_cNTs&branch_id=107</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get full detail of cosha</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>category_id</th>
                     <td class='tg-yw4l'>To get main category</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>subcategory_id</th>
                     <td class='tg-yw4l'>To get sub category</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>vendor_id</th>
                     <td class='tg-yw4l'>To get vendor</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>branch_id</th>
                     <td class='tg-yw4l'>To get branch detail</td>
                  </tr>             
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/goldInfo.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">10. Set Gold And Jewelry Add To Cart</h3>
      <p class="subheading">/api/setGoldAndJewelryAddToCart</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/setGoldAndJewelryAddToCart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/setGoldAndJewelryAddToCart?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5MzkwODI2LCJuYmYiOjE1MjkzOTA4MjYsImp0aSI6IjY0Rk5PS2RoRmxFZkVDZVoiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.unL0n3do3W4IWDpafiSjZY9tAyI4Qma06Qzed9VFfPE&product_id=118&cart_type=shopping&cart_sub_type=gold&attribute_id=99&write_on_ring=abc</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>cart_type</th>
                     <td class='tg-yw4l'>To get cart  type (travel)</td>
                  </tr>  
                   <tr>
                     <th class='tg-yw4l'>cart_sub_type</th>
                     <td class='tg-yw4l'>To get cart sub type (tailor)</td>
                  </tr>
                  
                  <tr>
                     <th class='tg-yw4l'>attribute_id</th>
                     <td class='tg-yw4l'>To get attribute detail</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setInvitation.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">11. Get Shopping Data From Add To Cart</h3>
      <p class="subheading">/api/getShoppingAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getShoppingAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getShoppingAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NDEyNDU3LCJuYmYiOjE1Mjk0MTI0NTcsImp0aSI6Ikt4U2I3NERVc3F1QTRrcUciLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.r60PUygjjQ9WYOj5jLwS6k2pbz3oyGvfNy18A5RfW7g&product_id=117&language_type=en
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getdressdata.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">12. Get Dress Data From Add To Cart</h3>
      <p class="subheading">/api/getDressAddToCartData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getDressAddToCartData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getDressAddToCartData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MzAxNjExMzYsIm5iZiI6MTUzMDE2MTEzNiwianRpIjoiMmMxbVpYeHl0Y2wyRU4xQSIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.lj2YnL7meXOpFB4yyO835X9lglIVoBqZfg5SPXm9mwE&product_id=110&language_type=en 
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Set data in add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/getdressdata.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">13. Delete Data From Add To Cart</h3>
      <p class="subheading">/api/deleteShoppingData</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/deleteShoppingData</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/deleteShoppingData?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5NDE1ODM2LCJuYmYiOjE1Mjk0MTU4MzYsImp0aSI6Im5DWUZWNHYxTUFmSmZjWlkiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.Zd9ak3e-OygnrQBcIPVDYkDmc_tOzvTkxA5eVXnqLxk&product_id=194
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Delete data from add to cart</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>  
                  <tr>
                     <th class='tg-yw4l'>product_id</th>
                     <td class='tg-yw4l'>To get product detail</td>
                  </tr>

               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data added in add to cart</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/setInvitation.png" style="height: 500px;"></td>
         </tr>
      </table>  
      <h2 class="modules">Module 10 - My Account</h2> 
      <h3 class="subheading">1. Get User's Occasion detail</h3>
      <p class="subheading">/api/getOccasionInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getOccasionInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getOccasionInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5OTAxOTIwLCJuYmYiOjE1Mjk5MDE5MjAsImp0aSI6IjJScWQycHZPeEFRZHBQTzAiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.LyXf5CGXSX1aLVt7Lpkm5XYEbuXb6TDf1VLQk-6olLk&language_type=en</p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get User's Occasion detail</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>           
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/occasioninfo.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">2. Get User's Studio detail</h3>
      <p class="subheading">/api/getStudioInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getStudioInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getStudioInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5OTAxOTIwLCJuYmYiOjE1Mjk5MDE5MjAsImp0aSI6IjJScWQycHZPeEFRZHBQTzAiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.LyXf5CGXSX1aLVt7Lpkm5XYEbuXb6TDf1VLQk-6olLk&language_type=en
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>To Get User's Studio detail</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>           
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/studioInfo.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">3. Upload Images</h3>
      <p class="subheading">/api/uploadPhoto</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/uploadPhoto</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/uploadPhoto?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5OTAxOTIwLCJuYmYiOjE1Mjk5MDE5MjAsImp0aSI6IjJScWQycHZPeEFRZHBQTzAiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.LyXf5CGXSX1aLVt7Lpkm5XYEbuXb6TDf1VLQk-6olLk&city_id=31&date=25-06-2018&occasion_id=1&venue=xyz&image=002.jpg
         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>Upload Images</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>city_id</th>
                     <td class='tg-yw4l'>To get city id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>date</th>
                     <td class='tg-yw4l'>To get date</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>occasion_id</th>
                     <td class='tg-yw4l'>To get occasion_id</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>venue</th>
                     <td class='tg-yw4l'>To get venue detail</td>
                  </tr>     
                  <tr>
                     <th class='tg-yw4l'>image</th>
                     <td class='tg-yw4l'>To get image detail</td>
                  </tr>            
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/uploadImages.png" style="height: 500px;"></td>
         </tr>
      </table>  
      <h3 class="subheading">4. User's Wallet Info</h3>
      <p class="subheading">/api/getWalletInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getWalletInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getWalletInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZGV2LmdvbGRlbmNhZ2UuY29tL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTI5OTAxOTIwLCJuYmYiOjE1Mjk5MDE5MjAsImp0aSI6IjJScWQycHZPeEFRZHBQTzAiLCJzdWIiOjEwLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.LyXf5CGXSX1aLVt7Lpkm5XYEbuXb6TDf1VLQk-6olLk&language_type=en

         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>Upload Images</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>          
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/user_wallet.png" style="height: 500px;"></td>
         </tr>
      </table>
      <h3 class="subheading">5. User's Review Info</h3>
      <p class="subheading">/api/getReviewInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getReviewInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getReviewInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MzE3MTYwOTIsIm5iZiI6MTUzMTcxNjA5MiwianRpIjoiUnV4czlmcTI4Yk10TzJIZyIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.kHgVIuc61KgdHdvOAurTJYUOCyGmEhV1Ra3PI7EuX3Q&language_type=en&order_id=12354&product_id=82

         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>Upload Images</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr> 
                   <tr>
                     <th class='tg-yw4l'>order_id</th>
                     <td class='tg-yw4l'>To get user's order_id</td>
                  </tr>          
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/userreview.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">6. User's Profile Info</h3>
      <p class="subheading">/api/getProfileInfo</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/getProfileInfo</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/getProfileInfo?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MzAwMDY3NjksIm5iZiI6MTUzMDAwNjc2OSwianRpIjoiSmcxcWRqTWpyb2xjYWNVMyIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.UgKnksAdx1FneF6sIfmx4mMrwT9cqKqXZl5XEegkGfs&language_type=en


         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>Upload Images</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>language_type</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr>        
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>data found</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>500</th>
                     <td class='tg-yw4l'>server not responding </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/my_account.png" style="height: 500px;"></td>
         </tr>
      </table> 
      <h3 class="subheading">7. Change password</h3>
      <p class="subheading">/api/changepassword</p>
      <table class='tg margleft'>
         <tr>
            <th class='tg-yw4l'>Method</th>
            <td class='tg-yw4l'>Post</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Endpoint</th>
            <td class='tg-yw4l'>/api/changepassword</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Postman URL</th>
            <td class='tg-yw4l'><p class="post_url">http://wisitech.in/api/changepassword?old_password=1234567&password=123456&password_confirmation=123456&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd2lzaXRlY2guaW4vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MzAwNzQzNzYsIm5iZiI6MTUzMDA3NDM3NiwianRpIjoiVGd5aDhYUmFvbnBGNjVHdSIsInN1YiI6MTAsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.Ys1NPLIh9klgHSEpmY3TVTv2X_p0xFK6CFiXo0s-BzQ&lang=en


         </p></td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Overview</th>
            <td class='tg-yw4l'>Upload Images</td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Param</th>
            <td class='tg-yw4l'>
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Name</th>
                     <th class='tg-yw4l'>Overview</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>token</th>
                     <td class='tg-yw4l'>To Authenticate User</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>lang</th>
                     <td class='tg-yw4l'>To get user language(ar/en)</td>
                  </tr> 
                  <tr>
                     <th class='tg-yw4l'>old_password</th>
                     <th class='tg-yw4l'>To get user old_password</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>password</th>
                     <td class='tg-yw4l'>To get new password</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>password_confirmation</th>
                     <td class='tg-yw4l'>confirm new password</td>
                  </tr>       
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>Code</th>
            <td class='tg-yw4l'>
      
               <table class='tg'>
                  <tr>
                     <th class='tg-yw4l'>Code</th>
                     <th class='tg-yw4l'>Meaning</th>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Password Changed successfully</td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>200</th>
                     <td class='tg-yw4l'>Sorry! Your old password didn't match. Enter it again. </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>204</th>
                     <td class='tg-yw4l'>Invalid Input </td>
                  </tr>
                  <tr>
                     <th class='tg-yw4l'>203</th>
                     <td class='tg-yw4l'>User not found </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <th class='tg-yw4l'>ScreenShot</th>
            <td class='tg-yw4l'><img src="http://wisitech.in//images/API/changed_password.png" style="height: 500px;"></td>
         </tr>
      </table>                 
      
   </div>
   </body>
</html> 


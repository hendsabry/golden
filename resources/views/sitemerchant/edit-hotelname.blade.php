@include('sitemerchant.includes.header') 
@php $hotel_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_EDITHOTALNAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EDITHOTALNAME') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EDITHOTALNAME') }} @endif</h5>
        @include('sitemerchant.includes.language') </header>
      </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
              <form name="form1" method="post" id="edithotel" action="{{ route('updatehotelname') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="parent_id"  value="{{ $parentcategory->mc_id }}">
                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HOTEL_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HOTEL_NAME'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
                      <input type="text" maxlength="40" class="english" name="mc_name" id="mc_name" value="{{ $child->mc_name }}" >
                    </div>
                    <div class="arabic ar">
                      <input class="arabic ar" maxlength="40"  id="mc_name_ar"  name="mc_name_ar" value="{{ $child->mc_name_ar }}"  type="text" >
                    </div>
                  </div>
                  </div>
                </div>
                <input type="hidden" name="description" value="hotel description">
                <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english"> @php echo lang::get('mer_en_lang.MER_HOTEL_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HOTEL_IMAGE'); @endphp </span> </label>

                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn"> <span class="english"> @php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                      <div class="form-upload-img"><img src="{{ $child->mc_img }}" width="150" height="150"></div>
                    </div>
                    </label>
                    <input id="company_logo" name="mc_img" class="info-file" type="file">
                  </div>
                  </div>
                </div>
                
                <div class="form_row">
                  <div class="form_row_left">
                  <input type="hidden" name="updatemc_img" value="{{ $child->mc_img }}">
                  <input type="hidden" name="id" value="{{ $serviceid }}">
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="تحديث">
                  </div>
                  <div class="english">
                    <input type="submit" name="submit" value="Update">
                  </div>
                  </div>
                </div>
              </form>            
            <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
            
            <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
            
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor -->
</div>
<script>
       $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#edithotel").validate({
                  ignore: [],
                  rules: {
                  mc_name: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },

                       description: {
                       required: true,
                      },

                      description_ar: {
                       required: true,
                      },

                      mc_img: {
                           accept:"png|jpe?g|gif",
                      },
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mc_name: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_HOTEL_NAME'); @endphp ",
                      },  

                 mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_HOTEL_NAME_AR'); @endphp ",
                      },    

                       description: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_DESCRIPTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_DESCRIPTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_DESCRIPTION') }} @endif ",
                      },

                       description_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_DESCRIPTION_AR'); @endphp  ",
                      },
                        mc_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },                       
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

 @if($mer_selected_lang_code !='ar')

              if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.description != "undefined" || valdata.description != null) 
              {
              $('.english_tab').trigger('click');     

              }
              if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
              {

              $('.english_tab').trigger('click');     

              }
              if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
              {

              $('.arabic_tab').trigger('click');     

              }

              if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
              {

              $('.arabic_tab').trigger('click');     

              }
@else
 
              if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
              {

              $('.arabic_tab').trigger('click');     

              }

              if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
              {

              $('.arabic_tab').trigger('click');     

              }
              if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.description != "undefined" || valdata.description != null) 
              {
              $('.english_tab').trigger('click');     

              }
              if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
              {

              $('.english_tab').trigger('click');     

              }

@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header')
@php $reception_hospitality_leftmenu =1; @endphp
<div class="merchant_vendor">  
@include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')    
    <div class="right_panel">
      <div class="inner">
  <div class="service_listingrow">
             <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.PACKEGINFORMATIONLIST')!= '') {{  trans(Session::get('mer_lang_file').'.PACKEGINFORMATIONLIST') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.PACKEGINFORMATIONLIST') }} @endif</h5>
      
      <div class="add"><a href="{{ route('reception-add-packege',['id' => $id,'shopid'=>$shopid])}}">@if (Lang::has(Session::get('mer_lang_file').'.ADD_PACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.ADD_PACKAGE') }} 
            @else  {{ trans($MER_OUR_LANGUAGE.'.ADD_PACKAGE') }} @endif</a></div>
            </div> <!-- service_listingrow -->
  @php $ids = request()->id; @endphp
          {!! Form::open(array('url'=>route('reception-list-packages',['id' => $id,'shopid'=>$shopid]),'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
      
      <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">        
        
        <div class="filter_left">
        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems">
           @php $statuss = request()->status; $searchh= request()->search; @endphp
            <select name="status" id="status">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
              <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
              <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
            </select>
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
          
        </div>
        
        <div class="search_box">
        <div class="search_filter">&nbsp;</div>
        <div class="filter_right">
          <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$searchh or ''}}" />
          <input type="button" class="icon_sch" id="submitdata" />
        </div>
        </div> <!-- search_box -->
        
      </div><!-- filter_area --> 
     
      {!! Form::close() !!}



    <div class="global_area">
            <div class="row">
               <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
        <div class="col-lg-12">
          <div class="table_wrap">
          
 
        
      
              <div class="panel-body panel panel-default"> 

 

 @if ($getAttr->count() < 1)

<div class="no-record-area">
@if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif
</div>
 @else
 

 
                  <div class="table hotel-list-table">
                          <div class="tr">
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '') {{ trans(Session::get('mer_lang_file').'.MER_NAME')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_NAME')}} @endif</div>
                            
                           
                             <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.PRICE')!= '') {{ trans(Session::get('mer_lang_file').'.PRICE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.PRICE')}} @endif</div>

                             
                              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CREATED_DATE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_CREATED_DATE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_CREATED_DATE')}} @endif</div>

                             <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{ trans(Session::get('mer_lang_file').'.MER_STATUS')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS')}} @endif</div>

                             <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTION')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION')}} @endif</div>
                             
                          </div>

                          @php $mc_name='mc_name';
                                $pro_title='pro_title';
                          @endphp
                   @if($mer_selected_lang_code !='en')
                                         @php $mc_name= 'mc_name_'.$mer_selected_lang_code;
                                          $pro_title='pro_title_'.$mer_selected_lang_code;
                                          @endphp
                   @endif                      
                   
              @foreach($getAttr as $val) 
              @php $cid = $val->pro_id; @endphp
                   <div class="tr">
                     <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_HOTEL_NAME')!= '') {{ trans(Session::get('mer_lang_file').'.MER_HOTEL_NAME')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_HOTEL_NAME')}} @endif">{{$val->$pro_title}}</div>
                    
                      <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_CREATED_DATE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_CREATED_DATE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_CREATED_DATE')}} @endif">{{$val->pro_price}}</div>


                     
        <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_CREATED_DATE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_CREATED_DATE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_CREATED_DATE')}} @endif">  {{ Carbon\Carbon::parse($val->updated_at)->format('F j, Y')}}</div>

                    <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{ trans(Session::get('mer_lang_file').'.MER_STATUS')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS')}} @endif"> 
@if($val->pro_status==1)
                      <span class="status_active  status_active2 cstatus" data-status="Active" data-id="{{$val->pro_id}}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTIVE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')}} @endif </span>  

@else
                      <span class="status_deactive  status_active2 cstatus" data-id="{{$val->pro_id}}" data-status="Inactive"> @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_DEACTIVE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')}} @endif </span>@endif
                       </div> 

                    <div class="td td5" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTION')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION')}} @endif"><a href="{{ route('reception-add-packege',['id' => $id,'cid'=>$cid,'shopid'=>$shopid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EDIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EDIT') }} @endif</a></div>
               
                    
                 </div> 
               @endforeach
            
        </div>  
 
@endif
      <!--PAGE CONTENT PART WILL COME INSIDE IT START-->


        <!--PAGE CONTENT PART WILL COME INSIDE IT END-->

          </div>
       
        </div> <!-- table_wrap -->
        </div>
      </div>
    </div> <!-- global_area -->
    </div>
  </div> <!-- right_panel -->
</div> <!-- merchant_vendor -->

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
 
    <div class="action_content"></div>
    <div class="action_btnrow">  <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a>  <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a></div>
  </div> 
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'reception_item_list'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
            $('form[name=test]').submit();
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script>
@include('sitemerchant.includes.footer')


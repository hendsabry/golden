<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT') }}  | {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_MERCHANT_STORES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_MERCHANT_STORES') : trans($MER_OUR_LANGUAGE.'.MER_MANAGE_MERCHANT_STORES') }}         </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
 <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
	 <link href="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
 <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">

   <!-- HEADER SECTION -->
         {!! $merchantheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      {!! $merchantleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a href="#"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_MERCHANT_STORES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_MERCHANT_STORES')   : trans($MER_OUR_LANGUAGE.'.MER_MANAGE_MERCHANT_STORES') }} </a></li>
                            </ul>
                    </div>
                </div>
				
	
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_MERCHANT_STORES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_MERCHANT_STORES')   : trans($MER_OUR_LANGUAGE.'.MER_MANAGE_MERCHANT_STORES') }}</h5>
            
        </header>
         @if (Session::has('result'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('result') !!}
	{{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
       </div>
		@endif
        <div id="div-1" class="accordion-body collapse in body">
            <div class="accordion-body collapse in body" id="div-1">
        

             <div class="panel_marg_clr ppd">          
           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row">
										<th aria-label="S.No: activate to sort column ascending" style="width: 61px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO') }}</th>
										<th aria-label="Product Name: activate to sort column ascending" style="width: 69px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_NAME')  :  trans($MER_OUR_LANGUAGE.'.MER_STORE_NAME') }}</th>
										<th aria-label="City: activate to sort column ascending" style="width: 81px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ? trans(Session::get('mer_lang_file').'.MER_PHONE')  : trans($MER_OUR_LANGUAGE.'.MER_PHONE') }}</th>
										<th aria-label="Store Name: activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS') :   trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }}</th>
                                        	<th aria-label="Store Name: activate to sort column ascending" style="width: 78px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY')  : trans($MER_OUR_LANGUAGE.'.MER_CITY') }}</th>
										<th aria-label="Original Price($): activate to sort column ascending" style="width: 75px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_IMAGE')   : trans($MER_OUR_LANGUAGE.'.MER_STORE_IMAGE')}}</th>									
										<th aria-label="Actions: activate to sort column ascending" style="width: 73px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT')  : trans($MER_OUR_LANGUAGE.'.MER_EDIT')}}</th>
										<th aria-label="Hot deals: activate to sort column ascending" style="width: 65px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_BLOCK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BLOCK')  : trans($MER_OUR_LANGUAGE.'.MER_BLOCK')}} / {{ (Lang::has(Session::get('mer_lang_file').'.MER_UNBLOCK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UNBLOCK')  : trans($MER_OUR_LANGUAGE.'.MER_UNBLOCK') }}</th>
	
										</tr>
                                    </thead>
                                    <tbody>
                                  <?php $i = 1; ?>
		
								     @foreach($store_return as $store_details)
										<tr class="gradeA odd">
                                            <td class="text-center">{{ $i }}</td>
                                            <td class="text-center">{{ $store_details->stor_name }}</td>
                                            <td class=" text-center">{{ $store_details->stor_phone  }}</td>
                                            <td class="text-center">{{ $store_details->stor_address1.$store_details->stor_address2 }}</td>
                                            <td class="text-center">{{ $store_details->ci_name }}</td>
                                             <td class="text-center"> 
											 <?php 
											$pro_img = $store_details->stor_img;
										   $prod_path = url(''). '/public/assets/default_image/No_image_store.png';
											 ?>
											@if($pro_img != '') {{--image is null --}}
												
													<?php
												  
												  $img_data = "public/assets/storeimage/".$pro_img; ?>
													  @if(file_exists($img_data))
														  {{--image not exists in folder --}}
																			<?php
																					$prod_path = url('').'/public/assets/storeimage/'.$pro_img;
																			 ?>
													  @else  
															 @if(isset($DynamicNoImage['store']))
															<?php 				
																$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['store']; ?>
																@if($DynamicNoImage['store'] !='' && file_exists($dyanamicNoImg_path))
																<?php  
																	$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['store']; ?>
																@endif
																					
															 @endif
																				 
																				 
															@endif
									   
										@else
												
													
													@if(isset($DynamicNoImage['store'])) {{-- check no_image_product is exist  --}}
															<?php						
																$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['store']; ?>
																@if($DynamicNoImage['store'] !='' && file_exists($dyanamicNoImg_path))
																<?php  
																	$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['store']; ?>
																@endif
																					
															 @endif
																				 
																				 
															@endif									 
											 
											 <img src="{{ $prod_path }}" height="45px" ></td>                                  
                                            <td class="text-center"><a href="<?php echo url('merchant_edit_shop/'.$store_details->stor_id."/".$store_details->stor_merchant_id); ?>" data-tooltip="{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT')  : trans($MER_OUR_LANGUAGE.'.MER_EDIT')}}"><i class="icon icon-edit icon-2x"></i></a></td>                                            
                                            <td class="text-center ">
                                            
                                            @if($store_details->stor_status == 1) 
                                            <a href="{{ url('merchant_block_shop/'.$store_details->stor_id."/0"."/".$store_details->stor_merchant_id) }}" data-tooltip="{{ (Lang::has(Session::get('mer_lang_file').'.MER_UNBLOCK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UNBLOCK')  : trans($MER_OUR_LANGUAGE.'.MER_UNBLOCK') }}"><i class="icon icon-ok icon-2x "></i></a> @elseif($store_details->stor_status == 0) 
                                            <a href="{{ url('merchant_block_shop/'.$store_details->stor_id."/1"."/".$store_details->stor_merchant_id) }}" data-tooltip="{{ (Lang::has(Session::get('mer_lang_file').'.MER_BLOCK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BLOCK')  : trans($MER_OUR_LANGUAGE.'.MER_BLOCK')}}"><i class="icon icon-ban-circle icon-2x icon-me"></i></a> 
                                            @endif
                                            </td>
                                        
                                        </tr>
<?php $i++;   ?>

							@endforeach
										</tbody>
                                </table></div>


                               
        </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p>&copy; <?php echo $SITENAME; ?>&nbsp;</p>
    </div>
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->   
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')}}",
               nextText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS') }}",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')}}",
               nextText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS') }}",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
      </script> 
	  <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>

@include('sitemerchant.includes.header') 
@php $makeup_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
     <header>
      @if($autoid=='')
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCT') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCT') }} @endif </h5>

        @else
          <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_PRODUCT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_PRODUCT') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_PRODUCT') }} @endif </h5>

        @endif
        @include('sitemerchant.includes.language') </header>
		   <div class="global_area">
      <div class="row">

          <div class="col-lg-12">
            <div class="box">
		
        <form name="form1" id="menucategory" method="post" action="{{ route('store-makeup-product') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
				  
                 <div class="form_row common_field">
              <div class="form_row_left">
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.SELECT_CATEGORY'); @endphp</span>
                     <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SELECT_CATEGORY'); @endphp </span>
                    </label>
              <div class="info100"> 
              <select class="small-sel" name="category" id="category">
                  <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>
                    @php $menu_name='attribute_title'@endphp
                   @if($mer_selected_lang_code !='en')
                   @php $menu_name= 'attribute_title_'.$mer_selected_lang_code; @endphp
                    @endif
                     @foreach($productcat as $val)
                  <option value="{{ $val->id }}" {{ isset($fetchfirstdata->attribute_id) && $fetchfirstdata->attribute_id ==$val->id ? 'selected' : ''}}>{{ $val->$menu_name }}</option>
                  @endforeach  
              </select>
              </div>
              </div>
              </div>
                  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.MER_PRODUCT_NAME'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRODUCT_NAME'); @endphp </span>
                    </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="{{ $fetchfirstdata->pro_title or '' }}"  class="english"  maxlength="150" />
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" value="{{ $fetchfirstdata->pro_title_ar or '' }}" class="arabic ar" name="mc_name_ar"  id="mc_name_ar"   type="text"  maxlength="150">
                      </div>
                    </div>
                    </div>
                  </div>
				  
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_PRODUCT_IMAGE'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRODUCT_IMAGE'); @endphp </span>
 </label>
                    <div class="info100">
                     
				  <div class="input-file-area">
                  <label for="company_logo">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
   <div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div>
                  </div>
                  </label>
                  <input id="company_logo" data-lbl="file_value1" name="proimage" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
                </div>

                <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                 @if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img !='')
                  <div class="form-upload-img">
                       <img src="{{ $fetchfirstdata->pro_Img or '' }}" >
                     </div>
                     @endif

                    </div>
                    </div>



      
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->








                    
                  </div>
                  
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span>
 </label>
                    <div class="info100">
                      
                        <input type="text" class="notzero" name="price" id="price" onkeypress="return isNumber(event)" value="{{ $fetchfirstdata->pro_price or '' }}"  maxlength="9" />
                     
                    </div>
                    </div>
                  </div>

                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="{{$fetchfirstdata->pro_discount_percentage or ''}}" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>


                   <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
                    <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span>
                    <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span>
                     </label>
                    <div class="info100">
                        <input type="text" class="notzero" name="quantity" id="quantity" onkeypress="return isNumber(event)" value="{{ $fetchfirstdata->pro_qty or '' }}" id="quantity"  maxlength="4" />
                        <input type="hidden" name="itemid" value="{{ $itemid }}">
                         <input type="hidden" name="id" value="{{ $id }}">
                         <input type="hidden" name="autoid" value="{{ $autoid }}">
                     
                    </div>
                    </div>
                  </div>
				  
				  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span>
 </label>
                    <div class="info100">
					   <div class="english"><textarea name="description" maxlength="500" id="description">{{ $fetchfirstdata->pro_desc or '' }}</textarea></div>
					    
              <div class="arabic ar"><textarea name="description_ar" maxlength="500" id="description_ar">{{ $fetchfirstdata->pro_desc_ar or '' }}</textarea></div>
                      
                    </div>
                    </div>
                  </div>
				  
                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  </div></div>
			   </form>
		
			 
			</div></div></div></div>
		

  </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>
 $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#menucategory").validate({

                  ignore: [],
                  rules: {
                  category: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },
                       mc_name_ar: {
                       required: true,
                      },

                      service_name_ar: {
                       required: true,
                      },

                       duration: {
                       required: true,
                      },
                       price: {
                       required: true,
                      },
                       quantity: {
                       required: true,
                      },

                      year_of_exp: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                       @if(isset($fetchfirstdata->pro_Img)!='')  
                        proimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        proimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             category: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",
                      },  

                  mc_name: {
                     required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_PRODUCTNAME'); @endphp",

                      },   

                          mc_name_ar: {
                             required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_PRODUCTNAME'); @endphp",
                 
                      },   

             

                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                      },

                        quantity: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY') }} @endif ",
                      },


                       year_of_exp: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXP') }} @endif ",
                      },
                     description: {
                       required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
                      },

                       description_ar: {
                         required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER_AR'); @endphp",
                      },

                  proimage: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },   


                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  @if($mer_selected_lang_code !='en')
                  
                    
                    
                     if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.image != "undefined" || valdata.image != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }
          @else

                if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
             
                     if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }  

                    if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }
                    
                     if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.image != "undefined" || valdata.image != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }
                      
                    
                @endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script> 
 
<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->
@include('sitemerchant.includes.footer')
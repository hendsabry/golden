`@include('sitemerchant.includes.header')
@php $reception_hospitality_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->itemid=='')
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CATEGORY') }} @endif </h5>
        @else
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_CATEGORY') }} @endif </h5>
        @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> @php $cid = request()->shopid; $id = request()->id; $itemid = request()->itemid; @endphp
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <form name="form1"  id ="menucategory" method="post" action="{{ route('recption-updatecategory') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CATEGORY_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" value="{{$getAttr->attribute_title or ''}}" class="english" name="category_name" maxlength="90" id="category_name" required="" >
                      </div>
                      <div class="arabic ar">
                        <input class="arabic ar" id="category_ar" maxlength="90"  name="category_name_ar"  value="{{$getAttr->attribute_title_ar or ''}}"  required=""  type="text" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <input type="hidden" name="id" value="{{ $id or ''}}">
                    <input type="hidden" name="cid" value="{{ $cid or ''}}">
                    <input type="hidden" name="itemid" value="{{ $itemid or ''}}">
                    @if($itemid=='')
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                    <div class="english">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <!-- form_row -->
                    @else
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="تحديث">
                    </div>
                    <div class="english">
                      <input type="submit" name="submit" value="Update">
                    </div>
                    <!-- form_row -->
                    @endif </div>
                </div>
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },
                     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
          category_name: {
     
                 required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_CATEGORY_NAME'); @endphp",
                      }, 


          category_name_ar: {
               required:  "@php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME_VALIDATION_AR'); @endphp",


                      }, 

                                                  
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                        @if($mer_selected_lang_code !='en')

                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    
                  
                      if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                       
@else  
 if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                      
 if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                   

@endif
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer') 
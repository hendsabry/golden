<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_SHIPPING_AND_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_SHIPPING_AND_DELIVERY') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_SHIPPING_AND_DELIVERY') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/success.css" />
     <link href="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         {!! $adminheader!!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
         {!! $adminleftmenus!!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY') : trans($MER_OUR_LANGUAGE.'.PAYU_SHIPPING_DELIVERY') }}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.PAYU_SHIPPING_DELIVERY') : trans($MER_OUR_LANGUAGE.'.PAYU_SHIPPING_DELIVERY') }}</h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body">
            {{ Form::open(['class' => 'form-horizontal'])}}

                 <!--   <div class="form-group col-lg-12">
                    	<div class="accordion-body collapse in body" id="div-1">
        <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label>
		   
		   </label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter">
		   </div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"><label></label></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"><label></label></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div><div role="grid" class="dataTables_wrapper form-inline" id="dataTables-example_wrapper"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="dataTables-example_length"></div></div><div class="col-sm-6"><div id="dataTables-example_filter" class="dataTables_filter"></div></div></div><div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-sm-6"><div id="dataTables-example_length" class="dataTables_length"></div></div><div class="col-sm-6"><div class="dataTables_filter" id="dataTables-example_filter"></div></div></div> -->
            <div class="panel_marg_clr ppd">
           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row">
                                          <th aria-sort="ascending" aria-label="S.No: activate to sort column ascending" style="width: 99px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc">{{ (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO') }}</th>
                                        <th aria-label="Customers: activate to sort column ascending" style="width: 111px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS_NAME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_PRODUCTS_NAME') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS_NAME') }}</th>
                                        <th aria-label="Product Title: activate to sort column ascending" style="width: 219px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_NAME') : trans($MER_OUR_LANGUAGE.'.MER_NAME') }}</th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ? trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }}</th>
                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DATE')!= '') ? trans(Session::get('mer_lang_file').'.MER_DATE') : trans($MER_OUR_LANGUAGE.'.MER_DATE') }}</th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }}</th>
                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DETAILS')!= '') ? trans(Session::get('mer_lang_file').'.MER_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_DETAILS') }}</th>
                                       <!-- <th aria-label="Transaction Date: activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"><?php //if (Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS');} ?></th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
										<?php $i=1; ?>
										@foreach($shippingdetails as $shipping)
										    
										  
										    
										   	@if($shipping->order_status==1)
											@php 
												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_SUCCESS')!= ''))? trans(Session::get('mer_lang_file').'.MER_SUCCESS'): trans($MER_OUR_LANGUAGE.'.MER_SUCCESS');
											@endphp
											@elseif($shipping->order_status==2) 
											@php
												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_COMPLETED')!= ''))? trans(Session::get('mer_lang_file').'.MER_COMPLETED'): trans($MER_OUR_LANGUAGE.'.MER_COMPLETED');
											@endphp
											elseif($shipping->order_status==3) 
											@php
												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_HOLD')!= ''))? trans(Session::get('mer_lang_file').'.MER_HOLD'): trans($MER_OUR_LANGUAGE.'.MER_HOLD');
											@endphp
											elseif($shipping->order_status==4) 
											@php
												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_FAILED')!= ''))? trans(Session::get('mer_lang_file').'.MER_FAILED'): trans($MER_OUR_LANGUAGE.'.MER_FAILED');
											@endphp
                                            @endif
           
											@php $merid  = Session::get('merchantid');  @endphp
											
										
										
										
										
										<tr class="gradeA odd">
                                            <td class="sorting_1">{{ $i }}</td>
                                           <td class="center     ">
                                            <ul>
											<?php
												$product_data = DB::table('nm_order_payu')->orderBy('order_date', 'desc')->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')->where('nm_order_payu.transaction_id', '=',$shipping->transaction_id)->where('nm_order_payu.order_merchant_id', $merid)->where('nm_order_payu.order_type', '=',1)->get(); ?>

												@foreach($product_data as $product_datavalues)
												<?php 	echo '<li style="list-style:upper-roman; text-align:left;">'. $product_datavalues->pro_title.'.</li>';    ?>
												@endforeach
												</td>
											</ul>
                                            <td class=" ">{{ $shipping->cus_name }}</td>
                                            <td class=" ">{{ $shipping->cus_email }} </td>
                                            <td class="center">{{ $shipping->order_date }}</td>
                                            <td class="center">{{ $shipping->ship_address1 }}</td>
                                     
                                      
                            <td class="center"><a class="btn btn-success" data-target="{{ '#uiModal'.$i}}" data-toggle="modal">{{ (Lang::has(Session::get('mer_lang_file').'.MER_VIEW_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_VIEW_DETAILS')}}</a></td>
                                              <!--<td class="center"><?php //echo $orderstatus;?></td>-->
                                            
                                          
                                           
                                        </tr>
                                        
                                        @php $i=$i+1 @endphp
                                           @endforeach 
                                    </tbody>
                                </table></div>

                                
        </div>
           
         {{ Form::close() }}
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
     
    <div class="modal fade in" id="formModal"  style="display:none;">
     <div class="modal-dialog">
                                    <div class="modal-content">
                                        
                                        <div class="modal-body">
                                          {{ Form::open(['role' => 'form'])}}
                                        <div class="form-group">
                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ? trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }}</label>
                                            <input class="form-control">
                                            <p class="help-block">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EXAMPLE_BLOCK_LEVEL_HELP_TEXT_HERE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EXAMPLE_BLOCK_LEVEL_HELP_TEXT_HERE') : trans($MER_OUR_LANGUAGE.'.MER_EXAMPLE_BLOCK_LEVEL_HELP_TEXT_HERE')}}.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS'): trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }}&nbsp;:&nbsp;</label>
											{{ (Lang::has(Session::get('mer_lang_file').'.MER_ASDSA_CALIFORNIA_CANADIAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ASDSA_CALIFORNIA_CANADIAN'): trans($MER_OUR_LANGUAGE.'.MER_ASDSA_CALIFORNIA_CANADIAN') }}
                                        </div>
                                        <div class="form-group">
                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MESSAGE')!= '') ? trans(Session::get('mer_lang_file').'.MER_MESSAGE'): trans($MER_OUR_LANGUAGE.'.MER_MESSAGE')}}</label>
                                            {{ Form::textarea('','',['id' => 'text4', 'class' => 'form-control'])}}
                                          
                                        </div>
                                        
                                       
                                    {{ Form::close() }}
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-default" type="button">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLOSE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLOSE') : trans($MER_OUR_LANGUAGE.'.MER_CLOSE') }}</button>
                                            <button class="btn btn-success" type="button">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEND')!= '') ? trans(Session::get('mer_lang_file').'.MER_SEND') : trans($MER_OUR_LANGUAGE.'.MER_SEND') }}</button>
                                        </div>
                                    </div>
                                </div>  </div>
     <!--END MAIN WRAPPER -->
<?php $i=1; $wallet =0;  ?>
@foreach($shippingdetails as $shipping)

	
@php 
$item_total=0;  $coupon_amount=0;
$subtotal =0;
$wallet_amt=0;
$all_tax_amt=0; $all_shipping_amt=0;

    $tax_amt = (($shipping->order_amt * $shipping->order_tax)/100);
    $all_tax_amt+=   (($shipping->order_amt * $shipping->order_tax)/100);

    $shipping_amt = $shipping->order_shipping_amt;
    $all_shipping_amt+= $shipping->order_shipping_amt; @endphp
   
    @if($shipping->coupon_amount !=0)
        @php $coupon_value = $shipping->coupon_amount;  @endphp
    @else
         @php $coupon_value = 0; @endphp
    @endif
  @php  
   /* $wallet = DB::table('nm_ordercod_wallet')->where('cod_transaction_id','=',$shipping->transaction_id)->get();
   
    if(count($wallet)!=0){
        $wallet_amt = $wallet[0]->wallet_used;
    }else{
        $wallet_amt = 0;
    }
    */
    @endphp
    @php
    $item_total = $shipping->order_amt;
    
    $grand_total = (($item_total+$tax_amt+$shipping_amt)-$wallet_amt)-$coupon_value;
	
@endphp   
										  


<div  id="{{ 'uiModal'.$i }}" class="modal fade in invoice-top" style="display:none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header" style="border-bottom:none; overflow: hidden;background: #f5f5f5;">
                                          
                                           
                                            <div class="col-lg-4"><img src="<?php echo $SITE_LOGO; ?>" alt="{{ (Lang::has(Session::get('mer_lang_file').'.LOGO')!= '') ?  trans(Session::get('mer_lang_file').'.LOGO') : trans($MER_OUR_LANGUAGE.'.LOGO') }}"></div>
                                                 <div class="col-lg-4" style="padding-top: 10px; text-align: center;"><strong>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TAX_INVOICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TAX_INVOICE') : trans($MER_OUR_LANGUAGE.'.MER_TAX_INVOICE') }}</strong></div>
                                                  <div class="col-lg-4" style="text-align: right;"><a href="" style="color:#d9534f" class="" data-dismiss="modal" CLASS="pull-right"><i class="icon-remove-sign icon-2x"></i></a></div>
                                        </div>
                                     <hr style="margin-top: 0px;">
                                      <div class="row">
                                      <div class="col-lg-12">
                                      <div class="col-lg-6">
									  @if($shipping->order_paytype == 1) 
                                      <h4>{{ (Lang::has(Session::get('mer_lang_file').'.MER_PAYUMONEY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PAYUMONEY') : trans($MER_OUR_LANGUAGE.'.MER_PAYUMONEY')}}</h4>
									  @elseif($shipping->order_paytype == 2)
                                       ?> <h4>{{ (Lang::has(Session::get('mer_lang_file').'.MER_WALLET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_WALLET'): trans($MER_OUR_LANGUAGE.'.MER_WALLET')}}</h4>
                                      @else 
									  ---
									  @endif
                                      @if($shipping->order_status == 1) 
										<b>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT_PAID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT_PAID') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT_PAID') }}
										@else  
                                      <b>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT_TO_PAY')!= '') ? trans(Session::get('mer_lang_file').'.MER_AMOUNT_TO_PAY') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT_TO_PAY') }} 
										@endif
									  
									  :{{ Helper::cur_sym() }} <?php
									  $product_titles=DB::table('nm_order_payu')
									  ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
									  ->leftjoin('nm_color', 'nm_order_payu.order_pro_color', '=', 'nm_color.co_id')
									  ->leftjoin('nm_size', 'nm_order_payu.order_pro_size', '=', 'nm_size.si_id')
									  ->where('transaction_id', '=', $shipping->transaction_id)
									  ->where('nm_order_payu.order_merchant_id', $merid)
									  ->where('nm_order_payu.order_type', '=',1)
									  ->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
									  @foreach($product_titles as $prd_tit) 
										  	<?php 
										  $subtotal=$prd_tit->order_amt; 
																$tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
																$total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 
																$total_ship_amt+= $prd_tit->order_shipping_amt;
																$total_item_amt+=$prd_tit->order_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->order_pro_id;
																
														$item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
														
															 $ship_amt = $prd_tit->order_shipping_amt;
															
														
															 //$item_tax = $codorderdet->cod_tax;
															/*if($prd_tit->coupon_amount != 0)
															{
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
															}
															else
															{
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
															}*/	
                                                            $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
                                                            
                                                            $wallet_amt +=  $prd_tit->wallet_amount;
                                                            $wallet     += $prd_tit->wallet_amount;	 ?>
									 @endforeach
									  {{ $grand_total-$wallet_amt }}</b>
                                      <br>
									  
                                      <span>({{ (Lang::has(Session::get('mer_lang_file').'.MER_INCLUSIVE_OF_ALL_CHARGES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INCLUSIVE_OF_ALL_CHARGES') : trans($MER_OUR_LANGUAGE.'.MER_INCLUSIVE_OF_ALL_CHARGES') }})</span>
                                     <br>
									 
                                     <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_ID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORDER_ID') : trans($MER_OUR_LANGUAGE.'.MER_ORDER_ID') }} : {{ $prd_tit->transaction_id }}</span>
                                       <br>
					 <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') : trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} : {{ $shipping->order_date }}</span>
                                      </div>
                                          <div class="col-lg-6" style="border-left:1px solid #eeeeee;">
                                         <h4>{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_DETAILS') }}</h4>                                        
                                          
										  {{ (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_NAME') : trans($MER_OUR_LANGUAGE.'.MER_NAME') }}  :{{ $shipping->ship_name }}<br>
										  
										  {{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ? trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} : {{ $shipping->ship_email}}<br>
										  
										  {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1') }} :{{ $shipping->ship_address1}}
                                          <br>
										  
										   {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS2') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS2') }} :{{ $shipping->ship_address2 }}
                                          <br> 
										  
										   @if($shipping->ship_ci_id!='')    
                            {{ (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY') : trans($MER_OUR_LANGUAGE.'.MER_CITY') }} : {{ $shipping->ship_ci_id }} <br>
                          @endif   
						  
                                          {{ (Lang::has(Session::get('mer_lang_file').'.MER_STATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STATE') : trans($MER_OUR_LANGUAGE.'.MER_STATE') }} : {{ $shipping->ship_state }}<br>
										  
										   @if($shipping->ship_country!='')    
										   {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_COUNTRY') }} : {{ $shipping->ship_country }}<br>
							@endif
										   
										   {{ (Lang::has(Session::get('mer_lang_file').'.MER_ZIPCODE')!= '') ? trans(Session::get('mer_lang_file').'.MER_ZIPCODE') : trans($MER_OUR_LANGUAGE.'.MER_ZIPCODE') }} : {{ $shipping->ship_postalcode }}<br>
										   
                                          {{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ? trans(Session::get('mer_lang_file').'.MER_PHONE')  : trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} : {{ $shipping->ship_phone }}
                                          </div>
                                            
                                             
                                      </div>
                                      </div>
                                      <hr>
                                      <div class="row">
                                      <div class="span12 text-center">
                                      <h4 class="text-center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_INVOICE_DETAILS') }}</h4>
                                      <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS') : trans($MER_OUR_LANGUAGE.'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS') }} </span>
                                      </div>
                                      </div>
                                      <br>
                                      <div style="overflow: auto;">
                                      <table class="inv-table" style="width:98%;" align="center" border="1" bordercolor="#e6e6e6">
                                  <tr style="border-bottom:1px solid #e6e6e6; background:#f5f5f5;">
									<th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PRODUCTS_NAME'): trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS_NAME') }}</th>
                                    <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COLOR'): trans($MER_OUR_LANGUAGE.'.MER_COLOR')}}</th>
                                      <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_SIZE')}}</th>
                                        <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_QUANTITY') :trans($MER_OUR_LANGUAGE.'.MER_QUANTITY')}}</th>
                                         <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ?   trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE') }}</th>
                                            <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SUB_TOTAL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SUB_TOTAL'): trans($MER_OUR_LANGUAGE.'.MER_SUB_TOTAL') }}</th>
                                               <th  width="" align="center">{{ (Lang::has(Session::get('lang_file').'.COUPON_AMOUNT')!= '') ?  trans(Session::get('lang_file').'.COUPON_AMOUNT') : trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT') }}</th>
											   <th  width="" align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS') : trans($MER_OUR_LANGUAGE.'.MER_PAYMENT_STATUS') }}
                                             <th  width="" align="center">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DELIVERY_STATUS') : trans($MER_OUR_LANGUAGE.'.BACK_DELIVERY_STATUS') }}</th>
                                         <?php //$subtotal=$shipping->order_amt;  
                                         
                                         ?>   
										 <?php
									  $product_titles=DB::table('nm_order_payu')
									  ->leftjoin('nm_product', 'nm_order_payu.order_pro_id', '=', 'nm_product.pro_id')
									  ->leftjoin('nm_color', 'nm_order_payu.order_pro_color', '=', 'nm_color.co_id')
									  ->leftjoin('nm_size', 'nm_order_payu.order_pro_size', '=', 'nm_size.si_id')
									  ->where('transaction_id', '=', $shipping->transaction_id)
									  ->where('nm_order_payu.order_merchant_id', $merid)
									  ->where('nm_order_payu.order_type', '=',1)
									  ->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax =$all_shipping_amt = 0; ?>
									  @foreach($product_titles as $prd_tit) 
                                       <?php
                                             $all_shipping_amt=$all_shipping_amt+ $prd_tit->order_shipping_amt; 
										  	
																$status=$prd_tit->delivery_status; ?>
											 @if($prd_tit->delivery_status==1)
                      @php
                      $orderstatus="Order Placed";
                      @endphp
                      @elseif($prd_tit->delivery_status==2) 
                      @php
                      $orderstatus="Order Packed";
                      @endphp
                      @elseif($prd_tit->delivery_status==3) 
                      @php
                      $orderstatus="Dispatched";
                      @endphp
                      @elseif($prd_tit->delivery_status==4) 
                      @php
                      $orderstatus="Delivered";
                      @endphp
                      @elseif($prd_tit->delivery_status==5)
                      @php
                        $orderstatus="cancel pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==6) 
                      @php
                      $orderstatus="cancelled";
                      @endphp
                      @elseif($prd_tit->delivery_status==7) 
                      @php
                      $orderstatus="return pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==8) 
                      @php
                      $orderstatus="Returned";
                      @endphp
            @elseif($prd_tit->delivery_status==9) 
                      @php
                      $orderstatus="Replace Pending";
                      @endphp
                      @elseif($prd_tit->delivery_status==10) 
                     @php
                      $orderstatus="Replaced";
                      @endphp
            @else
            @php $orderstatus = '';  @endphp
              @endif
											
											
											@if($prd_tit->order_status==1)
                      @php
                      $payment_status="Success";
                      @endphp
                      @elseif($prd_tit->order_status==2) 
                      @php
                      $payment_status="Order Packed";
                      @endphp
                      @elseif($prd_tit->order_status==3) 
                      @php
                      $payment_status="Hold";
                      @endphp
                      @elseif($prd_tit->order_status==4) 
                      @php
                      $payment_status="Faild";
                      @endphp
                      @endif
                      <?php 
										  $subtotal=$prd_tit->order_amt; 
																$tax_amt = (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
																$total_tax_amt+= (($prd_tit->order_amt * $prd_tit->order_tax)/100); 
																$total_ship_amt+= $prd_tit->order_shipping_amt;
																$total_item_amt+=$prd_tit->order_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->order_pro_id;
																
														$item_amt = $prd_tit->order_amt + (($prd_tit->order_amt * $prd_tit->order_tax)/100);
															
														
															 $ship_amt = $prd_tit->order_shipping_amt;
															
														
															 //$item_tax = $codorderdet->cod_tax;
															/*if($prd_tit->coupon_amount != 0)
															{
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
															}
															else
															{
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
															}*/		
                                                            $grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
									  ?>
                                    </tr>
                                    
                                     <tr>	
									  <td  width="" align="center"> @if($prd_tit->pro_title != '') {{ $prd_tit->pro_title }} @else {{  "-" }} @endif</td>
                                    <td  width="" align="center">@if($prd_tit->co_name!= '') {{ $prd_tit->co_name }} @else  {{ "-" }} @endif</td>
                                      <td  width="" align="center">@if($prd_tit->si_name != '') {{ $prd_tit->si_name}} @else {{ "-"}} @endif</td>
                                        <td  width="" align="center">{{ $prd_tit->order_qty }} </td>
                                        <td  width="" align="center">{{ Helper::cur_sym() }}  {{ $prd_tit->order_prod_unitPrice }} <?php //echo $prd_tit->pro_disprice;?> </td>
                                        <td  width="" align="center">{{ Helper::cur_sym() }}{{ $subtotal - $prd_tit->coupon_amount }} </td>
                                            @if($prd_tit->coupon_amount != 0)
															<td  width="" align="center">{{ Helper::cur_sym() }} {{ $prd_tit->coupon_amount  }}</td>
															@else <td  width="" align="center">{{ Helper::cur_sym() }} {{ $prd_tit->coupon_amount  }} </td>@endif
															<td  width="" align="center">{{ $payment_status }}</td>
											<td  width="" align="center"> {{ $orderstatus }}</td>											
                                    </tr>

									@endforeach
                                    </table></div>
                                    <br>
                                    <hr>
                                    <div class="">
                                    <div class="col-lg-6"></div>
                                     <div class="col-lg-6">
                               <div>{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPMENT_VALUE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPMENT_VALUE') : trans($MER_OUR_LANGUAGE.'.MER_SHIPMENT_VALUE') }}<p class="pull-right sss" style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $all_shipping_amt }} </p></div><br>
                                        <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TAX')!= '') ? trans(Session::get('mer_lang_file').'.MER_TAX') : trans($MER_OUR_LANGUAGE.'.MER_TAX')}}<p class="pull-right"style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $all_tax_amt }}</p></span><br>
                                        
                                    @if(count($wallet)!=0)
                                        <span>wallet : <p class="pull-right aaa" style="margin-right:-28px;"> {{ Helper::cur_sym() }} -{{ $wallet_amt}}</p></span><br>
                                     @endif
                                    

                                     <?php   /*if($shipping->coupon_amount !=0){ ?>
                                         <span>coupon Code : <b class="pull-right"style="margin-right:15px;">- <?php echo $coupon_value;?></b></span><br>
                <?php   }*/
                                     ?>
                                        <hr>

                                      <strong> <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT') }}<p class="pull-right bbb"style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $grand_total-$wallet_amt }}</p></span></strong>
                                     </div>
                                    </div>
                        
                                        
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-danger" type="button">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLOSE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLOSE') : trans($MER_OUR_LANGUAGE.'.MER_CLOSE') }}</button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

   <?php $i=$i+1; ?>
   @endforeach         
    <!-- FOOTER -->
    <div id="footer">
        <p>&copy; <?php echo $SITENAME; ?> &nbsp;<?php echo date("Y"); ?>&nbsp;</p>
    </div>
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
       <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>

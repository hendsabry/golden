<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<style>
  div.table {border-radius: 10px; display: table; margin: 20px 0 30px 3%; padding: 3px 3px 1px; width: 94%;}
div.tr {background: none repeat scroll 0 0 #ffffff; display: table-row;}
div.td { color:#000; display: table-cell; font-size: 14pt; padding: 5px 15px; vertical-align: top;  word-wrap:break-word; }
.table_heading{color: #4b4b4b; display: table-cell; font-size: 14pt; font-weight: bold; margin: 0; padding: 5px 15px 10px; text-transform:uppercase;}

@media only screen and (max-width: 600px) {
.table_heading{display:none!important;}
.table {width: 100%;}  
.table .td {width: 75%; border: 0; padding:0px 0px 5px 25%; float: left; min-height:25px; margin-bottom:10px; position: relative;text-align:left!important;}
.table .td:before{color: #626262;position: absolute; left: 10px; font-weight: bold; width:40%; float:left; line-height:13px; content: attr(data-title);}   
  

}
   


</style>
<head>
<?php /*if (Session::has('merchantid'))
{
    $merchantid=Session::get('merchantid');
}*/

?>
@if (Session::has('merchantid'))
<?php   $merchantid=Session::get('merchantid'); ?>
@endif
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }}| @if (Lang::has(Session::get('mer_lang_file').'.MER_DASHBOARD')!= '') 
     {{ trans(Session::get('mer_lang_file').'.MER_DASHBOARD') }}
       @else 
     {{  trans($MER_OUR_LANGUAGE.'.MER_DASHBOARD') }}
      @endif </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('')}}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('')}}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('')}}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
    <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>
    <!-- <script src="http://192.168.2.50/nexemerchant/public/assets/plugins/jquery-2.0.3.min.js"></script>-->
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	
        <!-- END HEADER SECTION -->

  {!!$merchantheader!!}

        <!-- MENU SECTION -->
       <div id="left" >
           
 
        </div>
        <!--END MENU SECTION -->
		<div class="container">
        	<div class="row">
                    

                </div>
        	
        </div>


        <!--PAGE CONTENT -->
        <div class=" container" >
            <div class="inner" style="min-height: 700px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                        	<header>
                <div class="icons"><i class="icon-dashboard"></i></div>
  <h5> @if (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_DASHBOARD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MERCHANT_DASHBOARD') }}  
  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_DASHBOARD') }} @endif </h5>
            

           
            </header>
              
@php $sold_cnt=0; @endphp
 @foreach($soldproductscnt as $soldres)
	@if($soldres->pro_no_of_purchase	>=$soldres->pro_qty)
		
@php		$sold_cnt++; @endphp
		@endif
	@endforeach 

 @php
  $active_withAvailableQty = ($activeproductscnt-$sold_cnt)>0?($activeproductscnt-$sold_cnt):0;
@endphp

        <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               <strong>@if (Lang::has(Session::get('mer_lang_file').'.MER_MONTH_WISE_TRANSACTIONS')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_MONTH_WISE_TRANSACTIONS') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_MONTH_WISE_TRANSACTIONS') }} @endif

                               </strong>
                            </div>
                            
                            <div class="panel-body panel panel-default"> 

                  <div class="table">
                          <div class="tr">
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.SERVICES_NAME')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.SERVICES_NAME') }}  @else {{ trans($MER_OUR_LANGUAGE.'.SERVICES_NAME') }} @endif</div>
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_DATE')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_DATE') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_DATE') }} @endif  </div>
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.PAYMENT_STATUS')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.PAYMENT_STATUS') }}  @else {{ trans($MER_OUR_LANGUAGE.'.PAYMENT_STATUS') }} @endif </div>
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif</div>
                          

                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                          </div>


                    @foreach($userservices['services'] as $val)
                  @if((Session::get('mer_lang_code'))== '' || (Session::get('mer_lang_code'))== 'en')  
                      @php    $sb_name = 'sb_name'; @endphp
                      @else @php  $sb_name = 'sb_name_'.Session::get('mer_lang_code'); @endphp @endif
                   <div class="tr">
                     <div class="td td1" data-title="Trivia">
 <b>{{ $val->$sb_name }}</b>

                     


                    </div>
                     <div class="td td2" data-title="Status">{{ Carbon\Carbon::parse($userservices['date'])->format('F j, Y') }} </div>

                     <div class="td td3" data-title="Rated">@if (Lang::has(Session::get('mer_lang_file').'.MER_PENDING')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_PENDING') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_PENDING') }} @endif</div>

                <div class="td td4" data-title="PTS">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</div>
                                <div class="td td5" data-title="PTS">
             <a href="{{route("service",['id' => $val->sb_id]) }}">Add | </a>
              <a href="{{route("servicelist",['id' => $val->sb_id]) }}">View</a>
               </div> 
            </div> 



            @endforeach 
        </div>

                          
           
            <?php /*
            <div class="panel-heading text-center">
                               <strong>Auction Transaction </strong>
                            </div>		
              <div class="demo-container" id="chart3" style="margin-top:20px; margin-left:20px; width:950px; height:470px;"></div>	
        */ ?>
        
      </div>
                             
		
                            </div>
                    </div>

                    
                    
                </div>
                
                
                 
                 <div class="row">
                    <div class="col-lg-12">
                   
                         <?php /*?><div class="panel panel-default">
                                <div class="panel-heading">
                                   Last one year Transactions report
                                </div>
                             
                            <div class="panel-body">
                              
								  <div class="demo-container" id="chart5" style="margin-top:20px; margin-left:20px; width:950px; height:470px;"></div>	
								</div>
                             
		
                            </div><?php */?>
                    </div>

                    
                   
                </div>
                 
                          
              

                
            </div>

        </div>
       
    </div>

    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    {!! $merchantfooter !!}
    <!--END FOOTER -->
    @if($dealchartdetails!='')
    @if($dealchartdetails!='0,0,0,0,0,0,0,0,0,0,0,0')
   
     <script class="code" type="text/javascript">
     $(document).ready(function(){ 
     
        $.jqplot.config.enablePlugins = true;
    
    <?php $s1 = "[" .$dealchartdetails. "]"; ?>
        var s1 = {{ $s1 }};
        var ticks = ['{{ (Lang::has(Session::get('mer_lang_file').'.MER_JAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JAN') : trans($MER_OUR_LANGUAGE.'.MER_JAN') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_FEB')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FEB') : trans($MER_OUR_LANGUAGE.'.MER_FEB') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAR') : trans($MER_OUR_LANGUAGE.'.MER_MAR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_APR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_APR'): trans($MER_OUR_LANGUAGE.'.MER_APR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAY'): trans($MER_OUR_LANGUAGE.'.MER_MAY')}}','{{ (Lang::has(Session::get('mer_lang_file').'.MER_JUNE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JUNE'): trans($MER_OUR_LANGUAGE.'.MER_JUNE')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_JULY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JULY'): trans($MER_OUR_LANGUAGE.'.MER_JULY')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_AUG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUG') : trans($MER_OUR_LANGUAGE.'.MER_AUG')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEP') :  trans($MER_OUR_LANGUAGE.'.MER_SEP')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_OCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_OCT') : trans($MER_OUR_LANGUAGE.'.MER_OCT')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_NOV')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NOV'): trans($MER_OUR_LANGUAGE.'.MER_NOV')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEC')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEC'): trans($MER_OUR_LANGUAGE.'.MER_DEC')}}'];
        
        plot1 = $.jqplot('chart2', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart2').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
  @endif
  @endif
  @if(($activedealscnt+$archievddealcnt)>0)  
    <script>
	$(document).ready(function(){
		
	plot6 = $.jqplot('chart6', [[{{ $activedealscnt }},{{ $archievddealcnt }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } } );
		});
	</script>
  @endif
  @if(($active_withAvailableQty+$sold_cnt)>0)
  <script>
	$(document).ready(function(){
		
	plot10 = $.jqplot('chart10', [[{{ $active_withAvailableQty }},{{ $sold_cnt }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } });
		});
	</script>
  @endif
  @if($productchartdetails!='')
  @if($productchartdetails!='0,0,0,0,0,0,0,0,0,0,0,0')
      <script class="code" type="text/javascript">
      $(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php $s1 = "[" .$productchartdetails. "]"; ?>
        var s1 = {{ $s1 }};
        var ticks = ['{{ (Lang::has(Session::get('mer_lang_file').'.MER_JAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JAN') : trans($MER_OUR_LANGUAGE.'.MER_JAN') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_FEB')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FEB') : trans($MER_OUR_LANGUAGE.'.MER_FEB') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAR') : trans($MER_OUR_LANGUAGE.'.MER_MAR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_APR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_APR'): trans($MER_OUR_LANGUAGE.'.MER_APR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAY'): trans($MER_OUR_LANGUAGE.'.MER_MAY')}}','{{ (Lang::has(Session::get('mer_lang_file').'.MER_JUNE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JUNE'): trans($MER_OUR_LANGUAGE.'.MER_JUNE')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_JULY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JULY'): trans($MER_OUR_LANGUAGE.'.MER_JULY')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_AUG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUG') : trans($MER_OUR_LANGUAGE.'.MER_AUG')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEP') :  trans($MER_OUR_LANGUAGE.'.MER_SEP')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_OCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_OCT') : trans($MER_OUR_LANGUAGE.'.MER_OCT')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_NOV')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NOV'): trans($MER_OUR_LANGUAGE.'.MER_NOV')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEC')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEC'): trans($MER_OUR_LANGUAGE.'.MER_DEC')}}'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    @endif
    @endif
    @if($auctionchartdetails!='0,0,0,0,0,0,0,0,0,0,0,0')   
     <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		<?php $s1 = "[" .$auctionchartdetails. "]"; ?>
        var s1 = {{ $s1 }};
        var ticks = ['{{ (Lang::has(Session::get('mer_lang_file').'.MER_JAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JAN') : trans($MER_OUR_LANGUAGE.'.MER_JAN') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_FEB')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FEB') : trans($MER_OUR_LANGUAGE.'.MER_FEB') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAR') : trans($MER_OUR_LANGUAGE.'.MER_MAR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_APR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_APR'): trans($MER_OUR_LANGUAGE.'.MER_APR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAY'): trans($MER_OUR_LANGUAGE.'.MER_MAY')}}','{{ (Lang::has(Session::get('mer_lang_file').'.MER_JUNE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JUNE'): trans($MER_OUR_LANGUAGE.'.MER_JUNE')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_JULY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JULY'): trans($MER_OUR_LANGUAGE.'.MER_JULY')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_AUG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUG') : trans($MER_OUR_LANGUAGE.'.MER_AUG')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEP') :  trans($MER_OUR_LANGUAGE.'.MER_SEP')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_OCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_OCT') : trans($MER_OUR_LANGUAGE.'.MER_OCT')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_NOV')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NOV'): trans($MER_OUR_LANGUAGE.'.MER_NOV')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEC')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEC'): trans($MER_OUR_LANGUAGE.'.MER_DEC')}}'];
        
        plot1 = $.jqplot('chart3', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart3').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    @endif
   
    <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    

    <!-- GLOBAL SCRIPTS -->
  	
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="{{ url('')}}/public/assets/js/bar_chart.js"></script>
    
    <!-- END PAGE LEVEL SCRIPTS -->
	<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>

</body>

    <!-- END BODY -->
</html>

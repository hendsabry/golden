@include('sitemerchant.includes.header')  
@php $mensbig_leftmenu =1; @endphp
<!--start merachant-->
<div class="merchant_vendor"> 
 @php
  $id                = $id;
  $sid               = $sid;
  $pid               = $pid;
  $cusid             = $cusid;
  $oid               = $oid;
  $itemid            = $itemid;
  $opid              = $opid;
  $ordid  = $oid;
  $getCustomer       = Helper::getuserinfo($cusid);
  $shipaddress       = Helper::getgenralinfo($oid);  
  $cityrecord        = Helper::getcity($shipaddress->shipping_city);
  $getorderedproduct = Helper::getorderedproduct($oid);
  @endphp
  <!--left panel-->
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif<a href="{{url('/')}}/mens-order/{{$id}}/{{$sid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a></h5>
        </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox" style="max-width: 850px;">
             <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} @endphp <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" > @php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      @endphp </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
                  @php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod' && strtolower($shipaddress->order_paytype)!='wallet'){ @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                  </div>
                  @php } @endphp
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                    <div class="info100"> @php if(isset($shipaddress->shipping_id) && $shipaddress->shipping_id!='')
                      { 
                      $getName = Helper::getShippingMethodName($shipaddress->shipping_id);
                      echo $getName;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100"> @php if(isset($shipaddress->shipping_charge) && $shipaddress->shipping_charge!='' && $shipaddress->shipping_charge!='0')
                      {
                      echo $shipaddress->shipping_charge;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                    <!-- box -->
                  </div>
                </div>
                <!--global end-->
              </div>
			@php 
			  $basetotal = 0;
			  $i = 1;
        $couponcode=0; $couponcodeinfo='';
			  if(count($productdata) > 0){

			  foreach($productdata as $val)
			  {
			    $basetotal            = ($basetotal+$val->total_price);
			    $pid                  = $val->product_id;
				$service_charge       = Helper::getshopname($val->bookingdetail->shop_id);
				$serviceInfo          = Helper::getProduckInfo($val->bookingdetail->service_id);
				$staffname            = Helper::getStaffNameOne($val->bookingdetail->staff_id);
				$getAddresInfo        = Helper::getuserinfo($val->bookingdetail->cus_id);
			  if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$priceperday = $serviceInfo->pro_disprice;}else{$priceperday = $serviceInfo->pro_price;}
        $couponcode=$couponcode+$val->coupon_code_amount;;
        $couponcodeinfo=$val->coupon_code;
			  @endphp
      <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
        <div class="style_area">
          <?php if($i==1){?>
          <div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.item_information')!= '') {{  trans(Session::get('mer_lang_file').'.item_information') }} @else  {{ trans($MER_OUR_LANGUAGE.'.item_information') }} @endif</div>
          <?php } ?>
          <div class="style_box_type">
            <div class="sts_box">
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Service')!= '') {{  trans(Session::get('mer_lang_file').'.Service') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Service') }} @endif</div>
                <div class="style_label_text"> 
                 <?php 
                            $protitle = 'pro_title'; 
                            if(Session::get('mer_lang_file')!='mer_en_lang')
                            {
                            $protitle = 'pro_title_ar'; 
                            }
                            echo $serviceInfo->$protitle;
                             ?>

                 </div>
              </div>
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.DURATION')!= '') {{  trans(Session::get('mer_lang_file').'.DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.DURATION') }} @endif</div>
                <div class="style_label_text"><?php if(isset($serviceInfo->service_hour) && $serviceInfo->service_hour!=''){echo $serviceInfo->service_hour;}else{echo 'N/A';} ?></div>
              </div>
              <div class="style_left">
                <div class="style_label">@if(Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif</div>
                <div class="style_label_text">SAR {{ number_format($val->total_price,2) }}</div>
              </div>
            </div>
            <div class="sts_box">
              
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.BOOKING_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKING_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKING_DATE') }} @endif</div>
                <div class="style_label_text"><?php if(isset($val->bookingdetail->booking_date) && $val->bookingdetail->booking_date!=''){echo date('d M Y',strtotime($val->bookingdetail->booking_date)); }else{echo 'N/A';}?></div>
              </div>
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.BOOKING_TIME')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKING_TIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKING_TIME') }} @endif</div>
                <div class="style_label_text"><?php if(isset($val->bookingdetail->start_time) && $val->bookingdetail->start_time!=''){echo $val->bookingdetail->start_time;}else{echo 'N/A';}?></div>
              </div>
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.BOOKING_PLACE')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKING_PLACE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKING_PLACE') }} @endif</div>
                <div class="style_label_text"><?php if(isset($val->bookingdetail->booking_place) && $val->bookingdetail->booking_place!=''){ 
                  if($val->bookingdetail->booking_place=='home')
                  { //echo $val->bookingdetail->booking_place; ?>
                    @if (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HOME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_HOME') }} @endif
                  <?php } else { ?>
                     @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP') }} @endif

                 <?php }}else{echo 'N/A';}?></div>
              </div>
            </div>
			<div class="sts_box">
			<div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.STAFF')!= '') {{  trans(Session::get('mer_lang_file').'.STAFF') }} @else  {{ trans($MER_OUR_LANGUAGE.'.STAFF') }} @endif</div>
                <div class="style_label_text"><?php if(isset($staffname->staff_member_name) && $staffname->staff_member_name!=''){ echo $staffname->staff_member_name;}else{echo 'N/A';} ?></div>
              </div>              
			  <?php if(isset($val->bookingdetail->booking_place) && $val->bookingdetail->booking_place=='home'){ ?> 			  
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.HOME_CHARGES')!= '') {{  trans(Session::get('mer_lang_file').'.HOME_CHARGES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.HOME_CHARGES') }} @endif</div>
                <div class="style_label_text"> SAR <?php echo $service_charge->home_visit_charge; ?></div>
              </div>
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_ADDRESS') }} @endif</div>
                <div class="style_label_text">{{$getAddresInfo->cus_address1 or ''}}</div>
              </div>
			  <?php } ?>
			</div>
            <!--div class="sts_box">
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</div>
                <div class="style_label_text"> @if($serviceInfo->pro_Img) <img src="{{ $serviceInfo->pro_Img }}" alt="" width="150"> @else
                  No image
                  @endif </div>
              </div>
              
            </div-->
          </div>
        </div>
      </div>
      @php $i++;} @endphp
      <div class="merchant-order-total-area">
        <div class="merchant-order-total-line"> @if(isset($couponcode) && $couponcode>=1)
             {{ (Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')}}: 

             @php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp  
          <br>

          {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')}}: {{ $couponcodeinfo }}
 <br>
          @endif



            @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             @endphp

          {{ (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp </div>
       <div class="merchant-order-total-line"> @if (Lang::has(Session::get('mer_lang_file').'.Total_Price')!= '') {{  trans(Session::get('mer_lang_file').'.Total_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Total_Price') }} @endif:
        <?php if(isset($shipaddress) && $shipaddress!=''){
				  echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);
				 } ?>
				 </div>
      </div> <!-- merchant-order-total-area -->
      <?php } ?>  
			 </div>
          <!-- box -->
        </div>
      </div>
      <!--global end-->
    </div>
  </div>
</div>
<!--end right panel-->
</div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $mensbig_leftmenu =1; @endphp
<div class="merchant_vendor">   @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      </header>
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
            <form name="form1" method="post" id="addbranch" action="{{ route('store_mansbarber-info') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="mc_name" value="{{ $fetchdata->mc_name or '' }}" maxlength="255" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="mc_name_ar" value=" {{ $fetchdata->mc_name_ar or '' }} "  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp </span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a> </label>
                  <div class="info100">
                    <input type="url" name="google_map_address"  maxlength="350"  data-validation="length required" 
                    
                     value=" {{ $fetchdata->google_map_address or '' }}" data-validation-length="max100">
                  </div>
                </div>
              </div>
   <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LONG')!= '') {{  trans(Session::get('mer_lang_file').'.LONG') }}  @else  {{ trans($OUR_LANGUAGE.'.LONG') }} @endif </label>
                  <input type="text" class="form-control" value="{{$fetchdata->longitude or ''}}" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LAT')!= '') {{  trans(Session::get('mer_lang_file').'.LAT') }}  @else  {{ trans($OUR_LANGUAGE.'.LAT') }} @endif </label>
                  <input type="text" class="form-control" value="{{$fetchdata->latitude or ''}}"  readonly="" name="latitude" />
          </div>
                  </div>



              <div class="form_row">
                <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_IMAGE'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value1"></div>
                    </div>
                    </label>
                    <input type="file" name="branchimage" id="company_logo" class="info-file">
                  </div>
                    <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                  <div class="form-upload-img">@if(isset($fetchdata->mc_img) && $fetchdata->mc_img !='') <img src="{{ $fetchdata->mc_img }}" width="150" height="150"> @endif </div>
                  @if($errors->has('branchimage')) <span class="error"> {{ $errors->first('branchimage') }} </span> @endif </div>


                <!--<div class="form_row_right common_field">
                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="address_image_tooltip"> <span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span> </a> </label>
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value2"></div>
                    </div>
                    </label>
                    <input type="file" name="address_image" id="company_logo1" class="info-file">
                  </div>
                    <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                  <div class="form-upload-img">@if(isset($fetchdata->address_image) && $fetchdata->address_image !='') <img src="{{ $fetchdata->address_image }}" > @endif</div>
                  @if($errors->has('address_image')) <span class="error"> {{ $errors->first('address_image') }} </span> @endif </div>
-->




              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_ADDRESS'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="address" maxlength="235"  data-validation="length required" 
     data-validation-error-msg="Please enter hall name" value="{{$fetchdata->address or ''}}" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="address_ar" value="{{$fetchdata->address_ar or ''}}"  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="englvish">
                      <select name="city_id" id="status" class="city_type">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif</option>
                        
               @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;">@if($mer_selected_lang_code !='en'){{$cbval->co_name_ar}} @else {{$cbval->co_name}} @endif</option>
                        
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                         @php $ci_id = $val->ci_id; @endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        
                        <option value="{{ $val->ci_id }}" {{ isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''}} >{{ $val->$ci_name }}</option>
                        
                        @endforeach
                        @endforeach
            
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form_row">
                <div class="form_row_left common_field">
                  <div class="info100" >
                    <div class="englivsh">
                      <div class="small_time ">
                        <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.OPENING'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.OPENING'); @endphp </span> </label>
                        <select name="opening_time" id="opening_time" >
                          <option value="">  @if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif</option>
                          <option value="12:00 AM"{{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:00 AM' ? 'selected' : ''}}> 12:00 AM</option>
                          <option value="00:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='00:30 AM' ? 'selected' : ''}}> 00.30 AM</option>
                          <option value="1.00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1.00 AM' ? 'selected' : ''}}> 1:00 AM</option>
                          <option value="1:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1:30 AM' ? 'selected' : ''}}> 1:30 AM</option>
                          <option value="2:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:00 AM' ? 'selected' : ''}}> 2:00 AM</option>
                          <option value="2:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:30 AM' ? 'selected' : ''}}> 2:30 AM</option>
                          <option value="3:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3:00 AM' ? 'selected' : ''}}> 3:00 AM</option>
                          <option value="3.30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3.30 AM' ? 'selected' : ''}}> 3:30 AM</option>
                          <option value="4.00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4.00 AM' ? 'selected' : ''}}> 4:00 AM</option>
                          <option value="4:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4:30 AM' ? 'selected' : ''}}> 4:30 AM</option>
                          <option value="5:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:00 AM' ? 'selected' : ''}}> 5:00 AM</option>
                          <option value="5:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:30 AM' ? 'selected' : ''}}>5:30 AM</option>
                          <option value="6:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:00 AM' ? 'selected' : ''}}> 6:00 AM</option>
                          <option value="6:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:30 AM' ? 'selected' : ''}}> 6:30 AM</option>
                          <option value="7:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:00 AM' ? 'selected' : ''}}> 7:00 AM</option>
                          <option value="7:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:30 AM' ? 'selected' : ''}} > 7:30 AM</option>
                          <option value="8:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:00 AM' ? 'selected' : ''}}> 8:00 AM</option>
                          <option value="8:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:30 AM' ? 'selected' : ''}}> 8:30 AM</option>
                          <option value="9:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:00 AM' ? 'selected' : ''}}> 9:00 AM</option>
                          <option value="9:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:30 AM' ? 'selected' : ''}}> 9:30 AM</option>
                          <option value="10:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:00 AM' ? 'selected' : ''}}> 10:00 AM</option>
                          <option value="10:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:30 AM' ? 'selected' : ''}}> 10:30 AM</option>
                          <option value="11:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:00 AM AM' ? 'selected' : ''}}> 11:00 AM</option>
                          <option value="11:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:30 AM' ? 'selected' : ''}}> 11:30 AM</option>
                          <option value="12:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:00 PM' ? 'selected' : ''}}> 12:00 PM</option>
                          <option value="12:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:30 PM' ? 'selected' : ''}}> 12.30 PM</option>
                          <option value="1.00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1.00 PM' ? 'selected' : ''}}> 1:00 PM</option>
                          <option value="1:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1:30 PM' ? 'selected' : ''}}> 1:30 PM</option>
                          <option value="2:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:00 PM' ? 'selected' : ''}}> 2:00 PM</option>
                          <option value="2:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:30 PM' ? 'selected' : ''}}> 2:30 PM</option>
                          <option value="3:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3:00 PM' ? 'selected' : ''}}> 3:00 PM</option>
                          <option value="3.30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3.30 PM' ? 'selected' : ''}}> 3:30 PM</option>
                          <option value="4.00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4.00 PM' ? 'selected' : ''}}> 4:00 PM</option>
                          <option value="4:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4:30 PM' ? 'selected' : ''}}> 4:30 PM</option>
                          <option value="5:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:00 PM' ? 'selected' : ''}}> 5:00 PM</option>
                          <option value="5:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:30 PM' ? 'selected' : ''}}>5:30 PM</option>
                          <option value="6:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:00 PM' ? 'selected' : ''}} > 6:00 PM</option>
                          <option value="6:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:30 PM' ? 'selected' : ''}}> 6:30 PM</option>
                          <option value="7:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:00 PM' ? 'selected' : ''}}> 7:00 PM</option>
                          <option value="7:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:30 PM' ? 'selected' : ''}}> 7:30 PM</option>
                          <option value="8:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:00 PM' ? 'selected' : ''}}> 8:00 PM</option>
                          <option value="8:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:30 PM' ? 'selected' : ''}}> 8:30 PM</option>
                          <option value="9:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:00 PM' ? 'selected' : ''}}> 9:00 PM</option>
                          <option value="9:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:30 PM' ? 'selected' : ''}}> 9:30 PM</option>
                          <option value="10:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:00 PM' ? 'selected' : ''}}> 10:00 PM</option>
                          <option value="10:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:30 PM' ? 'selected' : ''}}> 10:30 PM</option>
                          <option value="11:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:00 PM' ? 'selected' : ''}}> 11:00 PM</option>
                          <option value="11:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:30 PM' ? 'selected' : ''}}> 11:30 PM</option>
                        </select>
                      </div>
                      <div class="small_time">
                        <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.ClOSING'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.ClOSING'); @endphp </span> </label>
                        <select name="closing_time" id="closing_titme" >
                          <option value="">  @if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif</option>
                          <option value="12:00 AM"{{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:00 AM' ? 'selected' : ''}}> 12:00 AM</option>
                          <option value="00:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='00:30 AM' ? 'selected' : ''}}> 00.30 AM</option>
                          <option value="1.00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1.00 AM' ? 'selected' : ''}}> 1:00 AM</option>
                          <option value="1:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1:30 AM' ? 'selected' : ''}}> 1:30 AM</option>
                          <option value="2:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:00 AM' ? 'selected' : ''}}> 2:00 AM</option>
                          <option value="2:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:30 AM' ? 'selected' : ''}}> 2:30 AM</option>
                          <option value="3:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3:00 AM' ? 'selected' : ''}}> 3:00 AM</option>
                          <option value="3.30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3.30 AM' ? 'selected' : ''}}> 3:30 AM</option>
                          <option value="4.00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4.00 AM' ? 'selected' : ''}}> 4:00 AM</option>
                          <option value="4:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4:30 AM' ? 'selected' : ''}}> 4:30 AM</option>
                          <option value="5:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:00 AM' ? 'selected' : ''}}> 5:00 AM</option>
                          <option value="5:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:30 AM' ? 'selected' : ''}}>5:30 AM</option>
                          <option value="6:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:00 AM' ? 'selected' : ''}}> 6:00 AM</option>
                          <option value="6:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:30 AM' ? 'selected' : ''}}> 6:30 AM</option>
                          <option value="7:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:00 AM' ? 'selected' : ''}}> 7:00 AM</option>
                          <option value="7:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:30 AM' ? 'selected' : ''}} > 7:30 AM</option>
                          <option value="8:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:00 AM' ? 'selected' : ''}}> 8:00 AM</option>
                          <option value="8:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:30 AM' ? 'selected' : ''}}> 8:30 AM</option>
                          <option value="9:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:00 AM' ? 'selected' : ''}}> 9:00 AM</option>
                          <option value="9:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:30 AM' ? 'selected' : ''}}> 9:30 AM</option>
                          <option value="10:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:00 AM' ? 'selected' : ''}}> 10:00 AM</option>
                          <option value="10:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:30 AM' ? 'selected' : ''}}> 10:30 AM</option>
                          <option value="11:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:00 AM AM' ? 'selected' : ''}}> 11:00 AM</option>
                          <option value="11:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:30 AM' ? 'selected' : ''}}> 11:30 AM</option>
                          <option value="12:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:00 PM' ? 'selected' : ''}}> 12:00 PM</option>
                          <option value="12:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:30 PM' ? 'selected' : ''}}> 12.30 PM</option>
                          <option value="1.00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1.00 PM' ? 'selected' : ''}}> 1:00 PM</option>
                          <option value="1:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1:30 PM' ? 'selected' : ''}}> 1:30 PM</option>
                          <option value="2:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:00 PM' ? 'selected' : ''}}> 2:00 PM</option>
                          <option value="2:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:30 PM' ? 'selected' : ''}}> 2:30 PM</option>
                          <option value="3:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3:00 PM' ? 'selected' : ''}}> 3:00 PM</option>
                          <option value="3.30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3.30 PM' ? 'selected' : ''}}> 3:30 PM</option>
                          <option value="4.00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4.00 PM' ? 'selected' : ''}}> 4:00 PM</option>
                          <option value="4:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4:30 PM' ? 'selected' : ''}}> 4:30 PM</option>
                          <option value="5:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:00 PM' ? 'selected' : ''}}> 5:00 PM</option>
                          <option value="5:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:30 PM' ? 'selected' : ''}}>5:30 PM</option>
                          <option value="6:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:00 PM' ? 'selected' : ''}} > 6:00 PM</option>
                          <option value="6:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:30 PM' ? 'selected' : ''}}> 6:30 PM</option>
                          <option value="7:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:00 PM' ? 'selected' : ''}}> 7:00 PM</option>
                          <option value="7:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:30 PM' ? 'selected' : ''}}> 7:30 PM</option>
                          <option value="8:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:00 PM' ? 'selected' : ''}}> 8:00 PM</option>
                          <option value="8:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:30 PM' ? 'selected' : ''}}> 8:30 PM</option>
                          <option value="9:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:00 PM' ? 'selected' : ''}}> 9:00 PM</option>
                          <option value="9:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:30 PM' ? 'selected' : ''}}> 9:30 PM</option>
                          <option value="10:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:00 PM' ? 'selected' : ''}}> 10:00 PM</option>
                          <option value="10:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:30 PM' ? 'selected' : ''}}> 10:30 PM</option>
                          <option value="11:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:00 PM' ? 'selected' : ''}}> 11:00 PM</option>
                          <option value="11:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:30 PM' ? 'selected' : ''}}> 11:30 PM</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <span class="service_prov">@if (Lang::has(Session::get('mer_lang_file').'.Service_provider')!= '') {{  trans(Session::get('mer_lang_file').'.Service_provider') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Service_provider') }} @endif </span> </div>
                <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.SERVICEAVAIL'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SERVICEAVAIL'); @endphp </span> </label>
                  <div class="info100">
                    <div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" {{ isset($fetchdata->
                      service_availability) && ($fetchdata->service_availability =='1' or $fetchdata->service_availability =='3')  ? 'checked' : ''}} name="service_available[]" value="1" id="type">
                      <label for="type"> <span class="english">@php echo lang::get('mer_en_lang.HOME'); @endphp</span> <span class="arabic ar">@php echo lang::get('mer_ar_lang.HOME'); @endphp </span> </label>
                    </div>
                    <div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" {{ isset($fetchdata->
                      service_availability) && ($fetchdata->service_availability =='2' or $fetchdata->service_availability =='3') ? 'checked' : ''}}  data-validation="checkbox_group" name="service_available[]" value="2" id="dish">
                      <label for="dish"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP'); @endphp</span> <span class="arabic ar">@php echo lang::get('mer_ar_lang.MER_SHOP'); @endphp </span> </label>
                    </div>
                    <span for="service_available[]" generated="true" class="error"> </span> </div>
                </div>
              </div>
              <div class="form_row common_field">
                <div class="homevisit ">
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.HOMEVISIT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.HOMEVISIT'); @endphp </span> </label>
                    <div class="info100" >
                      <div class="englishs">
                        <input type="text" class="english small-sel notzero" onkeypress="return isNumber(event)" name="home_visit_charge" maxlength="8"  data-validation="length required" 
                       value="{{ $fetchdata->home_visit_charge or ''}}" data-validation-length="max35">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50">{{ $fetchdata->mc_discription or ''}} </textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50">{{ $fetchdata->mc_discription_ar or ''}}</textarea>
                    </div>
                  </div>
                </div>
                <div class="form_row_right english">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo7">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value6"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc" id="company_logo7" class="info-file">
                  </div>
                       <div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
                  @if(isset($fetchdata->terms_conditions)!='')
             
                  <div class="pdf_icon"> <a href="{{$fetchdata->terms_conditions}}" target="_blank"><img src="{{url('/themes/images/pdf.png')}}"> </a> </div>
                  <input type="hidden" value="{{ $fetchdata->terms_condition_name or ''}}" name="tmcvalue">
                  {{ $fetchdata->terms_condition_name or ''}}
                  @endif </div>
              </div>
				
				
				<div class="form_row arabic ar">
				<div class="form_row_right">
				 <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo9">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value8"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc_ar" id="company_logo9" class="info-file">
                  </div>
                   <div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
                  @if(isset($fetchdata->terms_conditions_ar)!='')
                 
                  <div class="pdf_icon"> <a href="{{$fetchdata->terms_conditions_ar}}" target="_blank"><img src="{{url('/themes/images/pdf.png')}}"> </a> </div>
                  <input type="hidden" value="{{ $fetchdata->terms_condition_name_ar or ''}}" name="tmcvalue_ar">
                  {{ $fetchdata->terms_condition_name_ar or ''}}
                  @endif 
				</div>
            </div>
				    
 <div class="form_row_left common_field">
                    <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>

                      <option value="1" <?php  if(isset($fetchdata->mc_status) && $fetchdata->mc_status==1){?> SELECTED <?php } ?>>@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif </option>
                     <option value="0" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==0){?> SELECTED <?php } ?>> @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif  </option>
                   </select>
                    
                    </div>
                  </div>
              <div class="form_row">
                <div class="form_row_left">
                  <div class="english">
                    <input type="hidden" name ="parent_id" value="{{ $id or '' }}">
                    <input type="hidden" name ="itemid" value="{{  $itemid or '' }}">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                  </div>
                </div>
              </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>
    
    $(document).ready(function(){
      
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
      if(checkedvalue==1) {  
      $('.homevisit').show();
    } else {
        $('.homevisit').hide();
    }

     $('#type').click(function(){
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
     if(checkedvalue==1) {
       $('.homevisit').show();
       } else {
           $('.homevisit').hide();

       }
     })

     $('#dish').click(function(){
        var checkedvalue =   $('input[name=service_available[]]:checked').val();
       if(checkedvalue==1 || $('input#type').is(':checked')) {
        $('.homevisit').show();
      } else {
          $('.homevisit').hide();
      }
    
     })

    })
  

   /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
  </script>
<script>
       

 $("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },

                      "service_available[]": {
                       required: true,
                      },

                       manager: {
                       required: true,
                      },
                  address_image: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       address: {
                       required: true,
                      },
                       address_ar: {
                       required: true,
                      },

                       about: {
                       required: true,
                      },
                       about_ar: {
                       required: true,
                      },
                       @if(isset($fetchdata->mc_img)!='')  
                        branchimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        branchimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif
                      
                       @if(isset($fetchdata->terms_conditions)!='') 
                        mc_tnc: {
                           required:false,
                           accept:"pdf",
                      },
                    @else
                         mc_tnc: {
                           required:true,
                           accept:"pdf",
                      },
                      @endif
                      @if(isset($fetchdata->terms_conditions_ar)!='') 
                        mc_tnc_ar: {
                           required:false,
                           accept:"pdf",
                      },
                    @else
                         mc_tnc_ar: {
                           required:true,
                           accept:"pdf",
                      },
                      @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             city_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",
                      },  

                 "service_available[]": {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SERVICE_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SERVICE_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR') }} @endif",
                      },  
                          mc_tnc: {
               accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_FILE') }} @endif",
        required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); @endphp",
                      },
                           mc_tnc_ar: {
               accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_FILE') }} @endif",
                  required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_VALID_FILE'); @endphp",
                                  
                      },

                         mc_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SHOPNAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SHOPNAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SHOPNAME') }} @endif",
                      },   

                          mc_name_ar: {
                  required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOPNAME_AR'); @endphp",
                      },   

             

                       address: {
               
                    required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },
  
                       address_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },
                        
                       about: {
              
                   required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); @endphp",
                      },
  
                       about_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); @endphp",
                      },
                  branchimage: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },  

                        address_image: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },  



                                                     
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                @if($mer_selected_lang_code !='en')
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }

                    
          @else
                 

                  if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       
                     }
                     if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       
                     }
 
                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
 
            if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                  

@endif

                    },

                submitHandler: function(form) {
                 var mapAdd = jQuery('input[name=google_map_address]').val();
                              if(mapAdd !='')
                              {
                              var long = jQuery('input[name=longitude]').val();
                              var lat  =  jQuery('input[name=latitude]').val();
                              if(long =='' && lat=='')
                              {
                              var allOk = 0;

                              }
                              else
                              {
                              var allOk = 1; 
                              }
                              }
                              else
                              {
                              var allOk = 1;
                              }

                              if(allOk == 1)
                              {
                              form.submit();
                              }
                              else
                              {
                              $('#maperror').html("<span class='error'>@if (Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= '') {{  trans(Session::get('mer_lang_file').'.get_Lat_Long_issue') }} @else  {{ trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue') }} @endif</span>");
                              }
                }
            });

 

</script>
@include('sitemerchant.includes.footer')
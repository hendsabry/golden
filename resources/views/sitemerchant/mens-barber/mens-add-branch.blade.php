 @inject('data','App\Help')
@include('sitemerchant.includes.header') 
@php $beauty_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_BRANCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_BRANCH') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_BRANCH') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <div class="one-call-form">


                <form name="form1" method="post" id="addbranch" action="" enctype="multipart/form-data">

                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <input type="text" id="hotal" value="taj" name="hotel1" readonly="" class="english">
                      <input type="text" id="hotal" value="taj" name="hotel1" readonly="" class="arabic ar">
                      <input type="hidden" id="hotal" value="52" name="hotel">
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); @endphp </span> </label>
                    <div class="info100">
                      <select id="city_id" name="city_id" class="small-sel">
                        <option value=""> Select City </option>
                        <option value="1">Coimbatore</option>
                        <option value="2">chennai</option>
                        <option value="3">Delhi</option>
                        <option value="4">Chennai</option>
                        <option value="5">Kolkata</option>
                        <option value="6">Bangalore</option>
                        <option value="7">Karachi </option>
                        <option value="8">Lahore </option>
                        <option value="9">Rawalpindi</option>
                        <option value="10">Islamabad</option>
                        <option value="11">fdgdfg</option>
                        <option value="12">Vatican City</option>
                        <option value="13">Lahore</option>
                        <option value="14">Lahore</option>
                        <option value="15">vadavalli</option>
                        <option value="16">peelamedu</option>
                        <option value="17">guindy</option>
                        <option value="18">Indira</option>
                        <option value="19">kochi</option>
                        <option value="20">hjhgkjhk</option>
                        <option value="21">jij</option>
                        <option value="22">California</option>
                        <option value="23">ravalpend</option>
                        <option value="24">los angeles</option>
                        <option value="25">Namakkal</option>
                        <option value="26">mumbai</option>
                        <option value="27">che</option>
                        <option value="28">Las Vegas</option>
                        <option value="29">Chennaix</option>
                        <option value="30">v</option>
                      </select>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); @endphp </span> </label>
                    <div class="info100">
                      <select id="manager" name="manager" class="small-sel">
                        <option value=""> Select  Branch Manager </option>
                        <option value="59">Ramu12</option>
                        <option value="60">Prabhakat</option>
                        <option value="61">Arun00</option>
                        <option value="62">sdfsdfsd</option>
                        <option value="65">jhgkjhk</option>
                        <option value="68">Banti</option>
                        <option value="69">dfgdf</option>
                        <option value="101">Himanshu</option>
                      </select>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" id="hotal_name" class="english" value="" name="mc_name" maxlength="40">
                      </div>
                      <div class="arabic ar">
                        <input type="text" onchange="check();" name="mc_name_ar" maxlength="40" class="arabic ar" id="ssb_name_ar">
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea class="english" cols="50" rows="4" id="description" name="description"></textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea cols="50" rows="4" id="description Arabic" name="description_ar" class="arabic ar"></textarea>
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value1"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input type="file" value="" required="" class="info-file" name="mc_img" id="company_logo">
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
				  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo1">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value2"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input type="file" value="" required="" class="info-file" name="mc_img" id="company_logo1">
                      </div>
                    </div>
                  </div>
				  
				  
                  <div class="form_row arabic ar">
                  <div class="form_row_left">
                    <input type="submit" name="submit" value="Submit">
                    </div>
                  </div>
                  <div class="form_row english">
                  <div class="form_row_left">
                    <input type="submit" name="submit" value="Submit">
                    </div>
                  </div>
                  <!-- form_row -->

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- <div merchant_vendor -->
<!-- action_popup -->
@include('sitemerchant.includes.footer')
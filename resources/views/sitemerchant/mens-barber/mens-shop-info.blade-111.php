@include('sitemerchant.includes.header') 
@php $mensbig_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english"> taj </div>
                  </div>
                </div>
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="hallname_ar"  maxlength="35" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp </span> 
                  <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a>
                  </label>
                  <div class="info100">
                    <input type="url" name="google_map_address"  data-validation="length required" 
		 data-validation-error-msg="Please enter google map address"  value="{{$getDb->google_map_address or ''}}" data-validation-length="max100">
                  </div>
                </div>
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value1"></div>
                    </div>
                    </label>
                    <input type="file" name="hallimage" id="company_logo" class="info-file">
                  </div>
                  <div class="form-upload-img">@if(isset($getDb->pro_Img) && $getDb->pro_Img !='') <img src="hallpics/thumb_{{$getDb->pro_Img}}" width="150" height="150"> @endif </div>
                  @if($errors->has('hallimage')) <span class="error"> {{ $errors->first('hallimage') }} </span> @endif </div>
                <div class="form_row_right">
                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="address_image_tooltip">
	<span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span>
</a>
</label>
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value2"></div>
                    </div>
                    </label>
                    <input type="file" name="hall_addressimg" id="company_logo1" class="info-file">
                  </div>
                  <div class="form-upload-img">@if(isset($getDb->hall_address_image) && $getDb->hall_address_image !='') <img src="hallpics/thumb_{{$getDb->hall_address_image}}" width="150" height="150"> @endif</div>
                  @if($errors->has('hall_addressimg')) <span class="error"> {{ $errors->first('hall_addressimg') }} </span> @endif </div>
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_ADDRESS'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="hallname_ar"  maxlength="35" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <select name="status" id="status" class="city_type">
                        <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif </option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> Riyadh</option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> Riyadh</option>
                      </select>
                    </div>
                    <div class="arabic ar">
                      <select name="status" id="status" class="city_type">
                        <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif </option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> Riyadh</option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> Riyadh</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_MANAGER'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_MANAGER'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <select name="status" id="status" class="city_type">
                        <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_MANAGER') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_MANAGER') }} @endif </option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> A</option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> B</option>
                      </select>
                    </div>
                    <div class="arabic ar">
                      <select name="status" id="status" class="city_type">
                        <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif </option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> Riyadh</option>
                        <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> Riyadh</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.OPENING'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.OPENING'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <div class="small_time ">
                        <input type="text" class="english " name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                      </div>
                      <div class="small_time">
                        <input type="text" class="english " name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                      </div>
                    </div>
                    <div class="arabic ar">
                      <div class="small_time ">
                        <input type="text" class="arabic ar " name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                      </div>
                      <div class="small_time">
                        <input type="text" class="arabic ar " name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form_row">
			  <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.SERVICEAVAIL'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SERVICEAVAIL'); @endphp </span> </label>
                  <div class="info100">
                    <div class="save-card-line">
                      <input type="radio" data-validation-qty="min1" data-validation="checkbox_group" name="hallfoodmenu[]" value="internal" id="type">
                      <label for="type"> <span class="english">@php echo lang::get('mer_en_lang.HOME'); @endphp</span> <span class="arabic ar">@php echo lang::get('mer_ar_lang.HOME'); @endphp </span> </label>
                    </div>
                    <div class="save-card-line">
                      <input type="radio" data-validation-qty="min1" data-validation="checkbox_group" name="hallfoodmenu[]" value="internal" id="dish">
                      <label for="dish"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP'); @endphp</span> <span class="arabic ar">@php echo lang::get('mer_ar_lang.MER_SHOP'); @endphp </span> </label>
                    </div>
                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.HOMEVISIT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.HOMEVISIT'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english small-sel" name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar small-sel" name="hallname_ar"  maxlength="35" >
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="form_row">
                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value1"></div>
                      <div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
                    </div>
                    </label>
                    <input type="file" name="hallimage" id="company_logo" class="info-file">
                  </div>
                </div>
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <textarea class="english" maxlength="5000" name="about" id="about" rows="4" cols="50"> </textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea class="arabic ar" name="about_ar" maxlength="5000" id="about_ar " rows="4" cols="50"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value1"></div>
                      <div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
                    </div>
                    </label>
                    <input type="file" name="hallimage" id="company_logo" class="info-file">
                  </div>
                </div>
                
              </div>
              
              <div class="form-btn-section english">
              <div class="form_row_left">
                <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                </div>
              </div>
              <div class="form-btn-section arabic ar">
              <div class="form_row_left">
                <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
@include('sitemerchant.includes.footer')
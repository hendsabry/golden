 @inject('data','App\Help')
@include('sitemerchant.includes.header')  
 <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
@php $mensbig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.mer_workerbooking')!= '') {{  trans(Session::get('mer_lang_file').'.mer_workerbooking') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.mer_workerbooking') }} @endif </h5>
      </header>
      
      
      <div class="worker_row">
      	<span class="worker_img"><img src="{{ $staffinformation->image }}" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></span>
        <span class="worker_name">{{ $staffinformation->staff_member_name }}</span>
      </div> <!-- worker_row -->
      
      <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition --> 
              

              <br>
@php
$id = request()->id;
$sid = request()->sid;
$workerid=request()->workerid;
@endphp
              
{!! Form::open(array('url'=>"mens-worker-booking/{$id}/{$sid}/{$workerid}",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}


 <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
     
        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
           
      

    <div class="order-filter-line order-line">
    <div class="of-date-box"><input type="text" class="cal-t" value="{{request()->date_to}}" autocomplete="off" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.Start_Date')!= '') {{  trans(Session::get('mer_lang_file').'.Start_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Start_Date') }} @endif" name="date_to" id="date_to" /><input type="text" autocomplete="off" value="{{request()->from_to}}" class="cal-t" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.End_Date')!= '') {{  trans(Session::get('mer_lang_file').'.End_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.End_Date') }} @endif" id="datepicker"   name="from_to"/></div><div class="of-orders">
     
      </div>
    <div class="search-box-field mems ">      
            <select name="serviceid" id="status" class="city_type">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICES') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICES') }} @endif </option>
              @foreach($staffservices as $staffservicelist)
              @php 

              if(Session::get('mer_lang_file')=='mer_en_lang'){ $ptitle=$staffservicelist->pro_title; }else{ $ptitle=$staffservicelist->pro_title_ar; } @endphp
              <option value="{{ $staffservicelist->product_id or 0}}" @php if($staffservicelist->product_id==request()->serviceid){ @endphp selected @php } @endphp > {{ $ptitle or ''}}</option>
              @endforeach
             
            </select>
            <input type="hidden" id="serachfirstfrm" name="serachfirstfrm" value="1">
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
       <span id="todata" class="error"></span>
      {!! Form::close() !!}

       
    </div> <!-- order-filter-line -->
    
      </div>
      <!-- filter_area -->






      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
          
            <div class="table_wrap"> 
              <!-- Display Message after submition --> 
              
              
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!-- Display Message after submition -->
              <div class="panel-body panel panel-default"> @if($reviewrating->count() <1)
                <div class="no-record-area">@if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
                @else
                <div class="table revie_table">
                  <div class="tr">
                    <div class="table_heading product_name">@if (Lang::has(Session::get('mer_lang_file').'.CUSTOMER')!= '') {{  trans(Session::get('mer_lang_file').'.CUSTOMER') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.CUSTOMER') }} @endif</div>
                    <div class="table_heading product_name">@if (Lang::has(Session::get('mer_lang_file').'.Service')!= '') {{  trans(Session::get('mer_lang_file').'.Service') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.Service') }} @endif</div>  
                    <div class="table_heading product_name">@if (Lang::has(Session::get('mer_lang_file').'.BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKINGDATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKINGDATE') }} @endif</div>
                    <div class="table_heading prod_comp">@if (Lang::has(Session::get('mer_lang_file').'.BOOKINGDURATION')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKINGDURATION') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKINGDURATION') }} @endif</div>
                  </div>
                  @foreach($reviewrating as $key=> $row)
                
                  <div class="tr">
                    <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.CUSTOMER')!= '') {{  trans(Session::get('mer_lang_file').'.CUSTOMER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CUSTOMER') }} @endif"> <span class="review_name">{{$data->CustName($row->cus_id)}}</span> </div>
                    <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Service')!= '') {{  trans(Session::get('mer_lang_file').'.Service') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Service') }} @endif">
                      @php
                      $serviceinfo = Helper::getorderedfromproducttbl($row->service_id); @endphp
                          {{ $serviceinfo }}
                    </div>
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKINGDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKINGDATE') }} @endif">
                      @php if(isset($row->booking_date) && $row->booking_date!='')
             { 
             $ordertime=strtotime($row->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             @endphp
                       

                    </div>
                    <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.BOOKINGDURATION')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKINGDURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKINGDURATION') }} @endif">
                       {{ $row->start_time }} - {{ $row->end_time }} 
                      
                     </div> 
                  </div>   
                  @endforeach </div>
              </div>
              @endif 
              
              </div>
                          {{ $reviewrating->links() }} 
  
          </div>
        </div>
        
      </div> <!-- global_area -->
      
    </div>
  </div>
</div>
</div>
@include('sitemerchant.includes.footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script>
 $(function() {
$( "#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
});

 $(function() {
 $("#datepicker").change(function () {
    var endDate  = document.getElementById("datepicker").value;
    var startDate = document.getElementById("date_to").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("datepicker").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");
        @else
        $('#todata').html("End date should be greater than Start date");
        @endif
        $('#to').show();
    }
    else
    {
         $('#todata').html("");

    }
});
 });

</script>
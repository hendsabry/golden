@include('sitemerchant.includes.header')    
@php $hall_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_HALL_PICTURE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALL_PICTURE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALL_PICTURE') }} @endif </h5>
      </header>
      <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->  
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box commonbox"> 
              
                           
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <div class="hall-top-form"> {!! Form::open(array('url' => '/add_hallpicture','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'add_hallpicture', 'name' =>'add_hallpicture' )) !!}
                <div class="hall-top-form-label">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD_HALLPICTURTE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD_HALLPICTURTE') }}  
                  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD_HALLPICTURTE') }} @endif </div>
                <div class="input-file-area">
                  <label for="company_logo">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
   <div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div>
                  </div>
                  </label>
                  <input id="company_logo" data-lbl="file_value1" name="hallpics[]" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
                </div>
                <!-- input-file-area -->
                 


                <div id="img_upload"></div>
                @if ($errors->has('hallpics'))
                <div class="error"> @if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_VALID_IMAGE') }} @endif </div>
                @endif
                <div class="img-upload-line">
                  <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
                  <span class="error pictureformat"></span>
                  @php $Count =  1;@endphp
                  <input type="hidden" name="pro_id" value="{{$_REQUEST['hid'] or ''}}">
                  <input type="hidden" id="count" name="count" value="{{$Count}}">
                 
                </div>
                <!-- img-upload-line --> 
                
                <div class="imgpics-area"> @if($getDbC >=1) 
                
                @foreach($getDb as $vals)
                <div class="imgpics-box">
                  <div class="imgpics"><img src="hallpics/small/thumb_@php echo $vals->image;  @endphp" ></div>
                  <div class="imgpics-delet">

 
                    <a data-status="Active" title="Remove Field" data-id="{{$vals->image}}" class="delet-icon status_active2 cstatus">&nbsp;</a>



                  </div>
                </div>
                @endforeach
                @endif </div>
              <div class="hall-top-form">
              <div class="hall-video-section">

                <div class="hall-video-label">@if (Lang::has(Session::get('mer_lang_file').'.MER_HALL_UPLOADVIDEO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALL_UPLOADVIDEO') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALL_UPLOADVIDEO') }} @endif </div>
                <div class="hall-video-form">
                  <input type="url" name="youtubevideo" id="youtubevideo" value="{{$getVideos->video_url}}">
                  @if ($errors->has('youtubevideo'))
                  <div class="error"> @if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_URL')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_URL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_URL') }} @endif </div>
                  @endif
                  <input type="hidden" name="pro_id" value="{{$_REQUEST['hid'] or ''}}">
                  
                <!-- hall-video-form -->
                <div class="clear"></div>
              
                {!! Form::close() !!}
                <div class="hall-video-area"> @if(isset($getVideos->video_url) && $getVideos->video_url!='')
                
                  <iframe class="hall-video-ifrem" src="{{$getVideos->video_url or ''}}"></iframe>
                  @endif </div>
                <!-- hall-video-area --> 
              </div>
              </div>
              <!-- hall-video-section --> 
            <div class="form_row">
                 <input class="pic-btn" type="submit" name="addhallpics" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBMIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SUBMIT') }} @endif">
               </div>  
            </div>
                
                </div>
              <!-- hall-top-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
              
            
          </div>
        </div>
      </div>
      <!--  global_area  --> 
    </div>
</div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script type="text/javascript">
		$(document).ready(function(){
		var maxField = 11;  
		var addButton = $('#add_button');  
		var wrapper = $('#img_upload');
		var x = @php echo $Count; @endphp;

		var y = x+1;  
		$(addButton).click(function(){  
		if(x < maxField){  
		x++; y++; 
		var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="hallpics[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div></div> ';
		$(wrapper).append(fieldHTML);  
		document.getElementById('count').value = parseInt(x);
		}
		});
		$(wrapper).on('click', '#remove_button', function(e){  
		e.preventDefault();
		$(this).parent('div').remove(); 
		x--;  
		document.getElementById('count').value = parseInt(x);
		});
		});
	</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div>
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  
 

     jQuery('.action_content').html('Do you want delete this record?')
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'deletehallpics'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});

jQuery('body').on('change','.info-file', function(){
            $('.pictureformat').html("");


  var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
var gid = $(this).data('lbl');
 $('#'+gid).html('');
             $('.pictureformat').html("Only formats are allowed : 'jpeg', 'jpg', 'png', 'gif', 'bmp'");
        }
});
 

</script>  

@include('sitemerchant.includes.footer')
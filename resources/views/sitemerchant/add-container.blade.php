@include('sitemerchant.includes.header') 
@php $hall_leftmenu =1; @endphp
<div class="merchant_vendor "> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_DISH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_DISH') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_DISH') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> 
            
            <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
            
            <form name="form1" id="add-container" method="post" action="{{ route('storehallmenu') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
             
               <div class="form_row common_field">
              <div class="form_row_left">
              <label class="form_label">
                <span class="english">@php echo lang::get('mer_en_lang.MER_Select_Menu'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Select_Menu'); @endphp </span>

          </label>
              <div class="info100">
              <select class="small-sel" name="menucategory" id="mencategory">
                <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_Select_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Select_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Select_Menu') }} @endif</option>
                 @php $menu_name='menu_name'@endphp
                 @if($mer_selected_lang_code !='en')
            @php $menu_name= 'menu_name_'.$mer_selected_lang_code; @endphp
            @endif
                        @foreach($menucat as $val)
                <option value="{{ $val->id }}">{{ $val->$menu_name }}</option>
                @endforeach  
              </select>
              </div>
              </div>
              </div>
              <div class="form_row">
              <div class="form_row_left">
              <label class="form_label">
                 <span class="english">@php echo lang::get('mer_en_lang.MER_Dish_Name'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Dish_Name'); @endphp </span> </label>
              <div class="info100">
              <div class="english">
              <input type="text" class="english" name="dish_name" maxlength="100" value="{!! Input::old('dish_name') !!}" id="dish_name" required="" >
                 </div>     
              <div class="arabic">
              <input class="arabic ar" id="dish_name_ar" maxlength="100"  name="dish_name_ar"   type="text" >
              </div>
              </div>
              </div>
              </div>

              
             
                
                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label disc_label">
                   <span class="english">@php echo lang::get('mer_en_lang.MER_CONTAINER'); @endphp</span>
                 <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CONTAINER'); @endphp </span>
                    </label>
					<label class="form_label disc_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                  <div class="info100">
                    @php $containername='title'@endphp
                   @if($mer_selected_lang_code !='en')
                    @php $containername= 'title_'.$mer_selected_lang_code; @endphp
                    @endif
                       @foreach($containerpackage as $key=> $val)
                      <div class="container_row">
                       
                        <div class="label_fl">{{ $val->$containername }}</div>
                        <div class="input_flr"><input type="text" onkeypress="return isNumber(event)" class="check notzero" name="containerprice[]"  maxlength="40"></div>
                         <input type="hidden" name="container_id[]" value="{{ $val->id }}">
                        <div class="discount_col_2">
            <div class="discount_col">
                    
                    <div class="info100">
            
            <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount[]" onkeypress="return isNumber(event)" maxlength="2" value="{{$data->discount or ''}}" id="discount" >
                      </div>
            
                    </div>
                    <!-- form_row -->
                  </div>
          </div>
                      </div>
                       @endforeach
                    </div>
                   </div>                    
                </div>
             
<span class="containersd"></span>

             
               <div class="form_row common_field">
              <div class="form_row_left">
              <label class="form_label">
                   <span class="english">@php echo lang::get('mer_en_lang.MER_Dish_Image'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Dish_Image'); @endphp </span>

          </label>
              <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn">
                         <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>
 </div>
                    </div>
                    </label>
                    <input id="company_logo" name="dish_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" type="file">
                  </div>
                  </div>
                 </div> 
               
               <div class="form_row">
               <div class="form_row_left english">   
<input type="hidden" name="hid" value="{{$_REQUEST['hid']}}">
<input type="hidden" name="bid" value="{{$_REQUEST['bid']}}">
         


              <input type="submit" name="submit" value="Submit">
              </div>

               <div class="form_row_right arabic ar arbic_right_btn">   
              <input type="submit" name="submit" value="خضع">
              </div>
              </div>
              
            </form>
          
          </div>
        </div>
      </div>
    </div>
    <!-- inner --> 
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  menucategory: {
                       required: true,
                      },

                       dish_name: {
                       required: true,
                      },
                       dish_name_ar: {
                       required: true,
                      },
                      
                  
                       
                       dish_image: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                            
           messages: {
             menucategory: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SELECT_CATEGORY') }} @endif",
                      },  
     
                dish_name: {
            required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_DISH_NAME'); @endphp",
                      },
                 dish_name_ar: {
               required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_DISH_NAME_AR'); @endphp",
                      }, 

             


                     dish_image: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid; 
            @if($mer_selected_lang_code !='en')
                    if (typeof valdata.menucategory != "undefined" || valdata.menucategory != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.dish_name != "undefined" || valdata.dish_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                   
                     
                     if (typeof valdata.containerprice != "undefined" || valdata.containerprice != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.dish_image != "undefined" || valdata.dish_image != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.dish_name_ar != "undefined" || valdata.dish_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
 
@else

                if (typeof valdata.dish_name_ar != "undefined" || valdata.dish_name_ar != null) 
                {

                $('.arabic_tab').trigger('click');     

                }

                
                if (typeof valdata.menucategory != "undefined" || valdata.menucategory != null) 
                {
                $('.english_tab').trigger('click'); 
                }

                if (typeof valdata.dish_name != "undefined" || valdata.dish_name != null) 
                {
                $('.english_tab').trigger('click'); 
                }

               
                if (typeof valdata.containerprice != "undefined" || valdata.containerprice != null) 
                {
                $('.english_tab').trigger('click'); 
                }
                if (typeof valdata.dish_image != "undefined" || valdata.dish_image != null) 
                {
                $('.english_tab').trigger('click'); 
                }


@endif

                    },

                submitHandler: function(form) {
 
              
                    $('.check').each(function() {    
                        if($(this).val() !='')
                        {
                       form.submit();
                        }
                        else
                        {
                         $('.containersd').html('<span class="error" style="text-align:left;">Please enter atleast one container price</span>'); 
                        }
                                  
                    });


                    //
                }
            });
 /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script> 
@include('sitemerchant.includes.footer')
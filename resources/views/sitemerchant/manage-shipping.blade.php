@include('sitemerchant.includes.header') 
@php $ship_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MANAGE_SHIPPING')!= '') {{  trans(Session::get('mer_lang_file').'.MANAGE_SHIPPING') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MANAGE_SHIPPING') }} @endif </h5>
         <span style="display: none;">@include('sitemerchant.includes.language') </span></header>
        <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif
              <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box box_shipping"> 
                @php  if($getShipping !=''){
                $buffet = explode(',',$getShipping->Buffet);
                $Desert = explode(',',$getShipping->Desert);
                $Dates = explode(',',$getShipping->Dates);
                $Cosha =  explode(',',$getShipping->Cosha);
                $Roses = explode(',',$getShipping->Roses);
                $Special_Events = explode(',',$getShipping->Special_Events);
                $Makeup_Product =  explode(',',$getShipping->Makeup_Product);
                $Acoustics = explode(',',$getShipping->Acoustics);
                $Tailors =  explode(',',$getShipping->Tailors);
                $Dresses =  explode(',',$getShipping->Dresses);
                $Abaya = explode(',',$getShipping->Abaya);
                $Oud_and_Perfumes =  explode(',',$getShipping->Oud_and_Perfumes);
                $Gold_and_Jewellery = explode(',',$getShipping->Gold_and_Jewellery);

              }
              else
              {

                 $buffet = array(9,10);
                $Desert = array(9,10);
                $Dates = array(9,10);
                $Cosha = array(9,10);
                $Roses = array(9,10);
                $Special_Events = array(9,10);
                $Makeup_Product =  array(9,10);
                $Acoustics =array(9,10);
                $Tailors = array(9,10);
                $Dresses =  array(9,10);
                $Abaya = array(9,10);
                $Oud_and_Perfumes =   array(9,10);
                $Gold_and_Jewellery = array(9,10);


              }
                
 

                @endphp
                <form name="manageshipping" method="post" id="manageshipping" action="{{ route('manageshipping') }}">
                  {{ csrf_field() }}
               
                    <!--div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.buffet'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.buffet'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_buffet[]" @if(isset($buffet[0]) && $buffet[0]=='1') checked  @endif value="1"  id="Aramex1" > <label for="Aramex1">Aramex</label>
                    <input  type="checkbox" name="needshipping_buffet[]" value="2" @if(isset($buffet[1]) && $buffet[1]=='2') checked  @endif   @if($buffet[0]== '2') checked @endif  id="pick1" ><label for="pick1"> Pick Logistics</label>
                     </div>
                    </div-->
                    
                    

                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Dessert'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Dessert'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_dessert[]" @if(isset($Desert[0]) && $Desert[0]=='1') checked  @endif  value="1" id="Aramex2" > <label for="Aramex2">Aramex</label>
                    <input  type="checkbox" name="needshipping_dessert[]" @if(isset($Desert[1]) && $Desert[1]=='2') checked  @endif   @if($Desert[0]== '2') checked @endif value="2" id="pick2" ><label for="pick2"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Dates'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Dates'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_dates[]" @if(isset($Dates[0]) && $Dates[0]=='1') checked  @endif  value="1" id="Aramex3" > <label for="Aramex3">Aramex</label>
                    <input  type="checkbox" name="needshipping_dates[]" @if(isset($Dates[1]) && $Dates[1]=='2') checked  @endif  @if($Dates[0]== '2') checked @endif  value="2" id="pick3" ><label for="pick3"> Pick Logistics</label>
                     </div>
                    </div>


                    <!--div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.cosha'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.cosha'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_cosha[]" @if(isset($Cosha[0]) && $Cosha[0]=='1') checked  @endif  value="1" id="Aramex4" > <label for="Aramex4">Aramex</label>
                    <input  type="checkbox" name="needshipping_cosha[]" @if(isset($Cosha[1]) && $Cosha[1]=='2') checked  @endif  @if($Cosha[0]== '2') checked @endif  value="2" id="pick4" ><label for="pick4"> Pick Logistics</label>
                     </div>
                    </div-->



                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Roses'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Roses'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_roses[]" @if(isset($Roses[0]) && $Roses[0]=='1') checked  @endif  value="1" id="Aramex5" > <label for="Aramex5">Aramex</label>
                    <input  type="checkbox" name="needshipping_roses[]" @if(isset($Roses[1]) && $Roses[1]=='2') checked  @endif  @if($Roses[0]== '2') checked @endif  value="2" id="pick5" ><label for="pick5"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.special_event'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.special_event'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_special_event[]"  @if(isset($Special_Events[0]) && $Special_Events[0]=='1') checked  @endif  value="1" id="Aramex6" > <label for="Aramex6">Aramex</label>
                    <input  type="checkbox" name="needshipping_special_event[]" @if(isset($Special_Events[1]) && $Special_Events[1]=='2') checked  @endif @if($Special_Events[0]== '2') checked @endif value="2" id="pick6" ><label for="pick6"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MAKEUP'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MAKEUP'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_makeup[]"  @if(isset($Makeup_Product[0]) && $Makeup_Product[0]=='1') checked  @endif  value="1" id="Aramex7" > <label for="Aramex7">Aramex</label>
                    <input  type="checkbox" name="needshipping_makeup[]"  @if(isset($Makeup_Product[1]) && $Makeup_Product[1]=='2') checked  @endif @if($Makeup_Product[0]== '2') checked @endif value="2"  id="pick7" ><label for="pick7"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.acoustics'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.acoustics'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_acoustics[]" @if(isset($Acoustics[0]) && $Acoustics[0]=='1') checked  @endif   value="1" id="Aramex8" > <label for="Aramex8">Aramex</label>
                    <input  type="checkbox" name="needshipping_acoustics[]" @if(isset($Acoustics[1]) && $Acoustics[1]=='2') checked  @endif   @if($Acoustics[0]== '2') checked @endif  value="2"  id="pick8" ><label for="pick8"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.tailor'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.tailor'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_tailor[]" @if(isset($Tailors[0]) && $Tailors[0]=='1') checked  @endif  value="1" id="Aramex9" > <label for="Aramex9">Aramex</label>
                    <input  type="checkbox" name="needshipping_tailor[]" @if(isset($Tailors[1]) && $Tailors[1]=='2') checked  @endif  @if($Tailors[0]== '2') checked @endif value="2" id="pick9" ><label for="pick9"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.dresses'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.dresses'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_dresses[]" @if(isset($Dresses[0]) && $Dresses[0]=='1') checked  @endif  value="1" id="Aramex11" > <label for="Aramex11">Aramex</label>
                    <input  type="checkbox" name="needshipping_dresses[]" @if(isset($Dresses[1]) && $Dresses[1]=='2') checked  @endif  @if($Dresses[0]== '2') checked @endif value="2"  id="pick11" ><label for="pick11"> Pick Logistics</label>
                     </div>
                    </div>



                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.abaya'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.abaya'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_abaya[]" @if(isset($Abaya[0]) && $Abaya[0]=='1') checked  @endif  value="1" id="Aramex12" > <label for="Aramex12">Aramex</label>
                    <input  type="checkbox" name="needshipping_abaya[]" @if(isset($Abaya[1]) && $Abaya[1]=='2') checked  @endif @if($Abaya[0]== '2') checked @endif  value="2" id="pick12" ><label for="pick12"> Pick Logistics</label>
                     </div>
                    </div>



                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.oudandperfumes'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.oudandperfumes'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_oudandperfumes[]" @if(isset($Oud_and_Perfumes[0]) && $Oud_and_Perfumes[0]=='1') checked  @endif   value="1" id="Aramex13" > <label for="Aramex13">Aramex</label>
                    <input  type="checkbox" name="needshipping_oudandperfumes[]" @if(isset($Oud_and_Perfumes[1]) && $Oud_and_Perfumes[1]=='2') checked  @endif @if($Oud_and_Perfumes[0]== '2') checked @endif   value="2" id="pick13" ><label for="pick13"> Pick Logistics</label>
                     </div>
                    </div>


                    <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.goldandjewelry'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.goldandjewelry'); @endphp </span> </label>
                    <div class="info100"> 
                    <input  type="checkbox" name="needshipping_goldandjewelry[]" @if(isset($Gold_and_Jewellery[0]) && $Gold_and_Jewellery[0]=='1') checked  @endif  value="1" id="Aramex14" > <label for="Aramex14">Aramex</label>
                    <input  type="checkbox" name="needshipping_goldandjewelry[]" @if(isset($Gold_and_Jewellery[1]) &&  trim($Gold_and_Jewellery[1])== '2') checked  @endif @if($Gold_and_Jewellery[0]== '2') checked @endif  value="2"  id="pick14" ><label for="pick14"> Pick Logistics</label>
                     </div>
                    </div>




                                    
                  <div class="form_row">
                  <div class="arabic ar form_row_left arbic_right_btn">
                    <input type="submit" name="submit" value="تحديث">
                    </div>
                    <div class="form_row_left english">
                    <input type="submit" name="submit" value="Update">
                  </div>
                  </div>
                  
                  
                  
                </form>
                
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
 
@include('sitemerchant.includes.footer')
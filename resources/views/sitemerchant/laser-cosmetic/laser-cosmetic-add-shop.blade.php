@include('sitemerchant.includes.header') 
@php $laser_cosmetic_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->sid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.mer_add_clinic')!= '') {{  trans(Session::get('mer_lang_file').'.mer_add_clinic') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_add_clinic') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.mer_update_clinic')!= '') {{  trans(Session::get('mer_lang_file').'.mer_update_clinic') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_update_clinic') }} @endif </h5>
        @endif
        
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <form name="form1" id="form1" method="post" action="{{ route('store-lasershop') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CLINIC_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CLINIC_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="{{ $fetchfirstdata->mc_name or '' }}"   class="english" maxlength="140">
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" class="arabic ar" value="{{ $fetchfirstdata->mc_name_ar or '' }} " name="mc_name_ar" type="text" data-validation-length="max80"  maxlength="40" >
                        <input type="hidden" name="parentid" value="{{ request()->id }}">
                        <input type="hidden" name="sid" value="{{ request()->sid }}">
                        <input type="hidden" name="autoid" value="{{ request()->autoid }}">
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_IMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          @if(!isset($getDb)) <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span> @endif
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file updateimage" type="file">
                        @if(isset($fetchfirstdata->mc_img) && $fetchfirstdata->mc_img!='')
                        <div class="form-upload-img"> <img src="{{ $fetchfirstdata->mc_img or '' }}" width="150" height="150"> </div>
                        @endif </div>
                    </div>
                  </div>
                  <div class="form_row_left"> @if($sid=='')
                    <div class="english">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                    @else
                    <div class="english">
                      <input type="submit" name="submit" value="Update">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="تحديث">
                    </div>
                    @endif </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>       
$("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
$("#form1").validate({
                  ignore: [],
                  rules: {
                  mc_name: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mc_name: {
              required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_CLINIC_NAME'); @endphp",
               
                      },  

                 mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_RESTURNANT_NAME_AR'); @endphp",
                      },    

                   
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
@if($mer_selected_lang_code !='en')
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.arabic_tab').trigger('click');     

                    }
                      if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }
            @else
 
              if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }

                      if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
 
            @endif


                    
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
  var createValidation = function() {
       
@if(isset($fetchfirstdata->mc_img) && $fetchfirstdata->mc_img  !='') 

        $(".updateimage").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
              required: false,
               accept:"png|jpe?g|gif",

              messages: {
                accept: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')}} @endif",
              }
            });
          });

 @else
 

 $(".updateimage").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
              required: true,
              accept:"png|jpe?g|gif",

              messages: {
                accept: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')}} @endif",
                required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')}} @endif"
              }
            });
          });


        @endif


};

$(function() {
   createValidation();
});
</script>
@include('sitemerchant.includes.footer')
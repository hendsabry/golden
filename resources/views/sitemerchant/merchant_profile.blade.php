 @include('sitemerchant.includes.header')  
@php $hall_leftmenu =1; @endphp 
<!--start merachant-->
<div class="merchant_vendor"> 
  
  <!--left panel--> 
   @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left') 
  <!--left panel end--> 
  <!--right panel-->
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_HALL_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALL_INFO') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALL_INFO') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition --> 
      <div class="global_area">
        <div class="row">
          
          <div class="col-lg-12">
            <div class="box"> 
              
              
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
        
        
        <div class="row">
        	<div class="col-lg-11 panel_marg">
                    
					{{ Form::open() }}
                    @foreach($merchant_setting_details as $merchant)  @endforeach
                    <div class="panel panel-default">
                        <div class="panel-heading">
                    {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')!= '') ? trans(Session::get('mer_lang_file').'.MER_MERCHANT_PROFILE')  : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_PROFILE')}}
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_FIRST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FIRST_NAME'): trans($MER_OUR_LANGUAGE.'.MER_FIRST_NAME') }}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->mer_fname }}
                    </div>
                </div>
                        </div>
						  <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_LAST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LAST_NAME'): trans($MER_OUR_LANGUAGE.'.MER_LAST_NAME')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->mer_lname}}
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PASSWORD'): trans($MER_OUR_LANGUAGE.'.MER_PASSWORD')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->mer_password }}
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL-ID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL-ID'): trans($MER_OUR_LANGUAGE.'.MER_EMAIL-ID')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->mer_email }}
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PHONE_NUMBER'): trans($MER_OUR_LANGUAGE.'.MER_PHONE_NUMBER')}} <span class="text-sub">*</span></label>
                    <div class="col-lg-4"> 
					{{ $merchant->mer_phone}}
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS_ONE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS_ONE'):trans($MER_OUR_LANGUAGE.'.MER_ADDRESS_ONE')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->mer_address1 }}
                    </div>
                </div>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS_TWO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS_TWO'): trans($MER_OUR_LANGUAGE.'.MER_ADDRESS_TWO')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
          			{{ $merchant->mer_address2 }}
                    </div>
                </div>
                        </div>
                         <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY'): trans($MER_OUR_LANGUAGE.'.MER_CITY')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->ci_name }}
                    </div>
                </div>
                        </div>
						
                        <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_COUNTRY')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->co_name }}
                    </div>
                </div>
                        </div>
                       
                    
                    <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.PAYFORT_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.PAYFORT_EMAIL') : trans($MER_OUR_LANGUAGE.'.PAYFORT_EMAIL')}}<span class="text-sub">*</span></label>
                    <div class="col-lg-4">
					{{ $merchant->mer_payment }}
                    </div>
                </div>
                        </div>
                      </div> 
                    </div>
					
					<div class="form-group" style="padding-bottom:10px;">
					<?php /*{!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-10', 'for' => 'pass1'])) !!}*/?>
                    

                    <div class="col-lg-2">
                     <a style="color:#fff; margin-left: 20px;" class="btn btn-warning btn-sm btn-grad" href="{{ url('sitemerchant_dashboard')}}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') ?  trans(Session::get('mer_lang_file').'.MER_BACK') : trans($MER_OUR_LANGUAGE.'.MER_BACK') }}</a>
                    </div>
					  
                </div>
                
                {{ Form::close() }}
                </div>
        <br>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $Rosestbig_leftmenu =1; @endphp

<div class="merchant_vendor">   @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
    
      <header>
        @if(request()->autoid=='')
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.ADDWRAPPINGTYPE')!= '') {{  trans(Session::get('mer_lang_file').'.ADDWRAPPINGTYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADDWRAPPINGTYPE') }} @endif </h5>
          @else
 <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.UPDATE_WRAPPING_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_WRAPPING_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_WRAPPING_TYPE') }} @endif </h5>
          @endif
          @include('sitemerchant.includes.language')
      </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
           @php  $id = request()->id; $itemid = request()->itemid; $autoid = request()->autoid; @endphp

              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->  

                <form name="form1"  id ="menucategory" method="post" action="{{ route('roses-updatewrapping') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form_row">
                    <div class="form_row_left">
                     <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.NAME'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.NAME'); @endphp </span>

              </label>  
                    <div class="info100">
                      <div class="english">
                      <input type="text" value="{{$getAttr->option_title or ''}}" class="english" name="category_name" maxlength="80" id="category_name" required="" >
                      </div>
                      <div class="arabic ar">
                      <input class="arabic ar" id="category_ar" maxlength="80"  name="category_name_ar"  value="{{$getAttr->option_title_ar or ''}}"  required=""  type="text" >
                      </div>
                       </div>
                       </div>
                  </div>



      <div class="form_row common_field">
                    <div class="form_row_left">
                      <label class="form_label">  
 @if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif
 
                       </label>
                      <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" required="" value="">
                      </div>
<span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                  @if(isset($getAttr->image) && $getAttr->image!='')

<div class="form-upload-img"><img src="{{$getAttr->image}}"></div>

                  @endif
                    </div>
                        
                        
                    </div>
                  </div>



                        <div class="form_row common_field">
                    <div class="form_row_left">
                     <label class="form_label">                     
                   @if (Lang::has(Session::get('mer_lang_file').'.PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PRICE') }} @endif 
                      </label>  
                    <div class="info100">
                      <div class="englishs">
                      <input type="text" onkeypress="return isNumber(event)" value="{{$getAttr->price or ''}}" class="notzero xs_small" name="price" maxlength="9" id="price" required="" >
                      </div>
                      
                       </div>
                       </div>
                  </div>

                  
                  <div class="form_row">
      
                  <input type="hidden" name="id" value="{{ $id or ''}}">    
                   <input type="hidden" name="itemid" value="{{ $itemid or ''}}">
                   <input type="hidden" name="autoid" value="{{ $autoid}}">
                      <input type="hidden" name="type" value="1">
                   
                   <div class="form_row">
                   <div class="form_row_left">
                   @if($autoid=='')
                   <div class="arabic ar">
                <input type="submit" name="submit" value="خضع">
         </div>
          <div class="english">
                <input type="submit" name="submit" value="Submit">
         </div> <!-- form_row -->
                  @else 
                   <div class="arabic ar">
        
                <input type="submit" name="submit" value="تحديث">
         </div>
          <div class="english">
                <input type="submit" name="submit" value="Update">
         </div> <!-- form_row -->
                  @endif
                  </div>
                  </div>
                  </div>
                  
                </form>


              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },
                        price: {
                        required: true,
                        },
               @if(isset($getAttr->image) && $getAttr->image!='')         
                     mc_img: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },
@else
 mc_img: {
                           required:true,
                           accept:"png|jpe?g|gif",
                      },

@endif
                       category_name_ar: {
                       required: true,
                      },

                       
                     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
                  category_name: {
                  required:  "@php echo lang::get('mer_en_lang.MER_NAME_VALIDATION'); @endphp",
                  }, 
                  category_name_ar: {
                  required:  "@php echo lang::get('mer_ar_lang.MER_NAME_VALIDATION'); @endphp",
                  },
                  mc_img: {
                  accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                  required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                  }, 
                  price: {
                  required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE') }} @endif",
                  }, 
                },
                invalidHandler: function(e, validation){
 
                    var valdata=validation.errorList;
                     console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


                    @if($mer_selected_lang_code !='en')
                      if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                       if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.arabic_tab').trigger('click'); 
                      }
                       if (typeof valdata.price != "undefined" || valdata.price != null) 
                      {
                      $('.arabic_tab').trigger('click'); 
                      }
                      if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                    @else  
                      if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                        if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                       if (typeof valdata.price != "undefined" || valdata.price != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      } 
                    @endif


                    },
 

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $Rosestbig_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              
             <form name="form1" method="post" id="addbranch" action="{{ route('store_roses-info') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="mc_name" value="{{ $fetchdata->mc_name or '' }}" maxlength="255"  
                       value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                    
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="mc_name_ar" value=" {{ $fetchdata->mc_name_ar or '' }} "  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">

                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp </span> 
                  <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a>
                  </label>
                  <div class="info100">
                    <input type="url" name="google_map_address"  data-validation="length required"                     
                     value=" {{ $fetchdata->google_map_address or '' }}" data-validation-length="max100">
                     </div>
                </div>

              </div>

              <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LONG')!= '') {{  trans(Session::get('mer_lang_file').'.LONG') }}  @else  {{ trans($OUR_LANGUAGE.'.LONG') }} @endif </label>
                  <input type="text" class="form-control" value="{{$fetchdata->longitude or ''}}" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LAT')!= '') {{  trans(Session::get('mer_lang_file').'.LAT') }}  @else  {{ trans($OUR_LANGUAGE.'.LAT') }} @endif </label>
                  <input type="text" class="form-control" value="{{$fetchdata->latitude or ''}}"  readonly="" name="latitude" />
          </div>
                  </div>
              <div class="form_row">
                <div class="form_row_left common_field">

                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_IMAGE'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value1"></div>
                    </div>
                    </label>
                    <input type="file" name="branchimage" id="company_logo" class="info-file">
                  </div>
                   <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                  @if(isset($fetchdata->mc_img) && $fetchdata->mc_img !='')
                  <div class="form-upload-img"> <img src="{{ $fetchdata->mc_img }}" width="150" height="150">  </div>@endif
                  @if($errors->has('branchimage')) <span class="error"> {{ $errors->first('branchimage') }} </span> @endif
                    </div> 

                
                <!--div class="form_row_right common_field">

                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="address_image_tooltip">
               <span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span>
              </a>
              </label>
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value2"></div>
                    </div>
                    </label>
                    <input type="file" name="address_image" id="company_logo1" class="info-file">
                  </div>
                   <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                  <div class="form-upload-img">@if(isset($fetchdata->address_image) && $fetchdata->address_image !='')
                   <img src="{{ $fetchdata->address_image }}" > @endif</div>
                  @if($errors->has('address_image')) <span class="error"> {{ $errors->first('address_image') }} </span> @endif

                   </div-->
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_ADDRESS'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="address" maxlength="235"  data-validation="length required" 
     data-validation-error-msg="Please enter hall name" value="{{$fetchdata->address or ''}}" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="address_ar" value="{{$fetchdata->address_ar or ''}}"  maxlength="255" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                 
                  <label class="form_label" > <span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span> <span class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp  </label>
                  <div class="info100" >
                    
                     <select name="city_id" id="status" class="city_type">
             <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif</option>
               @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> @if($mer_selected_lang_code !='en') {{$cbval->co_name_ar}} @else {{$cbval->co_name}} @endif</option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                         @php $ci_id = $val->ci_id; @endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" {{ isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''}} >{{ $val->$ci_name }}</option>
                        @endforeach
                        @endforeach
            </select>
                   
                  </div>

                </div>
              </div>
             
              <div class="form_row">
                 
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50">{{ $fetchdata->mc_discription or ''}} </textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50">{{ $fetchdata->mc_discription_ar or ''}}</textarea>
                    </div>
                  </div>
                </div>
                
                <div class="form_row_right english">

                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo7">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value6"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc" id="company_logo7" class="info-file">
                  </div>
                  
                    @if(isset($fetchdata->terms_conditions)!='') 
                    <div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
                   <div class="pdf_icon">
                   <a href="{{$fetchdata->terms_conditions}}" target="_blank"><img src="{{url('/themes/images/pdf.png')}}"> <span>{{ $fetchdata->terms_condition_name or ''}}</span></a>
                   
                   </div>
                   <input type="hidden" value="{{ $fetchdata->terms_condition_name or ''}}" name="tmcvalue">
                   
                       @endif 

                </div>
              </div>
              <div class="arabic ar">
                <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo11">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value10"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc_ar" id="company_logo11" class="info-file">
                  </div>
                  
                    @if(isset($fetchdata->terms_conditions_ar)!='') 
                    <div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
                   <div class="pdf_icon">
                   <a href="{{$fetchdata->terms_conditions_ar}}" target="_blank"><img src="{{url('/themes/images/pdf.png')}}"><span>{{ $fetchdata->terms_condition_name_ar or ''}}</span> </a>
                   
                   </div>
                   <input type="hidden" value="{{ $fetchdata->terms_condition_name_ar or ''}}" name="tmcvalue_ar">
                   
                       @endif 
                </div>
              </div>
              </div>
               <div class="form_row_left common_field">
                    <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>

                      <option value="1" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==1){?> SELECTED <?php } ?>>@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif </option>
                     <option value="0" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==0){?> SELECTED <?php } ?>> @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif  </option>
                   </select>
                    
                    </div>
                  </div>
              <div class="form_row">
              <div class="form_row_left">
              <div class="form-btn-section english">
                   <input type="hidden" name ="parent_id" value="{{ $id or '' }}">
                       <input type="hidden" name ="itemid" value="{{  $itemid or '' }}">
                <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
              </div>
              <div class="form-btn-section arabic ar">
                <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
              </div>
              </div>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor  --> 
   

<script>
       

 $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },
 
                     
                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       address: {
                       required: true,
                      },
                       address_ar: {
                       required: true,
                      },

                       about: {
                       required: true,
                      },
                       about_ar: {
                       required: true,
                      },
                    address_image: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },

                       @if(isset($fetchdata->mc_img) && $fetchdata->mc_img !='') 
                        branchimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        branchimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif
                      
                       @if(isset($fetchdata->terms_conditions)!='') 
                        mc_tnc: {
                           required:false,
                           accept:"pdf",
                      },
                    @else
                         mc_tnc: {
                           required:true,
                           accept:"pdf",
                      },
                      @endif


                       @if(isset($fetchdata->terms_conditions_ar)!='') 
                        mc_tnc_ar: {
                           required:false,
                           accept:"pdf",
                      },
                    @else
                         mc_tnc_ar: {
                           required:true,
                           accept:"pdf",
                      },
                      @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             city_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",
                      },  

             
                          mc_tnc: {
          
                  accept: "@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp",
                 required: "@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp",

                      },
           mc_tnc_ar: {
                       required: "@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file_ar'); @endphp",
                         accept: "@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file_ar'); @endphp",
                                 
                      },
                         mc_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SHOPNAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SHOPNAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SHOPNAME') }} @endif",
                      },   

                          mc_name_ar: {
                  required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOPNAME_AR'); @endphp",
                      },   

             

                      address: {
                         required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); @endphp",

                       },
                       address_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },
                        
                        about: {
                       required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); @endphp",  
               
                      },
  
                       about_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); @endphp",
                      },
                  branchimage: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },    

                      address_image: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                               


                                    

                      },                               
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                @if($mer_selected_lang_code !='en')
                      if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }
                      if (typeof valdata.address != "undefined" || valdata.address != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.about != "undefined" || valdata.about != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }

                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }
                      if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }
                      

                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                      {

                      $('.arabic_tab').trigger('click');     

                      }
                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');    
                      }
          @else


                      if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');   
                      }
                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                      if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     
                      }
                      if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.description != "undefined" || valdata.description != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                      if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                      {
                      $('.english_tab').trigger('click');
                      }

                      if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                      if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
                  

@endif

                    },

                submitHandler: function(form) {
                      var mapAdd = jQuery('input[name=google_map_address]').val();
                  if(mapAdd !='')
                  {
                  var long = jQuery('input[name=longitude]').val();
                  var lat  =  jQuery('input[name=latitude]').val();
                  if(long =='' && lat=='')
                  {
                  var allOk = 0;

                  }
                  else
                  {
                  var allOk = 1; 
                  }
                  }
                  else
                  {
                  var allOk = 1;
                  }

                  if(allOk == 1)
                  {
                  form.submit();
                  }
                  else
                  {
                  $('#maperror').html("<span class='error'>@if (Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= '') {{  trans(Session::get('mer_lang_file').'.get_Lat_Long_issue') }} @else  {{ trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue') }} @endif</span>");
                  }
                }
            });

 

</script> 
@include('sitemerchant.includes.footer')
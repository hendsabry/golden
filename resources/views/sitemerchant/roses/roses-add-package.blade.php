@include('sitemerchant.includes.header') 
@php $Rosestbig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>

      @if(request()->autoid =='')
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.ADDPACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADDPACKAGE') }} @endif </h5>
      @else
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATEPACKAGE') }}  
      @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE') }} @endif </h5>
      @endif

        @include('sitemerchant.includes.language') </header>
      <style type="text/css">
      .ms-drop ul li label input[type="checkbox"] {
      height: 20px;
      display: inline-block;
      width: 20px;
      position: relative;
      border: 1px solid red;
      opacity: 1;
      }
      </style>
      <link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }} </div>
              @endif
              <!-- Display Message after submition -->
                <form name="form1" method="post" id="add-container" action="{{ route('store-rosespackages') }}"" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PACKEGENAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKEGENAME'); @endphp </span></label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" maxlength="80" type="text" name="title" value="{{$getinfos->pro_title or ''}}" id="title" required="">
                      </div>
                      <div class="arabic">
                        <input class="arabic ar" maxlength="80" id="title_ar"  name="title_ar" value="{{$getinfos->pro_title_ar or ''}}"  type="text" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PACKAGEIMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKAGEIMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="img" class="info-file" type="file" >
                        <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                        @if(isset($getinfos->pro_Img) && $getinfos->pro_Img!='')
                        <div class="form-upload-img"><img src="{{$getinfos->pro_Img or ''}}"></div>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> </label>
                    <div class="info100">
                      <div>
                        <input class="xs_small notzero" maxlength="9" onkeypress="return isNumber(event)"  type="text" name="price" value="{{$getinfos->pro_price or ''}}" id="price" required="">
                      </div>
                    </div>
                  </div>   
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span> </label>
                    <div class="info100">
              <input type="text" class="xs_small notzero" maxlength="2"  onkeypress="return isNumber(event)"  name="discount" value="{{$getinfos->pro_discount_percentage or ''}}" id="discount" >
                    </div>
                  </div>
                </div>


                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.FlOWERTYPE'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.FlOWERTYPE'); @endphp </span> </label>
                    <div class="info100">                  
                        <select name="flowertype[]" id="flowertype" class="small-sel" multiple=""> 
                        @foreach( $simpleProductsMerchant as $values) 
                        @php $ProID = $values->pro_id; @endphp  
                        <option value="{{$values->pro_id}}" @if (in_array($ProID, $ChkrelArray)) {{"SELECTED"}}  @endif>  @if($mer_selected_lang_code !='en') {{$values->pro_title_ar}} @else  {{$values->pro_title}}  @endif</option>
                        @endforeach
  
                        </select>
                   <span for="flowertype" generated="true" class="error">  </span>
                    </div>
                  </div>

                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.WRAPTYPE'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.WRAPTYPE'); @endphp </span> </label>
                    <div class="info100">
                      <div>
                        <select name="wraptype" id="wraptype" class="small-sel">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>
                        @php $getWrapingtype = Helper::getWrappingType('25'); @endphp
                        @foreach( $getWrapingtype as $valus)  
                        @php $ids = $valus->id; @endphp   
                        <option value="{{$valus->id}}" @if(isset($GetrelatedWType) && $GetrelatedWType== $ids) {{"SELECTED"}}  @endif> @if($mer_selected_lang_code !='en') {{$valus->option_title_ar}} @else {{$valus->option_title}}  @endif</option>
                        @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              
                <div class="form_row common_field">
				          <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.WRAPDESIGN'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.WRAPDESIGN'); @endphp </span> </label>
                  <div class="info100">
                    <div>
                      <select name="wrapdesign" id="wrapdesign" class="small-sel">
                  <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>
                  @php $getWrapingtype = Helper::getWrappingType('26'); @endphp
                  @foreach( $getWrapingtype as $valus)   
                     @php $idss = $valus->id; @endphp   
                  <option value="{{$valus->id}}" @if(isset($idss) && $idss== $GetrelatedWDesign) {{"SELECTED"}}  @endif>
                    @if($mer_selected_lang_code !='en') {{$valus->option_title_ar}} @else {{$valus->option_title}}  @endif</option>
                  @endforeach
                      </select>
                    </div>
                  </div>
				  </div>
				  <div class="form_row_right">
				    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> </label>
                  <div class="info100">
                    <div>
                    <input class="xs_small notzero" onkeypress="return isNumber(event)"  maxlength="9" type="text" name="qty" value="{{$getinfos->pro_qty or ''}}" id="qty" required="">
                    </div>
                  </div>
				  </div>
          
                </div>
                <div class="form_row ">
				          <div class="form_row_left">
                   <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea class="english" cols="50" rows="4" maxlength="500" id="description" name="description">{{$getinfos->pro_desc or ''}}</textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea cols="50" rows="4" id="description_ar"  maxlength="500" name="description_ar">{{$getinfos->pro_desc_ar or ''}}</textarea>
                      </div>
                    </div>
				  </div>
				  
                </div>
                <!-- form_row -->
                <!-- form_row -->
                <div class="form_row">
                <div class="form_row_left">
                <div class="english">
                  
                <input type="hidden" name="id" value="{{request()->id}}">
                <input type="hidden" name="itemid"  value="{{request()->itemid}}">
                <input type="hidden" name="autoid"  value="{{request()->autoid}}">
                  <input type="submit" name="submit" value="Submit">
                </div>
                <div class="arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                </div></div>
                <!-- form_row -->
              </form>
              <!-- one-call-form -->
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
        <!-- global_area -->
      </div>
    </div>
    <!-- right_panel -->
  </div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                    title: {
                    required: true,
                    },
                    title_ar: {
                    required: true,
                    },
                    price: {
                    required: true,
                    },
                     "flowertype[]": {
                    required: true,
                    },
                    wraptype: {
                    required: true,
                    },
                    wrapdesign: {
                    required: true,
                    },
                    qty: {
                    required: true,
                    },
                    description: {
                    required: true,
                    },
                    description_ar: {
                    required: true,
                    },
                    @if(isset($getinfos->pro_Img) && $getinfos->pro_Img!='')
                    img: {
                    required: false,
                    accept:"png|jpe?g|gif",
                    },
                    @else
                    img: {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },
                   @endif
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
                title: {
              required:  " {{ trans('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME') }} ",
                },  

                title_ar: {
                required:  " {{ trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME') }} ",
                },
                price: {
                required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif",
                },
                qty: {
                required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_QUANTITY') }} @endif",
                },
                "flowertype[]": {
                required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_FLOWER_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_SELECT_FLOWER_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_FLOWER_TYPE') }} @endif",
                }, 
                wraptype: {
                required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_WRAP_TYPE') }} @endif",
                },
                wrapdesign: {
                required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_DESIGN')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_SELECT_WRAP_DESIGN') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_WRAP_DESIGN') }} @endif",
                },

                description: {
             required:  " {{ trans('mer_en_lang.MER_VALIDATION_ABOUT') }} ",
                }, 

                description_ar: {
                required:  " {{ trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR') }} ",
                },
                img: {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                },  

                      
                },
                invalidHandler: function(e, validation){
                   
                  var valdata=validation.invalid;

 @if($mer_selected_lang_code !='en')
                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                   if (typeof valdata.description != "undefined" || valdata.description != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }
                  if (typeof valdata.flowertype != "undefined" || valdata.flowertype != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                  if (typeof valdata.wraptype != "undefined" || valdata.wraptype != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                  if (typeof valdata.wrapdesign != "undefined" || valdata.wrapdesign != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }
                  if (typeof valdata.qty != "undefined" || valdata.qty != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                 
                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }
                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }




@else


                   if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.flowertype != "undefined" || valdata.flowertype != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.wraptype != "undefined" || valdata.wraptype != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.wrapdesign != "undefined" || valdata.wrapdesign != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.qty != "undefined" || valdata.qty != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.description != "undefined" || valdata.description != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                @endif

                  },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
 <script src="{{ url('')}}/themes/js/jquery.min.js"></script>
<script src="{{ url('')}}/themes/js/multiple-select.js"></script>
@if($mer_selected_lang_code !='en')
<script>
    $(function() {
        $('#flowertype').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });

          

    });
</script>
@else

<script>
    $(function() {
        $('#flowertype').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });

          

    });
</script>

@endif
@include('sitemerchant.includes.footer')
 @include('sitemerchant.includes.header')  
 <style type="text/css">
      .ms-drop ul li label input[type="checkbox"] {
      height: 20px;
      display: inline-block;
      width: 20px;
      position: relative;
      border: 1px solid red;
      opacity: 1;
      }
      </style>
      <link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />

@php $hall_leftmenu =1; 

if(isset($getDb->food_type) && ($getDb->food_type=='internal'  || $getDb->food_type=='both'))
{  $showMenu =1; } else { $showMenu =0; } 

@endphp 
<!--start merachant-->
<div class="merchant_vendor"> 
  
  <!--left panel--> 
  @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left') 
  <!--left panel end--> 
  <!--right panel-->
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_HALL_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALL_INFO') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALL_INFO') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> @php $getBinfo = Helper::getparentCat($_REQUEST['bid']);  
                $getBinfos = Helper::getparentCat( $getBinfo->parent_id );


              @endphp 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              @php
              if(isset($getDb->hall_address_image) && $getDb->hall_dimension !='')
              {
              
              $getDim = $getDb->hall_dimension;
              
              $getAllareas = explode('X',$getDim);
              $HallLength = $getAllareas[0];
              $HallWidth = $getAllareas[1];
              $HallArea = $getAllareas[2];
              
              }
              
              if(isset($getDb->food_type) && $getDb->food_type !='')
              {
              $hallfoodmenu = $getDb->food_type;
              $getHType = $getDb->hall_type;
              }
              else
              {
              $hallfoodmenu ='';
              $getHType = '';
              }
              
              @endphp
              {!! Form::open(array('url' => '/add_hallinfo','method' => 'POST', 'id'=> 'add_hallinfo','enctype' =>'multipart/form-data', 'name' =>'add_hallinfo' )) !!}
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HOTEL_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HOTEL_NAME'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english"> {{ $getBinfos->mc_name }} </div>
                    <div class="arabic ar"> {{ $getBinfos->mc_name_ar }} </div>
                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english"> {{ $getBinfo->mc_name }} </div>
                    <div class="arabic ar"> {{ $getBinfo->mc_name_ar }} </div>
                  </div>
                </div>
              </div>

               <div class="form_row common_field">
              <div class="form_row_left">
              <label class="form_label">
                <span class="english">@php echo lang::get('mer_en_lang.Select_Category'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Select_Category'); @endphp </span>

          </label>
              <div class="info100">  
              <select class="small-sel" name="category[]" id="category"   multiple="multiple">
           
                 @php $wedding_name='Wedding And Occasions'@endphp
                 @php $business_name='Business Meeting'@endphp
                 @if($mer_selected_lang_code !='en')
                 @php $wedding_name='زفاف و مناسبات'@endphp
                 @php $business_name='اجتماع عمل'@endphp
                 
            @endif

             @php
                  $halltype_one = 0; $halltype_two = 0;
  if(isset($getDb->hall_category_type) && $getDb->hall_category_type !='')
              {
                  $halltype = explode(',', $getDb->hall_category_type);

                  if(isset($halltype[0]) && $halltype[0]==1)
                  {
                  $halltype_two = 0;
                  $halltype_one = 1;
                  }
                  if(isset($halltype[1]) && $halltype[1]==2)
                  {
                  $halltype_two = 2;
                  $halltype_one = 1;
 
                  }
                  if(isset($halltype[0]) && $halltype[0]==2)
                  {
                  $halltype_two = 2;
                  $halltype_one = 0;
                  }
                }
                  @endphp

                  
                <option value="1" {{ isset($halltype_one) && $halltype_one ==1 ? 'selected' : ''}}>{{ $wedding_name }}</option>
                <option value="2" {{ isset($halltype_two) && $halltype_two ==2 ? 'selected' : ''}}>{{ $business_name }}</option>
              </select>
              </div>
              </div>
              </div>

              <input type="hidden" name="bid" value="{{$_REQUEST['bid'] or ''}}">
              <input type="hidden" name="pro_id" value="{{$_REQUEST['hid'] or ''}}">
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALL_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALL_NAME'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="hallname" maxlength="35"  data-validation="length required" 
		 data-validation-error-msg="Please enter hall name" value="{{$getDb->pro_title or ''}}" data-validation-length="max35">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="hallname_ar"  maxlength="35"   data-validation="length required" 
		 data-validation-error-msg="يرجى إدخال اسم القاعة" value="{{$getDb->pro_title_ar or ''}}" data-validation-length="max35">
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALL_TYPE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALL_TYPE'); @endphp </span> </label>
                  <div class="info100">
                    <div class="save-card-line">
                      <input id="men" type="checkbox" value="men"  name="halltype[]" @if($getHType=='men' || $getHType=='both') CHECKED @endif  data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="men"> <span class="english">@php echo lang::get('mer_en_lang.MER_MEN'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_MEN'); @endphp </span> </label>
                    </div>
                    <div class="save-card-line">
                      <input type="checkbox" value="women"  id="women" name="halltype[]" @if($getHType=='women' || $getHType=='both') CHECKED @endif data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="women"> <span class="english">@php echo lang::get('mer_en_lang.MER_WOMEN'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_WOMEN'); @endphp </span> </label>
                    </div>
                    <br>
                    <span for="halltype[]" generated="true" class="error"> </span> </div>
                </div>
              </div>
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.CAPACITY'); @endphp <span class="" style="font-size: 10pt; font-weight: normal;">(@php echo lang::get('mer_en_lang.People'); @endphp)</span></span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.CAPACITY'); @endphp <span class="" style="font-size: 10pt; font-weight: normal;">(@php echo lang::get('mer_ar_lang.People'); @endphp)</span> </span> </label>
                  <div class="engslish">
                    <input type="text" class="small-sel" name="hallcapicity"  maxlength="35"  data-validation-error-msg="يرجى إدخال اسم القاعة" value="{{$getDb->hallcapicity or ''}}" data-validation-length="max35">
                  </div>
                  @if($errors->has('hallcapicity')) <span class="error"> {{ $errors->first('hallcapicity') }} </span> @endif </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALL_PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALL_PRICE'); @endphp </span> </label>
                  <input type="text" name="price"  maxlength="8" onkeypress="return isNumber(event)" class="small-sel"  data-validation="length required" data-validation-error-msg="Please enter hall price" value="{{$getDb->pro_price or ''}}" data-validation-length="max30">
                  <br/>
                </div>
              </div>

               <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="{{$getDb->pro_discount_percentage or ''}}" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>

                     <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Insuranceamount'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Insuranceamount'); @endphp </span> </label>
                    <div class="ensglish">
                      <input type="text" class="small-sel" required="" name="Insuranceamount"  maxlength="15"   data-validation="length required" 
                    data-validation-error-msg="يرجى إدخال اسم القاعة" value="{{$getDb->Insuranceamount or ''}}" data-validation-length="max35">
                    </div>
                    @if($errors->has('Insuranceamount')) <span class="error"> {{ $errors->first('Insuranceamount') }} </span> @endif 
                    </div>


              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALLDIM'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALLDIM'); @endphp </span> </label>
                  <div class="info100">
                    <div class="hall_dimension" >
                      <div class="hall_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALLLENGTH'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALLLENGTH'); @endphp </span> </div>
                      <div class="hall_input">
                        <input type="text" name="hall_length"  maxlength="7" onkeypress="return isNumber(event)" data-validation="length required" data-validation-error-msg="Please enter hall length" value="{{$HallLength  or ''}}" data-validation-length="max7">
                      </div>
                      <div class="hall-parameter"> <span class="english">@php echo lang::get('mer_en_lang.MER_FT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_FT'); @endphp </span> </div>
                    </div>
                    <div class="hall_dimension" >
                      <div class="hall_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALLWIDTH'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALLWIDTH'); @endphp </span> </div>
                      <div class="hall_input">
                        <input type="text"  onkeypress="return isNumber(event)" maxlength="7"  name="hall_width"  data-validation="required" data-validation-error-msg="Please enter hall width" value="{{$HallWidth or ''}}" data-validation-length="max7">
                      </div>
                      <div class="hall-parameter"> <span class="english">@php echo lang::get('mer_en_lang.MER_FT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_FT'); @endphp </span> </div>
                    </div>
                    <div class="hall_dimension">
                      <div class="hall_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALLarea'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALLarea'); @endphp </span> </div>
                      <div class="hall_input">
                        <input type="text" onkeypress="return isNumber(event)" name="hall_area"  maxlength="7"  data-validation="required" data-validation-error-msg="Please enter hall area" value="{{$HallArea  or ''}}" data-validation-length="max7">
                      </div>
                      <div class="hall-parameter"> <span class="english">@php echo lang::get('mer_en_lang.MER_SQFT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SQFT'); @endphp </span> </div>
                    </div>
                  </div>
                </div>



                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_FOOD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_FOOD'); @endphp </span> </label>
                  <div class="info100 tooltip_icon">

                    @if($getBinfos->parent_id != 6)
                    <div class="save-card-line">
                      <input type="checkbox" id="internal" value="internal" name="hallfoodmenu[]" @if($hallfoodmenu=='internal' || $hallfoodmenu=='both') CHECKED @endif data-validation="checkbox_group" data-validation-qty="min1">
                      <label for="internal"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALLINTERNAL'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALLINTERNAL'); @endphp </span> <a class="tooltip_area" href="javascript:void(0);"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.List_the_food_choices_available_with_your_hall.?')!= '') {{  trans(Session::get('mer_lang_file').'.List_the_food_choices_available_with_your_hall.?') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.List_the_food_choices_available_with_your_hall.?') }} @endif
                      </span></a> </label>
                    </div>
                    <div class="save-card-line">
                      <input value="external" id="external" type="checkbox" name="hallfoodmenu[]" @if($hallfoodmenu=='external'  || $hallfoodmenu=='both') CHECKED @endif data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="external"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALLEXTERNAL'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALLEXTERNAL'); @endphp </span> <a class="tooltip_area" href="javascript:void(0);"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?')!= '') {{  trans(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Do_you_allow_food_from_outside?') }} @endif</span></a> </label>
                    </div>
                    <br/>
                    <span for="hallfoodmenu[]" generated="true" class="error"> </span>
                    @else
                
                      <div class="save-card-line">
                      <input value="external" id="external" type="checkbox" name="hallfoodmenu[]" @if($hallfoodmenu=='external'  || $hallfoodmenu=='both') CHECKED @endif data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="external"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALLEXTERNAL'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALLEXTERNAL'); @endphp </span> <a class="tooltip_area" href="javascript:void(0);"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?')!= '') {{  trans(Session::get('mer_lang_file').'.Do_you_allow_food_from_outside?') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Do_you_allow_food_from_outside?') }} @endif</span></a> </label>
                    </div>
                   @endif


                     </div>
                </div>
                

              </div>
              <div class="form_row ">
                <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALL_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALL_IMAGE'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value1"></div>
                    </div>
                    </label>
                    <input type="file" name="hallimage" id="company_logo" class="info-file">
                  </div>
                  
         
            <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
           
  
    <div class="form-upload-img">@if(isset($getDb->pro_Img) && $getDb->pro_Img !='') <img src="{{$getDb->pro_Img}}" width="150" height="150"> @endif </div>
 
                  @if($errors->has('hallimage')) <span class="error"> {{ $errors->first('hallimage') }} </span> @endif </div>
                <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_About'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_About'); @endphp </span> </label>
                  <div class="info100 english">
                    <textarea maxlength="500" name="about" id="about" rows="4" cols="50"  onkeyup="limitTextCount('about', 'divcount', 500,'@php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp');" onkeydown="limitTextCount('about', 'divcount', 500,'@php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp');">{{$getDb->about or ''}}</textarea>
                  </div>
                   <div class="info100 arabic ar">
                    <textarea maxlength="500" name="about_ar" id="about_ar" rows="4" cols="50"  onkeyup="limitTextCount('about_ar', 'divcount_ar', 500,'@php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp');" onkeydown="limitTextCount('about_ar', 'divcount_ar', 500,'@php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp');">{{$getDb->about_ar or ''}}</textarea>
                  </div>
                  @php 
                  if(isset($getDb->about) && $getDb->about!='') 
                  {
                     $chkChar = 500 - strlen(trim($getDb->about)); 
                  }
                  else
                  {
                     $chkChar = 500;
                  }
                  if(isset($getDb->about_ar) && $getDb->about_ar!='') 
                  {
                     $chkChar_ar = 500 - mb_strlen(trim($getDb->about_ar),'utf8'); 
                  }
                  else
                  {
                     $chkChar_ar = 500;
                  }
                 
                  @endphp
                    <div id="divcount" class="english">{{$chkChar or '500'}} @php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp</div>
                      <div id="divcount_ar" class="arabic ar">{{$chkChar_ar or '500'}}  @php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); @endphp</div>
                </div>
              </div>
              <input type="hidden" name="bid" value="{{$_REQUEST['bid'] or ''}}">
              <input type="hidden" name="pro_id" value="{{$_REQUEST['hid'] or ''}}">
              
              <div class="form-btn-section english">
              	<div class="form_row_left">
                <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                </div>
              </div>
              
              <div class="form-btn-section arabic ar">
              <div class="form_row_left arbic_right_btn">
                <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                </div>
              </div>
              
              {!! Form::close() !!} </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end right panel--> 
</div>
<!--end merachant--> 

<script>       
	$("form").data("validator").settings.ignore = "";
</script>
 
<script type="text/javascript">
  
$("#add_hallinfo").validate({
                  ignore: [],
                  rules: {
                  hallname: {
                       required: true,
                      },

                       hallname_ar: {
                       required: true,
                      },


                       category: {
                       required: true,
                      },

                    

                      hall_length: {
                       required: true,
                      },
                       hall_width: {
                       required: true,
                      },
                       hall_area: {
                       required: true,

                      },

                       'hallfoodmenu[]': {
                       required: true,
                      },

                       'halltype[]': {
                       required: true,
                      },


                       price: {
                       required: true,
                       min: 1,
                      },

                     @if(isset($getDb->pro_Img)!='')  
                      hallimage: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },
                      @else
                        hallimage: {
                           required:true,
                           accept:"png|jpe?g|gif",
                      },
                      @endif

                      hall_addressimg: {
                           accept:"png|jpe?g|gif",
                      },
                       hallcapicity: {
                       required: true,
                      },

                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                   
           messages: {
            
                 category: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SELECT_CATEGORY') }} @endif",
                      }, 

             hallname: {
         required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_HALL_NAME'); @endphp ", 
                      },  

                 hallname_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_HALL_NAME_AR'); @endphp ",
                      },  
 
                          hall_length: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_LENTH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_LENTH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_LENTH') }} @endif",
                      },   


                       hall_width: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_WIDTH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_WIDTH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_WIDTH') }} @endif ",
                      },

                       hall_area: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_AREA')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_AREA') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_AREA') }} @endif ",
                      },

                       'hallfoodmenu[]': {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_FOOD_MENMU')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_FOOD_MENMU') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_FOOD_MENMU') }} @endif ",
                      },

                       'halltype[]': {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_TYPE') }} @endif ",
                      },


                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_PRICE') }} @endif ",
                      },
                        hallimage: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                      },     

                       hall_addressimg: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },   
                          hallcapicity: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_CAPACITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HALL_CAPACITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HALL_CAPACITY') }} @endif",
                      },   
                    
                     
                },
                invalidHandler: function(e, validation){
                   // console.log("invalidHandler : event", e);
                    //console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    var valdata1=validation.errorList;

                     console.log("invalidHandler : validationerror",valdata1 ); 




                    var i;
                    var erroren=0;
                    var start=9;
                    var lang ='ar';
                    var val=0
                    
                    for (i = 0; i < start ;  i++) {
                      
                      if (typeof valdata1[i] !== 'undefined') {  
                        var data=valdata1[i].element.classList;
                        $.inArray('ar',data);
                                        console.log("invalidHandler : validation", $.inArray('ar',data)); 
                                              if($.inArray('ar',data) == -1) 
                                              {
                                                  val=1;

                                              } 

                                          }                                          
                                     }
                                        @if($mer_selected_lang_code !='ar')
                                              if(val==0)
                                              {

                                                $('.arabic_tab').trigger('click');  
                                              }
                                              else
                                              {

                                                $('.english_tab').trigger('click');
                                              }
                                        @else

                                         if(val==0)
                                              {
                                                     $('.english_tab').trigger('click');
                                               
                                              }
                                              else
                                              {

                                                    $('.arabic_tab').trigger('click');  

                                              
                                              }
                                       @endif       
                                        

                       

 
 

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 

@include('sitemerchant.includes.footer')


<script src="{{ url('')}}/themes/js/jquery.min.js"></script> 
<script src="{{ url('')}}/themes/js/multiple-select.js"></script> 
@if(Session::get('lang_file') =='ar_lang') 

<script>
    $(function() {
        $('#category').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });
      });
</script>
 @else 
<script>
    $(function() {
        $('#category').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });
     });
</script> 
@endif 


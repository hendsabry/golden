<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<?php /*if (Session::has('merchantid'))
{
    $merchantid=Session::get('merchantid');
}*/

?>
@if (Session::has('merchantid'))
<?php   $merchantid=Session::get('merchantid'); ?>
@endif
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }}| @if (Lang::has(Session::get('mer_lang_file').'.MER_DASHBOARD')!= '') 
     {{ trans(Session::get('mer_lang_file').'.MER_DASHBOARD') }}
       @else 
     {{  trans($MER_OUR_LANGUAGE.'.MER_DASHBOARD') }}
      @endif </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('')}}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('')}}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('')}}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
    <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>
    <!-- <script src="http://192.168.2.50/nexemerchant/public/assets/plugins/jquery-2.0.3.min.js"></script>-->
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
  {!!$merchantheader!!}
        <!-- END HEADER SECTION -->



        <!-- MENU SECTION -->
       <div id="left" >
           

        </div>
        <!--END MENU SECTION -->
    <div class="container">
          <div class="row">
                    

                </div>
          
        </div>


        <!--PAGE CONTENT -->
        <div class=" container" >
            <div class="inner" style="min-height: 700px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                          <header>
                <div class="icons"><i class="icon-dashboard"></i></div>
  <h5> @if (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_DASHBOARD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MERCHANT_DASHBOARD') }}  
  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_DASHBOARD') }} @endif </h5>
            

           
            </header>
              
@php $sold_cnt=0; @endphp
 @foreach($soldproductscnt as $soldres)
  @if($soldres->pro_no_of_purchase  >=$soldres->pro_qty)
    
@php    $sold_cnt++; @endphp
    @endif
  @endforeach 

 @php
  $active_withAvailableQty = ($activeproductscnt-$sold_cnt)>0?($activeproductscnt-$sold_cnt):0;
@endphp

              <div class="col-lg-12">
                        <div style="text-align: center;">
                           
                              <a class="quick-btn1 active" href="{{ url('mer_manage_product')}}">
                                <i class="icon-check icon-2x"></i>
                                <span> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE_PRODUCTS')!= '') 
                                {{ trans(Session::get('mer_lang_file').'.MER_ACTIVE_PRODUCTS') }}  
                                @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE_PRODUCTS') }} @endif </span>
                                <span class="label label-danger"> {{ $active_withAvailableQty }}</span>
                            </a>
               <a class="quick-btn1" href="{{ url('mer_sold_product')}}">
                                <i class="icon-check-minus icon-2x"></i>
                                <span>@if (Lang::has(Session::get('mer_lang_file').'.MER_SOLD_PRODUCTS')!= '') {{ trans(Session::get('mer_lang_file').'.MER_SOLD_PRODUCTS') }}  
                                @else {{ trans($MER_OUR_LANGUAGE.'.MER_SOLD_PRODUCTS') }} @endif</span>
                                <span class="label label-success">{{ $sold_cnt }} </span>
                            </a>
                          
                            <a class="quick-btn1" href="{{ url('mer_manage_deals') }}">  
                             <i class="icon-cloud-upload icon-2x"></i>
                         <span>   @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE_DEALS')!= '') 
                            {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE_DEALS') }}  
                                @else {{  trans($MER_OUR_LANGUAGE.'.MER_ACTIVE_DEALS') }}
                                 @endif
                               
                                
                                </span>
                                <span class="label label-warning">{{ $activedealscnt}}</span>
                            </a>
                              <a class="quick-btn1" href="{{ url('mer_expired_deals') }}">
                                <i class="icon-external-link icon-2x"></i>
                                <span>@if (Lang::has(Session::get('mer_lang_file').'.MER_EXPIRED_DEALS')!= '') {{   trans(Session::get('mer_lang_file').'.MER_EXPIRED_DEALS') }}  @else {{  trans($MER_OUR_LANGUAGE.'.MER_EXPIRED_DEALS') }}  @endif </span>
                                <span class="label btn-metis-2">{{ $archievddealcnt}}</span>
                            </a>
                          
                                          
                           
                            <a class="quick-btn1" href="{{ url('merchant_manage_shop').'/'.$merchantid}}">
                                <i class="icon-check icon-2x"></i>
                                <span>@if (Lang::has(Session::get('mer_lang_file').'.MER_STORES')!= '') 
                                {{  trans(Session::get('mer_lang_file').'.MER_STORES') }}  
                                @else {{  trans($MER_OUR_LANGUAGE.'.MER_STORES') }} @endif</span>
                                <span class="label label-danger">{{ $storescnt}}</span>
                            </a>
                            </div>
                        
                        <div style="height:30px"></div>

                    </div>
                        </div>
                    </div>
                </div>
                
                
                 <div class="row">
                    <div class="col-lg-12">
                 <button class="btn btn-success btn-sm btn-grad" style="margin-bottom:10px;"><a style="color:#fff" href="{{ url('storeview/'.base64_encode(base64_encode(base64_encode($mer_store_id)))) }}" target="_blank">
                 @if (Lang::has(Session::get('mer_lang_file').'.MER_GO_TO_LIVE')!= '') {{   trans(Session::get('mer_lang_file').'.MER_GO_TO_LIVE') }}  @else {{  trans($MER_OUR_LANGUAGE.'.MER_GO_TO_LIVE') }} @endif</a></button>
                
                </div>
                </div>
                
                
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                               @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICES')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_ADD_SERVICES') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICES') }} @endif
                            </div>

                             
                            <div class="panel-body col-lg-6  panel panel-default">
                       <div class="panel-heading text-center">

              <strong>@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') 
                               {{  trans(Session::get('mer_lang_file').'.MER_SERVICES') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICES') }} @endif </strong>
            </div>  
               <form name="form1" method="post" action="{{ route('storebranch') }}" enctype="multipart/form-data">
                 {{ csrf_field() }}
                 <label>City</label>
                <select name="city_id" id="city_id">
                   <option value=""> Select City </option>
                   @foreach($city as $val)
                  <option value="{{ $val->ci_id }}" {{ isset($ids->city_id) && $ids->city_id ==$val->ci_id ? 'selected' : ''}}>{{ $val->ci_name }}</option>
                   @endforeach
                </select> </br> </br>
                <label>Services Category</label>
                <input type="text" name="sec_main_category1" readonly="" id="sec_main_category" value="{{ $ssb_sb_id_data->smc_name }}" > </br> </br>
                <input type="hidden" name="sec_main_category" id="sec_main_category" value="{{ $ssb_sb_id_data->smc_id }}">
               <label>Services Subcategory</label>
                 <input type="text" name="sub_category_id1" readonly=""  id="sub_category_id" value="{{ $ssb_sb_id_datasub->sb_name }}"> 
                 <input type="hidden" name="sub_category_id"  id="sub_category_id" value=""> 
                </br> </br> 
                 <label>Hotel Name</label>
                <input type="text" name="ssb_name" readonly="" value="{{ $thirdcat->ssb_name }}" id="ssb_name" placeholder="Hotel name"> </br> </br> 
                <input type="hidden" name="secsubcategory_id" value="">
                <label>Branch Name</label>
                <input type="text" name="name1" value="{{ $ids->name }}" readonly="" id="name" placeholder="Branch name"> </br> </br> 
                <input type="hidden" name="name" value="{{ $ids->id }}">
                <label>Upload Hall Logo</label>
                <input type="file" name="thir_img" required="" value="">  </br> 
                <label>Google Map Address Url</label>
                <input type="text" name="ssb_name"  value="" id="ssb_name" placeholder="Google Map Address Url"> </br> </br> 
                <label>Address Image</label>
                <input type="file" name="thir_img" required="" value="">  </br> 
               <label>Hall Dimension</label></br>
                <input type="text" name="ssb_name"  value="" id="ssb_name" placeholder="Length"> Length </br> </br> 
                <input type="text" name="ssb_name"  value="" id="ssb_name" placeholder="Width"> Width </br> </br>
                <input type="text" name="ssb_name"  value="" id="ssb_name" placeholder="Area"> Area </br> </br>

                 <label>Food</label> </br> 
                 <input type="radio" name="internal" value="1">Internal
                   <input type="radio" name="internal" value="2">External<br><br>

                   <label>Hall Type</label> </br> 
                   <input type="radio" name="internal" value="1">Men
                   <input type="radio" name="internal" value="2">Women </br> </br>

                   <label>Hall Price</label>
                <input type="text" name="ssb_name"  value="" id="ssb_name" placeholder="Hall Price"> </br> </br> 
                <input type="hidden" name="mc_id" value=""> 
                <input type="submit" name="submit" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBMIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SUBMIT') }} @endif">
                </form>


          
    </div>
       
                           
                            </div>
                    </div>

                    
                     
                </div>
                
                
                
               
                
                
                 
                 <div class="row">
                    <div class="col-lg-12">
                   
                         <?php /*?><div class="panel panel-default">
                                <div class="panel-heading">
                                   Last one year Transactions report
                                </div>
                             
                            <div class="panel-body">
                              
                  <div class="demo-container" id="chart5" style="margin-top:20px; margin-left:20px; width:950px; height:470px;"></div>  
                </div>
                             
    
                            </div><?php */?>
                    </div>

                    
                   
                </div>
                 
                          
              

                
            </div>

        </div>
       
    </div>

    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    {!! $merchantfooter !!}
    <!--END FOOTER -->
    @if($dealchartdetails!='')
    @if($dealchartdetails!='0,0,0,0,0,0,0,0,0,0,0,0')
   
     <script class="code" type="text/javascript">
     $(document).ready(function(){ 
     
        $.jqplot.config.enablePlugins = true;
    
    <?php $s1 = "[" .$dealchartdetails. "]"; ?>
        var s1 = {{ $s1 }};
        var ticks = ['{{ (Lang::has(Session::get('mer_lang_file').'.MER_JAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JAN') : trans($MER_OUR_LANGUAGE.'.MER_JAN') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_FEB')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FEB') : trans($MER_OUR_LANGUAGE.'.MER_FEB') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAR') : trans($MER_OUR_LANGUAGE.'.MER_MAR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_APR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_APR'): trans($MER_OUR_LANGUAGE.'.MER_APR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAY'): trans($MER_OUR_LANGUAGE.'.MER_MAY')}}','{{ (Lang::has(Session::get('mer_lang_file').'.MER_JUNE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JUNE'): trans($MER_OUR_LANGUAGE.'.MER_JUNE')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_JULY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JULY'): trans($MER_OUR_LANGUAGE.'.MER_JULY')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_AUG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUG') : trans($MER_OUR_LANGUAGE.'.MER_AUG')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEP') :  trans($MER_OUR_LANGUAGE.'.MER_SEP')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_OCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_OCT') : trans($MER_OUR_LANGUAGE.'.MER_OCT')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_NOV')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NOV'): trans($MER_OUR_LANGUAGE.'.MER_NOV')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEC')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEC'): trans($MER_OUR_LANGUAGE.'.MER_DEC')}}'];
        
        plot1 = $.jqplot('chart2', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart2').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
  @endif
  @endif
  @if(($activedealscnt+$archievddealcnt)>0)  
    <script>
  $(document).ready(function(){
    
  plot6 = $.jqplot('chart6', [[{{ $activedealscnt }},{{ $archievddealcnt }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } } );
    });
  </script>
  @endif
  @if(($active_withAvailableQty+$sold_cnt)>0)
  <script>
  $(document).ready(function(){
    
  plot10 = $.jqplot('chart10', [[{{ $active_withAvailableQty }},{{ $sold_cnt }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer } });
    });
  </script>
  @endif
  @if($productchartdetails!='')
  @if($productchartdetails!='0,0,0,0,0,0,0,0,0,0,0,0')
      <script class="code" type="text/javascript">
      $(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
    
    <?php $s1 = "[" .$productchartdetails. "]"; ?>
        var s1 = {{ $s1 }};
        var ticks = ['{{ (Lang::has(Session::get('mer_lang_file').'.MER_JAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JAN') : trans($MER_OUR_LANGUAGE.'.MER_JAN') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_FEB')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FEB') : trans($MER_OUR_LANGUAGE.'.MER_FEB') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAR') : trans($MER_OUR_LANGUAGE.'.MER_MAR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_APR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_APR'): trans($MER_OUR_LANGUAGE.'.MER_APR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAY'): trans($MER_OUR_LANGUAGE.'.MER_MAY')}}','{{ (Lang::has(Session::get('mer_lang_file').'.MER_JUNE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JUNE'): trans($MER_OUR_LANGUAGE.'.MER_JUNE')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_JULY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JULY'): trans($MER_OUR_LANGUAGE.'.MER_JULY')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_AUG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUG') : trans($MER_OUR_LANGUAGE.'.MER_AUG')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEP') :  trans($MER_OUR_LANGUAGE.'.MER_SEP')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_OCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_OCT') : trans($MER_OUR_LANGUAGE.'.MER_OCT')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_NOV')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NOV'): trans($MER_OUR_LANGUAGE.'.MER_NOV')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEC')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEC'): trans($MER_OUR_LANGUAGE.'.MER_DEC')}}'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    @endif
    @endif
    @if($auctionchartdetails!='0,0,0,0,0,0,0,0,0,0,0,0')   
     <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
    
    <?php $s1 = "[" .$auctionchartdetails. "]"; ?>
        var s1 = {{ $s1 }};
        var ticks = ['{{ (Lang::has(Session::get('mer_lang_file').'.MER_JAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JAN') : trans($MER_OUR_LANGUAGE.'.MER_JAN') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_FEB')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FEB') : trans($MER_OUR_LANGUAGE.'.MER_FEB') }}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAR') : trans($MER_OUR_LANGUAGE.'.MER_MAR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_APR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_APR'): trans($MER_OUR_LANGUAGE.'.MER_APR')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_MAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MAY'): trans($MER_OUR_LANGUAGE.'.MER_MAY')}}','{{ (Lang::has(Session::get('mer_lang_file').'.MER_JUNE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JUNE'): trans($MER_OUR_LANGUAGE.'.MER_JUNE')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_JULY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_JULY'): trans($MER_OUR_LANGUAGE.'.MER_JULY')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_AUG')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUG') : trans($MER_OUR_LANGUAGE.'.MER_AUG')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEP')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEP') :  trans($MER_OUR_LANGUAGE.'.MER_SEP')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_OCT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_OCT') : trans($MER_OUR_LANGUAGE.'.MER_OCT')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_NOV')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NOV'): trans($MER_OUR_LANGUAGE.'.MER_NOV')}}', '{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEC')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEC'): trans($MER_OUR_LANGUAGE.'.MER_DEC')}}'];
        
        plot1 = $.jqplot('chart3', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart3').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    @endif
   
    <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    

    <!-- GLOBAL SCRIPTS -->
    
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
  <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="{{ url('')}}/public/assets/js/bar_chart.js"></script>
    
    <!-- END PAGE LEVEL SCRIPTS -->
  <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>

</body>

    <!-- END BODY -->
</html>

@include('sitemerchant.includes.header')  
@php $Tailorbig_leftmenu =1; @endphp
<!--start merachant-->
<div class="merchant_vendor"> 
@php
  $id                = $id;
  $opid              = $opid;
  $oid               = $oid;
  $cusid             = $cusid;
  $hid               = $hid;
  $prodid            = $prodid;
  $ordid=$oid;
  $getCustomer       = Helper::getuserinfo($cusid);
  $shipaddress       = Helper::getgenralinfo($oid);  
  $cityrecord        = Helper::getcity($shipaddress->shipping_city);
  $getorderedproduct = Helper::getorderedproduct($oid);
@endphp
  <!--left panel-->
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif
        <a href="{{url('/')}}/tailor-order/{{$id}}/{{$prodid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a></h5></div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      <?php //echo '<pre>';print_r($getCustomer);die; ?>
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} @endphp <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" > @php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      @endphp </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
                  @php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                  </div>
                  @php } @endphp
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                    <div class="info100">{{ $productdata[0]->shippingMethod }} </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100"> SAR  {{ $productdata[0]->shippingPrice }}</div>
                    <!-- box -->
                  </div>
          @php         $getsearchedproduct = Helper::searchorderedDetails($oid);
  if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ @endphp
                    <!--<div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONDATE') }} @endif</label>
                    <div class="info100"> @php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                      echo $getsearchedproduct->occasion_date;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                  @php } @endphp
                </div>-->
                <!--global end-->
              </div>
              <!-- hall-od-top -->
			  <div class="tailor_product">
                <div class="tailorpro-heading">@if (Lang::has(Session::get('mer_lang_file').'.PRODUCT_INFORMATION')!= '') {{  trans(Session::get('mer_lang_file').'.PRODUCT_INFORMATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PRODUCT_INFORMATION') }} @endif</div>
              @php 
              $basetotal = 0;
              $i = 1;
               $couponcode=0; $couponcodeinfo='';
              if(count($productdata) > 0){
              foreach($productdata as $val)
              {
              $basetotal            = ($basetotal+$val->total_price);
              $pid                  = $val->product_id;
              $serviceInfo          = Helper::getProduckInfo($val->product_id);		  
              if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}

              $couponcode=$couponcode+$val->coupon_code_amount;;
        $couponcodeinfo=$val->coupon_code;
$lan=Session::get('mer_lang_file');
        $fabinfo=Helper::getfabricnamefororder($val->fabric_id,$lan);
              @endphp   
                   
                <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                  <div class="style_area">
                    <div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.STYLE_AND_FABRIC_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.STYLE_AND_FABRIC_INFO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.STYLE_AND_FABRIC_INFO') }} @endif</div>
                    <div class="style_box_type">
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.STYLE')!= '') {{  trans(Session::get('mer_lang_file').'.STYLE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.STYLE') }} @endif</div>
                          <div class="style_label_text"> <?php echo $serviceInfo->pro_title; ?></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.STYLE_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.STYLE_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.STYLE_IMAGE') }} @endif</div>
                          <div class="style_label_text"> <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.STYLE_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.STYLE_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.STYLE_PRICE') }} @endif</div>
                          <div class="style_label_text"> SAR <?php echo $getPrice; ?></div>
                        </div>
                      </div>
					  <?php if(isset($val->fabric_name) && $val->fabric_name!=''){ ?>
                      <div class="sts_box">
					    <?php if(isset($val->fabric_name) && $val->fabric_name!=''){ ?>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.FABRICS_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.FABRICS_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.FABRICS_NAME') }} @endif</div>
                          <div class="style_label_text"> <?php echo $fabinfo->fabric_name; ?></div>
                        </div>
						<?php } if(isset($val->fabric_img) && $val->fabric_img!=''){ ?>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.FABRICS_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.FABRICS_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.FABRICS_IMAGE') }} @endif</div>
                          <div class="style_label_text"> <img src="<?php echo $val->fabric_img; ?>" width="80" height="80"/></div>
                        </div>
						<?php } if(isset($val->fabric_price) && $val->fabric_price!=''){ ?>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.FABRICS_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.FABRICS_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.FABRICS_PRICE') }} @endif</div>
                          <div class="style_label_text"> SAR <?php echo $val->fabric_price; ?></div>
                        </div>
						<?php }  ?>
                      </div>
					  <?php } ?>
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.QUANTITY') }} @endif</div>
                          <div class="style_label_text"> <?php echo $val->quantity; ?></div>
                        </div>
                        <div class="style_right">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.DELIVERY_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.DELIVERY_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.DELIVERY_DATE') }} @endif</div>
                          <div class="style_label_text"> 
						  <?php 
							$date = $val->created_at; //existing date

							if(isset($serviceInfo->deliver_day) && $serviceInfo->deliver_day!=''){
							if($serviceInfo->deliver_day==1){$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' day'));}else{$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' days'));}
							echo $newDate;
							}
						  ?> 
						  </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="style_area">
                    <div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.BODY_MEASUREMENT')!= '') {{  trans(Session::get('mer_lang_file').'.BODY_MEASUREMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BODY_MEASUREMENT') }} @endif</div>
                    <div class="style_box_type">
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.LENGTH')!= '') {{  trans(Session::get('mer_lang_file').'.LENGTH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LENGTH') }} @endif</div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->length; ?>@if (Lang::has(Session::get('mer_lang_file').'.CM')!= '') {{  trans(Session::get('mer_lang_file').'.CM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CM') }} @endif</div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.CHEST_SIZE')!= '') {{  trans(Session::get('mer_lang_file').'.CHEST_SIZE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CHEST_SIZE') }} @endif </div>
                          <div class="style_label_text"><?php echo $val->bookingdetail->chest_size; ?>@if (Lang::has(Session::get('mer_lang_file').'.CM')!= '') {{  trans(Session::get('mer_lang_file').'.CM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CM') }} @endif</div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.WAIST_SIZE')!= '') {{  trans(Session::get('mer_lang_file').'.WAIST_SIZE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.WAIST_SIZE') }} @endif </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->waistsize; ?>@if (Lang::has(Session::get('mer_lang_file').'.CM')!= '') {{  trans(Session::get('mer_lang_file').'.CM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CM') }} @endif</div>
                        </div>
                      </div>
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.SHOULDERS')!= '') {{  trans(Session::get('mer_lang_file').'.SHOULDERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SHOULDERS') }} @endif </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->soulders; ?>@if (Lang::has(Session::get('mer_lang_file').'.CM')!= '') {{  trans(Session::get('mer_lang_file').'.CM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CM') }} @endif</div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.NECK')!= '') {{  trans(Session::get('mer_lang_file').'.NECK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.NECK') }} @endif </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->neck; ?>@if (Lang::has(Session::get('mer_lang_file').'.CM')!= '') {{  trans(Session::get('mer_lang_file').'.CM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CM') }} @endif</div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.ARM_LENGTH')!= '') {{  trans(Session::get('mer_lang_file').'.ARM_LENGTH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ARM_LENGTH') }} @endif </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->arm_length; ?>@if (Lang::has(Session::get('mer_lang_file').'.CM')!= '') {{  trans(Session::get('mer_lang_file').'.CM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CM') }} @endif</div>
                        </div>
                      </div>
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.WRIST_DIAMETER')!= '') {{  trans(Session::get('mer_lang_file').'.WRIST_DIAMETER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.WRIST_DIAMETER') }} @endif </div>
                          <div class="style_label_text"> <?php echo $val->bookingdetail->wrist_diameter; ?>@if (Lang::has(Session::get('mer_lang_file').'.CM')!= '') {{  trans(Session::get('mer_lang_file').'.CM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CM') }} @endif</div>
                        </div>
                        <!--<div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.ARM_LENGTH')!= '') {{  trans(Session::get('mer_lang_file').'.ARM_LENGTH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ARM_LENGTH') }} @endif </div>
                          <div class="style_label_text"> 20</div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">Cutting</div>
                          <div class="style_label_text"> 20</div>
                        </div>-->
                      </div>
                    </div>
                  </div>
                </div>             
              @php $i++;} @endphp
              <div class="merchant-order-total-area">
                <div class="merchant-order-total-line">


                   @if(isset($couponcode) && $couponcode>=1)
             {{ (Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')}}: 

             @php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp  
          <br>

          {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')}}: {{ $couponcodeinfo }}
 <br>
          @endif



            @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             @endphp

          {{ (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp 



                 </div>
				  <div class="merchant-order-total-line">
                {{ (Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($MER_OUR_LANGUAGE.'.TOTAL_PRICE')}}:
                <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);
				 } ?>
              </div></div>
              <?php } ?>
			   </div>
            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
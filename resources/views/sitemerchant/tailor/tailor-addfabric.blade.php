@include('sitemerchant.includes.header') 
@php $Tailorbig_leftmenu =1; @endphp
<div class="merchant_vendor">
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
     <header>
      @if($autoid=='')
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADD_Fabric')!= '') {{  trans(Session::get('mer_lang_file').'.ADD_Fabric') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.ADD_Fabric') }} @endif </h5>

        @else
          <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATE_Fabric')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_Fabric') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_Fabric') }} @endif </h5>

        @endif
        @include('sitemerchant.includes.language') </header>
		   <div class="global_area">
      <div class="row">

          <div class="col-lg-12">
            <div class="box">
		
        <form name="form1" id="menucategory" method="post" action="{{ route('store-tailorfabrics') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
				  
                  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.name'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.name'); @endphp </span>
                    </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="{{ $categorysave->fabric_name or '' }}"  class="english"  maxlength="150" />
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" value="{{ $categorysave->fabric_name_ar or '' }}" class="arabic ar" name="mc_name_ar"  id="mc_name_ar"   type="text"  maxlength="150">
                      </div>
                    </div>
                    </div>
                  </div>
				  
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.Image'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Image'); @endphp </span>
 </label>
                    <div class="info100">
                     
				  <div class="input-file-area">
                  <label for="company_logo">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
   <div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div>
                  </div>
                  </label>
                  <input id="company_logo" data-lbl="file_value1" name="proimage" class="info-file" type="file">
                </div>
                <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                  @if(isset($categorysave->image) && $categorysave->image !='')
                 <div class="form-upload-img">
                       <img src="{{ $categorysave->image or '' }}" >
                     </div>
                     @endif 
                    </div>
                    </div>
                  </div>


    
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span>
 </label>
                    <div class="info100">
                      
                        <input type="text" class="notzero xs_small" name="price" id="price" onkeypress="return isNumber(event)" value="{{ $categorysave->price or '' }}"  maxlength="9" />
                        <input type="hidden" name="id" value="{{ $id or '' }}">
                        <input type="hidden" name="itemid" value="{{ $itemid or '' }}">
                        <input type="hidden" name="autoid" value="{{ $autoid or '' }}">
                     <input type="hidden" name="status" value="{{ $categorysave->status or 1 }}">
                    </div>
                    </div>
                  </div>

 
             
				 <!-- <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label">
                    <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span>
                    <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span>
                     </label>
                    <div class="info100">
        					   <div class="english"><textarea name="description" maxlength="500" id="description">{{ $categorysave->fabric_desc or '' }}</textarea></div>
                      <div class="arabic ar"><textarea name="description_ar" maxlength="500" id="description_ar">{{ $categorysave->fabric_desc_ar or '' }}</textarea></div>
                      
                    </div>
                    </div>
               </div>-->
                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  </div></div>
			   </form>
		
			 
			</div></div></div></div>
		

  </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>
 $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#menucategory").validate({

                  ignore: [],
                  rules: {
                
                       mc_name: {
                       required: true,
                      },
                       mc_name_ar: {
                       required: true,
                      },
 
                       price: {
                       required: true,
                      },
                       
    

                       @if(isset($categorysave->image)!='')  
                        proimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        proimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             

                  mc_name: {
                     required:  "@php echo lang::get('mer_en_lang.PLEASE_ENTER_NAME'); @endphp",

                      },   

                          mc_name_ar: {
                             required:  "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_NAME'); @endphp",
                 
                      },   

             

                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                      },

                        quantity: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY') }} @endif ",
                      },

                    
  

                  proimage: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },   


                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  @if($mer_selected_lang_code !='en')
                   
                     if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                       if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                        $('.arabic_tab').trigger('click');
                           

                    }
                     
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                     

                    }
                    
                   
                     
                      if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                    
                    
          @else


                       if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                        $('.arabic_tab').trigger('click');
                           

                    }
                     
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                    
                   
                     
                      if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    
                @endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script> 
 
 

 
@include('sitemerchant.includes.footer')
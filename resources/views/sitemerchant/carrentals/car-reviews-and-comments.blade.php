 @inject('data','App\Help')
@include('sitemerchant.includes.header')  

@php $Carrental_leftmenu =1; @endphp
<div class="merchant_vendor cont_add"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_REVIEWANDRATING')!= '') {{  trans(Session::get('mer_lang_file').'.MER_REVIEWANDRATING') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_REVIEWANDRATING') }} @endif </h5>
      </header>
      
      <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition --> 
              
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
          
            <div class="table_wrap"> 
              <!-- Display Message after submition --> 
              
              
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!-- Display Message after submition -->
              <div class="panel-body panel panel-default"> @if($reviewrating->count() <1)
                <div class="no-record-area">@if (Lang::has(Session::get('mer_lang_file').'.You_are_yet_to_get_reviews_or_ratings')!= '') {{ trans(Session::get('mer_lang_file').'.You_are_yet_to_get_reviews_or_ratings')}}  @else {{ trans($MER_OUR_LANGUAGE.'.You_are_yet_to_get_reviews_or_ratings')}} @endif</div>
                @else
                <div class="table revie_table">
                  <div class="tr">
                    <div class="table_heading product_name">@if (Lang::has(Session::get('mer_lang_file').'.MER_GIVEN_BY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_GIVEN_BY') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_GIVEN_BY') }} @endif</div>
                    <div class="table_heading product_name">@if (Lang::has(Session::get('mer_lang_file').'.MER_RREVIEW_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_RREVIEW_DATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_RREVIEW_DATE') }} @endif</div>  
                    <div class="table_heading prod_comp">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMENT') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMENT') }} @endif</div>
                    <div class="table_heading prod_rating">@if (Lang::has(Session::get('mer_lang_file').'.MER_RATING')!= '') {{  trans(Session::get('mer_lang_file').'.MER_RATING') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_RATING') }} @endif</div>
                  </div>
                  @foreach($reviewrating as $key=> $row)
                  @php $rating=($row->ratings/5)*100; @endphp
                  <div class="tr">
                    <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_GIVEN_BY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_GIVEN_BY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_GIVEN_BY') }} @endif">{{$data->CustName($row->customer_id)}}<!--  <span class="review_name">Test Name</span><span class="reivew_img"><img src="{{url('')}}/public/assets/img/dish.jpg" alt="" /></span> --></div>
                    <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_RREVIEW_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_RREVIEW_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_RREVIEW_DATE') }} @endif">{{date('d M Y',strtotime($row->created_at))}}</div>
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMENT') }} @endif">{{$row->comments}}</div>
                    <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_RATING')!= '') {{  trans(Session::get('mer_lang_file').'.MER_RATING') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_RATING') }} @endif">

                      <div class="containerdiv">
                        <div>
                      <!-- <img src="https://image.ibb.co/jpMUXa/stars_blank.png" alt="img">-->
						<img src="{{url('')}}/public/assets/img/stars_blank.png" alt="" />

                        </div>
                        <div class="cornerimage" style="width:{{$rating}}%;">
						<img src="{{url('')}}/public/assets/img/stars_full.png" alt="" />

                        <!--<img src="https://image.ibb.co/caxgdF/stars_full.png" alt="">-->
						
                        </div>                     
                      </div>
                     </div> 
                  </div>   
                  @endforeach </div>
              </div>
              @endif 
              
              </div>
                          {{ $reviewrating->links() }} 
  
          </div>
        </div>
        
      </div> <!-- global_area -->
      
    </div>
  </div>
</div>
</div>
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $Carrental_leftmenu =1; @endphp
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if($autoid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Add_Car_Models')!= '') {{  trans(Session::get('mer_lang_file').'.Add_Car_Models') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Add_Car_Models') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Update_Car_Models')!= '') {{  trans(Session::get('mer_lang_file').'.Update_Car_Models') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Update_Car_Models') }} @endif </h5>
        @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box arabic-ltr">
              <!-- Display Message after submition -->
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif
              <!-- Display Message after submition -->
              <form name="makeupartist" id="makeupartist" method="post" action="{{ route('store-car-service') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Model_Name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Model_Name'); @endphp</span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="model_name" maxlength="60" value="{{ $fetchdata->pro_title or '' }}" id="service_name"  >
                      </div>
                      <div class="arabic ar ">
                        <input type="text" class="arabic ar" name="model_name_ar" maxlength="60" value="{{ $fetchdata->pro_title_ar or '' }}" id="model_name_ar" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Price_Per_KM'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Price_Per_KM'); @endphp (%) </span> </label>
                    <div class="info100">
                      <div>
                        <input type="text" class="xs_small notzero" name="price"  onkeypress="return isNumber(event)"  maxlength="9" value="{{ $PriceKM->value or '' }}" id="price" >
                      </div>
                    </div>
                  </div>

                   <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="{{ $fetchdata->pro_qty or '' }}"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>

                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="{{$fetchdata->pro_discount_percentage or ''}}" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                      @if(isset($fetchdata->pro_Img)!='')
                      <div class="form-upload-img"> <img src="{{ $fetchdata->pro_Img or '' }}" width="150" height="150"> </div>
                      @endif </div>
                  </div>



<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->




                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Model_year'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Model_year'); @endphp </span> </label>
                    <div class="info100">
                      <div>
                        <input type="text" class="xs_small notzero" name="model_year" onkeypress="return isNumber(event)"  maxlength="9" value="{{ $CarModel->value or '' }}" id="model_year"  >
                      </div>
                    </div>
                  </div>
                 
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea name="description" class="english" maxlength="500" id="description" rows="4" cols="50">{{ $fetchdata->pro_desc or '' }} </textarea>
                      </div>
                      <div  class="arabic ar" >
                        <textarea class="arabic ar" name="description_ar"  maxlength="500" id="description_ar" rows="4" cols="50">{{ $fetchdata->pro_desc_ar or '' }}</textarea>
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="{{request()->id}}">
                      <input type="hidden" name="sid" value="{{request()->sid}}">
                      <input type="hidden" name="autoid" value="{{request()->autoid}}">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar arabic_left">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </form>
              <!-- one-call-form -->
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>
 $(function() {
 
$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});
 
</script>
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>
<script>
/* Action Popup */
jQuery('.cstatus').click(function(){
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id);
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-service-image') }}",
        data: {id:id},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script>
<script>
      
            $("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
  
$("#makeupartist").validate({

                  ignore: [],
                  rules: {
                
                       model_name: {
                       required: true,
                      },

                      model_name_ar: {
                       required: true,
                      },

                       model_year: {
                       required: true,
                      },
                       price: {
                       required: true,
                      },

                      service_date: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },
 
                       @if(isset($fetchdata->pro_Img)!='')  
                        stor_img: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        stor_img: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             model_name: {
          required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_MODEL_NAME'); @endphp",
                      },  
            model_name_ar: {
              required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_MODEL_NAME'); @endphp",
                      },  

                model_year: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_MODEL_YEAR')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_MODEL_YEAR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_MODEL_YEAR') }} @endif",
                      }, 
               
                  service_date: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_SERVICE_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_SERVICE_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_SERVICE_DATE') }} @endif",
                      },   

                      

                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                      },

 
                     description: {
           
                  required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
                      },

                       description_ar: {
             
                required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_SINGER_AR'); @endphp",
                      },

                  stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },   

                                                  
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  @if($mer_selected_lang_code !='en')
                    if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.image != "undefined" || valdata.image != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
          @else


             if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
 
            if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      
                    
                @endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>



<!-- Add More product images start -->
  <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->

@include('sitemerchant.includes.footer')
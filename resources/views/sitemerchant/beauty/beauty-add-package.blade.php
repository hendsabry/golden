@include('sitemerchant.includes.header') 
@php $beautybig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->serviceid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.ADDPACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADDPACKAGE') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATEPACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE') }} @endif </h5>
        @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }} </div>
              @endif
              <!-- Display Message after submition -->
              <form name="form1" id="addmanager" method="post" action="{{ route('storebeautypackageservice') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"><span class="english">@php echo lang::get('mer_en_lang.PACKEGENAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKEGENAME'); @endphp </span></label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" maxlength="60" type="text" name="title" value="{{$getservice->pro_title or ''}}" id="title" required="">
                      </div>
                      <div class="arabic">
                        <input class="arabic ar" maxlength="60" id="title_ar"  name="title_ar" value="{{$getservice->pro_title_ar or ''}}"  type="text" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PACKAGEIMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKAGEIMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="img" class="info-file" type="file">
                        @if(isset($getservice->pro_Img) && $getservice->pro_Img !='')
                        <div class="form-upload-img"><img src="{{$getservice->pro_Img or ''}}"></div>
                        @endif </div>
                    </div>
                  </div>
                </div>
                <!-- Add More product images start -->
                @php
                $GalleryCunt = $productGallery->count();
                $J=2; $k=3;
                @endphp
                
                @for($i=0;$i < $GalleryCunt;$i++)
                <div class="form_row_right common_field">
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo{{$k}}">
                      <div class="file-btn-area">
                        <div id="file_value{{$J}}" class="file-value"></div>
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      </div>
                      </label>
                      <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
                    </div>
                    @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
                    <div class="form-upload-img product_img_del"> <img src="{{ $productGallery[$i]->image or '' }}" >
                      <div class="imgpics-delet"> <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a> </div>
                    </div>
                    <span class="error file_value{{$J}}"></span> @endif
                    <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
                  </div>
                </div>
                @php  $J=$J + 1; $k=$k +1; @endphp
                @endfor
                <div id="img_upload" class="common_field"></div>
                <div class="img-upload-line common_field">
                  <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
                  <span class="error pictureformat"></span> @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp
                  <input type="hidden" id="count" name="count" value="{{$Count}}">
                </div>
                <!-- Add More product images end -->
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> </label>
                    <div class="info100">
                      <div>
                        <input class="small-sel notzero" maxlength="9" onkeypress="return isNumber(event)" type="text" name="pro_price"  value="{{$getservice->pro_price or ''}}" id="pro_price">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span> </label>
                    <div class="info100">
                      <input type="text" class="small-sel notzero" onkeypress="return isNumber(event)"  name="discount" maxlength="7" value="{{$getservice->pro_discount_percentage or ''}}" id="discount">
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_Duration'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Duration'); @endphp </span> </label>
                    <div class="info100">
                      <div>
                        <input class="small-sel notzero" onkeypress="return isNumber(event)" type="text" name="duration" maxlength="5" value="{{$getservice->service_hour or ''}}"  id="mer_duration"   required="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row ">
                <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.SELECTSERVICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SELECTSERVICE'); @endphp </span> </label>
                  <div class="info100"> @foreach($getAttr as $catvals)
                    @php  $Itemsarray = array();
                    $getProducts = Helper::getrelateditems($catvals->id,request()->itemid); 
                    if($getProducts->count() < 1) {continue; }
                    @endphp
                    <!--Loop start-->
                    <div class="save-card-line">
                      <label for="men"> @if($mer_selected_lang_code =='en')  {{$catvals->attribute_title or ''}} @else {{$catvals->attribute_title_ar or ''}} @endif </span> </label>
                    </div>
                    <div class="category_area"> @foreach($getProducts as $proitems)
                      <!--Loop -->
                      <div class="save-card-line"> @php
                        $pID = $proitems->pro_id;
                        foreach($getPkg as $valp)
                        {
                        $Itemsarray[] = $valp->item_id;
                        }
                        @endphp
                        <input type="checkbox" id="men{{$proitems->pro_id}}" @if (in_array($pID, $Itemsarray)) CHECKED @endif  name="productitems[]" value="{{$proitems->pro_id or ''}}">
                        <label for="men{{$proitems->pro_id}}"> <span class="englishd">@if($mer_selected_lang_code =='en')  {{$proitems->pro_title or ''}} @else  {{$proitems->pro_title_ar or ''}}  @endif<span class="sar">(SAR {{$proitems->pro_price or ''}})</span> </span> </label>
                      </div>
                      <!--Loop end -->
                      @endforeach </div>
                    @endforeach
                    <!--Loop End -->
                    <span for="productitems[]" generated="true" class="error"> </span> </div>
                  </label>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="{{request()->id}}">
                      <input type="hidden" name="hid" value="{{request()->hid}}">
                      <input type="hidden" name="itemid" value="{{request()->itemid}}">
                      <input type="hidden" name="serviceid" value="{{request()->serviceid}}">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                  </div>
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                </div>
                <!-- form_row -->
              </form>
            </div>
          </div>
        </div>
        <!-- global_area -->
      </div>
    </div>
    <!-- right_panel -->
  </div>
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">
$("#addmanager").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       pro_price: {
                       required: true,
                      },
                      duration: {
                       required: true,
                      },
                      "productitems[]": {
                       required: true,
                      },

                      @if(!isset($getservice->pro_Img))     
                      img: {
                      required: true,
                      accept:"png|jpe?g|gif",
                      },
                      @else
                      img: {
                      accept:"png|jpe?g|gif",
                      },
                      @endif
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
            
               required:  " {{ trans('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME') }} ",
                      },  

                title_ar: {
               required:  " {{ trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME') }} ",
                      },
                      "productitems[]": {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_SERVICE') }} @endif ",
                      },
                   pro_price: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE') }} @endif",
                      },
                       duration: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_DURATION') }} @endif",
                      },                   
                     img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    var valdata=validation.invalid;


             @if($mer_selected_lang_code !='en')
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                     if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                     
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                    if (typeof valdata.pro_price != "undefined" || valdata.pro_price != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                    if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }
if (typeof valdata.pro_price != "undefined" || valdata.pro_price != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }


@endif
                    
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script>
<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script>
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup -->
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>
<!-- Add More product images end -->
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header')
 @php if(request()->itemid==''){ $beauty_leftmenu =1; } else { $beautybig_leftmenu =1;   } @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_BRANCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_BRANCH') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_BRANCH') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <div class="one-call-form">
  {!! Form::open(array('url' => '/addbeautybranch','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'addbranch', 'name' =>'addbeautybranch' )) !!}
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                    <div class="info100">
                     @php $getShopName = Helper::getshopname(request()->hid); @endphp
                      <input type="text" id="shopname" value="{{ $getShopName->mc_name }}" name="shopname" readonly="" class="english">
                      <input type="text" id="shopname_ar" value="{{ $getShopName->mc_name_ar }}" name="shopname_ar" readonly="" class="arabic ar">
                      
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); @endphp </span> </label>
                    <div class="info100">
                      @php $getCitys = Helper::getcitylist();  @endphp
                      <select id="city_id" name="city_id" class="small-sel">
                        <option value=""> Select City </option>
                        @foreach($getCitys as $city)
                        <option value="{{$city->ci_id}}" @if(isset($getbranchinfo->city_id) && ($getbranchinfo->city_id == $city->ci_id))) SELECTED @endif>{{$city->ci_name}} {{$city->ci_name_ar}}</option>
                         @endforeach
                      </select>
                    </div>
                  </div> 
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); @endphp </span> </label>
                    <div class="info100">
                      @php $getManagers = Helper::getmanagerlist();  @endphp

                      <select id="manager" name="manager" class="small-sel">
                        <option value=""> Select  Branch Manager </option>
                        @foreach($getManagers as $gms)
                        <option value="{{$gms->mer_id}}" @if(isset($getbranchinfo->branch_manager_id) && ($getbranchinfo->branch_manager_id == $gms->mer_id)) SELECTED @endif >{{$gms->mer_fname or ''}} {{$gms->mer_lname or ''}} </option>
                       @endforeach
                      </select>
                    </div>
                  </div>  
                  <!-- form_row -->
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" id="branchname" class="english" name="branchname" maxlength="60" value="{{ $getbranchinfo->mc_name or ''}}">
                      </div>
                      <div class="arabic ar">
                        <input type="text" name="branchname_ar" maxlength="60" class="arabic ar" id="ssb_name_ar" value="{{ $getbranchinfo->mc_name_ar or '' }}">
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea class="english" cols="50" rows="4" id="description" name="description">{{ $getbranchinfo->mc_discription or '' }}</textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea cols="50" rows="4" id="description Arabic" name="description_ar" class="arabic ar">{{ $getbranchinfo->mc_discription_ar or '' }}</textarea>
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value1"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="mc_imgname" value="{{$dgfdsg->dsfds or ''}}">
                        <input type="file" required="" class="info-file" name="mc_img" id="company_logo">
                        @if(isset($getbranchinfo->mc_img) && $getbranchinfo->mc_img!='')
                        <img src="{{$getbranchinfo->mc_img or '' }}">
                        @endif
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
				  <div class="form_row common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo1">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value2"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="tnc_img" value="{{$dgfdsg->dsfds or ''}}">
                        <input type="file" required="" class="info-file" name="tnc" id="company_logo1">

                         @if(isset($getbranchinfo->terms_conditions) && $getbranchinfo->terms_conditions!='')
                      <div class="pdf_msg">Please upload only PDF file</div>
                      <a href="{{$getbranchinfo->terms_conditions or ''}}" target="_blank" class="pdf_icon"><img src="{{url('/themes/images/pdf.png')}}"> <span>{{$getbranchinfo->terms_condition_name or ''}}</span></a>
                        @endif
                      </div>
                    </div> 
                  </div>	  
                  <div class="form_row arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  <div class="form_row english">
                           <input type="hidden" name="hid" value="{{request()->hid}}">
                    <input type="hidden" name="id" value="{{request()->id}}">
                    <input type="hidden" name="itemid" value="{{request()->itemid or ''}}">                    
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <!-- form_row -->
                  <!-- form_row -->
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- <div merchant_vendor -->

<script type="text/javascript">  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },
                       manager: {
                       required: true,
                      },                 
                      branchname: {
                       required: true,
                      },
                       branchname_ar: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },
                       mc_img: {
                       required: true,
                        accept:"png|jpe?g|gif",
                      },
                       tnc: {
                         required: true,
                      accept:"pdf",
                      },                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                   
           messages: {
                  city_id: {
                  required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_SELECT_CITY'); @endphp ", 
                  },  
                  manager: {
                  required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SELECT_MANAGER'); @endphp ",
                  },  
                  branchname: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_BRANCH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_BRANCH_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ENTER_BRANCH_NAME') }} @endif",
                  },   
                  branchname_ar: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_BRANCH_NAME_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_BRANCH_NAME_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ENTER_BRANCH_NAME_AR') }} @endif ",
                  },
                  description: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_DESCRIPTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_DESCRIPTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ENTER_DESCRIPTION') }} @endif ",
                  },
                  description_ar: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_DESCRIPTION_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ENTER_DESCRIPTION_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ENTER_DESCRIPTION_AR') }} @endif ",
                  },
                  mc_img: {
                     required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SELECT_IMG')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SELECT_IMG') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SELECT_IMG') }} @endif ",
                  accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                  },     
                  tnc: {
                     required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_TERMANDCONDITION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_TERMANDCONDITION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_TERMANDCONDITION') }} @endif ",
                  accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                  },   
                   
                },
                invalidHandler: function(e, validation){
                    var valdata=validation.invalid;
                    var valdata1=validation.errorList;
                    var i;
                    var erroren=0;
                    var start=9;
                    var lang ='ar';
                    var val=0                    
                    for (i = 0; i < start ;  i++) {
                    if (typeof valdata1[i] !== 'undefined') {  
                    var data=valdata1[i].element.classList;
                    $.inArray('ar',data);
                    if($.inArray('ar',data) == -1) 
                    {
                    val=1;
                    } 
                    }                                          
                    }
                    @if($mer_selected_lang_code !='ar')
                    if(val==0)
                    {
                    $('.arabic_tab').trigger('click');  
                    }
                    else
                    {
                    $('.english_tab').trigger('click');
                    }
                    @else
                    if(val==0)
                    {
                    $('.english_tab').trigger('click');
                    }
                    else
                    {
                    $('.arabic_tab').trigger('click'); 
                    }
                    @endif  
                     },
                submitHandler: function(form) {
                    form.submit();
                }
            });
</script> 
<!-- action_popup -->
@include('sitemerchant.includes.footer')
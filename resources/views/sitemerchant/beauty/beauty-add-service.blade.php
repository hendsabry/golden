@include('sitemerchant.includes.header') 
@php $beautybig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>

      @if(request()->serviceid=='')
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_SERVICES') }}  
      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICES') }} @endif </h5>
      @else
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_SERVICES') }}  
      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_SERVICES') }} @endif </h5>

      @endif


        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->
              
              <form name="form1" id="addmanager" method="post" action="{{ route('storebeautyservice') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">



              <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.mer_service_name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_service_name'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
          <input type="text" class="english" name="service_name" maxlength="60" value="{{$getservice->pro_title or ''}}"  id="service_name" required="" >
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" name="service_name_ar" value="{{$getservice->pro_title_ar or ''}}"  maxlength="60" id="service_name_ar" required="" >
                    </div>
                  </div>
                  <!-- form_row --> 
                </div>



                <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.CATEGORY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.CATEGORY'); @endphp</span> </label>
                  <div class="info100">
                    <div>
                      @php
                      $hid = request()->hid;
                      $itemid = request()->itemid;
                      $Getcatinfo = Helper::getbeautyCat($itemid);                      
                      @endphp
                      <select class="small-sel" name="category" required="">
                          <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }}@else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>
                        @foreach($Getcatinfo as $vals)
                        <option value="{{$vals->id or ''}}" @if(isset($getservice->attribute_id) && $getservice->attribute_id== $vals->id) SELECTED @endif >  @if($mer_selected_lang_code !='en') {{$vals->attribute_title_ar or ''}} @else  {{$vals->attribute_title or ''}} @endif</option>
                      @endforeach
                      </select>
                    </div>
                   
                  </div>
                </div>
                
                </div>
                <!-- form_row -->
                
                
                <!-- form_row -->
                
                <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      </div>
                      </label>
                      <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      @if(isset($getservice->pro_Img) && $getservice->pro_Img !='')<div class="form-upload-img"><img src="{{$getservice->pro_Img or ''}}"></div> @endif
                    </div>
                  </div>
                  </div>


                   
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->


                  
                  <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_Duration'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Duration'); @endphp </span> </label>
                  <div class="info100">
                    <div class="englishs">
                      <input type="text" class="small-sel" name="duration" onkeypress="return isNumber(event)" maxlength="5" value="{{$getservice->service_hour or ''}}"  id="mer_durationlname" required="" >
                    </div>
                    
                  </div>
                  <!-- form_row --> 
                </div>
                </div>
                <!-- form_row -->
               
                
                <!-- form_row -->
                
                <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_AMOUNT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_AMOUNT'); @endphp </span> </label>
                  <div class="info100">
                    <div class="englishs">
                      <input type="text" class="small-sel notzero" name="amount"  onkeypress="return isNumber(event)"  maxlength="9" value="{{$getservice->pro_price or ''}}"  id="amount" required="" >
                    </div>
                   
                  </div>
                  </div>
                  <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                  <div class="info100">
                    <div class="englissh">
                      <input type="text" class="small-sel notzero" name="discount" maxlength="2"  onkeypress="return isNumber(event)" maxlength="7" value="{{$getservice->pro_discount_percentage or ''}}" id="discount" >
                    </div>
                    
                  </div>
                  <!-- form_row --> 
                </div>
                  <!-- form_row --> 
                </div>
                <!-- form_row -->
                
                
                <div class="form_row">
                 
                <div class="form_row_left ">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
                      <textarea name="description" class="english" id="description" rows="4" cols="50">{{$getservice->pro_desc or ''}}</textarea>
                    </div>
                    <div  class="arabic ar" >
                      <textarea class="arabic ar" name="description_ar" id="description_ar" rows="4" cols="50">{{$getservice->pro_desc_ar or ''}}</textarea>
                    </div>
                  </div>
                  <!-- form_row --> 
                  </div>
                </div>
                <!-- form_row -->
                
                
                <div class="form_row"> 
                <div class="form_row_left">
                <div class="english">


                   
                  <input type="hidden" name="id" value="{{request()->id}}">
                  <input type="hidden" name="hid" value="{{request()->hid}}">
                  <input type="hidden" name="itemid" value="{{request()->itemid}}">
                  <input type="hidden" name="serviceid" value="{{request()->serviceid}}">


                  <input type="submit" name="submit" value="Submit">
                </div>
                <!-- form_row -->
                <div class="arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                <!-- form_row -->
                </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 

<script type="text/javascript">
  
$("#addmanager").validate({
              ignore: [],
               rules: {
                  category: {
                       required: true,
                      },

                       service_name: {
                       required: true,
                      },
                      service_name_ar: {
                       required: true,
                      },                       

                       duration: {
                       required: true,
                      },

                      amount: {
                       required: true,
                      },
                    
                    
                       description: {
                       required: true,
                      },
                    @if(!isset($getservice->pro_Img))
                    stor_img: {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },
                    @else
                    stor_img: {
                    accept:"png|jpe?g|gif",
                    },
                    @endif   
                  description_ar: {
                    required: true,                    
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
              
           messages: {
             category: {
          
                 required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CATEGORY') }} @endif",
                      },  

                 service_name: {
               required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_SERVICE_NAME'); @endphp",
                      },  


          service_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_SERVICE_NAME'); @endphp",
                      },  

                 duration: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_DURATION') }} @endif",

              
                      }, 


                amount: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_AMOUNT') }} @endif",
                      },   

                

        description: {
               required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_DISCRIPTION'); @endphp",
                      },

                   description_ar: {
               required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_DISCRIPTION'); @endphp",
                      },
     

                         stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif ",
                      },   
                                             
                                        
                     
                },             
          invalidHandler: function(e, validation){ 
                    var valdata=validation.invalid;
 // category service_name service_name_ar stor_img  duration amount discount description description_ar       
 @if($mer_selected_lang_code !='en')
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                    $('.arabic_tab').trigger('click'); 
                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                    $('.arabic_tab').trigger('click'); 
                    }
                    if (typeof valdata.amount != "undefined" || valdata.amount != null) 
                    {
                    $('.arabic_tab').trigger('click');
                    }
                    if (typeof valdata.discount != "undefined" || valdata.discount != null) 
                    {
                    $('.arabic_tab').trigger('click');
                    }
                   
                  if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.arabic_tab').trigger('click');
                    }
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null)
                    {
                    $('.arabic_tab').trigger('click');
                    }

                    if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');  
                    }
                    
                      
          @else
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null)
                    {
                    $('.arabic_tab').trigger('click');
                    }

                    if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');  
                    }
                  if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.amount != "undefined" || valdata.amount != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.discount != "undefined" || valdata.discount != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    
@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
 /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

</script> 



<!-- Add More product images start -->
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->


@include('sitemerchant.includes.footer')
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_COLOR') :  trans($MER_OUR_LANGUAGE.'.MER_MANAGE_COLOR') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="public/assets/plugins/bootstrap/css/bootstrap.css" />
 <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css"/>
    <link rel="stylesheet" href="public/assets/css/theme.css" />
    <link rel="stylesheet" href="public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="public/assets/plugins/Font-Awesome/css/font-awesome.css" />
      <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif		
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
       {!! $merchantheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $merchantleftmenus !!}
       
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_COLOR'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_COLOR')}}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_COLOR')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_COLOR'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_COLOR')}}</h5>
            
        </header>
   @if (Session::has('success'))
		<div class="alert alert-success alert-dismissable">
	{{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
	{!! Session::get('success') !!}</div>
		@endif

   

   
 <div class="row">
   	
    <div class="col-lg-12 panel_marg_clr">
     <div class="panel_marg_clr ppd" style="padding: 10px;">     
    	<table  class="table table-bordered" id="dataTables-example">
              <thead>
                <tr>
                  <th style="width:10%;">{{ (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO') }}</th>
                  <th>{{ (Lang::has(Session::get('mer_lang_file').'.MER_COLOR_CODE')!= '')  ? trans(Session::get('mer_lang_file').'.MER_COLOR_CODE') : trans($MER_OUR_LANGUAGE.'.MER_COLOR_CODE') }}</th>
				  <th style="text-align:center;">{{ (Lang::has(Session::get('mer_lang_file').'.MER_COLOR_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COLOR_NAME'): trans($MER_OUR_LANGUAGE.'.MER_COLOR_NAME')}}</th>
				  <?php /*
                  <th style="text-align:center;"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_EDIT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_EDIT');} ?></th>
				  <th style="text-align:center;"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_DELETE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DELETE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DELETE');} ?></th>
               */?>
                </tr>
              </thead>
              <tbody>
              <?php $i=1;?>
                 @foreach($color_added_list as $color)
                <tr>
                  <td>{{ $i }}</td>
                
                  <td class="text-center">{!! $color->co_code!!}</td>
				  <td class="text-center" style="color:{!! $color->co_code!!};">{!! $color->co_name!!}</td>
	              <?php /*
                  <td class="text-center"><a href="{!! url('mer_edit_color').'/'.$color->co_id!!}" data-tooltip="Edit"><i class="icon icon-edit icon-2x" title="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_EDIT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_EDIT');} ?>"></i></a></td>
		          <td class="text-center"><a href="{!! url('mer_delete_color').'/'.$color->co_id!!}" data-tooltip="Delete"><i class="icon icon-trash icon-2x" title="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_DELETE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_DELETE');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_DELETE');} ?>"></i></a></td>
					*/?>
                </tr>
                <?php $i++;?>
                @endforeach	          
                
              </tbody>
            </table></div>
            <?php /**  echo $color_added_list->setPath('mer_manage_color');  **/ ?>
    </div>
   
   </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

     <!-- FOOTER -->
    {!! $merchantfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->  
    <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>  
 
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>

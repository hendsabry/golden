@include('sitemerchant.includes.header') 

@php $buffet_leftmenu =1; @endphp

<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')

  <div class="right_panel">

    <div class="inner">

      <header>

        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </h5>

        @include('sitemerchant.includes.language') </header>

      </header>

      <!-- Display Message after submition --> 

      @if (Session::has('message'))

      <div class="alert alert-info">{{ Session::get('message') }}</div>

      @endif

      <div class="error arabiclang"></div>

      <!-- Display Message after submition -->

      <div class="global_area">

        

        <div class="box">

          <form name="form1" method="post" id="addbranch" action="{{ route('store-branch') }}" enctype="multipart/form-data">

            {{ csrf_field() }}

            

              

                <div class="form_row">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_RESTURANT_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_RESTURANT_NAME'); @endphp </span> </label>

                    <div class="info100">

                      <div class="english"> {{ $shopName->mc_name or '' }} </div>

                      <div class="arabic ar"> {{ $shopName->mc_name_ar or '' }}

                        <input type="hidden" name="pid" value="{{ $pid or '' }}">

                        <input type="hidden" name="parent_id" value="{{ $parent_id }}">

                        <input type="hidden" name="sid" value="{{ $sid }}">

                      </div>

                    </div>

                  </div>

                </div>

                <div class="form_row">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>

                    <div class="info100" >

                      <div class="english">

                        <input type="text" class="english" name="mc_name" maxlength="235"  data-validation="length required" 

		                  value="{{$fetchdata->mc_name or ''}}" data-validation-length="max35">

                      </div>

                      <div class="arabic ar">

                        <input type="text" class="arabic ar" name="mc_name_ar" value="{{$fetchdata->mc_name_ar or ''}}"  maxlength="235" >

                      </div>

                    </div>

                  </div>

                   <div class="form_row_left common_field no_padding">

                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp </span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a> </label>

                    <div class="info100">

                      <input type="url" name="google_map_address"  maxlength="250"  data-validation="length required" 

                   value="{{$fetchdata->google_map_address or ''}}" data-validation-length="max100">

                    </div>

                  </div>





               

                  <span id="maperror"></span>

                  <div class="form_row_left common_field">

          <div class="lat_left">

                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LONG')!= '') {{  trans(Session::get('mer_lang_file').'.LONG') }}  @else  {{ trans($OUR_LANGUAGE.'.LONG') }} @endif </label>

                  <input type="text" class="form-control" value="{{$fetchdata->longitude or ''}}" readonly="" name="longitude" />

          </div>

          <div class="lat_right">

                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LAT')!= '') {{  trans(Session::get('mer_lang_file').'.LAT') }}  @else  {{ trans($OUR_LANGUAGE.'.LAT') }} @endif </label>

                  <input type="text" class="form-control" value="{{$fetchdata->latitude or ''}}"  readonly="" name="latitude" />

          </div>

                </div>

                <div class="form_row common_field">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); @endphp </span> </label>

                    <div class="input-file-area">

                      <label for="company_logo1">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>

                        <div class="file-value" id="file_value2"></div>

                      </div>

                      </label>

                      <input type="file" name="branchimage" id="company_logo1" class="info-file" accept="image/*" >

                    </div>

                    @if(!isset($getDb)) <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span> @endif

                     @if(isset($fetchdata->mc_img) && $fetchdata->mc_img !='')

                    <div class="form-upload-img"> <img src="{{$fetchdata->mc_img}}"> </div> @endif

                  </div>

                  <!--

                  <div class="form_row_right">

                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div" style="text-align:center;">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span></a> </label>

                    <div class="input-file-area">

                      <label for="company_logo7">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>

                        <div class="file-value" id="file_value6"></div>

                      </div>

                      </label>

                      <input accept="image/*" type="file" name="address_image" id="company_logo7" class="info-file">

                    </div>

                    @if(!isset($getDb)) <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span> @endif

                    

                    @if(isset($fetchdata->address_image) && $fetchdata->address_image !='')

                    <div class="form-upload-img"> <img src="{{ $fetchdata->address_image}}"></div> @endif

                  </div>-->







                </div>

                <div class="form_row">

                  <div class="form_row_left">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_ADDRESS'); @endphp </span> </label>

                    <div class="info100" >

                      <div class="english">

                        <input type="text" class="english" name="address" maxlength="235"  data-validation="length required" 

		                    value="{{$fetchdata->address or ''}}" data-validation-length="max35">

                      </div>

                      <div class="arabic ar">

                        <input type="text" class="arabic ar" name="address_ar" value="{{ $fetchdata->address_ar or ''}}"  maxlength="255" >

                      </div>

                    </div>

                  </div>

                  <div class="form_row_right common_field" >

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp </span> </label>

                    <div class="info100" >

                     

                        <select name="city_id" id="status" class="city_type">

                          <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif</option>

                          

               @php $getC = Helper::getCountry(); @endphp

                        @foreach($getC as $cbval)

                        

                          <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> {{$cbval->co_name or ''}}   </option>

                          

                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              

                        @foreach ($getCity as $val)

                         @php $ci_id = $val->ci_id; @endphp

                        @if($mer_selected_lang_code !='en')

                        @php $ci_name= 'ci_name_ar'; @endphp

                        @else

                         @php $ci_name= 'ci_name'; @endphp

                        @endif   

                        

                          <option value="{{ $val->ci_id }}" {{ isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''}} >{{ $val->$ci_name }}</option>

                          

                        @endforeach

                        @endforeach





            

                        </select>

                      

                    </div>

                  </div>

                </div>

                <div class="form_row ">





  <div class="form_row_left">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>

                    <div class="info100" >

                      <div class="english">

                        <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50">{{ $fetchdata->mc_discription or ''}} </textarea>

                      </div>

                      <div class="arabic ar">

                        <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50">{{ $fetchdata->mc_discription_ar or ''}}</textarea>

                      </div>

                    </div>

                  </div>









                  <div class="form_row_right common_field" >

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); @endphp </span> </label>

                    <div class="info100">

                      <select class="small-sel" name="manager"  id="manager">

                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_MANAGER') }}  

                        @else  {{ trans($MER_SELEST_MANAGER.'.MER_SELEST_MANAGER') }} @endif</option>

                         

                      @foreach ($manager as $val)

                       

                        <option value="{{ $val->mer_id }}" {{ isset($fetchdata->branch_manager_id) && $fetchdata->branch_manager_id ==$val->mer_id ? 'selected' : ''}}>{{ $val->mer_fname }}</option>

                         

                      @endforeach

 

                      </select>

                    </div>

                  </div>

                  <input type="hidden" class="english small-sel" name="Insuranceamount" maxlength="35"  data-validation="length required" value="0" data-validation-length="max35">



                  



                </div>

                <div class="form_row ">

                

                  <div class="form_row_left english">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>

                    <div class="input-file-area">

                      <label for="company_logo">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>

                        <div class="file-value" id="file_value1"></div>

                      </div>

                      </label>

                      <input type="file" name="mc_tnc" id="company_logo" class="info-file">

                    </div>

                    <div class="pdf_msg">

@if (Lang::has(Session::get('mer_lang_file').'.Please_upload_only_PDF_file')!= '') {{  trans(Session::get('mer_lang_file').'.Please_upload_only_PDF_file') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Please_upload_only_PDF_file') }} @endif

 </div>

                    @if(isset($fetchdata->terms_conditions)!='')

                    

                    <div class="pdf_icon"> <a href="{{$fetchdata->terms_conditions}}" target="_blank"><img src="{{url('/themes/images/pdf.png')}}"> </a> </div>

                    <input type="hidden" value="{{ $fetchdata->terms_condition_name or ''}}" name="tmcvalue">

                    {{ $fetchdata->terms_condition_name or ''}}

                    @endif </div>

                </div>



 <div class="form_row ">

                

                  <div class="form_row_left arabic ar">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>

                    <div class="input-file-area">

                      <label for="company_logo9">

                      <div class="file-btn-area">

                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>

                        <div class="file-value" id="file_value8"></div>

                      </div>

                      </label>

                      <input type="file" name="mc_tncar" id="company_logo9" class="info-file">

                    </div>

                    <div class="pdf_msg">

                    يرجى تحميل ملف PDF فقط

                    </div>

                    @if(isset($fetchdata->terms_conditions_ar)!='')

                    

                    <div class="pdf_icon"> <a href="{{$fetchdata->terms_conditions_ar}}" target="_blank"><img src="{{url('/themes/images/pdf.png')}}"> </a> </div>

                    <input type="hidden" value="{{ $fetchdata->terms_condition_name_ar or ''}}" name="tmcvaluear">

                    {{ $fetchdata->terms_condition_name_ar or ''}}

                    @endif </div>

                </div>



 



                

                <div class="form-btn-section english">

                  <div class="form_row_left">

                    <input type="hidden" value="{{ $fetchdata->mc_id or '' }}" name="itemid">

                    <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">

                  </div>

                </div>

                <div class="form-btn-section arabic ar">

                  <div class="form_row_left arbic_right_btn">

                    <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">

                  </div>

                </div>

              

          </form>

            </div>

        

      </div>

      <!-- global_area --> 

    </div>

  </div>

  <!-- right_panel --> 

</div>

<!-- merchant_vendor --> 

<script>

       

            $("form").data("validator").settings.ignore = "";

 </script> 

<script type="text/javascript">

  

$("#addbranch").validate({

                  ignore: [],

                  rules: {

                  city_id: {

                       required: true,

                      },



                       /*manager: {

                       required: true,

                      },*/



                       mc_name: {

                       required: true,

                      },



                      mc_name_ar: {

                       required: true,

                      },

                       address: {

                       required: true,

                      },

                       address_ar: {

                       required: false,

                      },



                       about: {

                       required: true,

                      },

                       about_ar: {

                       required: true,

                      },

                       @if(isset($fetchdata->mc_img)!='')  

                        branchimage: {

                           required:false,

                           accept:"png|jpeg|jpg|gif",        

                      },

                       @else

                        branchimage: {

                           required:true,

                           accept:"png|jpeg|jpg|gif",        

                      },

                      @endif



                      @if(isset($fetchdata->address_image)!='')  

                        address_image: {

                           required:false,

                           accept:"png|jpeg|jpg|gif",        

                      },

                       @else

                        address_image: {

                           required:false,

                           accept:"png|jpeg|jpg|gif",        

                      },

                      @endif









                      

                       @if(isset($fetchdata->terms_conditions)!='') 

                        mc_tnc: {

                           required:false,

                           accept:"pdf",

                      },

                       mc_tncar: {

                           required:false,

                           accept:"pdf",

                      },

                    @else

                         mc_tnc: {

                           required:true,

                           accept:"pdf",

                      },

                        mc_tncar: {

                           required:true,

                           accept:"pdf",

                      },

                      @endif



                     



                  },

                 highlight: function(element) {

            $(element).removeClass('error');

                },

             

           messages: {

             city_id: {

               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",

                      },  



                 /*manager: {

               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR') }} @endif",

                      },  */

                          mc_tnc: {

                                required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); @endphp",

                                accept: "@php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); @endphp",

                        },



                       mc_tncar: {

                                required: "@php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp",

                                accept: "@php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp",

                      },



                         mc_name: {

             

                   required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_BRANCH'); @endphp",

                      },   



                          mc_name_ar: {

               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_BRANCH_AR'); @endphp",

                      },   



             



                       address: {

            

               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); @endphp",

                      },

  

                       address_ar: {

               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); @endphp",

                      },

                        

                       about: {

          

                 required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); @endphp",

                      },

  

                       about_ar: {

               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); @endphp",

                      },

                  branchimage: {

                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",

                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",





                                    



                      },                                 

                      address_image: {

                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",

                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",





                                    



                      }, 

                },



                invalidHandler: function(e, validation){

                                    

                    var valdata=validation.invalid;

 console.log("invalidHandler : validation", valdata);

                @if($mer_selected_lang_code !='en')

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 

                    {

                        $('.english_tab').trigger('click'); 

                      

                    }

                     if (typeof valdata.address != "undefined" || valdata.address != null) 

                    {

                        $('.english_tab').trigger('click');

                           



                    }

                    if (typeof valdata.about != "undefined" || valdata.about != null) 

                    {

                        $('.english_tab').trigger('click');

                           



                    }

                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }

                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }

                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 

                    {

                        $('.english_tab').trigger('click'); 

                    



                    }

                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                  if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }



                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }



                    if (typeof valdata.mc_tncar != "undefined" || valdata.mc_tncar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }





                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

          @else





             if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }



                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

                     if (typeof valdata.mc_tncar != "undefined" || valdata.mc_tncar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                     

                    }

 

            if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 

                    {

                        $('.english_tab').trigger('click'); 

                      

                    }

                     if (typeof valdata.description != "undefined" || valdata.description != null) 

                    {

                        $('.english_tab').trigger('click');

                           



                    }

                      if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }



                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 

                    {

                        $('.english_tab').trigger('click'); 

                     



                    }

                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 

                    {

                        $('.english_tab').trigger('click'); 

                    



                    }

                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                  



@endif



                    },



                submitHandler: function(form) {



                  var mapAdd = jQuery('input[name=google_map_address]').val();

if(mapAdd !='')

{

 var long = jQuery('input[name=longitude]').val();

    var lat  =  jQuery('input[name=latitude]').val();

  if(long =='' && lat=='')

  {

  var allOk = 0;



  }

  else

  {

   var allOk = 1; 

  }

}

else

{

var allOk = 1;

}



if(allOk == 1)

{

  form.submit();

}

else

{

  $('#maperror').html("<span class='error'>@if (Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= '') {{  trans(Session::get('mer_lang_file').'.get_Lat_Long_issue') }} @else  {{ trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue') }} @endif</span>");

}

                    

                }

            });



 



</script> 

@include('sitemerchant.includes.footer')
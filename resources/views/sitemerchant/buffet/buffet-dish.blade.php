 @inject('data','App\Help')
@include('sitemerchant.includes.header') 
@php $buffet_leftmenu =1; @endphp
<div class="merchant_vendor"> 
@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_LIST_DISH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LIST_DISH') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LIST_DISH') }} @endif </h5>
        <div class="add"><a href="{{ route('buffet-add-dish',['id' =>$parent_id,'sid'=>$sid,'itemid'=>$itemid]) }} ">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_NEW')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_NEW') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_NEW') }} @endif</a></div>
          @php $statuss = request()->status;  @endphp

          </div> <!-- service_listingrow -->
        <form name="filter" id="filter" method="get" action="{{ route('buffet-dish',['id'=>$parent_id,'sid' =>$sid,'itemid'=>$itemid]) }} ">
        <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left two-srarch-box">
          <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems ">
            <select name="category" id="category" class="city_type">
              <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>
             @php $menu_name='attribute_title'@endphp
                 @if($mer_selected_lang_code !='en')
            @php $menu_name= 'attribute_title_'.$mer_selected_lang_code; @endphp
            @endif
                @foreach($menucat as $val)
                <option value="{{ $val->id }}" @if($category==$val->id) selected="selected" @endif>{{ $val->$menu_name }}</option>
                @endforeach  
               </select>

            <select name="status" id="status" class="city_type">
                <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
                <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
                <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
              </select>
         


            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
        </div>
        <div class="search_box">
        <div class="search_filter">&nbsp;</div>        
        <div class="filter_right">
       
          <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{ $search or '' }}" />
          <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
        </div>
        </div>
      </div>
      {!! Form::close() !!}
      <!-- filter_area --> 
    <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12"> 
            
            
            <div class="table_wrap">
              <div class="panel-body panel panel-default">
                @if($containerlist->count() < 1) 
                <div class="no-record-area">
                   @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif
              </div>
              @else
               
                <div class="table">
                  <div class="tr">
                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CATEGORY') }} @endif</div>

                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_DISH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISH_NAME') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISH_NAME') }} @endif</div>

                   <!--  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRICE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PRICE') }} @endif</div> -->

                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif</div> 
               <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif</div>

                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                  </div>

                    @php 
                    $mucucat='menu_name';
                    $dish_name='pro_title';
                    @endphp
                       @if($mer_selected_lang_code !='en')
                     @php 
                       $mucucat= 'menu_name_'.$mer_selected_lang_code;
                       $dish_name= 'pro_title_'.$mer_selected_lang_code;
                   @endphp
                   @endif
                 
                  @foreach($containerlist as $val) 
                  
                  @php
                  $mucucat = $data->dispAttributeName($val->attribute_id);
                  @endphp
                   @php
                    if($val->pro_status==1){
                    $status = trans(Session::get('mer_lang_file').'.MER_ACTIVE');
                     $dstatus = 'Active';
                    $status_class = 'status_active';
                    } else {
                    $status = trans(Session::get('mer_lang_file').'.MER_INACTIVE');
                    $dstatus = 'Inactive';
                    $status_class = 'status_deactive';
                    }
                    @endphp 
                  <div class="tr">
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CATEGORY') }} @endif">{{ $mucucat or '' }}</div>
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_DISH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISH_NAME') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISH_NAME') }} @endif">{{ $val->$dish_name }}</div>
  
                    @if($val->pro_Img !='')
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif">
                    <img src="{{ $val->pro_Img }}" width="150" alt=""> </div>
                    @else
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif">No image</div>
                    @endif
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif">{{ Carbon\Carbon::parse($val->updated_at)->format('F j, Y')}}</div>
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif"> <span class="<?php echo $status_class;?>  status_active2 cstatus" data-status="<?php echo $dstatus;?>"data-id="{{ $val->pro_id }}">{{ $status }}</span></div>
                    <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif"><a href="{{ route('buffet-add-dish',['id' => $parent_id,'sid'=>$sid,'itemid' =>$itemid,'autoid'=>$val->pro_id]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EDIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EDIT') }} @endif</a></div>
                  </div>
                  @endforeach 
                  
                  </div>
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
                @endif
              </div>
              {{ $containerlist->links() }} </div>
            <!-- table_wrap --> 
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
    <!-- inner --> 
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =$(this).data("status");
 var id =$(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

$('.status_yes').click(function(){
   var id =$(this).attr("data-id");
    var status1 =$(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

    jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'buffetdish'},
        success: function(data) {
         
            if(data==1){

               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
            $('form[name=test]').submit();
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script>

@include('sitemerchant.includes.footer')
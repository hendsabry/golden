@include('sitemerchant.includes.header') 
@php $buffet_leftmenu =1; @endphp
<div class="merchant_vendor cont_add">
@include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
    <header>
     @if($autoid=='')
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CONTAINER') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CONTAINER') }} @endif </h5>
            @else
            <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EDIT_CONTAINER') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EDIT_CONTAINER') }} @endif </h5>
            @endif
         </header>
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box notab">
            <!-- Display Message after submition -->
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}  </div>
            @endif
            <!-- Display Message after submition -->

              <form name="form1" id="add-container" method="post" action="{{ route('store-buffet-container') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">      
                  @if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Name') }}  
                 @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Name') }} @endif
                   </label>
                   <div class="info100">
                      
                      <select class="small-sel" name="title"  id="title">

                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER') }}  
                        @else  {{ trans($MER_SELEST_CONTAINER.'.MER_SELEST_CONTAINER') }} @endif</option>
                        @foreach($letter as $val)
                        
                        <option value="{{$val}}" {{isset($fetchfirstdata->option_title) && $fetchfirstdata->option_title==$val ? 'selected' : ''}}>{{ $val }}</option>
                        @endforeach 
                      </select>
                    </div>  
                    </div>
                </div>
               
                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
                      @if (Lang::has(Session::get('mer_lang_file').'.MER_No_People')!= '') {{  trans(Session::get('mer_lang_file').'.MER_No_People') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_No_People') }} @endif
                  </label>
                  <div class="info100">
                   <div>
                    <input class="xs_small notzero" maxlength="5" onkeypress="return isNumber(event)" type="text" name="no_people" value="{{ $fetchfirstdata->no_person or '' }}" id="no_people" required="">
                    <input type="hidden" name="autoid" name="{{ $autoid }}">
                    </div>
                  </div>
                  </div>
                </div>
              
                <div class="form_row" style="display: none;">
                  <div class="form_row_left">
                  <label class="form_label">

                   <span class="english">@php echo lang::get('mer_en_lang.MER_About'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_About'); @endphp </span> 
            </label>
                  <div class="info100">
                   <div class="english"> 
                    <textarea  maxlength="500" name="about" id="about" rows="4" cols="50">This is tested </textarea>
                     </div>
                    <div class="arabic"> 
                    <textarea class="arabic ar" maxlength="500" name="about_ar" id="about_ar " rows="4" cols="50">This is tested</textarea>
                    <input type="hidden" value="{{ $parent_id }}" name="parent_id">
                    <input type="hidden" value="{{ $sid }}" name="sid">
                    <input type="hidden" value="{{ $itemid }}" name="itemid">
                     <input type="hidden" value="{{ $autoid }}" name="autoid">
                    </div>
                    </div>
                    </div>
                 </div>
                <!-- form_row -->
                <div class="form_row common_field">
                  <div class="form_row_left">
                  <label class="form_label">
                    @if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Image') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Image') }} @endif
                     
          </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn">
                 @if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBMIT') }}  @else  {{ trans($OUR_LANGUAGE.'.MER_SUBMIT') }} @endif
 </div>
         
                      </div>
                      </label>
                      <input id="company_logo" name="img" class="info-file" type="file" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" required="" value="">
                    </div>
                    @if(isset($fetchfirstdata->image) && $fetchfirstdata->image!='')
                    <div class="form-upload-img">
                       <img src="{{ $fetchfirstdata->image or '' }}">
                     </div>
                     @endif
                  </div>
                  </div>
                </div>
                <!-- form_row -->


                <div class="form_row">
                <div class="form_row_left">
                <div class="english">
                  <input type="submit" name="submit" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBMIT') }}  @else  {{ trans($OUR_LANGUAGE.'.MER_SUBMIT') }} @endif">
                  </div>
                  </div>
                </div>

                <!-- form_row -->
              </form>

          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       no_people: {
                       required: true,
                      },
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       @if(isset($fetchfirstdata->image)!='')
                       img: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      },
                      @else
                        img: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
                      @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
             required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_SELECT_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_SELECT_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CONTAINER_SELECT_NAME') }} @endif",
                      },  
   
                
                       no_people: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_No_VALIDATION_PEOPLE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_No_VALIDATION_PEOPLE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_No_VALIDATION_PEOPLE') }} @endif",
                      }, 

                    about: {
            required:  " {{ trans('mer_en_lang.MER_VALIDATION_ABOUT') }} ",
                      }, 
 
                    about_ar: {
               required:  " {{ trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR') }} ",
                      },
                     img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                @if($mer_selected_lang_code !='en')
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                  
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
 if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                  
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }






@endif
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
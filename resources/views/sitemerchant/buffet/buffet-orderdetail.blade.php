@include('sitemerchant.includes.header')  
@php $buffet_leftmenu =1; @endphp
<!--start merachant-->
<div class="merchant_vendor"> @php
  $id = request()->id;
  $sid = request()->sid;
  $opid = request()->opid;
  $cusid = request()->cusid;
  $oid = request()->oid;
  $hid = request()->hid;
  $itemid=request()->itemid;
  $getCustomer = Helper::getuserinfo($cusid); 
  $shipaddress       = Helper::getgenralinfo($oid);
$cityrecord=Helper::getcity($shipaddress->shipping_city);
  //$getorderedproduct=Helper::getorderedproduct($opid);
  
  $getorderedproductdate=Helper::getorderedproduct($oid);
  $getorderedproduct=Helper::getfoodorderedproducttotal($oid,'buffet');
  
  @endphp
  <!--left panel-->
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif <a href="{{url('/')}}/buffet-orders/{{$id}}/{{$sid}}/{{$itemid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a> </h5>
      </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} else { echo 'N/A'; }@endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  @php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){ @endphp
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} @endphp <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a></div>
                  </div>
                  @php } @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" > @php 
                      $ordertime=strtotime($getorderedproductdate->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      @endphp </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
				   @php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                  </div>
				    @php } @endphp
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                    <div class="info100"> @php if(isset($shipaddress->shipping_id) && $shipaddress->shipping_id!='')
                      { 
                      $getName = Helper::getShippingMethodName($shipaddress->shipping_id);
                      echo $getName;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100"> @php if(isset($shipaddress->shipping_charge) && $shipaddress->shipping_charge!='' && $shipaddress->shipping_charge!='0')
                      {
                      echo $shipaddress->shipping_charge;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                    <!-- box -->
                  </div>
                 @php $getsearchedproduct = Helper::searchorderedDetails($oid);
 if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ @endphp
                    <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONDATE') }} @endif</label>
                    <div class="info100">@php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                      echo date('d M Y', strtotime($getsearchedproduct->occasion_date));
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                  @php } @endphp
                </div>
 


            @if($hallName!='')
            <div class="form_row">
            <div class="noneditbox">
            <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.HALL_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.HALL_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.HALL_NAME') }} @endif</label>
            <div class="info100">{{$hallName}}</div>
            <!-- box -->
            </div>

            <div class="noneditbox box-right">
            <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.HALL_LOCATION')!= '') {{  trans(Session::get('mer_lang_file').'.HALL_LOCATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.HALL_LOCATION') }} @endif</label>
            <div class="info100">{{$hallLocation}} </div>
            </div>

            </div>
            @endif

              </div>
              <!-- hall-od-top -->
              @php if(count($getbuffetfood_item)>0){ @endphp
              <div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.PRODUCT_INFORMATION')!= '') {{  trans(Session::get('mer_lang_file').'.PRODUCT_INFORMATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PRODUCT_INFORMATION') }} @endif</div>
              @php $k=0;  $basetotal = 0; @endphp
              @php $i=1; 

              @endphp
              @foreach($getbuffetfood_item as $bodymeasurement)
              @php 
             
              $basetotal            = ($basetotal+$bodymeasurement->price);
               $pid=$bodymeasurement->external_food_dish_id;
              $mid=$bodymeasurement->external_food_menu_id;
              $bid=$bodymeasurement->category_id;
              $conid=$bodymeasurement->container_id;
              $getorderedproducttbl=Helper::getorderedfromproducttbl($pid);
              $productproductimage=Helper::getproductimage($pid);
              
              $getcontainerinformation=Helper::getcontainerinfo($conid);
              
              //print_r($getcontainerinformation); die;
              $externalfoodmainmenu=Helper::getexternalfoodmainmenu($mid,$bid);
              
              if(Session::get('mer_lang_code')!='en') {  $ci_name='attribute_title_ar'; } else{ $ci_name='attribute_title';}
              if(Session::get('mer_lang_code')!='en') {  $con_name='option_title_ar'; } else{ $con_name='option_title';}
              @endphp
              <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                <div class="style_area">
                  <div class="style_box_type">
                    <div class="sts_box">
                      <div class="style_left">
                        <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_DISH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISH_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISH_NAME') }} @endif</div>
                        <div class="style_label_text"> {{$getorderedproducttbl or ''}} </div>
                      </div>
                     
                      <div class="style_left">
                        <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Dish_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Dish_Image') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Dish_Image') }} @endif</div>
                        <div class="style_label_text"> @if($productproductimage->pro_Img) <img src="{{ $productproductimage->pro_Img }}" alt="" width="150"> @else
                          No image
                          
                          @endif</div>
                      </div>
                       <div class="style_left">
                        <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif</div>
                        <div class="style_label_text">SAR {{number_format($bodymeasurement->price,2)}} </div>
                      </div>
                    </div>
                    <div class="sts_box">
                      <div class="style_left">
                        <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.CONTAINER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CONTAINER') }} @endif</div>
                        <div class="style_label_text"> {{$getcontainerinformation->$con_name or '' }} </div>
                      </div>
                      <div class="style_right">
                        <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_QUANTITY') }} @endif</div>
                        <div class="style_label_text">{{$bodymeasurement->quantity or ''}}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @php $k++; $i++; @endphp
              
              @endforeach
              <div class="merchant-order-total-area">
                <div class="merchant-order-total-line"> {{ (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: &nbsp;
                  @php $totalvatprice=$basetotal;
                     $vatamount = Helper::calculatevat($shipaddress->order_id,$totalvatprice);
                     echo 'SAR '.number_format(($vatamount),2);
                @endphp
                   </div>
                <!-- merchant-order-total-line -->
                <div class="merchant-order-total-line"> {{ (Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: &nbsp;
                  <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$vatamount),2);
				 } ?>
                </div>
                <!-- merchant-order-total-line -->
              </div>
              @php } @endphp </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
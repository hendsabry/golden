@include('sitemerchant.includes.header')
@php $singer_leftmenu =1; @endphp
 
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
    
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CATEGORY') }} @endif </h5>
          @include('sitemerchant.includes.language')
      </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
           @php $cid = request()->cid; $id = request()->id; @endphp

              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 

              <div class="one-call-form">
                <form name="form1"  id ="menucategory" method="post" action="{{ route('recption-updatecategory') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form_row">
                     <label class="form_label">Category Name</label> 
                    <div class="info100">
                      <input type="text" value="{{$getAttr->attribute_title or ''}}" class="english" name="category_name" maxlength="90" id="category_name" required="" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_CATEGORY_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CATEGORY_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CATEGORY_NAME') }} @endif">
                      @if(!empty($get_active_lang))
                      @foreach($get_active_lang as $get_lang)
                      <?php 
                                        $get_lang_code = $get_lang->lang_code;
                                        $get_lang_name = $get_lang->lang_name;
                                ?>
                                
                      <input class="arabic ar" id="category_{{ $get_lang_code}}" maxlength="90"  name="category_name_{{ $get_lang_code }}" placeholder="Enter Category Name In  {{ $get_lang_name }}"  value="{{$getAttr->attribute_title_ar or ''}}"  required=""  type="text" >
                      <div id="title_{{ $get_lang_code }}_error_msg"  style="color:#F00;font-weight:800"  > @if ($errors->has('Hotel_Name_'.$get_lang_code.'')) {{ $errors->first('Hotel_Name_'.$get_lang_code.'') }}@endif </div>
                      @endforeach
                      @endif </div>
                  </div>
                  
                   
                   <div class="form_row">
                     <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CATEGORY_DESCRIPTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CATEGORY_DESCRIPTION') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CATEGORY_DESCRIPTION') }} @endif</label>
                    <div class="info100">

                      <textarea class="english" name="category_description">{{$getAttr->attribute_description or ''}}</textarea>
                   
                      @if(!empty($get_active_lang))
                      @foreach($get_active_lang as $get_lang)
                      <?php 
                                        $get_lang_code = $get_lang->lang_code;
                                        $get_lang_name = $get_lang->lang_name;
                                ?>

                    <textarea class="arabic ar" name="category_description_ar">{{$getAttr->attribute_description_ar or ''}}</textarea>

 
                      <div id="title_{{ $get_lang_code }}_error_msg"  style="color:#F00;font-weight:800"  > @if ($errors->has('Hotel_Name_'.$get_lang_code.'')) {{ $errors->first('Hotel_Name_'.$get_lang_code.'') }}@endif </div>
                      @endforeach
                      @endif </div>
                  </div>
                  
                  
                   
                  
                  <div class="form_row">
      
                  <input type="hidden" name="id" value="{{ $id or ''}}">
                <input type="hidden" name="cid" value="{{ $cid or ''}}">
                  <input type="submit" name="submit" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBMIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SUBMIT') }} @endif">
                  </div>
                  
                </form>
              </div>
              
              
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
          category_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CATEGORY_NAME') }} @endif",
                      }, 
          category_name_ar: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CAT_ARABIC_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CAT_ARABIC_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CAT_ARABIC_AR') }} @endif",
                      },    
                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    
                  
                    else if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')

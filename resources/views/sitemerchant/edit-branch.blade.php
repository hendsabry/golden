@include('sitemerchant.includes.header') 
@php $hotel_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_BRANCH_EDIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BRANCH_EDIT') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BRANCH_EDIT') }} @endif</h5>
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
                <form name="form1" method="post"   id="editbranch" action="{{ route('updatebranch') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form_row ">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HOTEL_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HOTEL_NAME'); @endphp </span> </label>
                    <div class="info100"> @php                
                      $par_id = $alldata->parent_id;
                      $SecondCats=Helper::getparentCat($par_id);
                      $Hname = $SecondCats->mc_name;
                      $Hnames = $SecondCats->mc_name_ar;
                      @endphp
                      <input type="text" class="english" readonly="" name="hotel1" value="{{ $Hname or '' }}" id="hotal">
                      <input type="text" class="arabic ar" readonly="" name="hotel1" value="{{ $Hnames or '' }}" id="hotal">
                      <input type="hidden" name="hotel" value="{{ $alldata->parent_id }}" id="hotal">
                    </div>
                    </div>
                    
                    <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); @endphp </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="city_id"  id="city_id">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_CITY') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY') }} @endif</option>
            

                     @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;">{{$cbval->co_name}}</option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                        @php $ci_name='ci_name'@endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_'.$mer_selected_lang_code; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" {{ isset($val->ci_id) && $allid->city_id ==$val->ci_id ? 'selected' : ''}}>{{ $val->$ci_name }}</option>
                        @endforeach
                        @endforeach
                    



                    
                      </select>
                    </div>
                    </div>
                    
                  </div>
                  <!-- form_row -->

                  <div class="form_row">



 <div class="form_row_left">
                        <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>
                        <div class="info100"> 

                     
                            <input maxlength="50" type="text" name="mc_name" class="english" value="{{ $allid->mc_name }}" id="hotal_name" >
                      
                      
                            <input id="ssb_name_ar" class="arabic ar" maxlength="50" value="{{ $allid->mc_name_ar }}"  name="mc_name_ar"   type="text" onChange="check();">
                      
                          <div id="title_ar_error_msg"  style="color:#F00;font-weight:800"  > @if ($errors->has('Hotel_Name_ar')) {{ $errors->first('Hotel_Name_ar') }}@endif </div>
                        </div>
                    </div>


                  <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); @endphp </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="manager" id="manager">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_MANAGER') }}  
                        @else  {{ trans($MER_SELEST_MANAGER.'.MER_SELEST_MANAGER') }} @endif</option>
                        
                      @foreach ($manager as $val)
                      
                        <option value="{{ $val->mer_id }}" {{ isset($val->mer_id) && $allid->branch_manager_id ==$val->mer_id ? 'selected' : ''}}>{{ $val->mer_fname }}</option>
                        
                      @endforeach

                    
                      </select>
                    </div>
                    </div>
                    
                   
                    
                  </div>
                  <!-- form_row -->

                  <div class="form_row">
                  <div class="form_row_left"> 
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESS'); @endphp </span> </label>
                    <div class="info100">
                          <div class="english">
                            <input maxlength="150" type="text" name="address" class="english" value="{{ $allid->address }}" id="hotal_name" >
                          </div>
                          <div  class="arabic ar" >
                            <input id="ssb_name_ar" class="arabic ar" maxlength="150" value="{{ $allid->address_ar }}"  name="address_ar"   type="text" onChange="check();">
                          </div>
                          <div id="title_ar_error_msg"  style="color:#F00;font-weight:800"  > @if ($errors->has('Hotel_Name_ar')) {{ $errors->first('Hotel_Name_ar') }}@endif </div>
                        </div>
                    </div>
                    <!--div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Insuranceamount'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Insuranceamount'); @endphp </span> </label>
                    <div class="ensglish">
                      <input type="text" class="small-sel" name="Insuranceamount"  maxlength="15"   data-validation="length required" 
                    data-validation-error-msg="يرجى إدخال اسم القاعة" value="{{$allid->insurance_amount or ''}}" data-validation-length="max35">
                    </div>
                    @if($errors->has('Insuranceamount')) <span class="error"> {{ $errors->first('Insuranceamount') }} </span> @endif 
                    </div-->
                    </div>
                  <!-- form_row -->  
                                   
                  <div class="form_row common_field">
                    
                  <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label posrel"> 
                    <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp</span> 
                    <span class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp </span> 
                    <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a>
                    </label>
                    <div class="info100">
                      <input type="url" name="google_map_address"  data-validation="length required" 
     data-validation-error-msg="Please enter google map address"  value="{{$allid->google_map_address or ''}}" data-validation-length="max100">
                    </div>
                    </div>



 <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LONG')!= '') {{  trans(Session::get('mer_lang_file').'.LONG') }}  @else  {{ trans($OUR_LANGUAGE.'.LONG') }} @endif </label>
                  <input type="text" class="form-control" value="{{$allid->longitude or ''}}" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LAT')!= '') {{  trans(Session::get('mer_lang_file').'.LAT') }}  @else  {{ trans($OUR_LANGUAGE.'.LAT') }} @endif </label>
                  <input type="text" class="form-control" value="{{$allid->latitude or ''}}"  readonly="" name="latitude" />
          </div>
                </div>


                    
                  </div>
                  
                  <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); @endphp </span> </label>
                    
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      </div>
                      </label>
                      <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                      <input id="company_logo" name="mc_img" class="info-file" type="file">
                      <input type="hidden" name="updatemc_img" value="{{ $allid->mc_img }}">
                      <input type="hidden" name="id" value="{{ $id }}">
                      <div class="form-upload-img">@if($allid->mc_img !='')
                       <img src="{{ $allid->mc_img or '' }}" width="150" height="150"> 
                       @endif</div>
                    </div>
                    
                    </div>
                   <!--
                   <div class="form_row_right common_field">
                   <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="address_image_tooltip"><span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span></a>                   	
                   </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="hall_addressimg" id="company_logo1" class="info-file">
                    </div>
                    <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                    <div class="form-upload-img">@if(isset($allid->address_image) && $allid->address_image !='') <img src="{{$allid->address_image}}" width="150" height="150"> @endif</div>
                    @if($errors->has('hall_addressimg')) <span class="error"> {{ $errors->first('hall_addressimg') }} </span> @endif
                    <input type="hidden" name="Address_Img" value="{{$allid->address_image}}">
                    
                   </div>-->
                    
                  </div>
                  <!-- form_row -->
                  </div>
                  <!-- form_row -->                  
                                                      
                  <div class="form_row english">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.TERMSANDCONDITIONS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.TERMSANDCONDITIONS'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo2">
                        <div class="file-btn-area">
                          <div id="file_value3" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="updatemc_tnc" value="{{ $allid->terms_conditions }}">
                        <input id="company_logo2" name="mc_tnc" class="info-file" type="file"  value="">
                      </div>
                      <div class="pdf_msg"> 
                        
                        @php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp
                        
                        </div>
                      @if($allid->terms_conditions!='') 
                      
                      <a href="{{$allid->terms_conditions}}" target="_blank" class="pdf_icon"><img src="{{url('/themes/images/pdf.png')}}"> <span>{{$allid->terms_condition_name or ''}}</span></a>
 
                       @endif 
          <input type="hidden" name="termsconditionname" value="{{$allid->terms_condition_name or ''}}">
                    </div>
                      </div>
                  </div>
                  <!-- form_row -->

                          



   <div class="form_row arabic ar">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.TERMSANDCONDITIONS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.TERMSANDCONDITIONS'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo11">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value10"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="updatemc_tnc_ar" value="{{ $allid->terms_conditions_ar }}">
                        <input id="company_logo11" name="mc_tnc_ar" class="info-file" type="file"  value="">
                      </div>
                      <div class="pdf_msg"> 
                        
                        @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp
                        
                        </div>
                      @if($allid->terms_conditions_ar!='') 
                      
                      <a href="{{$allid->terms_conditions_ar}}" target="_blank" class="pdf_icon"><img src="{{url('/themes/images/pdf.png')}}"> <span>{{$allid->terms_condition_name_ar or ''}}</span></a>
 
                       @endif 
          <input type="hidden" name="termsconditionname_ar" value="{{$allid->terms_condition_name_ar or ''}}">
                    </div>
                      </div>
                  </div>
                  <!-- form_row -->











                  <div class="form_row"> 
                  
                  @php 
                    if(preg_match('/listbranch/',$_SERVER['HTTP_REFERER'])){
                    $previouspage =1;
                    } else {
                    $previouspage =0;  
                    }
                    
                    @endphp
                    <input type="hidden" name="redirctto" value="{{$previouspage}}">
                    <div class="form_row_left">
                    <div class="english">
                      <input type="submit"   name="submit" value="Update">
                    </div>
                    <div class="arabic ar">
                      <input   type="submit" name="submit" value="تحديث">
                    </div>
                    </div>
                    
                    
                    
                  </div>
                  <!-- form_row -->
                  
                </form>
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>       
$("form").data("validator").settings.ignore = "";
 </script> 
 
<script type="text/javascript">
  
$("#editbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },

                       manager: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                      mc_img: {
                           accept:"png|jpe?g|gif",
                      },

                       hall_addressimg: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },
                   mc_tnc: {
                           accept:"pdf",
                      },
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             city_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",
                      },  
 
                 manager: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER') }} @endif",
                      },  

                         mc_name: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_BRANCH'); @endphp",
                      },   

                          mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_BRANCH_AR'); @endphp",
                      },   


                       description: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ADDRESS') }} @endif ",
                      },

                       description_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },
                        mc_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },

                       hall_addressimg: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },                         
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

@if($mer_selected_lang_code !='en')

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
  if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                   

@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 

@include('sitemerchant.includes.footer')
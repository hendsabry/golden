@include('sitemerchant.includes.header') 
@php $SkinTeethbig_leftmenu =1; @endphp

@php
$getCloseTime = Helper::closetiming(request()->autoid);
$getOpenTime = Helper::opentiming(request()->autoid);
@endphp



<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.DRLIST')!= '') {{  trans(Session::get('mer_lang_file').'.DRLIST') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.DRLIST') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      </header>
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <form name="form1" method="post" id="addbranch" action="{{ route('store-doctor') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.SELECT_CATEGORY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SELECT_CATEGORY'); @endphp </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="category" id="category">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>
                        
                          @php $menu_name='Dermatology'@endphp
                         @if($mer_selected_lang_code !='en')
                         @php $menu_name= 'طب الأمراض الجلدية'; @endphp
                         @endif

                          @php $menu_name1='Dental Clinic'@endphp
                         @if($mer_selected_lang_code !='en')
                         @php $menu_name1= 'عيادة اسنان'; @endphp
                         @endif

                        <option value="26" {{ isset($fetchdata->attribute_id) && $fetchdata->attribute_id =='26' ? 'selected' : ''}}>{{$menu_name}}</option>
                        <option value="27" {{ isset($fetchdata->attribute_id) && $fetchdata->attribute_id =='27' ? 'selected' : ''}}>{{$menu_name1 }}</option>
                      </select>
                    </div>
                  </div>
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_NAME'); @endphp </span> </label>
                    <div class="info100" >
                      <div class="english">
                        <input type="text" class="english" name="mc_name" maxlength="235"  data-validation="length required" 
		                  value="{{$fetchdata->pro_title or ''}}" data-validation-length="max35">
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="arabic ar" name="mc_name_ar" value="{{$fetchdata->pro_title_ar or ''}}"  maxlength="235" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_IMAGE'); @endphp </span> </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="branchimage" id="company_logo1" class="info-file">
                    </div>
                    <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                    @if(isset($fetchdata->pro_Img) && $fetchdata->pro_Img !='')
                    <div class="form-upload-img"> <img src="{{ $fetchdata->pro_Img }}" > </div>
                    @endif
                    @if(!isset($getDb)) <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span> @endif
                    @if(isset($fetchdata->mc_img) && $fetchdata->mc_img !='')
                    <div class="form-upload-img"><img src="{{$fetchdata->mc_img}}" width="150" height="150"></div>
                    @endif </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Consulation_fees'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Consulation_fees'); @endphp </span> </label>
                    <div class="info100" >
                      <div class="engslish">
                        <input type="text" class="englssh xs_small notzero" name="consulation_fees" onkeypress="return isNumber(event)" id="consulation_fees" maxlength="10" required" 
		                    value="{{$fetchdata->pro_price or ''}}">
                      </div>
                    </div>
                  </div>

                  <div class="form_row_left common_field">
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.WORKING_HOURS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.WORKING_HOURS'); @endphp </span> </label>
                    <div class="form_opening">
                      <?php
$getCloseTime = Helper::closetiming(request()->autoid);
  $getOpenTime = Helper::opentiming(request()->autoid);
  $closetime =0;
                      ?>
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.FROM'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.FROM'); @endphp </span> </label>
                      <div class="info100" >
                        <div class="englishs">
                          <div class="small_time ">
                            <select name="opening_time" id="opening_time" >
                              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>

                              <option <?php if($getOpenTime =='12:00 AM'){ echo "selected"; }    ?> value="12:00 AM"{{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:00 AM' ? 'selected' : ''}} <?php if($getOpenTime=='12:00 AM'){  $check=1;   }else{    $check=0; echo "disabled='disabled'";} ?> > 12:00 AM</option>
                              <option value="00:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='00:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='00:30 AM' || $check==1)){  $check=1; if($getCloseTime=='00:30 AM'){  echo "disabled='disabled'"; $closetime =1;    }  }else{    $check=0; echo "disabled='disabled'";} ?>> 00.30 AM</option>
                              <option value="1.00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1.00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='1.00 AM' || $check==1)){
                                $check=1;  if($getCloseTime=='1.00 AM'){  echo "disabled='disabled'"; $closetime =1;    }
                                 }else{    $check=0; echo "disabled='disabled'";} ?>> 1:00 AM</option>
                              <option value="1:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='1:30 AM' || $check==1)){  $check=1;  if($getCloseTime=='1:30 AM'){  echo "disabled='disabled'"; $closetime =1;    }    }else{    $check=0; echo "disabled='disabled'";} ?>> 1:30 AM</option>
                              <option value="2:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='2:00 AM' || $check==1)){  $check=1;   if($getCloseTime=='2:00 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>> 2:00 AM</option>
                              <option value="2:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='2:30 AM' || $check==1)){  $check=1;  if($getCloseTime=='2:30 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>> 2:30 AM</option>
                              <option value="3:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='3:00 AM' || $check==1)){  $check=1; if($getCloseTime=='3:00 AM'){  echo "disabled='disabled'"; $closetime =1;  }  }else{    $check=0; echo "disabled='disabled'";} ?>> 3:00 AM</option>
                              <option value="3.30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3.30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='3.30 AM' || $check==1)){  $check=1; if($getCloseTime=='3.30 AM'){  echo "disabled='disabled'"; $closetime =1;  }  }else{    $check=0; echo "disabled='disabled'";} ?>> 3:30 AM</option>
                              <option value="4.00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4.00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='4.00 AM' || $check==1)){  $check=1;  if($getCloseTime=='4.00 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>> 4:00 AM</option>
                              <option value="4:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='4:30 AM' || $check==1)){  $check=1;  if($getCloseTime=='4:30 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>> 4:30 AM</option>
                              <option value="5:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='5:00 AM' || $check==1)){  $check=1; if($getCloseTime=='5:00 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 5:00 AM</option>
                              <option value="5:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='5:30 AM' || $check==1)){  $check=1;  if($getCloseTime=='5:30 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>>5:30 AM</option>
                              <option value="6:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='6:00 AM' || $check==1)){  $check=1;  if($getCloseTime=='6:00 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>> 6:00 AM</option>
                              <option value="6:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='6:30 AM' || $check==1)){  $check=1;  if($getCloseTime=='6:30 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>> 6:30 AM</option>
                              <option value="7:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='7:00 AM' || $check==1)){  $check=1;  if($getCloseTime=='7:00 AM'){  echo "disabled='disabled'"; $closetime =1;  } }else{    $check=0; echo "disabled='disabled'";} ?>> 7:00 AM</option>
                              <option value="7:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='7:30 AM' || $check==1)){  $check=1; if($getCloseTime=='7:30 AM'){  echo "disabled='disabled'"; $closetime =1;  }  }else{    $check=0; echo "disabled='disabled'";} ?> > 7:30 AM</option>
                              <option value="8:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='8:00 AM' || $check==1)){  $check=1;if($getCloseTime=='8:00 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 8:00 AM</option>
                              <option value="8:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='8:30 AM' || $check==1)){  $check=1;if($getCloseTime=='8:30 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 8:30 AM</option>
                              <option value="9:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='9:00 AM' || $check==1)){  $check=1; if($getCloseTime=='9:00 AM'){  echo "disabled='disabled'"; $closetime =1;  }  }else{    $check=0; echo "disabled='disabled'";} ?>> 9:00 AM</option>
                              <option value="9:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='9:30 AM' || $check==1)){ $check=1; if($getCloseTime=='9:30 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 9:30 AM</option>
                              <option value="10:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:00 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='10:00 AM' || $check==1)){ $check=1; if($getCloseTime=='10:00 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 10:00 AM</option>
                              <option value="10:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='10:30 AM' || $check==1)){ $check=1; if($getCloseTime=='10:30 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 10:30 AM</option>
                              <option value="11:00 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:00 AM AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='11:00 AM' || $check==1)){ $check=1; if($getCloseTime=='11:00 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 11:00 AM</option>
                              <option value="11:30 AM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:30 AM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='11:30 AM' || $check==1)){ $check=1; if($getCloseTime=='11:30 AM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 11:30 AM</option>
                              <option value="12:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='12:00 PM' || $check==1)){ $check=1; if($getCloseTime=='12:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 12:00 PM</option>
                              <option value="12:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='12:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='12:30 PM' || $check==1)){ $check=1; if($getCloseTime=='12:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 12.30 PM</option>
                              <option value="1.00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1.00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='1.00 PM' || $check==1)){ $check=1; if($getCloseTime=='1.00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 1:00 PM</option>
                              <option value="1:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='1:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='1:30 PM' || $check==1)){ $check=1; if($getCloseTime=='1:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 1:30 PM</option>
                              <option value="2:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='2:00 PM' || $check==1)){ $check=1; if($getCloseTime=='2:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 2:00 PM</option>
                              <option value="2:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='2:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='2:30 PM' || $check==1)){ $check=1; if($getCloseTime=='2:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 2:30 PM</option>
                              <option value="3:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='3:00 PM' || $check==1)){ $check=1; if($getCloseTime=='3:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 3:00 PM</option>
                              <option value="3.30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='3.30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='3.30 PM' || $check==1)){ $check=1; if($getCloseTime=='3.30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 3:30 PM</option>
                              <option value="4.00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4.00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='4.00 PM' || $check==1)){ $check=1; if($getCloseTime=='4.00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 4:00 PM</option>
                              <option value="4:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='4:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='4:30 PM' || $check==1)){ $check=1; if($getCloseTime=='4:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 4:30 PM</option>
                              <option value="5:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='5:00 PM' || $check==1)){ $check=1; if($getCloseTime=='5:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 5:00 PM</option>
                              <option value="5:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='5:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='5:30 PM' || $check==1)){ $check=1; if($getCloseTime=='5:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>>5:30 PM</option>
                              <option value="6:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='6:00 PM' || $check==1)){ $check=1; if($getCloseTime=='6:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 6:00 PM</option>
                              <option value="6:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='6:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='6:30 PM' || $check==1)){ $check=1; if($getCloseTime=='6:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 6:30 PM</option>
                              <option value="7:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='7:00 PM' || $check==1)){ $check=1; if($getCloseTime=='7:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 7:00 PM</option>
                              <option value="7:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='7:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='7:30 PM' || $check==1)){ $check=1; if($getCloseTime=='7:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 7:30 PM</option>
                              <option value="8:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='8:00 PM' || $check==1)){ $check=1; if($getCloseTime=='8:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 8:00 PM</option>
                              <option value="8:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='8:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='8:30 PM' || $check==1)){ $check=1; if($getCloseTime=='8:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 8:30 PM</option>
                              <option value="9:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='9:00 PM' || $check==1)){ $check=1; if($getCloseTime=='9:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 9:00 PM</option>
                              <option value="9:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='9:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='9:30 PM' || $check==1)){ $check=1; if($getCloseTime=='9:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 9:30 PM</option>
                              <option value="10:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='10:00 PM' || $check==1)){ $check=1; if($getCloseTime=='10:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 10:00 PM</option>
                              <option value="10:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='10:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='10:30 PM' || $check==1)){ $check=1; if($getCloseTime=='10:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 10:30 PM</option>
                              <option value="11:00 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:00 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='11:00 PM' || $check==1)){ $check=1; if($getCloseTime=='11:00 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 11:00 PM</option>
                              <option value="11:30 PM" {{ isset($fetchdata->opening_time) && $fetchdata->opening_time =='11:30 PM' ? 'selected' : ''}} <?php if($closetime==0 && ($getOpenTime=='11:30 PM' || $check==1)){ $check=1; if($getCloseTime=='11:30 PM'){  echo "disabled='disabled'"; $closetime =1;  }   }else{    $check=0; echo "disabled='disabled'";} ?>> 11:30 PM</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form_opening">
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.TO'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.TO'); @endphp </span> </label>
                      <div class="info100" >
                        <div class="englishs">
                          <div class="small_time ">
                            <select name="closing_time" id="closing_time" >
                              <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>
                                <option value="12:00 AM"{{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:00 AM' ? 'selected' : ''}} <?php if($getCloseTime !='12:00 AM'){  $check=1;   }else{    $check=1; } ?> > 12:00 AM</option>


                              <option value="00:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='00:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='00:30 AM' || $check ==0){ echo "disabled='disabled'"; $check=0;   }else{    $check=1; } ?>> 00.30 AM</option>
                              
                              <option value="1.00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1.00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='1.00 AM' || $check ==0){  $check=0;  echo "disabled='disabled'"; }else{    $check=1; } ?>> 1:00 AM</option>


                              <option value="1:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='1:30 AM' || $check ==0){  $check=0;  echo "disabled='disabled'"; }else{    $check=1; } ?>> 1:30 AM</option>


                              <option value="2:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='2:00 AM' || $check==0){  echo "disabled='disabled'";  $check=0;    }else{    $check=1;  } ?>> 2:00 AM</option>

                              <option value="2:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='2:30 AM' || $check==0){ echo "disabled='disabled'";  $check=0;   }else{    $check=1;  } ?>> 2:30 AM</option>




                              <option value="3:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='3:00 AM' || $check==0){ echo "disabled='disabled'";   $check=0;   }else{    $check=1;} ?>> 3:00 AM</option>


                              <option value="3.30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3.30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='3.30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 3:30 AM</option>


                              <option value="4.00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4.00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='4.00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 4:00 AM</option>
                              <option value="4:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='4:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 4:30 AM</option>
                              <option value="5:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='5:00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 5:00 AM</option>
                              <option value="5:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='5:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>>5:30 AM</option>
                              <option value="6:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='6:00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 6:00 AM</option>
                              <option value="6:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='6:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 6:30 AM</option>
                              <option value="7:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='7:00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 7:00 AM</option>
                              <option value="7:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='7:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 7:30 AM</option>
                              <option value="8:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='8:00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 8:00 AM</option>
                              <option value="8:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='8:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 8:30 AM</option>
                              <option value="9:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='9:00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 9:00 AM</option>
                              <option value="9:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='9:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 9:30 AM</option>
                              <option value="10:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:00 AM' ? 'selected' : ''}} <?php if($getCloseTime =='10:00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 10:00 AM</option>
                              <option value="10:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='10:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 10:30 AM</option>
                              <option value="11:00 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:00 AM AM' ? 'selected' : ''}} <?php if($getCloseTime =='11:00 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 11:00 AM</option>
                              <option value="11:30 AM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:30 AM' ? 'selected' : ''}} <?php if($getCloseTime =='11:30 AM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 11:30 AM</option>
                              <option value="12:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='12:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 12:00 PM</option>
                              <option value="12:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='12:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='12:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 12.30 PM</option>
                              <option value="1.00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1.00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='1.00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 1:00 PM</option>
                              <option value="1:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='1:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='1:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 1:30 PM</option>
                              <option value="2:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='2:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 2:00 PM</option>
                              <option value="2:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='2:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='2:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 2:30 PM</option>
                              <option value="3:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='3:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 3:00 PM</option>

                              <option value="3.30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='3.30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='3.30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 3:30 PM</option>


                              <option value="4.00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4.00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='4.00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 4:00 PM</option>
                              <option value="4:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='4:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='4:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 4:30 PM</option>
                              <option value="5:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='5:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 5:00 PM</option>
                              <option value="5:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='5:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='5:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>>5:30 PM</option>
                              <option value="6:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='6:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 6:00 PM</option>
                              <option value="6:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='6:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='6:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 6:30 PM</option>
                              <option value="7:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='7:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 7:00 PM</option>
                              <option value="7:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='7:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='7:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 7:30 PM</option>
                              <option value="8:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='8:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 8:00 PM</option>
                              <option value="8:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='8:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='8:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 8:30 PM</option>
                              <option value="9:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='9:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 9:00 PM</option>
                              <option value="9:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='9:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='9:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 9:30 PM</option>
                              <option value="10:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='10:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 10:00 PM</option>
                              <option value="10:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='10:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='10:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 10:30 PM</option>
                              <option value="11:00 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:00 PM' ? 'selected' : ''}} <?php if($getCloseTime =='11:00 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 11:00 PM</option>
                              <option value="11:30 PM" {{ isset($fetchdata->closing_time) && $fetchdata->closing_time =='11:30 PM' ? 'selected' : ''}} <?php if($getCloseTime =='11:30 PM' || $check==0){  $check=0; echo "disabled='disabled'";  }else{    $check=1;} ?>> 11:30 PM</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>



                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100" >
                      <div class="english">
                        <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50">{{ $fetchdata->pro_desc or ''}} </textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50">{{ $fetchdata->pro_desc_ar or ''}}</textarea>
                      </div>
                    </div>
                  </div>
                   
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.mer_specialist'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_specialist'); @endphp </span> </label>
                    <div class="info100" >
                      <div class="english">
                        <input type="text" class="english" name="specialist" id="specialist" maxlength="120" 
		                  value="{{$fetchdata->specialistion or ''}}" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="arabic ar" name="specialist_ar" id="specialist_ar" value="{{$fetchdata->specialistion_ar or ''}}"  maxlength="120" >
                      </div>
                    </div>
                  </div>
                  @if($alldata->count() >=1) 
                  
                  @foreach($alldata as $val)
                  <div class="add_row_wrap_new" style="padding:0;">
                    <div id="remove_button" class="remove_btn"><a href="javascript:void(0);" title="Remove field" class="status_active  status_active2 cstatus" data-status="Active" data-id="{{$val->id}}" style="float:left;">&nbsp;</a></div>
                    <div class="form_row_left common_field">
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Professional_Bio'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Professional_Bio'); @endphp </span> </label>
                      <div class="info100">
                        <div class="has-success">
                          <div class="englishp">
                            <input type="text" maxlength="50" class="sname" placeholder="Please enter in english" name="servicename[]" value="{{ $val->attribute_title }}">
                            <div class="proessional_bio">
                              <input type="text"  placeholder="يرجى الدخول باللغة العربية"  maxlength="50"  class="snamear" name="servicename_ar[]" value="{{ $val->attribute_title_ar }}">
                            </div>
                          </div>
                        </div>
                        <!-- has-success -->
                      </div>
                    </div>
                  </div>
                  @endforeach
                  
                  @else
                   <div class="form_row_left common_field">
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Professional_Bio'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Professional_Bio'); @endphp </span> </label>
                  <div class="info100">
                    <div class="has-success">
                      <div class="englishdf">
                        <input type="text" name="servicename[]"  placeholder="Please enter in english" maxlength="50" class="sname" >
                        <div class="proessional_bio">
                          <input type="text" placeholder="يرجى الدخول باللغة العربية"  name="servicename_ar[]" maxlength="50" class="sname" >
                        </div>
                      </div>
                    </div>
                    <!-- has-success -->
                  </div>   </div>
                  <!-- info100 -->
                  @endif
                  <div id="img_upload"></div>
                  <div class="form_row_left common_field">
                    <div id="add_button" class=""><a href="javascript:void(0);" class="form-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_ADDMORE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_ADDMORE'); @endphp </span> </a></div>
                  </div>
                  @php $Count = $getDbC +1;@endphp
                  <input type="hidden" id="count" name="count" value="{{$Count}}">
                  <input type="hidden" id="autoid" name="autoid" value="{{request()->autoid}}">
                  <input type="hidden" id="sid" name="sid" value="{{ request()->sid}}">
                  <input type="hidden" id="parent_id" name="parent_id" value="{{ request()->id}}">
                  <input type="hidden" id="incid" name="incid" value="{{ request()->incid}}">
          
                <div class="form_row_left">
                  <div class="english">
                    <input type="hidden" value="{{ $fetchdata->mc_id or '' }}" name="itemid">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                  </div>
                </div>
				</div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>

  
  $(document).ready(function(){
  var maxField = 11;  
  var addButton = $('#add_button');  
  var wrapper = $('#img_upload');
  var x = @php echo $Count; @endphp; 

  $(addButton).click(function(){  
  if(x < maxField){  
  x++; 
  var main = x;

  var testElement= document.getElementById('english_tab');
  var result = testElement.classList.contains('active')
  if(result)
  {
    var enclass=''
    var arclass='f'
  }
  else
  {
    var enclass='gg'
    var arclass= ''

  }



  var fieldHTML = '<div class="add_row_wrap_new main'+x+' "><div id="remove_button" class="remove_btn" onclick="javascript: removemain('+main+')"><a href="javascript:void(0);"  title="Remove field" > &nbsp; </a></div><div class="form_row"><label class="form_label"><span class=" '+enclass+' ">@if (Lang::has(Session::get('mer_lang_file').'.Professional_Bio')!= '') {{  trans(Session::get('mer_lang_file').'.Professional_Bio') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Professional_Bio') }} @endif </span></label><div class="info100"><div class="has-success"><div class="p '+enclass+' "><input type="text" class=" '+enclass+' sname " name="servicename[]" maxlength="50" placeholder="Please enter in english"  > <div class="proessional_bio"><input type="text" class="p '+arclass+' snamear" placeholder="يرجى الدخول باللغة العربية"  name="servicename_ar[]" maxlength="50" ></div></div></div></div></div>'; 
  $(wrapper).append(fieldHTML);  
  createValidation();
  document.getElementById('count').value = parseInt(x);
  }
  });
  /*$(wrapper).on('click', '#remove_button', function(e){  
  e.preventDefault();
  $(this).parent('div').remove(); 
  x--;  
  document.getElementById('count').value = parseInt(x);
  }); */ 
  });
  
  function removemain(val)
  {
       
      var x = document.getElementById('count').value;
      $('.main'+val).remove();
      x--;  
      document.getElementById('count').value = parseInt(x);

  }

       
 $("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  category: {
                       required: true,
                      },

                       manager: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },

                        specialist: {
                       required: true,
                      },
                       specialist_ar: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       consulation_fees: {
                       required: true,
                      },
                      
                       about: {
                       required: true,
                      },
                       about_ar: {
                       required: true,
                      },
                      'servicename[]': {
                       required: true,
                      },

                       opening_time: {
                       required: true,
                      },

                       closing_time: {
                       required: true,
                      },

                       @if(isset($fetchdata->pro_Img)!='')  
                        branchimage: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        branchimage: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif
                      
                       @if(isset($fetchdata->terms_conditions)!='') 
                        mc_tnc: {
                           required:false,
                           accept:"pdf",
                      },
                    @else
                         mc_tnc: {
                           required:true,
                           accept:"pdf",
                      },
                      @endif


                       @if(isset($fetchdata->terms_conditions_ar)!='') 
                        mc_tnc_ar: {
                           required:false,
                           accept:"pdf",
                      },
                    @else
                         mc_tnc_ar: {
                           required:true,
                           accept:"pdf",
                      },
                      @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             category: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",
                      }, 

                       opening_time: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_OPENING_TIME')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_OPENING_TIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_OPENING_TIME') }} @endif",
                      }, 

                      closing_time: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CLOSING_TIME')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CLOSING_TIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_CLOSING_TIME') }} @endif",
                      },  

                 manager: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR') }} @endif",
                      },  
                          mc_tnc: {
              
                        required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); @endphp",
                        accept: "@php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); @endphp",

                      },



                         mc_tnc_ar: {


                        required: "@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file_ar'); @endphp",
                        accept: "@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file_ar'); @endphp",
              
                      },

                         mc_name: {
                           required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_NAME'); @endphp",
                      },   

                          mc_name_ar: {
                      required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_NAME_AR'); @endphp",
                      }, 



                         specialist: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.Please_Enter_Specilist')!= '') {{  trans(Session::get('mer_lang_file').'.Please_Enter_Specilist') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Please_Enter_Specilist') }} @endif",
                      },   
                      

                        specialist_ar: {
                    required: "@php echo lang::get('mer_ar_lang.Please_Enter_Specilist_AR'); @endphp",
                      },      

             

                       consulation_fees: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.Please_Enter_Consulation_fees')!= '') {{  trans(Session::get('mer_lang_file').'.Please_Enter_Consulation_fees') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Please_Enter_Consulation_fees') }} @endif ",
                      },
  
                      
                       about: {
                        required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); @endphp", 
               
                      },
  
                       about_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_AR'); @endphp",
                      },
                      'servicename[]': {
                      required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_BIOGRAPHY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_BIOGRAPHY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_BIOGRAPHY') }} @endif"
                       },
                  branchimage: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },                                 
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                @if($mer_selected_lang_code !='en')


               
                 

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }

                   
                    
                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }

                    if (typeof valdata.specialist != "undefined" || valdata.specialist != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }



                    
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }
                      if (typeof valdata.opening_time != "undefined" || valdata.opening_time != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.closing_time != "undefined" || valdata.closing_time != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    } 
                 if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }
                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                    {

                        $('.arabic_tab').trigger('click');
                        

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                    

                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }

                     if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }
          @else

   if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }

                    
                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }
                  
                 if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                    

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }

                     if (typeof valdata.opening_time != "undefined" || valdata.opening_time != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.closing_time != "undefined" || valdata.closing_time != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }

                    if (typeof valdata.specialist != "undefined" || valdata.specialist != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.branchimage != "undefined" || valdata.branchimage != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }


@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

 var createValidation = function() {
  $(".sname").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_BIOGRAPHY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_BIOGRAPHY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_BIOGRAPHY') }} @endif"
      }
    });
  });


}
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script>
<!-- <div merchant_vendor -->
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup -->
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  





     jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Want_Delete')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Want_Delete') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Want_Delete') }} @endif')



jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'clinicskinteetch'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script>
@include('sitemerchant.includes.footer')
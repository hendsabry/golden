@include('sitemerchant.includes.header')  
@php $hall_leftmenu =1; @endphp 
 
<!--start merachant-->
<div class="merchant_vendor">
  <!--left panel-->
 @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')

@php
	$hid         = request()->hid;
	$bid         = request()->bid;
	$opid        = request()->opid;
	 $cusid       = request()->cust_id;
	$oid         = request()->oid;
	$orderid     = request()->orderid;
	$shipaddress       = Helper::getgenralinfo($orderid);
	$getCustomer = Helper::getuserinfoOrderdeatils($cusid,$orderid); 
	$cityrecord  = Helper::getcity($shipaddress->shipping_city);
	$getorderedproduct=Helper::getorderedproductdetailbasicdetails($orderid,'hall','hall',$hid);

  $productinformation=Helper::getProduckInfo($getorderedproduct->product_id);


@endphp
  <!--left panel end-->
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif<a href="{{url('/')}}/hall-order?hid={{$_REQUEST['hid']}}&bid={{$_REQUEST['bid']}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a></h5>
        </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($getCustomer->payer_name) && $getCustomer->payer_name!=''){ echo $getCustomer->payer_name; } @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($getCustomer->payer_email) && $getCustomer->payer_email!=''){ echo $getCustomer->payer_email; } @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($getCustomer->payer_phone) && $getCustomer->payer_phone!=''){ echo $getCustomer->payer_phone; }else{ echo 'N/A';} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){ echo $cityrecord->ci_name; }else{ echo 'N/A';} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($getCustomer->order_shipping_add) && $getCustomer->order_shipping_add!=''){echo $getCustomer->order_shipping_add; @endphp <a target="_blank" href="https://www.google.com/maps/place/<?=$getCustomer->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a>
                      @php }else{ echo 'N/A';} @endphp
                    </div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" > @php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      @endphp </div>
                  </div>
                </div>




                <!-- form_row -->
                <div class="form_row">
                 <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_TRANSACTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION') }} @endif #</label>
                    <div class="info100"> 

                      {{$shipaddress->transaction_id}}
                    </div>
                  </div>
                  
                     <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($getorderedproduct->order_id) && $getorderedproduct->order_id!=''){echo $getorderedproduct->order_id;
                        
                    } @endphp</div>
                  </div>

                </div>

                <div class="form_row">
                 <div class="noneditbox-sub-heading"> @if (Lang::has(Session::get('mer_lang_file').'.PAYMENTS')!= '') {{  trans(Session::get('mer_lang_file').'.PAYMENTS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PAYMENTS') }} @endif</div>
 <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Total_Price')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Total_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Total_Price') }} @endif</label>
                    <!-- <div class="info100" >SAR {{ number_format((float)$getorderedproduct->total_price, 2, '.', '') }}</div> -->

                    <div class="info100" >SAR <span id="calTotal"></span></div>


                  </div>
                    <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= '') {{  trans(Session::get('mer_lang_file').'.Insuranceamount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Insuranceamount') }} @endif</label>
                    <div class="info100" > {{ number_format((float)$getorderedproduct->insurance_amount, 2, '.', '') }}</div>
                  </div>
                </div>



                  @php $INSU = $getorderedproduct->insurance_amount;
                    $couponamountapplied=0;
                  if(isset($getorderedproduct->coupon_code) && $getorderedproduct->coupon_code!=''){

                    $couponamountapplied=$getorderedproduct->coupon_code_amount;
                   @endphp
                  <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE') }} @endif</label>
                    <div class="info100">@php if(isset($getorderedproduct->coupon_code) && $getorderedproduct->coupon_code!=''){echo $getorderedproduct->coupon_code;
                        
                    } @endphp</div>
                  </div>



                    <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COUPON_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COUPON_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COUPON_AMOUNT') }} @endif</label>
                    <div class="info100" >SAR {{ number_format((float)$getorderedproduct->coupon_code_amount, 2, '.', '') }}</div>
                  </div>
                </div>

                  @php } @endphp
 

                <div class="form_row">

                    <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Hall_Price')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Hall_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Hall_Price') }} @endif</label>
                    <div class="info100">SAR @php if(isset($productinformation->pro_price) && $productinformation->pro_price!=''){
                        if(isset($productinformation->pro_disprice) && $productinformation->pro_disprice>0){
                           echo number_format($productinformation->pro_disprice,2);
                           $pp=$productinformation->pro_disprice;
                      }else{

                         echo number_format($productinformation->pro_price,2);
                         $pp=$productinformation->pro_price;
                      }
                     
                        
                    } @endphp</div>
                  </div>

                   <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.REMAINING_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.REMAINING_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.REMAINING_AMOUNT') }} @endif</label>
                    <div class="info100" >
                    @php
                     $totalprice                  = $getorderedproduct->total_price;
                     $paid_total_amount           = $getorderedproduct->paid_total_amount;
                     $remainingamount = $totalprice-$paid_total_amount; 

                     $rp=$pp*75/100;
                     @endphp

                     SAR {{ number_format((float)$rp, 2, '.', '') }}</div>
                  </div>



                </div>

 
                <div class="form_row">

                    <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_AND_FOOD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_AND_FOOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_AND_FOOD') }} @endif</label>
                    <div class="info100">SAR <span id="serviceprice"></span> </div>
                  </div>

                    <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.Credited')!= '') {{  trans(Session::get('mer_lang_file').'.Credited') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Credited') }} @endif</label>
                    <div class="info100">SAR <span id="credited"></span> </div>
                  </div>
 
                </div> 

                <div class="form_row"> 

                        <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.VAT_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.VAT_CHARGE') }} @endif</label>
                    <div class="info100">

                       @php

                       $discouponamount=$paid_total_amount-$couponamountapplied;
                           $vatamonu = Helper::calculatevat($getorderedproduct->order_id,$discouponamount);
                        $tppaid=$discouponamount+$vatamonu;
                      @endphp
                      
                    SAR {{ number_format((float)$vatamonu, 2, '.', '') }}

                </div>
                  </div>


                   <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Advance_Amount')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Advance_Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Advance_Amount') }} @endif</label>
                    <div class="info100" >SAR {{ number_format((float)$tppaid, 2, '.', '') }}</div>
                  </div>
                  

                </div>

                 <div class="form_row">

                    <div class="noneditbox">
                    <label class="form_label">25% of @if (Lang::has(Session::get('mer_lang_file').'.MER_Hall_Price')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Hall_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Hall_Price') }} @endif</label>
                    <div class="info100"> @php 
                       $hp25=$pp*25/100;

                     @endphp

                      SAR {{ number_format((float)$hp25, 2, '.', '') }}
                   </div>
                  </div>

                   


                </div>



                <!-- form_row -->
               @php if(count($getoccasiondetail)>0){ @endphp
                    
                 <div class="form_row">
                  <div class="noneditbox-sub-heading"> @if (Lang::has(Session::get('mer_lang_file').'.OCCASION')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASION') }} @endif</div>
                   <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONDATE') }} @endif</label>
                    <div class="info100">
                      @php  $ocdate=strtotime($getoccasiondetail->occasion_date);
                      $ocdatef = date("d M Y",$ocdate);
                      @endphp
                      {{ $ocdatef }}   
@if($getoccasiondetail->occasion_id=='-1')
                        - 
                      @php  $ocdate=strtotime($getoccasiondetail->end_date);
                      $ocdatef = date("d M Y",$ocdate);
                      @endphp
                      {{ $ocdatef }} 

@endif
                    </div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONBUDGET')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONBUDGET') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONBUDGET') }} @endif</label>
                    <div class="info100" >SAR {{ number_format((float)$getoccasiondetail->budget, 2, '.', '') }}</div>
                  </div>

                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.total_member')!= '') {{  trans(Session::get('mer_lang_file').'.total_member') }} @else  {{ trans($MER_OUR_LANGUAGE.'.total_member') }} @endif</label>
                    <div class="info100">{{ $getoccasiondetail->total_member }}</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONTYPE')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONTYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONTYPE') }} @endif</label>
                    <div class="info100">
                        @php $getocc = Helper::business_occasion_type($getoccasiondetail->occasion_id); @endphp
                       {{ $getocc->title or ''}}</div>
                  </div>
                </div>



             @php } @endphp
                <!-- form_row -->
                 @php $AllServices = 0; @endphp
                <!-- form_row -->
                @php if(count($gethall_ordered_services)>0){ @endphp
                <div class="paid-ser-area">
                  <div class="noneditbox-sub-heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Paid_Services')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Paid_Services') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Paid_Services') }} @endif</div>
                  <div class="paid-ser-line">
                  
                    @foreach($gethall_ordered_services as $order_services)
                    @php
                     if(Session::get('mer_lang_file')=='mer_en_lang'){ $servicesfield='option_value_title'; }else{ $servicesfield='option_value_title_ar';}
                     $AllServices = $AllServices + $order_services->option_value_price;
                     @endphp
                    <div class="paid-ser-box">{{ $order_services->$servicesfield }}<span>SAR {{ number_format($order_services->option_value_price,2) }}</span></div>
                    @endforeach
                    
                  </div>
                </div>
                @php } 

                  if(count($gethall_internal_food_services)>0){
                @endphp

               </div>
              <!-- hall-od-top -->


                <div class="tailor_product">
                 <div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_HALLDishes')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALLDishes') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALLDishes') }} @endif</div>
                       
                            
           
           
                @php $i = 1;  $basetotal = 0; $basetotal=0; @endphp
                @foreach($gethall_internal_food_services as $internalfood)
                @php  if(Session::get('mer_lang_file')=='mer_en_lang'){ $internalfoodfield='internal_dish_name'; $internalfoodcontanierfield='container_title'; }else{ $internalfoodfield='internal_dish_name_ar'; $internalfoodcontanierfield='container_title_ar';} 
                $AllServices = $AllServices + $internalfood->price;
                $basetotal=$basetotal+$internalfood->price;
                @endphp
                


                    <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                  <div class="style_area">
                   
                    <div class="style_box_type">
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_DISH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISH_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISH_NAME') }} @endif</div>
                          <div class="style_label_text"> {{ $internalfood->$internalfoodfield }} </div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Dish_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Dish_Image') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Dish_Image') }} @endif</div>
                          <div class="style_label_text"> <img src="<?=$internalfood->dish_image?>" width="80" height="80"/></div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Qty')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Qty') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Qty') }} @endif</div>
                          <div class="style_label_text"> {{$internalfood->quantity}}</div>
                        </div>
                      </div>
            
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CONTAINER_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CONTAINER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CONTAINER_NAME') }} @endif</div>
                          <div class="style_label_text">{{ $internalfood->$internalfoodcontanierfield }}</div>
                        </div>
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Image') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Image') }} @endif</div>
                          <div class="style_label_text"> 
                            <div class="style_label_text"> <img src="<?=$internalfood->container_image?>" width="80" height="80"/></div>
              </div>
                        </div>

                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Price')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Price') }} @endif</div>
                          <div class="style_label_text"> 
                            <div class="style_label_text"> {{number_format($internalfood->price,2)}} </div>
              </div>
                        </div>
                      </div>

                      
                    </div>
                  </div>
                  
                </div> 




                  @php $i++; @endphp
                   @endforeach


                   
               <!-- <div class="merchant-order-total-area">
                <div class="merchant-order-total-line"> {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: &nbsp;
                  
                  @php if(isset($shipaddress->order_taxAmt) && $shipaddress->order_taxAmt!='')
                  {
                  echo 'SAR '.number_format($shipaddress->order_taxAmt,2);
                  }
                  else
                  {
                  echo 'N/A';
                  }
                  @endphp </div>
                 merchant-order-total-line 
                <div class="merchant-order-total-line"> {{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: &nbsp;
                  <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$shipaddress->order_taxAmt),2);
				 } ?>
                </div>-->                <!-- merchant-order-total-line -->
              </div>
                @php } @endphp
                
              </div>
            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
<script type="text/javascript">
  
$(document).ready(function(){ 
  $('#serviceprice').html('<?php echo $AllServices; ?>');
    $('#calTotal').html('<?php echo ($AllServices + $pp+$vatamonu + $INSU); ?>');
$('#credited').html('<?php echo ($tppaid - $INSU); ?>');
})
 

</script>
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $listmanagerblade =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_MANAGER') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_MANAGER') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
              <form name="form1" id="editmanager" method="post" action="{{ route('updatemanager') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_FIRST_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_FIRST_NAME'); @endphp </span> </label>
                  <div class="info100">
                      <div class="english">
                    <input type="text" class="english"  name="mer_fname" maxlength="40" value="{{ $edituser->mer_fname }}" id="mer_fname" required="" >
                      </div>      
    <div class="arabic ar" >
                    <input type="text" class="arabic ar" name="mer_fname_ar" maxlength="40" value="{{ $edituser->mer_fname_ar }}" id="mer_fname_ar" required="" >
                  </div> </div> 
                  </div>
                  
                  <div class="form_row_right">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_LAST_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_LAST_NAME'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
                    <input type="text" class="english" name="mer_lname" maxlength="40"  value="{{ $edituser->mer_lname }}" id="mer_lname"  required="">
                      </div>      
    <div class="arabic ar" >
                    <input maxlength="40" class="arabic ar" type="text" name="mer_lname_ar" value="{{ $edituser->mer_lname_ar }}" id="mer_lname_ar"  required="">
 </div>
                  </div>
                  </div>
                  
                </div>
                <!-- form_row --> 

                <div class="form_row">
                  <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_Email_Address'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Email_Address'); @endphp </span> </label>
                  <div class="info100">
                    <input type="email" name="mer_email" maxlength="50"  value="{{ $edituser->mer_email }}" id="email_id" onchange="check_email();" required="" >
                    <div id="email_id_error_msg"  style="color:#F00;font-weight:800"> </div>
                  </div>
                  </div>
                  <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CUSTOMERMOBILE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CUSTOMERMOBILE'); @endphp </span> </label>
                  <div class="info100">
                    <input class="small-sel" type="text" maxlength="12"  name="mobile" value="{{ $edituser->mer_phone }}" id="mobile"  required="">
                  </div>
                  </div>
                  
                </div>
                <!-- form_row -->

                <div class="form_row">
                	<div class="form_row_left common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp </span> </label>
                  <div class="info100">
                 


<select class="small-sel" name="city"  id="city">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_CITY') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY') }} @endif</option>

                        @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> @if($mer_selected_lang_code !='en') {{$cbval->co_name_ar }} @else {{$cbval->co_name}} @endif</option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                        @php $ci_name='ci_name'@endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_'.$mer_selected_lang_code; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" @if($edituser->city_name == $val->ci_id) selected  @endif>{{ $val->$ci_name }}</option>
                        @endforeach
                        @endforeach
                    
                      </select>




                    <input type="hidden" name="id" value="{{ $id }}" id="id" >
                  </div>
                  </div>
                  
                  <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                  
                  <div class="info100"> 
                    <input type="hidden" name="updatemc_img" value="{{ $edituser->stor_img }}">
                  </div>
                  
                  <div class="form_row common_field">
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                    </div>
                    </label>
                                      
                   @if($edituser->stor_img !='')
<span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
@endif
        
                    <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                  </div>

                </div>
                @if($edituser->stor_img !='')
                     <img src="{{ $edituser->stor_img or '' }}" width="150" height="150"> </br>
                    @endif
                  </div>
                  
                </div>
                <!-- form_row -->
                
                
                
                
                <!-- form_row -->
                
                <div class="form_row">
                <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.UPLOADCERTIFICATE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.UPLOADCERTIFICATE'); @endphp </span> </label>
                  
                  <div class="info100"> 
                    <input type="hidden" name="updatecertificate_img" value="{{ $edituser->certificates }}">
                  </div>
                  <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div id="file_value2" class="file-value"></div>
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                    </div>
                    </label>
                    <input id="company_logo1" name="storcertifiction_img" class="info-file" type="file" value="">
                  </div>
                  <div class="error certifications"> </div>

                  @if($edituser->certificates !='')
                    @php  $prod_path = url('').'/public/assets/img/pdflogo.png';
                    $Urls = url('').'/public/assets/storeimage/'.$edituser->certificates;
                    @endphp <a href="{{$Urls}}" target="_blank">

                       <img src="{{url('/themes/images/pdf.png')}}">
 
                    </a>

                    {{$edituser->certificates_name or ''}}
                    @endif
<input type="hidden" name="updatecertificate_name" value="{{$edituser->certificates_name or ''}}">
                </div>
                </div>
                </div>
                <!-- form_row -->
                
                <div class="form_row english">
                  <input type="submit" name="submit" value="Update">
                </div>
                <!-- form_row -->
                
                <div class="form_row arabic ar">
                  <input type="submit" name="submit" value="تحديث">
                </div>
                <!-- form_row -->
              </form>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>       
	$("form").data("validator").settings.ignore = "";
</script> 
<script type="text/javascript">
  
$("#editmanager").validate({
               ignore: [],
               rules: {
                  mer_fname: {
                       required: true,
                      },

                       mer_lname: {
                       required: true,
                      },
                  mer_fname_ar: {
                       required: true,
                      },
                      mer_lname_ar: {
                       required: true,
                      },
 
                       mer_email: {
                       required: true,
                      },

                      mobile: {
                       required: true,
                      },
                       city: {
                       required: true,
                      },
                                           stor_img: {
                           accept:"png|jpe?g|gif",
                      },
                         storcertifiction_img: {
                           accept:"pdf",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mer_fname: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_FIRST_NAME') }} @endif",
                      },  

                 mer_lname: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_LAST_NAME') }} @endif",
                      },  
                      
          mer_fname_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_FIRST_NAME_AR') }} @endif",
                      },  

                 mer_lname_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_LAST_NAME_AR') }} @endif",
                      }, 

                mer_email: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EMAIL') }} @endif",
                      },   

               mobile: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MOBILE') }} @endif",
                      },   


                       city: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY_TEXT') }} @endif ",
                      },

                         stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                    storcertifiction_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_PDF_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_PDF_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_PDF_FILE') }} @endif"
                      },                        
                     
                },
                invalidHandler: function(e, validation){
                   
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
 
  $("#company_logo1").change(function () {
        var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   $("#file_value2").html('');         
$(".certifications").html('Only PDF format allowed.');

         }
    });






</script> 
@include('sitemerchant.includes.footer')
 @inject('data','App\Help')
@include('sitemerchant.includes.header')  
 
@php $acoustics_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu') }} @endif</h5>
      </div>
       
 @php $id = request()->id; $hid= request()->hid; @endphp
          {!! Form::open(array('url'=>"acoustics-quoted-requested-list/{$id}/{$hid}",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
        @php $statuss = request()->status; $user_acceptance= request()->acceptance;  $searchh= request()->search; @endphp
        <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
        <div class="filter_area">
          
          <div class="filter_left two-srarch-box">
            <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
            <div class="search-box-field mems">
               <select name="status" id="status" class="city_type">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
              <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
              <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
            </select>
            
         
               <select name="acceptance" id="acceptance" class="city_type">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_ACCEPT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_ACCEPT') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_ACCEPT') }} @endif </option>
              <option value="1" @if(isset($user_acceptance) && $user_acceptance=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.YES')!= '') {{  trans(Session::get('mer_lang_file').'.YES') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.YES') }} @endif</option>
              <option value="0" @if(isset($user_acceptance) && $user_acceptance=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.NO')!= '') {{  trans(Session::get('mer_lang_file').'.NO') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.NO') }} @endif</option>
            </select>
              <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
           
            </div>
 




          </div>
          
          <div class="search_box">
            <div class="search_filter">&nbsp;</div>
            <div class="filter_right">
              <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$searchh or ''}}" />
              <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
            </div>
          </div>
        </div>
      <!-- filter_area --> 
       {!! Form::close() !!}




      <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition --> 
              
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
          
            <div class="table_wrap"> 
              <!-- Display Message after submition --> 
              
              <!-- Display Message after submition -->
              <div class="panel-body panel panel-default">
             


@if($getusers->count() <1)


<div class="no-record-area">
@if (Lang::has(Session::get('mer_lang_file').'.NORECORDFOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NORECORDFOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NORECORDFOUND')}} @endif

</div>

@else




                <div class="table">
                  <div class="tr">
<div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</div> 
<div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</div> 
<div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} @endif</div> 
<div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Request_Date')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Request_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Request_Date') }} @endif</div>
<div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                  </div>
                 
        @foreach($getusers as $vals)
      @php
                               if($vals->id!='') $details=Helper::detailid($vals->id);
                                @endphp


<div class="tr">
<div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif">{{$vals->cus_name}}</div>
<div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif">{{$vals->email}}</div>
<div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} @endif">{{$vals->cus_phone or 'N/A'}}</div>
<div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Request_Date')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Request_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Request_Date') }} @endif"> {{ Carbon\Carbon::parse($vals->created_at)->format('F j, Y')}}  </div>
<div class="td td5 view_center" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif">

 @if($vals->status==1)
<a href="{{ route('acoustics-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itmid' => $vals->id]) }}"><img src="{{url('')}}/public/assets/img/view-icon.png" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></a>

 @elseif($vals->status==2) 
 <a href="{{ route('acoustics-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itmid' => $vals->id,'autoid'=>$details->id]) }}">Replied</a>

@elseif($vals->status==3) 
 <a href="{{ route('acoustics-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itmid' => $vals->id,'autoid'=>$details->id]) }}">Confirmed by customer</a>
@endif



</div>
</div>
 
          @endforeach         
                
                  
                  
                  </div>


                       @endif
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
                
     
               </div>
            <!-- table_wrap --> 
            
          </div>
             
              
              </div>
                          
  
          </div>
        </div>
        
      </div> <!-- global_area -->
      
    </div>
  </div>
</div>
</div>
@include('sitemerchant.includes.footer')
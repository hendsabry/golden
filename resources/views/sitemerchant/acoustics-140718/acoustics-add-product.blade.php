@include('sitemerchant.includes.header') 
@php $acoustics_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
      <style type="text/css">
      .ms-drop ul li label input[type="checkbox"] {
      height: 20px;
      display: inline-block;
      width: 20px;
      position: relative;
      border: 1px solid red;
      opacity: 1;
      }
      </style>
      <link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />
  <div class="right_panel">
    <div class="inner">
      <header>
         @if(request()->autoid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.add_product')!= '') {{  trans(Session::get('mer_lang_file').'.add_product') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.add_product') }} @endif </h5>
          @else
          <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATE_PRODUCT')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_PRODUCT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_PRODUCT') }} @endif </h5>

          

          @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->
            
                <form name="form1" id="addmanager" method="post" action="{{ route('store-accousticproduct') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                 <!-- form_row -->
                    <div class="form_row common_field"> 
             
              </div>
                  <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.mer_category_name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_category_name'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
                      <input type="text" name="name" id="name" value="{{ $fetchfirstdata->pro_title or '' }}"  class="english"  data-validation-length="max80" maxlength="140">
                    </div>
                    <div class="arabic ar">
                      <input id="ssb_name_ar" class="arabic ar" name="name_ar" id="name_ar" type="text" value="{{ $fetchfirstdata->pro_title_ar or '' }}"   maxlength="140" >
                    </div>
                  </div>
                  </div>
				   <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                      @if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!='')
                      <div class="form-upload-img">
                       <img src="{{ $fetchfirstdata->pro_Img or '' }}" >
                     </div>
                     @endif
                    </div>
					</div>
					 
					
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
				   <div class="form_row_left common_field">
                    <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="price" id="price" maxlength="15" value="{{ $fetchfirstdata->pro_price or '' }}"  required="" >
                      </div>
                    </div>
					</div>
					 
                    <!-- form_row --> 
                  </div>
                  <!-- form_row -->

                  <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.Quantity_For_Sale'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Quantity_For_Sale'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="{{ $fetchfirstdata->pro_qty or '' }}"  required="" >
                      </div>
                    </div>
          </div>
           
                    <!-- form_row --> 
                  </div>
				  
   <div class="form_row">
           <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.Avilable_on_rent'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Avilable_on_rent'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
<div class="yesno-radio">
                        <input onclick="show1();" type="radio" class="notzero" name="avilable_on_rent" id="avilable_on_rent_yes" @php if(isset($getRentP->price) && $getRentP->price !='') echo 'checked'; @endphp value="1" > <label for="avilable_on_rent_yes">@if (Lang::has(Session::get('mer_lang_file').'.Yes')!= '') {{  trans(Session::get('mer_lang_file').'.Yes') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Yes') }} @endif</label>
</div>
<div class="yesno-radio">
                        <input onclick="show2();" type="radio" class="notzero" name="avilable_on_rent"  @php if(isset($getRentP->price) && $getRentP->price =='') echo 'checked';  @endphp id="avilable_on_rent_no" value="0" > <label for="avilable_on_rent_no">@if (Lang::has(Session::get('mer_lang_file').'.No')!= '') {{  trans(Session::get('mer_lang_file').'.No') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.No') }} @endif</label>
</div>
                      </div>
                    </div>
          </div>
				  </div>
 
 <div class="form_row" id="rentprice" <?php if(isset($getRentP->price) && $getRentP->price =='' ||  request()->autoid==''){?>  style="display: none;"; <?php } ?>>
           <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.Rent_price'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Rent_price'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                       <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="rent_price" id="rent_price" maxlength="15" value="{{ $getRentP->price or '' }}" >

                      </div>
                    </div>
          </div>

            <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.Insuranceamount'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Insuranceamount'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                       <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="insuranceamount" id="insuranceamount" maxlength="15" value="{{ $fetchfirstdata->Insuranceamount or '' }}" >

                      </div>
                    </div>
          </div>

            <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.Quantity_For_Rent'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Quantity_For_Rent'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity_for_rent" id="quantity_for_rent" maxlength="15" value="{{ $getQuantity->value or '' }}"  >
                      </div>
                    </div>
          </div>
           
                    <!-- form_row --> 
                  </div>

          </div>

 
				  <div class="form_row">
  				          <div class="form_row_left">
                      <label class="form_label"> 
                      	<span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> 
                          <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> 
                       </label>
                      <div class="info100">
                        
                        <div class="english">
                          <textarea name="description" class="english" maxlength="500" id="description" rows="4" cols="50">{{ $fetchfirstdata->pro_desc or ''  }} </textarea> 
                        </div>      
                        
                        <div  class="arabic ar" >     
                                <textarea class="arabic ar" maxlength="500" name="description_ar" id="description_ar" rows="4" cols="50">{{ $fetchfirstdata->pro_desc_ar or ''  }}</textarea>  
                        </div> 
                        
                      </div>
                      <!-- form_row --> 
  				          	</div>
                  </div>

                    <div class="form_row">
                    <div class="form_row_left">
                      <label class="form_label"> 
                        <span class="english">@php echo lang::get('mer_en_lang.Note'); @endphp</span> 
                          <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Note'); @endphp </span> 
                       </label>
                      <div class="info100">
                        
                        <div class="english">
                          <textarea name="note" class="english" maxlength="500" id="note" rows="4" cols="50">{{ $fetchfirstdata->notes or ''  }} </textarea> 
                        </div>      
                        
                        <div  class="arabic ar" >     
                                <textarea class="arabic ar" maxlength="500" name="note_ar" id="note_ar" rows="4" cols="50">{{ $fetchfirstdata->notes_ar or ''  }}</textarea>  
                        </div> 
                        
                      </div>
                      <!-- form_row --> 
                      </div>
                  </div>
                  
                  

                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                  <input type="hidden" name="hid" value="{{ $hid }}"> 
                   <input type="hidden" name="parentid" value="{{ $id }}">
                   <input type="hidden" name="autoid" id="autoid" value="{{ $autoid or '' }}">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <!-- form_row -->
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  <!-- form_row -->
                  </div></div>
                </form>
          
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
  
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addmanager").validate({


              ignore: [],
               rules: {
                 
                  category1: {
                       required: true,
                      },
                   
                    

                    name: {
                       required: true,
                      },

                       name_ar: {
                       required: true,
                      },

                      price: {
                       required: true,
                      },

                      quantity: {
                       required: true,
                      },

                       

                     description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                       @if(isset($fetchfirstdata->pro_Img)!='')  
                          stor_img: {
                           required: false,
                           accept:"png|jpe?g|gif",
                      },
                      @else
                       stor_img: {
                           required: true,
                           accept:"png|jpe?g|gif",
                      },
                       
                         
                      @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                
           messages: {
             
           
               category1: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",
                      },         



             name: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_NAME'); @endphp",
                      },  

                 name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_NAME_AR'); @endphp",
                      },

                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                      },


                       quantity: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY') }} @endif ",
                      },

                 


                   description: {
            

                required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
                      },

                       description_ar: {
              
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
                      },
        
                 
                  stor_img: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",

                 }, 
                       
                    
                },             
                   invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                     console.log("invalidHandler : validation", valdata);

 
                    @if($mer_selected_lang_code !='en')
                    if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                   
                   
                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                      {

                      $('.arabic_tab').trigger('click');


                      }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }

                      if (typeof valdata.quantity != "undefined" || valdata.quantity != null) 
                      {
                      $('.arabic_tab').trigger('click');     


                      }

                      if (typeof valdata.description != "undefined" || valdata.description != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');
                      }

                   
                    if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     


                    }


                      
                  @else
                      if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                      {

                      $('.arabic_tab').trigger('click');     
 
                      }


                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                      {

                      $('.arabic_tab').trigger('click');     


                      }
                      if (typeof valdata.name != "undefined" || valdata.name != null) 
                      {
                      $('.english_tab').trigger('click'); 

                      }
 

                      if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                      {

                      $('.english_tab').trigger('click');


                      }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                      {
                      $('.english_tab').trigger('click');
                      }

                      if (typeof valdata.quantity != "undefined" || valdata.quantity != null) 
                      {
                      $('.english_tab').trigger('click');     


                      }

                      if (typeof valdata.description != "undefined" || valdata.description != null) 
                      {
                      $('.english_tab').trigger('click');
                      }
 
                    
          @endif

                    },

                  submitHandler: function(form) {
                      form.submit();
                  }
              });
              $("#company_logo1").change(function () {
              var fileExtension = ['pdf'];
              $(".certifications").html('');
              if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
              $("#file_value2").html('');         
              $(".certifications").html('Only PDF format allowed.');

              }
              });

                $(document).ready(function(){
                      var autoid = $('#autoid').val();
                      var catid = $('#category').val();
                       if(catid=='2' || autoid=='') {
                       $('.displaycat').hide();
                      } else {
                      $('.displaycat').show();
                      }

                      });
                      function chceckCategory()
                      {
                      var catid =  $('#category').val()


                      if(catid==1) {
                      $('.displaycat').show();

                      $('#category1').attr('required','true');
                      } else {

                        $('.displaycat').hide();
                        $('#category1').removeAttr('required');
                      }


                }

    /* Mobile Number Validation */
       function isNumber(evt) {
       evt = (evt) ? evt : window.event;
       var charCode = (evt.which) ? evt.which : evt.keyCode;
       if (charCode > 31 && (charCode < 48 || charCode > 57)) {
       return false;
       }
       return true;
       }
     
    </script> 

<script src="{{ url('')}}/themes/js/jquery.min.js"></script> 
<script src="{{ url('')}}/themes/js/multiple-select.js"></script> 
@if(Session::get('lang_file') =='ar_lang') 

<script>
    $(function() {
        $('#ms_size').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });
      });
</script>
 @else 
<script>
    $(function() {
        $('#ms_size').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });
     });
</script> 
@endif 
<script type="text/javascript">
  
function show1(){
  document.getElementById('rentprice').style.display ='block';
}
function show2(){
  document.getElementById('rentprice').style.display = 'none';
}

</script>
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header')
@php $acoustics_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Acoustics_Info_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Acoustics_Info_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Acoustics_Info_Menu') }} @endif</h5>
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> {!! Form::open(array('url' => '/add_acousticsinfo','method' => 'POST', 'id'=> 'add_singerinfo','enctype' =>'multipart/form-data', 'name' =>'add_singerinfo' )) !!}
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_Singer_Name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Singer_Name'); @endphp </span> </label>
                <div class="info100">
                  <div class="english">
                    <input name="singer_name" maxlength="70" value="{{$getSinger->name or ''}}" id="singer_name" required="" type="text">
                  </div>
                  <div  class="arabic ar" >
                    <input class="arabic ar" name="singer_name_ar" maxlength="70" value="{{$getSinger->name_ar or ''}}" id="singer_name_ar" required="" type="text">
                  </div>
                </div>
              </div>
              
              <div class="form_row_right common_field">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_URL'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_URL'); @endphp </span> 
                <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a>
                </label>
                
                <div class="info100">
                  <div>
                    <input maxlength="250" type="url" name="google_map_url" value="{{ $getSinger->google_map_url or '' }}">
                  </div>
                </div>
              </div>
              
              
            </div>
            
            <div class="form_row">
              
              <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESS'); @endphp </span> </label>
                <div class="info100">
                  <div class="english">
                    <input name="address" maxlength="250" value="{{$getSinger->address or ''}}" id="address"  type="text">
                  </div>
                  <div  class="arabic ar" >
                    <input class="arabic ar" name="address_ar" maxlength="250" value="{{$getSinger->address_ar or ''}}" id="address_ar"  type="text">
                  </div>
                </div>
              </div>

              <div class="form_row_right  common_field">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp </span> </label>
                <div class="info100">
                  <select class="small-sel" id="singer_city" name="singer_city">
                    


<option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_CITY') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY') }} @endif</option>

                        @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;">{{$cbval->co_name}}</option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                        @php $ci_name='ci_name'@endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_'.$mer_selected_lang_code; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" @if(isset($getSinger->city_id) && ($getSinger->city_id == $val->ci_id)) SELECTED @endif>{{ $val->$ci_name }}</option>
                        @endforeach
                        @endforeach





                  </select>
                </div>
              </div>
            </div>
            
            <div class="form_row common_field">
              
              <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_Singer_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Singer_Image'); @endphp </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div id="file_value2" class="file-value"></div>
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                    </div>
                    <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                    </label>
                    <input type="hidden" name="singer_image_url" value="{{$getSinger->image or ''}}">
                    @if(!isset($getSinger->image) )
                    <input required="" id="company_logo1" name="singer_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" id="singer_image" class="info-file" value="{{$getSinger->image or ''}}" type="file">
                    @else
                    <input  id="company_logo1" name="singer_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" id="singer_image" class="info-file" value="{{$getSinger->image or ''}}" type="file">
                    @endif </div>
                  <div class="error certifications"> </div>
                  @if(isset($getSinger->image) && $getSinger->image!='' ) <img src="{{$getSinger->image or ''}}"> @endif </div>
              </div>
              
              <div class="form_row_right">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESS_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESS_IMAGE'); @endphp </span><a href="javascript:void(0);" class="address_image_tooltip"> <span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_office_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_office_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_office_address_img') }} @endif</span> </a> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo9">
                    <div class="file-btn-area">
                      <div id="file_value8" class="file-value"></div>
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                    </div>
                    </label>
                    <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                    <input type="hidden" name="address_image_name" value="{{$getSinger->address_image or ''}}">
                    @if(!isset($getSinger->address_image) )
                    <input  id="company_logo9" name="address_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" value="{{$getSinger->address_image or ''}}" type="file">
                    @else
                    <input  id="company_logo9" name="address_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" value="{{$getSinger->address_image or ''}}" type="file">
                    @endif </div>
                  <div class="error certifications"> </div>
                  @if(isset($getSinger->address_image) && $getSinger->address_image!='' ) <img src="{{$getSinger->address_image or ''}}"> @endif </div>
              </div>
            </div>
      
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_About_Singer'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_About_Singer'); @endphp </span> </label>
                <div class="info100">
                  <div class="english">
                    <textarea name="about_singer" id="about_singer" rows="4" cols="50" class="english" maxlength="500">{{$getSinger->about or ''}}</textarea>
                  </div>
                  <div  class="arabic ar" >
                    <textarea class="arabic ar" name="about_singer_ar" id="about_singer_ar" rows="4" cols="50"  maxlength="500">{{$getSinger->about_ar or ''}}</textarea>
                  </div>
                </div>
              </div>
              <div class="form_row_right english">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                    </div>
                    </label>
                    <input type="hidden" name="tnc_url" value="{{$getSinger->terms_conditions or ''}}">
                    @if(!isset($getSinger->terms_conditions) )
                    <input required="" id="company_logo" name="tnc" class="info-file" value="" type="file">
                    @else
                    <input id="company_logo" name="tnc" class="info-file" value="" type="file">
                    @endif </div>

<div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
@if(isset($getSinger->terms_conditions) && $getSinger->terms_conditions!='' )

              <a href="{{$getSinger->terms_conditions}}" target="_blank" class="pdf_icon"><img src="{{url('/themes/images/pdf.png')}}"> <span>{{$getSinger->terms_condition_name or ''}}</span></a>
                  <input type="hidden" name="tnc_name" value="{{$getSinger->terms_condition_name or ''}}">
                  @endif
                  <div class="error certifications"> </div>
                </div>
              </div>
            </div>
            
            <div class="arabic ar">
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                <div class="info100">
                  <div class="input-file-area">
                    <label for="company_logo11">
                    <div class="file-btn-area">
                      <div id="file_value10" class="file-value"></div>
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                    </div>
                    </label>
                    <input type="hidden" name="tnc_url_ar" value="{{$getSinger->terms_conditions_ar or ''}}">
                    @if(!isset($getSinger->terms_conditions_ar) )
                    <input required="" id="company_logo11" name="tnc_ar" class="info-file" value="" type="file">
                    @else
                    <input id="company_logo11" name="tnc_ar" class="info-file" value="" type="file">
                    @endif </div>
<div class="pdf_msg"><span class="english">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp </span></div>
@if(isset($getSinger->terms_conditions) && $getSinger->terms_conditions!='' )

              <a href="{{$getSinger->terms_conditions_ar}}" target="_blank" class="pdf_icon"><img src="{{url('/themes/images/pdf.png')}}"> <span>{{$getSinger->terms_condition_name_ar or ''}}</span></a>
                  <input type="hidden" name="tnc_name_ar" value="{{$getSinger->terms_condition_name_ar or ''}}">
                  @endif
                  <div class="error certifications"> </div>
                </div>
              </div>
            </div>
            </div>
            
            <div class="form_row">
              <div class="form_row_left">
              <div class="arabic ar">
                <input type="hidden" name="hid" value="{{$getSinger->id or ''}}">
                <input type="hidden" name="id" value="{{request()->id}}">
                <input type="submit" name="submit" value="خضع">
              </div>
              <div class="english">
                <input type="submit" name="submit" value="Submit">
              </div>
              </div>
              
              <!-- form_row --> 
            </div>
            {!! Form::close() !!} 
            <!-- one-call-form --> 
            <!-- Display Message after submition --> 
            
            <!-- Display Message after submition --> 
            <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script type="text/javascript">     
$("#add_singerinfo").validate({
                  ignore: [],
                  rules: {
                          singer_name: {
                          required: true,
                          },
                          singer_name_ar: {
                          required: true,
                          },
                           address_image: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                          address: {
                          required: true,
                          },
                          singer_image: {
                          accept:"png|jpe?g|gif",
                          },
                           address_image: {
                          accept:"png|jpe?g|gif",
                          },                 
                          singer_city: {
                          required: true,
                          },
                          about_singer: {
                          required: true,
                          },
                          about_singer_ar: {
                          required: true,
                          },



                     @if(!isset($getSinger->terms_conditions))                         
                          tnc: {
                            required: true,
                           accept:"pdf",
                          }, 
                          tnc_ar: {
                            required: true,
                           accept:"pdf",
                          },                         
                      @else
                      tnc: {
                            required: false,
                           accept:"pdf",
                          },
                           tnc_ar: {
                            required: false,
                           accept:"pdf",
                          }, 

                      @endif     
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                 
           messages: {
                  singer_name: {
                  required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_SINGER_NAME'); @endphp",
                  },  
                  singer_name_ar: {
                  required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SINGER_NAME_ARABIC'); @endphp",
                  },

                    address: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },
  
                       address_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },

                         
                  singer_image: {

                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                  
                  }, 
                  singer_city: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",
                  },  
                  about_singer: {
                  required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
                  },   
                  about_singer_ar: {
                 required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER_ARABIC'); @endphp ",
                  },
                   address_image: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      }, 
                  tnc: {
                                required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); @endphp",
                                accept: "@php echo lang::get('mer_en_lang.MER_VALIDATION_VALID_FILE'); @endphp",
                        },

                       tnc_ar: {
                                required: "@php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); @endphp",
                                accept: "@php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); @endphp",
                      },  
 
                },
                invalidHandler: function(e, validation){
                      var valdata=validation.invalid;
                   @if($mer_selected_lang_code !='en')

                      if (typeof valdata.singer_name != "undefined" || valdata.singer_name != null) 
                      {
                      $('.english_tab').trigger('click'); 
                      }
                        if (typeof valdata.singer_image != "undefined" || valdata.singer_image != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }
                        if (typeof valdata.singer_city != "undefined" || valdata.singer_city != null) 
                      {
                      $('.english_tab').trigger('click');    

                      }

                        if (typeof valdata.about_singer != "undefined" || valdata.about_singer != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }
                     
                        if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                      {
                      $('.english_tab').trigger('click');     

                      }

                         if (typeof valdata.singer_name_ar != "undefined" || valdata.singer_name_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }

                      if (typeof valdata.about_singer_ar != "undefined" || valdata.about_singer_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }
                      if (typeof valdata.tnc_ar != "undefined" || valdata.tnc_ar != null) 
                      {
                      $('.arabic_tab').trigger('click');     

                      }


                      @else

                            if (typeof valdata.singer_name_ar != "undefined" || valdata.singer_name_ar != null) 
                            {
                            $('.arabic_tab').trigger('click'); 
                            }
                            
                            

                             

                              if (typeof valdata.about_singer_ar != "undefined" || valdata.about_singer_ar != null) 
                            {
                            $('.arabic_tab').trigger('click');     

                            }                         
                          if (typeof valdata.tnc_ar != "undefined" || valdata.tnc_ar != null) 
                            {
                            $('.arabic_tab').trigger('click');     

                            }
                              if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }
                            if (typeof valdata.singer_city != "undefined" || valdata.singer_city != null) 
                            {
                            $('.english_tab').trigger('click');    

                            }

                              if (typeof valdata.singer_image != "undefined" || valdata.singer_image != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }


                              if (typeof valdata.about_singer != "undefined" || valdata.about_singer != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }
                              if (typeof valdata.singer_name != "undefined" || valdata.singer_name != null) 
                            {
                            $('.english_tab').trigger('click');     

                            }



                      @endif

                      },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
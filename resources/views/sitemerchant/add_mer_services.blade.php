@include('sitemerchant.includes.header')  
@php $serviceleftmenu =1; @endphp  
<div class="merchant_vendor">     


<div class="profile_box">
		 
			<div class="inner">
			
			<header>
						 
			<h5 class="global_head gds"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_SERVICESS') }}  
			@else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICESS') }} @endif </h5><div class="backs"><a href="{{ url('sitemerchant_dashboard') }}" >
			
			 @if (Lang::has(Session::get('mer_lang_file').'.BACK')!= '') {{  trans(Session::get('mer_lang_file').'.BACK') }}  
			@else  {{ trans($MER_OUR_LANGUAGE.'.BACK') }} @endif 
			 


			
			</a></div>
			</header>
                    <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->       
			<div class="global_area">	
				<div class="row">
				<div class="col-lg-12">
					<div class="add_service_page">
                    <div class="box commonbox">
			 
			@php $Checkservics=array(); @endphp

			@foreach($GetAlreadyServices as $vals)
			@php $Checkservics[] = $vals->service_id; @endphp
			@endforeach
 
			<!--PAGE CONTENT PART WILL COME INSIDE IT START-->

		{!! Form::open(array('url' => '/addservices','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'addservices', 'name' =>'addservices' )) !!}

			@foreach($getCatlists as $catlists)		  
			@php                
			$par_id = $catlists->mc_id;
			if($par_id == '1'){ continue;}
			$SecondCats=Helper::getChildCats($par_id);
			@endphp

			@foreach($SecondCats as $Seccatlists)
			@php
			$DataId = $Seccatlists->mc_id;
			@endphp
			<div class="addservices-row">
  			<div class="addservices-name">
         	@if (Config::get('app.locale') == 'ar') 
        	 {{$Seccatlists->mc_name_ar}}
        	@else 
        	 {{$Seccatlists->mc_name}}  
        	@endif 
 		     </div>

			@php                
			$par_id = $Seccatlists->mc_id;

			$ThirdCats=Helper::getChildCats($par_id);
			@endphp
			@if(count($ThirdCats) >=1 && $par_id !='37' && $par_id !='871' && $par_id !='87')
			@foreach($ThirdCats as $thicatlists)
			@php
			$DataIds = $thicatlists->mc_id;
			if($thicatlists->mc_id ==16){ continue;}
			@endphp
 			<div class="addservices-cell">
		        <input id="{{$thicatlists->mc_id}}" type="checkbox" name="cat[]" @php if(in_array($DataIds, $Checkservics)){ echo "CHECKED";  echo ' disabled=true'; }  @endphp  value="{{$thicatlists->mc_id}}"> 
		       <label for="{{$thicatlists->mc_id}}">
		        	@if (Config::get('app.locale') == 'ar') 
		        	{{$thicatlists->mc_name_ar}}
		        	@else 
		        	{{$thicatlists->mc_name}}
		        	@endif 
		      </label>
		      	</div>

                 
			@endforeach

 			@else
					<div class="addservices-cell"> 
					<input id="{{$Seccatlists->mc_id}}" type="checkbox" name="cat[]" @php if(in_array($DataId, $Checkservics)){ echo "CHECKED"; echo ' disabled=true'; }  @endphp  value="{{$Seccatlists->mc_id}}"> 
					<label for="{{$Seccatlists->mc_id}}">
					@if (Config::get('app.locale') == 'ar') 
					{{$Seccatlists->mc_name_ar}}
					@else 
					{{$Seccatlists->mc_name}}
					@endif 
					</label>
					</div>


                    @endif

			</div> <!-- addservices-row -->
			@endforeach
			@endforeach
 
            <input type="submit" name="submit" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_SERVICES') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICES') }} @endif">

        {!! Form::close() !!}


				<!--PAGE CONTENT PART WILL COME INSIDE IT END-->

					</div>
                    </div> <!-- add_service_page -->
				</div>
				</div>
				</div> <!--  global_area  -->
	 		</div>
 
		</div> <!-- right_panel -->
		       
                
</div> <!-- merchant_vendor -->
@include('sitemerchant.includes.footer')
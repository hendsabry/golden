@include('sitemerchant.includes.header') 
@php $MakupArtistbig_leftmenu =1; @endphp
<div class="merchant_vendor">   @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if($autoid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_SERVICES') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICES') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_SERVICES') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_SERVICES') }} @endif </h5>
        @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box arabic-ltr"> 
              
              <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->
              
              <form name="makeupartist" id="makeupartist" method="post" action="{{ route('store-service') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left common_field">
                   
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.CATEGORY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.CATEGORY'); @endphp</span> </label>
                      <div class="info100">
                      
                          <select class="small-sel" name="attribute_id" id="attribute_id">
                            <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }}  
                            @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>
                            
                                    @foreach ($attributesname as $val)
                                   @php $ci_name='attribute_title'@endphp
                                   @if($mer_selected_lang_code !='en')
                                   @php $ci_name= 'attribute_title_ar'; @endphp
                                  @endif    
                                  
                            <option value="{{ $val->id }}" {{ isset($fetchdata->attribute_id) && $fetchdata->attribute_id ==$val->id ? 'selected' : ''}}>{{ $val->$ci_name }}</option>
                            
                                @endforeach
                                  
                          </select>
                      
                      </div>
                    
                  </div>
				  
                  <div class="form_row_right arabic_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.mer_service_name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_service_name'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="service_name" maxlength="40" value="{{ $fetchdata->pro_title or '' }}" id="service_name"  >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="arabic ar" name="service_name_ar" maxlength="40" value="{{ $fetchdata->pro_title_ar or '' }}" id="service_name_ar" >
                      </div>
                    </div>
                  </div>
                </div>
                <!-- form_row -->
                
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                      <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                         @if(isset($fetchdata->pro_Img)!='')
                      <div class="form-upload-img"> <img src="{{ $fetchdata->pro_Img or '' }}" width="150" height="150"> </div>
                      @endif
                    </div>
                  </div>


      
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->







                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_Duration'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Duration'); @endphp </span> </label>
                    <div class="info100">
                      <div>
                        <input type="text" class="xs_small" name="duration" onkeypress="return isNumber(event)"  maxlength="9" value="{{ $fetchdata->service_hour or '' }}" id="duration"  >
                      </div>
                    </div>
                  </div>
                  
                  <!-- form_row --> 
                </div>
                
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> </label>
                    <div class="info100">
                 
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="price" maxlength="9" value="{{ $fetchdata->pro_price or '' }}" id="price" required="" >
                      
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp (%)</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp (%) </span> </label>
                    <div class="info100">
                     
                        <input type="text" class="xs_small notzero" name="discount_price"  onkeypress="return isNumber(event)"  maxlength="2" value="{{ $fetchdata->pro_discount_percentage or '' }}" id="discount_price" >
                      
                    </div>
                  </div>
                  <!-- form_row --> 
                </div>
                <!-- form_row -->
                
                <div class="form_row common_field">
                  <div class="form_row_right">
                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_HALL_UPLOADVIDEO'); @endphp </span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_youtube_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_youtube_add') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_youtube_add') }} @endif</span></a> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HALL_UPLOADVIDEO'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="url"  name="video_url" maxlength="400" value="{{ $fetchdata->video_url or '' }}" id="video_url" >
                        <div class="ex-show">Ex. https://youtu.be/IY9cIpidnOc</div>
                      </div>
                    </div>
                     @if(isset($fetchdata->video_url) && $fetchdata->video_url!='')<div class="hall-video-area">
                      <iframe class="hall-video-ifrem" src="{{$fetchdata->video_url or ''}}"></iframe>
                     </div>  @endif
                  </div>
                  
                  <!-- form_row --> 
                </div>
                <input type="hidden" name="id" value="{{ $id }}">
                        <input type="hidden" name="sid" value="{{ $itemid }}">
                        <input type="hidden" name="autoid" value="{{ $autoid }}">
                <div class="form_row">
                  <div class="form_row_left arabic_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea name="description" class="english" id="description" rows="4" cols="50">{{ $fetchdata->pro_desc or '' }} </textarea>
                      </div>
                      <div  class="arabic ar" >
                        <textarea class="arabic ar" name="description_ar" id="description_ar" rows="4" cols="50">{{ $fetchdata->pro_desc_ar or '' }}</textarea>
                      </div>
                    </div>
                    <!-- form_row --> 
                  </div>
                </div>
                
                <div class="form_row">
<div class="form_row_left">
                    <div class="english">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <!-- form_row -->
                    <div class="arabic ar">
                   
                      <input type="submit" name="submit" value="خضع">
                       </div>
                    </div>
                    <!-- form_row --> 

                  
                </div>
              </form>
              
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
</div>
<!-- merchant_vendor --> 

 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
   }    
   });  
   }

</script>
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup --> 
<script>
/* Action Popup */
jQuery('.cstatus').click(function(){
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id);
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-service-image') }}",
        data: {id:id},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script>
      
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#makeupartist").validate({

                  ignore: [],
                  rules: {
                  attribute_id: {
                       required: true,
                      },

                       service_name: {
                       required: true,
                      },

                      service_name_ar: {
                       required: true,
                      },

                       duration: {
                       required: true,
                      },
                       price: {
                       required: true,
                      },

                      year_of_exp: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                      @if(isset($getDb)!='')
                      "image[]": {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @else 
                         "image[]": {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif

                       @if(isset($fetchdata->pro_Img)!='')  
                        stor_img: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        stor_img: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             attribute_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",
                      },  

                          duration: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_DURATION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_DURATION') }} @endif",
                      }, 
               
                  service_name: {
          

               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_SERVICENAME'); @endphp",
                      },   

                          service_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SERVICENAME_AR'); @endphp",
                      },   

             

                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                      },


                       year_of_exp: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXP') }} @endif ",
                      },
                     description: {
 required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
 
                      },

                       description_ar: {
                         required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_SINGER_AR'); @endphp",
           
                      },

                  stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },   

                       "image[]": {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",

                      },                                 
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  @if($mer_selected_lang_code !='en')
                    
                     if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                      if (typeof valdata.image != "undefined" || valdata.image != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.arabic_tab').trigger('click');
                           

                    }
                      if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    

                    }
                   
                  
                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
          @else


                          if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                          {

                          $('.arabic_tab').trigger('click');     


                          }

                          if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                          {

                          $('.arabic_tab').trigger('click');     

                          }




                          if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                          {

                          $('.english_tab').trigger('click');     

                          }

                          if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                          {
                          $('.english_tab').trigger('click');


                          }

                          if (typeof valdata.price != "undefined" || valdata.price != null) 
                          {
                          $('.english_tab').trigger('click'); 


                          }

                          if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                          {
                          $('.english_tab').trigger('click'); 

                          }
                          if (typeof valdata.description != "undefined" || valdata.description != null) 
                          {
                          $('.english_tab').trigger('click');


                          }
                          if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                          {
                          $('.english_tab').trigger('click'); 


                          }

                    
                @endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script> 


<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->


@include('sitemerchant.includes.footer')
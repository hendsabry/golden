@include('sitemerchant.includes.header') 
@php $reception_hospitality_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.reception_hospitality')!= '') {{  trans(Session::get('mer_lang_file').'.reception_hospitality') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.reception_hospitality') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      </header>
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <form name="form1" method="post" id="reception" action="{{ route('update-shop-info',['id' => $parent_id,'shopid' => request()->shopid]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_NAME'); @endphp </span></label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="{{ $fetchdata->mc_name or ''}}" id="mc_name"  class="english"  data-validation-length="max80" maxlength="80">
                      </div>
                      <div class="arabic ar">
                        <input  class="arabic ar" value="{{ $fetchdata->mc_name_ar or ''}}" name="mc_name_ar"  id="mc_name_ar"   type="text" data-validation-length="max80"  maxlength="80" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_right common_field">
                    <label class="form_label posrel"><span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS_URL'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS_URL'); @endphp </span> <a href="javascript:void(0);" class="tooltip_area_wrap"> <span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_google_add') }} @endif</span> </a> </label>
                    <div class="info100">
                      <input type="url" name="google_map_address" id="google_map_address" value="{{ $fetchdata->google_map_address or '' }}" data-validation-length="max80">
                    </div>
                  </div>
                </div>

<span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LONG')!= '') {{  trans(Session::get('mer_lang_file').'.LONG') }}  @else  {{ trans($OUR_LANGUAGE.'.LONG') }} @endif </label>
                  <input type="text" class="form-control" value="{{$fetchdata->longitude or ''}}" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LAT')!= '') {{  trans(Session::get('mer_lang_file').'.LAT') }}  @else  {{ trans($OUR_LANGUAGE.'.LAT') }} @endif </label>
                  <input type="text" class="form-control" value="{{$fetchdata->latitude or ''}}"  readonly="" name="latitude" />
          </div>
                  </div>

                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_ADDRES'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_ADDRES'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text"  name="address" value="{{ $fetchdata->address or ''}}" >
                      </div>
                      <div class="arabic ar">
                        <input type="text"  name="address_ar" value="{{ $fetchdata->address_ar or ''}}" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); @endphp </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="city_id"  id="city_id">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif</option>
                        
                          
               @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        
                          
          <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;"> @if($mer_selected_lang_code !='en') {{$cbval->co_name_ar}} @else {{$cbval->co_name}} @endif</option>
                        
                          
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                         @php $ci_id = $val->ci_id; @endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        
                          
                        <option value="{{ $val->ci_id }}" {{ isset($fetchdata->city_id) && $fetchdata->city_id ==$val->ci_id ? 'selected' : ''}} >{{ $val->$ci_name }}</option>
                        
                          
                        @endforeach
                        @endforeach



                                      
                    
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label"><span class="english">@php echo lang::get('mer_en_lang.MER_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_IMAGE'); @endphp </span></label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"><span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span></div>
                        </div>
                        </label>
                        <span class="msg_img_replace"> @if(!isset($getDb))
                        @if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif
                        @endif </span>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" >
                        <input type="hidden" name="updateimage" value="{{ $fetchdata->mc_img or ''}}">
                        @if(isset($fetchdata->mc_img) &&  $fetchdata->mc_img!='')
                        @php
                        $img = $fetchdata->mc_img;
                        
                        @endphp
                        <div class="form-upload-img"> <img src="{{ $img or '' }}"  ></div>
                        @endif </div>
                    </div>
                  </div>
                  <!--div class="form_row_right">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label"><span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="address_image_tooltip"> <span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span> </a> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo1">
                        <div class="file-btn-area">
                          <div id="file_value2" class="file-value"></div>
                          <div class="file-btn"><span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span></div>
                        </div>
                        <span class="msg_img_replace"> @if(!isset($getDb))
                        @if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif
                        @endif </span>
                        </label>
                        <input id="company_logo1" name="address_image" class="info-file" type="file" >
                        @if(isset($fetchdata->address_image) &&  $fetchdata->address_image!='')
                        <div class="form-upload-img"> <img src="{{ $fetchdata->address_image }}" width="150" height="150"> </div>
                        <input type="hidden" name="update_add_img" value="{{ $fetchdata->address_image or '' }}">
                        @endif </div>
                    </div>
                  </div-->


                </div>
                 <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100" >
                      <div class="english">
                        <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50">{{ $fetchdata->mc_discription or ''}} </textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50">{{ $fetchdata->mc_discription_ar or ''}}</textarea>
                      </div>
                    </div>
                  </div>
                <div class="form_row">
                  <div class="form_row_left english">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.TERMSANDCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.TERMSANDCONDITION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo9">
                        <div class="file-btn-area">
                          <div id="file_value8" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo9" name="mc_tnc" accept="pdf" class="info-file" type="file" >
                        @if(isset($fetchdata->terms_conditions) &&  $fetchdata->terms_conditions!='')
                        <div class="pdf_msg">Please upload only PDF file</div>
                        <div class="form-upload-img"> <a class="pdf_icon" href="{{ $fetchdata->terms_conditions }}" target="_blank"> <img src="{{url('/themes/images/pdf.png')}}"> <span>{{$fetchdata->terms_condition_name or ''}}</span></a> </div>
                        <input type="hidden" name="updatemc_tnc" value="{{ $fetchdata->terms_condition_name or '' }}">
                        @endif </div>
                    </div>
                  </div>
                  <div class="form_row_left arabic ar">
                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.TERMSANDCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.TERMSANDCONDITION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo10">
                        <div class="file-btn-area">
                          <div id="file_value9" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo10" name="mc_tnc_ar" accept="pdf" class="info-file" type="file" >
                        <div class="pdf_msg">يرجى تحميل ملف PDF فقط </div>
                        @if(isset($fetchdata->terms_conditions_ar) &&  $fetchdata->terms_conditions_ar!='')
                        <div class="form-upload-img"> <a class="pdf_icon" href="{{ $fetchdata->terms_conditions_ar }}" target="_blank"> <img src="{{url('/themes/images/pdf.png')}}"> <span>{{$fetchdata->terms_condition_name_ar or ''}}</span></a> </div>
                        <input type="hidden" name="updatemc_tnc_ar" value="{{ $fetchdata->terms_condition_name_ar or '' }}">
                        @endif </div>
                    </div>
                  </div>
                </div>

                 <div class="form_row_left common_field">
                    <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif </label>
                    <div class="info100" >
                     <select class="city_type" name="mc_status" required="">
                       <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif </option>

                      <option value="1" <?php  if(isset($fetchdata->mc_status) && $fetchdata->mc_status==1){?> SELECTED <?php } ?>>@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif </option>
                     <option value="0" <?php if(isset($fetchdata->mc_status) && $fetchdata->mc_status==0){?> SELECTED <?php } ?>> @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif  </option>
                   </select>
                    
                    </div>
                  </div>

                  
                <div class="form_row">
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="shopid"  value="{{request()->shopid}}">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="Update">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="تحديث">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>       
$("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
$("#reception").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      }, 

                  mc_name: {
                       required: true,
                      },
                       address: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },
                      google_map_address: {
                       required: false,
                      },
    @if(isset($fetchdata->terms_conditions) &&  $fetchdata->terms_conditions!='')
                      mc_img: {
                      
                       accept:"png|jpe?g|gif",
                      },

                       address_image: {
                     
                       accept:"png|jpe?g|gif",
                      },

                       mc_tnc: {
                      required: false,  
                       accept:"pdf",
                      },
                        mc_tnc_ar: {
                       required: false,  
                       accept:"pdf",
                      },

                       about: {
                       required: true,
                      },
                      
                       about_ar: {
                       required: true,
                      },
                      
              @else

                mc_img: {
                    required: true,
                       accept:"png|jpe?g|gif",
                      },

                       address_image: { 
                       required: false,                  
                       accept:"png|jpe?g|gif",
                      },

                       mc_tnc: {  
                       required: true,                
                       accept:"pdf",
                      },
                    mc_tnc_ar: {
                       required: true,  
                       accept:"pdf",
                      },
                      

              @endif 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             city_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",
                      },
             mc_name: {

                required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_SHOP_NAME'); @endphp",
               
                      },  

                  mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOP_NAME_AR'); @endphp",
                      },

                      address: {

                        required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ADDRESS'); @endphp",

              
                      },

                      about: {

                         required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); @endphp",
               
                      },
  
                       about_ar: {
                        required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); @endphp",
                      },
                
  
                       address_ar: {
                       required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },

                        google_map_address: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_URL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_URL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SHOP_URL') }} @endif",
                      },    

                        mc_img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      }, 

                       address_image: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },    
                      mc_tnc: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_TNC')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_TNC') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_TNC') }} @endif",
                 accept: "@if (Lang::has(Session::get('lang_file').'.VALIDFILEPDF')!= '') {{  trans(Session::get('lang_file').'.VALIDFILEPDF')}}  @else {{ trans($MER_OUR_LANGUAGE.'.VALIDFILEPDF')}} @endif"
                      },   


                    mc_tnc_ar: {
                                required: "@php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); @endphp",
                                accept: "@php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file_ar'); @endphp",
                      },                    
                     
                },
               
                 invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                @if($mer_selected_lang_code !='en')
                    if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }

                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.arabic_tab').trigger('click');
                        

                    }
                    if (typeof valdata.address_image != "undefined" || valdata.address_image != null) 
                    {

                        $('.arabic_tab').trigger('click');
                        

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
          @else


             if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }


                    if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
 
            if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }

                     if (typeof valdata.address != "undefined" || valdata.address != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }


                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }

                    if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                     
                    }
 
                  

@endif

                    },

                submitHandler: function(form) {
                       var mapAdd = jQuery('input[name=google_map_address]').val();
                  if(mapAdd !='')
                  {
                  var long = jQuery('input[name=longitude]').val();
                  var lat  =  jQuery('input[name=latitude]').val();
                  if(long =='' && lat=='')
                  {
                  var allOk = 0;

                  }
                  else
                  {
                  var allOk = 1; 
                  }
                  }
                  else
                  {
                  var allOk = 1;
                  }

                  if(allOk == 1)
                  {
                  form.submit();
                  }
                  else
                  {
                  $('#maperror').html("<span class='error'>@if (Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= '') {{  trans(Session::get('mer_lang_file').'.get_Lat_Long_issue') }} @else  {{ trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue') }} @endif</span>");
                  }
                }
            });

</script>
@include('sitemerchant.includes.footer')
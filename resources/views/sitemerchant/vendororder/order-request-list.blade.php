@include('sitemerchant.includes.header')

@php
   // echo $merchantmanagerid=Session::get('merchant_managerid');
    // echo $LoginType   = Session::get('LoginType');
@endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="service_listingrow">
    <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ORDERS')!= '') {{  trans(Session::get('mer_lang_file').'.ORDERS') }}  
      @else  {{ trans($MER_OUR_LANGUAGE.'.ORDERS') }} @endif</h5>
  </div>
  
  <div class="global_area">
    <div class="row">
      <div class="col-lg-12">
        <div class="table_wrap">
		  
          <div class="panel-body panel panel-default">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            @if($vendororder->count() < 1)
            <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
            @else
			
            <div class="table dish-table">
              <div class="tr">
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Order_id')!= '') {{  trans(Session::get('mer_lang_file').'.Order_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Order_id') }} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</div>
                <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.ORDERFOR')!= '') {{  trans(Session::get('mer_lang_file').'.ORDERFOR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDERFOR') }} @endif</div>

                <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DATE') }} @endif</div>

                 
                  <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIONS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIONS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIONS') }} @endif</div>
              </div>
              @php $totalamount =0;  $detailedurl=''; $chkArry= array(); @endphp
              
              @foreach($vendororder as $value)
                @php
                Helper::updatevendororderstatus($value->id);
                $OrderID = $value->order_id;
                $product_sub_type = $value->product_sub_type;
                $shop_id = $value->shop_id;
                $chkSum = $OrderID.'_'.$shop_id.'_'.$product_sub_type;

                if(in_array($chkSum,$chkArry)){ continue;}
                array_push($chkArry,$chkSum);

                $userinfo=Helper::getuserinfo($value->cus_id);                  
                $pid=Helper::getparentCat($value->category_id);

                 
              if($value->product_sub_type=='hospitality'){ $detailedurl='reception-list-orders/14/'.$value->category_id; }
               if($value->product_sub_type=='dessert'){ $detailedurl='dessert-order/9/'.$pid->parent_id.'/'.$value->category_id; }
               if($value->product_sub_type=='events'){ $detailedurl='event-order/17/'.$value->category_id; }
               if($value->product_sub_type=='roses'){ $detailedurl='roses-order/15/'.$value->category_id; }
               if($value->product_sub_type=='cosha'){ $detailedurl='cosha-order/12/'.$value->category_id; }
               if($value->product_sub_type=='acoustic' || $value->product_sub_type=='recording'){                 
                $getSingerCid = Helper::getsingerid(27);
               $detailedurl='acoustic-order/27/'.$getSingerCid.'/'.$value->category_id; }
                if($value->product_sub_type=='perfume'){ $detailedurl='oudandperfumes-order/94/'.$pid->parent_id.'/'.$value->category_id; }
                 if($value->product_sub_type=='dress'){ $detailedurl='dresses-order/34/'.$value->category_id; }
                  if($value->product_sub_type=='abaya'){ $detailedurl='abaya-order/36/'.$value->category_id; }
                  if($value->product_sub_type=='tailor'){ $detailedurl='tailor-order/32/'.$value->category_id; }
                  if($value->product_sub_type=='car'){ $detailedurl='car-order/37/'.$value->category_id; }
                  if($value->product_sub_type=='men_saloon'){ $detailedurl='mens-order/20/'.$value->category_id; }
                   if($value->product_sub_type=='cosmetic'){ $detailedurl='laser-cosmetic-order/29/'.$value->category_id.'/'.$value->shop_id; }
                   if($value->product_sub_type=='buffet'){ $detailedurl='buffet-orders/8/'.$pid->parent_id.'/'.$value->category_id; }
                   if($value->product_sub_type=='dates'){ $detailedurl='dates-order/10/'.$value->category_id; }
                    if($value->product_sub_type=='beauty_centers'){ $detailedurl='beauty-order/19/'.$value->category_id.'/'.$value->shop_id; }
                    if($value->product_sub_type=='travel'){ $detailedurl='travel-order/87/'.$value->category_id; }

                      if($value->product_sub_type=='gold'){ $detailedurl='goldandjewelry-order/95/'.$pid->parent_id.'/'.$value->category_id; }
                      if($value->product_sub_type=='photography' || $value->product_sub_type=='video' ){ $detailedurl='photography-order/127/'.$value->category_id; }

                      if($value->product_sub_type=='singer'){ $detailedurl='singer-order-list/25/'.$value->shop_id; }

                      if($value->product_sub_type=='band'){ $detailedurl='popular-band-order-list/26/'.$value->shop_id; }
                      
                     if($value->product_sub_type=='makeup_artists'){ $detailedurl='makeup-artist-order/22/'.$value->category_id; }
                     if($value->product_sub_type=='makeup'){ $detailedurl='makeup-order/23/'.$value->category_id; }

                     if($value->product_sub_type=='spa'){ $detailedurl='spa-order/21/'.$value->category_id.'/'.$value->shop_id; }
                     if($value->product_sub_type=='hall'){ $detailedurl='hall-order?hid='.$value->product_id.'&bid='.$value->category_id; }
              @endphp

            
              <div class="tr">
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Order_id')!= '') {{  trans(Session::get('mer_lang_file').'.Order_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Order_id') }} @endif">{{$value->order_id or ''}} </div>
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif">

                  {{ $userinfo->cus_name or '' }}</div>
                <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif"> {{ $userinfo->email or '' }}  </div>
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.ORDERFOR')!= '') {{  trans(Session::get('mer_lang_file').'.ORDERFOR') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.ORDERFOR') }} @endif">  {{ $value->product_sub_type or ''}}  </div>

                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Event_DATE_TIME')!= '') {{  trans(Session::get('mer_lang_file').'.Event_DATE_TIME') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Event_DATE_TIME') }} @endif">   {{ Carbon\Carbon::parse($value->created_at)->format('F j, Y')}}    </div>
                

                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Note')!= '') {{  trans(Session::get('mer_lang_file').'.Note') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Note') }} @endif"> 
                  <a  href="{{ url('') }}/{{ $detailedurl }}">
                          <img src="{{url('')}}/public/assets/img/view-icon.png" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></a>


                        </div>
              </div>
              @endforeach </div>
            @endif
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          </div>
          {{ $vendororder->links() }} </div>
      </div>
    </div>
  </div>
</div>
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        @else
        $('#todata').html("End date should be greater than From date");
        @endif
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer') 
@include('sitemerchant.includes.header')
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="service_listingrow">
    <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.NOTIFICATION')!= '') {{  trans(Session::get('mer_lang_file').'.NOTIFICATION') }}  
      @else  {{ trans($MER_OUR_LANGUAGE.'.NOTIFICATION') }} @endif</h5>
  </div>
  
  <div class="global_area">
    <div class="row">
      <div class="col-lg-12">
        <div class="table_wrap">
		  
          <div class="panel-body panel panel-default">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            @if($msg->count() < 1)
            <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
            @else
			
            <div class="table dish-table">
              <div class="tr">
                <div class="table_heading" style="width:15px;">@if (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_S.NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_S.NO') }} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_MESSAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MESSAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MESSAGE') }} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_SENT_ON')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SENT_ON') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SENT_ON') }} @endif</div>
              </div>
              <?php $i=1; foreach($msg as $value) {?>            
              <div class="tr">
                <div class="td 1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_S.NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_S.NO') }} @endif"><?=$i?></div>
                <div class="td 2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_MESSAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MESSAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MESSAGE') }} @endif">{{$value->message}}</div>
                <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_SENT_ON')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SENT_ON') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SENT_ON') }} @endif">{{ Carbon\Carbon::parse($value->created_at)->format('F j, Y')}}</div>
              </div>
              <?php $i++;} ?>
			  </div>
            @endif
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          </div>
          {{ $msg->links() }} </div>
      </div>
    </div>
  </div>
</div>
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        @else
        $('#todata').html("End date should be greater than From date");
        @endif
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer') 
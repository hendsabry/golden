@inject('data','App\Help')
@include('sitemerchant.includes.header')  
 
@php $acoustics_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu') }} @endif</h5>
      </div>
      @php $id = request()->id; $hid= request()->hid;  $Cid= request()->Cid; @endphp
      {!! Form::open(array('url'=>"acoustics-quoted-requested-list/{$id}/{$hid}/{$Cid}",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
      @php $statuss = request()->status; $user_acceptance= request()->acceptance;  $searchh= request()->search; @endphp <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left">
          <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems">
            <select name="acceptance" id="acceptance">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_ACCEPT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_ACCEPT') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_ACCEPT') }} @endif </option>
              <option value="3" @if(isset($user_acceptance) && $user_acceptance=='3') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.YES')!= '') {{  trans(Session::get('mer_lang_file').'.YES') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.YES') }} @endif</option>
              <option value="2" @if(isset($user_acceptance) && $user_acceptance=='2') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.NO')!= '') {{  trans(Session::get('mer_lang_file').'.NO') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.NO') }} @endif</option>
            </select>
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
        </div>
        <div class="search_box">
          <div class="search_filter">&nbsp;</div>
          <div class="filter_right">
            <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$searchh or ''}}" />
            <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
          </div>
        </div>
      </div>
      <!-- filter_area -->
      {!! Form::close() !!}
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap">
              <!-- Display Message after submition -->
              <!-- Display Message after submition -->
              <div class="panel-body panel panel-default"> @if($getusers->count() <1)
                <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NORECORDFOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NORECORDFOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NORECORDFOUND')}} @endif </div>
                @else
                <div class="table">
                  <div class="tr">
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NAME') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Singer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Singer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Singer_Name') }} @endif</div>
                    <!--<div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} @endif</div> -->
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE') }} @endif</div>
                    <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                  </div>


                  @foreach($getusers as $vals)
                  @php
                  if($vals->id!='') $details=Helper::detailid($vals->id);
				  
                  @endphp
                  <div class="tr">
                    <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NAME') }} @endif">{{$vals->the_groooms_name}}</div>
                    <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Singer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Singer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Singer_Name') }} @endif">{{$vals->singer_name}}</div>
                    <!--<div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} @endif">{{$vals->
                    cus_phone or 'N/A'}}</div>
                  -->
                  <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE') }} @endif"> {{ Carbon\Carbon::parse($vals->created_at)->format('d M Y')}} </div>
                  <div class="td td5 view_center" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif"> 
				 
				  @if($vals->status==1) <a href="{{ route('acoustics-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itmid' => $vals->id,'autoid'=>'','Cid'=>$Cid]) }}"><img src="{{url('')}}/public/assets/img/view-icon.png" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></a> 
				  @elseif($vals->status==2) 
				  <a href="{{ route('acoustics-quoted-requested-view',['id' => request()->id,'hid' => request()->hid,'itmid' => $vals->id,'autoid'=>'', 'Cid'=>$Cid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Replied')!= '') {{  trans(Session::get('mer_lang_file').'.Replied') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Replied') }} @endif </a> @elseif($vals->status==3) <a href="{{ route('acoustics-quoted-requested-view',['id' =>$id,'hid' => $hid,'itmid' => $vals->id,'autoid'=>'','Cid'=>$Cid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Confirmed_by_customer')!= '') {{  trans(Session::get('mer_lang_file').'.Confirmed_by_customer') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Confirmed_by_customer') }}  @endif</a> 
				  @elseif($vals->status==5)<a href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.Deny_by_admin')!= '') {{  trans(Session::get('mer_lang_file').'.Deny_by_admin') }} 
				  @else  {{ trans($MER_OUR_LANGUAGE.'.Deny_by_admin') }} @endif</a> 
				  @elseif($vals->status==4) <a href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.Deny_by_user')!= '') {{  trans(Session::get('mer_lang_file').'.Deny_by_user') }} 
				  @else  {{ trans($MER_OUR_LANGUAGE.'.Deny_by_user') }} @endif</a> @endif </div>
                </div>
                @endforeach </div>
              @endif
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
            <!-- table_wrap -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- global_area -->
</div>
</div>
</div>
</div>
@include('sitemerchant.includes.footer')
 @inject('data','App\Help')
@include('sitemerchant.includes.header')  
  @php $id = request()->id; $hid= request()->hid;  $Cid= request()->Cid; @endphp
@php $acoustics_leftmenu =1;


$octype=Helper::business_occasion_type($getusers->occasion_type);
 @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu') }} @endif</h5>
      </header>
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      
      @if ($getusers->status =='5')
      <div class="alert alert-info">@if (Lang::has(Session::get('mer_lang_file').'.Deny_by_admin')!= '') {{  trans(Session::get('mer_lang_file').'.Deny_by_admin') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Deny_by_admin') }} @endif</div>
      @endif
      <!-- Display Message after submition -->


      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
          <div class="box commonbox needrtl">
          <form>
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif </label>
                <div class="info100">
                  <input class="english" disabled="" name="customer_name" id="customer_name" maxlength="60" value="{{$getusers->cus_name or ''}}" type="text">
                  <input class="arabic ar" disabled="" name="customer_name_ar" id="customer_name_ar" maxlength="60" value="{{$getusers->cus_name or ''}}" type="text">
                </div>
              </div>
              <div class="form_row_right">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif </label>
                <div class="info100">
                  <input class="" disabled="" name="email" id="email" value="{{$getusers->email or ''}}" maxlength="60" type="text">
                </div>
              </div>
            </div>
            <div class="form_row">
              <!--<div class="form_row_left">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} @endif </label>
                <div class="info100">
                  <input class="small-sel" disabled="" name="phone" id="phone" value="{{$getusers->cus_phone or ''}}" maxlength="10" type="text">
                </div>
              </div>-->
              <div class="form_row_left">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Singer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Singer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Singer_Name') }} @endif </label>
                <div class="info100">
                  <input name="location" disabled="" id="location" value="{{$getusers->singer_name or ''}}" type="text">
                </div>
              </div>
              <div class="form_row_right">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Hall')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Hall') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Hall') }} @endif </label>
                <div class="info100">
                  <input class="english" disabled="" name="hall" value="{{$getusers->hall or ''}}" id="hall" value="50" maxlength="" type="text">
                  <input class="arabic ar" disabled="" name="hall_ar" value="{{$getusers->hall or ''}}"   id="hall_ar" value="50" maxlength="" type="text">
                </div>
              </div>
            </div>
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Occasion_Type')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Occasion_Type') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Occasion_Type') }} @endif </label>
                <div class="info100">
                  <input class="english" disabled="" name="occasion_type" value="{{ $octype->title or ''}}"  id="occasion_type"   maxlength="50" type="text">
                  <input class="arabic ar" disabled="" name="occasion_type_ar" id="occasion_type_ar" value="{{ $octype->title_ar or ''}}" maxlength="50" type="text">
                </div>
              </div>
            </div>
            <div class="form_row"> @php $getNmae = Helper::getmerchantcity($getusers->city_id); @endphp
              <div class="form_row_left">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif </label>
                <div class="info100">
                  <input name="singer_city" disabled="" id="singer_city" maxlength="150" value="{{ $getNmae->ci_name}}" type="text">
                </div>
              </div>
              <div class="form_row_right">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_MUSIC_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MUSIC_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MUSIC_TYPE') }} @endif </label>
                <div class="info100">
                  <input class="english xs_small" disabled="" name="music_type" id="music_type" value="{{ $getusers->music_type or '' }}" type="text">
                </div>
              </div>
            </div>
            <div class="form_row">
              <div class="form_row_left">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SONG_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SONG_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SONG_NAME') }} @endif </label>
                <div class="info100">
                  <input class="english xs_small" disabled="" name="song_name" id="song_name" value="{{ $getusers->song_name or '' }}" type="text">
                </div>
              </div>
              <!--<div class="form_row_left">
                    <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DATE') }} @endif </label>
                    <div class="info100">
                      <input class="english xs_small cal-t" disabled="" name="request_date" id="request_date" value="{{ Carbon\Carbon::parse($getusers->
              created_at)->format('F j, Y') or ''}}" type="text">
              <input class="arabic ar xs_small cal-t" disabled="" name="request_date_ar" id="request_date_ar" value="{{ Carbon\Carbon::parse($getusers->created_at)->format('F j, Y') or ''}}" type="text">
            </div>
            </div>
            -->
            <div class="form_row_right">
              <div class="dd-time">
                <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Request_Date')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Request_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Request_Date') }} @endif </label>
                <div class="info100">
                  <input class="english cal-t" disabled="" name="date"  id="date" value="{{$getusers->date}}" type="text">
                  <input class="arabic ar cal-t" name="date_ar" id="date_ar" value="{{$getusers->date}}" type="text">
                </div>
              </div>
              <div class="dd-time">
                <label class="form_label"> <span class="english"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Time')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Time') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Time') }} @endif </label>
                <div class="info100">
                  <input class="english time-t" disabled="" name="time" id="time" value="{{ $getusers->time or '' }}" maxlength="10"  type="text">
                  <input class="arabic ar time-t" disabled="" name="time_ar" value="{{ $getusers->time or '' }}" id="time_ar" maxlength="10"  value="" type="text">
                </div>
              </div>
            </div>
            </div>
          </form>
          <form style="padding-top:0" name="form1" method="post" action="{{route('store-acousticscomment')}}" id="form1">
            {{ csrf_field() }}
            <div class="commenthide">
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PRICE') }} @endif </label>
                  <div class="info100">
                    <input class="small-sel" name="price" id="price" value="{{ $getquote->price or '' }}" maxlength="10" type="text">

                  </div>
                </div>
                <div class="form_row_right">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Comments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Comments') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Comments') }} @endif </label>
                  <div class="info100">
                    <div class="english">
                      <textarea class="english" id="comments" maxlength="300" name="comments">{{ $getquote->comment or '' }}</textarea>
                    </div>
                    <div class="arabic">
                      <textarea class="arabic ar" id="comments_ar" name="comments_ar"></textarea>
                    </div>
                    <input type="hidden" name="enqueryid" id="enqueryid" value="{{ $getusers->id }}">
                    <input type="hidden" name="catid" id="id" value="{{ $id }}">
                    <input type="hidden" name="hid" id="" value="{{ $hid}}">
                    <input type="hidden" name ="itmid" id="" value="{{ $itmid}}">
                    <input type="hidden" name ="autoid" id="autoid" value="{{ $autoid or ''}}">
                    <input type="hidden" name ="userid" id="" value="{{  $getusers->user_id}}">
                    <input type="hidden" name ="customername" id="" value="{{$getusers->cus_name}}">
                    <input type="hidden" name ="email" id="" value="{{$getusers->email}}">
                    <input type="hidden" name ="languagetype" id="" value="{{$getusers->language_type}}">
                  </div>


                </div>
              
            
              @if($getusers->status !='4'  && $getusers->status !='5')
              <div class="form_row_left lpd">
                <input type="submit" id="submit" name="addhallpics" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBMIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SUBMIT') }} @endif">
              </div>
              @endif </div></div>
            @if($autoid=='' && $getusers->status !='5')
            <div class="buttonhide">
              <div class="form_row_left">
                <input type="submit" id="commentsubmit" name="addhallpics" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_CONFIRM')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CONFIRM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CONFIRM') }} @endif">
              </div>
            </div>
              @endif 
            @if($getusers->status !='3'  &&  $getusers->status !='4'  &&  $getusers->status !='5')
            <div class="buttonhideright hds">
              <div class="form-btn-section">
                <input type="button" data-id="{{ $getusers->id }}" id="reject" name="reject" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_REJECT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_REJECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_REJECT') }} @endif">
              </div>
            </div>
            @endif
          </form>
        </div>
        <!-- box -->
      </div>
    </div>
  </div>
</div>
<!-- global_area -->
</div>
</div>
<script type="text/javascript">     
$("#form1").validate({
                  ignore: [],
                  rules: {
                         
                         price: {
                          required: true,
                          },
                          comments: {
                          required: true,
                          },
                          
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                 
           messages: {
                price: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif",
                  }, 
                   
                  comments: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_COMMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_COMMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_COMMENT') }} @endif",
                  }, 
                   
                   
                },
                invalidHandler: function(e, validation){
                      var valdata=validation.invalid;
                       if (typeof valdata.comments != "undefined" || valdata.price != null) 
                      {
                      $('.english_tab').trigger('click'); 
                       }
                      if (typeof valdata.comments != "undefined" || valdata.comments != null) 
                      {
                      $('.english_tab').trigger('click'); 
                       }
                     

                      },

                submitHandler: function(form) {
                    form.submit();
                }
            });

$(document).ready(function() {
  var autoid =  $('#autoid').val();
  if(autoid!='') {
  $('.commenthide').show();
} else {
    $('.commenthide').hide();
}
  $('#commentsubmit').click(function() {
    $('.commenthide').show();
    $('.buttonhide').hide();

  })  

});

$('#reject').click(function() {
var getID = $(this).data('id');
 
if(getID!=''){
     jQuery.ajax({
        type: "Post",
         headers: {
        'X-CSRF-TOKEN': jQuery('input[name="_token"]').val()
          },
        url: "{{ route('denyquote') }}",
        data: {id:getID},
        success: function(data) {         
            if(data==1){
               location.reload();
            }
      
        }
    });

}

});

</script>
@include('sitemerchant.includes.footer')
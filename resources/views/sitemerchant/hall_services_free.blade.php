@include('sitemerchant.includes.header')  
@php $hall_leftmenu =1; @endphp
<div class="merchant_vendor">
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
    <header>
      <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_HALL_FREE_SERVICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_HALL_FREE_SERVICE') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_HALL_FREE_SERVICE') }} @endif </h5>
        @include('sitemerchant.includes.language')
    </header>
    <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> 
             
            <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
            <div class="one-call-form">
            {!! Form::open(array('url' => '/add_freeservices','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'add_freeservices', 'name' =>'add_freeservices' )) !!}
            
            @if($getDbC >=1)
            
            
            @for($i=0;$i<$getDbC;$i++)
 
			     
            <div class="form_row posrel" >
             <div class="form_row_left">
            <div id="remove_button" class="remove_btn"><a href="javascript:void(0);" title="Remove field" class="status_active  status_active2 cstatus" data-status="Active" data-id="{{$getDb[$i]->id}}" style="float:right;">&nbsp;</a></div>
              
                <label class="form_label">
     <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NAME'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NAME'); @endphp </span>

 </label>

                <div class="info100">
                  <div class="has-success">
                    <div class="english">
                        <input type="text" maxlength="50" class="english sname" name="servicename[{{$i}}]" value="@php echo $getDb[$i]->value;  @endphp">
                    </div>
                      <div class="arabic ar">
                           <input type="text" maxlength="50"  class="arabic ar snamear" name="servicename_ar[{{$i}}]" value="@php echo $getDb[$i]->value_ar;  @endphp">
                     </div>
                  </div>
                  <!-- has-success --> 
                </div>
                </div>
                <!-- info100 --> 
                
           
            </div>
            <!-- form_row -->
            
         <!--    <div class="form_row">
            <div class="form_row_left">
                <label class="form_label">

 <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NOTES'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NOTES'); @endphp </span>
 </label>
            <div class="info100">
              <div class="has-success">
                <div class="english">
                <textarea name="servicenotes[{{$i}}]"  class="english end snotes" >@php echo $getDb[$i]->option_note_value;  @endphp</textarea>
               </div>
              <div class="arabic ar">
                <textarea class="arabic ar snotesar" name="servicenotes_ar[{{$i}}]">@php echo $getDb[$i]->option_note_value_ar;  @endphp</textarea>
              </div>

                <input type="text" class="ar" id="price" name="price[{{$i}}]" value="0">
              </div>
         
            </div>
       
               </div> 
            </div> -->
            <!-- form_row --> 
            @endfor              
            @else
            <div class="form_row">
                <div class="form_row_left">
                <label class="form_label">
 <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NAME'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NAME'); @endphp </span>

 </label>
                <div class="info100">
                  <div class="has-success">
                    <div class="english">
                      <input type="text" name="servicename[0]" maxlength="50" class="sname" >
                    </div>
                    <div class="arabic ar">  
                         <input type="text" class="arabic ar snamear" name="servicename_ar[0]" maxlength="50" >
                  </div>
                  </div>
                  <!-- has-success --> 
                </div>
                </div>
                <!-- info100 --> 
                
            </div>
            <!-- form_row -->
            
          <!--   <div class="form_row">
                <div class="form_row_left">
                <label class="form_label">
 <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NOTES'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NOTES'); @endphp </span>

 </label>
                <div class="info100">
                  <div class="has-success">
                    <div class="english">
                       <textarea name="servicenotes[0]" class="english snotes "></textarea>
                     </div>
                     <div class="arabic ar">  
                    <textarea class="arabic ar snotesar" name="servicenotes_ar[0]"></textarea>
                  </div>
                    <input type="text" class="ar" id="price" name="price[0]" value="0">
                    </textarea>
                  </div>
                 
                </div>
                </div>
             
                
            </div> -->
            <!-- form_row --> 
            @endif
            <div id="img_upload"></div>
            <div class="form_row">  
            <div class="form_row_left">
            <div id="add_button" class=""><a href="javascript:void(0);" class="form-btn">
<span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_ADDMORE'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_ADDMORE'); @endphp </span>
 </a></div></div>
 </div>
            @php $Count = $getDbC +1;@endphp
            <input type="hidden" id="hid" name="hid" value="{{$_REQUEST['hid']}}">
            <input type="hidden" id="count" name="count" value="{{$Count}}">
            <input type="hidden" id="product_option_id" name="product_option_id" value="1">
            
            <div class="form_row_left english"><input type="submit" name="addfreeservice" value="Submit"></div>
      <div class="form_row_right arabic ar arbic_right_btn"><input type="submit" onclick="" name="addfreeservice" value=" {{ trans('mer_ar_lang.MER_SUBMIT') }}"></div>

            {!! Form::close() !!} 
            
            <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
            </div> <!-- one-call-form -->
          </div>
          </div>
        </div>
      </div>
    </div>
    <!-- global_area --> 
  </div>
</div>
<!-- right_panel --> 
<!-- merchant_vendor --> 

<!-- --> 
<script type="text/javascript">
  
	$(document).ready(function(){
	var maxField = 11;  
	var addButton = $('#add_button');  
	var wrapper = $('#img_upload');
	var x = @php echo $Count; @endphp;  
	$(addButton).click(function(){  
	if(x < maxField){  
	x++; 
  var main = x;

  var testElement= document.getElementById('english_tab');
  var result = testElement.classList.contains('active')
  if(result)
  {
    var enclass=''
    var arclass='ar'
  }
  else
  {
    var enclass='ar'
    var arclass= ''

  }
	var fieldHTML = '<div class="form_row_left posrel main'+main+'"><div id="remove_button" class="remove_btn" onclick="javascript: removemain('+main+')"><a href="javascript:void(0);"  title="Remove field" > &nbsp; </a></div><div class="form_row"><label class="form_label"><span class="english '+enclass+' ">@php echo lang::get("mer_en_lang.MER_SERVICE_NAME"); @endphp</span><span  class="arabic '+arclass+'"> @php echo lang::get("mer_ar_lang.MER_SERVICE_NAME"); @endphp</span></label><div class="info100"><div class="has-success"><div class="english '+enclass+' "><input type="text" class="english '+enclass+' sname " name="servicename['+x+']" maxlength="50" ></div><div class="arabic '+arclass+' "><input type="text" class="arabic '+arclass+' snamear" name="servicename_ar['+x+']" maxlength="50" ></div></div></div></div></div></div>';  
	$(wrapper).append(fieldHTML);  
  createValidation();
	document.getElementById('count').value = parseInt(x);
	}
	});
	/*$(wrapper).on('click', '#remove_button', function(e){  
	e.preventDefault();
	$(this).parent('div').remove(); 
	x--;  
	document.getElementById('count').value = parseInt(x);
	}); */ 
	});
  
  function removemain(val)
  {
      var x = document.getElementById('count').value;
      $('.main'+val).remove();
      x--;  
      document.getElementById('count').value = parseInt(x);

  }


</script> 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Want_Delete')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Want_Delete') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Want_Delete') }} @endif')
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'deleterecord'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }

        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 

<script type="text/javascript">
  
$("#add_freeservices").validate({
                  ignore: [],
                  rules: {
                  'servicename[0]': {
                       required: true,
                      },

                      
                      'servicename_ar[0]': {
                       required: true,
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             'servicename[0]': {
              
                required: "{{ trans('mer_en_lang.MER_VALIDATION_FREE_SERVICES_NAME') }}",
                      },  
 
                 'servicename_ar[0]': {
               required: "{{ trans('mer_ar_lang.MER_VALIDATION_FREE_SERVICES_NAME_AR') }}",
                      }, 
 
                                    
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.errorList;
                    console.log("invalidHandler : validation", valdata[0].element.classList);
                    var i;
                    var erroren=0;
                    var start=10;
                    var lang ='ar';
                    var val=0
                    var vala = 0;
                    for (i = 0; i < start ;  i++) {
                      
                      if (typeof valdata[i] !== 'undefined') {  
                        var data=valdata[i].element.classList;
                        $.inArray('ar',data);
                                        console.log("invalidHandler : validation", $.inArray('ar',data)); 
                                              if($.inArray('ar',data) == -1) 
                                              {
                                                  val=1;

                                              } 

                                              if($.inArray('en',data) == -1) 
                                              {
                                                  vala=2;

                                              } 

                                          } 
                                         
                      }

                                              if(val==0)
                                              {

                                                $('.arabic_tab').trigger('click');  
                                              }
                                               
                                           
                                               


                    },


                submitHandler: function(form) {
                    form.submit();
                }
            });

 var createValidation = function() {
  $(".sname").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
     
         required: "{{ trans('mer_en_lang.MER_VALIDATION_FREE_SERVICES_NAME') }}"
      }
    });
  });

 

$(".snamear").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
        required: "{{ trans('mer_ar_lang.MER_VALIDATION_FREE_SERVICES_NAME_AR') }}"
      }
    });
  });
}

</script>


@include('sitemerchant.includes.footer')



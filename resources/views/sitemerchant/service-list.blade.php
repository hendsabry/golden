@include('sitemerchant.includes.header')
<div class="merchant_vendor" >
  <div class="inner">

      <!-- filter area start -->
      @if(Session::get('LoginType') !='4')
        <div class="service_heading_area no-margin">
          <h1 class="global_head" > @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICES') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICES') }} @endif </h1>
          <div class="add"><a href="{{url('/addservices')}}">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDMORE') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDMORE') }} @endif</a> </div>
          
        </div>
        @endif
        <!-- service_heading_area --> 
        
        {!! Form::open(array('url'=>'sitemerchant_dashboard','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
        <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
        
        {!! Form::close() !!} </div>
           {!! Form::open(array('url'=>'sitemerchant_dashboard','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
      <div class="filter_area">
          <div class="filter_left">
            <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
            <div class="search-box-field mems ">
              <select name="subscription" id="subscription">
                <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_MEMBERSHIP_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_MEMBERSHIP_TYPE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_MEMBERSHIP_TYPE') }} @endif </option>
                <option value="0" @if($subscription=='0') selected="selected" @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION') }} @endif</option>
                <option value="1" @if($subscription=='1') selected="selected" @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_SUBSCRIPTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBSCRIPTION') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SUBSCRIPTION') }} @endif</option>
              </select>
              <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
            </div>
          </div>
          <div class="search_box">
            <div class="search_filter">&nbsp;</div>
            <div class="filter_right">
              <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$search}}" />
              <input type="button" class="icon_sch" id="submitdata" />
            </div>
          </div>
        </div>
        {!! Form::close() !!}
      <!-- service list  area start -->
  
        <div class="panel-body panel panel-default"> @if($categoryname->count() < 1)
          <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
          @else
          <div class="table">
            <div class="tr">
               <div class="table_heading">  </div>

              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_NAME') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_NAME') }} @endif </div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MEMBERSHIP_TYPE') }} @endif </div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif </div>

              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.JOIN_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.JOIN_DATE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.JOIN_DATE') }} @endif</div>
                @if(Session::get('LoginType') !='4')
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PAYMENT') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENT') }} @endif</div>
              @endif
            </div>
               @php $mc_name = 'mc_name';  $i=1;              
               @endphp
            @foreach($categoryname as $val)  

            @php
                $getsubtype=Helper::subscription_plans_type($val->mc_id);
             @endphp           
         
            @if($mer_selected_lang_code =='ar')
            @php $mc_name= 'mc_name_ar'; @endphp
            @endif

            @if(Session::get('LoginType') =='4' && ( $val->cat_route =='makeup-artist-shop-info' || $val->cat_route =='roses-shop-info'  || $val->cat_route =='dates-info' || $val->cat_route =='car-rental' || $val->cat_route =='travel-info' || $val->cat_route =='abaya-shop-info'|| $val->cat_route =='mens-shop-info' || $val->cat_route =='dresses-shop-info'  || $val->cat_route =='photography-shop-info' || $val->cat_route =='event-shop-info' || $val->cat_route =='manage-singer' || $val->cat_route=='popular-band-info' || $val->cat_route=='acoustics' || $val->cat_route=='reception-shop-list' || $val->cat_route=='cosha-shop-info'  || $val->cat_route=='makeup-shop-info'))
            @php continue; @endphp 
            @endif
            @php   
            if(Session::get('LoginType') =='4')
            {
            $merchantManagerid = Session::get('merchant_managerid');
            $GetT = Helper::getAccess($merchantManagerid); 
            $getInfoID = $val->mc_id;
            if (!in_array($getInfoID, $GetT))
            {
            continue;
            }
            }  
            @endphp
            <div class="tr">  

      <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.SN')!= '') {{  trans(Session::get('mer_lang_file').'.SN') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SN') }} @endif">{{$i}} &nbsp;&nbsp;</div>
     <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_NAME') }} @endif"> 

@php if($getsubtype->services_id == '2' && $val->status == 0) {  $link =0; } else { $link =1; } @endphp

    @php if($val->status==1 || $val->status==0){ @endphp        
      @if($val->cat_route!='')
      @if($val->cat_route=='acoustics')
      @php  $getSinger = Helper::getsingerid($val->mc_id); $getSingerCid = Helper::getsingercid($val->mc_id); @endphp
      <a class="main-underscore"  @if($link==0) href="javascript:void(0);" @else href="{{ route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger,'Cid' => $getSingerCid ]) }}" @endif > 
      @elseif($val->cat_route =='manage-singer' || $val->cat_route=='popular-band-info' || $val->cat_route=='acoustics')
      @php  $getSinger = Helper::getsingerid($val->mc_id); @endphp
      <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger ]) }}" @endif> 
      @elseif($val->cat_route =='makeup-artist-shop-info' || $val->cat_route =='roses-shop-info'  || $val->cat_route =='dates-info' || $val->cat_route =='car-rental' || $val->cat_route =='travel-info' || $val->cat_route =='abaya-shop-info'|| $val->cat_route =='mens-shop-info' || $val->cat_route =='dresses-shop-info'  || $val->cat_route =='photography-shop-info' || $val->cat_route =='event-shop-info' )  
      @php  $itemid = Helper::getmakeupArtist($val->mc_id); @endphp
      <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>

      @elseif($val->cat_route =='makeup-shop-info')
      @php  $itemid = Helper::getmakeup($val->mc_id); @endphp
      <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>

      @elseif($val->cat_route =='cosha-shop-info')
      @php  $itemid = Helper::getCosha($val->mc_id); @endphp

      <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>

      @elseif($val->cat_route =='tailor-shop-info')
      @php  $itemid = Helper::getTailor($val->mc_id); @endphp

      <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>


        @else
        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id ]) }}" @endif>
        @endif
        {{ $val->$mc_name or '' }}</a>
        @else                
        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route('hotelnamelist',['id' => $val->mc_id]) }}" @endif>{{ $val->$mc_name or '' }}</a>
        @endif

        @php }else{ @endphp {{ $val->$mc_name or '' }} @php } @endphp
        </div>
        <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MEMBERSHIP_TYPE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MEMBERSHIP_TYPE') }} @endif">@if($getsubtype->services_id==1) @if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION') }} @endif @else @if (Lang::has(Session::get('mer_lang_file').'.MER_SUBSCRIPTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBSCRIPTION') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SUBSCRIPTION') }} @endif @endif </div>
      @if(Session::get('LoginType') !='4')
        <div class="td td4 " data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}   @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif"> @if($val->status==1)  @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
        
       <!--  <span class="status_tooltip">
        	<span>Click to deactivate</span>
        </span> -->
        
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif  @else @if (Lang::has(Session::get('mer_lang_file').'.MER_INACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_INACTIVE') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_INACTIVE') }} @endif @endif</div>
 
      @endif
 
        <div class="td td4 " data-title="@if (Lang::has(Session::get('mer_lang_file').'.JOIN_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.JOIN_DATE') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.JOIN_DATE') }} @endif"> {{ Carbon\Carbon::parse($val->UUN)->format('F j, Y')}} </div>

        <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif">
        @php if($val->status==1 || $val->status==0){ @endphp  

        @if($val->cat_route!='')



 @if($val->cat_route=='acoustics')
      @php  $getSinger = Helper::getsingerid($val->mc_id); $getSingerCid = Helper::getsingercid($val->mc_id); @endphp
      <a class="main-underscore"  @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger,'Cid' => $getSingerCid ]) }}" @endif> 
 
        @elseif($val->cat_route =='manage-singer' || $val->cat_route=='popular-band-info')
        @php  $getSinger = Helper::getsingerid($val->mc_id); @endphp
        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else href="{{ route("$val->cat_route",['id' => $val->mc_id ,'hid' => $getSinger ]) }}" @endif>
        @elseif($val->cat_route =='makeup-artist-shop-info'   || $val->cat_route =='roses-shop-info'  || $val->cat_route =='dates-info'  || $val->cat_route =='car-rental'  || $val->cat_route =='travel-info' || $val->cat_route =='abaya-shop-info' || $val->cat_route =='mens-shop-info' || $val->cat_route =='dresses-shop-info' || $val->cat_route =='photography-shop-info' || $val->cat_route =='event-shop-info' )
        @php  $itemid = Helper::getmakeupArtist($val->mc_id); @endphp

        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>

        @elseif($val->cat_route =='makeup-shop-info')
        @php  $itemid = Helper::getmakeup($val->mc_id); @endphp
        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>

        @elseif($val->cat_route =='cosha-shop-info')
        @php  $itemid = Helper::getCosha($val->mc_id); @endphp
        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>

        @elseif($val->cat_route =='tailor-shop-info')
        @php  $itemid = Helper::getTailor($val->mc_id); @endphp
        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id,'itemid' => $itemid ]) }}" @endif>
        @else
        <a class="main-underscore"  @if($link==0) href="javascript:void(0);" @else  href="{{ route("$val->cat_route",['id' => $val->mc_id ]) }}" @endif>
        @endif

        @if (Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VIEW') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VIEW') }} @endif  {{ $val->$mc_name }}</a>

        @else
 
        <a class="main-underscore" @if($link==0) href="javascript:void(0);" @else  href="{{ route('hotelnamelist',['id' => $val->mc_id]) }}" @endif>@if (Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VIEW') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VIEW') }} @endif  {{ $val->$mc_name }}</a>
        @endif

        @php }else { @endphp @if (Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VIEW') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VIEW') }} @endif  {{ $val->$mc_name }} @php } @endphp
        </div>

    @if(Session::get('LoginType') !='4')
  <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PAYMENT') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENT') }} @endif">

 @if($getsubtype->services_id==2 && $val->status==0)
  <a href="{{route("pay-now",['id' => $val->mc_id])}}">@if (Lang::has(Session::get('mer_lang_file').'.PAYNOW')!= '') {{  trans(Session::get('mer_lang_file').'.PAYNOW') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.PAYNOW') }} @endif </a>
  @else
  N/A
  @endif 

  </div>
  @endif

            </div>
            @php $i = $i+1; @endphp
            @endforeach </div>
          @endif 
          
          <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
          
          <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
          
        </div>
       @if(Session::get('LoginType') !='4')  {{ $categoryname->links() }}  @endif
      <!-- service list  area start--> 
 
</div>

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a><a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'servicelist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script> 
@include('sitemerchant.includes.footer') 
<div class="add_row_wrap"><div id="remove_button" class="remove_btn"><a href="javascript:void(0);"  title="Remove field">&nbsp;</a></div><div class="form_row"><div class="form_row_left"><label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_NAME') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_NAME') }} @endif </label><div class="info100"><div class="has-success">
	<input type="text" class="english sname " name="servicename['+x+']" maxlength="50" >
	<input type="text" class="arabic ar snamear" name="servicename_ar['+x+']" maxlength="50" >

</div></div></div></div><div class="form_row"><div class="form_row_left"><label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_NOTES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_NOTES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_NOTES') }} @endif</label><div class="info100"><div class="has-success">
	<textarea class="english snotes" name="servicenotes['+x+']"></textarea>
	<textarea class="arabic ar snotesar" name="servicenotes_ar['+x+']">	</textarea>

	<input class="ar" id="price" name="price['+x+']" value="0"></div></div></div></div></div>
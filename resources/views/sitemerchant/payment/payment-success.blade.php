@include('sitemerchant.includes.header')    
@php $Payment_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.PAYMENT_SUCCESS')!= '') {{  trans(Session::get('mer_lang_file').'.PAYMENT_SUCCESS') }}@else  {{ trans($MER_OUR_LANGUAGE.'.PAYMENT_SUCCESS') }} @endif </h5>
 </header>
    </div>
    @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
   <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box commonbox">
             
        Thank you for payment.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
@include('sitemerchant.includes.footer')
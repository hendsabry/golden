@include('sitemerchant.includes.header') 
@php $reception_hospitality_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
    <header>
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_ATTRIBUTES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_ATTRIBUTES') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_ATTRIBUTES') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
    </header>
    <div class="global_area">
      <div class="row">

		<!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <div class="error arabiclang"></div>
              <!-- Display Message after submition -->

          <div class="col-lg-12">
            <div class="box"> 
              
              
              <div class="one-call-form">
                <form name="form1" method="post" id="addhotel" action="{{ route('saveservice') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                 
                 <div class="form_row">
                    <label class="form_label">


                      @if (Lang::has(Session::get('mer_lang_file').'.MER_GOOGLE_MAP_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_ATTRIBUTES_NAME') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_ATTRIBUTES_NAME') }} @endif</label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="" id="hotal_name" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_GOOGLE_MAP_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_ATTRIBUTES_NAME') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_ATTRIBUTES_NAME') }} @endif" class="english"  data-validation-length="max80" maxlength="80">
                      </div>

                      <div class="arabic ar">
                        <input id="ssb_name_ar" class="arabic ar" name="mc_name_ar"  id="mc_name_ar" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_GOOGLE_MAP_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_ATTRIBUTES_NAME') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_ATTRIBUTES_NAME') }} @endif"  type="text" data-validation-length="max80"  maxlength="80" >
                      </div>
                    </div>
                  </div>

                <div class="form_row">
                          <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_SELECT_PARENT_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_SELECT_PARENT_NAME') }}  
                        @else  {{ trans($MER_SHOP_SELECT_PARENT_NAME.'.MER_SHOP_SELECT_PARENT_NAME') }} @endif</label>
                          <div class="info100">
                          <select class="small-sel" name="manager"  id="manager">
                            <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_SELECT_PARENT_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_SELECT_PARENT_NAME') }}  
                  @else  {{ trans($MER_SHOP_SELECT_PARENT_NAME.'.MER_SHOP_SELECT_PARENT_NAME') }} @endif</option>
                            <option value="">Himanshu</option>
                            <option value="">Himanshu</option>
                            <option value="">Himanshu</option> 
                          </select>
                      </div>
                   </div> <!-- form_row -->

                  <div class="form_row">
                  <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_DESCRIPTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DESCRIPTION') }}  
                   @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DESCRIPTION') }} @endif</label>
                      
                       <div class="info100"> 
                        <div class="english"> 
                        <textarea name="description" id="description" rows="4" cols="50" class="english"></textarea>  

                       </div>
                          <div  class="arabic ar" >                                                 
                        <textarea class="arabic ar" name="description_ar" id="description Arabic" rows="4" cols="50"></textarea>  
                      </div>
                          
                  </div>
                </div> <!-- form_row -->
                  <div class="form_row">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }}  
                            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" required="">
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  
                  <div class="form_row arabic ar">
                    <input type="submit" name="submit" value="خضع ">
                  </div>
                   <div class="form_row english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <!-- form_row -->
                </form>
              </div>
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>       
$("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
$("#addhotel").validate({
                  ignore: [],
                  rules: {
                  mc_name: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },

                       description: {
                       required: true,
                      },

                      description_ar: {
                       required: true,
                      },

                      mc_img: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mc_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HOTEL_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HOTEL_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HOTEL_NAME') }} @endif",
                      },  

                 mc_name_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_HOTEL_NAME_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_HOTEL_NAME_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_HOTEL_NAME_AR') }} @endif",
                      },    

                       description: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_DESCRIPTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_DESCRIPTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_DESCRIPTION') }} @endif ",
                      },

                       description_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_DESCRIPTION_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_DESCRIPTION_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_DESCRIPTION_AR') }} @endif ",
                      },
                        mc_img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },                       
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                        $('.arabiclang').html('Please enter all value in arabic language'); 
                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click'); 
                        $('.arabiclang').html('Please enter all value in arabic language'); 
                            

                    }


                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
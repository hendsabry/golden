<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | <?php if (Lang::has(Session::get('mer_lang_file').'.MER_LARAVEL_ECOMMERCE_MULTIVENDOR_ADMIN_WITHDRAW_FUND_REQUEST')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LARAVEL_ECOMMERCE_MULTIVENDOR_ADMIN_WITHDRAW_FUND_REQUEST');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_LARAVEL_ECOMMERCE_MULTIVENDOR_ADMIN_WITHDRAW_FUND_REQUEST');} ?></title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/main.css" />
      <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/new_css.css" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/theme.css" />
  <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/plan.css" />
<?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); if(count($favi)>0) { foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="<?php echo url(''); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php } ?>
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="<?php echo url(''); ?>/public/assets/css/datepicker.css" rel="stylesheet"> 
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  <link href="<?php echo url(''); ?>/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/timeline/timeline.css" />

  

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
          {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        {!! $adminleftmenus !!}
        
        
        <!--END MENU SECTION -->

    <div></div>


         <!--PAGE CONTENT -->
        <div id="content">
           
                <?php /* foreach($commison_amt as $commision) { } 
        $comminsion = $commision->mer_commission;
        
        $deal =  $deal_count * $deal_discount_count ;
        $commision_amt_deal = $deal * ($comminsion / 100);
        $deal_withdraw = $deal - $commision_amt_deal;
        
        $product =  $product_no_count * $product_discount_count ;
        $commision_amt_pro = $product * ($comminsion / 100);
        $product_withdraw = $product -$commision_amt_pro;

        /*new calculation for product *
        $product_order_amt; //this is sum of all paypal purchase product price
        //$product_ordercod_amt; //this is sum of all cod purchase product price
        $all_product_price = $product_order_amt + $product_ordercod_amt;

        $product_commission = (($product_order_amt * $comminsion)/100);  //product purchase commission
        $withdraw_product_amt = $product_order_amt - $product_commission;
        
        /*new calculation for deal*
        $deal_order_amt; //this is sum of all paypal purchase product price
       // $deal_ordercod_amt; //this is sum of all cod purchase product price
        $all_deal_price = $deal_order_amt + $deal_ordercod_amt;

        $deal_commission    = (($all_deal_price * $comminsion)/100);  //product purchase commission
        $withdraw_deal_amt = $deal_order_amt - $deal_commission;
        */
?>
    <?php

      $product_order_amt = $deal_order_amt  = $paidamounttomerchant = $merchantAmount = 0;
      $adminCommision = $balanceFrom_wallet_nd_coupon = $deal_pro_amt =  $wallet_plus_coupon_paypal = $wallet_plus_coupon_payumoney= $paidCommission =0;

      $paidDetails = DB::table('nm_cod_commission_paid')->select('nm_cod_commission_paid.paidAmount')->where('nm_cod_commission_paid.com_merchant_id','=',Session::get('merchantid'))->where('nm_cod_commission_paid.com_status','=','1')->first();
      if(count($paidDetails) > 0)
{
  $paidCommission =+$paidDetails->paidAmount;
}
      //print_r($paidDetails); exit;
      if(!empty($product_cod_orderDetails)){
        foreach ($product_cod_orderDetails as $product_cod) {
            $codamt = $product_cod->sumCodAmt +$product_cod->sumTax +$product_cod->sumShippingAmt; 
           // $product_order_amt += $codamt;
            $adminCommision += $product_cod->sumMerchantCommission; 
             $paidCommission = $paidCommission;
              $wallet_plus_coupon = $product_cod->sumWallet +  $product_cod->sumCoupon ;
            if($wallet_plus_coupon > $product_cod->sumMerchantCommission)
              $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $product_cod->sumMerchantCommission; 
            elseif($wallet_plus_coupon < $product_cod->sumMerchantCommission)
              $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $product_cod->sumMerchantCommission;
        }
         
      }if(!empty($product_order_orderDetails)){
        foreach ($product_order_orderDetails as $product_order) {
          $paypalAmt = $product_order->sumCodAmt +$product_order->sumTax +$product_order->sumShippingAmt;
          $product_order_amt += $paypalAmt; 
          $adminCommision += $product_order->sumMerchantCommission; 

          $merchantAmount += $product_order->sumMerchantAmount; 

           $wallet_plus_coupon = $product_order->sumWallet +  $product_order->sumCoupon ;
            $wallet_plus_coupon_paypal += $product_order->sumWallet +  $product_order->sumCoupon;
          /*if($wallet_plus_coupon > $product_order->sumMerchantCommission)
            $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $product_order->sumMerchantCommission; */
        }
          
      }

      if(!empty($product_order_orderDetails_payu)){
        foreach ($product_order_orderDetails_payu as $product_order) {
          $paypalAmt = $product_order->sumCodAmt +$product_order->sumTax +$product_order->sumShippingAmt;
          $product_order_amt += $paypalAmt; 
          $adminCommision += $product_order->sumMerchantCommission; 

          $merchantAmount += $product_order->sumMerchantAmount; 
           $wallet_plus_coupon_payumoney += $product_order->sumWallet +  $product_order->sumCoupon;

          $wallet_plus_coupon = $product_order->sumWallet +  $product_order->sumCoupon ;
          /*if($wallet_plus_coupon > $product_order->sumMerchantCommission)
            $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $product_order->sumMerchantCommission; */
        }
          
      }
      if(!empty($deal_cod_orderDetails)){
        foreach ($deal_cod_orderDetails as $deal_cod) {
            $codamt = $deal_cod->sumCodAmt +$deal_cod->sumTax +$deal_cod->sumShippingAmt; 
            //$deal_order_amt += $codamt;
            $adminCommision += $deal_cod->sumMerchantCommission; 

            $wallet_plus_coupon = $deal_cod->sumWallet +  $deal_cod->sumCoupon ;
            if($wallet_plus_coupon > $deal_cod->sumMerchantCommission)
              $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $deal_cod->sumMerchantCommission; 
            elseif($wallet_plus_coupon < $deal_cod->sumMerchantCommission)
              $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $deal_cod->sumMerchantCommission;
        }
         
      }if(!empty($deal_order_orderDetails)){
        foreach ($deal_order_orderDetails as $deal_order) {
          $paypalAmt = $deal_order->sumCodAmt +$deal_order->sumTax +$deal_order->sumShippingAmt;
          $deal_order_amt += $paypalAmt; 
          $adminCommision += $deal_order->sumMerchantCommission; 

          $merchantAmount += $deal_order->sumMerchantAmount; 

           $wallet_plus_coupon = $deal_order->sumWallet +  $deal_order->sumCoupon ;
            $wallet_plus_coupon_payumoney += $product_order->sumWallet +  $product_order->sumCoupon;
         /* if($wallet_plus_coupon > $deal_order->sumMerchantCommission)
            $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $deal_order->sumMerchantCommission;*/ 
        }
          
      }

      if(!empty($deal_order_orderDetails_payu)){
        foreach ($deal_order_orderDetails_payu as $deal_order) {
          $paypalAmt = $deal_order->sumCodAmt +$deal_order->sumTax +$deal_order->sumShippingAmt;
          $deal_order_amt += $paypalAmt; 
          $adminCommision += $deal_order->sumMerchantCommission; 

          $merchantAmount += $deal_order->sumMerchantAmount; 
           $wallet_plus_coupon_payumoney += $product_order->sumWallet +  $product_order->sumCoupon;

          $wallet_plus_coupon += $deal_order->sumWallet +  $deal_order->sumCoupon ;
         /* if($wallet_plus_coupon > $deal_order->sumMerchantCommission)
            $balanceFrom_wallet_nd_coupon += $wallet_plus_coupon - $deal_order->sumMerchantCommission;*/ 
        }
          
      }
      
     if(!empty($paidWithdrawAmount))
          $paidamounttomerchant = $paidWithdrawAmount[0]->paid_amt!=''?$paidWithdrawAmount[0]->paid_amt:0;
      //echo $commissionPaidDetails->sumPaidAmt;
       $commissionPaid = $commissionPaidDetails->sumPaidAmt==''?0:$commissionPaidDetails->sumPaidAmt;
      $deal_pro_amt   =  ($product_order_amt + $deal_order_amt); 
      $withdrawAmount =  $deal_pro_amt- $paidCommission ;
       //$withdrawAmount =  $merchantAmount + $balanceFrom_wallet_nd_coupon ;  
   
      
   $cur_withdraw = $commissionDetails->merchantAmt - $commissionDetails->commissionAmt - $commissionPaid-$wallet_plus_coupon_paypal-$wallet_plus_coupon_payumoney + $paidCommission;
    ?>
        
    <div class="inner"> 
      <div class="row"> 
        <div class="col-lg-12"> 
          <ul class="breadcrumb">
            <li class=""><a href="<?php echo url('sitemerchant_dashboard'); ?>"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_HOME');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_HOME');} ?></a></li>
            <li class="active"><a href="#"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_WITHDRAW_FUND_REQUEST')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_WITHDRAW_FUND_REQUEST');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_WITHDRAW_FUND_REQUEST');} ?></a></li>
          </ul>

        </div>
      </div>
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="box dark"> <header> 
            <div class="icons"><i class="icon-edit"></i></div>
            <h5><?php if (Lang::has(Session::get('mer_lang_file').'.MER_WITHDRAW_FUND_REQUEST')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_WITHDRAW_FUND_REQUEST');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_WITHDRAW_FUND_REQUEST');} ?></h5>
            </header> 
             @if (Session::has('success'))

              <div class="alert alert-success alert-dismissable">{!! Session::get('success') !!}

              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
             @endif
             <div style="display: none;" class="la-alert rec-select alert-success alert-dismissable"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_VALID_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_VALID_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_VALID_AMOUNT');} ?>  
             <button type="button" class="close closeAlert"  aria-hidden="true">×</button></div>

            <div class="row" style="padding: 10px;">
         
              <div class="col-lg-6"> 
                <div class="form-group"> 
                 {!! Form::open(array('url'=>'withdraw_submit','enctype'=>'multipart/form-data')) !!}
                  <label for="text1" class="control-label"> 
                 <?php if (Lang::has(Session::get('mer_lang_file').'.MER_TOTAL_PRODUCT_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TOTAL_PRODUCT_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TOTAL_PRODUCT_AMOUNT');} ?> 
                          : {{ Helper::cur_sym() }} <?php echo $product_order_amt; ?>
                  
                  </label>
                  <span data-tooltip='{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ONLINE_ALL_PRO')!= '') ? trans(Session::get('admin_lang_file').'.BACK_ONLINE_ALL_PRO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ONLINE_ALL_PRO') }}'> <i class="glyphicon glyphicon-info-sign"></i> </span>
                </div>
                <div class="form-group"> 
                  <label for="text1" class="control-label"> 
                <?php if (Lang::has(Session::get('mer_lang_file').'.MER_TOTAL_DEAL_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_TOTAL_DEAL_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_TOTAL_DEAL_AMOUNT');} ?> 
                : {{ Helper::cur_sym() }} {{ $deal_order_amt }}
                 
                 </label>
                  <span data-tooltip='{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ONLINE_ALL_PRO')!= '') ? trans(Session::get('admin_lang_file').'.BACK_ONLINE_ALL_PRO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ONLINE_ALL_PRO') }}'> <i class="glyphicon glyphicon-info-sign"></i> </span>
                </div>
              

              
                <div class="form-group"> 
                  <label for="text1" class="control-label"> 
               <?php if (Lang::has(Session::get('mer_lang_file').'.MER_ADMIN_COMMISION_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_ADMIN_COMMISION_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_ADMIN_COMMISION_AMOUNT');} ?> 
                          :  {{ $adminCommision }}
                 
                </label>
                 <span data-tooltip='{{ (Lang::has(Session::get('admin_lang_file').'.BACK_COMMSSION_TO_PAY_LIABLE')!= '') ? trans(Session::get('admin_lang_file').'.BACK_COMMSSION_TO_PAY_LIABLE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COMMSSION_TO_PAY_LIABLE') }}'> <i class="glyphicon glyphicon-info-sign"></i> </span>
                </div>
                
                <div class="form-group"> 
                  <label for="text1" class="control-label"> 
               <?php if (Lang::has(Session::get('mer_lang_file').'.MER_ADMIN_COMMISION_PAID')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_ADMIN_COMMISION_PAID');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_ADMIN_COMMISION_PAID');} ?> 
                          :  {{ $paidCommission }}
                 
                </label>
                 <span data-tooltip='{{ (Lang::has(Session::get('mer_lang_file').'.ADMIN_COMMISION_PAID_BY_MER')!= '') ? trans(Session::get('mer_lang_file').'.ADMIN_COMMISION_PAID_BY_MER') : trans($ADMIN_OUR_LANGUAGE.'.ADMIN_COMMISION_PAID_BY_MER') }}'> <i class="glyphicon glyphicon-info-sign"></i> </span>
                </div>

              
                <div class="form-group"> 
                  <label for="text1" class="control-label"> 
                    <?php if (Lang::has(Session::get('mer_lang_file').'.MER_WITHDRAW_ACCOUNT_BALANCE_IS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_WITHDRAW_ACCOUNT_BALANCE_IS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_WITHDRAW_ACCOUNT_BALANCE_IS');} ?>: 
                    {{ Helper::cur_sym() }} {{ $withdrawAmount }}
                 <input type="hidden" name="total_withdraw" id="total_withdraw" value="{{ $withdrawAmount }}" />
             </label>
              <span data-tooltip='(Total product amount- Admin commission)'> <i class="glyphicon glyphicon-info-sign"></i> </span>
                  
                </div>
           
              
                <div class="form-group"> 
                  <label for="text1" class="control-label"> 
                   <?php if (Lang::has(Session::get('mer_lang_file').'.MER_FUND_AMOUNT_RECEIVED')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_FUND_AMOUNT_RECEIVED');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_FUND_AMOUNT_RECEIVED');} ?>
                    :  {{ Helper::cur_sym() }} <?php echo $paidamounttomerchant; ?>
                 
                  </label>

                    <span data-tooltip='(Admin paid details)'> <i class="glyphicon glyphicon-info-sign"></i> </span>
                </div>
            
              
              
                <div class="form-group"> 
                  <label for="text1" class="control-label"> 
                 <?php if (Lang::has(Session::get('mer_lang_file').'.MER_BALANCE_WITHDRAW_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_BALANCE_WITHDRAW_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_BALANCE_WITHDRAW_AMOUNT');} ?> 
                  : {{ Helper::cur_sym() }} {{ $cur_withdraw }}
                 <input type="hidden" name="total_to_pay" id="total_to_pay" value="{{ $cur_withdraw }}" />
                  </label>
                  <span data-tooltip='(Totalamount) + (Coupon+any (hold by admin))'> <i class="glyphicon glyphicon-info-sign"></i> </span>
                 
                </div>
             
              
              
      
            
      
                <div class="form-group"> 
                  <label for="text1" class="control-label"> 
                <?php if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_AMOUNT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_AMOUNT');} ?>
                
                 </label>
                </div>
             

                @if($pendingRequestCount==0)
                  
                    <div class="form-group"> 
                      <label for="text1" class="control-label"> 
                     
                      <input  type="text" onkeypress="return isNumberKey(event)" min="0" class="form-control" required placeholder="" value="" id="pay" name="pay">
                       <input type="hidden" class="form-control" placeholder="" value="{!! Session::get('merchantid') !!}" name="mer_id" id="mer_id">
                    
                    </label>
                    </div>
                
                  <div class="form-group">
     <button class="btn btn-warning btn-sm btn-grad" type="submit" id="total_to_pay_submit" style="color:#fff"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SUBMIT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SUBMIT');} ?></button>
                         <button type="reset" class="btn btn-danger btn-sm btn-grad"><?php if (Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_RESET');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_RESET');} ?></button>
                       
                      </div>
 
                  
               </form>
                </div>
                </div>
                @else
                    <p class="alert_box">
                    <?php if (Lang::has(Session::get('mer_lang_file').'.MER_SRY_SND_RQT')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_SRY_SND_RQT');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_SRY_SND_RQT');} ?>
                    </p> 
                @endif   
              </div>
        </div>
          </div>
           
        
   
                    
                    </div>
                  </div>
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    <div style="clear: both;"></div>
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    {!! $adminfooter !!}
    <!--END FOOTER -->

  


     <!-- GLOBAL SCRIPTS -->
    <script src="<?php echo url(''); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
    <script> 
  $(document).ready(function() {
    $('#total_to_pay_submit').click(function() { 
       $(".rec-select").css({"display" : "none"});
      if($('#pay').val() == "")
      {
        $('#pay').focus();
        $('#pay').css('border', 'red');
        $(".rec-select").css({"display" : "block"});
        return false;
      }
      else 
      if(parseInt($('#pay').val()) > parseInt($('#total_to_pay').val()))
      { 
        $('#pay').focus();
        $('#pay').css('border', 'red');
        $(".rec-select").css({"display" : "block"});
        return false;
      }
      else 
      if(isNaN($('#pay').val()))
      { 
        $('#pay').focus();
        $('#pay').css('border', 'black solid 1px');
        $(".rec-select").css({"display" : "block"});
        return false;
      } else
      if($('#pay').val() <= 0)
      {
        $('#pay').focus();
        $('#pay').css('border', '#fff');
        $(".rec-select").css({"display" : "block"});
                return false;
      }
      else
      { 
        $('#pay').css('border', '');
        $('#error').html('');
      }
    });
  });
  </script>
     <script src="<?php echo url(''); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
    
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.resize.js"></script>
  <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/js/pie_chart.js"></script>
    
    


<script src="<?php echo url(''); ?>/public/assets/js/jquery-ui.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/validVal/js/jquery.validVal.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo url(''); ?>/public/assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>



<script src="<?php echo url(''); ?>/public/assets/plugins/autosize/jquery.autosize.min.js"></script>

       <script src="<?php echo url(''); ?>/public/assets/js/formsInit.js"></script>
        <script>
            $(function () { formInit(); });
        </script>

        
    
  <script src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
  <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="<?php echo url(''); ?>/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="<?php echo url(''); ?>/public/assets/js/bar_chart.js"></script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>

  <script type="text/javascript">
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode;
    //alert(charCode);  
    if (charCode > 31 && (charCode < 48 || charCode > 57 ) )
    {
        if(charCode!=46)
            return false;
    }
    
 return true;
  
}
 $(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
</script>  

</body>
     <!-- END BODY -->
</html>

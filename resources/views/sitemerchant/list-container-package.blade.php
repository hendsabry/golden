 @inject('data','App\Help')
@include('sitemerchant.includes.header') 
@php $hall_leftmenu =1; @endphp 
<div class="merchant_vendor">   
@include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')    
    <div class="right_panel">
      <div class="inner">
    <div class="service_listingrow">
            <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_con_title')!= '') {{  trans(Session::get('mer_lang_file').'.MER_con_title') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_con_title') }} @endif </h5>
        <div class="add"><a href="{{route('add-container-package',['hid'=>$hid,'bid' => $bid ]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CONTAINER') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CONTAINER') }} @endif</a></div>      
      </div>
        {!! Form::open(array('url'=>'list-container-package','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
        <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left">
        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems ">
            <select name="status" id="status">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
              <option value="1" @if($status==1) selected="selected" @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif</option>

              <option value="0" @if( $status=='0' ) selected="selected" @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_INACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_INACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_INACTIVE') }} @endif</option>
            </select>
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
          
        </div>
        <div class="search_box">
        <div class="search_filter">&nbsp;</div>
        <div class="filter_right">
          <input type="hidden" name="hid" value="{{$hid}}">
          <input type="hidden" name="bid" value="{{$bid}}">
          <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{ $search }}" />
          <input type="button" class="icon_sch" id="submitdata" />
        </div>
        </div>
      </div>
      <!-- filter_area --> 
      
      {!! Form::close() !!}
      <!-- Display Message after submition -->
      @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
        <div class="col-lg-12">
          <div class="table_wrap">
            

        
      
              <div class="panel-body panel panel-default"> 
               @if($containerlist->count() < 1) 
                <div class="no-record-area">
                   @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif
              </div>
              @else
          <div class="table container-table">
                          <div class="tr">
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Name') }} @endif</div>
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_No_People')!= '') {{  trans(Session::get('mer_lang_file').'.MER_No_People') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_No_People') }} @endif</div>
                            
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif</div> 
                         
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Image') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Image') }} @endif</div>
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif</div>                            
                             <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div>
                          </div>
                    @php 
                    $title='title';
                    $about='about';
                    @endphp
                       @if($mer_selected_lang_code !='en')
                     @php 
                       $title= 'title_'.$mer_selected_lang_code;
                       $about= 'about_'.$mer_selected_lang_code;
                   @endphp
                   @endif
                        
                        @foreach($containerlist as $val)
                        @php
                    if($val->status==1){
                    $status = trans(Session::get('mer_lang_file').'.MER_ACTIVE');
                    $dstatus = 'Active';
                    $status_class = 'status_active';
                    } else {
                    $status = trans(Session::get('mer_lang_file').'.MER_INACTIVE');
                    $dstatus = 'Inactive';
                    $status_class = 'status_deactive';
                    }


                    @endphp  
                     <div class="tr">
                     <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Name') }} @endif">{{ $val->$title }}</div>
                     <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_No_People')!= '') {{  trans(Session::get('mer_lang_file').'.MER_No_People') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_No_People') }} @endif">{{ $val->no_people }}</div>
                    
                     <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif">{{ Carbon\Carbon::parse($val->updated_at)->format('F j, Y')}}</div>
                   
                     
                      @if($val->img !='')
                     <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Image') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Image') }} @endif">   <img src="{{ $val->img }}"> </div>
                       @else
                        <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Image') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Image') }} @endif">No image</div>
                       @endif
                        <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif"> <span class="<?php echo $status_class;?>  status_active2 cstatus" data-status="<?php echo $dstatus;?>"data-id="{{ $val->id }}">{{ $status }}</span></div>
                       <div class="td td7" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif"><a href="{{ route('edit-container-package',['id' => $val->id,'hid' =>$hid,'bid' => $bid ]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EDIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EDIT') }} @endif</a></div>
                 </div>
                 @endforeach
                    
                   
           
        </div>  
 
        @endif
      <!--PAGE CONTENT PART WILL COME INSIDE IT START-->


        <!--PAGE CONTENT PART WILL COME INSIDE IT END-->

          </div>
       {{ $containerlist->links() }}
        </div> <!-- table_wrap -->
        </div>
      </div>
    </div> <!-- global_area -->
    </div>
    </div> <!-- right_panel -->
</div> <!-- merchant_vendor -->
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
   <!--  <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a><a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =$(this).data("status");
 var id =$(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

$('.status_yes').click(function(){
   var id =$(this).attr("data-id");
    var status1 =$(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     $.ajax({
        type: "GET",
        url: "{{ route('menu-hall-container-status') }}",
        data: {activestatus:activestatus,id:id},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
            $('form[name=test]').submit();
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script>
@include('sitemerchant.includes.footer')
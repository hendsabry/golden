@include('sitemerchant.includes.header') 
@php $reception_hospitality_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
    <header>
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
    </header>
    <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <div class="error arabiclang"></div>
              <!-- Display Message after submition -->
    <div class="global_area">
      <div class="row">
 
          <div class="col-lg-12">
            <div class="box"> 
              <div class="one-call-form">
                <form name="form1" method="post" id="reception" action="{{ route('storereceptionshopinfo',['id' => $id]) }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                   <div class="form_row common_field">
                   <label class="form_label">

                  <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_CITY'); @endphp</span>
                  <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); @endphp </span>

                   </label>
                                      <div class="info100"> <select class="small-sel" name="city_id"  id="city_id">
                                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_CITY') }}  
                              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY') }} @endif</option>
                                        @foreach ($city as $val)


                                    @php $ci_name='ci_name'@endphp
                                     @if($mer_selected_lang_code !='en')
                                                           @php $ci_name= 'ci_name_'.$mer_selected_lang_code; @endphp
                                     @endif    
                                        <option value="{{ $val->ci_id }}">{{ $val->$ci_name }}</option>
                                        @endforeach

                                      </select>
                 </div>
                   </div> <!-- form_row -->
                 

                  <div class="form_row">
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span>
                      </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="" id="mc_name" placeholder="" class="english"  data-validation-length="max80" maxlength="80">
                      </div>

                      <div class="arabic ar">
                        <input  class="arabic ar" value="" name="mc_name_ar"  id="mc_name_ar" placeholder=""  type="text" data-validation-length="max80"  maxlength="80" >
                      </div>
                    </div>
                  </div>



  <div class="form_row common_field">
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_ADDRES'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_ADDRES'); @endphp </span>
                      </label>
                    <div class="info100">
                      <div class="">
                      <textarea name="address"></textarea>
                      </div>
 
                    </div>
                  </div>





                 <div class="form_row common_field">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label">
                         <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_IMAGE'); @endphp</span>
                         <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_IMAGE'); @endphp </span>



                      </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"
                          >
                          <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
                                <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>
                          </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" type="file" >
                        <input type="hidden" name="updateimage" value="">
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field" >
                    <label class="form_label posrel">
                      <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS_URL'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS_URL'); @endphp </span>
                      <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a>
                      </label>
                    <div class="info100">
                        <input type="url" name="google_map_address" id="google_map_address" value=""  placeholder=""  data-validation-length="max80" maxlength="80">
                     
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label posrel">
                     <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span>
<a href="javascript:void(0);" class="address_image_tooltip"><span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span></a>   
                      </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo1">
                        <div class="file-btn-area">
                          <div id="file_value2" class="file-value"></div>
                          <div class="file-btn">
                                <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
                                <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>

                           </div>
                        </div>
                        </label>
                        <input id="company_logo1" name="address_image" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" type="file" >
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->


                   <div class="form_row common_field">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label posrel">
                     <span class="english">@php echo lang::get('mer_en_lang.TERMSANDCONDITION'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.TERMSANDCONDITION'); @endphp </span>
   
                      </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo9">
                        <div class="file-btn-area">
                          <div id="file_value8" class="file-value"></div>
                          <div class="file-btn">
                                <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
                                <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>

                           </div>
                        </div>
                        </label>
                        <input id="company_logo9" name="mc_tnc" accept="pdf" class="info-file" type="file" >
                      </div>
                    </div>
                  </div>







                  <div class="form_row">
               <div class="form-btn-section english"><input type="submit" id="hallsubmit" name="addhallpics" value="Submit"></div> 
 <div class="form-btn-section arabic ar"><input type="submit" id="hallsubmit" name="addhallpics" value="خضع"></div>
                  </div>
                  <!-- form_row -->
                </form>
              </div>
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 

<script>       
$("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
$("#reception").validate({
                  ignore: [],
                  rules: {

                  city_id: {
                       required: true,
                      },  
                  mc_name: {
                       required: true,
                      },
                      

                       mc_name_ar: {
                       required: true,
                      },
                      google_map_address: {
                       required: true,
                      },
                      mc_img: {
                      required: true,
                       accept:"png|jpe?g|gif",
                      },

                       address_image: {
                       accept:"png|jpe?g|gif",
                      },
                 
                       mc_tnc: {
                          required: true,
                       accept:"pdf",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             city_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",
                      },

             mc_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SHOP_NAME') }} @endif",
                      },  

                 mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOP_NAME_AR'); @endphp",
                      }, 

                        google_map_address: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_URL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_SHOP_URL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_SHOP_URL') }} @endif",
                      },    

                        mc_img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      }, 

                       address_image: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      }, 

                       mc_tnc: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_TNC')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_TNC') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_TNC') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },                        
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


              @if($mer_selected_lang_code =='ar')

                    if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                   
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                      if (typeof valdata.google_map_address != "undefined" || valdata.google_map_address != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.address_image != "undefined" || valdata.address_image != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                       if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       
                    }
               @else
               
                  if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                   
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.arabic_tab').trigger('click');     

                    }
                      if (typeof valdata.google_map_address != "undefined" || valdata.google_map_address != null) 
                    {

                        $('.arabic_tab').trigger('click');     

                    }
                     if (typeof valdata.address_image != "undefined" || valdata.address_image != null) 
                    {

                        $('.arabic_tab').trigger('click');     

                    }
                       if (typeof valdata.mc_tnc != "undefined" || valdata.mc_tnc != null) 
                    {

                        $('.arabic_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       
                    }



               @endif     

                    


                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
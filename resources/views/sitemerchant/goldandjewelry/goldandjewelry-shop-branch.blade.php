@include('sitemerchant.includes.header') 
@php $goldandjewelry_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BRANCHLIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BRANCHLIST') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BRANCHLIST') }} @endif </h5>
          @if(Session::get('LoginType') !='4')
        <div class="add"><a  href="{{ route('goldandjewelry-shop-info',['id' => request()->id, 'hid' => request()->hid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_BRANCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_BRANCH') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_BRANCH') }} @endif </a></div>
        @endif
      </div>
      <!-- service_listingrow -->

@php
$reqid = request()->id;
$hid = request()->hid;

@endphp

      {!! Form::open(array('url'=>"goldandjewelry-shop-branch/$reqid/$hid",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}

 
       <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left two-srarch-box">
          <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems ">
           @php $cityname = request()->cityname; $statuss = request()->status; $searchh= request()->search;  $getCityList = Helper::getcitylist(); @endphp

           <select name="cityname" id="cityname" class="city_type">
          

  <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif</option>
               @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;">  @if($mer_selected_lang_code !='en'){{$cbval->co_name_ar}}  @else {{$cbval->co_name}}  @endif </option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                            @php $ci_id = $val->ci_id; @endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" @if(isset($cityname) && $cityname== $ci_id) {{"SELECTED"}}  @endif >{{ $val->$ci_name }}</option>
                        @endforeach
                        @endforeach




            </select> 

            <select name="status" id="status" class="city_type">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
              <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
              <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
            </select>
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
        </div>
        <div class="search_box">
          <div class="search_filter">&nbsp;</div>
          <div class="filter_right">
            <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$searchh or ''}}" />
            <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
          </div>
        </div>
      </div>
      <!-- filter_area -->
  
      {!! Form::close() !!}


      <!-- Display Message after submition -->
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap">
              <div class="panel-body panel panel-default">

@if($productdata->count() <1)


<div class="no-record-area">
@if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif

</div>

@else


                <div class="table listbranch-table">
                  <div class="tr">
                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_NAME') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_NAME') }} @endif  </div>
                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BRANCH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BRANCH_NAME') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BRANCH_NAME') }} @endif  </div>
                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif </div>
                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MANAGER') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MANAGER') }} @endif </div>
                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif </div>
                    <div class="table_heading"> @if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif </div>                   
                    <div class="table_heading view_center"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BRANCH_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BRANCH_INFO') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BRANCH_INFO') }} @endif </div>
                  </div>

 
 
          @foreach($productdata as $val)
          @php
          $TopCat =  Helper::getshopname($val->parent_id);
          $TopCity =  Helper::getcity($val->city_id);
          $Topmanager =  Helper::getbranchmgr($val->branch_manager_id);
          @endphp
                  <div class="tr">
                    <div data-title=" @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_NAME') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_NAME') }} @endif" class="td td1">{{$TopCat->mc_name}}</div>
                    <div data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_BRANCH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BRANCH_NAME') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BRANCH_NAME') }} @endif" class="td td2">{{$val->mc_name}}</div>
                    <div data-title=" @if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif " class="td td3">{{$TopCity->ci_name}}</div>
                    <div data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MANAGER') }}   @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MANAGER') }} @endif" class="td td4">{{$Topmanager->mer_fname or 'N/A'}}</div>
                    <div data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }}    @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif" class="td td5"> <img src="{{$val->mc_img}}" alt="" width="150"> </div>
                   

      <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.STATUS')!= '') {{ trans(Session::get('mer_lang_file').'.STATUS')}}  @else {{ trans($MER_OUR_LANGUAGE.'.STATUS')}} @endif">
                    @if ($val->mc_status==1)
                   <span class="status_active  status_active2 cstatus" data-status="Active" data-id="{{$val->mc_id}}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTIVE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')}} @endif </span> 
                    @else
                    <span class="status_deactive  status_active2 cstatus" data-id="{{$val->mc_id}}" data-status="Inactive"> @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_DEACTIVE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')}} @endif</span>
                    @endif

                    </div>
 
             
                    <div data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_BRANCH_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BRANCH_INFO') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BRANCH_INFO') }} @endif " class="td td7 view_center"><a  href="{{route('goldandjewelry-shop-info',['id'=>$reqid,'hid'=>$hid,'itemid'=>$val->mc_id])}}"><img src="{{url('')}}/public/assets/img/view-icon.png" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></a></div>
                  </div>

@endforeach

                </div>

              
@endif
 
                <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
                <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
              </div>

                 <!-- PAGINATION --> 
              {{ $productdata->links() }} 
              <!-- PAGINATION --> 
            </div>
          </div>
        </div>
      </div>
      <!--global_area-->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- <div merchant_vendor -->
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup -->
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'hotellist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script>
<script type="text/javascript">

  $(document).ready(function(){
  $('form[name=test]').submit();
   $('#submitdata').click(function(){            
   $('form[name=filter]').submit();
  });
  });

</script>
@include('sitemerchant.includes.footer')
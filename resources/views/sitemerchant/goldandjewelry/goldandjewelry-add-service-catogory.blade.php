@include('sitemerchant.includes.header') 
@php $goldandjewelrybig_leftmenu  =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->catitemid=='')
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CATEGORY') }} @endif </h5>
        @else
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_CATEGORY') }} @endif </h5>
        @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> @php $hid = request()->hid; $id = request()->id; $itemid = request()->itemid; $catitemid = request()->catitemid; @endphp
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <form name="form1"  id ="menucategory" method="post" action="{{ route('goldandjewelry-updatecategory') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CATEGORY_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" value="{{$getAttr->attribute_title or ''}}" class="english" name="category_name" maxlength="90" id="category_name" required="" >
                      </div>
                      <div class="arabic ar">
                        <input class="arabic ar" id="category_ar" maxlength="90"  name="category_name_ar"  value="{{$getAttr->attribute_title_ar or ''}}"  required=""  type="text" >
                      </div>
                    </div>
                  </div>
                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CREATED_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CREATED_IMAGE'); @endphp </span> </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="catimage" id="company_logo1" class="info-file">
                    </div>
                     @if(!isset($getAttr->image)) <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span> @endif
                    @if(isset($getAttr->image) && $getAttr->image !='')
                    <div class="form-upload-img"> <img src="{{$getAttr->image}}"> </div>
                     <input type="hidden" value="{{ $getAttr->image }}" name="updatecatimage">
                      @endif 
                     </div>
                  <div class="form_row_right common_field">
                  <div class="info100">
                    <div class="save-card-line">
                      <input id="men" type="checkbox" value="1"  name="write_on_jewellery" @if(isset($getAttr->write_on_jewellery) && $getAttr->write_on_jewellery=='1') CHECKED @endif  data-validation="checkbox_group" data-validation-qty="min1" >
                      <label for="men"> <span class="english">@php echo lang::get('mer_en_lang.Write_On_jewellery'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Write_On_jewellery'); @endphp </span> </label>
                    </div>
                   
                    <br>
                    </div>
                </div>
                  <div class="form_row_left">
                    <input type="hidden" name="id" value="{{ $id or ''}}">
                    <input type="hidden" name="hid" value="{{ $hid or ''}}">
                    <input type="hidden" name="itemid" value="{{ $itemid or ''}}">
                    <input type="hidden" name="catitemid" value="{{ $catitemid or ''}}">
                   

                    @if($catitemid=='')
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                    <div class="english">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <!-- form_row -->
                    @else
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="تحديث">
                    </div>
                    <div class="english">
                      <input type="submit" name="submit" value="Update">
                    </div>
                    <!-- form_row -->
                    @endif </div>
                </div>
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },


                      @if(isset($getAttr->image) && $getAttr->image !='')
                      catimage: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      },
                      @else
                        catimage: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
                      @endif 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
           category_name: {
           
                required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_CATEGORY_NAME'); @endphp",
                      }, 
           category_name_ar: {
               required:  "@php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME_VALIDATION_AR'); @endphp",
                      },

                      catimage: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },
                },
                invalidHandler: function(e, validation){
 
                    var valdata=validation.errorList;
                  
                    var i;
                    var erroren=0;
                    var start=5;
                    var lang ='ar';
                    var val=0
                    
                    for (i = 0; i < start ;  i++) {
                      
                      if (typeof valdata[i] !== 'undefined') {  
                        var data=valdata[i].element.classList;
                        $.inArray('ar',data);
                                        console.log("invalidHandler : validation", $.inArray('ar',data)); 
                                              if($.inArray('ar',data) == -1) 
                                              {
                                                  val=1;

                                              } 

                                          } 
                                         
                      }

                                              if(val==0)
                                              {

                                                $('.arabic_tab').trigger('click');  
                                              }


                    },
 

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
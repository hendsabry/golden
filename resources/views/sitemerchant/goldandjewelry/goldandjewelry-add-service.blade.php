@include('sitemerchant.includes.header') 
@php $goldandjewelrybig_leftmenu  =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->serviceid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_PRODUCTS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_PRODUCTS') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_PRODUCTS') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATE_PRODUCT')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_PRODUCT') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_PRODUCT') }} @endif </h5>
        @endif
        
        
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition   caliber weight -->
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif
              <!-- Display Message after submition -->
              <form name="form1" id="addmanager" method="post" action="{{ route('storegoldandjewelryservice') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_PRODUCT_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRODUCT_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="service_name" maxlength="60" value="{{$getservice->pro_title or ''}}"  id="service_name" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="arabic ar" name="service_name_ar" value="{{$getservice->pro_title_ar or ''}}"  maxlength="60" id="service_name_ar" required="" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.CATEGORY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.CATEGORY'); @endphp</span> </label>
                    <div class="info100">
                      <div> @php
                        $hid = request()->hid;
                        $itemid = request()->itemid;
                        $Getcatinfo = Helper::getbeautyCat($itemid);                      
                        @endphp
                        <select class="small-sel" name="category" required="">
                          <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT') }} @endif</option>
                          @foreach($Getcatinfo as $vals)
                             @if($mer_selected_lang_code !='en')
                              @php $ci_name= 'attribute_title_ar'; @endphp
                              @else
                               @php $ci_name= 'attribute_title'; @endphp
                              @endif
                          <option value="{{$vals->id or ''}}" @if(isset($getservice->attribute_id) && $getservice->attribute_id== $vals->id) SELECTED @endif >{{$vals->$ci_name or ''}}</option>
                          
                          
                      @endforeach
                      
                        
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>

                             

                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                           
                            @if(isset($getservice->pro_Img) && $getservice->pro_Img !='')
                           <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                           @endif

                        @if(isset($getservice->pro_Img) && $getservice->pro_Img !='')

                        <div class="form-upload-img"><img src="{{$getservice->pro_Img or ''}}"></div> @endif </div>
                    </div>
                  </div>


   
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->

                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Weight'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Weight'); @endphp </span> </label>
                    <div class="info100">
                      <div class="englishs">
                        <input type="text" class="xs_small" name="weight" id="weight" onkeypress="return isNumber(event);" onkeyup="getpricecalculation('{{ $getCal->value or '' }}','{{ $itemid }}');"  maxlength="9" value="<?php if(isset($getAtt->value) && $getAtt->value!=''){echo $getAtt->value;}?>"  id="weight">
                      </div>
                    </div>
                  </div>
                     <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PRICE_OF_WORKMANSHIP'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PRICE_OF_WORKMANSHIP'); @endphp </span> </label>
                    <div class="info100">
                      <div class="englishs">
                        <input type="text" class="xs_small" name="workmanprice" id="workmanprice" onkeypress="return isNumber(event);" onkeyup="getpricecalculation('{{ $getCal->value or '' }}','{{ $itemid }}');" maxlength="9" value="<?php if(isset($getservice->workmanprice) && $getservice->workmanprice!=''){echo $getservice->workmanprice;}?>"  id="weight">
                      </div>
                    </div>
                  </div>

                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Caliber'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Caliber'); @endphp </span> </label>
                    <div class="info100">
                      <div class="englishs">
                        <select name="caliber" class="xs_small" onchange="getpricecalculation(this.value,'{{ $itemid }}');" required>
                            <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT') }} @endif</option>
                            <option value="16" <?php if(isset($getCal->value) && $getCal->value==16){echo 'Selected="Selected"';}?>>16</option>
                            <option value="18" <?php if(isset($getCal->value) && $getCal->value==18){echo 'Selected="Selected"';}?>>18</option>
                            <option value="21" <?php if(isset($getCal->value) && $getCal->value==21){echo 'Selected="Selected"';}?>>21</option>
                            <option value="22" <?php if(isset($getCal->value) && $getCal->value==22){echo 'Selected="Selected"';}?>>22</option>
                            <option value="24" <?php if(isset($getCal->value) && $getCal->value==24){echo 'Selected="Selected"';}?>>24</option>

                        </select>
                       
                      </div>
                    </div>
                  </div>
                 
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_AMOUNT'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_AMOUNT'); @endphp </span> </label>
                    <div class="info100">
                      <div class="englishs">
                        <input type="text" class="xs_small notzero" name="amount" onkeypress="return isNumber(event)" maxlength="9" value="<?php if(isset($getservice->pro_price) && $getservice->pro_price!=''){echo $getservice->pro_price;}?>"  id="amount" required="" readonly >
                      </div>
                    </div>
                  </div>
                   <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="<?php if(isset($getservice->pro_qty) && $getservice->pro_qty!=''){echo $getservice->pro_qty;}?>"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="<?php if(isset($getservice->pro_discount_percentage) && $getservice->pro_discount_percentage!=''){echo $getservice->pro_discount_percentage;}?>" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>


                      <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.PRODUCT_WEIGHT'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PRODUCT_WEIGHT'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumberKey(event,this)" name="pro_weight" id="pro_weight" maxlength="15" value="{{ $getservice->pro_weight or '' }}"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>


                  <div class="form_row_left ">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea name="description" class="english" id="description" rows="4" cols="50">{{$getservice->pro_desc or ''}}</textarea>
                      </div>
                      <div  class="arabic ar" >
                        <textarea class="arabic ar" name="description_ar" id="description_ar" rows="4" cols="50">{{$getservice->pro_desc_ar or ''}}</textarea>
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="{{request()->id}}">
                      <input type="hidden" name="hid" value="{{request()->hid}}">
                      <input type="hidden" name="itemid" value="{{request()->itemid}}">
                      <input type="hidden" name="serviceid" value="{{request()->serviceid}}">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->





<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script>
<script type="text/javascript">
  
$("#addmanager").validate({
              ignore: [],
               rules: {
                  category: {
                       required: true,
                      },

                       service_name: {
                       required: true,
                      },
                      service_name_ar: {
                       required: true,
                      },                       
                       amount: {
                       required: true,
                      },
                    
                    
                       description: {
                       required: true,
                      },
                    @if(!isset($getservice->pro_Img))
                    stor_img: {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },
                    @else
                    stor_img: {
                    accept:"png|jpe?g|gif",
                    },
                    @endif   
                    description_ar: {
                    required: true,                    
                      },

                       quantity: {
                       required: true,
                      },

                       caliber: {
                       required: true,
                      },

                        weight: {
                       required: true,
                      },


                      

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
              
           messages: {
              category: {
              
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_CATEGORY') }} @endif ",
                      },  
 

                 service_name: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_PRODUCTNAME'); @endphp",
                      },  
                

                   caliber: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_CALIBER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CALIBER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CALIBER') }} @endif",
                      },  


                  weight: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_WEIGHT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_WEIGHT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_WEIGHT') }} @endif",
                      },       


          service_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_SERVICE_NAME'); @endphp",
                      },  
 

                amount: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_AMOUNT') }} @endif",
                      },

               quantity: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY') }} @endif ",
                      },


                

              description: {
               required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_DISCRIPTION'); @endphp",
                      },

                   description_ar: {
               required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_DISCRIPTION'); @endphp",
                      },
     

                         stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif ",
                      },   
                                             
                                        
                     
                },             
          invalidHandler: function(e, validation){ 
                    var valdata=validation.invalid;
 // category service_name service_name_ar stor_img  duration amount discount description description_ar       
 @if($mer_selected_lang_code !='en')
                    
                    if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    

                     if (typeof valdata.quantity != "undefined" || valdata.quantity != null) 
                    {
                    $('.english_tab').trigger('click');
                    }


                   
                    if (typeof valdata.amount != "undefined" || valdata.amount != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.discount != "undefined" || valdata.discount != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null)
                    {
                    $('.arabic_tab').trigger('click');
                    }

                    if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');  
                    }

                     if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');  
                    }
if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                    $('.arabic_tab').trigger('click'); 
                    }
                 if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.arabic_tab').trigger('click');
                    }   
                      
          @else
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null)
                    {
                    $('.arabic_tab').trigger('click');
                    }

                    if (typeof valdata.service_name_ar != "undefined" || valdata.service_name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');  
                    }
                
                    if (typeof valdata.service_name != "undefined" || valdata.service_name != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                   
                   
                    if (typeof valdata.amount != "undefined" || valdata.amount != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.discount != "undefined" || valdata.discount != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                      if (typeof valdata.quantity != "undefined" || valdata.quantity != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                    if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                 if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.english_tab').trigger('click');
                    }  
                    
@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
 /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }



</script>



<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->
@include('sitemerchant.includes.footer')

<script type="text/javascript">
  
  function getpricecalculation(cali,branchid){  


var productwt=jQuery('#weight').val();
var productworkmanprice=jQuery('#workmanprice').val();

    jQuery.ajax({
        type: "GET",
        url: "{{ route('getgoldprice') }}",
        data: {title:cali,branchid:branchid},
        success: function(data) {
         
            var json = JSON.stringify(data);
              var obj = JSON.parse(json);
              var productcaliberprice=obj.calieberprice;
              var totalwtprice=parseInt(productwt)*parseFloat(productcaliberprice);
              var totalproductprice=parseFloat(totalwtprice) + parseFloat(productworkmanprice);
           // alert(totalproductprice);

            jQuery('#amount').val(totalproductprice);
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
  }
</script>
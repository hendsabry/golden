@include('sitemerchant.includes.header') 
@php $goldandjewelry_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->hid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_BEAUTYSHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_BEAUTYSHOP') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_BEAUTYSHOP') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_BEAUTYSHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_BEAUTYSHOP') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_BEAUTYSHOP') }} @endif </h5>
        @endif
        
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <form name="form1" method="post" id="addhotel" action="{{ route('savegoldandjewelryservice') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="{{$getlisting->mc_name or ''}}" id="hotal_name"  class="english"  data-validation-length="max80" maxlength="40">
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" class="arabic ar" name="mc_name_ar"  id="mc_name_ar"   type="text" data-validation-length="max80" value="{{$getlisting->mc_name_ar or ''}}"  maxlength="40" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <input type="hidden" name="parent_id" value="{{ request()->id}}">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_LOGO'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_LOGO'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>

                        <input  name="mcimg"  type="hidden" value="{{$getlisting->mc_img or ''}}" >
                        <input id="company_logo" name="mc_img" class="info-file" type="file" @if(!isset($hid)) required="" @endif>
                      </div>
                       <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                      @if(isset($getlisting->mc_img) && $getlisting->mc_img!='') <div class="form-upload-img"><img src="{{$getlisting->mc_img or ''}}"></div> @endif </div>
                  </div>
                  <div class="form_row_left">
                    <input  name="hid"  type="hidden" value="{{$hid or ''}}" >
                    <div class="english">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script type="text/javascript">
$("#addhotel").validate({
                  ignore: [],
                  rules: {
                  mc_name: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },
                  @if(isset($getlisting->mc_img) && $getlisting->mc_img=='')
                      mc_img: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },
                      @else
mc_img: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      },


                @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mc_name: {
       
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_SHOP_NAME'); @endphp",
                      },  

                 mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_SHOP_NAME_AR'); @endphp",
                      },    
 
                        mc_img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },                       
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

   @if($mer_selected_lang_code !='en')

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                   
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.arabic_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }

@else
                  if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }
                      if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                   
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }


@endif

 
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
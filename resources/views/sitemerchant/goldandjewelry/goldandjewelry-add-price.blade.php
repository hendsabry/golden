@include('sitemerchant.includes.header') 
@php $goldandjewelrybig_leftmenu  =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->serviceid=='')
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_GOLD_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_GOLD_PRICE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_GOLD_PRICE') }} @endif </h5>
        @else
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_GOLD_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_GOLD_PRICE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_GOLD_PRICE') }} @endif </h5>
        @endif </header>
      
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
               @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif
              <span id='allreq' style="display: none;"> @if (Lang::has(Session::get('mer_lang_file').'.Complete_all_the_price')!= '') {{  trans(Session::get('mer_lang_file').'.Complete_all_the_price') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.Complete_all_the_price') }} @endif</span>
              <form name="form" id="goldprice" method="post" action="{{ route('storegoldandjewelryprice') }}" enctype="multipart/form-data" onsubmit="return validate()">
                {{ csrf_field() }}
                <div class="form_row">
                  @php for ($i=0; $i <=4 ; $i++) { 
                      if($i==0){ $c=16;}
                      if($i==1){ $c=18;}
                      if($i==2){ $c=21;}
                      if($i==3){ $c=22;}
                      if($i==4){ $c=24;}
                    @endphp
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english">Caliber {{ $c }} (price/gm)</span> </label>
                    <div class="info100">
                      <div class="english">
                      <input type="text" maxlength="6" class="english" name="price[]" value="{{$getCalprice[$i]->price or ''}}" id="title" class="xs_small valid" onkeypress="return isNumber(event)">
                      <input type="hidden" name="title[]" value="{{ $c }}">
                      </div>                    
                    </div>
                  </div>
                   
                 @php }  @endphp                   
                  
                 
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="{{request()->id}}">
                      <input type="hidden" name="hid" value="{{request()->hid}}">
                      <input type="hidden" name="itemid" value="{{request()->itemid}}">
                      <input type="hidden" name="serviceid" value="{{request()->serviceid}}">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer')

<script type="text/javascript">

function validate(){

  var eduInput = document.getElementsByName('price[]');
  for (i=0; i<eduInput.length; i++)
    {
     if (eduInput[i].value == "")
      {
       jQuery('#allreq').css('display','');    
       return false;
      }
    }
}

</script>
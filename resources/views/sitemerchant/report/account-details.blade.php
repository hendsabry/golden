@include('sitemerchant.includes.header')
 
<div class="merchant_vendor">  
  @include('sitemerchant.includes.breadcrumb') 
  <div class="profile_box">
  <div class="profile_area">
<div class="inner">
<div class="row">
<div class="col-lg-12">
	 <header>
	 <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ACCOUNT_DETAILS')!= '') {{  trans(Session::get('mer_lang_file').'.ACCOUNT_DETAILS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ACCOUNT_DETAILS') }} @endif</h5>
<!-- Display Message after submition --> 
@if (Session::has('message'))
<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif 
<!-- Display Message after submition -->
</header>
	  
	 <div class="box commonbox"> 

{!! Form::open(array('url' => '/store-bank-info','method' => 'POST', 'id'=> 'store_bank_info', 'name' =>'store_bank_info' )) !!}

	  <div class="global_area">
	  <div class="form_row">
	  <div class="form_row_left">
	  <label class="form_label" for="text1">@if (Lang::has(Session::get('mer_lang_file').'.Bank_Name')!= '') {{  trans(Session::get('mer_lang_file').'.Bank_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Bank_Name') }} @endif </label>
	  <div class="info100"> 
	  <input class="form-control" id="bank_name" value="{{$getTotalAmount->bank_name or ''}}" maxlength="90" name="bank_name"  type="text"> </div>
	  </div>
	  <div class="form_row_right">
	  <label class="form_label" for="text1">@if (Lang::has(Session::get('mer_lang_file').'.Account_number')!= '') {{  trans(Session::get('mer_lang_file').'.Account_number') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Account_number') }} @endif </label>
	  <div class="info100"> <input class="form-control"  value="{{$getTotalAmount->account_number or ''}}" onkeypress="return isNumber(event)" id="ac_number" maxlength="18" name="ac_number" type="text"> </div>
	  </div>
	  </div> 
	  <div class="form_row">
	  <div class="form_row_right">
	  <label class="form_label" for="text1">@if (Lang::has(Session::get('mer_lang_file').'.Note')!= '') {{  trans(Session::get('mer_lang_file').'.Note') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Note') }} @endif </label>
	  <div class="info100"> <input class="form-control" value="{{$getTotalAmount->note or ''}}" id="note" maxlength="200" name="note"  type="text"> </div>
	  </div> 
	  </div>  
	  <div class="form_row">
	  	<div class="form_row_left">
	  <button class="form_button" type="submit" id="submit">@if (Lang::has(Session::get('mer_lang_file').'.UPDATE')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE') }} @endif</button>
	  </div>
	  </div>

	  </div>
	  </form> 

	</div>
</div>
</div>
</div>
</div>
</div>
 
</div>
@include('sitemerchant.includes.footer')

<script type="text/javascript">

$("#store_bank_info").validate({
                  ignore: [],
                  rules: {
                  bank_name: {
                       required: true,
                      },

                       ac_number: {
                       required: true,
                      },
                        
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
           
            bank_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_BANK_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_BANK_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_BANK_NAME') }} @endif",
                      }, 
             
                 ac_number: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_ACCOUNT_NUMBER')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_ACCOUNT_NUMBER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_ACCOUNT_NUMBER') }} @endif",
                      }, 

                   
                },
                invalidHandler: function(e, validation){
                     },

                submitHandler: function(form) {
                    form.submit();
                }
            });
 


</script> 
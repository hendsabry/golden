@include('sitemerchant.includes.header')
   <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor">  
  @include('sitemerchant.includes.breadcrumb') 
   <div class="service_listingrow">
             <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_REPORTING')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_REPORTING') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_REPORTING') }} @endif</h5>
			</div> 
{!! Form::open(array('url'=>"order-reporting",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!} 

        <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
        <div class="filter_area">
        <div class="order-filter-line">
         
        <div class="of-date-box studio-date-box">
           <input name="dateFrom"  class="cal-t"  placeholder="@if (Lang::has(Session::get('mer_lang_file').'.DateFrom')!= '') {{trans(Session::get('mer_lang_file').'.DateFrom') }} @else  {{trans($MER_OUR_LANGUAGE.'.DateFrom') }} @endif" id="dateFrom"  type="text"  maxlength="12"   value="{{request()->dateFrom}}" /> 
       
         <input name="dateTo" id="dateTo" type="text" maxlength="12" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.DateTo')!= '') {{trans(Session::get('mer_lang_file').'.DateTo') }} @else  {{trans($MER_OUR_LANGUAGE.'.DateTo') }} @endif"  class="cal-t studio-date" value="{{request()->dateTo}}" />
         <span for="dateTo" generated="true" class="error" id="todata"> </span>

         <input name="" value="@if (Lang::has(Session::get('mer_lang_file').'.Apply')!= '') {{  trans(Session::get('mer_lang_file').'.Apply') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Apply') }} @endif" class="applu_bts" type="submit">
        </div>
        </div>
        </div>
      <!-- filter_area -->   
      {!! Form::close() !!}
  <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
<div>&nbsp;</div>
  <div class="global_area">
	<div class="row">
	<div class="col-lg-12">
	
	
	<div id="chart-div"></div>
	@donutchart('IMDB', 'chart-div')
	<!--PAGE CONTENT PART WILL COME INSIDE IT END-->
	 
  <div class="clear"><br/></div>

{!! Form::open(array('url'=>"order-reporting",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!} 
<input type="hidden" name="dateFrom" value="{{request()->dateFrom}}">
<input type="hidden" name="dateTo" value="{{request()->dateTo}}">
<div class="order_page_select" style="max-width: 200px; width: 100%; float: left;">
<select name="year"  onchange="submit(this.value)"> 
	<option value="">Year</option>
	@php  for($i=2018;$i<=2025;$i++){ @endphp
	<option value="{{$i}}" @if($year== $i) {{'Selected'}}   @endif>{{$i}}</option>
 @php  } @endphp
</select>
</div>
<form >
  <div class="clear"><br/></div>

 <div id="perf_div" style="overflow: hidden; clear: both; width: 100%;"></div>
	@columnchart('Finances', 'perf_div')

	</div>
	</div>
 
</div>
</div>
@include('sitemerchant.includes.footer')
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        @else
        $('#todata').html("End date should be greater than From date");
        @endif
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>     
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

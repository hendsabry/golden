   <?php $current_route = Route::getCurrentRoute()->uri(); ?>
    <div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading">SERVICES</h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                <li <?php if($current_route == 'mer_services_dashboard' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('mer_services_dashboard');?>">
                        <i class="icon-dashboard"></i>&nbsp;Service Dashboard</a>                   
                </li>
  <li <?php if($current_route == 'mer_add_services' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                  
                    <a href="<?php echo url('mer_add_services');?>" >
                        <i class=" icon-plus-sign"></i>&nbsp;Add Services
	                </a>                   
                </li>
                <li <?php if($current_route == 'mer_manage_services' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
                    <a href="<?php echo url('mer_manage_services');?>" >
                        <i class=" icon-edit"></i>&nbsp; Manage Services
                   </a>                   
                </li>
	 
            </ul>

        </div>

<!-- HALL SECTION LEFT MENU START-->
@if(isset($hall_leftmenu) && $hall_leftmenu==1)
@php
$hallmenu = '';
$ids = request()->id;
 if(Request::is('list-hall')) {     $listhall = 'active';  } 
if(Request::is('sitemerchant_dashboard')) {     $sitemerchantdashboard = 'active'; } 
if(Request::is('hall-review-comments')) {     $hallreviewcomments = 'active'; $hallmenu = 'hall_riview';} 
if(Request::is('hall-order')) {     $hallorder = 'active'; } 
if(Request::is('list-hall-offer') || Request::is('add-hall-offer') || Request::is("edit-hall-offer/$ids")) {     $listhalloffer = 'active'; } 
if(Request::is('list-container-package')) {     $listcontainerpackage = 'active'; } 
if(Request::is('list-hall-menu') || Request::is('add-hall-menu') || Request::is("edit-hall-menu/$ids")) {     $listhallmenu = 'active'; }
if(Request::is('editmenucategroy')) {     $menulist = 'active'; } 
if(Request::is('categorymenu')) {     $menulist = 'active'; } 
if(Request::is('menulist')) {     $menulist = 'active'; } 
if(Request::is("edit-container-package/$ids")) {     $listcontainerpackage = 'active'; }
if(Request::is('add-container-package')) {     $listcontainerpackage = 'active'; } 
if(Request::is('hall-paid-services')) {     $hallpaidservices = 'active'; } 
if(Request::is('hall-free-services')) {     $hallfreeservices = 'active'; }
if(Request::is('hall-picture')) {     $hallpicture = 'active'; $hallmenu = 'menuright';} 
if(Request::is('hall-info')) {     $hallinfo = 'active'; } 
@endphp

<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span>

@php     
	$putY = Session::get('menuss');
	if($mer_selected_lang_code !='en' && $putY==5) 
	{
	$hotalHall ='كبير'; 
	}
	elseif($mer_selected_lang_code !='ar' && $putY==5) 
	{
	$hotalHall ='Large';
	}
	elseif($mer_selected_lang_code !='en' && $putY==6) 
	{
	$hotalHall ='صغير';
	}
	elseif($mer_selected_lang_code !='ar' && $putY==6) 
	{
	$hotalHall ='Small';
	}
	else
	{
	$hotalHall ='';
	}
 @endphp

	<span>{{$hotalHall}} @if (Lang::has(Session::get('mer_lang_file').'.HOTELS')!= '') {{  trans(Session::get('mer_lang_file').'.HOTELS') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOTELS') }} @endif</span></div> <!-- breadcrumb -->

@endif
<!-- HALL SECTION LEFT MENU END-->



@if(isset($serviceleftmenu) && $serviceleftmenu==1)
 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.HOTELS')!= '') {{  trans(Session::get('mer_lang_file').'.HOTELS') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOTELS') }} @endif</span></div> <!-- breadcrumb -->
@endif


@if(isset($wallet_inner_leftmenu) && $wallet_inner_leftmenu==1)
  <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.MyWallet')!= '') {{  trans(Session::get('mer_lang_file').'.MyWallet') }} @else {{ trans($MER_OUR_LANGUAGE.'.MyWallet') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- RECEPTION AND HOSPITALITY--> 

@if(isset($reception_hospitality_leftmenu) && $reception_hospitality_leftmenu==1)
 
@php
$idd = request()->id;
$ShopId = request()->shopid;
$itemid =  request()->itemid;
$cid = request()->cid;
if(Request::is("reception-hospitality/$idd/$ShopId"))            {     $receptionhospitality = 'active';        } 
if(Request::is("reception-shop-picture/$idd/$ShopId"))           {     $receptionshoppicture = 'active';        } 
if(Request::is("reception-list-attributes/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId/$itemid"))        {     $receptionlistattributes = 'active';     } 
if(Request::is("reception-list-informations/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId/$cid"))      {     $receptionlistinformations = 'active';   } 
if(Request::is("reception-list-workers/$idd/$ShopId"))           {     $receptionlistworkers = 'active';        } 
if(Request::is("reception-list-packages/$idd/$ShopId") || Request::is("reception-add-packege/$idd/$ShopId")  || Request::is("reception-add-packege/$idd/$ShopId/$cid"))          {     $receptionlistpackages = 'active';       } 
if(Request::is("reception-list-orders/$idd/$ShopId"))            {     $receptionlistorders = 'active';         } 
if(Request::is("reception-list-reviewcomments/$idd/$ShopId"))    {     $receptionlistreviewcomments = 'active'; }  
if(Request::is("reception-shop-list/$idd/$ShopId"))          {     $receptionshoplist = 'active'; }   
@endphp
 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.reception_hospitality')!= '') {{  trans(Session::get('mer_lang_file').'.reception_hospitality') }} @else {{ trans($MER_OUR_LANGUAGE.'.reception_hospitality') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- Start Singer--> 

@if(isset($singer_leftmenu) && $singer_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;
if(Request::is("manage-singer/$idd/$hid"))            {     $managesinger = 'active';        } 
if(Request::is("singer-photo-video/$idd/$hid"))           {     $singerphotovideo = 'active';        } 
if(Request::is("singer-comments-review/$idd/$hid"))        {     $singercommentsreview = 'active';     } 
if(Request::is("singer-quoted-requested-list/$idd/$hid"))      {     $singerrequestedlist = 'active';   } 
 @endphp
 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.singer')!= '') {{  trans(Session::get('mer_lang_file').'.singer') }} @else {{ trans($MER_OUR_LANGUAGE.'.singer') }} @endif</span></div> <!-- breadcrumb -->
@endif


<!-- End Singer--> 
 

<!-- Start Acoustics--> 

@if(isset($acoustics_leftmenu) && $acoustics_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;
$itmid = request()->itmid;
if(Request::is("acoustics/$idd/$hid")) {$managesinger = 'active';} 
if(Request::is("acoustics-photo-video/$idd/$hid")) {$singerphotovideo = 'active';} 
if(Request::is("acoustics-comments-review/$idd/$hid")) {$singercommentsreview = 'active';} 
if(Request::is("acoustics-quoted-requested-list/$idd/$hid") || Request::is("acoustics-quoted-requested-view/$idd/$hid/$itmid")) {$singerrequestedlist = 'active';} 


 @endphp
 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.acoustics')!= '') {{  trans(Session::get('mer_lang_file').'.acoustics') }} @else {{ trans($MER_OUR_LANGUAGE.'.acoustics') }} @endif</span></div> <!-- breadcrumb -->
@endif


<!-- End Acoustics--> 


<!-- Start Popular Band--> 

@if(isset($popular_band_leftmenu) && $popular_band_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;

if(Request::is("popular-band-info/$idd/$hid"))                      {    $popularbandinfo = 'active';        } 
if(Request::is("popular-band-photo-video/$idd/$hid"))               {    $popularbandphotovideo = 'active';        } 
if(Request::is("popular-band-comments-review/$idd/$hid"))           {   $popularbandcommentsreview = 'active';     } 
if(Request::is("popular-band-quoted-requested-list/$idd/$hid"))     {   $popularbandquotedrequestedlist = 'active';   } 

@endphp
 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.popular_band')!= '') {{  trans(Session::get('mer_lang_file').'.popular_band') }} @else {{ trans($MER_OUR_LANGUAGE.'.popular_band') }} @endif</span></div> <!-- breadcrumb -->
@endif


<!-- End Singer--> 

 
<!-- Buffet Menu Start ajit--> 

@if(isset($buffet_leftmenu) && $buffet_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$item = request()->itemid;
$autoid = request()->autoid;
if(Request::is("buffet-info/$idd/$sid") ||  Request::is("buffet-info/$idd/$sid/$item"))            {     $buffetinfo      = 'active';  }   
if(Request::is("buffet-picture/$idd/$sid/$item"))            {     $buffetpicture         = 'active';  } 
if(Request::is("buffet-menu-list/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item/$autoid"))      {     $buffetmenlist         = 'active';  }

 
if(Request::is("buffet-orders/$idd/$sid/$item"))  {     $buffetorder         = 'active';  } 
if(Request::is("buffet-list-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item/$autoid"))            {     $buffetmenlistcontainer         = 'active';  }
 
if(Request::is("buffet-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item/$autoid"))            {     $buffetdish        = 'active';  }
if(Request::is("buffet-reviews-and-comments/$idd/$sid/$item"))            {     $buffetreviewcomment        = 'active';  }      

@endphp

 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.buffet')!= '') {{  trans(Session::get('mer_lang_file').'.buffet') }} @else {{ trans($MER_OUR_LANGUAGE.'.buffet') }} @endif</span></div> <!-- breadcrumb -->




@endif
<!-- Buffet Menu End ajit--> 




@if(isset($listmanagerblade) && $listmanagerblade==1)
 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MANAGER') }} @else {{ trans($MER_OUR_LANGUAGE.'.MANAGER') }} @endif</span></div> <!-- breadcrumb -->

@endif

	@if(isset($hotel_leftmenu) && $hotel_leftmenu==1)
	@php 
	$id = request()->id;

	$putY = Session::get('menuss');  

	if($mer_selected_lang_code !='en' && $putY==5) 
	{
	$hotalHall ='كبير'; 
	}
	elseif($mer_selected_lang_code !='ar' && $putY==5) 
	{
	$hotalHall ='Large';
	}
	elseif($mer_selected_lang_code !='en' && $putY==6) 
	{
	$hotalHall ='صغير';
	}
	elseif($mer_selected_lang_code !='ar' && $putY==6) 
	{
	$hotalHall ='Small';
	}
	else
	{
	$hotalHall ='';
	}

	@endphp

 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>{{$hotalHall}} @if (Lang::has(Session::get('mer_lang_file').'.HOTELS')!= '') {{  trans(Session::get('mer_lang_file').'.HOTELS') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOTELS') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- Buffet Branch 3 links--> 

@if(isset($buffeth_leftmenu) && $buffeth_leftmenu==1)  
@php 
$ids = request()->id; 
$sid = request()->sid; 
@endphp
 <div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.buffet')!= '') {{  trans(Session::get('mer_lang_file').'.buffet') }} @else {{ trans($MER_OUR_LANGUAGE.'.buffet') }} @endif</span></div> <!-- breadcrumb -->
@endif


<!-- Beauty Branch 3 links--> 

@if(isset($beauty_leftmenu) && $beauty_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
 
if(Request::is("beauty-shop/$id") || Request::is("beauty-add-shop/$id") || Request::is("beauty-add-shop/$id/$hid")) {$beautyshop = 'active';} 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.beauty')!= '') {{  trans(Session::get('mer_lang_file').'.beauty') }} @else {{ trans($MER_OUR_LANGUAGE.'.beauty') }} @endif</span></div> <!-- breadcrumb -->
@endif


<!-- Start For Beauty Big Menu -->

@if(isset($beautybig_leftmenu) && $beautybig_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid  = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
$workerid =  request()->workerid;
 
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.beauty')!= '') {{  trans(Session::get('mer_lang_file').'.beauty') }} @else {{ trans($MER_OUR_LANGUAGE.'.beauty') }} @endif</span></div> <!-- breadcrumb -->
@endif
<!-- End For Beauty Big Menu -->



@if(isset($oudandperfumes_leftmenu) && $oudandperfumes_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("oudandperfumes-shop/$id") || Request::is("oudandperfumes-add-shop/$id")  || Request::is("oudandperfumes-add-shop/$id/$hid")) { $oudandperfumesshop = 'active';}  
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.oudandperfumes')!= '') {{  trans(Session::get('mer_lang_file').'.oudandperfumes') }} @else {{ trans($MER_OUR_LANGUAGE.'.oudandperfumes') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- Start For oudandperfumes Big Menu  -->
@if(isset($oudandperfumesbig_leftmenu) && $oudandperfumesbig_leftmenu==1) 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.oudandperfumes')!= '') {{  trans(Session::get('mer_lang_file').'.oudandperfumes') }} @else {{ trans($MER_OUR_LANGUAGE.'.oudandperfumes') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For oudandperfumes Big Menu -->

@if(isset($goldandjewelry_leftmenu) && $goldandjewelry_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("goldandjewelry-shop/$id") || Request::is("goldandjewelry-add-shop/$id")  || Request::is("goldandjewelry-add-shop/$id/$hid")) { $goldandjewelryshop = 'active';}  
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.goldandjewelry')!= '') {{  trans(Session::get('mer_lang_file').'.goldandjewelry') }} @else {{ trans($MER_OUR_LANGUAGE.'.goldandjewelry') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- Start For goldandjewelry Big Menu  -->
@if(isset($goldandjewelrybig_leftmenu) && $goldandjewelrybig_leftmenu==1) 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
 

 @endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.goldandjewelry')!= '') {{  trans(Session::get('mer_lang_file').'.goldandjewelry') }} @else {{ trans($MER_OUR_LANGUAGE.'.goldandjewelry') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For goldandjewelry Big Menu -->

<!-- Beauty Branch 3 links--> 

@if(isset($spa_leftmenu) && $spa_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("spa-shop/$id") || Request::is("spa-add-shop/$id")  || Request::is("spa-add-shop/$id/$hid")) {$spashop = 'active';}  
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.spa')!= '') {{  trans(Session::get('mer_lang_file').'.spa') }} @else {{ trans($MER_OUR_LANGUAGE.'.spa') }} @endif</span></div> <!-- breadcrumb -->
@endif



<!-- Start For Spa Big Menu  -->

@if(isset($spabig_leftmenu) && $spabig_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.spa')!= '') {{  trans(Session::get('mer_lang_file').'.spa') }} @else {{ trans($MER_OUR_LANGUAGE.'.spa') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For spa Big Menu -->
 
<!-- Start For Makup Artist Big Menu -->

@if(isset($MakupArtistbig_leftmenu) && $MakupArtistbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid= request()->autoid;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.makeup_artist')!= '') {{  trans(Session::get('mer_lang_file').'.makeup_artist') }} @else {{ trans($MER_OUR_LANGUAGE.'.makeup_artist') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Makup Artist Big Menu -->
 
<!-- Start tailor Big Menu -->

@if(isset($Tailorbig_leftmenu) && $Tailorbig_leftmenu==1)
 
@php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
 
@endphp 
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.tailor')!= '') {{  trans(Session::get('mer_lang_file').'.tailor') }} @else {{ trans($MER_OUR_LANGUAGE.'.tailor') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For tailor Big Menu -->



<!-- Start For Photography Big Menu -->

@if(isset($photographybig_leftmenu) && $photographybig_leftmenu==1)
 
@php
$idd = request()->id;
$itemid = request()->sid;
$autoid = request()->autoid;
 

@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.photography')!= '') {{  trans(Session::get('mer_lang_file').'.photography') }} @else {{ trans($MER_OUR_LANGUAGE.'.photography') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Photography Big Menu -->

<!-- Start For abaya Big Menu -->

@if(isset($Abayabig_leftmenu) && $Abayabig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.abaya')!= '') {{  trans(Session::get('mer_lang_file').'.abaya') }} @else {{ trans($MER_OUR_LANGUAGE.'.abaya') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For abaya Big Menu -->
 
<!-- Start For dresses Big Menu -->

@if(isset($dressesbig_leftmenu) && $dressesbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
 
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.dresses')!= '') {{  trans(Session::get('mer_lang_file').'.dresses') }} @else {{ trans($MER_OUR_LANGUAGE.'.dresses') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For dresses Big Menu -->

<!-- Start For Cosha Big Menu -->

@if(isset($Coshabig_leftmenu) && $Coshabig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.cosha')!= '') {{  trans(Session::get('mer_lang_file').'.cosha') }} @else {{ trans($MER_OUR_LANGUAGE.'.cosha') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Cosha Big Menu -->

<!-- Start For Special Event Big Menu -->

@if(isset($Eventbig_leftmenu) && $Eventbig_leftmenu==1)
 
@php
$idd = request()->id;
$shopid = request()->shopid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
 

@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Special_Events')!= '') {{  trans(Session::get('mer_lang_file').'.Special_Events') }} @else {{ trans($MER_OUR_LANGUAGE.'.Special_Events') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Special Event Big Menu -->

 

<!-- Start For Car rental Menu -->

@if(isset($Carrental_leftmenu) && $Carrental_leftmenu==1)
 
@php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.car-rental')!= '') {{  trans(Session::get('mer_lang_file').'.car-rental') }} @else {{ trans($MER_OUR_LANGUAGE.'.car-rental') }} @endif</span></div> <!-- breadcrumb -->
@endif




<!-- Start For Car rental Menu -->

@if(isset($Travel_leftmenu) && $Travel_leftmenu==1)
 
@php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;

 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Travel_Agency')!= '') {{  trans(Session::get('mer_lang_file').'.Travel_Agency') }} @else {{ trans($MER_OUR_LANGUAGE.'.Travel_Agency') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Special Event Big Menu -->
 
<!-- Start For Makup Artist Big Menu -->

@if(isset($Rosestbig_leftmenu) && $Rosestbig_leftmenu==1)
 
@php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
 

@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Roses')!= '') {{  trans(Session::get('mer_lang_file').'.Roses') }} @else {{ trans($MER_OUR_LANGUAGE.'.Roses') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Makup Artist Big Menu -->




<!-- Start For Men's Barber Big Menu -->

@if(isset($mensbig_leftmenu) && $mensbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;

 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Men_Barber_Saloon')!= '') {{  trans(Session::get('mer_lang_file').'.Men_Barber_Saloon') }} @else {{ trans($MER_OUR_LANGUAGE.'.Men_Barber_Saloon') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- Start For Men's Barber Big Menu End -->





<!-- Sratr For Dessert Front Menu -->

@if(isset($dessert_leftmenu) && $dessert_leftmenu==1)
@php 

$id= request()->id;
$services_id= request()->services_id;
$shopid= request()->shopid;
 
 
if(Request::is("dessert-shop/$id") || Request::is("dessert-add-shop/$services_id"))    {     $buffetresturant = 'active'; } 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Dessert')!= '') {{  trans(Session::get('mer_lang_file').'.Dessert') }} @else {{ trans($MER_OUR_LANGUAGE.'.Dessert') }} @endif</span></div> <!-- breadcrumb -->
@endif
<!-- End For Dessert Front Menu -->

<!-- Sratr For Dessert Inner Menu -->
@if(isset($dessert_inner_leftmenu) && $dessert_inner_leftmenu==1)
 
@php
$idd = request()->id;

$services_id = request()->services_id;
$shopid = request()->shopid;
$itemid = request()->itemid;
 $autoid = request()->autoid;
$offer_id = request()->offer_id;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Dessert')!= '') {{  trans(Session::get('mer_lang_file').'.Dessert') }} @else {{ trans($MER_OUR_LANGUAGE.'.Dessert') }} @endif</span></div> <!-- breadcrumb -->
@endif
<!-- End For Dessert Inner Menu -->


<!-- Sratr For Dates Inner Menu -->
@if(isset($dates_inner_leftmenu) && $dates_inner_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$offerid  = request()->offerid;
 
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Dates')!= '') {{  trans(Session::get('mer_lang_file').'.Dates') }} @else {{ trans($MER_OUR_LANGUAGE.'.Dates') }} @endif</span></div> <!-- breadcrumb -->
@endif
<!-- End For Dates Inner Menu -->



<!-- Laser Cosmetic 3 links--> 

@if(isset($laser_cosmetic_leftmenu) && $laser_cosmetic_leftmenu==1)
@php if(Request::is('/laser-cosmetic-shop/4')) {$lasershop = 'active';} 
if(Request::is('laser-cosmetic-shop-branch/4')) {$laserbranch = 'active';} 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Cosmetic_and_Laser')!= '') {{  trans(Session::get('mer_lang_file').'.Cosmetic_and_Laser') }} @else {{ trans($MER_OUR_LANGUAGE.'.Cosmetic_and_Laser') }} @endif</span></div> <!-- breadcrumb -->
@endif



<!-- Start For Laser Cosmetic Big Menu -->

@if(isset($laserbig_leftmenu) && $laserbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$incid = request()->incid;
 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.Cosmetic_and_Laser')!= '') {{  trans(Session::get('mer_lang_file').'.Cosmetic_and_Laser') }} @else {{ trans($MER_OUR_LANGUAGE.'.Cosmetic_and_Laser') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Laser Cosmetic Big Menu -->



<!-- Skin & Teeth 3 links--> 

@if(isset($skin_teeth_leftmenu) && $skin_teeth_leftmenu==1)
@php if(Request::is('/skin-teeth-shop/4')) {$skinshop = 'active';} 
if(Request::is('skin-teeth-shop-branch/4')) {$skinbranch = 'active';} 
@endphp
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.SKIN_AND_TEETH')!= '') {{  trans(Session::get('mer_lang_file').'.SKIN_AND_TEETH') }} @else {{ trans($MER_OUR_LANGUAGE.'.SKIN_AND_TEETH') }} @endif</span></div> <!-- breadcrumb -->
@endif



<!-- Start For Skin & Teeth Big Menu -->

@if(isset($SkinTeethbig_leftmenu) && $SkinTeethbig_leftmenu==1)
   
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}">@if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span>@if (Lang::has(Session::get('mer_lang_file').'.SKIN_AND_TEETH')!= '') {{  trans(Session::get('mer_lang_file').'.SKIN_AND_TEETH') }} @else {{ trans($MER_OUR_LANGUAGE.'.SKIN_AND_TEETH') }} @endif</span></div> <!-- breadcrumb -->
@endif

<!-- End For Skin & Teeth Big Menu -->
 

<!-- Start For Makeup Menu -->

@if(isset($makeup_leftmenu) && $makeup_leftmenu==1)
 
<div class="breadcrumb"><a href="{{url('/vendor-dashboard')}}"> @if (Lang::has(Session::get('mer_lang_file').'.HOME')!= '') {{  trans(Session::get('mer_lang_file').'.HOME') }} @else {{ trans($MER_OUR_LANGUAGE.'.HOME') }} @endif</a> <span class="breadcrumb-divder">&gt;</span><span> @if (Lang::has(Session::get('mer_lang_file').'.MAKEUP')!= '') {{  trans(Session::get('mer_lang_file').'.MAKEUP') }} @else {{ trans($MER_OUR_LANGUAGE.'.MAKEUP') }} @endif</span></div> <!-- breadcrumb -->
 

 
@endif
<!-- End For Makeup Menu -->
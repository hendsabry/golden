<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<?php /*if (Session::has('merchantid'))
{
    $merchantid=Session::get('merchantid');
}*/

?>
@if (Session::has('merchantid'))
<?php   $merchantid=Session::get('merchantid'); ?>
@endif
    <meta charset="UTF-8" />
    @if($mer_selected_lang_code !='ar')

@php 
$Title = 'Dashboard';
 if(Request::is('list-hall')) {     $Title = 'Hall List';  } 
if(Request::is('sitemerchant_dashboard')) {     $Title = 'Dashboard'; } 
if(Request::is('hall-review-comments')) {     $Title = 'Review and Comments'; } 
if(Request::is('hall-order')) {     $Title = 'Hall Orders'; } 
if(Request::is('list-hall-offer')) {     $Title = 'Hall Offers'; } 
if(Request::is('list-container-package')) {     $Title = 'Hall Container'; } 
if(Request::is('list-hall-menu')) {     $Title = 'Hall menu'; } 
if(Request::is('menulist')) {     $Title = 'Hall Menu List'; } 
if(Request::is('hall-paid-services')) {     $Title = 'Hall Paid List'; } 
if(Request::is('hall-free-services')) {     $Title = 'Hall Free List'; }
if(Request::is('hall-picture')) {     $Title = 'Hall Picture'; } 
if(Request::is('hall-info')) {     $Title = 'Hall Info'; } 
if(Request::is('hotelnamelist/4')) {     $Title = 'Hotel Listing'; } 

 if(Request::is('listmanager')) {     $Title = 'List Manager'; } 
if(Request::is('list-branch-all')) {     $Title = 'List Branch Manager'; } 
if(Request::is('hotelnamelist/4')) {     $Title = 'Hotel Name List'; } 
 
@endphp
@else
@php 
$Title = 'لوحة القيادة';
 if(Request::is('list-hall')) {     $Title = 'قائمة القاعة';  } 
if(Request::is('sitemerchant_dashboard')) {     $Title = 'لوحة القيادة'; } 
if(Request::is('hall-review-comments')) {     $Title = 'مراجعة وتعليقات'; } 
if(Request::is('hall-order')) {     $Title = 'أوامر قاعة'; } 
if(Request::is('list-hall-offer')) {     $Title = 'عروض القاعة'; } 
if(Request::is('list-container-package')) {     $Title = 'حاوية القاعة'; } 
if(Request::is('list-hall-menu')) {     $Title = 'قائمة القاعة'; } 
if(Request::is('menulist')) {     $Title = 'قائمة القاعة القائمة'; } 
if(Request::is('hall-paid-services')) {     $Title = 'قائمة القاعة المدفوعة'; } 
if(Request::is('hall-free-services')) {     $Title = 'قائمة مجانية للقاعة'; }
if(Request::is('hall-picture')) {     $Title = 'صورة القاعة'; } 
if(Request::is('hall-info')) {     $Title = 'معلومات القاعة'; } 
if(Request::is('hotelnamelist/4')) {     $Title = 'Hotel Listing'; } 

 if(Request::is('listmanager')) {     $Title = 'مدير قائمة'; } 
if(Request::is('list-branch-all')) {     $Title = 'قائمة مدير الفرع'; } 
if(Request::is('hotelnamelist/4')) {     $Title = 'قائمة اسم الفندق'; } 
 
@endphp

@endif

    <title>{{ $SITENAME }} |  {{$Title }}</title>
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/bootstrap/css/bootstrap_merchant.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/MoneAdmin_merchant.css" />
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/img/favicon.ico">
 @endif
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />

    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('')}}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('')}}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('')}}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
    <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/jquery.form-validator.js"></script>
	<script src="{{ url('')}}/public/assets/plugins/bootstrap/js/jquery.min.js"></script>
   <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <!-- <script src="http://192.168.2.50/nexemerchant/public/assets/plugins/jquery-2.0.3.min.js"></script>-->
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/height.js"></script>	
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/interface-harry.css" />		
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/interface-nidhi.css" />		
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/interface-prabhakar.css" />	
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/interface-media.css" />		
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
@if($mer_selected_lang_code =='ar')
<body class="padTop53 rtl mainrtl">
  @else
<body class="padTop53">
  @endif

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->

   {!!$merchantheader!!}
<!-- HALL SECTION LEFT MENU START-->
@php
 $checkVal= 0; 
@endphp

@if(isset($ship_leftmenu) && $ship_leftmenu==1 )


<div class="hall-menu-outer "><div class="hall_menu"></div></div>
<div  class="left_panel">

<div class="sidenav"> 
  <a class="active"  href="#"> @if (Lang::has(Session::get('mer_lang_file').'.MANAGE_SHIPPING')!= '') {{  trans(Session::get('mer_lang_file').'.MANAGE_SHIPPING') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MANAGE_SHIPPING') }} @endif </a>

</div>
</div>


@endif


@if(isset($hall_leftmenu) && $hall_leftmenu==1)
@php
$hallmenu = '';
$ids = request()->id;
$showMenus = 0; 
 $showMenu = 0;
if(isset($_REQUEST['hid']) && $_REQUEST['hid'] !='')
{ $hid = $_REQUEST['hid'];
  $showMenu = Helper::checkinnerfood($hid); 
}


if(Request::is('list-hall')) {     $listhall = 'active';  } 
if(Request::is('sitemerchant_dashboard')) {     $sitemerchantdashboard = 'active'; } 
if(Request::is('hall-review-comments')) {     $hallreviewcomments = 'active'; $hallmenu = 'hall_riview'; $showMenus =$showMenu;} 
if(Request::is('hall-order')) {     $hallorder = 'active'; $showMenus =$showMenu;} 
if(Request::is('list-hall-offer') || Request::is('add-hall-offer') || Request::is("edit-hall-offer/$ids")) {     $listhalloffer = 'active'; $showMenus =$showMenu;} 
if(Request::is('list-container-package')) {     $listcontainerpackage = 'active'; $showMenus =$showMenu; } 
if(Request::is('list-hall-menu') || Request::is('add-hall-menu') || Request::is("edit-hall-menu/$ids")) {     $listhallmenu = 'active'; $showMenus =$showMenu;}
if(Request::is('editmenucategroy')) {     $menulist = 'active'; $showMenus =$showMenu; } 
if(Request::is('categorymenu')) {     $menulist = 'active'; $showMenus =$showMenu; } 
if(Request::is('menulist')) {     $menulist = 'active'; $showMenus =$showMenu;} 
if(Request::is("edit-container-package/$ids")) {     $listcontainerpackage = 'active'; $showMenus =$showMenu; }
if(Request::is('add-container-package')) {     $listcontainerpackage = 'active'; $showMenus =$showMenu;} 
if(Request::is('hall-paid-services')) {     $hallpaidservices = 'active'; $showMenus =$showMenu; } 
if(Request::is('hall-free-services')) {     $hallfreeservices = 'active'; $showMenus =$showMenu; }
if(Request::is('hall-picture')) {     $hallpicture = 'active'; $hallmenu = 'menuright'; $checkVal=1; $showMenus =$showMenu; } 
if(Request::is('hall-info')) {     $hallinfo = 'active';  $showMenus =$showMenu; } 
@endphp

<div class="hall-menu-outer "><div class="hall_menu {{$hallmenu}}"></div></div>
<div  class="left_panel">

<div class="sidenav"> 
  <a class="{{$listhall or ''}}"  href="{{ url('/list-hall') }}@if(isset($_REQUEST['bid']) && $_REQUEST['bid']!='')/{{$_REQUEST['bid'] or ''}} @endif"> @if (Lang::has(Session::get('mer_lang_file').'.MER_HallLIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HallLIST') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HallLIST') }} @endif </a>

  <a class="{{$hallinfo or ''}}" href="{{ url('/hall-info') }}@if(isset($_REQUEST['bid']) && $_REQUEST['bid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif">  @if (Lang::has(Session::get('mer_lang_file').'.MER_HALL_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALL_INFO') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALL_INFO') }} @endif </a>

@php
if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')
{
@endphp

<a  class="{{$hallpicture or ''}}" href="{{ url('/hall-picture') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif">  @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALLPICTUREANDVIDEO') }} @endif </a>
   
  <div class="dropdown-btn"> @if (Lang::has(Session::get('mer_lang_file').'.MER_HALLSERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALLSERVICES') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALLSERVICES') }} @endif  
    
  </div>
  <div class="dropdown-container">
    <a  class="{{$hallfreeservices or ''}}" href="{{ url('/hall-free-services') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif"> @if (Lang::has(Session::get('mer_lang_file').'.MER_FREESERVICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FREESERVICE') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_FREESERVICE') }} @endif </a>
  <a class="{{$hallpaidservices or ''}}"  href="{{ url('/hall-paid-services') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_HALLPAIDSERVICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_HALLPAIDSERVICE') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_HALLPAIDSERVICE') }} @endif </a>
    
  </div>
@if($showMenus ==1)
  <a class="{{$menulist or ''}}" href="{{ url('/menulist') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif">  @if (Lang::has(Session::get('mer_lang_file').'.MER_HALLMENUCATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALLMENUCATEGORY') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALLMENUCATEGORY') }} @endif </a>
 
 <a class="{{$listcontainerpackage or ''}}" href="{{ url('/list-container-package') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Container')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container') }} @endif </a>

<a class="{{$listhallmenu or ''}}" href="{{ url('/list-hall-menu') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif">  @if (Lang::has(Session::get('mer_lang_file').'.MER_DISH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISH') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISH') }} @endif </a>
 
@endif


 
<a class="{{$listhalloffer or ''}}" href="{{ url('/list-hall-offer') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif">  @if (Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Offer') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_Offer') }} @endif </a>
 
<a class="{{$hallorder or ''}}"  href="{{ url('/hall-order') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif">  @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
 <a class="{{$hallreviewcomments or ''}}"  href="{{ url('/hall-review-comments') }}@if(isset($_REQUEST['hid']) && $_REQUEST['hid']!='')?hid={{$_REQUEST['hid'] or ''}}&bid={{$_REQUEST['bid']}} @endif">  @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
 
 @php
}

 @endphp

</div>
 
</div>
@endif
<!-- HALL SECTION LEFT MENU END-->


@if(isset($serviceleftmenu) && $serviceleftmenu==1)
 <div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
  <a class="{{$sitemerchantdashboard or ''}}" href="{{ url('/sitemerchant_dashboard') }}"> @if (Lang::has(Session::get('mer_lang_file').'.Services')!= '') {{  trans(Session::get('mer_lang_file').'.Services') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.Services') }} @endif </a>

</div>
</div>
@endif


@if(isset($wallet_inner_leftmenu) && $wallet_inner_leftmenu==1)
@php
if(Request::is("my-wallet") || Request::is("wallet-addamount")) { $wlt = 'active'; } 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
  <a class="{{$wlt or ''}}" href="{{ route('my-wallet') }}"> @if (Lang::has(Session::get('mer_lang_file').'.MyWallet')!= '') {{  trans(Session::get('mer_lang_file').'.MyWallet') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MyWallet') }} @endif </a>
</div>
</div>
@endif
 

<!-- RECEPTION AND HOSPITALITY--> 

@if(isset($reception_hospitality_leftmenu) && $reception_hospitality_leftmenu==1)
 
@php
$idd = request()->id;
$ShopId = request()->shopid;
$itemid =  request()->itemid;
$cid = request()->cid;
if(Request::is("reception-hospitality/$idd/$ShopId"))            {     $receptionhospitality = 'active';        } 
if(Request::is("reception-shop-picture/$idd/$ShopId"))           {   $checkVal=1;  $receptionshoppicture = 'active';        } 
if(Request::is("reception-list-attributes/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId") || Request::is("reception-addcategory/$idd/$ShopId/$itemid"))        {     $receptionlistattributes = 'active';     } 
if(Request::is("reception-list-informations/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId") || Request::is("reception-additems/$idd/$ShopId/$cid"))      {     $receptionlistinformations = 'active';   } 
if(Request::is("reception-list-workers/$idd/$ShopId"))           {     $receptionlistworkers = 'active';        } 
if(Request::is("reception-list-packages/$idd/$ShopId") || Request::is("reception-add-packege/$idd/$ShopId")  || Request::is("reception-add-packege/$idd/$ShopId/$cid"))          {     $receptionlistpackages = 'active';       } 
if(Request::is("reception-list-orders/$idd/$ShopId"))            {     $receptionlistorders = 'active';         } 
if(Request::is("reception-list-reviewcomments/$idd/$ShopId"))    {     $receptionlistreviewcomments = 'active'; }  
if(Request::is("reception-shop-list/$idd/$ShopId"))          {     $receptionshoplist = 'active'; }   
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
@if($ShopId =='')
   <a class="{{$receptionhospitality or ''}}" href="{{ route('reception-add-shopinfo',['id' => request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.SHOPINFO')!= '') {{  trans(Session::get('mer_lang_file').'.SHOPINFO') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.SHOPINFO') }} @endif </a>
@endif
@if($ShopId !='')

  <a class="{{$receptionhospitality or ''}}" href="{{ route('reception-hospitality',['id' => request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.SHOPINFO')!= '') {{  trans(Session::get('mer_lang_file').'.SHOPINFO') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.SHOPINFO') }} @endif </a>





  <a class="{{$receptionshoppicture or ''}}" href="{{ route('reception-shop-picture',['id' => request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>



  <a class="{{$receptionlistattributes or ''}}" href="{{ route('reception-list-attributes',['id' =>request()->id ,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.CATEGORY') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.CATEGORY') }} @endif </a>



  <a class="{{$receptionlistinformations or ''}}" href="{{ route('reception-list-informations',['id' =>request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.ITEAMINFORMATION')!= '') {{  trans(Session::get('mer_lang_file').'.ITEAMINFORMATION') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.ITEAMINFORMATION') }} @endif </a>



  <a class="{{$receptionlistworkers or ''}}" href="{{ route('reception-list-workers',['id' =>request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.WORKERSNATIONLITY')!= '') {{  trans(Session::get('mer_lang_file').'.WORKERSNATIONLITY') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.WORKERSNATIONLITY') }} @endif </a>



  <a class="{{$receptionlistpackages or ''}}" href="{{ route('reception-list-packages',['id' =>request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGES') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.PACKAGES') }} @endif </a>

  <a class="{{$receptionlistorders or ''}}" href="{{ route('reception-list-orders',['id' =>request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.ORDERS')!= '') {{  trans(Session::get('mer_lang_file').'.ORDERS') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.ORDERS') }} @endif </a>


 <a class="{{$receptionlistreviewcomments or ''}}" href="{{ route('reception-list-reviewcomments',['id' =>request()->id,'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- Start Singer--> 

@if(isset($singer_leftmenu) && $singer_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;
if(Request::is("manage-singer/$idd/$hid"))            {     $managesinger = 'active';        } 
if(Request::is("singer-photo-video/$idd/$hid"))           {   $checkVal=1;  $singerphotovideo = 'active';        } 
if(Request::is("singer-comments-review/$idd/$hid"))        {     $singercommentsreview = 'active';     } 
if(Request::is("singer-quoted-requested-list/$idd/$hid"))      {     $singerrequestedlist = 'active';   } 
if(Request::is("singer-order-list/$idd/$hid"))      {     $singerorderlist = 'active';   } 
 @endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="{{$managesinger or ''}}" href="{{ route('manage-singer',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Singer_Info_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Singer_Info_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Singer_Info_Menu') }} @endif</a>

 @if($hid !='')
<a class="{{$singerphotovideo or ''}}" href="{{ route('singer-photo-video',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Singer_Photo_Video_Menu') }} @endif</a>
<a class="{{$singercommentsreview or ''}}" href="{{ route('singer-comments-review',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Review_Comments_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Review_Comments_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Review_Comments_Menu') }} @endif</a>
<a class="{{$singerrequestedlist or ''}}" href="{{ route('singer-quoted-requested-list',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu') }} @endif</a>
<a class="{{$singerorderlist or ''}}" href="{{ route('singer-order-list',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.ORDERS')!= '') {{  trans(Session::get('mer_lang_file').'.ORDERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDERS') }} @endif</a>


@endif

</div>
</div>
@endif


<!-- End Singer--> 




<!-- Start Acoustics--> 

@if(isset($acoustics_leftmenu) && $acoustics_leftmenu==1)
 
@php
$idd = request()->id;
$Cid= request()->Cid;
$hid = request()->hid;
$itmid = request()->autoid;
$itmids = request()->itmid;
if(Request::is("acoustics/$idd/$hid") || Request::is("acoustics/$idd/$hid/$Cid")) {$managesinger = 'active';} 
if(Request::is("acoustics-photo-video/$idd/$hid") || Request::is("acoustics-photo-video/$idd/$hid/$Cid")) {$checkVal=1; $singerphotovideo = 'active';} 
if(Request::is("acoustics-comments-review/$idd/$hid") || Request::is("acoustics-comments-review/$idd/$hid/$Cid")) {$singercommentsreview = 'active';} 
if(Request::is("acoustics-quoted-requested-list/$idd/$hid") || Request::is("acoustics-quoted-requested-list/$idd/$hid/$Cid") || Request::is("acoustics-quoted-requested-view/$idd/$hid/$itmids")) {$singerrequestedlist = 'active';}

if(Request::is("acoustic-offer/$idd/$hid") || Request::is("acoustic-offer/$idd/$hid/$Cid") || Request::is("acoustic-addoffer/$idd/$hid") || Request::is("acoustic-addoffer/$idd/$hid/$itmid")) {$acousticpffer = 'active';}  

if(Request::is("product-item/$idd/$hid")|| Request::is("product-item/$idd/$hid/$Cid")|| Request::is("product-additem/$idd/$hid") || Request::is("product-additem/$idd/$hid/$itmid")) {$products = 'active';}

if(Request::is("acoustic-order/$idd/$hid") || Request::is("acoustic-order/$idd/$hid/$Cid")) {$acousticorder = 'active';} 
 

 @endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="{{$managesinger or ''}}" href="{{ route('acoustics',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Acousticsinfo')!= '') {{  trans(Session::get('mer_lang_file').'.Acousticsinfo') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Acousticsinfo') }} @endif</a>

 @if($hid!='')
<a class="{{$singerphotovideo or ''}}" href="{{ route('acoustics-photo-video',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Singer_Photo_Video_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Singer_Photo_Video_Menu') }} @endif</a>


<a class="{{$products or ''}}" href="{{ route('product-item',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRODUCTS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS') }} @endif</a>

 <a class="{{$acousticpffer or ''}}" href="{{ route('acoustic-offer',['id' => request()->id,'hid'=>request()->hid,'Cid' => request()->Cid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
 
<a class="{{$acousticorder or ''}}" href="{{ route('acoustic-order',['id' => request()->id,'hid'=>request()->hid,'Cid' => request()->Cid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>

<a class="{{$singercommentsreview or ''}}" href="{{ route('acoustics-comments-review',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Review_Comments_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Review_Comments_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Review_Comments_Menu') }} @endif</a>

<a class="{{$singerrequestedlist or ''}}" href="{{ route('acoustics-quoted-requested-list',['id' => request()->id,'hid' => request()->hid,'Cid' => request()->Cid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu') }} @endif</a>
 @endif

</div>
</div>
@endif


<!-- End Acoustics--> 


<!-- Start Popular Band--> 

@if(isset($popular_band_leftmenu) && $popular_band_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;

if(Request::is("popular-band-info/$idd/$hid"))                      {    $popularbandinfo = 'active';        } 
if(Request::is("popular-band-photo-video/$idd/$hid"))               {  $checkVal=1;  $popularbandphotovideo = 'active';        } 
if(Request::is("popular-band-comments-review/$idd/$hid"))           {   $popularbandcommentsreview = 'active';     } 
if(Request::is("popular-band-quoted-requested-list/$idd/$hid"))     {   $popularbandquotedrequestedlist = 'active';   } 
if(Request::is("popular-band-order-list/$idd/$hid"))      {     $popularbandorderlist = 'active';   } 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$popularbandinfo or ''}}" href="{{ route('popular-band-info',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_POPULAR_BAND_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_POPULAR_BAND_INFO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_POPULAR_BAND_INFO') }} @endif</a>
@if($hid!='')
<a class="{{$popularbandphotovideo or ''}}" href="{{ route('popular-band-photo-video',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Popupr_Band_Photo_video')!= '') {{  trans(Session::get('mer_lang_file').'.Popupr_Band_Photo_video') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Popupr_Band_Photo_video') }} @endif</a>

<a class="{{$popularbandcommentsreview or ''}}" href="{{ route('popular-band-comments-review',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.popular_band_comments_review')!= '') {{  trans(Session::get('mer_lang_file').'.popular_band_comments_review') }} @else  {{ trans($MER_OUR_LANGUAGE.'.popular_band_comments_review') }} @endif</a>

<a class="{{$popularbandquotedrequestedlist or ''}}" href="{{ route('popular-band-quoted-requested-list',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu')!= '') {{  trans(Session::get('mer_lang_file').'.Quoted_Requested_List_Menu') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Quoted_Requested_List_Menu') }} @endif</a>

<a class="{{$popularbandorderlist or ''}}" href="{{ route('popular-band-order-list',['id' => request()->id,'hid' => request()->hid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.ORDERS')!= '') {{  trans(Session::get('mer_lang_file').'.ORDERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDERS') }} @endif</a>
 @endif


 

</div>
</div>
@endif


<!-- End Singer--> 





 

<!-- Buffet Menu Start ajit--> 

@if(isset($buffet_leftmenu) && $buffet_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$item = request()->itemid;
$autoid = request()->autoid;
if(Request::is("buffet-info/$idd/$sid") ||  Request::is("buffet-info/$idd/$sid/$item"))            {     $buffetinfo      = 'active';  }   
if(Request::is("buffet-picture/$idd/$sid/$item"))            {  $checkVal=1;   $buffetpicture         = 'active';  } 
if(Request::is("buffet-menu-list/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item") || Request::is("add-buffet-menu-category/$idd/$sid/$item/$autoid"))      {     $buffetmenlist         = 'active';  }

 
if(Request::is("buffet-orders/$idd/$sid/$item"))  {     $buffetorder         = 'active';  } 
if(Request::is("buffet-list-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item") || Request::is("buffet-add-container/$idd/$sid/$item/$autoid"))            {     $buffetmenlistcontainer         = 'active';  }
 
if(Request::is("buffet-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item") || Request::is("buffet-add-dish/$idd/$sid/$item/$autoid"))            {     $buffetdish        = 'active';  }
if(Request::is("buffet-reviews-and-comments/$idd/$sid/$item"))            {     $buffetreviewcomment        = 'active';  }      

@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$buffetlistbranch or ''}}" href="{{ route('buffet-list-branch',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST') }} @endif </a>


 <a class="{{ $buffetinfo or '' }}" href="{{ route('buffet-info',['id' => request()->id,'sid' =>request()->sid,'itemid' =>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>


@if($item!='')
  <a class="{{$buffetpicture or ''}}" href="{{ route('buffet-picture',['id' => request()->id,'sid' => request()->sid,'itemid' =>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
  <a class="{{$buffetmenlist or ''}}" href="{{ route('buffet-menu-list',['id' => request()->id,'sid' => request()->sid,'itemid' =>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_HALLMENU')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALLMENU') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_HALLMENU') }} @endif </a>
  
    <a class="{{$buffetmenlistcontainer or ''}}" href="{{ route('buffet-list-container',['id' => request()->id,'sid'=>request()->sid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Container')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Container') }} @endif </a>

   <a class="{{$buffetdish or ''}}" href="{{ route('buffet-dish',['id' => request()->id,'sid' =>request()->sid,'itemid'=>request('itemid')]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_DISH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISH') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_DISH') }} @endif </a>
<a class="{{$buffetorder or ''}}" href="{{ route('buffet-orders',['id' => request()->id,'sid'=>request()->sid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>

  <a class="{{$buffetreviewcomment or ''}}" href="{{ route('buffet-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
  @endif
</div>
</div>
@endif
<!-- Buffet Menu End ajit--> 




@if(isset($listmanagerblade) && $listmanagerblade==1)
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">

 <a class="{{$listmanager or ''}}" href="{{ url('/listmanager') }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MANAGER') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_MANAGER') }} @endif </a>
</div>
</div>


@endif

@if(isset($hotel_leftmenu) && $hotel_leftmenu==1)


@php 
$id = request()->id;
 if(Request::is("edithotelname/$id")) {     $hotelnamelist = 'active';  } 
 if(Request::is("service/$id")) {     $hotelnamelist = 'active';  } 

if(Request::is('listmanager')) {     $listmanager = 'active'; } 
if(Request::is('list-branch-all')) {     $listbranchall = 'active'; } 
   $putsY = session()->get('menuss');   
if(Request::is('hotelnamelist/4')) {     $hotelnamelist = 'active'; } 

@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">
@if($id==4 || $putsY==4)

@php if($id==4){ $putY = Session::put('menuss',4); } @endphp
  <a class="{{$hotelnamelist or ''}}" href="{{ url('/hotelnamelist/4') }}">  @if (Lang::has(Session::get('mer_lang_file').'.MER_HOTELS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HOTELS') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HOTELS') }} @endif </a>


 
 @elseif($id==5 || $putsY==5)
@php if($id==5){ $putY = Session::put('menuss',5); }

 if($mer_selected_lang_code !='en') 
 {
$hotalHall ='كبير'; 
 }
 else
 {
   $hotalHall ='Large';
 }

   

 @endphp
  <a class="{{$hotelnamelist or ''}}" href="{{ url('/hotelnamelist/5') }}">{{$hotalHall or ''}} @if (Lang::has(Session::get('mer_lang_file').'.MER_HOTELS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HOTELS') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HOTELS') }} @endif </a>
 @elseif($id==6 ||  $putsY==6)
@php if($id==6){ $putY = Session::put('menuss',6); }

 if($mer_selected_lang_code !='en') 
 {
$hotalHall ="صالات الأفراح";
 }
 else
 {
  $hotalHall ='Small';
 }

 @endphp
  <a class="{{$hotelnamelist or ''}}" href="{{ url('/hotelnamelist/6') }}">{{$hotalHall or ''}} @if (Lang::has(Session::get('mer_lang_file').'.MER_HOTELS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HOTELS') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HOTELS') }} @endif </a>

@endif
   </div>
   </div>
@endif

<!-- Buffet Branch 3 links--> 

@if(isset($buffeth_leftmenu) && $buffeth_leftmenu==1)  
@php 
$ids = request()->id; 
$sid = request()->sid;
if(Request::is("buffet-resturant/$ids") || Request::is("buffet-add-resturant/$ids") || Request::is("buffet-add-resturant/$ids/$sid")){   $buffetresturant = 'active'; } 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4"> 

  <a class="{{$buffetresturant or ''}}" href="{{route('buffet-resturant',['id' =>request()->id])}}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_RESTURANT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_RESTURANT') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_RESTURANT') }} @endif </a>
 

   </div>
   </div>
@endif


<!-- Beauty Branch 3 links--> 

@if(isset($beauty_leftmenu) && $beauty_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
 
if(Request::is("beauty-shop/$id") || Request::is("beauty-add-shop/$id") || Request::is("beauty-add-shop/$id/$hid")) {$beautyshop = 'active';} 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">

  <a class="{{$beautyshop or ''}}" href="{{route('beauty-shop',['id' =>request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP') }} @endif </a>
 
 

   </div>
   </div>
@endif


<!-- Start For Beauty Big Menu -->

@if(isset($beautybig_leftmenu) && $beautybig_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid  = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
$workerid =  request()->workerid;
if(Request::is("beauty-shop-branchh/$idd"))            {     $beautyshopbranch      = 'active';  }   
if(Request::is("beauty-picture/$idd/$hid/$itemid"))            {   $checkVal=1;  $beautytpicture         = 'active';  } 
if(Request::is("beauty-shop-info/$idd/$hid") || Request::is("beauty-shop-info/$idd/$hid/$itemid"))           {     $beautyshopinfo          = 'active';  }



if(Request::is("beauty-service-category/$idd/$hid/$itemid") || Request::is("beauty-add-service-catogory/$idd/$hid/$itemid") || Request::is("beauty-add-service-catogory/$idd/$hid/$itemid/$catitemid"))           {     $beautyservicecat          = 'active';  }
 if(Request::is("beauty-service/$idd/$hid/$itemid") || Request::is("beauty-add-service/$idd/$hid/$itemid") || Request::is("beauty-add-service/$idd/$hid/$itemid/$serviceid"))                {     $beautyservice              = 'active';  }
 
if(Request::is("beauty-package/$idd/$hid/$itemid") || Request::is("beauty-add-package/$idd/$hid/$itemid") || Request::is("beauty-add-package/$idd/$hid/$itemid/$serviceid"))  {     $beautypackage   = 'active';  }
if(Request::is("beauty-worker/$idd/$hid/$itemid") || Request::is("beauty-add-worker/$idd/$hid/$itemid") || Request::is("beauty-add-worker/$idd/$hid/$itemid/$workerid"))  {     $beautyworker   = 'active';  }
if(Request::is("buffet-add-branch/$idd"))  {     $buffetlistbranch   = 'active';  }
if(Request::is("beauty-order/$idd/$hid/$itemid"))  {     $beautyorders   = 'active';  }
if(Request::is("beauty-add-worker/$idd"))  {     $beautyworker   = 'active';  }
if(Request::is("beauty-reviews-and-comments/$idd/$hid/$itemid"))  {     $beautyreviewsandcomments   = 'active';  }
if(Request::is("beauty-offer/$idd/$hid/$itemid") || Request::is("beauty-add-offer/$idd/$hid/$itemid")  || Request::is("beauty-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $beautyoffer   = 'active';  }




@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$beautyshopbranch or ''}}" href="{{ route('beauty-shop-branch',['id' => request()->id,'hid' => $hid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST') }} @endif </a>


 <a class="{{$beautyshopinfo or ''}}" href="{{ route('beauty-shop-info',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>
  


@if(request()->itemid!='')

   <a class="{{$beautytpicture or ''}}" href="{{ route('beauty-picture',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

  <a class="{{$beautyservicecat or ''}}" href="{{ route('beauty-service-category',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY') }} @endif </a>
  
  <a class="{{$beautyservice or ''}}" href="{{ route('beauty-service',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICES') }} @endif </a>
  
  <a class="{{$beautypackage or ''}}" href="{{ route('beauty-package',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.PACKAGES') }} @endif </a>
  
   <a class="{{$beautyworker or ''}}" href="{{ route('beauty-worker',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.WORKERS')!= '') {{  trans(Session::get('mer_lang_file').'.WORKERS') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.WORKERS') }} @endif </a>


<a class="{{$beautyorders or ''}}" href="{{ route('beauty-order',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$beautyoffer or ''}}" href="{{ route('beauty-offer',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$beautyreviewsandcomments or ''}}" href="{{ route('beauty-reviews-and-comments',['id' => request()->id,'hid' => $hid,'itemid' => $itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

@endif
</div>
</div>
@endif
<!-- End For Beauty Big Menu -->



@if(isset($oudandperfumes_leftmenu) && $oudandperfumes_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("oudandperfumes-shop/$id") || Request::is("oudandperfumes-add-shop/$id")  || Request::is("oudandperfumes-add-shop/$id/$hid")) { $oudandperfumesshop = 'active';}  
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav 4">
<a class="{{$oudandperfumesshop or ''}}" href="{{route('oudandperfumes-shop',['id' =>request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP') }} @endif </a>

</div>
</div>
@endif
<!-- Start For oudandperfumes Big Menu  -->
@if(isset($oudandperfumesbig_leftmenu) && $oudandperfumesbig_leftmenu==1) 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
if(Request::is("oudandperfumes-shop-branch/$idd"))            {     $oudandperfumesshopbranch      = 'active';  }   
if(Request::is("oudandperfumes-picture/$idd/$hid/$itemid"))            {  $checkVal=1;   $oudandperfumespicture         = 'active';  } 
if(Request::is("oudandperfumes-shop-info/$idd/$hid") || Request::is("oudandperfumes-shop-info/$idd/$hid/$itemid"))           {     $oudandperfumesshopinfo          = 'active';  }
if(Request::is("oudandperfumes-service-category/$idd/$hid/$itemid"))           {     $oudandperfumesservicecat          = 'active';  }
if(Request::is("oudandperfumes-add-service-catogory/$idd/$hid/$itemid") || Request::is("oudandperfumes-add-service-catogory/$idd/$hid/$itemid/$catitemid"))  {$oudandperfumesservicecat  = 'active';  }
if(Request::is("oudandperfumes-service/$idd/$hid/$itemid"))                {     $oudandperfumesservice              = 'active';  }
if(Request::is("oudandperfumes-add-service/$idd/$hid/$itemid") || Request::is("oudandperfumes-add-service/$idd/$hid/$itemid/$serviceid"))  {     $oudandperfumesservice   = 'active';  }
if(Request::is("oudandperfumes-package/$idd/$hid/$itemid"))  {     $oudandperfumespackage   = 'active';  }
if(Request::is("oudandperfumes-add-package/$idd/$hid/$itemid") || Request::is("oudandperfumes-add-package/$idd/$hid/$itemid/$serviceid"))  {     $oudandperfumespackage   = 'active';  }
if(Request::is("oudandperfumes-worker/$idd/$hid/$itemid"))  {     $oudandperfumesworker   = 'active';  }
if(Request::is("oudandperfumes-add-branch/$idd/$hid/$itemid"))  {     $oudandperfumeslistbranch   = 'active';  }
if(Request::is("oudandperfumes-order/$idd/$hid/$itemid"))  {     $oudandperfumesorders   = 'active';  }
if(Request::is("oudandperfumes-add-worker/$idd/$hid/$itemid"))  {     $oudandperfumesworker   = 'active';  }
if(Request::is("oudandperfumes-reviews-and-comments/$idd/$hid/$itemid"))  {     $oudandperfumesreviewsandcomments   = 'active';  }
if(Request::is("oudandperfumes-offer/$idd/$hid/$itemid"))  {     $oudandperfumesoffer   = 'active';  }
if(Request::is("oudandperfumes-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $oudandperfumesoffer   = 'active';  }
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="{{$oudandperfumesshopbranch or ''}}" href="{{ route('oudandperfumes-shop-branch',['id' => request()->id,'hid'=>request()->hid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST') }} @endif </a>


 <a class="{{$oudandperfumesshopinfo or ''}}" href="{{ route('oudandperfumes-shop-info',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>
  
  @if($itemid !='')
   <a class="{{$oudandperfumespicture or ''}}" href="{{ route('oudandperfumes-picture',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

  <a class="{{$oudandperfumesservicecat or ''}}" href="{{ route('oudandperfumes-service-category',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_CATEGORY') }} @endif </a>
  
  <a class="{{$oudandperfumesservice or ''}}" href="{{ route('oudandperfumes-service',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRODUCTS') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS') }} @endif </a>
  
 


<a class="{{$oudandperfumesorders or ''}}" href="{{ route('oudandperfumes-order',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$oudandperfumesoffer or ''}}" href="{{ route('oudandperfumes-offer',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$oudandperfumesreviewsandcomments or ''}}" href="{{ route('oudandperfumes-reviews-and-comments',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

  @endif

</div>
</div>
@endif

<!-- End For oudandperfumes Big Menu -->

@if(isset($goldandjewelry_leftmenu) && $goldandjewelry_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("goldandjewelry-shop/$id") || Request::is("goldandjewelry-add-shop/$id")  || Request::is("goldandjewelry-add-shop/$id/$hid")) { $goldandjewelryshop = 'active';}  
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav 4">
<a class="{{$goldandjewelryshop or ''}}" href="{{route('goldandjewelry-shop',['id' =>request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP') }} @endif </a>

</div>
</div>
@endif
<!-- Start For goldandjewelry Big Menu  -->
@if(isset($goldandjewelrybig_leftmenu) && $goldandjewelrybig_leftmenu==1) 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
if(Request::is("goldandjewelry-shop-branch/$idd"))            {     $goldandjewelryshopbranch      = 'active';  }   
if(Request::is("goldandjewelry-picture/$idd/$hid/$itemid"))            {   $checkVal=1;  $goldandjewelrypicture         = 'active';  } 
if(Request::is("goldandjewelry-shop-info/$idd/$hid") || Request::is("goldandjewelry-shop-info/$idd/$hid/$itemid"))           {     $goldandjewelryshopinfo          = 'active';  }
if(Request::is("goldandjewelry-service-category/$idd/$hid/$itemid"))           {     $goldandjewelryservicecat          = 'active';  }
if(Request::is("goldandjewelry-add-service-catogory/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-service-catogory/$idd/$hid/$itemid/$catitemid"))  {$goldandjewelryservicecat  = 'active';  }
if(Request::is("goldandjewelry-service/$idd/$hid/$itemid"))                {     $goldandjewelryservice              = 'active';  }
if(Request::is("goldandjewelry-add-service/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-service/$idd/$hid/$itemid/$serviceid"))  {     $goldandjewelryservice   = 'active';  }
if(Request::is("goldandjewelry-package/$idd/$hid/$itemid"))  {     $goldandjewelrypackage   = 'active';  }
if(Request::is("goldandjewelry-add-package/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-package/$idd/$hid/$itemid/$serviceid"))  {     $goldandjewelrypackage   = 'active';  }
if(Request::is("goldandjewelry-worker/$idd/$hid/$itemid"))  {     $goldandjewelryworker   = 'active';  }
if(Request::is("goldandjewelry-add-branch/$idd/$hid/$itemid"))  {     $goldandjewelrylistbranch   = 'active';  }
if(Request::is("goldandjewelry-order/$idd/$hid/$itemid"))  {     $goldandjewelryorders   = 'active';  }
if(Request::is("goldandjewelry-add-worker/$idd/$hid/$itemid"))  {     $goldandjewelryworker   = 'active';  }
if(Request::is("goldandjewelry-reviews-and-comments/$idd/$hid/$itemid"))  {     $goldandjewelryreviewsandcomments   = 'active';  }
 
if(Request::is("goldandjewelry-offer/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-offer/$idd/$hid/$itemid") || Request::is("goldandjewelry-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $goldandjewelryoffer   = 'active';  }

 @endphp


<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
 <a class="{{$goldandjewelryshopbranch or ''}}" href="{{ route('goldandjewelry-shop-branch',['id' => request()->id,'hid'=>request()->hid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST') }} @endif </a>


 <a class="{{$goldandjewelryshopinfo or ''}}" href="{{ route('goldandjewelry-shop-info',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>
  
  @if($itemid !='')
   <a class="{{$goldandjewelrypicture or ''}}" href="{{ route('goldandjewelry-picture',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

  <a class="{{$goldandjewelrypicture or ''}}" href="{{ route('goldandjewelry-manageprice',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_GOLD_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_GOLD_PRICE') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_GOLD_PRICE') }} @endif </a>

  <a class="{{$goldandjewelryservicecat or ''}}" href="{{ route('goldandjewelry-service-category',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRODUCT_CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_CATEGORY') }} @endif </a>
  
  <a class="{{$goldandjewelryservice or ''}}" href="{{ route('goldandjewelry-service',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCTS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRODUCTS') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_PRODUCTS') }} @endif </a>
  
 


<a class="{{$goldandjewelryorders or ''}}" href="{{ route('goldandjewelry-order',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$goldandjewelryoffer or ''}}" href="{{ route('goldandjewelry-offer',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$goldandjewelryreviewsandcomments or ''}}" href="{{ route('goldandjewelry-reviews-and-comments',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

  @endif

</div>
</div>
@endif

<!-- End For goldandjewelry Big Menu -->

<!-- Beauty Branch 3 links--> 

@if(isset($spa_leftmenu) && $spa_leftmenu==1)
@php 
$id = request()->id;
$hid = request()->hid;
if(Request::is("spa-shop/$id") || Request::is("spa-add-shop/$id")  || Request::is("spa-add-shop/$id/$hid")) {$spashop = 'active';}  
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav 4">

  <a class="{{$spashop or ''}}" href="{{route('spa-shop',['id' =>request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP') }} @endif </a>
  
   </div>
   </div>
@endif



<!-- Start For Spa Big Menu  -->

@if(isset($spabig_leftmenu) && $spabig_leftmenu==1)
 
@php
$idd = request()->id;
$hid = request()->hid;
$itemid = request()->itemid;
$catitemid = request()->catitemid;
$serviceid = request()->serviceid;
if(Request::is("spa-shop-branch/$idd"))            {     $spashopbranch      = 'active';  }   
if(Request::is("spa-picture/$idd/$hid/$itemid"))            {  $checkVal=1;   $spapicture         = 'active';  } 
if(Request::is("spa-shop-info/$idd/$hid") || Request::is("spa-shop-info/$idd/$hid/$itemid"))           {     $spashopinfo          = 'active';  }
if(Request::is("spa-service-category/$idd/$hid/$itemid"))           {     $spaservicecat          = 'active';  }
if(Request::is("spa-add-service-catogory/$idd/$hid/$itemid") || Request::is("spa-add-service-catogory/$idd/$hid/$itemid/$catitemid"))  {$spaservicecat  = 'active';  }
if(Request::is("spa-service/$idd/$hid/$itemid"))                {     $spaservice              = 'active';  }
if(Request::is("spa-add-service/$idd/$hid/$itemid") || Request::is("spa-add-service/$idd/$hid/$itemid/$serviceid"))  {     $spaservice   = 'active';  }
if(Request::is("spa-package/$idd/$hid/$itemid"))  {     $spapackage   = 'active';  }
if(Request::is("spa-add-package/$idd/$hid/$itemid") || Request::is("spa-add-package/$idd/$hid/$itemid/$serviceid"))  {     $spapackage   = 'active';  }
if(Request::is("spa-worker/$idd/$hid/$itemid"))  {     $spaworker   = 'active';  }
if(Request::is("spa-add-branch/$idd/$hid/$itemid"))  {     $spalistbranch   = 'active';  }
if(Request::is("spa-order/$idd/$hid/$itemid"))  {     $spaorders   = 'active';  }
if(Request::is("spa-add-worker/$idd/$hid/$itemid"))  {     $spaworker   = 'active';  }
if(Request::is("spa-reviews-and-comments/$idd/$hid/$itemid"))  {     $spareviewsandcomments   = 'active';  }
if(Request::is("spa-offer/$idd/$hid/$itemid"))  {     $spaoffer   = 'active';  }
if(Request::is("spa-add-offer/$idd/$hid/$itemid/$serviceid"))  {     $spaoffer   = 'active';  }
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$spashopbranch or ''}}" href="{{ route('spa-shop-branch',['id' => request()->id,'hid'=>request()->hid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST') }} @endif </a>


 <a class="{{$spashopinfo or ''}}" href="{{ route('spa-shop-info',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>
  
  @if($itemid !='')
   <a class="{{$spapicture or ''}}" href="{{ route('spa-picture',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

  <a class="{{$spaservicecat or ''}}" href="{{ route('spa-service-category',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY') }} @endif </a>
  
  <a class="{{$spaservice or ''}}" href="{{ route('spa-service',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICES') }} @endif </a>
  
  <a class="{{$spapackage or ''}}" href="{{ route('spa-package',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.PACKAGES') }} @endif </a>
  
   <a class="{{$spaworker or ''}}" href="{{ route('spa-worker',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.WORKERS')!= '') {{  trans(Session::get('mer_lang_file').'.WORKERS') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.WORKERS') }} @endif </a>


<a class="{{$spaorders or ''}}" href="{{ route('spa-order',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$spaoffer or ''}}" href="{{ route('spa-offer',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$spareviewsandcomments or ''}}" href="{{ route('spa-reviews-and-comments',['id' => request()->id,'hid'=>request()->hid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

  @endif

</div>
</div>
@endif

<!-- End For spa Big Menu -->
 
<!-- Start For Makup Artist Big Menu -->

@if(isset($MakupArtistbig_leftmenu) && $MakupArtistbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid= request()->autoid;
if(Request::is("makeup-artist-shop-info/$idd/$sid")) {$makeupartist = 'active';}   
if(Request::is("makeup-artist-picture/$idd/$sid")) {$checkVal=1; $artistpicture = 'active';} 
if(Request::is("makeup-artist-service-category/$idd/$sid") || Request::is("makeup-artist-add-service-catogory/$idd/$sid")  || Request::is("makeup-artist-add-service-catogory/$idd/$sid/$autoid")) {$artistservicecat = 'active';}
if(Request::is("makeup-artist-service/$idd/$sid") || Request::is("makeup-artist-add-service/$idd/$sid") || Request::is("makeup-artist-add-service/$idd/$sid/$autoid")) {$artistservice = 'active';}
if(Request::is("makeup-artist-order/$idd/$sid"))   {$spaorders = 'active';}
if(Request::is("makeup-artist-package/$idd/$sid")) {$artistpackage = 'active';}
if(Request::is("makeup-artist-worker/$idd/$sid") || Request::is("makeup-artist-add-worker/$idd/$sid")  || Request::is("makeup-artist-add-worker/$idd/$sid/$autoid")) {$artistworker = 'active';}
if(Request::is("makeup-artist-reviews-and-comments/$idd/$sid")) {$spareviewsandcomments = 'active';}
if(Request::is("makeup-artist-offer/$idd/$sid") || Request::is("makeup-artist-add-offer/$idd/$sid") || Request::is("makeup-artist-add-offer/$idd/$sid/$autoid")) {$spaoffer = 'active';}
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$makeupartist or ''}}" href="{{ route('makeup-artist-shop-info',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
  
   <a class="{{$artistpicture or ''}}" href="{{ route('makeup-artist-picture',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

  <a class="{{$artistservicecat or ''}}" href="{{ route('makeup-artist-service-category',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY') }} @endif </a>
  
  
  
  <a class="{{$artistservice or ''}}" href="{{ route('makeup-artist-service',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICES') }} @endif </a>
  
  
    <a class="{{$artistworker or ''}}" href="{{ route('makeup-artist-worker',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.WORKERS')!= '') {{  trans(Session::get('mer_lang_file').'.WORKERS') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.WORKERS') }} @endif </a>  


<a class="{{$spaorders or ''}}" href="{{ route('makeup-artist-order',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$spaoffer or ''}}" href="{{ route('makeup-artist-offer',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$spareviewsandcomments or ''}}" href="{{ route('makeup-artist-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

</div>
</div>
@endif

<!-- End For Makup Artist Big Menu -->
 
<!-- Start tailor Big Menu -->

@if(isset($Tailorbig_leftmenu) && $Tailorbig_leftmenu==1)
 
@php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
if(Request::is("tailor-shop-info/$idd/$itemid")) {$tailorshopinfo = 'active';}   
if(Request::is("tailor-picture/$idd/$itemid")) {$checkVal=1; $tailorpicture = 'active';} 
if(Request::is("tailor-product/$idd/$itemid") || Request::is("tailor-addproduct/$idd/$itemid")|| Request::is("tailor-addproduct/$idd/$itemid/$autoid")) {$tailorproduct = 'active';}

if(Request::is("tailor-fabric/$idd/$itemid") || Request::is("tailor-addfabric/$idd/$itemid")|| Request::is("tailor-addfabric/$idd/$itemid/$autoid")) {$tailorfabric = 'active';}


if(Request::is("tailor-order/$idd/$itemid")) {$spaorders = 'active';}
if(Request::is("tailor-reviews-and-comments/$idd/$itemid")) {$spareviewsandcomments = 'active';}
if(Request::is("tailor-offer/$idd/$itemid") || Request::is("tailor-add-offer/$idd/$itemid") || Request::is("tailor-add-offer/$idd/$itemid/$autoid") ) {$tailorpffer = 'active';}
@endphp 
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$tailorshopinfo or ''}}" href="{{ route('tailor-shop-info',['id' => request()->id,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
    
    @if($itemid!='')
   <a class="{{$tailorpicture or ''}}" href="{{ route('tailor-picture',['id' => request()->id,'itemid' =>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

  <a class="{{$tailorfabric or ''}}" href="{{ route('tailor-fabric',['id' =>request()->id,'itemid' =>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Fabric')!= '') {{  trans(Session::get('mer_lang_file').'.Fabric') }}  @else {{ trans($MER_OUR_LANGUAGE.'.Fabric') }} @endif </a>
  
  <a class="{{$tailorproduct or ''}}" href="{{ route('tailor-product',['id' =>request()->id,'itemid' =>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Stitch_Designs')!= '') {{  trans(Session::get('mer_lang_file').'.Stitch_Designs') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.Stitch_Designs') }} @endif </a>

<a class="{{$spaorders or ''}}" href="{{ route('tailor-order',['id' => request()->id,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$tailorpffer or ''}}" href="{{ route('tailor-offer',['id' => request()->id,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$spareviewsandcomments or ''}}" href="{{ route('tailor-reviews-and-comments',['id' => request()->id,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
   @endif
</div>
</div>
@endif

<!-- End For tailor Big Menu -->



<!-- Start For Photography Big Menu -->

@if(isset($photographybig_leftmenu) && $photographybig_leftmenu==1)
 
@php
$idd = request()->id;
$itemid = request()->sid;
$autoid = request()->autoid;
if(Request::is("photography-shop-info/$idd/$itemid") || Request::is("photography-shop-info/$idd")) {$makeupartist = 'active';}   
if(Request::is("photography-picture/$idd/$itemid")) {$checkVal=1; $photographypicture = 'active';} 
if(Request::is("photography-package/$idd/$itemid") || Request::is("photography-add-package/$idd/$itemid") || Request::is("photography-add-package/$idd/$itemid/$autoid")) {$photographypackage = 'active';}
if(Request::is("photography-order/$idd/$itemid")) {$photographyorders = 'active';}
if(Request::is("photography-reviews-and-comments/$idd/$itemid")) {$photographyreviewsandcomments = 'active';}
if(Request::is("photography-offer/$idd/$itemid") || Request::is("photography-add-offer/$idd/$itemid") || Request::is("photography-add-offer/$idd/$itemid/$autoid")) {$photographyoffer = 'active';}

@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$makeupartist or ''}}" href="{{ route('photography-shop-info',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
  @if($itemid !='')
 <a class="{{$photographypicture or ''}}" href="{{ route('photography-picture',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
  
 <a class="{{$photographypackage or ''}}" href="{{ route('photography-package',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.PACKAGES') }} @endif </a>

<a class="{{$photographyorders or ''}}" href="{{ route('photography-order',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$photographyoffer or ''}}" href="{{ route('photography-offer',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$photographyreviewsandcomments or ''}}" href="{{ route('photography-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>    
@endif
</div>
</div>
@endif

<!-- End For Photography Big Menu -->

<!-- Start For abaya Big Menu -->

@if(isset($Abayabig_leftmenu) && $Abayabig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
if(Request::is("abaya-shop-info/$idd/$sid")) {$abayashop = 'active';}   

if(Request::is("abayatailor-fabric/$idd/$sid") || Request::is("abayatailor-addfabric/$idd/$sid")) {$abayafabshop = 'active';}   

if(Request::is("abaya-picture/$idd/$sid")) {$checkVal=1; $abayapicure = 'active';} 
if(Request::is("abaya-category/$idd/$sid")|| Request::is("abaya-add-catogory/$idd/$sid") || Request::is("abaya-add-catogory/$idd/$sid/$autoid")) {$abayacategory = 'active';} 
if(Request::is("abaya-service-item/$idd")) {$abayaservice = 'active';}
if(Request::is("abaya-order/$idd/$sid")) {$abayaorder = 'active';}
if(Request::is("abaya-item/$idd/$sid")|| Request::is("abaya-add-item/$idd/$sid") || Request::is("abaya-add-item/$idd/$sid/$autoid")) {$abayaItem = 'active';}
if(Request::is("abaya-offer/$idd/$sid")|| Request::is("abaya-add-offer/$idd/$sid") || Request::is("abaya-add-offer/$idd/$sid/$autoid")) {$abayaoffer = 'active';}
if(Request::is("abaya-reviews-and-comments/$idd/$sid")) {$spareviewsandcomments = 'active';}
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="{{$abayashop or ''}}" href="{{ route('abaya-shop-info',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
  
  @if($sid!='')
   <a class="{{$abayapicure or ''}}" href="{{ route('abaya-picture',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

   <!-- <a class="{{$abayacategory or ''}}" href="{{ route('abaya-category',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.CATEGORY') }} @endif </a>  -->   


  <a class="{{$abayafabshop or ''}}" href="{{ route('abayatailor-fabric',['id' =>request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Fabric')!= '') {{  trans(Session::get('mer_lang_file').'.Fabric') }}  @else {{ trans($MER_OUR_LANGUAGE.'.Fabric') }} @endif </a>

    <a class="{{$abayaItem or ''}}" href="{{ route('abaya-item',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.abaya_item')!= '') {{  trans(Session::get('mer_lang_file').'.abaya_item') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.abaya_item') }} @endif </a>
  
<a class="{{$abayaorder or ''}}" href="{{ route('abaya-order',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$abayaoffer or ''}}" href="{{ route('abaya-offer',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$spareviewsandcomments or ''}}" href="{{ route('abaya-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- End For abaya Big Menu -->
 
<!-- Start For dresses Big Menu -->

@if(isset($dressesbig_leftmenu) && $dressesbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
if(Request::is("dresses-shop-info/$idd/$sid")) {$dressesshop = 'active';}   
if(Request::is("dresses-picture/$idd/$sid")) {$checkVal=1; $dressespicure = 'active';} 
if(Request::is("dresses-category/$idd/$sid")|| Request::is("dresses-add-catogory/$idd/$sid") || Request::is("dresses-add-catogory/$idd/$sid/$autoid")) {$dressescategory = 'active';}
if(Request::is("dresses-service-item/$idd")) {$dressesservice = 'active';}
if(Request::is("dresses-order/$idd/$sid")) {$dressesorder = 'active';}
if(Request::is("dresses-item/$idd/$sid")|| Request::is("dresses-add-item/$idd/$sid") || Request::is("dresses-add-item/$idd/$sid/$autoid")) {$dressesItem = 'active';}
if(Request::is("dresses-offer/$idd/$sid")|| Request::is("dresses-add-offer/$idd/$sid") || Request::is("dresses-add-offer/$idd/$sid/$autoid")) {$dressesoffer = 'active';}
if(Request::is("dresses-reviews-and-comments/$idd/$sid")) {$dressesreviewsandcomments = 'active';}

if(Request::is("dresses-size/$idd/$sid") || Request::is("dresses-add-size/$idd/$sid") || Request::is("dresses-add-size/$idd/$sid/$autoid") ) {$dressessize = 'active';}   
 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="{{$dressesshop or ''}}" href="{{ route('dresses-shop-info',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
  
  @if($sid!='')
   <a class="{{$dressespicure or ''}}" href="{{ route('dresses-picture',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

   <!---<a class="{{$dressessize or ''}}" href="{{ route('dresses-size',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.SIZE')!= '') {{  trans(Session::get('mer_lang_file').'.SIZE') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.SIZE') }} @endif </a>   --> 

   <a class="{{$dressescategory or ''}}" href="{{ route('dresses-category',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.CATEGORY') }} @endif </a>    

    <a class="{{$dressesItem or ''}}" href="{{ route('dresses-item',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.dresses_item')!= '') {{  trans(Session::get('mer_lang_file').'.dresses_item') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.dresses_item') }} @endif </a>
  
<a class="{{$dressesorder or ''}}" href="{{ route('dresses-order',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$dressesoffer or ''}}" href="{{ route('dresses-offer',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$dressesreviewsandcomments or ''}}" href="{{ route('dresses-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- End For dresses Big Menu -->

<!-- Start For Cosha Big Menu -->

@if(isset($Coshabig_leftmenu) && $Coshabig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
if(Request::is("cosha-shop-info/$idd/$sid")) {$coshashop = 'active';}   
if(Request::is("cosha-picture/$idd/$sid")) {$checkVal=1; $coshapicure = 'active';} 
if(Request::is("cosha-category/$idd/$sid")|| Request::is("cosha-add-catogory/$idd/$sid") || Request::is("cosha-add-catogory/$idd/$sid/$autoid")) {$coshacategory = 'active';}
if(Request::is("cosha-service-item/$idd")) {$coshaservice = 'active';}
if(Request::is("cosha-order/$idd/$sid")) {$coshaorder = 'active';}
if(Request::is("cosha-item/$idd/$sid")|| Request::is("cosha-add-item/$idd/$sid") || Request::is("cosha-add-item/$idd/$sid/$autoid")) {$coshaItem = 'active';}
if(Request::is("cosha-offer/$idd/$sid")|| Request::is("cosha-add-offer/$idd/$sid") || Request::is("cosha-add-offer/$idd/$sid/$autoid")) {$coshaoffer = 'active';}
if(Request::is("cosha-reviews-and-comments/$idd/$sid")) {$spareviewsandcomments = 'active';}
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="{{$coshashop or ''}}" href="{{ route('cosha-shop-info',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
  
  @if($sid!='')
   <a class="{{$coshapicure or ''}}" href="{{ route('cosha-picture',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

   <a class="{{$coshacategory or ''}}" href="{{ route('cosha-category',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.CATEGORY') }} @endif </a>    

    <a class="{{$coshaItem or ''}}" href="{{ route('cosha-item',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.cosha_item')!= '') {{  trans(Session::get('mer_lang_file').'.cosha_item') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.cosha_item') }} @endif </a>
  
<a class="{{$coshaorder or ''}}" href="{{ route('cosha-order',['id' => request()->id,'sid' =>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$coshaoffer or ''}}" href="{{ route('cosha-offer',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$spareviewsandcomments or ''}}" href="{{ route('cosha-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- End For Cosha Big Menu -->

<!-- Start For Special Event Big Menu -->

@if(isset($Eventbig_leftmenu) && $Eventbig_leftmenu==1)
 
@php
$idd = request()->id;
$shopid = request()->shopid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
if(Request::is("event-shop-info/$idd") || Request::is("event-shop-info/$idd/$shopid")) {$eventshop = 'active';}   
if(Request::is("event-picture/$idd/$shopid")) {$checkVal=1; $eventpicure = 'active';} 
if(Request::is("event-package/$idd/$shopid") || Request::is("event-add-package/$idd/$shopid")  || Request::is("event-add-package/$idd/$shopid/$itemid")) {$eventpackage = 'active';} 
if(Request::is("event-category/$idd/$shopid")) {$eventcategory = 'active';}
if(Request::is("event-service/$idd/$shopid")) {$eventservice = 'active';}
if(Request::is("event-item-information/$idd/$shopid") || Request::is("event-item-add-information/$idd/$shopid") || Request::is("event-item-add-information/$idd/$shopid/$cid") ) {$eventItemservice = 'active';}
if(Request::is("event-order/$idd/$shopid")) {$eventorder = 'active';}
if(Request::is("event-package/$idd/$shopid")) {$eventpackage = 'active';}
if(Request::is("event-worker/$idd/$shopid")) {$eventworker = 'active';}
if(Request::is("event-reviews-and-comments/$idd/$shopid")) {$eventreviewsandcomments = 'active';}
if(Request::is("event-item/$idd/$shopid") ) {$eventItem = 'active';} 
if(Request::is("event-offer/$idd/$shopid") || Request::is("event-add-offer/$idd/$shopid") || Request::is("event-add-offer/$idd/$shopid/$autoid")) {$eventoffer = 'active';}

@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="{{$eventshop or ''}}" href="{{ route('event-shop-info',['id' => request()->id, 'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.special_event_info')!= '') {{  trans(Session::get('mer_lang_file').'.special_event_info') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.special_event_info') }} @endif </a>
  
  @if($shopid!='')
   <a class="{{$eventpicure or ''}}" href="{{ route('event-picture',['id' => request()->id, 'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
  
   <a class="{{$eventpackage or ''}}" href="{{ route('event-package',['id' => request()->id, 'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CATEGORY') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_CATEGORY') }} @endif </a>  

  <a class="{{$eventItemservice or ''}}" href="{{ route('event-item-information',['id' => request()->id, 'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.item_information')!= '') {{  trans(Session::get('mer_lang_file').'.item_information') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.item_information') }} @endif </a>
  
 

  
<a class="{{$eventorder or ''}}" href="{{ route('event-order',['id' => request()->id, 'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$eventoffer or ''}}" href="{{ route('event-offer',['id' => request()->id, 'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$eventreviewsandcomments or ''}}" href="{{ route('event-reviews-and-comments',['id' => request()->id, 'shopid' => request()->shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- End For Special Event Big Menu -->









<!-- Start For Car rental Menu -->

@if(isset($Carrental_leftmenu) && $Carrental_leftmenu==1)
 
@php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;
if(Request::is("car-rental/$idd") || Request::is("car-rental/$idd/$shopid")) {$eventshop = 'active';}   
if(Request::is("car-picture/$idd/$shopid")) {$checkVal=1; $eventpicure = 'active';} 

 
 
if(Request::is("car-items/$idd/$shopid") || Request::is("car-add-items/$idd/$shopid") || Request::is("car-add-items/$idd/$shopid/$autoid") ) {$eventItemservice = 'active';}

if(Request::is("car-order/$idd/$shopid")) {$eventorder = 'active';}
 
 
if(Request::is("car-reviews-and-comments/$idd/$shopid")) {$eventreviewsandcomments = 'active';}
if(Request::is("car-item/$idd/$shopid") ) {$eventItem = 'active';}

 
if(Request::is("car-offer/$idd/$shopid") || Request::is("car-add-offer/$idd/$shopid") || Request::is("car-add-offer/$idd/$shopid/$autoid")) {$eventoffer = 'active';}


@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="{{$eventshop or ''}}" href="{{ route('car-rental',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.CarRentalInfo')!= '') {{  trans(Session::get('mer_lang_file').'.CarRentalInfo') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.CarRentalInfo') }} @endif </a>
  @if($shopid!='')
   <a class="{{$eventpicure or ''}}" href="{{ route('car-picture',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
 

  <a class="{{$eventItemservice or ''}}" href="{{ route('car-items',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Car_Models')!= '') {{  trans(Session::get('mer_lang_file').'.Car_Models') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.Car_Models') }} @endif </a>
   
<a class="{{$eventorder or ''}}" href="{{ route('car-order',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$eventoffer or ''}}" href="{{ route('car-offer',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$eventreviewsandcomments or ''}}" href="{{ route('car-reviews-and-comments',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif




<!-- Start For Car rental Menu -->

@if(isset($Travel_leftmenu) && $Travel_leftmenu==1)
 
@php
$idd = request()->id;
$shopid = request()->sid;
$autoid = request()->autoid;
$cid = request()->cid;
$itemid = request()->itemid;

if(Request::is("travel-info/$idd") || Request::is("travel-info/$idd/$shopid")) {$eventshop = 'active';}   
if(Request::is("travel-picture/$idd/$shopid")) {$checkVal=1; $eventpicure = 'active';} 

 
 
if(Request::is("travel-items/$idd/$shopid") || Request::is("travel-add-items/$idd/$shopid") || Request::is("travel-add-items/$idd/$shopid/$autoid") ) {$eventItemservice = 'active';}

if(Request::is("travel-order/$idd/$shopid")) {$eventorder = 'active';}
 
 
if(Request::is("travel-reviews-and-comments/$idd/$shopid")) {$eventreviewsandcomments = 'active';}
if(Request::is("travel-item/$idd/$shopid") ) {$eventItem = 'active';}

 
if(Request::is("travel-offer/$idd/$shopid") || Request::is("travel-add-offer/$idd/$shopid") || Request::is("travel-add-offer/$idd/$shopid/$autoid")) {$eventoffer = 'active';}


@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">

 <a class="{{$eventshop or ''}}" href="{{ route('travel-info',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.TravelInfo')!= '') {{  trans(Session::get('mer_lang_file').'.TravelInfo') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.TravelInfo') }} @endif </a>
  @if($shopid!='')
   <a class="{{$eventpicure or ''}}" href="{{ route('travel-picture',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
 

  <a class="{{$eventItemservice or ''}}" href="{{ route('travel-items',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.PACKAGES') }} @endif </a>
   
<a class="{{$eventorder or ''}}" href="{{ route('travel-order',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$eventoffer or ''}}" href="{{ route('travel-offer',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$eventreviewsandcomments or ''}}" href="{{ route('travel-reviews-and-comments',['id' => request()->id, 'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- End For Special Event Big Menu -->












<!-- Start For Makup Artist Big Menu -->

@if(isset($Rosestbig_leftmenu) && $Rosestbig_leftmenu==1)
 
@php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
if(Request::is("roses-shop-info/$idd") || Request::is("roses-shop-info/$idd/$itemid")) {$inforoses = 'active';}   
if(Request::is("roses-picture/$idd/$itemid")) {$checkVal=1; $rosespicture = 'active';} 
if(Request::is("roses-package/$idd/$itemid") || Request::is("roses-add-package/$idd/$itemid") || Request::is("roses-add-package/$idd/$itemid/$autoid")) {$rosespackage = 'active';}
if(Request::is("roses-order/$idd/$itemid")) {$artistservice = 'active';}
if(Request::is("roses-type/$idd/$itemid") || Request::is("roses-add-type/$idd/$itemid") || Request::is("roses-add-type/$idd/$itemid/$autoid") ) {$rosestype = 'active';}
if(Request::is("roses-order/$idd/$itemid")) {$rosesorders = 'active';}
if(Request::is("roses-reviews-and-comments/$idd/$itemid")) {$rosesreviewsandcomments = 'active';}
if(Request::is("roses-offer/$idd/$itemid") || Request::is("roses-add-offer/$idd/$itemid/$autoid") || Request::is("roses-add-offer/$idd/$itemid") ) {$rosesoffer = 'active';} 
if(Request::is("wrapping-type/$idd/$itemid") || Request::is("roses-add-wraping-type/$idd/$itemid") || Request::is("roses-add-wraping-type/$idd/$itemid/$autoid")) {$wrappingtype = 'active';}
if(Request::is("wrapping-design/$idd/$itemid") || Request::is("roses-add-wraping-design/$idd/$itemid") || Request::is("roses-add-wraping-design/$idd/$itemid/$autoid")) {$wrappingdesign = 'active';}

@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$inforoses or ''}}" href="{{ route('roses-shop-info',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
  @if($itemid!='')
   <a class="{{$rosespicture or ''}}" href="{{ route('roses-picture',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>


  <a class="{{$wrappingtype or ''}}" href="{{ route('wrapping-type',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.WRAPPINGTYPE')!= '') {{  trans(Session::get('mer_lang_file').'.WRAPPINGTYPE') }}  @else {{ trans($MER_OUR_LANGUAGE.'.WRAPPINGTYPE') }} @endif </a>

    <a class="{{$wrappingdesign or ''}}" href="{{ route('wrapping-design',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.WRAPPINGDESIGN')!= '') {{  trans(Session::get('mer_lang_file').'.WRAPPINGDESIGN') }}   @else {{ trans($MER_OUR_LANGUAGE.'.WRAPPINGDESIGN') }} @endif </a>


  <a class="{{$rosestype or ''}}" href="{{ route('roses-type',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.ROSESTYPE')!= '') {{  trans(Session::get('mer_lang_file').'.ROSESTYPE') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.ROSESTYPE') }} @endif </a>

    <a class="{{$rosespackage or ''}}" href="{{ route('roses-package',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.PACKAGES')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.PACKAGES') }} @endif </a>
   


<a class="{{$rosesorders or ''}}" href="{{ route('roses-order',['id'=>request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= ''){{trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$rosesoffer or ''}}" href="{{ route('roses-offer',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
   
  
  <a class="{{$rosesreviewsandcomments or ''}}" href="{{ route('roses-reviews-and-comments',['id' => request()->id,'itemid' => request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- End For Makup Artist Big Menu -->




<!-- Start For Men's Barber Big Menu -->

@if(isset($mensbig_leftmenu) && $mensbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;

if(Request::is("mens-shop-info/$idd") || Request::is("mens-shop-info/$idd/$sid"))            {     $mens_review      = 'active';  }    
if(Request::is("mens-picture/$idd/$sid"))   {   $checkVal=1;  $menspicture          = 'active';  }



if(Request::is("mens-service-category/$idd/$sid") || Request::is("mens-add-service-catogory/$idd/$sid") || Request::is("mens-add-service-catogory/$idd/$sid/$autoid"))  {     $mensservicecategory          = 'active';  }

if(Request::is("mens-service/$idd/$sid") || Request::is("mens-add-service/$idd/$sid")  || Request::is("mens-add-service/$idd/$sid/$autoid"))  {$mensservice     = 'active';  }


if(Request::is("mens-worker/$idd/$sid") || Request::is("mens-add-worker/$idd/$sid") || Request::is("mens-add-worker/$idd/$sid/$autoid"))   {     $mensworker    = 'active';  }

if(Request::is("mens-order/$idd/$sid"))  {  $mensorder   = 'active';  }

if(Request::is("mens-offer/$idd/$sid") || Request::is("mens-add-offer/$idd/$sid") || Request::is("mens-add-offer/$idd/$sid/$autoid"))  {  $mensoffer   = 'active';  }

if(Request::is("mens-reviews-and-comments/$idd/$sid"))  {  $mensreview   = 'active';  }
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">


 <a class="{{$mens_review or ''}}" href="{{ route('mens-shop-info',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.mens_barbe_info')!= '') {{  trans(Session::get('mer_lang_file').'.mens_barbe_info') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.mens_barbe_info') }} @endif </a>

  @if($sid!='')
   <a class="{{$menspicture or ''}}" href="{{ route('mens-picture',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

  <a class="{{$mensservicecategory or ''}}" href="{{ route('mens-service-category',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_CATEGORY') }}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_CATEGORY') }} @endif </a>
  
  <a class="{{$mensservice or ''}}" href="{{ route('mens-service',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICES') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICES') }} @endif </a>
  
 
  
   <a class="{{$mensworker or ''}}" href="{{ route('mens-worker',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.WORKERS')!= '') {{  trans(Session::get('mer_lang_file').'.WORKERS') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.WORKERS') }} @endif</a>


<a class="{{$mensorder or ''}}" href="{{ route('mens-order',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
  <a class="{{$mensoffer or ''}}" href="{{ route('mens-offer',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </a>
  
  <a class="{{$mensreview or ''}}" href="{{ route('mens-reviews-and-comments',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
@endif
</div>
</div>
@endif

<!-- Start For Men's Barber Big Menu End -->





<!-- Sratr For Dessert Front Menu -->

@if(isset($dessert_leftmenu) && $dessert_leftmenu==1)
@php 

$id= request()->id;
$services_id= request()->services_id;
$shopid= request()->shopid;
 
 
if(Request::is("dessert-shop/$id"))    {  $id = request()->id;   $buffetresturant = 'active'; } 
 

if(Request::is("dessert-add-shop/$services_id"))    {  $id = request()->services_id;   $buffetresturant = 'active'; } 

if(Request::is("dessert-list-branch/$services_id/$shopid"))    {  $id = request()->services_id;   $buffetresturant = 'active'; } 
@endphp


<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >

<div class="sidenav">
<a class="{{$buffetresturant or ''}}" href="{{ route('dessert-shop',['id' =>$id]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP') }} @endif</a>



   </div>
   </div>


@endif
<!-- End For Dessert Front Menu -->

<!-- Sratr For Dessert Inner Menu -->
@if(isset($dessert_inner_leftmenu) && $dessert_inner_leftmenu==1)
 
@php
$idd = request()->id;

$services_id = request()->services_id;
$shopid = request()->shopid;
$itemid = request()->itemid;
$autoid = request()->autoid;
$offer_id = request()->offer_id;
if(Request::is("buffet-list-branch/$idd"))            {     $buffetlistbranch      = 'active';  }   
if(Request::is("dessert-picture-video/$services_id/$shopid/$itemid")) { $checkVal=1;    $buffetpicture         = 'active';  } 
if(Request::is("buffet-main-cource/$idd"))           {     $buffetmaincource          = 'active';  }
if(Request::is("buffet-dish/$idd"))           {     $buffetdish          = 'active';  }
if(Request::is("buffet-desert/$idd"))                {     $buffedesert               = 'active';  }
if(Request::is("buffet-orders/$idd"))                {     $buffeorders               = 'active';  }
if(Request::is("buffet-reviews-and-comments/$idd"))  {     $buffereviewsandcomments   = 'active';  }
if(Request::is("buffet-menu-list/$idd"))  {     $buffetmenu   = 'active';  }
if(Request::is("add-buffet-menu-category/$idd"))  {     $buffetmenu   = 'active';  }
if(Request::is("buffet-add-branch/$idd"))  {     $buffetlistbranch   = 'active';  }
if(Request::is("buffet-list-container/$idd"))  {     $buffetlistcontainer   = 'active';  }
if(Request::is("dessert-info/$services_id/$shopid") || Request::is("dessert-info/$services_id/$shopid/$itemid"))  {     $buffetinfo   = 'active';  }


if(Request::is("dessert-type/$services_id/$shopid/$itemid") || Request::is("dessert-add-type/$services_id/$shopid/$itemid") || Request::is("dessert-add-type/$services_id/$shopid/$itemid/$autoid"))  {     $buffetType   = 'active';  }


if(Request::is("dessert-list-offer/$services_id/$shopid/$itemid") || Request::is("dessert-add-offer/$services_id/$shopid/$itemid") || Request::is("dessert-add-offer/$services_id/$shopid/$itemid/$offer_id"))  {     $dessertlistoffer   = 'active';  }

 if(Request::is("dessert-order/$services_id/$shopid/$itemid"))  {     $dessertorders   = 'active';  }
 if(Request::is("dessert-reviews-and-comments/$services_id/$shopid/$itemid"))  {     $dessertreviewsandcomments   = 'active';  }



$shopid = request()->shopid;
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$buffetlistbranch or ''}}" href="{{ route('dessert-list-branch',['services_id' => request()->services_id,'shopid'=>$shopid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST') }} @endif</a>


 <a class="{{$buffetinfo or ''}}" href="{{ route('dessert-info',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid' => request()->itemid] )}}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>

@if(request()->itemid!='')
  <a class="{{$buffetpicture or ''}}" href="{{ route('dessert-picture-video',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
  
  <a class="{{$buffetType or ''}}" href="{{ route('dessert-type',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Type_Dishes')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Type_Dishes') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Type_Dishes') }} @endif</a>
  
   <a class="{{$dessertlistoffer or ''}}" href="{{ route('dessert-list-offer',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Offer') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Offer') }} @endif</a>

  
<a class="{{$dessertorders or ''}}" href="{{ route('dessert-order',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>

  <a class="{{$dessertreviewsandcomments or ''}}" href="{{ route('dessert-reviews-and-comments',['services_id' => request()->services_id,'shopid'=>$shopid,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

  @endif
</div>
</div>
@endif
<!-- End For Dessert Inner Menu -->


<!-- Sratr For Dates Inner Menu -->
@if(isset($dates_inner_leftmenu) && $dates_inner_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$offerid  = request()->offerid;
if(Request::is("dates-info/$idd") || Request::is("dates-info/$idd/$sid")) {$dateinfo = 'active';  }   
if(Request::is("dates-picture-video/$idd/$sid")) {$checkVal=1; $datespicture = 'active';  } 
if(Request::is("buffet-main-cource/$idd")) {$buffetmaincource = 'active';  }
if(Request::is("dates-type/$idd/$sid") || Request::is("dates-add-type/$idd/$sid") || Request::is("dates-add-type/$idd/$sid/$autoid")) {$datetype = 'active';  }

 if(Request::is("dates-order/$idd/$sid")) {$datesorders = 'active';  }
  if(Request::is("dates-reviews-and-comments/$idd/$sid")) {$datescomment = 'active';  }
 

if(Request::is("dates-list-offer/$idd/$sid") || Request::is("dates-add-offer/$idd/$sid") || Request::is("dates-add-offer/$idd/$sid/$offerid")) {$datelistoffer = 'active';  }
 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$dateinfo or ''}}" href="{{ route('dates-info',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>

@if($sid!='')
  <a class="{{$datespicture or ''}}" href="{{ route('dates-picture-video',['id' => request()->id,'sid' => request()->sid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
  
 
  <a class="{{$datetype or ''}}" href="{{ route('dates-type',['id' => request()->id,'sid' => request()->sid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Types')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Types') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Types') }} @endif</a>
  
   <a class="{{$datelistoffer or ''}}" href="{{ route('dates-list-offer',['id' => request()->id,'sid' => request()->sid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Offer') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Offer') }} @endif</a>

  
<a class="{{$datesorders or ''}}" href="{{ route('dates-order',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>

  <a class="{{$datescomment or ''}}" href="{{ route('dates-reviews-and-comments',['id' => request()->id,'sid' => request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>
  @endif
</div>
</div>
@endif
<!-- End For Dates Inner Menu -->



<!-- Laser Cosmetic 3 links--> 

@if(isset($laser_cosmetic_leftmenu) && $laser_cosmetic_leftmenu==1)
@php if(Request::is('/laser-cosmetic-shop/4')) {$lasershop = 'active';} 
if(Request::is('laser-cosmetic-shop-branch/4')) {$laserbranch = 'active';} 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>

<div  class="left_panel">
<div class="sidenav 4">

  <a class="{{$lasershop or ''}}" href="{{route('laser-cosmetic-shop',['id' =>request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Clinic')!= '') {{  trans(Session::get('mer_lang_file').'.Clinic') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.Clinic') }} @endif </a>
 
</div>
</div>
@endif



<!-- Start For Laser Cosmetic Big Menu -->

@if(isset($laserbig_leftmenu) && $laserbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;

$incid = request()->incid;

if(Request::is("laser-cosmetic-shop/$idd")) {$lasershop = 'active';} 

if(Request::is("laser-cosmetic-shop-branch/$idd/$sid") || Request::is("laser-cosmetic-shop-info/$idd/$sid")) {$laserbranch = 'active';} 

if(Request::is("laser-cosmetic-shop-info/$idd/$sid/$autoid")) {$laseshopinfo = 'active';} 

if(Request::is("laser-cosmetic-picture/$idd/$sid/$autoid")) {$checkVal=1; $lasepicture = 'active';} 

if(Request::is("laser-cosmetic-shop-info/$idd/$sid")) {$laseshopinfo = 'active'; }

if(Request::is("laser-cosmetic-shop-info/$idd/$sid/$autoid")) {$laseshopinfo = 'active'; }

if(Request::is("doctors/$idd/$sid/$autoid") || Request::is("add-doctors/$idd/$sid/$autoid") || Request::is("add-doctors/$idd/$sid/$autoid/$incid")) {$doctors = 'active'; }


if(Request::is("laser-cosmetic-reviews-and-comments/$idd/$sid/$autoid")) {$lasereviewsandcomments = 'active'; }

if(Request::is("laser-cosmetic-order/$idd/$sid/$autoid")) {$laseorders = 'active'; }

if(Request::is("laser-offer/$idd/$sid/$autoid") || Request::is("laser-addoffer/$idd/$sid/$autoid") || Request::is("laser-addoffer/$idd/$sid/$autoid/$incid")) {$laseroffer = 'active'; }


@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">

 <a class="{{$lasershop or ''}}" href="{{ route('laser-cosmetic-shop',['id' => request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Clinic')!= '') {{  trans(Session::get('mer_lang_file').'.Clinic') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.Clinic') }} @endif </a>

  <a class="{{$laserbranch or ''}}" href="{{route('laser-cosmetic-shop-branch',['id' =>request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BRANCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BRANCH') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_BRANCH') }} @endif </a>

  @if($autoid!='')
 <a class="{{$laseshopinfo or ''}}" href="{{ route('laser-cosmetic-shop-info',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>

  <a class="{{$lasepicture or ''}}" href="{{ route('laser-cosmetic-picture',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>

<a class="{{$doctors or ''}}" href="{{ route('doctors',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.DRLIST')!= '') {{  trans(Session::get('mer_lang_file').'.DRLIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.DRLIST') }} @endif </a>

  <a class="{{$laseroffer or ''}}" href="{{ route('laser-offer',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Offer') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Offer') }} @endif </a>   
    



 <a class="{{$lasereviewsandcomments or ''}}" href="{{ route('laser-cosmetic-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

<a class="{{$laseorders or ''}}" href="{{ route('laser-cosmetic-order',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>
  
@endif


</div>
</div>
@endif

<!-- End For Laser Cosmetic Big Menu -->



<!-- Skin & Teeth 3 links--> 

@if(isset($skin_teeth_leftmenu) && $skin_teeth_leftmenu==1)
@php if(Request::is('/skin-teeth-shop/4')) {$skinshop = 'active';} 
if(Request::is('skin-teeth-shop-branch/4')) {$skinbranch = 'active';} 
@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>

<div  class="left_panel">
<div class="sidenav 4">

  <a class="{{$skinshop or ''}}" href="{{route('skin-teeth-shop',['id' =>request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Clinic')!= '') {{  trans(Session::get('mer_lang_file').'.Clinic') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.Clinic') }} @endif </a>
 



</div>
</div>
@endif



<!-- Start For Skin & Teeth Big Menu -->

@if(isset($SkinTeethbig_leftmenu) && $SkinTeethbig_leftmenu==1)
 
@php
$idd = request()->id;
$sid = request()->sid;
$autoid = request()->autoid;
$incid = request()->incid;
if(Request::is("skin-teeth-shop-branch/$idd/$sid") || Request::is("skin-teeth-shop-info/$idd/$sid")) {$skinshopbranch = 'active';} 

if(Request::is("skin-teeth-shop-info/$idd/$sid/$autoid")) {$skinshopinfo = 'active';} 

if(Request::is("skin-teeth-doctors/$idd/$sid/$autoid") || Request::is("skin-teeth-add-doctors/$idd/$sid/$autoid") || Request::is("skin-teeth-add-doctors/$idd/$sid/$autoid/$incid")) {$skindoctor = 'active'; }

if(Request::is("skin-teeth-picture/$idd/$sid/$autoid")) {$checkVal=1; $skinpicture = 'active'; }       

if(Request::is("skin-teeth-reviews-and-comments/$idd/$sid/$autoid")) {$skinreview = 'active'; }

if(Request::is("skin-teeth-offer/$idd/$sid/$autoid") || Request::is("skin-addoffer/$idd/$sid/$autoid") || Request::is("skin-addoffer/$idd/$sid/$autoid/$incid")) {$skinoffer = 'active'; }

if(Request::is("skin-teeth-order/$idd/$sid/$autoid")) {$skinorder = 'active'; }

@endphp   
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel" >
<div class="sidenav">
<a class="{{$skinshop or ''}}" href="{{route('skin-teeth-shop',['id' =>request()->id]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.Clinic')!= '') {{  trans(Session::get('mer_lang_file').'.Clinic') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.Clinic') }} @endif </a>

 <a class="{{$skinshopbranch or ''}}" href="{{ route('skin-teeth-shop-branch',['id' => request()->id,'sid'=>request()->sid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_LIST')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_LIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_LIST') }} @endif </a>
 
   @if($autoid!='')
 <a class="{{$skinshopinfo or ''}}" href="{{ route('skin-teeth-shop-info',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </a>
  <a class="{{$skinpicture or ''}}" href="{{ route('skin-teeth-picture',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a>
<a class="{{$skindoctor or ''}}" href="{{ route('skin-teeth-doctors',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.DRLIST')!= '') {{  trans(Session::get('mer_lang_file').'.DRLIST') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.DRLIST') }} @endif </a>  
    
 <a class="{{$skinoffer or ''}}" href="{{ route('skin-teeth-offer',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Offer') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Offer') }} @endif </a>  


 <a class="{{$skinreview or ''}}" href="{{ route('skin-teeth-reviews-and-comments',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>

<a class="{{$skinorder or ''}}" href="{{ route('skin-teeth-order',['id' => request()->id,'sid'=>request()->sid,'autoid'=>request()->autoid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a>

 @endif
</div>
</div>
@endif

<!-- End For Skin & Teeth Big Menu -->
 

<!-- Start For Makeup Menu -->

@if(isset($makeup_leftmenu) && $makeup_leftmenu==1)
 
@php
$idd = request()->id;
$itemid = request()->itemid;
$autoid = request()->autoid;
if(Request::is("makeup-shop-info/$idd/$itemid") || Request::is("makeup-shop-info/$idd"))           {     $makeupshopinfo          = 'active';  }
if(Request::is("makeup-picture-video/$idd/$itemid"))      { $checkVal=1;    $makeuppicturevideo      = 'active';  }

if(Request::is("makeup-product/$idd/$itemid") || Request::is("makeup-add-product/$idd/$itemid") || Request::is("makeup-add-product/$idd/$itemid/$autoid") )      {     $makeupproduct      = 'active';  } 



if(Request::is("makeup-order/$idd/$itemid"))      {     $makeuporder      = 'active';  } 
if(Request::is("makeup-offer/$idd/$itemid") || Request::is("makeup-add-offer/$idd/$itemid")  || Request::is("makeup-add-offer/$idd/$itemid/$autoid"))      {     $makeupoffer      = 'active';  } 


if(Request::is("makeup-reviews-comments/$idd/$itemid"))      {     $makeupreviewscomments      = 'active';} 
if(Request::is("makeup-service-category/$idd/$itemid"))      {     $makeupservicecategory      = 'active';} 

if(Request::is("makeup-service-add-category/$idd/$itemid") || Request::is("makeup-service-add-category/$idd/$itemid/$autoid"))      {     $makeupservicecategory      = 'active';} 

@endphp
<div class="hall-menu-outer"><div class="hall_menu"></div></div>
<div  class="left_panel">
<div class="sidenav">
  <a class="{{$makeupshopinfo or ''}}" href="{{ route('makeup-shop-info',['id' => request()->id,'itemid'=>request()->itemid]) }}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_SHOP_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SHOP_INFO') }} @else {{ trans($MER_OUR_LANGUAGE.'.MER_SHOP_INFO') }} @endif </a>
   @if($itemid!='' )
  <a class="{{$makeuppicturevideo or ''}}" href="{{ route('makeup-picture-video',['id' => request()->id,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{ trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }} @else {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </a> 

  <a class="{{$makeupservicecategory or ''}}" href="{{ route('makeup-service-category',['id' => request()->id,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_CATEGORIES')!= '') {{ trans(Session::get('mer_lang_file').'.MER_CATEGORIES') }} @else {{ trans($MER_OUR_LANGUAGE.'.MER_CATEGORIES') }} @endif </a> 
     
  <a class="{{$makeupproduct or ''}}" href="{{ route('makeup-product',['id' => request()->id,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.product_list')!= '') {{ trans(Session::get('mer_lang_file').'.product_list') }} @else {{ trans($MER_OUR_LANGUAGE.'.product_list') }} @endif </a> 
     
        
  
  <a class="{{$makeuporder or ''}}" href="{{ route('makeup-order',['id' => request()->id,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{ trans(Session::get('mer_lang_file').'.MER_Order') }} @else {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </a> 
  
    <a class="{{$makeupoffer or ''}}" href="{{ route('makeup-offer',['id' => request()->id,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Offer')!= '') {{ trans(Session::get('mer_lang_file').'.MER_Offer') }} @else {{ trans($MER_OUR_LANGUAGE.'.MER_Offer') }} @endif </a> 
  
    <a class="{{$makeupreviewscomments or ''}}" href="{{ route('makeup-reviews-comments',['id' => request()->id,'itemid'=>request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Reviewandcomments')!= '') {{ trans(Session::get('mer_lang_file').'.MER_Reviewandcomments') }} @else {{ trans($MER_OUR_LANGUAGE.'.MER_Reviewandcomments') }} @endif </a>  
  
  @endif
  
</div>
</div>
@endif

<!-- End For Makeup Menu -->
 
<div class="overlay"></div>
<!-- Menu Script start -->
<script>
$('.hall-menu-outer').click(function(){
$('.sidenav').slideToggle(500); 
$('.overlay').css('display','block'); 
});
$('.overlay').click(function(){
$('.sidenav').slideUp(500); 
$('.overlay').css('display','none');  
});
</script>

<!-- Menu Script End -->

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}
</script>
 @if(isset($checkVal) && $checkVal==1)
  
<script>
  $(window).load(function(){

  jQuery('input[name=youtubevideo]').on("focusout", function(e){   
  var url = jQuery('input[name=youtubevideo]').val();
 
 jQuery('.youtubeerror').remove();


 var res = url.split("https://youtu.be/");

    if(res[1]== undefined)
    {
      jQuery('input[name=youtubevideo]').val('');
    jQuery('.ex-show').last().append("<span class='error youtubeerror'>@if (Lang::has(Session::get('mer_lang_file').'.Please_enter_valid_url')!= '') {{ trans(Session::get('mer_lang_file').'.Please_enter_valid_url') }} @else {{ trans($MER_OUR_LANGUAGE.'.Please_enter_valid_url') }} @endif</span>");
    }
    else if(res[1].length != 11)
    {
      jQuery('input[name=youtubevideo]').val('');
    jQuery('.ex-show').last().append("<span class='error youtubeerror'>@if (Lang::has(Session::get('mer_lang_file').'.Please_enter_valid_url')!= '') {{ trans(Session::get('mer_lang_file').'.Please_enter_valid_url') }} @else {{ trans($MER_OUR_LANGUAGE.'.Please_enter_valid_url') }} @endif</span>");

    }
    else
    {
jQuery('.youtubeerror').remove();

    }


  });



 $("input[name='submit']").click(function(e){      
      var aboutVideo = jQuery("#about").val();
      var aboutVideoAr = jQuery("#about_ar").val();
      if($.trim(aboutVideoAr)== '' && $.trim(aboutVideo)== '')
      {
      return true;
      }
      if($.trim(aboutVideoAr)!= '' && $.trim(aboutVideo)!= '')
      {
      return true;
      }
 
      @if($mer_selected_lang_code !='ar')

      if($.trim(aboutVideoAr)== '' )
      {   e.preventDefault();
      $('.arabic_tab').trigger('click');
      }
      if($.trim(aboutVideo)=='')
      {   e.preventDefault();
      $('.english_tab').trigger('click');  
      }

      @else

      if($.trim(aboutVideo)=='')
      {   e.preventDefault();
      $('.english_tab').trigger('click');  
      }
      if($.trim(aboutVideoAr)== '' )
      {   e.preventDefault();
      $('.arabic_tab').trigger('click');
      }


      @endif

 });

 
$('body').on('change', '.info-file', function() {  
        var fileUpload = $(this)[0];   
        var record = $(this);
       var getlab = $(this).data('lbl');            
                var reader = new FileReader();
                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                     var image = new Image();
                    image.src = e.target.result;
                    image.onload = function () {
                        var height = this.height;
                        var width = this.width;
                       if (height < 445 && width < 775)
                        {                            
                             @if($mer_selected_lang_code !='ar')
                             $('.pictureformat').html("Please upload image of minimum dimension 776px X 450px (width and height)");
                             @else
                              $('.pictureformat').html("يرجى تحميل صورة الحد الأدنى للبعد 776 بكسل × 450 بكسل (العرض والارتفاع)");
                             @endif
                              $('#'+getlab).html('');
                              record.val(''); 
                            //return false;
                        }

                        if (height > 505 || width > 805)
                        {
                        @if($mer_selected_lang_code !='ar')
                        $('.pictureformat').html("Please upload image of maximum dimension 800px X 500px (width and height)");
                        @else
                        $('.pictureformat').html("يرجى تحميل صورة بالحد الأقصى للبعد 800 بكسل × 500 بكسل (العرض والارتفاع)");
                        @endif
                        $('#'+getlab).html('');
                        record.val(''); 
                        }

                        //return true;
                    };
                }
           
    });
  @if($mer_selected_lang_code !='ar')
   $('.pictureformat').html("<div class='ex-show' style='color:#737373;'>Please upload image between 776px X 450px to 800px X 500px (Width X Height)</div");
    @else
  $('.pictureformat').html("<div class='ex-show' style='color:#737373;'>يرجى تحميل الصورة بين 776 بكسل × 450 بكسل إلى 800 بكسل × 500 بكسل (العرض × الارتفاع)</div");
      @endif
 });
  
</script>
@endif


<div class="profile-menu">&nbsp;</div>




 
<script type="text/javascript">
$('body').click(function() {
$("#navbarCollapse").removeClass('in');
$("#navbarCollapse").addClass('collapse');
$("#navbarCollapse").css('height','1px');

});

</script>

 <script>
	$( ".arabic_tab" ).click(function() {

	$( ".arabic" ).removeClass('ar');
	$( ".box" ).removeClass('arabic-ltr');
	$( ".box" ).addClass('rtl');
	$( ".english" ).addClass('ar');
	$( ".form_label" ).removeClass('ar');
	$( ".form_label" ).removeClass('english');
	$( ".form_label" ).addClass('arabic');
	$( ".english_tab" ).removeClass('active');
	$( ".arabic_tab" ).addClass('active');
	
<?php if($mer_selected_lang_code=='ar'){ ?>	 
 $( ".common_field" ).removeClass('ar');


<?php } else {?>
 $( ".common_field" ).addClass('ar');
<?php } ?>


	});


	$( ".english_tab" ).click(function() {
 
	$( ".english" ).removeClass('ar');
	$( ".box" ).addClass('arabic-ltr');
	$( ".box" ).removeClass('rtl');
	$( ".form_label" ).removeClass('ar');
	$( ".form_label" ).removeClass('arabic');
	$( ".form_label" ).addClass('english');
	$( ".arabic" ).addClass('ar');
	$( ".arabic_tab" ).removeClass('active');
	$( ".english_tab" ).addClass('active');
 <?php if($mer_selected_lang_code=='en'){ ?>	 
 $( ".common_field" ).removeClass('ar');
<?php } else {?>
 $( ".common_field" ).addClass('ar');
<?php } ?>	 

	});
	
	
	jQuery(".dropbtn").click(function(){
	jQuery(".dropdown-content").fadeIn(500);
	jQuery(".profile-menu").fadeIn(300);
})

jQuery(".profile-menu").click(function(){
	jQuery(".dropdown-content").fadeOut(500);
	jQuery(".profile-menu").fadeOut(300);
})

	
</script>

@if($mer_selected_lang_code !='en')
<script type="text/javascript">
 $(document).ready(function(){
 $( ".box" ).addClass('arabic-ltr');
 if($('.box').hasClass('commonbox'))
 {
 $( ".box" ).removeClass('arabic-ltr');
 }
if($('.box').hasClass('needrtl'))
 {
 $( ".box" ).addClass('rtl');

 }

 }) 
</script>
 
<script type="text/javascript">
 $(document).ready(function(){
 $( ".arabic_tab" ).addClass('active');
 $( ".english_tab" ).removeClass('active');
  $( ".arabic_tab" ).trigger('click');
 

 }) 
</script>
  
@endif


<script type="text/javascript">
 $(window).load(function(){
 if($('.box').hasClass('notab'))
 {
$( ".notab" ).removeClass('arabic-ltr');
 }
var checklist = $('body').find('.service_listingrow').length;
if( checklist >=1)
{
$( ".alert-info" ).addClass('alert-full');	
}
}) 
</script>
 
<script type="text/javascript">
function limitTextCount(limitField_id, limitCount_id, limitNum,chars)
{ 
    var limitField = document.getElementById(limitField_id);
    var limitCount = document.getElementById(limitCount_id);
    var fieldLEN = limitField.value.length;

    if (fieldLEN > limitNum)
    {
        limitField.value = limitField.value.substring(0, limitNum);
    }
    else
    {
        limitCount.innerHTML = (limitNum - fieldLEN) + ' '+chars;
    }
}
</script> 
 

   <!-- FOOTER -->
   {!! $merchantfooter !!}
   <!--END FOOTER -->
    <script>
    jQuery(function() {
 
    	jQuery('input[name=google_map_address]').on("focusout", function(e){   	 

            var url = jQuery('input[name=google_map_address]').val();
            var regex = new RegExp('@(.*),(.*),');
            var lon_lat_match = url.match(regex);
            var lon = lon_lat_match[1];
            var lat = lon_lat_match[2];

            jQuery('input[name=longitude]').val(lat);
            jQuery('input[name=latitude]').val(lon);
        });
    });
 

</script>

  <script>
    jQuery(function() {
 
    	jQuery('input[name=google_map_url]').on("focusout", function(e){   	 

            var url = jQuery('input[name=google_map_url]').val();
            var regex = new RegExp('@(.*),(.*),');
            var lon_lat_match = url.match(regex);
            var lon = lon_lat_match[1];
            var lat = lon_lat_match[2];

            jQuery('input[name=longitude]').val(lat);
            jQuery('input[name=latitude]').val(lon);
        });
    });
  jQuery(function() {
jQuery('input[name=coupon]').on("keyup", function(e){ 
  var str =  jQuery(this).val();
  jQuery('.couponvalidation').html('');
   jQuery("input[name=submit]").prop('disabled', false);
    jQuery.ajax({
        type: "GET",
        url: "{{ route('checkuniquecode') }}",
        data: {code:str},
        success: function(data) {
         if(data == 1)
         {      
<?php if($mer_selected_lang_code=='ar'){ ?>  
jQuery("input[name=coupon]").parent().after("<div class='couponvalidation' style='color:red;margin-bottom: 3px;'>يرجى إدخال رمز القسيمة الفريد</div>");
         <?php } else {?> 
jQuery("input[name=coupon]").parent().after("<div class='couponvalidation' style='color:red;margin-bottom: 3px;'>Please enter unique coupon code</div>");
         <?php } ?>     
 jQuery("input[name=submit]").prop('disabled','disabled');
         }
         else
         {
           //alert('all ok');   
         }
           
        }
    });
 

 });
    });

</script>
 
</body>

    <!-- END BODY -->
</html>

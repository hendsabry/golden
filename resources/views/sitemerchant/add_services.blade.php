<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>
    <meta charset="UTF-8" />
    <title>
        <?php echo $SITENAME; ?> | Add Services</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/theme.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link href="<?php echo url('')?>/public/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css'>
    <link rel='stylesheet prefetch' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'>
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/datepicker.css" />
    <?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); if(count($favi)>0) { foreach($favi as $fav) {} ?>
        <link rel="shortcut icon" href="<?php echo url(''); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
        <?php } ?>
            <!--END GLOBAL STYLES -->

            <!-- PAGE LEVEL STYLES -->
            <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
            <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/wysihtml5/dist/bootstrap-wysihtml5-0.0.2.css" />
            <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/Markdown.Editor.hack.css" />
            <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/CLEditor1_4_3/jquery.cleditor.css" />
            <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/jquery.cleditor-hack.css" />
            <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/bootstrap-wysihtml5-hack.css" />
            <style>
                ul.wysihtml5-toolbar > li {
                    position: relative;
                }
            </style>
            <!--END GLOBAL STYLES -->
            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="padTop53 ">

    <!-- MAIN WRAPPER -->
    <div id="wrap">

        <!-- HEADER SECTION -->
        {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

        <div></div>

        <!--PAGE CONTENT -->
        <div id="content">

            <div class="inner">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="breadcrumb">
                            <li class=""><a>Home</a></li>
                            <li class="active"><a>Add Services</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box dark">
                            <header>
                                <div class="icons"><i class="icon-edit"></i></div>
                                <h5>Add Services</h5>

                            </header>
							
							
							

                            @if (Session::has('message'))
                            <p style="background-color:green;color:#fff;">{!! Session::get('message') !!}</p>
                            @endif
							
							
							
							
                           <div class="row">
							<div id="div-1" class="accordion-body collapse in body col-lg-11 panel_marg" style="padding-bottom:10px;">
                                {!! Form::open(array('url'=>'mer_add_service_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data')) !!}
                                <div id="error_msg" style="color:#F00;font-weight:800"> </div>
								
								
								 <div class="panel panel-default">

                                            <div class="panel-heading"> Add Services </div>
											
										<div class="panel-body">				
                                <div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Service Name<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <input id="service_name" placeholder="Enter Service Name" name="service_name" class="form-control" type="text" value="{!!Input::old('service_name')!!}" required>
										 @if ($errors->has('service_name'))
                                <p class="error-block" style="color:red;">{{ $errors->first('service_name') }}</p> @endif
										
                                    </div>
                                </div>
								</div>
																
                               	<div class="panel-body">
                                <div class="form-group">
                                    <label for="pass1" class="control-label col-lg-3">Service Type<span class="text-sub"></span></label>

                                    <div class="col-lg-4">
                                        <select class="form-control" id="service_type" name="service_type" onChange="get_maincategory(this.value)" required>
                                            <option value="">--Select Service Type--</option>
                                            <?php foreach($getservicetypes as $service_type)  { ?>
                                                <option value="<?php echo $service_type->service_type_id; ?>">
                                                    <?php echo $service_type->service_type_name; ?>
                                                </option>
                                                <?php } ?>
                                        </select>
										<p class="error-block" style="color:red;">
									@if ($errors->has('service_type')) {{ $errors->first('service_type') }} @endif </p>
                                    </div>
                                    <label for="text2">  More Custom Service Type  <a href="<?php echo url('')?>/mer_manage_service_type"> ADD</a></label>
                                </div>
                                </div>
							
								<div class="panel-body" id="img_upload">
														<div class="form-group">
															<label for="text2" class="control-label col-lg-3">Service Timing<span class="text-sub">*</span></label>
															<div class="col-lg-4">
																<input id="service_timing" placeholder="Enter Service Timing" name="service_timing[]" class="form-control" type="number" value="" required>
															</div>
                                                            <div class="col-lg-3">
																<select class="form-control" name="duration[]" id="duration">
                                                                    <option value="1" @if(old('duration')== '1') selected @endif>Minutes</option>
                                                                    <option value="2" @if(old('duration')== '2') selected @endif>Hours</option>
                                                                    <option value="3" @if(old('duration')== '3') selected @endif>Days</option>
                                                                    <option value="4" @if(old('duration')== '4') selected @endif>Weeks</option>
                                                                    <option value="5" @if(old('duration')== '5') selected @endif>Month</option>
                                                                    <option value="6" @if(old('duration')== '6') selected @endif>Year</option>
                                                                </select>
															</div>
                                                            <p class="error-block" style="color:red;">@if ($errors->has('duration')) {{ $errors->first('duration') }} @endif </p>
                                                            <a href="javascript:void(0);"  title="Add field"  id="add_button" class="btn btn-xs btn-warning addService"><span class="glyphicon glyphicon-plus" ></span></a>
														</div>
														
                                <div class="">
								<div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Original Price<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <input placeholder="Numbers Only" class="form-control" type="number" id="Original_Price" name="Original_Price[]" value="" required >
										<p class="error-block" style="color:red;">
									@if ($errors->has('Original_Price')) {{ $errors->first('Original_Price') }} @endif </p>
                                    </div>
                                </div></div>
								
								<div class="">
                                <div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Discounted Price<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <input placeholder="Numbers Only" class="form-control" type="number" id="Discounted_Price" name="Discounted_Price[]" value="" required>
										<p class="error-block" style="color:red;">
									@if ($errors->has('Discounted_Price')) {{ $errors->first('Discounted_Price') }} @endif </p>
                                    </div>
                                </div></div>
								</div>
								
								
									<div class="panel-body">
                                <div class="form-group">

                                    <label for="text2" class="control-label col-lg-3">Calendar Option<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
									
									<label class="sample checkbox-inline" id="chk_radio">
                                        <input type="radio" id="calendar_option" name="calendar_option" onClick="setVisibility('sub4', 'block');" value="1" @if(old('calendar_option')== '1') checked @endif onChange="setshipVisibility('showship2','block');" required>
                                        <span>Yes</span></label>
										
										<label class="sample checkbox-inline" id="chk_radio">
                                        <input type="radio" name="calendar_option" id="calendar_option" onChange="setshipVisibility('showship2','block');" value="2" @if(old('calendar_option')== '2') checked @endif required>
                                        <span>No</span> </label>
                                        
											<p class="error-block" style="color:red;">
									@if ($errors->has('calendar_option')) {{ $errors->first('calendar_option') }} @endif </p>
                                    </div>
                                </div></div>
							
                                <!--<div class="panel-body" id="showship" style="display:none;">
                                    <div class="form-group">
                                        <label for="text1" class="control-label col-lg-2">Start Date<span class="text-sub">*</span></label>

                                        <div id="pageContentArea" class="pageSection">

                                            <input type="text" id="txtDateRange" name="date" class="inputField shortInputField dateRangeField" placeholder="Select a Date" data-from-field="txtDateFrom" data-to-field="txtDateTo" />
                                            <input type="hidden" id="txtDateFrom" value="" />
                                            <input type="hidden" id="txtDateTo" value="" />

                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body" id="showship1" style="display:none;">
									<div class="form-group">
										<div class="col-sm-6">
											<input type="button" class="my_button" name="buttonName" value="ADD" >
											
										</div>
									</div>
                                </div>-->
									
                                <!--<div class="panel-body" id="showship2" 
                                 @if(old('calendar_option')== '1') style='display:block;' 
                                @elseif (old('calendar_option')== '2') style='display:none;' 
                                @elseif (old('calendar_option')== '') style='display:none;' 
                                @endif
                                >
                                    <div class="form-group">
                                        <label class="control-label col-lg-3" for="text1">Select Time<span class="text-sub">*</span></label>
                                        <div class="col-lg-4">
                                            <?php foreach($get_service_time as $service_time){ ?>
                                                <div class="col-sm-4" style="padding-left:0;">
                                                    <label class="sample checkbox-inline">
                                                        <input type="checkbox" name="service_time[]" value="{{$service_time->service_time_id}}"  @if(old('service_time[]')) checked @endif 
                                                        >
                                                        <span>{{$service_time->service_time}}</span>
                                                    </label>
                                                </div>
                                                <?php } ?>   
                                        </div>
                                    </div>
                                </div>-->
								
								
								
									<div class="panel-body">
                                <div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Select Store<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <select class="form-control" id="store_name" name="store_name" onChange="get_maincategory(this.value)" required>
                                            <option value="{!!Input::old('store_name')!!}">Select Store</option>
                                            <?php foreach($getstore as $store)  { ?>
                                                <option value="<?php echo $store->stor_id; ?>">
                                                    <?php echo $store->stor_name; ?>
                                                </option>
                                                <?php } ?>
                                        </select>
										
										<p class="error-block" style="color:red;">
									@if ($errors->has('store_name')) {{ $errors->first('store_name') }} @endif </p>
                                    </div>
                                </div></div>
								
									
									<div class="panel-body">			
                                <div class="form-group">
                                    <label for="text1" class="control-label col-lg-3">Meta keywords<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <textarea class="form-control" id="Meta_Keywords" name="Meta_Keywords" placeholder="Enter Keyword" value="{!!Input::old('Meta_Keywords')!!}" required></textarea>
										<p class="error-block" style="color:red;">
									@if ($errors->has('Meta_Keywords')) {{ $errors->first('Meta_Keywords') }} @endif </p>
                                    </div>
                                </div></div>
								
								<div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="text1">Meta description<span class="text-sub">*</span></label>

                                    <div class="col-lg-4">
                                        <textarea class="form-control" id="Meta_Description" name="Meta_Description" placeholder="Enter Description" value="{!!Input::old('Meta_Description')!!}" required></textarea>
										<p class="error-block" style="color:red;">
									@if ($errors->has('Meta_Description')) {{ $errors->first('Meta_Description') }} @endif </p>
                                    </div>
                                </div></div>
								
								
									<div class="panel-body">
                                <div class="form-group">
                                    <label for="pass1" class="control-label col-lg-3"><span class="text-sub"></span></label>

                                    <div class="col-lg-4">
                                        <button class="btn btn-success btn-sm btn-grad" id="submit_product"><a style="color:#fff">Add Services</a></button>
                                        <button type="reset" class="btn btn-default btn-sm btn-grad" style="color:#000">Reset</button>
                                    </div>
                                </div></div>

                            </div>
                            <div class="yourDialog">

                            </div> </div>
                            </form> </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!--END PAGE CONTENT -->

    </div>

    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    {!! $adminfooter !!}
    <!--END FOOTER -->

    <script src="<?php echo url('')?>/public/assets/plugins/jquery-2.0.3.min.js"></script>

    <script type="text/javascript">
        function addproductimageFormField() {
            var id = document.getElementById("aid").value;
            var count_id = document.getElementById("count").value;
            if (count_id < 4) {
                document.getElementById('count').value = parseInt(count_id) + 1;
                var count_id_new = document.getElementById("count").value;
                jQuery.noConflict()
                jQuery("#divTxt").append("<div id='row" + count_id + "' style='width:100%'><input type='file' name='file_more" + count_id + "' id='file" + count_id_new + "'  />&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + count_id + "\"); return false;' style='color:#F60;' >Remove</a></div>");
                jQuery('#row' + id).highlightFade({
                    speed: 1000
                });
                id = (id - 1) + 2;
                document.getElementById("aid").value = id;
            }
        }

        function removeFormField(id) {
            //alert(id);
            var count_id = document.getElementById("count").value;
            document.getElementById('count').value = parseInt(count_id) - 1;
            jQuery(id).remove();
        }
    </script>

    <script>
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;

        }

        function getshop_ajax(id) {

            var passmerchantid = 'id=' + id;
            $.ajax({
                type: 'get',
                data: passmerchantid,
                url: 'product_getmerchantshop',
                success: function(responseText) {
                    if (responseText) {
                        $('#Select_Shop').html(responseText);
                    }
                }
            });
        }

        function getcolorname(id) {

            var passcolorid = 'id=' + id + "&co_text_box=" + $('#co').val();
            $.ajax({
                type: 'get',
                data: passcolorid,
                url: 'product_getcolor',
                success: function(responseText) {
                    if (responseText) {
                        // alert(responseText)
                        var get_result = responseText.split(',');
                        if (get_result[3] == "success") {
                            $('#colordiv').css('display', 'block');

                            $('#showcolor').append('<span style="width:195px; display:inline-block;padding:10px;border:4px solid ' + get_result[2] + ';margin:5px 0px; display:inline-table">' + get_result[0] + '<input type="checkbox"  name="colorcheckbox' + get_result[1] + '"style="padding-left:30px;" checked="checked" value="1" ></span>&nbsp;&nbsp;');

                            var co_name_js = $('#co').val();
                            $('#co').val(get_result[1] + "," + co_name_js);

                        } else if (get_result[3] == "failed") {
                            alert("Already color is choosed.");
                        } else {
                            alert("something went wrong .");
                        }

                    }
                }
            });

        }

        function getsizename(id) {
            var passsizeid = 'id=' + id + "&si_text_box=" + $('#si').val();
            $.ajax({
                type: 'get',
                data: passsizeid,
                url: 'product_getsize',
                success: function(responseText) {
                    if (responseText) { //alert(responseText);
                        var get_result = responseText.split(',');
                        if (get_result[3] == "success") {
                            var count = parseInt($('#productsizecount').val()) + 1;
                            $("#productsizecount").val(count);
                            $('#sizediv').css('display', 'block');
                            //$('#quantitydiv').css('display', 'block'); 

                            $('#showsize').append('<span style="padding-right:0px; width:150px;display:inline-block;">Select Size:<span style="color:#ff0099;padding-left:5px">' + get_result[2] + '<input type="checkbox"  name="sizecheckbox' + get_result[1] + '"style="padding-left:30px;" checked="checked" value="1" ></span></span>');
                            $('#showquantity').append('<input type="text" name="quantity' + get_result[1] + '" value="1" style="padding:10px;border:5px solid gray;margin:0px;" onkeypress="return isNumberKey(event)" ></input> ');

                            var co_name_js = $('#si').val();
                            $('#si').val(get_result[1] + "," + co_name_js);

                            //alert($('#si').val());
                        } else if (get_result[3] == "failed") {
                            alert("Already size is choosed.");
                        } else {
                            alert("something went wrong .");
                        }

                    }
                }
            });

        }

        function get_maincategory(id) {
            var passcategoryid = 'id=' + id;
            $.ajax({
                type: 'get',
                data: passcategoryid,
                url: 'product_getmaincategory',
                success: function(responseText) {
                    if (responseText) {
                        $('#Product_MainCategory').html(responseText);
                    }
                }
            });
        }

        function get_subcategory(id) {
            var passsubcategoryid = 'id=' + id;
            $.ajax({
                type: 'get',
                data: passsubcategoryid,
                url: 'product_getsubcategory',
                success: function(responseText) {
                    if (responseText) {
                        $('#Product_SubCategory').html(responseText);
                    }
                }
            });
        }

        function get_second_subcategory(id) {
            var passsecondsubcategoryid = 'id=' + id;
            $.ajax({
                type: 'get',
                data: passsecondsubcategoryid,
                url: 'product_getsecondsubcategory',
                success: function(responseText) {
                    if (responseText) {
                        $('#Product_SecondSubCategory').html(responseText);
                    }
                }
            });
        }

        $(document).ready(function() {

                    $('#specificationdetails').hide();
                    var title = $('#Product_Title');
                    var category = $('#Product_Category');
                    var maincategory = $('#Product_MainCategory');
                    var subcategory = $('#Product_SubCategory');
                    var secondsubcategory = $('#Product_SecondSubCategory');
                    var originalprice = $('#Original_Price');
                    var inctax = $('#inctax');
                    var discountprice = $('#Discounted_Price');
                    var shippingamt = $('#Shipping_Amount');
                    var description = $('#Description');
                    var wysihtml5 = $('#wysihtml5');
                    var merchant = $('#Select_Merchant');
                    var shop = $('#Select_Shop');

                    var metakeyword = $('#Meta_Keywords');
                    var metadescription = $('#Meta_Description');
                    var file = $('#file0');
                    var pquantity = $('#Quantity_Product');

                    //   var countryid=$("#selectcountry1 option:selected").val();
    </script>

    <script src="<?php echo url('')?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('')?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS

<!-- Date Picker-->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>

    <script>
        (function($) {
            function compareDates(startDate, endDate, format) {
                var temp, dateStart, dateEnd;
                try {
                    dateStart = $.datepicker.parseDate(format, startDate);
                    dateEnd = $.datepicker.parseDate(format, endDate);
                    if (dateEnd < dateStart) {
                        temp = startDate;
                        startDate = endDate;
                        endDate = temp;
                    }
                } catch (ex) {}
                return {
                    start: startDate,
                    end: endDate
                };
            }

            $.fn.dateRangePicker = function(options) {
                options = $.extend({
                    "changeMonth": false,
                    "changeYear": false,
                    "numberOfMonths": 1,
                    "rangeSeparator": " - ",
                    "useHiddenAltFields": false

                }, options || {});

                var myDateRangeTarget = $(this);
                var onSelect = options.onSelect || $.noop;
                var onClose = options.onClose || $.noop;
                var beforeShow = options.beforeShow || $.noop;
                var beforeShowDay = options.beforeShowDay;
                var lastDateRange;

                function storePreviousDateRange(dateText, dateFormat) {
                    var start, end;
                    dateText = dateText.split(options.rangeSeparator);
                    if (dateText.length > 0) {
                        start = $.datepicker.parseDate(dateFormat, dateText[0]);
                        if (dateText.length > 1) {
                            end = $.datepicker.parseDate(dateFormat, dateText[1]);
                        }
                        lastDateRange = {
                            start: start,
                            end: end
                        };
                    } else {
                        lastDateRange = null;
                    }
                }

                options.beforeShow = function(input, inst) {
                    var dateFormat = myDateRangeTarget.datepicker("option", "dateFormat");
                    storePreviousDateRange($(input).val(), dateFormat);
                    beforeShow.apply(myDateRangeTarget, arguments);
                };

                options.beforeShowDay = function(date) {
                    var out = [true, ""],
                        extraOut;
                    if (lastDateRange && lastDateRange.start <= date) {
                        if (lastDateRange.end && date <= lastDateRange.end) {
                            out[1] = "ui-datepicker-range";
                        }
                    }

                    if (beforeShowDay) {
                        extraOut = beforeShowDay.apply(myDateRangeTarget, arguments);
                        out[0] = out[0] && extraOut[0];
                        out[1] = out[1] + " " + extraOut[1];
                        out[2] = extraOut[2];
                    }
                    return out;
                };

                options.onSelect = function(dateText, inst) {
                    var textStart;
                    if (!inst.rangeStart) {
                        inst.inline = true;
                        inst.rangeStart = dateText;
                    } else {
                        inst.inline = false;
                        textStart = inst.rangeStart;
                        if (textStart !== dateText) {
                            var dateFormat = myDateRangeTarget.datepicker("option", "dateFormat");
                            var dateRange = compareDates(textStart, dateText, dateFormat);
                            myDateRangeTarget.val(dateRange.start + options.rangeSeparator + dateRange.end);
                            inst.rangeStart = null;
                            if (options.useHiddenAltFields) {
                                var myToField = myDateRangeTarget.attr("data-to-field");
                                var myFromField = myDateRangeTarget.attr("data-from-field");
                                $("#" + myFromField).val(dateRange.start);
                                $("#" + myToField).val(dateRange.end);
                            }
                        }
                    }
                    onSelect.apply(myDateRangeTarget, arguments);
                };

                options.onClose = function(dateText, inst) {
                    inst.rangeStart = null;
                    inst.inline = false;
                    onClose.apply(myDateRangeTarget, arguments);
                };

                return this.each(function() {
                    if (myDateRangeTarget.is("input")) {
                        myDateRangeTarget.datepicker(options);
                    }
                    myDateRangeTarget.wrap("<div class=\"dateRangeWrapper\"></div>");
                });
            };
        }(jQuery));

        $(document).ready(function() {
            $("#txtDateRange").dateRangePicker({

                showOn: "focus",
                rangeSeparator: " to ",
                dateFormat: "dd-mm-yy",
                useHiddenAltFields: true,
                minDate: 'dateToday',
                constrainInput: true
            });
        });
    </script>
    <script>
        $("#type").click(function() {

            $(".yourDialog").dialog();

        });
    </script>
    <!--text box for Calender-->
    <script language="JavaScript">
        function setshipVisibility(id,visibility) {
			
            var calenter_id = $('input[name="calendar_option"]:checked').val();

            if (calenter_id == 1) {
                document.getElementById(id).style.display = visibility;
                document.getElementById(id1).style.display = visibility;
                document.getElementById(id2).style.display = visibility;
            } else if (calenter_id == 2) {
                document.getElementById(id).style.display = 'none';
                document.getElementById(id1).style.display = 'none';
                document.getElementById(id2).style.display = 'none';
            }
        };
    </script>
	
	<script>
	$(".my_button").click(function(e) {
		
    var date = $('#txtDateRange').val();
	alert(date);
	$.ajax(
	{
	url: "<?php echo URL::to('/'); ?>/add_service_date_ajax",
	type: 'get',
	data: {date:date},			
	success: function(data)
	{ 
		alert(data);
	 //$("#location_ajax").html(data);
		
	   }});
	});
	</script>
	<script src="<?php echo url('')?>/public/assets/plugins/jquery-2.0.3.min.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
					    var maxField = 100; //Input fields increment limitation
					    var addButton = $('#add_button'); //Add button selector
					    var wrapper = $('#img_upload'); //Input field wrapper //div
					    var fieldHTML = '<div><div class="" id="img_upload"><div class="form-group"><label for="text2" class="control-label col-lg-3">Service Timing<span class="text-sub">*</span></label><div class="col-lg-4" ><input id="service_timing" placeholder="Enter Service Timing" name="service_timing[]" class="form-control" type="text" value="" required></div><div class="col-lg-3"><select class="form-control" name="duration[]" id="duration"><option value="1">Minutes</option><option value="2">Hours</option><option value="3">Days</option><option value="4">Weeks</option><option value="5">Month</option><option value="6">Year</option></select></div></div><div class=""><div class="form-group"><label for="text1" class="control-label col-lg-3">Original Price<span class="text-sub">*</span></label><div class="col-lg-4"><input placeholder="Numbers Only" class="form-control" type="text" id="Original_Price" name="Original_Price[]" value="" required ><p class="error-block" style="color:red;">@if ($errors->has("Original_Price")) {{ $errors->first("Original_Price") }} @endif </p></div></div></div><div class=""><div class="form-group"><label for="text1" class="control-label col-lg-3">Discounted Price<span class="text-sub">*</span></label> <div class="col-lg-4"><input placeholder="Numbers Only" class="form-control" type="text" id="Discounted_Price" name="Discounted_Price[]" value="" required><p class="error-block" style="color:red;">@if ($errors->has("Discounted_Price")) {{ $errors->first("Discounted_Price") }} @endif </p></div><div id="remove_button"><a href="javascript:void(0);"  title="Remove field" class="btn btn-xs btn-info removeDiv"><span class="glyphicon glyphicon-minus"></span></a></div></div></div></div>'; //New input field html 
					    var x = 1; //Initial field counter is 1
					    $(addButton).click(function(){ //Once add button is clicked
					        if(x < maxField){ //Check maximum number of input fields
					            x++; //Increment field counter
					            $(wrapper).append(fieldHTML); 
					            
					        }
					    });
					    $(wrapper).on('click', '#remove_button', function(e){ //Once remove button is clicked
					        e.preventDefault();
					        $(this).parent().parent().parent('div').remove(); //Remove field html
					        x--; //Decrement field counter
					       // document.getElementById('count').value = parseInt(count_id)-1;
					    });
					});
					 
				</script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
<!-- END BODY -->

</html>
@include('sitemerchant.includes.header')  
@php $reception_hospitality_leftmenu =1; @endphp 
<!--start merachant-->
<div class="merchant_vendor"> 
  <!--left panel--> 
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left') 
  <!--left panel end--> 
  
  <!--right panel-->
  <div class=" right_panel" >
    <div class="inner"> 
      <!--haeding panel-->
      <div class="service_listingrow"> 
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </h5>
    </div> <!-- service_listingrow -->


@php $ids = request()->id; @endphp
 {!! Form::open(array('url'=>"reception-list-orders/$ids",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
 <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left">
        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems ">
            <select name="status" id="status">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
              <option value="1" @if( isset($status) && $status==1) selected="selected" @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif</option>

              <option value="0" @if( isset($status) &&  $status=='0' ) selected="selected" @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_INACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_INACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_INACTIVE') }} @endif</option>
            </select>
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
          
        </div>
        <div class="search_box">
        <div class="search_filter">&nbsp;</div>
        <div class="filter_right">  
 
          <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{ $search or ''}}" />
          <input type="button" class="icon_sch" id="submitdata" />
        </div>
        </div>
      </div>
      <!-- filter_area --> 
      
      {!! Form::close() !!}

      <!--haeding panel end-->
      <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition --> 
      <div class="global_area"><!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap"> 
              
              
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <div class="panel-body panel panel-default"> @if(isset($alldata) !='')
                <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
                @else
                <div class="table merchant-order-table">
                  <div class="tr">
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERMOBILE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDERSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDERSTATUS') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_AMOUNT') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_AMOUNT') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                  </div>





  <div class="tr">
                     <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif">Arun</div>
                     <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERMOBILE') }} @endif">456456545</div>
                     <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDERSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDERSTATUS') }} @endif">Pending</div>
                     <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE') }} @endif">6Apr,2019</div>
                     <div class="td td5" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_AMOUNT') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_AMOUNT') }} @endif">799</div>
                     <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS') }} @endif">Pending</div>
                        <div class="td td7" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif"><span class="status_active2 cstatus" data-status="" >{{ $status or '' }}</span></div>
                     
                 </div>







                </div>
                @endif </div>
            </div>
          </div>
        </div>
        <!--global end--> 
        
      </div>
    </div>
  </div>
  <!--end right panel--> 
  
</div>
<!--end merachant--> 

@include('sitemerchant.includes.footer')
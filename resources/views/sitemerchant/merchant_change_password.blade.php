﻿ @include('sitemerchant.includes.header')  
@php $hall_leftmenu =1; @endphp 

       <div class="merchant_vendor"> 


         <!-- HEADER SECTION -->

        <!-- END HEADER SECTION -->
      <div class="profile_box">

         <!--PAGE CONTENT -->
        <div class="profile_area">
           
                <div class="inner">
                    
            <div class="row change_psw_page">
<div class="col-lg-12">
<header>
           <h5 class="global_head">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')!= '')   ? trans(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_CHANGE_PASSWORD') }}
        
            
        </header>

 @if (Session::has('pwd_error'))

<div class="error">{!! Session::get('pwd_error') !!}</div>
 
		@endif
 @if (Session::has('success'))

 <div class="alert alert-info">{{ Session::get('success') }}</div>
	 
		@endif
    <div class="box">
        


	 





        <div id="div-1" class="global_area">
  {!! Form::open(array('url'=>'change_password_submit','class'=>'form-horizontal', 'id'=>'forgotpass')) !!}   
	<div id="error_msg" class="passerror"> </div>        


		<input type="hidden" value="{{ $mer_id }}" name="merchant_id" id="merchant_id">
 
                <div class="form_row">
			<div class="form_row_left">
                    <label class="form_label">{{ (Lang::has(Session::get('mer_lang_file').'.MER_OLD_PASSWORD')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_OLD_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_OLD_PASSWORD') }}</label>

                    <div class="info100">
					{{ Form::password('oldpwd', array('class' => 'pass_input','id' => 'oldpwd')) }}
                       
                    </div>
					</div>
					
                </div>
				
                <div class="form_row">
                <div class="form_row_left">
					 <label class="form_label"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_NEW_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_NEW_PASSWORD') :  trans($MER_OUR_LANGUAGE.'.MER_NEW_PASSWORD') }}</label>
					   <div class="info100">
					{{ Form::password('pwd', array('class' => 'pass_input','id' => 'pwd')) }}
                      
                    </div>
					
					</div>
				</div>
                <div class="form_row">
				<div class="form_row_left">
                    <label for="text2" class="form_label">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD')  : trans($MER_OUR_LANGUAGE.'.MER_CONFIRM_PASSWORD')}}</label>

                    <div class="info100">
					{{ Form::password('confirmpwd', array('class' => 'pass_input','id' => 'confirmpwd')) }}
                        
                    </div>
					</div>
                </div>
				<div class="form_row">
				{!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'form_label', 'for' => 'pass1'])) !!}
                   

                    <div class="info100">
                     <div class="form_row_left">
                     <button id="updatepwd"   class="form_button" style="color:#fff"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE'): trans($MER_OUR_LANGUAGE.'.MER_UPDATE')}}</button>
                    </div>
                   
                    </div>
					  
                </div>

                
				{{ Form::close() }}
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 </div>   </div>
        </div>
    

<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>   

<script type="text/javascript">
  
$("#forgotpass").validate({
                  ignore: [],
                  rules: {
                  oldpwd: {
                       required: true,
                      },

                       pwd: {
                       required: true,
                      },

                       confirmpwd: {
                       required: true,
                      },
 
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             








           messages: {
             oldpwd: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_OLD_PASSWORD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_OLD_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_OLD_PASSWORD') }} @endif",
                      },  

                 pwd: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_NEW_PASSWORD') }} @endif",
                      },  

                         confirmpwd: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD') }} @endif",
                      },   
                    
                     
                },

                invalidHandler: function(e, validation){
                     

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>

@include('sitemerchant.includes.footer')
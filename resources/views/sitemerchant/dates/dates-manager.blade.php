@include('sitemerchant.includes.header') 
@php $dates_inner_leftmenu =1; @endphp
<div class="merchant_vendor">
<div class="breadcrumb"><a href="#">Home</a> <span class="breadcrumb-divder">&gt;</span><span>Buffet Resturant</span></div> <!-- breadcrumb -->
 @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_LISTMANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LISTMANAGER') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LISTMANAGER') }} @endif </h5>
        <div class="add"><a  href="{{ route('dessert-add-manager',['id' => request()->id]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_MANAGER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_MANAGER') }} @endif</a> </div>
        </div> <!-- service_listingrow -->
        {!! Form::open(array('url'=>"listmanager",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
        <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
        <div class="filter_area">
          <div class="filter_left">
          <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
            <div class="search-box-field mems "> @php $statuss = request()->status; $searchh= request()->search; @endphp
              <select name="status" id="status">
                <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
                <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
                <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
              </select>
              <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
            </div>
          </div>
          <div class="search_box">
        <div class="search_filter">&nbsp;</div>
          <div class="filter_right">
            <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$searchh or ''}}" />
            <input type="button" class="icon_sch" id="submitdata" />
          </div>
          </div><!-- filter_area --> 
        </div>
        <!-- filter_area --> 
        {!! Form::close() !!} 
        <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="table_wrap"> 
            
            
            <div class="panel-body panel panel-default">
             
              <div class="table dessert-manager-table">
                <div class="tr">
                  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '') {{ trans(Session::get('mer_lang_file').'.MER_NAME')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_NAME')}} @endif</div>
                  
                  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Email_Address')!= '') {{ trans(Session::get('mer_lang_file').'.MER_Email_Address')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Email_Address')}} @endif</div>
                  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif</div> 

                  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_User_Image')!= '') {{ trans(Session::get('mer_lang_file').'.MER_User_Image')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_User_Image')}} @endif</div>
                  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.STATUS')!= '') {{ trans(Session::get('mer_lang_file').'.STATUS')}}  @else {{ trans($MER_OUR_LANGUAGE.'.STATUS')}} @endif</div>
                  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTION')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION')}} @endif</div>
                </div>
               
                <div class="tr">
                  <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_FIRST_NAME')!= '') {{ trans(Session::get('mer_lang_file').'.MER_FIRST_NAME')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_FIRST_NAME')}} @endif">Mohan gulam</div>
                
                  <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Email_Address')!= '') {{ trans(Session::get('mer_lang_file').'.MER_Email_Address')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_Email_Address')}} @endif">arlusdfsdn@wisitech.com</div>
				   <div class="td td3" data-title="Last Modified Date">May 17, 2018</div>
				  <div class="td td4" data-title="Image"><img src="{{url('')}}/public/assets/img/dish.jpg" alt="" /></div>
				  <div class="td td5" data-title="Status"><span data-id="82" data-status="Active" class="status_active  status_active2 cstatus"> Active </span></div>
				  <div class="td td6" data-title="Action"><a href="#"> Edit </a></div>
                  
                
                  
                </div>
                </div>
           
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
           </div>
          <!-- table_wrap --> 
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel --> 
  
</div>
<!-- merchant_vendor -->

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div>
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'merchantlist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 

<script type="text/javascript">
      
          $(document).ready(function(){
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

</script> 
@include('sitemerchant.includes.footer')
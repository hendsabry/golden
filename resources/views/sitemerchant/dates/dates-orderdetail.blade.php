@include('sitemerchant.includes.header')  
@php $dates_inner_leftmenu =1; @endphp 
 
<!--start merachant-->
<div class="merchant_vendor">
  @php

$id = request()->id;
$sid = request()->sid;
 $opid = request()->opid;
$cusid = request()->cusid;
$oid = request()->oid;
 $hid = request()->hid;

 $ordid     = $oid;
$getCustomer = Helper::getuserinfo($cusid); 
$shipaddress       = Helper::getgenralinfo($oid);
$cityrecord=Helper::getcity($shipaddress->shipping_city);

 

$getorderedproductdate=Helper::getorderedproduct($oid);
$getorderedproduct=Helper::getfoodorderedproducttotal($oid,'dates');

@endphp
  <!--left panel-->
 @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->


  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif
		<a href="{{url('/')}}/dates-order/{{$id}}/{{$sid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a>
		</h5>
        </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} @endphp <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" > @php 
                      $ordertime=strtotime($getorderedproductdate->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      @endphp </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
                  @php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!='COD'){ @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                  </div>
                    @php } @endphp
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                    <div class="info100"> {{ $getbody_measurement[0]->shippingMethod }} </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100"> SAR  {{ $getbody_measurement[0]->shippingPrice }}</div>
                    <!-- box -->
                  </div>

                     @php $getsearchedproduct = Helper::searchorderedDetails($oid);
 if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ @endphp
                    <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONDATE') }} @endif</label>
                    <div class="info100"> @php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                     echo date('d M Y', strtotime($getsearchedproduct->occasion_date));
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                  @php } @endphp



                </div>
                
                </div>
        
                @php if(count($getbody_measurement)>0){ @endphp
                
          
               
                @php $i=1; $basetotal = 0; @endphp
                
                @foreach($getbody_measurement as $buyproduct)
                @php 
                  $pid=$buyproduct->product_id;
                $getorderedproducttbl=Helper::getorderedfromproducttbl($pid);
                $productproductimage=Helper::getproductimage($pid);
                
                $getorderedproductprice=Helper::getorderedfromproductprice($pid,$oid);
  $basetotal            = ($basetotal+$getorderedproductprice->total_price);
                  if($buyproduct->attribute_id=='34'){ $orderedby='Kg';} else{  $orderedby='Piece';}
                 @endphp
                
                
                  <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                  <div class="style_area">
       <?php if($i==1){?><div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.PRODUCT_INFORMATION')!= '') {{  trans(Session::get('mer_lang_file').'.PRODUCT_INFORMATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PRODUCT_INFORMATION') }} @endif</div><?php } ?>

                    <div class="style_box_type">
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_DISH_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISH_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISH_NAME') }} @endif</div>
                          <div class="style_label_text"> {{$getorderedproducttbl}} </div>
                        </div>
                        <div class="style_right">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Dish_Image')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Dish_Image') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Dish_Image') }} @endif</div>
                          <div class="style_label_text"> @if($productproductimage->pro_Img)   
                           <img src="{{ $productproductimage->pro_Img }}" alt="" width="150">
                          @else
                           No image

                          @endif</div>
                        </div>
                      </div>
                      <div class="sts_box">
                        <div class="style_left">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_QUANTITY') }} @endif</div>
                          <div class="style_label_text"> {{ $buyproduct->order_option_value[0]->quantity or ''}} {{$orderedby}} </div>
                        </div>

                        <div class="style_right">
                          <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Total_Price')!= '') {{  trans(Session::get('mer_lang_file').'.Total_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Total_Price') }} @endif</div>
                          <div class="style_label_text">SAR {{ $getorderedproductprice->total_price }}</div>
                        </div>
                      </div>
                  
                    </div>
                  </div>
                  
                </div>




@php $i++; @endphp

                @endforeach
                
              
<div class="merchant-order-total-area">
                <div class="merchant-order-total-line">
				 {{ (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: &nbsp;
				 
                   @php 
                            $get_vat_ammount = Helper::calculatevat($ordid,$basetotal);
                              $totalamount=$basetotal+$get_vat_ammount;
                        @endphp
        @php if(isset($get_vat_ammount) && $get_vat_ammount!='')
         {
          echo 'SAR '.number_format($get_vat_ammount,2);
         }
         else
         {
          echo 'N/A';
         }
        @endphp
				  
				   </div>  <!-- merchant-order-total-line -->
               <div class="merchant-order-total-line">
			    {{ (Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($MER_OUR_LANGUAGE.'.TOTAL_PRICE')}}: &nbsp; 
			    
                <?php if(isset($totalamount) && $totalamount!=''){
         echo 'SAR '.number_format(($totalamount),2);
         } ?>  
				 
				 </div> <!-- merchant-order-total-line -->
              </div>
                @php } @endphp

                  



               

            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
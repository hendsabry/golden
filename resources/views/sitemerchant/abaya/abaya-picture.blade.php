@include('sitemerchant.includes.header')    
@php $Abayabig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Pictures_Video')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Pictures_Video') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_Pictures_Video') }} @endif </h5>
        @include('sitemerchant.includes.language')
      </header
    
    >
    </div>
    @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box commonbox"> 
            <form name="form1" method="post" id="form1" action="{{ route('abaya-picture-videourl') }}"" enctype="multipart/form-data">
             {{ csrf_field() }}
          <div class="hall-top-form common_field"> 
          <div class="hall-top-form-label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Upload_Picture')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Upload_Picture') }}  
                  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Upload_Picture') }} @endif </div>
              <div class="input-file-area">
                  <label for="company_logo">
                  <div class="file-btn-area">
                    <div id="file_value1" class="file-value"></div>
   <div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div>
                  </div>
                  </label>
                  <input id="company_logo" data-lbl="file_value1" name="image[]" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
                </div>

                <div id="img_upload"></div>
                @if ($errors->has('image'))
                <div class="error"> @if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_VALID_IMAGE') }} @endif </div>
                @endif
                
                <div class="img-upload-line">
                  <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
                  <span class="error pictureformat"></span>
                  @php $Count =  1;@endphp
                   @php if(count($getDb)>=1){ $Count =  count($getDb);}else { $Count = 1; } @endphp
                  <input type="hidden" name="parent_id" value="{{$parentid or ''}}">
                  <input type="hidden" name="itemid" value="{{$itemid or ''}}">
                  <input type="hidden" id="count" name="count" value="{{$Count}}">
                 
                </div>
                </div>
                
                
                <!-- img-upload-line --> 
                <div class="imgpics-area common_field"> @if($getDbC >=1) 
                
                @foreach($getDb as $vals)
                <div class="imgpics-box">
                  <div class="imgpics"><img src=" {{ $vals->image }}" ></div>
                  <div class="imgpics-delet">
                    <a data-status="Active" data-id="{{$vals->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>
                  </div>
                </div>
                @endforeach
                @endif </div>
            
               <div class="hall-top-form">
               <div class="hall-video-section common_field">

                <div class="hall-video-label posrel"><span class="arabic_txt">@if (Lang::has(Session::get('mer_lang_file').'.MER_HALL_UPLOADVIDEO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_HALL_UPLOADVIDEO') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_HALL_UPLOADVIDEO') }} @endif</span> <a href="javascript:void(0);" class="tooltip_area_wrap">
  <span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_youtube_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_youtube_add') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_youtube_add') }} @endif</span>
</a></div>
                <div class="hall-video-form">
                  <input type="url" name="youtubevideo" id="youtubevideo" value="{{$getVideos->mc_video_url or ''}}">
                   <div class="ex-show">Ex - https://youtu.be/IY9cIpidnOc</div>
                  @if ($errors->has('youtubevideo'))
                  <div class="error"> @if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_URL')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_URL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_URL') }} @endif </div>
                  @endif
                  
                <!-- hall-video-form -->
                <div class="clear"></div>
                 @if(isset($getVideos->mc_video_url) && $getVideos->mc_video_url!='')<div class="hall-video-area">
                  <iframe class="hall-video-ifrem" src="{{$getVideos->mc_video_url or ''}}"></iframe></div>
                  @endif 
                  </div>
                   
                <!-- hall-video-area --> 
              </div>
              <div class="hall-video-section marb0">
                  <div class="hall-video-label">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.mer_about_video'); @endphp</span>
                     <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_about_video'); @endphp </span>
                  </label>
                   </div>
                  <div class="hall-video-form">
                    <div class="english">
                      <textarea class="english" maxlength="500" name="about_video" id="about" rows="4" cols="50">{{$getVideos->mc_video_description or ''}} </textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea class="arabic ar" name="about_video_ar" maxlength="500" id="about_ar" rows="4" cols="50">{{$getVideos->mc_video_description_ar or ''}}</textarea>
                    </div>
                    
                  </div>
                </div>
              </div>

        
        
        		<div class="form_row">
                <div class="form_row_left">
        		<div class="english">
                  <input type="submit" name="submit" value="Submit">
                </div>
                <div class="arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div> 
                </div>
                </div>
     
     </div>
     </div>
     
      
      
      </div></div></div></div>
    
    
       {!! Form::close() !!}


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
<script type="text/javascript">
    $(document).ready(function(){
    var maxField = 9;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = @php echo $Count; @endphp;

    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div></div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;  
    document.getElementById('count').value = parseInt(x);
    });
    });
  </script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});

jQuery('body').on('change','.info-file', function(){
            $('.pictureformat').html("");


  var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
var gid = $(this).data('lbl');
 $('#'+gid).html('');
 $(this).val('');
$('.pictureformat').html("Only formats are allowed : 'jpeg', 'jpg', 'png', 'gif', 'bmp'");
return false;
        }

@php if($Count > 9){ @endphp;
 var gid = $(this).data('lbl');
$('#'+gid).html('');
$('.info-file').val('');
$('.pictureformat').html("You can only upload 10 images");

  @php } @endphp
});




</script>  


@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $Abayabig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
      <style type="text/css">
      .ms-drop ul li label input[type="checkbox"] {
      height: 20px;
      display: inline-block;
      width: 20px;
      position: relative;
      border: 1px solid red;
      opacity: 1;
      }
      </style>
      <link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />
 

  <div class="right_panel">
    <div class="inner">
      <header>
         @if(request()->autoid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.add_item')!= '') {{  trans(Session::get('mer_lang_file').'.add_item') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.add_item') }} @endif </h5>
          @else
          <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATE_ITEM')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_ITEM') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_ITEM') }} @endif </h5>

          

          @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->
            
                <form name="form1" id="addmanager" method="post" action="{{ route('store-abayaitem') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                 <!-- form_row -->
                    <div class="form_row common_field"> 
              <div class="form_row_left">
                    <label class="form_label">
                      <span class="english">  @php echo lang::get('mer_en_lang.SELECT_YOUR_CHOICE'); @endphp</span>
                     <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SELECT_YOUR_CHOICE'); @endphp </span>
                    </label>
              <div class="info100">
              <select class="small-sel" name="category" onchange="chceckCategory()" id="category">
                 <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>
                        @php $menu_name='Tailor'@endphp
                       @if($mer_selected_lang_code !='en')
                       @php $menu_name= 'تصميم كوشا الخاص بك'; @endphp
                        @endif

                         @php $menu_name_readyar='Ready Made'@endphp
                       @if($mer_selected_lang_code !='en')
                       @php $menu_name_readyar= 'جاهز الصنع كوشا' @endphp
                        @endif
                        
                  <option value="1" {{ isset($fetchfirstdata->parent_attribute_id) && $fetchfirstdata->parent_attribute_id =='1' ? 'selected' : '' }}>{{ $menu_name }}</option>
                  <option value="2" {{ isset($fetchfirstdata->parent_attribute_id) && $fetchfirstdata->parent_attribute_id =='2' ? 'selected' : '' }}>{{ $menu_name_readyar }}</option>
                
              </select>
              </div>
              </div>
              </div>

                  
                  <div class="form_row">
                  <div class="form_row_left ">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.mer_category_name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_category_name'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
                      <input type="text" name="name" id="name" value="{{ $fetchfirstdata->pro_title or '' }}"  class="english"  data-validation-length="max80" maxlength="140">
                    </div>
                    <div class="arabic ar">
                      <input id="ssb_name_ar" class="arabic ar" name="name_ar" id="name_ar" type="text" value="{{ $fetchfirstdata->pro_title_ar or '' }}"   maxlength="140" >
                    </div>
                  </div>
                  </div>
				   <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                      @if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!='')
                      <div class="form-upload-img">
                       <img src="{{ $fetchfirstdata->pro_Img or '' }}" >
                     </div>
                     @endif
                    </div>

  </div>
       
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->
					 
					
                  </div>
                  <!-- form_row -->
                  <div class="form_row">
				   <div class="form_row_left common_field">
                    <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="price" id="price" maxlength="15" value="{{ $fetchfirstdata->pro_price or '' }}"  required="" >
                      </div>
                    </div>
					</div>
          
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="{{$fetchfirstdata->pro_discount_percentage or ''}}" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>


 

<div class="form_row common_field">
                    <div class="form_row_left tailorcase"  @if(isset($fetchfirstdata->parent_attribute_id) && $fetchfirstdata->parent_attribute_id !='1' )  style="display: none;" @endif>
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.Choose_Fabrics'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Choose_Fabrics'); @endphp </span>
                    </label>
                    <div class="info100">
 
                      <select name="fabric[]" id="ms_size"  multiple="multiple">
                       
                    @if(count($productfabric) >=1) 

                    @php
                    $getArry= array(); 
                    if(count($Oval) >=1) 
                    {
                    foreach($Oval as $val)
                    {
                      array_push($getArry, $val['short_name']);
                    }
                    }

                    @endphp 

                    @foreach($productfabric as $fab)
 


                    <option value="{{$fab->id}}" <?php if(in_array($fab->id,$getArry)) { echo 'SELECTED'; } ?>>  @if($mer_selected_lang_code !='en') {{$fab->fabric_name_ar}} @else {{$fab->fabric_name }} @endif</option>
                    @endforeach                         
                     @endif
                     </select> 
                    </div>
                       <span for="fabric" generated="true" class="error" style="display: none;">Please choose fabric.</span>
                    </div>

<div class="form_row_left tailorcase"   @if(isset($fetchfirstdata->parent_attribute_id) && $fetchfirstdata->parent_attribute_id !='1' )  style="display: none;" @endif >
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.NO_OF_DAY'); @endphp</span>
                      <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.NO_OF_DAY'); @endphp </span>
                    </label>
<div class="info100"><input type="text" class="xs_small notzero" name="noofday" onkeypress="return isNumber(event)" maxlength="2" value="{{$fetchfirstdata->pro_no_of_purchase or ''}}" id="noofday" ></div>

                  </div>


				 

      

         <div class="form_row common_field  " id="fabrica"  @if(isset($fetchfirstdata->parent_attribute_id) && $fetchfirstdata->parent_attribute_id !='2' )  style="display: none;" @endif >
               
                   <div class="sizechartlabel">
                   <span class="english">@php echo lang::get('mer_en_lang.SIZECHART'); @endphp</span>
                 <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SIZECHART'); @endphp </span>
                   </div>
                     <!--label class="form_label dress_qunt"> 
                      <span class="english">@php echo lang::get('mer_en_lang.MER_QUANTITY'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_QUANTITY'); @endphp </span> 
                     </label-->
                  
                    @php $containername='title'@endphp
                   @if($mer_selected_lang_code !='en')
                    @php $containername= 'title_'.$mer_selected_lang_code; @endphp
                    @endif
                       
                       @foreach($Size as $key=> $val)
                      @if(isset($fetchfirstdata->pro_id) && $fetchfirstdata->pro_id!='') 
                      @php
                      $names = 'Size'.$val->name;
                      $quantityinstock=Helper::getquantity($names,$fetchfirstdata->pro_id);
                      if($val->name == '0' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_0  = $quantityinstock->value;
                      }
                      if($val->name == '2' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_2  = $quantityinstock->value;
                      }
                      if($val->name == '4' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_4  = $quantityinstock->value;
                      }
                      if($val->name == '6' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_6  = $quantityinstock->value;
                      }
                      if($val->name == '8' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_8  = $quantityinstock->value;
                      }
                      if($val->name == '10' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_10  = $quantityinstock->value;
                      }
                      if($val->name == '12' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_12  = $quantityinstock->value;
                      }
                      if($val->name == '14' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_14  = $quantityinstock->value;
                      }
                      if($val->name == '16' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_16  = $quantityinstock->value;
                      }
                      if($val->name == '18' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_18  = $quantityinstock->value;
                      }
                      if($val->name == '20' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_20  = $quantityinstock->value;
                      }
                      if($val->name == '22' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_22  = $quantityinstock->value;
                      }
                      if($val->name == '24' && isset($quantityinstock->value) && $quantityinstock->value!='')
                      {   
                      $quantityinstock_24  = $quantityinstock->value;
                      }
                      @endphp
                      @endif

                      <!--div class="container_row">                       
                      <div class="label_fl">{{$val->name or ''}}</div>
                      <div class="input_flr"><input type="text" value="{{ $quantityinstock->value or '' }}" onkeypress="return isNumber(event)" class="check notzero" name="mulquantity[]"  maxlength="40"></div>
                      <input type="hidden" name="size[]" value="{{ $val->name }}">                        
                      </div-->
                       @endforeach
                     
                
                        
				   
				  <div class="size-chart-table"><div class="table">
<div class="tr">
<div class="table_heading">US</div>
<div class="table_heading">UK</div>
<div class="table_heading">Germany</div>
<div class="table_heading">France</div>
<div class="table_heading">Italy</div>
<div class="table_heading">Kore</div>
<div class="table_heading">Qty</div>
</div> <!-- tr -->

<div class="tr">
<div class="td td1" data-title="US">0</div>
<div class="td td2" data-title="UK">4</div>                     
<div class="td td3" data-title="Germany">30</div>
<div class="td td4" data-title="France">32</div>   
<div class="td td5" data-title="Italy">36</div>   
<div class="td td6" data-title="Kore">44</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$quantityinstock_0 or ''}}" class="check notzero" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size0"></div>   
 </div> <!-- tr -->

<div class="tr">
<div class="td td1" data-title="US">2</div>
<div class="td td2" data-title="UK">6</div>                     
<div class="td td3" data-title="Germany">32</div>
<div class="td td4" data-title="France">34</div>   
<div class="td td5" data-title="Italy">38</div>   
<div class="td td6" data-title="Kore">44</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero" name="mulquantity[]" value="{{$quantityinstock_2 or ''}}" /><input type="hidden" name="size[]" value="Size2"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">4</div>
<div class="td td2" data-title="UK">8</div>                     
<div class="td td3" data-title="Germany">34</div>
<div class="td td4" data-title="France">36</div>   
<div class="td td5" data-title="Italy">40</div>   
<div class="td td6" data-title="Kore">55</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero" name="mulquantity[]" value="{{$quantityinstock_4 or ''}}"/><input type="hidden" name="size[]" value="Size4"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">6</div>
<div class="td td2" data-title="UK">10</div>                     
<div class="td td3" data-title="Germany">36</div>
<div class="td td4" data-title="France">38</div>   
<div class="td td5" data-title="Italy">42</div>   
<div class="td td6" data-title="Kore">55</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_6 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size6"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">8</div>
<div class="td td2" data-title="UK">12</div>                     
<div class="td td3" data-title="Germany">38</div>
<div class="td td4" data-title="France">40</div>   
<div class="td td5" data-title="Italy">44</div>   
<div class="td td6" data-title="Kore">66</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_8 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size8"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">10</div>
<div class="td td2" data-title="UK">14</div>                     
<div class="td td3" data-title="Germany">40</div>
<div class="td td4" data-title="France">42</div>   
<div class="td td5" data-title="Italy">46</div>   
<div class="td td6" data-title="Kore">66</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_10 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size10"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">12</div>
<div class="td td2" data-title="UK">16</div>                     
<div class="td td3" data-title="Germany">42</div>
<div class="td td4" data-title="France">44</div>   
<div class="td td5" data-title="Italy">48</div>   
<div class="td td6" data-title="Kore">77</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_12 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size12"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">14</div>
<div class="td td2" data-title="UK">18</div>                     
<div class="td td3" data-title="Germany">44</div>
<div class="td td4" data-title="France">46</div>   
<div class="td td5" data-title="Italy">50</div>   
<div class="td td6" data-title="Kore">77</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_14 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size14"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">16</div>
<div class="td td2" data-title="UK">20</div>                     
<div class="td td3" data-title="Germany">46</div>
<div class="td td4" data-title="France">48</div>   
<div class="td td5" data-title="Italy">52</div>   
<div class="td td6" data-title="Kore">88</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_16 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size16"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">18</div>
<div class="td td2" data-title="UK">22</div>                     
<div class="td td3" data-title="Germany">48</div>
<div class="td td4" data-title="France">50</div>   
<div class="td td5" data-title="Italy">54</div>   
<div class="td td6" data-title="Kore">88</div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_18 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size18"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">20</div>
<div class="td td2" data-title="UK">24</div>                     
<div class="td td3" data-title="Germany">50</div>
<div class="td td4" data-title="France">52</div>   
<div class="td td5" data-title="Italy">56</div>   
<div class="td td6" data-title="Kore"></div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_20 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size20"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">22</div>
<div class="td td2" data-title="UK">26</div>                     
<div class="td td3" data-title="Germany">52</div>
<div class="td td4" data-title="France">54</div>   
<div class="td td5" data-title="Italy">58</div>   
<div class="td td6" data-title="Kore"></div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_22 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size22"></div>   
 </div> <!-- tr -->
 
<div class="tr">
<div class="td td1" data-title="US">24</div>
<div class="td td2" data-title="UK">28</div>                     
<div class="td td3" data-title="Germany">54</div>
<div class="td td4" data-title="France">56</div>   
<div class="td td5" data-title="Italy">60</div>   
<div class="td td6" data-title="Kore"></div>   
<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_24 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size24"></div>   
 </div> <!-- tr -->
					 
					 </div> <!-- table -->
					 </div> <!-- size-chart-table -->
				   
				                 
                </div>

       

         <!--  <div class="form_row common_field">
                  <div class="form_row_left common_field quan">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="{{ $fetchfirstdata->pro_qty or '' }}"  >
                      </div>
                    </div>
                  </div>
           
                    
                  </div> -->
                    <!-- form_row --> 
                  </div>
                  <!-- form_row -->
				  
                    
                   <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.PRODUCT_WEIGHT'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PRODUCT_WEIGHT'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumberKey(event,this)" name="pro_weight" id="pro_weight" maxlength="15" value="{{ $fetchfirstdata->pro_weight or '' }}"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>
				  
				  <div class="form_row">
				  <div class="form_row_left">
                    <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> 
                     </label>
                    <div class="info100">
                      
                      <div class="english">
                        <textarea name="description" class="english" maxlength="500" id="description" rows="4" cols="50">{{ $fetchfirstdata->pro_desc or ''  }} </textarea> 
                      </div>      
                      
                      <div  class="arabic ar" >     
                              <textarea class="arabic ar" maxlength="500" name="description_ar" id="description_ar" rows="4" cols="50">{{ $fetchfirstdata->pro_desc_ar or ''  }}</textarea>  
                      </div> 
                      
                    </div>
                    <!-- form_row --> 
					</div>
					
                  </div>
                  
                  

                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
          <input type="hidden" name="sid" value="{{ $sid }}"> 
           <input type="hidden" name="parentid" value="{{ $id }}">
           <input type="hidden" name="autoid" id="autoid" value="{{ $autoid or '' }}">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <!-- form_row -->
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  <!-- form_row -->
                  </div></div>
                </form>
          
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
  
 </div>


<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addmanager").validate({


              ignore: [],
               rules: {
                 
                  category: {
                       required: true,
                      },
                   
                    

                    name: {
                       required: true,
                      },

                       name_ar: {
                       required: true,
                      },

                      price: {
                       required: true,
                      },

                       

                     description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                       @if(isset($fetchfirstdata->pro_Img)!='')  
                          stor_img: {
                           required: false,
                           accept:"png|jpe?g|gif",
                      },
                      @else
                       stor_img: {
                           required: true,
                           accept:"png|jpe?g|gif",
                      },
                       
                         
                      @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                
           messages: {
             
              category: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_TAILOR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_TAILOR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_TAILOR') }} @endif",
                      }, 

               category1: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",
                      },         



             name: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_NAME'); @endphp",
                      },  

                 name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_NAME_AR'); @endphp",
                      },

                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                      },

                 


                   description: {
               
                required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
                      },

                       description_ar: {

                         required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_SINGER_AR'); @endphp",

              
                      },
        
                 
                  stor_img: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",

                 }, 

                  quantity: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY') }} @endif ",
                      },

                       
                    
                },             
                   invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                     console.log("invalidHandler : validation", valdata);

 
                    @if($mer_selected_lang_code !='en')
                    if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                   
                    if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                   
                    if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                     if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                   
                    if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     

                    }

                    if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                    $('.arabic_tab').trigger('click');     

                    }

                     if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                    $('.arabic_tab').trigger('click');     

                    }
                    

                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     

                    }

                      
@else
                    if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                   if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                  if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }

                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                     
                      
                     
                     if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     
                      if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }


                    
@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
  $("#company_logo1").change(function () {
        var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   $("#file_value2").html('');         
$(".certifications").html('Only PDF format allowed.');

         }
    });

  $(document).ready(function(){
    var autoid = $('#autoid').val();
    var catid = $('#category').val();
    //alert(catid);
     if(catid=='1' || autoid=='') {
      
      $('.displaycat').hide();
      $('.sizedisp').hide();
      $('.quan').hide();
   
    
    
     } 
     else 
     {

           $('.displaycat').show();
          
     }
    
    });
  function chceckCategory()
  {
     var catid =  $('#category').val();
   

     if(catid==1) 
     {

          $('.displaycat').show();
          $('.sizedisp').show();
          $('#fabrica').hide();
          $('.quan').show();
          $('#category1').attr('required','true');
          $('#quantity').attr('required','true');   
  $('.tailorcase').show();
          


     } 
     else 
     {
          $('.displaycat').hide();
          $('.sizedisp').hide();
          $('#fabrica').show();
          $('#category1').removeAttr('required');
          $('#quantity').removeAttr('required');
          $('.quan').hide();
          $('.tailorcase').hide();
     }


  }




/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
  function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
</script> 





<!-- Add More product images start -->

 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->



@include('sitemerchant.includes.footer')


<script src="{{ url('')}}/themes/js/jquery.min.js"></script> 
<script src="{{ url('')}}/themes/js/multiple-select.js"></script> 
@if(Session::get('lang_file') =='ar_lang') 

<script>
    $(function() {
        $('#ms_size').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });
      });
</script>
 @else 
<script>
    $(function() {
        $('#ms_size').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });
     });
</script> 
@endif 

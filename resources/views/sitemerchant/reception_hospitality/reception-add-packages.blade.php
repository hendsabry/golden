@include('sitemerchant.includes.header')
@php $reception_hospitality_leftmenu =1;
$cid = request()->cid;
$id = request()->id;
 @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(isset($cid) && $cid !='')
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATEPACKAGE') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE') }} @endif </h5>
        @else
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.ADD_PACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.ADD_PACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADD_ITEM') }} @endif </h5>
        @endif
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <form name="form1"  id ="menucategory" method="post" action="{{ route('recption-update-packege') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PACKEGENAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKEGENAME'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
                      <input class="english" id="packeg_name" maxlength="90"  name="packeg_name"  value="{{$packeg->pro_title or ''}}"  required=""  type="text" >
                    </div>
                    <div class="arabic">
                      <input class="arabic ar" id="packeg_name_ar" maxlength="90"  name="packeg_name_ar"  value="{{$packeg->pro_title_ar or ''}}"  required=""  type="text" >
                    </div>
                  </div>
                </div>
              </div>
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY') }}  
                  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_NATIONALITY') }} @endif </label>
                  <div class="info100">
                  <select name="staff_nationality"  id="staff_nationality">
                    <option value="" >@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY') }}  
                    @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_NATIONALITY') }} @endif </option>
                    
                    
          
                           @php $co_name='co_name'@endphp
                           @if($mer_selected_lang_code !='en')
                                                 @php $co_name= 'co_name_'.$mer_selected_lang_code; @endphp
                           @endif  

                           @foreach($countrylist as $val)
                           @php

                           @endphp
                             
                    
                    <option value="{{ $val->co_id }}" @if(isset($packeg->staff_nationality) && $val->co_id==$packeg->staff_nationality) selected @endif >
                    <div class="english">
                    {{  $val->$co_name }}
                    </div>
                    </option>
                    
                    
                           @endforeach

                      
                  
                  </select>
                  @if($errors->has('workers_nationality')) <span class="help-block text-red"> {{ $errors->first('workers_nationality') }} </span> @endif
                  <input type="hidden" name="shopid" value="{{ request()->shopid }}">
                </div>
              </div>
              </div>
              <div class="form_row common_field ">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.NO_STAFF'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.NO_STAFF'); @endphp </span> </label>
                  <div class="info100">
                    <input id="no_of_staff" class="xs_small" onkeypress="return isNumber(event);" maxlength="10"  name="no_of_staff" value="{{$packeg->no_of_staff or ''}}"  type="text" >
                  </div>
                </div>
              </div>
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PRICE'); @endphp </span> </label>
                  <div class="info100">
                    <input id="price" class="xs_small" onkeypress="return isNumber(event);" maxlength="10"  name="price" value="{{$packeg->pro_price or ''}}"   type="text" >
                  </div>
                </div>
              </div>

<div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_IMAGE'); @endphp </span> </label>
                    
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      </div>
                      </label>
                      <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                      <input id="company_logo" name="mc_img" class="info-file" type="file">
                      <input type="hidden" name="updatemc_img" value="{{ $packeg->pro_Img or '' }}">
                      <input type="hidden" name="id" value="{{ $id }}">
                      <div class="form-upload-img">@if(isset($packeg->pro_Img) && $packeg->pro_Img !='')
                       <img src="{{ $packeg->pro_Img or '' }}" width="150" height="150"> 
                       @endif</div>
                    </div>
                    
                    </div>


   <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="{{ $packeg->pro_qty or '' }}"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>


                <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="{{$packeg->pro_discount_percentage or ''}}" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.DESCRIPTION'); @endphp </span> </label>
                  <div class="info100">
                    <div class="">
                      <textarea class="english" id="packegdescription"  name="packegdescription" >{{$packeg->pro_desc or ''}}</textarea>
                    </div>
                    <div class="" >
                      <textarea class="arabic ar" id="packegdescription_ar"  name="packegdescription_ar" >{{$packeg->pro_desc_ar or ''}}</textarea>
                    </div>
                    @php  $id = request()->id; $cid = request()->cid; @endphp
                    <input type="hidden" name="id" value="{{ $id or ''}}">
                    <input type="hidden" name="cid" value="{{ $cid or ''}}">
                    <input type="hidden" name="shopid" value="{{ $shopid or ''}}">
                  </div>
                </div>
              </div>
              @php $k=1; @endphp
              @foreach($getAttr as $key=>$value)   
              
              @php $items_a = Helper::getProductByAttribute($shopid,$value->id);     @endphp                 
              
              @if(count($items_a)>0)
              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label" for="intezxrnal{{$k}}"> <span class="english">{{$value->attribute_title}}</span> <span  class="arabic ar"> {{$value->attribute_title_ar}} </span> </label>
                  <div class="info100"> @foreach($items_a as $value1)
                    <div class="save-card-line">
                      <input type="radio" data-validation="checkbox_group" name="items{{$value->id}}[]" value="{{$value1->pro_id}}" id="internal{{$value1->pro_id}}"  class="items{{$value->id}} cbox_child_{{$k}}" @if(in_array($value1->
                      pro_id, $selecteditemsdata)) checked="checked" @endif>
                      <label for="internal{{$value1->pro_id}}"> <span class="english">{{ $value1->pro_title or ''}}</span> <span class="arabic ar">{{ $value1->pro_title_ar or ''}}</span> </label>
                    </div>
                    @endforeach </div>
                  <span for="items{{$value->id}}[]" generated="true" class="error"></span> </div>
              </div>
              @php $k=$k+1; @endphp
              @endif
              
              @endforeach
              <div class="form_row">
                <div class="form_row_left"> @if($cid=='')
                  <div class="english">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                  </div>
                  @else
                  <div class="english">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="Update">
                  </div>
                  <div class="arabic ar">
                    <input type="submit" id="hallsubmit" name="addhallpics" value="تحديث">
                  </div>
                  @endif </div>
              </div>
            </form>
            <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>       
	$("form").data("validator").settings.ignore = "";
</script>
<script type="text/javascript">
  var vali ='mer_en_lang.MER_VALIDATION_PRICE'
$("#menucategory").validate({
                  ignore: [],
                  rules: {

                    packeg_name: {
                       required: true,
                      },
                     packeg_name_ar: {
                       required: true,
                      },
                     workers_nationality: {
                     required: true,
                    },
                     no_of_staff: {
                     required: true,
                    },
                    price: {
                     required: true,
                    },
                    
                 
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
             messages: {
             packeg_name: {
               required: "{{ trans('mer_en_lang.MER_VALIDATION_PACKEG_NAME') }}",
                      }, 
               packeg_name_ar: {
               required: "{{ trans('mer_ar_lang.MER_VALIDATION_PACKEG_NAME_AR') }}",
                      }, 
               no_of_staff: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_NO_STAFF')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_NO_STAFF')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_NO_STAFF')}} @endif",
                      },
              
               workers_nationality: {
               required: " @if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_WORKERS_NATIONALITY')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_WORKERS_NATIONALITY')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_WORKERS_NATIONALITY')}} @endif ",
                      },  
                      price: {
               required: " @if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')}} @endif ",
                      },                                                           
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                   @if($mer_selected_lang_code !='ar')


                    if (typeof valdata.packeg_name != "undefined" || valdata.packeg_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                    else if (typeof valdata.no_of_staff != "undefined" || valdata.no_of_staff != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }    
                    else if (typeof valdata.packegdescription != "undefined" || valdata.packegdescription != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.workers_nationality != "undefined" || valdata.workers_nationality != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }                    
                  
                    else if (typeof valdata.packeg_name_ar != "undefined" || valdata.packeg_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                    else if (typeof valdata.packegdescription_ar != "undefined" || valdata.packegdescription_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                @else
                   
                      if (typeof valdata.packeg_name_ar != "undefined" || valdata.packeg_name_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    } 
                    else if (typeof valdata.no_of_staff != "undefined" || valdata.no_of_staff != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }    
                    else if (typeof valdata.packegdescription_ar != "undefined" || valdata.packegdescription_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.workers_nationality != "undefined" || valdata.workers_nationality != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }  
                     else if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }                    
                  
                    else if (typeof valdata.packeg_name != "undefined" || valdata.packeg_name != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                    else if (typeof valdata.packegdescription != "undefined" || valdata.packegdescription != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                   @endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

 var createValidation = function() {
@foreach($getAttr as $key=>$value)   

@php $items_a = Helper::getProductByAttribute($shopid,$value->id);     @endphp                 
         
@if(count($items_a)>0) 

        $(".items{{$value->id}}").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
              required: true,
              messages: {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PACKEGE_ITEMS')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_PACKEGE_ITEMS')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PACKEGE_ITEMS')}} @endif"
              }
            });
          });
        @endif
@endforeach

};

$(function() {
   createValidation();
});

 


 $('.cbox').click(function(event) {
  if(this.checked) {
     var child = $(this).data('child');
      $('.'+child).each(function() {
          this.checked = true;
      });
  }
  else {
         var child = $(this).data('child');
      $('.'+child).each(function() {
          this.checked = false;
      });
  }
});



</script>
@include('sitemerchant.includes.footer') 
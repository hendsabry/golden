@include('sitemerchant.includes.header')  
@php $reception_hospitality_leftmenu =1; @endphp 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
   <div class="service_listingrow"> 
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </h5>
    </div>

@php
$id = request()->id;
$sid = request()->shopid;
@endphp

 {!! Form::open(array('url'=>"reception-list-orders/{$id}/{$sid}",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}


 <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
     
        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
           
      

    <div class="order-filter-line order-line">
    <div class="of-date-box"><input type="text" class="cal-t" value="{{request()->date_to}}" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.Start_Date')!= '') {{  trans(Session::get('mer_lang_file').'.Start_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Start_Date') }} @endif" name="date_to" autocomplete="off" id="date_to" /><input type="text" value="{{request()->from_to}}" class="cal-t" autocomplete="off" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.End_Date')!= '') {{  trans(Session::get('mer_lang_file').'.End_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.End_Date') }} @endif" id="datepicker"  name="from_to"/></div><div class="of-orders">

      <!--<select name="order_days">
      <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_ORDERS')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_ORDERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_ORDERS') }} @endif</option>
      <option value="10_days" @if(request()->order_days =='10_days') {{'selected'}} @endif >@if (Lang::has(Session::get('mer_lang_file').'.Last_10_Days')!= '') {{  trans(Session::get('mer_lang_file').'.Last_10_Days') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_10_Days') }} @endif </option>
      <option  value="1_month" @if(request()->order_days =='1_month') {{'selected'}} @endif>@if (Lang::has(Session::get('mer_lang_file').'.Last_1_Month')!= '') {{  trans(Session::get('mer_lang_file').'.Last_1_Month') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_1_Month') }} @endif </option> 
      <option  value="6_month" @if(request()->order_days =='6_month') {{'selected'}} @endif>@if (Lang::has(Session::get('mer_lang_file').'.Last_6_Months')!= '') {{  trans(Session::get('mer_lang_file').'.Last_6_Months') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_6_Months') }} @endif </option>
      <option  value="1_year" @if(request()->order_days =='1_year') {{'selected'}} @endif>@if (Lang::has(Session::get('mer_lang_file').'.Last_1_Years')!= '') {{  trans(Session::get('mer_lang_file').'.Last_1_Years') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_1_Years') }} @endif </option>
      </select>-->
      </div>
    <div class="search-box-field mems ">
            <select name="status" id="status" class="city_type">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_STATUS') }} @endif </option>
              <option value="1" @if(request()->status =='1') {{'selected'}} @endif> @if (Lang::has(Session::get('mer_lang_file').'.IN_PROCESS')!= '') {{  trans(Session::get('mer_lang_file').'.IN_PROCESS') }}@else  {{ trans($MER_OUR_LANGUAGE.'.IN_PROCESS') }} @endif</option>

              <option value="2" @if(request()->status =='2') {{'selected'}} @endif> @if (Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.COMPLETE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.COMPLETE') }} @endif</option>
            </select>
            <input type="hidden" id="serachfirstfrm" name="serachfirstfrm" value="1">
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
       <span id="todata" class="error"></span>
      {!! Form::close() !!}

       {!! Form::open(array('url'=>"reception-list-orders/{$id}/{$sid}",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter2')) !!}

      <div class="filter_right" style="width:100%;">  
         <input name="searchkeyword" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{ request()->searchkeyword }}" />
          <input type="button" class="icon_sch" id="submitdata"  onclick="submit();" />
        </div>
      {!! Form::close() !!}
    </div> <!-- order-filter-line -->
    
      </div>
      <!-- filter_area --> 
      
    
      <div class="global_area"><!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap"> 
      <div class="panel-body panel panel-default">
      @if($getorderedproducts->count() < 1)
                <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
                @else
      <div class="table merchant-order-table">
      <div class="tr">
	                 <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif</div>
             
            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= '') {{  trans(Session::get('mer_lang_file').'.PAYMENTREQUEST') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST') }} @endif</div>
                  
                    
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS') }} @endif</div>
                    <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                  </div>

               
           @foreach($getorderedproducts as $orderedproductbasicinfo)
           @php 
           $getCustomer = Helper::getuserinfo($orderedproductbasicinfo->cus_id); 
           $isalreadymaderequest = Helper::ispaymentreuqest($orderedproductbasicinfo->order_id,$orderedproductbasicinfo->product_id);
           $ordertime=strtotime($orderedproductbasicinfo->created_at);
           $orderedtime = date("d M Y",$ordertime);
        @endphp 
           <div class="tr">
                     <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif">{{ $orderedproductbasicinfo->order_id  }}</div>

                     <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif">
                      @php if(isset($getCustomer->cus_name) && $getCustomer->cus_name!=''){echo $getCustomer->cus_name;}else{echo 'N/A';} if(isset($getCustomer->email) && $getCustomer->email!=''){echo '<br />'.$getCustomer->email;}else{echo '<br />N/A';} @endphp                      
                  </div>
           
            <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif">{{ $orderedtime }}</div>
                     <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= '') {{  trans(Session::get('mer_lang_file').'.PAYMENTREQUEST') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST') }} @endif">
                      @php 
                          if($orderedproductbasicinfo->status!=1){
                      if ($isalreadymaderequest>0){ @endphp @if (Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.COMPLETE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.COMPLETE') }} @endif  @php } else{ @endphp
                     <span id="paymentstatus"> <a href="#" onclick="paymentrequest({{$mer_id}},{{$orderedproductbasicinfo->product_id}},{{$orderedproductbasicinfo->order_id}},{{$orderedproductbasicinfo->total_price}});">
                      @if (Lang::has(Session::get('mer_lang_file').'.REQUEST')!= '') {{  trans(Session::get('mer_lang_file').'.REQUEST') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.REQUEST') }} @endif
                    </a>
                      </span>
                      @php } }else{ @endphp
                      @if (Lang::has(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.WAIT_FOR_ORDER_COMPLETE') }} @endif
                    @php } @endphp
                  </div>
                     
                    
                     <div class="td td5" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif">{{ number_format((float)$orderedproductbasicinfo->sum, 2, '.', '') }}</div>
                     <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDERSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDERSTATUS') }} @endif">
                      @php if($orderedproductbasicinfo->status==1){ @endphp
                      <select name="ost" onchange="updateproductstatus('{{ $orderedproductbasicinfo->id }}',this.value);">
                        <option value="1">@if (Lang::has(Session::get('mer_lang_file').'.IN_PROCESS')!= '') {{  trans(Session::get('mer_lang_file').'.IN_PROCESS') }}@else  {{ trans($MER_OUR_LANGUAGE.'.IN_PROCESS') }} @endif</option>
                        <option value="2">@if (Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.COMPLETE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.COMPLETE') }} @endif</option>
                      </select>
                        @php }else{ echo "Complete";} @endphp
                    </div>
                        <div class="td td7 view_center" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif"><a href="{{ url('') }}/reception-list-orders/orderdetail/{{$id}}/{{$sid}}/{{ $orderedproductbasicinfo->id }}/{{ $orderedproductbasicinfo->product_id }}/{{ $orderedproductbasicinfo->cus_id }}/{{ $orderedproductbasicinfo->order_id }}"><img src="{{url('')}}/public/assets/img/view-icon.png" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></a></div>
                     
                 </div>
@endforeach
    @endif  
      </div>
      
      </div>
      {{ $getorderedproducts->links() }}
      </div></div></div></div>
              
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
 <script>
 $(function() {
$( "#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
});

 $(function() {
 $("#datepicker").change(function () {
    var endDate  = document.getElementById("datepicker").value;
    var startDate = document.getElementById("date_to").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("datepicker").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("??? ?? ???? ????? ???????? ???? ?? ????? ?????");
        @else
        $('#todata').html("End date should be greater than Start date");
        @endif
        $('#to').show();
    }
    else
    {
         $('#todata').html("");

    }
});
 });

</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer')

<script type="text/javascript">
  function updateproductstatus(orderproductid,statusvalue){
  
var orderproductid=orderproductid;
  var statusvalue=statusvalue;
        $.ajax({ 
                      
              url:"{{url('abaya-order/getproductstatus')}}",     
            type: 'post', // performing a POST request
            data : {
              orderproductid: orderproductid,
              orderstatus: statusvalue,
              _token: '{{csrf_token()}}'
            },
            datatype: 'application/json',
                  success: function(data) {
                  var json = JSON.stringify(data);
                  var obj = JSON.parse(json);
                  console.log(obj);
                  location.reload();
              },
                  error: function() { /*alert('Failed!');*/ return false; },
        
        });
  }

</script>
<script type="text/javascript">
  
  function submitfrm(){ 
 
    document.searchfrm.submit();
  }
</script>
<script type="text/javascript">
  
  function paymentrequest(merchant_id,product_id,order_id,amount){

      $.ajax({ 
                      
              url:"{{url('/requestpayment')}}",     
            type: 'post', // performing a POST request
            data : {
              main_cat_id: 14,
              merchant_id: merchant_id,
              product_id: product_id,
              order_id: order_id,
              amount: amount,
              _token: '{{csrf_token()}}'
            },
            datatype: 'application/json',
                  success: function(data) {
               var json = JSON.stringify(data);
                var obj = JSON.parse(json);
          console.log(obj);
            
              document.getElementById('paymentstatus').innerHTML = 'Complete';
          
              },
                  error: function() { /*alert('Failed!');*/ return false; },
        
        });

  }
</script>
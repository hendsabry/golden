@include('sitemerchant.includes.header') 
@php $hall_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_MENU_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_MENU_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_MENU_CATEGORY') }} @endif</h5>
      </header>
      @include('sitemerchant.includes.language')
      
       <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box">             
           
              <form name="form1" id="menucategory" method="post" action="{{ route('storecategory') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">
                    <span class="english">@php echo lang::get('mer_en_lang.MER_CATEGORY_NAME'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); @endphp </span>
          </label>
                  <div class="info100">
                   <div class="english"> 
                    <input type="text" class="english" name="category_name" maxlength="90" value="{!! Input::old('category_name') !!}" id="category_name" required="" >
                     </div>
                    <div class="arabic">
                    <input type="text" class="arabic ar" name="category_name_ar" maxlength="90" value="{!! Input::old('category_name_ar') !!}" id="category_name_ar" required="" >
                  </div>
                  </div>
                  </div>

                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CREATED_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CREATED_IMAGE'); @endphp </span> </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="catimage" id="company_logo1" class="info-file">
                    </div>
                     </div>
				  </div> <!-- form_row -->
                <div class="form_row">
                <div class="form_row_left english">
                  <input type="submit" name="submit" value="Submit">
                  <input type="hidden" name="hid" value="{{ $hid }}">
                   <input type="hidden" name="bid" value="{{ $bid }}">
                   </div>
                   
                   <div class="form_row_left arabic ar">
                  <input type="submit" name="submit" value="خضع">
                  </div>
                   
                </div>
				 


              </form>
           
            
            <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
            
            <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },

                      catimage: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      }, 
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             category_name: {
           required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_CATEGORY_NAME'); @endphp",
                      },  
 
                 category_name_ar: {
               required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_CAT_ARABIC_AR'); @endphp",
                      }, 

                    catimage: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },       
                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

 @if($mer_selected_lang_code !='en')

                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 

                     if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }                   
                  
                    if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
                  if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }
                  if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  } 

                   if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }                   
                   

@endif




                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
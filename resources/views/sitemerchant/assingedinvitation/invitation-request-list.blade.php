@include('sitemerchant.includes.header')
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="service_listingrow">
    <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Assigned_Invitation')!= '') {{  trans(Session::get('mer_lang_file').'.Assigned_Invitation') }}  
      @else  {{ trans($MER_OUR_LANGUAGE.'.Assigned_Invitation') }} @endif</h5>
  </div>
  
  <div class="global_area">
    <div class="row">
      <div class="col-lg-12">
        <div class="table_wrap">
		  
          <div class="panel-body panel panel-default">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            @if($getorderinvitation->count() < 1)
            <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
            @else
			
            <div class="table dish-table">
              <div class="tr">
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Order_id')!= '') {{  trans(Session::get('mer_lang_file').'.Order_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Order_id') }} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Occasion_Name')!= '') {{  trans(Session::get('mer_lang_file').'.Occasion_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Occasion_Name') }} @endif</div>
                <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.Venue')!= '') {{  trans(Session::get('mer_lang_file').'.Venue') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Venue') }} @endif</div>

                <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.Event_DATE_TIME')!= '') {{  trans(Session::get('mer_lang_file').'.Event_DATE_TIME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Event_DATE_TIME') }} @endif</div>

                 <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.Number_of_Invitees')!= '') {{  trans(Session::get('mer_lang_file').'.Number_of_Invitees') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Number_of_Invitees') }} @endif</div>
                  <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.Invitations_Type')!= '') {{  trans(Session::get('mer_lang_file').'.Invitations_Type') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Invitations_Type') }} @endif</div>
                  <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.Price')!= '') {{  trans(Session::get('mer_lang_file').'.Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Price') }} @endif</div>
                  <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIONS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIONS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIONS') }} @endif</div>
              </div>
              @php $totalamount =0; @endphp
              @foreach($getorderinvitation as $value)              
             @php $userinfo=Helper::getuserinfo($value->customer_id);
                  $electronicinvitationtproduct=Helper::getorderedelectronicinvitation($value->order_id,'invitations'); 
                  $amo=$electronicinvitationtproduct->total_price; 
                  $vatamonu = Helper::calculatevat($value->order_id,$amo);  
                  $totalamount= $vatamonu+$amo;    
              @endphp

             <?php if($value->invitaion_mode==1){ $ocmode='Text Message'; }elseif($value->invitaion_mode==2){ $ocmode='Email'; }else{ $ocmode='Invitations Card';}

              ?>
              <div class="tr">
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Order_id')!= '') {{  trans(Session::get('mer_lang_file').'.Order_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Order_id') }} @endif">{{$value->order_id or ''}}</div>
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif">

                  {{ $userinfo->cus_name }}</div>
                <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Occasion_Name')!= '') {{  trans(Session::get('mer_lang_file').'.Occasion_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Occasion_Name') }} @endif">  {{ $value->occasion_name }} </div>
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Venue')!= '') {{  trans(Session::get('mer_lang_file').'.Venue') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Venue') }} @endif">   {{ $value->venue }} </div>

                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Event_DATE_TIME')!= '') {{  trans(Session::get('mer_lang_file').'.Event_DATE_TIME') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Event_DATE_TIME') }} @endif">   {{ Carbon\Carbon::parse($value->date)->format('F j, Y')}} / {{ $value->time }}  </div>
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Number_of_Invitees')!= '') {{  trans(Session::get('mer_lang_file').'.Number_of_Invitees') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Number_of_Invitees') }} @endif">   {{ $value->no_of_invitees }} </div>
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Note')!= '') {{  trans(Session::get('mer_lang_file').'.Note') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Note') }} @endif">   {{ $ocmode }} </div>
                
                  <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Price')!= '') {{  trans(Session::get('mer_lang_file').'.Price') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Price') }} @endif">SAR {{ number_format($totalamount,2) }} </div>

                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIONS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIONS') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIONS') }} @endif"> 
                  <a  href="{{ url('') }}/manage_invitees_list/{{$value->order_id}}">
                          <img src="{{url('')}}/public/assets/img/view-icon.png" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></a>


                        </div>
              </div>
              @endforeach </div>
            @endif
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          </div>
          {{ $getorderinvitation->links() }} </div>
      </div>
    </div>
  </div>
</div>
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        @else
        $('#todata').html("End date should be greater than From date");
        @endif
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer') 
@include('sitemerchant.includes.header')
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
  <div class="service_listingrow">
    <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.Invitees_list')!= '') {{  trans(Session::get('mer_lang_file').'.Invitees_list') }}  
      @else  {{ trans($MER_OUR_LANGUAGE.'.Invitees_list') }} @endif</h5>

      <div style="float:right;"><a  href="{{ url('') }}/manage_assigned_invitation" class="btn">Back</a></div>
  </div>
  
  <div class="global_area">
    <div class="row">
      <div class="col-lg-12">
        <div class="table_wrap">
		  
          <div class="panel-body panel panel-default">
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            @if($getorderinvitation->count() < 1)
            <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
            @else
			
            <div class="table dish-table">
              <div class="tr">
                
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Invitees_Name')!= '') {{  trans(Session::get('mer_lang_file').'.Invitees_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Invitees_Name') }} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Invitees_Email')!= '') {{  trans(Session::get('mer_lang_file').'.Invitees_Email') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Invitees_Email') }} @endif</div>

                <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.Invitees_Address')!= '') {{  trans(Session::get('mer_lang_file').'.Invitees_Address') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Invitees_Address') }} @endif</div>

                

                 <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif</div>
                
                 
              </div>
              @php $totalamount =0; @endphp
              @foreach($getorderinvitation as $value)              
          
              <?php
                   if($value->status==1){ 
               if (Lang::has(Session::get('mer_lang_file').'.Attended')!= ''){ $atst=trans(Session::get('mer_lang_file').'.Attended'); }else { $atst=trans($MER_OUR_LANGUAGE.'.Attended'); }

               }else{
                  if (Lang::has(Session::get('mer_lang_file').'.NotAttended')!= ''){ $atst=trans(Session::get('mer_lang_file').'.NotAttended'); }else{  $atst=trans($MER_OUR_LANGUAGE.'.NotAttended'); }
              }?>
           
              <div class="tr">
                
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif">

                  {{ $value->invite_name or '' }}</div>
                <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Occasion_Name')!= '') {{  trans(Session::get('mer_lang_file').'.Occasion_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Occasion_Name') }} @endif">  {{ $value->invite_email or '' }} </div>
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Invitees_Address')!= '') {{  trans(Session::get('mer_lang_file').'.Invitees_Address') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Invitees_Address') }} @endif">   {{ $value->address }} </div>
              
                <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif">   {{ $atst or '' }} </div>
               
              </div>
              @endforeach </div>
            @endif
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          </div>
          {{ $getorderinvitation->links() }} </div>
      </div>
    </div>
  </div>
</div>
<script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        @else
        $('#todata').html("End date should be greater than From date");
        @endif
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer') 
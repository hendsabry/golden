@include('sitemerchant.includes.header') 
@php $buffet_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
   <div class="service_listingrow"> 
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </h5>
		</div>
{!! Form::open(array('url'=>'hall-order','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
 <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left">
        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems ">
            <select name="status" id="status">
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
              <option value="1" @if( isset($status) && $status==1) selected="selected" @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif</option>

              <option value="0" @if( isset($status) &&  $status=='0' ) selected="selected" @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_INACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_INACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_INACTIVE') }} @endif</option>
            </select>
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
          
        </div>
        <div class="search_box">
        <div class="search_filter">&nbsp;</div>
        <div class="filter_right">  
          <input type="hidden" name="hid" value="{{$_REQUEST['hid'] or ''}}">
          <input type="hidden" name="bid" value="{{$_REQUEST['bid'] or ''}}">
          <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{ $search or ''}}" />
          <input type="button" class="icon_sch" id="submitdata" />
        </div>
        </div>
      </div>
      <!-- filter_area --> 
      
      {!! Form::close() !!}
	  
	    <div class="global_area"><!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap"> 
			<div class="panel-body panel panel-default">
			
			<div class="table">
			<div class="tr">
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERMOBILE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDERSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDERSTATUS') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_AMOUNT') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_AMOUNT') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                  </div>
				  
				   <div class="tr">
                     <div class="td td1" data-title="Customer Name/Email">Arun</div>
                     <div class="td td2" data-title="Mobile">456456545</div>
                     <div class="td td3" data-title="Order Status">Pending</div>
                     <div class="td td5" data-title="Booking Date ">6Apr,2019</div>
                     <div class="td td5" data-title="Amount">799</div>
                     <div class="td td5" data-title="Payment Status">Pending</div>
                        <div class="td td3" data-title="Action"><span class="status_active2 cstatus" data-status="" >{{ $status or '' }}</span></div>
                     
                 </div>

			
			
			</div>
			
			</div>
			
			</div></div></div></div>
              
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
@include('sitemerchant.includes.footer')
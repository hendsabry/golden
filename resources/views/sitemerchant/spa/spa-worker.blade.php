@include('sitemerchant.includes.header')
@php $spabig_leftmenu =1; $id = request()->id;
$hid = request()->hid;
$itemid = request()->itemid; @endphp

<div class="merchant_vendor">  
@include('sitemerchant.includes.breadcrumb')   @include('sitemerchant.includes.left')    
    <div class="right_panel">
        <div class="inner">
    <div class="service_listingrow">
             <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.WORKERS')!= '') {{  trans(Session::get('mer_lang_file').'.WORKERS') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.WORKERS') }} @endif</h5>
      
      <div class="add"><a  href="{{ route('spa-add-worker',['id' => request()->id,'hid' => request()->hid,'itemid' => request()->itemid]) }}">@if (Lang::has(Session::get('mer_lang_file').'.ADDWORKER')!= '') {{  trans(Session::get('mer_lang_file').'.ADDWORKER') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.ADDWORKER') }} @endif</a></div>
            </div>

 
      
       <!-- Display Message after submition -->
{!! Form::open(array('url'=>"spa-worker/$id/$hid/$itemid",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}
<a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left">
        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          
            <div class="search-box-field mems">
           @php $statuss = request()->status; $searchh= request()->search; @endphp
            <select name="status" id="status" >
              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
              <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
              <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
            </select>
      
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
           
        
          
        </div>
        <div class="search_box">
        <div class="search_filter">&nbsp;</div>
        <div class="filter_right">
         
          <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$searchh or  ''}}" />
          <input type="button" name="search" class="icon_sch" id="submitdata" onclick="submit();" />
        </div>
        </div>
      </div>
      <!-- filter_area --> 
      
      {!! Form::close() !!}
    
    <!-- Display Message after submition -->
      @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
    <div class="global_area">
            <div class="row">
        <div class="col-lg-12">
          <div class="table_wrap">
              <div class="panel-body panel panel-default"> 

                 @if($alldata->count() < 1)
                  <div class="no-record-area">
                  @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif
                  </div>
                  @else
                  <div class="table hotel-list-table">
                          <div class="tr">
                            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.BACK_NAME')!= '') {{ trans(Session::get('mer_lang_file').'.BACK_NAME')}}  @else {{ trans($MER_OUR_LANGUAGE.'.BACK_NAME')}} @endif</div>
                             <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE')}} @endif  </div>
                              <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.BOOKING')!= '') {{ trans(Session::get('mer_lang_file').'.BOOKING')}}  @else {{ trans($MER_OUR_LANGUAGE.'.BOOKING')}} @endif</div>
                <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.REVIEW')!= '') {{ trans(Session::get('mer_lang_file').'.REVIEW')}}  @else {{ trans($MER_OUR_LANGUAGE.'.REVIEW')}} @endif</div>
                             <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{ trans(Session::get('mer_lang_file').'.MER_STATUS')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS')}} @endif</div>
                             <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTION')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION')}} @endif</div>
                       
                          </div>
               
                          @php $mc_name='staff_member_name'@endphp
                           @if($mer_selected_lang_code !='en')
                           @php $mc_name= 'staff_member_name_'.$mer_selected_lang_code; @endphp
                           @endif  
                       @foreach($alldata as $val)
                        <div class="tr">
                         <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.BACK_NAME')!= '') {{ trans(Session::get('mer_lang_file').'.BACK_NAME')}}  @else {{ trans($MER_OUR_LANGUAGE.'.BACK_NAME')}} @endif">{{ $val->$mc_name }}</div>
                         <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE')}} @endif  ">

                        @if($val->image)   
                           <img src="{{ $val->image }}" width="150">
                          @else
                           No image

                          @endif

                         </div>
                         <div class="td td3 view_center" data-title="@if (Lang::has(Session::get('mer_lang_file').'.BOOKING')!= '') {{ trans(Session::get('mer_lang_file').'.BOOKING')}}  @else {{ trans($MER_OUR_LANGUAGE.'.BOOKING')}} @endif  "><img src="{{url('')}}/public/assets/img/calendar-icon-s.png" alt="" />

                            @php 
                           
                            $workerratingavarage = Helper::workerratingavarage($val->id); 
                            $todaysbooking = Helper::stafftodaybookingslot($val->id);                              
                              if(count($todaysbooking)>0){                             
                             @endphp
                                 <ul>
                                @foreach($todaysbooking as $bookslot)
                                  <li>{{ $bookslot->start_time }} - {{ $bookslot->end_time }}</li>
                                @endforeach
                                  </ul>

                              @php  }else{ echo 'No Booking Today'; } @endphp
                              <br>

                           <a href="{{ route('spa-worker-booking',['id'=>$id,'hid'=>request()->hid,'itemid'=>request()->itemid, 'workerid'=>$val->id]) }}">
                             @if (Lang::has(Session::get('mer_lang_file').'.VIEW')!= '') {{ trans(Session::get('mer_lang_file').'.VIEW')}}  @else {{ trans($MER_OUR_LANGUAGE.'.VIEW')}} @endif
                           </a>
                        </div>
                        <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.REVIEW')!= '') {{ trans(Session::get('mer_lang_file').'.REVIEW')}}  @else {{ trans($MER_OUR_LANGUAGE.'.REVIEW')}} @endif">


                             @php if($workerratingavarage>0){ @endphp
                           <a href="{{ route('beauty-worker-review',['id'=>$id,'hid'=>request()->hid,'itemid'=>request()->itemid, 'workerid'=>$val->id]) }}">

 @if (Lang::has(Session::get('mer_lang_file').'.VIEW')!= '') {{ trans(Session::get('mer_lang_file').'.VIEW')}}  @else {{ trans($MER_OUR_LANGUAGE.'.VIEW')}} @endif

                        </a><br>
                        <img src="{{url('/')}}/themes/images/star{{ $workerratingavarage }}.png">
                     
                      @php }else{ echo '-';} @endphp



                         </div>
                      
                         <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{ trans(Session::get('mer_lang_file').'.MER_STATUS')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS')}} @endif">@if($val->status==1) 

                      <span class="status_active  status_active2 cstatus" data-status="Active" data-id="{{$val->id}}"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTIVE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE')}} @endif </span> @else 


                      <span class="status_deactive  status_active2 cstatus" data-id="{{$val->id}}" data-status="Inactive"> @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_DEACTIVE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE')}} @endif </span> @endif</div>
                       <div class="td td5" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{ trans(Session::get('mer_lang_file').'.MER_ACTION')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION')}} @endif "><a href="{{ route('spa-add-worker',['id'=>$id,'hid'=>request()->hid,'itemid'=>request()->itemid, 'workerid'=>$val->id]) }}">@if (Lang::has(Session::get('mer_lang_file').'.EDIT')!= '') {{ trans(Session::get('mer_lang_file').'.EDIT')}}  @else {{ trans($MER_OUR_LANGUAGE.'.EDIT')}} @endif</a></div>
                      </div>
                   @endforeach

  
        </div>  
        @endif

      


        <!--PAGE CONTENT PART WILL COME INSIDE IT END-->

          </div>
          {{ $alldata->links() }}
    
        </div> <!-- table_wrap -->
        </div>
      </div>
    </div> <!-- global_area -->
    
    </div>
  </div> <!-- right_panel -->
</div> <!-- merchant_vendor -->

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
   <!--  <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a><a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
     }else {
         var activestatus = 0;
     } 
     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'makeupartistworker'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script>

@include('sitemerchant.includes.footer')


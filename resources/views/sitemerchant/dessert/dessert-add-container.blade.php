@include('sitemerchant.includes.header') 
@php $dessert_inner_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
<div class="right_panel">
<div class="inner">
    <header>
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CONTAINER') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CONTAINER') }} @endif </h5>
      @include('sitemerchant.includes.language') </header>
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <!-- Display Message after submition -->
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}  </div>
            @endif
            <!-- Display Message after submition -->
            <div class="one-call-form">
              <form name="form1" id="add-container" method="post" action="" enctype="multipart/form-data">
               
                <div class="form_row">
                  <label class="form_label">       <span class="english">@php echo lang::get('mer_en_lang.MER_Container_Name'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Container_Name'); @endphp </span></label>
                  <div class="info100">
                  <div class="english">
                    <input class="english" maxlength="150" type="text" name="title" value="{!! Input::old('title') !!}" id="title" required="">
                     </div>
                     <div class="arabic">
                    <input class="arabic ar" maxlength="150" id="title_ar"  name="title_ar" value=""  type="text" >
                    </div>

                     </div>  
                </div>
               
                <div class="form_row ">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.MER_No_People'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_No_People'); @endphp </span> 
</label>
                  <div class="info100">
                   <div>
                    <input class="small-sel" maxlength="100" type="text" name="no_people" value="" id="no_people" required="">
                    </div>
                  </div>
                </div>
              
                <div class="form_row" style="display: none;">
                  <label class="form_label">
                   <span class="english">@php echo lang::get('mer_en_lang.MER_About'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_About'); @endphp </span> 
            </label>
                  <div class="info100">
                   <div class="english"> 
                    <textarea  maxlength="5000" name="about" id="about" rows="4" cols="50">This is tested </textarea>
                     </div>
                    <div class="arabic"> 
                    <textarea class="arabic ar" maxlength="5000" name="about_ar" id="about_ar " rows="4" cols="50">This is tested</textarea>
                    
                    </div>
                    </div>
                 </div>
                <!-- form_row -->
                <div class="form_row">
                  <label class="form_label">
                    <span class="english">@php echo lang::get('mer_en_lang.MER_Container_Image'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Container_Image'); @endphp </span>
          </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn">
 <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
 <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>
 </div>
                      </div>
                      </label>
                      <input id="company_logo" name="img" class="info-file" type="file" required="" value="">
                    </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row english">
                  <input type="submit" name="submit" value="Submit">
                </div>

                    <div class="form_row arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                <!-- form_row -->
              </form>
            </div>
            <!-- one-call-form -->
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       price: {
                       required: true,
                      },
                      no_people: {
                       required: true,
                      },
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       img: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CONTAINER_NAME') }} @endif",
                      },  

                title_ar: {
               required:  " {{ trans('mer_ar_lang.MER_VALIDATION_CONTAINER_NAME_AR') }} ",
                      },
                   price: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif",
                      },
                       no_people: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif",
                      }, 

                    about: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ABOUT') }} @endif",
                      }, 

                    about_ar: {
               required:  " {{ trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR') }} ",
                      },
                     img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    else if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                     else if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
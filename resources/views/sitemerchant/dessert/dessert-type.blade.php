@include('sitemerchant.includes.header') 
@php $dessert_inner_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Type_Dishes')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Type_Dishes') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Type_Dishes') }} @endif</h5>
        <div class="add"><a href="{{ route('dessert-add-type',['services_id' =>$services_id,'shopid' => $shopid,'branch_id' => $branch_id]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_Add_Type')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Add_Type') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Add_Type') }} @endif</a></div>
      </div>
      @php
      $services_id = request()->services_id;
      $shopid = request()->shopid;
      $itemid = request()->itemid;
      @endphp
      {!! Form::open(array('url'=>"dessert-type/$services_id/$shopid/$itemid",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!} <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left">
          <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems ">
            <div class="search-box-field mems"> @php $statuss = request()->status; $searchh= request()->search; @endphp
              <select name="status" id="status">
                <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.ALL')!= '') {{  trans(Session::get('mer_lang_file').'.ALL') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.ALL') }} @endif </option>
                <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
                <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
				<option value="44" @if(isset($statuss) && $statuss=='44') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_Type')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Type') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Type') }} @endif</option>
				<option value="45" @if(isset($statuss) && $statuss=='45') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.Dish')!= '') {{  trans(Session::get('mer_lang_file').'.Dish') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.Dish') }} @endif</option>
              </select>
              <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
            </div>
            
          </div>
        </div>
        <div class="search_box">
          <div class="search_filter">&nbsp;</div>
          <div class="filter_right">
            <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{$searchh or ''}}" />
            <input type="button" class="icon_sch" id="submitdata" onclick="submit();" />
          </div>
        </div>
      </div>
      <!-- filter_area -->   
      {!! Form::close() !!}
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap">
  <div class="panel-body panel panel-default">



@if($getdesart->count() < 1)
                <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
                @else
 

                <div class="table dish-table">
                  <div class="tr">
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NAME') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</div>
                    
					<div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CATEGORY') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_QUANTITY') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PR') }} @endif</div>
					<div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif</div>
                    <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
                  </div>


                    @php 
                    $mucucat='menu_name';
                    $pro_title='pro_title';
                    @endphp
                       @if($mer_selected_lang_code !='en')
                     @php 
                       $mucucat= 'menu_name_'.$mer_selected_lang_code;
                       $pro_title= 'pro_title_'.$mer_selected_lang_code;
                   @endphp
                   @endif

                   @foreach($getdesart as $key=>$value)

                   
                   @php
                    if($value->pro_status==1){
                    $status = trans(Session::get('mer_lang_file').'.MER_ACTIVE');
                     $dstatus = 'Active';
                    $status_class = 'status_active';
                    } else {
                    $status = trans(Session::get('mer_lang_file').'.MER_INACTIVE');
                    $dstatus = 'Inactive';
                    $status_class = 'status_deactive';
					
                    }
					//echo '<pre>';print_r($value->pro_mr_id);die;
					$catdata = Helper::getProduckInfoAttribute($value->attribute_id);
					$extradata = Helper::getFabricName($value->pro_id,$value->pro_mr_id);
					//echo '<pre>';print_r($extradata);die;
                    @endphp 
 
                  <div class="tr">
                    <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NAME') }} @endif">{{$value->$pro_title or ''}}</div>
                    <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif"><img src="{{$value->pro_Img or ''}}" width="150" alt=""  /></div>
                    
					
					<div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.CATEGORY') }} @endif">{{$catdata->attribute_title or ''}}</div>
					<div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_QUANTITY') }} @endif">{{$value->pro_qty or ''}}</div>
					<div class="td td5" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_PR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PR') }} @endif">
					<?php if(isset($extradata->discount_price) && $extradata->discount_price!='0'){echo number_format(@$extradata->discount_price,2);}else{echo number_format(@$extradata->value,2);} ?>
					</div>
					<div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif">{{ Carbon\Carbon::parse($value->updated_at)->format('d M Y')}}</div>
                    <div class="td td7" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif"> <span class="<?php echo $status_class;?>  status_active2 cstatus" data-status="<?php echo $dstatus;?>"data-id="{{ $value->pro_id }}">{{ $status }}</span></div>
                    <div class="td td8" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif"><a href="{{ route('dessert-add-type',['services_id' => $services_id,'shopid'=>$shopid,'itemid' =>$branch_id,'autoid'=>$value->pro_id]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EDIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EDIT') }} @endif</a></div>
                  </div>
                  @endforeach
 
                </div>

                @endif


                <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
                <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
              </div>
               {{ $getdesart->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!-- <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>

<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =$(this).data("status");
 var id =$(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

$('.status_yes').click(function(){
   var id =$(this).attr("data-id");
    var status1 =$(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

    jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'buffetdish'},
        success: function(data) {
         
            if(data==1){

               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
            $('form[name=test]').submit();
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script>

@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $dessert_inner_leftmenu =1; $attribute_id = '';@endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        @if(request()->autoid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Add_Type')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Add_Type') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Add_Type') }} @endif</h5>
        @else
  <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Update_Type')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Update_Type') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Update_Type') }} @endif</h5>
        @endif
      </header>
      @include('sitemerchant.includes.language')
      <!-- Display Message after submition -->
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">

                <form name="type" id="type" method="post" action="{{ route('update-dessart-type') }}" enctype="multipart/form-data">
                  <input type="hidden" name="services_id" value="{{$services_id}}">
                  <input type="hidden" name="shopid" value="{{$shopid}}">
                  <input type="hidden" name="branch_id" value="{{$branch_id}}">
                  <input type="hidden" name="proid" value="{{$product_id or ''}}">
               {{ csrf_field() }}
              <div class="one-call-form">
              <div class="form_row">
			   <div class="form_row_left">
              <label class="form_label">
              <span class="english">@if (Lang::has(Session::get('mer_lang_file').'.MER_TITLE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_TITLE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_TITLE') }} @endif</span>
              <span class="arabic ar">اسم</span>
              </label>
<div class="info100" >
              <div class="english">
              <input class="english" name="title" maxlength="50" value="{{$getdesart->pro_title or ''}}"   type="text">
              </div>
              <div class="arabic ar">
              <input class="arabic ar" name="title_ar" maxlength="50" value="{{$getdesart->pro_title_ar or ''}}"   type="text">
              </div>
</div></div>

              </div>

              <div class="form_row common_field">
			   <div class="form_row_left">
              <label class="form_label">
              <span class="english">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</span>
              <span class="arabic ar">صورة</span>
              </label>
              <div class="info100"> 
              <div class="input-file-area">
              <label for="company_logo">
              <div class="file-btn-area">
              <div id="file_value1" class="file-value"></div>
              <div class="file-btn">
              <span >@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</span>

              </div>
              </div>
              </label>
              <input id="company_logo" name="pro_img" class="info-file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" class="info-file" value="" type="file">
              </div>


              @if(isset($getdesart->pro_Img) && $getdesart->pro_Img!='')<div class="form-upload-img">
                
              <img src="{{ $getdesart->pro_Img or '' }}"> </div>
              @endif
             
              </div>
            </div>     </div>	


 
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->







            				  
              @php
              $getAttr_info ='';
              if(count( $getproductOption) >=1)
              {
              $getAttr_info = $getdesart->attribute_id;
              $discount = '';
              $product_option_value_id ='';
              $product_option_id ='';
              $discount_one ='';
              $product_option_value_id_one ='';
              $product_option_id_one ='';
              $discount_alt = '';
              $product_option_value_id_alt = '';
 
              if($getAttr_info == '45')
              {
              $discount = $getproductOption[0]->discount;
              $product_option_value_id = $getproductOption[0]->value;
              }

              if($getAttr_info == '44')
              {
              if($getproductOption[0]->product_option_id=='21')
              {
              $discount_alt = $getproductOption[0]->discount;
              $product_option_value_id_alt = $getproductOption[0]->value;
              $product_option_id = $getproductOption[0]->product_option_id;
              $discount_one = $getproductOption[1]->discount;
              $product_option_value_id_one = $getproductOption[1]->value;
              $product_option_id_one = $getproductOption[1]->product_option_id;
              }
              else
              {
              $discount_alt = $getproductOption[1]->discount;
              $product_option_value_id_alt = $getproductOption[1]->value;
              $product_option_id = $getproductOption[1]->product_option_id;
              $discount_one = $getproductOption[0]->discount;
              $product_option_value_id_one = $getproductOption[0]->value;
              $product_option_id_one = $getproductOption[0]->product_option_id;  
              }
 
              } 
              }
              @endphp

              <div class="form_row common_field" >
			     <div class="form_row_left">
              <label class="form_label">
              <span class="english">@if (Lang::has(Session::get('mer_lang_file').'.TYPEOFDISH')!= '') {{  trans(Session::get('mer_lang_file').'.TYPEOFDISH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.TYPEOFDISH') }} @endif </span>
              </label>
              <div class="info100">           
            <div class="save-card-line">
            <input name="attribute_id" value="44" @if($getAttr_info=='44') CHECKED @endif class="attri" id="attri0" data-name="Type" type="radio">
            <label for="attri0"> <span class="cat_face">@if (Lang::has(Session::get('mer_lang_file').'.Type')!= '') {{  trans(Session::get('mer_lang_file').'.Type') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Type') }} @endif</span> </label>
            </div>
            <div class="save-card-line">
            <input name="attribute_id" value="45" @if($getAttr_info=='45') CHECKED @endif class="attri" id="attri1" data-name="Dish" type="radio">
            <label for="attri1"> <span class="cat_face">@if (Lang::has(Session::get('mer_lang_file').'.Dish')!= '') {{  trans(Session::get('mer_lang_file').'.Dish') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Dish') }} @endif</span> </label>
            </div>
            <span for="attribute_id" generated="true" class="error"></span>      
            </div>
              <div class="form_labs type lbs " @if(isset($getAttr_info) && $getAttr_info !='44') style="display: none;" @endif>
 
              <div class="form_tabs"> 

              <div class="per_box">
              <div class="perarea">
              <div class="per_left">@if (Lang::has(Session::get('mer_lang_file').'.Per_KG')!= '') {{  trans(Session::get('mer_lang_file').'.Per_KG') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Per_KG') }} @endif</div>
              <div class="per_right"> <input class="perkg notzero" name="type_perkg" onkeypress="return isNumber(event)" maxlength="7" value="{{$product_option_value_id_alt  or ''}}"  type="text"></div>
              </div>
              <div class="perarea">
              <div class="per_left">@if (Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISCOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT') }} @endif</div>
              <div class="per_right"> <input class="perkg notzero" onkeypress="return isNumber(event)" name="type_perkg_discount" maxlength="2" value="{{$discount_alt  or ''}}"  type="text"></div>       
              </div></div> 


 <div class="per_box" style="display: none;">
              <div class="perarea">
              <div class="per_left">@if (Lang::has(Session::get('mer_lang_file').'.Per_Pieces')!= '') {{  trans(Session::get('mer_lang_file').'.Per_Pieces') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Per_Pieces') }} @endif </div>
              <div class="per_right"> <input class="perkg notzero" onkeypress="return isNumber(event)" name="type_perpiece" maxlength="7" value="{{$product_option_value_id_one  or ''}}"  type="text"></div>       
              </div>
              <div class="perarea">
              <div class="per_left">@if (Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DISCOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DISCOUNT') }} @endif</div>
              <div class="per_right"> <input class=" perkg notzero" onkeypress="return isNumber(event)" name="type_perpiece_discount" maxlength="2" value="{{$discount_one  or ''}}"  type="text"></div>
              </div>
              </div> 


              </div> 
              </div> 
              <div class="form_labs dish" @if(isset($getAttr_info) &&  $getAttr_info !='45') style="display: none;" @endif>
               <div class="form_tabs">
              <div class="per_box">

              <div class="perarea">
              <div class="per_left">@if (Lang::has(Session::get('mer_lang_file').'.Per_Pieces')!= '') {{  trans(Session::get('mer_lang_file').'.Per_Pieces') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Per_Pieces') }} @endif </div>
              <div class="per_right"> <input class="perkg notzero" onkeypress="return isNumber(event)" name="dish_per_qty" maxlength="7" value="{{$product_option_value_id or ''}}"   type="text"></div>
 

              </div>



              <div class="perarea">
              <div class="per_left">@if (Lang::has(Session::get('mer_lang_file').'.Discount')!= '') {{  trans(Session::get('mer_lang_file').'.Discount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Discount') }} @endif %</div>
              <div class="per_right"> <input class=" perkg notzero" onkeypress="return isNumber(event)" name="dish_per_qty_discount" maxlength="2" value="{{$discount  or ''}}"   type="text"></div>
              </div>
              </div>	
              </div>
              </div>
              <div class="form_labs ">

              <div class="form_tabs">
              <div class="per_box">
              <div class="perarea">
              <div class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.Available_Qty')!= '') {{  trans(Session::get('mer_lang_file').'.Available_Qty') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Available_Qty') }} @endif </div>
              <div class="info100"> <input class="perkg notzero" name="aval_qty" onkeypress="return isNumber(event)" maxlength="7" value="{{$getdesart->pro_qty or ''}}" required="" type="text"></div> 
              <span for="aval_qty" generated="true" class="error"></span>
              </div>          
              </div>

              </div>
              </div>

              <div class="form_labs ">

              <div class="form_tabs">
              <div class="per_box">
              <div class="perarea">
              <div class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.PRODUCT_WEIGHT')!= '') {{  trans(Session::get('mer_lang_file').'.PRODUCT_WEIGHT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PRODUCT_WEIGHT') }} @endif </div>
              <div class="info100"> <input class="perkg notzero" name="pro_weight" onkeypress="return isNumberKey(event,this)" maxlength="7" value="{{$getdesart->pro_weight or ''}}" required="" type="text"></div> 
             
              </div>          
              </div>

              </div>
              </div>







 </div>	
              </div>	
			  
			  
			  
			   
            <div class="form_row">
			 <div class="form_row_left">
              <label class="form_label">

<span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>

             <div class="info100">
        
              <div class="english">          
              <textarea name="pro_description" maxlength="500">{{$getdesart->pro_desc  or ''}}</textarea>
              </div>
              <div class="arabic ar">
             
              <textarea name="pro_description_ar"  maxlength="500">{{$getdesart->pro_desc_ar or ''}}</textarea>

              </div>

</div>
              </div> </div>

    <div class="form_row">
	 <div class="form_row_left">
              <div class="english">
              <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
              </div> 
              <div class="arabic ar">
              <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
              </div>	    
     </div>
   </div>
              </div>


              <!-- one-call-form -->
            </div>
            <!-- box -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('sitemerchant.includes.footer')


<script>
  $(function() {
  $('.attri').click(function() {
  var datavalid = $(this).data('name');
  if(datavalid=='Dish')
  {
  $('.dish').show();
  $('.type').hide();
  }
  else
  {
  $('.dish').hide();
  $('.type').show();
  }
  });
  });
</script>


<script type="text/javascript">

$("#type").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       pro_description: {
                       required: true,
                      },
                       pro_description_ar: {
                       required: true,
                      },
 @if(isset($getdesart->pro_Img) && $getdesart->pro_Img!='')
                      pro_img: {                                            
                       required: false,
                       accept:'png|jpeg|gif|jpg',                  
                      },
@else
                    pro_img: {                                            
                    required: true,
                    accept:'png|jpeg|gif|jpg',                  
                    },

@endif
                      aval_qty: {
                       required: true,
                      },
                      attribute_id: {
                       required: true,
                      },
 
                      
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
               required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_TITLE'); @endphp",
                      },              
                title_ar: {
             required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_TITLE'); @endphp",
                      },
              pro_description: {
               required:  "@php echo lang::get('mer_en_lang.PLEASE_ENTER_DESCRIPTION'); @endphp",
                      },              
                pro_description_ar: {
             required:  "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_DESCRIPTION'); @endphp",
                      },
                        pro_img: {
             required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
             accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                      },
                        aval_qty: {
             required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_QUANTITY') }} @endif",
                      },

                     attribute_id: {
             required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_Choose_Type')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_Choose_Type') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_Choose_Type') }} @endif",
                      },  
                                  
                },
                invalidHandler: function(e, validation){
 
                    var valdata=validation.invalid;
                                  @if($mer_selected_lang_code !='en')

                                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  }                    
                                  if (typeof valdata.pro_description != "undefined" || valdata.pro_description != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.aval_qty != "undefined" || valdata.aval_qty != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.pro_img != "undefined" || valdata.pro_img != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 
if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 


                                  if (typeof valdata.pro_description_ar != "undefined" || valdata.pro_description_ar != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 


                                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                                  {

                                  $('.arabic_tab').trigger('click');     

                                  }
                                  @else



                                  if (typeof valdata.pro_description_ar != "undefined" || valdata.pro_description_ar != null) 
                                  {
                                  $('.arabic_tab').trigger('click'); 
                                  } 


                                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                                  {

                                  $('.arabic_tab').trigger('click');     

                                  }
                                  if (typeof valdata.attribute_id != "undefined" || valdata.attribute_id != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 

                                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  }                    
                                  if (typeof valdata.pro_description != "undefined" || valdata.pro_description != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.aval_qty != "undefined" || valdata.aval_qty != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 
                                  if (typeof valdata.pro_img != "undefined" || valdata.pro_img != null) 
                                  {
                                  $('.english_tab').trigger('click'); 
                                  } 


                                  @endif
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
 /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

 function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
</script> 



<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->

@include('sitemerchant.includes.header') 
@php $dessert_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_BRANCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_BRANCH') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_BRANCH') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <form name="form1" method="post" id="addbranch" action="{{ route('storebranch') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_HOTEL_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_HOTEL_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <input class="english" type="text" readonly="" name="hotel1" value="{{ $alldata->mc_name or '' }}" id="hotal" >
                      <input class="arabic ar" type="text" readonly="" name="hotel1" value="{{ $alldata->mc_name_ar  or '' }}" id="hotal" >
                      <input type="hidden" name="hotel" value="{{ $alldata->mc_id  or ''}}" id="hotal">
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_CITY'); @endphp </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="city_id"  id="city_id">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_CITY') }}  
                        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELEST_CITY') }} @endif</option>
                        
                        
                        @php
$city = array();
                        @endphp
                        
                      @foreach ($city as $val)


                  @php $ci_name='ci_name'@endphp
                   @if($mer_selected_lang_code !='en')
                                         @php $ci_name= 'ci_name_'.$mer_selected_lang_code; @endphp
                   @endif    
                      
                        
                        
                        
                        <option value="{{ $val->ci_id or '' }}">{{ $val->$ci_name or '' }}</option>
                        
                        
                        
                        
                      @endforeach

                    
                      
                      
                      
                      </select>
                    </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SELEST_MANAGER'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SELEST_MANAGER'); @endphp </span> </label>
                    <div class="info100">
                      <select class="small-sel" name="manager"  id="manager">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_MANAGER') }}  
                        @else  {{ trans($MER_SELEST_MANAGER.'.MER_SELEST_MANAGER') }} @endif</option>
                        
                        
                           @php
$manager = array();
                        @endphp
                        
                      @foreach ($manager as $val)
                      
                        
                        
                        
                        <option value="{{ $val->mer_id or '' }}">{{ $val->mer_fname  or ''}}</option>
                        
                        
                        
                        
                      @endforeach

                    
                      
                      
                      
                      </select>
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" maxlength="40" name="mc_name" value=""  class="english" id="hotal_name" >
                      </div>
                      <div  class="arabic ar" >
                        <input id="ssb_name_ar" class="arabic ar"  maxlength="40"  name="mc_name_ar"  type="text" onChange="check();">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESS'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" maxlength="40" name="mc_name" value=""  class="english" id="hotal_name" >
                      </div>
                      <div  class="arabic ar" >
                        <input id="ssb_name_ar" class="arabic ar"  maxlength="40"  name="mc_name_ar"  type="text" onChange="check();">
                      </div>
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Insuranceamount'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Insuranceamount'); @endphp </span> </label>
                    <div class="ensglish">
                      <input type="text" class="small-sel" name="Insuranceamount"  maxlength="15"   data-validation="length required" 
                    data-validation-error-msg="يرجى إدخال اسم القاعة" value="{{$getDb->Insuranceamount or ''}}" data-validation-length="max35">
                    </div>
                    @if($errors->has('Insuranceamount')) <span class="error"> {{ $errors->first('Insuranceamount') }} </span> @endif </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp </span> <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a> </label>
                    <div class="info100">
                      <input type="url" name="google_map_address"  maxlength="150"  data-validation="length required" 
		 data-validation-error-msg="Please enter google map address"  value="{{$getDb->google_map_address or ''}}" data-validation-length="max100">
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" required="" value="">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="address_image_tooltip">
	<span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span>
</a>
</label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="hall_addressimg" id="company_logo1" class="info-file">
                    </div>
                    <div class="form-upload-img">@if(isset($getDb->hall_address_image) && $getDb->hall_address_image !='') <img src="hallpics/thumb_{{$getDb->hall_address_image}}" width="150" height="150"> @endif</div>
                    @if($errors->has('hall_addressimg')) <span class="error"> {{ $errors->first('hall_addressimg') }} </span> @endif </div>
                  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.TERMSANDCONDITIONS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.TERMSANDCONDITIONS'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo2">
                        <div class="file-btn-area">
                          <div id="file_value3" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo2" name="mc_tnc" class="info-file" type="file" required="" value="">
                      </div>
                      <span class="certifications error"></span> </div>
                  </div>
                </div>
                <div class="form_row arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                <div class="form_row english">
                  <input type="submit" name="submit" value="Submit">
                </div>
                <!-- form_row -->
              </form>
            </div>
            <!-- one-call-form --> 
          </div>
        </div>
      </div>
    </div>
    <!-- global_area --> 
  </div>
</div>
<!-- right_panel -->
</div>
<!-- merchant_vendor --> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  city_id: {
                       required: true,
                      },

                       manager: {
                       required: true,
                      },

                       mc_name: {
                       required: true,
                      },

                      mc_name_ar: {
                       required: true,
                      },
                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                      mc_img: {
                           required:true,
                           accept:"png|jpe?g|gif",
                      },
                        mc_tnc: {
                           required:true,
                           
                      },
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             city_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif",
                      },  

                 manager: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_MANAGER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MANAGER_AR') }} @endif",
                      },  
                         mc_tnc: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_TNC')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_TNC') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_TNC') }} @endif",
                      },

                         mc_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_BRANCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_BRANCH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_BRANCH') }} @endif",
                      },   

                          mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_BRANCH_AR'); @endphp",
                      },   


                       description: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ADDRESS') }} @endif ",
                      },
  
                       description_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ADDRESS'); @endphp",
                      },
                        mc_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },                       
                     
                },

                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                @if($mer_selected_lang_code !='en')
                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
          @else


             if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
 
            if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                  

@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });















</script> 
@include('sitemerchant.includes.footer') 
<script>
jQuery("#company_logo2").change(function(){  
 var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        jQuery("#file_value3").html('');    
        jQuery(".certifications").html('Please upload PDF file');

         }
         else
         {
         //jQuery("#file_value3").html(fake); 
         }



});


</script> 
@include('sitemerchant.includes.header') 
@php $dessert_leftmenu =1; @endphp  
<div class="merchant_vendor">     
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
    <div class="right_panel">
      <div class="inner">
	  <header>
            <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_MANAGER') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_MANAGER') }} @endif </h5>
            @include('sitemerchant.includes.language')
            </header>
			<div class="global_area">
        <div class="row">
        <div class="col-lg-12">
          <div class="box">
            

        
      <!-- Display Message after submition -->
      @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
             <div class="one-call-form">

                   <form name="form1" id="addmanager" method="post" action="{{ route('storemanager') }}" enctype="multipart/form-data">
           
                         <div class="form_row">
                       <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_NAME'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_NAME'); @endphp </span>
 </label>
     <input type="text" class="english" name="mer_fname" maxlength="40" value="{!! Input::old('mer_fname') !!}" id="mer_fname" required="" >


<input type="text" class="arabic ar" name="mer_fname_ar" maxlength="40" value="{!! Input::old('mer_fname_ar') !!}" id="mer_fname_ar" required="" >



					   </div> <!-- form_row -->
					    <!-- form_row -->
					   <div class="form_row">
                        <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_Email_Address'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Email_Address'); @endphp </span>
 </label>
                       <div class="info100"><input maxlength="60"  type="email" name="mer_email" value="" id="email_id" onchange="check_email();" required="" > 
                        <div id="email_id_error_msg"  style="color:#F00;font-weight:800"> </div></div>
                      </div> <!-- form_row -->
					   <div class="form_row">
                        <label class="form_label">

<span class="english">@php echo lang::get('mer_en_lang.MER_CUSTOMERMOBILE'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CUSTOMERMOBILE'); @endphp </span>
 </label>
                      <div class="info100"><input class="small-sel" maxlength="12"  type="text" name="mobile" value="{!! Input::old('mobile') !!}" id="mobile"  required="" ></div> 
                       </div> <!-- form_row -->
					   <div class="form_row">
                        <label class="form_label">

<span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp </span>

                         </label>
                        <div class="info100"><input class="small-sel" maxlength="40"  type="text" name="city" value="{!! Input::old('city') !!}" id="city"  required="" ></div> 
                       </div> <!-- form_row -->
					   <div class="form_row">
                       <label class="form_label">

<span class="english">@php echo lang::get('mer_en_lang.MER_PASSWORD'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PASSWORD'); @endphp </span>

                         </label>
                       <div class="info100"><input type="password" maxlength="20"  class="small-sel" name="mer_password" value="" id="mer_password" required="" ></div> 
					   </div> <!-- form_row -->
                       
					   <div class="form_row">
                       <label class="form_label">
                        <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span>
 </label>
                      <div class="info100"> 
					  <div class="input-file-area">
                    <label for="company_logo">
                    <div class="file-btn-area">
                      <div id="file_value1" class="file-value"></div>
                      <div class="file-btn">
   <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>
 </div>
                    </div>
                    </label>
                    <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                  </div>
					  </div>
					  </div> <!-- form_row -->


              <div class="form_row">
              <label class="form_label">
              	<span class="english">@php echo lang::get('mer_en_lang.UPLOADCERTIFICATE'); @endphp</span>
              	<span  class="arabic ar"> @php echo lang::get('mer_ar_lang.UPLOADCERTIFICATE'); @endphp </span>
              </label>

              <div class="info100">
              <div class="input-file-area">
              <label for="company_logo1">
              <div class="file-btn-area">
              <div id="file_value2" class="file-value"></div>
              <div class="file-btn">
    <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
              <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>
 </div>
              </div>
              </label>
              <input id="company_logo1" name="storcertifiction_img" class="info-file" type="file" value="">
              </div>
<div class="error certifications"> </div>
              </div>  
              </div>  <!-- form_row -->


     		<div class="form_row english">
                       <input type="submit" name="submit" value="Submit">
               </div> <!-- form_row -->
			<div class="form_row arabic ar">
                       <input type="submit" name="submit" value="خضع">
					     </div> <!-- form_row -->
                  </form>
           
        </div>   <!-- one-call-form -->
 

      <!--PAGE CONTENT PART WILL COME INSIDE IT START-->


        <!--PAGE CONTENT PART WILL COME INSIDE IT END-->

          </div>
        </div>
        </div>
		</div><!-- global_area -->
      </div>
    </div><!-- right_panel -->
    </div> <!-- merchant_vendor -->
    <script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script>

 <script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
  
$("#addmanager").validate({
               rules: {
                  mer_fname: {
                       required: true,
                      },

                       mer_lname: {
                       required: true,
                      },
                      mer_fname_ar: {
                       required: true,
                      },

                       mer_lname_ar: {
                       required: true,
                      },

                       mer_email: {
                       required: true,
                      },

                      mobile: {
                       required: true,
                      },
                       city: {
                       required: true,
                      },
                      pa: {
                       required: true,
                      },
                       mer_password: {
                       required: true,
                      },

                      stor_img: {
                           accept:"png|jpe?g|gif",
                      },
                 
 storcertifiction_img: {
                           accept:"pdf",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mer_fname: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_FIRST_NAME') }} @endif",
                      },  

                 mer_lname: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_LAST_NAME') }} @endif",
                      },  


          mer_fname_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_FIRST_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_FIRST_NAME') }} @endif",
                      },  

                 mer_lname_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_LAST_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_LAST_NAME') }} @endif",
                      }, 


                mer_email: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EMAIL') }} @endif",
                      },   

               mobile: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MOBILE') }} @endif",
                      },   


                       city: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY_TEXT') }} @endif ",
                      },

                   mer_password: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PASSWORD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PASSWORD') }} @endif ",
                      },
     

                         stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },   
                       
                    storcertifiction_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_PDF_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_PDF_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_PDF_FILE') }} @endif"
                      },                        
                                        
                     
                },             
          invalidHandler: function(e, validation){
                  
                    var valdata=validation.invalid;
                    if (typeof valdata.mer_fname != "undefined" || valdata.mer_fname != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                    else if (typeof valdata.mer_lname != "undefined" || valdata.mer_lname != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                     else if (typeof valdata.mer_email != "undefined" || valdata.mer_email != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                     else if (typeof valdata.mobile != "undefined" || valdata.mobile != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                    else if (typeof valdata.city != "undefined" || valdata.city != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     else if (typeof valdata.pa != "undefined" || valdata.pa != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     else if (typeof valdata.mer_password != "undefined" || valdata.mer_password != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    
                    else if (typeof valdata.mer_fname_ar != "undefined" || valdata.mer_fname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                  else if (typeof valdata.mer_lname_ar != "undefined" || valdata.mer_lname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                      


                    },








                submitHandler: function(form) {
                    form.submit();
                }
            });
  $("#company_logo1").change(function () {
        var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   $("#file_value2").html('');         
$(".certifications").html('Only PDF format allowed.');

         }
    });


</script>

@include('sitemerchant.includes.footer')
 @inject('data','App\Help')
@include('sitemerchant.includes.header') 
@php $dessert_inner_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Container')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container') }}  
          @else {{ trans($MER_OUR_LANGUAGE.'.MER_Container') }} @endif </h5>
        <div class="add"><a  href="{{ route('dessert-add-container',['id' => request()->id]) }}">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CONTAINER') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CONTAINER') }} @endif</a></div>
      </div>
      <!-- Display Message after submition -->
      {!! Form::open(array('url'=>'sitemerchant_dashboard','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!} <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
      <div class="filter_area">
        <div class="filter_left">
          <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>
          <div class="search-box-field mems ">
            <div class="search-box-field mems"> @php $statuss = request()->status; $searchh= request()->search; @endphp
              <select name="status" id="status">
                <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_STATUS') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_STATUS') }} @endif </option>
                <option value="1" @if(isset($statuss) && $statuss=='1') {{"SELECTED"}}  @endif> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</option>
                <option value="0" @if(isset($statuss) && $statuss=='0') {{"SELECTED"}}  @endif > @if (Lang::has(Session::get('mer_lang_file').'.MER_DEACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DEACTIVE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DEACTIVE') }} @endif</option>
              </select>
              <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
            </div>
            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />
          </div>
        </div>
        <div class="search_box">
          <div class="search_filter">&nbsp;</div>
          <div class="filter_right">
            <input name="search" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="" />
            <input type="button" name="search" class="icon_sch" id="submitdata" />
          </div>
        </div>
      </div>
      <!-- filter_area -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap">
              <div class="panel-body panel panel-default">
                <div class="table dessert-container-table">
                  <div class="tr">
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Name') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_No_People')!= '') {{  trans(Session::get('mer_lang_file').'.MER_No_People') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_No_People') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div>
                  </div>
                  <div class="tr">
                    <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Name') }} @endif">A</div>
                    <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_No_People')!= '') {{  trans(Session::get('mer_lang_file').'.MER_No_People') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_No_People') }} @endif">23</div>
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_LAST_MODIFIED_DATE') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_LAST_MODIFIED_DATE') }} @endif">May 14, 2018 </div>
                    <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif"><img src="{{url('')}}/public/assets/img/dish.jpg" alt="" /></div>
                    <div class="td td5" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_STATUS') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_STATUS') }} @endif"><span class="status_active"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ACTIVE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTIVE') }}  
                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTIVE') }} @endif</span></div>
                    <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif"><a href="#">Edit</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <!--  <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div> -->
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a><a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup -->
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =$(this).data("status");
 var id =$(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     $('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

$('.status_yes').click(function(){
   var id =$(this).attr("data-id");
    var status1 =$(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     $.ajax({
        type: "GET",
        url: "{{ route('menu-hall-container-status') }}",
        data: {activestatus:activestatus,id:id},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script>
<script type="text/javascript">
      
          $(document).ready(function(){
            $('form[name=test]').submit();
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });

    </script>
@include('sitemerchant.includes.footer')
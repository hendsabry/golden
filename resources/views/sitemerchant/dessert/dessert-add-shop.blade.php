@include('sitemerchant.includes.header') 
@php $dessert_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->shopid =='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_SHOP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_SHOP') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_SHOP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_SHOP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_SHOP') }} @endif </h5>
        @endif
        
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> {!! Form::open(array('url'=>'dessert-add-update-shop','id'=>'addhotel','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
              <div class="one-call-form">
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                    <input type="hidden" name="service_id" id="service_id" value="{{$services_id}}">
                    <input type="hidden" name="shop_id" id="shop_id" value="{{$shopid}}">
                    <div class="info100">
                      <div class="english">
                        <input type="text" name="mc_name" id="mc_name" value="{{$alldata->mc_name or ''}}" id="hotal_name"  class="english"  data-validation-length="max80" maxlength="40">
                      </div>
                      <div class="arabic ar">
                        <input id="ssb_name_ar" class="arabic ar" name="mc_name_ar"  id="mc_name_ar"   type="text" data-validation-length="max80"  maxlength="40" value="{{$alldata->mc_name_ar or ''}}" >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form_row common_field">
                  <div class="form_row_left">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_IMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="mc_img" class="info-file" type="file" >
                        @if(isset($alldata->mc_img) && $alldata->mc_img!='')
                        <div class="form-upload-img"> <img src="{{$alldata->mc_img}}"> </div>
                        @endif </div>
                    </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <div class="english">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </div>
              {{ Form::close() }} </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>       
$("form").data("validator").settings.ignore = "";
 </script>
<script type="text/javascript">
$("#addhotel").validate({
                  ignore: [],
                  rules: {
                  mc_name: {
                       required: true,
                      },

                       mc_name_ar: {
                       required: true,
                      },
                    @if(isset($alldata->mc_img) && $alldata->mc_img!='')
                    mc_img: {
                    required: false,
                    accept:"png|jpe?g|gif",
                    },
                    @else
                    mc_img: {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },

                    @endif     

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             mc_name: {
                 required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_SHOP_NAME'); @endphp",
                      },  

                 mc_name_ar: {
               required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_SHOP_NAME'); @endphp",
                      },    
                    
                        mc_img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },                       
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


@if($mer_selected_lang_code !='en')

                    if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click'); 
                             

                    }

@else
  if (typeof valdata.mc_name_ar != "undefined" || valdata.mc_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     }

                      if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click'); 
                             

                    }

if (typeof valdata.mc_name != "undefined" || valdata.mc_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                        $('.english_tab').trigger('click');     

                    }
                     if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                    {

                        $('.english_tab').trigger('click');     

                    }

@endif
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $Eventbig_leftmenu =1; $cid = request()->cid;
$id = request()->id;
$shopid = request()->shopid; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(isset($cid) && $cid !='')
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.UPDATE_ITEM')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_ITEM') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_ITEM') }} @endif </h5>
        @else
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.ADD_ITEM')!= '') {{  trans(Session::get('mer_lang_file').'.ADD_ITEM') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADD_ITEM') }} @endif </h5>
        @endif
        
        
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <form name="form1"  id ="menucategory" method="post" action="{{ route('event-updateitems') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label "> <span class="english">@php echo lang::get('mer_en_lang.CATEGORY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.CATEGORY'); @endphp </span> </label>
                    @php               
                    $SecondCats=Helper::getAttributes($shopid);
                    @endphp
                    <div class="info100">
                      <select name="attribute">
                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif</option>
                                              
                        
                    @foreach($SecondCats as $val)                  
                        
                        
                        <option value="{{$val->id}}" @if($val->id == $getAttr->attribute_id ) selected="selected"  @endif  >@if($mer_selected_lang_code !='en') {{$val->attribute_title_ar}} @else {{$val->attribute_title}}  @endif</option>
                        
                        
                        
                    @endforeach
                    
                      
                      
                      </select>
                    </div>
                  </div>
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.ITEMNAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.ITEMNAME'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" id="itemname" maxlength="90"  name="itemname"  value="{{$getAttr->pro_title or ''}}"  required=""  type="text" >
                      </div>
                      <div class="arabic ar">
                        <input class="arabic ar" id="itemname_ar" maxlength="90"  name="itemname_ar"  value="{{$getAttr->pro_title_ar or ''}}"  required=""  type="text" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PRICE'); @endphp </span> </label>
                    <div class="info100">
                      <input id="price" class="xs_small" onkeypress="return isNumber(event);" maxlength="10"  name="price" value="{{$getAttr->pro_price or ''}}"  required=""  type="text" >
                    </div>
                  </div>

                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="{{$getAttr->pro_discount_percentage or ''}}" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>

                  <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="{{ $getAttr->pro_qty or '' }}"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>


                    <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.PRODUCT_WEIGHT'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PRODUCT_WEIGHT'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumberKey(event,this)" name="pro_weight" id="pro_weight" maxlength="15" value="{{ $getAttr->pro_weight or '' }}"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>



                  <div class="form_row_left common_field">
                    <input type="hidden" name="mc_id" value="">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.UPLOADIMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.UPLOADIMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="uploadimage"   class="info-file rimage" type="file"  >
                        <input type="hidden" name="updateimage" value="{{ $getAttr->pro_Img or ''}}">
                        @if(isset($getAttr->pro_Img) && $getAttr->pro_Img !='') <div class="form-upload-img"><img src="{{$getAttr->pro_Img}}" width="150" height="150"></div> @endif
                      </div>
                    </div>
                  </div>



<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->

                   <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100" >
                      <div class="english">
                        <textarea class="english" maxlength="500" name="about" id="about" rows="4" cols="50">{{ $getAttr->pro_desc or ''}} </textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea class="arabic ar" name="about_ar" maxlength="500" id="about_ar " rows="4" cols="50">{{ $getAttr->pro_desc_ar or ''}}</textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left">
                    <input type="hidden" name="cid" value="{{$cid or ''}}">
                    <input type="hidden" name="id" value="{{$id or ''}}">
                    <input type="hidden" name="shopid" value="{{$shopid or ''}}">
                    @if($cid=='')
                    <div class="english">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="خضع">
                    </div>
                    @else
                    <div class="english">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="Update">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" id="hallsubmit" name="addhallpics" value="تحديث">
                    </div>
                    @endif </div>
                </div>
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
 
$("#menucategory").validate({
                  ignore: [],
                  rules: {

                    itemname: {
                       required: true,
                      },
                     itemname_ar: {
                       required: true,
                      },
                     attribute: {
                     required: true,
                    },
                    price: {
                     required: true,
                    }, 

                     about: {
                       required: true,
                      },
                    about_ar: {
                       required: true,
                      },                   
                    quantity: {
                       required: true,
                      },

                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
             messages: {
             itemname: {
               required: "{{ trans('mer_en_lang.MER_VALIDATION_ITEM_NAME') }}",
                      }, 
               itemname_ar: {
               required: "{{ trans('mer_ar_lang.MER_VALIDATION_ITEM_NAME_AR') }}",
                      }, 
               attribute: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ATTRIBUTE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_ATTRIBUTE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ATTRIBUTE')}} @endif",
                      },            
               price: {
               required: " @if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE')}} @endif ",
                      }, 

                   about: {

                         required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); @endphp",
               
                      },
  
                       about_ar: {
                  required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT'); @endphp",
                      },  

                    quantity: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY') }} @endif ",
                      },      
              
                                                            
                     
                },
                invalidHandler: function(e, validation){
                    //console.log("invalidHandler : event", e);
                    //console.log("invalidHandler : validation", validation);

                    var valdata=validation.invalid;
                    console.log("invalidHandler : validation", valdata);

                    @if($mer_selected_lang_code !='en')

                    
                     if (typeof valdata.itemname != "undefined" || valdata.itemname != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                       
                     if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                    
                      if (typeof valdata.uploadimage != "undefined" || valdata.uploadimage != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }                    
                  
                     if (typeof valdata.itemname_ar != "undefined" || valdata.itemname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                     if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    } 
                    
                     if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    } 
                    if (typeof valdata.attribute != "undefined" || valdata.attribute != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }  
                  @else

if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    } 
                    
                    if (typeof valdata.itemname != "undefined" || valdata.itemname != null) 
                    {
                      
                        $('.english_tab').trigger('click');     

                    }
                    
                     if (typeof valdata.itemname != "undefined" || valdata.itemname != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                     if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }    
                     if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }  
                    
                      if (typeof valdata.uploadimage != "undefined" || valdata.uploadimage != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }              

 if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
 if (typeof valdata.attribute != "undefined" || valdata.attribute != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    } 
                  @endif


                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });


var createValidation = function() {
       
         
@if(!isset($getAttr->pro_Img )) 



        $(".rimage").each(function() {         

            $(this).rules('remove');
            $(this).rules('add', {
             required: true,
             accept:'jpg|png|jpeg',
              messages: {
                required: " @if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE')}} @endif ",                          
               accept  :"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')}} @endif",
              }
            });
          });
@else   


$(".rimage").each(function() {
            $(this).rules('remove');
            $(this).rules('add', {
           
             accept:'jpg|png|jpeg',
              messages: {
                                       
               accept  :"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{ trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')}}  @else {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE')}} @endif",
              }
            });
          });    
        @endif


};

$(function() {
   createValidation();
});

</script>
<script type="text/javascript">
  

 function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
  
</script>


@include('sitemerchant.includes.footer')
<script type="text/javascript">
  jQuery("#company_logo").change(function(){
  var fake = this.value.replace("C:\\fakepath\\", ""); 
  var fileExtension = ['png','jpeg','jpg','gif'];
  $(".certifications").html('');
  if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
  jQuery("#file_value1").html('');         
  jQuery(".certifications").html('Please upload valid image');
  }
  else
  {
  jQuery("#file_value2").html(fake); 
  }
  });
</script>


<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
@if($mer_selected_lang_code !='en')
jQuery(window).load(function(){

  jQuery('.arabic_tab').trigger('click');
})
@endif

</script>

<!-- Add More product images end -->


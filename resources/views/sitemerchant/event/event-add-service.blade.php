@include('sitemerchant.includes.header') 
@php $Eventbig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_SERVICES') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_SERVICES') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->
              
                <form name="form1" id="addmanager" method="post" action="{{ route('storemanager') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  
                  <div class="form_row">
				  <div class="form_row_left">
                     <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.mer_service_name'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_service_name'); @endphp </span> 
                     </label>
                     <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="mer_lname" maxlength="40" value="{!! Input::old('mer_lname') !!}" id="mer_lname" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="arabic ar" name="mer_lname_ar" maxlength="40" value="{!! Input::old('mer_lname_ar') !!}" id="mer_lname_ar" required="" >
                      </div>
                    </div>
					</div>
						  <div class="form_row_right">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                    </div>
					</div>
                  </div>
                  <!-- form_row -->                                    
                  
             
                  
                  <div class="form_row common_field">
				  
					<div class="form_row_left">
                     <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="xs_small" name="mer_lname" maxlength="40" value="{!! Input::old('mer_lname') !!}" id="mer_lname" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="xs_small" name="mer_lname_ar" maxlength="40" value="{!! Input::old('mer_lname_ar') !!}" id="mer_lname_ar" required="" >
                      </div>
                    </div>
					</div>
                    
                    <div class="form_row_right">
                     <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.BACK_QUANTITY'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_QUANTITY'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="xs_small" name="mer_lname" maxlength="40" value="{!! Input::old('mer_lname') !!}" id="mer_lname" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="xs_small" name="mer_lname_ar" maxlength="40" value="{!! Input::old('mer_lname_ar') !!}" id="mer_lname_ar" required="" >
                      </div>
                    </div>
					</div>
                  </div>
                  <!-- form_row -->
                  
                  
                  
                  <!-- form_row -->
                  
                  <div class="form_row">
				  	
                    <!-- form_row --> 
						
					<div class="form_row_left">
					 <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp </span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp  </span> 
                     </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="xs_small" name="mer_lname" maxlength="40" value="{!! Input::old('mer_lname') !!}" id="mer_lname" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="xs_small" name="mer_lname_ar" maxlength="40" value="{!! Input::old('mer_lname_ar') !!}" id="mer_lname_ar" required="" >
                      </div>
                    </div>
				
					</div>
                    
                    <div class="form_row_right">
                   <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> 
                     </label>
                    <div class="info100">
                      
                      <div class="english">
                        <textarea name="description" class="english" id="description" rows="4" cols="50">we  </textarea> 
                      </div>      
                      
                      <div  class="arabic ar" >     
                              <textarea class="arabic ar" name="description_ar" id="description_ar" rows="4" cols="50">wq1wqwq1w</textarea>  
                      </div> 
                      
                    </div>
</div>
                  </div>
                  <!-- form_row -->

                  
                  <div class="form_row english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <!-- form_row -->
                  <div class="form_row arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  <!-- form_row -->
                </form>
           
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addmanager").validate({
              ignore: [],
               rules: {
                  mer_fname: {
                       required: true,
                      },

                       mer_lname: {
                       required: true,
                      },
                      mer_fname_ar: {
                       required: true,
                      },

                       mer_lname_ar: {
                       required: true,
                      },

                       mer_email: {
                       required: true,
                      },

                      mobile: {
                       required: true,
                      },
                       city: {
                       required: true,
                      },
                    
                       mer_password: {
                       required: true,
                      },

                      stor_img: {
                        required: true,
                           accept:"png|jpe?g|gif",
                      },
                 
                  storcertifiction_img: {
                    required: true,
                           accept:"pdf",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                
           messages: {
             mer_fname: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_FIRST_NAME'); @endphp",
                      },  

                 mer_lname: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_LAST_NAME'); @endphp",
                      },  


          mer_fname_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_FIRST_NAME_AR'); @endphp",
                      },  

                 mer_lname_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_LAST_NAME_AR'); @endphp",
                      }, 


                mer_email: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EMAIL') }} @endif",
                      },   

               mobile: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_MOBILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_MOBILE') }} @endif",
                      },   


                       city: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY_TEXT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY_TEXT') }} @endif ",
                      },

                   mer_password: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PASSWORD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PASSWORD') }} @endif ",
                      },
     

                         stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif ",
                      },   
                       
                    storcertifiction_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_PDF_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_PDF_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_PDF_FILE') }} @endif",
                                        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_FILE') }} @endif ",
                      },                        
                                        
                     
                },             
          invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                     console.log("invalidHandler : validation", valdata);

 
 @if($mer_selected_lang_code !='en')
                    if (typeof valdata.mer_fname != "undefined" || valdata.mer_fname != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.mer_lname != "undefined" || valdata.mer_lname != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.mer_email != "undefined" || valdata.mer_email != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.mobile != "undefined" || valdata.mobile != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.city != "undefined" || valdata.city != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.mer_password != "undefined" || valdata.mer_password != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                    if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                    if (typeof valdata.storcertifiction_img != "undefined" || valdata.storcertifiction_img != null)
                    {
                    $('.english_tab').trigger('click');
                    }

                    if (typeof valdata.mer_fname_ar != "undefined" || valdata.mer_fname_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');  
                    }
                    if (typeof valdata.mer_lname_ar != "undefined" || valdata.mer_lname_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     


                    }
                      
@else
                    if (typeof valdata.mer_fname_ar != "undefined" || valdata.mer_fname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                   if (typeof valdata.mer_lname_ar != "undefined" || valdata.mer_lname_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                  if (typeof valdata.mer_fname != "undefined" || valdata.mer_fname != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.mer_lname != "undefined" || valdata.mer_lname != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      if (typeof valdata.mer_email != "undefined" || valdata.mer_email != null) 
                    {
                        $('.english_tab').trigger('click'); 
                     

                    }
                      if (typeof valdata.mobile != "undefined" || valdata.mobile != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    

                    }
                     if (typeof valdata.city != "undefined" || valdata.city != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                      if (typeof valdata.mer_password != "undefined" || valdata.mer_password != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                      if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }

                       if (typeof valdata.storcertifiction_img != "undefined" || valdata.storcertifiction_img != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                    
@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
  $("#company_logo1").change(function () {
        var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   $("#file_value2").html('');         
$(".certifications").html('Only PDF format allowed.');

         }
    });


</script> 
@include('sitemerchant.includes.footer')
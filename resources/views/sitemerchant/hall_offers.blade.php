@include('sitemerchant.includes.header')  
@php $hall_leftmenu =1; @endphp    
@include('sitemerchant.includes.breadcrumb')   @include('sitemerchant.includes.left')
<div class=" container" >
  <div class="inner" style="min-height: 700px;">
    <div class="row">
      <div class="col-lg-12">
        <div class="box">
          <header>
            <div class="icons"><i class="icon-dashboard"></i></div>
            <h5> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif </h5>
          </header>
          
          <!-- Display Message after submition --> 
          @if (Session::has('message'))
          <div class="alert alert-info">{{ Session::get('message') }}</div>
          @endif 
          <!-- Display Message after submition --> 
          
          <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
          
          <div class="heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER') }} @endif</div>
          <div class="table">
            <div class="tr">
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif</div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERMOBILE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERMOBILE') }} @endif</div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDERSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDERSTATUS') }} @endif</div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_BOOKINGDATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BOOKINGDATE') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BOOKINGDATE') }} @endif</div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_AMOUNT') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_AMOUNT') }} @endif</div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS') }} @endif</div>
              <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  
                @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>
            </div>
            <div class="tr">
              <div class="td td3" data-title="Rated">ddd</div>
              <div class="td td3" data-title="Rated">dd</div>
              <div class="td td3" data-title="Rated">dd</div>
              <div class="td td3" data-title="Rated">ddd</div>
              <div class="td td3" data-title="Rated">ddd</div>
              <div class="td td3" data-title="Rated">vvv </div>
              <div class="td td4" data-title="PTS"><a href="">Edit</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('sitemerchant.includes.footer')
 @include('sitemerchant.includes.header')  
@php $hall_leftmenu =1; @endphp 

<!-- MAIN WRAPPER -->
<div class="merchant_vendor">
  <div class="profile_box"> 
    
    <!--PAGE CONTENT -->
    <div class="profile_area">
      <div class="inner">
        <div class="row">
          <div class="col-lg-12">
            <header>
              <h5 class="global_head">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_MERCHANT_ACCOUNT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_MERCHANT_ACCOUNT')  }}</h5>
            </header>
            @if ($errors->any()) <br>
            <ul style="color:red;">
              <div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>
                ')) !!}
                {{ Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ]) }} </div>
            </ul>
            @endif
            @if (Session::has('mail_exist'))
            <div class="alert alert-warning alert-dismissable" >{!! Session::get('mail_exist') !!}
              {{ Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ]) }} </div>
            @endif
            @if (Session::has('result'))
            <div class="alert alert-warning alert-dismissable alert_suc" >{!! Session::get('result') !!}
              {{ Form::button('×',['class' => 'close' ,'data-dismiss'=>'alert', 'aria-hidden' => 'true' ]) }} </div>
            @endif
            <div class="box commonbox"> @foreach($merchant_details as $fetch_mer_details)  @endforeach
              {!! Form::open(array('url'=>'edit_merchant_account_submit','class'=>'form-horizontal', 'id'=>'editaccount','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
              <div class="global_area">
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_FIRST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FIRST_NAME') : trans($MER_OUR_LANGUAGE.'.MER_FIRST_NAME') }}</label>
                    <div class="info100"> {{ Form::hidden('mer_id',$fetch_mer_details->mer_id) }}
                      {{ Form::text('first_name',$fetch_mer_details->mer_fname,['class' => 'form-control','id' => 'first_name','maxlength' => 40]) }} </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_LAST_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LAST_NAME') : trans($MER_OUR_LANGUAGE.'.MER_LAST_NAME') }}</label>
                    <div class="info100"> {{ Form::text('last_name',$fetch_mer_details->mer_lname,['class' => 'form-control','id' => 'last_name','maxlength' => 40]) }} </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL') :  trans($MER_OUR_LANGUAGE.'.MER_EMAIL')}}</label>
                    <div class="info100"> {{ Form::text('email_id',$fetch_mer_details->mer_email,['class' => 'form-control','id' => 'email_id','maxlength' => 50]) }} </div>
                  </div>
				  <?php if(isset($fetch_mer_details->mer_phone) && $fetch_mer_details->mer_phone!=''){$rec_array = explode('-',$fetch_mer_details->mer_phone);} ?>
                  <div class="form_row_right country_row">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PHONE') : trans($MER_OUR_LANGUAGE.'.MER_PHONE')}}</label>
                    <div class="info100 meredit-ac">
					 <!--<input type="text" name="country_code" id="country_code" maxlength="5" value="<?php if(isset($rec_array[0]) && $rec_array[0]!=''){ echo ltrim($rec_array[0],'+'); }?>" onkeypress='return isNumberKey(event);' class="t-box countrycode"/>-->


             <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode" required>      
      @foreach($getCountry as $Ccode)
        <option value="{{ $Ccode->country_code}}" <?php if(isset($rec_array[1]) && $rec_array[1]==$Ccode->country_code){ echo 'selected'; } ?>>+{{ $Ccode->country_code}}</option>        
        @endforeach
    </select>

					  <input class="form-control small-sel" id="phone_no" maxlength="12" name="phone_no" type="text" value="<?php if(isset($rec_array[1]) && $rec_array[1]!=''){ echo ltrim($rec_array[1],'+'); }?>">

				    </div>
					<span for="country_code" generated="true" class="error"></span>
					<span for="phone_no" generated="true" class="error"></span>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }}</label>
                    <div class="info100">
                      <select class="form-control" name="select_mer_city" id="select_mer_city" >
                        <option value="">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT') }} </option>
                      </select>
                    </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY')!= '')   ? trans(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_COUNTRY') }}</label>
                    <div class="info100">
                      <select class="form-control" name="select_mer_country" id="select_mer_country" onChange="select_mer_city_ajax(this.value)" >
                        <option value=""> {{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT'): trans($MER_OUR_LANGUAGE.'.MER_SELECT')}} </option>
                        
                          @foreach($country_details as $country_fetch)
          				 
                        <option value="{{ $country_fetch->co_id }}"  @if($fetch_mer_details->mer_co_id == $country_fetch->co_id) {{ 'selected' }} @endif >{{ $country_fetch->co_name }}</option>
                        
           				   @endforeach
       					 
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1') }}</label>
                    <div class="info100"> {{ Form::text('addreess_one',$fetch_mer_details->mer_address1,['class' => 'form-control','id' => 'addreess_one','maxlength' => 200]) }} </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS2') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS2')}}</label>
                    <div class="info100"> {{ Form::text('address_two',$fetch_mer_details->mer_address2,['class' => 'form-control','id' => 'address_two','maxlength' => 200]) }} </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.ZIP_CODE')!= '') ?  trans(Session::get('mer_lang_file').'.ZIP_CODE') : trans($MER_OUR_LANGUAGE.'.ZIP_CODE')}}</label>
                    <div class="info100"> {{ Form::text('zipcode',$fetch_mer_details->zipcode,['class' => 'form-control zip','id' => 'zipcode','maxlength' => 7]) }} </div>
                  </div>
                  <div class="form_row_right">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.USERIMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.USERIMAGE') : trans($MER_OUR_LANGUAGE.'.USERIMAGE')}}</label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo1">
                        <div class="file-btn-area">
                          <div id="file_value2" class="file-value"></div>
                          <div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div>
                        </div>
                        </label>
                        <input id="company_logo1" name="stor_img" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
                      </div>
                      <!-- input-file-area --> 
                      
                      @if($fetch_mer_details->stor_img!='') <img src="{{$fetch_mer_details->stor_img}}" width="100" height="100" class="profile_img"> @else
                     
                      @endif </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.CertificateChamber')!= '') ?  trans(Session::get('mer_lang_file').'.CertificateChamber') : trans($MER_OUR_LANGUAGE.'.CertificateChamber')}}</label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn">@if (Lang::has(Session::get('lang_file').'.UPLOAD')!= '') {{  trans(Session::get('lang_file').'.UPLOAD')}}  @else {{ trans($MER_OUR_LANGUAGE.'.UPLOAD')}} @endif</div>
                        </div>
                        </label>
                        <input class="info-file"  accept="pdf" type="file" id="company_logo" name="company_logo">
                      </div>
 
                    </div>
                    @if($fetch_mer_details->certificates !='')
                    <input type="hidden" name="certificates" value="{{$fetch_mer_details->certificates}}">
                    <div class="profile-pdf-line"> <a href="{{$fetch_mer_details->certificates}}" target="_blank"> <img src="{{url('/themes/images/pdf.png')}}"> </a>
                      <input type="hidden" name="certificatesname" value="{{$fetch_mer_details->certificates_name or ''}}">
                      {{$fetch_mer_details->certificates_name or ''}} </div>
                    @endif </div>
                     <div class="form_row_right">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.Shop_link_in_maroof')!= '') ?  trans(Session::get('mer_lang_file').'.Shop_link_in_maroof') : trans($MER_OUR_LANGUAGE.'.Shop_link_in_maroof') }}</label>
                    <div class="info100"> {{ Form::text('shop_link_in_maroof',$fetch_mer_details->shop_link_in_maroof,['class' => 'form-control','id' => 'shop_link_in_maroof','maxlength' => 255]) }} </div>
                  </div>
                </div>

<div class="form_row">
                <div class="form_row_right">
                    <label class="form_label" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.sales_rep_code')!= '') ?  trans(Session::get('mer_lang_file').'.sales_rep_code') : trans($MER_OUR_LANGUAGE.'.sales_rep_code') }}
 </label>
                    <div class="info100"> {{  $fetch_mer_details->sales_rep_code or 'N/A'  }} </div>
                  </div>
            </div>  
                  <div class="form_row">
                    <button class="form_button" type="submit" id="submit" >{{ (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_UPDATE'): trans($MER_OUR_LANGUAGE.'.MER_UPDATE')}}</button>
                  </div>
             
              </div>
              {{ Form::close() }} </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--END PAGE CONTENT --> 

<!--END MAIN WRAPPER --> 

<!-- GLOBAL SCRIPTS --> 

<script>
	$( document ).ready(function() {
	
	$('#submit').click(function() {
    var file		 	 = $('#file');
	var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      	if(file.val() == "")
 		{
 		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else
		{
		file.css('border', ''); 				
		}
	});
	
	 var passData = 'city_id_ajax=<?php echo $fetch_mer_details->mer_ci_id; ?>&country_id_ajax=<?php echo $fetch_mer_details->mer_co_id; ?>' ;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_edit_merchant'); ?>',
				  success: function(responseText){  
				  //alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
			
	});
	</script> 
<script>
	

	
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_merchant'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
	}
	</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#editaccount").validate({
                  ignore: [],
                  rules: {
                  first_name: {
                       required: true,
                      },

                       last_name: {
                       required: true,
                      },

                       email_id: {
                       required: true,
                      },

                      select_mer_country: {
                       required: true,
                      },
                       select_mer_city: {
                       required: true,
                      },
					   country_code: {
                       required: true,
                      },
                       phone_no: {
                       required: true,
                      },
                      addreess_one: {
                       required: true,
                      },
                      zipcode: {
                       required: true,
                       number: true
                      },

                      stor_img: {
                           required:false,
                           accept:"png|jpe?g|gif",
                      },

                      company_logo: {
                           required:false,
                           accept:"pdf",
                      },

                      
                 

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
             first_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_FIRSTNAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_FIRSTNAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_FIRSTNAME') }} @endif",
                      },  

                 last_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_LASTNAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_LASTNAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_LASTNAME') }} @endif",
                      },  

                         email_id: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EMAILADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EMAILADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EMAILADDRESS') }} @endif",
                      },   

                          select_mer_country: {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_COUNTRY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_COUNTRY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_COUNTRY') }} @endif",
                      },   


                       select_mer_city: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CITY') }} @endif ",
                      },
                        country_code: {
                 required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_COUNTRY_CODE_MSG')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COUNTRY_CODE_MSG') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COUNTRY_CODE_MSG') }} @endif ",
                      },
                       phone_no: {
                 required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PHONE') }} @endif ",
                      },
                            addreess_one: {
                 required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ADDRESS') }} @endif ",
                      },
                            zipcode: {
           required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ZIP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ZIP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ZIP') }} @endif ",
                      },
                        stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      }, 

            company_logo: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_FILE') }} @endif",
                                    


                                    

                      },               



                                          
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                     

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer') 
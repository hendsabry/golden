<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->



 <!-- BEGIN HEAD -->

<head>

    <meta charset="UTF-8" />

    <title>{{ $SITENAME }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT')}} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY'): trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')}}</title>

     <meta content="width=device-width, initial-scale=1.0" name="viewport" />

	<meta content="" name="description" />

	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <![endif]-->

    <!-- GLOBAL STYLES -->

    <!-- GLOBAL STYLES -->

    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />

    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />

    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />

    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />

     <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif	

    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />

    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/success.css" />

     <link href="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <!--END GLOBAL STYLES -->

       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->



</head>

     <!-- END HEAD -->



     <!-- BEGIN BODY -->

<body class="padTop53 " >



    <!-- MAIN WRAPPER -->

    <div id="wrap">





         <!-- HEADER SECTION -->

         {!! $merchantheader!!}

        <!-- END HEADER SECTION -->

        <!-- MENU SECTION -->

         {!! $merchantleftmenus!!}

        <!--END MENU SECTION -->



		<div></div>



         <!--PAGE CONTENT -->

        <div id="content">

           

                <div class="inner">

                    <div class="row">

                    <div class="col-lg-12">

                        	<ul class="breadcrumb">

                            	<li class=""><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>

                                <li class="active"><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ? trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')  : trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')}}</a></li>

                            </ul>

                    </div>

                </div>

            <div class="row">

<div class="col-lg-12">

    <div class="box dark">

        <header>

            <div class="icons"><i class="icon-edit"></i></div>

            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ? trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')  : trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')}}</h5>

            

        </header>

        <div id="div-1" class="accordion-body collapse in body">
		{{ Form::open(['class' => 'form-horizontal']) }}
            

                <div class="col-lg-12">

                    
        <div class="panel_marg_clr ppd">
       <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">

                                    <thead>

                                        <tr role="row">

                                        <th aria-sort="ascending" aria-label="S.No: activate to sort column ascending" style="width: 99px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc">S.No</th>

                                        <th aria-label="Customers: activate to sort column ascending" style="width: 111px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Deal Name</th>

                                        <th aria-label="Product Title: activate to sort column ascending" style="width: 219px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Name</th>

                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Email-ID</th>

                                        <th aria-label="Amount(S/): activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Date</th>

                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Address</th>

                                        <th aria-label=" Tax (S/): activate to sort column ascending" style="width: 119px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Details</th>

                                      <!--  <th aria-label="Transaction Date: activate to sort column ascending" style="width: 125px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">Delivery Status</th>-->

                                        </tr>

                                    </thead>

                                    <tbody>

                                   

										<?php $i=1; ?>

										@foreach($deal_cod as $shipping)

										     
           
											@if($shipping->cod_status==1)

											 <?php 

												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_SUCCESS')!= ''))? trans(Session::get('mer_lang_file').'.MER_SUCCESS') : trans($MER_OUR_LANGUAGE.'.MER_SUCCESS');
												?>
											

											@elseif($shipping->cod_status==2) 

											
												<?php 	
												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_COMPLETED')!= ''))? trans(Session::get('mer_lang_file').'.MER_COMPLETED') : trans($MER_OUR_LANGUAGE.'.MER_COMPLETED');
												?>
											

											@elseif($shipping->cod_status==3) 

											<?php 

												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_HOLD')!= ''))? trans(Session::get('mer_lang_file').'.MER_HOLD') : trans($MER_OUR_LANGUAGE.'.MER_HOLD');

											?>

											@elseif($shipping->cod_status==4) 

											<?php 

												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_FAILED')!= ''))? trans(Session::get('mer_lang_file').'.MER_FAILED') : trans($MER_OUR_LANGUAGE.'.MER_FAILED');

											?>
											@endif
    <?php    
$merid = Session::get('merchantid');
											?>

										

										

										

										

										<tr class="gradeA odd">

                                            <td class="sorting_1">{{ $i }}</td>
											<td class="     ">
                                            <ul>
											<?php
											$product_data = DB::table('nm_ordercod')
											->orderBy('cod_date', 'desc')
											->leftjoin('nm_customer', 'nm_ordercod.cod_cus_id', '=', 'nm_customer.cus_id')
											->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')
											->where('nm_ordercod.cod_transaction_id', '=',$shipping->cod_transaction_id)
											->where('nm_ordercod.cod_order_type', '=',2)
											 ->where('nm_ordercod.cod_merchant_id', $merid)
											->get(); ?>
                                        
											@foreach($product_data as $product_datavalues)
												
											<?php
												echo '<li style="list-style:upper-roman; text-align:left;">'. $shipping->deal_title.'.</li>';
																							
											


											
											?>
											@endforeach
											</ul>
											</td>
                                            

                                            <td class=" ">{{ $shipping->cus_name}} </td>

                                            <td class=" ">{{ $shipping->cus_email}} </td>

                                            <td class="center">{{ $shipping->cod_date}}</td>

                                            <td class="center">{{ $shipping->ship_address1}}</td>

                                     

                                      

                            <td class="center"><a class="btn btn-success" data-target=" {{ '#uiModal'.$i }}" data-toggle="modal">{{ (Lang::has(Session::get('mer_lang_file').'.MER_VIEW_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW_DETAILS'): trans($MER_OUR_LANGUAGE.'.MER_VIEW_DETAILS')}}</a></td>

                                              <?php /* <td class="center"><?php echo $orderstatus;?></td>   **/?>

                                            

                                          

                                           

                                        </tr>

                                        

                                        <?php $i=$i+1; ?>
										@endforeach
										</tbody>

                                </table></div>


								{{ Form::close() }}

        </div>

    </div>

</div>

   

    </div>

                    

                    </div>

                    

                    

                    



                </div>

            <!--END PAGE CONTENT -->

 

        </div>

    <div class="modal fade in" id="formModal"  style="display:none;">

     <div class="modal-dialog">

                                    <div class="modal-content">

                                        

                                        <div class="modal-body">
				
											{{ Form::open(['role' => 'form']) }}
                                          

                                        <div class="form-group">

                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL_ID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL_ID'): trans($MER_OUR_LANGUAGE.'.MER_EMAIL_ID')}}</label>

                                            <input class="form-control">

                                           <p class="help-block">{{ (Lang::has(Session::get('mer_lang_file').'.MER_BLOCK_LEVEL_HELP')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_BLOCK_LEVEL_HELP'): trans($MER_OUR_LANGUAGE.'.MER_BLOCK_LEVEL_HELP')}}.</p>

                                        </div>

                                        <div class="form-group">

                                            <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS'): trans($MER_OUR_LANGUAGE.'.MER_ADDRESS')}}&nbsp;:&nbsp;</label>

												{{ (Lang::has(Session::get('mer_lang_file').'.MER_ASDSA_CALIFORNIA_CANADIAN')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ASDSA_CALIFORNIA_CANADIAN') : trans($MER_OUR_LANGUAGE.'.MER_ASDSA_CALIFORNIA_CANADIAN')}}

                                        </div>

                                        <div class="form-group">

                                          <label>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MESSAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MESSAGE'): trans($MER_OUR_LANGUAGE.'.MER_MESSAGE')}}</label>

											  {{ Form::textarea('','',['id' =>'text4', 'class' => 'form-control']) }} 
                                         

                                        </div>

                                        

                                       

									   {{ Form::close() }}

                                        </div>

                                        <div class="modal-footer">
										
										{{ Form::button('Close',['class' => 'btn btn-default' , 'data-dismiss' => 'modal']) }}
                                            

                                           <button class="btn btn-success" type="button"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_SEND')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEND')  : trans($MER_OUR_LANGUAGE.'.MER_SEND')}}</button>

                                        </div>

                                    </div>

                                </div>  </div>

     <!--END MAIN WRAPPER -->

<?php $i=1; ?>

@foreach($deal_cod as $shipping)


	@if($shipping->cod_status==1)
					 <?php
	 $orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_SUCCESS')!= ''))? trans(Session::get('mer_lang_file').'.MER_SUCCESS') : trans($MER_OUR_LANGUAGE.'.MER_SUCCESS');
												?>
											
		@elseif($shipping->cod_status==2) 

		
												<?php 	
												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_COMPLETED')!= ''))? trans(Session::get('mer_lang_file').'.MER_COMPLETED') : trans($MER_OUR_LANGUAGE.'.MER_COMPLETED');
												?>
											

											@elseif($shipping->cod_status==3) 

											<?php 

												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_HOLD')!= ''))? trans(Session::get('mer_lang_file').'.MER_HOLD') : trans($MER_OUR_LANGUAGE.'.MER_HOLD');

											?>

											@elseif($shipping->cod_status==4) 

											<?php 

												$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_FAILED')!= ''))? trans(Session::get('mer_lang_file').'.MER_FAILED') : trans($MER_OUR_LANGUAGE.'.MER_FAILED');

											?>
											@endif
	
           

        

  

										  





<div  id="{{ 'uiModal'.$i}}" class="modal fade in invoice-top" style="display:none;">

                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header" style="border-bottom:none; overflow: hidden;background: #f5f5f5;">

                                          

                                           

                                                <div class="col-lg-4"><img src="{{ $SITE_LOGO}}" alt="" style=""></div>

                                                 <div class="col-lg-4" style="padding-top: 10px; text-align: center;"><strong>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TAX_INVOICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TAX_INVOICE')  : trans($MER_OUR_LANGUAGE.'.MER_TAX_INVOICE') }}</strong></div>

                                                  <div class="col-lg-4"><a style="color:#d9534f;"  href="class="btn btn-default" data-dismiss="modal" CLASS="pull-right"><i class="icon-remove-sign icon-2x"></i></a></div>

                                        </div>

                                    
									<?php $subtotal=$shipping->cod_qty*$shipping->cod_amt;  ?> 
									<?php $totamt=round($subtotal + ($subtotal*($shipping->deal_inctax/100))); ?>
                                      <div class="row">

                                      <div class="col-lg-12">

                                      <div class="col-lg-6" >

                                       <h4>{{ (Lang::has(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CASH_ON_DELIVERY'): trans($MER_OUR_LANGUAGE.'.MER_CASH_ON_DELIVERY')}}</h4>

                                      @if($shipping->cod_status == 1) 
									  <b>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT_PAID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT_PAID') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT_PAID')}}
									  @else
                                      <b>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT_TO_PAY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT_TO_PAY'): trans($MER_OUR_LANGUAGE.'.MER_AMOUNT_TO_PAY')}} 
									  @endif 
									  
									  :{{ Helper::cur_sym() }} <?php
									  $product_titles=DB::table('nm_ordercod')
									  ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $shipping->cod_transaction_id)
									   ->where('nm_ordercod.cod_merchant_id', $merid)
									  ->where('nm_ordercod.cod_order_type', '=',2)->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
									  @foreach($product_titles as $prd_tit) 
										 <?php  	
										  $subtotal=$prd_tit->cod_amt; 
																$tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
																$total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
																$total_ship_amt+= $prd_tit->cod_shipping_amt;
																$total_item_amt+=$prd_tit->cod_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->cod_pro_id;
																
														$item_amt = $prd_tit->cod_amt + (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
														
															 $ship_amt = $prd_tit->cod_shipping_amt;
															
														
															 //$item_tax = $codorderdet->cod_tax;
															 ?>
															@if($prd_tit->coupon_amount != 0)
															<?php
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount); ?>
															
															@else
															<?php 
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
															?>
																@endif		@endforeach															
									  {{ $grand_total}}</b>

                                      <br>

                                      <span>(({{ (Lang::has(Session::get('mer_lang_file').'.MER_INCLUSIVE_OF_ALL_CHARGES')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INCLUSIVE_OF_ALL_CHARGES'): trans($MER_OUR_LANGUAGE.'.MER_INCLUSIVE_OF_ALL_CHARGES')}}) )</span>

                                     <br>
									 
									 <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_ID')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORDER_ID'): trans($MER_OUR_LANGUAGE.'.MER_ORDER_ID')}}: {{ $shipping->ship_trans_id }}</span>
									 <br>

                                       <br>

					<span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE'): trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE')}}: {{ $shipping->cod_date}}</span>

                                      </div>

                                          <div class="col-lg-6" style="border-left:1px solid #eeeeee;">
											
                                          <h4>{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPPING_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_DETAILS')}}</h4>
                                       
										 
										 {{ (Lang::has(Session::get('mer_lang_file').'.MER_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NAME'): trans($MER_OUR_LANGUAGE.'.MER_NAME')}} : {{ $shipping->ship_name}}<br>
										   
										   {{ (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EMAIL'): trans($MER_OUR_LANGUAGE.'.MER_EMAIL')}} : {{ $shipping->ship_email}}<br>
										   
										   {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') : trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1')}} : {{ $shipping->ship_address1}}<br>
										  
										  {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS2'): trans($MER_OUR_LANGUAGE.'.MER_ADDRESS2')}} : {{ $shipping->ship_address2}}<br>
										  
										  
										  {{ (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY'): trans($MER_OUR_LANGUAGE.'.MER_CITY')}} : {{ $shipping->ship_ci_id}}<br>
										  
										  {{ (Lang::has(Session::get('mer_lang_file').'.MER_STATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STATE'): trans($MER_OUR_LANGUAGE.'.MER_STATE')}} : {{ $shipping->ship_state}}<br>
										   
										   {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_COUNTRY')}} : {{ $shipping->ship_country }}<br>
										   
										   {{ (Lang::has(Session::get('mer_lang_file').'.MER_ZIPCODE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ZIPCODE') :  trans($MER_OUR_LANGUAGE.'.MER_ZIPCODE')}} : {{ $shipping->ship_postalcode}}<br>
                                          
                                          {{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PHONE'): trans($MER_OUR_LANGUAGE.'.MER_PHONE')}} : {{ $shipping->ship_phone }}
                                          </div>

                                            

                                             

                                      </div>

                                      </div>

                                      <hr>

                                      <div class="row">

                                      <div class="span12 text-center">

                                      <h4 class="text-center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_INVOICE_DETAILS')  : trans($MER_OUR_LANGUAGE.'.MER_INVOICE_DETAILS')}}</h4>

                                      <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS') : trans($MER_OUR_LANGUAGE.'.MER_THIS_SHIPMENT_CONTAINS_FOLLOWING_ITEMS')}}  </span>

                                      </div>

                                      </div>

                                      <hr>
							<?php /**		<!--<?php //if($shipping->deal_title !=''){ ?>
                                    <h4 class="text-center"><?php //echo $shipping->deal_title;?></h4>
									<?php // } //else {?>
									<h4 class="text-center"><?php //echo "---"?></h4>
									<?php //} ?>--> **/ ?>
                    <div style="overflow: auto;">
                      <table style="width:98%;" align="center" border="1" bordercolor="#e6e6e6">

                                  <tr style="border-bottom:1px solid #e6e6e6; background:#f5f5f5;">
										<td   align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_NAME')  : trans($MER_OUR_LANGUAGE.'.MER_DEAL_NAME')}}</td>&nbsp;
                                   
                                        <td   align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_QUANTITY') : trans($MER_OUR_LANGUAGE.'.MER_QUANTITY')}}</td>

                                         <td   align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE')}}</td>

                                            <td  align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SUB_TOTAL')!= '') ? trans(Session::get('mer_lang_file').'.MER_SUB_TOTAL')   : trans($MER_OUR_LANGUAGE.'.MER_SUB_TOTAL') }}</td>

                                               <td   align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS')!= '') ? trans(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS') : trans($MER_OUR_LANGUAGE.'.MER_PAYMENT_STATUS') }}</td>
                                             <td   align="center">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS') : trans($MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS') }}</td>&nbsp;

                                          

                                    </tr>

                                    <?php
									  $product_titles=DB::table('nm_ordercod')
									  ->leftjoin('nm_deals', 'nm_ordercod.cod_pro_id', '=', 'nm_deals.deal_id')->leftjoin('nm_color', 'nm_ordercod.cod_pro_color', '=', 'nm_color.co_id')->leftjoin('nm_size', 'nm_ordercod.cod_pro_size', '=', 'nm_size.si_id')->where('cod_transaction_id', '=', $shipping->cod_transaction_id)
									  ->where('nm_ordercod.cod_merchant_id', $merid)
									  ->where('nm_ordercod.cod_order_type', '=',2)->get();
									  $total_item_amt = $total_tax_amt = $total_ship_amt = $coupon_amount = $item_tax = 0; ?>
									  @foreach($product_titles as $prd_tit) 
										  	
															<?php 	$status=$prd_tit->delivery_status; ?>
											@if($prd_tit->delivery_status==1)
											<?php
											$orderstatus="Order Placed";
											?>
											@elseif($prd_tit->delivery_status==2) 
											<?php
											$orderstatus="Order Packed";
											?>
											@elseif($prd_tit->delivery_status==3) 
											<?php
											$orderstatus="Dispatched";
											?>
											@elseif($prd_tit->delivery_status==4) 
											<?php
											$orderstatus="Delivered";
											?>
											@endif
											
											
											@if($prd_tit->cod_status==1)
											<?php
											$payment_status="Success";
											?>
											@elseif($prd_tit->cod_status==2) 
											<?php
											$payment_status="Order Packed";
											?>
											@elseif($prd_tit->cod_status==3) 
											<?php
											$payment_status="Hold";
											?>
											@elseif($prd_tit->cod_status==4) 
											<?php
											$payment_status="Faild";
											?>
											@endif
											<?php
											$subtotal=$prd_tit->cod_amt; 
																$tax_amt = (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
																$total_tax_amt+= (($prd_tit->cod_amt * $prd_tit->cod_tax)/100); 
																$total_ship_amt+= $prd_tit->cod_shipping_amt;
																$total_item_amt+=$prd_tit->cod_amt;
																$coupon_amount+= $prd_tit->coupon_amount;
																$prodct_id = $prd_tit->cod_pro_id;
																
														$item_amt = $prd_tit->cod_amt + (($prd_tit->cod_amt * $prd_tit->cod_tax)/100);
															
														
															 $ship_amt = $prd_tit->cod_shipping_amt;
															
														
															 //$item_tax = $codorderdet->cod_tax;
															 ?>
															@if($prd_tit->coupon_amount != 0)
															<?php
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt - $coupon_amount);
															?>
															@else
															<?php
																$grand_total =  ($total_item_amt + $total_ship_amt + $total_tax_amt);
															?>
															@endif				
									  

                                     <tr>	
									 <td   align="center">{{ $prd_tit->deal_title}}</td>&nbsp;
										
                                   
                                        <td   align="center">{{ $prd_tit->cod_qty}} </td>

                                          <td   align="center">{{ Helper::cur_sym() }}  {{ $prd_tit->cod_amt }} </td>
 
                                            <td   align="center">{{ Helper::cur_sym() }}  {{ $subtotal }}
<td  align="center">{{ $payment_status}}</td>
											<td   align="center">{{ $orderstatus}}</td>											</td>

                                             

                                              

                                    </tr>
									@endforeach
                                    </table></div>

                                    <br>

                                    <hr>

                                    <div class="">

                                    <div class="col-lg-6"></div>

                                     <div class="col-lg-6">

                               <div><span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPMENT_VALUE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SHIPMENT_VALUE'): trans($MER_OUR_LANGUAGE.'.MER_SHIPMENT_VALUE')}}<b class="pull-right" style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $total_ship_amt }} </b></span></div><br>

                                       <div> <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_TAX')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TAX'): trans($MER_OUR_LANGUAGE.'.MER_TAX')}}<b class="pull-right"style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $total_tax_amt}}</b></span></div>

                                        <hr>



                                      <div> <span>{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AMOUNT'): trans($MER_OUR_LANGUAGE.'.MER_AMOUNT')}}<b class="pull-right"style="margin-right:15px;">{{ Helper::cur_sym() }} {{ $grand_total}}</b></span></div>

                                     </div>

                                    </div>

                        
									<hr>
                                        

                                        <div class="modal-footer">

                                            <button data-dismiss="modal" class="btn btn-danger" type="button">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLOSE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLOSE') : trans($MER_OUR_LANGUAGE.'.MER_CLOSE')}}</button>

                                            

                                        </div>

                                    </div>

                                </div>

                            </div>



   <?php $i=$i+1; ?>
@endforeach   

    <!-- FOOTER -->

    <div id="footer">

       {!! $merchantfooter !!}

    </div>

    <!--END FOOTER -->





     <!-- GLOBAL SCRIPTS -->

    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>

     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- END GLOBAL SCRIPTS -->   

       <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>

    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>

     <script>

         $(document).ready(function () {

             $('#dataTables-example').dataTable();

         });

    </script>
<script type="text/javascript">
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
</script>  
</body>

     <!-- END BODY -->

</html>


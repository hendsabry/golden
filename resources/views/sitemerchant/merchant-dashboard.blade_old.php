@include('sitemerchant.includes.header')
 
<div class="merchant_vendor cont_add">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.DASHBOARD')!= '') {{  trans(Session::get('mer_lang_file').'.DASHBOARD') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.DASHBOARD') }} @endif</h5>
      
      </header>
   
  @php   $merchantid=Session::get('merchantid'); @endphp
      <!-- service_listingrow --> 
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">  

              <div class="dsah">
			  <a href="{{ url('listmanager') }}">
             <div class="dash_box">
			 
			 <div class="manager_img"><img src="{{ url('/public/assets/img/manager.jpg') }}" /></div>
			 <div class="manger_text"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGER') : trans($MER_OUR_LANGUAGE.'.MER_MANAGER') }}</div>
			 
			 
			 </div></a>
			  <a href="{{ url('edit_merchant_info',['id' =>$merchantid]) }}">
			  <div class="dash_box">
			 
			 <div class="manager_img"><img src="{{ url('/public/assets/img/vendor-info.jpg') }}" /></div>
			 <div class="manger_text">{{ (Lang::has(Session::get('mer_lang_file').'.SETTINGS')!= '') ? trans(Session::get('mer_lang_file').'.SETTINGS') : trans($MER_OUR_LANGUAGE.'.SETTINGS') }}</div>
			
			 
			 </div> </a>
			  <a href="{{ url('fund-request') }}">
			   <div class="dash_box">
			  
			 <div class="manager_img"><img src="{{ url('/public/assets/img/fund.jpg') }}" /></div>
			 <div class="manger_text">{{ (Lang::has(Session::get('mer_lang_file').'.FUND_REQUESTS')!= '') ? trans(Session::get('mer_lang_file').'.FUND_REQUESTS') : trans($MER_OUR_LANGUAGE.'.FUND_REQUESTS') }}</div>
			
			 
			 </div>
			  </a>
			  <a href="{{ url('sitemerchant_dashboard') }}">
			  <div class="dash_box">
			 <div class="manager_img"><img src="{{ url('/public/assets/img/services.jpg') }}" /></div>
			 <div class="manger_text">{{ (Lang::has(Session::get('mer_lang_file').'.Services')!= '') ? trans(Session::get('mer_lang_file').'.Services') : trans($MER_OUR_LANGUAGE.'.Services') }}</div>
			 
			 </div></a>
			 <a href="{{ url('account-details') }}">
			 <div class="dash_box">
			 <div class="manager_img"><img src="{{ url('/public/assets/img/account.jpg') }}" /></div>
			 <div class="manger_text">@if (Lang::has(Session::get('mer_lang_file').'.AccountDetail')!= '') {{  trans(Session::get('mer_lang_file').'.AccountDetail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.AccountDetail') }} @endif </div>
			 
			 </div></a>

			 <a href="{{ url('recieve-payment') }}">
			 <div class="dash_box">
			 <div class="manager_img"><img src="{{ url('/public/assets/img/payment.jpg') }}" /></div>
			 <div class="manger_text">@if (Lang::has(Session::get('mer_lang_file').'.Payment')!= '') {{  trans(Session::get('mer_lang_file').'.Payment') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Payment') }} @endif</div>
			 
			 </div>
              
             </a>

             	<a href="{{ url('recieve-orders') }}">
			 <div class="dash_box">
			 <div class="manager_img"><img src="{{ url('/public/assets/img/basket_50x50.jpg') }}" /></div>
			 <div class="manger_text">@if (Lang::has(Session::get('mer_lang_file').'.ORDERS')!= '') {{  trans(Session::get('mer_lang_file').'.ORDERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDERS') }} @endif</div>
			 
			 </div>
              
             </a>




              </div>

          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor -->

  
  


@include('sitemerchant.includes.footer') 
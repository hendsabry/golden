@include('sitemerchant.includes.header') 
@php $photographybig_leftmenu =1; @endphp
<div class="merchant_vendor">   @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if(request()->autoid !='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATEPACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.ADDPACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADDPACKAGE') }} @endif </h5>
        @endif
        
        
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <!-- Display Message after submition -->
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }} </div>
              @endif
              <!-- Display Message after submition -->
              <form name="form1" id="add-container" method="post" action="{{ route('store-photography-package') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PACKEGENAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKEGENAME'); @endphp </span></label>
                    <div class="info100">
                      <div class="english">
                        <input class="english" maxlength="100" type="text" name="title" value="{{$fetchfirstdata->pro_title or ''}}" id="title" required="">
                      </div>
                      <div class="arabic">
                        <input class="arabic ar" maxlength="100" id="title_ar"  name="title_ar" value="{{$fetchfirstdata->pro_title_ar or ''}}"  type="text" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.packagefor'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.packagefor'); @endphp</span> </label>
                    <div class="info100">
                      <div class="englisSh"> @php 
                        if(isset($fetchfirstdata->attribute_id) && $fetchfirstdata->attribute_id!='')
                        {
                        $attID =  $fetchfirstdata->attribute_id;
                        }
                        else
                        {
                        $attID =  0; 
                        }
                        @endphp
                        <select class="small-sel" name="package_for" onchange="checkinfo(this.value);">
                          <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif</option>
                          <option value="142" @if($attID==142) {{"SELECTED"}} @endif>Photography</option>
                          <option value="143" @if($attID==143) {{"SELECTED"}} @endif>Videography</option>
                        </select>
                      </div>
                    </div>
                  </div>

                   <div class="form_row_left common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.numberof_cameras'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.numberof_cameras'); @endphp </span> </label>
                    <div class="info100" >
                      <div>
                        <input class="xs_small notzero" maxlength="2" type="text" onkeypress="return isNumber(event)" name="no_of_camra" value="{{$CameraNumber->value or ''}}" id="no_of_camra">
                      </div>
</div> 

                  <div class="form_row_left common_field forpicture" @if($attID !=142) style="display: none;" @endif>
                    
                      <div class="video_dur"  style="margin-top:30px; float:left; width:100%;">
                        <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.numberof_picture'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.numberof_picture'); @endphp </span> </label>
                        <div class="info100">
                          <div>
                            <input class="xs_small notzero" maxlength="5" type="text" onkeypress="return isNumber(event)" name="no_of_picture" value="{{$PictureNumber->value or ''}}" id="no_of_picture">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field forvideo" @if($attID !=143) style="display: none;" @endif >
                    <div class="numb_pics" >
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.duration_video'); @endphp <span class="msg_img_replace">(Hrs)</span></span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.duration_video'); @endphp <span class="msg_img_replace">(Hrs)</span></span> </label>
                      <div class="info100" >
                        <div>
                          <input class="xs_small notzero" maxlength="4" type="text"  onkeypress="return isNumber(event)" name="duration" value="{{$VideoDuration->value or ''}}" id="duration">
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> </label>
                    <div class="info100">
                      <input class="xs_small notzero" maxlength="9" type="text"  onkeypress="return isNumber(event)" name="price" value="{{$fetchfirstdata->pro_price or ''}}" id="price" required="">
                    </div>
                  </div>

  <div class="form_row" style="display: none;">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="hidden" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="9999"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>

                  
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span> </label>
                    <div class="info100">
                      <div class="englishd">
                        <input type="text" class="xs_small notzero" name="discount"  onkeypress="return isNumber(event)" maxlength="2" value="{{$fetchfirstdata->pro_discount_percentage or ''}}" id="discount"  >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_IMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="proimage" class="info-file" type="file"   value="">
                          <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
                        @if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!='')
                        <div class="form-upload-img"><img src="{{$fetchfirstdata->pro_Img or ''}}"></div>
                        @endif </div>
                    </div>
                  </div>


   
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->



                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea name="description" maxlength="500" class="english" required=""  id="description" rows="4" cols="50">{{$fetchfirstdata->pro_desc or ''}}</textarea>
                      </div>
                      <div  class="arabic ar" >
                        <textarea class="arabic ar" maxlength="500"  name="description_ar" required=""  id="description_ar" rows="4" cols="50">{{$fetchfirstdata->pro_desc_ar or ''}} </textarea>
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="{{request()->id}}">
                      <input type="hidden" name="sid" value="{{request()->sid}}">
                      <input type="hidden" name="autoid" value="{{request()->autoid}}">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- global_area -->
      </div>
    </div>
    <!-- right_panel -->
  </div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                      title: {
                       required: true,
                      },
                       title_ar: {
                       required: true,
                      },
                       package_for: {
                       required: true,
                      },
                     
                       price: {
                       required: true,
                      },                   
                      description: {
                       required: true,
                      },
                      description_ar: {
                       required: true,
                      },
              @if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!='')
                      proimage: {
                       required:false,
                       accept:"png|jpe?g|gif",
                      },
                      @else
                      proimage: {
                       required:true,
                       accept:"png|jpe?g|gif",
                      },
                      @endif
                    
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {

                    title: {
                    required:  " {{ trans('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME') }} ",
                    }, 
                   
                    price: {
                    required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE') }} @endif",
                    },
                    package_for: {
                    required:  "@if (Lang::has(Session::get('mer_lang_file').'.Please_enter_package_for')!= '') {{  trans(Session::get('mer_lang_file').'.Please_enter_package_for') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Please_enter_package_for') }} @endif",
                    }, 
                    no_of_camra: {
                    required:  "@if (Lang::has(Session::get('mer_lang_file').'.Please_enter_no_of_camera')!= '') {{  trans(Session::get('mer_lang_file').'.Please_enter_no_of_camera') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Please_enter_no_of_camera') }} @endif",
                    }, 
                    duration: {
                    required:  "@if (Lang::has(Session::get('mer_lang_file').'.Please_enter_duration')!= '') {{  trans(Session::get('mer_lang_file').'.Please_enter_duration') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Please_enter_duration') }} @endif",
                    }, 
                    no_of_picture: {
                    required:  "@if (Lang::has(Session::get('mer_lang_file').'.Please_enter_no_of_picture')!= '') {{  trans(Session::get('mer_lang_file').'.Please_enter_no_of_picture') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Please_enter_no_of_picture') }} @endif",
                    }, 
                    price: {
                    required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE') }} @endif",
                    }, 
                    description: {
                    required:  " {{ trans('mer_en_lang.MER_VALIDATION_ABOUT') }} ",
                    },
                     title_ar: {
                    required:  " {{ trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME') }} ",
                    },
                    description_ar: {
                    required:  " {{ trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR') }} ",
                    },
                    proimage: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                     },  
                                       
                     
                },
                invalidHandler: function(e, validation){
                   
                    var valdata=validation.invalid;
    @if($mer_selected_lang_code !='en')
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');    
                    }

                     if (typeof valdata.package_for != "undefined" || valdata.package_for != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                      
                    }

@else
                      if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     
                    }
                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');    
                    }
                     if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.proimage != "undefined" || valdata.proimage != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.package_for != "undefined" || valdata.package_for != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }

@endif


                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

function checkinfo(str)
{
  if(str == 142)
  {
  $('.forpicture').show();
  $('.forvideo').hide();
  $('.forpicture').removeClass('ar');
  $('.forvideo').addClass('ar');
  $('#no_of_camra').attr("required", true);
  $('#no_of_picture').attr("required", true);
  $('#duration').removeAttr('required');


  }
  if(str == 143)
  {


  $('.forpicture').hide();
  $('.forvideo').show();
 $('.forpicture').addClass('ar');
  $('.forvideo').removeClass('ar');
  $('#no_of_camra').removeAttr('required');
  $('#no_of_picture').removeAttr('required');
  $('#duration').attr("required", true);

  }
 

}
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script>


<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->
@include('sitemerchant.includes.footer')
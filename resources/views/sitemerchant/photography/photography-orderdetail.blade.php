@include('sitemerchant.includes.header')  
@php $photographybig_leftmenu =1; @endphp 
 
<!--start merachant-->
<div class="merchant_vendor">
  <!--left panel-->
 @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->
@php
	$id                = request()->id;
	$sid               = request()->sid;
	$opid              = request()->opid;
	$cusid             = request()->cusid;
	$oid               = request()->oid;
  $ordid=$oid;
	$getCustomer       = Helper::getuserinfo($cusid);
	$shipaddress       = Helper::getgenralinfo($oid);  
	$cityrecord        = Helper::getcity($shipaddress->shipping_city);
	$getorderedproduct = Helper::getorderedproduct($oid);
	//echo '<pre>';print_r($shipaddress);die;
@endphp

  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif
        <a href="{{url('/')}}/photography-order/{{$id}}/{{$sid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a></h5></div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} @endphp  <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" >
                        @php 
						$ordertime=strtotime($getorderedproduct->created_at);
                        $orderedtime = date("d M Y",$ordertime);
						if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                       @endphp
                   </div>
                  </div>
                </div>
				<div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
                  @php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                   <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                  </div>
                  @php } @endphp
                </div>
				<div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                   <div class="info100">
				   @php if(isset($shipaddress->shipping_id) && $shipaddress->shipping_id!='')
					 { 
					  $getName = Helper::getShippingMethodName($shipaddress->shipping_id);
					  echo $getName;
					 }
					 else
					 {
					  echo 'N/A';
					 }
					@endphp
				 </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
				  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100"> 
					@php if(isset($shipaddress->shipping_charge) && $shipaddress->shipping_charge!='' && $shipaddress->shipping_charge!='0')
					 {
					  echo $shipaddress->shipping_charge;
					 }
					 else
					 {
					  echo 'N/A';
					 }
					 @endphp
				 </div>
                  </div>
                  <!--<div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Total_Price')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Total_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Total_Price') }} @endif</label>
                    <div class="info100">SRA @php if(isset($getorderedproduct->total_price) && $getorderedproduct->total_price!=''){echo  number_format((float)$getorderedproduct->total_price, 2, '.', '');} @endphp</div>
                  </div>-->
                  
                </div>
                
               <!-- <div class="noneditbox-sub-heading">@if (Lang::has(Session::get('mer_lang_file').'.item_information')!= '') {{  trans(Session::get('mer_lang_file').'.item_information') }} @else  {{ trans($MER_OUR_LANGUAGE.'.item_information') }} @endif</div>-->
              </div>
			   @php $pro_title='pro_title';   @endphp
                   @if($mer_selected_lang_code !='en')
                                         @php $pro_title='pro_title_'.$mer_selected_lang_code; @endphp
                   @endif 
                @php 
				$basetotal = 0;$couponcode=0; $couponcodeinfo='';
				$i = 1;
				if(count($getPhotographyStudio) > 0){
				foreach($getPhotographyStudio as $bodymeasurement)
				{
				  $basetotal            = ($basetotal+$bodymeasurement->total_price);
                  $pid                  = $bodymeasurement->product_id;
                  $getorderedproducttbl = Helper::getProduckInfo($pid);
				  $productproductimage  = Helper::getproductimage($pid);
				  if(isset($getorderedproducttbl->pro_disprice) && $getorderedproducttbl->pro_disprice!='0'){$getPrice = $getorderedproducttbl->pro_disprice;}else{$getPrice = $getorderedproducttbl->pro_price;}
				  $department_name = Helper::getProduckInfoAttribute($getorderedproducttbl->attribute_id);
				  $extraInfo       = Helper::getExtraInfo($bodymeasurement->product_id,$bodymeasurement->order_id,$bodymeasurement->cus_id);
				  $productinfo     = Helper::getAllProductAttribute($bodymeasurement->product_id,$bodymeasurement->merchant_id);
				  //echo '<pre>';print_r($productinfo[0]->value);die;
              $couponcode=$couponcode+$bodymeasurement->coupon_code_amount;;
        $couponcodeinfo=$bodymeasurement->coupon_code;
				@endphp
			  <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                    <div class="style_area">
                      <?php if($i==1){?><div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.item_information')!= '') {{  trans(Session::get('mer_lang_file').'.item_information') }} @else  {{ trans($MER_OUR_LANGUAGE.'.item_information') }} @endif</div><?php } ?>
                      <div class="style_box_type">
                        <div class="sts_box">
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.PACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PACKAGE') }} @endif</div>
                            <div class="style_label_text"> @php if(isset($getorderedproducttbl->pro_title) && $getorderedproducttbl->pro_title!=''){echo $getorderedproducttbl->$pro_title;} @endphp</div>
                          </div>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Quantity')!= '') {{  trans(Session::get('mer_lang_file').'.Quantity') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Quantity') }} @endif</div>
                            <div class="style_label_text"> {{$bodymeasurement->quantity or '' }}</div>
                          </div>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif</div>
                            <div class="style_label_text"> SAR {{number_format($bodymeasurement->total_price,2)}}</div>
                          </div>
                        </div>
                        <div class="sts_box">
						  <?php if(isset($department_name->attribute_title) && $department_name->attribute_title!=''){ ?>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_FOR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FOR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FOR') }} @endif</div>
                            <div class="style_label_text"> {{$department_name->attribute_title}}</div>
                          </div>
						  <?php } if(isset($extraInfo->date) && $extraInfo->date!=''){ ?>	
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DATE') }} @endif</div>
                            <div class="style_label_text"> <?php echo date('d M Y',strtotime($extraInfo->date)); ?></div>
                          </div>
						  <?php } if(isset($extraInfo->time) && $extraInfo->time!=''){ ?>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Time')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Time') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Time') }} @endif</div>
                            <div class="style_label_text"> <?php echo $extraInfo->time; ?></div>
                          </div>
						 <?php }  ?>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Location')!= '') {{  trans(Session::get('mer_lang_file').'.Location') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Location') }} @endif</div>
                            <div class="style_label_text"> <?php if(isset($extraInfo->elocation) && $extraInfo->elocation!=''){echo $extraInfo->elocation; }else{echo 'N/A';} ?></div>
                          </div>
                        </div>
                        <div class="sts_box">
						  <?php if(isset($productinfo[0]->value) && $productinfo[0]->value!=''){ if(isset($bodymeasurement->product_sub_type) && $bodymeasurement->product_sub_type=='video'){  ?>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Duration_of_Video')!= '') {{  trans(Session::get('mer_lang_file').'.Duration_of_Video') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Duration_of_Video') }} @endif</div>
                            <div class="style_label_text"> {{$productinfo[0]->value}}</div>
                          </div>
						  <?php } else{ ?>
						  <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Number_of_Pictures')!= '') {{  trans(Session::get('mer_lang_file').'.Number_of_Pictures') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Number_of_Pictures') }} @endif</div>
                            <div class="style_label_text"> {{$productinfo[0]->value}}</div>
                          </div>
						  <?php } } if(isset($productinfo[1]->value) && $productinfo[1]->value!=''){ ?>	
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.Number_of_Cameras')!= '') {{  trans(Session::get('mer_lang_file').'.Number_of_Cameras') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Number_of_Cameras') }} @endif</div>
                            <div class="style_label_text"> <?php echo $productinfo[1]->value; ?></div>
                          </div>
						  <?php }  ?>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</div>
                            <div class="style_label_text"> 
							@if($productproductimage->pro_Img)   
                               <img src="{{ $productproductimage->pro_Img }}" alt="" width="150">
                            @else
                                No image
                            @endif
						  </div>
                          </div>
						  
                        </div>
                      </div>
                    </div>
                    
                  </div>
			       @php $i++;} @endphp
                <div class="merchant-order-total-area">
                <div class="merchant-order-total-line"> @if(isset($couponcode) && $couponcode>=1)
             {{ (Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')}}: 

             @php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp  
          <br>

          {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')}}: {{ $couponcodeinfo }}
 <br>
          @endif



            @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             @endphp

          {{ (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp 
				 </div>
				 
                <div class="merchant-order-total-line">
				{{ (Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($MER_OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
				<?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);
				 } ?>              
				</div></div>
				<?php } ?>
              <!-- hall-od-top -->
              
            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
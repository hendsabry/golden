@include('sitemerchant.includes.header')
@php $popular_band_leftmenu =1; @endphp
<!--start merachant-->
<div class="merchant_vendor">
 @php

  $id                = $id;
  $hid               = $hid;
  $aid               = $aid;
  $cusid             = $cusid;
  $orderid           = $orderid;

  $getCustomer       = Helper::getuserinfo($cusid);
  $shipaddress       = Helper::getgenralinfo($orderid);  
  $cityrecord        = Helper::getcity($shipaddress->shipping_city);
  $getorderedproduct = Helper::getorderedproduct($orderid);
  @endphp
  <!--left panel-->
 @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->


  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif
     <a href="{{url('/')}}/popular-band-order-list/{{$id}}/{{$hid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a>
    </h5>
       </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} @endphp <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" > @php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      @endphp </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
                    @php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                  </div> 
                    @php } @endphp


                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                    <div class="info100"> @php if(isset($shipaddress->shipping_id) && $shipaddress->shipping_id!='')
                      { 
                      $getName = Helper::getShippingMethodName($shipaddress->shipping_id);
                      echo $getName;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100"> @php if(isset($shipaddress->shipping_charge) && $shipaddress->shipping_charge!='' && $shipaddress->shipping_charge!='0')
                      {
                      echo $shipaddress->shipping_charge;
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                    <!-- box -->
                  </div>
                </div>
                <!--global end-->
              </div>
        @php 
        $basetotal = 0;
        $i = 1;
        if(count($productdata) > 0){
        foreach($productdata as $val)
        {
          $basetotal            = ($basetotal+$val->total_price);
          $pid                  = $val->product_id;    

$nm_musi  = Helper::nm_music($val->product_id);  

$MuSName = Helper::getMusicName($hid);

       $enq_id               = Helper::quoteRequested($val->shop_id);    
        $enq_id               = Helper::quoteRequested($val->shop_id);  
        $dget_category        = Helper::quoteRequestedName($enq_id->enq_id);
        @endphp
      <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
        <div class="style_area">
          <?php if($i==1){?>
          <div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.DETAIL_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.DETAIL_INFO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.DETAIL_INFO') }} @endif</div>
          <?php } ?>
          <div class="style_box_type">
            <div class="sts_box">
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.name')!= '') {{  trans(Session::get('mer_lang_file').'.name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.name') }} @endif</div>
                <div class="style_label_text"> 

                  <?php if(isset($nm_musi->name) && $nm_musi->name!=''){echo $nm_musi->name;}else{echo $MuSName;} ?>
                    


                  </div>
              </div> 
              <div class="style_left">
                <div class="style_label">&nbsp;</div>
                <div class="style_label_text">&nbsp;</div>
              </div>    


        <!--div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Location')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Location') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Location') }} @endif</div>
                <div class="style_label_text"> <?php if(isset($dget_category->location) && $dget_category->location!=''){echo $dget_category->location;}else{echo 'N/A';} ?></div>
              </div-->             
              <div class="style_left">
                <div class="style_label">@if(Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif</div>
                <div class="style_label_text">SAR {{ number_format($val->total_price,2) }}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @php $i++;} @endphp
      <div class="merchant-order-total-area">
                <div class="merchant-order-total-line">@if (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.VAT_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.VAT_CHARGE') }} @endif: 

                   @php
                      $vatamonu = Helper::calculatevat($shipaddress->order_id,$basetotal);
                        $totalnetamount=$basetotal + $vatamonu;
                      @endphp

          @php if(isset($vatamonu) && $vatamonu > 0 )
          {
          echo 'SAR '.number_format($vatamonu,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp </div>
      <div class="merchant-order-total-line">
        @if (Lang::has(Session::get('mer_lang_file').'.Total_Price')!= '') {{  trans(Session::get('mer_lang_file').'.Total_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Total_Price') }} @endif:
        <?php if(isset($shipaddress) && $shipaddress!=''){
         echo 'SAR '.number_format(($totalnetamount),2);
         } ?>
      </div></div>
      <?php } ?>

            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
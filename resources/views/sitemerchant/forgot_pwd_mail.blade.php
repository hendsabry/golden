﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
     <meta charset="UTF-8" />
    <title><?php echo $SITENAME; ?> <?php if (Lang::has(Session::get('mer_lang_file').'.MER_ADMIN_DASHBOARD_TEMPLATE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_ADMIN_DASHBOARD_TEMPLATE');}  else { echo trans($OUR_LANGUAGE.'.MER_ADMIN_DASHBOARD_TEMPLATE');} ?> | <?php if (Lang::has(Session::get('mer_lang_file').'.MER_FORGOT_PASSWORD_PAGE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_FORGOT_PASSWORD_PAGE');}  else { echo trans($OUR_LANGUAGE.'.MER_FORGOT_PASSWORD_PAGE');} ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
     <!-- PAGE LEVEL STYLES -->
     <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/css/login.css" />
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/magic/magic.css" />
<?php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); if(count($favi)>0) { foreach($favi as $fav) {} ?>
    <link rel="shortcut icon" href="<?php echo url(''); ?>/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
<?php } ?>
     <!-- END PAGE LEVEL STYLES -->
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body style="background:url(assets/img/bg1.jpg) no-repeat center; background-position:cover" >

   <!-- PAGE CONTENT --> 
    <div class="container">
    <div class="text-center">
        <img src="<?php echo $SITE_LOGO; ?>" alt="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_LOGO')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_LOGO');}  else { echo trans($OUR_LANGUAGE.'.MER_LOGO');} ?>" />
    </div>

    
    <div class="tab-content">
    
        <div id="login" class="tab-pane active"  >
        
   	{!! Form::open(array('url'=>'forgot_pwd_email_submit','class'=>'form-signin')) !!}


	@foreach($merchantdetails as $info)


<input type="hidden" value="<?php echo $info->mer_id;?>" name="merchant_id">
	@endforeach
<div id="error_msg"  style="color:#F00;font-weight:800"  > </div>   
<div class="user_area">     
 <div class="user_text">
                    <?php if (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PASSWORD_RESET1')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_MERCHANT_PASSWORD_RESET1');}  else { echo trans($OUR_LANGUAGE.'.MER_MERCHANT_PASSWORD_RESET1');} ?>
                </div>
             <div class="user_input">   <input type="password" class="form-control" placeholder="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_NEW_PASSWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_NEW_PASSWORD');}  else { echo trans($OUR_LANGUAGE.'.MER_NEW_PASSWORD');} ?>" name="pwd" id="pwd"/>
                 <input type="password" class="form-control" placeholder="<?php if (Lang::has(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_CONFIRM_PASSWORD');}  else { echo trans($OUR_LANGUAGE.'.MER_CONFIRM_PASSWORD');} ?>" name="confirmpwd" id="confirmpwd"/>
				 </div>
				 </div>
               

		 <button id="updatepwd"   class="btn btn-warning btn-sm btn-grad recover" style="color:#fff"> <?php if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_UPDATE');}  else { echo trans($OUR_LANGUAGE.'.MER_UPDATE');} ?></button>
            </form>
    @if (Session::has('error'))
		<div class="alert alert-danger alert-dismissable" align="center" style="height:50px;width:270px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>{!! Session::get('error') !!}</div>
		@endif
        </div>
 
 
 
        
    </div>
    <div class="text-center ">
        <ul class="list-inline">
    
 		<li><a  href="<?php echo url('')?>/sitemerchant"    ><?php if (Lang::has(Session::get('mer_lang_file').'.MER_BACK_TO_LOGIN')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_BACK_TO_LOGIN');}  else { echo trans($OUR_LANGUAGE.'.MER_BACK_TO_LOGIN');} ?></a></li>
		 
        </ul>
    </div>


</div>

	  <!--END PAGE CONTENT -->     
	      
      <!-- PAGE LEVEL SCRIPTS -->
   <script src="<?php echo url('')?>/public/assets/plugins/jquery-2.0.3.min.js"></script>

  <script>
   $(document).ready(function(){
		
var pwd= $('#pwd');
var confirmpwd= $('#confirmpwd');
	 $('#updatepwd').click(function() {
 
		 if(pwd.val()=="")
		{
			pwd.css('border', '1px solid red');
			confirmpwd.css('border', ''); 
			$('#error_msg').html('<?php if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_NEW_PASSWORD');}  else { echo trans($OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_NEW_PASSWORD');} ?>');
			pwd.focus();
			return false;
		}
		else if(confirmpwd.val()=="")
		{
			confirmpwd.css('border', '1px solid red');
			 pwd.css('border', ''); 
			$('#error_msg').html('<?php if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD');}  else { echo trans($OUR_LANGUAGE.'.MER_PLEASE_PROVIDE_CONFIRM_PASSWORD');} ?>');
			confirmpwd.focus();
			return false;
		}
		else if(pwd.val() != confirmpwd.val())
		{
			confirmpwd.css('border', '1px solid red');
			pwd.css('border', '1px solid red');
			$('#error_msg').html('<?php if (Lang::has(Session::get('mer_lang_file').'.MER_BOTH_PASSWORDS_DO_NOT_MATCH')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_BOTH_PASSWORDS_DO_NOT_MATCH');}  else { echo trans($OUR_LANGUAGE.'.MER_BOTH_PASSWORDS_DO_NOT_MATCH');} ?>');
			 
			return false;
		
		}
		 
		 
		

});
 });
   
   </script>

  
   <script src="<?php echo url('')?>/public/assets/plugins/bootstrap/js/bootstrap.js"></script>
   <script src="<?php echo url('')?>/public/assets/js/login.js"></script>
      <!--END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
    <!-- END BODY -->
</html>

@include('sitemerchant.includes.header')    
@php $hall_leftmenu =1; @endphp
<div class="merchant_vendor">
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
<div class="right_panel">
  <div class="inner">
    <header>
      <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_HALL_PAID_SERVICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_HALL_PAID_SERVICE') }}  
        @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_HALL_PAID_SERVICE') }} @endif </h5>
      @include('sitemerchant.includes.language') </header>
    <!-- Display Message after submition --> 
    @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif 
    <!-- Display Message after submition -->
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box"> 
            
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <div class="one-call-form"> {!! Form::open(array('url' => '/add_paidservices','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'add_freeservices', 'name' =>'add_freeservices' )) !!}
              
              @if($getDbC >=1)
              
              @for($i=0;$i<$getDbC;$i++)
 

            
              <div class="form_row posrel">
                  <div class="form_row_left">
                <div id="remove_button" class="remove_btn"><a href="javascript:void(0);" title="Remove field" class="status_active  status_active2 cstatus" data-status="Active" data-id="{{$getDb[$i]->id}}">&nbsp;</a></div>
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NAME'); @endphp </span> </label>
                <div class="info100">
                  <div class="has-success">
                    <div class="english">
                      <input type="text" class="english" maxlength="50"  name="servicename[{{$i}}]" value="@php echo $getDb[$i]->value;  @endphp">
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" maxlength="50"  name="servicename_ar[{{$i}}]" value="@php echo $getDb[$i]->value_ar;  @endphp">
                    </div>
                  </div>
                  <!-- has-success --> 
                </div>
                </div>
                <!-- info100 --> 
              </div>
              <!-- form_row -->
              
              <div class="form_row common_field">
                 <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_PRICE'); @endphp </span> </label>
                <div class="info100">
                  <div class="has-success ">
                    <input type="text" class="small-sel sprice" maxlength="7"  onkeypress="return isNumber(event)"  id="price" name="price[{{$i}}]" value="@php echo $getDb[$i]->price;  @endphp">
                  </div>
                  <!-- has-success --> 
                </div>
                <!-- info100 --> 
              </div></div>
              <!-- form_row -->
              
           <!--    <div class="form_row">
                <div class="form_row_left">
<label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NOTES'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NOTES'); @endphp </span> </label>
                <div class="info100">
                  <div class="has-success">
                    <div class="english">
                      <textarea class="english" name="servicenotes[{{$i}}]">@php echo $getDb[$i]->option_note_value;  @endphp</textarea>
                    </div>
                    <div class="arabic ar">
                      <textarea type="text" name="servicenotes_ar[{{$i}}]" class="arabic ar">@php echo $getDb[$i]->option_note_value_ar;  @endphp</textarea>
                    </div>
                  </div>
              
                </div>
             
              </div>  </div> -->
              <!-- form_row --> 
              
              @endfor
              
              @else
              <div class="form_row"> <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NAME'); @endphp </span> </label>
                <div class="info100">
                  <div class="has-success">
                    <div class="english">
                      <input class="english" type="text" name="servicename[0]" maxlength="50" >
                    </div>
                    <div class="ar arabic">
                      <input type="text" class="ar arabic" name="servicename_ar[0]" maxlength="50" >
                    </div>
                  </div>
                  <!-- has-success --> 
                </div></div>
                <!-- info100 --> 
              </div>
              <!-- form_row -->
              
              <div class="form_row common_field">  <div class="form_row_left">
                <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_PRICE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_PRICE'); @endphp </span> </label>
                <div class="info100">
                  <div class="has-success">
                    <input type="text" class="inp_price sprice notzero" maxlength="7"  id="price" onkeypress="return isNumber(event)"  name="price[0]" value="">
                  </div>
                  <!-- has-success --> 
                </div></div>
                <!-- info100 --> 
                
              </div>
              <!-- form_row -->
              <!-- 
              <div class="form_row"> <div class="form_row_left">
                <label class="form_label"><span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_NOTES'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_NOTES'); @endphp </span> </label>
                <div class="info100">
                  <div class="has-success">
                    <div class="english">
                      <textarea class="english" type="text" name="servicenotes[0]"></textarea>
                    </div>
                    <div>
                      <div class="arabic ar">
                        <textarea type="text" class="arabic ar " name="servicenotes_ar[0]"></textarea>
                      </div>
                    </div>
                  
                  </div></div>
              
                </div>
          
                </div> -->
                @endif
                <div id="img_upload"></div>
                <div class="form_row">
				<div class="form_row_left">
                  <div id="add_button" class=""><a href="javascript:void(0);" class="form-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_ADDMORE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_ADDMORE'); @endphp </span> </a></div>
				  </div>
                </div>
                @php $Count = $getDbC +1;@endphp
				
                <input type="hidden" id="count" name="count" value="{{$Count}}">
                <input type="hidden" id="hid" name="hid" value="{{$_REQUEST['hid']}}">
                <input type="hidden" id="product_option_id" name="product_option_id" value="4">
                <div class="form_row english">
				<div class="form_row_left">
                  <input type="submit" name="addfreeservice" value="Submit">
				  </div>
                </div>
                <div class="form_row arabic ar">
				<div class="form_row_left">
                  <input type="submit" name="addfreeservice" value=" {{ trans('mer_ar_lang.MER_SUBMIT') }}">
				  </div>
                </div>
                {!! Form::close() !!} 
                
                <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              </div>
              
              
              
            </div><!-- one-call-form --> 
            
            
          </div>
        </div>
      </div>
    </div>
    <!-- global_area --> 
  </div>
  <!-- right_panel --> 
</div>
</div>
<!-- merchant_vendor --> 

<script type="text/javascript">
		$(document).ready(function(){
		var maxField = 11;  
		var addButton = $('#add_button');  
		var wrapper = $('#img_upload');
		var x = @php echo $Count; @endphp;  
		$(addButton).click(function(){  
		if(x < maxField){  
		x++; 

      var testElement= document.getElementById('english_tab');
      var result = testElement.classList.contains('active')
      if(result)
      {
        var enclass=''
        var arclass='ar'
      }
      else
      {
        var enclass='ar'
        var arclass= ''

      }

 

    var fieldHTML = '<div class="add_row_wrap addrow_service main'+x+'"><div id="remove_button" class="remove_btn" onclick="javascript: removemain('+x+')"><a href="javascript:void(0);"  title="Remove field">&nbsp;</a></div><div class="form_row"><div class="form_row_left"><label class="form_label"><span class="english '+enclass+'">@php echo lang::get('mer_en_lang.MER_SERVICE_NAME'); @endphp</span><span  class="arabic '+arclass+' "> @php echo lang::get('mer_ar_lang.MER_SERVICE_NAME'); @endphp</span></label><div class="info100"><div class="has-success"><div class="english '+enclass+' "> <input type="text" class="english sname  '+enclass+' " name="servicename['+x+']" maxlength="50" ></div><div class="arabic  '+arclass+'"><input type="text" class="arabic  '+arclass+' snamear" name="servicename_ar['+x+']" maxlength="50" ></div></div></div></div></div><div class="form_row common_field"><div class="form_row_left"><label><span class="english '+enclass+' ">@php echo lang::get('mer_en_lang.MER_SERVICE_PRICE'); @endphp</span><span  class="arabic  '+arclass+'"> @php echo lang::get('mer_ar_lang.MER_SERVICE_PRICE'); @endphp</span></label><div class="info100"><div class="has-success"><input type="text" class="inp_price  sprice" onkeypress="return isNumber(event)" maxlength="7"   name="price['+x+']" value=""></div></div></div></div></div></div></div></div>';

   
		$(wrapper).append(fieldHTML);  
     createValidation();
		document.getElementById('count').value = parseInt(x);
		}
		});
		/*$(wrapper).on('click', '#remove_button', function(e){  
		e.preventDefault();
		$(this).parent('div').remove(); 
		x--;  
		document.getElementById('count').value = parseInt(x);
		});*/
		});

     function removemain(val)
  {
      var x = document.getElementById('count').value;
      $('.main'+val).remove();
      x--;  
      document.getElementById('count').value = parseInt(x);

  }
	</script>
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Want_Delete')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Want_Delete') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Want_Delete') }} @endif')
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'deleterecord'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#add_freeservices").validate({
                  ignore: [],
                  rules: {
                  'servicename[0]': {
                       required: true,
                      },

                      'servicenotes[0]': {
                       required: true,
                      },
                      'servicenotes_ar[0]': {
                       required: true,
                      },

                      'servicename_ar[0]': {
                       required: true,
                      },

                      'price[0]': {
                       required: true,
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             'servicename[0]': {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PAID_SERVICES_NAME') }} @endif",
                      },  

                      'servicenotes[0]': {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NOTES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NOTES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PAID_SERVICES_NOTES') }} @endif",
                      }, 

                 'servicename_ar[0]': {
               required: "{{ trans('mer_ar_lang.MER_VALIDATION_FREE_SERVICES_NAME_AR') }}",
                      }, 

              'servicenotes_ar[0]': {
               required: "{{ trans('mer_ar_lang.MER_VALIDATION_FREE_SERVICES_NOTES_AR') }}",
                      }, 



              'price[0]': {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PAID_PRICE') }} @endif",
                      }, 
                                    
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.errorList;
                    console.log("invalidHandler : validation", valdata[0].element.classList);
                    var i;
                    var erroren=0;
                    var start=10;
                    var lang ='ar';
                    var val=0
                    
                    for (i = 0; i < start ;  i++) {
                      
                      if (typeof valdata[i] !== 'undefined') {  
                        var data=valdata[i].element.classList;
                        $.inArray('ar',data);
                                        console.log("invalidHandler : validation", $.inArray('ar',data)); 
                                              if($.inArray('ar',data) == -1) 
                                              {
                                                  val=1;

                                              } 

                                          } 
                                         
                      }

                                             @if($mer_selected_lang_code !='ar')
                                              if(val==0)
                                              {

                                                $('.arabic_tab').trigger('click');  
                                              }
                                              else
                                              {

                                                $('.english_tab').trigger('click');
                                              }
                                        @else

                                         if(val==0)
                                              {
                                                     $('.english_tab').trigger('click');
                                               
                                              }
                                              else
                                              {

                                                    $('.arabic_tab').trigger('click');  

                                              
                                              }
                                       @endif       
                                        


                    },


                submitHandler: function(form) {
                    form.submit();
                }
            });

 var createValidation = function() {
  $(".sname").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PAID_SERVICES_NAME') }} @endif"
      }
    });
  });

  $(".snotes").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NOTES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_SERVICES_NOTES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PAID_SERVICES_NOTES') }} @endif"
      }
    });
  });

$(".snotesar").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
        required: "{{ trans('mer_ar_lang.MER_VALIDATION_FREE_SERVICES_NOTES_AR') }}"
      }
    });
  });

$(".snamear").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
        required: "{{ trans('mer_ar_lang.MER_VALIDATION_FREE_SERVICES_NAME_AR') }}"
      }
    });
  });

$(".sprice").each(function() {
    $(this).rules('remove');
    $(this).rules('add', {
      required: true,
      messages: {
        required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PAID_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PAID_PRICE') }} @endif"
      }
    });
  });


}

</script> 
@include('sitemerchant.includes.footer') 



<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT')!= '')   ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT') }} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_STORE_DETAILS')!= '') ? trans(Session::get('mer_lang_file').'.MER_ADD_STORE_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_ADD_STORE_DETAILS') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="{{ url('') }}/public/assets/css/plan.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         {!! $merchantheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      {!! $merchantleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_STORE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_ADD_STORE') : trans($MER_OUR_LANGUAGE.'.MER_ADD_STORE') }}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_STORE_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_STORE_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_ADD_STORE_DETAILS')}}</h5>
            
        </header>
        @if ($errors->any())
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>')) !!}
		{{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
         
        </div>
		</ul>	
		@endif
         @if (Session::has('mail_exist'))
		<div class="alert alert-warning alert-dismissable">{!! Session::get('mail_exist') !!}
        {{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
		</div>
		@endif
        
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    
                    {!! Form::open(array('url'=>'merchant_add_shop_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
                   
					
					 <div class="panel panel-default">
                        <div class="panel-heading">
						{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_DETAILS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_STORE_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_STORE_DETAILS') }} 
                        </div>

                        
                <div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_NAME')  :  trans($MER_OUR_LANGUAGE.'.MER_STORE_NAME') }}<span class="text-sub">*</span></label>

						<div class="col-lg-4">
						{{ Form::hidden('store_merchant_id',$id,array('id'=>'store_merchant_id')) }}
							<input type="hidden" name="store_merchant_id" id="store_merchant_id" value="{{ $id }}" ?>
							<input type="text" class="form-control" placeholder="Enter Store Name {{ $default_lang }}" id="store_name" name="store_name" value="{!! Input::old('store_name') !!}"  maxlength="150">
							<div id="store_name_error_msg"  style="color:#F00;font-weight:800"  > </div>
						</div>
					</div>
                </div>
				<?php 
				/* print_r($get_active_lang); */ ?>
				@if(!empty($get_active_lang)) 
				@foreach($get_active_lang as $get_lang) 
				<?php 
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="panel-body">
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">Store Name({{ $get_lang_name }})<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input id="store_name_<?php echo strtolower($get_lang_name); ?>"  name="store_name_<?php echo strtolower($get_lang_name); ?>" placeholder="Enter Store Name In {{ $get_lang_name}}" class="form-control" type="text"><div id="store_name_{{ $get_lang_code}}_error_msg"  style="color:#F00;font-weight:800"  ></div>
					</div>
                </div>
                </div>
				@endforeach
				@endif
				<!--
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Store Name({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

						<div class="col-lg-4">
							
							<input type="text" class="form-control" placeholder="Enter Store Name In {{ Helper::lang_name() }}" id="store_name_french" name="store_name_french" value="{!! Input::old('store_name_french') !!}"  >
							<div id="store_name_fr_error_msg"  style="color:#F00;font-weight:800"  > </div>
						</div>
					</div>
                </div>-->
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PHONE')!= '') ? trans(Session::get('mer_lang_file').'.MER_PHONE')  : trans($MER_OUR_LANGUAGE.'.MER_PHONE') }} <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					{{ Form::text('store_pho',Input::old('store_pho'),array('id'=>'store_pho','class' => 'form-control','maxlength' =>'16','placeholder' => 'Enter Phone Number')) }}
                        
                        <div id="pho_no_error_msg"  style="color:#F00;font-weight:800"  > </div>
                    </div>
                </div>
                        </div>
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS1')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADDRESS1') :   trans($MER_OUR_LANGUAGE.'.MER_ADDRESS1') }}<span class="text-sub">*</span></label>

						<div class="col-lg-4">
						
							<input type="text" class="form-control" placeholder="Enter Address One {{ $default_lang }}" id="store_add_one" maxlength="150" name="store_add_one" value="{!! Input::old('store_add_one') !!}"  >
							<div id="address1_error_msg"  style="color:#F00;font-weight:800"  > </div>
						</div>
					</div>
                </div>
				<?php 
				/* print_r($get_active_lang); */ ?>
				@if(!empty($get_active_lang)) 
				@foreach($get_active_lang as $get_lang) 
				<?php 
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="panel-body">
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">Address 1({{ $get_lang_name}})<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input id="store_add_one_<?php echo strtolower($get_lang_name); ?>"  name="store_add_one_<?php echo strtolower($get_lang_name); ?>" placeholder="Enter Address One In {{ $get_lang_name}}" class="form-control" type="text" ><div id="address1_{{ $get_lang_code}}_error_msg"  style="color:#F00;font-weight:800"  ></div>
					</div>
                </div>
                </div>
				@endforeach
						@endif
				<!--
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Address One({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Address One In {{ Helper::lang_name() }}" id="store_add_one_french" name="store_add_one_french" value="{!! Input::old('store_add_one_french') !!}"  >
							<div id="address1_fr_error_msg"  style="color:#F00;font-weight:800"  > </div>
						</div>
					</div>
                </div>-->
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_COUNTRY')}}<span class="text-sub">*</span></label>

						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Address two {{ $default_lang }}" id="store_add_two" maxlength="150" name="store_add_two" value="{!! Input::old('store_add_two') !!}"   >
							<div id="address2_error_msg"  style="color:#F00;font-weight:800"> </div>
						</div>
					</div>
                </div>
				<?php 
				/* print_r($get_active_lang); */ ?>
				@if(!empty($get_active_lang)) 
				@foreach($get_active_lang as $get_lang) 
				<?php 
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="panel-body">
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">Address 2({{ $get_lang_name }})<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input id="store_add_two_<?php echo strtolower($get_lang_name); ?>"  name="store_add_two_<?php echo strtolower($get_lang_name); ?>" placeholder="Enter Address Two In {{ $get_lang_name}}" class="form-control" type="text" ><div id="address2_{{ $get_lang_code}}_error_msg"  style="color:#F00;font-weight:800"  ></div>
					</div>
                </div>
                </div>
				@endforeach
				@endif
				<!--
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Address two({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

						<div class="col-lg-4">
							<input type="text" class="form-control" placeholder="Enter Address two In {{ Helper::lang_name() }}" id="store_add_two_french" name="store_add_two_french" value="{!! Input::old('store_add_two_french') !!}"   >
							<div id="address2_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
						</div>
					</div>
                </div>-->
                
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_COUNTRY')}}<span class="text-sub">*</span></label>

                    <div class="col-lg-4"> 
                       <select class="form-control" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >
                        <option value="">-- {{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')}} --</option>
                          @foreach($country_details as $country_fetch) 
          				<option value="{{ $country_fetch->co_id }}" <?php if(Input::old('select_country')==$country_fetch->co_id) echo 'selected';?>><?php echo $country_fetch->co_name; ?></option>
                     @endforeach
       					 </select>
                        <div id="country_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') ? trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') : trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       <select class="form-control" name="select_city" id="select_city" >
           				<option value="">--{{ (Lang::has(Session::get('mer_lang_file').'.MER_SELECT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SELECT') : trans($MER_OUR_LANGUAGE.'.MER_SELECT')}}--</option>
                  </select>
                  <div id="city_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ZIPCODE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ZIPCODE'): trans($MER_OUR_LANGUAGE.'.MER_ZIPCODE')}}<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					{{ Form::text('zip_code',Input::old('zip_code'),array('id'=>'zip_code','class' => 'form-control','maxlength' =>'16','placeholder' => 'Enter zipcode' )) }}
                        
                        <div id="zip_code_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
				</div>
				
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS'): trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS') }}<span class="text-sub"></span></label>

						<div class="col-lg-4">
						   <textarea  class="form-control" placeholder="Enter Meta Keywords {{ $default_lang }}" name="meta_keyword" id="meta_keyword" >{!! Input::old('meta_keyword') !!}</textarea>
						   <div id="meta_key_error_msg"  style="color:#F00;font-weight:800"> </div>
						</div>
					</div>
                </div>
				<?php 
				/* print_r($get_active_lang); */ ?>
				@if(!empty($get_active_lang)) 
				@foreach($get_active_lang as $get_lang)
				<?php
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="panel-body">
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_KEYWORDS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_META_KEYWORDS'): trans($MER_OUR_LANGUAGE.'.MER_META_KEYWORDS') }}({{ $get_lang_name }})<span class="text-sub"></span></label>

                    <div class="col-lg-4">
						<textarea  class="form-control" placeholder="Enter Meta Keywords In {{ $get_lang_name }}" name="meta_keyword_<?php echo strtolower($get_lang_name); ?>" id="meta_keyword_{{ strtolower($get_lang_name)}}" ></textarea><div id="meta_key_{{ $get_lang_code}}_error_msg"  style="color:#F00;font-weight:800"  ></div>
					</div>
                </div>
                </div>
				@endforeach
				@endif
				<!--
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Meta Keywords({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

						<div class="col-lg-4">
						   <textarea  class="form-control" placeholder="Enter Meta Keywords In {{ Helper::lang_name() }}" name="meta_keyword_french" id="meta_keyword_french" >{!! Input::old('meta_keyword_french') !!}</textarea>
						   <div id="meta_key_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
						</div>
					</div>
                </div>-->
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_META_DESCRIPTION')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_META_DESCRIPTION') : trans($MER_OUR_LANGUAGE.'.MER_META_DESCRIPTION')}}<span class="text-sub"></span></label>

						<div class="col-lg-4">
						   <textarea id="meta_description" placeholder="Enter Meta Description {{ $default_lang }}" name="meta_description" class="form-control">{!! Input::old('meta_description') !!}</textarea>
						   <div id="meta_desc_error_msg"  style="color:#F00;font-weight:800"> </div>
						</div>
					</div>
                </div>
				<?php 
				/* print_r($get_active_lang); */ ?>
				@if(!empty($get_active_lang)) 
				@foreach($get_active_lang as $get_lang) 
				<?php 
				$get_lang_code = $get_lang->lang_code;
				$get_lang_name = $get_lang->lang_name;
				?>
				<div class="panel-body">
				<div class="form-group"> 
                    <label for="text1" class="control-label col-lg-2">Meta Description({{ $get_lang_name}})<span class="text-sub"></span></label>

                    <div class="col-lg-4">
						<textarea  class="form-control" placeholder="Enter Meta Description In {{ $get_lang_name }}" name="meta_description_<?php echo strtolower($get_lang_name); ?>" id="meta_description_<?php echo strtolower($get_lang_name); ?>" ></textarea><div id="meta_desc_{{ $get_lang_code }}_error_msg"  style="color:#F00;font-weight:800"  ></div>
					</div>
                </div>
                </div>
				@endforeach
				@endif
				<!--
				<div class="panel-body">
                    <div class="form-group">
						<label class="control-label col-lg-2" for="text1">Meta Description({{ Helper::lang_name() }})<span class="text-sub">*</span></label>

						<div class="col-lg-4">
						   <textarea id="meta_description_french" placeholder="Enter Meta Description In {{ Helper::lang_name() }}" name="meta_description_french" class="form-control">{!! Input::old('meta_description_french') !!}</textarea>
						   <div id="meta_desc_fr_error_msg"  style="color:#F00;font-weight:800"> </div>
						</div>
					</div>
                </div>-->
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_WEBSITE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_WEBSITE') : trans($MER_OUR_LANGUAGE.'.MER_WEBSITE') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                        <input type="url" class="form-control" placeholder="https://www.google.co.in/" id="website" name="website" value="{!! Input::old('website') !!}"  >
                    </div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
						   {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-2', 'for' => 'text1'])) !!}
                    

                    <div class="col-lg-4">
                        <input id="pac-input"  class="form-control" type="text" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_TYPE_YOUR_LOCATION_HERE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TYPE_YOUR_LOCATION_HERE')  : trans($MER_OUR_LANGUAGE.'.MER_TYPE_YOUR_LOCATION_HERE') }} ({{ (Lang::has(Session::get('mer_lang_file').'.MER_AUTO-COMPLETE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_AUTO-COMPLETE') :  trans($MER_OUR_LANGUAGE.'.MER_AUTO-COMPLETE') }})" onclick="check();">
                        <div id="location_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
					<div class="col-lg-4"></div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MAP_SEARCH_LOCATION')!= '') ?  trans(Session::get('mer_lang_file').'.MAP_SEARCH_LOCATION') : trans($MER_OUR_LANGUAGE.'.MAP_SEARCH_LOCATION')}}<span class="text-sub">*</span><br><span  style="color:#999">({{ (Lang::has(Session::get('mer_lang_file').'.MER_DRAG_MARKER_TO_GET_LATITUDE_&_LONGITUDE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DRAG_MARKER_TO_GET_LATITUDE_&_LONGITUDE'): trans($MER_OUR_LANGUAGE.'.MER_DRAG_MARKER_TO_GET_LATITUDE_&_LONGITUDE')}} )</span></label>

                    <div class="col-lg-4">
                        <div id="map_canvas" style="width:300px;height:250px;" ></div>
                    </div>
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_LATITUDE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LATITUDE') : trans($MER_OUR_LANGUAGE.'.MER_LATITUDE') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					{{ Form::text('latitude',Input::old('latitude'),array('id'=>'latitude','class' => 'form-control','readonly')) }}
                       
                    </div>
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_LONGITUDE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_LONGITUDE') : trans($MER_OUR_LANGUAGE.'.MER_LONGITUDE') }}<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
					{{ Form::text('longitude',Input::old('longitude'),array('id'=>'longitude','class' => 'form-control','readonly')) }}
                        
                    </div>
                </div>
                        </div>
							
						<div class="panel-body">
                           <div class="form-group">
						  
                    <label class="control-label col-lg-2" for="text1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_IMAGE')   : trans($MER_OUR_LANGUAGE.'.MER_STORE_IMAGE')}}<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                     <span class="errortext red" style="color:red;"><em>{{ (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_IMAGE_SIZE_MUST_BE') : trans($MER_OUR_LANGUAGE.'.MER_IMAGE_SIZE_MUST_BE') }}  {{ $STORE_WIDTH}} x  {{ $STORE_HEIGHT }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_PIXELS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PIXELS') : trans($MER_OUR_LANGUAGE.'.MER_PIXELS') }}</em></span>
                       <input type="file" id="file" name="file" >
                       <div id="img_error_msg"  style="color:#F00;font-weight:800"> </div>
                    </div>
                </div>
                        </div>
                        
                              <div class="form-group">
							  {!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-3', 'for' => 'pass1'])) !!}
                    

                    <div class="col-lg-8">
                     <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" >{{ (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SUBMIT') : trans($MER_OUR_LANGUAGE.'.MER_SUBMIT')}}</a></button>
                     <button class="btn btn-danger btn-sm btn-grad" type="reset" ><a style="color:#ffffff;">{{ (Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_RESET') : trans($MER_OUR_LANGUAGE.'.MER_RESET') }}</a></button>
                   
                    </div>
            
                </div>
                    </div>
					
                    	
				
		
                
                {{ Form::close() }}
                </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      {!! $merchantfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
 <script>
 function check() { 
      var mer_id     = $('#store_merchant_id').val();
      //var longitude  = $('#longitude').val();
      var longitude = document.getElementById("longitude").value;
      var latitude   = $('#latitude').val();
      var datas      = "mer_id="+mer_id+"&latitude="+latitude+"&longitude="+longitude;
     
            $.ajax({
            type: 'GET',
                  url: '<?php echo url('check_store_exists'); ?>',
          data: datas,
          success: function(response){  
                    
                     if(response==1){  //already exist
            alert("This Store Already Exist with this same address");
            $("#exist").val("1"); //already exist
            $('#latitude').val('');
            $('#longitude').val('');
            $("#latitude").css('border', '1px solid red'); 
            $('#longitude').css('border', '1px solid red');
            $('#pac-input').css('border', '1px solid red');
            $('#location_error_msg').html("Store Already Exist with this same address");  
            $("#pac-input").focus();
            return false;          
           }else
           {
            $("#latitude").css('border', ''); 
            $('#longitude').css('border', '');
            $('#pac-input').css('border', '');
            $('#location_error_msg').html(""); 
           // $("#pac-input").blur(); 
           }
         }    
      });
    
}

 

$( document ).ready(function() {
      var store_pho   = $('#store_pho');
      var zip_code    = $('#zip_code');

       /*Phone number*/
      $('#store_pho').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            store_pho.css('border', '1px solid red'); 
			$('#pho_no_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')}}');
			store_pho.focus();
			return false;
        }else{			
            store_pho.css('border', ''); 
			$('#pho_no_error_msg').html('');	        
		}
      });
      /*zip code*/
      $('#zip_code').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            zip_code.css('border', '1px solid red'); 
			$('#zip_code_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_NUMBERS_ONLY_ALLOWED'): trans($MER_OUR_LANGUAGE.'.MER_NUMBERS_ONLY_ALLOWED')}}');
			zip_code.focus();
			return false;
        }else{			
            zip_code.css('border', ''); 
			$('#zip_code_error_msg').html('');	        
		}
      });

	$('#submit').click(function() {
		<?php 
		/* print_r($get_active_lang); */
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_code = $get_lang->lang_code;
		$get_lang_name = strtolower($get_lang->lang_name);
		?>
		var store_name_<?php echo $get_lang_code; ?>  = $('#store_name_<?php echo $get_lang_name; ?>');
		var store_add_one_<?php echo $get_lang_code; ?>  = $('#store_add_one_<?php echo $get_lang_name; ?>');
		var store_add_two_<?php echo $get_lang_code; ?>  = $('#store_add_two_<?php echo $get_lang_name; ?>');
		var meta_keyword_<?php echo $get_lang_code; ?>  = $('#meta_keyword_<?php echo $get_lang_name; ?>');
		var meta_description_<?php echo $get_lang_code; ?>  = $('#meta_description_<?php echo $get_lang_name; ?>');
		<?php } }?>
        var store_name	   = $('#store_name');
        var store_pho      = $('#store_pho');
        var store_add_one  = $('#store_add_one');
        var store_add_two  = $('#store_add_two');
        var select_country = $('#select_country');
        var select_city    = $('#select_city');
        var zip_code       = $('#zip_code');
        var meta_keyword   = $('#meta_keyword');
        var meta_description = $('#meta_description');
        var location       = $('#pac-input');
        var file		   = $('#file');

        /*store name*/
        if(store_name.val() == ''){
			store_name.css('border', '1px solid red'); 
			$('#store_name_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_NAME'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_NAME')}}');
			store_name.focus();
			return false;
		}else{
            store_name.css('border', ''); 
            $('#store_name_error_msg').html('');
        }
		<?php 
		/* print_r($get_active_lang); */
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_code = $get_lang->lang_code;
		$get_lang_name = $get_lang->lang_name;
		?>
		/*store name in french*/
        if(store_name_<?php echo $get_lang_code; ?>.val() == ''){
			store_name_<?php echo $get_lang_code; ?>.css('border', '1px solid red'); 
			$('#store_name_<?php echo $get_lang_code; ?>_error_msg').html('Please Enter Store Name In <?php echo $get_lang_name; ?>');
			store_name_fr.focus();
			return false;
		}else{
            store_name_<?php echo $get_lang_code; ?>.css('border', ''); 
            $('#store_name_<?php echo $get_lang_code; ?>_error_msg').html('');
        }
		
		<?php } }?>
		

        /*store Phone Number*/
        if(store_pho.val() == ''){
			store_pho.css('border', '1px solid red'); 
			$('#pho_no_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_CONTACT_NUMBER')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_CONTACT_NUMBER') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_CONTACT_NUMBER')}}');
			store_pho.focus();
			return false;
		}else{
            store_pho.css('border', ''); 
            $('#pho_no_error_msg').html('');
        }

        /*store Address 1*/
        if(store_add_one.val() == ''){
			store_add_one.css('border', '1px solid red'); 
			$('#address1_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_ADDRESS') }} {{ $default_lang }}');
			store_add_one.focus();
			return false;
		}else{
            store_add_one.css('border', ''); 
            $('#address1_error_msg').html('');
        }
		<?php 
		/* print_r($get_active_lang); */
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_code = $get_lang->lang_code;
		$get_lang_name = $get_lang->lang_name;
		?>
		/*store Address 1 in french*/
        if(store_add_one_<?php echo $get_lang_code; ?>.val() == ''){
			store_add_one_<?php echo $get_lang_code; ?>.css('border', '1px solid red'); 
			$('#address1_<?php echo $get_lang_code; ?>_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_ADDRESS') }} In {{ $get_lang_name }}');
			store_add_one_<?php echo $get_lang_code; ?>.focus();
			return false;
		}else{
            store_add_one_<?php echo $get_lang_code; ?>.css('border', ''); 
            $('#address1_<?php echo $get_lang_code; ?>_error_msg').html('');
        }
		<?php } }?>

        /*store Address 2*/
        if(store_add_two.val() == ''){
			store_add_two.css('border', '1px solid red'); 
			$('#address2_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS_2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS_2') :  trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_ADDRESS_2') }} {{ $default_lang }}');
			store_add_two.focus();
			return false;
		}else{
            store_add_two.css('border', ''); 
            $('#address2_error_msg').html('');
        }
		<?php 
		/* print_r($get_active_lang); */
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_code = $get_lang->lang_code;
		$get_lang_name = $get_lang->lang_name;
		?>
		/*store Address 1 in french*/
        if(store_add_two_<?php echo $get_lang_code; ?>.val() == ''){
			store_add_two_<?php echo $get_lang_code; ?>.css('border', '1px solid red'); 
			$('#address2_<?php echo $get_lang_code; ?>_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS_2')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_STORE_ADDRESS_2') :  trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_STORE_ADDRESS_2') }} In {{ $get_lang_name }}');
			store_add_two_<?php echo $get_lang_code; ?>.focus();
			return false;
		}else{
            store_add_two_<?php echo $get_lang_code; ?>.css('border', ''); 
            $('#address2_<?php echo $get_lang_code; ?>_error_msg').html('');
        }
		<?php } }?>
		

        /*Country*/
        if(select_country.val() == 0){
			select_country.css('border', '1px solid red'); 
			$('#country_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_COUNTRY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_COUNTRY') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_COUNTRY')}}');
			select_country.focus();
			return false;
		}else{
            select_country.css('border', ''); 
            $('#country_error_msg').html('');
        }

        /*city*/
        if(select_city.val() == 0){
			select_city.css('border', '1px solid red'); 
			$('#city_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CITY'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CITY') }}');
			select_city.focus();
			return false;
		}else{
            select_city.css('border', ''); 
            $('#city_error_msg').html('');
        } 

        /*zip_code*/
        if(zip_code.val() == ''){
			zip_code.css('border', '1px solid red'); 
			$('#zip_code_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_ZIP_CODE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_ZIP_CODE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_ZIP_CODE')  }}');
			zip_code.focus();
			return false;
		}else{
            zip_code.css('border', ''); 
            $('#zip_code_error_msg').html('');
        } 

        /*meta_keyword*/ 
        /*if(meta_keyword.val() == 0){
			meta_keyword.css('border', '1px solid red'); 
			$('#meta_key_error_msg').html('<?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_KEYWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_KEYWORD');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_META_KEYWORD');}*/ ?> <?php /*echo $default_lang;*/ ?>');
			meta_keyword.focus();
			return false;
		}else{
            meta_keyword.css('border', ''); 
            $('#meta_key_error_msg').html('');
        } */
		<?php 
		/* print_r($get_active_lang); */
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_code = $get_lang->lang_code;
		$get_lang_name = $get_lang->lang_name;
		?>
		/*meta_keyword in french*/ 
        /*if(meta_keyword_<?php /*echo $get_lang_code;*/ ?>.val() == 0){
			meta_keyword_<?php //echo $get_lang_code; ?>.css('border', '1px solid red'); 
			$('#meta_key_<?php //echo $get_lang_code; ?>_error_msg').html('<?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_KEYWORD')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_KEYWORD');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_META_KEYWORD');}*/ ?> In <?php /*echo $get_lang_name;*/ ?>');
			meta_keyword_<?php /*echo $get_lang_code;*/ ?>.focus();
			return false;
		}else{
            meta_keyword_<?php //echo $get_lang_code; ?>.css('border', ''); 
            $('#meta_key_<?php //echo $get_lang_code; ?>_error_msg').html('');
        }
		<?php } }  ?>
		

        /*meta_description*/ 
        /*if(meta_description.val() == 0){
			meta_description.css('border', '1px solid red'); 
			$('#meta_desc_error_msg').html('<?php /*if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_DESCRIPTION')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_DESCRIPTION');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_META_DESCRIPTION');}*/ ?> <?php /*echo $default_lang;*/ ?>');
			meta_description.focus();
			return false;
		}else{
            meta_description.css('border', ''); 
            $('#meta_desc_error_msg').html('');
        }*/
		<?php 
		/* print_r($get_active_lang); */
		if(!empty($get_active_lang)) { 
		foreach($get_active_lang as $get_lang) {
		$get_lang_code = $get_lang->lang_code;
		$get_lang_name = $get_lang->lang_name;
		?>
		if(meta_description_<?php echo $get_lang_code; ?>.val() == 0){
			meta_description_<?php echo $get_lang_code; ?>.css('border', '1px solid red'); 
			$('#meta_desc_<?php echo $get_lang_code; ?>_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_DESCRIPTION')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_META_DESCRIPTION')  : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_META_DESCRIPTION') }} In {{ $get_lang_name }}');
			meta_description_{{ $get_lang_code }}.focus();
			return false;
		}else{
            meta_description_<?php echo $get_lang_code; ?>.css('border', ''); 
            $('#meta_desc_<?php echo $get_lang_code; ?>_error_msg').html('');
        }
		<?php } }?>
		/*meta_description in french*/ 
        

        /*latitude*/ 
        if(location.val() == 0){
			location.css('border', '1px solid red'); 
			$('#location_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_YOUR_LOCATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_ENTER_YOUR_LOCATION') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_ENTER_YOUR_LOCATION') }}');
			location.focus();
			return false;
		}else{
            location.css('border', ''); 
            $('#location_error_msg').html('');
        }


    /*Image */ 
	var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      	if(file.val() == ""){
 		    file.focus();
		    file.css('border', '1px solid red'); 
            $('#img_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_IMAGE') : trans($MER_OUR_LANGUAGE.'.MER_PLEASE_CHOOSE_IMAGE') }}');		
		    return false;
		}else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
		    file.focus();
		    file.css('border', '1px solid red'); 	
            $('#img_error_msg').html('{{ (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE')!= '') ?   trans(Session::get('mer_lang_file').'.MER_PLEASE_CHOOSE_VALID_IMAGE'): trans($MER_OUR_LANGUAGE.'.MER_PLEASE_CHOOSE_VALID_IMAGE')}}');		
		    return false;
		}else{
		    file.css('border', ''); 
            $('#img_error_msg').html('');				
		}
	});
});
	</script>
     <script>

     
	
	function select_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_merchant'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_city').html(responseText);					   
				   }
				}		
			});		
	}
	
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		// alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city_merchant'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_mer_city').html(responseText);					   
				   }
				}		
			});	
	}

 

	</script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
    <?php $city_det = DB::table('nm_emailsetting')->first();?>
    <script type="text/javascript">
    var map;

    function initialize() {


 		var myLatlng = new google.maps.LatLng(<?php echo $city_det->es_latitude; ?>,<?php echo $city_det->es_longitude; ?>);
        
        var mapOptions = {

           zoom: 10,
                center: myLatlng,
                disableDefaultUI: true,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
	 		  var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				draggable:true,    
            });	
		google.maps.event.addListener(marker, 'dragend', function(e) {
      check();
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
             $('#latitude').val(lat);
			 $('#longitude').val(lng);
			}); 
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
 
        google.maps.event.addListener(autocomplete, 'place_changed', function () {

 
                var place = autocomplete.getPlace();
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                $('#latitude').val(latitude);
			          $('#longitude').val(longitude);
                 check();
             
	        if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
				var myLatlng = place.geometry.location;	
				var latlng = new google.maps.LatLng(latitude, longitude);
                marker.setPosition(latlng);

			google.maps.event.addListener(marker, 'dragend', function(e) {

   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
             
			 $('#latitude').val(lat);
			 $('#longitude').val(lng);
			});
            } else {
                map.setCenter(place.geometry.location);	
				
                map.setZoom(17);
            }
        });



    }


    google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- END GLOBAL SCRIPTS -->   
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });

  $('#store_name').bind('keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/g,'') ); }
);

   $.ajax( {
            type: 'get',
         data: {'city_id_ajax':'<?php echo Input::old('select_city'); ?>','country_id_ajax':'<?php echo Input::old('select_country'); ?>'},
         url: '<?php echo url('ajax_select_city_edit'); ?>',
         success: function(responseText){  
           if(responseText)
           { 
               
          $('#select_city').html(responseText);             
           }
        }   
      }); 
</script>
</body>
     <!-- END BODY -->
</html>

@include('sitemerchant.includes.header') 
@php $Coshabig_leftmenu =1; @endphp  
<div class="merchant_vendor">    
@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
     <div class="right_panel">
      <div class="inner">
     <header>
@if($autoid=='')
<h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_OFFER') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_OFFER') }} @endif </h5>

@else
<h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_OFFER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_OFFER') }}  
@else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_OFFER') }} @endif </h5>

@endif



            </header>
             @include('sitemerchant.includes.language') 
        <div class="global_area">

        <div class="row">
        <div class="col-lg-12">
          <div class="box">         
 

 <form name="form" id="offer" method="post" action="{{ route('store-coshaoffer') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form_row">
        <div class="form_row_left">    

        <label class="form_label">
        <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_TITLE'); @endphp</span>
        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_TITLE'); @endphp </span>

        </label>
        <div class="info100">
        <div class="english">
        <input type="text" maxlength="150" class="english" name="title" value="{{$categorysave->title or ''}}" id="title"> 
        </div>
        <div class="arabic">                 
        <input class="arabic ar" maxlength="150" id="title_ar"  name="title_ar"  value="{{$categorysave->title_ar or ''}}" required=""  type="text">
        </div>
        </div></div>
        <div class="form_row_right common_field"> 

        <label class="form_label">
        <span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp</span>
        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span>
        </label>
        <div class="info100">
        <input type="text" class="notzero xs_small" maxlength="2" name="discount" onkeypress="return isNumber(event)"  value="{{$categorysave->discount or ''}}" id="discount" required=""> 
        </div>
        </div>
        </div>
        <div class="form_row common_field">
        <div class="form_row_left">   
        <label class="form_label">
        <span class="english">@php echo lang::get('mer_en_lang.MER_START_DATE'); @endphp</span>
        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_START_DATE'); @endphp </span>
        </label>
        <div class="info100">
        <input type="text" name="Date_from" value="{{$categorysave->start_date or ''}}"  id="datepicker"  class="cal-t"> 
        </div>
        </div> 
        <div class="form_row_right">  
        <label class="form_label">
        <span class="english">@php echo lang::get('mer_en_lang.MER_END_DATE'); @endphp</span>
        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_END_DATE'); @endphp </span>

        </label>
        <div class="info100">
        <input type="text" name="date_to" value="{{$categorysave->end_date or ''}}" id="date_to" required=""   class="cal-t"> 
        <span for="date_to" generated="true" class="error" id="todata"> </span>
    
         
        </div>
         </div>
        </div> 
        <div class="form_row common_field">
        <div class="form_row_left">    

        <label class="form_label">
        <span class="english">@php echo lang::get('mer_en_lang.COUPON'); @endphp</span>
        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.COUPON'); @endphp </span>

        </label>
        <div class="info100">                      
        <input type="text" class="xs_small" maxlength="50" name="coupon" value="{{$categorysave->coupon or ''}}" id="coupon"> 

        </div></div>

        </div> 
        <div class="form_row">
        <div class="form_row_left">
        <div class="english">
              <input type="hidden" name="parentid" value="{{$id}}"> 
        <input type="hidden" name="sid" value="{{$sid}}"> 
        <input type="hidden" name="autoid" value="{{$autoid}}"> 
        
        <input type="submit" name="submit" value="Submit">
        </div>

        <div class="arabic ar">
        <input type="submit" name="submit" value="خضع">
        </div>
        </div></div>
        </form>
           
 
 
 
        <!--PAGE CONTENT PART WILL COME INSIDE IT END-->

          </div>
        </div>
        </div>
    </div>
      </div>
    </div>
    </div>
   

<script>
 $(function() {
$( "#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#date_to").change(function () {
    var startDate = document.getElementById("datepicker").value;
    var endDate = document.getElementById("date_to").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("date_to").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");
        @else
        $('#todata').html("End date should be greater than Start date");
        @endif
        $('#to').show();
    }
    else
    {
        $('#to').hide();

    }
});
 });

</script>
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">

$("#offer").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       discount: {
                       required: true,
                      },
                      Date_from: {
                       required: true,
                      },
                    date_to: {
                       required: true,
                      },
                      coupon: {
                       required: true,
                      },
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
               required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_OFFER_TITLE'); @endphp",
                      },  
                       
            coupon: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_COUPON')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_COUPON') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_COUPON') }} @endif",
                      }, 
                title_ar: {
               required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_OFFER_TITLE_AR'); @endphp",
                      },
                 discount: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_DISCOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_DISCOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_DISCOUNT') }} @endif",
                      }, 

                   Date_from: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_START_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_START_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_START_DATE') }} @endif",
                      }, 

                    date_to: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_END_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_END_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_END_DATE') }} @endif",
                      }, 
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


      @if($mer_selected_lang_code !='en')
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.discount != "undefined" || valdata.discount != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                    if (typeof valdata.Date_from != "undefined" || valdata.Date_from != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                    if (typeof valdata.date_to != "undefined" || valdata.date_to != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                    
                    if (typeof valdata.coupon != "undefined" || valdata.coupon != null) 
                    {
                        $('.arabic_tab').trigger('click'); 
                    }
                    


                     if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
      @else
                    if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {

                    $('.arabic_tab').trigger('click');     

                    }
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.discount != "undefined" || valdata.discount != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.Date_from != "undefined" || valdata.Date_from != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.date_to != "undefined" || valdata.date_to != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.coupon != "undefined" || valdata.coupon != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    

      @endif
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script> 

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer')
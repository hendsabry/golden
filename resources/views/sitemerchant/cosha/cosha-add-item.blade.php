@include('sitemerchant.includes.header') 
@php $Coshabig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
         @if(request()->autoid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.add_item')!= '') {{  trans(Session::get('mer_lang_file').'.add_item') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.add_item') }} @endif </h5>
          @else
          <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATE_ITEM')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_ITEM') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_ITEM') }} @endif </h5>

          

          @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->
            
                <form name="form1" id="addmanager" method="post" action="{{ route('store-coshaitem') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                 <!-- form_row -->
                    <div class="form_row common_field"> 
              <div class="form_row_left">
                    <label class="form_label">
                      <span class="english">  @php echo lang::get('mer_en_lang.SELECT_YOUR_CHOICE'); @endphp</span>
                     <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SELECT_YOUR_CHOICE'); @endphp </span>
                    </label>
              <div class="info100">
              <select class="small-sel" name="category" onchange="chceckCategory()" id="category">
                 <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>
                        @php $menu_name='Design Your Cosha'@endphp
                       @if($mer_selected_lang_code !='en')
                       @php $menu_name= 'تصميم كوشا الخاص بك'; @endphp
                        @endif

                         @php $menu_name_readyar='Ready Made Cosha'@endphp
                       @if($mer_selected_lang_code !='en')
                       @php $menu_name_readyar= 'جاهز الصنع كوشا' @endphp
                        @endif
                        
                  <option value="1" {{ isset($fetchfirstdata->parent_attribute_id) && $fetchfirstdata->parent_attribute_id =='1' ? 'selected' : '' }}>{{ $menu_name }}</option>
                  <option value="2" {{ isset($fetchfirstdata->parent_attribute_id) && $fetchfirstdata->parent_attribute_id =='2' ? 'selected' : '' }}>{{ $menu_name_readyar }}</option>
                
              </select>
              </div>
              </div>
              </div>

                 <div class="form_row displaycat common_field">
              <div class="form_row_left">
                    <label class="form_label">
                      <span class="english">@php echo lang::get('mer_en_lang.SELECT_CATEGORY'); @endphp</span>
                    </label>
              
              <div class="info100">
              <select class="small-sel common_field" name="category1" id="category1">
                  <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>
                    @php $menu_name='attribute_title'@endphp
                   @if($mer_selected_lang_code !='en')
                   @php $menu_name= 'attribute_title_'.$mer_selected_lang_code; @endphp
                    @endif
                     @foreach($category as $val)
                  <option value="{{ $val->id }}" {{ isset($fetchfirstdata->attribute_id) && $fetchfirstdata->attribute_id ==$val->id ? 'selected' : ''}}>{{ $val->$menu_name }}</option>
                  @endforeach  
              </select>
              </div>
              </div>
              </div>
                  
                  <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.mer_category_name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_category_name'); @endphp </span> </label>
                  <div class="info100">
                    <div class="english">
                      <input type="text" name="name" id="name" value="{{ $fetchfirstdata->pro_title or '' }}"  class="english"  data-validation-length="max80" maxlength="140">
                    </div>
                    <div class="arabic ar">
                      <input id="ssb_name_ar" class="arabic ar" name="name_ar" id="name_ar" type="text" value="{{ $fetchfirstdata->pro_title_ar or '' }}"   maxlength="140" >
                    </div>
                  </div>
                  </div>
   <div class="form_row">
                  <div class="form_row_left common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.QTYSTOCK'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.QTYSTOCK'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="{{ $fetchfirstdata->pro_qty or '' }}"  required="" >
                      </div>
                    </div>
                  </div>
           
                    <!-- form_row --> 
                  </div>

                  
				   <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                      @if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!='')
                      <div class="form-upload-img">
                       <img src="{{ $fetchfirstdata->pro_Img or '' }}" >
                     </div>
                     @endif
                    </div>
					</div>
					 
					
   
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value=""  data-min-width="800" data-min-height="600" data-max-width="8000" data-max-height="6000" >
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
  <!-- Add More product images end -->







                  </div>
                  <!-- form_row -->
                  <div class="form_row">
				   <div class="form_row_left common_field">
                    <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englisch">
                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="price" id="price" maxlength="15" value="{{ $fetchfirstdata->pro_price or '' }}"  required="" >
                      </div>
                    </div>
					</div>
					  <div class="form_row_right common_field"> 

        <label class="form_label">
        <span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp</span>
        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span>
        </label>
        <div class="info100">
        <input type="text" class="notzero xs_small" maxlength="2" name="discount"  onkeypress="return isNumber(event)"  value="{{$fetchfirstdata->pro_discount_percentage or ''}}" id="discount">

          <input type="hidden" name="sid" value="{{ $sid }}"> 
           <input type="hidden" name="parentid" value="{{ $id }}">
           <input type="hidden" name="autoid" id="autoid" value="{{ $autoid or '' }}">
        </div>
        </div>
                    <!-- form_row --> 
                  </div>
                  <!-- form_row -->
				   <div class="form_row_right common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Insuranceamount'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Insuranceamount'); @endphp </span> </label>
                    <div class="ensglish">
                      <input type="text" class="small-sel" required="" name="Insuranceamount"  maxlength="15"   data-validation="length required" 
                    data-validation-error-msg="يرجى إدخال اسم القاعة" value="{{$fetchfirstdata->Insuranceamount or ''}}" data-validation-length="max35">
                    </div>
                    @if($errors->has('Insuranceamount')) <span class="error"> {{ $errors->first('Insuranceamount') }} </span> @endif 
                    </div>


				  
				  <div class="form_row">
				  <div class="form_row_left">
                    <label class="form_label"> 
                    	<span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> 
                     </label>
                    <div class="info100">
                      
                      <div class="english">
                        <textarea name="description" class="english" maxlength="500" id="description" rows="4" cols="50" onkeyup="limitTextCount('description', 'divcount', 500,'@php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp');" onkeydown="limitTextCount('description', 'divcount', 500,'@php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp');">{{ $fetchfirstdata->pro_desc or ''  }} </textarea> 
                      </div>      
                      
                      <div  class="arabic ar" >     
                              <textarea class="arabic ar" maxlength="500" name="description_ar" id="description_ar" rows="4" cols="50" onkeyup="limitTextCount('description_ar', 'divcount_ar', 500,'@php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); @endphp');" onkeydown="limitTextCount('description_ar', 'divcount_ar', 500,'@php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); @endphp');">{{ $fetchfirstdata->pro_desc_ar or ''  }}</textarea>  
                      </div> 
                      <div id="divcount" class="english">500 @php echo lang::get('mer_en_lang.MER_DESCRIPTION_TXT'); @endphp</div>
                      <div id="divcount_ar" class="arabic ar">500 @php echo lang::get('mer_ar_lang.MER_DESCRIPTION_TXT'); @endphp</div>
                    </div>
                    <!-- form_row --> 
					</div>
					
                  </div>
                  
                  

                  <div class="form_row">
                  <div class="form_row_left">
                  <div class="english">
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <!-- form_row -->
                  <div class="arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  <!-- form_row -->
                  </div></div>
                </form>
          
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
  
$("#addmanager").validate({


              ignore: [],
               rules: {
                 
                  category: {
                       required: true,
                      },
                   
                    

                    name: {
                       required: true,
                      },

                       name_ar: {
                       required: true,
                      },

                      price: {
                       required: true,
                      },

                       

                     description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

                       @if(isset($fetchfirstdata->pro_Img)!='')  
                          stor_img: {
                           required: false,
                           accept:"png|jpe?g|gif",
                      },
                      @else
                       stor_img: {
                           required: true,
                           accept:"png|jpe?g|gif",
                      },
                       
                         
                      @endif

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
                
           messages: {
             
              category: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",
                      }, 

               category1: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",
                      },         



             name: {
               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_NAME'); @endphp",
                      },  

                 name_ar: {
               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_NAME_AR'); @endphp",
                      },

                       price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                      },

                 


                   description: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT_SINGER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT_SINGER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ABOUT_SINGER') }} @endif ",
                      },

                       description_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT_SINGER_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT_SINGER_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ABOUT_SINGER_AR') }} @endif ",
                      },
        
                 
                  stor_img: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",

                 }, 
                       
                    
                },             
                   invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                     console.log("invalidHandler : validation", valdata);

 
                    @if($mer_selected_lang_code !='en')
                    if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                   
                    if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                     if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                   
                    if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.english_tab').trigger('click');
                    }

                    if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');
                    }
                    if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                    $('.arabic_tab').trigger('click');     


                    }
                      
@else
                     
                   if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }
                  if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     
                      
                     
                     if (typeof valdata.category != "undefined" || valdata.category != null) 
                    {

                        $('.english_tab').trigger('click');
                        

                    }
                     
                     
  if (typeof valdata.description != "undefined" || valdata.description != null) 
                    {
                    $('.english_tab').trigger('click');
                    }
                   
                    if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                    $('.english_tab').trigger('click');
                    }


                    
@endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });
  $("#company_logo1").change(function () {
        var fileExtension = ['pdf'];
        $(".certifications").html('');
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
   $("#file_value2").html('');         
$(".certifications").html('Only PDF format allowed.');

         }
    });

  $(document).ready(function(){
    var autoid = $('#autoid').val();
    var catid = $('#category').val();
     if(catid=='2' || autoid=='') {
    $('.displaycat').hide();
    
     } else {
        $('.displaycat').show();
     }
    
    });
  function chceckCategory()
  {
     var catid =  $('#category').val()
     

     if(catid==1) {
          $('.displaycat').show();

          $('#category1').attr('required','true');
     } else {

            $('.displaycat').hide();
            $('#category1').removeAttr('required');
     }


  }




/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

   /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
</script> 


<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="{{url('/')}}/themes/js/imageupload/jquery.checkImageSize.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'  data-min-width="800" data-min-height="600" data-max-width="8000" data-max-height="6000" ><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->

@include('sitemerchant.includes.footer')


<script type="text/javascript">
$("input[type=file]").checkImageSize({
  minWidth: 860,
  minHeight: 518,
  maxWidth: 860,
  maxHeight: 518,
  showError: true, 
  ignoreError: false
});

</script>



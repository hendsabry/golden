@include('sitemerchant.includes.header') 
@php $Coshabig_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')
<div class="right_panel">
<div class="inner">
    <header>
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.ADDPACKAGE') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.ADDPACKAGE') }} @endif </h5>
      @include('sitemerchant.includes.language') </header>
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <!-- Display Message after submition -->
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}  </div>
            @endif
            <!-- Display Message after submition -->
            <div class="one-call-form">
              <form name="form1" id="add-container" method="post" action="" enctype="multipart/form-data">
               
                <div class="form_row">
                  <label class="form_label">       <span class="english">@php echo lang::get('mer_en_lang.PACKEGENAME'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKEGENAME'); @endphp </span></label>
                  <div class="info100">
                  <div class="english">
                    <input class="english" maxlength="150" type="text" name="title" value="{!! Input::old('title') !!}" id="title" required="">
                     </div>
                     <div class="arabic">
                    <input class="arabic ar" maxlength="150" id="title_ar"  name="title_ar" value=""  type="text" >
                    </div>

                     </div>  
                </div>
               
                
              <div class="form_row">
                  <label class="form_label">
                    <span class="english">@php echo lang::get('mer_en_lang.PACKAGEIMAGE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKAGEIMAGE'); @endphp </span>
          </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn">
 <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
 <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>
 </div>
                      </div>
                      </label>
                      <input id="company_logo" name="img" class="info-file" type="file" required="" value="">
                    </div>
                  </div>
                </div>
                
				 
				 <div class="form_row ">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> 
</label>
                  <div class="info100">
                   <div>
                    <input class="small-sel" maxlength="100" type="text" name="no_people" value="" id="no_people" required="">
                    </div>
                  </div>
                </div>
				
				<div class="form_row"> 

                         <label class="form_label">
                          <span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span>
 </label>
              <div class="info100">
                       <input type="text" class="small-sel" maxlength="100" name="discount" value="{!! Input::old('discount') !!}" id="discount" required=""> 
             </div>
             
             </div>
				
				<div class="form_row ">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.MER_Duration'); @endphp <span class="msg_img_replace">(@php echo lang::get('mer_en_lang.HRS'); @endphp)</span></span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Duration'); @endphp <span class="msg_img_replace">(@php echo lang::get('mer_ar_lang.HRS'); @endphp)</span> </span> 
</label>
                  <div class="info100">
                   <div>
                    <input class="small-sel" maxlength="100" type="text" name="no_people" value="" id="no_people" required="">
                    </div>
                  </div>
                </div>
				
				<div class="form_row ">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.SELECTSERVICE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SELECTSERVICE'); @endphp </span> 
</label>
                  <div class="info100">
				  <div class="cat_box">
                 <div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english cat_face">Face </span> <span class="arabic ar cat_face">Face  </span> </label>
                    </div>
					<div class="category_area">
					<div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Face 1 <span class="sar">(SAR 20)</span>  </span> <span class="arabic ar">Face 1<span class="sar">(SAR 20)</span>  </span> </label>
                    </div><div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Face 2<span class="sar">(SAR 20)</span>  </span> <span class="arabic ar">Face 2<span class="sar">(SAR 20)</span>    </span> </label>
                    </div><div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Face 3 <span class="sar">(SAR 20)</span> </span> <span class="arabic ar">Face 3 <span class="sar">(SAR 20)</span>   </span> </label>
                    </div>
					
					</div>
					</div>
					<div class="cat_box">
                 <div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english cat_face">Face </span> <span class="arabic ar cat_face">Face  </span> </label>
                    </div>
					<div class="category_area">
					<div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Face 1 <span class="sar">(SAR 20)</span>  </span> <span class="arabic ar">Face 1<span class="sar">(SAR 20)</span>  </span> </label>
                    </div><div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Face 2 <span class="sar">(SAR 20)</span> </span> <span class="arabic ar">Face 2 <span class="sar">(SAR 20)</span>   </span> </label>
                    </div><div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Face 3<span class="sar">(SAR 20)</span>  </span> <span class="arabic ar">Face 3<span class="sar">(SAR 20)</span>    </span> </label>
                    </div>
					
					</div>
					</div>
					<div class="cat_box">
                 <div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english cat_face">Nails  </span> <span class="arabic ar cat_face">Nails  </span> </label>
                    </div>
					<div class="category_area">
					<div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Nails 1 <span class="sar">(SAR 20)</span>  </span> <span class="arabic ar">Nails 1 <span class="sar">(SAR 20)</span>  </span> </label>
                    </div><div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Nails 2 <span class="sar">(SAR 20)</span>  </span> <span class="arabic ar">Nails 2 <span class="sar">(SAR 20)</span>    </span> </label>
                    </div><div class="save-card-line">
                      <input type="checkbox" data-validation-qty="min1" data-validation="checkbox_group" name="halltype[]" value="men" id="men">
                      <label for="men"> <span class="english">Nails 3 <span class="sar">(SAR 20)</span>  </span> <span class="arabic ar">Nails 3 <span class="sar">(SAR 20)</span>    </span> </label>
                    </div>
					
					</div>
					</div>
                  </div>
                </div>
				
				
                <!-- form_row -->
                
                <!-- form_row -->
                <div class="form_row english">
                  <input type="submit" name="submit" value="Submit">
                </div>

                    <div class="form_row arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                <!-- form_row -->
              </form>
            </div>
            <!-- one-call-form -->
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       price: {
                       required: true,
                      },
                      no_people: {
                       required: true,
                      },
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       img: {
                       required: true,
                       accept:"png|jpe?g|gif",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CONTAINER_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CONTAINER_NAME') }} @endif",
                      },  

                title_ar: {
               required:  " {{ trans('mer_ar_lang.MER_VALIDATION_CONTAINER_NAME_AR') }} ",
                      },
                   price: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif",
                      },
                       no_people: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif",
                      }, 

                    about: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_ABOUT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_ABOUT') }} @endif",
                      }, 

                    about_ar: {
               required:  " {{ trans('mer_ar_lang.MER_VALIDATION_ABOUT_AR') }} ",
                      },
                     img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    else if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                     else if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
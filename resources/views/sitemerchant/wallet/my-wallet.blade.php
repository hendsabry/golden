@include('sitemerchant.includes.header') 
@php $wallet_inner_leftmenu =1; 
 
@endphp
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MyWallet')!= '') {{  trans(Session::get('mer_lang_file').'.MyWallet') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MyWallet') }} @endif</h5>


        <div class="add"><a href="{{ route('wallet-addamount')}}">@if (Lang::has(Session::get('mer_lang_file').'.MER_MyWallet')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MyWallet') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MyWallet') }} @endif</a></div>



      </div>
  
 

      {!! Form::open(array('url'=>"my-wallet",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!} 

        <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>
        <div class="filter_area">
        <div class="order-filter-line">
        <div class="filter_left studio-amount">
          <input name="amount" maxlength="6"  placeholder="@if (Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{trans(Session::get('mer_lang_file').'.Amount') }} @else  {{trans($MER_OUR_LANGUAGE.'.Amount') }} @endif" onkeypress="return isNumber(event)"  type="text" value="{{request()->amount}}" /> 
      
        </div>
        <div class="of-date-box studio-date-box">
           <input name="dateFrom"  class="cal-t"  placeholder="@if (Lang::has(Session::get('mer_lang_file').'.DateFrom')!= '') {{trans(Session::get('mer_lang_file').'.DateFrom') }} @else  {{trans($MER_OUR_LANGUAGE.'.DateFrom') }} @endif" id="dateFrom"  type="text"  maxlength="12"   value="{{request()->dateFrom}}" /> 
       
         <input name="dateTo" id="dateTo" type="text" maxlength="12" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.DateTo')!= '') {{trans(Session::get('mer_lang_file').'.DateTo') }} @else  {{trans($MER_OUR_LANGUAGE.'.DateTo') }} @endif"  class="cal-t studio-date" value="{{request()->dateTo}}" />
         <span for="dateTo" generated="true" class="error" id="todata"> </span>

         <input name="" value="@if (Lang::has(Session::get('mer_lang_file').'.Apply')!= '') {{  trans(Session::get('mer_lang_file').'.Apply') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Apply') }} @endif" class="applu_bts" type="submit">
        </div>
        </div>
        </div>
      <!-- filter_area -->   
      {!! Form::close() !!}



      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="table_wrap">
              <div class="panel-body panel panel-default">
                @if($getTotalAmount->count() < 1)
                <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>
                @else
                <div class="table dish-table">
                  <div class="tr">            

                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Amount_Paid')!= '') {{  trans(Session::get('mer_lang_file').'.Amount_Paid') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount_Paid') }} @endif</div>
                   <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Payment_date')!= '') {{  trans(Session::get('mer_lang_file').'.Payment_date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Payment_date') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Mode')!= '') {{  trans(Session::get('mer_lang_file').'.Mode') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Mode') }} @endif</div>
                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Total_Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Total_Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Total_Amount') }} @endif</div>
                    <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.Note')!= '') {{  trans(Session::get('mer_lang_file').'.Note') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Note') }} @endif</div>

                  </div>
                @php $TotalAmount =0; @endphp
                 @foreach($getTotalAmount as $value)
                 @php $Amt = $value->amount; @endphp
                    <div class="tr">
                    <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Amount_Paid')!= '') {{  trans(Session::get('mer_lang_file').'.Amount_Paid') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount_Paid') }} @endif">{{$value->amount or ''}}</div>
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Payment_date')!= '') {{  trans(Session::get('mer_lang_file').'.Payment_date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Payment_date') }} @endif"> {{ Carbon\Carbon::parse($value->created_at)->format('F j, Y')}} </div>
                    
                    <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Mode')!= '') {{  trans(Session::get('mer_lang_file').'.Mode') }}  
                    @else  {{ trans($MER_OUR_LANGUAGE.'.Mode') }} @endif"> <span class="   status_active2 cstatus" data-status=" "data-id=" ">@php $Type = $value->transaction_type; $status = $value->status; @endphp @if($Type==1) @if (Lang::has(Session::get('mer_lang_file').'.Credit')!= '') {{  trans(Session::get('mer_lang_file').'.Credit') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Credit') }} @endif   @if($status==1)
                    <span class="error">@if (Lang::has(Session::get('mer_lang_file').'.Failed')!= '') {{  trans(Session::get('mer_lang_file').'.Failed') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Failed') }} @endif</span> @else <span class="error">@if (Lang::has(Session::get('mer_lang_file').'.Sucess')!= '') {{  trans(Session::get('mer_lang_file').'.Sucess') }}    @php $TotalAmount =$TotalAmount + $Amt; @endphp  @else  {{ trans($MER_OUR_LANGUAGE.'.Sucess') }} @endif  </span>  @endif @else @if (Lang::has(Session::get('mer_lang_file').'.Debit')!= '') {{  trans(Session::get('mer_lang_file').'.Debit') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Debit') }} @endif @php $TotalAmount =$TotalAmount - $Amt; @endphp  @endif</span></div>

                    <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Total_Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Total_Amount') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Total_Amount') }} @endif">
                    {{$TotalAmount  or '0' }}</div>
                    <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Note')!= '') {{  trans(Session::get('mer_lang_file').'.Note') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Note') }} @endif">
                    {{$value->notes  or 'N/A' }}</div>
                    </div>
 
                  @endforeach   
 
                </div>

               @endif
 
              </div>
               {{ $getTotalAmount->links() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 <script>
 $(function() {
$( "#dateFrom" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#dateTo" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

 $(function() {
 $("#dateTo").change(function () {
    var startDate = document.getElementById("dateFrom").value;
    var endDate = document.getElementById("dateTo").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("dateTo").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من التاريخ");
        @else
        $('#todata').html("End date should be greater than From date");
        @endif
      
    }
    else
    {
        $('#todata').hide();

    }
});
 });
   
</script>     
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $wallet_inner_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb')   @include('sitemerchant.includes.left')
<div class="right_panel">
 <div class="inner">
      <header>
    
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_MyWallet')!= '') {{  trans(Session::get('mer_lang_file').'.MER_MyWallet') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_MyWallet') }} @endif</h5>
       
      </header>
     
      
       <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
      <div class="global_area">
	  <div class="row">
        <div class="col-lg-12">
          <div class="box">             

              <form name="form1" id="updatewallet" method="post" action="{{ route('updateWallet') }}">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">
                   @if (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_AMOUNT') }} @endif
                  </label>
                  <div class="info100">              
                    <input type="text"  name="amount" onkeypress="return isNumber(event)"  maxlength="9" id="amount" required="" >
                  </div>
                  </div>
          		</div> <!-- form_row -->
                <div class="form_row"> 
				<div class="form_row_left">
                  <input type="submit" name="submit" value="@if (Lang::has(Session::get('mer_lang_file').'.Pay_Now')!= '') {{  trans(Session::get('mer_lang_file').'.Pay_Now') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Pay_Now') }} @endif"> 
				  </div>
                </div> 
              </form>

          </div>
        </div>
      </div>
	  </div>
    </div>
  </div>
</div>
</div>
<!-- merchant_vendor --> 
  
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#updatewallet").validate({
                  ignore: [],
                  rules: {
                  amount: {
                       required: true,
                      },                       
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             amount: {
         required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_AMOUNT') }} @endif",
                      },  
 
                                                  
                     
                },
                invalidHandler: function(e, validation){
                var valdata=validation.invalid;
                     },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
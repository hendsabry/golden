@include('sitemerchant.includes.header')
@php $reception_hospitality_leftmenu =1; @endphp
<div class="merchant_vendor cont_add">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_WORKERS_NATIONALTY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_WORKERS_NATIONALTY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_WORKERS_NATIONALTY') }} @endif</h5>
      
      </header>
      <style type="text/css">
       .ms-drop ul li label input[type="checkbox"] {
    height: 20px;
    display: inline-block;
    width: 20px;
    position: relative;
    border: 1px solid red;
    opacity: 1;
}


      </style>
      <link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />
      <!-- service_listingrow --> 
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">  

              <div class="box notab">
              
                  <form  id="nationality" name="nationality" method="post" action="{{ route('storenationality') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                     
                    <div class="form_row">
					 <div class="form_row_left">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_NATIONALITY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_NATIONALITY') }} @endif </label>
                    <div class="info100">
                      
                      <select name="workers_nationality[]" multiple="" id="workers_nationality" class="english">
                           @php $co_name='co_name'@endphp
                           @if($mer_selected_lang_code !='en')
                           @php $co_name= 'co_name_'.$mer_selected_lang_code; @endphp
                           @endif  

                           @foreach($countrylist as $val)
                           @php

                           $checked = in_array($val->co_id, $natid) ? 'selected' : '';
                          $getWP = Helper::getworkerprice($val->co_id);
                           @endphp


                             <option value="{{ $val->$co_name }}~{{$getWP}}" {{ $checked }}> {{  $val->$co_name }}</option>
                           @endforeach

                      </select>
                      @if($errors->has('workers_nationality'))
                       <span class="help-block text-red error">
                       {{ $errors->first('workers_nationality') }}
                       </span>
                   @endif
                      <input type="hidden" name="shopid" value="{{ request()->shopid }}">
                    </div>
					</div>
                    </div>

                  <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.WorkerPrice')!= '') {{  trans(Session::get('mer_lang_file').'.WorkerPrice') }}  
                  @else  {{ trans($MER_OUR_LANGUAGE.'.WorkerPrice') }} @endif </label>
                  <div class="info100">

             <!--  <input type="text" name="worker_price" maxlength="6" required="" value="{{$getWP->value or ''}}" class="xs_small notzero"> -->

 <div id="result" > @php $i=1; @endphp @foreach($countrydata as $vals) 

@if($mer_selected_lang_code =='en')
 <div class="result-prise-line" >Price for {{$vals->country_name or ''}}<br/>
  @else
 <div class="result-prise-line" >السعر ل  {{$vals->country_name_ar or ''}}<br/>
  @endif
<input type="text" name="worker_price{{$i}}" maxlength="6" required="" value="{{$vals->worker_price or ''}}" class="xs_small notzero">  @php $i=$i + 1; @endphp  
 </div>
@endforeach
 </div>





                  </div>
                  </div>
                  </div>
                                    
                    <div class="form_row">
					 <div class="form_row_left">
                      <input type="submit" name="submit" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_SUBMIT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SUBMIT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SUBMIT') }} @endif">
                   </div>
				    </div>
                    
                  </form>
             
              </div>

          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor -->

<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_popup_title">@if (Lang::has(Session::get('mer_lang_file').'.MER_Action')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Action') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Action') }} @endif</div>
    <div class="action_content"></div>
    <div class="action_btnrow"> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> <a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");
 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
  

 if(status=='Active') {
    jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_De_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_De_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_De_Activate_Record') }} @endif')
 } else {

     jQuery('.action_content').html('@if (Lang::has(Session::get('mer_lang_file').'.MER_Activate_Record')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Activate_Record') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Activate_Record') }} @endif')
 }

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

     jQuery.ajax({
        type: "GET",
        url: "{{ route('change-status') }}",
        data: {activestatus:activestatus,id:id,from:'hotellist'},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script> 
<script type="text/javascript">
      
          $(document).ready(function(){
            $('form[name=test]').submit();
             $('#submitdata').click(function(){            
             $('form[name=filter]').submit();
          });
          });
    

    </script> 
     
 <script type="text/javascript">
  
$("#nationality").validate({
                  ignore: [],
                  rules: {
                  worker_price: {
                       required: true,
                      },                        

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
           

                 worker_price: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_WORKER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_WORKER_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_WORKER_PRICE') }} @endif",
                      },  
               
                },

                invalidHandler: function(e, validation){                 
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            }); 

</script> 
  
    <script src="{{ url('')}}/themes/js/jquery.min.js"></script>
<script src="{{ url('')}}/themes/js/multiple-select.js"></script>
    @if($mer_selected_lang_code !='en')
<script>
    $(function() {
        $('#workers_nationality,#ms_2,#ms_3,#ms_4,#ms_5,#ms_6,#ms_7,#ms_8,#ms_9,#ms_10').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "تحديد"
        });

          

    });
</script>
@else

<script>
    $(function() {
        $('#workers_nationality').change(function() {
           // console.log($(this).val());
        }).multipleSelect({
            width: '100%',
            placeholder: "Select"
        });

          

    });
</script>

@endif
 
 
<script type="text/javascript">
  


$(function() {
    $("#workers_nationality").change(function() {
        var multipleValues = $("#workers_nationality").val() || "";
    

        var result = "";
        if (multipleValues != "") {
            var aVal = multipleValues.toString().split(",");

            $.each(aVal, function(i, value) {

              var addd = value.split("~");
 
                
                @if($mer_selected_lang_code =='en')   
                 result += "<div class='clear'>Price for "+addd[0];  
                result += "<input required placeholder='Enter price for "+addd[0]+"' value='"+addd[1]+"' class='notzero' maxlength='6' type='text' name='worker_price" + (parseInt(i) + 1) + "' >";    
                @else
                 result += "<div class='clear'>السعر ل  "+addd[0]; 
            result += "<input required placeholder='أدخل السعر ل  "+addd[0]+"' value='"+addd[1]+"' class='notzero' maxlength='6' type='text' name='worker_price" + (parseInt(i) + 1) + "' >";  
                @endif        
                result += "</div>";
            });


        }
        //Set Result
        $("#result").html(result);

    });
});


</script>

 
@include('sitemerchant.includes.footer') 
@include('sitemerchant.includes.header') 
@php $Travel_leftmenu =1; @endphp
<link rel="stylesheet" href="{{ url('')}}/public/assets/themes/js/timepicker/jquery.datetimepicker.css" />
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header> @if($autoid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_PACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_PACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_PACKAGE') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_PACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_PACKAGE') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_PACKAGE') }} @endif </h5>
        @endif
        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box arabic-ltr">
              <!-- Display Message after submition -->
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif
              <!-- Display Message after submition -->
              <form name="makeupartist" id="makeupartist" method="post" action="{{ route('store-travel-service') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PACKEGENAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKEGENAME'); @endphp</span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="model_name" maxlength="60" value="{{ $fetchdata->pro_title or '' }}" id="service_name"  >
                      </div>
                      <div class="arabic ar ">
                        <input type="text" class="arabic ar" name="model_name_ar" maxlength="60" value="{{ $fetchdata->pro_title_ar or '' }}" id="model_name_ar" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_PRICE'); @endphp </span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_PRICE'); @endphp (%) </span> </label>
                    <div class="info100">
                      <div>
                        <input type="text" class="xs_small notzero" name="price"  onkeypress="return isNumber(event)"  maxlength="12" value="{{ $fetchdata->pro_price or '' }}" id="price" >
                      </div>
                    </div>
                  </div>

                   <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_DISCOUNT'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_DISCOUNT'); @endphp  %</span> </label>
                    <div class="info100">
                      <div class="englissh">
                        <input type="text" class="xs_small notzero" name="discount" onkeypress="return isNumber(event)" maxlength="2" value="{{$fetchdata->pro_discount_percentage or ''}}" id="discount" >
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">
                      </div>
                      @if(isset($fetchdata->pro_Img)!='')
                      <div class="form-upload-img"> <img src="{{ $fetchdata->pro_Img or '' }}" width="150" height="150"> </div>
                      @endif </div>
                  </div>

      
<!-- Add More product images start -->
        @php
        $GalleryCunt = $productGallery->count();
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)
        <div class="form_row_right common_field">
        <div class="info100">
        <div class="input-file-area">
        <label for="company_logo{{$k}}">
        <div class="file-btn-area">
        <div id="file_value{{$J}}" class="file-value"></div>
        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
        </div>
        </label>
        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">
        </div>
        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')
        <div class="form-upload-img product_img_del">
        <img src="{{ $productGallery[$i]->image or '' }}" >
        <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>

        </div>
        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">
        </div>

        </div>
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        <div id="img_upload_pro" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button_pro"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>
        <span class="error pictureformat"></span>
        @php $Countpro =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Countpro = $GalleryCunt+1;} @endphp                
        <input type="hidden" id="Countpro" name="Countpro" value="{{$Countpro}}">
        </div>
  <!-- Add More product images end -->

                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Location'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Location'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class=" " name="location" maxlength="90" value="{{ $Location->value or '' }}" id="location" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class=" " name="location_ar" maxlength="90" value="{{ $Location->value_ar or '' }}" id="location_ar" required="" >
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  @php
                  $PDC = 1;
                  if(isset($PackageDate->value) && $PackageDate->value!='')
                  {
                  $getPD = explode(',', $PackageDate->value);
                  $PDC = count($getPD);
                  @endphp
                  <div class="form_row_left common_field"> @for($i=0; $i< $PDC;$i++)
                    @php $getPDs = $getPD[$i]; if($getPDs==''){ continue;} @endphp
                    <div class="add_row_wrap_new posrel" >
                      <div id="remove_button" class="remove_btn"><a href="javascript:void(0);" title="Remove field" class="status_active  status_active2 cstatus" data-status="Active" data-id="{{ $getPDs or '' }}" data-proid="{{ request()->autoid }}" style="float:left;">&nbsp;</a></div>
                      <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PackageSchedule'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PackageSchedule'); @endphp </span> </label>
                      <div class="info100">
                        <div>
                          <input type="text" class="datetimepicker" name="package_schedule[]" maxlength="20" value="{{ $getPDs or '' }}" id="datetimepicker" required="" >
                        </div>
                      </div>
                      <!-- info100 -->
                    </div>
                    @endfor
                    <!-- form_row -->
                  </div>
                  @php } else { @endphp
                  <div class="form_row_left common_field posrel">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PackageSchedule'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PackageSchedule'); @endphp </span> </label>
                    <div class="info100">
                      <input type="text" class="datetimepicker" name="package_schedule[]" maxlength="20" value="{{ $getPDs or '' }}" id="datetimepicker" required="" >
                    </div>
                    <!-- info100 -->
                  </div>
                  @php } @endphp
                  <div id="img_upload" class="common_field"></div>
                  <div class="form_row_left common_field">
                    <div id="add_button" class=""><a href="javascript:void(0);" class="form-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_SERVICE_ADDMORE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SERVICE_ADDMORE'); @endphp </span> </a></div>
                  </div>
                  @php $Count = 1;@endphp
                  <input type="hidden" id="count" name="count" value="{{$Count}}">
                  <!-- form_row -->
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.PackageDuration'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PackageDuration'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class=" " name="package_duration" maxlength="40" value="{{ $PackageDuration->value or '' }}" id="package_duration" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class=" " name="package_duration_ar" maxlength="40" value="{{ $PackageDuration->value_ar or '' }}" id="package_duration_ar" required="" >
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.ExtraService'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.ExtraService'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea  class=" " name="extra_service" maxlength="255" id="extra_service" rows="4" cols="50" required="" >{{ $ExtraService->value or '' }}</textarea>
                      </div>
                      <div  class="arabic ar">

                        <textarea  class=" " name="extra_service_ar" maxlength="255" id="extra_service" rows="4" cols="50" required="" >{{ $ExtraService->value_ar or '' }}</textarea>
                      
                      </div>
                    </div>
                  </div>
                  <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                    <div class="info100">
                      <div class="english">
                        <textarea name="description" class="english" maxlength="500" id="description" rows="4" cols="50">{{ $fetchdata->pro_desc or '' }} </textarea>
                      </div>
                      <div  class="arabic ar" >
                        <textarea class="arabic ar" name="description_ar"  maxlength="500" id="description_ar" rows="4" cols="50">{{ $fetchdata->pro_desc_ar or '' }}</textarea>
                      </div>
                    </div>
                    <!-- form_row -->
                  </div>
                  <div class="form_row_left">
                    <div class="english">
                      <input type="hidden" name="id" value="{{request()->id}}">
                      <input type="hidden" name="sid" value="{{request()->sid}}">
                      <input type="hidden" name="autoid" value="{{request()->autoid}}">
                      <input type="submit" name="submit" value="Submit">
                    </div>
                    <!-- form_row -->
                    <div class="arabic ar arabic_left">
                      <input type="submit" name="submit" value="خضع">
                    </div>
                    <!-- form_row -->
                  </div>
                </div>
              </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
              <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
            </div>
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
<!-- merchant_vendor -->
<script>  
  $(document).ready(function(){
  var maxField = 5;  
  var addButton = $('#add_button');  
  var wrapper = $('#img_upload');
  var x = @php echo $Count; @endphp; 

  $(addButton).click(function(){  
  if(x < maxField){  
  x++; 
  var main = x;

  var testElement= document.getElementById('english_tab');
  var result = testElement.classList.contains('active')

  var enclass=''
  var arclass='f'


  var fieldHTML = '<div class="add_row_wrap_new main'+x+' "><div id="remove_button" class="remove_btn" onclick="javascript: removemain('+main+')"><a href="javascript:void(0);"  title="Remove field" > &nbsp; </a></div><div class="form_row"><label class="form_label"><span class=" '+enclass+' ">@if (Lang::has(Session::get('mer_lang_file').'.PackageSchedule')!= '') {{  trans(Session::get('mer_lang_file').'.PackageSchedule') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.PackageSchedule') }} @endif </span></label><div class="info100"><div class="has-success"><div class="p '+enclass+' "><input type="text" class="datetimepicker" name="package_schedule[]" maxlength="20"> </div></div></div></div>'; 
  $(wrapper).append(fieldHTML);  
  createValidation();
  document.getElementById('count').value = parseInt(x);
  }
  });
   
  });
  
  function removemain(val)
  {
       
      var x = document.getElementById('count').value;
      $('.main'+val).remove();
      x--;  
      document.getElementById('count').value = parseInt(x);

  }

       
 $("form").data("validator").settings.ignore = "";
 </script>
<script>
 $(function() {
 
$( "#datepicker" ).datetimepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});
 
</script>
<script>

$("form").data("validator").settings.ignore = "";
</script>
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a> </div>
  </div>
</div>
<!-- action_popup -->
<script>
/* Action Popup */
jQuery('.cstatus').click(function(){
 var id =jQuery(this).data("id");
  var proid =jQuery(this).data("proid");
 jQuery('.action_yes').attr('data-id',id);
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif
 
jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
 
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 

 
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-attribute') }}",
        data: {times:id,proids:proid},
        success: function(data) {
         
            if(data==1){
              location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});

jQuery('.action_no').click(function(){
jQuery('.action_popup').fadeOut(500); 
jQuery('.overlay_popup').fadeOut(500);
});
</script>
<script>
$("form").data("validator").settings.ignore = "";
</script>
<script type="text/javascript">  
$("#makeupartist").validate({

                  ignore: [],
                  rules: {
                
                       model_name: {
                       required: true,
                      },

                      model_name_ar: {
                       required: true,
                      },
 
                       price: {
                       required: true,
                      },

                      location: {
                       required: true,
                      },
                      "package_schedule[]": {
                       required: true,
                      },
                      package_duration: {
                       required: true,
                      },
                      extra_service: {
                       required: true,
                      },

                       description: {
                       required: true,
                      },
                       description_ar: {
                       required: true,
                      },

 
                       @if(isset($fetchdata->pro_Img)!='')  
                        stor_img: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        stor_img: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
                model_name: {
                required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_PACKAGE_NAME'); @endphp",
                },  
                model_name_ar: {
                required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME'); @endphp",
                },  

                location: {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_LOCATION')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_LOCATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_LOCATION') }} @endif",
                }, 

                "package_schedule[]": {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_SCHEDULE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_SCHEDULE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PACKAGE_SCHEDULE') }} @endif",
                },   

                price: {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",
                },


                package_duration: {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_DURATION')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PACKAGE_DURATION') }} @endif ",
                },


                extra_service: {
                required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_EXTRA_SERVICES')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_EXTRA_SERVICES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_EXTRA_SERVICES') }} @endif ",
                },
                description: {          
                required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",
                },

                description_ar: {          
                required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_SINGER_AR'); @endphp",
                },

                stor_img: {
                accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                },   

                                                  
                     
                },

                invalidHandler: function(e, validation){
       
                    var valdata=validation.invalid;

       @if($mer_selected_lang_code !='en')
              if (typeof valdata.model_name != "undefined" || valdata.model_name != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.location != "undefined" || valdata.location != null) 
              {
              $('.english_tab').trigger('click');
              }
              if (typeof valdata.package_schedule != "undefined" || valdata.package_schedule != null) 
              {
              $('.english_tab').trigger('click');
              }
              if (typeof valdata.package_duration != "undefined" || valdata.package_duration != null) 
              {
              $('.english_tab').trigger('click');
              }
              if (typeof valdata.price != "undefined" || valdata.price != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.extra_service != "undefined" || valdata.extra_service != null) 
              {
              $('.english_tab').trigger('click'); 
              }
              if (typeof valdata.description != "undefined" || valdata.description != null) 
              {
              $('.english_tab').trigger('click');
             }
              if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
              {
              $('.english_tab').trigger('click');    
              }

              if (typeof valdata.model_name_ar != "undefined" || valdata.model_name_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
              if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
          @else

              if (typeof valdata.model_name_ar != "undefined" || valdata.model_name_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
              if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
               if (typeof valdata.extra_service_ar != "undefined" || valdata.extra_service_ar != null) 
              {
              $('.arabic_tab').trigger('click');     
              }
              if (typeof valdata.model_name != "undefined" || valdata.model_name != null) 
              {
              $('.english_tab').trigger('click'); 

              }
              if (typeof valdata.location != "undefined" || valdata.location != null) 
              {
              $('.english_tab').trigger('click');


              }
              if (typeof valdata.package_schedule != "undefined" || valdata.package_schedule != null) 
              {
              $('.english_tab').trigger('click');


              }
              if (typeof valdata.package_duration != "undefined" || valdata.package_duration != null) 
              {
              $('.english_tab').trigger('click');


              }
              if (typeof valdata.price != "undefined" || valdata.price != null) 
              {
              $('.english_tab').trigger('click'); 


              }
              if (typeof valdata.extra_service != "undefined" || valdata.extra_service != null) 
              {
              $('.english_tab').trigger('click'); 


              }
              if (typeof valdata.description != "undefined" || valdata.description != null) 
              {

              $('.english_tab').trigger('click');


              }

              if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
              {

              $('.english_tab').trigger('click');     


              }
                    
                @endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

    /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script>
<script src="{{ url('')}}/public/assets/themes/js/timepicker/jquery.js"></script>
<script src="{{ url('')}}/public/assets/themes/js/timepicker/jquery.datetimepicker.js"></script>
<script> 
$(window).load(function(){
$("body").on('mouseover', '.datetimepicker', function() {  
$(this).datetimepicker({

ampm: true, // FOR AM/PM FORMAT
    format : 'Y-m-d g:i A' });
 
});

})
 
</script>



<!-- Add More product images start -->
 
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button_pro');  
    var wrapper = $('#img_upload_pro');
    var x = <?php echo $Countpro;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';
    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

 
<div class="overlay_popup"></div>
<div class="action_popup">
  <div class="action_active_popup">
    
    <div class="action_content"></div>
    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>
  </div>
</div>
<!-- action_popup --> 
<script type="text/javascript">
/* Action Popup */
jQuery('.cstatus').click(function(){  
 var status =jQuery(this).data("status");
 var id =jQuery(this).data("id");

 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);
 
@if($mer_selected_lang_code !='en')
jQuery('.action_content').html('هل تريد حذف هذا السجل؟');
@else
jQuery('.action_content').html('Do you want to delete this record?');
@endif

jQuery('.action_popup').fadeIn(500);  
jQuery('.overlay_popup').fadeIn(500);

jQuery('.status_yes').click(function(){
   var id =jQuery(this).attr("data-id");
    var status1 =jQuery(this).attr("data-checkstatus");
     if(status1=='Active'){
      var activestatus = 1;
      
     }else {
         var activestatus = 0;

     } 
var products = 1;
     jQuery.ajax({
        type: "GET",
        url: "{{ route('delete-shop-image') }}",
        data: {id:id,products:products},
        success: function(data) {
         
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    });
    
})
});
</script>

<!-- Add More product images end -->





@include('sitemerchant.includes.footer')
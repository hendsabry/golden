@include('sitemerchant.includes.header')  
@php $Travel_leftmenu =1; @endphp
 
<!--start merachant-->
<div class="merchant_vendor">
  <!--left panel-->
 @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->
@php
		$id        = $id;
		$sid       = $sid;
		$proid     = $proid;
		$cusid     = $cusid;
		$ordid     = $ordid;
		$productid = $productid;
		$getCustomer       = Helper::getuserinfoOrderdeatils($cusid,$ordid);
		$shipaddress       = Helper::getgenralinfo($ordid);  
		$cityrecord        = Helper::getcity($shipaddress->shipping_city);
		$getorderedproduct = Helper::getorderedproductdetail($ordid,'travel','travel');
	@endphp

  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif
        <a href="{{url('/')}}/travel-order/{{$id}}/{{$sid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a></h5></div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
			  <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} else { echo 'N/A'; }@endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;}else{ echo 'N/A' ;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add; @endphp  <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a>
                      @php }else{ echo 'N/A'; } @endphp
                    </div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" >
                        @php 
						$ordertime=strtotime($getorderedproduct->created_at);
                        $orderedtime = date("d M Y",$ordertime);
						if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                       @endphp
                   </div>
                  </div>
                </div>
				<div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    @php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod' ){ @endphp
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                   <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                   @php } @endphp
                  </div>
                </div>
				<div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                   <div class="info100">
				   @php if(isset($shipaddress->shipping_id) && $shipaddress->shipping_id!='')
					 { 
					  $getName = Helper::getShippingMethodName($shipaddress->shipping_id);
					  echo $getName;
					 }
					 else
					 {
					  echo 'N/A';
					 }
					@endphp
				 </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
				  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100"> 
					@php if(isset($shipaddress->shipping_charge) && $shipaddress->shipping_charge!='' && $shipaddress->shipping_charge!='0')
					 {
					  echo $shipaddress->shipping_charge;
					 }
					 else
					 {
					  echo 'N/A';
					 }
					 @endphp
				 </div>
            <!-- box -->
          </div>
           @php $getsearchedproduct = Helper::searchorderedDetails($ordid);
 if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ @endphp
                    <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONDATE') }} @endif</label>
                    <div class="info100"> @php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                     echo date('d M Y', strtotime($getsearchedproduct->occasion_date));
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                  @php } @endphp
        </div>
        <!--global end-->
      </div>
	  @php 
				$basetotal = 0; $couponcode=0; $couponcodeinfo='';
				$i = 1;
				if(count($productdata) > 0){
				foreach($productdata as $val)
				{
				  $basetotal            = ($basetotal+$val->total_price);
                  $pid                  = $val->product_id;
				  $serviceInfo = Helper::getProduckInfo($val->product_id); 
				  if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$priceperday = $serviceInfo->pro_disprice;}else{$priceperday = $serviceInfo->pro_price;}
				  $SecondCats  = Helper::getProAttr($val->product_id);
				  $average     = Helper::getProAverage($val->product_id);
				  $moredetail  = Helper::bookingCarTravelAgency($val->cus_id,$val->order_id,$val->product_id);

          $packageduration=Helper::orderedproductattribute($val->product_id);
				  
          $couponcode=$couponcode+$val->coupon_code_amount;
              $couponcodeinfo=$val->coupon_code;
				@endphp
			  <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
                    <div class="style_area">
                      <?php if($i==1){?><div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.item_information')!= '') {{  trans(Session::get('mer_lang_file').'.item_information') }} @else  {{ trans($MER_OUR_LANGUAGE.'.item_information') }} @endif</div><?php } ?>
                      <div class="style_box_type">
                        <div class="sts_box">
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.PACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PACKAGE') }} @endif</div>
                            <div class="style_label_text"> @php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){
                               if(Session::get('mer_lang_file')=='mer_ar_lang'){
                              echo $serviceInfo->pro_title_ar;
                            }else{
                              echo $serviceInfo->pro_title;
                                
                                }
                            } @endphp</div>
                          </div>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.NO_OF_PEOPLE')!= '') {{  trans(Session::get('mer_lang_file').'.NO_OF_PEOPLE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.NO_OF_PEOPLE') }} @endif</div>
                            <div class="style_label_text"> {{$val->quantity or '' }}</div>
                          </div>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.EXTRS_SERVICE')!= '') {{  trans(Session::get('mer_lang_file').'.EXTRS_SERVICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.EXTRS_SERVICE') }} @endif</div>
                            <div class="style_label_text"> <?php  $get_car_model = Helper::getProductName($val->product_id,$val->merchant_id,'Extra Service');
										if(isset($get_car_model->value) && $get_car_model->value!=''){
                          if(Session::get('mer_lang_file')=='mer_ar_lang'){
                             echo $get_car_model->value_ar;
                          }else{
                                echo $get_car_model->value;
                      }


                    }else{echo 'N/A';}?></div>
                          </div>
                        </div>
                        <div class="sts_box">
						  
                          <div class="style_left">
                            <div class="style_label">@if(Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif</div>
                            <div class="style_label_text"> SAR {{number_format($val->total_price,2)}}</div>
                          </div>
						  <?php $get_location = Helper::getProductName($val->product_id,$val->merchant_id,'Location');  ?>
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.LOCATION')!= '') {{  trans(Session::get('mer_lang_file').'.LOCATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.LOCATION') }} @endif</div>
                            <div class="style_label_text"><?php if(isset($get_location->value) && $get_location->value!=''){
                             if(Session::get('mer_lang_file')=='mer_ar_lang'){
                             echo $get_location->value_ar;
                          }else{
                             echo $get_location->value;
                           }

                           }else{echo 'N/A';} ?></div>
                          </div> 
						  
						  <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.PACKAGE_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.PACKAGE_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PACKAGE_DATE') }} @endif</div>
                            <div class="style_label_text"><?php echo $moredetail->rental_date.' '.$moredetail->rental_time;?></div>
                          </div>
                        </div>
                        <div class="sts_box">
                          <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_IMAGE') }} @endif</div>
                            <div class="style_label_text"> 
							@if($serviceInfo->pro_Img)   
                               <img src="{{ $serviceInfo->pro_Img }}" alt="" width="150">
                            @else
                                No image
                            @endif
						  </div>
                          </div>
						  
						                
                            <div class="style_left">
                            <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_DURATION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_DURATION') }} @endif</div>
                            <div class="style_label_text"> 
                               <?php $get_package = Helper::getProductName($val->product_id,$val->merchant_id,'Package Duration');  ?>
                                 

                                    <?php if(isset($get_package->value) && $get_package->value!=''){
                             if(Session::get('mer_lang_file')=='mer_ar_lang'){
                             echo $get_package->value_ar;
                          }else{
                             echo $get_package->value;
                           }

                           }else{echo 'N/A';} ?>


              </div>
                          </div>


                        </div>
                      </div>
                    </div>
                    
                  </div>
			       @php $i++;} @endphp
                <div class="merchant-order-total-area">
                <div class="merchant-order-total-line">@if(isset($couponcode) && $couponcode>=1)
             {{ (Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')}}: 

             @php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp  
          <br>

          {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')}}: {{ $couponcodeinfo }}
 <br>
          @endif



            @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             @endphp

          {{ (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp
         </div>
				 <div class="merchant-order-total-line">
        {{ (Lang::has(Session::get('mer_lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('mer_lang_file').'.TOTAL_PRICE'): trans($MER_OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
        <?php if(isset($shipaddress) && $shipaddress!=''){
         echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);
         } ?>              
        </div></div>
				<?php } ?>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div></div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
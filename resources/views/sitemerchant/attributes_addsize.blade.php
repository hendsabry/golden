<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_ATTRIBUTES_ADD_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ATTRIBUTES_ADD_SIZE') : trans($MER_OUR_LANGUAGE.'.MER_ATTRIBUTES_ADD_SIZE')}}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="public/assets/css/main.css" />
    <link rel="stylesheet" href="public/assets/css/theme.css" />
    <link rel="stylesheet" href="public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif		
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
        {!! $merchantheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      {!! $merchantleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_SIZE')  : trans($MER_OUR_LANGUAGE.'.MER_ADD_SIZE')}}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ADD_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ADD_SIZE')  : trans($MER_OUR_LANGUAGE.'.MER_ADD_SIZE')}}</h5>            
            
        </header>
          @if ($errors->any()) 
        <div class="alert alert-warning alert-dismissable">
	{{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
	{!! implode('', $errors->all('<li>:message</li>')) !!}</div>
        @endif
        
        @if (Session::has('exist_result'))
		 <div class="alert alert-danger alert-dismissable">{!! Session::get('exist_result') !!}
         {{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
		 
         </div>
		@endif
        <div id="div-1" class="accordion-body collapse in body">
		
            {!! Form::open(array('url'=>'mer_addsize_submit','class'=>'form-horizontal')) !!}
			

                <div class="form-group">
        
                    <label for="text1" class="control-label col-lg-1">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SIZE') :  trans($MER_OUR_LANGUAGE.'.MER_SIZE')}} <span class="text-sub">*</span></label>

                    <div class="col-lg-3">
                        <input id="text1" placeholder="{{ (Lang::has(Session::get('mer_lang_file').'.MER_SIZE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SIZE') :  trans($MER_OUR_LANGUAGE.'.MER_SIZE')}}" name="file_size" class="form-control" type="text" value="{!! Input::old('si_name') !!}">
                    </div>
                </div>
                <div class="form-group">
				{!! Html::decode(Form::label('','<span class="text-sub"></span>',['class' => 'control-label col-lg-1', 'for' => 'pass1'])) !!}
                    
                    <div class="col-lg-8">
                     <button class="btn btn-warning btn-sm btn-grad"  type="submit" style="color:#ffffff">{{ (Lang::has(Session::get('mer_lang_file').'.MER_SAVE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SAVE') : trans($MER_OUR_LANGUAGE.'.MER_SAVE')}}</a></button>
                     <button class="btn btn-danger btn-sm btn-grad" type="reset" style="color:#ffffff">{{ (Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_RESET') : trans($MER_OUR_LANGUAGE.'.MER_RESET')}}</a></button>
                    </div>
					  
                </div>

                
        {!! Form::close() !!}
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   {!! $merchantfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>

﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_PRODUCTS_HOLD_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.MER_MERCHANT_PRODUCTS_HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_PRODUCTS_HOLD_ORDERS') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
     <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
@php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get();  @endphp
     @if(count($favi)>0)  @foreach($favi as $fav) @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
@endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/success.css" />
	 <link href="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">
 <!-- HEADER SECTION -->
{!!$merchantheader!!}
        <!-- MENU SECTION -->
{!!$merchantleftmenus!!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '') ? trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME') }}</a></li>
                                <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_COD_HOLD_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.MER_COD_HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.MER_COD_HOLD_ORDERS') }}</a></li>
                            </ul>
                    </div>
                </div>
				
				<center><div class="cal-search-filter">
		<!--  <form  action="{!!action('MerchantTransactionController@cod_hold_orders')!!}" method="POST"> -->
		 	{{ Form::open(array('action'=>'MerchantTransactionController@cod_hold_orders','method'=>'POST')) }}
							<input type="hidden" name="_token"  value="<?php echo csrf_token(); ?>">
							 <div class="row">
							 <br>
							 
							 
							   <div class="col-sm-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top">{{ (Lang::has(Session::get('mer_lang_file').'.MER_FROM_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FROM_DATE') : trans($MER_OUR_LANGUAGE.'.MER_FROM_DATE') }}</div>
							<div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
							{{ Form::text('from_date',$from_date,array('id'=>'datepicker-8','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly')) }}
							 
							  </div>
							  </div>
							   </div>
							    <div class="col-sm-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top">{{ (Lang::has(Session::get('mer_lang_file').'.MER_TO_DATE')!= '') ? trans(Session::get('mer_lang_file').'.MER_TO_DATE') : trans($MER_OUR_LANGUAGE.'.MER_TO_DATE') }}</div>
							<div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
                             {{ Form::text('to_date',$to_date,array('id'=>'datepicker-9','class'=>'form-control','placeholder'=>'DD/MM/YYYY','required'=>'required','readonly'=>'readonly')) }}
							 
							  </div>
							  </div>
							   </div>
							   
							   <div class="form-group">
							   <div class="col-sm-2">
							 <input type="submit" name="submit" class="btn btn-block btn-success" value="{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') ?  trans(Session::get('mer_lang_file').'.MER_SEARCH') : trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }}">
							 </div>
                             <div class="col-sm-2">
								<a href="{{ url('').'/merchant_product_cod_hold_orders' }}">
									<button type="button" name="reset" class="btn btn-block btn-info">{{ (Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_RESET') : trans($MER_OUR_LANGUAGE.'.MER_RESET') }}</button>
								</a>
							</div>
							</div>
							
							 {{ Form::close()}}
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_COD_HOLD_ORDERS')!= '') ? trans(Session::get('mer_lang_file').'.MER_COD_HOLD_ORDERS') : trans($MER_OUR_LANGUAGE.'.MER_COD_HOLD_ORDERS') }}</h5>
            
        </header>
         <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
         	{{ Form::button('x',['class'=>'close closeAlert','aria-hidden'=>'true'])}}
         <!-- <button type="button" class="close closeAlert"  aria-hidden="true">×</button> --></div>
        <div id="div-1" class="accordion-body collapse in body">
            <form class="form-horizontal">

               <!--input type="hidden" id="return_url" value="<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" /-->
				<input type="hidden" id="return_url" value="{{ URL::to('/') }}" />
	   <div class="panel_marg_clr ppd">   
		   <table id="dataTables-example" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">	
					<th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 69px;" aria-label="S.No: activate to sort column ascending" aria-sort="ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '')  ? trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO') }}</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 78px;" aria-label="Name: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERS')!= '') ? trans(Session::get('mer_lang_file').'.MER_CUSTOMERS') : trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERS') }}</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 158px;" aria-label="Email: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE')!= '') ? trans(Session::get('mer_lang_file').'.MER_PRODUCT_TITLE') : trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_TITLE') }}</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="City: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_AMOUNT_S')!= '') ? trans(Session::get('mer_lang_file').'.MER_AMOUNT_S') : trans($MER_OUR_LANGUAGE.'.MER_AMOUNT_S') }}({{ Helper::cur_sym() }})</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 84px;" aria-label="Joined Date: activate to sort column ascending"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_TAX_S')!= '') ? trans(Session::get('mer_lang_file').'.MER_TAX_S') : trans($MER_OUR_LANGUAGE.'.MER_TAX_S') }}({{ Helper::cur_sym() }})</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 84px;" aria-label="Joined Date: activate to sort column ascending"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT')!= '') ? trans(Session::get('mer_lang_file').'.MER_SHIPPING_AMOUNT') : trans($MER_OUR_LANGUAGE.'.MER_SHIPPING_AMOUNT') }}({{ Helper::cur_sym() }})</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 101px;" aria-label="Send Mail: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS')!= '') ? trans(Session::get('mer_lang_file').'.MER_PAYMENT_STATUS') : trans($MER_OUR_LANGUAGE.'.MER_PAYMENT_STATUS') }}</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 88px;" aria-label="Edit: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TRANSACTION_DATE') : trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION_DATE') }}</th>
					<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 72px;" aria-label="Status: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_TRANSACTION_TYPE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_TRANSACTION_TYPE') : trans($MER_OUR_LANGUAGE.'.MER_TRANSACTION_TYPE') }}</th>
					 <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 72px;" aria-label="Status: activate to sort column ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DELIVERY_STATUS') : trans($MER_OUR_LANGUAGE.'.MER_DELIVERY_STATUS') }}</th>
					</tr>
                                    </thead>
                                    <tbody>
                                     @php $i = 1 ; @endphp
									  
@if(isset($_POST['submit']))
			
				@if(count($allprod_holdreports)>0) 
				@foreach($allprod_holdreports as $allorders_list) 
				@php	
					$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_HOLD')!= ''))? trans(Session::get('mer_lang_file').'.MER_HOLD'): trans($MER_OUR_LANGUAGE.'.MER_HOLD'); @endphp
					@if($allorders_list->cod_paytype==1)
					@php
						$ordertype=((Lang::has(Session::get('mer_lang_file').'.MER_PAYPAL')!= ''))? trans(Session::get('mer_lang_file').'.MER_PAYPAL'): trans($MER_OUR_LANGUAGE.'.MER_PAYPAL'); @endphp
					
					@else
					@php
						$ordertype=((Lang::has(Session::get('mer_lang_file').'.MER_COD')!= ''))? trans(Session::get('mer_lang_file').'.MER_COD'): trans($MER_OUR_LANGUAGE.'.MER_COD'); @endphp
					@endif
				@php	$total_tax = ($allorders_list->cod_amt * $allorders_list->cod_tax)/100 ; @endphp
					
					
			

					<tr class="gradeA odd">
                                            <td class="sorting_1">{{ $i }}</td>
                                            <td class="     "><?php echo $allorders_list->cus_name; echo '('.$allorders_list->cus_email.')';?></td>
                                             <td class="     ">{{ $allorders_list->pro_title }}</td>
                                            <td class="center     ">{{ $allorders_list->cod_amt }}</td>
                                            <td class="center     ">{{ $total_tax }}</td>
											<td class="center">{{ $allorders_list->cod_shipping_amt }}</td>
                                            <td class="center     "><a href="#" class="colr3">{{ $orderstatus }}</a></td>
                                            <td class="center     ">{{ $allorders_list->cod_date }}</td>
                                            <td class="center     "><a href="#" class="colr2">{{ $ordertype }}</a></td>
                                          <td class="center     ">
                                          	 @if($allorders_list->delivery_status==5)
                                                <span>Cancel Request Pending</span>
                                             @elseif($allorders_list->delivery_status==7)
                                                <span>Return Request Pending</span>
                                            @elseif($allorders_list->delivery_status==9)
                                                <span>Replace Request Pending</span>
                                             @else
                  <select name="<?php  echo 'status'.$allorders_list->cod_id;?>" class="btn btn-default" onchange="update_order_cod(this.value,'<?php echo $allorders_list->cod_transaction_id;?>')">
                                              <option value="1" <?php if($allorders_list->delivery_status==2 || $allorders_list->delivery_status==3 || $allorders_list->delivery_status==4) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==1){?> selected <?php } ?>>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_PLACED')!= '') ? trans(Session::get('mer_lang_file').'.MER_ORDER_PLACED') : trans($MER_OUR_LANGUAGE.'.MER_ORDER_PLACED') }}</option>
                                               
                                                <option value="6" <?php if($allorders_list->delivery_status==1 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==5) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==6){?> selected <?php } ?>>{{ (Lang::has(Session::get('mer_lang_file').'.MER_CANCELLED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CANCELLED') : trans($MER_OUR_LANGUAGE.'.MER_CANCELLED') }}</option>

                                                  <option value="2" <?php if($allorders_list->delivery_status==3 || $allorders_list->delivery_status==4) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==2){?> selected <?php } ?>>{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_PACKED')!= '') ? trans(Session::get('mer_lang_file').'.MER_ORDER_PACKED') : trans($MER_OUR_LANGUAGE.'.MER_ORDER_PACKED') }}</option>
												   <option value="3" <?php if($allorders_list->delivery_status==4) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==3){?> selected <?php } ?>>{{ (Lang::has(Session::get('mer_lang_file').'.MER_DISPATCHED')!= '') ? trans(Session::get('mer_lang_file').'.MER_DISPATCHED') : trans($MER_OUR_LANGUAGE.'.MER_DISPATCHED') }}</option>
												    <option value="4" <?php if($allorders_list->delivery_status==4){?> selected <?php } ?>>{{ (Lang::has(Session::get('mer_lang_file').'.MER_DELIVERED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DELIVERED') : trans($MER_OUR_LANGUAGE.'.MER_DELIVERED') }}</option>
												    <option value="8" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==7) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==8){?> selected <?php } ?>>{{ (Lang::has(Session::get('mer_lang_file').'.MER_RETURNED')!= '') ? trans(Session::get('mer_lang_file').'.MER_RETURNED') : trans($MER_OUR_LANGUAGE.'.MER_RETURNED') }}</option>

												    <option value="10" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==10 || $allorders_list->delivery_status==9) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==10){?> selected <?php } ?>>{{ (Lang::has(Session::get('mer_lang_file').'.MER_REPALCED')!= '') ?  trans(Session::get('mer_lang_file').'.MER_REPALCED') : trans($MER_OUR_LANGUAGE.'.MER_REPALCED') }}</option>
                                          </select>   @endif
                                              </td>


                                           
                                        </tr>

@php $i++; @endphp @endforeach @endif
			
@else
	
	@if(count($holdorders)>0) 
						@foreach($holdorders as $allorders_list) 
					
				@php	$orderstatus=((Lang::has(Session::get('mer_lang_file').'.MER_HOLD')!= ''))? trans(Session::get('mer_lang_file').'.MER_HOLD'): trans($MER_OUR_LANGUAGE.'.MER_HOLD');
					$ordertype = ''; @endphp
					@if($allorders_list->cod_paytype==1)
					@php
						$ordertype=((Lang::has(Session::get('mer_lang_file').'.MER_PAYPAL')!= ''))? trans(Session::get('mer_lang_file').'.MER_PAYPAL'): trans($MER_OUR_LANGUAGE.'.MER_PAYPAL'); @endphp
					
					@else
					@php
						$ordertype=((Lang::has(Session::get('mer_lang_file').'.MER_COD')!= ''))? trans(Session::get('mer_lang_file').'.MER_COD'): trans($MER_OUR_LANGUAGE.'.MER_COD'); @endphp
					@endif
			@php		$total_tax = ($allorders_list->cod_amt * $allorders_list->cod_tax)/100 ;
					@endphp
			

					<tr class="gradeA odd">
                                            <td class="sorting_1">{{ $i }}</td>
                                            <td class="     "><?php echo $allorders_list->cus_name; echo '('.$allorders_list->cus_email.')';?></td>
                                           <td class="     ">{{ $allorders_list->pro_title }}</td>
                                            <td class="center     ">{{ $allorders_list->cod_amt }}</td>
                                            <td class="center     ">{{ $total_tax }}</td>
											<td class="center">{{ $allorders_list->cod_shipping_amt }}</td>
                                            <td class="center     "><a href="#" class="colr3">{{ $orderstatus }}</a></td>
                                            <td class="center     ">{{ $allorders_list->cod_date }}</td>
                                            <td class="center     "><a href="#" class="colr2">{{ $ordertype }}</a></td>
                                          <td class="center     ">
                                          	 @if($allorders_list->delivery_status==5)
                                                <span>Cancel Request Pending</span>
                                             @elseif($allorders_list->delivery_status==7)
                                                <span>Return Request Pending</span>
                                            @elseif($allorders_list->delivery_status==9)
                                                <span>Replace Request Pending</span>
                                             @else
                  <select name="<?php  echo 'status'.$allorders_list->cod_id;?>" class="btn btn-default" onchange="update_order_cod(this.value,'<?php echo $allorders_list->cod_transaction_id;?>','<?php echo $allorders_list->pro_id;?>')">
                                                    <option value="1" <?php if($allorders_list->delivery_status==2 || $allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==1){?> selected <?php } ?>>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PLACED')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ORDER_PLACED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PLACED') }}</option>
                                               
                                                <option value="6" <?php if($allorders_list->delivery_status==1 || $allorders_list->delivery_status==5) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==6){?> selected <?php } ?>>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCELLED')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCELLED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCELLED') }}</option>

                                                  <option value="2" <?php if($allorders_list->delivery_status==3 || $allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?><?php if($allorders_list->delivery_status==2){?> selected <?php } ?>> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_PACKED')!= '') ? trans(Session::get('admin_lang_file').'.BACK_ORDER_PACKED') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_PACKED') }}</option>
												   <option value="3" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==3){?> selected <?php } ?>>@if (Lang::has(Session::get('admin_lang_file').'.BACK_DISPATCHED')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DISPATCHED') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DISPATCHED') }} @endif</option>
												    <option value="4" <?php if($allorders_list->delivery_status==6 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==10) {?>style="display:none" <?php } ?> <?php if($allorders_list->delivery_status==4){?> selected <?php } ?>>@if (Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERED')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_DELIVERED') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERED') }} @endif</option>
												    <option value="8" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==8 || $allorders_list->delivery_status==7) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==8){?> selected <?php } ?>>@if (Lang::has(Session::get('admin_lang_file').'.BACK_RETURNED')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_RETURNED') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_RETURNED') }} @endif</option>

												    <option value="10" <?php if($allorders_list->delivery_status==4 || $allorders_list->delivery_status==10 || $allorders_list->delivery_status==9) {?>style="display:block" <?php }else { echo 'style="display:none"';} ?> <?php if($allorders_list->delivery_status==10){?> selected <?php } ?>>@if (Lang::has(Session::get('admin_lang_file').'.BACK_REPALCED')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_REPALCED') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_REPALCED') }} @endif</option>
                                          </select>   @endif
                                              </td>


                                           
                                        </tr>

@php $i++; @endphp @endforeach @endif @endif			
							
								</tbody>
                                </table></div>

        </div>
                </div>

         </form>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
   {!!$merchantfooter!!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
   <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
 
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script> 
    
    <script>
	function update_order_cod(id,orderid,proid)
	{
			 var passdata= 'id='+id+"&order_id="+orderid+"&proid="+proid;
			 var pathnametemp =$('#return_url').val();
				
			  $.ajax( {
			      type: 'get',
				  data: passdata,
				  url: 'update_cod_order_status',
				   beforeSend: function() {
				        // setting a timeout
				        //$(placeholder).addClass('loading');
				       document.getElementById("loader").style.display = "block";
				    },
				  success: function(responseText){ 
				  // document.getElementById("myDIV").style.display = "block";
					//alert(responseText);
					document.getElementById("loader").style.display = "none";
					document.getElementById("loadalert").style.display = "block";
					if(responseText=="success")
					{ 	 
						setTimeout(function () {
							 location.reload();  
						},1000);
					   
					}
				}		
			});	
	}

	
	</script>
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"<?php if (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS');} ?>",
               nextText:"<?php if (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS');} ?>",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"<?php if (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS');} ?>",
               nextText:"<?php if (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '') { echo  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS');}  else { echo trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS');} ?>",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
          /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/
$(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
      </script>
<script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>

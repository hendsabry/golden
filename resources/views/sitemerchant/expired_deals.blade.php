<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME}} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_EXPIRED_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_EXPIRED_DEALS'): trans($MER_OUR_LANGUAGE.'.MER_MANAGE_EXPIRED_DEALS')}}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
   <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
	 <link href="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
     <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


        <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
       {!! $adminleftmenus !!}
        <!--END MENU SECTION -->
		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME')}}</a></li>
                                <li class="active"><a >{{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_EXPIRED_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_EXPIRED_DEALS') : trans($MER_OUR_LANGUAGE.'.MER_MANAGE_EXPIRED_DEALS')}}</a></li>
                            </ul>
                    </div>
                </div>
				<center><div class="cal-search-filter">
				
				{{ Form::open(['action' => 'MerchantdealsController@expired_deals', 'method' => 'POST']) }}
		 
							<input type="hidden" name="_token"  value="{!! csrf_token()!!}">
							 <div class="row">
							 <br>
							  <div class="col-sm-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top">{{ (Lang::has(Session::get('mer_lang_file').'.MER_FROM_DATE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_FROM_DATE') : trans($MER_OUR_LANGUAGE.'.MER_FROM_DATE')}}</div>
							<div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
							{{ Form::text('from_date',$from_date,array('id'=>'datepicker-8','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly')) }}
							 
							 
							  </div>
							  </div>
							   </div>
							    <div class="col-sm-4">
							    <div class="item form-group">
							<div class="col-sm-6 date-top">
							{{ (Lang::has(Session::get('mer_lang_file').'.MER_TO_DATE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_TO_DATE')  : trans($MER_OUR_LANGUAGE.'.MER_TO_DATE')}}
							</div>
							<div class="col-sm-6 place-size">
                            <span class="icon-calendar cale-icon"></span>
							{{ Form::text('to_date',$to_date,array('id'=>'datepicker-9','class' => 'form-control','placeholder' =>'DD/MM/YYYY','required', 'readonly')) }}
							 
							 
							  </div>
							  </div>
							   </div>
							   
							   <div class="form-group">
							   <div class="col-sm-2">
							    <input type="submit" name="submit" class="btn btn-block btn-success" value="{{ (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_SEARCH') : trans($MER_OUR_LANGUAGE.'.MER_SEARCH')}}">
							   </div>
                               <div class="col-sm-2">
                                    <a href="{{ url('').'/mer_expired_deals'}}"><button type="button" name="reset" class="btn btn-block btn-info">{{ (Lang::has(Session::get('mer_lang_file').'.MER_RESET')!= '') ?  trans(Session::get('mer_lang_file').'.MER_RESET')  : trans($MER_OUR_LANGUAGE.'.MER_RESET')}}</button></a>
                                </div>
							</div>
							
							 {{ Form::close() }}</div>
							 </center>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MANAGE_EXPIRED_DEALS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MANAGE_EXPIRED_DEALS') : trans($MER_OUR_LANGUAGE.'.MER_MANAGE_EXPIRED_DEALS')}}</h5>
            
         </header>
          <div style="display: none;" class="la-alert date-select1 alert-success alert-dismissable">End date should be greater than Start date!
		  {{ Form::button('×',['class' => 'close closeAlert' , 'aria-hidden' => 'true']) }}
         </div>
           @if (Session::has('block_message'))
		<div class="alert alert-success alert-dismissable">{!! Session::get('block_message') !!}
	{{ Form::button('×',['class' => 'close' , 'data-dismiss' => 'alert','aria-hidden' => 'true']) }}
        </div>
		@endif
        <div id="div-1" class="accordion-body collapse in body">
           
        <div class="panel_marg_clr ppd">
           <table aria-describedby="dataTables-example_info" class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
                                    <thead>
                                        <tr role="row">
										<th aria-label="Rendering engine: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting_asc" aria-sort="ascending">{{ (Lang::has(Session::get('mer_lang_file').'.MER_S.NO')!= '') ?  trans(Session::get('mer_lang_file').'.MER_S.NO') : trans($MER_OUR_LANGUAGE.'.MER_S.NO') }}</th>
										<th aria-label="Browser: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEALS_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEALS_NAME')  : trans($MER_OUR_LANGUAGE.'.MER_DEALS_NAME')}}</th>
										
										<th aria-label="Engine version: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_STORE_NAME')!= '') ?  trans(Session::get('mer_lang_file').'.MER_STORE_NAME')  :  trans($MER_OUR_LANGUAGE.'.MER_STORE_NAME') }}</th>
										<th aria-label="Platform(s): activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CITY')  : trans($MER_OUR_LANGUAGE.'.MER_CITY') }}</th>										
                                        <th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_ORIGINAL_PRICE') : trans($MER_OUR_LANGUAGE.'.MER_ORIGINAL_PRICE')}}({{ Helper::cur_sym() }})</th>
										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DISCOUNTED_PRICE'): trans($MER_OUR_LANGUAGE.'.MER_DISCOUNTED_PRICE') }}({{ Helper::cur_sym() }})</th>
										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting"> {{ (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_IMAGE')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_IMAGE'): trans($MER_OUR_LANGUAGE.'.MER_DEAL_IMAGE')}} </th>	
									 
										<th aria-label="CSS grade: activate to sort column ascending" style="width: 100px;" colspan="1" rowspan="1" aria-controls="dataTables-example" tabindex="0" class="sorting">{{ (Lang::has(Session::get('mer_lang_file').'.MER_DEAL_DETAILS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_DEAL_DETAILS') : trans($MER_OUR_LANGUAGE.'.MER_DEAL_DETAILS')}}</th>

										</tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = 1 ; ?>
                                   
@if(isset($_POST['submit']))
				@if(count($merchantexp_dealrep)>0) 
			@foreach($merchantexp_dealrep as $deal_list)  
									<?php  $deal_get_img = explode("/**/",$deal_list->deal_image); ?>
									  @if($deal_list->deal_status == 1)
									  <?php 
										  $process = "<a href='".url('block_deals/'.$deal_list->deal_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>"; ?>
								   @elseif($deal_list->deal_status == 0) {
										<?php 		
										$process = "<a href='".url('block_deals/'.$deal_list->deal_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>"; ?>
									  @endif
                                    <tr class="gradeA odd">
                                            <td class="sorting_1">{{ $i}}</td>
                                            <td class="  ">{{ substr($deal_list->deal_title,0,45)}}</td>
                                       		<td class="center  ">{{ $deal_list->stor_name}}</td>
                                            <td class="center">{{ $deal_list->ci_name}}</td>
                                            <td class="center  ">{{ $deal_list->deal_original_price}}</td>
                                            <td class="center  ">{{ $deal_list->deal_discount_price}}</td>
                                            <td class="center  ">
											@php $pro_img = $deal_get_img[0];
							   $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  @endphp
							
							  @if($deal_get_img !='') {{-- check image is available  --}}
								
					  
								
						   @if($deal_get_img[0] !='')  {{-- //check image is null --}}
						    
							@php  
							   $img_data = "public/assets/deals/".$pro_img;  @endphp
							    @if(file_exists($img_data))  
									 @php
								 	             $prod_path = url('').'/public/assets/deals/'.$pro_img;
								     @endphp
								@else
									@if(isset($DynamicNoImage['dealImg'])) {{-- //check no_product_image is exist  --}}
										 					
											@php
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];
											@endphp
												@if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path))
												 @php
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												@endphp	
												@endif
									     @endif
										 
										 
                                @endif
					       
					      
						   @else
						   @php
							    $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  @endphp
								@if(isset($DynamicNoImage['dealImg'])) {{-- //check no_product_image is exist  --}}
										 	@php					
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];
											@endphp
												@if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path))
												@php 
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												@endphp	
												@endif
										
									     @endif
										 
					@endif
						    
					  
			      
				  @else
				  
					 @php  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; @endphp
					   @if(isset($DynamicNoImage['dealImg']))
										 				
							@php				
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											@endphp
											
											  @if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !='')
											
												@php
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											    @endphp
											@endif
											
									     @endif

									 
								 
					  
					
				 @endif											
											<a ><img style="height:40px;" src="{{ $prod_path }}"></a></td>
                                            <td class="center  "><a href="{{ url('mer_deal_details')."/".$deal_list->deal_id }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW'): trans($MER_OUR_LANGUAGE.'.MER_VIEW')}}</a>&nbsp;|&nbsp;<a href="{{ url('mer_edit_deals')."/".$deal_list->deal_id}}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT') : trans($MER_OUR_LANGUAGE.'.MER_EDIT')}}</a></td>
                                     </tr>
<?php $i++; ?>
@endforeach
@endif 	
			
@else
	
	@if(count($deal_details)>0) 
									  @foreach($deal_details as $deal_list)  
									<?php  $deal_get_img = explode("/**/",$deal_list->deal_image); ?>
									  @if($deal_list->deal_status == 1)
									  <?php 
										  $process = "<a href='".url('block_deals/'.$deal_list->deal_id.'/0')."' > <i style='margin-left:10px;' class='icon icon-ok icon-me'></i> </a>"; ?>
								   @elseif($deal_list->deal_status == 0) 
										<?php 	
										  $process = "<a href='".url('block_deals/'.$deal_list->deal_id.'/1')."' > <i style='margin-left:10px;' class='icon icon-ban-circle icon-me'></i> </a>"; ?>
									  @endif
									  
                                    <tr class="gradeA odd">
                                            <td class="sorting_1">{{ $i}}</td>
                                            <td class="  ">{{ substr($deal_list->deal_title,0,45)}}</td>
                                       		<td class="center">{{ $deal_list->stor_name}}</td>
                                            <td class="center">{{ $deal_list->ci_name}}</td>
                                            <td class="center">{{ $deal_list->deal_original_price}}</td>
                                            <td class="center">{{ $deal_list->deal_discount_price}}</td>
                                            <td class="center">
												@php $pro_img = $deal_get_img[0];
							   $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  @endphp
							
							  @if($deal_get_img !='') {{-- check image is available  --}}
								
					  
								
						   @if($deal_get_img[0] !='')  {{-- //check image is null --}}
						    
							@php  
							   $img_data = "public/assets/deals/".$pro_img;  @endphp
							    @if(file_exists($img_data))  
									 @php
								 	             $prod_path = url('').'/public/assets/deals/'.$pro_img;
								     @endphp
								@else
									@if(isset($DynamicNoImage['dealImg'])) {{-- //check no_product_image is exist  --}}
										 					
											@php
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['dealImg'];
											@endphp
												@if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path))
												 @php
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												@endphp	
												@endif
									     @endif
										 
										 
                                @endif
					       
					      
						   @else
						   @php
							    $prod_path = url('').'/public/assets/default_image/No_image_deal.png';  @endphp
								@if(isset($DynamicNoImage['dealImg'])) {{-- //check no_product_image is exist  --}}
										 	@php					
											$dyanamicNoImg_path= "public/assets/noimage/".$DynamicNoImage['productImg'];
											@endphp
												@if($DynamicNoImage['dealImg'] !='' && file_exists($dyanamicNoImg_path))
												@php 
													$prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg'];
												@endphp	
												@endif
										
									     @endif
										 
					@endif
						    
					  
			      
				  @else
				  
					 @php  $prod_path = url('').'/public/assets/default_image/No_image_deal.png'; @endphp
					   @if(isset($DynamicNoImage['dealImg']))
										 				
							@php				
											 $dyanamicNoImg_path="public/assets/noimage/".$DynamicNoImage['dealImg'];
											@endphp
											
											  @if(file_exists($dyanamicNoImg_path) && $DynamicNoImage['dealImg'] !='')
											
												@php
												 $prod_path = url('').'/public/assets/noimage/'.$DynamicNoImage['dealImg']; 
											    @endphp
											@endif
											
									     @endif

									 
								 
					  
					
				 @endif												
											<a ><img style="height:40px;" src="{{ $prod_path }}"></a></td>
                                            <td class="center  "><a href="<?php echo url('mer_deal_details')."/".$deal_list->deal_id; ?>">{{ (Lang::has(Session::get('mer_lang_file').'.MER_VIEW')!= '') ?  trans(Session::get('mer_lang_file').'.MER_VIEW'): trans($MER_OUR_LANGUAGE.'.MER_VIEW')}}</a>&nbsp;|&nbsp;<a href="{{ url('mer_edit_deals')."/".$deal_list->deal_id}}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT') :  trans($MER_OUR_LANGUAGE.'.MER_EDIT')}}</a></td>
                                    </tr>
<?php $i++;  ?>

@endforeach
@endif
@endif
									
										</tbody>
                                </table></div>

                                   </div>
        </div>
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

   <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER --> 
     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
        <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/dataTables/dataTables.bootstrap.js"></script>
     <script>
         $(document).ready(function () {
             $('#dataTables-example').dataTable();
         });
    </script>
    <!-- END GLOBAL SCRIPTS -->   
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	   <script>
         $(function() {
            $( "#datepicker-8" ).datepicker({
               prevText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')  : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')}}",
               nextText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS') }}",
               showOtherMonths:true,
               selectOtherMonths: false
            });
            $( "#datepicker-9" ).datepicker({
               prevText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_PREVIOUS_MONTHS') : trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_PREVIOUS_MONTHS')}}",
               nextText:"{{ (Lang::has(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_CLICK_FOR_NEXT_MONTHS') :  trans($MER_OUR_LANGUAGE.'.MER_CLICK_FOR_NEXT_MONTHS') }}",
               showOtherMonths:true,
               selectOtherMonths: true
            });
         });
          /** Check start date and end date**/
         $("#datepicker-8,#datepicker-9").change(function() {
    var startDate = document.getElementById("datepicker-8").value;
    var endDate = document.getElementById("datepicker-9").value;
     if (this.id == 'datepicker-8') {
              if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-8').val('');
                   $(".date-select1").css({"display" : "block"});
                    return false;
                }
            } 

             if(this.id == 'datepicker-9') {
                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    $('#datepicker-9').val('');
                     $(".date-select1").css({"display" : "block"});
                     return false;
                    //alert("End date should be greater than Start date");
                }
                }
                
            
      //document.getElementById("ed_endtimedate").value = "";
   
  });
/*Start date end date check ends*/
$(".closeAlert").click(function(){
    $(".alert-success").hide();
  });
      </script>
	  <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>

@include('sitemerchant.includes.header') 
@php $hall_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_CATEGORY') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">

                <form name="form1"  id ="menucategory" method="post" action="{{ route('updatecategory') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form_row">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CATEGORY_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); @endphp </span> </label>
                    <div class="info100">
                      <input type="text" class="english" name="category_name" maxlength="90" value="{{ $menucategory->menu_name }}" id="category_name" required="" >
                      @if(!empty($get_active_lang))
                      @foreach($get_active_lang as $get_lang)
                      <?php 
                                        $get_lang_code = $get_lang->lang_code;
                                        $get_lang_name = $get_lang->lang_name;
                                ?>
                      <input class="arabic ar" id="category_{{ $get_lang_code}}" maxlength="90" value="{{ $menucategory->menu_name_ar }}"  name="category_name_{{ $get_lang_code }}" value="" required=""  type="text" >
                      <div id="title_{{ $get_lang_code }}_error_msg"  style="color:#F00;font-weight:800"  > @if ($errors->has('Hotel_Name_'.$get_lang_code.'')) {{ $errors->first('Hotel_Name_'.$get_lang_code.'') }}@endif </div>
                      @endforeach
                      @endif </div>
                      </div>
                  </div>
                  <div class="form_row_left common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CREATED_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CREATED_IMAGE'); @endphp </span> </label>
                    <div class="input-file-area">
                      <label for="company_logo1">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        <div class="file-value" id="file_value2"></div>
                      </div>
                      </label>
                      <input type="file" name="catimage" id="company_logo1" class="info-file">
                    </div>
                    @if(!isset($menucategory->menu_image)) <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span> @endif
                    @if(isset($menucategory->menu_image) && $menucategory->menu_image !='')
                    <div class="form-upload-img"> <img src="{{$menucategory->menu_image}}"> </div>

                    @endif </div>
                  <div class="form_row">
                  <div class="form_row_left">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="hid" value="{{ $hid }}">
                    <input type="hidden" name="bid" value="{{ $bid }}">
                    <div class="english">
                      <input type="submit" name="submit" value="Update">
                    </div>
                    <div class="arabic ar">
                      <input type="submit" name="submit" value="تحديث">
                    </div>
                    </div>
                  </div>
                </form>

              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
$("form").data("validator").settings.ignore = "";
 </script> 
<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },

                      catimage: {
                       required: false,
                       accept:"png|jpe?g|gif",
                      }, 

                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             category_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CATEGORY_NAME') }} @endif",
                      },  

                 category_name_ar: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CAT_ARABIC_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CAT_ARABIC_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CAT_ARABIC_AR') }} @endif",
                      },  

                    catimage: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },      
                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;


   @if($mer_selected_lang_code !='en')

                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    
                  
                     if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
                    if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {

                    $('.arabic_tab').trigger('click');     

                    }
                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                    $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.catimage != "undefined" || valdata.catimage != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

@endif
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
@include('sitemerchant.includes.footer')
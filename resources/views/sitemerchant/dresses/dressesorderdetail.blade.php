@include('sitemerchant.includes.header')  
@php $dressesbig_leftmenu =1; @endphp
<!--start merachant-->
<div class="merchant_vendor">
  <!--left panel-->
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <!--left panel end-->
  <!--right panel-->
@php
 
	$id                = $id;
	$sid               = $sid;
	$autoid            = $autoid;
	$proid             = $proid;
	$ordid             = $ordid;
	$cusid             = $cusid;
	$productid         = $productid;
	$getCustomer       = Helper::getuserinfo($cusid);
	$shipaddress       = Helper::getgenralinfo($ordid); 
    $oid = $ordid;
	$cityrecord        = Helper::getcity($shipaddress->shipping_city);
	$getorderedproduct = Helper::getorderedproduct($ordid);
@endphp
  <div class=" right_panel" >
    <div class="inner">
      <!--haeding panel-->
      <div class="service_listingrow">
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_Order_Detail')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order_Detail') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order_Detail') }} @endif <a href="{{url('/')}}/dresses-order/{{$id}}/{{$sid}}" class="order-back-page">@if (Lang::has(Session::get('mer_lang_file').'.MER_BACK')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BACK') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BACK') }} @endif</a></h5>
       </div>
      <!-- service_listingrow -->
      <!--haeding panel end-->
      <!-- Display Message after submition -->
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <!--global start-->
        <div class="row">
          <div class="col-lg-12">
            <div class="box hall-od commonbox">
              <div class="hall-od-top">
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Customer_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Customer_Name') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Customer_Name') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_name) && $shipaddress->payer_name!=''){echo $shipaddress->payer_name;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_EMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EMAIL') }} @endif</label>
                    <div class="info100" >@php if(isset($shipaddress->payer_email) && $shipaddress->payer_email!=''){echo $shipaddress->payer_email;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_PHONE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_PHONE') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->payer_phone) && $shipaddress->payer_phone!=''){echo $shipaddress->payer_phone;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CITY') }} @endif</label>
                    <div class="info100">@php if(isset($cityrecord->ci_name) && $cityrecord->ci_name!=''){echo $cityrecord->ci_name;} @endphp</div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ADDRESS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADDRESS') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_shipping_add) && $shipaddress->order_shipping_add!=''){echo $shipaddress->order_shipping_add;} @endphp <a target="_blank" href="https://www.google.com/maps/place/<?=$shipaddress->order_shipping_add?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a></div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_DATE') }} @endif</label>
                    <div class="info100" > @php 
                      $ordertime=strtotime($getorderedproduct->created_at);
                      $orderedtime = date("d M Y",$ordertime);
                      if(isset($orderedtime) && $orderedtime!=''){echo $orderedtime;}
                      @endphp </div>
                  </div>
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_COMMISSION_PAYMENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_COMMISSION_PAYMENT') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_paytype) && $shipaddress->order_paytype!=''){echo $shipaddress->order_paytype;} @endphp</div>
                  </div>
                   @php if(isset($shipaddress->order_paytype) && strtolower($shipaddress->order_paytype)!='cod'){ @endphp
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.transaction_id')!= '') {{  trans(Session::get('mer_lang_file').'.transaction_id') }} @else  {{ trans($MER_OUR_LANGUAGE.'.transaction_id') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->transaction_id) && $shipaddress->transaction_id!=''){echo $shipaddress->transaction_id;} @endphp</div>
                  </div>
                    @php } @endphp
                </div>
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.ORDER_ID')!= '') {{  trans(Session::get('mer_lang_file').'.ORDER_ID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ORDER_ID') }} @endif</label>
                    <div class="info100">@php if(isset($shipaddress->order_id) && $shipaddress->order_id!=''){echo $shipaddress->order_id;} @endphp</div>
                  </div>
                  <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_METHOD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_METHOD') }} @endif</label>
                    <div class="info100"> 
 
 {{ $productdata[0]->shippingMethod }}
 
                    </div>
                  </div>
                </div>
                <!-- form_row -->
                <div class="form_row">
                  <div class="noneditbox">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE')!= '') {{  trans(Session::get('mer_lang_file').'.MR_SHIPPING_CHARGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MR_SHIPPING_CHARGE') }} @endif</label>
                    <div class="info100">SAR  {{ $productdata[0]->shippingPrice }}</div>
                    <!-- box -->
                  </div>

                     @php $getsearchedproduct = Helper::searchorderedDetails($oid);
 if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!=''){ @endphp
                    <div class="noneditbox box-right">
                    <label class="form_label">@if (Lang::has(Session::get('mer_lang_file').'.OCCASIONDATE')!= '') {{  trans(Session::get('mer_lang_file').'.OCCASIONDATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.OCCASIONDATE') }} @endif</label>
                    <div class="info100"> @php if(isset($getsearchedproduct->occasion_date) && $getsearchedproduct->occasion_date!='')
                      {                      
                     echo date('d M Y', strtotime($getsearchedproduct->occasion_date));
                      }
                      else
                      {
                      echo 'N/A';
                      }
                      @endphp </div>
                  </div>
                  @php } @endphp


                </div>
                <!--global end-->
              </div>
			   @php 
			  $basetotal = 0;  $couponcode=0; $couponcodeinfo='';
			  $i = 1;
			  if(count($productdata) > 0){
			  foreach($productdata as $val)
			  {
			  $basetotal            = ($basetotal+$val->total_price);
			  $pid                  = $val->product_id;
			  $serviceInfo          = Helper::getProduckInfo($val->product_id);
			  $getAttributeInfo     = Helper::getAllProductAttribute($val->product_id,$val->merchant_id);
			  $getPrice=0;
        $newbprice=0;

         $couponcode=$couponcode+$val->coupon_code_amount;;
        $couponcodeinfo=$val->coupon_code;
        if(strtolower($val->buy_rent)=='rent'){
       if(isset($val->bookingdetail->return_time) && isset($val->bookingdetail->rental_time)){
        $time1=str_replace(',','',$val->bookingdetail->return_time);
        $time2=str_replace(',','',$val->bookingdetail->rental_time);
         $hourdiff = round((strtotime($time1) - strtotime($time2))/3600, 1);
        if($hourdiff>0){
          $bprice=$val->total_price-$val->bookingdetail->insurance_amount; 
          $newbprice=$bprice;      
        $getPrice=$bprice/$hourdiff;   }  else{ $getPrice=0;}  
        } 
      }else{

      if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
    }



			  @endphp
      <div class="sts_boxs" <?php if($i%2==0){echo 'style="background:#f5f7f8;"';}?>>
        <div class="style_area">
          <?php if($i==1){?>
          <div class="style_head">@if (Lang::has(Session::get('mer_lang_file').'.item_information')!= '') {{  trans(Session::get('mer_lang_file').'.item_information') }} @else  {{ trans($MER_OUR_LANGUAGE.'.item_information') }} @endif</div>
          <?php } ?>
          <div class="style_box_type">
            <div class="sts_box">
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.DRESS')!= '') {{  trans(Session::get('mer_lang_file').'.DRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.DRESS') }} @endif</div>
                <div class="style_label_text"> <?php 
                            $protitle = 'pro_title'; 
                            if(Session::get('mer_lang_file')!='mer_en_lang')
                            {
                            $protitle = 'pro_title_ar'; 
                            }
                            echo $serviceInfo->$protitle;
                             ?></div>
              </div>
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_QUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_QUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_QUANTITY') }} @endif</div>
                <div class="style_label_text">{{$val->quantity}} </div>
              </div>
              <div class="style_left">
                <div class="style_label">@if(Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif</div>
                <div class="style_label_text">SAR {{number_format($val->total_price,2)}}</div>
              </div>
            </div>
            <div class="sts_box">
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Price')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Price') }} @endif</div>
                <div class="style_label_text">SAR 
                      <?php if($newbprice>0){?>  <?php echo number_format($newbprice,2); ?> <?php }else{?>
                  <?php echo number_format($getPrice,2); ?>
                    
                    <?php } ?>
                  </div>
              </div>
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.SIZE')!= '') {{  trans(Session::get('mer_lang_file').'.SIZE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SIZE') }} @endif</div>
                <div class="style_label_text"><?php if(isset($val->product_size) && $val->product_size!=''){ $size = str_replace('Size','',$val->product_size); echo $size;}?></div>
              </div>
			  <?php if(isset($val->buy_rent) && $val->buy_rent!=''){?>
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_FOR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FOR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FOR') }} @endif</div>
                  <div class="style_label_text">
                    @if (Lang::has(Session::get('mer_lang_file').'.MER_RENT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_RENT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_RENT') }} @endif<?php //echo ucfirst($val->buy_rent); ?>                    
                  </div>
              </div>
			  <?php } ?>
            </div>
			
			  <?php 
			  if(isset($val->buy_rent) && $val->buy_rent=='rent'){           

           ?><div class="sts_box">
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.RENT_CHARGES')!= '') {{  trans(Session::get('mer_lang_file').'.RENT_CHARGES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.RENT_CHARGES') }} @endif</div>
                <div class="style_label_text">SAR <?php echo number_format($getPrice,2); ?></div>
              </div>
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.BOOKING_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.BOOKING_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.BOOKING_DATE') }} @endif</div>
                <div class="style_label_text">{{$val->bookingdetail->rental_time or ''}}</div>
              </div>
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_Returning_Date')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Returning_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Returning_Date') }} @endif</div>
                <div class="style_label_text">{{$val->bookingdetail->return_time or ''}}</div>
              </div>
			 </div> 
			 <?php }
			  //echo '<pre>';print_r($val);
			  ?> 
			  
			  <div class="sts_box">
			  <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.INSURANCEAMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.INSURANCEAMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.INSURANCEAMOUNT') }} @endif</div>
                <div class="style_label_text">SAR @php if(isset($val->bookingdetail->insurance_amount) && $val->bookingdetail->insurance_amount!=''){echo $val->bookingdetail->insurance_amount;}else{echo 'N/A';} @endphp</div>
              </div>
              <div class="style_left">
                <div class="style_label">@if (Lang::has(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PRODUCT_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PRODUCT_IMAGE') }} @endif</div>
                <div class="style_label_text"> @if($serviceInfo->pro_Img) <img src="{{ $serviceInfo->pro_Img }}" alt="" width="150"> @else
                  No image
                  @endif </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      @php $i++;} @endphp
      <div class="merchant-order-total-area">
                <div class="merchant-order-total-line">
          @if(isset($couponcode) && $couponcode>=1)
             {{ (Lang::has(Session::get('mer_lang_file').'.COUPON_AMOUNT')!= '')  ?  trans(Session::get('mer_lang_file').'.COUPON_AMOUNT'): trans($MER_OUR_LANGUAGE.'.COUPON_AMOUNT')}}: 

             @php if(isset($couponcode) && $couponcode!='')
          {
          echo 'SAR '.number_format($couponcode,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp  
          <br>

          {{ (Lang::has(Session::get('mer_lang_file').'.MER_COUPON_CODE')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_COUPON_CODE'): trans($MER_OUR_LANGUAGE.'.MER_COUPON_CODE')}}: {{ $couponcodeinfo }}
 <br>
          @endif



            @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount;
             @endphp

          {{ (Lang::has(Session::get('mer_lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('mer_lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($ordid,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp 
 </div>
		  <div class="merchant-order-total-line">
        @if (Lang::has(Session::get('mer_lang_file').'.Total_Price')!= '') {{  trans(Session::get('mer_lang_file').'.Total_Price') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Total_Price') }} @endif:
        <?php if(isset($shipaddress) && $shipaddress!=''){
				 echo 'SAR '.number_format(($basetotal+$vatamount-$couponamount),2);
				 } ?>
      </div></div>
      <?php } ?>
            </div>
            <!-- box -->
          </div>
        </div>
        <!--global end-->
      </div>
    </div>
  </div>
  <!--end right panel-->
</div>
<!--end merachant-->
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $dressesbig_leftmenu =1; @endphp
<div class="merchant_vendor">
@include('sitemerchant.includes.breadcrumb')   @include('sitemerchant.includes.left')
<div class="right_panel">
 <div class="inner">
      <header>
        @if(request()->autoid =='')
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_CATEGORY') }} @endif</h5>
          @else
           <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPDATE_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPDATE_CATEGORY') }} @endif</h5>
          @endif
      </header>
      @include('sitemerchant.includes.language')
      
       <!-- Display Message after submition --> 
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif 
            <!-- Display Message after submition -->
      <div class="row">
        <div class="col-lg-12">
          <div class="box">             

              <form name="form1" id="menucategory" method="post" action="{{ route('store-dresses-category') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">
                    <span class="english">{{ lang::get('mer_en_lang.MER_CATEGORY_NAME') }}</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); @endphp </span>
 </label>
                  <div class="info100">
                   <div class="english"> 
                    <input type="text" class="english" name="category_name" maxlength="90" value="{{ $datafirst->attribute_title or ''}}" id="category_name" required="" >
                     </div>
                    <div class="arabic">
                    <input type="text" class="arabic ar" name="category_name_ar" maxlength="90" value="{{ $datafirst->attribute_title_ar or ''}}" id="category_name_ar" required="" >
                  </div>
                  </div>
                  </div>
          		</div> <!-- form_row -->


 <div class="form_row_right common_field">
                    <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_IMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_IMG'); @endphp </span>  </label>
                    <div class="input-file-area">
                      <label for="company_logo7">
                      <div class="file-btn-area">
                        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        <div class="file-value" id="file_value6"></div>
                      </div>
                      </label>
                      <input type="file" name="image" id="company_logo7" class="info-file">
                    </div>
                 
                    @if(isset($datafirst->image) && $datafirst->image !='')<div class="form-upload-img"> <img src="{{ $datafirst->image}}" width="150" height="150"></div> @endif
                  </div>




                <div class="form_row">
                <div class="form_row_left">
                <div class="english">
                  <input type="submit" name="submit" value="Submit">
                  <input type="hidden" name="parent_id" value="{{ $id or '' }}">
                  <input type="hidden" name="sid" value="{{ $sid or ''}}">
                  <input type="hidden" name="autoid" value="{{ $autoid or ''}}">
                

                </div>
       			<div class="arabic ar">
                  <input type="submit" name="submit" value="خضع">
             
                </div>
</div></div>

              </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             category_name: {
           required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_CATEGORY_NAME'); @endphp",
                      },  
 
                 category_name_ar: {
               required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_CAT_ARABIC_AR'); @endphp",
                      },    
                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

 @if($mer_selected_lang_code !='en')

                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }                   
                  
                    if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
                  if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }
                  if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }  

@endif




                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
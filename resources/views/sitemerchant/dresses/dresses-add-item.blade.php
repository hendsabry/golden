@include('sitemerchant.includes.header') 

@php $dressesbig_leftmenu =1; @endphp

<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')

      <style type="text/css">

      .ms-drop ul li label input[type="checkbox"] {

      height: 20px;

      display: inline-block;

      width: 20px;

      position: relative;

      border: 1px solid red;

      opacity: 1;

      }

      </style>

      <link rel="stylesheet" href="{{ url('')}}/themes/css/multiple-select.css" />

  <div class="right_panel">

    <div class="inner">

      <header>

         @if(request()->autoid=='')

        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADD_DRESSES')!= '') {{  trans(Session::get('mer_lang_file').'.ADD_DRESSES') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.ADD_DRESSES') }} @endif </h5>

          @else

          <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATE_DRESSES')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATE_DRESSES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATE_DRESSES') }} @endif </h5>



          



          @endif

        @include('sitemerchant.includes.language') </header>

      <div class="global_area">

        <div class="row">

          <div class="col-lg-12">

            <div class="box"> 

              

              <!-- Display Message after submition --> 

              @if (Session::has('message'))

              <div class="alert alert-info">{{ Session::get('message') }}</div>

              @endif 

              <!-- Display Message after submition -->

            

                <form name="form1" id="addmanager" method="post" action="{{ route('store-dressesitem') }}" enctype="multipart/form-data">

                  {{ csrf_field() }}

                 <!-- form_row -->

                    <div class="form_row common_field"> 

             

              </div>



                 <div class="form_row   common_field">

              <div class="form_row_left">

                    <label class="form_label">

                      <span class="englicsh">

                      @if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif



                     </span>

                    </label>

              

              <div class="info100">

              <select class="small-sel common_field" name="category1" id="category1">

                  <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_CATEGORY') }} @endif</option>

                    @php $menu_name='attribute_title'@endphp

                   @if($mer_selected_lang_code !='en')

                   @php $menu_name= 'attribute_title_'.$mer_selected_lang_code; @endphp

                    @endif

                     @foreach($category as $val)

                  <option value="{{ $val->id }}" {{ isset($fetchfirstdata->attribute_id) && $fetchfirstdata->attribute_id ==$val->id ? 'selected' : ''}}>{{ $val->$menu_name }}</option>

                  @endforeach  

              </select>

              </div>

              </div>

              </div>

                  

                  <div class="form_row">

                  <div class="form_row_left">

                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.mer_category_name'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.mer_category_name'); @endphp </span> </label>

                  <div class="info100">

                    <div class="english">

                      <input type="text" name="name" id="name" value="{{ $fetchfirstdata->pro_title or '' }}"  class="english"  data-validation-length="max80" maxlength="140">

                    </div>

                    <div class="arabic ar">

                      <input id="ssb_name_ar" class="arabic ar" name="name_ar" id="name_ar" type="text" value="{{ $fetchfirstdata->pro_title_ar or '' }}"   maxlength="140" >

                    </div>

                  </div>

                  </div>

				   <div class="form_row_right common_field">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>

                    <div class="info100">

                      <div class="input-file-area">

                        <label for="company_logo">

                        <div class="file-btn-area">

                          <div id="file_value1" class="file-value"></div>

                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>

                        </div>

                        </label>

                        <input id="company_logo" name="stor_img" class="info-file" type="file" value="">

                      </div>

                      

                      @if(isset($fetchfirstdata->pro_Img) && $fetchfirstdata->pro_Img!='')

                      <span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>

                      <div class="form-upload-img">

                       <img src="{{ $fetchfirstdata->pro_Img or '' }}" >

                     </div>

                     @endif

                    </div>

					</div>

					 

					 

<!-- Add More product images start -->

        @php

        $GalleryCunt = $productGallery->count();

        $J=2; $k=3;

        @endphp



        @for($i=0;$i < $GalleryCunt;$i++)

        <div class="form_row_right common_field">

        <div class="info100">

        <div class="input-file-area">

        <label for="company_logo{{$k}}">

        <div class="file-btn-area">

        <div id="file_value{{$J}}" class="file-value"></div>

        <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>

        </div>

        </label>

        <input id="company_logo{{$k}}" name="image[]" class="info-file proImgess" data-lbl="file_value{{$J}}" type="file" value="">

        </div>

        @if(isset($productGallery[$i]->image) && $productGallery[$i]->image!='')

        <div class="form-upload-img product_img_del">

        <img src="{{ $productGallery[$i]->image or '' }}" >

        <div class="imgpics-delet">

        <a data-status="Active" data-id="{{$productGallery[$i]->id}}" title="Remove Field" class="delet-icon status_active2 cstatus">&nbsp;</a>



        </div>

        </div>

        <span class="error file_value{{$J}}"></span>  

        @endif

        <input type="hidden" name="privius[]" value="{{$productGallery[$i]->id}}">

        </div>



        </div>

        @php  $J=$J + 1; $k=$k +1; @endphp

        @endfor

 

        <div id="img_upload" class="common_field"></div>

        <div class="img-upload-line common_field">

        <div id="add_button"><a href="javascript:void(0);" class="form-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SERVICE_ADDMORE') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SERVICE_ADDMORE') }} @endif</a></div>

        <span class="error pictureformat"></span>

        @php $Count =  1; $GalleryCunt = $productGallery->count(); if($GalleryCunt >=1){ $Count = $GalleryCunt+1;} @endphp                

        <input type="hidden" id="count" name="count" value="{{$Count}}">

        </div>

  <!-- Add More product images end -->





                  </div>

                  <!-- form_row -->

                  <div class="form_row">

				   <div class="form_row_left common_field">

                    <label class="form_label"> 

                    	<span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> 

                     </label>

                    <div class="info100">

                      <div class="englisch">

                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="price" id="price" maxlength="15" value="{{ $fetchfirstdata->pro_price or '' }}"  required="" >

                      </div>

                    </div>

					</div>

					<!--   <div class="form_row_right common_field"> 



        <label class="form_label">

        <span class="english">@php echo lang::get('mer_en_lang.SIZE'); @endphp</span>

        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SIZE'); @endphp </span>

        </label>

        <div class="info100">  

      

        <select id="ms_size" class="multicheck xs_small" multiple="multiple" name="size[]"> 

            

        @foreach($Size as $sizes)

        @php

        $p = $sizes->name;

        @endphp

        <option value="{{$sizes->name or ''}}" @if (in_array($p, $CheckSize)) {{'selected'}} @endif>

              @if($mer_selected_lang_code =='en')

                        {{$sizes->name or ''}}

                      @else

               {{$sizes->name_ar or ''}}

                    @endif

      </option>

                      @endforeach

        </select>



<span for="ms_size" generated="true" class="error"></span>

        </div>

        </div> -->



             <div class="form_row common_field">

                  <div class="form_row_left">

                  <label class="form_label dress_qunt">

                   <span class="english">@php echo lang::get('mer_en_lang.SIZE'); @endphp</span>

                 <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SIZE'); @endphp </span>

                    </label>

                     <label class="form_label dress_qunt"> 

                      <span class="english">@php echo lang::get('mer_en_lang.MER_QUANTITY'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_QUANTITY'); @endphp </span> 

                     </label>

                  <div class="info100">

                    @php $containername='title'@endphp

                   @if($mer_selected_lang_code !='en')

                    @php $containername= 'title_'.$mer_selected_lang_code; @endphp

                    @endif

                       

                       @foreach($Size as $key=> $val)

                       @php

                       if(isset($fetchfirstdata->pro_id) && $fetchfirstdata->pro_id!='') {

                       $quantityinstock=Helper::getquantity($val->name,$fetchfirstdata->pro_id);

 

                      $names = 'Size'.$val->name;

                      $quantityinstock=Helper::getquantity($names,$fetchfirstdata->pro_id);

                      if($val->name == '0' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_0  = $quantityinstock->value;

                      }



                      if($val->name == '0' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_0  = $quantityinstock->value_ar;

                      }



                      if($val->name == '2' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_2  = $quantityinstock->value;

                      }



                      if($val->name == '2' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_2  = $quantityinstock->value_ar;

                      }



                      if($val->name == '4' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_4  = $quantityinstock->value;

                      }



                      if($val->name == '4' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_4  = $quantityinstock->value_ar;

                      }



                      if($val->name == '6' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_6  = $quantityinstock->value;

                      }



                      if($val->name == '6' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_6  = $quantityinstock->value_ar;

                      }



                      if($val->name == '8' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_8  = $quantityinstock->value;

                      }



                      if($val->name == '8' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_8  = $quantityinstock->value_ar;

                      }



                      if($val->name == '10' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_10  = $quantityinstock->value;

                      }



                       if($val->name == '10' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_10  = $quantityinstock->value_ar;

                      }



                      if($val->name == '12' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_12  = $quantityinstock->value;

                      }



                      if($val->name == '12' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_12  = $quantityinstock->value_ar;

                      }



                      if($val->name == '14' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_14  = $quantityinstock->value;

                      }



                      if($val->name == '14' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_14  = $quantityinstock->value_ar;

                      }



                      if($val->name == '16' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_16  = $quantityinstock->value;

                      }



                      if($val->name == '16' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_16  = $quantityinstock->value_ar;

                      }



                      if($val->name == '18' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_18  = $quantityinstock->value;

                      }



                      if($val->name == '18' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_18  = $quantityinstock->value_ar;

                      }



                      if($val->name == '20' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_20  = $quantityinstock->value;

                      }



                      if($val->name == '20' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_20  = $quantityinstock->value_ar;

                      }



                      if($val->name == '22' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_22  = $quantityinstock->value;

                      }



                      if($val->name == '22' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_22  = $quantityinstock->value_ar;

                      }



                      if($val->name == '24' && isset($quantityinstock->value) && $quantityinstock->value!='')

                      {   

                      $quantityinstock_24  = $quantityinstock->value;

                      }

                

                      if($val->name == '24' && isset($quantityinstock->value_ar) && $quantityinstock->value_ar!='')

                      {   

                      $rentquantityinstock_24  = $quantityinstock->value_ar;

                      } 

                       }

                       @endphp

                   

                      <!--div class="container_row">

                       

                        <div class="label_fl">{{$val->name or ''}}</div>



                        <div class="input_flr"><input type="text" value="{{ $quantityinstock->value or '' }}" onkeypress="return isNumber(event)" class="check notzero" name="mulquantity[]"  maxlength="40"></div>

                         <input type="hidden" name="size[]" value="{{ $val->name }}">

                        

                      </div-->

                       @endforeach

                     

				    <div class="size-chart-table"><div class="table">

<div class="tr">

<div class="table_heading">US</div>

<div class="table_heading">UK</div>

<div class="table_heading">Germany</div>

<div class="table_heading">France</div>

<div class="table_heading">Italy</div>

<div class="table_heading">Kore</div>

<div class="table_heading">Qty</div>

<div class="table_heading">Rent Qty</div>

</div> <!-- tr -->



<div class="tr">

<div class="td td1" data-title="US">0</div>

<div class="td td2" data-title="UK">4</div>                     

<div class="td td3" data-title="Germany">30</div>

<div class="td td4" data-title="France">32</div>   

<div class="td td5" data-title="Italy">36</div>   

<div class="td td6" data-title="Kore">44</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$quantityinstock_0 or ''}}" class="check notzero" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size0"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_0 or ''}}" class="check notzero" name="rentquantity[]"/></div>  

 </div> <!-- tr -->



<div class="tr">

<div class="td td1" data-title="US">2</div>

<div class="td td2" data-title="UK">6</div>                     

<div class="td td3" data-title="Germany">32</div>

<div class="td td4" data-title="France">34</div>   

<div class="td td5" data-title="Italy">38</div>   

<div class="td td6" data-title="Kore">44</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero" name="mulquantity[]" value="{{$quantityinstock_2 or ''}}" /><input type="hidden" name="size[]" value="Size2"></div>

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_2 or ''}}" class="check notzero" name="rentquantity[]"/></div>    

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">4</div>

<div class="td td2" data-title="UK">8</div>                     

<div class="td td3" data-title="Germany">34</div>

<div class="td td4" data-title="France">36</div>   

<div class="td td5" data-title="Italy">40</div>   

<div class="td td6" data-title="Kore">55</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero" name="mulquantity[]" value="{{$quantityinstock_4 or ''}}"/><input type="hidden" name="size[]" value="Size4"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_4 or ''}}" class="check notzero" name="rentquantity[]"/></div>   

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">6</div>

<div class="td td2" data-title="UK">10</div>                     

<div class="td td3" data-title="Germany">36</div>

<div class="td td4" data-title="France">38</div>   

<div class="td td5" data-title="Italy">42</div>   

<div class="td td6" data-title="Kore">55</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_6 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size6"></div>  

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_6 or ''}}" class="check notzero" name="rentquantity[]"/></div>  

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">8</div>

<div class="td td2" data-title="UK">12</div>                     

<div class="td td3" data-title="Germany">38</div>

<div class="td td4" data-title="France">40</div>   

<div class="td td5" data-title="Italy">44</div>   

<div class="td td6" data-title="Kore">66</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_8 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size8"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_8 or ''}}" class="check notzero" name="rentquantity[]"/></div>   

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">10</div>

<div class="td td2" data-title="UK">14</div>                     

<div class="td td3" data-title="Germany">40</div>

<div class="td td4" data-title="France">42</div>   

<div class="td td5" data-title="Italy">46</div>   

<div class="td td6" data-title="Kore">66</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_10 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size10"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_10 or ''}}" class="check notzero" name="rentquantity[]"/></div>   

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">12</div>

<div class="td td2" data-title="UK">16</div>                     

<div class="td td3" data-title="Germany">42</div>

<div class="td td4" data-title="France">44</div>   

<div class="td td5" data-title="Italy">48</div>   

<div class="td td6" data-title="Kore">77</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_12 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size12"></div>  

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_12 or ''}}" class="check notzero" name="rentquantity[]"/></div>  

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">14</div>

<div class="td td2" data-title="UK">18</div>                     

<div class="td td3" data-title="Germany">44</div>

<div class="td td4" data-title="France">46</div>   

<div class="td td5" data-title="Italy">50</div>   

<div class="td td6" data-title="Kore">77</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_14 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size14"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_14 or ''}}" class="check notzero" name="rentquantity[]"/></div>   

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">16</div>

<div class="td td2" data-title="UK">20</div>                     

<div class="td td3" data-title="Germany">46</div>

<div class="td td4" data-title="France">48</div>   

<div class="td td5" data-title="Italy">52</div>   

<div class="td td6" data-title="Kore">88</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_16 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size16"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_16 or ''}}" class="check notzero" name="rentquantity[]"/></div>   

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">18</div>

<div class="td td2" data-title="UK">22</div>                     

<div class="td td3" data-title="Germany">48</div>

<div class="td td4" data-title="France">50</div>   

<div class="td td5" data-title="Italy">54</div>   

<div class="td td6" data-title="Kore">88</div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_18 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size18"></div>  

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_18 or ''}}" class="check notzero" name="rentquantity[]"/></div>  

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">20</div>

<div class="td td2" data-title="UK">24</div>                     

<div class="td td3" data-title="Germany">50</div>

<div class="td td4" data-title="France">52</div>   

<div class="td td5" data-title="Italy">56</div>   

<div class="td td6" data-title="Kore"></div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_20 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size20"></div>

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_20 or ''}}" class="check notzero" name="rentquantity[]"/></div>    

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">22</div>

<div class="td td2" data-title="UK">26</div>                     

<div class="td td3" data-title="Germany">52</div>

<div class="td td4" data-title="France">54</div>   

<div class="td td5" data-title="Italy">58</div>   

<div class="td td6" data-title="Kore"></div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_22 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size22"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_22 or ''}}" class="check notzero" name="rentquantity[]"/></div>   

 </div> <!-- tr -->

 

<div class="tr">

<div class="td td1" data-title="US">24</div>

<div class="td td2" data-title="UK">28</div>                     

<div class="td td3" data-title="Germany">54</div>

<div class="td td4" data-title="France">56</div>   

<div class="td td5" data-title="Italy">60</div>   

<div class="td td6" data-title="Kore"></div>   

<div class="td td7" data-title="Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" class="check notzero"  value="{{$quantityinstock_24 or ''}}" name="mulquantity[]"/><input type="hidden" name="size[]" value="Size24"></div> 

<div class="td td7" data-title="rent Qty"><input type="text"  onkeypress="return isNumber(event)" maxlength="4" value="{{$rentquantityinstock_24 or ''}}" class="check notzero" name="rentquantity[]"/></div>   

 </div> <!-- tr -->

           

           </div> <!-- table -->

           </div> <!-- size-chart-table -->

                    </div>

                   </div>                    

                </div>







                    <!-- form_row --> 

                  </div>

                 

                  <!--  <div class="form_row">

                  <div class="form_row_left common_field">

                    <label class="form_label"> 

                      <span class="english">@php echo lang::get('mer_en_lang.Quantity_For_Sale'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Quantity_For_Sale'); @endphp </span> 

                     </label>

                    <div class="info100">

                      <div class="englisch">

                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity" id="quantity" maxlength="15" value="{{ $fetchfirstdata->pro_qty or '' }}"  required="" >

                      </div>

                    </div>

          </div>

           

                    

                  </div> -->





                    <div class="form_row_left common_field">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Discount_For_Sale'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Discount_For_Sale'); @endphp  %</span> </label>

                    <div class="info100">

                      <div class="englissh">

                        <input type="text" class="xs_small notzero" name="discount_for_sale" onkeypress="return isNumber(event)" maxlength="7" value="{{$fetchfirstdata->pro_discount_percentage or ''}}" id="discount" >

                      </div>

                    </div>

                    <!-- form_row -->

                  </div>







				  

   <div class="form_row">

           <div class="form_row_left common_field">

                    <label class="form_label"> 

                      <span class="english">@php echo lang::get('mer_en_lang.Avilable_on_rent'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Avilable_on_rent'); @endphp </span> 

                     </label>

                    <div class="info100">

                      <div class="englisch">  

<div class="yesno-radio">

                        <input onclick="show1();" type="radio" class="notzero" name="avilable_on_rent" id="avilable_on_rent_yes" @php if(isset($getRentP->price) && $getRentP->price !='') echo 'checked'; @endphp value="1" > <label for="avilable_on_rent_yes">@if(Lang::has(Session::get('mer_lang_file').'.Yes')!= '') {{  trans(Session::get('mer_lang_file').'.Yes') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.Yes') }} @endif</label>

</div>

<div class="yesno-radio">

                        <input onclick="show2();" type="radio" class="notzero" name="avilable_on_rent"  @php if(count($getRentP) < 1 && request()->autoid !='') echo 'checked';  @endphp id="avilable_on_rent_no" value="0" > <label for="avilable_on_rent_no">@if (Lang::has(Session::get('mer_lang_file').'.No')!= '') {{  trans(Session::get('mer_lang_file').'.No') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.No') }} @endif</label>

</div>

                      </div>

                    </div>

          </div>

				  </div>



 <div class="form_row" id="rentprice" <?php if(isset($getRentP->price) && $getRentP->price !=''){?>  style="display: none;"; <?php } ?>>

           <div class="form_row_left common_field">

                    <label class="form_label"> 

                      <span class="english">@php echo lang::get('mer_en_lang.Rent_price'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Rent_price'); @endphp </span> 

                     </label>

                    <div class="info100">

                      <div class="englisch">

                       <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="rent_price" id="rent_price" maxlength="15" value="{{ $getRentP->price or '' }}" >



                      </div>

                    </div>







          </div>

           <div class="form_row_left common_field">

                    <label class="form_label"> 

                      <span class="english">@php echo lang::get('mer_en_lang.Insuranceamount'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Insuranceamount'); @endphp </span> 

                     </label>

                    <div class="info100">

                      <div class="englisch">

                       <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="insuranceamount" id="insuranceamount" maxlength="15" value="{{ $fetchfirstdata->Insuranceamount or '' }}" >



                      </div>

                    </div>

          </div>



           <!--  <div class="form_row">

                  <div class="form_row_left common_field">

                    <label class="form_label"> 

                      <span class="english">@php echo lang::get('mer_en_lang.Quantity_For_Rent'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Quantity_For_Rent'); @endphp </span> 

                     </label>

                    <div class="info100">

                      <div class="englisch">

                        <input type="text" class="xs_small notzero" onkeypress="return isNumber(event)" name="quantity_for_rent" id="quantity_for_rent" maxlength="15" value="{{ $getQuantity->value or '' }}" >

                      </div>

                    </div>

          </div>

           

                   

                  </div> -->



                    <div class="form_row_left common_field">

                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.Discount_For_Rent'); @endphp %</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Discount_For_Rent'); @endphp  %</span> </label>

                    <div class="info100">

                      <div class="englissh">

                        <input type="text" class="xs_small notzero" name="discount_for_rent" onkeypress="return isNumber(event)" maxlength="7" value="{{$getDiscount->discount or ''}}" id="discount" >

                      </div>

                    </div>

                    <!-- form_row -->

                  </div>



                  



                   <div class="form_row">

                  <div class="form_row_left common_field">

                    <label class="form_label"> 

                      <span class="english">@php echo lang::get('mer_en_lang.PRODUCT_WEIGHT'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PRODUCT_WEIGHT'); @endphp </span> 

                     </label>

                    <div class="info100">

                      <div class="englisch">

                        <input type="text" class="xs_small notzero" onkeypress="return isNumberKey(event,this)" name="pro_weight" id="pro_weight" maxlength="15" value="{{ $fetchfirstdata->pro_weight or '' }}"  required="" >

                      </div>

                    </div>

                  </div>

           

                    <!-- form_row --> 

                  </div>



          </div>



 

				  <div class="form_row">

				  <div class="form_row_left">

                    <label class="form_label"> 

                    	<span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> 

                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> 

                     </label>

                    <div class="info100">

                      

                      <div class="english">

                        <textarea name="description" class="english" maxlength="500" id="description" rows="4" cols="50">{{ $fetchfirstdata->pro_desc or ''  }} </textarea> 

                      </div>      

                      

                      <div  class="arabic ar" >     

                              <textarea class="arabic ar" maxlength="500" name="description_ar" id="description_ar" rows="4" cols="50">{{ $fetchfirstdata->pro_desc_ar or ''  }}</textarea>  

                      </div> 

                      

                    </div>

                    <!-- form_row --> 

					</div>

					

                  </div>

                  

                  



                  <div class="form_row">

                  <div class="form_row_left">

                  <div class="english">

                  <input type="hidden" name="sid" value="{{ $sid }}"> 

                   <input type="hidden" name="parentid" value="{{ $id }}">

                   <input type="hidden" name="autoid" id="autoid" value="{{ $autoid or '' }}">

                    <input type="submit" name="submit" value="Submit">

                  </div>

                  <!-- form_row -->

                  <div class="arabic ar">

                    <input type="submit" name="submit" value="خضع">

                  </div>

                  <!-- form_row -->

                  </div></div>

                </form>

          

              <!-- one-call-form --> 

              

              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 

              

              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 

              

            </div>

          </div>

        </div>

      </div>

      <!-- global_area --> 

    </div>

  </div>

  <!-- right_panel --> 

</div>

<!-- merchant_vendor --> 

  

<script>

       

            $("form").data("validator").settings.ignore = "";

 </script> 

<script type="text/javascript">

  

$("#addmanager").validate({





              ignore: [],

               rules: {

                 

                  category1: {

                       required: true,

                      },

                   

                    



                    name: {

                       required: true,

                      },



                       name_ar: {

                       required: true,

                      },



                      price: {

                       required: true,

                      },



                     description: {

                       required: true,

                      },

                       description_ar: {

                       required: true,

                      },

                      "size[]": {

                       required: true,

                      },



                       @if(isset($fetchfirstdata->pro_Img)!='')  

                          stor_img: {

                           required: false,

                           accept:"png|jpe?g|gif",

                      },

                      @else

                       stor_img: {

                           required: true,

                           accept:"png|jpe?g|gif",

                      },

                       

                      @endif



                  },

                 highlight: function(element) {

            $(element).removeClass('error');

                },

                

           messages: {

             

           

               category1: {

               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_CATEGORY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_CATEGORY') }} @endif",

                      },         



                "size[]": {

               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SIZE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_PLEASE_SELECT_SIZE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PLEASE_SELECT_SIZE') }} @endif",

                      }, 



             name: {

               required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_NAME'); @endphp",

                      },  



                 name_ar: {

               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_NAME_AR'); @endphp",

                      },



                       price: {

               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif ",

                      },

                   quantity: {

               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRODUCTQUANTITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRODUCTQUANTITY') }} @endif ",

                      },

                 





                   description: {

            



                required: "@php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",

                      },



                       description_ar: {

              

               required: "@php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_SINGER'); @endphp",

                      },

        

                 

                  stor_img: {

                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",

                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",



                 }, 

                       

                    

                },             

                   invalidHandler: function(e, validation){

                    console.log("invalidHandler : event", e);

                    console.log("invalidHandler : validation", validation);

                    console.log("invalidHandler : validation", validation);

                    var valdata=validation.invalid;

                     console.log("invalidHandler : validation", valdata);



 

                    @if($mer_selected_lang_code !='en')



                    if (typeof valdata.name != "undefined" || valdata.name != null) 

                    {

                    $('.english_tab').trigger('click'); 

                    }

                   

                    

                     if (typeof valdata.description != "undefined" || valdata.description != null) 

                    {

                    $('.english_tab').trigger('click');

                    }

                   if (typeof valdata.category1 != "undefined" || valdata.category1 != null) 

                    {

                    $('.arabic_tab').trigger('click');

                    }

                   

                    if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 

                    {

                    $('.arabic_tab').trigger('click');     

                    }

                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 

                    {

                    $('.arabic_tab').trigger('click');

                    }

                  if (typeof valdata.price != "undefined" || valdata.price != null) 

                    {

                    $('.arabic_tab').trigger('click');

                    }









                    if (typeof valdata.quantity != "undefined" || valdata.quantity != null) 

                    {

                    $('.arabic_tab').trigger('click');

                    }



                   

                    

                      

                   @else



                    if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }

                  

                     if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 

                    {

                      

                        $('.arabic_tab').trigger('click');     

                       



                    }

                  if (typeof valdata.name != "undefined" || valdata.name != null) 

                    {

                        $('.english_tab').trigger('click'); 

                      

                    }

if (typeof valdata.quantity != "undefined" || valdata.quantity != null) 

                    {

                    $('.english_tab').trigger('click');

                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 

                    {

                        $('.english_tab').trigger('click'); 

                      

                    }



                     if (typeof valdata.description != "undefined" || valdata.description != null) 

                    {

                        $('.english_tab').trigger('click'); 

                      

                    }

                     

                      

                     

                     if (typeof valdata.category1 != "undefined" || valdata.category1 != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }

                     

                      if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 

                    {



                        $('.english_tab').trigger('click');

                        



                    }





                    

@endif



                    },



                submitHandler: function(form) {

                    form.submit();

                }

            });

  $("#company_logo1").change(function () {

        var fileExtension = ['pdf'];

        $(".certifications").html('');

        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {

   $("#file_value2").html('');         

$(".certifications").html('Only PDF format allowed.');



         }

    });



  $(document).ready(function(){



   $('#rentprice').hide();









    var autoid = $('#autoid').val();

    var catid = $('#category').val();

     if(catid=='2' || autoid=='') {

    $('.displaycat').hide();

    

     } else {

        $('.displaycat').show();

         $('#rentprice').show();



     }

    

    });

  function chceckCategory()

  {

     var catid =  $('#category').val()

     



     if(catid==1) {

          $('.displaycat').show();



          $('#category1').attr('required','true');

     } else {



            $('.displaycat').hide();

            $('#category1').removeAttr('required');

     }





  }









/* Mobile Number Validation */

   function isNumber(evt) {

   evt = (evt) ? evt : window.event;

   var charCode = (evt.which) ? evt.which : evt.keyCode;

   if (charCode > 31 && (charCode < 48 || charCode > 57)) {

   return false;

   }

   return true;

   }

 





 function isNumberKey(evt, obj) {



            var charCode = (evt.which) ? evt.which : event.keyCode

            var value = obj.value;

            var dotcontains = value.indexOf(".") != -1;

            if (dotcontains)

                if (charCode == 46) return false;

            if (charCode == 46) return true;

            if (charCode > 31 && (charCode < 48 || charCode > 57))

                return false;

            return true;

        }

</script> 



<script src="{{ url('')}}/themes/js/jquery.min.js"></script> 

<script src="{{ url('')}}/themes/js/multiple-select.js"></script> 

  @if($mer_selected_lang_code !='en')



<script>

    $(function() {

        $('#ms_size').change(function() {

           // console.log($(this).val());

        }).multipleSelect({

            width: '100%',

            placeholder: "تحديد"

        });

      });

</script>

 @else 

<script>

    $(function() {

        $('#ms_size').change(function() {

           // console.log($(this).val());

        }).multipleSelect({

            width: '100%',

            placeholder: "Select"

        });

     });

</script> 

@endif 

<script type="text/javascript">

  

function show1(){

  document.getElementById('rentprice').style.display ='block';

}

function show2(){

  document.getElementById('rentprice').style.display = 'none';

}



</script>







<!-- Add More product images start -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

 <script type="text/javascript">

    $(document).ready(function(){

    var maxField = 10;  

    var addButton = $('#add_button');  

    var wrapper = $('#img_upload');

    var x = <?php echo $Count;?>;

    var y = x+1;  

    $(addButton).click(function(){  

    if(x < maxField){  

    x++; y++; 

    var fieldHTML = '<div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value'+x+'" class="file-value"></div><div class="file-btn">@if (Lang::has(Session::get('mer_lang_file').'.MER_UPLOAD')!= '') {{  trans(Session::get('mer_lang_file').'.MER_UPLOAD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_UPLOAD') }} @endif</div></div></label><input class="info-file proImgess" id="company_logo'+y+'" data-lbl="file_value'+x+'" type="file" name="image[]" data-info='+x+'><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span> </div> ';

    $(wrapper).append(fieldHTML);  

    document.getElementById('count').value = parseInt(x);

    }

    });

    $(wrapper).on('click', '#remove_button', function(e){  

    e.preventDefault();

    $(this).parent('div').remove(); 

    x--;   y--; 

    document.getElementById('count').value = parseInt(x);

    });

    });



$('body').on('change',".proImgess",function(){

    var fileExtension = ['png','jpeg','jpg','gif'];        

        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {

         var Dl =  $(this).data('lbl');

         @if(Session::get('lang_file') =='ar_lang')

        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');

        @else

        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');

        @endif

        $(this).val('');

         $('.file-value').html('');

         }



});

</script> 



 

<div class="overlay_popup"></div>

<div class="action_popup">

  <div class="action_active_popup">

    

    <div class="action_content"></div>

    <div class="action_btnrow"><a class="action_no" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_NO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_NO') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_NO') }} @endif</a> <a class="action_yes status_yes" data-id="" data-checkstatus="" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.MER_YES')!= '') {{  trans(Session::get('mer_lang_file').'.MER_YES') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_YES') }} @endif</a>  </div>

  </div>

</div>

<!-- action_popup --> 

<script type="text/javascript">

/* Action Popup */

jQuery('.cstatus').click(function(){  

 var status =jQuery(this).data("status");

 var id =jQuery(this).data("id");



 jQuery('.action_yes').attr('data-id',id).attr('data-checkstatus',status);

 

@if($mer_selected_lang_code !='en')

jQuery('.action_content').html('هل تريد حذف هذا السجل؟');

@else

jQuery('.action_content').html('Do you want to delete this record?');

@endif



jQuery('.action_popup').fadeIn(500);  

jQuery('.overlay_popup').fadeIn(500);



jQuery('.status_yes').click(function(){

   var id =jQuery(this).attr("data-id");

    var status1 =jQuery(this).attr("data-checkstatus");

     if(status1=='Active'){

      var activestatus = 1;

      

     }else {

         var activestatus = 0;



     } 

var products = 1;

     jQuery.ajax({

        type: "GET",

        url: "{{ route('delete-shop-image') }}",

        data: {id:id,products:products},

        success: function(data) {

         

            if(data==1){

               location.reload();

            }

            

            // $('.main-pro-bg').html(data);

            //console.log(data);

        }

    });

    

})

});

</script>



<!-- Add More product images end -->





@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 

@php $dressesbig_leftmenu =1; @endphp

  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />

  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />

<div class="merchant_vendor">

@include('sitemerchant.includes.breadcrumb')  @include('sitemerchant.includes.left')

<div class="right_panel">

  <div class="inner">

   <div class="service_listingrow"> 

        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_Order')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Order') }}  

          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Order') }} @endif </h5>

    </div>



@php

$id = request()->id;

$sid = request()->sid;

@endphp



{!! Form::open(array('url'=>"dresses-order/{$id}/{$sid}",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter')) !!}







 <a href="javascript:void(0);" class="filter_mobile">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</a>

      <div class="filter_area">

      @if(Session::has('message'))

         <div class="alert alert-info">{{ Session::get('message') }}</div>

        @endif 

        <div class="search_filter">@if (Lang::has(Session::get('mer_lang_file').'.MER_FILTERS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_FILTERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_FILTERS') }} @endif</div>

           

      



    <div class="order-filter-line order-line">

    <div class="of-date-box"><input type="text" class="cal-t" value="{{request()->date_to}}" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.Start_Date')!= '') {{  trans(Session::get('mer_lang_file').'.Start_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Start_Date') }} @endif" name="date_to" autocomplete="off" id="date_to" /><input type="text" value="{{request()->from_to}}" class="cal-t" autocomplete="off" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.End_Date')!= '') {{  trans(Session::get('mer_lang_file').'.End_Date') }} @else  {{ trans($MER_OUR_LANGUAGE.'.End_Date') }} @endif" id="datepicker"  name="from_to"/></div><div class="of-orders">



      <!--<select name="order_days">

      <option value="">@if (Lang::has(Session::get('mer_lang_file').'.SELECT_ORDERS')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT_ORDERS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_ORDERS') }} @endif</option>

      <option value="10_days" @if(request()->order_days =='10_days') {{'selected'}} @endif >@if (Lang::has(Session::get('mer_lang_file').'.Last_10_Days')!= '') {{  trans(Session::get('mer_lang_file').'.Last_10_Days') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_10_Days') }} @endif </option>

      <option  value="1_month" @if(request()->order_days =='1_month') {{'selected'}} @endif>@if (Lang::has(Session::get('mer_lang_file').'.Last_1_Month')!= '') {{  trans(Session::get('mer_lang_file').'.Last_1_Month') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_1_Month') }} @endif </option> 

      <option  value="6_month" @if(request()->order_days =='6_month') {{'selected'}} @endif>@if (Lang::has(Session::get('mer_lang_file').'.Last_6_Months')!= '') {{  trans(Session::get('mer_lang_file').'.Last_6_Months') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_6_Months') }} @endif </option>

      <option  value="1_year" @if(request()->order_days =='1_year') {{'selected'}} @endif>@if (Lang::has(Session::get('mer_lang_file').'.Last_1_Years')!= '') {{  trans(Session::get('mer_lang_file').'.Last_1_Years') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Last_1_Years') }} @endif </option>

      </select>-->

      </div>

    <div class="search-box-field mems ">

            <select name="status" id="status" class="city_type">

              <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.MER_ORDER_STATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDER_STATUS') }}  

              @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDER_STATUS') }} @endif </option>

              <option value="1" @if(request()->status =='1') {{'selected'}} @endif> @if (Lang::has(Session::get('mer_lang_file').'.IN_PROCESS')!= '') {{  trans(Session::get('mer_lang_file').'.IN_PROCESS') }}@else  {{ trans($MER_OUR_LANGUAGE.'.IN_PROCESS') }} @endif</option>



              <option value="2" @if(request()->status =='2') {{'selected'}} @endif> @if (Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.COMPLETE') }}  

              @else  {{ trans($MER_OUR_LANGUAGE.'.COMPLETE') }} @endif</option>

            </select>

            <input type="hidden" id="serachfirstfrm" name="serachfirstfrm" value="1">

            <input name="" type="submit" value="@if(Lang::has(Session::get('mer_lang_file').'.MER_APPLY')!=''){{trans(Session::get('mer_lang_file').'.MER_APPLY')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_APPLY')}}@endif" class="applu_bts" />

          </div>

       <span id="todata" class="error"></span>

      {!! Form::close() !!}



       {!! Form::open(array('url'=>"dresses-order/{$id}/{$sid}",'class'=>'form-horizontal', 'accept-charset' => 'UTF-8','method'=>'get','name'=>'filter2')) !!}



      <div class="filter_right" style="width:100%;">  

         <input name="searchkeyword" type="text" placeholder="@if (Lang::has(Session::get('mer_lang_file').'.MER_SEARCH')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SEARCH') }}@else  {{ trans($MER_OUR_LANGUAGE.'.MER_SEARCH') }} @endif" value="{{ $searchkeyword }}" />

          <input type="button" class="icon_sch" id="submitdata"  onclick="submit();" />

        </div>

      {!! Form::close() !!}

    </div> <!-- order-filter-line -->

    

      </div>

      <!-- filter_area --> 

      

    

      <div class="global_area"><!--global start-->

        <div class="row">

          <div class="col-lg-12">

            <div class="table_wrap"> 

      <div class="panel-body panel panel-default">

      @if($getorderedproducts->count() < 1)

                <div class="no-record-area"> @if (Lang::has(Session::get('mer_lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('mer_lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($MER_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif </div>

                @else

      <div class="table merchant-order-table order_table">

      <div class="tr">

        <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Order_id')!= '') {{  trans(Session::get('mer_lang_file').'.Order_id') }}    @else  {{ trans($MER_OUR_LANGUAGE.'.Order_id') }} @endif</div>

        

                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif</div>

             

            <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.BACK_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_ORDER_DATE') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_ORDER_DATE') }} @endif</div>

                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= '') {{  trans(Session::get('mer_lang_file').'.PAYMENTREQUEST') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST') }} @endif</div>

                  

                    

                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.BACK_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_AMOUNT') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_AMOUNT') }} @endif</div>

					  <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= '') {{  trans(Session::get('mer_lang_file').'.Insuranceamount') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.Insuranceamount') }} @endif</div>

                    <div class="table_heading">@if (Lang::has(Session::get('mer_lang_file').'.MER_PAYMENTSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_PAYMENTSTATUS') }} @endif</div>

                    <div class="table_heading view_center">@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif</div>

                  </div>



                @php  $couponAmt=0; $couponcodeinfo=''; @endphp

           @foreach($getorderedproducts as $orderedproductbasicinfo)



                    @php $getCustomer = Helper::getuserinfo($orderedproductbasicinfo->cus_id); 

                        $isalreadymaderequest = Helper::ispaymentreuqest($orderedproductbasicinfo->order_id,$orderedproductbasicinfo->product_id);

                    

                        $ordertime=strtotime($orderedproductbasicinfo->created_at);

                       $orderedtime = date("d M Y",$ordertime);



                       $couponAmt     = $orderedproductbasicinfo->coupon_code_amount;

                         $couponcodeinfo=$orderedproductbasicinfo->coupon_code;



                    @endphp 

    



           <div class="tr">

   <div class="td td1" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Order_id')!= '') {{  trans(Session::get('mer_lang_file').'.Order_id') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.Order_id') }} @endif"> 

                    {{ $orderedproductbasicinfo->order_id }}</div>





                     <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL')!= '') {{  trans(Session::get('mer_lang_file').'.MER_CUSTOMERNAMEEMAIL') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_CUSTOMERNAMEEMAIL') }} @endif">{{ $getCustomer->cus_name or '' }}

                    {{ $getCustomer->email or '' }}</div>

           

            <div class="td td3" data-title="@if (Lang::has(Session::get('mer_lang_file').'.BACK_ORDER_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.BACK_ORDER_DATE') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.BACK_ORDER_DATE') }} @endif">{{ $orderedtime }}</div>

                     <div class="td td4" data-title="@if (Lang::has(Session::get('mer_lang_file').'.PAYMENTREQUEST')!= '') {{  trans(Session::get('mer_lang_file').'.PAYMENTREQUEST') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.PAYMENTREQUEST') }} @endif">



                      @php

                      $nam2=$orderedproductbasicinfo->sum-$couponAmt-$orderedproductbasicinfo->insurance_amou;

                      $vatamonu2 = Helper::calculatevat($orderedproductbasicinfo->order_id,$nam2);

                        $totalrefnetamount=$nam2 + $vatamonu2;

                      @endphp



                      @php 

                          if($orderedproductbasicinfo->status!=1){

                      if ($isalreadymaderequest>0){ @endphp @if (Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.COMPLETE') }}  

              @else  {{ trans($MER_OUR_LANGUAGE.'.COMPLETE') }} @endif  @php } else{ @endphp

                     <span id="paymentstatus"> <a href="#" onclick="paymentrequest({{$mer_id}},{{$orderedproductbasicinfo->product_id}},{{$orderedproductbasicinfo->order_id}},{{$totalrefnetamount}});">

                      @if (Lang::has(Session::get('mer_lang_file').'.REQUEST')!= '') {{  trans(Session::get('mer_lang_file').'.REQUEST') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.REQUEST') }} @endif

                    </a>

                      </span>

                      @php } }else{ @endphp

                      @if (Lang::has(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.WAIT_FOR_ORDER_COMPLETE') }}  

              @else  {{ trans($MER_OUR_LANGUAGE.'.WAIT_FOR_ORDER_COMPLETE') }} @endif

                    @php } @endphp

                  </div>

                     

                    

                     <div class="td td5" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Amount')!= '') {{  trans(Session::get('mer_lang_file').'.Amount') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.Amount') }} @endif">

                         @php

                         $nam=$orderedproductbasicinfo->sum-$couponAmt;

                      $vatamonu = Helper::calculatevat($orderedproductbasicinfo->order_id,$nam);

                        $totalnetamount=$nam + $vatamonu;

                      @endphp



                      {{ number_format((float)$totalnetamount, 2, '.', '') }}</div>

					  

					   <div class="td td2" data-title="@if (Lang::has(Session::get('mer_lang_file').'.Insuranceamount')!= '') {{  trans(Session::get('mer_lang_file').'.Insuranceamount') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.Insuranceamount') }} @endif">

					  <?php

					   if(isset($orderedproductbasicinfo->refund_amount) && ($orderedproductbasicinfo->refund_amount!='0' || $orderedproductbasicinfo->refund_amount!='0.00' || $orderedproductbasicinfo->refund_amount!=''))

					   { 

					     if(Lang::has(Session::get('mer_lang_file').'.MER_REQUEST_SENT')!= ''){ echo trans(Session::get('mer_lang_file').'.MER_REQUEST_SENT');}else{ echo trans($MER_OUR_LANGUAGE.'.MER_REQUEST_SENT');} 

						 

						 }else{

					     if(isset($orderedproductbasicinfo->insurance_amount) && ($orderedproductbasicinfo->insurance_amount!='0' || $orderedproductbasicinfo->insurance_amount!='0.00' || $orderedproductbasicinfo->insurance_amount!='')){echo number_format($orderedproductbasicinfo->insurance_amount,2);?><p><a class="dialog_link" rel="<?php echo $orderedproductbasicinfo->id.'-'.$orderedproductbasicinfo->insurance_amount; ?>" href="javascript:void(0);">@if (Lang::has(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN')!= '') {{  trans(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.REQUEST_FOR_AMT_RETURN') }} @endif</a></p><?php }else{ echo number_format($orderedproductbasicinfo->insurance_amount,2);}

					  }

					  ?>

					  </div>

                     

					 <div class="td td6" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ORDERSTATUS')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ORDERSTATUS') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ORDERSTATUS') }} @endif">

					  

                      @php if($orderedproductbasicinfo->status==1){ @endphp

                      <select name="ost" onchange="updateproductstatus('{{ $orderedproductbasicinfo->id }}',this.value,'{{ $orderedproductbasicinfo->order_id }}');">

                        <option value="1">@if (Lang::has(Session::get('mer_lang_file').'.IN_PROCESS')!= '') {{  trans(Session::get('mer_lang_file').'.IN_PROCESS') }}@else  {{ trans($MER_OUR_LANGUAGE.'.IN_PROCESS') }} @endif</option>

                        <option value="2"> @if (Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= '') {{  trans(Session::get('mer_lang_file').'.COMPLETE') }}  

              @else  {{ trans($MER_OUR_LANGUAGE.'.COMPLETE') }} @endif</option>

                      </select>

                        @php }else{ @endphp

                          @if (Lang::has(Session::get('mer_lang_file').'.COMPLETE')!= '') {{ trans(Session::get('mer_lang_file').'.COMPLETE') }}  

                          @else  {{ trans($MER_OUR_LANGUAGE.'.COMPLETE') }} @endif

                        @php } @endphp

                    </div>

                        <div class="td td7 view_center" data-title="@if (Lang::has(Session::get('mer_lang_file').'.MER_ACTION')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ACTION') }}  

                      @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ACTION') }} @endif"><a href="{{ route('dresse-orderdetails',['id'=>request()->id,'sid'=>request()->sid,'proid' =>$orderedproductbasicinfo->id,'cusid'=>$orderedproductbasicinfo->cus_id,'ordid'=>$orderedproductbasicinfo->order_id,'product_id'=> $orderedproductbasicinfo->product_id]) }}"><img src="{{url('')}}/public/assets/img/view-icon.png" title="@if (Lang::has(Session::get('mer_lang_file').'.mer_view_title')!= '') {{  trans(Session::get('mer_lang_file').'.mer_view_title') }} @else  {{ trans($MER_OUR_LANGUAGE.'.mer_view_title') }} @endif" alt="" /></a></div>

                     

                 </div>

@endforeach

      @endif  

      </div>

      

      </div>

      

      </div></div></div></div>

              

      <!-- global_area --> 

    </div>

  </div>

  <!-- right_panel --> 

</div>

<!-- merchant_vendor -->

	<div class="overlay_popup"></div>

<div id="dialog" title="@if (Lang::has(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN_INSUR')!= '') {{  trans(Session::get('mer_lang_file').'.REQUEST_FOR_AMT_RETURN_INSUR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.REQUEST_FOR_AMT_RETURN_INSUR') }} @endif" style="display:none;">

  <form name="frm_refund" id="frm_refund" action="{{ route('refund_insurance_amount_update') }}" method="post" enctype="multipart/form-data">

    {{ csrf_field() }}

  <input type="hidden" name="insuranceid" class="insuranceid"/>

  <input type="hidden" name="at_in" value="" class="at_in_id showdataid"/>

  <div class="dialog-row">

  <div class="dialog-label"> @if (Lang::has(Session::get('mer_lang_file').'.DEPOSITED_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.DEPOSITED_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.DEPOSITED_AMOUNT') }} @endif </div>

  <div class="dialog-field"><strong>SAR <span class="showdataid"></span></strong></div>

  </div>

    <div class="dialog-row">

  <div class="dialog-label">@if (Lang::has(Session::get('mer_lang_file').'.RETURN_AMOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.RETURN_AMOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.RETURN_AMOUNT') }} @endif</div>

  <div class="dialog-field">

  <input type="text" class="showdataid amt-t" name="refund_amount" id="refund_amount" required onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" onblur="return validAmount();"/>

  </div>

  <span class="showerror_msg"></span>

  </div>

    <div class="dialog-row">

  <div class="dialog-label">Note</div>

  <div class="dialog-field"><textarea name="note" id="note"/></textarea></div>

  </div>

  <div class="dialog-btn-row">

<input type="submit" class="buttonshowhide" name="refund" value="@if (Lang::has(Session::get('mer_lang_file').'.MER_REFUND')!= '') {{  trans(Session::get('mer_lang_file').'.MER_REFUND') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_REFUND') }} @endif"/>

  </div>



  </form>

</div>











<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

@include('sitemerchant.includes.footer')





<script>

/*

$(document).ready(function()

{

	$('.demo_pop').click(function(){

	$('.overlay_popup').hide();

});

});

*/

</script>



<script>

$('.dialog_link').click(function() 

{

    $('#dialog').css('display','block');

	$('.overlay_popup').css('display','block');

    var relval = $(this).attr('rel');

	var arraydata = relval.split('-');

	//alert(arraydata[0]);

	$('.insuranceid').val(arraydata[0]);

	$('.showdataid').val(arraydata[1]);

	$('.showdataid').html(arraydata[1]);

	$("#dialog").dialog();

	$('.ui-button').addClass('demo_pop');

	return false;

});



$(document).on('click', '.demo_pop', function() 

{

    $('.overlay_popup').hide();

});

/*

$('body').on('click','#ui-datepicker-div', function(){

	  $('.overlay_popup').hide();

});

*/

function validAmount()

{

 var old_ant = $('.at_in_id').val();

 var new_ant = $('.amt-t').val();

 if(old_ant !='' && new_ant !='')

 {

   if(new_ant > old_ant)

   {

    $('.showerror_msg').html('<span style="color:red;">Not Valid Amount</span>');

	$('.buttonshowhide').css('display','none');

    return false;

   }

   else

   {

     $('.showerror_msg').html('');

     $('.buttonshowhide').css('display','block');

     return true;

   }

 }

 else

 {

   return false;

 }

}

</script>

 

<script>

$(function() 

{

	$( "#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });

	$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});

});



 $(function() {

 $("#datepicker").change(function () {

    var endDate  = document.getElementById("datepicker").value;

    var startDate = document.getElementById("date_to").value;



    if ((Date.parse(startDate) > Date.parse(endDate))) {

        document.getElementById("datepicker").value = "";

         @if($mer_selected_lang_code !='en')

        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");

        @else

        $('#todata').html("End date should be greater than Start date");

        @endif

        $('#to').show();

    }

    else

    {

         $('#todata').html("");



    }

});

 });



</script>



<script type="text/javascript">

  function updateproductstatus(orderproductid,statusvalue,orderid){

  

var orderproductid=orderproductid;

  var statusvalue=statusvalue;

  var orderid=orderid;

        $.ajax({ 

                      

              url:"{{url('abaya-order/getproductstatus')}}",     

            type: 'post', // performing a POST request

            data : {

              orderproductid: orderproductid,

              orderstatus: statusvalue,

              postedorderid: orderid,

              _token: '{{csrf_token()}}'

            },

            datatype: 'application/json',

                  success: function(data) {

               var json = JSON.stringify(data);

                var obj = JSON.parse(json);

          console.log(obj);

          location.reload();

              },

                  error: function() { /*alert('Failed!');*/ return false; },

        

        });

  }



</script>



<script type="text/javascript">

  

  function submitfrm(){ 



    document.searchfrm.submit();

  }

</script>



<script type="text/javascript">

  

  function paymentrequest(merchant_id,product_id,order_id,amount){



      $.ajax({ 

                      

              url:"{{url('/requestpayment')}}",     

            type: 'post', // performing a POST request

            data : {

              main_cat_id: 34,

              merchant_id: merchant_id,

              product_id: product_id,

              order_id: order_id,

              amount: amount,

              _token: '{{csrf_token()}}'

            },

            datatype: 'application/json',

                  success: function(data) {

               var json = JSON.stringify(data);

                var obj = JSON.parse(json);

          console.log(obj);

            

              document.getElementById('paymentstatus').innerHTML = 'Complete';

          

              },

                  error: function() { /*alert('Failed!');*/ return false; },

        

        });



  }

</script>
@include('sitemerchant.includes.header') 
@php $hall_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_EDIT_CONTAINER') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_EDIT_CONTAINER') }} @endif </h5>
      </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif 
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
                <form name="form1" method="post" id="add-container" action="{{ route('updatecontainerpackage') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form_row">
                  <div class="form_row_left">
                  <label class="form_label">      
                  @if (Lang::has(Session::get('mer_lang_file').'.MER_Container_Name')!= '') {{  trans(Session::get('mer_lang_file').'.MER_Container_Name') }}  
                 @else  {{ trans($MER_OUR_LANGUAGE.'.MER_Container_Name') }} @endif
                   </label>
                   <div class="info100">
                      
                      <select class="small-sel" name="title"  id="title">

                        <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELEST_CONTAINER') }}  
                        @else  {{ trans($MER_SELEST_CONTAINER.'.MER_SELEST_CONTAINER') }} @endif</option>
                        @foreach($letter as $val)
                        
                        <option value="{{$val}}" {{isset($menucontainerpackage->title) && $menucontainerpackage->title==$val ? 'selected' : ''}}>{{ $val }}</option>
                        @endforeach 
                      </select>
                    </div>  
                    </div>
                </div>
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_No_People'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_No_People'); @endphp </span> </label>
                    <div class="info100">
                      <input class="small-sel notzero" type="text" maxlength="100" onkeypress="return isNumber(event)" name="no_people" value="{{ $menucontainerpackage->no_people }}" id="no_people" required="">
                    </div>
                    </div>
                  </div> 
                  <div class="form_row" style="display:none;">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_About'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_About'); @endphp </span> </label>
                    <div class="info100">
                      <textarea class="english" maxlength="5000"  name="about" id="about" rows="4" cols="50">{{ $menucontainerpackage->about }} </textarea>
                      <textarea class="arabic ar" maxlength="5000" name="about_ar" id="about_ar" rows="4" cols="50">{{ $menucontainerpackage->about_ar }}</textarea>
                    </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <input type="hidden" name="updatemc_img" value="{{ $menucontainerpackage->img }}">
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                    <div class="form_row_left">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_Container_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Container_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          
                                            @if($menucontainerpackage->img !='')
<span class="msg_img_replace">@if (Lang::has(Session::get('mer_lang_file').'.mer_img_replace')!= '') {{  trans(Session::get('mer_lang_file').'.mer_img_replace') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_img_replace') }} @endif</span>
@endif
                          
                          <div class="file-btn">

<span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>



 </div>
 <div class="form-upload-img">
                   @if(isset($menucontainerpackage->img) &&  $menucontainerpackage->img !='')
                    <img src="{{ $menucontainerpackage->img }}" > @endif</div>
                        </div>
                        </label>
                        <input id="company_logo" name="img" class="info-file" accept="image/gif, image/jpeg, image/png, image/jpg, image/gif" type="file"  value="">
                      </div>
                    </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row_left english">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input type="hidden" name="hid" value="{{ $hid }}">
                    <input type="hidden" name="bid" value="{{ $bid }}">
                    <input type="submit" name="submit" value="Update">
                  </div>

                    <div class="form_row_right arabic ar arbic_right_btn">
                  
                    <input type="submit" name="submit" value="تحديث">
                  </div>
                  <!-- form_row -->
                </form>
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
            </div>
          </div>
        </div>
        <!--global_area--> 
      </div>
    </div>
    <!--right_panel--> 
  </div>
</div>
<!--merchant_vendor--> 
<script type="text/javascript">

$("#add-container").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       
                      no_people: {
                       required: true,
                      },
                    about: {
                       required: true,
                      },
                      about_ar: {
                       required: true,
                      },
                       img: {
                       accept:"png|jpe?g|gif",
                      },

                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
             required: " {{ trans('mer_en_lang.MER_VALIDATION_CONTAINER_SELECT_NAME') }} ",
                      },  
  
                title_ar: {
               required:  " @php echo lang::get('mer_ar_lang.MER_VALIDATION_CONTAINER_NAME_AR'); @endphp",
                      },
                  
                       no_people: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_PRICE') }} @endif",
                      }, 

                    about: {
                required:  " @php echo lang::get('mer_en_lang.MER_VALIDATION_ABOUT'); @endphp",
                      }, 

                    about_ar: {
               required:  " @php echo lang::get('mer_ar_lang.MER_VALIDATION_ABOUT_AR'); @endphp",
                      },
                     img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
  @if($mer_selected_lang_code !='en')

                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
                   if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                      if (typeof valdata.about_ar != "undefined" || valdata.about_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.no_people != "undefined" || valdata.no_people != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.about != "undefined" || valdata.about != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

@endif
                    },

                submitHandler: function(form) {
                    form.submit();
                }
   
   /* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }
         });

</script> 
@include('sitemerchant.includes.footer')
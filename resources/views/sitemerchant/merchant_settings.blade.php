﻿<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | {{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_SETTINGS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_SETTINGS') : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_SETTINGS')}}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/main-merchant.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/theme.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/MoneAdmin.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif
    <link rel="stylesheet" href="<?php echo url('')?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
          {!! $merchantheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
	 {!! $merchantleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb" style="color:#C00;" >
                            	<li class=""><a href="{{ url('sitemerchant_dashboard') }}">{{ (Lang::has(Session::get('mer_lang_file').'.MER_HOME')!= '')  ?  trans(Session::get('mer_lang_file').'.MER_HOME') : trans($MER_OUR_LANGUAGE.'.MER_HOME') }}</a></li>
                                <li class="active"><a href="#">{{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_SETTINGS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_SETTINGS')   : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_SETTINGS') }}</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>{{ (Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_SETTINGS')!= '') ?  trans(Session::get('mer_lang_file').'.MER_MERCHANT_SETTINGS')   : trans($MER_OUR_LANGUAGE.'.MER_MERCHANT_SETTINGS') }}</h5>
            
        </header>
        <div id="div-1" class="accordion-body collapse in body"  style="border:1px solid #999;margin-top:10px; padding-bottom: 0px;">
            <form class="form-horizontal">

                <div class="form-group">
                 

                    <div class="">
				

		<?php $merchant_id = Session::get('merchantid'); 
 
?>



                         <div class="col-lg-6 col-xs-6 set-info" ><center>
@if (Session::has('merchantid'))
<a href="{{ url('edit_merchant_info/'.$merchant_id) }}" class="btn btn-info" style="color:#ffffff;" >{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_INFORMATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_INFORMATION') :  trans($MER_OUR_LANGUAGE.'.MER_EDIT_INFORMATION') }}</a>@else <a class="btn btn-info"> href="{{  url('sitemerchant') }}" style="color:#ffffff;" >{{ (Lang::has(Session::get('mer_lang_file').'.MER_EDIT_INFORMATION')!= '') ?  trans(Session::get('mer_lang_file').'.MER_EDIT_INFORMATION') : trans($MER_OUR_LANGUAGE.'.MER_EDIT_INFORMATION') }}</a>@endif</center></div> 

<div class="col-lg-6 col-xs-6 set-info"><center>@if (Session::has('merchantid')) <a href="{{ url('change_merchant_password/'.$merchant_id)}}" class="btn btn-danger" style="color:#ffffff;">{{ (Lang::has(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_CHANGE_PASSWORD')}}</a>@else <a clas=="btn btn-danger" href="<?php echo url('sitemerchant'); ?>" >{{ (Lang::has(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD')!= '') ?  trans(Session::get('mer_lang_file').'.MER_CHANGE_PASSWORD') : trans($MER_OUR_LANGUAGE.'.MER_CHANGE_PASSWORD')}}</a>@endif</center></div>
                    </div>
                </div>
				{{ Form::close() }}
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      {!! $merchantfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/jquery-2.0.3.min.js"></script>
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->   
     <script type="text/javascript">
  $.ajaxSetup({
  headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>
</body>
     <!-- END BODY -->
</html>

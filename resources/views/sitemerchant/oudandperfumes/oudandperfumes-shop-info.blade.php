@include('sitemerchant.includes.header') 
@php $oudandperfumesbig_leftmenu =1; @endphp
<div class="merchant_vendor"> @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.MER_BUFFET_INFO')!= '') {{  trans(Session::get('mer_lang_file').'.MER_BUFFET_INFO') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_BUFFET_INFO') }} @endif </h5>
        @include('sitemerchant.includes.language') </header>
      </header>
      <!-- Display Message after submition --> 
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <div class="error arabiclang"></div>
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
  {!! Form::open(array('url' => '/addoudandperfumesbranch','method' => 'POST','enctype' =>'multipart/form-data', 'id'=> 'addbranch', 'name' =>'addbeautybranch' )) !!}


              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_SHOP_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_SHOP_NAME'); @endphp </span> </label>
                  <div class="info100"> @php $getShopName = Helper::getshopname(request()->hid); @endphp
                    <div class="english"> {{ $getShopName->mc_name or '' }} </div>
                     <div class="arabic ar"> {{ $getShopName->mc_name_ar or '' }} </div>
                  </div>
                </div>
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_NAME'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_NAME'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                        <input type="text" id="branchname" class="english" name="branchname" maxlength="60" value="{{ $getbranchinfo->mc_name or ''}}">
                      </div>
                      <div class="arabic ar">
                        <input type="text" name="branchname_ar" maxlength="60" class="arabic ar" id="ssb_name_ar" value="{{ $getbranchinfo->mc_name_ar or '' }}">
                      </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_GOOGLE_MAP_ADDRESS'); @endphp </span> 
                  <a href="javascript:void(0);" class="tooltip_area_wrap"><span class="tooltip_div">@if (Lang::has(Session::get('mer_lang_file').'.mer_google_add')!= '') {{  trans(Session::get('mer_lang_file').'.mer_google_add') }}  @else  {{ trans($OUR_LANGUAGE.'.mer_google_add') }} @endif</span></a>
                  </label>
                  <div class="info100">
                    <input type="url" name="google_map_address"   value="{{$getbranchinfo->google_map_address or ''}}" >
                  </div>
                </div>
              </div>


 <span id="maperror"></span>
                  <div class="form_row_left common_field">
          <div class="lat_left">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LONG')!= '') {{  trans(Session::get('mer_lang_file').'.LONG') }}  @else  {{ trans($OUR_LANGUAGE.'.LONG') }} @endif </label>
                  <input type="text" class="form-control" value="{{$getbranchinfo->longitude or ''}}" readonly="" name="longitude" />
          </div>
          <div class="lat_right">
                  <label class="form_label"> @if (Lang::has(Session::get('mer_lang_file').'.LAT')!= '') {{  trans(Session::get('mer_lang_file').'.LAT') }}  @else  {{ trans($OUR_LANGUAGE.'.LAT') }} @endif </label>
                  <input type="text" class="form-control" value="{{$getbranchinfo->latitude or ''}}"  readonly="" name="latitude" />
          </div>
                  </div>

              <div class="form_row common_field">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_BRANCH_IMAGE'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_BRANCH_IMAGE'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div class="file-value" id="file_value1"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input type="hidden" name="mc_imgname" value="{{$getbranchinfo->mc_img or ''}}">
                        <input type="file" @if(!isset($getbranchinfo->mc_img)) required @endif class="info-file" name="mc_img" id="company_logo">
                        @if(isset($getbranchinfo->mc_img) && $getbranchinfo->mc_img!='')
                       <div class="form-upload-img"><img src="{{$getbranchinfo->mc_img or '' }}"></div>
                        @endif
                      </div>
                    </div> </div>


                <!--div class="form_row_right">
                  <label class="form_label posrel"> <span class="english">@php echo lang::get('mer_en_lang.MER_ADDRESSIMG'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_ADDRESSIMG'); @endphp </span> <a href="javascript:void(0);" class="address_image_tooltip">
  <span class="add_img_tooltip">@if (Lang::has(Session::get('mer_lang_file').'.mer_address_img')!= '') {{  trans(Session::get('mer_lang_file').'.mer_address_img') }}  @else  {{ trans($MER_OUR_LANGUAGE.'.mer_address_img') }} @endif</span>
</a>
</label>
                  <div class="input-file-area">
                    <label for="company_logo1">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value2"></div>
                    </div>
                    </label>
                    <input type="file" name="hall_addressimg" id="company_logo1" class="info-file">
                  </div>
                  
                    @if(isset($getbranchinfo->address_image) && $getbranchinfo->address_image !='')
                  <input type="hidden" name="mc_addimgname" value="{{$getbranchinfo->address_image}}">
                  <div class="form-upload-img"><img src="{{$getbranchinfo->address_image or '' }}">  </div> @endif</div-->






                  
              </div>
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.BACK_ADDRESS'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.BACK_ADDRESS'); @endphp </span> </label>
                  <div class="info100" >
                    <div class="english">
                      <input type="text" class="english" name="address" maxlength="55"  value="{{$getbranchinfo->address or ''}}"  >
                    </div>
                    <div class="arabic ar">
                      <input type="text" class="arabic ar" value="{{$getbranchinfo->address_ar or ''}}"  name="address_ar"  maxlength="55" >
                    </div>
                  </div>
                </div>
                <div class="form_row_right common_field">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_CITY'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CITY'); @endphp </span> </label>
                  <div class="info100" >
                
                    <div class="">
                     @php $getCitys = Helper::getcitylist();  $lang  = Session::get('lang_file'); @endphp
                      <select id="city_id" name="city_id" class="small-sel">
                       

                         <option value="">@if (Lang::has(Session::get('mer_lang_file').'.MER_SELECT_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_SELECT_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_SELECT_CITY') }} @endif</option>
               @php $getC = Helper::getCountry(); @endphp
                        @foreach($getC as $cbval)
                        <option value="" disabled="" style="background-color: #94aeb3; color: #FFF;">@if($mer_selected_lang_code !='en') {{$cbval->co_name_ar}} @else {{$cbval->co_name}} @endif</option>
                        @php $getCity = Helper::getCityb($cbval->co_id); @endphp              
                        @foreach ($getCity as $val)
                            @php $ci_id = $val->ci_id; @endphp
                        @if($mer_selected_lang_code !='en')
                        @php $ci_name= 'ci_name_ar'; @endphp
                        @else
                         @php $ci_name= 'ci_name'; @endphp
                        @endif   
                        <option value="{{ $val->ci_id }}" @if(isset($getbranchinfo->city_id) && ($getbranchinfo->city_id == $val->ci_id))) SELECTED @endif>{{ $val->$ci_name }}</option>
                        @endforeach
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="form_row common_field">
                <div class="form_row_left ">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_MANAGER'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_MANAGER'); @endphp </span> </label>
                  <div class="info100" >
                    <div class=" ">
                      @php $getManagers = Helper::getmanagerlist();  @endphp

                      <select id="manager" name="manager" class="small-sel">
                        <option value=""> @if (Lang::has(Session::get('mer_lang_file').'.SELECT')!= '') {{  trans(Session::get('mer_lang_file').'.SELECT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT') }} @endif</option>
                        @foreach($getManagers as $gms)
                        <option value="{{$gms->mer_id}}" @if(isset($getbranchinfo->branch_manager_id) && ($getbranchinfo->branch_manager_id == $gms->mer_id)) SELECTED @endif >{{$gms->mer_fname or ''}} {{$gms->mer_lname or ''}} </option>
                       @endforeach
                      </select>
                    </div>
                   
                  </div>
                </div>
                
              </div>
              
             
              <div class="form_row">
               
                <div class="form_row_left ">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_DESCRIPTION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DESCRIPTION'); @endphp </span> </label>
                  <div class="info100" >
                   <div class="english">
                        <textarea class="english" cols="50" rows="4" id="description" name="description" maxlength="500">{{ $getbranchinfo->mc_discription or '' }}</textarea>
                      </div>
                      <div class="arabic ar">
                        <textarea cols="50" rows="4" id="description Arabic" name="description_ar" class="arabic ar"  maxlength="500">{{ $getbranchinfo->mc_discription_ar or '' }}</textarea>
                      </div>
                  </div>
                </div>

                 <div class="form_row_right english">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo9">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value8"></div>
                    </div>
                    </label>
                    <input type="file" name="tnc" id="company_logo9" class="info-file">
                   <input type="hidden" name="tnc_img" value="{{$getbranchinfo->terms_conditions or ''}}">
                   <input type="hidden" name="tncname" value="{{$getbranchinfo->terms_condition_name or ''}}">
                   <div class="pdf_msg">@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp</span> <span  class="arabic ar">  </div>
                      @if(isset($getbranchinfo->terms_conditions) && $getbranchinfo->terms_conditions!='')
                      
                      <a href="{{$getbranchinfo->terms_conditions or ''}}" target="_blank" class="pdf_icon"><img src="{{url('/themes/images/pdf.png')}}"> <span>{{$getbranchinfo->terms_condition_name or ''}}</span></a>
                        @endif


                  </div>
                </div>

              </div>
              
              <div class="arabic ar">
              <div class="form_row">
                <div class="form_row_left">
                  <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_TERMSCONDITION'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_TERMSCONDITION'); @endphp </span> </label>
                  <div class="input-file-area">
                    <label for="company_logo8">
                    <div class="file-btn-area">
                      <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                      <div class="file-value" id="file_value7"></div>
                    </div>
                    </label>
                    <input type="file" name="mc_tnc_ar" id="company_logo8" class="info-file">
                  <input type="hidden" name="tnc_img_ar" value="{{$getbranchinfo->terms_conditions_ar or ''}}">
                   <input type="hidden" name="tncname_ar" value="{{$getbranchinfo->terms_condition_name_ar or ''}}">
                   <div class="pdf_msg">  <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp</div>
                      @if(isset($getbranchinfo->terms_conditions_ar) && $getbranchinfo->terms_conditions_ar!='')
                      
                      <a href="{{$getbranchinfo->terms_conditions_ar or ''}}" target="_blank" class="pdf_icon"><span>{{$getbranchinfo->terms_condition_name_ar or ''}}</span> <img src="{{url('/themes/images/pdf.png')}}"> </a>
                        @endif


                  </div>
                </div>
              </div>
              </div>
              
              <div class="form-btn-section english">
              <div class="form_row_left">
                        <input type="hidden" name="hid" value="{{request()->hid}}">
                    <input type="hidden" name="id" value="{{request()->id}}">
                    <input type="hidden" name="itemid" value="{{ request()->itemid}}">   
                <input type="submit" id="beautysubmit" name="beautysubmit" value="Submit">
                </div>
              </div>
              <div class="form-btn-section arabic ar">
              <div class="form_row_left">
                <input type="submit" id="beautysubmit" name="beautysubmit" value="خضع">
              </div>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>


<script type="text/javascript">
  
$("#addbranch").validate({
                  ignore: [],
                  rules: {
                  'branchname': {
                       required: true,
                      },

                      'branchname_ar': {
                       required: true,
                      },
                 
                      'address': {
                       required: true,
                      },
                      'address_ar': {
                       required: true,
                      },
                      'city_id': {
                       required: true,
                      },
                      /*'manager': {
                       required: true,
                      },*/
                      
                       'description': {
                       required: true,
                      },
                      'description_ar': {
                       required: true,
                      },

                    @if(isset($getbranchinfo->terms_conditions) && $getbranchinfo->terms_conditions!='')
                    'tnc': {
                    required: false,
                    accept:"pdf",
                    },
                     'mc_tnc_ar': {
                    required: false,
                    accept:"pdf",
                    },
                    'mc_img': {
                    required: false,
                    accept:"png|jpe?g|gif",
                    },
                  

                    @else
                    'tnc': {
                    required: true,
                    accept:"pdf",
                    },
                    'mc_img': {
                    required: true,
                    accept:"png|jpe?g|gif",
                    },
                    'mc_tnc_ar': {
                    required: true,
                    accept:"pdf",
                    },
                     @endif
                     
                     'hall_addressimg': {
                    required: false,
                    accept:"png|jpe?g|gif",
                    },

                     


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: { 

                    branchname: {
                   required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_BRANCH_NAME'); @endphp",
                    },  
                    branchname_ar: {
                     required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_BRANCH_NAME'); @endphp",
                    },  
                    address: {
                    required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_ADDRESS'); @endphp",
                    },
                    address_ar: {
                      required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_ADDRESS'); @endphp",
                    }, 
                    city_id: {
                      required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CITY')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_CITY') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_CITY') }} @endif ",
                    }, 
                   /* manager: {
                    required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_CHOOSE_MANAGER')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_CHOOSE_MANAGER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_CHOOSE_MANAGER') }} @endif ",
                    },*/
           
                    description: {
                    required: "@php echo lang::get('mer_en_lang.PLEASE_ENTER_DESCRIPTION'); @endphp",
                    },

                    description_ar: {
                    required: "@php echo lang::get('mer_ar_lang.PLEASE_ENTER_DESCRIPTION'); @endphp",
                    },

                    mc_img: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    },

                     hall_addressimg: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    },    

                    tnc: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_FILE') }} @endif",
                     required: "@php echo lang::get('mer_en_lang.Please_upload_only_PDF_file'); @endphp",
                    },   
                     mc_tnc_ar: {
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_FILE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_FILE') }} @endif",                    
                     required: "@php echo lang::get('mer_ar_lang.Please_upload_only_PDF_file'); @endphp",
                    },                  
         
                     
                },
                invalidHandler: function(e, validation){
                    
                  var valdata=validation.invalid;

   @if($mer_selected_lang_code !='en')

                  if (typeof valdata.branchname != "undefined" || valdata.branchname != null)  
                  {
                  $('.english_tab').trigger('click'); 
                  }


                   if (typeof valdata.hall_addressimg != "undefined" || valdata.hall_addressimg != null)  
                    {
                    $('.english_tab').trigger('click'); 
                    }

                  if (typeof valdata.address != "undefined" || valdata.address != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                
                  if (typeof valdata.description != "undefined" || valdata.description != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                    if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }
                 if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                  if (typeof valdata.branchname_ar != "undefined" || valdata.branchname_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }

@else
                  if (typeof valdata.mc_tnc_ar != "undefined" || valdata.mc_tnc_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                 if (typeof valdata.address_ar != "undefined" || valdata.address_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                  if (typeof valdata.description_ar != "undefined" || valdata.description_ar != null) 
                  {
                  $('.arabic_tab').trigger('click'); 
                  }

                  if (typeof valdata.branchname_ar != "undefined" || valdata.branchname_ar != null) 
                  {
                  $('.arabic_tab').trigger('click');     
                  }
                if (typeof valdata.branchname != "undefined" || valdata.branchname != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.address != "undefined" || valdata.address != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                   if (typeof valdata.hall_addressimg != "undefined" || valdata.hall_addressimg != null)  
                    {
                    $('.english_tab').trigger('click'); 
                    }
                  if (typeof valdata.city_id != "undefined" || valdata.city_id != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.manager != "undefined" || valdata.manager != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.description != "undefined" || valdata.description != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.tnc != "undefined" || valdata.tnc != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.mc_img != "undefined" || valdata.mc_img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

@endif

               

                    },


                submitHandler: function(form) {
                   var mapAdd = jQuery('input[name=google_map_address]').val();
                              if(mapAdd !='')
                              {
                              var long = jQuery('input[name=longitude]').val();
                              var lat  =  jQuery('input[name=latitude]').val();
                              if(long =='' && lat=='')
                              {
                              var allOk = 0;

                              }
                              else
                              {
                              var allOk = 1; 
                              }
                              }
                              else
                              {
                              var allOk = 1;
                              }

                              if(allOk == 1)
                              {
                              form.submit();
                              }
                              else
                              {
                              $('#maperror').html("<span class='error'>@if (Lang::has(Session::get('mer_lang_file').'.get_Lat_Long_issue')!= '') {{  trans(Session::get('mer_lang_file').'.get_Lat_Long_issue') }} @else  {{ trans($MER_OUR_LANGUAGE.'.get_Lat_Long_issue') }} @endif</span>");
                              }
                }
            });
 
    
   $(document).ready(function(){
      
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
      if(checkedvalue==1) {  
      $('.homevisit').show();
    } else {
        $('.homevisit').hide();
    }

     $('#type').click(function(){
     var checkedvalue =   $('input[name=service_available[]]:checked').val();
     if(checkedvalue==1) {
       $('.homevisit').show();
       } else {
           $('.homevisit').hide();

       }
     })

     $('#dish').click(function(){
       if(checkedvalue==1) {
        $('.homevisit').show();
      } else {
          $('.homevisit').hide();
      }
    
     })

    })
</script>




<!-- merchant_vendor --> 
@include('sitemerchant.includes.footer')
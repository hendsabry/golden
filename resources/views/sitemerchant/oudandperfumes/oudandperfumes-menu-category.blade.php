@include('sitemerchant.includes.header') 
@php $buffet_leftmenu =1; @endphp
<div class="merchant_vendor">  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
       <div class="inner">
	    <header>
        <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_ADD_MENU_CATEGORY')!= '') {{  trans(Session::get('mer_lang_file').'.MER_ADD_MENU_CATEGORY') }}  
          @else  {{ trans($MER_OUR_LANGUAGE.'.MER_ADD_MENU_CATEGORY') }} @endif</h5>
      </header>
	    @include('sitemerchant.includes.language')
		<div class="row">
        <div class="col-lg-12">
          <div class="box">             
           
            <div class="one-call-form">
			<div class="form_row">
                  <label class="form_label">
                    <span class="english">@php echo lang::get('mer_en_lang.MER_CATEGORY_NAME'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_CATEGORY_NAME'); @endphp </span>
 </label>
                  <div class="info100">
                   <div class="english"> 
                    <input type="text" class="english" name="category_name" maxlength="90" value="{!! Input::old('category_name') !!}" id="category_name" required="" >
                     </div>
                    <div class="arabic">
                    <input type="text" class="arabic ar" name="category_name_ar" maxlength="90" value="{!! Input::old('category_name_ar') !!}" id="category_name_ar" required="" >
                  </div>
                  </div>
				  </div>
				            <div class="form_row english">
                  <input type="submit" name="submit" value="Submit">
               
                </div>
 <div class="form_row arabic ar">
                  <input type="submit" name="submit" value="خضع">
             
                </div>

			</div></div></div></div>
	   </div>
  </div>
</div>
</div>
<!-- merchant_vendor --> 
<script>
   function check_email(){
   var email_id = $('#email_id').val();
   $.ajax({
   type: 'get',
   data: 'email_id='+email_id,
   url: '<?php echo url('check_mer_email'); ?>',
   success: function(responseText){  
    //alert(responseText);
    $('#email_id_error_msg').html(responseText);  
    if(responseText!=''){
      $("#email_id").css('border', '1px solid red'); 
      $("#email_id").focus();
    }
    else
      $("#email_id").css('border', '1px solid #ccc'); 
   
    
    
    
   }    
   });  
   }

</script> 
<script>
       
            $("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">
$("#menucategory").validate({
                  ignore: [],
                  rules: {
                  category_name: {
                       required: true,
                      },

                       category_name_ar: {
                       required: true,
                      },
                 
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             category_name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_CATEGORY_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_CATEGORY_NAME') }} @endif",
                      },  

                 category_name_ar: {
               required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_CAT_ARABIC_AR'); @endphp",
                      },    
                                                              
                     
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    if (typeof valdata.category_name != "undefined" || valdata.category_name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    
                  
                    else if (typeof valdata.category_name_ar != "undefined" || valdata.category_name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

</script>
@include('sitemerchant.includes.footer')
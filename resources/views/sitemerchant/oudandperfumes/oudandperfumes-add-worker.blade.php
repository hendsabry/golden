@include('sitemerchant.includes.header') 
@php $oudandperfumesbig_leftmenu =1; @endphp
<div class="merchant_vendor">   @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
  <div class="right_panel">
    <div class="inner">
      <header>
        @if(request()->workerid=='')
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADDWORKER')!= '') {{  trans(Session::get('mer_lang_file').'.ADDWORKER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADDWORKER') }} @endif </h5>
        @else
        <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATEWORKER')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATEWORKER') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATEWORKER') }} @endif </h5>
        @endif

        @include('sitemerchant.includes.language') </header>
      <div class="global_area">
        <div class="row">
          <div class="col-lg-12">
            <div class="box"> 
              
              <!-- Display Message after submition --> 
              @if (Session::has('message'))
              <div class="alert alert-info">{{ Session::get('message') }}</div>
              @endif 
              <!-- Display Message after submition -->
              <div class="one-call-form">
                <form name="form1" id="addmanager" method="post" action="{{ route('storespaworker') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  
                  
                  <!-- form_row -->                                    
                  
                  <div class="form_row">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.MER_NAME'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_NAME'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="english">
                        <input type="text" class="english" name="name" maxlength="50" value="{{ $fetchdata->staff_member_name or '' }}" id="name" required="" >
                      </div>
                      <div class="arabic ar">
                        <input type="text" class="arabic ar" name="name_ar" maxlength="50" value="{{ $fetchdata->staff_member_name_ar or '' }}" id="name_ar" required="" >
                      </div>
                    </div>
                    <!-- form_row --> 
                  </div>
                  <!-- form_row -->
                  
                  <div class="form_row common_field">
                    <label class="form_label"> <span class="english">@php echo lang::get('mer_en_lang.MER_User_Image'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_User_Image'); @endphp </span> </label>
                    <div class="info100">
                      <div class="input-file-area">
                        <label for="company_logo">
                        <div class="file-btn-area">
                          <div id="file_value1" class="file-value"></div>
                          <div class="file-btn"> <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span> <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span> </div>
                        </div>
                        </label>
                        <input id="company_logo" name="stor_img" class="info-file" type="file">

                           @if(isset($fetchdata->image) && $fetchdata->image!='')
                       <img src="{{ $fetchdata->image or '' }}"/>
                       @endif
                      </div>
                    </div>
                  </div>
                  <!-- form_row -->
                  <div class="form_row common_field">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.EXPERT'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.EXPERT'); @endphp </span> 
            </label>
                <div class="info100">
                @foreach($getAttr as $catvals)
                @php  $Itemsarray = array();
                $getProducts = Helper::getrelateditems($catvals->id,request()->itemid); 
                if($getProducts->count() < 1) {continue; }
                @endphp
                <!--Loop start-->
                <div class="cat_box"> 
                <div class="save-card-line">

 

                <input type="checkbox" class="cdata " id="attri_{{$catvals->id}}" data-id="{{$catvals->id}}"><label for="attri_{{$catvals->id}}">    @if($mer_selected_lang_code =='en')  {{$catvals->attribute_title or ''}} @else {{$catvals->attribute_title_ar or ''}} @endif </span> </label>
                </div>
                <div class="category_area" id="childcbox_{{$catvals->id}}">            
                @foreach($getProducts as $proitems)
                <!--Loop -->
                <div class="save-card-line">
                @php
                $pID = $proitems->pro_id;
                foreach($getPkg as $valp)
                {
                $Itemsarray[] = $valp->item_id;
                }
                @endphp
                <input  type="checkbox" id="men{{$proitems->pro_id}}"@if(in_array($proitems->pro_id, $proid)) CHECKED @endif  name="productitems[]" value="{{$proitems->pro_id or ''}}">
                <label for="men{{$proitems->pro_id}}"> <span class="englishd">@if($mer_selected_lang_code =='en')  {{$proitems->pro_title or ''}} @else  {{$proitems->pro_title_ar or ''}}  @endif<span class="sar">(SAR {{$proitems->pro_price or ''}})</span>  </span>   </label>
                </div>
                <!--Loop end -->                
                @endforeach
                </div>
                </div>
                @endforeach 
                <!--Loop End --> 

                </div>
                <span for="productitems[]" generated="true" class="error">  </span>
                </div>
                  
                  <div class="form_row common_field">
                    <label class="form_label"> 
                      <span class="english">@php echo lang::get('mer_en_lang.YEAROFEXP'); @endphp</span> 
                        <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.YEAROFEXP'); @endphp </span> 
                     </label>
                    <div class="info100">
                      <div class="englishs">
                        <input type="text" class="small-sel" name="year_of_exp" maxlength="10" value="{{ $fetchdata->experience or '' }}" id="year_of_exp" required="" >
                      </div>
                      
                    </div>
                    <!-- form_row --> 
                  </div>
                  <!-- form_row --> 
                  
                  <div class="form_row english">
                <input type="hidden" name="workerid" value="{{request()->workerid}}">
                <input type="hidden" name="itemid" value="{{request()->itemid}}">
                <input type="hidden" name="hid" value="{{request()->hid}}">
                <input type="hidden" name="id" value="{{request()->id}}">
                    
                    <input type="submit" name="submit" value="Submit">
                  </div>
                  <!-- form_row -->
                  <div class="form_row arabic ar">
                    <input type="submit" name="submit" value="خضع">
                  </div>
                  <!-- form_row -->
                </form>
              </div>
              <!-- one-call-form --> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT START--> 
              
              <!--PAGE CONTENT PART WILL COME INSIDE IT END--> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- global_area --> 
    </div>
  </div>
  <!-- right_panel --> 
</div>
<!-- merchant_vendor --> 
<script type="text/javascript">
 

  
$("#addmanager").validate({

                  ignore: [],
                  rules: {
                    name: {
                         required: true,
                        },

                       name_ar: {
                       required: true,
                      },

                      year_of_exp: {
                       required: true,
                      },

                       "productitems[]": {
                       required: true,
                      },
                      
                       @if(isset($fetchdata->image)!='')  
                        stor_img: {
                           required:false,
                           accept:"png|jpeg|jpg|gif",        
                      },
                       @else
                        stor_img: {
                           required:true,
                           accept:"png|jpeg|jpg|gif",        
                      },
                      @endif


                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
             
           messages: {
                name: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_NAME') }} @endif",
                      }, 

                    name_ar: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXPERTISES_AR')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXPERTISES_AR') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXPERTISES_AR') }} @endif",
                      },     


                       year_of_exp: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXP')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXP') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXP') }} @endif ",
                      },

                      "productitems[]": {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_EXPERTISE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_EXPERTISE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_EXPERTISE') }} @endif ",
                      },
                    
                  stor_img: {
                                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif",
                                    required:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",


                                    

                      },   

                                                       
                     
                },
                errorPlacement: function(error, element) {
                   if (element.attr("name") == "element_name") {
                      error.insertAfter('#element_div_id');
                   }
                   else {
                      error.insertAfter(element);
                   }
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;

                  @if($mer_selected_lang_code !='en')
                    if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                    if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      
                     
                     if (typeof valdata.stor_img != "undefined" || valdata.stor_img != null) 
                    {
                      
                        $('.english_tab').trigger('click');     
                       

                    }

                      
                     if (typeof valdata.name_ar != "undefined" || valdata.description_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                     
                    }
          @else


             if (typeof valdata.name_ar != "undefined" || valdata.name_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     
                       

                    }

                     
 
            if (typeof valdata.name != "undefined" || valdata.name != null) 
                    {
                        $('.english_tab').trigger('click'); 
                      
                    }
                     if (typeof valdata.year_of_exp != "undefined" || valdata.year_of_exp != null) 
                    {
                        $('.english_tab').trigger('click');
                           

                    }
                      

                    
                @endif

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

 
 
 
$(document).ready(function() {
  $('.cdata').click(function() {
   var childP = $(this).data('id');

    var checked = $(this).prop('checked');
    $('#childcbox_'+childP).find('input:checkbox').prop('checked', checked);
  });
})


</script> 
@include('sitemerchant.includes.footer')
@include('sitemerchant.includes.header') 
@php $oudandperfumesbig_leftmenu =1; @endphp
<div class="merchant_vendor">
  @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
<div class="right_panel">
<div class="inner">
    <header>

      @if(request()->serviceid=='')
      <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.ADDPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.ADDPACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ADDPACKAGE') }} @endif </h5>
      @else
    <h5 class="global_head">@if (Lang::has(Session::get('mer_lang_file').'.UPDATEPACKAGE')!= '') {{  trans(Session::get('mer_lang_file').'.UPDATEPACKAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.UPDATEPACKAGE') }} @endif </h5>
      @endif

      @include('sitemerchant.includes.language') </header>
    <div class="global_area">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <!-- Display Message after submition -->
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}  </div>
            @endif
            <!-- Display Message after submition -->
            <div class="one-call-form">
             <form name="form1" id="addmanager" method="post" action="{{ route('storespapackageservice') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
               
                <div class="form_row">
                  <label class="form_label"><span class="english">@php echo lang::get('mer_en_lang.PACKEGENAME'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKEGENAME'); @endphp </span></label>
                  <div class="info100">
                  <div class="english">
                    <input class="english" maxlength="60" type="text" name="title" value="{{$getservice->pro_title or ''}}" id="title" required="">
                     </div>
                     <div class="arabic">
                    <input class="arabic ar" maxlength="60" id="title_ar"  name="title_ar" value="{{$getservice->pro_title_ar or ''}}"  type="text" >
                    </div>

                     </div>  
                </div>
               
                
              <div class="form_row common_field">
                  <label class="form_label">
                    <span class="english">@php echo lang::get('mer_en_lang.PACKAGEIMAGE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.PACKAGEIMAGE'); @endphp </span>
          </label>
                  <div class="info100">
                    <div class="input-file-area">
                      <label for="company_logo">
                      <div class="file-btn-area">
                        <div id="file_value1" class="file-value"></div>
                        <div class="file-btn">
 <span class="english">@php echo lang::get('mer_en_lang.MER_UPLOAD'); @endphp</span>
 <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_UPLOAD'); @endphp </span>
 </div>
                      </div>
                      </label>
                      <input id="company_logo" name="img" class="info-file" type="file">
                          @if(isset($getservice->pro_Img) && $getservice->pro_Img !='')<img src="{{$getservice->pro_Img or ''}}"> @endif
                    </div>
                  </div>
                </div>
                
         
         <div class="form_row common_field">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.MER_PRICE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_PRICE'); @endphp </span> 
</label>
                  <div class="info100">
                   <div>
                    <input class="small-sel" maxlength="9" type="text" name="pro_price"  value="{{$getservice->pro_price or ''}}" id="pro_price">
                    </div>
                  </div>
                </div>
        
        <div class="form_row common_field"> 

                         <label class="form_label">
                          <span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span>
 </label>
              <div class="info100">
                       <input type="text" class="small-sel" onkeypress="return isNumber(event)"  name="discount" maxlength="7" value="{{$getservice->pro_discount_percentage or ''}}" id="discount"> 
             </div>
             
             </div>
        
        <div class="form_row common_field">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.MER_Duration'); @endphp  </span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_Duration'); @endphp  </span> 
</label>
                  <div class="info100">
                   <div>
                    <input class="small-sel" onkeypress="return isNumber(event)" type="text" name="duration" maxlength="5" value="{{$getservice->service_hour or ''}}"  id="mer_duration"   required="">
                    </div>
                  </div>
                </div>
 
        <div class="form_row common_field">
                  <label class="form_label">
                     <span class="english">@php echo lang::get('mer_en_lang.SELECTSERVICE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.SELECTSERVICE'); @endphp </span> 
</label>
                  <div class="info100">
       @foreach($getAttr as $catvals)
            @php  $Itemsarray = array();
            $getProducts = Helper::getrelateditems($catvals->id,request()->itemid); 
            if($getProducts->count() < 1) {continue; }
            @endphp
        <!--Loop start-->
          <div class="cat_box">
                      <div class="save-card-line">
                     <label for="men">   @if($mer_selected_lang_code =='en')  {{$catvals->attribute_title or ''}} @else {{$catvals->attribute_title_ar or ''}} @endif </span> </label>
                    </div>
          <div class="category_area">            
            @foreach($getProducts as $proitems)
                  <!--Loop -->
                  <div class="save-card-line">
              @php
              $pID = $proitems->pro_id;
              foreach($getPkg as $valp)
              {
              $Itemsarray[] = $valp->item_id;
              }
              @endphp
      <input type="radio" id="men{{$proitems->pro_id}}" @if (in_array($pID, $Itemsarray)) CHECKED @endif  name="productitems[]" value="{{$proitems->pro_id or ''}}">
                  <label for="men{{$proitems->pro_id}}"> <span class="englishd">@if($mer_selected_lang_code =='en')  {{$proitems->pro_title or ''}} @else  {{$proitems->pro_title_ar or ''}}  @endif<span class="sar">(SAR {{$proitems->pro_price or ''}})</span>  </span>   </label>
                  </div>
                  <!--Loop end -->                
          @endforeach
          </div>
          </div>
          @endforeach 
          <!--Loop End --> 

<span for="productitems[]" generated="true" class="error">   </span>
                  </div>
                </div>
        
        
                <!-- form_row -->
                
                <!-- form_row -->
                <div class="form_row english">

             

         <input type="hidden" name="id" value="{{request()->id}}">
         <input type="hidden" name="hid" value="{{request()->hid}}">
         <input type="hidden" name="itemid" value="{{request()->itemid}}">
         <input type="hidden" name="serviceid" value="{{request()->serviceid}}">
                  <input type="submit" name="submit" value="Submit">
                </div>

                    <div class="form_row arabic ar">
                  <input type="submit" name="submit" value="خضع">
                </div>
                <!-- form_row -->
              </form>
            </div>
            <!-- one-call-form -->
            <!--PAGE CONTENT PART WILL COME INSIDE IT START-->
            <!--PAGE CONTENT PART WILL COME INSIDE IT END-->
          </div>
        </div>
      </div>
      <!-- global_area -->
    </div>
  </div>
  <!-- right_panel -->
</div>
</div>
<!-- merchant_vendor -->
<script type="text/javascript">
    
$("#addmanager").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       pro_price: {
                       required: true,
                      },
                      duration: {
                       required: true,
                      },
                      "productitems[]": {
                       required: true,
                      },

                      @if(!isset($getservice->pro_Img))     
                      img: {
                      required: true,
                      accept:"png|jpe?g|gif",
                      },
                      @else
                      img: {
                      accept:"png|jpe?g|gif",
                      },
                      @endif
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_NAME')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PACKAGE_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PACKAGE_NAME') }} @endif",
                      },  

                title_ar: {
               required:  " {{ trans('mer_ar_lang.PLEASE_ENTER_PACKAGE_NAME') }} ",
                      },
                      "productitems[]": {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_SELECT_SERVICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_SELECT_SERVICE') }} @endif ",
                      },
                   pro_price: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_PRICE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_PRICE') }} @endif",
                      },
                       duration: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION')!= '') {{  trans(Session::get('mer_lang_file').'.PLEASE_ENTER_DURATION') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_DURATION') }} @endif",
                      },                   
                     img: {
                  required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_IMAGE') }} @endif",
                    accept:"@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_VALID_IMAGE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_VALID_IMAGE') }} @endif"
                      },  

                                                              
                     
                },
                invalidHandler: function(e, validation){
                    var valdata=validation.invalid;


             @if($mer_selected_lang_code !='en')
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.price != "undefined" || valdata.price != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                     
                    if (typeof valdata.img != "undefined" || valdata.img != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                    if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }
@else
                  if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                  {

                  $('.arabic_tab').trigger('click');     

                  }

                  if (typeof valdata.title != "undefined" || valdata.title != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.price != "undefined" || valdata.price != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }
                  if (typeof valdata.duration != "undefined" || valdata.duration != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }

                  if (typeof valdata.img != "undefined" || valdata.img != null) 
                  {
                  $('.english_tab').trigger('click'); 
                  }


@endif
                    
                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }


</script> 
@include('sitemerchant.includes.footer')
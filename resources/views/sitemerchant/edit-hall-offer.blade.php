@include('sitemerchant.includes.header') 
@php $hall_leftmenu =1; @endphp 
<div class="merchant_vendor">      
 @include('sitemerchant.includes.breadcrumb') @include('sitemerchant.includes.left')
 

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />

     <div class="right_panel">
      <div class="inner">
       <header>
            
             <h5 class="global_head"> @if (Lang::has(Session::get('mer_lang_file').'.MER_OFFER_UPDATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_OFFER_UPDATE') }}  
            @else  {{ trans($MER_OUR_LANGUAGE.'.MER_OFFER_UPDATE') }} @endif </h5>
            </header>
             @include('sitemerchant.includes.language') 
             <!-- Display Message after submition -->
      @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
      <!-- Display Message after submition -->
      <div class="global_area">
        <div class="row">
        <div class="col-lg-12">
          <div class="box">
         

        
      
           <div class="one-call-form">

                   <form name="form1" id="offer" method="post" action="{{ route('update-hall-offer') }}" enctype="multipart/form-data">
                 {{ csrf_field() }}
          <div class="form_row">
                      <div class="form_row_left">   
                     <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_OFFER_TITLE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_OFFER_TITLE'); @endphp </span>

 </label>
            <div class="info100">
             <div class="english">
                       <input type="text" name="title" maxlength="50" value="{{ $halloffer->title }}" id="title" required=""> 
             </div>
                      
                 <div class="arabic">      
                <input class="arabic ar" id="title_ar" maxlength="50"  value="{{ $halloffer->title_ar }}"  name="title_ar"   value="" required=""  type="text"></div> </div></div>
                <div class="form_row_right common_field">

                           <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.MER_DISCOUNT'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_DISCOUNT'); @endphp </span>
 </label>
                <div class="info100">
                       <input type="text" class="notzero xs_small" onkeypress="return isNumber(event)" name="discount"  maxlength="5"  value="{{ $halloffer->discount }}" id="price" required="" > </div>
</div></div>
 <div class="form_row common_field">
<div class="form_row_left"> 
                      <label class="form_label">

<span class="english">@php echo lang::get('mer_en_lang.MER_START_DATE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_START_DATE'); @endphp </span>

               </label>
             <div class="info100">
                       <input type="text" name="Date_from" maxlength="10" value="{{ $halloffer->start_date }}" id="datepicker" required=""  class="cal-t">
                   <input type="hidden" name="hid" value="{{$hid}}">
                   <input type="hidden" name="bid" value="{{$bid}}">    
             </div>
             </div>
             <div class="form_row_right">
                      <label class="form_label">

<span class="english">@php echo lang::get('mer_en_lang.MER_END_DATE'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.MER_END_DATE'); @endphp </span>
 </label>
                   <div class="info100">     <input type="text" maxlength="10" name="date_to" value="{{ $halloffer->end_date }}" id="date_to" required="" placeholder=">@if(Lang::has(Session::get('mer_lang_file').'.MER_END_DATE')!=''){{trans(Session::get('mer_lang_file').'.MER_END_DATE')}}@else{{trans($MER_OUR_LANGUAGE.'.MER_END_DATE')}}@endif" class="cal-t"> 
           </div>
             
             </div>
             </div>



<div class="form_row common_field">
          <div class="form_row_left">    
                        
                        <label class="form_label">
<span class="english">@php echo lang::get('mer_en_lang.COUPON'); @endphp</span>
            <span  class="arabic ar"> @php echo lang::get('mer_ar_lang.COUPON'); @endphp </span>

 </label>
             <div class="info100">                      
          <input type="text" maxlength="50" name="coupon" value="{{ $halloffer->coupon }}" id="coupon" class="xs_small"> 
                        
            </div></div>
           
             </div>
             
                       <input type="hidden" name="id" value="{{ $id }}">
                     <div class="form_row english">  <input type="submit" name="submit" value="Update"></div>

                       <div class="form_row arabic ar">  <input type="submit" name="submit" value="تحديث"></div>


                  </form>
       </div>  
 

      <!--PAGE CONTENT PART WILL COME INSIDE IT START-->


        <!--PAGE CONTENT PART WILL COME INSIDE IT END-->

          </div>
        </div>
        </div>
    </div>
      </div>
    </div>
    </div>


<script>
 $(function() {
$( "#datepicker,#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd', minDate: 0});
});

</script>
<script>
 $(function() {
$( "#datepicker,#date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
});

 $(function() {
 $("#date_to").change(function () {
    var startDate = document.getElementById("datepicker").value;
    var endDate = document.getElementById("date_to").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        document.getElementById("date_to").value = "";
         @if($mer_selected_lang_code !='en')
        $('#todata').html("يجب أن يكون تاريخ الانتهاء أكبر من تاريخ البدء");
        @else
        $('#todata').html("End date should be greater than Start date");
        @endif
        $('#to').show();
    }
    else
    {
        $('#to').hide();

    }
});
 });


</script>
<script>
       
$("form").data("validator").settings.ignore = "";
 </script>

<script type="text/javascript">

$("#offer").validate({
                  ignore: [],
                  rules: {
                  title: {
                       required: true,
                      },

                       title_ar: {
                       required: true,
                      },
                       discount: {
                       required: true,
                      },
                      Date_from: {
                       required: true,
                      },
                    date_to: {
                       required: true,
                      },
                        coupon: {
                       required: true,
                      },
                  },
                 highlight: function(element) {
            $(element).removeClass('error');
                },
               
           messages: {
             title: {
               required:  "@php echo lang::get('mer_en_lang.MER_VALIDATION_OFFER_TITLE'); @endphp",
                      },  
            coupon: {
               required: "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_COUPON')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_COUPON') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_COUPON') }} @endif",
                      }, 

                title_ar: {
             required:  "@php echo lang::get('mer_ar_lang.MER_VALIDATION_OFFER_TITLE_AR'); @endphp",
                      },
                 discount: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_DISCOUNT')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_DISCOUNT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_DISCOUNT') }} @endif",
                      }, 

                   Date_from: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_START_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_START_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_START_DATE') }} @endif",
                      }, 

                    date_to: {
               required:  "@if (Lang::has(Session::get('mer_lang_file').'.MER_VALIDATION_END_DATE')!= '') {{  trans(Session::get('mer_lang_file').'.MER_VALIDATION_END_DATE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.MER_VALIDATION_END_DATE') }} @endif",
                      }, 
                },
                invalidHandler: function(e, validation){
                    console.log("invalidHandler : event", e);
                    console.log("invalidHandler : validation", validation);
                    var valdata=validation.invalid;
                    if (typeof valdata.title != "undefined" || valdata.title != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }

                     if (typeof valdata.discount != "undefined" || valdata.discount != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.Date_from != "undefined" || valdata.Date_from != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    if (typeof valdata.date_to != "undefined" || valdata.date_to != null) 
                    {
                        $('.english_tab').trigger('click'); 
                    }
                    
                     if (typeof valdata.title_ar != "undefined" || valdata.title_ar != null) 
                    {
                      
                        $('.arabic_tab').trigger('click');     

                    }

                    },

                submitHandler: function(form) {
                    form.submit();
                }
            });

/* Mobile Number Validation */
   function isNumber(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   return false;
   }
   return true;
   }

</script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
@include('sitemerchant.includes.footer')

@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
@php 
global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
 
if($Current_Currency =='') { 
  $Current_Currency = 'SAR'; 
 } 
 
  @endphp
  
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')    
    <!-- Display Message after submition -->
    
    <!-- Display Message after submition -->
    <form name="form" id="quote_view" method="post" action="{{url('updaterequestaquoteview')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
	<input type="hidden" name="last_id" value="{{$id}}" />
    <div class="myaccount_right" id="change_password">
	<div class="dash_select"> <a href="{{route('my-request-a-quote')}}">{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE_DETAIL')}}</a> </div>
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.REQUEST_A_QUOTE_DETAIL'): trans($OUR_LANGUAGE.'.REQUEST_A_QUOTE_DETAIL')}}</h1>
      <div class="field_group top_spacing_margin_occas myprofile_page"> 
	  
	    @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif   
		
		   
        <div class="checkout-box"> 
          <div class="checkout-form"> 
		    <div class="singer_price">{{currency($vendoramt->price, 'SAR',$Current_Currency) }}</div>
            <div class="vendor_reply"><b>@if(Lang::has(Session::get('lang_file').'.VENDOR_REPLY')!= '') {{ trans(Session::get('lang_file').'.VENDOR_REPLY')}}  @else {{ trans($OUR_LANGUAGE.'.VENDOR_REPLY')}} @endif</b>: <?php  echo nl2br($vendoramt->comment);?></div>
		    <?php if(!empty($allinquiry->the_groooms_name)){ ?>
		    <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.THE_GROOM_NAME')!= '') {{ trans(Session::get('lang_file').'.THE_GROOM_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.THE_GROOM_NAME')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="groom_name" id="groom_name" value="{{$allinquiry->the_groooms_name}}" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->hall)){ ?>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.HALL')!= '') {{ trans(Session::get('lang_file').'.HALL')}}  @else {{ trans($OUR_LANGUAGE.'.HALL')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="hall" id="hall" type="text" maxlength="100" value="{{$allinquiry->hall}}" readonly="readonly">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->occasion_type)){
          $ocid=$allinquiry->occasion_type_id; 
          $ocassionnameformhelper=Helper::getOccasionName($ocid); 
          $mctitle_name = 'title'; 
        if(Session::get('lang_file')!='en_lang')
          {
              $mctitle_name = 'title_ar'; 
            }
            if($ocid>0){ $octype=$ocassionnameformhelper->$mctitle_name; }else{  $octype=$allinquiry->occasion_type; }
       ?>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.OCCASION_TYPE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_TYPE'): trans($OUR_LANGUAGE.'.OCCASION_TYPE')}}</div>
                <div class="checkout-form-bottom">
                  <input type="text" maxlength="150" name="occasion" id="occasion" value="{{$octype}}" readonly="readonly" class="t-box" />
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->city_id)){ ?>
            <div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CITY')}}</div>
                <div class="checkout-form-bottom">
				  <?php 
				  $getcityname = Helper::getcity($allinquiry->city_id);
				  $mc_name='ci_name';if(Session::get('lang_file')!='en_lang'){$mc_name= 'ci_name_ar';} ?>
                  <input type="text" maxlength="150" name="city" id="city"  value="{{$getcityname->$mc_name or ''}}" readonly="readonly"  class="t-box" />
                </div>                
              </div>
            </div>
			<?php }if(!empty($allinquiry->location)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.LOCATION')!= '') {{ trans(Session::get('lang_file').'.LOCATION')}}  @else {{ trans($OUR_LANGUAGE.'.LOCATION')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" type="text" name="location" maxlength="100" id="location" value="{{$allinquiry->location or ''}}" readonly="readonly">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->duration)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.DURATION')!= '') {{ trans(Session::get('lang_file').'.DURATION')}}  @else {{ trans($OUR_LANGUAGE.'.DURATION')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="duration" id="duration" type="text" maxlength="50" value="{{$allinquiry->duration or ''}}" readonly="readonly">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->date)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.DATE')!= '') {{ trans(Session::get('lang_file').'.DATE')}}  @else {{ trans($OUR_LANGUAGE.'.DATE')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box cal-t datetimepicker" name="date" id="date" value="{{ date('d M Y',strtotime($allinquiry->date) )}} {{$allinquiry->time}}" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->recording_section)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.RECORDING_SECTION')!= '') {{ trans(Session::get('lang_file').'.RECORDING_SECTION')}}  @else {{ trans($OUR_LANGUAGE.'.RECORDING_SECTION')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="recording_section" id="recording_section" value="{{$allinquiry->recording_section or ''}}" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->music_type)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.MUSIC_TYPE')!= '') {{ trans(Session::get('lang_file').'.MUSIC_TYPE')}}  @else {{ trans($OUR_LANGUAGE.'.MUSIC_TYPE')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="music_type" id="music_type" value="{{$allinquiry->music_type or ''}}" readonly="readonly"  type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->singer_name)){

$mid=$allinquiry->music_id; 
          $singernameformhelper=Helper::nm_music($mid); 
          $singertitle_name = 'name'; 
        if(Session::get('lang_file')!='en_lang')
          {
              $singertitle_name = 'name_ar'; 
            }
            if($mid>0){ $ocsingertype=$singernameformhelper->$singertitle_name; }else{  $ocsingertype=$allinquiry->singer_name; }
       ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.SINGER_NAME')!= '') {{ trans(Session::get('lang_file').'.SINGER_NAME')}}  @else {{ trans($OUR_LANGUAGE.'.SINGER_NAME')}} @endif</div>
                <div class="checkout-form-bottom">
                  <input class="t-box" name="singer_name" id="singer_name" value="{{$ocsingertype}}" readonly="readonly" type="text">
                </div>
              </div>
            </div>
			<?php }if(!empty($allinquiry->occasion_type)){ ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')}}</div>
                <div class="checkout-form-bottom">
                  <input type="text" maxlength="150" name="occasion" id="occasion" value="{{$octype}}" readonly="readonly" class="t-box" />
                </div>
              </div>
            </div>
			<?php }  ?>
			<div class="checkout-form-row">
              <div class="checkout-form-cell">
                <div class="checkout-form-top">@if(Lang::has(Session::get('lang_file').'.Your_COMMENTS')!= '') {{ trans(Session::get('lang_file').'.Your_COMMENTS')}}  @else {{ trans($OUR_LANGUAGE.'.Your_COMMENTS')}} @endif</div>
                <div class="checkout-form-bottom">
                  <textarea class="text-area" name="comments" readonly="readonly" id="comments">{{$allinquiry->user_comment or ''}}</textarea>
                </div>
              </div>
            </div>
		 
			<!--div class="checkout-form-cell">
                <div class="checkout-form-top"></div>
                <div class="checkout-form-bottom">
                  <div class="gender-radio-box">
                    <input id="confirm" name="status" type="radio" value="3" @php if($allinquiry->status==3){echo 'Checked';} @endphp>
                    <label  for="confirm">{{ (Lang::has(Session::get('lang_file').'.CONFIRMED')!= '')  ?  trans(Session::get('lang_file').'.CONFIRMED'): trans($OUR_LANGUAGE.'.CONFIRMED')}}</label>
                  </div>
                  <div class="gender-radio-box">
                    <input id="deny" name="status" type="radio" value="4" @php if($allinquiry->status==4){echo 'Checked';} @endphp>
                    <label for="deny">{{ (Lang::has(Session::get('lang_file').'.DENY')!= '')  ?  trans(Session::get('lang_file').'.DENY'): trans($OUR_LANGUAGE.'.DENY')}}</label>
                  </div>
                </div>
                @if($errors->has('gender'))<span class="error">{{ $errors->first('gender') }}</span>@endif
              </div-->
              <!-- checkout-form-cell -->
            </div>
			<?php 
			if($allinquiry->status!=3){if($allinquiry->status!=4){ ?>
			<!--div class="myprf_btn">
        <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.UPDATE')!= '')  ?  trans(Session::get('lang_file').'.UPDATE'): trans($OUR_LANGUAGE.'.UPDATE')}}" class="form-btn btn-info-wisitech">
      </div-->
	  


  <div class="myprf_btn confirmandpay">
        <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.Confirm_and_pay')!= '')  ?  trans(Session::get('lang_file').'.Confirm_and_pay'): trans($OUR_LANGUAGE.'.Confirm_and_pay')}}" class="form-btn btn-info-wisitech">
      </div>
 <div class="myprf_btn deny">
        <input type="submit" name="submit" value="{{ (Lang::has(Session::get('lang_file').'.DENY_BTN')!= '')  ?  trans(Session::get('lang_file').'.DENY_BTN'): trans($OUR_LANGUAGE.'.DENY_BTN')}}" class="form-btn btn-info-wisitech">
      </div>


<?php }} ?>

            <!-- checkout-form-row -->
          </div>
		  
          <!-- checkout-form -->
        </div>      
		
        <!-- checkout-box -->
      </div>    
      
      </form>      
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
<script>
jQuery(document).ready(function(){
jQuery.validator.addMethod("laxEmail", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
}, "Please enter valid email address."); 
 jQuery("#change_password").validate({
    rules: {          
          "old_password" : {
            required : true
          },  
          "new_password" : {
            required : true,
            minlength: 8,
          },
          "confirm_password" : {
            required : true,
            equalTo: "#new_password"
          },              
         },
         messages: {
          "old_password": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_OLD_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_OLD_PASSWORD') }} @endif"
          },
          "new_password": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD') }} @endif",
            minlength:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NEW_PASSWORD_MIN') }} @endif"
          },
          "confirm_password": {
            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_CONFIRM_PASSWORD') }} @endif",
            equalTo:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PASSWORD_MATCH') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PASSWORD_MATCH') }} @endif"
          },          
         }
 });
 jQuery(".btn-info-wisitech").click(function() {
   if(jQuery("#change_password").valid()) {        
    jQuery('#change_password').submit();
   }
  });
  
   jQuery("#old_password").blur(function(){
   var str = jQuery('#old_password').val();
        jQuery.ajax({
		   type: 'get',
		   data: 'password='+str,
		   url: '<?php echo url('change-pass-imagewithajax'); ?>',
		   success: function(responseText)
		   {
			jQuery('#psserrror').html(responseText);
			//location.reload();
		  }       
		});
   });
});
</script>
@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_SECURITY')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_SECURITY'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_SECURITY')}}</h1>
      <div class="field_group security_wrap">
        <div class="main_user">
		 @if(count($productdetails) >0)
          <div class="myaccount-table">
            <div class="mytr">
              <div class="mytable_heading"></div>
			  <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.ORDERID')!= '')  ?  trans(Session::get('lang_file').'.ORDERID'): trans($OUR_LANGUAGE.'.ORDERID')}}</div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.OCCASION_TYPE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_TYPE'): trans($OUR_LANGUAGE.'.OCCASION_TYPE')}}</div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}</div>

              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.REFOUNDED_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.REFOUNDED_AMOUNT'): trans($OUR_LANGUAGE.'.REFOUNDED_AMOUNT')}}</div>

              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}</div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}</div>
              <!--<div class="mytable_heading order_id ">{{ (Lang::has(Session::get('lang_file').'.MY_PAYMENT_METHOD')!= '')  ?  trans(Session::get('lang_file').'.MY_PAYMENT_METHOD'): trans($OUR_LANGUAGE.'.MY_PAYMENT_METHOD')}}</div>-->
            </div>
            @php $i=1; @endphp
            @if(count($productdetails) > 0)
            @foreach($productdetails as $val)
            <div class="mytr">
              <div class="mytd" data-title="Sl. No.">{{$i}}</div>
			  <div class="mytd" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDERID')!= '')  ?  trans(Session::get('lang_file').'.ORDERID'): trans($OUR_LANGUAGE.'.ORDERID')}}"><?php echo '<a href="'.route('order-details',['id'=>$val->order_id]).'">'.$val->order_id.'</a>';  ?></div>
              <div class="mytd" data-title="Occasion Type"><a href="#">
              <?php 
              if(isset($val->search_occasion_id) && $val->search_occasion_id!='0')
              {
                $setTitle = Helper::getOccasionName($val->search_occasion_id);
                $mc_name='title';
                if(Session::get('lang_file')!='en_lang')
                {
                   $mc_name= 'title_ar';
                }                
                echo '<a href="'.route('order-details',['id'=>$val->order_id]).'">'.$setTitle->$mc_name.'</a>';
              }
              else
              {
                if(isset($val->main_occasion_id) && $val->main_occasion_id!='0')
                { 
                   if(Session::get('lang_file')!='en_lang')
                   {
                     $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
                   }
                   else
                   {
                    $getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
                   } 
                   foreach($getArrayOfOcc as $key=>$ocval)
                   {
                    if($val->main_occasion_id==$key)
                    {
                     $occasion_name = $ocval;
                    }
                   }
                 }
                echo '<a href="'.route('order-details',['id'=>$val->order_id]).'">'.$occasion_name.'</a>';
               }
              ?>

              </a></div>
              <div class="mytd" data-title="Amount">SAR {{ number_format($val->insurance_amount,2) }}</div>
              <div class="mytd refundtip" data-title="Refonded Amount">
                @php if(isset($val->refund_amount) && $val->refund_amount>0) { $reamu='SAR '.number_format($val->refund_amount,2); }else{ $reamu='N/A';} @endphp
                 {{$reamu}}
                 @php if(isset($val->refund_message) && $val->refund_message!='') { @endphp
                 <span class="refundtiptext">{{ $val->refund_message }}</span>
                 @php } @endphp
               </div>
              <div class="mytd" data-title="Date">{{ Carbon\Carbon::parse($val->order_date)->format('d M Y')}}</div>
              <div class="mytd" data-title="Status">
                @php if($val->refund_status>0){ 
                 if(Session::get('lang_file')!='en_lang')
                   {
                $st='ردها';
                }else{
                $st='Refunded';
              }

                 }else{ 

                 if(Session::get('lang_file')!='en_lang')
                   {
                 $st='مستردة';
               }else{
                $st='Refundable';
             }

               } @endphp

                {{ $st }} </div>
              <!--<div class="mytd order_id " data-title="Payment">Wallet</div>-->
            </div>
            @php $i++; @endphp
            @endforeach
            @else
            <div class="no-record-area" align="center">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
            @endif
          </div>
		  @else
		 <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
		 @endif
        </div>
      </div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 

<style>


.refundtip .refundtiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.refundtip:hover .refundtiptext {
  visibility: visible;
}
</style>
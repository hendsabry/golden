<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="right_col" role="main">
            <div id="appUser">
              <div class="page-title">
                <div class="mainView-details bordr-bg align-head">
             <div class="title_left title-block arabic-right-align">
            <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HALL_INSURANCE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_HALL_INSURANCE') }}</h3>
                         <div class="back-btn-area">
                       <a href="/admin/hall_insurance" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
                      </div>      
                </div>
              </div>
          <div class="clearfix"></div>
          <div class="viewListtab">
            <div class="userinformation bordr-bg paddzero small-width-column2">
              <ul class="paddleftright">
                  <li class="row ">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMER_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CUSTOMER_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMER_NAME') }}</label></div>
                  <div class="col-sm-9 col-xs-12"><p>{{$insurace->getUser[0]->cus_name}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_E-MAIL_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_E-MAIL_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_E-MAIL_ADDRESS') }}</label></div>
                  <div class="col-sm-9 col-xs-12">
                 <p>{{$insurace->getUser[0]->email}}</p>
                  </div>
                </li>
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO') }}</label></div>
                  <div class="col-sm-9 col-xs-12">
                 <p>{{$insurace->getUser[0]->cus_phone}}
                 </p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_VENDOR_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDOR_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDOR_NAME') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>{{$insurace->getMerchant[0]->mer_fname}} {{$insurace->getMerchant[0]->mer_lname}}</p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_E-MAIL_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_E-MAIL_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_E-MAIL_ADDRESS') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>{{$insurace->getMerchant[0]->mer_email}}</p>
                  </div>
                </li>
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>{{$insurace->getMerchant[0]->mer_address1}}
                  </p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_HALL_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HALL_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_HALL_NAME') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if(Session::get('admin_lang_file') == 'admin_ar_lang')
                    {{$insurace->getProduct[0]->pro_title_ar}}
                  @else
                  {{$insurace->getProduct[0]->pro_title}}
                @endif</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_BRANCH_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BRANCH_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_BRANCH_NAME') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if(Session::get('admin_lang_file') == 'admin_ar_lang')
                    {{$insurace->getProduct[0]->getCategory[0]->mc_name_ar}}
                  @else
                   {{$insurace->getProduct[0]->getCategory[0]->mc_name}}
                @endif
                    </p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                   @if(Session::get('admin_lang_file') == 'admin_ar_lang')
                    {{$insurace->getProduct[0]->getCategory[0]->address_ar}}
                  @else
                   {{$insurace->getProduct[0]->getCategory[0]->address}}
                @endif
                  </p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_BOOKING_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BOOKING_DATE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_BOOKING_DATE') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                    <?php echo(date('M d, Y', strtotime($insurace->order_date)));?>
                  </p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INSURANCE_AMT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INSURANCE_AMT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INSURANCE_AMT') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$insurace->getProduct[0]->Insuranceamount}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                    <button class="btn btn-primary" id="full_btn" type="reset">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_FULL_REFUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FULL_REFUND') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FULL_REFUND') }}</button>
                    <button class="btn btn-primary" id="partial_btn" type="reset">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PARTIAL_REFUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PARTIAL_REFUND') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PARTIAL_REFUND') }}</button>
                  </p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label></label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                    <div class="hidden" id="full">
                      <p>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_FULL_REFUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FULL_REFUND') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FULL_REFUND') }}</p>
                      <form action="/admin/hall_insurance/refund" id="demo-form2" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="amount" value="{{$insurace->getProduct[0]->Insuranceamount}}">
                      <input type="hidden" name="merchant_id" value="{{$insurace->order_merchant_id}}">
                      <input type="hidden" name="order_id" value="{{$insurace->order_id}}">
                      <input type="hidden" name="transation_id" value="{{$insurace->transaction_id}}">
                     <input type="text" id="title"  required class="form-control col-md-7 col-xs-12" value="{{$insurace->getProduct[0]->Insuranceamount}}" disabled="disabled">

                     <textarea class="form-control" rows="3"
                          name="note" id="my_form" placeholder='' maxlength="200"  required="required"></textarea>

                          <button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button>

                           <button class="btn btn-primary refund" type="button">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL') }}</button>
                           </div>
                         </form>
                           <div class="hidden" id="partial">
                            <p>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PARTIAL_REFUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PARTIAL_REFUND') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PARTIAL_REFUND') }}</p>
                            <form action="/admin/hall_insurance/refund" id="demo-form2" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="merchant_id" value="{{$insurace->order_merchant_id}}">
                          <input type="hidden" name="order_id" value="{{$insurace->order_id}}">
                          <input type="hidden" name="transation_id" value="{{$insurace->transaction_id}}">
                           <input type="text" id="title" name="amount" required class="form-control col-md-7 col-xs-12 partial" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">

                     <textarea class="form-control" rows="3"
                          name="note" id="my_form" placeholder='' maxlength="200" required></textarea>

                          <button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button>

                           <button class="btn btn-primary refund" type="button" >{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL') }}</button>
                            </form>
                           </div>
                  </div>

                </li>
              </ul>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript">
  $(document).on('click','#full_btn',function(){
    $('#partial').addClass('hidden');
     $('#full').removeClass('hidden');
    });
  $(document).on('click','#partial_btn',function(){
    $('#full').addClass('hidden');
     $('#partial').removeClass('hidden');
    });

  $('.partial').on('keyup keydown', function(e){
        if ($(this).val() > <?php echo($insurace->getProduct[0]->Insuranceamount);?> 
            && e.keyCode != 46
            && e.keyCode != 8
           ) {
           e.preventDefault();     
           $(this).val(<?php echo($insurace->getProduct[0]->Insuranceamount);?>);
        }
    });
  $(document).on('click','.refund',function(){
    $('#full').addClass('hidden');
     $('#partial').addClass('hidden');
    });
</script>
    <!-- END GLOBAL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>
    <!-- END BODY -->
</html>
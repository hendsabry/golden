<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
         <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="right_col" role="main">
  <div id="appUser">
    <div class="page-title">
      <div class="mainView-details bordr-bg">
        <div class="row">
        <div class="col-sm-6">
          <div class="title_left">
            <h3 class="padd">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_OCCASION_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OCCASION_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_OCCASION_DETAIL') }}</h3>
            <p>{{$occasion->occasion_name}}</p>
            <p class="paddNone"><?php echo date('d ,M Y', strtotime($occasion->created_at));?></p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="user-view-info">
            <div class="userImage marBotm">
              <figure>

                @if($occasion->images != null) <img src="{{ url('') }}{{ $occasion->images }}"> @else<img src="{{ url('') }}/public/assets/adimage/user-dummy.png">@endif
              </figure>
              <a href="/admin/occasions" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
            </div>
            <div class="view-userDetails">
              <div class="view-userDetails-inner">
                <h3>{{$occasion->occasion_name}}</h3>
                <p class="username">{{$occasion->occasion_venue}}</p>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      
    </div>
    <div class="clearfix"></div>
    <div class="viewListtab">
        <div class="userinformation bordr-bg paddzero small-width-column2">
              <ul class="paddleftright">
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_POSTED_BY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_POSTED_BY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_POSTED_BY') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if(isset($occasion->getUser)) {{$occasion->getUser->cus_name}} @endif</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if(isset($occasion->getUser)) {{$occasion->getUser->email}} @endif</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                  @if(isset($occasion->getUser) && $occasion->getUser->cus_phone != ''){{$occasion->getUser->country_code}}  {{$occasion->getUser->cus_phone}} @else N/A @endif</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_OCCASION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OCCASION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_OCCASION') }}</label></div>
                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p> @if(Session::get('admin_lang_file') == 'admin_ar_lang') {{$occasion->occasion_name_ar}} @else  {{$occasion->occasion_name}} @endif </p></div>
                </li>
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_VENUE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENUE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_VENUE') }}</label></div>
                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$occasion->occasion_venue}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_OCCASION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_OCCASION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_OCCASION') }}</label></div>
                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo date('d,M Y', strtotime($occasion->occasion_date ));?></p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESCRIPTION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DESCRIPTION') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if($occasion->description != ''){{$occasion->description}} @else N/A @endif</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_HASHTAGS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HASHTAGS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_HASHTAGS') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                  @if($occasion->hastags != ''){{$occasion->hastags}} @else N/A @endif</p></div>
                </li>
                  <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_IMAGES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IMAGES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGES') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                     <img src="{{ $occasion->images }}" class="maxwidth_img">
                     @php if(isset($occasionImage) && count($occasionImage)>0){ @endphp
                      @foreach($occasionImage as $img)
                     <img src="{{ $img->image_url }}" class="maxwidth_img">
                    @endforeach 
                    @php } @endphp
                  </p></div>
                </li>
    
              </ul>
              
            </div>
        </div>
  </div>
</div>
</div>
</div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('siteadmin.includes.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>

    <!-- END BODY -->
</html>
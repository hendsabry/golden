          <?php //print_r(Session::all()); exit();?>
          <!DOCTYPE html>
          <!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
          <!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
          <!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
          <!-- BEGIN HEAD -->
          <head>
          <meta charset="UTF-8" />
          <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
          <meta content="width=device-width, initial-scale=1.0" name="viewport" />
          <meta content="" name="description" />
          <meta content="" name="author" />
          <meta name="_token" content="{!! csrf_token() !!}"/>
          <!--[if IE]>
          <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
          <![endif]-->
          <!-- GLOBAL STYLES -->
          <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
          <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
          @if(Session::get('admin_lang_file') == 'admin_ar_lang')
          <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
          <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
          @endif
          <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
          <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
          <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
          @php 
          $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
          <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
          @endif
          <!--END GLOBAL STYLES -->

          <!-- PAGE LEVEL STYLES -->
          <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
          <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
          <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
          <link href="{{ url('') }}/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
          <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
          <!-- END PAGE LEVEL  STYLES -->
          <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
          <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]-->
          </head>

          <!-- END HEAD -->

          <!-- BEGIN BODY -->
          <body class="padTop53 " >

          <!-- MAIN WRAPPER -->
          <div id="wrap" >
          <!-- HEADER SECTION -->
          @include('admin.common.admin_header')
          <!-- END HEADER SECTION -->
          @include('admin.common.left_menu_common')

          <!--PAGE CONTENT -->
          <div class=" col-sm-9 col-xs-12 right-side-bar">
          <div class="right_col" role="main">
          @if(Session::has('message'))
          <div class="alert alert-danger no-border">
          <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
          @foreach($errors->all() as $error)
          <li>{!! $error !!}</li>
          @endforeach
          </div>
          @endif
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
          <div class="x_content padduser-block">
          <div class="top_back_btn">
          <div class="title-block">
          <h3>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_CURRENCY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EDIT_CURRENCY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_CURRENCY') }}</h3>
          </div>
          <div class="back-btn-area">
          <a href="/admin/currency" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
          </div>

          </div>

          <form action="/admin/currency/edit/{{ $users->cur_id }}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left form-section" enctype="multipart/form-data" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="form-group">
          <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_COUNTRY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY') }} <span class="required">*</span></label>
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <select name="cur_name" class="selectpicker form-control" data-live-search="true" required="required" id="country">
          <option  value="">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY') }}</option>
          @foreach($countries as $country)
          <option value="{{ $country->co_id }}" <?php if ($country->co_id == $users->cur_name): ?> selected
          <?php endif ?>>  @if(Session::get('admin_lang_file') == 'admin_ar_lang') {{ $country->co_name_ar }} @else {{ $country->co_name }} @endif</option>
          @endforeach
          </select>
          </div>
          </div>
          <div class="form-group">
          <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CONVERSION_RATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CONVERSION_RATE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CONVERSION_RATE') }} <span class="required">*</span>
          </label>
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <input type="text" id="cur_conversion_rate" name="cur_conversion_rate" required class="form-control col-md-7 col-xs-12" maxlength="10" value="{{ $users->cur_conversion_rate}}">
          </div>
          </div> 

          <div class="form-group">
          <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CURRENCY_SYMBOL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CURRENCY_SYMBOL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CURRENCY_SYMBOL') }} <span class="required">*</span>
          </label>
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <input type="text" id="cur_symbol" name="cur_symbol" required class="form-control col-md-7 col-xs-12" maxlength="50" value="{{ $users->cur_symbol }}">
          </div>
          </div> 
          <div class="form-group gender">
          <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DEFAULT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DEFAULT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DEFAULT') }}</label>
          <div class="col-md-3 col-sm-3 col-xs-6 marTop">                   
          <input type="radio" class="flat" name="cur_default" id="active" value="1" @if($users->cur_default == 1) checked @endif required />
          <label for="active"  class="gender-name">
          {{ (Lang::has(Session::get('admin_lang_file').'.BACK_YES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_YES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_YES') }} </label>
          </div> 
          <div class="col-md-3 col-sm-3 col-xs-6 marTop">                   
          <input type="radio" class="flat" name="cur_default" id="inactive" value="0" @if($users->cur_default == 0) checked @endif />
          <label for="inactive" class="gender-name">
          {{ (Lang::has(Session::get('admin_lang_file').'.BACK_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO') }}  </label>
          </div>
          </div>                   

          <div class="form-group gender">
          <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS') }}</label>
          <div class="col-md-3 col-sm-3 col-xs-6 marTop">                        
          <input type="radio" class="flat" name="cur_status" id="active" value="1" @if($users->cur_status == 1) checked @endif required /><label for="active" class="gender-name">
          {{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE') }} </label>
          </div> 
          <div class="col-md-3 col-sm-3 col-xs-6 marTop">                          
          <input type="radio" class="flat" name="cur_status" id="inactive" value="0" @if($users->cur_status == 0) checked @endif /><label for="inactive" class="gender-name">
          {{ (Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE') }}  </label>
          </div>
          </div> 

          <div class="ln_solid"></div>
          <div class="form-group">
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 col-md-offset-4">
          <a class="btn btn-primary" href="/admin/currency" type="button">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL') }}</a>
          <button class="btn btn-primary" type="reset">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }}</button>
          <button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button>
          </div>
          </div>
          </form>
          </div>
          </div>
          </div>

          </div>

          </div>
          <!--END MAIN WRAPPER -->

          <!-- FOOTER -->
          @include('admin.common.admin_footer')
          <!--END FOOTER -->
          <!-- GLOBAL SCRIPTS -->

          <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
          <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
          <!-- END GLOBAL SCRIPTS -->
          <!-- PAGE LEVEL SCRIPTS -->
          <!-- END PAGE LEVEL SCRIPTS -->
          <script type="text/javascript">
          $.ajaxSetup({
          headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
          });
          </script>

          <script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
          <script src="{{ url('')}}/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
          <script type="text/javascript">
          $(window).on('load',function(){
          $('#demo').datepicker({
          format: "dd-mm-yyyy",
          autoclose: true,
          endDate: '+0d'   
          });
          });
          </script>

          <script type="text/javascript">
          $(document).on('click', '.day', function () {
          $(".datepicker").css("display", "none");
          });

          $(document).on('click', '#demo', function () {
          $(".datepicker").css("display", "block");
          });
          </script>
          </body>

          <!-- END BODY -->
          </html>
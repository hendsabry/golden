<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
          <div class="right_col" role="main">
          <div id="appUserList">

            <div class="clearfix"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="page-title">
              <div class="title_left">
                <h3>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CURRENCY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CURRENCY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CURRENCY') }}</h3>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hight_overhid"> 
           <!--  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
              <div class="mainSeacrh_box">
              <div class="input-groupSearch clearfix search-form">
                <input v-model="search_keyword" value="" placeholder="{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH_BY_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SEARCH_BY_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH_BY_NAME') }}" autocomplete="off" class="form-control pr-Large20" id="navbar-search-input" type="text" @keyup="search">
              </div>
            </div>
            </div>  -->
           <!--  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
              <select v-model="status" class="form-control dtmagin">
                 <option value="" selected="selected">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PLEASE_SELECT_STATUS') }}</option>
                  <option  value="1">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE') }}</option>
                  <option  value="0">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE') }}</option>
              </select>
             </div> -->
             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<!-- <button type="submit" :disabled="isProcessing" @click="search" class="btn theme-default-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button>
<button type="submit" :disabled="isProcessing" @click="clearFilter" class="btn theme-default-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }}</button> -->
@if(Session::get('access_permission_type')>0)
  <button type="submit" :disabled="isProcessing" @click="addSales" class="btn theme-default-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_CURRENCY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADD_CURRENCY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_CURRENCY') }}</button>
  @endif
              </div>
            </div>
            </div>
                  <div class="x_content UsersList">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_COUNTRY_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY_NAME') }} </th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CURRENCY_SYMBOL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CURRENCY_SYMBOL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CURRENCY_SYMBOL') }} </th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CONVERSION_RATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CONVERSION_RATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CONVERSION_RATE') }}</th>

                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DEFAULT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DEFAULT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DEFAULT') }}</th> 
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_UPDATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LAST_UPDATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_UPDATE') }}</th>                    
                             
                             <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_STATUS') }}</th>

                              
                            <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION') }}</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-if="currency" v-for="ud in currency"> 
                              <td><p v-if="ud.country">@{{ ud.country.co_name}}</p></td>
                               <td>@{{ ud.cur_symbol}}</td>
                            <td>@{{ ud.cur_conversion_rate}}</td>                            
                            <td>@{{ud.cur_default}} </td>                         
                            <td>@{{ ud.last_update}}</td>
                            <td>
                              <div>
                                <label class="switch" >
                                 <input class="example2" type="checkbox" v-bind:id="ud.cur_id" v-bind:checked="ud.cur_status == 1" @click="statusChanged(ud.cur_id)">
                                  <div class="slider round"></div>
                                </label>
                                </div>
                            </td>
                            <td class="last">
                            <button  @click="ViewUser(ud.cur_id)" class="btn view-delete"><i class="fa fa-eye"></i> </button>
                              @if(Session::get('access_permission_type')>0)
                             <button @click="EditUser(ud.cur_id)" class="btn view-delete"><i class="fa fa-edit"></i> </button>
                             <button  @click="DeleteUser(ud.cur_id)" class="btn view-delete"><i class="fa fa-trash-o"></i> </button>
                             @endif
                            </td>
                          </tr>
                          
                          <tr v-if="currency ==''">
                            <td colspan="7">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND') }}</td></tr>
                        </tbody>
                      </table>
                    </div>      
                  </div>
                  <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">@{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
                </div>
              </div>
          </div>
        </div>
          </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._currency = [{!! $currency->toJson() !!}];
</script>
<script src="{{ url('')}}/public/assets/js/admin/currencylist.js"></script>  
</body>

    <!-- END BODY -->
</html>
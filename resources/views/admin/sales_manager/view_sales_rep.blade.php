<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="UTF-8" />
  <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
@if(Session::get('admin_lang_file') == 'admin_ar_lang')
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
@endif
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
@php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
<link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
<link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

  <!-- MAIN WRAPPER -->
  <div id="wrap" >
    <!-- HEADER SECTION -->
    @include('admin.common.admin_header')
    <!-- END HEADER SECTION -->
    @include('admin.common.left_menu_common')

    <!--PAGE CONTENT -->
    <div class=" col-sm-9 col-xs-12 right-side-bar">
     <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="right_col" role="main">
        <div id="appUser">
          <div class="page-title">
            <div class="mainView-details bordr-bg align-head">
            <div class="title_left title-block arabic-right-align">
                    <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_REP')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_REP') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_REP') }}</h3> 
                    <div class="back-btn-area">
                       <a href="/admin/sales_rep" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>   
                  </div>           
                  </div>
            </div>
          </div>
      <div class="clearfix"></div>
      <div class="viewListtab">
        <div class="userinformation bordr-bg paddzero small-width-column2">
          <ul class="paddleftright">
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->adm_fname}} {{$sub_admin->adm_lname}}</p></div>
            </li>

            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_HEAD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_HEAD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_HEAD') }} </label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sales_manager->adm_fname}} {{$sales_manager->adm_lname}}</p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS') }} </label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->adm_address1}}</p></div>
            </li>

            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo date('F j, Y', strtotime($sub_admin->created_at ));?></p></div>
            </li>
            <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AGE') }}</label></div>
                  <div  class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo((date('Y') - date('Y',strtotime($sub_admin->dob))));?> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_YEAR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_YEAR') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_YEAR') }}</p></div>
                </li>
             <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_CODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_CODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_CODE') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->code}}</p></div>
            </li>
          @if(!$offer_plan[0]->offer->isEmpty())  
           <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_OFFER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OFFER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_OFFER') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                <table style="width:100%">
                  <tr>
                    <th>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ID') }}</th>
                    <th>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_OFFER_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OFFER_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_OFFER_NAME') }}</th>
                    <th>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME') }}</th>
                  </tr>
                  @foreach($offer_plan[0]->offer as $key =>$value)
                  <tr>
                    <td>{{ $value['id'] }}</td> 
                  <td>{{ $value['offer_name'] }}</td> 
                  <td><?php echo date('d,M Y', strtotime($value['updated_at'] ));?></td>
                </tr>
                  @endforeach
                 
                </table>
                </div> 
              </li>
              @endif
               @if(!$offer_plan[0]->plan->isEmpty()) 
              <li class="row">
                <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PLAN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PLAN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PLAN') }}</label></div>
                <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <table style="width:100%">
                    <tr>
                      <th>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ID') }}</th>
                      <th>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PLAN_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PLAN_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PLAN_NAME') }}</th>
                      <th>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME') }}</th>
                    </tr>
                  @foreach($offer_plan[0]->plan as $key =>$value)
                  <tr>
                    <td>{{ $value['id'] }}</td> 
                  <td>{{ $value['plan_name'] }}</td> 
                  <td><?php echo date('d,M Y', strtotime($value['updated_at'] ));?></td>
                  </tr>
                  @endforeach
                </table>
                  </div> 
                </li>
                @endif

          </ul>

        </div>





          <div class="x_content UsersList">

            <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.Subscribed_Vendors')!= '') ?  trans(Session::get('admin_lang_file').'.Subscribed_Vendors') :  trans($ADMIN_OUR_LANGUAGE.'.Subscribed_Vendors') }}</h3> 
                      
                        <div class="table-responsive">
                      
                          <table class="table table-striped jambo_table bulk_action">
                            <thead>
                              <tr class="headings">
                                <th class="column-title"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME') }} </th>
                                <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL') }} </th>
                                <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NUMBER') }}</th>

                                 <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.ADDRESS') : trans($ADMIN_OUR_LANGUAGE.'.ADDRESS') }}</th>

                                <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN') }}</th>
                                
                                 
                               
                              </tr>
                            </thead>
                            <tbody>


                                @php $isvendor=count($assignedvendorlist);
                                    if($isvendor>0){
                                 @endphp
                                  @foreach($assignedvendorlist as $merchantlist)

                                  @php 
                                    $cityname=Helper::getcity($merchantlist->mer_ci_id);
                                    $con_name=Helper::getNationID($merchantlist->mer_co_id);

                                  @endphp
                              <tr>
                                
                                <td> {{ $merchantlist->mer_fname or ''}} {{ $merchantlist->mer_lname or ''}}</td>
                                <td> {{ $merchantlist->mer_email or ''}} </td>
                                <td> {{ $merchantlist->mer_phone or ''}}</td>
                                <td> {{ $merchantlist->mer_address1 or ''}}, {{ $merchantlist->mer_address2 or ''}} <br>
                                {{ $cityname->ci_name }}, {{ $con_name }} - {{ $merchantlist->zipcode or ''}} </td>
                                <td> <?php echo date('d,M Y', strtotime($merchantlist['last_login'] ));?></td>                      
                                
                              </tr>
                              @endforeach
                              @php }else{ @endphp

                                                            <tr >
                                <td colspan="7">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND') }}</td></tr>

                                @php } @endphp

                            </tbody>
                          </table>
                        </div>      
                      </div>






      </div>
    </div>
  </div>
</div>
<!--END MAIN WRAPPER -->

<!-- FOOTER -->
@include('admin.common.admin_footer')
<!--END FOOTER -->




<!-- GLOBAL SCRIPTS -->

<script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- END GLOBAL SCRIPTS -->

<!-- PAGE LEVEL SCRIPTS -->



<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>

<!-- END BODY -->
</html>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
     <meta charset="UTF-8" />
    <title>{{ $SITENAME}} | {{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_LOGIN')!= '') ? trans(Session::get('admin_lang_file').'.BACK_ADMIN_LOGIN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_LOGIN') }} </title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta name="_token" content="{!! csrf_token() !!}"/>

     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
     <!-- PAGE LEVEL STYLES -->
     <link rel="stylesheet" href="public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="public/assets/css/login.css" />
    <link rel="stylesheet" href="public/assets/plugins/magic/magic.css" />
	<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{$fav->imgs_name}}">
 @endif 
     <!-- END PAGE LEVEL STYLES -->
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
      @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach 
    <link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/ {{ $fav->imgs_name }} ">
 @endif  
</head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body style="background:url(assets/img/bg1.jpg) no-repeat center; -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;" oncontextmenu="return false" >

   <!-- PAGE CONTENT --> 
    <div class="container">
    <div class="text-center">
	   	  <img src="{{ $SITE_LOGO }}" alt="Logo" /></a>
       
    </div>
    
    
    <div class="tab-content">
    
        <div id="login" class="tab-pane active">
        
             {!! Form::open(array('url'=>'forget','class'=>'form-signin')) !!}
              @if (Session::has('login_error'))
		<div class="alert alert-danger alert-dismissable" id="error_div" align="center" style="height:50px;width:298px;">{!! Session::get('login_error') !!}</div>
		@endif
        @if (Session::has('login_success'))
		<div class="alert alert-success alert-dismissable" id="success_div" align="center" style="height:50px;width:298px;">{!! Session::get('login_success') !!}</div>
		@endif
            <p class="text-muted text-center btn-block  btn-primary    disabled">
                    {{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET_PASSWORD') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET_PASSWORD') }}
                </p>
                <input type="hidden" name="adm_id" value="{{$adm_id}}">
                <input type="hidden" name="adm_email" value="{{$adm_email}}">
                <input type="password" placeholder="{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PASSWORD'): trans($ADMIN_OUR_LANGUAGE.'.BACK_PASSWORD')}}" name="adm_password" class="form-control" required="required" onfocus="this.placeholder = ''" onblur="this.placeholder = '{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PASSWORD'): trans($ADMIN_OUR_LANGUAGE.'.BACK_PASSWORD')}}'"/ maxlength="20">
                <input type="password"  value="{{ Input::old('admin_name')}}" placeholder="{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CONFIRM_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CONFIRM_PASSWORD'): trans($ADMIN_OUR_LANGUAGE.'.BACK_CONFIRM_PASSWORD')}}" name="adm_password_confirm" class="form-control" tabindex="1" required="required" onfocus="this.placeholder = ''" onblur="this.placeholder = '{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CONFIRM_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CONFIRM_PASSWORD'): trans($ADMIN_OUR_LANGUAGE.'.BACK_CONFIRM_PASSWORD')}}'" maxlength="20" />
                <center><button class="btn text-muted text-center  btn-warning" type="submit">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT_PASSWORD') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT_PASSWORD') }}</button></center>
            {{ Form::close() }}
            
        </div>  
    </div>
</div>

	  <!--END PAGE CONTENT -->     
	      
      <!-- PAGE LEVEL SCRIPTS -->
   <script src="public/assets/plugins/jquery-2.0.3.min.js"></script>
   <script src="public/assets/plugins/bootstrap/js/bootstrap.js"></script>
   <script src="public/assets/js/login.js"></script>
      <!--END PAGE LEVEL SCRIPTS -->

 <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
</body>
    <!-- END BODY -->
</html>

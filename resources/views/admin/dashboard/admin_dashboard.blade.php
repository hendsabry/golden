<?php $current_route = Route::getCurrentRoute()->uri(); ?>
<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
   <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
   @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif 
        @php
        $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  
        @foreach($favi as $fav) 
        <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
        @endforeach
        @endif

    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<style>
 .navbar-top-links li {display: inline-block!important;}  
 .navbar-top-links li a{padding:6px 8px 6px 8px; margin-bottom:0; border-radius: 0px;} 

</style>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
         <!-- HEADER SECTION -->
        @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')
       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
            <div class="inner" style="min-height: 700px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box arabic-right-align_box">
                        	<header>
                <div class="icons"><i class="icon-dashboard"></i></div>
                <h5>@if(Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }} @endif</h5>
            </header>
             
  @php $sold_cnt=0; @endphp
 @foreach($soldproductscnt as $soldres)
	@if($soldres->pro_no_of_purchase >= $soldres->pro_qty)
	
		@php $sold_cnt++; @endphp

	@endif
 @endforeach

            	
                        <div style="text-align: center;"> 
                        <a class="quick-btn1" href="/admin/user_management">
                                <i class="icon-check icon-2x"></i>
                                <span>@if (Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMERS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_CUSTOMERS') }}  @else {{  trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMERS') }} @endif</span>
                                <span class="label label-danger">{{ $customers }}</span>
                            </a>                  
                              <a class="quick-btn1 active" href="/admin/vendor_management">
                                <i class="icon-check icon-2x"></i>
                                <span>@if (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANTS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_MERCHANTS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANTS') }} @endif</span>
                                <span class="label label-danger">{{ $merchantscnt }}</span>
                            </a>
							<a class="quick-btn1" href="/admin/sub_admin">
                                <i class="icon-check-minus icon-2x"></i>
                                <span>@if (Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN') }} @endif</span>
                                <span class="label label-success"> {{ $subadmins }}</span>
                            </a> 
                        </div>                        
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                            <div class="panel-heading heading-2">
                                 @if (Lang::has(Session::get('admin_lang_file').'.BACK_NEW_CUSTOMERS_MONTH_WISE')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_NEW_CUSTOMERS_MONTH_WISE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_NEW_CUSTOMERS_MONTH_WISE') }} @endif
                                                      </div>
                             
                            <div class="panel-body">   
                                @if($cus_count!='0,0,0,0,0,0,0,0,0,0,0,0')
                                   
							         <div class="demo-container bordercolr" id="chart1"></div>
                                @else
                                     No Customers List.
                                @endif         
							</div>
                            </div>
                    </div>
                </div>

                 <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                            <div class="panel-heading heading-2">
                             @if (Lang::has(Session::get('admin_lang_file').'.BACK_NEW_MERCHANTS_MONTH_WISE')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_NEW_MERCHANTS_MONTH_WISE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_NEW_MERCHANTS_MONTH_WISE') }} @endif
                                                      </div>
                             
                            <div class="panel-body">   
                                @if($merchant_chart!='0,0,0,0,0,0,0,0,0,0,0,0') 
                                     <div class="demo-container bordercolr" id="chart7"></div>
                                @else
                                     No Customers List.
                                @endif         
                            </div>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                                <div class="panel-heading heading-2">
                                   @if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_ONE_YEAR_TRANSACTIONS_REPORT') }} @endif
                                </div>
                             
                            <div class="panel-body">
                                   @if($transaction_chart!='0,0,0,0,0,0,0,0,0,0,0,0') 
                                  <div class="demo-container bordercolr" id="chart5" style="S width:1015px; height:470px;"></div>
                                  @else
                                    No Transactions to list.
                                  @endif  
                                </div>
                            </div>
                    </div> 
                </div>



                <div class="row hidden">
                    <div class="col-lg-12">
                         <div class="panel panel-default bordercolr">
                            <div class="panel-heading heading-2">
                               @if (Lang::has(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMER_AND_PRODUCT_COUNT_DEAL_COUNT')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_TOTAL_CUSTOMER_AND_PRODUCT_COUNT_DEAL_COUNT') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TOTAL_CUSTOMER_AND_PRODUCT_COUNT_DEAL_COUNT') }} @endif 
                            </div>
                             <div class=" panel-body pie-chart">
                            <div class="col-lg-4 ">
                            
			<div class="demo-container bordercolr">
			@if($admin_users+$fb_users+$website_users!=0)
			<div id="chart6" class="boxHeight"></div>
			 <div class="table-responsive relative-td">
            <table width="100%" border="0">
                              <tbody><tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_USER')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADMIN_USER') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_USER') }} @endif</label>
                                    <span class="label label-danger">{{ $admin_users }}</span>
                                </td>
                                 <td style="background:#eaa228 ">
                                    <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_FACEBOOK_USER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_FACEBOOK_USER') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_FACEBOOK_USER') }} @endif</label>
                                    <span class="label label-danger">{{ $fb_users }}</span>
                                </td>
                                
                                <td style="background:#C5B47F">
                                    <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE_USER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_WEBSITE_USER') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE_USER') }} @endif</label>
                                    <span class="label label-danger">{{ $website_users }}</span>
                                    
                                </td>                   
                              </tr>
                            </tbody></table></div>
				@else 
					No Customers List.
				@endif 
		    </div>
          
		</div>
       
       <div class="col-lg-4 ">
                              
			<div class="demo-container bordercolr"> 
			@if($activeproductscnt+$sold_cnt+$inactive_cnt!=0)
			<div id="chart10" class="boxHeight"></div>
			 <div class="table-responsive relative-td">
            <table width="100%" border="0">
                              <tbody><tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE_PRODUCTS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_ACTIVE_PRODUCTS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE_PRODUCTS') }}  @endif</label>
                                    <span class="label label-danger">{{ $activeproductscnt }}</span>
                                </td>
                                <td style="background:#eaa228">
                                <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SOLD_PRODUCTS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SOLD_PRODUCTS') }}   @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SOLD_PRODUCTS') }} @endif</label>
                                <span class="label label-danger">{{ $sold_cnt }}</span>
                                </td>
                                <td style="background:#C5B47F">
                                <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_INACTIVE_PRODUCTS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_INACTIVE_PRODUCTS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_INACTIVE_PRODUCTST') }}  @endif</label>
                                <span class="label label-danger">{{ $inactive_cnt }}</span>
                                </td>
                                 
                                                              
                              </tr>
                            </tbody></table> </div> 
				@else 
					No Product List.
				@endif 
		    </div>
          
		</div>
		
		<div class="col-lg-4 ">
                              
			<div class="demo-container bordercolr">
			@if($active_cnt+$archievd_cnt+$inactivedeal_cnt!=0)
			<div id="chart11" class="boxHeight"></div>
			      <div class="table-responsive relative-td">
            <table width="100%" border="0">
                              <tbody><tr>
                                <td style="background:#4bb2c5">
                                    <label class="label label-active">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE_DEALS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ACTIVE_DEALS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE_DEALS') }} @endif</label>
                                    <span class="label label-danger">{{ $active_cnt }}</span>
                                </td>
                                <td style="background:#eaa228">
                                <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_EXPIRED_DEALS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_EXPIRED_DEALS') }} @endif</label>
                                <span class="label label-danger">{{ $archievd_cnt }}</span>
                                </td>
                                <td style="background:#C5B47F">
                                    <label class="label label-archive">@if (Lang::has(Session::get('admin_lang_file').'.BACK_INACTIVE_DEALS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_INACTIVE_DEALS') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_INACTIVE_DEALS') }} @endif</label>
                                     <span class="label label-danger">{{ $inactivedeal_cnt }}</span>
                                </td> 
                                                              
                              </tr>
                            </tbody></table> </div>
				@else 
					No Deal List.
				@endif 
		    </div>
          
		</div>
                             
		</div>
                            </div>
                    </div>

                 
                </div>
                
            </div>

        </div>
</div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    @include('admin.common.admin_footer')    <!--END FOOTER -->
    @if(($admin_users+$fb_users+$website_users)>0)
    <script>
	 $(document).ready(function(){
		
	plot6 = $.jqplot('chart6', [[{{ $admin_users }},{{ $fb_users }},{{ $website_users }} ]], { seriesDefaults:{renderer:$.jqplot.PieRenderer}
	} );
		}); 
	</script>
    @endif
    @if(($activeproductscnt+$sold_cnt+$inactive_cnt)>0)
      <script>
	$(document).ready(function(){
		
	plot10 = $.jqplot('chart10', [[{{ $activeproductscnt }},{{ $sold_cnt }} , {{ $inactive_cnt }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer} });
		});
	</script>
    @endif
	@if(($active_cnt+$archievd_cnt+$inactivedeal_cnt)>0)
	 <script>

	$(document).ready(function(){
		
	plot11 = $.jqplot('chart11', [[{{ $active_cnt }} ,{{ $archievd_cnt }}, {{ $inactivedeal_cnt }} ]], {seriesDefaults:{renderer:$.jqplot.PieRenderer} });
		});
	</script>
    @endif
    @if($cus_count!='0,0,0,0,0,0,0,0,0,0,0,0')
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
		
		{{ $s1 = "[" .$cus_count. "]" }}
        var s1 =  {{ $s1 }}
        var ticks = ['@if (Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= '') {{   trans(Session::get('admin_lang_file').'.BACK_JANUARY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_FEBRUARY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_MARCH') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_APRIL') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL') }}@endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MAY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY') }} @endif','@if (Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_JUNE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_JULY')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_AUGUST')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER')}} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_OCTOBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER')}}  @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_NOVEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER')}} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DECEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER')}} @endif'];
        
        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickOptions:{
                            formatString:'%b&nbsp;%#d'
                      },
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart1').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    @endif  
     @if($merchant_chart!='0,0,0,0,0,0,0,0,0,0,0,0')
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true;
        
        {{ $s1 = "[" .$merchant_chart. "]" }}
        var s1 =  {{ $s1 }}
        var ticks = ['@if (Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= '') {{   trans(Session::get('admin_lang_file').'.BACK_JANUARY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_FEBRUARY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_MARCH') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_APRIL') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL') }}@endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MAY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY') }} @endif','@if (Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_JUNE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_JULY')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_AUGUST')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER')}} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_OCTOBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER')}}  @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_NOVEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER')}} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DECEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER')}} @endif'];
        
        plot1 = $.jqplot('chart7', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    tickOptions:{
                            formatString:'%b&nbsp;%#d'
                      },
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart7').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
    @endif   
    @if($transaction_chart!='0,0,0,0,0,0,0,0,0,0,0,0')
    <script class="code" type="text/javascript">$(document).ready(function(){
        $.jqplot.config.enablePlugins = true; 
        {{  $s1 = "[" .$transaction_chart. "]" }}
        {{ $s2 = "[" .$transaction_credit. "]" }}
        var s1 = {{ $s1 }}
         var s2 = {{ $s2 }}
       
        //var ticks = ['Jan', 'Feb', 'Mar', 'Apr', 'May','June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
         var ticks = ['@if (Lang::has(Session::get('admin_lang_file').'.BACK_JANUARY')!= '') {{   trans(Session::get('admin_lang_file').'.BACK_JANUARY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JANUARY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_FEBRUARY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_FEBRUARY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_FEBRUARY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_MARCH')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_MARCH') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MARCH') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_APRIL')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_APRIL') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_APRIL') }}@endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_MAY')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_MAY') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_MAY') }} @endif','@if (Lang::has(Session::get('admin_lang_file').'.BACK_JUNE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_JUNE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JUNE') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_JULY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_JULY')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_JULY') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_AUGUST')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_AUGUST')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_AUGUST') }} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEPTEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SEPTEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEPTEMBER')}} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_OCTOBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_OCTOBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_OCTOBER')}}  @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_NOVEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_NOVEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_NOVEMBER')}} @endif', '@if (Lang::has(Session::get('admin_lang_file').'.BACK_DECEMBER')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_DECEMBER')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DECEMBER')}} @endif'];
        
        plot1 = $.jqplot('chart5', [s1,s2], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults:{
                renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });
    
        $('#chart5').bind('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
            }
        );
    });</script>
     @endif     
    <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.jqplot.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('')}}/public/assets/js/chart/jqplot.barRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jqplot.pieRenderer.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jqplot.pointLabels.min.js"></script>
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="{{ url('') }}/public/assets/js/bootstrap-datepicker.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.js"></script>
    <script src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.resize.js"></script>
    <script  src="{{ url('')}}/public/assets/plugins/flot/jquery.flot.categories.js"></script>
    <script  src="{{ url('') }}/public/assets/plugins/flot/jquery.flot.errorbars.js"></script>
	<script  src="{{ url('') }}/public/assets/plugins/flot/jquery.flot.navigate.js"></script>
    <script  src="{{ url('') }}/public/assets/plugins/flot/jquery.flot.stack.js"></script>    
    <script src="{{ url('') }}/public/assets/js/bar_chart.js"></script>
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>


</body>

    <!-- END BODY -->
</html>
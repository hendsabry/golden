<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="UTF-8" />
  <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
@if(Session::get('admin_lang_file') == 'admin_ar_lang')
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
@endif
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
@php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
<link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
<link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

  <!-- MAIN WRAPPER -->
  <div id="wrap" >
    <!-- HEADER SECTION -->
    @include('admin.common.admin_header')
    <!-- END HEADER SECTION -->
    @include('admin.common.left_menu_common')

    <!--PAGE CONTENT -->
    <div class=" col-sm-9 col-xs-12 right-side-bar">
      <div class="right_col" role="main">
        <div class="" id="appUser">
          <div class="page-title">
            <div class="mainView-details">
              <div class="row">
                <div class="col-sm-6">
                  <div class="title_left">
                    <h3 class="padd">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_VENDOR_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDOR_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDOR_DETAIL') }}</h3>            
                  </div>
                </div>
                <div class="view-userDetails">
                  <div class="view-userDetails-inner">  
                    <a href="/admin/vendor_management" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="clearfix"></div>
      <div class="viewListtab">
        <div class="userinformation paddzero">
          <ul class="paddleftright">
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME') }}</label></div>
              <div class="col-sm-9"><p>{{$sub_admin->mer_fname}} {{$sub_admin->mer_lname}}</p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL') }}</label></div>
              <div class="col-sm-9"><p>{{$sub_admin->mer_email}}</p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_PHONE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_PHONE') }}</label></div>
              <div class="col-sm-9"><p>{{$sub_admin->mer_phone}}</p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_GENDER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GENDER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GENDER') }}</label></div>
              <div class="col-sm-9"><p>{{$sub_admin->gender}}</p></div>
            </li>

            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AGE') }} </label></div>
              <div class="col-sm-9"><p>{{$sub_admin->age}} Years</p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_BIRTH') }} </label></div>
              <div class="col-sm-9"><p><?php echo date('F j, Y', strtotime($sub_admin->dob ));?></p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS') }} </label></div>
              <div class="col-sm-9"><p>{{$sub_admin->mer_address1}}</p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN') }} </label></div>
              <div class="col-sm-9"><p></p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS') }} </label></div>
              <div class="col-sm-9"><p>{{$sub_admin->mer_staus}}</p></div>
            </li>
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AVAIL_PLAN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AVAIL_PLAN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AVAIL_PLAN') }}</label></div>
              <div class="col-sm-9"><p></p></div>
            </li> 
            <li class="row">
              <div class="col-sm-3"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_WALLET_AMT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_WALLET_AMT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_WALLET_AMT') }} </label></div>
              <div class="col-sm-9"><p>{{$sub_admin->wallet}}</p></div>
            </li>
          </ul>
           <form action="/admin/vendor_management/vendor_payment/{{$sub_admin->mer_id}}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">                    
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ORDER_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ORDER_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ORDER_ID') }} <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="lead_name" name="order_id" required="required" class="form-control col-md-7 col-xs-12" maxlength="20" value="{{$pay->order_id}}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_DETAIL') }} <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="lead_name" name="payment_detail" required="required" class="form-control col-md-7 col-xs-12" maxlength="200">
                  </div>
                </div>    
                                 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT') }} <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="client_email" name="amount" required="required" class="form-control col-md-7 col-xs-12" maxlength="50" value="{{$pay->amount}}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_ID') }}<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="transation_id" name="transation_id" required="required" class="form-control col-md-7 col-xs-12" maxlength="10"> 
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_MODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_MODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_MODE') }}<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="target" name="payment_mode" required="required" class="form-control col-md-7 col-xs-12" maxlength="100"> 
                  </div>
                </div>                      
                
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE') }}<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="demo" name="date" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NOTES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NOTES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NOTES') }}<span class="required"></span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea class="form-control" rows="3"
                    name="notes" id="my_form" placeholder='{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NOTES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NOTES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NOTES') }}' maxlength="200"></textarea>
                  </div>
                </div>                          
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a class="btn btn-primary" href="/admin/vendor_management" type="button">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL') }}</a>
                    <button class="btn btn-primary" type="reset">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }}</button>
                    <button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button>
                  </div>
                </div>
              </form>

        </div>
      </div>
    </div>
  </div>
</div>
<!--END MAIN WRAPPER -->

<!-- FOOTER -->
@include('admin.common.admin_footer')
<!--END FOOTER -->




<!-- GLOBAL SCRIPTS -->
<script src="{{ url('')}}/public/assets/js/bootstrap-datepicker.js"></script> 
<script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- END GLOBAL SCRIPTS -->

<!-- PAGE LEVEL SCRIPTS -->
<script>
    $( function() {
      $( "#demo" ).datepicker({ format: 'dd-mm-yyyy'});
    } );
</script>
<script type="text/javascript">
$(document).on('click', '.day', function () {
 $(".datepicker").css("display", "none");
});

$(document).on('click', '#demo', function () {
 $(".datepicker").css("display", "block");
});
</script>


<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>

<!-- END BODY -->
</html>
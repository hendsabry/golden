<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
   @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
                    <div class="right_col" role="main">
          <div id="appVendorList">

            <div class="clearfix"></div>

            <div class="">
             <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    
                    <div class="page-title padd">
              <div class="col-md-12 col-sm-12 col-xs-12 hight_overhid">
              <div class="title-block arabic-right-align">
                <h3>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_MAIN_CATEGORY_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MAIN_CATEGORY_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_MAIN_CATEGORY_DETAIL') }}
                </h3>
                 <div class="back-btn-area">
                       <a href="/admin/category_management/main_category/{{$subCategory_main->parent_id}}" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
              </div>
              <div class="cateblock clearboth">
               <!--  <figure class="img-block"><img src="{{ url('') }}{{ $subCategory_main->mc_img }}"/></figure> -->
               <h2> @if(Session::get('admin_lang_file') == 'admin_ar_lang')
                {{ $subCategory_main->mc_name_ar }}
                @else
                {{ $subCategory_main->mc_name }}
                @endif
                </h2></div>
              </div>
               <div class="col-md-12 col-sm-12 col-xs-12 hight_overhid">
                <div class="row">
               <div class="right-panel_active">
              <!--  <div class="col-md-4 col-sm-3 col-xs-3"> </div>
                 <div class="col-md-3 col-sm-3 col-xs-3">                 
                  <h5>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DESCRIPTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESCRIPTION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DESCRIPTION') }}</h5>
                  </div>
                <div class="col-md-5 col-sm-3 col-xs-3"> 
                <p>{{ strip_tags( $subCategory_main->mc_discription ) }}</p>
                  </div>
              
              </div> -->

              
              </div>

             </div>
            <input id="lang" class="form-control" value="{{Session::get('admin_lang_code')}}" type="hidden">
              <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="sub-category_heading">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LIST_OF_ASSOCIATED_VENDORS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LIST_OF_ASSOCIATED_VENDORS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_LIST_OF_ASSOCIATED_VENDORS') }}</h4></div> 
              </div>
              
                  <div class="x_content VendorList">
                 
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO') }} </th>
                          
                            <th class="column-title"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_NAME') }} </th>

                            <th class="column-title"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_EMAIL') }} </th>
                            <th class="column-title"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_PHONE_NUMBER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_PHONE_NUMBER') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_PHONE_NUMBER') }} </th>
                            <th class="column-title"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CATEGORIES_MANAGEMENT_LAST_LOGGED_IN') }} </th>
                            <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION') }}</span>
                            </th> 
                          </tr>
                        </thead>
                        @if(!empty($vendorlist))
                        <tbody> 
                        <?php $i = 1;                         
                          foreach($vendorlist as $key =>$value)
                          { 
						   $catid = Helper::getAllCategoryById($id,$id2);
						   //echo $catid->vendor_id;
						  ?>
                           <tr>
                          <td><?php echo  $i;?> </td> 
                          <td>{{ $value['mer_fname'] }}{{ $value['mer_lname'] }}</td> 
                          <td>{{ $value['mer_email'] }}</td>   
                          <td>{{ $value['mer_phone'] }}</td> 
                          <td>{{ $value['last_login'] }}</td>   
                          <td><a href="/admin/category_management/category/vendor_detail/{{ $value['mer_id'] }}" class="btn view-delete"><i class="fa fa-eye"></i></a></td>                 
                          </tr>
                          <?php  
						  $i++;
						  }
						  ?>                       
                         
                           @else 
                           <tr>
                            <td colspan="7">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND') }}</td>
                          </tr>                
                          @endif 
                        </tbody>
                      </table>
                    </div>        
                  </div>
                  <!-- <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">@{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
          </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._vendorlist = [{!! $vendorlist->toJson() !!}];
</script>
<script src="{{ url('')}}/public/assets/js/admin/vendorslist.js"></script>  
</body>

    <!-- END BODY -->
</html>
<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
          <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="right_col" role="main">
            <div class="" id="appUser">
              <div class="page-title">
                <div class="mainView-details bordr-bg">
                  <div class="title_left title-block arabic-right-align">
                        <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION') }}</h3>
                         <div class="back-btn-area ">
                       <a href="/admin/wallet" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
                      </div>      
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="viewListtab">
            <div class="userinformation bordr-bg paddzero small-width-column2">
              <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title numrical_2"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO') }} </th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_TYPE') }} </th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PAYER_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYER_TYPE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYER_TYPE') }}</th>
                            <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT') }}</span>
                            </th>
                            <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME') }}</span>
                            </th>
                            <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NOTES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NOTES') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NOTES') }}</span>
                            </th>
                             <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE_WALLET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION_TYPE_WALLET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION_TYPE_WALLET') }}</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                          $i = 1
                          @endphp
                          @foreach($wallets as $val)
                          <tr>
                            <td>{{$i}}</td>
                            <td>{{$val->transction_id}}</td>
                            <td>{{$val->getVendor[0]->mer_fname}} {{$val->getVendor[0]->mer_lname}}</td>
                            <td>{{$val->amount}}</td>
                            <td>{{ $val->created_at}}</td>
                            <td>{{ $val->notes}}</td>
                            <td>@if($val->transaction_type == 1) Cr @else Dr @endif</td>
                          </tr>
                          @php
                          $i = 1+$i
                          @endphp
                          @endforeach
                          @if(!isset($wallets))
                          <tr>
                            <td colspan="7">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND') }}</td>
                          </tr>
                          @endif
                        </tbody>
                      </table> 
              </div>  
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript">
    $('.partial').on('keyup keydown', function(e){
     if ($(this).val() > <?php echo($marchant->wallet);?> 
            && e.keyCode != 46
            && e.keyCode != 8
           ) {
           e.preventDefault();     
           $(this).val(<?php echo($marchant->wallet);?>);
        }
    });
</script>
    <!-- END GLOBAL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>
    <!-- END BODY -->
</html>
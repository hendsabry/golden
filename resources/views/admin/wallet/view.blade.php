<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class="col-sm-9 col-xs-12 right-side-bar">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="right_col" role="main">
            <div id="appUser">
              <div class="page-title">
                <div class="mainView-details bordr-bg">
                  <div class="title-block arabic-right-align">
                        <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DEDUCT_BALANCE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DEDUCT_BALANCE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DEDUCT_BALANCE') }}</h3>
                         <div class="back-btn-area ">
                       <a href="/admin/wallet" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
                      </div>      
                </div>
              </div>
          <div class="clearfix"></div>
          <div class="viewListtab">
            <div class="userinformation bordr-bg paddzero small-width-column2">
              <ul class="paddleftright">
                <li class="row">
                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><label></label></div>
                  <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                           <div class="" id="partial"><!--
                            <p></p>-->
                            <form action="/admin/payment/mer-deduct" id="demo-form2" class="form-horizontal form-label-left paddNone form-padding" enctype="multipart/form-data" method="post">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="merchant_id" value="{{$marchant->mer_id}}">
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="Amount">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT') }}</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
             <input type="text" id="title" name="amount" required class="form-control col-md-7 col-xs-12 partial" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"></div>
        </div>
                           
                           <div class="form-group">
                          <label class="control-label col-md-4 col-sm-4 col-xs-12" for="transaction">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION') }}</label>            
                          <div class="col-md-8 col-sm-8 col-xs-12"> <input type="text" id="title" name="transaction_id" required placeholder="{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ENTER_TRANSACTION_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ENTER_TRANSACTION_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ENTER_TRANSACTION_ID') }}" class="form-control col-md-7 col-xs-12" maxlength="25"></div>
                           </div>
                          <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="transaction">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NOTES')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NOTES') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NOTES') }}</label>
                         <div class="col-md-8 col-sm-8 col-xs-12">
                          <textarea class="form-control" rows="3" name="notes" id="my_form" placeholder='' maxlength="200" required></textarea></div></div>
                         
                         <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
             @if($marchant->wallet > 0)
                          <div class="col-md-8 col-sm-8 col-xs-12"><button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button></div>
                          @endif
                          </div>
                            </form>
                           </div>
                  </div>

                </li>
              </ul> 
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript">
  $('.partial').on('keyup keydown', function(e){
     if ($(this).val() > <?php echo($marchant->wallet);?> 
            && e.keyCode != 46
            && e.keyCode != 8
           ) {
           e.preventDefault();     
           $(this).val(<?php echo($marchant->wallet);?>);
        }
    });
</script>
    <!-- END GLOBAL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>
    <!-- END BODY -->
</html>
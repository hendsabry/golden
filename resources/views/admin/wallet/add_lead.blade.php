<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="UTF-8" />
  <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
@if(Session::get('admin_lang_file') == 'admin_ar_lang')
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
@endif
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
@php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
<link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
<link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<link href="{{ url('') }}/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
<script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

  <!-- MAIN WRAPPER -->
  <div id="wrap" >
    <!-- HEADER SECTION -->
    @include('admin.common.admin_header')
    <!-- END HEADER SECTION -->
    @include('admin.common.left_menu_common')

    <!--PAGE CONTENT -->
    <div class=" col-sm-9 col-xs-12 right-side-bar">
      <div class="right_col" role="main">
        @if(Session::has('message'))
        <div class="alert alert-danger no-border">
          <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
          @foreach($errors->all() as $error)
          <li>{!! $error !!}</li>
          @endforeach
        </div>
        @endif
        @if(Session::has('warning'))
        <div class="alert alert-danger no-border">
          <button type="button" class="close" data-dismiss="alert" id="warning"><span>×</span><span class="sr-only">Close</span></button>
          <li>{!! Session::get('warning') !!}</li>
        </div>
        @endif
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content padduser-block">
              <div class="title-block">
                <h3>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NEW_LEAD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NEW_LEAD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NEW_LEAD') }}</h3>
                <div class="back-btn-area ">
                  <a href="/admin/lead" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                </div>
              </div>
              <form action="/admin/lead/add" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">                    
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LEAD_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LEAD_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LEAD_NAME') }} <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="lead_name" name="lead_name" required="required" class="form-control col-md-7 col-xs-12" maxlength="20">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SERVICE_CATEGORY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICE_CATEGORY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICE_CATEGORY') }} <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="service_cat_sub_cat" id="service_cat_sub_cat" required="required">
                      <option>Select Category</option>
                      @foreach($cat as $events)
                      <option value="{{ $events->mc_id }}"> {{ $events->mc_name }} </option>
                      @endforeach
                    </select>  
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_EXECUTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_EXECUTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_EXECUTIVE') }} <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="sales_executive" id="sales_executive" required="required">
                      <option>Select Sales Rep</option>
                      @foreach($sales_executive as $events)
                      <option value="{{ $events->adm_id }}"> {{ $events->adm_fname }}{{ $events->adm_lname }} 
                      </option>
                      @endforeach
                    </select>  
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_HEAD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_HEAD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_HEAD') }}<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="sales_head" id="sales_head">
                      <option value="{{ $user->adm_id }}"> {{ $user->adm_fname }} </option>
                    </select> 
                  </div>
                </div>                     
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CLIENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CLIENT_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CLIENT_EMAIL') }} <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" id="client_email" name="client_email" required="required" class="form-control col-md-7 col-xs-12" maxlength="50">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CLIENT_PHONE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CLIENT_PHONE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CLIENT_PHONE') }}<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="client_ph" name="client_ph" required="required" class="form-control col-md-7 col-xs-12" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"> 
                  </div>
                </div>                      
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS') }}<span class="required"></span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea class="form-control" rows="3"
                    name="address" id="my_form" placeholder='{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS') }}' maxlength="200"></textarea>
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE') }}<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="demo" name="date_time" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS') }}</label>
                  <div class="col-md-3 col-sm-3 col-xs-6">
                    <label for="active">
                    {{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE') }} </label>
                    <input type="radio" class="flat" name="status" id="active" value="1" checked="" required />
                  </div> 
                  <div class="col-md-3 col-sm-3 col-xs-6">
                    <label for="inactive">
                    {{ (Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE') }}  </label>
                    <input type="radio" class="flat" name="status" id="inactive" value="0" />
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_TARGET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TARGET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_TARGET') }}<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="target" name="target" required="required" class="form-control col-md-7 col-xs-12" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"> 
                  </div>
                </div>                     
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a class="btn btn-primary" href="/admin/lead" type="button">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL') }}</a>
                    <button class="btn btn-primary" type="reset">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }}</button>
                    <button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>

    </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <!-- PAGE LEVEL SCRIPTS -->  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
      $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
      });
    </script>

    <script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
    <script src="{{ url('')}}/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
        $('#demo').datepicker({
          format: "dd-mm-yyyy",
          autoclose: true,
          todayHighlight: true,
          endDate: '+0d'   
        });
      });
    </script>

    <script type="text/javascript">
      $(document).on('click', '.day', function () {
        $(".datepicker").css("display", "none");
      });

      $(document).on('click', '#demo', function () {
        $(".datepicker").css("display", "block");
      });
    </script>
    <script type="text/javascript">
      $(document).on('click','#warning',function(){
        $.ajax({
          url: "/session_flush",
          data: {},
          type: "POST",
          cache: false,
          success: function(html){
          }
        });
      });
    </script>
    <script>
      function ValidateEmail(inputText)
      {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(inputText.value.match(mailformat))
        {
          document.form1.adm_email.focus();
          return true;
        }
        else
        {
          alert("You have entered an invalid email address!");
          document.form1.adm_email.focus();
          return false;
        }
      }
    </script>

  </body>

  <!-- END BODY -->
  </html>
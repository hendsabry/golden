<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>

    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
   @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class="col-sm-9 col-xs-12 right-side-bar">
                    <div class="right_col" role="main">
          <div id="appUserList">
            <div class="clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  
                    <div class="page-title">
              <div class="title_left">
                  <h3>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_WALLET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_WALLET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_WALLET') }}</h3>
              </div>             
              <input id="lang" class="form-control" value="{{Session::get('admin_lang_code')}}" type="hidden">
            </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hight_overhid">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="mainSeacrh_box">
                    <div class="input-groupSearch clearfix search-form">
                      <input id="from_date"  placeholder="{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_DATE') }}" class="form-control" type="text">
                    </div>
                  </div>
                </div>  
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
                  <button type="submit" :disabled="isProcessing" @click="search" class="btn theme-default-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}</button>
                  <button type="reset" :disabled="isProcessing" @click="clearFilter" class="btn theme-default-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }}</button>
                </div>
              </div>

        <div class="add-manage-details">
        <ul class="nav nav-tabs">
        <li id="en" class="active"><a data-toggle="tab" href="#home">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_VENDOR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDOR') : trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDOR') }}</a></li>
        <!-- <li id="ar"><a data-toggle="tab" href="#menu1">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USERS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USERS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USERS') }}</a></li> -->
        </ul>
        <div class="tab-content">
        <div id="home" class="tab-pane bordr-bg fade in active english-sec">
          <div class="x_content x_content_main UsersList">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title numrical_2"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME') }} </th>
                            <th class="column-title numrical_2"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL') }} </th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT') }} (SAR)</th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE') }}</th>
                            <th class="column-title "><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AVAIL_BALANCE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AVAIL_BALANCE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AVAIL_BALANCE') }}</span>
                            </th>                           
                             <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION') }}</span>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-if="marchant" v-for="ud in marchant">                      
                           <td>@{{ ud.mer_fname }}</td>
                           <td>@{{ ud.mer_email }}</td>
                            <td>@{{ ud.last_amount }}</td>
                            <td>@{{ ud.created }}</td> 
                            <td>@{{ ud.wallet }}</td>
                            <td class="last">
                              <button  @click="Transaction(ud.mer_id)" class="btn view-delete"><i class="fa fa-dollar">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_TRANSACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TRANSACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TRANSACTION') }}</i> </button>
                            <button  @click="View(ud.mer_id)" class="btn view-delete"><i class="fa fa-eye">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DEDUCT_BALANCE_1')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DEDUCT_BALANCE_1') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DEDUCT_BALANCE_1') }}</i> </button>
                            </td>
                          </tr>
                          <tr v-if="marchant ==''">
                            <td colspan="7">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND') }}</td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination_1.current_page_1 > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage_1(pagination_1.current_page_1 - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber_1"
              v-bind:class="[ page == isActived_1 ? 'active' : '']"> <a href="#"
              @click.prevent="changePage_1(page)">@{{ page }}</a> </li>
              <li v-if="pagination_1.current_page_1 < pagination_1.last_page_1"> <a href="#" aria-label="Next"
              @click.prevent="changePage_1(pagination_1.current_page_1 + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
              </div>        
              </div>

        </div>
        <!-- <div id="menu1" class="tab-pane bordr-bg fade arabic_sec">
          <div class="x_content x_content_main UsersList">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title numrical_2"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NAME') : trans($ADMIN_OUR_LANGUAGE.'.BACK_NAME') }} </th>
                            <th class="column-title numrical_2"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL') }} </th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AMOUNT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AMOUNT') }} (SAR)</th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE') }}</th>
                            <th class="column-title "><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AVAIL_BALANCE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AVAIL_BALANCE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_AVAIL_BALANCE') }}</span>
                            </th>                           
                          </tr>
                        </thead>
                        <tbody>
                          <tr v-if="users" v-for="(ud, index) in users">
                           
                           <td>@{{ ud.cus_name}}</td>
                           <td>@{{ ud.email }}</td>
                            <td>@{{ ud.last_amount }}</td>
                            <td>@{{ ud.created}}</td>                         
                            <td>@{{ ud.wallet}}</td>
                                                      
                          </tr>
                          <tr v-if="users ==''">
                            <td colspan="7">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RECORD_NOT_FOUND') : trans($ADMIN_OUR_LANGUAGE.'.BACK_RECORD_NOT_FOUND') }}</td>
                          </tr>
                        </tbody>
                      </table>
              <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">@{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
                    </div>        
                  </div>
    </div> -->
        </div>
        </div>
        </div>
                </div>
            </div>            
          </div>
        </div>
          </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="{{ url('')}}/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
      $('#from_date').datepicker({
        format: "MM dd, yyyy",
        autoclose: true,
        todayHighlight: true    
        });

        $('#from_date').on('change',function(){
          var dated = $(this).val();
        });
      });
    </script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">
window._marchant = [{!! $marchant->toJson() !!}];
// window._users = [{!! $users->toJson() !!}];

</script>
<script src="{{ url('')}}/public/assets/js/admin/walletlist.js"></script>  
</body>

    <!-- END BODY -->
</html>
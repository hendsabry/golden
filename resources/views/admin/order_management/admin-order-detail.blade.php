<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class="col-sm-9 col-xs-12 right-side-bar">
            <div class="right_col" role="main">
              <div id="appUserList">
                <div class="clearfix"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    
                                <div class="x_content UsersList">
                     
                                     
									   
									   
									   
									   
									   
									  <div class="main_user">
		  <?php 
		  //echo '<pre>';print_r($productdetails);die;
		  if(!count($productdetails)<1){?>
          <div class="types_ocss">
            <div class="types_ocs_left">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('admin_lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('admin_lang_file').'.TYPE_OF_OCCASION'): trans($ADMIN_OUR_LANGUAGE.'.TYPE_OF_OCCASION')}}: </div>
                <div class="wed_text">
                @php
				if(isset($productdetails[0]->search_occasion_id) && $productdetails[0]->search_occasion_id!=''){
                $setTitle = Helper::getOccasionName($productdetails[0]->search_occasion_id);
                @endphp
                @php $mc_name='title'@endphp
                @if(Session::get('admin_lang_file')!='admin_en_lang')
                @php $mc_name= 'title_ar'; @endphp
                @endif
                @php echo $setTitle->$mc_name; 
				}
				else
				{
					 if(isset($productdetails[0]) && $productdetails[0]!='0')
					 { 
						 if(Session::get('admin_lang_file')!='admin_en_lang')
						 {
						   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
						 }
						 else
						 {
							$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
						 } 
						 foreach($getArrayOfOcc as $key=>$ocval)
						 {
						  if($productdetails[0]->main_occasion_id==$key)
						  {
						   $occasion_name = $ocval;
						  }
						 }				  
				       echo $occasion_name;
				   }
				}
				@endphp 
                </div>
              </div>
               @if(isset($productdetails[0]->occasion_date) && $productdetails[0]->occasion_date!='')
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('admin_lang_file').'.OCCASION_DATE')!= '')  ?  trans(Session::get('admin_lang_file').'.OCCASION_DATE'): trans($ADMIN_OUR_LANGUAGE.'.OCCASION_DATE')}}:</div>
                <div class="wed_text">
                 
                  {{ Carbon\Carbon::parse($productdetails[0]->occasion_date)->format('d M Y')}}
                  
                </div>
              </div>

				@endif

			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('admin_lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('admin_lang_file').'.ORDER_ID'): trans($ADMIN_OUR_LANGUAGE.'.ORDER_ID')}}:</div>
                <div class="wed_text">
                  {{$productdetails[0]->order_id}}
                </div>
              </div>
            </div>
			<?php 
			$getSearchData = Helper::searchDetails($productdetails[0]->order_id);
			?>
            <div class="types_ocs_right">
			  @if(isset($getSearchData->budget) && $getSearchData->budget!='' && strtolower($getSearchData->budget)!='n/a')
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('admin_lang_file').'.Budget')!= '')  ?  trans(Session::get('admin_lang_file').'.Budget'): trans($ADMIN_OUR_LANGUAGE.'.Budget')}}:</div>
                <div class="wed_text">
                  @if(isset($getSearchData->budget) && $getSearchData->budget!='')
                  SAR {{number_format($getSearchData->budget,2)}}
                  @endif
                </div>
              </div>
			  @endif
			  @if(isset($getSearchData->total_member) && $getSearchData->total_member!='')
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('admin_lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('admin_lang_file').'.NO_OF_ATTENDANCE'): trans($ADMIN_OUR_LANGUAGE.'.NO_OF_ATTENDANCE')}}:</div>
                <div class="wed_text">
                 @if(isset($getSearchData->total_member) && $getSearchData->total_member!='')
                  {{$getSearchData->total_member}}
                  @endif
                </div>
              </div>
			  @endif
            </div>
          </div>
		  
          <div class="list_of_service">
            <h1 class="list_heading">{{ (Lang::has(Session::get('admin_lang_file').'.LIST_OF_SERVICES')!= '')  ?  trans(Session::get('admin_lang_file').'.LIST_OF_SERVICES'): trans($ADMIN_OUR_LANGUAGE.'.LIST_OF_SERVICES')}}</h1>
            <div class="main_box_table_area">
              <div class="myaccount-table mts order-detail-table">
                <div class="mytr mytr-head">
                   
                  <div class="mytable_heading grey"> {{ (Lang::has(Session::get('admin_lang_file').'.SERVICES')!= '')  ?  trans(Session::get('admin_lang_file').'.SERVICES'): trans($ADMIN_OUR_LANGUAGE.'.SERVICES')}} </div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('admin_lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('admin_lang_file').'.SERVICES_PROVIDER'): trans($ADMIN_OUR_LANGUAGE.'.SERVICES_PROVIDER')}}</div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('admin_lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('admin_lang_file').'.ORDER_DATE'): trans($ADMIN_OUR_LANGUAGE.'.ORDER_DATE')}}</div>
                  <?php //if(isset($productdetails[0]->product_type) && ($productdetails[0]->product_sub_type=='singer' || $productdetails[0]->product_sub_type=='band')){}else{ ?>                                     
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('admin_lang_file').'.STATUS')!= '')  ?  trans(Session::get('admin_lang_file').'.STATUS'): trans($ADMIN_OUR_LANGUAGE.'.STATUS')}}</div>
                  <?php //} ?>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('admin_lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('admin_lang_file').'.AMOUNT'): trans($ADMIN_OUR_LANGUAGE.'.AMOUNT')}}</div>
				  <div class="mytable_heading grey">{{ (Lang::has(Session::get('admin_lang_file').'.VIEW_DETAILS')!= '')  ?  trans(Session::get('admin_lang_file').'.VIEW_DETAILS'): trans($ADMIN_OUR_LANGUAGE.'.VIEW_DETAILS')}}</div>
                </div>                 
                <?php  
				$i=1;$basetotal = 0; $couponcode = 0; if(isset($productdetails) && count($productdetails) > 0){ foreach($productdetails as $val){ 
							if(isset($val->product_type) && $val->product_type=='hall' && $val->product_sub_type=='hall'){
									$cp=$val->paid_total_amount;
							}else{
									$cp=$val->sum;
							}
					$basetotal = ($basetotal+$cp);  

				 $couponcode = Helper::getorderedfromcoupanamount($val->order_id); ?>
                <div class="mytr mybacg">               
                  <div class="mytd mies miestd1" data-title=" {{ (Lang::has(Session::get('admin_lang_file').'.SERVICES')!= '')  ?  trans(Session::get('admin_lang_file').'.SERVICES'): trans($ADMIN_OUR_LANGUAGE.'.SERVICES')}}">
                    @php 
					if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='beauty_centers')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مراكز تجميل';
					   }
					   else
					   {
						   $service_name = 'Beauty Centers'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='spa')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'منتجع صحي';
					   }
					   else
					   {
						   $service_name = 'Spa'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='makeup_artists')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فنان ماكياج';
					   }
					   else
					   {
						   $service_name = 'Makeup Artist'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='makeup')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'منتجات المكياج';
					   }
					   else
					   {
						   $service_name = 'Makeup Products'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='beauty' && $val->product_sub_type=='men_saloon')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'حلاق';
					   }
					   else
					   {
						   $service_name = 'Barber'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='car_rental' && $val->product_sub_type=='car')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'تأجير السيارات';
					   }
					   else
					   {
						   $service_name = 'Car Rentals'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='clinic' && $val->product_sub_type=='cosmetic')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مستحضرات التجميل والليزر';
					   }
					   else
					   {
						   $service_name = 'Cosmetics And Laser'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='clinic' && $val->product_sub_type=='skin')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'طب الأسنان و الأمراض الجلدية';
					   }
					   else
					   {
						   $service_name = 'Dental And Dermatology'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='travel' && $val->product_sub_type=='travel')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'طب الأسنان و الأمراض الجلدية';
					   }
					   else
					   {
						   $service_name = 'Travel Agencies'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='tailor')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'الخياطين';
					   }
					   else
					   {
						   $service_name = 'Tailors'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='dress')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فساتين';
					   }
					   else
					   {
						   $service_name = 'Dresses'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='perfume')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'العطور';
					   }
					   else
					   {
						   $service_name = 'Perfumes'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='abaya')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'العباءة';
					   }
					   else
					   {
						   $service_name = 'Abaya'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='shopping' && $val->product_sub_type=='gold')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مجوهرات';
					   }
					   else
					   {
						   $service_name = 'Jewellery'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='hall' && $val->product_sub_type=='hall')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فندق القاعات';
					   }
					   else
					   {
						   $service_name = 'Hotal Halls'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='food' && $val->product_sub_type=='dates')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'تواريخ';
					   }
					   else
					   {
						   $service_name = 'Dates'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='food' && $val->product_sub_type=='buffet')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'البوفيهات';
					   }
					   else
					   {
						   $service_name = 'Buffets'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='cosha')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'اكتشف';
					   }
					   else
					   {
						   $service_name = 'Kosha'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && ($val->product_sub_type=='photography' || $val->product_sub_type=='video'))
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'استوديو تصوير';
					   }
					   else
					   {
						   $service_name = 'Photography Studio'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='hospitality')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'الاستقبال والضيافة';
					   }
					   else
					   {
						   $service_name = 'Reception & Hospitality'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='music' && $val->product_sub_type=='acoustic')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'نظام الصوت';
					   }
					   else
					   {
						   $service_name = 'Sound System'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='roses')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'ورود';
					   }
					   else
					   {
						   $service_name = 'Roses'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='events')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'حدث مميز';
					   }
					   else
					   {
						   $service_name = 'Special Event'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='singer' && $val->product_sub_type=='singer')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'مطرب';
					   }
					   else
					   {
						   $service_name = 'Singer'; 
					   }						 			  
				       echo $service_name;					  
					}
                    else if(isset($val->product_type) && $val->product_type=='band' && $val->product_sub_type=='band')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'فرق شعبية';
					   }
					   else
					   {
						   $service_name = 'Popular Bands'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='recording' && $val->product_sub_type=='recording')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'تسجيل';
					   }
					   else
					   {
						   $service_name = 'Recording'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='food' && $val->product_sub_type=='dessert')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'الحلوى';
					   }
					   else
					   {
						   $service_name = 'Desserts'; 
					   }						 			  
				       echo $service_name;					  
					}
					else if(isset($val->product_type) && $val->product_type=='occasion' && $val->product_sub_type=='invitations')
					{					  
					   if(Session::get('lang_file')!='en_lang')
					   {
						   $service_name = 'دعوة الكترونية';
					   }
					   else
					   {
						   $service_name = 'Electronic Invitation'; 
					   }						 			  
				       echo $service_name;					  
					}
					/*if(isset($val->category_id) && $val->category_id!='')
					{
                       $dget_category_name = Helper::getshopname($val->category_id);
					   $mc_name = 'mc_name';
                       if(Session::get('lang_file')!='en_lang')
					   {
                          $mc_name= 'mc_name_ar'; 
					   }
					   echo $dget_category_name->$mc_name;
					}
					*/
					$electstatus='';
						if($val->product_sub_type=='invitations' && $val->product_type=='occasion'){				
							$electstatus=Helper::getInvitationOrderdate($val->order_id,$val->id);
					      }
                    @endphp
                  </div>
                  <div class="mytd mies miestd2" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($ADMIN_OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">
                    <?php                   
                    if(isset($val->merchant_id) && $val->merchant_id!='0')
					{
					  $get_vendor_name = Helper::getServiceProviderName($val->merchant_id);
					  echo $get_vendor_name;
                    }
					else
					{?>
					  {{ (Lang::has(Session::get('lang_file').'.ADMIN')!= '')  ?  trans(Session::get('lang_file').'.ADMIN'): trans($ADMIN_OUR_LANGUAGE.'.ADMIN')}}
					<?php } 
					if(isset($val->shop_id) && $val->shop_id!='')
					{
                       if(isset($val->product_type) && ($val->product_sub_type=='singer' || $val->product_sub_type=='band' || $val->product_sub_type=='recording'))
                       {
                         $enq_id = Helper::quoteRequested($val->shop_id);
                         $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
						 echo ' / '.$dget_category->singer_name;                       
                       }
                       else
                       {
                         $dget_category_name = Helper::getshopname($val->shop_id);
                         $mc_name = 'mc_name';
                         if(Session::get('lang_file')!='en_lang')
					     {
                           $mc_name= 'mc_name_ar'; 
					     }
					     echo ' / '.$dget_category_name->$mc_name;
                       }
					   
					}
					?>
                  </div>
                  <div class="mytd mies miestd3" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($ADMIN_OUR_LANGUAGE.'.ORDER_DATE')}}">
                    @if(isset($val) && $val!='')
                    {{ Carbon\Carbon::parse($val->order_date)->format('d M Y')}}
                    @endif
                  </div>
                   <?php if(isset($val->product_type) && ($val->product_sub_type=='singer' || $val->product_sub_type=='band' || $val->product_sub_type=='recording')){?>
				   <div class="mytd mies miestd4" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($ADMIN_OUR_LANGUAGE.'.STATUS')}}">@if(Lang::has(Session::get('lang_file').'.CONFIRMED')!= '') {{ trans(Session::get('lang_file').'.CONFIRMED')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.CONFIRMED')}} @endif</div>
				   <?php }else{ ?>
                  @if(isset($val) && $val!='')
                   @if($val->status==1 || $electstatus=='process')
                  <div class="mytd mies miestd4" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">@if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif</div>
                  @elseif($val->status==2 || $electstatus=='complete')
                  <div class="mytd mies miestd4" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">@if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif</div>
                   @elseif($val->status==3)
                  <div class="mytd mies miestd4" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($ADMIN_OUR_LANGUAGE.'.STATUS')}}">@if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.Hold')}} @endif</div>
                   @elseif($val->status==4)
                  <div class="mytd mies miestd4" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($ADMIN_OUR_LANGUAGE.'.STATUS')}}">@if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.Failed')}} @endif</div>
                  @endif
                  @endif
				  <?php } ?>
                  <div class="mytd mies miestd5" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($ADMIN_OUR_LANGUAGE.'.AMOUNT')}}">
                    @if(isset($val->product_type) && $val->product_type!='hall')
                    SAR {{ number_format($val->sum,2) }}
                    @else
                    SAR {{ number_format($val->paid_total_amount,2) }}
                    @endif
                  </div>
				  <div class="mytd mies miestd6" data-title="{{ (Lang::has(Session::get('lang_file').'.VIEW_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.VIEW_DETAIL'): trans($ADMIN_OUR_LANGUAGE.'.VIEW_DETAIL')}}">
                    
					<a href="{{ url('') }}/admin/serviceorder/{{ $val->cus_id }}/{{ $val->product_type }}/{{ $val->product_sub_type }}/{{ $val->order_id }}/{{ $val->category_id }}/{{ $val->product_id }}/{{ $val->merchant_id }}/{{ $val->shop_id }}">
					{{ (Lang::has(Session::get('lang_file').'.VIEW_DETAIL')!= '')  ?  trans(Session::get('lang_file').'.VIEW_DETAIL'): trans($ADMIN_OUR_LANGUAGE.'.VIEW_DETAIL')}}</a>
                  </div>
                </div>
                <?php $i++;} }else{ ?>
                <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
                <?php } ?>

              </div>
			<div class="main_total"><div class="vat-tax-line">


				@if($couponcode >=1)
{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($ADMIN_OUR_LANGUAGE.'.COUPON')}}: 
		 <?php echo 'SAR '.number_format($couponcode,2); ?>
@endif




			<br />


				@if(Lang::has(Session::get('lang_file').'.VAT')!= ''){{ trans(Session::get('lang_file').'.VAT')}}@else {{ trans($ADMIN_OUR_LANGUAGE.'.VAT')}} @endif: 

				
									
									@php $totalvatprice=$basetotal-$couponcode;

								 $vatamount = Helper::calculatevat($productdetails[0]->order_id,$totalvatprice);
										 echo 'SAR '.number_format(($vatamount),2);
								@endphp
								

				</div><br />



			{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($ADMIN_OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
			<?php if(isset($productdetails) && $productdetails!=''){
			echo 'SAR '.number_format($basetotal+$productdetails[0]->order_taxAmt-$couponcode,2);
			} ?>
			</div>
 


			  
            </div>
          </div>
		  <?php }else{?>
			<div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
			<?php } ?>
        </div> 
									   
									   
									   
									   
									 
									
									 
									 
									 
									 
									 
                                  
                                  </div>
                                  
                                </div>
                              </div>
              </div>
            </div>
       </div>
	   
	   </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script>
<script type="text/javascript">

</script>
<script src="{{ url('')}}/public/assets/js/admin/offerlist.js"></script>  
</body>

    <!-- END BODY -->
</html>
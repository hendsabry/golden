<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="UTF-8" />
  <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{!! csrf_token() !!}"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
@if(Session::get('admin_lang_file') == 'admin_ar_lang')
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
@endif
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
@php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
<link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
<link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

  <!-- MAIN WRAPPER -->
  <div id="wrap" >
    <!-- HEADER SECTION -->
    @include('admin.common.admin_header')
    <!-- END HEADER SECTION -->
    @include('admin.common.left_menu_common')

    <!--PAGE CONTENT -->
    <div class=" col-sm-9 col-xs-12 right-side-bar">
      <div class="right_col" role="main">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div id="appUser">
          <div class="page-title">
            <div class="mainView-details bordr-bg align-head">
               <div class="title_left title-block arabic-right-align">
            <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LEAD_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LEAD_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LEAD_DETAIL') }}</h3>            
                  </div>
               <div class="back-btn-area"> 
                    <a href="/admin/lead" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                  </div>
            </div>
          </div>
      <div class="clearfix"></div>
      <div class="viewListtab">
        <div class="userinformation bordr-bg paddzero small-width-column2">
          <ul class="paddleftright">
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LEAD_ID')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LEAD_ID') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LEAD_ID') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->lead_id}}</p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LEAD_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LEAD_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LEAD_NAME') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->lead_name}}</p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SERVICE_CATEGORY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICE_CATEGORY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICE_CATEGORY') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                @if(Session::get('admin_lang_file') == 'admin_ar_lang'){{$sub_admin->Get_cat->mc_name_ar}} @else {{$sub_admin->Get_cat->mc_name}} @endif
                </p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_EXECUTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_EXECUTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_EXECUTIVE') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if(isset($sub_admin->Sub)) {{$sub_admin->Sub->adm_fname}} {{$sub_admin->Sub->adm_lname}} @else N/A @endif</p></div>
            </li>

            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_HEAD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_HEAD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_HEAD') }} </label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p> @if(isset($sub_admin->manager)) {{$sub_admin->manager->adm_fname}} {{$sub_admin->manager->adm_lname}} @else N/A @endif</p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CLIENT_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CLIENT_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CLIENT_EMAIL') }} </label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->client_email}}</p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CLIENT_PHONE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CLIENT_PHONE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CLIENT_PHONE') }} </label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->client_ph}}</p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS') }} </label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->address}}</p></div>
            </li>
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME') }}</label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo date('F j, Y', strtotime($sub_admin->created_at ));?></p></div>
            </li> 
            <li class="row">
              <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS') }} </label></div>
              <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->status}}</p></div>
            </li>
          </ul>

        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<!--END MAIN WRAPPER -->

<!-- FOOTER -->
@include('admin.common.admin_footer')
<!--END FOOTER -->




<!-- GLOBAL SCRIPTS -->

<script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!-- END GLOBAL SCRIPTS -->

<!-- PAGE LEVEL SCRIPTS -->



<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
  });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>

<!-- END BODY -->
</html>
<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/datepicker3.css"/>
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
@if(Session::get('admin_lang_file') == 'admin_ar_lang')
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
@endif
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
<link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
@php 
$favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
<link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
<link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
<script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
<script src="{{ url('') }}/public/assets/js/admin/tinymce/tinymce.min.js"></script>
<!-- END PAGE LEVEL  STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
        <!-- HEADER SECTION -->
        @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
        @include('admin.common.left_menu_common')

        <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
            <div class="right_col" role="main">
                @if(Session::has('message'))
                <div class="alert alert-danger no-border">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </div>
                @endif
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="add-manage-details">
                        <ul class="nav nav-tabs">
                            <li id="en" class="active"><a data-toggle="tab" href="#home">English</a></li>
                            <li id="ar"><a data-toggle="tab" href="#menu1">Arabic</a></li>
                        </ul>
                        <form action="/admin/electronic_invitation/add" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="tab-content">
                                <div id="home" class="tab-pane bordr-bg fade in active english-sec">
                                <div class="paddmar-block">
                                    <div class="top_back_btn">
                                    <label class="title-block minus-mar-left" for="page-name">
                                            <h3 class="margpadd">Add New Designer Card</h3>
                                            <div  id="error"></div>
                                        </label>
                                            <div class="back-btn-area">
                                            <a href="/admin/electronic_invitation/list" class="back-btn">Back</a>
                                            </div>
                                            </div>
                                    <div class="form-group clearboth">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <input type="hidden" id="pro_mc_id" name="pro_mc_id" class="form-control col-md-7 col-xs-12" value="16">
                                        </div>
                                    </div>
                                    <div class="form-group gender">
                                    <label class="control-label col-md-1 col-sm-6 col-xs-12" name="event" for="page-name">Event Type<span class="required">*</span>
                                    </label>
                                            <div class="col-md-3 col-sm-3 col-xs-12 marTop">
                                                    <label class="clearfix checkbox radio-btn " for="event1">Business Meeting</label>
                                                        <input class="radio-btn-top" type="radio" id="event1"
                                                        name="event_type" value="1" onClick="showeventsubtype(1)">
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-12 marTop">
                                                    <label class="clearfix checkbox radio-btn " for="event2">Wedding & Other Ocassion
                                                        </label>
                                                        <input class="radio-btn-top" type="radio" id="event2"
                                                        name="event_type" value="2" onClick="showeventsubtype(2)">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12" for="page-name">Event Sub Type</label>
                                        <div class="col-md-3 col-sm-6 col-xs-12">

                                     @if(Session::get('admin_lang_file') == 'admin_ar_lang')    

                                    <div class="sub_event_en">
                                    <select name="event_sub_type" id="sub_type_ar" class="form-control">
                                    <option>Select Event sub type</option>
                                    @foreach($event as $events)
                                    <option value="{{ $events->id }}">{{  $events->title_ar }} </option>
                                    @endforeach
                                    </select>  
                                    </div>
                                
                                     @else
                                    
                                     <div class="sub_event_ar">
                                    <select name="event_sub_type" id="sub_type_en" class="form-control">
                                    <option>Select Event sub type</option>
                                    @foreach($event as $events)
                                    <option value="{{ $events->id }}"> {{ $events->title }} </option>
                                    @endforeach
                                    </select>  
                                    </div>

                                    @endif
                                   </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12" for="page-name">Designer Card Name<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <input type="text" id="pro_title" name="pro_title" class="form-control col-md-7 col-xs-12" maxlength="20">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12" for="page-name">Amount<span class="required">*</span>
                                        </label>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <input type="text" id="pro_price" name="pro_price" class="form-control col-md-7 col-xs-12" maxlength="20">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12">Description<span class="required"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" rows="3"
                                            name="pro_desc" id="my_form1" placeholder='Description' maxlength="200"></textarea>
                                        </div>
                                    </div>
                                    @if(Session::get('admin_lang_file') == 'admin_en_lang')
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12">Image</label>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <input type="file" name="pro_Img" id="image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                                        </div>
                                    </div> 
                                    @endif      
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-1">
                                            <button type="button" onClick="validateForm()" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <div id="menu1" class="tab-pane bordr-bg fade arabic_sec">
								 <div class="paddmar-block">
                                <div class="top_back_btn">
                                <div class="back-btn-area">
                                            <a href="/admin/electronic_invitation/list" class="back-btn">إلى الوراء</a>
                                        </div>
                                    <div class="title-block minus-mar-left">       
                                        <h3 class="AR-paddmarg">إضافة فئة / الفئة الفرعية</h3>
                                    </div>
                                    </div>
                                    <div class="form-group clearboth">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12" for="page-name"><span class="required">*</span>اسم الحزمة</label>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <input type="text" id="pro_title_ar" name="pro_title_ar" class="form-control col-md-7 col-xs-12" maxlength="20">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12"><span class="required"></span>وصف</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" rows="3"
                                            name="pro_desc_ar" id="my_form2" placeholder='وصف' maxlength="200"></textarea>
                                        </div>
                                    </div>
                                    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
                                    <div class="form-group">
                                        <label class="control-label col-md-1 col-sm-6 col-xs-12">صورة</label>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <input type="file" name="pro_Img" id="image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                                        </div>
                                    </div> 
                                    @endif      
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
									<label class="control-label col-md-1 col-sm-3 col-xs-12"></label>
                                         <div class="col-md-3 col-sm-6 col-xs-12">
                                            <button class="btn btn-primary" onClick="firstpage()" type="button">إلغاء</button>
                                            <button class="btn btn-primary" onClick="firstpage()" type="reset">إعادة تعيين</button>
                                            <button type="submit" class="btn btn-success">خضع</button>
                                        </div>
                                    </div>
                                </div>
								</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    @include('admin.common.admin_footer')
    <!--END FOOTER -->




    <!-- GLOBAL SCRIPTS -->

    <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="{{ url('')}}/public/assets/js/bootstrap-datepicker.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
    <script src="https://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script type="text/javascript"> 
        tinymce.init({
            selector: 'textarea',
            plugins: ["image"],
            image_advtab: true,
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code image | forecolor backcolor emoticons",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
//external_filemanager_path:"/filemanager/",
//          filemanager_title:"Responsive Filemanager" ,
//          external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
//          image_advtab: true ,
//          file_browser_callback: function(field_name, url, type, win) {
//              if(type=='image') $('#my_form input').click();
//          }
});
</script>

<!-- PAGE LEVEL SCRIPTS -->



<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
<script>
    $( function() {
        $( "#demo" ).datepicker({ format: 'dd-mm-yyyy'});
    } );
</script>
<script type="text/javascript">
var b ='';
        function showeventsubtype(val) {
        //alert(val);
        b = val;
        $.ajax({
          url: "/event_sub_type",
          data: {'flag':'event',<?php if (Session::get('admin_lang_file') == 'admin_ar_lang') {?> 'language':'ar'<?php } else ?>'language':'en' <?php ?>,'val':val},
          cache: false,
          success: function(html){
            setTimeout(function(){
                <?php if (Session::get('admin_lang_file') == 'admin_ar_lang') {?>
                $("#sub_type_ar").html(html);
                <?php } else{ ?>
                $("#sub_type_en").html(html);
               <?php }  ?>
                }, 500);
          }
        });
    }

    function validateForm()
    {
// alert('hello');
var a = document.getElementsByName("pro_title")[0].value;
//alert(a);
if (a==null || a=="" || b=="")
{
    $( "#error" ).append( "<p style='color:red'>Please Fill All Required Field</p>" );; 
    return false;
}else{
    $( "#home" ).removeClass("active");
    $( "#home" ).addClass("fade");
    $( "#menu1" ).addClass("active");
    $( "#menu1" ).removeClass("fade");
    $( "#en" ).removeClass("active");
    $( "#ar" ).addClass("active");
    $( "#error" ).remove();
}
}

function firstpage()
{ 
    //alert('hello');
    $( "#home" ).removeClass("fade");
    $( "#home" ).addClass("active");
    $( "#menu1" ).addClass("fade");
    $( "#menu1" ).removeClass("active");
    $( "#ar" ).removeClass("active");
    $( "#en" ).addClass("active");
}
</script>
</body>

<!-- END BODY -->
</html>
<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class="col-sm-9 col-xs-12 right-side-bar">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="right_col" role="main">
            <div id="appUser">
              <div class="page-title">
                <div class="mainView-details bordr-bg align-head">
                  <div class="title_left title-block arabic-right-align">
                        <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_DESIGNER_CARD_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_DESIGNER_CARD_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_DESIGNER_CARD_DETAIL') }}</h3>
                         <div class="back-btn-area ">
                       <a href="/admin/electronic_invitation/list" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
                      </div>      
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="viewListtab">
            <div class="userinformation bordr-bg paddzero small-width-column2">
              <ul class="paddleftright">
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_EVENT_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_EVENT_TYPE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_EVENT_TYPE') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$elect_invite->event_type}}</p></div>
                </li>
                <!--  <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_EVENT_SUB_TYPE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_EVENT_SUB_TYPE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_EVENT_SUB_TYPE') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$elect_invite->Sub->title}}</p></div>
                </li> -->
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_PRICE') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$elect_invite->pro_price}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE_DISCOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION_PRICE_DISCOUNT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION_PRICE_DISCOUNT') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$elect_invite->pro_discount}}</p></div>
                </li>
                 @if(Session::get('admin_lang_file') == 'admin_ar_lang')
                  <li class="row ">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>اسم البطاقة</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$elect_invite->pro_title_ar}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>وصف</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                    <?php echo strip_tags($elect_invite->pro_desc_ar);?></p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>صورة البطاقة</label></div>   
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><img  src="{{ url('') }}{{ $elect_invite->pro_Img }}" style="width:10%;">
                  </div>
                </li>
                 @else
                    <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>Card Name</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$elect_invite->pro_title}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>Description</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo strip_tags($elect_invite->pro_desc);?></p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>Card Image</label></div>   
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><img  src="{{ url('') }}{{ $elect_invite->pro_Img }}" style="width:10%;">
                  </div>
                </li>

                 @endif
                
              </ul>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>
    <!-- END BODY -->
</html>
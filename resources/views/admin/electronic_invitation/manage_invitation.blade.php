<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>

    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

     <!--PAGE CONTENT -->
        <div class="col-sm-9 col-xs-12 right-side-bar">
                   <div class="right_col" role="main">
            @if(Session::has('msg'))
                <div class="alert alert-success no-border">
                  Information Successfully Updated 
                   
                </div>
            @endif
          


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="page-title">


        <div class="title_left">
        <h3 class="elec">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION') }}</h3>
        <span class="invlist vist">
        <a href="{{route('admin/manage_electronic')}}">
        <h3>{{ (Lang::has(Session::get('admin_lang_file').'.MANAGE_ELECTRONIC_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.MANAGE_ELECTRONIC_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.MANAGE_ELECTRONIC_INVITATION') }}</h3>
        </a>
        </span>
        </div>
        </div>


        <div class="add-manage-details">
          
        <form action="/admin/electronic/update" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left sigle_form_left" enctype="multipart/form-data" method="post" onSubmit="return fullvalidateForm()">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="tab-content">
        <div id="home" class="tab-pane bordr-bg fade in active english-sec">
<div class="paddmar-block">
        <!--div class="top_back_btn">
                <label class="title-block minus-mar-left" for="page-name">
                 <h3 class="margpadd">Add Page Detail</h3>
        <div  id="error"></div>
                </label>
               <div class="back-btn-area">
       <a href="/admin/static_content_management" class="back-btn">Back</a>
      </div>
            </div-->
            
        
        <div class="form-group clearboth">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name">Name (English) <span class="required">*</span>
        </label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="name_en" value="{{$getInfo->mc_name or ''}}" name="name_en" class="form-control col-md-7 col-xs-12" maxlength="60">
        </div>
        </div>

     
        <div class="form-group clearboth">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name">
Name (Arabic)   <span class="required">*</span> </label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="name_ar" name="name_ar" value="{{$getInfo->mc_name_ar or ''}}" required class="form-control col-md-7 col-xs-12" maxlength="60">
        </div>
        </div>


        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Address(English) <span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" id="Address_en" value="{{$getInfo->address or ''}}" name="Address_en" required="" class="form-control col-md-7 col-xs-12" maxlength="220">
        </div>
        </div>

        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Address(Arabic)  <span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" id="Address_ar" name="Address_ar" value="{{$getInfo->address_ar or ''}}"  required class="form-control col-md-7 col-xs-12" maxlength="220">
        </div>
        </div>



 <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">About Shop(English) <span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <textarea id="des_en" name="des_en" cols="100" rows="6" required="">{{$getInfo->mc_discription or ''}}</textarea>

        </div>
        </div>

<div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">About Shop(Arabic)<span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <textarea id="des_ar" name="des_ar" cols="100" rows="6" required="">{{$getInfo->mc_discription_ar or ''}}</textarea>
         </div>
        </div>





 <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Enter YouTube Video URL <span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <input type="text" id="youtubeurl" name="youtubeurl" value="{{$getInfo->mc_video_url or ''}}" required="" class="form-control col-md-7 col-xs-12" maxlength="220">
        </div>
        </div>

 <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description"> </span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <iframe src="{{$getInfo->mc_video_url or ''}}" width="400" height="200"></iframe>
         
        </div>
        </div>



 <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">About Video(English) <span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <textarea id="about_en" name="about_en" cols="100" rows="6" required="">{{$getInfo->mc_video_description or ''}}</textarea>
        </div>
        </div>

        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">About Video(Arabic)<span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <textarea id="about_ar" name="about_ar" required cols="100" rows="7">{{$getInfo->mc_video_description_ar or ''}}</textarea>
        </div>
        </div>



<div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Logo 
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
@if($getInfo->mc_img!='')
<img src="{{$getInfo->mc_img or ''}}" >
 <div class="imgpics-delet">
        <a data-status="Active" data-id="{{$getInfo->mc_id}}" title="Remove Field" class="deletelogo delet-icon status_active2 cstatus">Delete</a>

        </div>
@else
        <div class="input-file-area">
        <label for="company_logo_1">
        <div class="file-btn-area">
        <div id="file_value_1" class="file-value"></div>
        <div class="file-btn"> </div>
        </div>
        </label>
        <input id="company_logo_1" data-lbl="file_value_1" name="logo" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
        </div>
@endif
              
        </div>
        </div>








        
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Image 
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="input-file-area">
        <label for="company_logo">
        <div class="file-btn-area">
        <div id="file_value1" class="file-value"></div>
        <div class="file-btn"> </div>
        </div>
        </label>
        <input id="company_logo" data-lbl="file_value1" name="proimage[]" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif">
        </div>

              
        </div>
        </div>

 <div id="img_upload" class="common_field"></div>
        <div class="img-upload-line common_field">
        <div id="add_button"><a href="javascript:void(0);" class="form-btn">Add More</a></div>
        <span class="error pictureformat"></span>
        @php $Count =  1;    @endphp                
        <input type="hidden" id="count" name="count" value="{{$Count}}">
        </div>
		<div class="rlc">
      
<!-- Add More product images start -->
        @php
        $GalleryCunt = count($fetchfirstdata);
        $J=2; $k=3;
        @endphp

        @for($i=0;$i < $GalleryCunt;$i++)


<div class="form-group electronic">
        
          @if(isset($fetchfirstdata[$i]->image) && $fetchfirstdata[$i]->image!='')
        <div class="form-upload-img product_img_del sizeimg">
        <img src="{{ $fetchfirstdata[$i]->image or '' }}" >
        
        </div>
		<div class="imgpics-delet">
        <a data-status="Active" data-id="{{$fetchfirstdata[$i]->id}}" title="Remove Field" class="delete delet-icon status_active2 cstatus">Delete</a>

        </div>
        <span class="error file_value{{$J}}"></span>  
        @endif
        </div>
 

         
        @php  $J=$J + 1; $k=$k +1; @endphp
        @endfor
 
        
  <!-- Add More product images end -->

 </div>
        
        <div class="ln_solid"></div>
        <div class="form-group gprs">
          <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12 hidden-xs" for="description">
        </label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <button type="submit" class="btn btn-success">Update</button>
        </div>
        </div>
  </div>
        </div>

       
        </div>
        </form>
        </div>
        </div>
        </div>
        </div>
        </div>


    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="{{ url('')}}/public/assets/js/bootstrap-datepicker.js"></script> 
    <!-- END GLOBAL SCRIPTS -->
    <script src="https://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script type="text/javascript"> 
tinymce.init({
            selector: 'textareads',
            plugins: ["image"],
            image_advtab: true,
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code image | forecolor backcolor emoticons",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
        });
</script>

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
<script>
    $( function() {
      $( "#demo" ).datepicker({ format: 'dd-mm-yyyy'});
    } );
</script>
<script type="text/javascript">
    function validateForm()
    {
       // alert('hello');
         var a = document.getElementsByName("title")[0].value;
         var b=tinyMCE.get('my_form').getContent();
         var c=document.getElementsByName("tags")[0].value;
         var d=document.getElementsByName("meta_title")[0].value;
        if (a==null || a==""|| b==null || b=="" || c==null || c=="" || d==null || d=="" )
         {
            $( "#error" ).append( "<p style='color:red'>Please Fill All Required Field</p>" );; 
             return false;
         }else{
            $( "#home" ).removeClass("active");
            $( "#home" ).addClass("fade");
            $( "#menu1" ).addClass("active");
            $( "#menu1" ).removeClass("fade");
            $( "#en" ).removeClass("active");
            $( "#ar" ).addClass("active");
            $( "#error" ).remove();
         }
    }

    function fullvalidateForm()
    {
         var a = document.getElementsByName("title")[0].value;
         var b=tinyMCE.get('my_form').getContent();
         var c=document.getElementsByName("tags")[0].value;
         var d=document.getElementsByName("meta_title")[0].value;
         var f=document.getElementsByName("title_ar")[0].value;
         var g=tinyMCE.get('my_form3').getContent();
         var h=document.getElementsByName("tags_ar")[0].value;
         var i=document.getElementsByName("meta_title_ar")[0].value;

         
        if (a==null || a==""|| b==null || b=="" || c==null || c=="" || d==null || d=="" || f==null || f==""|| g==null || g=="" || h==null || h=="" || i==null || i=="" )
         {
            $( "#error_full" ).append( "<p style='color:red'>Please Fill All Required Field</p>" ); 
             return false;
         }else{
            $( "#error_full" ).remove();
            return true;
         }
    }

     function firstpage()
    { 
            $( "#home" ).removeClass("fade");
            $( "#home" ).addClass("active");
            $( "#menu1" ).addClass("fade");
            $( "#menu1" ).removeClass("active");
            $( "#ar" ).removeClass("active");
            $( "#en" ).addClass("active");
    }
</script>
<script type="text/javascript">
  jQuery("input#image").change(function () {
  var imag =  jQuery(this).val();
  var output = imag.split("\\").pop();
  $('.commit').text(output);
});
</script>
<script type="text/javascript">
  $("#reset").click(function(e) {
    $('.commit').text('<?php echo( (Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE')); ?>');
});


  jQuery(".delete").click(function () {
   var dels =  jQuery(this).data('id');
 swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this file!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#dd6b55',
            cancelButtonColor: '#999',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No',
            closeOnConfirm: false
        }, function() { 
    jQuery.ajax({
        type: "GET",
        url: "{{ route('admin/deleteImages') }}",
        data: {id:dels,from:'deletehallpics'},
        success: function(data) {
       
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    }) 
           });
 });
  jQuery(".deletelogo").click(function () {
   var dels =  jQuery(this).data('id');
 swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this file!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#dd6b55',
            cancelButtonColor: '#999',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No',
            closeOnConfirm: false
        }, function() { 
    jQuery.ajax({
        type: "GET",
        url: "{{ route('admin/deleteImages') }}",
        data: {id:dels,from:'deletelogo'},
        success: function(data) {
       
            if(data==1){
               location.reload();
            }
            
            // $('.main-pro-bg').html(data);
            //console.log(data);
        }
    }) 
           });
 });

</script>


<!-- Add More product images start -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
 <script type="text/javascript">
    $(document).ready(function(){
    var maxField = 10;  
    var addButton = $('#add_button');  
    var wrapper = $('#img_upload');
    var x = <?php echo $Count;?>;
    var y = x+1;  
    $(addButton).click(function(){  
    if(x < maxField){  
    x++; y++; 
  




 var fieldHTML = '<div class="form-group"><label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Image</label><div class="col-lg-9 col-md-9 col-sm-12 col-xs-12"><div class="input-file-area"><label for="company_logo'+y+'"><div class="file-btn-area"><div id="file_value1" class="file-value"></div><div class="file-btn"></div></div></label><input id="company_logo'+y+'" data-lbl="file_value1" name="proimage[]" class="info-file" type="file"  accept="image/gif, image/jpeg, image/png, image/jpg, image/gif"></div></div><div id="remove_button"><a href="javascript:void(0);"  title="Remove Field">&nbsp;</a></div><span class="error file_value'+x+'"></span></div>';






    $(wrapper).append(fieldHTML);  
    document.getElementById('count').value = parseInt(x);
    }
    });
    $(wrapper).on('click', '#remove_button', function(e){  
    e.preventDefault();
    $(this).parent('div').remove(); 
    x--;   y--; 
    document.getElementById('count').value = parseInt(x);
    });
    });

$('body').on('change',".proImgess",function(){
    var fileExtension = ['png','jpeg','jpg','gif'];        
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
         var Dl =  $(this).data('lbl');
         @if(Session::get('lang_file') =='ar_lang')
        $('.'+Dl).html('يرجى تحميل صورة (JPG ، JPEG ، PNG) صالحة');
        @else
        $('.'+Dl).html('Please upload valid (JPG, JPEG, PNG) image ');
        @endif
        $(this).val('');
         $('.file-value').html('');
         }

});
</script> 

</body>

    <!-- END BODY -->
</html>
<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="right_col" role="main">
		  @if(Session::has('message'))
                <div class="alert alert-success no-border">Information Successfully Updated </div>
            @endif
            <div id="appUser">
			
          <div class="page-title">
                <div class="mainView-details bordr-bg align-head">
                  <div class="title_left title-block arabic-right-align">
                        <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_DETAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_DETAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_DETAIL') }}</h3>
                         <!-- <div class="back-btn-area">
                       <a href="/admin/designcard/invitation" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div> -->
                      </div>      
                </div>
              </div>
          <div class="clearfix"></div>
          <div class="viewListtab">
            <div class="userinformation bordr-bg paddzero small-width-column2">
              <ul class="paddleftright"><form action="/admin/designcard/update/{{$invitation->id}}" id="demo-form2" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
				<input type="hidden" name="customer_id" value="{{$invitation->customer_id}}">           
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_MODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_MODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_MODE') }}</label></div>
                 <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> @if($invitation->invitaion_mode == 1)
                  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_TEXT_MESSAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_TEXT_MESSAGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_TEXT_MESSAGE') }}
                  @elseif($invitation->invitaion_mode == 2)
                  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EMAIL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL') }}
                  @elseif($invitation->invitaion_mode == 3)
                  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DESIGNER_CARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESIGNER_CARD') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DESIGNER_CARD') }}
                  @endif
                  </div>
                </li>
                @if($invitation->invitaion_mode == 3)
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DESIGNER_CARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DESIGNER_CARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DESIGNER_CARD') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>@if(Session::get('admin_lang_file') == 'admin_ar_lang')
                  {{$invitation->getPackage[0]->pro_title}}
                @else
                 {{$invitation->getPackage[0]->pro_title_ar}}
                 @endif
              </p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_SELECTED_DESIGN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_SELECTED_DESIGN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_SELECTED_DESIGN') }}</label>
                  </div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <figure class="block-img"> 
                    <img src="{{ url('') }}{{$invitation->getPackage[0]->pro_Img}}"></figure>
                  </div>
                </li>
                @endif
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_MESSAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_MESSAGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_MESSAGE') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>{{$invitation->invitation_msg}}</p>
                  </div>
                </li>
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_VENU')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_VENU') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_VENU') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>{{$invitation->venue}}</p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_BOOKING_AMOUNT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_BOOKING_AMOUNT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_BOOKING_AMOUNT') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <p>
				  
				  @php 
				  	$basetotal=0;
					$basetotal=$invitation->no_of_invitees*$invitation->getPackage[0]->pro_price;
					 $vatamonu = Helper::calculatevat($invitation->order_id,$basetotal);
					 $totalamount=$basetotal+$vatamonu;
				  @endphp
				  SAR {{ number_format($totalamount,2) }}
				  </p>
                  </div>
                </li>
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_DATE_OF_OCCASION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_DATE_OF_OCCASION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_DATE_OF_OCCASION') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <p><?php echo date('d,M Y', strtotime($invitation->date));?></p></div>
                </li>
                
               <!-- <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_STATUS') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12">
                    <select name="status" class="form-control">
                 <option value="" selected="selected">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PLEASE_SELECT_STATUS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PLEASE_SELECT_STATUS') }}</option>
                  <option  value="1" @if($invitation->status == 1) selected @endif>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE') }}</option>
                  <option  value="0" @if($invitation->status == 0) selected @endif>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE') }}</option>
                  </select>
                  </div>
                  </div>
                </li>-->
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_ASSIGN_VENDOR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_ASSIGN_VENDOR') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_ASSIGN_VENDOR') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12">
                  <select name="vendor_id" class="form-control">
                 <option value="" selected="selected">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_VENDOR_IN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_VENDOR_IN') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_VENDOR_IN') }}</option>
                  @foreach($merchant as $march)
                  <option  value="{{$march->mer_id}}" @if($invitation->vendor_id == $march->mer_id) selected @endif>{{ $march->mer_fname }} {{$march->mer_lname}}</option>
                  @endforeach
                  </select>
                  </div>                    
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label></label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <div class="ln_solid"></div>
				  <?php if(isset($invitation->vendor_id) && $invitation->vendor_id!=''){}else{?>
                  <button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_UPDATE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_UPDATE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_UPDATE') }}</button>
                  <a href="/admin/designcard/invitation" class="btn btn-primary" type="button">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_CANSEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_CANSEL') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_CANSEL') }}</a>
				  <?php } if(isset($invitation->vendor_id) && $invitation->vendor_id!=''){ ?> 
                  <a href="/admin/designcard/invitation/list/{{$invitation->packge_id}}/{{$invitation->customer_id}}/{{$invitation->order_id}}" class="btn btn-primary" type="button">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_INVITATION_LIST')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_INVITATION_LIST') : trans($ADMIN_OUR_LANGUAGE.'.BACK_INVITATION_LIST') }}</a>
				  <?php } ?>
                  </div>
                </li> 
              </form>
              </ul>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>
    <!-- END BODY -->
</html>
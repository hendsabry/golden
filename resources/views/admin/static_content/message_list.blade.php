<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/switch.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{ $fav->imgs_name }}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }}/public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
   @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
         <div class="right_col" role="main">
          <div id="appMessageList">
            <div class="clearfix"></div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="page-title">
              <div class="top_back_btn paddLeftRight">
                  <div class="title-block">
                          <h3>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_BROADCAST_LIST')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BROADCAST_LIST') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_BROADCAST_LIST') }}</h3>
                        </div>
                       <div class="back-btn-area">
                       <a href="/admin/static_content_management" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
                      </div>
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 hight_overhid">               
             <div class="col-md-4 col-sm-3 col-xs-3"> 
             <button type="submit" :disabled="isProcessing" @click="broardcastMessage" class="btn theme-default-btn margleftt">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NEW_MESSAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NEW_MESSAGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NEW_MESSAGE') }}</button>
              </div>

            </div>
            </div>
            </div>
                  <div class="x_content StaticContentManagementList">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action message_table_area">
                        <thead>
                          <tr class="headings">
                            <th class="column-title maxwidth"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_SNO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SNO') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SNO') }} </th>
                          
                            <th class="column-title maxwidth"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_MESSAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_MESSAGE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_MESSAGE') }} </th>
                            <th class="column-title">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_SENT_ON')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_SENT_ON') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_SENT_ON') }} 
                            </th>
                             <th class="column-title no-link last"><span class="nobr">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_ACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_ACTION') }}</span>
                            </th> 
                           </tr>
                        </thead>
                        <tbody>
                          <tr v-if="msg" v-for="(ud, index) in msg">
                            <td>
                          @{{ index+1 }}
                            </td>
                            <td>
                          @{{ ud.message }}
                            </td>
                            <td class="last">@{{ ud.created_at }} </td>
                             <td class="last">
                              
                            <button  @click="view(ud.id)" class="btn view-delete"><i class="fa fa-eye"></i> </button>                           
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>        
                  </div>
                  <div class="paging">
               <div class="pull-right mt-lg">
              <ul class="pagination  pagination-separated">
              <li v-if="pagination.current_page > 1"> <a href="#" aria-label="Previous"
              @click.prevent="changePage(pagination.current_page - 1)"> <span aria-hidden="true" class="fa fa-angle-left"></span> </a> </li>
              <li v-for="page in pagesNumber"
              v-bind:class="[ page == isActived ? 'active' : '']"> <a href="#"
              @click.prevent="changePage(page)">@{{ page }}</a> </li>
              <li v-if="pagination.current_page < pagination.last_page"> <a href="#" aria-label="Next"
              @click.prevent="changePage(pagination.current_page + 1)"> <span aria-hidden="true" class="fa fa-angle-right"></span> </a> </li>
              </ul>
              </div>
              </div>
                </div>
              </div>
          </div>
        </div>
          </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <!-- PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<!-- <script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> -->
<script type="text/javascript">
window._msg = [{!! $msg->toJson() !!}];
</script>
<script src="{{ url('')}}/public/assets/js/admin/messagelist.js"></script>  
</body>
    <!-- END BODY -->
</html>
<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/datepicker3.css"/>
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
      <script src="{{ url('') }}/public/assets/js/admin/tinymce/tinymce.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
     @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
                   <div class="right_col" role="main">
            @if(Session::has('message'))
                <div class="alert alert-danger no-border">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </div>
            @endif
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="add-manage-details">
        <ul class="nav nav-tabs">
            <li id="en" class="active"><a data-toggle="tab" href="#home">English</a></li>
            <li id="ar"><a data-toggle="tab" href="#menu1">عربى</a></li>
        </ul>
        <form action="/admin/static_content_management/edit/{{$static_content->id}}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left sigle_form_left" enctype="multipart/form-data" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="tab-content">
        <div id="home" class="tab-pane bordr-bg fade in active english-sec">
        <div class="paddmar-block">
        <div class="top_back_btn">
                <label class="title-block minus-mar-left" for="page-name">
                <h3 class="margpadd">Edit CMS Page</h3>
                <div  id="error"></div>
                </label>
                <div class="back-btn-area">
                <a href="/admin/static_content_management" class="back-btn">Back</a>
                </div>
            </div>        
        <div class="form-group clearboth">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name">Page Name<span class="required">*</span>
        </label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="title" name="title" class="form-control col-md-7 col-xs-12" value="{{$static_content->title}}" maxlength="20">
        </div>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Description <span class="required">*</span>
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <textarea class="form-control" rows="3" name="content" id="my_form" placeholder='Description' maxlength="200">{{$static_content->content}}</textarea>
        </div>
        </div>
        @if(Session::get('admin_lang_file') == 'admin_en_lang')
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12">Image</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
        @if($static_content->images != '')
        <div class="upload_image"><img src="{{ url('') }}{{$static_content->images}}"></div>
        @endif
        <div class="custom-file">
                        <label for="image" class="custom-file-upload">
                        <div class="browse-text"><i class="fa fa-cloud-upload"></i> Browse</div>
                        <span class="commit">No file selected</span>
                        </label>
                          <input type="file" name="images" id="image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                           </div>
        </div>
        </div>
        @endif
		
		  @if($static_content->id==19)
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12"  for="first-name">Map Url <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="title" name="mapurl"  class="form-control col-md-7 col-xs-12" value="{{$static_content->mapurl}}">
        </div>
        </div>
		@else
		<input type="hidden" id="title" name="mapurl"  class="form-control col-md-7 col-xs-12" value="">
        @endif
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="first-name">Alt Tag <span class="required">*</span>
        </label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="title" name="tags"  class="form-control col-md-7 col-xs-12" value="{{$static_content->tags}}" maxlength="20">
        </div>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="first-name">Meta Title <span class="required">*</span>
        </label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="title" name="meta_title"  class="form-control col-md-7 col-xs-12" value="{{$static_content->meta_title}}" maxlength="20">
        </div>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description">Meta Description
        </label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <textarea class="form-control" rows="3"
        name="meta_description" id="my_form1" placeholder='Meta Description' maxlength="200">{{$static_content->meta_description}}</textarea>
        </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12 hidden-xs"></label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <button type="button" onClick="validateForm()" class="btn btn-success">Submit</button>
        </div>
        </div>
        </div>
</div>
        <div id="menu1" class="tab-pane bordr-bg fade arabic_sec">
        <div class="paddmar-block">
        <div class="top_back_btn">
              <div class="back-btn-area">
        <a href="/admin/static_content_management" class="back-btn">الى الخلف</a>
        </div>
       <div class="title-block minus-mar-left"> <h3 class="AR-paddmarg">تحرير صفحة</h3></div>
            </div>
        
        <div class="form-group clearboth">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="page-name"><span class="required">*</span> اسم الصفحة</label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="title" name="title_ar" required class="form-control col-md-7 col-xs-12" value="{{$static_content->title_ar}}">
        </div>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description"><span class="required">*</span> وصف</label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <textarea class="form-control" rows="3"
        name="content_ar" id="my_form2" placeholder='Description' maxlength="200">{{$static_content->content_ar}}</textarea>
        </div>
        </div>
        @if(Session::get('admin_lang_file') == 'admin_ar_lang')
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12">تحميل الصور</label>
        <div class="col-md-3 col-sm-6 col-xs-12 block-img">
        @if($static_content->images != '')
        <div class="upload_image"><img src="{{ url('') }}{{$static_content->images}}"></div>
        @endif
        <div class="custom-file">
                        <label for="image" class="custom-file-upload">
                        <div class="browse-text"><i class="fa fa-cloud-upload"></i>  تصفح</div>
                        <span class="commit">لم يتم اختيار اي ملف</span>
                        </label>
                          <input type="file" name="images" id="image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                           </div>
        </div>
        </div>
        @endif
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-122" for="first-name"><span class="required">*</span>العلامات البديلة</label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="title" name="tags_ar" required class="form-control col-md-7 col-xs-12" value="{{$static_content->tags_ar}}" maxlength="20">
        </div>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="first-name"><span class="required">*</span>عنوان الفوقية</label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="title" name="meta_title_ar" required class="form-control col-md-7 col-xs-12" value="{{$static_content->meta_title_ar}}" maxlength="20">
        </div>
        </div>
        <div class="form-group">
        <label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12" for="description"> ميتا الوصف</label>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <textarea class="form-control" rows="3"
        name="meta_description_ar" id="my_form3" placeholder='ميتا الوصف' maxlength="200">{{$static_content->meta_description_ar}}</textarea>
        </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
		<label class="control-label col-lg-3 col-md-3 col-sm-6 col-xs-12 hidden-xs"></label>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <button class="btn btn-primary" onClick="firstpage()" type="button">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL_1')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL_1') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL_1') }}</button>
        <button class="btn btn-primary" id="reset" onClick="firstpage()" type="reset">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET_1')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET_1') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET_1') }}</button>
        <button type="submit" class="btn btn-success">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT_1')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT_1') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT_1') }}</button>
        </div>
        </div>
        </div>
        </div>
        </div>
        </form>
        </div>
        </div>
        </div>
        </div>
        </div>


    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="{{ url('')}}/public/assets/js/bootstrap-datepicker.js"></script> 
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
<script src="https://tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script type="text/javascript"> 
tinymce.init({
            selector: 'textarea',
            plugins: ["image"],
            image_advtab: true,
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code image | forecolor backcolor emoticons",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
        });
</script>
<script>
    $( function() {
      $( "#demo" ).datepicker({ format: 'dd-mm-yyyy'});
    } );
</script>
<script type="text/javascript">
    function validateForm()
    {
       // alert('hello');
         var a = document.getElementsByName("title")[0].value;
         var b=document.getElementsByName("content")[0].value;
         var c=document.getElementsByName("tags")[0].value;
         var d=document.getElementsByName("meta_title")[0].value;
     
        if (a==null || a==""|| b==null || b=="" || c==null || c=="" || d==null || d=="" )
         {
            $( "#error" ).append( "<p style='color:red'>Please Fill All Required Field</p>" );; 
             return false;
         }else{
            $( "#home" ).removeClass("active");
            $( "#home" ).addClass("fade");
            $( "#menu1" ).addClass("active");
            $( "#menu1" ).removeClass("fade");
            $( "#en" ).removeClass("active");
            $( "#ar" ).addClass("active");
            $( "#error" ).remove();
         }
    }

     function firstpage()
    { 
            $( "#home" ).removeClass("fade");
            $( "#home" ).addClass("active");
            $( "#menu1" ).addClass("fade");
            $( "#menu1" ).removeClass("active");
            $( "#ar" ).removeClass("active");
            $( "#en" ).addClass("active");
    }
</script>
<script type="text/javascript">
  jQuery("input#image").change(function () {
  var imag =  jQuery(this).val();
  var output = imag.split("\\").pop();
  $('.commit').text(output);
});
</script>
<script type="text/javascript">
  $("#reset").click(function(e) {
    $('.commit').text('<?php echo( (Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE')); ?>');
});
</script>
</body>

    <!-- END BODY -->
</html>
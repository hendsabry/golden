<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
<meta charset="UTF-8" />
<title><?php echo $SITENAME; ?>|
<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_SETTINGS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_SETTINGS');} ?>
</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="_token" content="{!! csrf_token() !!}"/>
<!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
<!-- GLOBAL STYLES -->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/main.css" />
@if(Session::get('admin_lang_file') == 'admin_ar_lang')
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
@endif
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/theme.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
@if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
<link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{$fav->imgs_name}} ">
@endif
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="<?php echo url(''); ?>/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
<link rel="stylesheet" href="http://wisitech.in/public/assets/css/font-awesome/css/font-awesome.min.css" />
<!--END GLOBAL STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head><!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="padTop53 " >

<!-- MAIN WRAPPER -->
<div id="wrap"> 
  
  <!-- HEADER SECTION --> 
  @include('admin.common.admin_header') 
  <!-- END HEADER SECTION --> 
  <!-- MENU SECTION --> 
  @include('admin.common.left_menu_common') 
  <!--END MENU SECTION -->
  
  <div class=" col-sm-9 col-xs-12 right-side-bar panel_body_content">
    <div id="content">
      <div class="inner setting_content_area">
        <div class="row">
          <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
          </div>
          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
            <div class="back-btn-area "> <a href="/siteadmin_dashboard" class="profile_back_btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_HOME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME') }}</a> </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="box dark">
              <header>
                <div class="icons"><i class="icon-edit"></i></div>
                <h5>
                  <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_SETTINGS');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS');} ?>
                </h5>
              </header>
              @if ($errors->any()) 
                <div class="alert alert-danger alert-dismissable msg">{!! implode('', $errors->all(':message
                  ')) !!}
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                </div>
              @endif
              @if ( Session::has('warning')) 
                <div class="alert alert-danger alert-dismissable msg">{!! Session::get('warning') !!}
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="warning">×</button>
                </div>
              @endif
              
              @if (Session::has('success'))
              <div class="alert alert-success alert-dismissable">{!! Session::get('success') !!}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              </div>
              @endif
              <div id="div-1" class="accordion-body collapse in body content_body small-width-column"> {!! Form::open(array('url'=>'admin/settings/submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
                <?php foreach($admin_setting_details as $admin_get) { }?>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_URL1')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_URL1');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_URL1');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="url1" class="form-control" value="<?php echo $admin_get->url1; ?>" type="text" required
                    maxlength="70">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_URL2')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_URL2');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_URL2');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="url2" class="form-control" value="<?php echo $admin_get->url2; ?>" type="text" required
                    maxlength="70">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_URL3')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_URL3');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_URL3');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="url3" class="form-control" value="<?php echo $admin_get->url3; ?>" type="text" required
                    maxlength="70">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_URL4')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_URL4');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_URL4');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="url4" class="form-control" value="<?php echo $admin_get->url4; ?>" type="text" required
                    maxlength="70">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_URL5')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_URL5');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_URL5');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="url5" class="form-control" value="<?php echo $admin_get->url5; ?>" type="text" required
                    maxlength="70">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_URL6')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_URL6');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_URL6');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="url6" class="form-control" value="<?php echo $admin_get->url6; ?>" type="text" required
                    maxlength="70">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NUMBER');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="phone_no" class="form-control" value="<?php echo $admin_get->phone_no; ?>" type="text" maxlength="15" minlength="6"  oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                  </div>
                </div>
                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADDRESS1');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="address" class="form-control" value="<?php echo $admin_get->address; ?>" type="text" required
                    maxlength="100">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_EMAIL');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_EMAIL');} ?>
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="email" class="form-control" value="<?php echo $admin_get->email; ?>" type="text" required
                    maxlength="50">
                  </div>
                </div>

                <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                       <h5>  <?php if (Lang::has(Session::get('admin_lang_file').'.VAT_MANAGEMENT')!= '') { echo  trans(Session::get('admin_lang_file').'.VAT_MANAGEMENT');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.VAT_MANAGEMENT');} ?></h5>
                    </label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <h5>value (in %)</h5>
                  </div>
                </div>

                @foreach($country_admin_menu as $countrylist)

                  <div class="form-group">
                  <label for="text1" class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    {{ $countrylist->co_name }}
                    <span class="text-sub">*</span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <input id="text1" placeholder="" name="taxpercentage[]" class="form-control" value="<?php echo $countrylist->vat; ?>" type="text" onkeypress="return isNumberKey(event);" required
                    maxlength="2">
                    <input id="text1" placeholder="" name="contryid[]" class="form-control" value="<?php echo $countrylist->co_id; ?>" type="hidden" required
                    maxlength="50">
                  </div>
                </div>

                @endforeach



                <div class="form-group">
                  <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="pass1"><span class="text-sub"></span></label>
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                    <button class="btn btn-warning btn-sm btn-grad" type="submit" >
                    <a style="color:#fff" >
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_UPDATE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_UPDATE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_UPDATE');} ?>
                    </a>
                    </button>
                    <a href="<?php echo url('siteadmin_dashboard'); ?>" style="color:#000" class="btn btn-default btn-sm btn-grad" >
                    <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_BACK')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_BACK');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_BACK');} ?>
                    </a> </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--END PAGE CONTENT --> 
    
  </div>
</div>

<!--END MAIN WRAPPER --> 

<!-- FOOTER --> 
@include('admin.common.admin_footer') 
<!--END FOOTER --> 

<!-- GLOBAL SCRIPTS --> 
<script src="<?php echo url(''); ?>/public/assets/plugins/jquery-2.0.3.min.js"></script> 

<script src="<?php echo url(''); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo url(''); ?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
<!-- END GLOBAL SCRIPTS --> 

<script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script> 
<script type="text/javascript">
  $(document).on('click','#warning',function(){
       $.ajax({
          url: "/session_flush",
          data: {},
          type: "POST",
          cache: false,
          success: function(html){
          }
        });
    });
</script>
</body>
<!-- END BODY -->
</html>
<script language="javascript">
  function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode != 46  && (charCode < 48 || charCode > 57))) {
            return false;
        } else {
            return true;
        }
    }
</script>


<!DOCTYPE html>
<html lang="en">
	<head>
		{{--<meta name="keywords" content="{!! $common_page_dta['meta_keyword'] !!}" />--}}
		{{--<meta name="description" content="{!! $common_page_dta['meta_description'] !!}" />--}}
		<meta name="author" content="" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Confirmation | Golden Cage.</title>
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="#" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="#" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="#" />
		<link rel="apple-touch-icon-precomposed" sizes="32x32" href="#" />
		<link rel="shortcut icon" href="{{ URL::asset('assets/images/ico/favicon-2.svg') }}" />

		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/jquery-ui/1.11.4/themes/smoothness/jquery-ui.css') }}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/style.css') }}" media="all" />
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/css/icons.css') }}" media="all" />

		<script src="{{ URL::asset('assets/js/jquery/1.10.2/jquery-1.10.2.min.js') }}"></script>
		 <!-- js -->
		<script src="{{ URL::asset('assets/js/jquery-ui/1.11.4/jquery-ui.js') }}"></script>
	</head>
	<body  id="home" class="wide body-light">
		<header class="header innerHeader">
			{{--<div class="topHead">--}}
				{{--<div class="smallLogo"><a href="{{ url('/') }}" title="Golden Cage"><!--<i class="main-logo"></i>--><img src="{{ URL::asset('assets/images/logo-1.svg') }}"></a></div>        --}}
			{{--</div>--}}
		</header>

		<div id="content_wrapper">
            <p>Dear {!! $respect !!}  @if(trim($user_name) == '') {!! $user_email !!} @else {!! $user_name !!} @endif,</p>
            <p> Golden Cage has shared a message with you. <br />Through the web	portal you can log in .</p>
           	<p>UserName => {!! $user_email !!} ,<br /> Message => {!! $msg !!}</p>
           	<p>Click here to login  <a href="https://www.goldencage.org">https://www.goldencage.org</a></p>
           	<p>If you have difficulties in logging in, please send us an email at <a href="info@aligsociety.org">info@goldencage.org </a> and our technical team will be happy to assist you.</p>
           	<p>Thank You</p>
           	<p>Golden Cage</p>
		</div>
		<footer id="footer" style="margin-top: 10px">
			<div class="bottom-footer" style="text-align: center;">
				<p class="copy-right gray" style="margin: 0;">Copyright 2018 || Golden Cage.</p>
			</div>
		</footer>
	</body>
</html>
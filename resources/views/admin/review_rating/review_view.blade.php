<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="right_col" role="main">
            <div id="appUser">
              <div class="page-title">
                <div class="mainView-details bordr-bg">
                  <div class="title_left title-block arabic-right-align">
                        <h3 class="maxwith">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING') }}</h3>
                         <div class="back-btn-area ">
                       <a href="/admin/review" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
                      </div>      
                </div>
              </div>
          <div class="clearfix"></div>
		  
		  @if ($errors->any()) 
                <div class="alert alert-danger alert-dismissable msg">{!! implode('', $errors->all(':message
                  ')) !!}
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                </div>
              @endif
              @if ( Session::has('warning')) 
                <div class="alert alert-danger alert-dismissable msg">{!! Session::get('warning') !!}
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="warning">×</button>
                </div>
              @endif
              
              @if (Session::has('success'))
              <div class="alert alert-success alert-dismissable">{!! Session::get('success') !!}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              </div>
              @endif
		  
		  
          <div class="viewListtab">
            <div class="userinformation bordr-bg paddzero small-width-column2">
              <ul class="paddleftright">
                  <li class="row ">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_REVIEWS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REVIEWS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEWS') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if(isset($review->comments)){{$review->comments}} @endif</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RATING')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RATING') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RATING') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>@if(isset($review->ratings)) {{$review->ratings}} @endif</p>
                  </div>
                </li>
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_GIVEN_TO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GIVEN_TO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GIVEN_TO') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>
                  @if(isset($review->review_type))
                   @if(Session::get('admin_lang_file') == 'admin_ar_lang')
                   @if($review->review_type == 'product' && !$review->getProduct->isEmpty())
                        {{$review->getProduct[0]->pro_title_ar}}
                        @elseif($review->review_type == 'worker' && !$review->getWorker->isEmpty())
                        {{$review->getWorker[0]->staff_member_name_ar}}
                        @elseif($review->review_type == 'shop' && !$review->getCategory->isEmpty())
                        {{$review->getCategory[0]->mc_name_ar}}
                        @endif
                   @else
                        @if($review->review_type == 'product' && !$review->getProduct->isEmpty())
                        {{$review->getProduct[0]->pro_title}}
                        @elseif($review->review_type == 'worker' && !$review->getWorker->isEmpty())
                        {{$review->getWorker[0]->staff_member_name}}
                        @elseif($review->review_type == 'shop' && !$review->getCategory->isEmpty())
                        {{$review->getCategory[0]->mc_name}}
                        @endif
                   @endif
                   @endif
                 </p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_GIVEN_BY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GIVEN_BY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GIVEN_BY') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>@if(isset($review->getUser[0])) 
                  {{$review->getUser[0]->cus_name}}
                @endif</p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_GIVEN_FOR')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GIVEN_FOR') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GIVEN_FOR') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>@if(isset($review->review_type)){{$review->review_type}} @endif</p>
                  </div>
                </li>
                 <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_TIME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_TIME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_TIME') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                 <p>
                  @if(isset($review->created_at))<?php echo(date('M d, Y h:s', strtotime($review->created_at)));?>@endif</p>
                  </div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>
                    @if(isset($review->status)) @if($review->status == 1) {{ (Lang::has(Session::get('admin_lang_file').'.BACK_APPROVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_APPROVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_APPROVE') }} @else {{ (Lang::has(Session::get('admin_lang_file').'.BACK_UNAPPROVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_UNAPPROVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_UNAPPROVE') }} @endif @endif</p></div>
                </li>

                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12">
                    <form action="/admin/review/change_status" enctype="multipart/form-data" method="post" >
                    {{ csrf_field() }}
					@php $getreviewinfo = Helper::getSingleReview($review->comment_id);@endphp
                    <input type="text" class="hidden" name="id" value="{{$review->comment_id}}">
                    <input type="text" class="hidden" name="status" value="1">
					<input type="text" class="hidden" name="vendor_id" value="{{$getreviewinfo->vandor_id}}">
					<input type="text" class="hidden" name="customer_id" value="{{$getreviewinfo->customer_id}}">
					<input type="text" class="hidden" name="ratings" value="{{$getreviewinfo->ratings}}">
                    <!--button type="submit" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_APPROVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_APPROVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_APPROVE') }}</button-->
                    </form>
                  </div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12">
                  <form action="/admin/review/change_status" enctype="multipart/form-data" method="post">
                  {{ csrf_field() }}
                  <input type="text" class="hidden" name="id" value="{{$review->comment_id}}">
                  <input type="text" class="hidden" name="status" value="2">
                       <!--button type="submit" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_REJECTED')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REJECTED') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_REJECTED') }}</button-->
                    </form>
                  </div>
                </li>

              </ul>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
    <!-- GLOBAL SCRIPTS -->
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>
<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
<script type="text/javascript">
  $(document).on('click','#warning',function(){
       $.ajax({
          url: "/session_flush",
          data: {},
          type: "POST",
          cache: false,
          success: function(html){
          }
        });
    });
</script>
</body>
    <!-- END BODY -->
</html>
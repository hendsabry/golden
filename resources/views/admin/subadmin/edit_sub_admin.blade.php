<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
    <link href="{{ url('') }}/public/assets/css/admin/bootstrap-datepicker.min.css" rel="stylesheet" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
                   <div class="right_col" role="main">
            @if(Session::has('message'))
                <div class="alert alert-danger no-border">
                <button type="button" class="close" data-dismiss="alert"><span>�</span><span class="sr-only">Close</span></button>
                    @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </div>
            @endif
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content padduser-block">
                  <div class="top_back_btn">
                  <div class="title-block">
                        <h3>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_EDIT_SUB_ADMIN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EDIT_SUB_ADMIN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EDIT_SUB_ADMIN') }}</h3>
                        </div>
                        <div class="back-btn-area">
                       <a href="/admin/sub_admin" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
                      </div>
                      
                    </div>
                    <form action="/admin/subadmin/edit/{{ $sub_admin->adm_id }}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left form-section" enctype="multipart/form-data" method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_FIRST_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FIRST_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FIRST_NAME') }}<span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="adm_fname" required class="form-control col-md-7 col-xs-12" value="{{ $sub_admin->adm_fname }}" maxlength="20">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LAST_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_NAME') }} <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="adm_lname" required class="form-control col-md-7 col-xs-12" value="{{ $sub_admin->adm_lname }}" maxlength="20">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL') }} <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="email" id="title" name="adm_email" required class="form-control col-md-7 col-xs-12" value="{{$sub_admin->adm_email}}" maxlength="70">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO') }} <span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="row row-area">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        <select name="country_code" class="selectpicker form-control" data-live-search="true" required="required">
                        <option  value="">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY_CODE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY_CODE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY_CODE') }}</option>
                        @foreach($countries as $country)
                        <option value="{{ $country->country_code }}" <?php if ($country->country_code == $sub_admin->country_code): ?> selected
                          <?php endif ?>>{{ $country->country_code }}</option>
                        @endforeach
                        </select>
                        </div>
                         <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="title" name="adm_phone" required class="form-control col-md-7 col-xs-12" value="{{$sub_admin->adm_phone}}" minlength="6" maxlength="16" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
                        </div>
                        </div>
                        </div>
                      </div>
                       <div class="form-group gender">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_GENDER')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_GENDER') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_GENDER') }}</label>
                        <div class="col-md-3 col-sm-3 col-xs-12 marTop">
                        <input type="radio" class="flat" name="gender" id="active_1" value="0"  @if($sub_admin->gender == 0) checked @endif />
						<label for="active_1" class="gender-name">
                        {{ (Lang::has(Session::get('admin_lang_file').'.BACK_MALE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MALE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_MALE') }} 
                        </label> 
                        
                        </div> 
                        <div class="col-md-3 col-sm-3 col-xs-12 marTop" >
                         <input type="radio" class="flat" name="gender" id="inactive_1" value="1"  @if($sub_admin->gender == 1) checked @endif/>
						 <label for="inactive_1" class="gender-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_FEMALE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FEMALE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FEMALE') }} 
                          </label>
                        
                      </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_BIRTH') }}<span class="required">*</span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="text" id="demo" name="dob" required class="form-control col-md-7 col-xs-12" value="<?php echo(date('d-m-Y', strtotime($sub_admin->dob)));?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADDRESS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS') }}<span class="required"></span>
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <textarea class="form-control" rows="3"
                          name="adm_address1" id="my_form" placeholder='Description' maxlength="200">{{$sub_admin->adm_address1}}</textarea>
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_COUNTRY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY') }} <span class="required">*</span></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <select name="adm_co_id" class="selectpicker form-control" data-live-search="true" required="required" id="country">
                        <option disabled value="">--Select Country--</option>
                        @foreach($countries as $country)
                        <option value="{{ $country->co_id }}" <?php if ($country->co_id == $sub_admin->adm_co_id): ?> selected
                          <?php endif ?>>  @if(Session::get('admin_lang_file') == 'admin_ar_lang') {{ $country->co_name_ar }} @else {{ $country->co_name }} @endif</option>
                        @endforeach
                        </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CITY') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY') }}  <span class="required">*</span></label>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <select class="selectpicker form-control" data-live-search="true" name="adm_ci_id" required="required" id="city">
                          <option disabled value="">Please City</option>
                           @foreach($cities as $city)
                        <option value="{{ $city->ci_id }}" <?php if ($city->ci_id == $sub_admin->adm_ci_id): ?> selected
                          <?php endif ?>>  @if(Session::get('admin_lang_file') == 'admin_ar_lang') {{ $city->ci_name_ar }} @else {{ $city->ci_name }} @endif</option>
                        @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBADMIN_ROLE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBADMIN_ROLE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBADMIN_ROLE') }}<span class="required">*</span></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <select class="form-control" name="access_group_id" id="access_group_id" required="required">
                          <option value="">--Select Role--</option>
                          @foreach($groups as $group)
                            <option value="{{$group->id}}" <?php if ($group->id == $sub_admin->access_group_id): ?> selected
                          <?php endif ?>>{{$group->group_name}}</option>
                          @endforeach
                          </select>
                        </div>
                      </div>

                       <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IMAGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE') }}</label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          @if($sub_admin->image != '') 
                          <div class="upload_image"><figure class="block-img"><img src="{{ url('') }}{{$sub_admin->image}}"> </figure></div>@endif
                          <div class="custom-file">
                        <label for="image" class="custom-file-upload">
                        <div class="browse-text"><i class="fa fa-cloud-upload"></i> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_BROWSE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BROWSE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_BROWSE') }}</div>
                        <span class="commit">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE') }}</span>
                        </label>
                        <input type="file" name="image" id="image" accept="image/x-png,image/gif,image/jpeg"  class="file-styled">
                           </div>
                           
                        </div>
                      </div>
                       <div class="form-group gender">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS') }}</label>
                      <div class="col-md-3 col-sm-3 col-xs-6 marTop">
					   <input type="radio" class="flat" name="status" id="active" value="1" @if($sub_admin->status == 1) checked @endif required />
                        <label for="active" class="gender-name">
                        {{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE') }}</label>                       
                        </div> 
                        <div class="col-md-3 col-sm-3 col-xs-6 marTop">
						<input type="radio" class="flat" name="status" id="inactive" value="0" @if($sub_admin->status == 0) checked @endif />
                          <label for="inactive" class="gender-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE') }}</label>
                      </div>
                      </div>

						 <div class="form-group gender">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12">{{ (Lang::has(Session::get('admin_lang_file').'.PERMISSIONTYPE')!= '') ?  trans(Session::get('admin_lang_file').'.PERMISSIONTYPE') :  trans($ADMIN_OUR_LANGUAGE.'.PERMISSIONTYPE') }}</label>
                      <div class="col-md-3 col-sm-3 col-xs-6 marTop">
					   <input type="radio" class="flat" name="permission_status" id="active" value="1" @if($sub_admin->permission_status == 1) checked @endif required />
                        <label for="active" class="gender-name">
                        {{ (Lang::has(Session::get('admin_lang_file').'.WRITEPERMISSION')!= '') ?  trans(Session::get('admin_lang_file').'.WRITEPERMISSION') :  trans($ADMIN_OUR_LANGUAGE.'.WRITEPERMISSION') }}</label>                       
                        </div> 
                        <div class="col-md-3 col-sm-3 col-xs-6 marTop">
						<input type="radio" class="flat" name="permission_status" id="inactive" value="0" @if($sub_admin->permission_status == 0) checked @endif />
                          <label for="inactive" class="gender-name">{{ (Lang::has(Session::get('admin_lang_file').'.READPERMISSION')!= '') ?  trans(Session::get('admin_lang_file').'.READPERMISSION') :  trans($ADMIN_OUR_LANGUAGE.'.READPERMISSION') }}</label>
                      </div>
                      </div>
						 <div class="form-group gender">
						 </div>

                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NEW_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_NEW_PASSWORD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_NEW_PASSWORD') }}
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="password" id="upassword" name="upassword"  class="form-control col-md-7 col-xs-12" value="" maxlength="20">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12" for="first-name">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT_CONFIRM_PASSWORD') }} 
                        </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <input type="password" id="cpassword" name="cpassword" class="form-control col-md-7 col-xs-12" value="" maxlength="20">
                        </div>
                      </div>




                      <div class="ln_solid"></div>
                      <div class="form-group">
                      <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                          <a class="btn btn-primary" type="button" href="/admin/sub_admin">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CANCEL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_CANCEL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_CANCEL') }}</a>
                          <button class="btn btn-primary" id="reset" type="reset">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_RESET') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }}</button>
                         <input name="submit" type="submit" class="btn btn-success" value="{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }}">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

        </div>

        </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
<script src="{{ url('')}}/public/assets/js/admin/bootstrap-datepicker.min.js"></script>
 <script type="text/javascript">
      $(window).on('load',function(){
      $('#demo').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        endDate: '+0d'   
        });
      });
    </script>
<script type="text/javascript">
  $(document).on('change','#country',function(){
        $.ajax({
          url: "/get_city",
          data: {'ci_con_id':$('#country').val(),'flag':'country',<?php if (Session::get('admin_lang_file') == 'admin_ar_lang') {?> 'language':'ar'<?php } else ?>'language':'en' <?php ?>},
          cache: false,
          success: function(html){
            setTimeout(function(){     
                $("#city").html(html);
                }, 500);
          }
        });
    });
</script>
<script type="text/javascript">
$(document).on('click', '.day', function () {
 $(".datepicker").css("display", "none");
});

$(document).on('click', '#demo', function () {
 $(".datepicker").css("display", "block");
});
</script>
<script type="text/javascript">
  jQuery("input#image").change(function () {
  var imag =  jQuery(this).val();
  var output = imag.split("\\").pop();
  $('.commit').text(output);
});
</script>
<script type="text/javascript">
  $("#reset").click(function(e) {
    $('.commit').text('<?php echo( (Lang::has(Session::get('admin_lang_file').'.BACK_NO_FILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_NO_FILE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_NO_FILE')); ?>');
});
</script>
</body>

    <!-- END BODY -->
</html>
   <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">

jQuery("#demo-form2").validate({
                  ignore: [],
                  rules: {
                          upassword: {
                          required: false,
                           minlength: 8
                          },
                           cpassword : {
                             minlength : 8,
                             equalTo : "#upassword"
                           }
                          },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
             
                },
             
           messages: {
         
          upassword: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_PASSWORD'): trans(Session::get('lang_file').'.USER_PASSWORD'); @endphp",
          minlength: "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'); @endphp",
                      },
             cpassword: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.USER_CONFIRM_PASSWORD')!= '')  ?  trans(Session::get('lang_file').'.USER_CONFIRM_PASSWORD'): trans(Session::get('lang_file').'.USER_CONFIRM_PASSWORD'); @endphp",
          minlength: "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'): trans(Session::get('lang_file').'.PLEASE_ENTER_CHARACTERS'); @endphp",
        equalTo: "@php echo (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE')!= '')  ?  trans(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE'): trans(Session::get('lang_file').'.PLEASE_ENTER_THE_SAME_VALUE'); @endphp",
                      },
                     
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 


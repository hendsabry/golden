<?php //print_r(Session::all()); exit();?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} |  {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('') }}/public/assets/plugins/bootstrap/css/bootstrap.css" />
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    @if(Session::get('admin_lang_file') == 'admin_ar_lang')
     <link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
      <link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
     @endif
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/theme.css" />
    <link rel="stylesheet" href=" {{ url('') }}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/font-awesome/css/font-awesome.min.css" />
         @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp @if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/{{$fav->imgs_name}}">
@endif
    <!--END GLOBAL STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link href="{{ url('') }}/public/assets/css/layout2.css" rel="stylesheet" />
    <link href="{{ url('') }}/public/assets/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="{{ url('') }}/stylesheet" href="public/assets/plugins/timeline/timeline.css" />
     <script class="include" type="text/javascript" src="{{ url('') }} /public/assets/js/chart/jquery.min.js"></script>
    <!-- END PAGE LEVEL  STYLES -->
     <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
 <!-- HEADER SECTION -->
	 @include('admin.common.admin_header')
        <!-- END HEADER SECTION -->
       @include('admin.common.left_menu_common')

       <!--PAGE CONTENT -->
        <div class=" col-sm-9 col-xs-12 right-side-bar">
         <div class="col-md-12 col-sm-12 col-xs-12">
                   <div class="right_col" role="main">
  <div id="appUser">
    <div class="page-title">
      <div class="mainView-details bordr-bg">
        <div class="row">
        <div class="col-sm-6">
          <div class="title_left">
            <h3 class="padd">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN_STATUS') }}</h3>
            <p>{{$sub_admin->adm_fname}} {{$sub_admin->adm_lname}}</p>
            <p class="paddNone"><?php echo date('d,M Y', strtotime($sub_admin->created_at));?></p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="user-view-info">
            <div class="userImage marBotm">
              <figure> @if($sub_admin->image != null && $sub_admin->image != '') <img src="{{ url('') }}{{ $sub_admin->image }}"> @else<img src="{{ url('') }}/public/assets/adimage/user-dummy.png">@endif </figure>
               <a href="/admin/sub_admin" class="back-btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a>
            </div>
            <div class="view-userDetails">
              <div class="view-userDetails-inner">
                <h3>{{$sub_admin->adm_fname}} {{$sub_admin->adm_lname}}</h3>
                <p class="username">{{ $sub_admin->adm_email }}</p>
                <div class="status">
                  @if($sub_admin->status == 1)
                  <div>
                  <span class="active"></span>
                  <h2 >{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE') }} </h2>
                  
                  </div>
                  @else
                  <div>
                  <span class="Inactive"></span>
                  <h2 >{{ (Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE') }} </h2>
                  </div>
                  @endif
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      
    </div>
    <div class="clearfix"></div>
    <div class="viewListtab">
        <div class="userinformation bordr-bg paddzero small-width-column2">
              <ul class="paddleftright">
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_FULL_NAME')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_FULL_NAME') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_FULL_NAME') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->adm_fname}} {{$sub_admin->adm_lname}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_EMAIL') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL') }} </label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->adm_email}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NO')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PHONE_NO') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NO') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{$sub_admin->country_code}} - {{$sub_admin->adm_phone}}</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_AGE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_AGE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_AGE') }}</label></div>
                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo((date('Y') - date('Y',strtotime($sub_admin->dob))));?></p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DATE_OF_BIRTH') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DATE_OF_BIRTH') }}</label></div>
                  <div v-if="userdata" class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p><?php echo date('d,M Y', strtotime($sub_admin->dob ));?></p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LOCATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LOCATION') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LOCATION') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if(Session::get('admin_lang_file') == 'admin_ar_lang') {{ $sub_admin->getCity[0]->ci_name_ar }} , {{ $sub_admin->getCountry[0]->co_name_ar }}  @else {{ $sub_admin->getCity[0]->ci_name }} , {{ $sub_admin->getCountry[0]->co_name }} @endif</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_LOGGED_IN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_LAST_LOGGED_IN') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_LOGGED_IN') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>N/A</p></div>
                </li>
                <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBADMIN_ROLE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBADMIN_ROLE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBADMIN_ROLE') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>{{ $sub_admin->get_role[0]->group_name }}</p></div>
                </li>
                  <li class="row">
                  <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12"><label>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATUS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATUS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_STATUS') }}</label></div>
                  <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"><p>@if($sub_admin->status == 1) {{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ACTIVE') }} @else {{ (Lang::has(Session::get('admin_lang_file').'.BACK_IN_ACTIVE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_IN_ACTIVE') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_IN_ACTIVE') }} @endif</p></div>
                </li>
    
              </ul>
              
            </div>
        </div>
  </div>
</div>
          </div>
          </div>
    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
     @include('admin.common.admin_footer')
    <!--END FOOTER -->
 
   
    

    <!-- GLOBAL SCRIPTS -->
  
     <script src="{{ url('')}}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo url('');?>/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
   
 
  
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>

<script src="{{ url('')}}/public/assets/js/sweetalert.min.js"></script> 
</body>

    <!-- END BODY -->
</html>
@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_REVIEWS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_REVIEWS')}}</h1>
      <div class="field_group top_spacing_margin_occas">
	  @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="main_user"> 
          @php 
		  if(count($setRewiev) > 0)
		  {
            foreach($setRewiev as $val){
		  @endphp
          <div class="types_ocss">
            <div class="types_ocs_left">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')}}: </div>
                <div class="wed_text"> 
					<?php 
					if(isset($val->search_occasion_id) && $val->search_occasion_id!='0')
					{
						$setTitle = Helper::getOccasionName($val->search_occasion_id);
						if(Session::get('lang_file')!='en_lang')
						{
							$mc_name= 'title_ar';
							$setTitle->$mc_name;
						}
						else
						{
							$mc_name='title';
						}
						echo $setTitle->$mc_name;
					}
					else
					{
					  if(isset($val->main_occasion_id) && $val->main_occasion_id!='0')
					  { 
						 if(Session::get('lang_file')!='en_lang')
						 {
						   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'الزفاف والمناسبات');
						 }
						 else
						 {
							$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
						 } 
						 foreach($getArrayOfOcc as $key=>$ocval)
						 {
						  if($val->main_occasion_id==$key)
						  {
						   $occasion_name = $ocval;
						  }
						 }
						 echo $occasion_name;					
					 }
				   }
                  ?> 
				  </div>
              </div>
            </div>
            <div class="types_ocs_right">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.OCCASION_DATE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_DATE'): trans($OUR_LANGUAGE.'.OCCASION_DATE')}}:</div>
                <div class="wed_text">@php if(isset($val->occasion_date) && $val->occasion_date!=''){ echo date('d M Y',strtotime($val->occasion_date)); } else{ echo 'N/A'; } @endphp</div>
              </div>
            </div>
          </div>
          <div class="list_of_service">
            <div class="main_box_table_area">
              <div class="myaccount-table mts">
                <div class="mytr">
                  <div class="mytable_heading grey"> {{ (Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')}} </div>
				 <div class="mytable_heading grey"> {{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} </div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}</div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}</div>
                  <div class="mytable_heading grey">{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}</div>
                  <div class="mytable_heading order_id grey">&nbsp;</div>
                </div>
				<?php 
				$getAllOrder = Helper::getAllOrderWithService($val->order_id); 
				foreach($getAllOrder as $orderval)
				{ 
								  
				?>
                <div class="mytr mybacg">
                  <div class="mytd mies " data-title="Services"> {{ $val->order_id }}</div>
				  <div class="mytd mies " data-title="Services">

            {{ ucwords($orderval->product_sub_type) }} 
				  @php 
            if($orderval->product_sub_type=='band' || $orderval->product_sub_type=='singer'){
              if(isset($orderval->product_id) && $orderval->product_id!='')
            {
              //echo $orderval->order_id;
             // $singerinfo = Helper::getMusicInfo($orderval->product_id);
             //$catshop=$singerinfo->name;
            }

          }else{
					if(isset($orderval->category_id) && $orderval->category_id!='')
					{
                       $dget_category_name = Helper::getshopname($orderval->category_id);
					   $mc_name = 'mc_name';
                       if(Session::get('lang_file')!='en_lang')
					   {
                          $mc_name= 'mc_name_ar'; 
					   }
					   //$catshop=$dget_category_name->$mc_name;
            if(isset($dget_category_name->$mc_name) && $dget_category_name->$mc_name !='')
            {
            $catshop=$dget_category_name->$mc_name;
            }
            else
            {
            $catshop='N/A';
            }
         

					}

          }
             @endphp
				  </div>
				  <div class="mytd mies " data-title="Service Provider">
				  @php 

          if(isset($catshop) && $catshop!=''){
          echo $catshop.' / ';
        }


				    if(isset($orderval->merchant_id) && $orderval->merchant_id!='0')
					{
						$get_vendor_name = Helper::getServiceProviderName($orderval->merchant_id);
						echo $get_vendor_name;
                    }
					@endphp
				  </div>
                  <div class="mytd mies " data-title="Order Date">
				    @if(isset($orderval->created_at) && $orderval->created_at!='')
                     {{ Carbon\Carbon::parse($orderval->created_at)->format('d M Y')}}
                     @else
                     @php echo $na='N/A'; @endphp 
                    @endif
				  </div>
                   @if(isset($orderval->status) && $orderval->status!='')
                   @if($orderval->status==1)
                  <div class="mytd mies" data-title="Status">@if(Lang::has(Session::get('lang_file').'.Pending')!= '') {{ trans(Session::get('lang_file').'.Pending')}}  @else {{ trans($OUR_LANGUAGE.'.Pending')}} @endif</div>
                  @elseif($orderval->status==2)
                  <div class="mytd mies" data-title="Status">@if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif</div>
                   @elseif($orderval->status==3)
                  <div class="mytd mies" data-title="Status">@if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif</div>
                   @elseif($orderval->status==4)
                  <div class="mytd mies" data-title="Status">@if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif</div>
                  @endif
                  @endif
				  @if(isset($orderval->status) && $orderval->status!='')
                   @if($orderval->status==1)
                   <div class="mytd mies order_id review_popup_link">{{ (Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')}}</div>
                   @elseif($orderval->status==2)
                   <div class="mytd mies order_id review_popup_link"><a href="{{ route('reviewdisplay',[$orderval->shop_id,$orderval->order_id,$orderval->merchant_id]) }}">{{ (Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')}}</a></div>
                   @elseif($orderval->status==3)
                   <div rel="" class="mytd mies order_id review_popup_link"><a href="javascript:void(0)">{{ (Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')}}</a></div>
                   @elseif($orderval->status==4)
                  <div rel="" class="mytd mies order_id review_popup_link"><a href="javascript:void(0)">{{ (Lang::has(Session::get('lang_file').'.REVIEW')!= '')  ?  trans(Session::get('lang_file').'.REVIEW'): trans($OUR_LANGUAGE.'.REVIEW')}}</a></div>
                  @endif
                  @endif
                </div>
				<?php } ?>
                <!-- review_popup -->
              </div>
            </div>
          </div>
          @php } } else{ @endphp
          <div class="no-record-area" align="center">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
          @php } @endphp
          </div>
      </div>
	  <div align="left">{{ $setRewiev->links()}}<div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
</div>
</div>
</div></div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer')

@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap occ">
  <!-- search-section -->
  <div class="page-left-right-wrapper not-sp">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT_OCCASIONS'): trans($OUR_LANGUAGE.'.MY_ACCOUNT_OCCASIONS')}}</h1>
      
      <?php /*?>{!! Form::open(array('url'=>"my-account-ocassion",'class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8','method'=>'post','name'=>'filter')) !!}
      <div class="dash_select">
        <div class="search-box-field">
          <select name="search_key" id="search_key" onChange="document.filter.submit();">
            <option value="all"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='all'){echo 'Selected="Selected"';}?>>{{ (Lang::has(Session::get('lang_file').'.ALL')!= '')  ?  trans(Session::get('lang_file').'.ALL'): trans($OUR_LANGUAGE.'.ALL')}}</option>
            <option value="date"<?php if(isset($_REQUEST['search_key']) && $_REQUEST['search_key']=='date'){echo 'Selected="Selected"';}?>>Date</option>
          </select>
        </div>
      </div>
      {!! Form::close() !!}
<?php */?>
      <div class="field_group top_spacing_margin_occas">
	    @if (Session::has('message'))
         <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="main_user">
		 @if(count($setOrder) > 0)
          <div class="myaccount-table">
            <div class="mytr">
              <div class="mytable_heading"></div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')}}</div>
              <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}</div>
              <div class="mytable_heading order_id ">{{ (Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')}}</div>
            </div>
            @php $i = 1; @endphp
            @if(count($setOrder) >0)
            @foreach($setOrder as $val)           
            <div class="mytr">
              <div class="mytd " data-title="Sl. No.">{{$i}}</div>
              <div class="mytd " data-title="Occasions Name">
			  @php
			  if(isset($val->search_occasion_id) && $val->search_occasion_id!='0'){
              $setTitle = Helper::getOccasionName($val->search_occasion_id);
              @endphp
              @php $mc_name='title'@endphp
              @if(Session::get('lang_file')!='en_lang')
              @php $mc_name= 'title_ar'; @endphp
              @endif
               <a href="{{ route('order-details',['id'=>$val->order_id]) }}">{{@$setTitle->$mc_name}}</a>
			   @php } else{
			   if(isset($val->main_occasion_id) && $val->main_occasion_id!='0'){ 
				 if(Session::get('lang_file')!='en_lang')
				 {
				   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
				 }
				 else
				 {
					$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
				 } 
				 foreach($getArrayOfOcc as $key=>$ocval)
				 {
				  if($val->main_occasion_id==$key)
				  {
				   $occasion_name = $ocval;
				  }
				 }	
				 @endphp
				 <a href="{{ route('order-details',['id'=>$val->order_id]) }}"> @php echo $occasion_name; @endphp </a>			   
			   @php }} @endphp
            </div>
              <div class="mytd " data-title="Date">{{ Carbon\Carbon::parse($val->order_date)->format('d M Y')}}</div>
              <div class="mytd order_id" data-title="Order ID"><a href="{{ route('order-details',['id'=>$val->order_id]) }}">{{ $val->order_id }}</a></div>
            </div>
            @php $i++; @endphp 
            @endforeach
            @else
            <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
            @endif
         </div>
		 @else
		 <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
		 @endif
        </div>        
      </div>
      
    </div>
    {{ $setOrder->links() }}
    <!-- page-right-section -->
  </div>

  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
<script type="text/javascript">
    /*function SearchData(str)
    {
     $.ajax({
     type: 'get',
     data: 'search_key='+str,
     url: '<?php echo url('my-account-ocassion-withajax'); ?>',
     success: function(responseText){  
      alert(responseText);
      $('#email_id_error_msg').html(responseText);  
      if(responseText!=''){
        $("#email_id").css('border', '1px solid red'); 
        $("#email_id").focus();
      }
      else
        $("#email_id").css('border', '1px solid #ccc');  
     }    
     });  
   }*/
</script>
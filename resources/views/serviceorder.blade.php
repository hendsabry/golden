@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">
  <!-- search-section -->
  <div class="page-left-right-wrapper">
    <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
    @include('includes.left_menu')
    <div class="myaccount_right">
      <div class="dash_select"> <a href="{{ route('order-details',[$order_id]) }}">{{ (Lang::has(Session::get('lang_file').'.BACK_TO_ORDER_SUMMARY')!= '')  ?  trans(Session::get('lang_file').'.BACK_TO_ORDER_SUMMARY'): trans($OUR_LANGUAGE.'.BACK_TO_ORDER_SUMMARY')}}</a> </div>
      <div class="field_group top_spacing_margin_occas">
        <div class="main_user">
		  <?php  $couponcode =0;
		  //echo '<pre>';print_r($productdetails);die;
		  if(isset($productdetails) && !count($productdetails)<1){?>
          <div class="types_ocss">
            <div class="types_ocs_left">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')}}: </div>
                <div class="wed_text">
                @php
				if(isset($productdetails->search_occasion_id) && $productdetails->search_occasion_id!=''){
                $setTitle = Helper::getOccasionName($productdetails->search_occasion_id);
                @endphp
                @php $mc_name='title'@endphp
                @if(Session::get('lang_file')!='en_lang')
                @php $mc_name= 'title_ar'; @endphp
                @endif
                @php echo $setTitle->$mc_name; 
				}
				else
				{
					 if(isset($productdetails) && $productdetails!='0')
					 { 
						 if(Session::get('lang_file')!='en_lang')
						 {
						   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
						 }
						 else
						 {
							$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
						 } 
						 foreach($getArrayOfOcc as $key=>$ocval)
						 {
						  if($productdetails->main_occasion_id==$key)
						  {
						   $occasion_name = $ocval;
						  }
						 }				  
				       echo $occasion_name;
				   }
				}
				@endphp 
                </div>
              </div>
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.OCCASION_DATE')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_DATE'): trans($OUR_LANGUAGE.'.OCCASION_DATE')}}:</div>
                <div class="wed_text">
              

 <?php if(isset($productdetails->occasion_date) && $productdetails->occasion_date!=''){ ?>
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($productdetails->occasion_date)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($productdetails->occasion_date)->format('d M Y')}}
											@endif   
											
											<?php }else{ echo 'N/A'; } ?>

                </div>
              </div>
			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')}}:</div>
                <div class="wed_text">
                  {{$order_id}}
                </div>
              </div>

              
              @if(isset($productdetails->order_paytype))
              			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.MY_PAYMENT')!= '')  ?  trans(Session::get('lang_file').'.MY_PAYMENT'): trans($OUR_LANGUAGE.'.MY_PAYMENT')}}:</div>
                <div class="wed_text">
                	<?php if(isset($productdetails->order_paytype) && $productdetails->order_paytype=='Bank_Transfer'){?>
				 {{ (Lang::has(Session::get('lang_file').'.NET_BANKING')!= '')  ?  trans(Session::get('lang_file').'.NET_BANKING'): trans($OUR_LANGUAGE.'.NET_BANKING')}}
				<?php } ?>
                 <?php if(isset($productdetails->order_paytype) && $productdetails->order_paytype=='Credit/Debit Cards'){?>
				 {{ (Lang::has(Session::get('lang_file').'.CREDIT_DEBIT')!= '')  ?  trans(Session::get('lang_file').'.CREDIT_DEBIT'): trans($OUR_LANGUAGE.'.CREDIT_DEBIT')}}
				<?php } if(isset($productdetails->order_paytype) && $productdetails->order_paytype=='Wallet'){ ?>
				{{ (Lang::has(Session::get('lang_file').'.Mobile_Wallets')!= '')  ?  trans(Session::get('lang_file').'.Mobile_Wallets'): trans($OUR_LANGUAGE.'.Mobile_Wallets')}}
				<?php } if(isset($productdetails->order_paytype) && ($productdetails->order_paytype=='COD' || $productdetails->order_paytype=='cod')){ ?>
				{{ (Lang::has(Session::get('lang_file').'.COD')!= '')  ?  trans(Session::get('lang_file').'.COD'): trans($OUR_LANGUAGE.'.COD')}}
				<?php } ?>
                </div>
              </div>
              @endif
 
@if(isset($productdetails->shipping_id) && $productdetails->shipping_id!='' && $productdetails->shipping_id!='3')
			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.SHIPPING_METHOD')!= '')  ?  trans(Session::get('lang_file').'.SHIPPING_METHOD'): trans($OUR_LANGUAGE.'.SHIPPING_METHOD')}}:</div>
                <div class="wed_text">
                 @php 
				 
				  $getName = Helper::getShippingMethodName($productdetails->shipping_id);
                  echo $getName;
				 @endphp
                </div>
              </div>

@endif


			  
            </div>
			<?php 
			$getSearchData = Helper::searchDetails($productdetails->order_id);
			?>
            <div class="types_ocs_right">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}:</div>
                <div class="wed_text">
                  @php if(isset($getSearchData->budget) && $getSearchData->budget!='' && strtolower($getSearchData->budget)!='n/a')
				 {
                  echo 'SAR '.number_format($getSearchData->budget,2);
				 }
				 else
				 {
				  echo 'N/A';
				 }
                 @endphp
                </div>
              </div>
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_ATTENDANCE'): trans($OUR_LANGUAGE.'.NO_OF_ATTENDANCE')}}:</div>
                <div class="wed_text">
                 @php if(isset($getSearchData->total_member) && $getSearchData->total_member!='')
				 {
                  echo $getSearchData->total_member;
				 }
				 else
				 {
				  echo 'N/A';
				 }
                 @endphp
                </div>
				
              </div>
              @if(isset($productdetails->created_at) && $productdetails->created_at!='')
			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}:</div>
                <div class="wed_text">
				

					@if(Session::get('lang_file')!='en_lang')
					{{ Carbon\Carbon::parse($productdetails->created_at)->format('Y  M d')}}
					@else
					{{ Carbon\Carbon::parse($productdetails->created_at)->format('d M Y')}}
					@endif
				
                </div>
              </div>@endif
			   @php if(strtolower($productdetails->order_paytype)!='cod' && strtolower($productdetails->order_paytype)!='wallet'){ @endphp
			  <div class="occs">
			     
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.TRANSACTION_ID')!= '')  ?  trans(Session::get('lang_file').'.TRANSACTION_ID'): trans($OUR_LANGUAGE.'.TRANSACTION_ID')}}:</div>
               


                <div class="wed_text">

				@php 
			if(isset($productdetails->order_paytype) && ($productdetails->order_paytype!='COD' || $productdetails->order_paytype!='cod')){
				if(isset($productdetails->transaction_id) && $productdetails->transaction_id!='')
				{
				  if(isset($productdetails->order_paytype) && $productdetails->order_paytype=='3'){echo 'N/A';}
				  else{echo $productdetails->transaction_id;}
				}
			}
				@endphp

                </div>
              </div>
              @php } @endphp

@if(isset($productdetails->shipping_charge) && $productdetails->shipping_charge!='' && $productdetails->shipping_charge!='0')
			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.SHIPPING_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.SHIPPING_CHARGE'): trans($OUR_LANGUAGE.'.SHIPPING_CHARGE')}}:</div>
                <div class="wed_text">
                  @php  
                  echo $productdetails->shipping_charge;
				  
                 @endphp
                </div>
              </div>
@endif







			  
            </div>
          </div>
          <div class="list_of_service">
			<div class="types_ocs_left">
			  <div class="occs">
               <b>
			@php
			if(isset($productdetails->product_type) && $productdetails->product_type=='beauty' && $productdetails->product_sub_type=='beauty_centers')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'مراكز تجميل';
			   }
			   else
			   {
				   $service_name = 'Beauty Centers'; 
			   }						 			  
			   echo $service_name;					  
			}
			if(isset($productdetails->product_type) && $productdetails->product_type=='beauty' && $productdetails->product_sub_type=='spa')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'منتجع صحي';
			   }
			   else
			   {
				   $service_name = 'Spa'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='beauty' && $productdetails->product_sub_type=='makeup_artists')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'فنان ماكياج';
			   }
			   else
			   {
				   $service_name = 'Makeup Artist'; 
			   }						 			  
			   echo $service_name;				  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='beauty' && $productdetails->product_sub_type=='makeup')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'منتجات المكياج';
			   }
			   else
			   {
				   $service_name = 'Makeup Products'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='beauty' && $productdetails->product_sub_type=='men_saloon')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'حلاق';
			   }
			   else
			   {
				   $service_name = 'Barber'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='car_rental' && $productdetails->product_sub_type=='car')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'تأجير السيارات';
			   }
			   else
			   {
				   $service_name = 'Car Rentals'; 
			   }						 			  
			   echo $service_name;				  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='travel' && $productdetails->product_sub_type=='travel')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'طب الأسنان و الأمراض الجلدية';
			   }
			   else
			   {
				   $service_name = 'Travel Agencies'; 
			   }						 			  
			   echo $service_name;				  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='clinic' && $productdetails->product_sub_type=='cosmetic')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'مستحضرات التجميل والليزر';
			   }
			   else
			   {
				   $service_name = 'Cosmetics And Laser'; 
			   }						 			  
			   echo $service_name;						  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='clinic' && $productdetails->product_sub_type=='skin')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'طب الأسنان و الأمراض الجلدية';
			   }
			   else
			   {
				   $service_name = 'Dental And Dermatology'; 
			   }						 			  
			   echo $service_name;							  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='shopping' && $productdetails->product_sub_type=='tailor')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'الخياطين';
			   }
			   else
			   {
				   $service_name = 'Tailors'; 
			   }						 			  
			   echo $service_name;						  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='shopping' && $productdetails->product_sub_type=='dress')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'فساتين';
			   }
			   else
			   {
				   $service_name = 'Dresses'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='shopping' && $productdetails->product_sub_type=='perfume')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'العطور';
			   }
			   else
			   {
				   $service_name = 'Perfumes'; 
			   }						 			  
			   echo $service_name;						  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='shopping' && $productdetails->product_sub_type=='abaya')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'العباءة';
			   }
			   else
			   {
				   $service_name = 'Abaya'; 
			   }						 			  
			   echo $service_name;						  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='shopping' && $productdetails->product_sub_type=='gold')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'مجوهرات';
			   }
			   else
			   {
				   $service_name = 'Jewellery'; 
			   }						 			  
			   echo $service_name;						  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='hall' && $productdetails->product_sub_type=='hall')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'فندق القاعات';
			   }
			   else
			   {
				   $service_name = 'Hotel Halls'; 
			   }						 			  
			   echo $service_name;						  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='food' && $productdetails->product_sub_type=='dates')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'تواريخ';
			   }
			   else
			   {
				   $service_name = 'Dates'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='food' && $productdetails->product_sub_type=='buffet')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'البوفيهات';
			   }
			   else
			   {
				   $service_name = 'Buffets'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='occasion' && $productdetails->product_sub_type=='cosha')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'اكتشف';
			   }
			   else
			   {
				   $service_name = 'Kosha'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='food' && $productdetails->product_sub_type=='dessert')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'الحلوى';
			   }
			   else
			   {
				   $service_name = 'Desserts'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='occasion' && ($productdetails->product_sub_type=='photography' || $productdetails->product_sub_type=='video'))
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'استوديو تصوير';
			   }
			   else
			   {
				   $service_name = 'Photography Studio'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='occasion' && $productdetails->product_sub_type=='hospitality')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'الاستقبال والضيافة';
			   }
			   else
			   {
				   $service_name = 'Reception & Hospitality'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='music' && $productdetails->product_sub_type=='acoustic')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'أنظمة الصوت';
			   }
			   else
			   {
				   $service_name = 'Sound Sysytems'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='occasion' && $productdetails->product_sub_type=='roses')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'ورود';
			   }
			   else
			   {
				   $service_name = 'Roses'; 
			   }						 			  
			   echo $service_name;					  
			}
            
            else if(isset($productdetails->product_type) && $productdetails->product_type=='band' && $productdetails->product_sub_type=='band')
            {					  
               if(Session::get('lang_file')!='en_lang')
               {
                   $service_name = 'فرق شعبية';
               }
               else
               {
                   $service_name = 'Popular Bands'; 
               }						 			  
               echo $service_name;					  
            }
			else if(isset($productdetails->product_type) && $productdetails->product_type=='singer' && $productdetails->product_sub_type=='singer')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'مطرب';
			   }
			   else
			   {
				   $service_name = 'Singer'; 
			   }						 			  
			   echo $service_name;					  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='recording' && $productdetails->product_sub_type=='recording')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'أنظمة الصوت';
			   }
			   else
			   {
				   $service_name = 'Sound Systems'; 
			   }						 			  
			   echo $service_name;				  
			}
			else if(isset($productdetails->product_type) && $productdetails->product_type=='occasion' && $productdetails->product_sub_type=='invitations')
			{					  
			   if(Session::get('lang_file')!='en_lang')
			   {
				   $service_name = 'دعوة الكترونية';
			   }
			   else
			   {
				   $service_name = 'Electronic Invitation'; 
			   }						 			  
			   echo $service_name;					  
			}
			@endphp
			</b> 
            </div>
            <div class="occs">
                <div class="occus_text">
				@php if(isset($productdetails->product_type) && ($productdetails->product_type=='car_rental' || $productdetails->product_type=='travel')){ @endphp
				{{ (Lang::has(Session::get('lang_file').'.AGENCY_NAME')!= '')  ?  trans(Session::get('lang_file').'.AGENCY_NAME'): trans($OUR_LANGUAGE.'.AGENCY_NAME')}}
				@php } elseif(isset($productdetails->product_type) && $productdetails->product_type=='hall'){ @endphp 
				{{ (Lang::has(Session::get('lang_file').'.HOTAL_NAME')!= '')  ?  trans(Session::get('lang_file').'.HOTAL_NAME'): trans($OUR_LANGUAGE.'.HOTAL_NAME')}}
				@php }elseif(isset($productdetails->product_type) && $productdetails->product_type=='clinic'){ @endphp 
				{{ (Lang::has(Session::get('lang_file').'.CLINIC_NAME')!= '')  ?  trans(Session::get('lang_file').'.CLINIC_NAME'): trans($OUR_LANGUAGE.'.CLINIC_NAME')}}
				@php }elseif(isset($productdetails->product_type) && ($productdetails->product_type=='band' || $productdetails->product_type=='singer')){ @endphp 
				{{ (Lang::has(Session::get('lang_file').'.Name')!= '')  ?  trans(Session::get('lang_file').'.Name'): trans($OUR_LANGUAGE.'.Name')}}
				@php }else{ @endphp
				{{ (Lang::has(Session::get('lang_file').'.SHOP_NAME')!= '')  ?  trans(Session::get('lang_file').'.SHOP_NAME'): trans($OUR_LANGUAGE.'.SHOP_NAME')}}@php }@endphp: </div>
                <div class="wed_text">
                  <?php				  
				    if(isset($productdetails->shop_id) && $productdetails->shop_id!='')
					{
                       if(isset($productdetails->product_type) && ($productdetails->product_sub_type=='singer' || $productdetails->product_sub_type=='band' || $productdetails->product_sub_type=='recording'))
                       {
                         $enq_id = Helper::quoteRequested($productdetails->shop_id);
                         $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
						 echo $dget_category->singer_name;                       
                       }
                       else
                       {
                         $dget_category_name = Helper::getshopname($productdetails->shop_id);
                         $mc_name = 'mc_name';
                         if(Session::get('lang_file')!='en_lang')
					     {
                           $mc_name= 'mc_name_ar'; 
					     }
					     echo $dget_category_name->$mc_name;
                       }
					   
					}
					
				    /*if(isset($productdetails->shop_id) && $productdetails->shop_id!='')
					{
					   $dget_category_name = Helper::getshopname($productdetails->shop_id);
					   $mc_name = 'mc_name';
					   if(Session::get('lang_file')!='en_lang')
					   {
						  $mc_name= 'mc_name_ar'; 
					   }
					   echo $dget_category_name->$mc_name;
					}*/
				 ?>
                </div>
              </div>
			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.ADDRESS')!= '')  ?  trans(Session::get('lang_file').'.ADDRESS'): trans($OUR_LANGUAGE.'.ADDRESS')}}:</div>
                <div class="wed_text">
                  <?php
				    if(isset($productdetails->shop_id) && $productdetails->shop_id!='')
					{
					   if(isset($productdetails->product_type) && ($productdetails->product_sub_type=='singer' || $productdetails->product_sub_type=='band' || $productdetails->product_sub_type=='recording'))
                       {
                         $enq_id = Helper::quoteRequested($productdetails->shop_id);
                         $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
						 $musicAddress = Helper::getMusicInfo($dget_category->music_id);
						 //echo '<pre>';print_r($musicAddress);die;
						 if(isset($musicAddress->address) && $musicAddress->address!=''){echo $musicAddress->address;}else{echo 'N/A';}
						 echo @$musicAddress->address;  
						 if(isset($musicAddress->google_map_url) && $musicAddress->google_map_url!=''){?>
						 <a target="_blank" href="<?=@$musicAddress->google_map_url?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a>
						 <?php }                     
                       }
                       else
                       {
                       	 $address = 'address';
						   if(Session::get('lang_file')!='en_lang')
						   {
							  $address= 'address_ar'; 
						   }
					       $dget_category_name = Helper::getshopname($productdetails->shop_id);
					       if(isset($dget_category_name->$address) && $dget_category_name->$address!=''){
						  
						   echo $dget_category_name->$address;
						   if(isset($dget_category_name->google_map_address) && $dget_category_name->google_map_address!=''){?>
					   <a target="_blank" href="<?=@$dget_category_name->google_map_address?>"><img width="30" src="{{ url('') }}/themes/images/placemarker.png" /></a>
					  <?php }

                           } else{ echo 'N/A'; }


					   } 


					}else{
					  	echo 'N/A';
					  } ?>
                </div>
              </div>
			  <?php /*if(isset($productdetails->insurance_amount) && $productdetails->insurance_amount!=''){ ?>
			  <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')}}:</div>
                <div class="wed_text">
                  @php
				   echo 'SAR '.number_format($productdetails->insurance_amount,2);
				  @endphp
                </div>
              </div>
			  <?php }*/ ?>
			</div>
			<br />
              <div class="myaccount-table mts">
            
                  <div class="table-cart-wrapper all-order-table-wrapper">
                    <table class="serviceorder-det-table">
                      <tbody>
                        <tr class="tr table_heading_tr">
                          <td class="table_heading td1">{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}</td>
						  <?php if(isset($productdetails->product_type) && ($productdetails->product_sub_type=='singer' || $productdetails->product_sub_type=='band' || $productdetails->product_sub_type=='recording')){?>
						  <td class="table_heading td3">{{ (Lang::has(Session::get('lang_file').'.NAME')!= '')  ?  trans(Session::get('lang_file').'.NAME'): trans($OUR_LANGUAGE.'.NAME')}}</td>
						  <?php }else{  
						  if(isset($productdetails->product_sub_type) && ($productdetails->product_sub_type=='spa' || $productdetails->product_sub_type=='beauty_centers' || $productdetails->product_sub_type=='cosmetic' || $productdetails->product_sub_type=='skin' || $productdetails->product_sub_type=='makeup_artists' || $productdetails->product_sub_type=='men_saloon' || $productdetails->product_sub_type=='invitations'))
						  {}else if(isset($productdetails->product_sub_type) && $productdetails->product_sub_type=='car'){?>
						  <td class="table_heading td2">{{ (Lang::has(Session::get('lang_file').'.DAYS')!= '')  ?  trans(Session::get('lang_file').'.DAYS'): trans($OUR_LANGUAGE.'.DAYS')}}</td>
						  <?php } else if($productdetails->product_sub_type=='travel'){?>
						  <td class="table_heading td2">{{ (Lang::has(Session::get('lang_file').'.NO_OF_PEOPLE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_PEOPLE'): trans($OUR_LANGUAGE.'.NO_OF_PEOPLE')}}</td>
						  <?php } else { ?>
                          <td class="table_heading td2">{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}</td>
						  <?php } ?>
						  <td class="table_heading td3">{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}</td>
						  <?php } ?>
                          <td class="table_heading td4">{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}</td>
                        </tr>
                        <tr class="tr mobile-first-row">
						<?php if(isset($productdetails->product_sub_type) && ($productdetails->product_sub_type=='spa' || $productdetails->product_sub_type=='beauty_centers'))
						  {echo '<td colspan="3">';}else if(isset($productdetails->product_sub_type) && $productdetails->product_sub_type=='car'){echo '<td colspan="4">';}else if(isset($productdetails->product_sub_type) && $productdetails->product_sub_type=='travel'){echo '<td colspan="4">';}?>
                          
								<!-------- Singer Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu=0; if(isset($getSinger) && count($getSinger) > 0){ foreach($getSinger as $val){ $basetotal = ($basetotal+$val->total_price);
								 			$totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else
                                                {{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
                                                   @endif
                                                @endif
												</td>
												
												<td class="td td2" data-title="">
												<?php 
												 $enq_id = Helper::quoteRequested($val->shop_id);
												 $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
												 echo $dget_category->singer_name;
												?>
												</td>                                                                
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								      
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php if(isset($productdetails->order_taxAmt) && $productdetails->order_taxAmt!='')
								 {
								  echo 'SAR '.number_format($productdetails->order_taxAmt,2);
								 }
								 else
								 {
								  echo 'N/A';
								 }
								@endphp
								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$productdetails->order_taxAmt-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                      
                                <!-------- Singer Order End------------------>
								
								<!-------- Popular Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu=0; if(isset($getBand) && count($getBand) > 0){ foreach($getBand as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else
                                                {{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
                                                   @endif
                                                @endif
												</td>
												
												<td class="td td2" data-title="">
												<?php 
												 $enq_id = Helper::quoteRequested($val->shop_id);
												 $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
												 echo $dget_category->singer_name;
												?>
												</td>                                                                
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								      
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php if(isset($productdetails->order_taxAmt) && $productdetails->order_taxAmt!='')
								 {
								  echo 'SAR '.number_format($productdetails->order_taxAmt,2);
								 }
								 else
								 {
								  echo 'N/A';
								 }
								@endphp
								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$productdetails->order_taxAmt-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                      
                                <!-------- Popular Order End------------------>
								
								<!-------- Sound Sysytem form Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu=0; if(isset($getRecording) && count($getRecording) > 0){ foreach($getRecording as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                               
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else
                                                {{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
                                                   @endif
                                                @endif
												</td>
												
												<td class="td td3" data-title="">
												<?php 
												 $enq_id = Helper::quoteRequested($val->shop_id);
												 $dget_category = Helper::quoteRequestedName($enq_id->enq_id);
												 echo $dget_category->singer_name;
												?>
												</td>                                                                
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								      
                         
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php if(isset($productdetails->order_taxAmt) && $productdetails->order_taxAmt!='')
								 {
								  echo 'SAR '.number_format($productdetails->order_taxAmt,2);
								 }
								 else
								 {
								  echo 'N/A';
								 }
								@endphp
								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$productdetails->order_taxAmt-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                      
                                <!-------- Sound Sysytem form Order End------------------>
								<!-------- Electronic Invitation Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu=0; if(isset($getInvitations) && count($getInvitations) > 0){ foreach($getInvitations as $val){
								 		 $no_of_invitees = Helper::noofinvitees($val->order_id);
								 		
								 			$no_of_attends = Helper::noofattends($val->order_id);
								  $basetotal = ($basetotal+$val->total_price);
								  	  $totalinsu = ($totalinsu+$val->insurance_amount);
								  ?>
                                <div class="goldencartitems">
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else


                                                {{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
                                                   @endif
                                                @endif
												</td>										
											
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                                                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								      <div class="myac-all-order">
										<?php 
										  $serviceInfo = Helper::getProduckInfo($val->product_id);
										  $getInorder = Helper::getInvitationOrder($val->order_id,$val->cus_id);
										  //echo '<pre>';print_r($getInorder);die;
										  if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
										  //$moredetail  = Helper::bookingCarRental($val->cus_id,$val->order_id,$val->product_id);
										 ?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.INVITATION_TYPE')!= '')  ?  trans(Session::get('lang_file').'.INVITATION_TYPE'): trans($OUR_LANGUAGE.'.INVITATION_TYPE')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	  
											<?php 
											if(isset($val->bookingdetail) && $val->bookingdetail!='')
										    { //echo '<pre>';print_r($val->bookingdetail->);
											?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.OCCASION_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASION_NAME'): trans($OUR_LANGUAGE.'.OCCASION_NAME')}}:</span> <?php echo $val->bookingdetail->occasion_name; ?> </div>  <!-- all-order-td -->
											<?php } ?> 	

											 <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.No_of_invites')!= '')  ?  trans(Session::get('lang_file').'.No_of_invites'): trans($OUR_LANGUAGE.'.No_of_invites')}}:</span>  {{ $no_of_invitees }}</div> <!-- all-order-td -->
											 <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DATE')!= '')  ?  trans(Session::get('lang_file').'.DATE'): trans($OUR_LANGUAGE.'.DATE')}}:</span>  <?php if(isset($getInorder->date) && $getInorder->date!=''){echo date('Y M d',strtotime($getInorder->date));} ?></div> <!-- all-order-td -->	
											 <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.TIME')!= '')  ?  trans(Session::get('lang_file').'.TIME'): trans($OUR_LANGUAGE.'.TIME')}}:</span>  {{ $getInorder->time or '' }}</div> <!-- all-order-td -->		  
											 <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.VENUE')!= '')  ?  trans(Session::get('lang_file').'.VENUE'): trans($OUR_LANGUAGE.'.VENUE')}}:</span>  {{ $getInorder->venue or '' }}</div> <!-- all-order-td -->	
											 <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.MESSAGE')!= '')  ?  trans(Session::get('lang_file').'.MESSAGE'): trans($OUR_LANGUAGE.'.MESSAGE')}}:</span>  {{ $getInorder->invitation_msg or '' }}</div> <!-- all-order-td -->		  
											<?php 
											if(isset($no_of_attends) && $no_of_attends>0)
										    { //echo '<pre>';print_r($val->bookingdetail->);
											?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.No_of_Attended')!= '')  ?  trans(Session::get('lang_file').'.No_of_Attended'): trans($OUR_LANGUAGE.'.No_of_Attended')}}:</span> {{ $no_of_attends }} ( <a href="{{ route('invitation-attended-list',['id'=>$val->order_id]) }}"> {{ (Lang::has(Session::get('lang_file').'.VIEW_ALL')!= '')  ?  trans(Session::get('lang_file').'.VIEW_ALL'): trans($OUR_LANGUAGE.'.VIEW_ALL')}} </a>) </div>  <!-- all-order-td -->
											<?php } ?> 
									          
											</div> <!-- all-order-tr -->	
											
                                        </div>


                                        	<div class="all-order-tr">
										  
										   	
									          
											</div> <!-- all-order-tr -->	
											
                                        </div>






                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 

									@php
                         $nam=$basetotal-$couponcode-$totalinsu;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?>
                      			<br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif

								
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                      
                                <!-------- Electronic Invitation Order End------------------>
                                <!-------- Car Rental Order Start------------------>
								 <?php $i=1;$basetotal = 0;$couponcode=0; $totalinsu ==0;								 	
								 if(isset($getOrderCarRental) && count($getOrderCarRental) > 0){ foreach($getOrderCarRental as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$couponcode=Helper::getorderedfromcoupanamount($val->order_id); 
								 			$totalinsu = ($totalinsu+$val->insurance_amount);
								 			$discountcouponcode=$val->coupon_code;	
								 	?>
                                <div class="goldencartitems">
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else


                                                {{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
                                                   @endif
                                                @endif
												</td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}elseif(isset($val->product_sub_type) && $val->product_sub_type=='car'){?>
												<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.DAYS')!= '')  ?  trans(Session::get('lang_file').'.DAYS'): trans($OUR_LANGUAGE.'.DAYS')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
												<?php }else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								      <div class="myac-all-order">
										<?php 
										  $serviceInfo = Helper::getProduckInfo($val->product_id);
										  if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){ $getprice = $serviceInfo->pro_disprice;}else{ $getprice  = $serviceInfo->pro_price;}
										  //$moredetail  = Helper::bookingCarRental($val->cus_id,$val->order_id,$val->product_id);
										 ?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	  
											<?php if(isset($val->bookingdetail[0]->rental_date) && $val->bookingdetail[0]->rental_date!=''){ ?>          
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}:</span> 
												@php
												if(isset($val->bookingdetail[0]->rental_date) && $val->bookingdetail[0]->rental_date!=''){
                      $bookorderrental_datetime=strtotime($val->bookingdetail[0]->rental_date);
                     $bokkingrentalorderedtime = date("d M Y",$bookorderrental_datetime);      
                                    echo $bokkingrentalorderedtime;
                                }
                                   @endphp



										</div>  <!-- all-order-td -->	
									<?php } if(isset($val->bookingdetail[0]->return_date) && $val->bookingdetail[0]->return_date!=''){?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.RETURNING_DATE')!= '')  ?  trans(Session::get('lang_file').'.RETURNING_DATE'): trans($OUR_LANGUAGE.'.RETURNING_DATE')}}:</span> 
												@php
												if(isset($val->bookingdetail[0]->return_date) && $val->bookingdetail[0]->return_date!=''){
                      $bookordertime=strtotime($val->bookingdetail[0]->return_date);
                     $bokkingorderedtime = date("d M Y",$bookordertime);      
                                    echo $bokkingorderedtime;
                                }
                                   @endphp
												 </div>  <!-- all-order-td -->	
									    <?php } 
										$get_car_model = Helper::getProductName($val->product_id,$val->merchant_id,'Car Model');
										if(isset($get_car_model->value) && $get_car_model->value!=''){?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CAR_MODEL')!= '')  ?  trans(Session::get('lang_file').'.CAR_MODEL'): trans($OUR_LANGUAGE.'.CAR_MODEL')}}:</span> <?=$get_car_model->value?> </div>  <!-- all-order-td -->	
									    <?php } ?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRICE_PER_DAY')!= '')  ?  trans(Session::get('lang_file').'.PRICE_PER_DAY'): trans($OUR_LANGUAGE.'.PRICE_PER_DAY')}}:</span> SAR <?php echo @$getprice; ?> </div>  <!-- all-order-td -->	
									          
											</div> <!-- all-order-tr -->	
											<div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                        </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php
                         $nam=$basetotal-$couponcode-$totalinsu;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?><br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                      
                                <!-------- Car Rental Order End------------------>
                                
                                <!-------- Travel Agency Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu=0; if(isset($getOrderTravel) && count($getOrderTravel) > 0){ foreach($getOrderTravel as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">                                     
                                  <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else


                                                {{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
                                                @endif  @endif
												</td>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									<div class="myac-all-order">
										<?php $serviceInfo = Helper::getProduckInfo($val->product_id);

											$packageduration = Helper::orderedproductattribute($val->product_id);
										 ?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}}:</span>  <?php echo $serviceInfo->pro_title; ?>

										</div> <!-- all-order-td -->	 
											<?php $get_location = Helper::getProductName($val->product_id,$val->merchant_id,'Location'); if(isset($get_location->value) && $get_location->value!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')}}:</span>  <?php echo $get_location->value; ?></div> <!-- all-order-td -->	 <?php } ?>
											<?php if(isset($val->bookingdetail->rental_date) && $val->bookingdetail->rental_date!=''){ ?>           
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PACKAGE_DATE')!= '')  ?  trans(Session::get('lang_file').'.PACKAGE_DATE'): trans($OUR_LANGUAGE.'.PACKAGE_DATE')}}:</span> <?php echo $val->bookingdetail->rental_date.' '.$val->bookingdetail->rental_time;?> <br>
											<span>{{ (Lang::has(Session::get('lang_file').'.PACKAGE_DURATION')!= '')  ?  trans(Session::get('lang_file').'.PACKAGE_DURATION'): trans($OUR_LANGUAGE.'.PACKAGE_DURATION')}}:</span>
												{{ $packageduration->value }}</div>  <!-- all-order-td -->	
											<?php }  
										$get_car_model = Helper::getProductName($val->product_id,$val->merchant_id,'Extra Service');
										if(isset($get_car_model->value) && $get_car_model->value!=''){?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.EXTRS_SERVICE')!= '')  ?  trans(Session::get('lang_file').'.EXTRS_SERVICE'): trans($OUR_LANGUAGE.'.EXTRS_SERVICE')}}:</span> <?=$get_car_model->value?> </div>  <!-- all-order-td -->	
									    <?php } ?>
											
											</div> <!-- all-order-tr -->
											<div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>	
                                        </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 

									 @php
                      $vatamonu = Helper::calculatevat($val->order_id,$val->total_price);
                        $totalnetamount=$val->total_price + $vatamonu;
                      @endphp
								@php if(isset($vatamonu) && $vatamonu!='')
								 {
								  echo 'SAR '.number_format($vatamonu,2);



								 }
								 else
								 {
								  echo 'N/A';
								 }
								@endphp
								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($totalnetamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>      
                                <!--------  Travel Agency Order End------------------>
								
                                <!-------- Beauty Center Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu=0; $couponcode=0; if(isset($getOrderBeautyCenters) && count($getOrderBeautyCenters) > 0){ foreach($getOrderBeautyCenters as $val){ 

								 	$couponcode=Helper::getorderedfromcoupanamount($val->order_id); 
								 			
								 			$discountcouponcode=$val->coupon_code;

								 	$basetotal = ($basetotal+$val->total_price);
								 	$totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
                                                

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else
												{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
												@endif
                                                @endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php										
										if(isset($val->bookingdetail) && $val->bookingdetail!='')
										{
										  $service_charge = Helper::getshopname($val->bookingdetail->shop_id);
										  $serviceInfo = Helper::getProduckInfo($val->bookingdetail->service_id);
										  $staffname = Helper::getStaffNameOne($val->bookingdetail->staff_id);
										  $getAddresInfo = Helper::getuserinfo($val->bookingdetail->cus_id);
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	 
									 
										     <?php if(isset($staffname->staff_member_name) && $staffname->staff_member_name!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STAFF_NAME')!= '')  ?  trans(Session::get('lang_file').'.STAFF_NAME'): trans($OUR_LANGUAGE.'.STAFF_NAME')}}:</span> <?php echo $staffname->staff_member_name; ?></div>  <!-- all-order-td -->	
											<?php } if(isset($serviceInfo->service_hour) && $serviceInfo->service_hour!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DURATION')!= '')  ?  trans(Session::get('lang_file').'.DURATION'): trans($OUR_LANGUAGE.'.DURATION')}}:</span> <?php echo $serviceInfo->service_hour; ?></div>  <!-- all-order-td -->	 
											<?php } ?>
											</div> <!-- all-order-tr -->	
								          <div class="all-order-tr">
										  
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_PLACE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_PLACE'): trans($OUR_LANGUAGE.'.BOOKING_PLACE')}}:</span> <?php echo $val->bookingdetail->booking_place; ?></div> <!-- all-order-td -->	
										 									 
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}: </span><?php echo date('d M Y',strtotime($val->bookingdetail->booking_date)); ?></div> <!-- all-order-td -->
										<?php if(isset($val->bookingdetail->booking_place) && $val->bookingdetail->booking_place=='home'){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.ADDRESS')!= '')  ?  trans(Session::get('lang_file').'.ADDRESS'): trans($OUR_LANGUAGE.'.ADDRESS')}}: </span><?php echo $getAddresInfo->cus_address1; ?></div> <!-- all-order-td -->
										<?php if(isset($service_charge->home_visit_charge) && $service_charge->home_visit_charge!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.HOME_VISIT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.HOME_VISIT_CHARGE'): trans($OUR_LANGUAGE.'.HOME_VISIT_CHARGE')}}:</span> SAR <?php echo number_format($service_charge->home_visit_charge,2); ?></div>  <!-- all-order-td -->	 
											
										<?php }} if(isset($val->bookingdetail->start_time) && $val->bookingdetail->start_time!=''){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_TIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_TIME'): trans($OUR_LANGUAGE.'.BOOKING_TIME')}}: </span><?php echo $val->bookingdetail->start_time; ?></div> <!-- all-order-td -->
										<?php } ?>		 
										 </div> <!-- all-order-tr -->	
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
							 
																		
										<?php } ?>
                                        </div>
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php
                         $nam=$basetotal-$couponcode-$totalinsu;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?><br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								  echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                 
                                <!-------- Beauty Center End------------------>
								
								<!-------- Beauty Spa Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu =0; $couponcode=0;if(isset($getOrderSpa) && count($getOrderSpa) > 0){ 

								 	foreach($getOrderSpa as $val){
								 			$couponcode=Helper::getorderedfromcoupanamount($val->order_id); 
								 			$basetotal = ($basetotal+$val->total_price);
								 			$discountcouponcode=$val->coupon_code;
								 			$totalinsu = ($totalinsu+$val->insurance_amount);
								 		?>
                                <div class="goldencartitems">                
                                    
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
											@if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
											<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <div class="myac-all-order">
										<?php										
										if(isset($val->bookingdetail) && $val->bookingdetail!='')
										{
										  $service_charge = Helper::getshopname($val->bookingdetail->shop_id);
										  $serviceInfo = Helper::getProduckInfo($val->bookingdetail->service_id);
										  $staffname = Helper::getStaffNameOne($val->bookingdetail->staff_id);
										  $getAddresInfo = Helper::getuserinfo($val->bookingdetail->cus_id);
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	 
									        <?php if(isset($staffname->staff_member_name) && $staffname->staff_member_name!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STAFF_NAME')!= '')  ?  trans(Session::get('lang_file').'.STAFF_NAME'): trans($OUR_LANGUAGE.'.STAFF_NAME')}}:</span> <?php echo $staffname->staff_member_name; ?></div>  <!-- all-order-td -->	
											<?php }if(isset($serviceInfo->service_hour) && $serviceInfo->service_hour!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DURATION')!= '')  ?  trans(Session::get('lang_file').'.DURATION'): trans($OUR_LANGUAGE.'.DURATION')}}:</span> <?php echo $serviceInfo->service_hour; ?></div>  <!-- all-order-td -->	 
											<?php } ?>
											</div> <!-- all-order-tr -->	
								          <div class="all-order-tr">
										  
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_PLACE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_PLACE'): trans($OUR_LANGUAGE.'.BOOKING_PLACE')}}:</span> <?php echo $val->bookingdetail->booking_place; ?></div> <!-- all-order-td -->	
										 									 
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}: </span><?php echo date('d M Y',strtotime($val->bookingdetail->booking_date)); ?></div> <!-- all-order-td -->
										<?php if(isset($val->bookingdetail->booking_place) && $val->bookingdetail->booking_place=='home'){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.ADDRESS')!= '')  ?  trans(Session::get('lang_file').'.ADDRESS'): trans($OUR_LANGUAGE.'.ADDRESS')}}: </span><?php echo $getAddresInfo->cus_address1; ?></div> <!-- all-order-td -->
										<?php if(isset($service_charge->home_visit_charge) && $service_charge->home_visit_charge!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.HOME_VISIT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.HOME_VISIT_CHARGE'): trans($OUR_LANGUAGE.'.HOME_VISIT_CHARGE')}}:</span> SAR <?php echo number_format($service_charge->home_visit_charge,2); ?></div>  <!-- all-order-td -->	 
											
										<?php }} if(isset($val->bookingdetail->start_time) && $val->bookingdetail->start_time!=''){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_TIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_TIME'): trans($OUR_LANGUAGE.'.BOOKING_TIME')}}: </span><?php echo $val->bookingdetail->start_time; ?></div> <!-- all-order-td -->
										<?php } ?>		 
										 </div> <!-- all-order-tr -->	
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
										<?php } ?>
                                        </div>						
                                  
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php
                         $nam=$basetotal-$couponcode-$totalinsu =0;;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?><br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                     
                                <!-------- Beauty Spa End------------------>
								
								<!-------- Makeup Products Order Start------------------>
								 <?php $i=1;$basetotal = 0; $couponcode=0; $totalinsu =0; if(isset($getOrderMakeup) && count($getOrderMakeup) > 0){ foreach($getOrderMakeup as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 	$couponcode=Helper::getorderedfromcoupanamount($val->order_id); 
								 			
								 			$discountcouponcode=$val->coupon_code;
								 	?>
                                <div class="goldencartitems">                
                                    
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										       
											@if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif

												</td>
											<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <div class="myac-all-order">
										<?php 
										$serviceInfo = Helper::getProduckInfo($val->product_id); 
										if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	 
											 <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_IMAGE')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_IMAGE'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
											</div> <!-- all-order-tr -->
                                        </div>						
                                  
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php
                         $nam=$basetotal-$couponcode-$totalinsu;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?><br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif
								
							
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                     
                                <!-------- Makeup Products Order End------------------>
								
								<!-------- Sound Sysytems Acoustic Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu =0; if(isset($SoundSysytemsAcoustic) && count($SoundSysytemsAcoustic) > 0){ foreach($SoundSysytemsAcoustic as $val){ $basetotal = ($basetotal+$val->total_price);
								 	$totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">                
                                    
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
											@if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
											<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <div class="myac-all-order">
										<?php										
										  $serviceInfo = Helper::getProduckInfo($val->product_id);
										  if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_NAME')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_NAME'): trans($OUR_LANGUAGE.'.PRODUCT_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
										    @php if($val->buy_rent=='buy'){ @endphp
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->
											@php } @endphp	
											</div> <!-- all-order-tr -->	
								          <div class="all-order-tr">  
										<?php if(isset($val->insurance_amount) && $val->insurance_amount!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')}}: SAR </span><?php echo number_format($val->insurance_amount,2); ?></div> <!-- all-order-td -->		
										<?php }if(isset($val->bookingdetail->rental_date) && $val->bookingdetail->rental_date!=''){ ?>								 
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}: </span><?php echo date('d M Y',strtotime($val->bookingdetail->rental_date)).' '.$val->bookingdetail->rental_time; ?></div> <!-- all-order-td -->
										<?php }if(isset($val->bookingdetail->return_date)){ ?>								 
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.RETURNING_DATE')!= '')  ?  trans(Session::get('lang_file').'.RETURNING_DATE'): trans($OUR_LANGUAGE.'.RETURNING_DATE')}}: </span><?php echo date('d M Y',strtotime($val->bookingdetail->return_date)).' '.$val->bookingdetail->return_time; ?></div> <!-- all-order-td -->
										<?php } ?>
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                        </div>						
                                  
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php if(isset($productdetails->order_taxAmt) && $productdetails->order_taxAmt!='')
								 {
								  echo 'SAR '.number_format($productdetails->order_taxAmt,2);
								 }
								 else
								 {
								  echo 'N/A';
								 }
								@endphp
								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$productdetails->order_taxAmt-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                     
                                <!-------- Sound Sysytems Acoustic Order End------------------>
								
								<!-------- Kosha Order Start------------------>
								 <?php $i=1;$basetotal = 0; $couponcode=0; $totalinsu =0; if(isset($getOrderKosha) && count($getOrderKosha) > 0){ foreach($getOrderKosha as $val){ $basetotal = ($basetotal+$val->total_price);
								 	$totalinsu = ($totalinsu+$val->insurance_amount);
								 	$couponcode=Helper::getorderedfromcoupanamount($val->order_id); 
								 	$orderedprodattrib=Helper::getproductorderattribute($val->order_id,$val->product_id); 								 			
								 			$discountcouponcode=$val->coupon_code;
								 	?>
                                <div class="goldencartitems">                
                                    
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										       
											@if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif

												</td>
											<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
												<?php if(isset($orderedprodattrib->quantity) && $orderedprodattrib->quantity>0){ $pqty=$orderedprodattrib->quantity; }else{ $pqty=$val->quantity; } ?>
											 @if(isset($pqty) && $pqty!='')
                                            {{ $pqty}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <div class="myac-all-order">
										<?php 
										$serviceInfo = Helper::getProduckInfo($val->product_id); 
										if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->	                    <?php if(isset($val->insurance_amount) && $val->insurance_amount!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')}}:</span>SAR <?php echo number_format($val->insurance_amount,2); ?></div> <!-- all-order-td -->	
											<?php } ?>
											
											</div> <!-- all-order-tr -->
											<div class="all-order-tr">
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_IMAGE')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_IMAGE'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
											</div>
                                        </div>						
                                  
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php
                         $nam=$basetotal-$couponcode-$totalinsu;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?><br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif
								 </div>
							
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                    
                                <!-------- Kosha Order End------------------>
								
								 <!-------- Makeup Artist Order Start------------------>
								 <?php $i=1;$basetotal = 0;if(isset($getOrderMakeupArtist) && count($getOrderMakeupArtist) > 0){ foreach($getOrderMakeupArtist as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										       @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers' || $val->product_sub_type=='makeup_artists' || $val->product_sub_type=='men_saloon')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <div class="myac-all-order">
										<?php										
										if(isset($val->bookingdetail) && $val->bookingdetail!='')
										{
										  $service_charge = Helper::getshopname($val->bookingdetail->shop_id);
										  $serviceInfo = Helper::getProduckInfo($val->bookingdetail->service_id);
										  $staffname = Helper::getStaffNameOne($val->bookingdetail->staff_id);
										  $getAddresInfo = Helper::getuserinfo($val->bookingdetail->cus_id);
										?>
										  <div class="all-order-tr">
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	 
									 
										     <?php if(isset($staffname->staff_member_name) && $staffname->staff_member_name!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STAFF_NAME')!= '')  ?  trans(Session::get('lang_file').'.STAFF_NAME'): trans($OUR_LANGUAGE.'.STAFF_NAME')}}:</span> <?php echo $staffname->staff_member_name; ?></div>  <!-- all-order-td -->	
											<?php } if(isset($serviceInfo->service_hour) && $serviceInfo->service_hour!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DURATION')!= '')  ?  trans(Session::get('lang_file').'.DURATION'): trans($OUR_LANGUAGE.'.DURATION')}}:</span> <?php echo $serviceInfo->service_hour; ?></div>  <!-- all-order-td -->	 
											<?php } ?>
											</div> <!-- all-order-tr -->	
								          <div class="all-order-tr">
										  
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_PLACE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_PLACE'): trans($OUR_LANGUAGE.'.BOOKING_PLACE')}}:</span> <?php echo $val->bookingdetail->booking_place; ?></div> <!-- all-order-td -->	
										 									 
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}: </span><?php echo date('d M Y',strtotime($val->bookingdetail->booking_date)); ?></div> <!-- all-order-td -->
										<?php if(isset($val->bookingdetail->booking_place) && $val->bookingdetail->booking_place=='home'){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.ADDRESS')!= '')  ?  trans(Session::get('lang_file').'.ADDRESS'): trans($OUR_LANGUAGE.'.ADDRESS')}}: </span><?php echo $getAddresInfo->cus_address1; ?></div> <!-- all-order-td -->
										<?php if(isset($service_charge->home_visit_charge) && $service_charge->home_visit_charge!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.HOME_VISIT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.HOME_VISIT_CHARGE'): trans($OUR_LANGUAGE.'.HOME_VISIT_CHARGE')}}:</span> SAR <?php echo $service_charge->home_visit_charge; ?></div>  <!-- all-order-td -->	 
											
										<?php }} if(isset($val->bookingdetail->start_time) && $val->bookingdetail->start_time!=''){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_TIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_TIME'): trans($OUR_LANGUAGE.'.BOOKING_TIME')}}: </span><?php echo $val->bookingdetail->start_time; ?></div> <!-- all-order-td -->
										<?php } ?>		 
										 </div> <!-- all-order-tr -->	
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
										<?php } ?>
                                        </div>
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php if(isset($productdetails->order_taxAmt) && $productdetails->order_taxAmt!='')
								 {
								  echo 'SAR '.number_format($productdetails->order_taxAmt,2);
								 }
								 else
								 {
								  echo 'N/A';
								 }
								@endphp
								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$productdetails->order_taxAmt-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                    
                                <!-------- Makeup Artist Order End------------------>
								
								 <!-------- Barber Order Start------------------>
								 <?php $i=1;$basetotal = 0; $couponcode=0; $totalinsu =0; if(isset($getOrderBarber) && count($getOrderBarber) > 0){ foreach($getOrderBarber as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 	$couponcode=Helper::getorderedfromcoupanamount($val->order_id); 
								 			
								 			$discountcouponcode=$val->coupon_code;	
								 	?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										      @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers' || $val->product_sub_type=='makeup_artists' || $val->product_sub_type=='men_saloon')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <div class="myac-all-order">
										<?php										
										if(isset($val->bookingdetail) && $val->bookingdetail!='')
										{
										  $service_charge = Helper::getshopname($val->bookingdetail->shop_id);
										  $serviceInfo = Helper::getProduckInfo($val->bookingdetail->service_id);
										  $staffname = Helper::getStaffNameOne($val->bookingdetail->staff_id);
										  $getAddresInfo = Helper::getuserinfo($val->bookingdetail->cus_id);
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	 
									 
										     <?php if(isset($staffname->staff_member_name) && $staffname->staff_member_name!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STAFF_NAME')!= '')  ?  trans(Session::get('lang_file').'.STAFF_NAME'): trans($OUR_LANGUAGE.'.STAFF_NAME')}}:</span> <?php echo $staffname->staff_member_name; ?></div>  <!-- all-order-td -->	
											<?php } if(isset($serviceInfo->service_hour) && $serviceInfo->service_hour!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DURATION')!= '')  ?  trans(Session::get('lang_file').'.DURATION'): trans($OUR_LANGUAGE.'.DURATION')}}:</span> <?php echo $serviceInfo->service_hour; ?></div>  <!-- all-order-td -->	 
											<?php } ?>
											</div> <!-- all-order-tr -->	
								          <div class="all-order-tr">
										  
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_PLACE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_PLACE'): trans($OUR_LANGUAGE.'.BOOKING_PLACE')}}:</span> <?php echo $val->bookingdetail->booking_place; ?></div> <!-- all-order-td -->	
										 									 
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}: </span><?php echo date('d M Y',strtotime($val->bookingdetail->booking_date)); ?></div> <!-- all-order-td -->
										<?php if(isset($val->bookingdetail->booking_place) && $val->bookingdetail->booking_place=='home'){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.ADDRESS')!= '')  ?  trans(Session::get('lang_file').'.ADDRESS'): trans($OUR_LANGUAGE.'.ADDRESS')}}: </span><?php echo $getAddresInfo->cus_address1; ?></div> <!-- all-order-td -->
										<?php if(isset($service_charge->home_visit_charge) && $service_charge->home_visit_charge!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.HOME_VISIT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.HOME_VISIT_CHARGE'): trans($OUR_LANGUAGE.'.HOME_VISIT_CHARGE')}}:</span> SAR <?php echo number_format($service_charge->home_visit_charge,2); ?></div>  <!-- all-order-td -->	 
											
										<?php }} if(isset($val->bookingdetail->start_time) && $val->bookingdetail->start_time!=''){ ?>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_TIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_TIME'): trans($OUR_LANGUAGE.'.BOOKING_TIME')}}: </span><?php echo $val->bookingdetail->start_time; ?></div> <!-- all-order-td -->
										<?php } ?>		 
										 </div> <!-- all-order-tr -->
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>	
										<?php } ?>
                                        </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">	
											{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php
                         $nam=$basetotal-$couponcode-$totalinsu;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?><br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                     
                                <!-------- Barber Order End------------------>
                                
								<!-------- Dental And Dermatology Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu =0; $couponcode=0; $couponcodeinfo=''; if(isset($getSkin) && count($getSkin) > 0){ foreach($getSkin as $val){ $basetotal = ($basetotal+$val->total_price);
								 	$couponcode=$couponcode+$val->coupon_code_amount;
								 	$totalinsu = ($totalinsu+$val->insurance_amount);
                                     $couponcodeinfo=$val->coupon_code;
								 	?>
                                <div class="goldencartitems">
                              
                                 
                                      <table width="100%" class="serviceorder-det-table" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										       @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers' || $val->product_sub_type=='cosmetic' || $val->product_sub_type=='skin')){}elseif(isset($val->product_sub_type) && $val->product_sub_type=='car'){?>
												<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.DAYS')!= '')  ?  trans(Session::get('lang_file').'.DAYS'): trans($OUR_LANGUAGE.'.DAYS')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
												<?php }else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								  <div class="myac-all-order">
										<?php 
										$serviceInfo = Helper::getProduckInfo($val->product_id);
										$department_name = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DR_NAME')!= '')  ?  trans(Session::get('lang_file').'.DR_NAME'): trans($OUR_LANGUAGE.'.DR_NAME')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<?php if(isset($department_name->attribute_title) && $department_name->attribute_title!=''){ ?>  
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DEPARTMENT')!= '')  ?  trans(Session::get('lang_file').'.DEPARTMENT'): trans($OUR_LANGUAGE.'.DEPARTMENT')}}:</span>  <?php echo $department_name->attribute_title; ?></div> <!-- all-order-td --><?php } ?>	   
											<?php if(isset($val->bookingdetail->booking_date) && $val->bookingdetail->booking_date!=''){ ?>          
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}:</span> <?=$val->bookingdetail->booking_date?> </div>  <!-- all-order-td -->	
									<?php } if(isset($val->bookingdetail->start_time) && $val->bookingdetail->start_time!=''){?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_TIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_TIME'): trans($OUR_LANGUAGE.'.BOOKING_TIME')}}:</span> <?=$val->bookingdetail->start_time?> </div>  <!-- all-order-td -->	
											<?php } if(isset($val->bookingdetail->file_no) && $val->bookingdetail->file_no!=''){?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FILE_NUMBER')!= '')  ?  trans(Session::get('lang_file').'.FILE_NUMBER'): trans($OUR_LANGUAGE.'.FILE_NUMBER')}}:</span> <?=$val->bookingdetail->file_no?> </div>  <!-- all-order-td -->	
                                            
									    <?php } ?>    
											
											</div> <!-- all-order-tr -->	
											<div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                        </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php $totalvatprice=$basetotal-$couponcode-$totalinsu;

								 $vatamount = Helper::calculatevat($val->order_id,$totalvatprice);
										 echo 'SAR '.number_format(($vatamount),2);
								@endphp
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}:<br> 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}:
										 {{ $couponcodeinfo or '' }}<br>
								@endif
								 </div>

								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                               
                                <!-------- Dental And Dermatology Order End------------------>
								
								<!-------- Dental And Dermatology Order Start------------------>

								
								 <?php $i=1;$basetotal = 0; $couponcode=0;  $couponcodeinfo=''; $totalinsu=0; if(isset($getCosmetic) && count($getCosmetic) > 0){ foreach($getCosmetic as $val){ $basetotal = ($basetotal+$val->total_price);
                                     $couponcode=$couponcode+$val->coupon_code_amount;
                                     $couponcodeinfo=$val->coupon_code;
                                     $totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">
                              
                                 
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										      @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers' || $val->product_sub_type=='cosmetic' || $val->product_sub_type=='skin')){}elseif(isset($val->product_sub_type) && $val->product_sub_type=='car'){?>
												<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.DAYS')!= '')  ?  trans(Session::get('lang_file').'.DAYS'): trans($OUR_LANGUAGE.'.DAYS')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
												<?php }else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
                             
                            
								  <div class="myac-all-order">
										<?php 
										$serviceInfo = Helper::getProduckInfo($val->product_id); 
										$department_name = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DR_NAME')!= '')  ?  trans(Session::get('lang_file').'.DR_NAME'): trans($OUR_LANGUAGE.'.DR_NAME')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<?php if(isset($department_name->attribute_title) && $department_name->attribute_title!=''){ ?>  
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DEPARTMENT')!= '')  ?  trans(Session::get('lang_file').'.DEPARTMENT'): trans($OUR_LANGUAGE.'.DEPARTMENT')}}:</span>  <?php echo $department_name->attribute_title; ?></div> <!-- all-order-td --><?php } ?>	 
											<?php if(isset($val->bookingdetail->booking_date) && $val->bookingdetail->booking_date!=''){ ?>          
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}:</span> <?=$val->bookingdetail->booking_date?> </div>  <!-- all-order-td -->	
									<?php } if(isset($val->bookingdetail->start_time) && $val->bookingdetail->start_time!=''){?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_TIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_TIME'): trans($OUR_LANGUAGE.'.BOOKING_TIME')}}:</span> <?=$val->bookingdetail->start_time?> </div>  <!-- all-order-td -->	
											<?php } if(isset($val->bookingdetail->file_no) && $val->bookingdetail->file_no!=''){?>		
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FILE_NUMBER')!= '')  ?  trans(Session::get('lang_file').'.FILE_NUMBER'): trans($OUR_LANGUAGE.'.FILE_NUMBER')}}:</span> <?=$val->bookingdetail->file_no?> </div>  <!-- all-order-td -->	

									    <?php } ?>    
											
											</div> <!-- all-order-tr -->	
											<div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                        </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 


									@php $totalvatprice=$basetotal-$couponcode-$totalinsu;

								 $vatamount = Helper::calculatevat($val->order_id,$totalvatprice);
										 echo 'SAR '.number_format(($vatamount),2);
								@endphp

								@if($couponcode >=1)<br>
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 
								{{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}:
										 {{ $couponcodeinfo or '' }}<br>
								@endif
								 </div>

								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                  
                                <!-------- Dental And Dermatology Order End------------------>
								
								<!-------- Jewellery & Gold Order Start------------------>
								 <?php $i=1;$basetotal = 0;$couponcode = 0; $couponAmt=0; $totalinsu =0;if(isset($getJewellery) && count($getJewellery) > 0){ foreach($getJewellery as $val){ $basetotal = ($basetotal+$val->total_price);
								$couponAmt     = $val->coupon_code_amount;
								$couponcodeinfo= $val->coupon_code; 
								$couponcode = $val->coupon_code_amount;
								 $totalinsu = ($totalinsu+$val->insurance_amount);
								 ?>
                                <div class="goldencartitems">                                     
                                  <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									<div class="myac-all-order">
										<?php 
										$serviceInfo = Helper::getProduckInfo($val->product_id);
										if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										$getAttributeInfo = Helper::getAllProductAttribute($val->product_id,$val->merchant_id);
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_NAME')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_NAME'): trans($OUR_LANGUAGE.'.PRODUCT_NAME')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->	
											<?php $i=0; if(count($getAttributeInfo) > 0){foreach($getAttributeInfo as $setVal){?>
											<div class="all-order-td"><span>{{ ucfirst($setVal->attribute_title)}}:</span>  {{ $setVal->value}}<?php if($i%2==0){?> {{ (Lang::has(Session::get('lang_file').'.GM')!= '')  ?  trans(Session::get('lang_file').'.GM'): trans($OUR_LANGUAGE.'.GM')}}<?php } ?></div> <!-- all-order-td -->	
											<?php $i++;}} ?>
											
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WRITE_ON_YOUR_RING')!= '')  ?  trans(Session::get('lang_file').'.WRITE_ON_YOUR_RING'): trans($OUR_LANGUAGE.'.WRITE_ON_YOUR_RING')}}:</span>  <?php if(isset($val->product_size) && $val->product_size!=''){ echo $val->product_size;}else{echo 'N/A';} ?></div> <!-- all-order-td -->	
											</div> <!-- all-order-tr -->	
										  <div class="all-order-tr">
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  </div>
                                        </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">@php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp
								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>         
                                <!--------  Jewellery & Gold Order End------------------>
								
								<!-------- Perfumes Order Start------------------>
								 <?php $i=1;$basetotal = 0; $basetotal = 0;$couponcode = 0; $couponcode=0; $totalinsu =0;

								 if(isset($getPerfume) && count($getPerfume) > 0){ foreach($getPerfume as $val){ $basetotal = ($basetotal+$val->total_price);
								 	$totalinsu = ($totalinsu+$val->insurance_amount);
								 		$couponAmt     = $val->coupon_code_amount;
								         $couponcodeinfo=$val->coupon_code;
								          $couponcode= $val->coupon_code_amount;
								 	?>
                                <div class="goldencartitems">                                     
                                  <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										     @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									<div class="myac-all-order">
										<?php 
										  $serviceInfo = Helper::getProduckInfo($val->product_id); 
										  if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_NAME')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_NAME'): trans($OUR_LANGUAGE.'.PRODUCT_NAME')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->	
											</div> <!-- all-order-tr -->	
										  <div class="all-order-tr">
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  </div>
                                        </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">
								
									@php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp



								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>         
                                <!--------  Perfumes Order End------------------>
								
							
								<!-------- Hotal Halls Orders Start------------------>
								 <?php 
								 $i=1;
								 $basetotal = 0; $totalinsu = 0;								 
								 if(isset($getHotalHalls) && count($getHotalHalls) > 0)
								 { 


								   foreach($getHotalHalls as $val){ $basetotal = ($basetotal+$val->paid_total_amount);
								   		$totalinsu = ($totalinsu+$val->insurance_amount);
								   	?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										      @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @php if(isset($val->quantity) && ($val->quantity!='' && $val->quantity!='0') ){echo $val->quantity;}else{echo '1';}@endphp
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											<!-- SAR {{ number_format($val->total_price,2) }} -->
											SAR {{ number_format($val->paid_total_amount,2) }}
											
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php
										$serviceInfo  = Helper::getProduckInfo($val->product_id);
										$hallinfoData = Helper::hallPaidServiceOrderDetails($val->order_id,$val->product_id);
										?>
										  <div class="all-order-tr">
										    <?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.HALL')!= '')  ?  trans(Session::get('lang_file').'.HALL'): trans($OUR_LANGUAGE.'.HALL')}}: </span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<?php } ?>
											</div> <!-- all-order-tr -->

											<?php
                                           
                                          if(isset($val->insurance_amount) && $val->insurance_amount!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.ADVANCE_PAYMENT')!= '')  ?  trans(Session::get('lang_file').'.ADVANCE_PAYMENT'): 

											trans($OUR_LANGUAGE.'.ADVANCE_PAYMENT')}}: SAR </span><?php

											   $price = $serviceInfo->pro_netprice;
											   $netprice = ($price*25)/100;

											 echo  number_format($netprice,2); ?></div> <!-- all-order-td -->	
											<?php
											} 
                                           
                                           if(isset($val->insurance_amount) && $val->insurance_amount!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.HALL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.HALL_PRICE'): 

											trans($OUR_LANGUAGE.'.HALL_PRICE')}}: SAR </span><?php

											 echo  number_format($serviceInfo->pro_netprice,2); ?></div> <!-- all-order-td -->	
											<?php
											} 

											
                                          ?>
											<?php if(isset($val->insurance_amount) && $val->insurance_amount!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')}}: SAR </span><?php echo number_format($val->insurance_amount,2); ?></div> <!-- all-order-td -->	
											<?php
											} 
                                           
                                           if(isset($val->insurance_amount) && $val->insurance_amount!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.REMAINING_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.REMAINING_AMOUNT'): 

											trans($OUR_LANGUAGE.'.REMAINING_AMOUNT')}}: SAR </span><?php
                                              
                                            $paidtotalamount = $val->paid_total_amount;
                                            $totalamount = $val->total_price;
                                            $remainingamount = $totalamount-$paidtotalamount;
                                           

											 echo  number_format($remainingamount,2); ?> </div> <!-- all-order-td -->	
											<?php
											}  ?>




                                           <?php
											if(count($hallinfoData)> 0)
											{
											    ?><b>{{ (Lang::has(Session::get('lang_file').'.PAID_SERVICE')!= '')  ?  trans(Session::get('lang_file').'.PAID_SERVICE'): trans($OUR_LANGUAGE.'.PAID_SERVICE')}}</b><br /><?php
												foreach($hallinfoData as $hallData)
												{
												  echo $hallData->option_value_title.': SAR '.number_format($hallData->option_value_price,2).' <br>';
												}	
											}										
											if(count($val->bookingdetail)> 0){
											if(isset($serviceInfo->food_type) && $serviceInfo->food_type!=''){ ?>
											<div class="all-order-tr">	<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_TYPE')!= '')  ?  trans(Session::get('lang_file').'.DISHT_TYPE'): trans($OUR_LANGUAGE.'.DISHT_TYPE')}}: </span>
											<?php
                                           
                                            if($serviceInfo->food_type =='external') {?>
                                                 
                                                 {{ (Lang::has(Session::get('lang_file').'.FOOD_FROM_OUTSIDE')!= '')  ?  trans(Session::get('lang_file').'.FOOD_FROM_OUTSIDE'): trans($OUR_LANGUAGE.'.FOOD_FROM_OUTSIDE')}}
                                               <?php }  else {?>

                                              {{ (Lang::has(Session::get('lang_file').'.OUR_MENU')!= '')  ?  trans(Session::get('lang_file').'.OUR_MENU'): trans($OUR_LANGUAGE.'.OUR_MENU')}}

                                               <?php } ?>

                                            
											


											 </div>
											<div class="all-order-tr-heading">{{ (Lang::has(Session::get('lang_file').'.FOOD_ORDERED')!= '')  ?  trans(Session::get('lang_file').'.FOOD_ORDERED'): trans($OUR_LANGUAGE.'.FOOD_ORDERED')}}</div>
											
											 <?php } ?>
									    <?php 
										foreach($val->bookingdetail as $value){?>
										<div class="all-order-wrap">
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_NAME')!= '')  ?  trans(Session::get('lang_file').'.DISHT_NAME'): trans($OUR_LANGUAGE.'.DISHT_NAME')}}:</span> {{$value->internal_dish_name}} </div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CONTAINER')!= '')  ?  trans(Session::get('lang_file').'.CONTAINER'): trans($OUR_LANGUAGE.'.CONTAINER')}}:</span> {{$value->container_title}}</div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_IMAGES')!= '')  ?  trans(Session::get('lang_file').'.DISHT_IMAGES'): trans($OUR_LANGUAGE.'.DISHT_IMAGES')}}:</span> <img width="75" height="75" src="<?=$value->dish_image?>" /></div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CONTAINER_IMAGES')!= '')  ?  trans(Session::get('lang_file').'.CONTAINER_IMAGES'): trans($OUR_LANGUAGE.'.CONTAINER_IMAGES')}}:</span> <img width="75" height="75" src="<?=$value->container_image?>" /></div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.QUANTITY')!= '')  ?  trans(Session::get('lang_file').'.QUANTITY'): trans($OUR_LANGUAGE.'.QUANTITY')}}:</span> {{$value->quantity}}</div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}:</span> {{number_format($value->price,2)}}</div>
										</div>
										<?php } } ?>
                                        </div>
										</div>
										
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<!-- <div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php if(isset($productdetails->order_taxAmt) && $productdetails->order_taxAmt!='')
								 {
								  echo 'SAR '.number_format($productdetails->order_taxAmt,2);
								 }
								 else
								 {
								  echo 'N/A';
								 }
								@endphp
								 </div> -->
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
								@endif
								<small>
									{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}:
									@php $totalvatprice=$basetotal-$couponcode-$totalinsu;

								 $vatamount = Helper::calculatevat($val->order_id,$totalvatprice);
										 echo 'SAR '.number_format(($vatamount),2);
								@endphp
								</small>
									<br>
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal-$couponcode+$vatamount),2);
								 } ?>            
								</div>
								<?php } ?>                                     
                                <!-------- Hotal Halls Orders End------------------>
                    
                                <!-------- Buffets Orders Start------------------>
								 <?php 
								 $i=1;
								 $basetotal = 0;	$totalinsu = 0;						 
								 if(isset($getBuffets) && count($getBuffets) > 0)
								 { 
								 	//echo "<pre>";
								 	//print_r($getBuffets);
								 	$jhjj=0;
								   foreach($getBuffets as $val){ $basetotal = ($basetotal+$val->total_price);
								   	$totalinsu = ($totalinsu+$val->insurance_amount);
                                         $jhjj++;
								   	?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										      @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @php if(isset($val->quantity) && ($val->quantity!='' && $val->quantity!='0') ){echo $val->quantity;}else{echo '1';}@endphp
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php
										$serviceInfo = Helper::getProduckInfo($val->product_id);
										?>
										  <div class="all-order-tr">
										    <?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SERVICE_NAME')!= '')  ?  trans(Session::get('lang_file').'.SERVICE_NAME'): trans($OUR_LANGUAGE.'.SERVICE_NAME')}}: </span>  <?php echo $serviceInfo->pro_title; ?>    </div> <!-- all-order-td -->	
											<?php } ?>
											</div> <!-- all-order-tr -->
											<div class="all-order-tr">	
											<b></b>
											<?php if(isset($serviceInfo->food_type) && $serviceInfo->food_type!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_TYPE')!= '')  ?  trans(Session::get('lang_file').'.DISHT_TYPE'): trans($OUR_LANGUAGE.'.DISHT_TYPE')}}: </span>{{$serviceInfo->food_type}} </div><?php } ?><br /><br />
									    
                                        </div>
										 <?php 
										 //echo '<pre>';print_r($val->bookingdetail);die;
										if(count($val->bookingdetail) >0){
										foreach($val->bookingdetail as $value){
										$setname = Helper::setExtraProductFieldType($value->container_id);
										$dishname = Helper::getProduckInfo($value->external_food_dish_id);
										
										?>
										<div class="all-order-wrap">
										<!--<div class="all-order-td"><span>Menu:</span></div>-->										
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_NAME')!= '')  ?  trans(Session::get('lang_file').'.DISHT_NAME'): trans($OUR_LANGUAGE.'.DISHT_NAME')}}:</span> {{$dishname->pro_title}}</div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CONTAINER')!= '')  ?  trans(Session::get('lang_file').'.CONTAINER'): trans($OUR_LANGUAGE.'.CONTAINER')}}:</span> {{$setname->option_title}}</div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_IMAGES')!= '')  ?  trans(Session::get('lang_file').'.DISHT_IMAGES'): trans($OUR_LANGUAGE.'.DISHT_IMAGES')}}:</span> <img width="75" height="75" src="<?php echo $dishname->pro_Img; ?>" /></div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CONTAINER_IMAGES')!= '')  ?  trans(Session::get('lang_file').'.CONTAINER_IMAGES'): trans($OUR_LANGUAGE.'.CONTAINER_IMAGES')}}:</span> <img width="75" height="75" src="<?php echo $setname->image; ?>" /></div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.QUANTITY')!= '')  ?  trans(Session::get('lang_file').'.QUANTITY'): trans($OUR_LANGUAGE.'.QUANTITY')}}:</span> {{$value->quantity}}</div>
										<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}:</span> SAR {{number_format($value->price,2)}}</div>
										</div>
										<?php }} ?>
										
										
										</div>
										
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">

									@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
								@endif
								
									{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}:
									@php $totalvatprice=$basetotal-$couponcode-$totalinsu;

								 $vatamount = Helper::calculatevat($val->order_id,$totalvatprice);
										 echo 'SAR '.number_format(($vatamount),2);
								@endphp
								
									<br>
								




								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal-$couponcode+$vatamount),2);
								 } ?>              
								</div>
								<?php } ?>                                     
                                <!-------- Buffets Orders End------------------>
								
								<!-------- Dates Orders Start------------------>
								 <?php 
								 $i=1;
								 $basetotal = 0; $couponcode=0; $totalinsu = 0; 								 
								 if(isset($getDates) && count($getDates) > 0)
								 { 
								   foreach($getDates as $val){ $basetotal = ($basetotal+$val->total_price);
								   			$totalinsu = ($totalinsu+$val->insurance_amount);
								   		$couponcode=Helper::getorderedfromcoupanamount($val->order_id); 
								 			
								 			$discountcouponcode=$val->coupon_code;	
								   	?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										       @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif

												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ 

													if($val->product_sub_type=='dessert' || $val->product_sub_type=='dates'){
														$dishqty = Helper::productdishtype($val->product_id);
															$dishunitqty = Helper::productdishdatestype($val->product_id);
														
													}

															

													?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											
											 @php if(isset($val->quantity) && ($val->quantity!='' && $val->quantity!='0') ){echo $val->quantity.' '. $dishunitqty;}else{echo '1'.$dishunitqty;}@endphp
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php
										$serviceInfo = Helper::getProduckInfo($val->product_id);
										if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										?>
										  <div class="all-order-tr">
										    <?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_NAME')!= '')  ?  trans(Session::get('lang_file').'.DISHT_NAME'): trans($OUR_LANGUAGE.'.DISHT_NAME')}}: </span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}:</span>  SAR {{ number_format($val->total_price,2) }}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
											<?php } ?>
											</div> <!-- all-order-tr -->
											
										</div>
										
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">{{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($OUR_LANGUAGE.'.VAT_CHARGE')}}: 
								@php
                         $nam=$basetotal-$couponcode-$totalinsu;
                      $vatamonu = Helper::calculatevat($productdetails->order_id,$nam);
                        
                      @endphp

                      <?php echo 'SAR '.number_format($vatamonu,2); ?><br>
                      @if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.COUPON_CODE')!= '')  ?  trans(Session::get('lang_file').'.COUPON_CODE'): trans($OUR_LANGUAGE.'.COUPON_CODE')}}: {{ $discountcouponcode }}
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamonu-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                     
                                <!-------- Dates Orders End------------------>
								
								<!-------- Desserts Orders Start------------------>
								 <?php 
								 $i=1;
								 $basetotal = 0;	$totalinsu = 0;							 
								 if(isset($getDesserts) && count($getDesserts) > 0)
								 { 
								   foreach($getDesserts as $val){ $basetotal = ($basetotal+$val->total_price);
								   		$totalinsu = ($totalinsu+$val->insurance_amount);
								   	?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										       @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ 

														if($val->product_sub_type=='dessert'){
															
														$dishqty = Helper::productdishtype($val->product_id);
														$dishunitqty = Helper::productdishdatestype($val->product_id);
													}

														
													?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @php if(isset($val->quantity) && ($val->quantity!='' && $val->quantity!='0') ){echo $val->quantity.' '. $dishunitqty;}else{echo '1'.$dishunitqty;}@endphp
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php
										$serviceInfo = Helper::getProduckInfo($val->product_id);
										//if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}

										if(isset($val->total_price) && $val->total_price!='0'){$getPrice = $val->total_price;}

										?>
										  <div class="all-order-tr">
										    <?php if(isset($serviceInfo->pro_title) && $serviceInfo->pro_title!=''){ ?>
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DISHT_NAME')!= '')  ?  trans(Session::get('lang_file').'.DISHT_NAME'): trans($OUR_LANGUAGE.'.DISHT_NAME')}}: </span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}:</span>  SAR <?php echo number_format($getPrice,2); ?></div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
											<?php } ?>
											</div> <!-- all-order-tr -->
											
										</div>
										
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">
								  @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp 

								 </div>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?>
								@endif
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                     
                                <!-------- Desserts Orders End------------------>
								
								<!-------- Tailors Order Start------------------>
								 <?php $i=1;$basetotal = 0; $totalinsu =0;  $couponcode=0; $couponcodeinfo='';if(isset($getOrderTailor) && count($getOrderTailor) > 0){ foreach($getOrderTailor as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 	 $couponcode=$couponcode+$val->coupon_code_amount;;
        	$couponcodeinfo=$val->coupon_code;
								 	?>
                                <div class="goldencartitems">
                              
                                 
                                      <table width="100%" class="serviceorder-det-table" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										      @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}elseif(isset($val->product_sub_type) && $val->product_sub_type=='car'){?>
												<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.DAYS')!= '')  ?  trans(Session::get('lang_file').'.DAYS'): trans($OUR_LANGUAGE.'.DAYS')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
												<?php }else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format(($val->total_price),2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								      <div class="myac-all-order">
										<?php 
										  $serviceInfo = Helper::getProduckInfo($val->product_id);
										  if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										  //$fabric_name = Helper::getFabricInfo($val->fabric_id,$val->merchant_id); 
										?>
										  <div class="all-order-tr">
										   <b>{{ (Lang::has(Session::get('lang_file').'.STYLE_AND_FABRIC_INFO')!= '')  ?  trans(Session::get('lang_file').'.STYLE_AND_FABRIC_INFO'): trans($OUR_LANGUAGE.'.STYLE_AND_FABRIC_INFO')}}</b><br /><br />
										  </div>
										  <div class="all-order-tr">
										    
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STYLE')!= '')  ?  trans(Session::get('lang_file').'.STYLE'): trans($OUR_LANGUAGE.'.STYLE')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->												
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STYLE_PRICE')!= '')  ?  trans(Session::get('lang_file').'.STYLE_PRICE'): trans($OUR_LANGUAGE.'.STYLE_PRICE')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->
											<?php 
											$date = $val->created_at; //existing date
											if(isset($val->deliver_day) && $val->deliver_day!=''){
											if($val->deliver_day==1){$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' day'));}else{$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' days'));}
											?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DELIVERY_DATE')!= '')  ?  trans(Session::get('lang_file').'.DELIVERY_DATE'): trans($OUR_LANGUAGE.'.DELIVERY_DATE')}}:</span>  <?php echo $newDate; ?></div> <!-- all-order-td -->
											<?php } if(isset($val->fabric_name) && $val->fabric_name!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FABRICS_NAME')!= '')  ?  trans(Session::get('lang_file').'.FABRICS_NAME'): trans($OUR_LANGUAGE.'.FABRICS_NAME')}}:</span>  <?php echo $val->fabric_name; ?></div> <!-- all-order-td -->
											<div class="all-order-td" style="width:100%;"><span>{{ (Lang::has(Session::get('lang_file').'.FABRICS_PRICE')!= '')  ?  trans(Session::get('lang_file').'.FABRICS_PRICE'): trans($OUR_LANGUAGE.'.FABRICS_PRICE')}}:</span> SAR <?php echo $val->fabric_price; ?></div> <!-- all-order-td -->												
											<?php } ?>
											<div style="margin-top: 10px;"><br /><br /><b>{{ (Lang::has(Session::get('lang_file').'.BODY_MEASUREMENT')!= '')  ?  trans(Session::get('lang_file').'.BODY_MEASUREMENT'): trans($OUR_LANGUAGE.'.BODY_MEASUREMENT')}}</b><br /><br />	 </div>
											
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')}}:</span>  <?php echo $val->bookingdetail->length; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')}}:</span>  <?php echo $val->bookingdetail->chest_size; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}} </div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')}}:</span>  <?php echo $val->bookingdetail->waistsize; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')}}:</span>  <?php echo $val->bookingdetail->soulders; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')}}:</span>  <?php echo $val->bookingdetail->neck; ?>  {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')}}:</span>  <?php echo $val->bookingdetail->arm_length; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')}}:</span>  <?php echo $val->bookingdetail->wrist_diameter; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<?php /* ?><div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CUTTING')!= '')  ?  trans(Session::get('lang_file').'.CUTTING'): trans($OUR_LANGUAGE.'.CUTTING')}}:</span>  <?php echo $val->bookingdetail->cutting; ?></div> <!-- all-order-td --><?php */ ?>
										  </div> <!-- all-order-tr -->	
										  <div class="all-order-tr">
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STYLE_IMAGE')!= '')  ?  trans(Session::get('lang_file').'.STYLE_IMAGE'): trans($OUR_LANGUAGE.'.STYLE_IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php  if(isset($val->fabric_img) && $val->fabric_img!=''){?> 
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FABRICS_IMAGE')!= '')  ?  trans(Session::get('lang_file').'.FABRICS_IMAGE'): trans($OUR_LANGUAGE.'.FABRICS_IMAGE')}}:</span>  <img src="<?php echo $val->fabric_img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  </div>
										  <?php } ?>
                                       </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line"> 



									     @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp 

	@if($couponcode >=1)
	<br>
								
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										{{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif


							
								 </div>
							
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                
                                <!-------- Tailors Order End------------------>
								
								<!-------- Dresses Order Start------------------>
								 <?php 
								      $i=1;$basetotal = 0;$couponcode = 0; $totalinsu = 0;
									  if(isset($getOrderDress) && count($getOrderDress) > 0){ foreach($getOrderDress as $val)
								      { 
									     $basetotal     = ($basetotal+($val->total_price));  
										 $couponcode    = $couponcode + $val->coupon_code_amount; 
									     $coupon_code   = $val->coupon_code;
								         $couponAmt     = $val->coupon_code_amount;
								         $couponcodeinfo=$val->coupon_code;
								         $totalinsu = ($totalinsu+$val->insurance_amount);
								  ?>
                                <div class="goldencartitems">
                              
                                 
                                      <table class="serviceorder-det-table" width="100%" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}elseif(isset($val->product_sub_type) && $val->product_sub_type=='car'){?>
												<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.DAYS')!= '')  ?  trans(Session::get('lang_file').'.DAYS'): trans($OUR_LANGUAGE.'.DAYS')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
												<?php }else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format(($val->total_price),2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php 
										//echo '<pre>';print_r($val->bookingdetail->rental_time);
										  $serviceInfo = Helper::getProduckInfo($val->product_id);
										  if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										  	$rentpriceinfo=0;
										  if(isset($val->buy_rent) && $val->buy_rent=='rent'){

										  	if(isset($val->bookingdetail->return_time) && isset($val->bookingdetail->rental_time)){
        									$time1=str_replace(',','',$val->bookingdetail->return_time);
       										 $time2=str_replace(',','',$val->bookingdetail->rental_time);
										  	//$time1=$val->bookingdetail->return_time;
									        //$time2=$val->bookingdetail->rental_time;
									        $hourdiff = round((strtotime($time1) - strtotime($time2))/3600, 1);
									          $bprice=$val->total_price-$val->bookingdetail->insurance_amount;
									        @$rentpriceinfo=$bprice/$hourdiff;
									    }else{ $rentpriceinfo=0; }

										  }
										  	if($rentpriceinfo<1){
										  	 $rentpriceinfo = Helper::getrentinformation($val->product_id);
										  	}
										?>
										  <div class="all-order-tr">
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DRESS')!= '')  ?  trans(Session::get('lang_file').'.DRESS'): trans($OUR_LANGUAGE.'.DRESS')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->
										    <?php if(isset($val->buy_rent) && $val->buy_rent!='rent'){ ?>              
										      <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> 
										  <?php } ?>
										      <!-- all-order-td -->	
											<?php if(isset($val->buy_rent) && $val->buy_rent!=''){?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FOR')!= '')  ?  trans(Session::get('lang_file').'.FOR'): trans($OUR_LANGUAGE.'.FOR')}}:</span>  <?php echo ucfirst($val->buy_rent); ?></div> <!-- all-order-td -->
											<?php } if(isset($val->product_size) && $val->product_size!=''){ $size = str_replace('Size','',$val->product_size);?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SIZES')!= '')  ?  trans(Session::get('lang_file').'.SIZES'): trans($OUR_LANGUAGE.'.SIZES')}}:</span>  <?php echo $size; ?></div> <!-- all-order-td -->
											<?php } if(isset($serviceInfo->pro_disprice) && ($serviceInfo->pro_disprice!='0' || $serviceInfo->pro_disprice!='0.00')){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;} if(isset($val->buy_rent) && $val->buy_rent=='rent'){?>
											
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.RENT_CHARGES_HR')!= '')  ?  trans(Session::get('lang_file').'.RENT_CHARGES_HR'): trans($OUR_LANGUAGE.'.RENT_CHARGES_HR')}}:</span> <?php echo number_format($rentpriceinfo,2); ?></div> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.BOOKING_DATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKING_DATE'): trans($OUR_LANGUAGE.'.BOOKING_DATE')}}: </span> {{$val->bookingdetail->rental_time or ''}}</div> 	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.RETURNING_DATE')!= '')  ?  trans(Session::get('lang_file').'.RETURNING_DATE'): trans($OUR_LANGUAGE.'.RETURNING_DATE')}}:</span> {{$val->bookingdetail->return_time or ''}}</div> 

<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')}}:</span>  


  @php
				   echo 'SAR '.number_format($val->bookingdetail->insurance_amount,2);
				  @endphp

</div>
 <?php } ?>
											</div> <!-- all-order-tr -->	
											@if($coupon_code!='')
										  <div class="all-order-tr">										  	
										  	<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}:</span> {{$coupon_code or ''}}</div>
										  	<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.couponAmt')!= '')  ?  trans(Session::get('lang_file').'.couponAmt'): trans($OUR_LANGUAGE.'.couponAmt')}}:</span> SAR {{ number_format($couponAmt,2) }}</div> 
										  	</div>
										  	@endif
									<div class="all-order-tr">
									<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
									</div>
									</div>
								      	
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">
								

									@php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp



								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                 
                                <!-------- Dresses Order End------------------>
								
								
								<!-------- Abaya Order Start------------------>
								 <?php 
								 $i=1;$basetotal = 0; $basetotal = 0;$couponcode = 0; $couponAmt=0; $totalinsu = 0; if(isset($getOrderAbaya) && count($getOrderAbaya) > 0){ 
								 		

								 	foreach($getOrderAbaya as $val){ $basetotal = ($basetotal+$val->total_price);
								 				$totalinsu = ($totalinsu+$val->insurance_amount);
								 			 $couponAmt     = $val->coupon_code_amount;
								           $couponcodeinfo= $val->coupon_code; 
								           $couponcode = $val->coupon_code_amount;              
								 	?>
                                <div class="goldencartitems">
                              
                                 
                                      <table width="100%" class="serviceorder-det-table" style="background:none;">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
											@if(Session::get('lang_file')!='en_lang')
											{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
											@else
											{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
											@endif   
											@endif
												</td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}elseif(isset($val->product_sub_type) && $val->product_sub_type=='car'){?>
												<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.DAYS')!= '')  ?  trans(Session::get('lang_file').'.DAYS'): trans($OUR_LANGUAGE.'.DAYS')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php }else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                             {{$val->quantity}}
                                             @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format(($val->total_price),2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
								      <div class="myac-all-order">
										<?php 
										  $serviceInfo = Helper::getProduckInfo($val->product_id);
										  if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										  $fabric_name = Helper::getFabricName($val->product_id,$val->merchant_id); 
										?>
										  <div class="all-order-tr">
										   <?php if(count($val->bookingdetail) > 0){ ?>
										    <div class="all-order-tr">
										   <b>{{ (Lang::has(Session::get('lang_file').'.STYLE_AND_FABRIC_INFO')!= '')  ?  trans(Session::get('lang_file').'.STYLE_AND_FABRIC_INFO'): trans($OUR_LANGUAGE.'.STYLE_AND_FABRIC_INFO')}}</b><br /><br />
										  </div>
										  <div class="all-order-tr">
										    
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STYLE')!= '')  ?  trans(Session::get('lang_file').'.STYLE'): trans($OUR_LANGUAGE.'.STYLE')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->												
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STYLE_PRICE')!= '')  ?  trans(Session::get('lang_file').'.STYLE_PRICE'): trans($OUR_LANGUAGE.'.STYLE_PRICE')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->
											<?php 
											$date = $val->created_at; //existing date
											if(isset($val->deliver_day) && $val->deliver_day!=''){
											if($serviceInfo->deliver_day==1){$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' day'));}else{$newDate = date('d M Y', strtotime($date .'+'.$val->deliver_day.' days'));}
											?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DELIVERY_DATE')!= '')  ?  trans(Session::get('lang_file').'.DELIVERY_DATE'): trans($OUR_LANGUAGE.'.DELIVERY_DATE')}}:</span>  <?php echo $newDate; ?></div> <!-- all-order-td --><br />
											<?php }  if(isset($val->fabric_name) && $val->fabric_name!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FABRICS_NAME')!= '')  ?  trans(Session::get('lang_file').'.FABRICS_NAME'): trans($OUR_LANGUAGE.'.FABRICS_NAME')}}:</span>  <?php echo $val->fabric_name; ?></div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FABRICS_PRICE')!= '')  ?  trans(Session::get('lang_file').'.FABRICS_PRICE'): trans($OUR_LANGUAGE.'.FABRICS_PRICE')}}:</span> SAR <?php echo $val->fabric_price; ?></div> <!-- all-order-td -->												
											<?php } }else{?>
											<b>{{ (Lang::has(Session::get('lang_file').'.DESIGN')!= '')  ?  trans(Session::get('lang_file').'.DESIGN'): trans($OUR_LANGUAGE.'.DESIGN')}}</b><br /><br />
											
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.DESIGN')!= '')  ?  trans(Session::get('lang_file').'.DESIGN'): trans($OUR_LANGUAGE.'.DESIGN')}}:</span>  <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<?php if(isset($fabric_name->option_title) && $fabric_name->option_title!=''){?> 
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SIZES')!= '')  ?  trans(Session::get('lang_file').'.SIZES'): trans($OUR_LANGUAGE.'.SIZES')}}:</span>  <?php echo $fabric_name->option_title; ?></div> <!-- all-order-td -->
											<?php } } ?>
											
											<?php if(count($val->bookingdetail) > 0){ ?>
											<br /><br /><b>{{ (Lang::has(Session::get('lang_file').'.BODY_MEASUREMENT')!= '')  ?  trans(Session::get('lang_file').'.BODY_MEASUREMENT'): trans($OUR_LANGUAGE.'.BODY_MEASUREMENT')}}</b><br /><br />	 
											
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')}}:</span>  <?php echo $val->bookingdetail->length; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')}}:</span>  <?php echo $val->bookingdetail->chest_size; ?>{{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}} </div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')}}:</span>  <?php echo $val->bookingdetail->waistsize; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')}}:</span>  <?php echo $val->bookingdetail->soulders; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')}}:</span>  <?php echo $val->bookingdetail->neck; ?></div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')}}:</span>  <?php echo $val->bookingdetail->arm_length; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')}}:</span>  <?php echo $val->bookingdetail->wrist_diameter; ?> {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</div> <!-- all-order-td -->
											<?php /* ?><div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.CUTTING')!= '')  ?  trans(Session::get('lang_file').'.CUTTING'): trans($OUR_LANGUAGE.'.CUTTING')}}:</span>  <?php echo $val->bookingdetail->cutting; ?></div> <!-- all-order-td --><?php */ ?><?php } ?>
										  </div> <!-- all-order-tr -->	
										  
										  <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                       </div>
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">
								@php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp
								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								 echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                
                                <!-------- Abaya Order End------------------>
								
								<!-------- Photography Studio Order Start------------------>
								 <?php $i=1;$basetotal = 0; $couponcode = 0; $couponAmt=0; $totalinsu = 0; if(isset($photographyStudio) && count($photographyStudio) > 0){ foreach($photographyStudio as $val){ $basetotal = ($basetotal+$val->total_price);
								 		$couponAmt     = $val->coupon_code_amount;
								           $couponcodeinfo= $val->coupon_code; 
								           $couponcode = $val->coupon_code_amount; 
								           $totalinsu = ($totalinsu+$val->insurance_amount);
								 	?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
                                                

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else
												{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
												@endif
                                                @endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php	
										//echo '<pre>';print_r($val);die;									
										$serviceInfo     = Helper::getProduckInfo($val->product_id);
										$department_name = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
										$extraInfo       = Helper::getExtraInfo($val->product_id,$val->order_id,$val->cus_id);
										$productinfo     = Helper::getAllProductAttribute($val->product_id,$val->merchant_id);
										//echo '<pre>';print_r($productinfo[0]->value);die;		
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<?php if(isset($productinfo[0]->value) && $productinfo[0]->value!=''){  ?>
											<div class="all-order-td"><span>
											<?php if(isset($val->product_sub_type) && $val->product_sub_type=='video'){ ?>
											{{ (Lang::has(Session::get('lang_file').'.Duration_of_Video')!= '')  ?  trans(Session::get('lang_file').'.Duration_of_Video'): trans($OUR_LANGUAGE.'.Duration_of_Video')}}
											<?php } else{ ?>
											{{ (Lang::has(Session::get('lang_file').'.Number_of_Pictures')!= '')  ?  trans(Session::get('lang_file').'.Number_of_Pictures'): trans($OUR_LANGUAGE.'.Number_of_Pictures')}}
											<?php } ?>
											:</span> <?php echo $productinfo[0]->value; ?></div> <!-- all-order-td -->	
											<?php }if(isset($productinfo[1]->value) && $productinfo[1]->value!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Number_of_Cameras')!= '')  ?  trans(Session::get('lang_file').'.Number_of_Cameras'): trans($OUR_LANGUAGE.'.Number_of_Cameras')}}:</span> <?php echo $productinfo[1]->value; ?></div> <!-- all-order-td -->	
											<?php } if(isset($department_name->attribute_title) && $department_name->attribute_title!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FOR')!= '')  ?  trans(Session::get('lang_file').'.FOR'): trans($OUR_LANGUAGE.'.FOR')}}:</span> <?php echo $department_name->attribute_title; ?></div> <!-- all-order-td -->				<?php }if(isset($extraInfo->date) && $extraInfo->date!=''){ ?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Date')!= '')  ?  trans(Session::get('lang_file').'.Date'): trans($OUR_LANGUAGE.'.Date')}}:</span> <?php echo date('d M Y',strtotime($extraInfo->date)); ?></div> <!-- all-order-td -->				            <?php }if(isset($extraInfo->time) && $extraInfo->time!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Time')!= '')  ?  trans(Session::get('lang_file').'.Time'): trans($OUR_LANGUAGE.'.Time')}}:</span> <?php echo $extraInfo->time; ?></div> <!-- all-order-td -->				            <?php }  if(isset($extraInfo->elocation) && $extraInfo->elocation!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')}}:</span> <?php echo $extraInfo->elocation; ?></div> <!-- all-order-td -->	<?php }  ?>
											 
											</div> <!-- all-order-tr -->	
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                        </div>
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">
@php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu = 0;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp
								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								  echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                 
                                <!-------- Photography Studio Order End------------------>
								
								<!-------- Reception & Hospitality Order Start------------------>
								 <?php $i=1;$basetotal = 0;$couponcode = 0; $totalinsu=0; $couponAmt=0; $ik=1; $jk=1;
								 if(isset($ReceptionHospitality) && count($ReceptionHospitality) > 0){ 
								 	foreach($ReceptionHospitality as $val){ 
								 		$basetotal = ($basetotal+$val->total_price);
								 		$totalinsu = ($totalinsu+$val->insurance_amount);
								 $couponAmt     = $val->coupon_code_amount;
								           $couponcodeinfo= $val->coupon_code; 
								           $couponcode = $val->coupon_code_amount; 
								 ?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
                                                

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y M d')}}
												@else
												{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
												@endif
                                                @endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers' || $val->product_sub_type=='hospitality' )){ ?>
												<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											N/A
											</td>
											<?php }else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php	
										//echo '<pre>';print_r($val);die;									
										$serviceInfo     = Helper::getProduckInfo($val->product_id);
										$department_name = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
										$extraInfo       = Helper::getExtraHospitalityInformationOfProduct($val->shop_id,$val->order_id,$val->cus_id,$val->buy_rent);
										$productinfo     = Helper::getAllProductAttribute($val->product_id,$val->merchant_id);
										if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										//echo $val->product_id.'-'.$val->merchant_id;
										//echo '<pre>';print_r($extraInfo);
										

										$getnewproducthospitality=Helper::getmyaccountproductorderattribute($val->order_id,$val->buy_rent);
										

										 if(!$getnewproducthospitality->isEmpty()){
										 	foreach ($getnewproducthospitality as $receptionhospitalityproduct) {						 		
										 	
										?>


										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_NAME')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_NAME'): trans($OUR_LANGUAGE.'.PRODUCT_NAME')}}:</span> <?php echo $receptionhospitalityproduct->pro_title; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo number_format($receptionhospitalityproduct->price,2); ?></div> <!-- all-order-td -->	
											<?php if(isset($receptionhospitalityproduct->attribute_title) && $receptionhospitalityproduct->attribute_title=='Package'){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.TYPE')!= '')  ?  trans(Session::get('lang_file').'.TYPE'): trans($OUR_LANGUAGE.'.TYPE')}}:</span> 
											{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}}
											</div> <!-- all-order-td -->				
											<?php }else{?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.TYPE')!= '')  ?  trans(Session::get('lang_file').'.TYPE'): trans($OUR_LANGUAGE.'.TYPE')}}:</span> 
											{{ (Lang::has(Session::get('lang_file').'.Design_your_Package')!= '')  ?  trans(Session::get('lang_file').'.Design_your_Package'): trans($OUR_LANGUAGE.'.Design_your_Package')}}
											</div> <!-- all-order-td -->
											<?php }?>

											<?php if(isset($receptionhospitalityproduct->quantity) && $receptionhospitalityproduct->quantity>0){ ?>

												<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.QUANTITY')!= '')  ?  trans(Session::get('lang_file').'.QUANTITY'): trans($OUR_LANGUAGE.'.QUANTITY')}}:</span> <?php echo $receptionhospitalityproduct->quantity; ?></div>
											<?php }?>

											<?php if($ik==1 && $val->buy_rent=='design'){?>
											<?php if(isset($extraInfo->no_of_staff) && $extraInfo->no_of_staff!=''){ ?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Number_of_Staff')!= '')  ?  trans(Session::get('lang_file').'.Number_of_Staff'): trans($OUR_LANGUAGE.'.Number_of_Staff')}}:</span> <?php echo $extraInfo->no_of_staff; ?></div> 
											<!-- all-order-td -->
											<?php }?>

											<?php if(isset($receptionhospitalityproduct->worker_price) && $receptionhospitalityproduct->worker_price!=''){ ?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.STAFF_CHARGES')!= '')  ?  trans(Session::get('lang_file').'.STAFF_CHARGES'): trans($OUR_LANGUAGE.'.STAFF_CHARGES')}}:</span> <?php echo number_format($receptionhospitalityproduct->worker_price,2); ?></div> 
											<!-- all-order-td -->
											<?php }?>

											<?php if(isset($extraInfo->nationality) && $extraInfo->nationality!=''){$country_name = Helper::getNationID($extraInfo->nationality); ?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Nationality')!= '')  ?  trans(Session::get('lang_file').'.Nationality'): trans($OUR_LANGUAGE.'.Nationality')}}:</span> <?php echo $country_name; ?></div> <!-- all-order-td -->	
											<?php }?>





											<?php if(isset($extraInfo->date) && $extraInfo->date!=''){ ?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Date')!= '')  ?  trans(Session::get('lang_file').'.Date'): trans($OUR_LANGUAGE.'.Date')}}:</span> <?php echo date('d M Y',strtotime($extraInfo->date)); ?></div> <!-- all-order-td -->				            
											<?php }if(isset($extraInfo->time) && $extraInfo->time!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Time')!= '')  ?  trans(Session::get('lang_file').'.Time'): trans($OUR_LANGUAGE.'.Time')}}:</span> <?php echo $extraInfo->time; ?></div> <!-- all-order-td -->				            <?php }  if(isset($extraInfo->elocation) && $extraInfo->elocation!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')}}:</span> <?php echo $extraInfo->elocation; ?></div> <!-- all-order-td -->	<?php }  ?>
											 <?php $ik++; }?>




											 	<?php if($jk==1 && $val->buy_rent=='package'){?>									<?php if(isset($extraInfo->date) && $extraInfo->date!=''){ ?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Date')!= '')  ?  trans(Session::get('lang_file').'.Date'): trans($OUR_LANGUAGE.'.Date')}}:</span> <?php echo date('d M Y',strtotime($extraInfo->date)); ?></div> <!-- all-order-td -->				            
											<?php }if(isset($extraInfo->time) && $extraInfo->time!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Time')!= '')  ?  trans(Session::get('lang_file').'.Time'): trans($OUR_LANGUAGE.'.Time')}}:</span> <?php echo $extraInfo->time; ?></div> <!-- all-order-td -->				            <?php }  if(isset($extraInfo->elocation) && $extraInfo->elocation!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.LOCATION')!= '')  ?  trans(Session::get('lang_file').'.LOCATION'): trans($OUR_LANGUAGE.'.LOCATION')}}:</span> <?php echo $extraInfo->elocation; ?></div> <!-- all-order-td -->	<?php }  ?>
											 <?php $jk++; }?>







											</div> <!-- all-order-tr -->


										 <div class="all-order-tr">
										  <?php if(isset($receptionhospitalityproduct->pro_Img) && $receptionhospitalityproduct->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $receptionhospitalityproduct->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
										  	<hr>

 											<?php } }?>



                                        </div>
                                 
                                </div>
								<?php $i++; } ?> 
								<div class="main_total">
								<div class="vat-tax-line">
@php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp
								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								  echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                 
                                <!-------- Reception & Hospitality Order End------------------>
								
								<!-------- Roses Order Start------------------>
								 <?php $i=1;$basetotal = 0; $couponcode = 0; $couponAmt=0; $totalinsu=0; if(isset($orderRoses) && count($orderRoses) > 0){ foreach($orderRoses as $val){ $basetotal = ($basetotal+$val->total_price);
								 $couponAmt      = $val->coupon_code_amount;
								 $couponcodeinfo = $val->coupon_code; 
								 $couponcode = $val->coupon_code_amount;
								 $totalinsu = ($totalinsu+$val->insurance_amount);
								 ?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
                                                

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else
												{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
												@endif
                                                @endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php	
										//echo '<pre>';print_r($val);die;									
										$serviceInfo         = Helper::getProduckInfo($val->product_id);
										$department_name     = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
										$extraInfo           = Helper::getExtraInformationOfProduct($val->shop_id,$val->order_id,$val->cus_id);
										$getPackage          = Helper::getPackage($val->shop_id,$val->product_id);									
										if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										
										//echo $val->product_id.'-'.$val->merchant_id;
										//echo '<pre>';print_r($wrappingType);
										//echo '<pre>';print_r($getPtoductOptionVal);
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_NAME')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_NAME'): trans($OUR_LANGUAGE.'.PRODUCT_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->	
											<?php if(isset($serviceInfo->packege) && $serviceInfo->packege=='yes'){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FOR')!= '')  ?  trans(Session::get('lang_file').'.FOR'): trans($OUR_LANGUAGE.'.FOR')}}:</span> 
											{{ (Lang::has(Session::get('lang_file').'.Package')!= '')  ?  trans(Session::get('lang_file').'.Package'): trans($OUR_LANGUAGE.'.Package')}}
											</div> <!-- all-order-td -->				
											<?php }else{?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FOR')!= '')  ?  trans(Session::get('lang_file').'.FOR'): trans($OUR_LANGUAGE.'.FOR')}}:</span> 
											{{ (Lang::has(Session::get('lang_file').'.Single')!= '')  ?  trans(Session::get('lang_file').'.Single'): trans($OUR_LANGUAGE.'.Single')}}
											</div> <!-- all-order-td -->
											<?php } if(count($getPackage) >0){ ?>	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.FLOWER_TYPE')!= '')  ?  trans(Session::get('lang_file').'.FLOWER_TYPE'): trans($OUR_LANGUAGE.'.FLOWER_TYPE')}}:</span> 
											<?php foreach($getPackage as $pdata){ $getName = Helper::getProduckInfo($pdata->item_id); echo $getName->pro_title.','; } ?>
											</div> <!-- all-order-td -->
											<?php 
											 }
											 if(isset($serviceInfo->packege) && $serviceInfo->packege=='yes')
											 {
											  $wrappingType        = Helper::setExtraProductField($val->product_id,$val->merchant_id,$option_id=25);
									          $wrappingDesign      = Helper::setExtraProductField($val->product_id,$val->merchant_id,$option_id=26);
											 ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WRAPPING_TYPE')!= '')  ?  trans(Session::get('lang_file').'.WRAPPING_TYPE'): trans($OUR_LANGUAGE.'.WRAPPING_TYPE')}}:</span> 
											<?php if(isset($wrappingType->option_title) && $wrappingType->option_title!=''){echo $wrappingType->option_title.': SAR '.number_format($wrappingType->price,2);}else{echo 'N/A';} ?>
											</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WRAPPING_DESIGN')!= '')  ?  trans(Session::get('lang_file').'.WRAPPING_DESIGN'): trans($OUR_LANGUAGE.'.WRAPPING_DESIGN')}}:</span> 
											<?php if(isset($wrappingDesign->option_title) && $wrappingDesign->option_title!=''){echo $wrappingDesign->option_title.': SAR '.number_format($wrappingDesign->price,2);}else{echo 'N/A';} ?>
											</div> <!-- all-order-td -->
											 <?php 
											} 
											if(isset($serviceInfo->packege) && $serviceInfo->packege=='no')
											{
											 $wrappingTypeOrder = Helper::setMorePackage($val->order_id,$val->product_id,$option_id=25);
									         $wrappingDesignOrder = Helper::setMorePackage($val->order_id,$val->product_id,$option_id=26);
											 
									        //print_r($wrappingTypeOrder->product_option_value_id);
											//$wrappingType        = Helper::setFieldShow($wrappingTypeOrder->product_option_value_id);
											 							   
											?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WRAPPING_TYPE')!= '')  ?  trans(Session::get('lang_file').'.WRAPPING_TYPE'): trans($OUR_LANGUAGE.'.WRAPPING_TYPE')}}:</span> 
											<?php echo @$wrappingType->option_title.': SAR '.number_format(@$wrappingTypeOrder->value,2); ?>
											</div> <!-- all-order-td -->
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.WRAPPING_DESIGN')!= '')  ?  trans(Session::get('lang_file').'.WRAPPING_DESIGN'): trans($OUR_LANGUAGE.'.WRAPPING_DESIGN')}}:</span> 
											<?php echo @$wrappingDesign->option_title.': SAR '.number_format(@$wrappingDesignOrder->value,2); ?>
											</div> <!-- all-order-td -->
										   <?php } ?>
											</div> <!-- all-order-tr -->	
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                        </div>
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">  
										   @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp
								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                 
                                <!-------- Roses Order End------------------>
								
								<!-------- Special Event Order Start------------------>
								 <?php $i=1;$basetotal = 0; $couponcode = 0; $couponAmt=0; $totalinsu=0; if(isset($orderSpecialEvent) && count($orderSpecialEvent) > 0){ foreach($orderSpecialEvent as $val){ $basetotal = ($basetotal+$val->total_price);
								 	$totalinsu = ($totalinsu+$val->insurance_amount);
								  $couponAmt     = $val->coupon_code_amount;
								           $couponcodeinfo= $val->coupon_code; 
								           $couponcode = $val->coupon_code_amount;  
								 ?>
                                <div class="goldencartitems">
                               
                                      <table width="100%" class="serviceorder-det-subtable">
                                        <tbody>
                                          <tr class="tr">
										   <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
										        @if(isset($val->created_at) && $val->created_at!='')
                                                

												@if(Session::get('lang_file')!='en_lang')
												{{ Carbon\Carbon::parse($val->created_at)->format('Y  M d')}}
												@else
												{{ Carbon\Carbon::parse($val->created_at)->format('d M Y')}}
												@endif
                                                @endif
												 </td>
												<?php if(isset($val->product_sub_type) && ($val->product_sub_type=='spa' || $val->product_sub_type=='beauty_centers')){}else{ ?>
											<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">
											 @if(isset($val->quantity) && $val->quantity!='')
                                            {{ $val->quantity}}
                                            @endif 
											</td>
											<?php } ?>
                                            <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.STATUS')!= '')  ?  trans(Session::get('lang_file').'.STATUS'): trans($OUR_LANGUAGE.'.STATUS')}}">
											@if(isset($val) && $val!='')
											   @if($val->status==1)
											   @if(Lang::has(Session::get('lang_file').'.Process')!= '') {{ trans(Session::get('lang_file').'.Process')}}  @else {{ trans($OUR_LANGUAGE.'.Process')}} @endif
											   @elseif($val->status==2)
											   @if(Lang::has(Session::get('lang_file').'.Delivered')!= '') {{ trans(Session::get('lang_file').'.Delivered')}}  @else {{ trans($OUR_LANGUAGE.'.Delivered')}} @endif
											   @elseif($val->status==3)
											   @if(Lang::has(Session::get('lang_file').'.Hold')!= '') {{ trans(Session::get('lang_file').'.Hold')}}  @else {{ trans($OUR_LANGUAGE.'.Hold')}} @endif
											   @elseif($val->status==4)
											   @if(Lang::has(Session::get('lang_file').'.Failed')!= '') {{ trans(Session::get('lang_file').'.Failed')}}  @else {{ trans($OUR_LANGUAGE.'.Failed')}} @endif
											   @endif
											 @endif
											</td>                            
											
                                            <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.AMOUNT'): trans($OUR_LANGUAGE.'.AMOUNT')}}">
											@if(isset($val) && $val!='')
											SAR {{ number_format($val->total_price,2) }}
											@endif
											</td>
                                          </tr>
                                        </tbody>
                                      </table>
									  <div class="myac-all-order">
										<?php	
										//echo '<pre>';print_r($val);die;									
										$serviceInfo     = Helper::getProduckInfo($val->product_id);
										$department_name = Helper::getProduckInfoAttribute($serviceInfo->attribute_id);
										$extraInfo       = Helper::getExtraInformationOfProduct($val->shop_id,$val->order_id,$val->cus_id);
										$productinfo     = Helper::getAllProductAttribute($val->product_id,$val->merchant_id);
										if(isset($serviceInfo->pro_disprice) && $serviceInfo->pro_disprice!='0'){$getPrice = $serviceInfo->pro_disprice;}else{$getPrice = $serviceInfo->pro_price;}
										?>
										  <div class="all-order-tr">
										  
										    <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.PRODUCT_NAME')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT_NAME'): trans($OUR_LANGUAGE.'.PRODUCT_NAME')}}:</span> <?php echo $serviceInfo->pro_title; ?></div> <!-- all-order-td -->	
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.Price')!= '')  ?  trans(Session::get('lang_file').'.Price'): trans($OUR_LANGUAGE.'.Price')}}:</span>  SAR <?php echo $getPrice; ?></div> <!-- all-order-td -->	
											<?php if(isset($department_name->attribute_title) && $department_name->attribute_title!=''){ ?>
											<div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.TYPE')!= '')  ?  trans(Session::get('lang_file').'.TYPE'): trans($OUR_LANGUAGE.'.TYPE')}}:</span> 
											<?php echo $department_name->attribute_title; ?>
											</div> <!-- all-order-td -->				
											<?php } ?>
											 
											</div> <!-- all-order-tr -->	
										 <div class="all-order-tr">
										  <?php if(isset($serviceInfo->pro_Img) && $serviceInfo->pro_Img!=''){ ?>
										  <div class="all-order-td"><span>{{ (Lang::has(Session::get('lang_file').'.IMAGE')!= '')  ?  trans(Session::get('lang_file').'.IMAGE'): trans($OUR_LANGUAGE.'.IMAGE')}}:</span>  <img src="<?php echo $serviceInfo->pro_Img; ?>" width="80" height="80"/></div> <!-- all-order-td -->
										  <?php } ?>
										  </div>
                                        </div>
                                 
                                </div>
								<?php $i++;} ?> 
								<div class="main_total">
								<div class="vat-tax-line">  
										   @php if(isset($couponcode) && $couponcode>=1){ $couponamount=$couponcode; }else{ $couponamount=0; }
              $calnettotalamount=$basetotal-$couponamount-$totalinsu;
             @endphp

          {{ (Lang::has(Session::get('lang_file').'.VAT_CHARGE')!= '')  ?  trans(Session::get('lang_file').'.VAT_CHARGE'): trans($MER_OUR_LANGUAGE.'.VAT_CHARGE')}}: 
          @php $vatamount=Helper::calculatevat($val->order_id,$calnettotalamount); @endphp

          @php if(isset($vatamount) && $vatamount!='')
          {
          echo 'SAR '.number_format($vatamount,2);
          }
          else
          {
          echo 'N/A';
          }
          @endphp
								<br>
								@if($couponcode >=1)
								{{ (Lang::has(Session::get('lang_file').'.COUPON')!= '')  ?  trans(Session::get('lang_file').'.COUPON'): trans($OUR_LANGUAGE.'.COUPON')}}: 
										 <?php echo 'SAR '.number_format($couponcode,2); ?><br>
										 {{ (Lang::has(Session::get('lang_file').'.coupon_code')!= '')  ?  trans(Session::get('lang_file').'.coupon_code'): trans($OUR_LANGUAGE.'.coupon_code')}}: <?php echo  $couponcodeinfo;?><br>
								@endif
								
								 </div>
								
								{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}: 
								<?php if(isset($productdetails) && $productdetails!=''){
								echo 'SAR '.number_format(($basetotal+$vatamount-$couponcode),2);
								 } ?>              
								</div>
								<?php } ?>                                 
                                <!-------- Special Event Order End------------------>
                          </td>
                        </tr>
                        <?php /*?><tr class="tr total-prise">
                          <td class="td td4" data-title="Total" colspan="6">
						  <span>{{ (Lang::has(Session::get('lang_file').'.TOTAL_PRICE')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_PRICE'): trans($OUR_LANGUAGE.'.TOTAL_PRICE')}}</span>: 
							
				          </td>
                        </tr><?php */?>
                      </tbody>
                    </table>
                    <!-- table -->
                  </div>
                  <!-- tatal-services-pay-area -->
          
              
              

          
          
            </div>
          </div>
		  <?php }else{?>
			<div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div> <!-- no-record-area -->
			<?php } ?>
        </div>
      </div></div>
    </div>
    <!-- page-right-section -->
  </div>
  <!-- page-left-right-wrapper -->
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer') 
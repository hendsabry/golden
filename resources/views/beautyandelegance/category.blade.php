@include('includes.navbar')
<div class="outer_wrapper diamond_fullwidth"> @include('includes.header')
  <div class="inner_wrap">
    <div class="search-section">
      <div class="mobile-back-arrow"><img src="{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
      @include('includes.searchweddingandoccasions') </div>
    <!-- search-section -->
    <div class="page-left-right-wrapper"> @include('includes.mobile-modify')

        <div class="page-right-section">

		<div class="diamond_main_wrapper"> @if(Session::get('lang_file')!='en_lang')
		 <img src="{{ url('') }}/themes/images/beauty_ar.jpg" alt="" usemap="#hall-subcategory" hidefocus="true"> 
		 @else 
		 <img src="{{ url('') }}/themes/images/beauty.jpg" alt="" usemap="#hall-subcategory"> 
		 @endif
          <map name="hall-subcategory" id="hall-subcategory">
                <area shape="poly" coords="572,112,469,12,299,178,292,188,295,189,648,190" href="{{ url('') }}/beautycenters/18/19" />
				<area shape="poly" coords="108,372,835,374,926,466,833,551,434,552,103,550,12,465" href="{{ url('') }}/spa/18/21" />
				<area shape="poly" coords="283,555,830,555,767,621,654,733,560,733,282,732,105,555" href="{{ url('') }}/makeupartists/18/22" />
				<area shape="poly" coords="652,736,283,734,280,732,290,741,384,833,470,920" href="{{ url('') }}/makeupproducts/18/23" />
				<area shape="poly" coords="649,192,288,192,108,369,109,371,830,371,826,371" href="{{ url('') }}/barbers/18/20" />
          </map>
        </div>
		
		

          <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
        </div>
        <!-- page-right-section -->

    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')
@include('includes.navbar')
 @php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
  $Current_Currency = 'SAR'; 
  } 
@endphp
<div class="outer_wrapper">
  <div class="vendor_header">
    <div class="inner_wrap">
      <div class="vendor_header_left">
        <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{ $fooddateshopdetails[0]->mc_img }}" alt="" /></a></div>
      </div>  
      <!-- vendor_header_left -->
      @include('includes.vendor_header')
      <!-- vendor_header_right -->
    </div>
  </div>

  <!-- vemdor_header -->
  <div class="common_navbar">
    <div class="inner_wrap">
      <div id="menu_header" class="content">
        <ul>
          <li><a href="#about_shop" class="active">@if (Lang::has(Session::get('lang_file').'.About_Shop')!= '') {{  trans(Session::get('lang_file').'.About_Shop') }} @else  {{ trans($OUR_LANGUAGE.'.About_Shop') }} @endif</a></li>
          <li><a href="#video">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif</a></li>
          <?php if(count($fooddateshopreview) > 0){ ?>
          <li><a href="#our_client">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</a></li>
          <?php } ?>
          <li><a href="#choose_package">@if (Lang::has(Session::get('lang_file').'.Choose_Package')!= '') {{  trans(Session::get('lang_file').'.Choose_Package') }} @else  {{ trans($OUR_LANGUAGE.'.Choose_Package') }} @endif</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- common_navbar -->
  <div class="inner_wrap service-wrap diamond_space">
    <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
      <div class="service_detail_row">
        <div class="gallary_detail">
          <section class="slider">
            <div id="slider" class="flexslider">
              <ul class="slides">
                @foreach($fooddateshopgallery as $shopgallery)
                @php 
                $im=$shopgallery->image;
                $gimg=str_replace('thumb_','',$im);
                @endphp
                <li> <img src="{{ $gimg }}" /> </li>
                @endforeach
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides">
                @foreach($fooddateshopgallery as $shopgallerythumb)
                <li> <img src="{{ $shopgallerythumb->image }}" /> </li>
                @endforeach
              </ul>
            </div>
          </section>
        </div>
        @php $cid=$fooddateshopdetails[0]->city_id;
        $getCity = Helper::getcity($cid);
        if(Session::get('lang_file') !='en_lang'){
        $city_name= 'ci_name_ar';
        }else{
        $city_name= 'ci_name'; 
        }  
        
        @endphp
        <div class="service_detail">
          <!-- Example DataTables Card-->
          <div class="detail_title">{{ $fooddateshopdetails[0]->mc_name }}</div>
          <div class="detail_hall_description">{{ $fooddateshopdetails[0]->address }}</div>
          <div class="detail_hall_subtitle">@if (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '') {{  trans(Session::get('lang_file').'.ABOUT_SHOP') }} @else  {{ trans($OUR_LANGUAGE.'.ABOUT_SHOP') }} @endif</div>
          <div class="detail_about_hall">
            <div class="comment more">{{ $fooddateshopdetails[0]->mc_discription }}</div>
          </div>
          <div class="detail_hall_dimention">@if (Lang::has(Session::get('lang_file').'.CITY')!= ''){{  trans(Session::get('lang_file').'.CITY') }}@else{{ trans($OUR_LANGUAGE.'.CITY') }}@endif: <span>{{ $getCity->$city_name }}</span></div>
          <div class="detail_hall_dimention">@if (Lang::has(Session::get('lang_file').'.TIME')!= '') {{  trans(Session::get('lang_file').'.TIME') }} @else  {{ trans($OUR_LANGUAGE.'.TIME') }} @endif: <span>{{ $fooddateshopdetails[0]->opening_time }} - {{ $fooddateshopdetails[0]->closing_time }} </span></div>
          @php  $homevisitcharges=currency($fooddateshopdetails[0]->home_visit_charge, 'SAR',$Current_Currency, $format = false); @endphp

             @php if($fooddateshopdetails[0]->google_map_address!=''){   $lat=$fooddateshopdetails[0]->latitude;   
              $long=$fooddateshopdetails[0]->longitude;  

               @endphp
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          @php }  @endphp 
 </div>
      </div>
      <!-- service_detail_row -->
      <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
        <?php if(isset($fooddateshopdetails[0]->mc_video_url) && $fooddateshopdetails[0]->mc_video_url!=''){ ?>
        <div class="service-video-area">
          <div class="service-video-cont">{{ $fooddateshopdetails[0]->mc_video_description }}</div>
          <div class="service-video-box">
            <iframe class="service-video" src="{{ $fooddateshopdetails[0]->mc_video_url }}?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <?php } ?>
        <!-- service-video-area -->
        <?php if(count($fooddateshopreview) > 0){ ?>
        <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                <ul class="slides">
                  @foreach($fooddateshopreview as $customerreview)
                  <li>
                    <div class="testimonial_row">
                      <div class="testim_left">
                        <div class="testim_img"><img src="{{ $customerreview->cus_pic }}"></div>
                      </div>
                      <div class="testim_right">
                        <div class="testim_description">{{ $customerreview->comments }}</div>
                        <div class="testim_name">{{ $customerreview->cus_name }}</div>
                        <div class="testim_star"><img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png"></div>
                      </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
        </div>
        <?php } ?>
      </div>
      <!-- service-mid-wrapper -->
      @php $tbl_field='service_id';  @endphp
      <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                @php $k=1; @endphp
                @foreach($servicecategoryAndservices as $categories)
                @php 
                if(count($categories->serviceslist)>0){
                if($k==1){ $cl='select'; $HH = $categories->serviceslist[0]->pro_id; }else{ $cl=''; } 
                @endphp
                <li><a href="#{{$k}}" data-tabid="{{$k}}" class="cat tabIDs{{$k}} {{$cl}}" data-toggle="tab" onclick="getbeautyservice('{{ $categories->serviceslist[0]->pro_id }}','{{ $branchid }}','{{$tbl_field}}','{{$k}}');">{{ $categories->attribute_title }}</a></li>
                @php $k++; } @endphp
                @endforeach
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
      <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
        
        <div class="service-display-right">
          <div class="diamond_main_wrapper">
            <div class="diamond_wrapper_outer"> @php  $z=1;     @endphp
              @foreach($servicecategoryAndservices as $productservice)
              @php    $k=count($productservice->serviceslist); 
              if($k >0 ){
              @endphp
              @php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp
              <div class="diamond_wrapper_main tab-pane fade {{ $addactcon }}"  id="{{ $z}}"> @php if($k < 6){ @endphp
                @php  $i=1;     @endphp
                <div class="diamond_wrapper_inner "> @foreach($productservice->serviceslist as $getallcats)
                  @php if($k==3){ $cl='category_wrapper'.$i; }else{ $cl=''; } @endphp
				  @php 
        $im=$getallcats->pro_Img;
       $gimg=str_replace('thumb_','',$im);
        @endphp
                  <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a href="javascript:void(0);" onclick="getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                    <div class="category_wrapper {{ $cl }}" style="background:url({{ $gimg or '' }});">
                      <div class="category_title">
                        <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                      </div>
                    </div>
                    </a> </div>
                  @php $i=$i+1; @endphp
                  
                  @endforeach </div>
                <!------------ 6th-------------->
                @php }elseif($k==6){ @endphp
                @php $j=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productservice->serviceslist as $getallcats)
                  @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                  @php if($j==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($j==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($j==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($j==6){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($j==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==4){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($j==5){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($j==6){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $j=$j+1; @endphp
                  @endforeach </div>
                <!------------ 7th-------------->
                @php }elseif($k==7){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productservice->serviceslist as $getallcats)
                  @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                  


                  @php if($l==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($l==3){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp
                          @php if($l==7){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==2){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==7){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!------------ 8th-------------->
                @php }elseif($k==8){ @endphp
                @php $l=1; @endphp
                <div class="diamond_wrapper_inner"> @foreach($productservice->serviceslist as $getallcats)
                  @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                  @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                  @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                  
                  @php if($l==1){ $classrd='category_wrapper1'; @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($l==2){ @endphp
                    <div class="row_3of5 rows5row"> @php } @endphp 
                      @php if($l==4){  @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp
                          @php if($l==8){ $classrd='category_wrapper9'; @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                            <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                              <div class="category_title">
                                <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                              </div>
                            </div>
                            </a> @php if($l==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php if($l==8){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp
                  @php $l=$l+1; @endphp
                  @endforeach </div>
                <!---------- 9th ------------------->
                @php }elseif($k>=9){ @endphp
                <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                  @foreach($productservice->serviceslist as $getallcats)
                  @php if($i==1) { $k=9; }else{ $k=$i;}
                  if($i<=9){
                  @endphp
                  
                  
                  @php if($i==1){ @endphp
                  <div class="row_1of5 rows5row"> @php } @endphp 
                    @php if($i==2){ @endphp
                    <div class="row_2of5 rows5row"> @php } @endphp 
                      @php if($i==4){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($i==7){ @endphp
                        <div class="row_4of5 rows5row"> @php } @endphp 
                          @php if($i==9){ @endphp
                          <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');"> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});"> <span class="category_title"><span class="category_title_inner">{{ $getallcats->pro_title or ''}}</span></span> </span> </a> @php if($i==1){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==3){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    @php if($i==9){ @endphp
                    <div class="clear"></div>
                  </div>
                  @php } @endphp 
                  
                  @php $i=$i+1; } @endphp
                  @endforeach </div>
                @php } @endphp </div>
              @php $z=$z+1; }@endphp
              @endforeach </div>
          </div>
          <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></span></div>
          @php  $PP=1; $jk=0; @endphp
          @foreach($servicecategoryAndservices as $productservice)
          @php 
          $TotalPage = $productservice->serviceslist->count(); 
          if($TotalPage > 9)
          {
          $paginations = ceil($TotalPage/9);
          }
          else
          {
          $paginations =0;
          }
          if(count($productservice->serviceslist) < 1) {continue; }
          
          @endphp
          
          @if($paginations!=0)
          <div class="pagnation-area" id="tab{{$PP}}" style="display: none;">
            <ul class="pagination">
              <li class="disabled"><span>«</span></li>
              @for($KO=1;$KO<=$paginations;$KO++)
              <li class="@if($KO==1) active @endif paginate" data-page="{{$KO}}" data-attribute_id="{{ $productservice->id }}"  data-branchid="{{ $branchid }}" data-tblfield="{{$tbl_field}}" data-tabid="{{$PP}}"><span>{{$KO}}</span></li>
              @endfor
              <li><a href="#?page={{$paginations}}" rel="next">»</a></li>
            </ul>
          </div>
          @endif
          
          @php  $PP=$PP+1; $jk++; @endphp
          @endforeach </div>
        <!-- service-display-right -->
        {!! Form::open(['url' => 'beautyshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
        <!-- container-section -->
        <div class="service-display-left" id="selectedproduct">
      <b>{{ Session::get('status') }}</b>
          <div class="service-left-display-img product_gallery"><a name="common_linking" class="linking">&nbsp;</a>
             @php    $pro_id = $beautyshopleftproduct->pro_id; @endphp
             @include('includes/product_multiimages')
          </div>
          <div class="service-product-name">{{ $beautyshopleftproduct->pro_title }}</div>
          <div class="service-beauty-description">{{ $beautyshopleftproduct->pro_desc }}</div>
          <div class="container-section">
            <div class="duration">@if (Lang::has(Session::get('lang_file').'.DURATION')!= '') {{  trans(Session::get('lang_file').'.DURATION') }} @else  {{ trans($OUR_LANGUAGE.'.DURATION') }} @endif <span>{{ $beautyshopleftproduct->service_hour }} Hrs</span> </div>
            <div class="beauty-prise"> @php if($beautyshopleftproduct->pro_discount_percentage>0){ $productprice= $beautyshopleftproduct->pro_price*(100-$beautyshopleftproduct->pro_discount_percentage)/100;}else{ $productprice='';}
              if($productprice==''){ @endphp <span> {{currency(number_format($beautyshopleftproduct->pro_price,2), 'SAR',$Current_Currency) }}</span> @php }else{ @endphp <span class="strike">{{currency(number_format($beautyshopleftproduct->pro_price,2), 'SAR',$Current_Currency) }} </span><span> {{currency(number_format($productprice,2), 'SAR',$Current_Currency) }} </span> @php  } @endphp </div>
         
 





            @php if($beautyshopleftproduct->video_url!=''){ @endphp
            <div class="staff_area vds">
              <div class="leftbar_title">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif  </div>
              <div class="choose_staf_box">
                <iframe class="service-videso" src="{{ $beautyshopleftproduct->video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
            @php }
            @endphp </div>
        </div>
        <!-- container-section -->
        <div class="service-display-left bgs">
          <div class="leftbar_title">@if (Lang::has(Session::get('lang_file').'.BOOK_APPOINTMENT')!= '') {{  trans(Session::get('lang_file').'.BOOK_APPOINTMENT') }} @else  {{ trans($OUR_LANGUAGE.'.BOOK_APPOINTMENT') }} @endif</div>
          <div class="books_form">
            <div class="books_male_area">
      
           <?php if(isset($fooddateshopdetails[0]->service_availability) && $fooddateshopdetails[0]->service_availability==1){ ?>
            <div class="books_male_left">
                <input id="Home" name="appointmentfor" type="radio" value="home" Onchange="addextracharges({{$homevisitcharges}},'add');">
                <label for="Home">@if (Lang::has(Session::get('lang_file').'.HOME')!= '') {{  trans(Session::get('lang_file').'.HOME') }} @else  {{ trans($OUR_LANGUAGE.'.HOME') }} @endif </label>
              </div>
            <?php }if(isset($fooddateshopdetails[0]->service_availability) && $fooddateshopdetails[0]->service_availability==2){ ?>
            <div class="books_male_left">
                <input id="Shop" name="appointmentfor" type="radio" value="shop" Onchange="addextracharges({{$homevisitcharges}},'remove');">
                <label for="Shop">@if (Lang::has(Session::get('lang_file').'.SHOP')!= '') {{  trans(Session::get('lang_file').'.SHOP') }} @else  {{ trans($OUR_LANGUAGE.'.SHOP') }} @endif</label>
              </div>
            <?php }if(isset($fooddateshopdetails[0]->service_availability) && $fooddateshopdetails[0]->service_availability==3){ ?>
             <div class="books_male_left">
                <input id="Home" name="appointmentfor" type="radio" value="home" Onchange="addextracharges({{$homevisitcharges}},'add');">
                <label for="Home">@if (Lang::has(Session::get('lang_file').'.HOME')!= '') {{  trans(Session::get('lang_file').'.HOME') }} @else  {{ trans($OUR_LANGUAGE.'.HOME') }} @endif </label>
              </div>
              <div class="books_male_left">
                <input id="Shop" name="appointmentfor" type="radio" value="shop" Onchange="addextracharges({{$homevisitcharges}},'remove');">
                <label for="Shop">@if (Lang::has(Session::get('lang_file').'.SHOP')!= '') {{  trans(Session::get('lang_file').'.SHOP') }} @else  {{ trans($OUR_LANGUAGE.'.SHOP') }} @endif</label>
              </div>
            <?php } ?>      
              <label for="appointmentfor" class="error"></label>
            </div>
            <div id="shophomevisitcharges" style="display: none;">@if (Lang::has(Session::get('lang_file').'.HOME_CHARGES')!= '') {{  trans(Session::get('lang_file').'.HOME_CHARGES') }} @else  {{ trans($OUR_LANGUAGE.'.HOME_CHARGES') }} @endif - {{ Session::get('currency') }} {{ $homevisitcharges }}</div>
            <div id="shopaddr" style="display: none;">{{ $fooddateshopdetails[0]->address }}</div>
            <span id="btime" style="display: none;">
            <div class="books_male_area">
              <div class="book_label">@if (Lang::has(Session::get('lang_file').'.Date')!= '') {{  trans(Session::get('lang_file').'.Date') }} @else  {{ trans($OUR_LANGUAGE.'.Date') }} @endif</div>
              <div class="book_input">
                <input name="bookingdate" type="text"  id="bookingdate" autocomplete="off" class="t-box cal-t" readonly="" onchange="getdatefield();"/>
              </div>
            </div>
            
            </span> </div>


              <div id="staff_area_heading" class="staff_area chosestaffheading" style="display: none;">@if (Lang::has(Session::get('lang_file').'.Choose_Staff')!= '') {{  trans(Session::get('lang_file').'.Choose_Staff') }} @else  {{ trans($OUR_LANGUAGE.'.Choose_Staff') }} @endif</div>

        <div class="availibity_staff" id="availibitystaff" style="display: none;"><a href="javascript:void(0);" onclick="staffavailabilitychart('{{ $branchid }}'); ">@if (Lang::has(Session::get('lang_file').'.Staff_Availibility_Chart')!= '') {{  trans(Session::get('lang_file').'.Staff_Availibility_Chart') }} @else  {{ trans($OUR_LANGUAGE.'.Staff_Availibility_Chart') }} @endif</a></div>

            <span><label for="staffid" class="error"></label></span>
          <span id="ourstaff" style="display: none;"> @php if(isset($servicestaffexpert) && $servicestaffexpert!=''){ @endphp
          @php  $servicestaffid=''; if(count($servicestaffexpert)>0){ @endphp
          <div class="staff_area" id="staffarea" style="display: none;">
            <!--<div class="leftbar_title">@if (Lang::has(Session::get('lang_file').'.Choose_Staff')!= '') {{  trans(Session::get('lang_file').'.Choose_Staff') }} @else  {{ trans($OUR_LANGUAGE.'.Choose_Staff') }} @endif</div>-->
            <div class="choose-line-section"> @foreach($servicestaffexpert as $staffandreview)
              @php 
              $noofreviews=0;
              if(isset($staffandreview->staffreviews) && $staffandreview->staffreviews!=''){
              $noofreviews=sizeof($staffandreview->staffreviews); 
              }
              $starrating=0;
              if(isset($staffandreview->staffratingsavg) && $staffandreview->staffratingsavg!=''){
              $starrating=ceil($staffandreview->staffratingsavg); 
              } 
              
              $servicestaffid.= $staffandreview->staffinformation[0]->id.',';
              
              @endphp
              <div class="choose-line">
                <div class="choose-line-img"><img src="{{$staffandreview->staffinformation[0]->image or ''}}" alt="" /></div>
                <div class="choose-line-cont">
                  <div class="choose-line-neme">{{$staffandreview->staffinformation[0]->staff_member_name or ''}}</div>
                  <div class="choose-line-experience"> {{$staffandreview->staffinformation[0]->experience or ''}} @if (Lang::has(Session::get('lang_file').'.Yrs_Exp')!= '') {{  trans(Session::get('lang_file').'.Yrs_Exp') }} @else  {{ trans($OUR_LANGUAGE.'.Yrs_Exp') }} @endif</div>
                   <div class="choose-line-neme" id="st{{ $staffandreview->staffinformation[0]->id }}"></div>
                <div class="choose-line-neme" id="booking{{ $staffandreview->staffinformation[0]->id }}"></div>
                  <div class="choose-line-review"> @php if($starrating>0){ @endphp <img src="{{url('/')}}/themes/images/star{{ $starrating or ''}}.png"> @php } @endphp

                    @php if($noofreviews>0){ @endphp <span onclick="checkstaffreview('{{ $staffandreview->staffinformation[0]->id or '' }}');">{{$noofreviews or ''}} @if (Lang::has(Session::get('lang_file').'.REVIEW')!= '') {{  trans(Session::get('lang_file').'.REVIEW') }} @else  {{ trans($OUR_LANGUAGE.'.REVIEW') }} @endif</span> @php } @endphp </div>
                  <div class="choose-line-radio">
                    <input id="name" name="staffid" type="radio" value="{{ $staffandreview->staffinformation[0]->id or '' }}" onclick="checkstaffavailability('{{ $staffandreview->staffinformation[0]->id or ''}}');">
                    <label for="name">&nbsp;</label>
                  </div>
                </div>
              </div>
              <!-- choose-line -->
              @endforeach </div>
            <!-- choose-line-section -->
          </div>
          @php } @endphp
          @php } @endphp </span> 
		  
		  
		  
		  <span id="finalsbt" style="display: none;">
          <div class="total_food_cost">
            <div class="total_price"> @php if($productprice!=''){ $totalprice=$productprice;}else{$totalprice=$beautyshopleftproduct->pro_price;} @endphp
              @if (Lang::has(Session::get('lang_file').'.Total_Price')!= ''){{trans(Session::get('lang_file').'.Total_Price')}}@else{{ trans($OUR_LANGUAGE.'.Total_Price')}}@endif: {{ Session::get('currency') }} <span id="totalprice">{{currency($totalprice, 'SAR',$Current_Currency) }} </span></div>
          </div>
      
          <div class="btn_row"> @php if($category_id==20){ $sub_type='men_saloon'; }else{ $sub_type='makeup_artists'; } @endphp
            <input type="hidden" name="service_duration" id="service_duration" value="{{ $beautyshopleftproduct->service_hour }}">
            <input type="hidden" name="opening_time" id="opening_time" value="{{ $fooddateshopdetails[0]->opening_time }}">
            <input type="hidden" name="closing_time" id="closing_time" value="{{ $fooddateshopdetails[0]->closing_time }}">
            <input type="hidden" name="category_id" value="{{ $subsecondcategoryid}}">
            <input type="hidden" name="subsecondcategoryid" value="{{ $category_id }}">
            <input type="hidden" name="cart_sub_type" value="{{ $sub_type }}">
            <input type="hidden" name="branch_id" value="{{ $branchid }}">
            <input type="hidden" name="shop_id" value="{{ $shop_id }}">
            <input type="hidden" name="vendor_id" value="{{ $beautyshopleftproduct->pro_mr_id }}">
            <input type="hidden" name="product_id" id="product_id" value="{{ $beautyshopleftproduct->pro_id }}">
            <input type="hidden" name="product_price" id="product_price" value="{{currency(number_format($totalprice,2), 'SAR',$Current_Currency, $format = false) }}">
            <input type="hidden" name="servicestaffid" id="servicestaffid" value="{{ $servicestaffid }}">
            <input type="hidden" name="product_orginal_price" id="product_orginal_price" value="{{currency(number_format($totalprice,2), 'SAR',$Current_Currency, $format = false) }}">
             <input type="hidden" name="todaydate" id="todaydate" value="{{ date('jMY') }}">
            <input type="submit" name="booknow" value="@if (Lang::has(Session::get('lang_file').'.booknow')!= '') {{  trans(Session::get('lang_file').'.booknow') }} @else  {{ trans($OUR_LANGUAGE.'.booknow') }} @endif" class="form-btn addto_cartbtn">
          </div>
		  <div class="terms_conditions"><a href="{{ $fooddateshopdetails[0]->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}} <a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a> </div>
          </span>
          <!-- container-section -->
        </div>
        <!-- service-display-left -->
        {!! Form::close() !!} </div>
      <!-- service-display-left -->
    </div>
    <!--service-display-section-->
    <!--service-display-section-->
    @php
    $sesionData = session()->all();
    if(isset($sesionData) && $sesionData!='')
    {
    $url = url('search-results?weddingoccasiontype='.$sesionData['searchdata']['weddingoccasiontype'].'&noofattendees='.$sesionData['searchdata']['noofattendees'].'&budget='.$sesionData['searchdata']['budget'].'&cityid='.$sesionData['searchdata']['cityid'].'&occasiondate='.$sesionData['searchdata']['occasiondate'].'&gender='.$sesionData['searchdata']['gender'].'&basecategoryid='.$sesionData['searchdata']['mainselectedvalue'].'&maincategoryid='.$sesionData['searchdata']['maincategoryid'].'&mainselectedvalue='.$sesionData['searchdata']['mainselectedvalue']); 
    }        
    @endphp
    <div class="sticky_other_service"> <a href="{{$url}}">
      <div class="sticky_serivce">@if(Lang::has(Session::get('lang_file').'.OTHER_SERVICE')!= '') {{ trans(Session::get('lang_file').'.OTHER_SERVICE')}}  @else {{ trans($OUR_LANGUAGE.'.OTHER_SERVICE')}} @endif</div>
      <div class="sticku_service_logo"><img src="{{ url('') }}/themes/images/logo.png"></div>
      </a> </div>
    <!-- other_serviceinc -->
    <!-- other_serviceinc -->
  </div>
  <!-- detail_page -->
</div>
</div>
 
 <!-- availibility_popup -->

<!-- outer_wrapper -->
@include('includes.footer')
@include('includes.popupmessage') 
@php $lang = Session::get('lang_file'); @endphp


<!----------------  staff chart availability------------>
<div class="avalb_popup_overlay"></div>
<div class="availibility_popup">
  <div class="avalb_date">@if(Lang::has(Session::get('lang_file').'.DATE')!= '') {{ trans(Session::get('lang_file').'.DATE')}}  @else {{ trans($OUR_LANGUAGE.'.DATE')}} @endif <span id="bookingdatepopup"></span> <a href="javascript:void(0);" class="avalb_close_popup">X</a></div>
  <div class="avalb_tbl_row">
    <div class="avalb_tbl_col3">@if(Lang::has(Session::get('lang_file').'.IMAGE')!= '') {{ trans(Session::get('lang_file').'.IMAGE')}}  @else {{ trans($OUR_LANGUAGE.'.IMAGE')}} @endif</div>
    <div class="avalb_tbl_col3">@if(Lang::has(Session::get('lang_file').'.NAME')!= '') {{ trans(Session::get('lang_file').'.NAME')}}  @else {{ trans($OUR_LANGUAGE.'.NAME')}} @endif</div>
    <div class="avalb_tbl_col3">@if(Lang::has(Session::get('lang_file').'.TIME')!= '') {{ trans(Session::get('lang_file').'.TIME')}}  @else {{ trans($OUR_LANGUAGE.'.TIME')}} @endif</div>
  </div>
<span id="staffpopup"></span>
</div>



<div class="review_overlay"></div>
<div class="user_review">
  <a href="javascript:void(0);" class="user_review_close">X</a>
  <div class="user_review_heading">Reviews By Customer</div>
  
  <div id="content-1" class="content mCustomScrollbar">
  
  <div class="rating_raea_box">
<span id="staffreviewsection"></span>

  </div>
  </div>
</div>



<script type="text/javascript">
  
  function getdatefield(){
   
       jQuery('.sel-time').css("display", "block");
      jQuery('.staffid').css("display", "block");
      jQuery('.sel-time').val('');
      jQuery(".staffid").prop("checked", false);
  }

</script>


<script>
function checkstaffreview(stid){
  //alert(stid);
jQuery('.user_review').css('display','block');
    jQuery('.review_overlay').css('display','block');
var product_id=jQuery('#product_id').val();

     if(stid){
        jQuery.ajax({
           type:"GET",          
            url:"{{url('beautyshop/getstaffreview')}}?staffid="+stid+"&product_id="+product_id,
           success:function(res){               
              if(res){
                 var json = JSON.stringify(res);
              var obj = JSON.parse(json);
        
                 console.log(obj);
                
                  workernoreview=obj.workerreview.length;
                  
                  if(length>0){
                    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June","July", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    jQuery('#staffreviewsection').html('');
                    for(jk=0; jk<workernoreview; jk++)
                      {
                        if(obj.workerreview[jk].revieweduser[0].cus_pic==''){ var cus_pic="{{url('/')}}/themes/images/reviewuser.png"; }else{ var cus_pic=obj.workerreview[jk].revieweduser[0].cus_pic; }
                        var posteddate = new Date(obj.workerreview[jk].created_at);
                        var getmonth=monthNames[posteddate.getMonth()];
                        var getdate=posteddate.getDate();
                        var getyear=posteddate.getFullYear();
                        var formateddate=getdate+' '+getmonth+' '+getyear;                        
                          jQuery('#staffreviewsection').append('<div class="rating_box"><div class="rpx_box"><div class="review_left"><img src="'+cus_pic+'" alt="" /></div><div class="review_right"><div class="review_box_type"><div class="review_name_area"><div class="review_cont_type"><div class="review_name">'+obj.workerreview[jk].revieweduser[0].cus_name+'</div><div class="review_star">Posted<span> '+formateddate+'</span></div></div><div class="review_date"><img src="{{url('/')}}/themes/images/star'+obj.workerreview[jk].ratings+'.png" alt="" /></div></div></div></div></div><div class="rating_desc_po"><div class="review_desc">'+obj.workerreview[jk].comments+'</div></div></div>');
                     }     
                  }else{
                    jQuery('#staffreviewsection').html('');
                  }
                  
                 

             }
           }
        });
    }



}

  jQuery(document).ready(function(){
  
    jQuery('.review_overlay, .user_review_close').click(function(){
  jQuery('.user_review').hide();
    jQuery('.review_overlay').hide();
  });
  });

</script>


<script type="text/javascript">
  <?php if(isset($HH) && $HH!=''){ ?>  
jQuery(window).load(function(){ 
  
            getbeautyservice('<?php echo $HH;?>','<?php echo $branchid; ?>','<?php echo $tbl_field; ?>','1');

 jQuery(".staffradio").attr('disabled', true);

});

<?php } ?>
</script>














<script type="text/javascript">
  function showstafflist(){
    jQuery('#staffarea').css('display','block');  
    jQuery('#availibitystaff').css('display','block'); 
      jQuery('#staff_area_heading').css('display','block'); 
  }

</script>
<script>
  jQuery(document).ready(function(){  
    jQuery('.availibity_staff a').click(function(){   
      jQuery('.availibility_popup').css('display','block');
      jQuery('.avalb_popup_overlay').css('display','block');      
    });
    jQuery('.avalb_close_popup').click(function(){
      jQuery('.availibility_popup').css('display','none');
      jQuery('.avalb_popup_overlay').css('display','none');     
    });
  });
  
</script>

    <script type="text/javascript">
  function staffavailabilitychart(branchid){
       var bookingdate=jql('#bookingdate').val();
      var product_id=jql('#product_id').val();
      var servicestaffid=jql('#servicestaffid').val();

       if(servicestaffid){
        jQuery.ajax({
           type:"GET",
           url:"{{url('beautyshop/staffavailabilitychart')}}?branchid="+branchid+"&servicestaffid="+servicestaffid+"&bookingdate="+bookingdate+"&product_id="+product_id,
           success:function(res){               
              if(res){
                 var json = JSON.stringify(res);
              var obj = JSON.parse(json);
        
                 console.log(obj);
                
                  length=obj.bookingopen.length;
                  jql('#bookingdatepopup').html(bookingdate);
                 
                  if(length>0){
                      jql('#staffpopup').html('');
                      for(jk=0; jk<length; jk++)
                        {
                          bookedslot=obj.bookingopen[jk].bookedslot.length;                   
                      jql('#staffpopup').append('<div class="avalb_row"><div class="avalb_tbl_col3"><img src="'+obj.bookingopen[jk].image+'"></div><div class="avalb_tbl_col3">'+obj.bookingopen[jk].staff_member_name+'</div><div class="avalb_tbl_col3"><span id="tslot'+jk+'"></span></div>');
                                if(bookedslot>0){                           
                                for(k=0; k<length; k++){
                                  jql('#tslot'+jk).append('<div class="time_slot">'+obj.bookingopen[jk].bookedslot[k].start_time+'-'+obj.bookingopen[jk].bookedslot[k].end_time+'</div>'); 
                                  }
                                 }else{
                                        jql('#tslot'+jk).append('<div class="time_slot">-</div>');  
                              }
                      
                            //jql('#staffpopup').append('</div>');
                      } 
                   
                  
                  }else{
                    jql('#staffpopup').html('<div class="avalb_row">No Data Found</div>');
                  }              
                 

             }
           }
        });
    }


    
  


  }

</script>




<!-------------- end  staff chart availability -------->




<script>
	jQuery(document).ready(function(){	
		jQuery('.availibity_staff a').click(function(){		
			jQuery('.availibility_popup').css('display','block');
			jQuery('.avalb_popup_overlay').css('display','block');			
		});
		jQuery('.avalb_close_popup').click(function(){
			jQuery('.availibility_popup').css('display','none');
			jQuery('.avalb_popup_overlay').css('display','none');			
		});
	});
	
</script>



<!--------start check staff availability-------->
<script type="text/javascript">
  function checkstaffavailability(selectedstaffid,st){
      var service_duration=jql('#service_duration').val();
      var staffid=selectedstaffid;
      var bookingdate=jql('#bookingdate').val();
      var bookingtime=jql('#bookingtime').val();

       if(staffid){
        jQuery.ajax({
           type:"GET",
           url:"{{url('beautyshop/staffavailability')}}?staffid="+staffid+"&service_duration="+service_duration+"&bookingdate="+bookingdate+"&bookingtime="+bookingtime,
           success:function(res){               
              if(res){
                 var json = JSON.stringify(res);
              var obj = JSON.parse(json);
        
                 console.log(obj);
                
                  length=obj.bookingopen.length;
                  if(length>0){
                    jql('#bookingtime').val('');                    
                    jql('input[name="staffid"]').prop('checked', false);
                    jql('#st'+staffid).html('already booked');
                    bookingrecorddata=obj.bookingrecord.length;
                    
                    if(bookingrecorddata>0){
                      jql('#booking'+staffid).html('<ul id="bslot" style="display:none;">');
                      for(k=0; k<bookingrecorddata; k++)
                        {
                        jql('#booking'+staffid).append('<li>'+obj.bookingrecord[k].start_time+' - '+obj.bookingrecord[k].end_time+'</li>');
                        }
                      jql('#booking'+staffid).append('<ul>');
                    }
                  }else{
                    jql('#st'+staffid).html('');
                  }
                  
                 

             }
           }
        });
    }


    
  


  }

</script>




<script type="text/javascript">

  function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  jQuery.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }
function OnKeyDown(event) { event.preventDefault(); };
</script>
<!------------- home charges calculation---------->
<script type="text/javascript">
  
  function addextracharges(hc,act){
      jQuery('#btime').css('display','');
      jQuery('#ourstaff').css('display','');
      jQuery('#finalsbt').css('display','');
      var productprice=document.getElementById('product_price').value;
      var product_orginal_price=document.getElementById('product_orginal_price').value;
      
      if(act=='add'){
      var homeservicescharges=parseFloat(product_orginal_price) + parseFloat(hc);
      jQuery('#shopaddr').css('display','none');
      jQuery('#shophomevisitcharges').css('display','');
      }
      if(act=='remove'){
      var homeservicescharges=parseFloat(product_orginal_price);
      jQuery('#shopaddr').css('display','');
       jQuery('#shophomevisitcharges').css('display','none');
      }
       homeservicescharges = homeservicescharges; 
       jQuery('[name=product_price]').val(homeservicescharges);
      var fprice=homeservicescharges.toFixed(2);
        jQuery('#totalprice').html(fprice);
  }
</script>
<!--------------- end calculation---------->
<!---------- validation ---------->
<script type="text/javascript">
jQuery("#cartfrm").validate({
                  ignore: [],
                  rules: {
                    appointmentfor : {required :true},
            bookingdate: {
                       required: true,
                      },           

                       bookingtime: {
                       required: true,
                      
                      },
                  staffid:{
                        required: true,
                      },
                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
              appointmentfor:{
                required: "@php echo (Lang::has(Session::get('lang_file').'.BOOKINGFOR')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGFOR'): trans($OUR_LANGUAGE.'.BOOKINGFOR'); @endphp",
              },
          bookingdate: {
               required:  "@php echo (Lang::has(Session::get('lang_file').'.BOOKINGDATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGDATE'): trans($OUR_LANGUAGE.'.BOOKINGDATE'); @endphp", 
                      },
                 bookingtime: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.BOOKINGTIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGTIME'): trans($OUR_LANGUAGE.'.BOOKINGTIME'); @endphp",
                            },
                    staffid: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.STAFFSLECT')!= '')  ?  trans(Session::get('lang_file').'.STAFFSLECT'): trans($OUR_LANGUAGE.'.STAFFSLECT'); @endphp",
                            },
               
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });




</script>
<!---------- end validation------>
<!------------------ tabs----------------->
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
</script>

<script src="{{url('/')}}/themes/js/timepicker/jquery-clockpicker.min.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery-clockpicker.min.css" rel="stylesheet" />
<script src="{{url('/')}}/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>

<!------------ end tabs------------------>
<!--------- date picker script-------->
<script type="text/javascript">
            // When the document is ready
           // alert(new Date().getTime());
      var jql = jQuery.noConflict();
            jql(document).ready(function () {


             var endtime=jQuery('#closing_time').val(); 
               var startdbtime=jQuery('#opening_time').val();  
               
                   var currentTime= moment();    // e.g. 11:00 pm
                    var mstartTime = moment(startdbtime, "HH:mm a");
                    var mendTime = moment(endtime, "HH:mm a");
                   var amIBetween = currentTime.isBetween(mstartTime , mendTime);

                   var current = new Date(); 
                    var followingDay = new Date(current.getTime() + 86400000);
                   
  var d = new Date(); 
  //d.setDate(d.getDate() + 4);
  var currMonth = d.getMonth();
  var currYear = d.getFullYear();
   if(amIBetween===true){
  var currDay = d;
   }else{
  var currDay = followingDay;
  }
  var ymMonth = (d.getMonth() + 3)
  var dd = d.getDate();
  var startDate = new Date(currYear, currMonth, currDay);
  var endsDate = new Date(currYear, ymMonth, dd);
  jql('#bookingdate').datepicker({
  //format: "yyyy-mm-dd",
   autoclose: true,
  format: "d M yyyy",      
  startDate:currDay,
  endDate:endsDate,
  }); 
            
 jql("#bookingdate").datepicker("setDate",currDay);
            var time = new Date();
          var curretime= time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });


               
                var  bookingdate=jql('#bookingdate').val();

                var  starttime=jql('#opening_time').val(); 

                 // var  starttime=curretime;  


               var endtime=jql('#closing_time').val();
                   var service_duration=jql('#service_duration').val();
                  var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A'); 
          jql('#bookingtime').timepicker('remove');
          jql('#bookingtime').timepicker({
          'minTime': starttime,
          'maxTime': netime,
          'dynamic': true,
          'timeFormat': 'g:i A',          
          }); 
            });
        </script>
<!--------- end date picker script------>

<script type="text/javascript">
  function getbeautyservice(selecteddateproduct,branchid,tbl,Ks){

      <?php  if (Lang::has(Session::get('lang_file').'.DURATION')!= '') $dur=  trans(Session::get('lang_file').'.DURATION'); else  $dur= trans($OUR_LANGUAGE.'.DURATION');        
              if (Lang::has(Session::get('lang_file').'.Video')!= '') $vid=  trans(Session::get('lang_file').'.Video'); else  $dur= trans($OUR_LANGUAGE.'.Video');
            
           ?>

      if(Ks!=undefined)
      {
      jQuery('.pagnation-area').hide()
      jQuery('#tab'+Ks).show()
      }
      jQuery('#availibitystaff').css('display','none'); 
         jQuery('#staff_area_heading').css('display','none');
     jQuery('[name=servicestaffid]').val('');
     jQuery('#bookingtime').val('');
     
    jQuery('#btime').css('display','none');
      jQuery('#ourstaff').css('display','none');
      jQuery('#finalsbt').css('display','none');
      jQuery('input[name=appointmentfor]').attr('checked',false);
      jQuery('#shopaddr').css('display','none');
      jQuery('#staffarea').css('display','block'); 
    var dateproductid=selecteddateproduct;

        if(selecteddateproduct){
        jQuery.ajax({
           type:"GET",
           url:"{{url('beautyandeleganceshop/getcartproduct')}}?product_id="+dateproductid+"&branchid="+branchid+"&tblfld="+tbl,
           success:function(res){               
            if(res){
               var json = JSON.stringify(res);
      var obj = JSON.parse(json);
      //alert(obj);
  <?php $Cur = Session::get('currency'); ?>
              
                length=obj.productsericeinfo.length;
                //alert(length);
                
                if(length>0){
     for(i=0; i<length; i++)
      {
        if(obj.productprice==obj.productsericeinfo[i].pro_price){
          TotalPricea = getChangedPrice(obj.productsericeinfo[i].pro_price);  
          var productrealprice='<span><?php echo $Cur; ?> '+TotalPricea+'</span>';
          var productrealpriceamount=obj.productsericeinfo[i].pro_price;
          productrealpriceamount = getChangedPrice(productrealpriceamount); 
        }else{
          productrealpriceamounat = getChangedPrice(obj.productsericeinfo[i].pro_price); 
          diss = getChangedPrice(obj.productprice); 
          var productrealprice='<span class="strike"><?php echo $Cur; ?> '+productrealpriceamounat+' </span><span> <?php echo $Cur; ?> '+diss+'</span>';
          var productrealpriceamount=obj.productprice;
          productrealpriceamount = getChangedPrice(productrealpriceamount); 
        }
        //alert(productrealpriceamount);
        jQuery('#selectedproduct').html('<div class="service-left-display-img product_gallery"><a name="common_linking" class="linking">&nbsp;</a> </div><div class="service-product-name">'+obj.productsericeinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productsericeinfo[i].pro_desc+'</div><div class="container-section"><div class="duration"><?php echo $dur;?> <span>'+obj.productsericeinfo[i].service_hour+' Hrs</span></div><span id="packageservices"></span><div class="beauty-prise">'+productrealprice+'</div>');

 checkgallery(selecteddateproduct);


        if(obj.productsericeinfo[i].video_url!=''){
jQuery('#selectedproduct').append('<div class="staff_area vds"><div class="leftbar_title"><?php echo $vid;?></div><div class="choose_staf_box"><iframe class="service-videso"  src="'+obj.productsericeinfo[i].video_url+'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div></div>');
        }
        jQuery('#selectedproduct').append('</div>');
        jQuery('[name=product_id]').val(obj.productsericeinfo[i].pro_id);
        jQuery('[name=service_duration]').val(obj.productsericeinfo[i].service_hour);       
        jQuery('[name=product_orginal_price]').val(productrealpriceamount);
        jQuery('[name=product_price]').val(productrealpriceamount);
        jQuery('#totalprice').html(productrealprice);
      }
      
      }
      <?php 
           if (Lang::has(Session::get('lang_file').'.Choose_Staff')!= '') $ChooseStaff=  trans(Session::get('lang_file').'.Choose_Staff'); else  $ChooseStaff= trans($OUR_LANGUAGE.'.Choose_Staff');
           if (Lang::has(Session::get('lang_file').'.Yrs_Exp')!= '') $Yrs_Exp=  trans(Session::get('lang_file').'.Yrs_Exp'); else  $Yrs_Exp= trans($OUR_LANGUAGE.'.Yrs_Exp');
           if (Lang::has(Session::get('lang_file').'.Time')!= '') $StaffAvailibilityTime=  trans(Session::get('lang_file').'.Time'); else  $StaffAvailibilityTime= trans($OUR_LANGUAGE.'.Time');
             if (Lang::has(Session::get('lang_file').'.REVIEW')!= '') $staff_REVIEW=  trans(Session::get('lang_file').'.REVIEW'); else  $staff_REVIEW= trans($OUR_LANGUAGE.'.REVIEW');
      ?>
      expertstaff=obj.expertstaff.length;
                jQuery("#ourstaff").html("");
            if(expertstaff>0){
              var servicestaffid=new Array();

     for(k=0; k<expertstaff; k++)
      {
          servicestaffid.push(obj.expertstaff[k].staff_id);
        var reviews=obj.expertstaff[k].staffreviews.length;

        if(reviews>0){
          var starreview='<div class="choose-line-review"><img src="{{url('/')}}/themes/images/star'+obj.expertstaff[k].staffratingsavg+'.png"><span onclick="checkstaffreview('+obj.expertstaff[k].staff_id+');">'+reviews+' <?php echo $staff_REVIEW; ?></span></div>';
        }else{ var starreview='';}
        if(k==0){ //var staffheading='<div class="leftbar_title"><?php echo $ChooseStaff; ?></div>'; 
        var staffheading=''; }else{ var staffheading='';}
      jQuery('#ourstaff').append('<div class="staff_area" id="staffarea">'+staffheading+'<div class="choose-line-section"><div class="choose-line"><div class="choose-line-img"><img src="'+obj.expertstaff[k].staffinformation[0].image+'" alt="" /></div><div class="choose-line-cont"><div class="choose-line-neme">'+obj.expertstaff[k].staffinformation[0].staff_member_name+'</div><div class="choose-line-experience">'+obj.expertstaff[k].staffinformation[0].experience+' <?php echo $Yrs_Exp; ?></div>'+starreview+'</div><div><div class="choose-line-radio ja"><input id="staffid'+obj.expertstaff[k].staff_id+'" name="staffid" class="workercl staffradio" type="radio" value="'+obj.expertstaff[k].staff_id+'" onclick="workeravaility('+obj.expertstaff[k].staff_id+');"><label for="name">&nbsp;</label><div class="book_input bks"><input name="bookingtime'+obj.expertstaff[k].staff_id+'" type="text" id="bookingtime'+obj.expertstaff[k].staff_id+'" onkeydown="OnKeyDown(event)" autocomplete="off" class="t-box sel-time" placeholder="<?php echo $StaffAvailibilityTime; ?>" onMouseOver="workeravaility('+obj.expertstaff[k].staff_id+');"  onchange="selectedworker('+obj.expertstaff[k].staff_id+');" /></div></div></div></div></div></div>');
      
        }
      jQuery('[name=servicestaffid]').val(servicestaffid);
      }

      packageiteminformation=obj.packageiteminformation.length;
      //alert(packageiteminformation);
      if(packageiteminformation>0){
        for(jk=0; jk<packageiteminformation; jk++)
            {
              TotalPriceaa = getChangedPrice(obj.packageiteminformation[jk].iteminformation[0].pro_price);  
              
              
      jQuery('#packageservices').append('<div>'+obj.packageiteminformation[jk].iteminformation[0].pro_title+' - <?php echo $Cur; ?> '+TotalPriceaa+'</div>');
            }
      }

      productextraimages=obj.extraproductimages.length;

      jQuery("#productextraimages").html("");
      if(productextraimages>0){
        jQuery('#productextraimages').append('<div class="staff_area top_staff_spc"><div class="leftbar_title">Photos</div><div class="choose_staf_box">');
      
        for(z=0; z<productextraimages; z++)
            {
            jQuery('#productextraimages').append('<div class="choose_name"><div class="choose_img"><img src="'+obj.extraproductimages[z].pro_image+'" alt="" /></div></div>');
            }
        jQuery('#productextraimages').append('</div></div>');
      }


      var cdt = jQuery('input[name=bookingtime]');

       var  starttime=jQuery('#opening_time').val();      
               var endtime=jQuery('#closing_time').val();
                   var service_duration=jQuery('#service_duration').val();
                  var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A'); 
        jQuery('#bookingtime').timepicker('remove');      
      
  jQuery('#bookingtime').timepicker({
          'minTime': starttime,
          'maxTime': netime,
          'dynamic': true,
          'timeFormat': 'g:i A',          
          });

    jQuery('html, body').animate({
       scrollTop: (jQuery('.service-display-left').offset().top)
    }, 'slow'); 

           }
           }
        });
    }
 


      
   
  }


function checkgallery(str)
{
  jQuery.ajax({
     type:"GET",
     url:"{{url('getmultipleImages')}}?product_id="+str,
     success:function(res)
     { 
  
     jQuery('.product_gallery').html(res);
     }
   });
 
}
</script>


<script type="text/javascript">
  function workeravaility(wid){   

    var workerid=wid;
      var bookingdate=jql('#bookingdate').val();
var workerbookedslot=[];
 
      if(workerid){

 


        jQuery.ajax({
           type:"GET",
           url:"{{url('beautyshop/workeravailability')}}?staffid="+workerid+"&bookingdate="+bookingdate,
           success:function(res){               
              if(res){
                 var json = JSON.stringify(res);
              var obj = JSON.parse(json);
        
                 //console.log(obj);
                
                  workerbookedslots=obj.workerbookingrecord.length;
                  var produt_service_duration=jQuery('#service_duration').val()-0.5;
                  if(workerbookedslots>0){
                    for(lk=0; lk<workerbookedslots; lk++)
                    {
                    //alert(obj.workerbookingrecord[lk].start_time);
                    var inRange = [];
                    var newstartbookedslot=obj.workerbookingrecord[lk].start_time;
                        
                   var newservicebookingslottime=moment(newstartbookedslot, 'h:mm A').subtract('hours', produt_service_duration).format('h:mm A'); 

                      //inRange.push(obj.workerbookingrecord[lk].start_time.split(" ").slice(0, 3).join(''));
                     // alert(newservicebookingslottime);

                      inRange.push(newservicebookingslottime.split(" ").slice(0, 3).join(''));
                      inRange.push(obj.workerbookingrecord[lk].end_time.split(" ").slice(0, 3).join(''));
                      workerbookedslot.push(inRange);
                     }
                  }else{
                    //jql('#st'+staffid).html('');
                  }
                  console.log(workerbookedslot);
                
             }
        var timeNow = new Date();

        var currhours   = timeNow.getHours();

         var currmins   = timeNow.getMinutes();

         var hours   = timeNow.getHours()+1;

        var ampm = hours >= 12 ? 'pm' : 'am';

if(currmins>30){
         var newstarttime=hours+':'+'00 '+ampm;

}else{


         var newstarttime=currhours+':'+'30 '+ampm;
}


          var todaydate=jQuery('#todaydate').val(); 
          var bookingdate=jQuery('#bookingdate').val();
           var n= bookingdate.split(" ").slice(0, 3).join('');
           var endtime=jQuery('#closing_time').val();  

           var startdbtime=jQuery('#opening_time').val();    

             var service_duration=jQuery('#service_duration').val();
                   var netime=moment(endtime, 'h:mm A').subtract('hours', service_duration).format('h:mm A');   
         
         if(todaydate==n){
              var starttime=newstarttime; 
                  var currentTime= moment();    // e.g. 11:00 pm
                    var mstartTime = moment(startdbtime, "HH:mm a");
                    var mendTime = moment(netime, "HH:mm a");
                   var amIBetween = currentTime.isBetween(mstartTime , mendTime);

                        if(amIBetween===true){
                        jQuery('#bookingtime'+wid).css("display", "block");
                        jQuery('#staffid'+wid).css("display", "block");

                                  
                    jQuery('#bookingtime'+wid).timepicker('remove');      
                    
                  jQuery('#bookingtime'+wid).timepicker({
                        'minTime': starttime,
                        'maxTime': netime,
                        'dynamic': true,
                        'timeFormat': 'g:i A',  
                        'disableTimeRanges': workerbookedslot        
                        }); 
                      }else{
                          jQuery('#bookingtime'+wid).timepicker('remove');
                          jQuery('#bookingtime'+wid).css("display", "none");
                          jQuery('#staffid'+wid).css("display", "none");
                          

                      }



                  }else{
             var starttime=jQuery('#opening_time').val();
                    jQuery('#bookingtime'+wid).css("display", "block");
                        jQuery('#staffid'+wid).css("display", "block");

                  jQuery('#bookingtime'+wid).timepicker('remove');      
                    
                  jQuery('#bookingtime'+wid).timepicker({
                        'minTime': starttime,
                        'maxTime': netime,
                        'dynamic': true,
                        'timeFormat': 'g:i A',  
                        'disableTimeRanges': workerbookedslot        
                        }); 
               }
                






    
          }
        });
    }

  }


</script>






<script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>

<script type="text/javascript">

  var showChar = 400;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

</script>

<script type="text/javascript">
  
jQuery('.paginate').click(function(){
var page = jQuery(this).data('page');
var attribute_id = jQuery(this).data('attribute_id');
var branchid = jQuery(this).data('branchid');
 
if(page!='')
{

    jQuery.ajax({
           type:"GET",
           url:"{{url('newbeautyandeleganceshop')}}?page="+page+"&attribute_id="+attribute_id+"&branchid="+branchid,
           success:function(res){               
            if(res){
        jQuery('.diamond_main_wrapper').html(res);

        }
      }
      });
              

}


})
jQuery(window).bind("load", function() {
   jQuery('.tabIDs1').trigger('click');
});



</script>
 
<link rel="stylesheet" type="text/css" href="{{url('/')}}/themes/js/fancybox/jquery.fancybox-1.3.4.css" />

<link rel="stylesheet" href="{{url('/')}}/themes/js/fancybox/jquery.mThumbnailScroller.css">

<script src="{{url('/')}}/themes/js/fancybox/jquery.mThumbnailScroller.js"></script>

<script type="text/javascript">
  jQuery(document).ready(function() { 
  jQuery("a[rel=example_group]").fancybox({
      'transitionIn'    : 'none',
      'transitionOut'   : 'none',
      'titlePosition'   : 'over',
      'titleFormat'   : function(title, currentArray, currentIndex, currentOpts) {
        return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
      }
    });
    });
</script>

<script>
(function($){
jQuery(window).load(function(){
jQuery("#content-6").mThumbnailScroller({
type:"hover-precise"
});
});
})(jQuery);
</script>

<script type="text/javascript">  
  function selectedworker(workerid){       
    jQuery("#staffid"+workerid).prop("checked", true);
  }
</script>


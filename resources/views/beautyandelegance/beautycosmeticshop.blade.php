@include('includes.navbar')
  @php 
  global $Current_Currency;
  $Current_Currency  = Session::get('currency'); 
  if($Current_Currency =='') 
  { 
	$Current_Currency = 'SAR'; 
  } 
@endphp
<div class="outer_wrapper">
  <div class="inner_wrap">
    <div class="vendor_header">
      <div class="inner_wrap">
        <div class="vendor_header_left">
          <div class="vendor_logo" ><a href="javascript:void(0);"><img src="{{ $fooddateshopdetails[0]->mc_img }}" alt="" style="max-height: 168px;" /></a></div>
        </div>
        <!-- vendor_header_left -->
        @include('includes.vendor_header') </div>
    </div>
    <!-- vemdor_header -->
    <div class="common_navbar">
      <div class="inner_wrap">
        <div id="menu_header" class="content">
          <ul>
            <li><a href="#about_shop" class="active">@if (Lang::has(Session::get('lang_file').'.About_Shop')!= '') {{  trans(Session::get('lang_file').'.About_Shop') }} @else  {{ trans($OUR_LANGUAGE.'.About_Shop') }} @endif</a></li>
            <li><a href="#video">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif</a></li>
			<?php if(count($fooddateshopreview) > 0){ ?>
            <li><a href="#our_client">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</a></li>
			<?php } ?>
            <li><a href="#choose_package">@if (Lang::has(Session::get('lang_file').'.Choose_Package')!= '') {{  trans(Session::get('lang_file').'.Choose_Package') }} @else  {{ trans($OUR_LANGUAGE.'.Choose_Package') }} @endif</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- common_navbar -->
    <div class="inner_wrap service-wrap diamond_space">
      <div class="detail_page"> <a name="about_shop" class="linking">&nbsp;</a>
        <div class="service_detail_row">
          <div class="gallary_detail">
            <section class="slider">
              <div id="slider" class="flexslider">
                <ul class="slides">
                  @foreach($fooddateshopgallery as $shopgallery)
                  @php 
                  $im=$shopgallery->image;
                  $gimg=str_replace('thumb_','',$im);
                  @endphp
                  <li> <img src="{{ $gimg }}" /> </li>
                  @endforeach
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                <ul class="slides">
                  @foreach($fooddateshopgallery as $shopgallerythumb)
                  <li> <img src="{{ $shopgallerythumb->image }}" /> </li>
                  @endforeach
                </ul>
              </div>
            </section>
          </div>
		  @php $cid=$fooddateshopdetails[0]->city_id;
        $getCity = Helper::getcity($cid);
        if(Session::get('lang_file') !='en_lang'){
        $city_name= 'ci_name_ar';
        }else{
        $city_name= 'ci_name'; 
        }  
        
        @endphp
          <div class="service_detail">
            <!-- Example DataTables Card-->
            <div class="detail_title">{{ $fooddateshopdetails[0]->mc_name }}</div>
            <div class="detail_hall_description">{{ $fooddateshopdetails[0]->address }}</div>
            <div class="detail_hall_subtitle">@if (Lang::has(Session::get('lang_file').'.ABOUT_SHOP')!= '') {{  trans(Session::get('lang_file').'.ABOUT_SHOP') }} @else  {{ trans($OUR_LANGUAGE.'.ABOUT_SHOP') }} @endif</div>
            <div class="detail_about_hall">
              <div class="comment more">{{ $fooddateshopdetails[0]->mc_discription }}</div>
            </div>
            <div class="detail_hall_dimention">@if (Lang::has(Session::get('lang_file').'.CITY')!= ''){{trans(Session::get('lang_file').'.CITY')}}@else{{trans($OUR_LANGUAGE.'.CITY')}}@endif: <span>{{ $getCity->$city_name }}</span></div>
             @php if($fooddateshopdetails[0]->google_map_address!=''){  $lat='28.5561912';   $long='77.2512172';    @endphp
          <div class="detail_hall_dimention" id="map" width="450" height="230" style="height: 230px!important;">
         </div>
          @php }  @endphp 
            @php  $homevisitcharges=$fooddateshopdetails[0]->home_visit_charge; @endphp </div>
        </div>
        <!-- service_detail_row -->
        <div class="service-mid-wrapper"> <a name="video" class="linking">&nbsp;</a>
		  <?php if(isset($fooddateshopdetails[0]->mc_video_url) && $fooddateshopdetails[0]->mc_video_url!=''){ ?>
          <div class="service-video-area">
            <div class="service-video-cont">{{ $fooddateshopdetails[0]->mc_video_description }}


            </div>
            <div class="service-video-box">
              <iframe class="service-video" src="{{ $fooddateshopdetails[0]->mc_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>
		  <?php } ?>
          <!-- service-video-area -->
		  <?php if(count($fooddateshopreview) > 0){ ?>
          <div class="service_list_row service_testimonial"> <a name="our_client" class="linking">&nbsp;</a>
            <div class="common_title">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
            <div class="testimonial_slider">
              <section class="slider">
                <div class="flexslider1">
                  <ul class="slides">
                    @foreach($fooddateshopreview as $customerreview)
                    <li>
                      <div class="testimonial_row">
                        <div class="testim_left">
                          <div class="testim_img"><img src="{{ $customerreview->cus_pic }}"></div>
                        </div>
                        <div class="testim_right">
                          <div class="testim_description">{{ $customerreview->comments }}</div>
                          <div class="testim_name">{{ $customerreview->cus_name }}</div>
                          <div class="testim_star"><img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png"></div>
                        </div>
                      </div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </section>
            </div>
          </div>
		  <?php } ?>
        </div>
        <!-- service-mid-wrapper -->
        @php $tbl_field='service_id'; if(count($servicecategoryAndservices)>0){ @endphp
        <div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
          <div class="service_line">
            <div class="service_tabs">
              <div id="content-5" class="content">
                <ul>
                  @php $k=1; @endphp
                  @foreach($servicecategoryAndservices as $categories)
                  @php 
                  if(count($categories->serviceslist)>0){
                  if($k==1){ $cl='select'; $HH = $categories->serviceslist[0]->pro_id; }else{ $cl=''; } 
                  @endphp
                  <li><a href="#{{$k}}" data-tabid="{{$k}}" class="cat {{$cl}}" data-toggle="tab" onclick="getbeautyservice('{{ $categories->serviceslist[0]->pro_id }}','{{ $branchid }}','{{$tbl_field}}','{{$k}}');">{{ $categories->attribute_title }}</a></li>
                  @php $k++; } @endphp
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="service_bdr"></div>
          </div>
        </div>
        <div class="service-display-section"> <a name="choose_package" class="linking">&nbsp;</a>
          <div class="service-display-right">
            <div class="diamond_main_wrapper">
              <div class="diamond_wrapper_outer"> @php  $z=1;     @endphp
                @foreach($servicecategoryAndservices as $productservice)
                @php    $k=count($productservice->serviceslist); 
                if($k >0 ){
                @endphp
                @php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp
                <div class="diamond_wrapper_main tab-pane fade {{ $addactcon }}"  id="{{ $z}}"> @php if($k < 6){ @endphp
                  @php  $i=1;     @endphp
                  <div class="diamond_wrapper_inner "> @foreach($productservice->serviceslist as $getallcats)
                    @php
                      if($k<=3)
                  {
                  $bgImg = str_replace('thumb_','',$getallcats->pro_Img);  
                  }
                  else
                  {
                  $bgImg = $getallcats->pro_Img;  
                  }
                    @endphp
                    <div class="row_{{$i}}of{{$k}} rows{{$k}}row"> <a href="#" onclick="getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                      <div class="category_wrapper" style="background:url({{ $bgImg or '' }});">
                        <div class="category_title">
                          <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                        </div>
                      </div>
                      </a> </div>
                    @php $i=$i+1; @endphp
                    
                    @endforeach </div>
                  <!------------ 6th-------------->
                  @php }elseif($k==6){ @endphp
                  @php $j=1; @endphp
                  <div class="diamond_wrapper_inner"> @foreach($productservice->serviceslist as $getallcats)
                    @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
                    @php if($j==1){ @endphp
                    <div class="row_1of5 rows5row"> @php } @endphp 
                      @php if($j==2){ @endphp
                      <div class="row_2of5 rows5row"> @php } @endphp 
                        @php if($j==3){  @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp 
                          @php if($j==5){ @endphp
                          <div class="row_4of5 rows5row"> @php } @endphp
                            @php if($j==6){ @endphp
                            <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                              <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                                <div class="category_title">
                                  <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                                </div>
                              </div>
                              </a> @php if($j==1){ @endphp
                              <div class="clear"></div>
                            </div>
                            @php } @endphp 
                            @php if($j==2){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($j==4){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($j==5){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp
                      @php if($j==6){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php $j=$j+1; @endphp
                    @endforeach </div>
                  <!------------ 7th-------------->
                  @php }elseif($k==7){ @endphp
                  @php $l=1; @endphp
                  <div class="diamond_wrapper_inner"> @foreach($productservice->serviceslist as $getallcats)
                    @php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
                    
                    @php if($l==1){ @endphp
                    <div class="row_1of5 rows5row"> @php } @endphp 
                      @php if($l==2){ @endphp
                      <div class="row_2of5 rows5row"> @php } @endphp 
                        @php if($l==3){  @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp 
                          @php if($l==6){ @endphp
                          <div class="row_4of5 rows5row"> @php } @endphp
                            @php if($l==7){ @endphp
                            <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                              <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                                <div class="category_title">
                                  <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                                </div>
                              </div>
                              </a> @php if($l==1){ @endphp
                              <div class="clear"></div>
                            </div>
                            @php } @endphp 
                            @php if($l==2){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==5){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==6){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp
                      @php if($l==7){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php $l=$l+1; @endphp
                    @endforeach </div>
                  <!------------ 8th-------------->
                  @php }elseif($k==8){ @endphp
                  @php $l=1; @endphp
                  <div class="diamond_wrapper_inner"> @foreach($productservice->serviceslist as $getallcats)
                    @php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
                    @php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
                    @php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
                    
                    @php if($l==1){ $classrd='category_wrapper1'; @endphp
                    <div class="row_1of5 rows5row"> @php } @endphp 
                      @php if($l==2){ @endphp
                      <div class="row_3of5 rows5row"> @php } @endphp 
                        @php if($l==4){  @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp 
                          @php if($l==6){ @endphp
                          <div class="row_3of5 rows5row"> @php } @endphp
                            @php if($l==8){ $classrd='category_wrapper9'; @endphp
                            <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
                              <div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
                                <div class="category_title">
                                  <div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div>
                                </div>
                              </div>
                              </a> @php if($l==1){ @endphp
                              <div class="clear"></div>
                            </div>
                            @php } @endphp 
                            @php if($l==3){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($l==5){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($l==7){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp
                      @php if($l==8){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp
                    @php $l=$l+1; @endphp
                    @endforeach </div>
                  <!---------- 9th ------------------->
                  @php }elseif($k>=9){ @endphp
                  <div class="diamond_wrapper_inner"> @php $i=1; @endphp
                    @foreach($productservice->serviceslist as $getallcats)
                    @php if($i==1) { $k=9; }else{ $k=$i;} 
                    if($i<=9){
                    @endphp
                    
                    
                    @php if($i==1){ @endphp
                    <div class="row_1of5 rows5row"> @php } @endphp 
                      @php if($i==2){ @endphp
                      <div class="row_2of5 rows5row"> @php } @endphp 
                        @php if($i==4){ @endphp
                        <div class="row_3of5 rows5row"> @php } @endphp 
                          @php if($i==7){ @endphp
                          <div class="row_4of5 rows5row"> @php } @endphp 
                            @php if($i==9){ @endphp
                            <div class="row_5of5 rows5row"> @php } @endphp <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');"> <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});"> <span class="category_title"><span class="category_title_inner">{{ $getallcats->pro_title or ''}}</span></span> </span> </a> @php if($i==1){ @endphp
                              <div class="clear"></div>
                            </div>
                            @php } @endphp 
                            @php if($i==3){ @endphp
                            <div class="clear"></div>
                          </div>
                          @php } @endphp 
                          @php if($i==6){ @endphp
                          <div class="clear"></div>
                        </div>
                        @php } @endphp 
                        @php if($i==8){ @endphp
                        <div class="clear"></div>
                      </div>
                      @php } @endphp 
                      @php if($i==9){ @endphp
                      <div class="clear"></div>
                    </div>
                    @php } @endphp 
                    
                    @php $i=$i+1; } @endphp
                    @endforeach </div>
                  @php } @endphp </div>
                @php $z=$z+1; }@endphp
                @endforeach </div>
            </div>
            <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
            @php  $PP=1; $jk=0; @endphp
            @foreach($servicecategoryAndservices as $productservice)
            @php 
            $TotalPage = $productservice->serviceslist->count(); 
            if($TotalPage > 9)
            {
            $paginations = ceil($TotalPage/9);
            }
            else
            {
            $paginations =0;
            }
            if(count($productservice->serviceslist) < 1) {continue; }
            @endphp
            
            @if($paginations!=0)
            <div class="pagnation-area" id="tab{{$PP}}" style="display: none;">
              <ul class="pagination">
                <li class="disabled"><span>«</span></li>
                @for($KO=1;$KO<=$paginations;$KO++)
                <li class="@if($KO==1) active @endif paginate" data-page="{{$KO}}" data-attribute_id="{{ $productservice->id }}"  data-branchid="{{ $branchid }}" data-tblfield="{{$tbl_field}}" data-tabid="{{$PP}}"><span>{{$KO}}</span></li>
                @endfor
                <li><a href="#?page={{$paginations}}" rel="next">»</a></li>
              </ul>
            </div>
            @endif
            
            @php  $PP=$PP+1; $jk++; @endphp
            @endforeach </div>
          <!-- service-display-right -->
          {!! Form::open(['url' => 'beautycosmeticshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
          <!-- container-section -->
          <div class="service-display-left"> <span id="selectedproduct">
            <div class="service-left-display-img product_gallery">
   @php    $pro_id = $beautyshopleftproduct->pro_id; @endphp
             @include('includes/product_multiimages')
            

            </div>
            <div class="service-product-name">{{ $beautyshopleftproduct->pro_title }}</div>
            <div class="service-beauty-description">{{ $beautyshopleftproduct->pro_desc }}</div>
            <div class="beauty-prise"> @php if($beautyshopleftproduct->pro_discount_percentage>0){ $productprice= $beautyshopleftproduct->pro_price*(100-$beautyshopleftproduct->pro_discount_percentage)/100;}else{ $productprice='';}
              if($productprice==''){ @endphp <span> {{   currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency) }} </span> @php }else{ @endphp <span class="strike">{{   currency($beautyshopleftproduct->pro_price, 'SAR',$Current_Currency) }} </span><span> {{   currency($productprice, 'SAR',$Current_Currency) }} </span> @php  } @endphp </div>
            </span>
            <div class="service-radio-line" id="qtyop">
              <div class="service_quantity_box" id="service_quantity_box">
                <div class="service_qunt">@if(Lang::has(Session::get('lang_file').'.QUANTITY')!= '') {{ trans(Session::get('lang_file').'.QUANTITY')}}  @else {{ trans($OUR_LANGUAGE.'.QUANTITY')}} @endif</div>
                <div class="service_qunatity_row">
                  <div class="td td2 quantity food-quantity" data-title="Total Quality">
                    <div class="quantity">
                      <button type="button" id="sub" class="sub" onClick="return pricecalculation('remove');" ></button>
                      <input type="number" name="itemqty" id="qty" value="1" min="1" max="9" readonly onkeyup="isNumberKey(event); pricecalculation('pricewithqty');" onkeydown="isNumberKey(event); pricecalculation('pricewithqty');" />
                      <button type="button" id="add" class="add" onClick="return pricecalculation('add');"></button>
                    </div>
                    <label for="qty" id="errorqty" class="error"></label>
                  </div>
                </div>
              </div>
              <span id="maxqty" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
            </div>
            <span id="addtocartprice" >
            <div class="container_total_price foods-totals"> @php if($productprice!=''){ $tprice=$productprice; }else{ $tprice=$beautyshopleftproduct->pro_price; } @endphp
              @if (Lang::has(Session::get('lang_file').'.Total_Price')!= ''){{trans(Session::get('lang_file').'.Total_Price')}}@else{{ trans($OUR_LANGUAGE.'.Total_Price')}}@endif: <span class="cont_final_price" id="cont_final_price">{{   currency($tprice, 'SAR',$Current_Currency) }} </span></div>
            </span> @php if($productprice>0){ $totalprice=$productprice;}else{$totalprice=$beautyshopleftproduct->pro_price;} @endphp
			
            <div class="btn_row">
              <input type="hidden" name="category_id" value="{{ $subsecondcategoryid}}">
              <input type="hidden" name="subsecondcategoryid" value="{{ $category_id }}">
              <input type="hidden" name="cart_sub_type" value="makeup">
              <input type="hidden" name="branch_id" value="{{ $branchid }}">
              <input type="hidden" name="shop_id" value="{{ $shop_id }}">
              <input type="hidden" name="vendor_id" value="{{ $beautyshopleftproduct->pro_mr_id }}">
              <input type="hidden" name="product_id" id="product_id" value="{{ $beautyshopleftproduct->pro_id }}">
              <input type="hidden" name="pro_qty" id="pro_qty" value="{{ $beautyshopleftproduct->pro_qty }}">
              
              <input type="hidden" name="product_price" id="product_price" value="{{currency(number_format($totalprice,2), 'SAR',$Current_Currency, $format = false) }}">
              <input type="hidden" name="product_orginal_price" id="product_orginal_price" value="{{currency($totalprice, 'SAR',$Current_Currency, $format = false) }}">
              <input type="submit" name="submit" id="sbtn" value="{{ (Lang::has(Session::get('lang_file').'.ADD_TO_CART')!= '')  ?  trans(Session::get('lang_file').'.ADD_TO_CART'): trans($OUR_LANGUAGE.'.ADD_TO_CART')}}" class="form-btn">
            </div>
			<div class="terms_conditions"><a href="{{ $fooddateshopdetails[0]->terms_conditions }}" target="_blank">{{ (Lang::has(Session::get('lang_file').'.Terms_Conditions')!= '')  ?  trans(Session::get('lang_file').'.Terms_Conditions'): trans($OUR_LANGUAGE.'.Terms_Conditions')}}<a class="diamond-ancar-btn" href="#choose_package"><img src="{{url('/themes/images/service-up-arrow.png')}}" alt=""></a> </div>
          </div>
          @php } @endphp
          <!-- container-section -->
        </div>
        <!-- service-display-left -->
      </div>
      {!! Form::close() !!} </div>
    <!-- service-display-left -->
  </div>
  <!--service-display-section-->
  <!--service-display-section-->
  @php
  $sesionData = session()->all();
  if(isset($sesionData) && $sesionData!='')
  {
  $url = url('search-results?weddingoccasiontype='.$sesionData['searchdata']['weddingoccasiontype'].'&noofattendees='.$sesionData['searchdata']['noofattendees'].'&budget='.$sesionData['searchdata']['budget'].'&cityid='.$sesionData['searchdata']['cityid'].'&occasiondate='.$sesionData['searchdata']['occasiondate'].'&gender='.$sesionData['searchdata']['gender'].'&basecategoryid='.$sesionData['searchdata']['mainselectedvalue'].'&maincategoryid='.$sesionData['searchdata']['maincategoryid'].'&mainselectedvalue='.$sesionData['searchdata']['mainselectedvalue']); 
  }        
  @endphp
  <div class="sticky_other_service"> <a href="{{$url}}">
    <div class="sticky_serivce">@if(Lang::has(Session::get('lang_file').'.OTHER_SERVICE')!= '') {{ trans(Session::get('lang_file').'.OTHER_SERVICE')}}  @else {{ trans($OUR_LANGUAGE.'.OTHER_SERVICE')}} @endif</div>
    <div class="sticku_service_logo"><img src="{{ url('') }}/themes/images/logo.png"></div>
    </a> </div>
  <!-- other_serviceinc -->
  <!-- other_serviceinc -->
</div>
<!-- detail_page -->
</div>
</div>
<!-- outer_wrapper -->
</div>
@include('includes.footer')
@include('includes.popupmessage') 
<script language="javascript">
  function getChangedPrice(price)
  {
  var UpdatedPrice = '';
  $.ajax({
     async: false,
     type:"GET",
     url:"{{url('getChangedprice')}}?price="+price,
     success:function(res)
     {  
      UpdatedPrice =res;
     }
   });
  return UpdatedPrice;
  }

$('.add').click(function () {

  var proqty=document.getElementById('pro_qty').value;
  var currentvalue=parseInt($(this).prev().val());
		if (currentvalue < proqty) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}else{
       //jQuery('#maxqty').css("display", "block");
       $('.action_popup').fadeIn(500);
         $('.overlay').fadeIn(500);
         $('#showmsg').show();
         $('#hidemsgab').hide();
         $('#showmsgab').show();

    }
});
$('.sub').click(function () {
   //jQuery('#maxqty').css("display", "none");
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});

</script>
<script type="text/javascript">
	 function pricecalculation(act){
	 		
							
							var no=1;
							
							var currentquantity=document.getElementById('qty').value;
							var unititemprice=document.getElementById('product_orginal_price').value;
							var totalproqty=document.getElementById('pro_qty').value;

							if(currentquantity<1){
								document.getElementById('qty').value=1;
								 var qty= parseInt(no);
							}else{
							if(act=='pricewithqty'){
								var qty=parseInt(currentquantity)
							}
							if(act=='add'){
									 var qty= parseInt(currentquantity)+parseInt(no);
								 }
							if(act=='remove'){ 
									if(parseInt(currentquantity)==1){
								      var qty=parseInt(currentquantity)
								   }else{
								      var qty=parseInt(currentquantity)-parseInt(no);
									}

							 }
							}
              if(totalproqty<qty){
                var orderedqty=qty-1;
              }else{
                 var orderedqty=qty;
              }
							var producttotal=orderedqty*unititemprice;
							//alert(producttotal);
							jQuery('[name=product_price]').val(producttotal);
							var fprice=producttotal.toFixed(2);

							 <?php $Cur = Session::get('currency'); ?>
							document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur;?> '+fprice;
							
							
					
	 }

</script>

<!------------------ tabs----------------->
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
</script>
<script src="{{url('/')}}/themes/js/timepicker/jquery-clockpicker.min.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery-clockpicker.min.css" rel="stylesheet" />
<!------------ end tabs------------------>
<script type="text/javascript">
function checkgallery(str)
{
  jQuery.ajax({
     type:"GET",
     url:"{{url('getmultipleImages')}}?product_id="+str,
     success:function(res)
     { 
      jQuery('.product_gallery').html(res);
     }
   });
 
}

	function getbeautyservice(selecteddateproduct,branchid,tbl,Ks){
		if(Ks!=undefined)
			{
			jQuery('.pagnation-area').hide()
			jQuery('#tab'+Ks).show()
			}
		   var Add_to_Cart = "@if (Lang::has(Session::get('lang_file').'.Add_to_Cart')!= '') {{  trans(Session::get('lang_file').'.Add_to_Cart') }} @else  {{ trans($MER_OUR_LANGUAGE.'.Add_to_Cart') }} @endif";

       var SOLDOUT = "@if (Lang::has(Session::get('lang_file').'.SOLD_OUT')!= '') {{  trans(Session::get('lang_file').'.SOLD_OUT') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SOLD_OUT') }} @endif";
      jQuery('#errorqty').html('');
		var dateproductid=selecteddateproduct;

			  if(selecteddateproduct){
        jQuery.ajax({
           type:"GET",
           url:"{{url('beautycosmeticshop/getcartproduct')}}?product_id="+dateproductid+"&branchid="+branchid+"&tblfld="+tbl,
           success:function(res){               
            if(res){
            	 var json = JSON.stringify(res);
			           var obj = JSON.parse(json);
			//alert(obj);
             <?php $Cur = Session::get('currency'); ?>
            	
            		length=obj.productsericeinfo.length;
            		//alert(length);
            		
            		if(length>0){
		 for(i=0; i<length; i++)
			{
				if(obj.productprice==obj.productsericeinfo[i].pro_price){
					TotalPrices = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					var productrealprice='<span><?php echo $Cur; ?> '+TotalPrices+'</span>';
					var productrealpriceamount= parseFloat(TotalPrices).toFixed(2);
          TotalPricesss = getChangedPrice(obj.TotalPrices); 
				}else{
					TotalPrices = getChangedPrice(obj.productsericeinfo[i].pro_price); 
					TotalPricesss = getChangedPrice(obj.productprice); 
					var productrealprice='<span class="strike"><?php echo $Cur; ?> '+TotalPrices+' </span><span> <?php echo $Cur; ?> '+TotalPricesss+'</span>';
					var productrealpriceamount=parseFloat(TotalPricesss).toFixed(2);
				}
        var qty=parseInt(obj.productsericeinfo[i].pro_qty);

         if(qty<1){
          $("#sbtn").val(SOLDOUT);
          $('#qtyop').css('display','none');
          $(':input[type="submit"]').prop('disabled', true);
         }else{
          $("#sbtn").val(Add_to_Cart);
          $('#qtyop').css('display','block');
          $(':input[type="submit"]').prop('disabled', false);
         }

				jQuery('#selectedproduct').html('<div class="service-left-display-img product_gallery"></div><div class="service-product-name">'+obj.productsericeinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productsericeinfo[i].pro_desc+'</div><div class="beauty-prise">'+productrealprice+'</div><span id="productextraimages"></span></div>');
        checkgallery(selecteddateproduct);
				jQuery('[name=product_id]').val(obj.productsericeinfo[i].pro_id);
				jQuery('[name=itemqty]').val(1);
        document.getElementById('cont_final_price').innerHTML = '<?php echo $Cur; ?> '+productrealpriceamount;
        jQuery('[name=pro_qty]').val(qty);
				jQuery('[name=product_price]').val(productrealpriceamount);
				jQuery('[name=product_orginal_price]').val(productrealpriceamount);
				jQuery('#totalprice').html(parseFloat(TotalPricesss).toFixed(2));

				
			}
			
			}

			jQuery('html, body').animate({
       scrollTop: (jQuery('.service-display-left').offset().top)
    }, 'slow');



           }



           }
        });
    }


		
	}

</script>
<script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">

  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

</script>
<script type="text/javascript">
	
jQuery('.paginate').click(function(){
var page = jQuery(this).data('page');
var attribute_id = jQuery(this).data('attribute_id');
var branchid = jQuery(this).data('branchid');
 
if(page!='')
{

    jQuery.ajax({
           type:"GET",
           url:"{{url('newbeautycosmaticshop')}}?page="+page+"&attribute_id="+attribute_id+"&branchid="+branchid,
           success:function(res){               
            if(res){
				jQuery('.diamond_main_wrapper').html(res);

			  }
			}
			});
            	

}


})


<?php if(isset($HH) && $HH!=''){ ?>
jQuery(window).load(function(){ 
  
  getbeautyservice('<?php echo $HH;?>','<?php echo $branchid; ?>','service_id','1');
})

<?php } ?>
</script>


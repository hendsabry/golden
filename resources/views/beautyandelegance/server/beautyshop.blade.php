@include('includes.navbar')
<div class="outer_wrapper">
<div class="inner_wrap">
<div class="vendor_header">
	<div class="inner_wrap">                
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{ $fooddateshopdetails[0]->mc_img }}" alt="" /></a></div>                      	
        </div> <!-- vendor_header_left -->
        
       @include('includes.vendor_header') <!-- vendor_header_right -->
	</div>
</div><!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">

       <ul>
        <li><a href="#about_shop" class="active">@if (Lang::has(Session::get('lang_file').'.About_Shop')!= '') {{  trans(Session::get('lang_file').'.About_Shop') }} @else  {{ trans($OUR_LANGUAGE.'.About_Shop') }} @endif</a></li>
        <li><a href="#video">@if (Lang::has(Session::get('lang_file').'.Video')!= '') {{  trans(Session::get('lang_file').'.Video') }} @else  {{ trans($OUR_LANGUAGE.'.Video') }} @endif</a></li>
        <li><a href="#our_client">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</a></li>
        <li><a href="#choose_package">@if (Lang::has(Session::get('lang_file').'.Choose_Package')!= '') {{  trans(Session::get('lang_file').'.Choose_Package') }} @else  {{ trans($OUR_LANGUAGE.'.Choose_Package') }} @endif</a></li>
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->




 
<div class="inner_wrap service-wrap diamond_space">
<div class="detail_page">	
<a name="about_shop" class="linking">&nbsp;</a>
    <div class="service_detail_row">
    	<div class="gallary_detail">
        	<section class="slider">
        		<div id="slider" class="flexslider">
          <ul class="slides">
		  @foreach($fooddateshopgallery as $shopgallery)
		   @php 
		   	$im=$shopgallery->image;
		   $gimg=str_replace('thumb_','',$im);
		    @endphp
             <li>
  	    	    <img src="{{ $gimg }}" />
  	    		</li>
			@endforeach
  	    		 	    		
          </ul>
        </div>
        		<div id="carousel" class="flexslider">
          <ul class="slides">
           @foreach($fooddateshopgallery as $shopgallerythumb)

             <li>
  	    	    <img src="{{ $shopgallerythumb->image }}" />
  	    		</li>
			@endforeach
  	    		
           
          </ul>
        </div>
      		</section>
            
        </div>
        
        <div class="service_detail">
        	<!-- Example DataTables Card-->
        	@php $cid=$fooddateshopdetails[0]->city_id;
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 @endphp
	   
        	<div class="detail_title">{{ $fooddateshopdetails[0]->mc_name }}</div>
            <div class="detail_hall_description">{{ $fooddateshopdetails[0]->address }}</div>
            <div class="detail_hall_subtitle">About Shop</div>
            <div class="detail_about_hall">
            	<div class="comment more">{{ $fooddateshopdetails[0]->mc_discription }}</div>
            </div>
              <div class="detail_hall_dimention">City: <span>{{ $getCity->$city_name }}</span></div>
   @php  $homevisitcharges=$fooddateshopdetails[0]->home_visit_charge; @endphp
@php if($fooddateshopdetails[0]->google_map_address!=''){ @endphp
   <div class="detail_hall_dimention"><iframe src="{{ $fooddateshopdetails[0]->google_map_address }}" width="450" height="230" frameborder="0" style="border:0" allowfullscreen></iframe></div>
 @php } @endphp
             
        </div> 
        
    </div> <!-- service_detail_row -->
    
    
	      <div class="service-mid-wrapper">
		         	<a name="video" class="linking">&nbsp;</a>
        
        <div class="service-video-area">
         <div class="service-video-cont">{{ $fooddateshopdetails[0]->mc_video_description }}</div>
          <div class="service-video-box">
		  
		  
            <iframe class="service-video" src="{{ $fooddateshopdetails[0]->mc_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
        <!-- service-video-area -->
        
         
           
           <div class="service_list_row service_testimonial">
        	<a name="our_client" class="linking">&nbsp;</a>
          <div class="common_title">@if (Lang::has(Session::get('lang_file').'.What_Our_Client_Says')!= '') {{  trans(Session::get('lang_file').'.What_Our_Client_Says') }} @else  {{ trans($OUR_LANGUAGE.'.What_Our_Client_Says') }} @endif</div>
          <div class="testimonial_slider">
            <section class="slider">
              <div class="flexslider1">
                          <ul class="slides">
						  @foreach($fooddateshopreview as $customerreview)
                            <li>
                            	<div class="testimonial_row">
                                	<div class="testim_left"><div class="testim_img"><img src="{{ $customerreview->cus_pic }}"></div></div>
                                    <div class="testim_right">
                                    	<div class="testim_description">{{ $customerreview->comments }}</div>
                                    	<div class="testim_name">{{ $customerreview->cus_name }}</div>
                                        <div class="testim_star"><img src="{{url('/')}}/themes/images/star{{ $customerreview->ratings }}.png"></div>
                                    </div>
                                </div>
                            </li>
					 @endforeach
                           
                          </ul>
                        </div>
            </section>
          </div>
        </div>    
          </div> <!-- service-mid-wrapper -->
	





@php if($category_id==21){ $tbl_field='service_id'; }else{ $tbl_field='shop_id'; } @endphp

	<div class="service_bottom"><a name="kosha" class="linking">&nbsp;</a>
        <div class="service_line">
          <div class="service_tabs">
            <div id="content-5" class="content">
              <ul>
                
                @php $k=1; @endphp
                	@foreach($servicecategoryAndservices as $categories)
                	@php 
                		if(count($categories->serviceslist)>0){
                		if($k==1){ $cl='select'; }else{ $cl=''; } 
                	@endphp
                <li><a href="#{{$k}}"  class="cat {{$cl}}" data-toggle="tab" onclick="getbeautyservice('{{ $categories->serviceslist[0]->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">{{ $categories->attribute_title }}</a></li>
                @php $k++; } @endphp
                @endforeach
                @php if(count($shopavailablepackage)>0){ @endphp
                <li><a href="#100" class="cat" data-toggle="tab" onclick="getbeautyservice('{{ $shopavailablepackage[0]->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">Package</a></li>
                @php } @endphp
              </ul>
            </div>
          </div>
          <div class="service_bdr"></div>
        </div>
      </div>
	
	<div class="service-display-section">
    <a name="choose_package" class="linking">&nbsp;</a>
	<div class="service-display-right">
	
			<div class="diamond_main_wrapper">
	  <div class="diamond_wrapper_outer">
		
		@php  $z=1;     @endphp
			@foreach($servicecategoryAndservices as $productservice)
			@php    $k=count($productservice->serviceslist); 
						if($k >0 ){
			 @endphp
			  @php if($z==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp 
		<div class="diamond_wrapper_main tab-pane fade {{ $addactcon }}"  id="{{ $z}}">
			
			
			
		
			@php if($k < 6){ @endphp
			@php  $i=1;     @endphp
			<div class="diamond_wrapper_inner ">
			@foreach($productservice->serviceslist as $getallcats)
									<div class="row_{{$i}}of{{$k}} rows{{$k}}row">
					  <a href="#" onclick="getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}}  </div></div>
						</div>
					</a>
				</div>
				 @php $i=$i+1; @endphp
				  
				@endforeach
				</div> 
				<!------------ 6th-------------->
				@php }elseif($k==6){ @endphp
				@php $j=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($productservice->serviceslist as $getallcats)
			 @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
			 @php if($j==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($j==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($j==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($j==5){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($j==6){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					<a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($j==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($j==4){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==5){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($j==6){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $j=$j+1; @endphp
				@endforeach
				</div>
				<!------------ 7th-------------->
				@php }elseif($k==7){ @endphp
				@php $l=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($productservice->serviceslist as $getallcats)
				@php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
			
			 @php if($l==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($l==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($l==7){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					 <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==6){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!------------ 8th-------------->
				@php }elseif($k==8){ @endphp
				@php $l=1; @endphp
			
					<div class="diamond_wrapper_inner">
					@foreach($productservice->serviceslist as $getallcats)
				@php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
				@php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
				@php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
				
			 @php if($l==1){ $classrd='category_wrapper1'; @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_3of5 rows5row"> @php } @endphp 
			 @php if($l==4){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_3of5 rows5row"> @php } @endphp
			  @php if($l==8){ $classrd='category_wrapper9'; @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					  <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==8){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!---------- 9th ------------------->
		@php }elseif($k==9){ @endphp
		
		
					<div class="diamond_wrapper_inner">
	  			@php $i=1; @endphp
		  @foreach($productservice->serviceslist as $getallcats)
		  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
		  

		 @php if($i==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		  @php if($i==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
		   @php if($i==4){ @endphp <div class="row_3of5 rows5row"> @php } @endphp 
		    @php if($i==7){ @endphp  <div class="row_4of5 rows5row"> @php } @endphp 
			@php if($i==9){ @endphp  <div class="row_5of5 rows5row"> @php } @endphp 
            <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
              <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});">
                <span class="category_title"><span class="category_title_inner">{{ $getallcats->pro_title or ''}}</span></span>
              </span>
            </a>
		 @php if($i==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($i==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($i==6){ @endphp <div class="clear"></div> </div> @php } @endphp 
		    @php if($i==8){ @endphp <div class="clear"></div></div> @php } @endphp 
			@php if($i==9){ @endphp  <div class="clear"></div> </div> @php } @endphp 
		 
		    @php $i=$i+1; @endphp
		   @endforeach 
	  
		  
        </div>
		
		
		
				
			@php } @endphp
				 	
				
		</div>
		@php $z=$z+1; }@endphp
		@endforeach 
		
		<!-------------- package--------->



		<div class="diamond_wrapper_main tab-pane fade"  id="100">
			
			
			
		
			@php 
				$k=count($shopavailablepackage);
			if($k < 6){ @endphp
			@php  $i=1;     @endphp
			<div class="diamond_wrapper_inner ">
			@foreach($shopavailablepackage as $getallcats)
									<div class="row_{{$i}}of{{$k}} rows{{$k}}row">
					  <a href="#" onclick="getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}}  </div></div>
						</div>
					</a>
				</div>
				 @php $i=$i+1; @endphp
				  
				@endforeach
				</div> 
				<!------------ 6th-------------->
				@php }elseif($k==6){ @endphp
				@php $j=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($shopavailablepackage as $getallcats)
			 @php if($j==3 || $j==4){ $classrd='category_wrapper2';  }else{ $classrd=''; } @endphp
			 @php if($j==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($j==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($j==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($j==5){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($j==6){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					<a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($j==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($j==4){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($j==5){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($j==6){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $j=$j+1; @endphp
				@endforeach
				</div>
				<!------------ 7th-------------->
				@php }elseif($k==7){ @endphp
				@php $l=1; @endphp
			<div class="diamond_wrapper_inner">
			@foreach($shopavailablepackage as $getallcats)
				@php if($l==3 || $l==4 || $l==5){ $seven=$l+1; $classrd='category_wrapper'.$seven;  }else{ $classrd=''; } @endphp
			
			 @php if($l==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
			 @php if($l==3){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_4of5 rows5row"> @php } @endphp
			  @php if($l==7){ @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					 <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==2){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==6){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!------------ 8th-------------->
				@php }elseif($k==8){ @endphp
				@php $l=1; @endphp
			
					<div class="diamond_wrapper_inner">
					@foreach($shopavailablepackage as $getallcats)
				@php if($l==2 || $l==4){  $classrd='category_wrapper2';  } @endphp
				@php if($l==3 || $l==5){  $classrd='category_wrapper3';  } @endphp
				@php if($l==6 || $l==7){ $eight=$l+1; $classrd='category_wrapper'.$eight;  } @endphp
				
			 @php if($l==1){ $classrd='category_wrapper1'; @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		     @php if($l==2){ @endphp  <div class="row_3of5 rows5row"> @php } @endphp 
			 @php if($l==4){  @endphp <div class="row_3of5 rows5row"> @php } @endphp 
			  @php if($l==6){ @endphp <div class="row_3of5 rows5row"> @php } @endphp
			  @php if($l==8){ $classrd='category_wrapper9'; @endphp <div class="row_5of5 rows5row"> @php } @endphp 		
					  <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
						<div class="category_wrapper {{ $classrd }}" style="background:url({{ $getallcats->pro_Img or '' }});">
							<div class="category_title"><div class="category_title_inner">{{ $getallcats->pro_title or ''}} </div></div>
						</div>
					</a>
			@php if($l==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($l==5){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($l==7){ @endphp <div class="clear"></div> </div> @php } @endphp
		  @php if($l==8){ @endphp <div class="clear"></div> </div> @php } @endphp
				@php $l=$l+1; @endphp
				@endforeach
				</div>
				<!---------- 9th ------------------->
		@php }elseif($k==9){ @endphp
		
		
					<div class="diamond_wrapper_inner">
	  			@php $i=1; @endphp
		  @foreach($shopavailablepackage as $getallcats)
		  @php if($i==1) { $k=9; }else{ $k=$i;} @endphp
		  

		 @php if($i==1){ @endphp <div class="row_1of5 rows5row"> @php } @endphp 
		  @php if($i==2){ @endphp  <div class="row_2of5 rows5row"> @php } @endphp 
		   @php if($i==4){ @endphp <div class="row_3of5 rows5row"> @php } @endphp 
		    @php if($i==7){ @endphp  <div class="row_4of5 rows5row"> @php } @endphp 
			@php if($i==9){ @endphp  <div class="row_5of5 rows5row"> @php } @endphp 
            <a href="#" onclick="return getbeautyservice('{{ $getallcats->pro_id }}','{{ $branchid }}','{{$tbl_field}}');">
              <span class="category_wrapper category_wrapper{{$k}}" style="background:url({{ $getallcats->pro_Img or '' }});">
                <span class="category_title"><span class="category_title_inner">{{ $getallcats->pro_title or ''}}</span></span>
              </span>
            </a>
		 @php if($i==1){ @endphp <div class="clear"></div> </div> @php } @endphp 
		  @php if($i==3){ @endphp <div class="clear"></div> </div> @php } @endphp 
		   @php if($i==6){ @endphp <div class="clear"></div> </div> @php } @endphp 
		    @php if($i==8){ @endphp <div class="clear"></div></div> @php } @endphp 
			@php if($i==9){ @endphp  <div class="clear"></div> </div> @php } @endphp 
		 
		    @php $i=$i+1; @endphp
		   @endforeach 
	  
		  
        </div>
		
		
		
				
			@php } @endphp
				 	
				
		</div>




		<!------- end package------------->

		
		
	  </div>
  </div>
  <div class="diamond_shadow">{{ $fooddateshopproducts->links() }}</span></div>
  <div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>
	
	</div> <!-- service-display-right -->

	 {!! Form::open(['url' => 'beautyshop/addcartproduct', 'method' => 'post', 'name'=>'cartfrm', 'id'=>'cartfrm', 'enctype' => 'multipart/form-data']) !!}
	<!-- container-section -->
	<div class="service-display-left" id="selectedproduct">
          <div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="{{ $beautyshopleftproduct->pro_Img }}" alt="" /></div>
          <div class="service-product-name">{{ $beautyshopleftproduct->pro_title }}</div>
          <div class="service-beauty-description">{{ $beautyshopleftproduct->pro_desc }}</div>
          <div class="container-section">
            <div class="duration">Duration <span>{{ $beautyshopleftproduct->service_hour }} Hrs</span>
            		
            </div>
            
            <div class="beauty-prise">
            		@php if($beautyshopleftproduct->pro_discount_percentage!=''){ $productprice= $beautyshopleftproduct->pro_price*(100-$beautyshopleftproduct->pro_discount_percentage)/100;}else{ $productprice='';}
            		if($productprice==''){ @endphp
             <span>SAR {{ $beautyshopleftproduct->pro_price }}</span>
             	@php }else{ @endphp
             	 <span class="strike">SAR {{ $beautyshopleftproduct->pro_price }} </span><span> SAR {{ number_format($productprice,2) }}</span>

           @php  } @endphp
         </div>
          </div>
        
          <!-- container-section -->
        </div>


        <div class="service-display-left">
          <div class="leftbar_title">Book an Appointment For</div>
          <div class="books_form">
            <div class="books_male_area">
              <div class="books_male_left">
                <input id="Home" name="appointmentfor" type="radio" value="home" Onchange="addextracharges({{$homevisitcharges}},'add');">
				<label for="Home">Home </label>
              </div>
              <div class="books_male_left">
                <input id="Shop" name="appointmentfor" type="radio" value="shop" Onchange="addextracharges({{$homevisitcharges}},'remove');">
                <label for="Shop">Shop</label>
              </div>
<label for="appointmentfor" class="error"></label>
					
            </div><div id="shopaddr" style="display: none;">{{ $fooddateshopdetails[0]->address }}</div>
            <div class="books_male_area">
              <div class="book_label">Date</div>
              <div class="book_input">
                <input name="bookingdate" type="text"  id="bookingdate" autocomplete="off" class="t-box cal-t" />
              </div>
            </div>
            <div class="books_male_area">
              <div class="book_label">Time</div>
              <div class="book_input">
                
                	<input name="bookingtime" type="text" id="bookingtime" autocomplete="off" class="t-box cal-t" />
                 
              </div>
            </div>
          </div>
          <span id="ourstaff"> 
          	@php 
          	if(isset($servicestaffexpert) && $servicestaffexpert!=''){ @endphp
          @php  if(count($servicestaffexpert)>0){ @endphp
          <div class="staff_area" id="staffarea">
            <div class="leftbar_title">Choose Staff</div>
			<div class="choose-line-section">

				@foreach($servicestaffexpert as $staffandreview)
				@php 
				$noofreviews=0;
				if(isset($staffandreview->staffreviews) && $staffandreview->staffreviews!=''){
				 $noofreviews=sizeof($staffandreview->staffreviews); 
				}
					$starrating=0;
					if(isset($staffandreview->staffratingsavg) && $staffandreview->staffratingsavg!=''){
						$starrating=ceil($staffandreview->staffratingsavg); 
						}	
						
						
			
				 @endphp
			<div class="choose-line">
			<div class="choose-line-img"><img src="{{$staffandreview->staffinformation[0]->image or ''}}" alt="" /></div> 
			<div class="choose-line-cont">
			<div class="choose-line-neme">{{$staffandreview->staffinformation[0]->staff_member_name or ''}}</div> 
			<div class="choose-line-experience"> {{$staffandreview->staffinformation[0]->experience or ''}} yrs <span>exp</span></div>
			<div class="choose-line-review">
				@php if($starrating>0){ @endphp
				<img src="{{url('/')}}/themes/images/star{{ $starrating or ''}}.png">
				@php } @endphp
				@php if($noofreviews>0){ @endphp
				<span>{{$noofreviews or ''}} Review</span>
				@php } @endphp
			</div>
			<div class="choose-line-radio"><input id="name" name="staffid" type="radio" value="{{ $staffandreview->staffinformation[0]->id or '' }}">
                  <label for="name">&nbsp;</label></div>
			</div> 
			</div> <!-- choose-line -->
			@endforeach
			
			</div> <!-- choose-line-section -->
			
             
          </div>@php } @endphp
          @php } @endphp
           </span>
          <div class="total_food_cost">
            <div class="total_price">
            	@php if($productprice!=''){ $totalprice=$productprice;}else{$totalprice=$beautyshopleftproduct->pro_price;} @endphp
            	Total Price: SAR <span id="totalprice">{{ number_format($totalprice,2) }}</span></div>
          </div>
          <div class="btn_row">
          	@php if($category_id==21){ $sub_type='spa'; }else{ $sub_type='beauty_centers'; } @endphp
          	<input type="hidden" name="service_duration" id="service_duration" value="{{ $beautyshopleftproduct->service_hour }}">
          	<input type="hidden" name="opening_time" id="opening_time" value="{{ $fooddateshopdetails[0]->opening_time }}">
          	<input type="hidden" name="closing_time" id="closing_time" value="{{ $fooddateshopdetails[0]->closing_time }}">
          	<input type="hidden" name="category_id" value="{{ $subsecondcategoryid}}">
          	<input type="hidden" name="subsecondcategoryid" value="{{ $category_id }}">
          	<input type="hidden" name="cart_sub_type" value="{{ $sub_type }}">
          	<input type="hidden" name="branch_id" value="{{ $branchid }}">
          	<input type="hidden" name="shop_id" value="{{ $shop_id }}">
          	<input type="hidden" name="vendor_id" value="{{ $beautyshopleftproduct->pro_mr_id }}">
          	<input type="hidden" name="product_id" id="product_id" value="{{ $beautyshopleftproduct->pro_id }}">
          	<input type="hidden" name="product_price" id="product_price" value="{{ $totalprice }}">
<input type="hidden" name="product_orginal_price" id="product_orginal_price" value="{{ $totalprice }}">
            <input type="submit" name="booknow" value="Book Now" class="form-btn addto_cartbtn">
          </div>
          <!-- container-section -->
        </div>
        <!-- service-display-left -->
      </div>
	{!! Form::close() !!}
	</div> <!-- service-display-left -->
	</div> <!--service-display-section-->


 
	<div class="sticky_other_service">

<div class="sticky_serivce">Other Services</div>
<div class="sticku_service_logo"><img src="{{ url('') }}/themes/images/logo.png"></div>

</div>

	<div class="other_serviceinc">
<div class="other_servrow">
	<div class="serv_title">Other Services</div>
    <a href="javascript:void(0);" class="serv_delete">X</a>
</div>

@include('includes.customer-budget-section')
</div> <!-- other_serviceinc -->


    
</div> <!-- detail_page -->




</div>





 <!-- outer_wrapper -->

@include('includes.footer')
<div class="action_popup">
  <div class="action_active_popup">
    <div class="action_content">{{ Session::get('status') }}</div>
    <div class="action_btnrow"><input type="hidden" id="delid" value=""/>
      <a class="action_yes status_yes" href="javascript:void(0);"> Ok </a> </div>
  </div>
</div>
       
@if(Session::has('status'))
<script type="text/javascript">
jQuery(document).ready(function()
{
 jQuery('.action_popup').fadeIn(500);
 jQuery('.overlay').fadeIn(500);
});
</script>
@endif
<script type="text/javascript">
jQuery('.status_yes').click(function()
{
 jQuery('.overlay, .action_popup').fadeOut(500);
});
</script>
<!------------- home charges calculation---------->
<script type="text/javascript">
	
	function addextracharges(hc,act){
			var productprice=document.getElementById('product_price').value;
			var product_orginal_price=document.getElementById('product_orginal_price').value;
			if(act=='add'){
			var homeservicescharges=parseFloat(product_orginal_price) + parseFloat(hc);
			jQuery('#shopaddr').css('display','none');
			}
			if(act=='remove'){
			var homeservicescharges=parseFloat(product_orginal_price);
			jQuery('#shopaddr').css('display','');
			}
			jQuery('[name=product_price]').val(homeservicescharges);
				//jQuery('#totalprice').html(homeservicescharges);
				var fprice=homeservicescharges.toFixed(2);
				jQuery('#totalprice').html(fprice);
	}
</script>
<!--------------- end calculation---------->

<!---------- validation ---------->
<script type="text/javascript">
jQuery("#cartfrm").validate({
                  ignore: [],
                  rules: {
                  	appointmentfor : {required :true},
				  	bookingdate: {
                       required: true,
                      },                

                       bookingtime: {
                       required: true,
                      
                      },
					   
                  },
                 highlight: function(element) {
            jQuery(element).removeClass('error');
                },
             
           messages: {
           		appointmentfor:{
           			required: "@php echo (Lang::has(Session::get('lang_file').'.BOOKINGFOR')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGFOR'): trans($OUR_LANGUAGE.'.BOOKINGFOR'); @endphp",
           		},
		   		bookingdate: {
               required:  "@php echo (Lang::has(Session::get('lang_file').'.BOOKINGDATE')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGDATE'): trans($OUR_LANGUAGE.'.BOOKINGDATE'); @endphp", 
                      },
                 bookingtime: {
               required: "@php echo (Lang::has(Session::get('lang_file').'.BOOKINGTIME')!= '')  ?  trans(Session::get('lang_file').'.BOOKINGTIME'): trans($OUR_LANGUAGE.'.BOOKINGTIME'); @endphp",
                            },
			         
                },
                 submitHandler: function(form) {
                    form.submit();
                }
            });

</script> 
<!---------- end validation------>

<!------------------ tabs----------------->
<script type="text/javascript">
jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = jQuery(e.target).attr("href") // activated tab
jQuery('.cat').removeClass("select") // activated tab
jQuery(this).addClass("select") // activated tab
  //alert(target);
});
</script>

<script src="{{url('/')}}/themes/js/timepicker/jquery-clockpicker.min.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery-clockpicker.min.css" rel="stylesheet" />
<script src="{{url('/')}}/themes/js/timepicker/jquery.timepicker.js"></script>
<link href="{{url('/')}}/themes/js/timepicker/jquery.timepicker.css" rel="stylesheet" />
<!------------ end tabs------------------>

<!--------- date picker script-------->
 <script type="text/javascript">
            // When the document is ready
			var jql = jQuery.noConflict();
            jql(window).load(function () {
                jql('#bookingdate').datepicker({
                    format: "yyyy-mm-dd",
					           startDate: new Date(),
                }); 
                jql("#bookingdate").datepicker("setDate", new Date());

                /*jql('#bookingtime').clockpicker({
                    autoclose: true
                });*/
                  var starttime=jql('#opening_time').val();
                  var endtime=jql('#closing_time').val();
                 
				jql('#bookingtime').timepicker({
					'minTime': starttime,
					'maxTime': endtime,
					'timeFormat': 'g:i A',					
					'showDuration': false
				}); 
            });
        </script>
<!--------- end date picker script------>

<script type="text/javascript">
	function getbeautyservice(selecteddateproduct,branchid,tbl){
		
		var dateproductid=selecteddateproduct;

			  if(selecteddateproduct){
        jQuery.ajax({
           type:"GET",
           url:"{{url('beautyshop/getcartproduct')}}?product_id="+dateproductid+"&branchid="+branchid+"&tblfld="+tbl,
           success:function(res){               
            if(res){
            	 var json = JSON.stringify(res);
			var obj = JSON.parse(json);
			//alert(obj);
console.log(obj);
            	
            		length=obj.productsericeinfo.length;
            		//alert(length);
            		
            		if(length>0){
		 for(i=0; i<length; i++)
			{
				if(obj.productprice==obj.productsericeinfo[i].pro_price){
					var nprice=obj.productsericeinfo[i].pro_price;
					var productrealprice='<span>SAR '+nprice+'</span>';
					var productrealpriceamount=obj.productsericeinfo[i].pro_price;
				}else{
					var productrealprice='<span class="strike">SAR '+obj.productsericeinfo[i].pro_price+' </span><span> SAR '+obj.productprice.toFixed(2)+'</span>';
					var productrealpriceamount=obj.productprice;
				}
				jQuery('#selectedproduct').html('<div class="service-left-display-img"><a name="common_linking" class="linking">&nbsp;</a><img src="'+obj.productsericeinfo[i].pro_Img+'" alt="" /></div><div class="service-product-name">'+obj.productsericeinfo[i].pro_title+'</div><div class="service-beauty-description">'+obj.productsericeinfo[i].pro_desc+'</div><div class="container-section"><div class="duration">Duration <span>'+obj.productsericeinfo[i].service_hour+' Hrs</span></div><span id="packageservices"></span><div class="beauty-prise">'+productrealprice+'</div></div>');
				jQuery('[name=product_id]').val(obj.productsericeinfo[i].pro_id);				
				jQuery('[name=service_duration]').val(obj.productsericeinfo[i].service_hour);
				jQuery('[name=product_price]').val(productrealpriceamount);
				jQuery('#totalprice').html(productrealprice);
			}
			
			}

			expertstaff=obj.expertstaff.length;
            		jQuery("#ourstaff").html("");
            if(expertstaff>0){

		 for(k=0; k<expertstaff; k++)
			{
				var reviews=obj.expertstaff[k].staffreviews.length;
				if(reviews>0){
					var starreview='<div class="choose-line-review"><img src="{{url('/')}}/themes/images/star'+obj.expertstaff[k].staffratingsavg+'.png"><span>'+reviews+' Review</span></div>';
				}else{ var starreview='';}
				if(k==0){ var staffheading='<div class="leftbar_title">Choose Staff</div>'; }else{ var staffheading='';}
			jQuery('#ourstaff').append('<div class="staff_area">'+staffheading+'<div class="choose-line-section"><div class="choose-line"><div class="choose-line-img"><img src="'+obj.expertstaff[k].staffinformation[0].image+'" alt="" /></div><div class="choose-line-cont"><div class="choose-line-neme">'+obj.expertstaff[k].staffinformation[0].staff_member_name+'</div><div class="choose-line-experience">'+obj.expertstaff[k].staffinformation[0].experience+' yrs <span>exp</span> </div>'+starreview+'<div class="choose-line-radio"><input id="name" name="staffid" type="radio" value="'+obj.expertstaff[k].staff_id+'"><label for="name">&nbsp;</label></div></div></div></div></div>');
			
				}
			
			}

			packageiteminformation=obj.packageiteminformation.length;
			//alert(packageiteminformation);
			if(packageiteminformation>0){
				for(jk=0; jk<packageiteminformation; jk++)
						{
							//alert(obj.packageiteminformation[jk].iteminformation[0].pro_title);
			jQuery('#packageservices').append('<div>'+obj.packageiteminformation[jk].iteminformation[0].pro_title+' - SAR '+obj.packageiteminformation[jk].iteminformation[0].pro_price+'</div>');
						}
			}


           }
           }
        });
    }


		
	}

</script>




  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>
<style type="text/css">
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;
}
</style>
<script type="text/javascript">

  var showChar = 200;
  var ellipsestext = "...";
  var moretext = "more";
  var lesstext = "less";
  jQuery('.more').each(function() {
    var content = jQuery(this).html();

    if(content.length > showChar) {

      var c = content.substr(0, showChar);
      var h = content.substr(showChar-1, content.length - showChar);

      var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

      jQuery(this).html(html);
    }

  });

  jQuery(".morelink").click(function(){
    if(jQuery(this).hasClass("less")) {
      jQuery(this).removeClass("less");
      jQuery(this).html(moretext);
    } else {
      jQuery(this).addClass("less");
      jQuery(this).html(lesstext);
    }
    jQuery(this).parent().prev().toggle();
    jQuery(this).prev().toggle();
    return false;
  });

</script>




<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>
<meta charset="UTF-8" />
<title><?php echo $SITENAME; ?>|
<?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_PROFILE');} ?>
</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="_token" content="{!! csrf_token() !!}" />
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<!-- GLOBAL STYLES -->
<!-- GLOBAL STYLES -->
<link rel="stylesheet" href="public/assets/plugins/bootstrap/css/bootstrap.css" />
<?php $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); ?>
@if(count($favi)>0)  @foreach($favi as $fav)  @endforeach
<link rel="shortcut icon" href="{{ url('')}}/public/assets/favicon/{{$fav->imgs_name}} ">
@endif
<link rel="stylesheet" href="public/assets/css/main.css" />
@if(Session::get('admin_lang_file') == 'admin_ar_lang')
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/bootstrap-rtl.css" />
<link rel="stylesheet" href="{{ url('') }}/public/assets/css/custom-ar.css" />
@endif
<link rel="stylesheet" href="public/assets/css/theme.css" />
<link rel="stylesheet" href="public/assets/css/plan.css" />
<link rel="stylesheet" href="public/assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="public/assets/plugins/Font-Awesome/css/font-awesome.css" />
<link rel="stylesheet" href="http://wisitech.in/public/assets/css/font-awesome/css/font-awesome.min.css" />
<!--END GLOBAL STYLES -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head><!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="padTop53 ">

<!-- MAIN WRAPPER -->
<div id="wrap">

<!-- HEADER SECTION --> 
@include('admin.common.admin_header') 
<!-- END HEADER SECTION --> 
@include('admin.common.left_menu_common') 
<!-- END HEADER SECTION -->

<div class=" col-sm-9 col-xs-12 right-side-bar panel_body_content">
  <div id="content">
    <div class="inner">
      <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
          <ul class="breadcrumb">
            <li class=""> <a>
              <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_HOME');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME');} ?>
              </a> </li>
            <li class="active"> <a>
              <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_PROFILE');} ?>
              </a> </li>
          </ul>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
          <div class="back-btn-area "> <a href="/siteadmin_dashboard" class="profile_back_btn">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_BACK')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADMIN_BACK') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_BACK') }}</a> </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="box dark">
            <header>
              <div class="icons"><i class="icon-edit"></i></div>
              <h5>
                <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_PROFILE');} ?>
              </h5>
            </header>
            <div class="panel_marg small-width-column">
              <?php foreach($admin_setting_details as $admin) { } ?>
              <div class="panel "> 
                
                <!--<div class="profile-heading_2">
                                               <?php /*?> <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADMIN_PROFILE');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADMIN_PROFILE');} ?><?php */?>
                                            </div>-->
                
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_FIRST_NAME')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_FIRST_NAME');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_FIRST_NAME');} ?>
                      <span class="text-sub">*</span></label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->adm_fname; ?> </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_LAST_NAME')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_LAST_NAME');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_LAST_NAME');} ?>
                      <span class="text-sub">*</span></label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->adm_lname; ?> </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_EMAIL')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_EMAIL');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_EMAIL');} ?>
                      <span class="text-sub">*</span></label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->adm_email; ?> </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_PHONE_NUMBER');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE_NUMBER');} ?>
                      <span class="text-sub">*</span></label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->adm_phone; ?> </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADDRESS1');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1');} ?>
                      <span class="text-sub">*</span></label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->adm_address1; ?> </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_ADDRESS2');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2');} ?>
                    </label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->adm_address2; ?> </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_COUNTRY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_COUNTRY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_COUNTRY');} ?>
                      <span class="text-sub">*</span></label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->co_name; ?> </div>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label col-lg-2 col-md-3 col-sm-5 col-xs-12" for="text1">
                      <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_CITY')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_CITY');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_CITY');} ?>
                      <span class="text-sub">*</span></label>
                    <div class="col-lg-10 col-md-9 col-sm-7 col-xs-12"> <?php echo $admin->ci_name; ?> </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-lg-10" for="pass1"><span class="text-sub"></span></label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- FOOTER --> 
@include('admin.common.admin_footer') 
<!--END FOOTER --> 

<!-- GLOBAL SCRIPTS --> 
<script src="public/assets/plugins/jquery-2.0.3.min.js"></script> 
<script src="public/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
<script type="text/javascript">
            $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
        </script>
</body>
</html>
<?php $current_route = Route::getCurrentRoute()->uri(); ?>
<div class="col-sm-3 col-xs-12 left-side-bar">
 <!-- LOGO SECTION -->
<header class="navbar-header">
    <a href="<?php echo url('')?>/siteadmin_dashboard" class="navbar-brand"><img src="<?php echo $SITE_LOGO; ?>" alt="Logo" /></a>
</header>
<div class="user-profile">
<figure class="user-photo">
  <img src="{{ url('') }}/public/assets/adimage/user-dummy.png">
</figure>
<h3>@if(isset($user->adm_fname)) {{$user->adm_fname}} @endif @if(isset($user->adm_lname)) {{$user->adm_lname}} @endif</h3>
</div>
  <!-- END LOGO SECTION -->
   <div id="left">
   <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    SUB MENU <i class="icon-align-justify"></i>
                </a>
   <!--<div class="media user-media well-small">
        <div class="media-body">
            <h5 class="media-heading">{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCTS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PRODUCTS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCTS') }}</h5>
            
        </div>
    </div>-->
   <ul id="menu" class="collapse"> 
        <li <?php if($current_route == 'admin/user_management' || $current_route == 'admin/user/view/{id}' || $current_route == 'admin/user_management/add' || $current_route == 'admin/user_management/edit/{id}') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="{{ url('/admin/user_management')}}">
                <i class="fa fa-users"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_USER_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_USER_MANAGEMENT') }}</a>                   
        </li>
        <li <?php if($current_route == 'admin/sub_admin' || $current_route == 'admin/subadmin/add' || $current_route == 'admin/sub_admin/view/{id}' || $current_route == 'admin/subadmin/edit/{id}') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
          
            <a href="{{ url('/admin/sub_admin')}}">
                <i class="fa fa-user"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUB_ADMIN')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUB_ADMIN'): trans($ADMIN_OUR_LANGUAGE.'.BACK_SUB_ADMIN')}}
            </a>                   
        </li>
    
        <li <?php if($current_route == 'manage_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="#" >
                <i class="fa fa-tasks"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ACCESS_LEVEL_MANAGEMENT'): trans($ADMIN_OUR_LANGUAGE.'.BACK_ACCESS_LEVEL_MANAGEMENT')}}
           </a>                   
        </li>
    
        <li <?php if($current_route == 'product_bulk_upload' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="#" >
                <i class="fa fa-shopping-cart"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_SALES_REP_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SALES_REP_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SALES_REP_MANAGEMENT')}}
           </a>                   
        </li>
        <?php    /**   product_bulk_upload_edit future option 
                        <li <?php if($current_route == 'product_bulk_upload_edit' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="<?php echo url('product_bulk_upload_edit');?>" >
                <i class=" icon-upload"></i>&nbsp; <?php if (Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCT_BULK_UPLOAD_EDIT')!= '') { echo  trans(Session::get('admin_lang_file').'.BACK_PRODUCT_BULK_UPLOAD_EDIT');}  else { echo trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCT_BULK_UPLOAD_EDIT');} ?>
           </a>                   
        </li> **/   ?>
    <li <?php if($current_route == 'sold_product' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="#" >
                <i class="fa fa-shopping-cart"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_VENDORS_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_VENDORS_MANAGEMENT'): trans($ADMIN_OUR_LANGUAGE.'.BACK_VENDORS_MANAGEMENT')}}
           </a>                   
        </li>
         <li <?php if( $current_route == "manage_product_shipping_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
            <a href="#" >
                <i class="fa fa-credit-card"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PAYMENT_TRANSACTION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_PAYMENT_TRANSACTION')}}
           </a>                   
        </li>
    
        </li>
          <li <?php if($current_route == 'admin/static_content_management' || $current_route == 'admin/static_content_management/add' || $current_route == 'admin/static_content_management/view/{id}' || $current_route == 'admin/static_content_management/broadcasst_message/list' || $current_route == 'admin/static_content_management/broadcasst_message' || $current_route == 'admin/static_content_management/edit/{id}' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="{{ url('/admin/static_content_management')}}" >
                <i class="fa fa-tasks"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_STATIC_CONTENT_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_STATIC_CONTENT_MANAGEMENT')}}
           </a>                   
        </li>
    
    <?php $general=DB::table('nm_generalsetting')->get(); ?> @foreach($general as $gs) @endforeach
     @if($gs->gs_payment_status == 'COD')   <li <?php if( $current_route == "manage_cashondelivery_details" ) { ?> class="panel active"  <?php } else { echo 'class="panel"';  }?>>
            <a href="#" >
                <i class="fa fa-user-plus"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ELECTRONIC_INVITATION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_ELECTRONIC_INVITATION')}}
           </a>                   
        </li> @endif
    
        <li <?php if($current_route == 'manage_review' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="#" >
                <i class="fa fa-star"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_REVIEW_RATING')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_REVIEW_RATING') : trans($ADMIN_OUR_LANGUAGE.'.BACK_REVIEW_RATING') }}
           </a>                   
        </li>
         <li <?php if($current_route == 'manage_review' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="#" >
                <i class="fa fa-tasks"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SUBSCRIPTION_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBSCRIPTION_MANAGEMENT') }}
           </a>                   
        </li>
         <li <?php if($current_route == 'admin/occasions' || $current_route == 'admin/occasions/view/{id}') { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="{{ url('/admin/occasions')}}" >
                <i class="fa fa-calendar"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_OCCASION')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_OCCASION') : trans($ADMIN_OUR_LANGUAGE.'.BACK_OCCASION') }}
           </a>                   
        </li>
         <li <?php if($current_route == 'manage_review' ) { ?> class="panel active" <?php } else { ?> class="panel" <?php } ?> >
            <a href="#" >
                <i class="fa fa-tasks"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_DELIVERY_MANAGEMENT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DELIVERY_MANAGEMENT') : trans($ADMIN_OUR_LANGUAGE.'.BACK_DELIVERY_MANAGEMENT') }}
           </a>                   
        </li>
    </ul>
   </div>                
</div>

<?php //print_r(session::all()); ?>
<?php  $current_route = Route::getCurrentRoute()->uri();

if(isset($routemenu))
{

$menus=$routemenu;
$menu = strtoupper($menus);
}
 ?>
<div oncontextmenu="return false"></div>
  <script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vuex/2.3.1/vuex.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.3.1/vue-resource.min.js"></script> 
<!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script>  -->
     <script type="text/javascript">
 var __lc = {};
__lc.license = 4302571;

(function() {
 var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
 lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>



  <div id="top" class="container">

             <nav class="navbar navbar-inverse navbar-fixed-top">
             
               
                <ul class="nav navbar-top-links navbar-right">
<?php $newsubscriberscount=Session::get('newsubscriberscount');
	$blogcmtcount=Session::get('blogcmtcount');
$adrequestcnt=Session::get('adrequestcnt');

 $inqcmt = DB::table('nm_enquiry')->where('enq_readstatus', '=', 0)->count();
 $blogcmt = DB::table('nm_blog_cus_comments')->where('cmt_msg_status', '=', 0)->count();
 $adcmt = DB::table('nm_add')->where('ad_read_status', '=', 0)->count();

?>
                    <!-- MESSAGES SECTION -->
                    <li class="dropdown">
                        <a href="{{ url('manage_inquires') }}" data-original-title="Tooltip on bottom" data-toggle="tooltip" data-placement="bottom" title="Inquiries">
                            <span class="label label-warning">{{ $inqcmt }}</span> 
                            <i class="icon-envelope-alt"></i>
                        </a>
		  </li>
		   <li class="dropdown">
                        <a href="{{ url('manage_blogcmts') }}" data-original-title="Tooltip on bottom" data-toggle="tooltip" data-placement="bottom" title="Blog comments">
                            <span class="label label-success">{{ $blogcmt}}</span>    <i class="icon-comment-alt"></i>
                        </a>
		  </li>
		 <?php /*?> <li class="dropdown">
                        <a href="<?php echo url('manage_subscribers');?>" data-original-title="Tooltip on bottom" data-toggle="tooltip" data-placement="bottom" title="New Subscribers">
                            <span class="label label-success"><?php echo $newsubscriberscount;?></span>    <i class="icon-bookmark-empty"></i>&nbsp; 
                        </a>
		  </li><?php */?>
            <li class="dropdown">
                        <a href="{{ url('manage_ad') }}" data-original-title="Tooltip on bottom" data-toggle="tooltip" data-placement="bottom" title="Advertise request">
                            <span class="label label-success">{{ $adcmt }}</span>    <i class="icon-bookmark-empty"></i>
                        </a>
		  </li>
                    <!--END MESSAGES SECTION -->
                   
                           <select name="Language_change" id="Language_change" onchange="Lang_change()">
                                    <?php foreach($Admin_Active_Language as $Active_Lang) {?>
                                        <option value="{{$Active_Lang->lang_code}}" @if($admin_selected_lang_code == $Active_Lang->lang_code)selected @endif >{{$Active_Lang->lang_name}}</option>
                                    <?php } ?>
                           </select>
                     
                     <li><a href="{{ url('admin_profile') }}" class="btn btn-default"><i class="icon-user"></i> {{(Lang::has(Session::get('admin_lang_file').'.BACK_MY_PROFILE')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MY_PROFILE') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MY_PROFILE') }} </a>
                            </li>
                            <li><a href="{{ url('admin_settings') }}" class="btn btn-default"><i class="icon-gear"></i> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_SETTINGS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS') }} </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="{{ url('admin_logout')}}" class="btn btn-default"><i class="icon-signout"></i> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_LOGOUT')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_LOGOUT') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_LOGOUT') }} </a>
                            </li>
                    
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>
            
			<!--<div class="mainmenu"> 
            <ul class="">
                <li><a href="{{ url('siteadmin_dashboard') }}" @if($menu=="DASHBOARD") class="active" @endif>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_DASHBOARD') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_DASHBOARD') }}</a></li>

                <li><a href="{{ url('general_setting') }}" @if($menu=="SETTINGS") class="active" @endif>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_SETTINGS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SETTINGS'): trans($ADMIN_OUR_LANGUAGE.'.BACK_SETTINGS')}}</a>  </li>


                <li><a href="{{ url('product_dashboard')}}" @if($menu=="PRODUCTS") class="active" @endif> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_PRODUCTS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_PRODUCTS') :  trans($ADMIN_OUR_LANGUAGE.'.BACK_PRODUCTS')}} </a>  </li> 
                
                
                <li><a href="{{ url('customer_dashboard')}}" @if($menu=="CUSTOMER") class="active" @endif>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_CUSTOMERS')!= '')  ?  trans(Session::get('admin_lang_file').'.BACK_CUSTOMERS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_CUSTOMERS')}} </a>  </li>

                <li><a href="{{ url('merchant_dashboard') }}" @if($menu=="MERCHANT") class="active" @endif> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANTS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MERCHANTS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANTS') }} </a>  </li>

                 <li><a href="{{ url('merchant_dashboard') }}" @if($menu=="SERVICES") class="active" @endif> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANTS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_SERVICES') : trans($ADMIN_OUR_LANGUAGE.'.BACK_SERVICES') }} </a>  </li>


                <li><a href="{{ url('manage_publish_blog') }}" @if($menu=="BLOG") class="active" @endif>{{ (Lang::has(Session::get('admin_lang_file').'.BACK_BLOGS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_BLOGS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_BLOGS')}}</a> </li>
                </ul>


            </div>-->

                  

        </div>
<script type="text/javascript">
    function Lang_change() 
    {
        var language_code = $("#Language_change option:selected").val();
        $.ajax
        ({
            type:'POST',
            url:"<?php echo url('admin_new_change_languages');?>",
            data:{'language_code':language_code},
            success:function(data)
            {
                window.location.reload();
            }
        });
    }
</script>


  <?php  $current_route = Route::getCurrentRoute()->uri(); ?>
<div id="left">
            <div class="media user-media well-small">
                <!-- <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="public/assets/img/user.gif" />
                </a> -->
                
                <div class="media-body">
                    <h5 class="media-heading"> {{ (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANTS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MERCHANTS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANTS')}} </h5>
                    
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">
                 <li <?php if($current_route == 'merchant_dashboard'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?> >
                    <a href="{{ url('merchant_dashboard') }}" >
                        <i class="icon-dashboard"></i>&nbsp; {{ (Lang::has(Session::get('admin_lang_file').'.BACK_MERCHANTS_DASHBOARD')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MERCHANTS_DASHBOARD'): trans($ADMIN_OUR_LANGUAGE.'.BACK_MERCHANTS_DASHBOARD')}}</a>                   
                </li>
                   <li <?php if($current_route == 'add_merchant'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?> >
                    <a href="{{ url('add_merchant') }}" >
                        <i class="icon-user"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT'):  trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT') }}
	                </a>                   
                </li>
                 <li <?php if($current_route == 'manage_merchant'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="{{ url('manage_merchant') }}" >
                        <i class="icon-ok-sign"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MANAGE_MERCHANT_ACCOUNTS') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_MERCHANT_ACCOUNTS') }}
                   </a>                   
                </li>
				  <li <?php if($current_route == 'manage_store_review'){ echo 'class="panel active"'; } else { echo 'class="panel"'; } ?>>
                    <a href="{{ url('manage_store_review') }}" >
                        <i class="icon-comment"></i>&nbsp;{{ (Lang::has(Session::get('admin_lang_file').'.BACK_MANAGE_STORE_REVIEW')!= '') ?  trans(Session::get('admin_lang_file').'.BACK_MANAGE_STORE_REVIEW') : trans($ADMIN_OUR_LANGUAGE.'.BACK_MANAGE_STORE_REVIEW') }}
                   </a>                   
                </li>
				
            </ul>


        </div>

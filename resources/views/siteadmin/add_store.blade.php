<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

 <!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8" />
    <title>{{ $SITENAME }} | @if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT_STORE') }} @endif</title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
		<meta name="_token" content="{!! csrf_token() !!}"/>
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="{{ url('') }}/public/assets/css/main.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/theme.css" />
	  <link rel="stylesheet" href="{{ url('')}}/public/assets/css/plan.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/css/MoneAdmin.css" />
    <link rel="stylesheet" href="{{ url('')}}/public/assets/plugins/Font-Awesome/css/font-awesome.css" />
     @php 
     $favi = DB::table('nm_imagesetting')->where('imgs_type', '=', 2)->get(); @endphp
    @if(count($favi)>0)  
    @foreach($favi as $fav) @endforeach
    <link rel="shortcut icon" href="{{ url('') }}/public/assets/favicon/<?php echo $fav->imgs_name; ?>">
@endif	
    <!--END GLOBAL STYLES -->
       <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
     <!-- END HEAD -->

     <!-- BEGIN BODY -->
<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap">


         <!-- HEADER SECTION -->
         {!! $adminheader !!}
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
      {!! $adminleftmenus !!}
        <!--END MENU SECTION -->

		<div></div>

         <!--PAGE CONTENT -->
        <div id="content">
           
                <div class="inner">
                    <div class="row">
                    <div class="col-lg-12">
                        	<ul class="breadcrumb">
                            	<li class=""><a href="#">@if (Lang::has(Session::get('admin_lang_file').'.BACK_HOME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_HOME') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_HOME') }} @endif</a></li>
                                <li class="active"><a href="#">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT_STORE') }} @endif</a></li>
                            </ul>
                    </div>
                </div>
            <div class="row">
<div class="col-lg-12">
    <div class="box dark">
        <header>
            <div class="icons"><i class="icon-edit"></i></div>
            <h5>@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADD_MERCHANT_STORE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADD_MERCHANT_STORE') }} @endif</h5>
            
        </header>
        @if ($errors->any())
         <br>
		 <ul style="color:red;">
		<div class="alert alert-danger alert-dismissable">{!! implode('', $errors->all(':message<br>')) !!}
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
		</ul>	
		@endif
         @if (Session::has('mail_exist'))
		<div class="alert alert-warning alert-dismissable">{!! Session::get('mail_exist') !!}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
		@endif
        
        <div class="row">
        	<div class="col-lg-11 panel_marg"style="padding-bottom:10px;">
                    
                    {!! Form::open(array('url'=>'add_store_submit','class'=>'form-horizontal','enctype'=>'multipart/form-data', 'accept-charset' => 'UTF-8')) !!}
                   
					
					 <div class="panel panel-default">
                        <div class="panel-heading">
                      @if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_DETAILS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORE_DETAILS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_DETAILS') }} @endif
                        </div>
                    <div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_NAME')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORE_NAME') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_NAME') }} @endif<span class="text-sub">*</span></label>

						<div class="col-lg-4">
              {{ Form::hidden('store_merchant_id',$id,array('id'=>'store_merchant_id'))}}
							<!-- <input type="hidden" name="store_merchant_id" id="store_merchant_id" value="<?php echo $id; ?>"> -->
							<input type="text" maxlength="150" class="form-control" placeholder="Enter Store Name {{ $default_lang }}" id="store_name" name="store_name" value="{!! Input::old('store_name') !!}"  >
						</div>
						</div>
                    </div>
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
				@php	$get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Store Name({{ $get_lang_name }})<span class="text-sub">*</span></label>

						<div class="col-lg-4">
							<input type="text" maxlength="150" class="form-control" placeholder="Enter Store Name In {{ $get_lang_name }}" id="store_name_{{ $get_lang_name }}" name="store_name_{{ $get_lang_name }}" value="{!! Input::old('store_name_'.$get_lang_name) !!}"  >
						</div>
						</div>
                    </div>
					@endforeach @endif
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_PHONE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_PHONE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PHONE') }} @endif <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('store_pho',Input::old('store_pho'),array('id'=>'store_pho','class'=>'form-control','placeholder'=>'Enter Phone Number','maxlength'=>'16')) }}
                        <!-- <input type="text" maxlength="16" class="form-control" placeholder="Enter Phone Number" id="store_pho" name="store_pho" value="{!! Input::old('store_pho') !!}"  > -->
                    </div>
                    <div id="store_pho_error_msg" style="color:#F00;font-weight:800"></div>
                </div>
                        </div>
						
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS1')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADDRESS1') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS1') }} @endif<span class="text-sub">*</span></label>

							<div class="col-lg-4">

								<input type="text" class="form-control" placeholder="Enter Store Address one {{ $default_lang }}" id="store_add_one" name="store_add_one" value="{!! Input::old('store_add_one') !!}"  >
							</div>

						</div>
                    </div>
					
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
				@php	$get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address One({{ $get_lang_name }})<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address one In {{ Helper::lang_name() }}" id="store_add_one_{{ $get_lang_name }}" name="store_add_one_{{ $get_lang_name }}" value="{!! Input::old('store_add_one_'.$get_lang_name) !!}"  >
							</div>
						</div>
                    </div>
					 @endforeach @endif
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ADDRESS2')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ADDRESS2') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ADDRESS2') }} @endif<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address two {{ $default_lang }}" id="store_add_two" name="store_add_two" value="{!! Input::old('store_add_two') !!}"   >
							</div>
						</div>
                    </div>
					
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
				@php	$get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Address two({{$get_lang_name }})<span class="text-sub">*</span></label>

							<div class="col-lg-4">
								<input type="text" class="form-control" placeholder="Enter Store Address two In {{ $get_lang_name}}" id="store_add_two_{{ $get_lang_name }}" name="store_add_two_{{ $get_lang_name }}" value="{!! Input::old('store_add_two_'.$get_lang_name) !!}" >
							</div>
						</div>
                    </div>
                     @endforeach @endif
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SELECT_COUNTRY') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_COUNTRY') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4"> 
                       <select class="form-control" name="select_country" id="select_country" onChange="select_city_ajax(this.value)" >
                        <option value="">-- @if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SELECT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT') }} @endif --</option>
                         @foreach($country_details as $country_fetch)
          				 <option value="{{ $country_fetch->co_id }}" <?php if(Input::old('select_country')==$country_fetch->co_id) echo 'selected';?>><?php echo $country_fetch->co_name; ?></option>
           				   @endforeach
       					 </select>
                    </div>
                </div>
                        </div>
						 <div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT_CITY')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SELECT_CITY') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT_CITY') }} @endif <span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       <select class="form-control" name="select_city" id="select_city" required>
           				<option value="">-- @if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SELECT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SELECT') }} @endif --</option>
                  </select>
                    </div>
                      <div id="city_error_msg" style="color:#F00;font-weight:800"></div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_ZIPCODE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_ZIPCODE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_ZIPCODE') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                       {{ Form::text('zip_code',Input::old('zip_code'),array('id'=>'zip_code','class'=>'form-control','placeholder'=>'Enter Zipcode','maxlength'=>'12')) }}
                       <!--  <input type="text" maxlength="12" class="form-control" placeholder="Enter Zipcode" id="zip_code" name="zip_code" value="{!! Input::old('zip_code') !!}"  > -->
                    </div>
                     <div id="zip_code_error_msg" style="color:#F00;font-weight:800"></div>
                </div>
                        </div>
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_META_KEYWORDS')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_META_KEYWORDS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_META_KEYWORDS') }} @endif<span class="text-sub"></span></label>

							<div class="col-lg-4">
								<textarea  class="form-control" name="meta_keyword" placeholder="Enter Meta Keywords {{ $default_lang }}" id="meta_keyword" >{!! Input::old('meta_keyword') !!}</textarea>
							</div>
						</div>
                    </div>
					
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
				@php	$get_lang_name = $get_lang->lang_name; @endphp
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Meta Keywords({{ $get_lang_name }})<span class="text-sub"></span></label>

							<div class="col-lg-4">
								<textarea  class="form-control" name="meta_keyword_{{ $get_lang_name }}" id="meta_keyword_{{ $get_lang_name }}" placeholder="Enter Meta Keywords In {{ $get_lang_name }}" >{!! Input::old('meta_keyword_'.$get_lang_name) !!}</textarea>
							</div>
						</div>
                    </div>
					 @endforeach @endif
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_META_DESCRIPTION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_META_DESCRIPTION') }} @endif<span class="text-sub"></span></label>

							<div class="col-lg-4">
								<textarea id="meta_description"  name="meta_description" placeholder="Enter Meta Description {{ $default_lang }}" class="form-control">{!! Input::old('meta_description') !!}</textarea>
							</div>
						</div>
                    </div>
					
					@if(!empty($get_active_lang))  
					@foreach($get_active_lang as $get_lang) 
					$get_lang_name = $get_lang->lang_name;
					
					<div class="panel-body">
                        <div class="form-group">
							<label class="control-label col-lg-2" for="text1">Meta Description({{ $get_lang_name }})<span class="text-sub"></span></label>

							<div class="col-lg-4">
								<textarea id="meta_description_{{ $get_lang_name }}"  name="meta_description_{{ $get_lang_name }}" placeholder=" Enter Meta Description In {{ $get_lang_name }}" class="form-control">{!! Input::old('meta_description_'.$get_lang_name) !!}</textarea>
							</div>
						</div>
                    </div>
					@endforeach @endif
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_WEBSITE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_WEBSITE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_WEBSITE') }} @endif<span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input type="url" class="form-control" id="website" name="website" value="{!! Input::old('website') !!}"  placeholder="http://www.example.com" >
                    </div>
                </div>
                        </div>
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1"><span class="text-sub"></span></label>

                    <div class="col-lg-4">
                        <input id="pac-input" onclick="check();" class="form-control" type="text" placeholder="@if (Lang::has(Session::get('admin_lang_file').'.BACK_TYPE_YOUR_LOCATION')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_TYPE_YOUR_LOCATION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_TYPE_YOUR_LOCATION') }} @endif ( @if (Lang::has(Session::get('admin_lang_file').'.BACK_AUTO_COMPLETE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_AUTO_COMPLETE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_AUTO_COMPLETE') }} @endif )">

                    </div>
					<div class="col-lg-4"></div>
                </div>
                        </div>
                        <input type="hidden" name="exist" id="exist" value="">
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_SEARCH_LOCATION')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_SEARCH_LOCATION') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SEARCH_LOCATION') }} @endif<span class="text-sub">*</span><br><span  style="color:#999">( @if (Lang::has(Session::get('admin_lang_file').'.BACK_DRAG_MARKER')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_DRAG_MARKER') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_DRAG_MARKER') }} @endif & @if (Lang::has(Session::get('admin_lang_file').'.BACK_LONGITUDE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LONGITUDE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LONGITUDE') }} @endif )</span></label>

                    <div class="col-lg-4">
                        <div id="map_canvas" style="width:300px;height:250px;" ></div>
                        <div id="location_error_msg"  style="color:#F00;font-weight:800" ></div>
                    </div>
                    
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_LATITUDE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LATITUDE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LATITUDE') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('latitude',Input::old('latitude'),array('id'=>'latitude','class'=>'form-control','readonly'=>'readonly')) }}
                       <!--  <input type="text" class="form-control" placeholder="" id="latitude" name="latitude" value="{!! Input::old('latitude') !!}"  readonly> -->
                    </div>
                </div>
                        </div>
							<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_LONGITUDE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_LONGITUDE') }}  else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_LONGITUDE') }} @endif<span class="text-sub">*</span></label>

                    <div class="col-lg-4">
                      {{ Form::text('longitude',Input::old('longitude'),array('id'=>'longitude','class'=>'form-control','readonly'=>'readonly')) }}
                       <!--  <input type="text" class="form-control" placeholder="" id="longitude" name="longitude" value="{!! Input::old('longitude') !!}"  readonly > -->
                    </div>
                </div>
                        </div>
							
						<div class="panel-body">
                           <div class="form-group">
                    <label class="control-label col-lg-2" for="text1">@if (Lang::has(Session::get('admin_lang_file').'.BACK_STORE_IMAGE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_STORE_IMAGE') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_STORE_IMAGE') }} @endif<span class="text-sub">*</span></label>
				
                    <div class="col-lg-4">
                       <input type="file" id="file" name="file" placeholder="Fruit ball" required>
                        <span class="errortext red" style="color:red"><em>@if (Lang::has(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_IMAGE_SIZE_MUST_BE') }}  @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_IMAGE_SIZE_MUST_BE') }} @endif {{ $STORE_WIDTH }} x {{ $STORE_HEIGHT }} @if (Lang::has(Session::get('admin_lang_file').'.BACK_PIXELS')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_PIXELS') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_PIXELS') }} @endif</em></span>
                    </div>
                </div>

                        </div>
                        
                    </div>
					
                    	
				
					<div class="form-group">
                    <label class="control-label col-lg-3" for="pass1"><span class="text-sub"></span></label>

                    <div class="col-lg-8">
                     <button class="btn btn-warning btn-sm btn-grad" type="submit" id="submit" ><a style="color:#fff" >@if (Lang::has(Session::get('admin_lang_file').'.BACK_SUBMIT')!= '') {{  trans(Session::get('admin_lang_file').'.BACK_SUBMIT') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_SUBMIT') }} @endif</a></button>
                     <button class="btn btn-default btn-sm btn-grad" type="reset" ><a style="color:#000">@if (Lang::has(Session::get('admin_lang_file').'.BACK_RESET')!= '') {{ trans(Session::get('admin_lang_file').'.BACK_RESET') }} @else {{ trans($ADMIN_OUR_LANGUAGE.'.BACK_RESET') }} @endif</a></button>
                   
                    </div>
					  
                </div>
                
                </form>
                </div>
        
        </div>
    </div>
</div>
   
    </div>
                    
                    </div>
                    
                    
                    

                </div>
            <!--END PAGE CONTENT -->
 
        </div>
    
     <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
      {!! $adminfooter !!}
    <!--END FOOTER -->


     <!-- GLOBAL SCRIPTS -->
    <script src="{{ url('') }}/public/assets/plugins/jquery-2.0.3.min.js"></script>
    <script>

	$( document ).ready(function() {
	$('#submit').click(function() {

    var longitude  = $('#longitude').val();
    var latitude   = $('#latitude').val();
    var select_city = $('#select_city');
        /*Store Address check*/
		if(($('#exist').val())==""){
      
			$('#location_error_msg').html('Please Choose Different Adrress, Store already exists in this same address');
			title.css('border', '1px solid red'); 
			title.focus();
			return false;
		}else if(($('#exist').val())==1){
     
			$('#location_error_msg').html('Please Choose Different Adrress, Store already exists in this same address');
			title.css('border', '1px solid red'); 
			title.focus();
			return false;
		}else{
			title.css('border', ''); 
			$('#location_error_msg').html('');
		}

    if(longitude.val() == ''){
            longitude.css('border', '1px solid red'); 
            $('#longitude_error_msg').html('Please Select longitude');
            longitude.focus();
            return false;
        }else{
            longitude.css('border', ''); 
            $('#longitude_error_msg').html('');
        }

        if(latitude.val() == ''){
        latitude.css('border', '1px solid red'); 
            $('#latitude_msg').html('Please Select latitude');
            latitude.focus();
            return false;
        }else{
            latitude.css('border', ''); 
            $('#latitude_error_msg').html('');
        }



    var file		 	 = $('#file');
	  var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
      	if(file.val() == "")
 		{
 		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else if ($.inArray($('#file').val().split('.').pop().toLowerCase(), fileExtension) == -1) { 				
		file.focus();
		file.css('border', '1px solid red'); 		
		return false;
		}			
		else
		{
		file.css('border', ''); 				
		}
	});
	});

	    
function check() { 
			var mer_id     = $('#store_merchant_id').val();
			var longitude  = $('#longitude').val();
			var latitude   = $('#latitude').val();
			var datas      = "mer_id="+mer_id+"&latitude="+latitude+"&longitude="+longitude;
      
            $.ajax({
			      type: 'GET',
                  url: '<?php echo url('check_store_exists'); ?>',
				  data: datas,
				  success: function(response){  
                    
                     if(response==1){  //already exist
					  alert("This Store Already Exist with this same address");
					  $("#exist").val("1"); //already exist
            $('#latitude').val('');
            $('#longitude').val('');
					  $("#latitude").css('border', '1px solid red'); 
					  $('#longitude').css('border', '1px solid red');
            $('#pac-input').css('border', '1px solid red');
					  $('#location_error_msg').html("Store Already Exist with this same address");	
					  $("#pac-input").focus();
					  return false;				   
				   }else
           {
            $("#latitude").css('border', ''); 
            $('#longitude').css('border', '');
            $('#pac-input').css('border', '');
            $('#location_error_msg').html(""); 
           // $("#pac-input").blur(); 
           }
				 }		
			});
    
}
	function select_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		 //alert(passData);
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){  
				 // alert(responseText);
				   if(responseText)
				   { 
					$('#select_city').html(responseText);					   
				   }
				}		
			});		
	}
	
	function select_mer_city_ajax(city_id)
	{
		 var passData = 'city_id='+city_id;
		
		   $.ajax( {
			      type: 'get',
				  data: passData,
				  url: '<?php echo url('ajax_select_city'); ?>',
				  success: function(responseText){  
				  if(responseText){ 
					$('#select_mer_city').html(responseText);					   
				  }
				}		
			});	
	}

  

	</script>
     <script src="{{ url('') }}/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('') }}/public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=<?php echo $GOOGLE_KEY;?>'></script>
    @php $city_det = DB::table('nm_emailsetting')->first(); @endphp
    <script type="text/javascript">
    var map;

    function initialize() {


 		var myLatlng = new google.maps.LatLng(<?php echo $city_det->es_latitude; ?>,<?php echo $city_det->es_longitude; ?>);
        var mapOptions = {

           zoom: 10,
                center: myLatlng,
                disableDefaultUI: true,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
	 		  var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
				draggable:true,    
            });	
		google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longitude').val(lng);
           check();
			});
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
 
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
 
            var place = autocomplete.getPlace();
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                
          $('#latitude').val(latitude);
			    $('#longitude').val(longitude);
                check();
               
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
				var myLatlng = place.geometry.location;	
				var latlng = new google.maps.LatLng(latitude, longitude);
                marker.setPosition(latlng);
                
			google.maps.event.addListener(marker, 'dragend', function(e) {
   			 
			 var lat = this.getPosition().lat();
  			 var lng = this.getPosition().lng();
			 $('#latitude').val(lat);
			 $('#longitude').val(lng);
            check();
            });
            } else {
                map.setCenter(place.geometry.location);	
				
                map.setZoom(17);
            }
        });



    }


    google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <!-- END GLOBAL SCRIPTS -->   
     <!---Right Click Block Code---->
<script language="javascript">
document.onmousedown=disableclick;
status="Cannot Access for this mode";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;    
   }
}
</script>

	<script type="text/javascript">
   $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
   });
</script>


<!---F12 Block Code---->
<script type='text/javascript'>

$('#store_name').bind('keyup blur',function(){ 
    var node = $(this);
    node.val(node.val().replace(/[^a-z 0-9 A-Z_-]/g,'') ); }
);

$( document ).ready(function() {
  var phone_no         = $('#phone_no');
  var store_pho        = $('#store_pho');
  var zip_code         = $('#zip_code');
   $('#phone_no').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            phone_no.css('border', '1px solid red'); 
            $('#phone_no_error_msg').html('Numbers Only Allowed');
            phone_no.focus();
            return false;
        }else{          
            phone_no.css('border', ''); 
            $('#phone_no_error_msg').html('');          
        }
    });
    /*Store Phone Number*/
    $('#store_pho').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            store_pho.css('border', '1px solid red'); 
            $('#store_pho_error_msg').html('Numbers Only Allowed');
            store_pho.focus();
            return false;
        }else{          
            store_pho.css('border', ''); 
            $('#store_pho_error_msg').html('');         
        }
    });
   
    /*Store Zipcode*/
    $('#zip_code').keypress(function (e){
        if(e.which!=8 && e.which!=0 && e.which!=13 && (e.which<48 || e.which>57)){
            zip_code.css('border', '1px solid red'); 
            $('#zip_code_error_msg').html('Numbers Only Allowed');
            zip_code.focus();
            return false;
        }else{          
            zip_code.css('border', ''); 
            $('#zip_code_error_msg').html('');          
        }
    });


         $.ajax( {
            type: 'get',
         data: {'city_id_ajax':'<?php echo Input::old('select_city'); ?>','country_id_ajax':'<?php echo Input::old('select_country'); ?>'},
         url: '<?php echo url('ajax_select_city_edit'); ?>',
         success: function(responseText){  
           if(responseText)
           { 
               
          $('#select_city').html(responseText);             
           }
        }   
      }); 

});


/*
$(document).keydown(function(event){
    if(event.keyCode==123){
    return false;
   }
else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
      return false;  //Prevent from ctrl+shift+i
   }
});*/
</script>
</body>
     <!-- END BODY -->
</html>

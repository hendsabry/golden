@include('includes.navbar')
@php 
global $Current_Currency;
$Current_Currency  = Session::get('currency'); 
if($Current_Currency =='') { 
$Current_Currency = 'SAR'; 
} 
if(isset($_REQUEST['test']) && $_REQUEST['test']==1)
{
die;
}

@endphp
@php $productInsuranceamount=0; @endphp
<div class="outer_wrapper">
@include('includes.header')
  <div class="inner_wrap"> 
    <div class="search-section">
      <div class="event-result-form">
        <div class="search-box search-box1">
          <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.TYPE_OF_OCCASION')!= '')  ?  trans(Session::get('lang_file').'.TYPE_OF_OCCASION'): trans($OUR_LANGUAGE.'.TYPE_OF_OCCASION')}}</div>
          <div class="search-noneedit"> {{ $otype }}</div>
        </div>
        <!-- search-box -->
        <div class="search-box search-box2">
          <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.NO_OF_ATTENDANCE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_ATTENDANCE'): trans($OUR_LANGUAGE.'.NO_OF_ATTENDANCE')}}</div>
          <div class="search-noneedit">{{ Session::get('searchdata.noofattendees')}}</div>
        </div>
        <!-- search-box -->
        <div class="search-box search-box3">
          <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.Budget')!= '')  ?  trans(Session::get('lang_file').'.Budget'): trans($OUR_LANGUAGE.'.Budget')}}</div>
          <div class="search-noneedit">{{ Session::get('currency') }} {{ Session::get('searchdata.budget')}}</div>
        </div>
        <!-- search-box -->
        <!--<div class="search-box search-box4">
<div class="search-box-label">Gender</div>
<div class="search-noneedit">Female</div>
</div>-->
        <!-- search-box -->
        <div class="search-box search-box5">
          <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')}}</div>
          <div class="search-noneedit">@php $cid=Session::get('searchdata.cityid');
 $getCity = Helper::getcity($cid);
 if(Session::get('lang_file') !='en_lang'){
                        $city_name= 'ci_name_ar';
                        }else{
                    $city_name= 'ci_name'; 
                       }  

 echo $getCity->$city_name; @endphp</div>
        </div>
        <!-- search-box -->
        <div class="search-box search-box6">
          <div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.OccasionDate')!= '')  ?  trans(Session::get('lang_file').'.OccasionDate'): trans($OUR_LANGUAGE.'.OccasionDate')}}</div>
          <div class="search-noneedit">{{ Session::get('searchdata.occasiondate')}}</div>
        </div>
        <div class="search-box search-box6">
<div class="search-box-label">{{ (Lang::has(Session::get('lang_file').'.GENDER')!= '')  ?  trans(Session::get('lang_file').'.GENDER'): trans($OUR_LANGUAGE.'.GENDER')}}</div>
<div class="search-noneedit">
 @php if(Session::get('searchdata.gender')=='male'){ 
    if (Lang::has(Session::get('lang_file').'.MALE')!= '') { echo trans(Session::get('lang_file').'.MALE'); } else  { echo trans($OUR_LANGUAGE.'.MALE'); }
}
@endphp

@php if(Session::get('searchdata.gender')=='female'){ 
 if (Lang::has(Session::get('lang_file').'.FEMALE')!= '') { echo trans(Session::get('lang_file').'.FEMALE'); } else  { echo trans($OUR_LANGUAGE.'.FEMALE'); }
}
@endphp
@php if(Session::get('searchdata.gender')=='Both'){ 
if (Lang::has(Session::get('lang_file').'.BOTH')!= '') { echo trans(Session::get('lang_file').'.BOTH'); } else  { echo trans($OUR_LANGUAGE.'.BOTH'); }
}
@endphp
</div>
</div>
        <!-- search-box -->
      </div>




      <!-- modify-search-form -->
    </div>

    @php     
    $ProIds = Session::get( 'ProIds' ); 
    $ExtraQty = Session::get( 'ExtraQty' ); 
    @endphp
    @if(isset($ProIds) && count($ProIds)>=1)

  <div class="productExtraqty error" style="padding-top: 10px;"> 
      <ul>
      @php $i=0; @endphp
      @foreach($ProIds as $pid)
      @php $pname = Helper::getproductNames($pid); @endphp
      <li>Max quantity ({{$pname}}) available is {{$ExtraQty[$i]}}</li>
      @php $i=0; @endphp
      @endforeach
      </ul>
   </div>
    @endif


    <div class="tatal-services-area">
      <div class="mobile-filter-line">
        <div class="mobile-search mobile-search1row"><span>Event Result</span></div>
      </div>
      <div class="tatal-services-inner-area">
        <div class="tatal-services-heading">{{ (Lang::has(Session::get('lang_file').'.TOTAL_SERVICES')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_SERVICES'): trans($OUR_LANGUAGE.'.TOTAL_SERVICES')}}</div>
        <div class="table-cart-wrapper">
          <table class="table-cart tatal-services-table">
            <tr class="tr table_heading_tr">
               
              <td class="table_heading td1">{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}</td>
			  <td class="table_heading td2">{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}</td>
              <td class="table_heading td3">{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}</td>
              <td class="table_heading td4">{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}</td>
            </tr>
			@php $k=1; $basetotal=0; @endphp
            <tr class="tr mobile-first-row">
              <td colspan="4"><div class="demo">
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        @php   if(count($productinfo)>0){

                  foreach($productinfo as $productinfo)
                  {
                  if(isset($productinfo[0]->pro_discount_percentage) && $productinfo[0]->pro_discount_percentage!=''){ $hallprice=$productinfo[0]->pro_disprice; }else{ $hallprice=$productinfo[0]->pro_price; }
                           $remainingamount= ($hallprice*75)/100;
                           $remainingamount=currency($remainingamount, 'SAR',$Current_Currency, $format = false);
                         @endphp
						 <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingOne">
                       
                          <table width="100%">
                            <tr class="tr hall-cart-tr ">
							<td colspan="4" class="hall-cart-td">
							<table width="100%">
							<tr>
              


							<td class="td td1 cast-hall mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> <a href="#" class="cart-delete" data-pro="hall" data-id="{{$productinfo[0]->pro_id}}">&nbsp;</a>  {{ $productinfo[0]->pro_title or '' }}</td>


							<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">&nbsp;</td>
							<td class="td td3 hall-sp" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}"> @php $getC = Helper::getparentCat($productinfo[0]->pro_mc_id); @endphp
                                {{ $getC->mc_name  }}</td>
							<td class="td td4 cast-hall" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{ $hallprice }}</td>
							</tr>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">{{ (Lang::has(Session::get('lang_file').'.Advance_Payment_for_Hall_Booking')!= '')  ?  trans(Session::get('lang_file').'.Advance_Payment_for_Hall_Booking'): trans($OUR_LANGUAGE.'.Advance_Payment_for_Hall_Booking')}}</td>
							<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">
                @php
                      if($productinfo[0]->Insuranceamount!='')
                                { $productInsuranceamount=$productinfo[0]->Insuranceamount; }
                                else{ $productInsuranceamount=0;}
                 $finalprice=$hallprice;
                                   //$finalprice=currency($hallprice, 'SAR',$Current_Currency, $format = false); 
                                   
                                   $advanceprice=($finalprice*25)/100;
                                   $remainingammount=$finalprice-$advanceprice;

                                @endphp
                                @php $advance_price=currency($advanceprice, 'SAR',$Current_Currency, $format = false); 
                                  $basetotal= $basetotal + $advance_price;
                                @endphp
                               {{ Session::get('currency') }} {{ number_format($advance_price, 2) }}</td>
							</tr>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">{{ (Lang::has(Session::get('lang_file').'.INSURANCE_AMOUNT')!= '')  ?  trans(Session::get('lang_file').'.INSURANCE_AMOUNT'): trans($OUR_LANGUAGE.'.INSURANCE_AMOUNT')}}</td>
							<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">
                  {{ Session::get('currency') }} {{ number_format($productInsuranceamount,2) }}
              </td>
							</tr>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">
							 <h4 class="panel-title"> 
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">{{ (Lang::has(Session::get('lang_file').'.Items_Ordered')!= '')  ?  trans(Session::get('lang_file').'.Items_Ordered'): trans($OUR_LANGUAGE.'.Items_Ordered')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></a> </h4></td>
							
							<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">
                  @php  $pst=0; if(count($productservices)>0){ 
                          
                  @endphp
                              @foreach($productservices as $adopedproductservices)
                            @foreach($adopedproductservices->getProductOptionValue as $addoptedamount) 
                               @php $pst=$pst+currency($addoptedamount->price, 'SAR',$Current_Currency, $format = false); @endphp
                            @endforeach
                            @endforeach
                  @php } @endphp
                  

                      @php  if(count($internal_foods)>0){ @endphp
                            @foreach($internal_foods as $internalfoodcost)
                                 @php 
                                 $itemdishselcted_price=currency($internalfoodcost['final_price'], 'SAR',$Current_Currency, $format = false); 
                                  $pst=$pst+$itemdishselcted_price;
                                 @endphp
                            @endforeach
                     @php } @endphp



              {{ Session::get('currency') }} {{ number_format($pst,2) }}</td>
							</tr>
							
							<tr><td colspan="4" class="hall-more-less"><div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
                           
                            
                            @php   if(count($productservices)>0){ 
                            $k=1; $ij=1; $z=0;
                            
                            //echo $productservices[0]->getProductOptionValue[0]->option_title;
                            @endphp 
                            @foreach($productservices as $adopedproductservices)
                            @foreach($adopedproductservices->getProductOptionValue as $addoptedservices) 
                            @php
                                $ps=currency($addoptedservices->price, 'SAR',$Current_Currency, $format = false);
                               
                                $basetotal=$basetotal+$ps;
                              
                                @endphp
                            <tr class="tr">
                              
                              <td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">
                                
                                 <a href="#" data-id="{{ $addoptedservices->id }}" data-pro="hall_attr" class="cart-delete">&nbsp;</a>

                                {{ $addoptedservices->option_title }}
                                <!-- <a href="#" class="services-edit">&nbsp;</a>--></td>
								<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  </td>
                              <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">Hall Services</td>
                              <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">
                                  @php $item_price=currency($addoptedservices->price, 'SAR',$Current_Currency, $format = false); @endphp
                                {{ Session::get('currency') }} {{ number_format($item_price, 2) }}</td>
                            </tr>
                            @php $k++; $ij++; $z++; @endphp
                            @endforeach
                            @endforeach
                            @php } 
                           
                             //$basetotal=($basetotal*25)/100;
                               $basetotal=$basetotal+currency($productInsuranceamount, 'SAR',$Current_Currency, $format = false);
                            
                            if(count($internal_foods)>0){ 
                              //dd($internal_foods);
                            @endphp
                            @foreach($internal_foods as $internalfood)
                            <tr class="tr">
                             
                              <td class="td td1" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-pro="hall_internal" data-id="{{ $internalfood['dish_id'] }}"  >&nbsp;</a>{{ $internalfood['dish_name'] }}<br />
                                {{ $internalfood['container_title'] }} X {{ $internalfood['quantity'] }}
                                <!--<a href="#" class="services-edit">&nbsp;</a>--></td>
							                 <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">


                            @php
                            $Qran = rand(111,999);
                            
                            $dishID = $internalfood['dish_id'];
                            $IntQty = $internalfood['quantity'];
                            @endphp
                            <div class="quantity-box">
                            <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$dishID or 0}}','basecontainerprice149','remove','{{$Qran}}','internalfood');"></button>


                            <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$IntQty}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$dishID or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','internalfood');" type="number">


                            <button type="button" id="add" class="add" onclick="return pricecalculation('{{$dishID or 0}}','basecontainerprice149','add','{{$Qran}}','internalfood');"></button>
                            </div> 

                                </td>
                              


                              <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}"> </td>


                              <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }}
                                @php $itemselcted_price=currency($internalfood['final_price'], 'SAR',$Current_Currency, $format = false); @endphp
                               {{ number_format($itemselcted_price, 2) }}</td>
                            </tr>
                            @php $basetotal=$basetotal+$itemselcted_price; @endphp
                            @php $k++; @endphp
                            @endforeach
                            @php }  @endphp
                          </table>
                        </div>
                      </div></td></tr>
							
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">{{ (Lang::has(Session::get('lang_file').'.Total_Cost')!= '')  ?  trans(Session::get('lang_file').'.Total_Cost'): trans($OUR_LANGUAGE.'.Total_Cost')}}</td>
							<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">
                @php 
                    $totalcast=$finalprice+$productInsuranceamount+$pst;
                 @endphp
             {{ Session::get('currency') }} {{ number_format($totalcast,2) }}</td>
							</tr>
							
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">{{ (Lang::has(Session::get('lang_file').'.Payable_Amount')!= '')  ?  trans(Session::get('lang_file').'.Payable_Amount'): trans($OUR_LANGUAGE.'.Payable_Amount')}}</td>
							<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">

                 @php   
                    $totalpayable=$advance_price+$productInsuranceamount+$pst;
                 @endphp
              {{ Session::get('currency') }} {{ number_format($totalpayable,2) }}</td>
							</tr>
														
							<tr class="hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">{{ (Lang::has(Session::get('lang_file').'.Balance_Payment')!= '')  ?  trans(Session::get('lang_file').'.Balance_Payment'): trans($OUR_LANGUAGE.'.Balance_Payment')}}</td>
							<td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">&nbsp;</td>
							<td class="td td3 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">&nbsp;</td>
							<td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}"> {{ Session::get('currency') }} {{ number_format($remainingammount,2) }}</td>
							</tr>
							
							</table>
							</td>
							
							
							
                             
                            </tr>
                          </table>
                        
                      </div>
                      @php $k++; } } @endphp
                      
						</div>
                      <!----cart external food ---->
                      @php 
                    
                      if(count(@$cart_fooditem) > 0 || count(@$cart_fooddateitem) > 0 || count(@$cart_fooddessertitem) > 0){ 
                      $f=1;
                      $foodtotal=0;

                      @endphp
                      @php if(count($cart_fooditem)>0){
                        

                       @endphp
                      @foreach($cart_fooditem as $cartfoodbasictotal)
                          @php if(count($cartfoodbasictotal->cart_external_food_dish)>0){

                           @endphp
                            @foreach($cartfoodbasictotal->cart_external_food_dish as $extrenalprice)
                             @php $foodtotal=$foodtotal+$extrenalprice->price; @endphp
                            @endforeach
                              @php }  @endphp
                      @endforeach
                      @php }  @endphp
                     
                      @php if(count($cart_fooddateitem) > 0){ @endphp
                      @foreach($cart_fooddateitem as $cartdatefoodbasicinfo)
                      @php $productdateprice=$cartdatefoodbasicinfo->cart_option_value[0]->quantity * $cartdatefoodbasicinfo->cart_option_value[0]->value;
                      $foodtotal=$foodtotal+$productdateprice; @endphp
                      @endforeach
                      
                      @php }  @endphp
                      
                      @php  if(count($cart_fooddessertitem)>0){ 

                      @endphp
                      @foreach($cart_fooddessertitem as $cartdessertfoodbasicinfo)
                    @php $productdessertprice=$cartdessertfoodbasicinfo->cart_option_value[0]->quantity * $cartdessertfoodbasicinfo->cart_option_value[0]->value;
                      $foodtotal=$foodtotal+$productdessertprice; @endphp
                      @endforeach
                      @php }   @endphp


					  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
                            
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">
                         

                                {{ (Lang::has(Session::get('lang_file').'.EXFood')!= '')  ?  trans(Session::get('lang_file').'.EXFood'): trans($OUR_LANGUAGE.'.EXFood')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 hall-info-price mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{ number_format($foodtotal, 2) }}</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
                            @php if(count(@$cart_fooditem)>0){
                           
                            @endphp
                            @php $k=0; @endphp
                            @foreach($cart_fooditem as $cartfoodbasicinfo)
                            

                            @php  
                            if(Session::get('lang_file')=='ar_lang'){ $protitle=$cartfoodbasicinfo->getProduct[0]->pro_title_ar; }else{ $protitle=$cartfoodbasicinfo->getProduct[0]->pro_title; } 
                            
                            if(Session::get('lang_file')=='ar_lang'){
if(count($cartfoodbasicinfo->branch_info)>=1){
                             $promctitle=$cartfoodbasicinfo->branch_info[0]->mc_name_ar;
}else {   $promctitle= ''; }
                              }else{ 
if(count($cartfoodbasicinfo->branch_info)>=1){
                              $promctitle=$cartfoodbasicinfo->branch_info[0]->mc_name; 
                            }else {   $promctitle= ''; }
                            } 
                            $jk=0;
                            //echo "<pre>";
                            //print_r($cartfoodbasicinfo->cart_external_food_dish);
                            @endphp
                            
                            @foreach($cartfoodbasicinfo->cart_external_food_dish as $orderdishinfo)
                            <tr class="tr">
                      
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">{{ $protitle or '' }}<br>

                                {{ $cartfoodbasicinfo->dish_container_info[$k][0]->short_name or '' }} X {{ $orderdishinfo->quantity or '' }}

                                 @php $k=$k+1; @endphp
                                <!-- <a href="#" class="services-edit">&nbsp;</a>--></td>
								 <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">  @php
              $Qran = rand(111,999);
              @endphp
                 </td>
                              <td class="td td3" data-title="Service Provider">{{ $promctitle or '' }}</td>
                              <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{ number_format($orderdishinfo->price, 2) }}</td>
                            </tr>
                            <?php 

                            $basetotal=$basetotal+$orderdishinfo->price;
                            $jk++; ?>
                            @endforeach

                            @php $f++;
                            
                            @endphp
                            @endforeach
                            @php }  @endphp
                            
                           
                            @php if(count($cart_fooddateitem)>0){ 
                             //echo "<pre>";
                               //print_r($cart_fooddateitem);
                            @endphp
                            @foreach($cart_fooddateitem as $cartdatefoodbasicinfo)
                            @php 
                           $productprice=$cartdatefoodbasicinfo->cart_option_value[0]->quantity * $cartdatefoodbasicinfo->cart_option_value[0]->value;
                          
                            if($cartdatefoodbasicinfo->cart_option_value[0]->product_option_id=='22'){ $wt="Kg"; }else{ $wt="Piece";} 
                            @endphp
                            <tr class="tr">
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">
                                <a href="#" class="cart-delete" data-pro="food" data-id="{{$cartdatefoodbasicinfo->cart_option_value[0]->id or 0}}">&nbsp;</a>
                                {{ $cartdatefoodbasicinfo->getProduct[0]->pro_title or '' }}<br>
                                {{ $cartdatefoodbasicinfo->cart_option_value[0]->quantity or '' }} X {{ $wt or '' }}</td>
							  
                                      <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">  @php
              $Qran = rand(111,999);
              @endphp

                    <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$cartdatefoodbasicinfo->id or 0}}','','remove','{{$Qran}}','food');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$cartdatefoodbasicinfo->cart_option_value[0]->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$cartdatefoodbasicinfo->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','food');" type="number" readonly>
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$cartdatefoodbasicinfo->id or 0}}','','add','{{$Qran}}','food');"></button>
                </div> 
                <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $cartdatefoodbasicinfo->cart_option_value[0]->product_id }}">
                   <span id="{{$cartdatefoodbasicinfo->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>

                 </td>
                	 
                              <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{ $cartdatefoodbasicinfo->shop_info[0]->mc_name or '' }}</td>
                              <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{ number_format($productprice,2) }}</td>
                            </tr>
                            @php $f++;
                           $basetotal=$basetotal+$productprice;
							
                            @endphp
                            @endforeach
                            
                            @php }   @endphp
                            @php if(count($cart_fooddessertitem)>0){ @endphp
                            @foreach($cart_fooddessertitem as $cartdessertfoodbasicinfo)
                            @php   $productpricedessert=$cartdessertfoodbasicinfo->cart_option_value[0]->quantity * $cartdessertfoodbasicinfo->cart_option_value[0]->value;
                            if($cartdessertfoodbasicinfo->cart_option_value[0]->product_option_id=='21' || $cartdessertfoodbasicinfo->cart_option_value[0]->product_option_id=='22'){ $wt3="Kg"; }else{ $wt3="Piece";} 
                            @endphp
                            <tr class="tr">
                           
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">
                                <a href="#" class="cart-delete" data-pro="food" data-id="{{$cartdessertfoodbasicinfo->cart_option_value[0]->id or 0}}">&nbsp;</a>
                                {{ $cartdessertfoodbasicinfo->getProduct[0]->pro_title or '' }}<br> {{ $cartdessertfoodbasicinfo->cart_option_value[0]->quantity or '' }} X {{ $wt3 }}</td>
								 <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">  @php
              $Qran = rand(111,999);
              @endphp


                   <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$cartdessertfoodbasicinfo->id or 0}}','','remove','{{$Qran}}','food');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$cartdessertfoodbasicinfo->cart_option_value[0]->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$cartdessertfoodbasicinfo->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','food');" type="number" readonly>
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$cartdessertfoodbasicinfo->id or 0}}','','add','{{$Qran}}','food');"></button>
                </div> 
                <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $cartdessertfoodbasicinfo->cart_option_value[0]->product_id }}">
                   <span id="{{$cartdessertfoodbasicinfo->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>



                 </td>
                   
                              <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{ $cartdessertfoodbasicinfo->shop_info[0]->mc_name or '' }}</td>
                              <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{ number_format($productpricedessert,2) }}</td>
                            </tr>
                            @php $f++;
                            $basetotal=$basetotal+$productpricedessert;
                            @endphp
                            @endforeach
                            
                            @php }   @endphp
                          </table>
                        </div>
                      </div>
                     </div>
					  @php } @endphp
                      <!----end cart external food ---->
					
                      <!-------- Tailor Start------------------>
					  @php $totalsum = 0; if(count($carttailortitem) > 0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.TAILOR')!= '')  ?  trans(Session::get('lang_file').'.TAILOR'): trans($OUR_LANGUAGE.'.TAILOR')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($carttailortitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($carttailortitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $bodymesurmentdata = Helper::bodymesurment($val->cart_id,$val->product_id);
                $fabric_name_data = Helper::fabricname($val->product_id,$val->fabric_name);
                $shopname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $tailorshopname=$shopname->mc_name;
             }else{
             $tailorshopname=$shopname->mc_name_ar;
           }
               
						   $basetotal = $basetotal + $val->total_price;
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                                <div class="bodymesurment">
                                  <ul>
								  <?php if(isset($fabric_name_data->option_title) && $fabric_name_data->option_title!=''){ ?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.Fabrics')!= '')  ?  trans(Session::get('lang_file').'.Fabrics'): trans($OUR_LANGUAGE.'.Fabrics')}} - {{$fabric_name_data->option_title}} </li>
                                  <?php } ?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')}}- {{$bodymesurmentdata->length}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')}}- {{$bodymesurmentdata->chest_size}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')}}- {{$bodymesurmentdata->waistsize}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')}}- {{$bodymesurmentdata->soulders}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')}}- {{$bodymesurmentdata->neck}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')}}- {{$bodymesurmentdata->arm_length}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>

                                     <li>{{ (Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')}}- {{$bodymesurmentdata->wrist_diameter}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
                                     <!-- <li>{{ (Lang::has(Session::get('lang_file').'.CUTTING')!= '')  ?  trans(Session::get('lang_file').'.CUTTING'): trans($OUR_LANGUAGE.'.CUTTING')}}- 

                                          @php if($bodymesurmentdata->cutting=='Ample'){ @endphp
                                            {{ (Lang::has(Session::get('lang_file').'.AMPLE')!= '')  ?  trans(Session::get('lang_file').'.AMPLE'): trans($OUR_LANGUAGE.'.AMPLE')}}
                                          @php } @endphp
                                     
                                             @php if($bodymesurmentdata->cutting=='Normal'){ @endphp
                                            {{ (Lang::has(Session::get('lang_file').'.NORMAL')!= '')  ?  trans(Session::get('lang_file').'.NORMAL'): trans($OUR_LANGUAGE.'.NORMAL')}}
                                          @php } @endphp

                                           @php if($bodymesurmentdata->cutting=='Curved'){ @endphp
                                           {{ (Lang::has(Session::get('lang_file').'.CURVED')!= '')  ?  trans(Session::get('lang_file').'.CURVED'): trans($OUR_LANGUAGE.'.CURVED')}}
                                          @php } @endphp

                                       </li>-->
                                  </ul>

                                </div>
                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	
 @php
              $Qran = rand(111,999);
              @endphp
               <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','');"></button>
                </div> 

                 </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{ $getVendorName->mer_fname or '' }} /
                {{ $tailorshopname or '' }}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  </div>
					  @php } @endphp
                      <!-------- Tailor end------------------>					
					
                      <!-------- Electronics Start------------------>
					  @php $totalsum = 0; if(count($cartinvitationsitem) > 0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingelectronics">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseelectronics" aria-expanded="true" aria-controls="collapseelectronics">
                          <table width="100%">
                          <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.ELECTRONIC_INVITATION')!= '')  ?  trans(Session::get('lang_file').'.ELECTRONIC_INVITATION'): trans($OUR_LANGUAGE.'.ELECTRONIC_INVITATION')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartinvitationsitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseelectronics" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingelectronics">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($cartinvitationsitem as $val)
						  { 
						   $getVendorName = Helper::getbranchmgr($val->merchant_id);
						   $basetotal = $basetotal + $val->total_price;
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}</td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  </div>
					  @php } @endphp
                      <!-------- Electronics end------------------>				  
					  
					  <!-------- Abaya Start------------------>
					  @php $totalsum = 0;  if(count($cartabayatitem) >0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseabaya" aria-expanded="true" aria-controls="collapseabaya">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.ABAYA')!= '')  ?  trans(Session::get('lang_file').'.ABAYA'): trans($OUR_LANGUAGE.'.ABAYA')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartabayatitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseabaya" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingabaya">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($cartabayatitem as $val)
						  { 
						       $getVendorName     = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
							   $bodymesurmentdata = Helper::bodymesurment($val->cart_id,$val->product_id);
							   $fabric_name_data  = Helper::fabricname($val->product_id,$val->fabric_name);
							   $shopname          = Helper::getparentCat($val->shop_id);
							   if(Session::get('lang_file')=='en_lang')
							   {
							     $tailorshopname=$shopname->mc_name;
							   }
							   else
							   {
							     $tailorshopname=$shopname->mc_name_ar;
						       }			   
						       $basetotal = $basetotal + $val->total_price;
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}
							  <div class="bodymesurment">
                                  <ul>
								  <?php if(isset($fabric_name_data->option_title) && $fabric_name_data->option_title!=''){ ?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.Fabrics')!= '')  ?  trans(Session::get('lang_file').'.Fabrics'): trans($OUR_LANGUAGE.'.Fabrics')}} - {{$fabric_name_data->option_title}} </li>
                                  <?php }if(isset($bodymesurmentdata->length) && $bodymesurmentdata->length!=''){?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.LENGTH')!= '')  ?  trans(Session::get('lang_file').'.LENGTH'): trans($OUR_LANGUAGE.'.LENGTH')}}- {{$bodymesurmentdata->length}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
									<?php } if(isset($bodymesurmentdata->chest_size) && $bodymesurmentdata->chest_size!=''){?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.CHEST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.CHEST_SIZE'): trans($OUR_LANGUAGE.'.CHEST_SIZE')}}- {{$bodymesurmentdata->chest_size}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
									<?php } if(isset($bodymesurmentdata->waistsize) && $bodymesurmentdata->waistsize!=''){?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.WAIST_SIZE')!= '')  ?  trans(Session::get('lang_file').'.WAIST_SIZE'): trans($OUR_LANGUAGE.'.WAIST_SIZE')}}- {{$bodymesurmentdata->waistsize}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
									<?php } if(isset($bodymesurmentdata->soulders) && $bodymesurmentdata->soulders!=''){?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.SHOULDERS')!= '')  ?  trans(Session::get('lang_file').'.SHOULDERS'): trans($OUR_LANGUAGE.'.SHOULDERS')}}- {{$bodymesurmentdata->soulders}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
									<?php } if(isset($bodymesurmentdata->neck) && $bodymesurmentdata->neck!=''){?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.NECK')!= '')  ?  trans(Session::get('lang_file').'.NECK'): trans($OUR_LANGUAGE.'.NECK')}}- {{$bodymesurmentdata->neck}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
									<?php } if(isset($bodymesurmentdata->arm_length) && $bodymesurmentdata->arm_length!=''){?>
                                    <li>{{ (Lang::has(Session::get('lang_file').'.ARM_LENGTH')!= '')  ?  trans(Session::get('lang_file').'.ARM_LENGTH'): trans($OUR_LANGUAGE.'.ARM_LENGTH')}}- {{$bodymesurmentdata->arm_length}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
                                    <?php } if(isset($bodymesurmentdata->wrist_diameter) && $bodymesurmentdata->wrist_diameter!=''){?>
                                     <li>{{ (Lang::has(Session::get('lang_file').'.WRIST_DIAMETER')!= '')  ?  trans(Session::get('lang_file').'.WRIST_DIAMETER'): trans($OUR_LANGUAGE.'.WRIST_DIAMETER')}}- {{$bodymesurmentdata->wrist_diameter}} {{ (Lang::has(Session::get('lang_file').'.CM')!= '')  ?  trans(Session::get('lang_file').'.CM'): trans($OUR_LANGUAGE.'.CM')}}</li>
									 <?php } ?>
                                  </ul>
                                </div>
							  </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">

                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','{{ $val->product_size or ''}}','remove','{{$Qran}}','dress');"></button>
                <input name="itemqty[]" id="qty{{$Qran}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','{{ $val->product_size or ''}}','pricewithqty','{{$Qran}}','dress');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','{{ $val->product_size or ''}}','add','{{$Qran}}','dress');"></button>
                </div> 
                 <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>


              </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  </div>
					  @php } @endphp
                      <!-------- Abaya end------------------>
					  
					  <!-------- Car Rental Start------------------>
					  @php $totalsum = 0; if(count($cartcarrentaltitem) >0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingtwo">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsecar_rental" aria-expanded="true" aria-controls="collapsecar_rental">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.CAR_RENTAL')!= '')  ?  trans(Session::get('lang_file').'.CAR_RENTAL'): trans($OUR_LANGUAGE.'.CAR_RENTAL')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartcarrentaltitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsecar_rental" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingcar_rental">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
              
              //print_r($cartcarrentaltitem);
						  foreach($cartcarrentaltitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
              
               $shopname=Helper::getparentCat($val->shop_id);
						   $basetotal = $basetotal + $val->total_price;


                if(Session::get('lang_file')=='en_lang'){
               $shopname=$shopname->mc_name;
               $getVendorName=$getVendorName->mer_fname;
             }else{
              $shopname=$shopname->mc_name_ar;
              $getVendorName=$getVendorName->mer_fname_ar;
           }


						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}
                                  @php if(count($val->bookedandrentaldate)>0) { 
                                    $ijk=1;
                                  echo "<br>";
                                    foreach($val->bookedandrentaldate as $bookedreturndate){
                                    @endphp

                                        {{ $bookedreturndate->value }} @php if($ijk==1){ echo '-'; } @endphp

                                  @php $ijk++; } } @endphp

                              </td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 @php
              $Qran = rand(111,999);
              @endphp
                 </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName or '' }} / {{ $shopname or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					 </div>
					  @php } @endphp
                      <!-------- Car Rental end------------------>
					 
					  <!-------- Travel Agency Start------------------>
					  @php $totalsum = 0; if(count($carttraveltitem) > 0){ @endphp
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingtravel">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetravel" aria-expanded="true" aria-controls="collapsetravel">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.TRAVEL_AGENCY')!= '')  ?  trans(Session::get('lang_file').'.TRAVEL_AGENCY'): trans($OUR_LANGUAGE.'.TRAVEL_AGENCY')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($carttraveltitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsetravel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtravel">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
              //dd($carttraveltitem);
						  foreach($carttraveltitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
               if(Session::get('lang_file')=='en_lang'){
               $shopname=$shopname->mc_name;
               $getVendorName=$getVendorName->mer_fname;
             }else{
              $shopname=$shopname->mc_name_ar;
              $getVendorName=$getVendorName->mer_fname_ar;
           }

						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                                {{ (Lang::has(Session::get('lang_file').'.NO_OF_PEOPLE')!= '')  ?  trans(Session::get('lang_file').'.NO_OF_PEOPLE'): trans($OUR_LANGUAGE.'.NO_OF_PEOPLE')}} {{$val->quantity or 0}}

                             <br>
 @if(isset($carttraveltitem->product_rent->return_date) && $carttraveltitem->product_rent->return_date !='')

       {{ Carbon\Carbon::parse($carttraveltitem->product_rent->return_date)->format('h:i A d M Y')}}

    @endif                 
                              
                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 @php
              $Qran = rand(111,999);
              @endphp
                


                 </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName or ''}} / {{ $shopname or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					 </div>
					  @php } @endphp
                      <!-------- Travel Agency end------------------>
					  
					  <!-------- Kosha Start------------------>
					  @php $totalsum = 0; if(count($cartcoshaitem) >0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingkosha">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsekosha" aria-expanded="true" aria-controls="collapsekosha">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.KOSHA')!= '')  ?  trans(Session::get('lang_file').'.KOSHA'): trans($OUR_LANGUAGE.'.KOSHA')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							 <td class="td td2 sp-blank"></td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartcoshaitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsekosha" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingkosha">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($cartcoshaitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
                 $shopname=Helper::getparentCat($val->shop_id);
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}</td>
							 

                <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	

              @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','');"></button>
                </div> 

 <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>

  </td>



							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}}  / {{ $shopname->mc_name or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					 </div>
					  @php } @endphp
                      <!-------- Kosha end------------------>
					  
					  <!-------- Photo Graphy Start------------------>
					  @php 

             $totalsum = 0; if(count($cartphotoitem) >0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingphotography">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsephotography" aria-expanded="true" aria-controls="collapsephotography">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.PhotoGraphy')!= '')  ?  trans(Session::get('lang_file').'.PhotoGraphy'): trans($OUR_LANGUAGE.'.PhotoGraphy')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartphotoitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsephotography" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingphotography">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
            // echo "<pre>";
             // print_r($cartphotoitem);
						  foreach($cartphotoitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
               $getbookinginfor=Helper::getbookinginformation($val->cart_id,$val->product_id);
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                               {{ Carbon\Carbon::parse($getbookinginfor->date)->format('d M Y')}}<br>
                                  {{$getbookinginfor->time}}<br>{{$getbookinginfor->elocation}}
                                  
                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	

             @php
              $Qran = rand(111,999);
              @endphp
               

  </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  </div>
					  @php } @endphp
                      <!-------- Photo Graphy end------------------>
					  
					  <!-------- Video Graphy Start------------------>
					  @php $totalsum = 0; if(count($cartvideographyitem) >0){ @endphp
                        <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingvideography">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsevideography" aria-expanded="true" aria-controls="collapsevideography">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.VideoGraphy')!= '')  ?  trans(Session::get('lang_file').'.VideoGraphy'): trans($OUR_LANGUAGE.'.VideoGraphy')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartvideographyitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsevideography" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingvideography">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($cartvideographyitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
               $getbookinginfor=Helper::getbookinginformation($val->cart_id,$val->product_id);
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                               {{ Carbon\Carbon::parse($getbookinginfor->date)->format('d M Y')}}<br>
                                  {{$getbookinginfor->time}}<br>{{$getbookinginfor->elocation}}</td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	

        @php
              $Qran = rand(111,999);
              @endphp            


  </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					 </div>
					  @php } @endphp
                      <!-------- Video Graphy end------------------>
					  
					  
					  <!-------- Roses Start------------------>
					  @php $totalsum = 0; if(count($cartrosesitem) >0){ @endphp
                        <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingroses">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseroses" aria-expanded="true" aria-controls="collapseroses">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.SpecialRoses')!= '')  ?  trans(Session::get('lang_file').'.SpecialRoses'): trans($OUR_LANGUAGE.'.SpecialRoses')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartrosesitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseroses" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingroses">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($cartrosesitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
                //echo $val->product_id.'------------'.$val->cart_id;
               $getroseotions=Helper::getroseotion($val->product_id,$val->cart_id);

             //print_r($getroseotions);

						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>

                                  @php
                                         if(isset($getroseotions) && count($getroseotions)>0){ 
                                         $jk=0;
                                           

                                           foreach($getroseotions as $prodotions){


                                               if($jk==0){
                                                if(Lang::has(Session::get('lang_file').'.Wrapping_Type')!= ''){ $wraptypedesign= trans(Session::get('lang_file').'.Wrapping_Type');  }else{ $wraptypedesign= trans($OUR_LANGUAGE.'.Wrapping_Type'); }  
                                          }else{
                                                 if(Lang::has(Session::get('lang_file').'.Wrapping_Design')!= ''){ $wraptypedesign= trans(Session::get('lang_file').'.Wrapping_Design');  }else{ $wraptypedesign= trans($OUR_LANGUAGE.'.Wrapping_Design'); } 
                                        }
                                                     echo $wraptypedesign.'- '.$prodotions->option_title."<br>";
                                                 $jk++;  }
                                         }
                                  @endphp

                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">

               @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','rose');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','rose');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','rose');"></button>
                </div> 
<input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>

  </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}} </td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  
					  </div>
					  @php } @endphp
                      <!-------- Roses end------------------>
					  
					  <!-------- Special Events Start------------------>
					  @php $totalsum = 0; if(count($carteventsitem) >0){ @endphp
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingevents">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseevents" aria-expanded="true" aria-controls="collapseevents">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.special_event')!= '')  ?  trans(Session::get('lang_file').'.special_event'): trans($OUR_LANGUAGE.'.special_event')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($carteventsitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseevents" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingevents">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($carteventsitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}</td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	

                @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','');"></button>
                </div> 
<input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>

  </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}} </td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					 	</div>
					  @php } @endphp
                      <!-------- Special Events end------------------>
					  
					  <!-------- Reception & Hospitality Start------------------>
					  @php $totalsum = 0; if(count($carthospitalityitem) >0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headinghospitality">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsehospitality" aria-expanded="true" aria-controls="collapsehospitality">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.Reception_andHospitality')!= '')  ?  trans(Session::get('lang_file').'.Reception_andHospitality'): trans($OUR_LANGUAGE.'.Reception_andHospitality')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} 
                                @php
                                 $zk=1;
                                 foreach($carthospitalityitem as $val){ 
                                  if($zk==1){ $twoprice=$val->total_price+$val->worker_price;}else{ $twoprice=$val->total_price;}
                                 $totalsum = $totalsum + $twoprice;
                                  $zk++; } 
                                  echo number_format($totalsum,2); 
                                 @endphp
                              </td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsehospitality" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headinghospitality">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
              $z=1;
						  foreach($carthospitalityitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               if($z==1){ $twprice=$val->total_price+$val->worker_price;}else{ $twprice=$val->total_price;}
						   $basetotal = $basetotal + $twprice;
            $shopname=Helper::getparentCat($val->shop_id);
            $bookinginfo=Helper::getbookinginformation($val->cart_id,$val->shop_id);

             
						  @endphp
                            <tr>							   
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">

                                <a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}} <br>

                                   @php if(isset($bookinginfo->date) && $bookinginfo->date!='')
                                   { 
                                   $ordertime=strtotime($bookinginfo->date);
                                   $orderedtime = date("d M Y",$ordertime);
                                   echo $orderedtime;
                                   } 
                                   @endphp

                                <br>

                                {{ $bookinginfo->time or '' }}<br>
                                {{ $bookinginfo->elocation or '' }}
                                <input type="hidden" name="qty" value="{{$val->quantity or 1}}"></td>

<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> 

                @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','');"></button>
                </div> 
                  <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>

  </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}} </td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $z++; $i++;} @endphp

              @if($carthospitalityitem[0]->noofstaff>0)

                <tr>                 
                              <td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}">

                                 {{ (Lang::has(Session::get('lang_file').'.Worker')!= '')  ?  trans(Session::get('lang_file').'.Worker'): trans($OUR_LANGUAGE.'.Worker')}}<br>
                                

<td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> 
   {{ $carthospitalityitem[0]->noofstaff or ''}}
              
  </td>
                <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}"></td>
                <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{ $carthospitalityitem[0]->worker_price or ''}}</td>
                            </tr>
@endif


                          </table>
                        </div>
                      </div>
					 </div>
					  @php } @endphp
                      <!-------- Reception & Hospitality end------------------>
					  
					  <!-------- Music Start------------------>
					  @php $totalsum = 0; if(count($cartmusictitem) > 0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingmusic">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsemusic" aria-expanded="true" aria-controls="collapsemusic">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.MUSIC')!= '')  ?  trans(Session::get('lang_file').'.MUSIC'): trans($OUR_LANGUAGE.'.MUSIC')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartmusictitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsemusic" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingmusic">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
              
						  foreach($cartmusictitem as $key=>$val){ 
 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
               $shopname=Helper::getparentCat($val->shop_id);
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br/>
                                @if( isset($cartmusictitem->product_rent->rental_date) && $cartmusictitem->product_rent->rental_date !='')
 Booking date/time :  {{$cartmusictitem->product_rent->rental_date}} {{$cartmusictitem->product_rent->rental_time}}<br/>
 Return date/time : {{$cartmusictitem->product_rent->return_date}} {{$cartmusictitem->product_rent->return_time}} 
     
      @endif

                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">

               @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','rose');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','rose');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','rose');"></button>
                </div> 
<input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>

  </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}} </td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  </div>
					  @php } @endphp
                      <!-------- Music end------------------>
					  
					  <!-------- Gold & Jewelry Start------------------>
					  @php $totalsum = 0; if(count($cartgoldtitem) > 0){ @endphp
					    <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headinggold">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsegold" aria-expanded="true" aria-controls="collapsegold">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.GOLD_AND_JEWELRY')!= '')  ?  trans(Session::get('lang_file').'.GOLD_AND_JEWELRY'): trans($OUR_LANGUAGE.'.GOLD_AND_JEWELRY')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartgoldtitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsegold" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headinggold">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;

						  foreach($cartgoldtitem as $val){    
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;


$shopname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $makeptodushopname=$shopname->mc_name;
             }else{
             $makeptodushopname=$shopname->mc_name_ar;
           }






						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br/>
                @if(isset($val->product_size) && $val->product_size!='')
                {{$val->product_size}}
                @endif
                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	

                   @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','');"></button>
                </div> 
<input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
              </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{$makeptodushopname or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  </div>
					  @php } @endphp
                      <!-------- Gold & Jewelry end------------------>
					  
					  <!-------- Perfume Start------------------>
					  @php $totalsum = 0; if(count($cartperfumetitem) > 0){ @endphp
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperfume" aria-expanded="true" aria-controls="collapseperfume">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.PERFUME')!= '')  ?  trans(Session::get('lang_file').'.PERFUME'): trans($OUR_LANGUAGE.'.PERFUME')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartperfumetitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperfume" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperfume">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($cartperfumetitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;

$shopname=Helper::getparentCat($val->shop_id);
             
               $s=$shopname->mc_name;

						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}</td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	
               

               @php
              $Qran = rand(111,999);              
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','');"></button>
                </div> 
                  <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
                 </td>
							  <td class="td td3" data-title="Services">{{$getVendorName->mer_fname or ''}} / {{$s}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					  </div>
					  @php } @endphp
                      <!-------- Perfume end------------------>
					  
					  <!-------- Dress Start------------------>
					  @php $totalsum = 0; if(count($cartdressitem) > 0){ @endphp
                        <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingdress">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsedress" aria-expanded="true" aria-controls="collapsedress">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.DRESSES')!= '')  ?  trans(Session::get('lang_file').'.DRESSES'): trans($OUR_LANGUAGE.'.DRESSES')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartdressitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsedress" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingdress">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
						  @php 
						  $i=1;
						  $k++;
						  foreach($cartdressitem as $val){ 
						   $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
						   $basetotal = $basetotal + $val->total_price;
						  @endphp
                            <tr>
							  
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                                  {{ $val->product_size }}
                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','{{ $val->product_size or ''}}','remove','{{$Qran}}','dress');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','dress');" type="number" readonly>
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','{{ $val->product_size or ''}}','add','{{$Qran}}','dress');"></button>
                </div> 
                <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                   <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
              </td>
							  <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}}</td>
							  <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
							@php $i++;} @endphp
                          </table>
                        </div>
                      </div>
					 </div>
					  @php } @endphp
                      <!-------- Dress end------------------>




 <!-------- Makeup cosmetic Start------------------>
            @php $totalsum = 0; if(count($cartmakeupcosmeticitem) > 0){ @endphp
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsemakeup" aria-expanded="true" aria-controls="collapseperfume">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.MAKEUPCOSMETIC')!= '')  ?  trans(Session::get('lang_file').'.MAKEUPCOSMETIC'): trans($OUR_LANGUAGE.'.MAKEUPCOSMETIC')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	  </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartmakeupcosmeticitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsemakeup" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperfume">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              @php 
              $i=1;
			  $k++;
              foreach($cartmakeupcosmeticitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
                $shopname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $makeptodushopname=$shopname->mc_name;
             }else{
             $makeptodushopname=$shopname->mc_name_ar;
           }
              @endphp
                            <tr>
                
                              <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}} <br>
                                
                              </td>
							  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	

             @php
              $Qran = rand(111,999);
              @endphp
                <div class="quantity-box">
                <button type="button" id="sub" class="sub" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','remove','{{$Qran}}','');"></button>
                <input name="itemqty[]" id="qty{{$Qran or 0}}" value="{{$val->quantity or 0}}" min="1" maxlength="5" onkeyup="isNumberKey(event); pricecalculation('{{$val->id or 0}}','basecontainerprice149','pricewithqty','{{$Qran}}','');" type="number">
                <button type="button" id="add" class="add" onclick="return pricecalculation('{{$val->id or 0}}','basecontainerprice149','add','{{$Qran}}','');"></button>
                </div> 
                  <input type="hidden" name="{{$Qran}}" id="{{$Qran}}" value="{{ $val->product_id }}">
                  <span id="{{$val->id or 0}}{{$Qran}}" style="display: none;">@if (Lang::has(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')!= ''){{trans(Session::get('lang_file').'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@else{{ trans($OUR_LANGUAGE.'.PRODUCT_QUANTITY_NOT_AVAILABLE_MAX')}}@endif</span>
  </td>
                <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or '' }} / {{ $makeptodushopname or '' }}</td>
                <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
              @php $i++;} @endphp
                          </table>
                        </div>
                      </div>
            </div>
			@php } @endphp
                      <!-------- makeupcosmetic end------------------>





<!-------- beauty center Start------------------>
            @php $totalsum = 0; if(count($cartbeautycenteritem) > 0){ $k++; @endphp
                      <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperbeautycenter" aria-expanded="true" aria-controls="collapseperbeautycenter">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.BEAUTY_CENTERS')!= '')  ?  trans(Session::get('lang_file').'.BEAUTY_CENTERS'): trans($OUR_LANGUAGE.'.BEAUTY_CENTERS')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartbeautycenteritem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperbeautycenter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperbeautycenter">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              @php 
              $i=1;
			  $k++;
        //print_r($cartbeautycenteritem);
              foreach($cartbeautycenteritem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $shopname=Helper::getparentCat($val->shop_id);
               if(Session::get('lang_file')=='en_lang'){
               $beautyshopname=$shopname->mc_name;
               if(isset($val->booked_staffandotherinformation->staff_member_name) && $val->booked_staffandotherinformation->staff_member_name!='')
               {
                $wrakername=$val->booked_staffandotherinformation->staff_member_name;
               }
               else
               {
                $wrakername='';
               }
               
             }else{
              $beautyshopname=$shopname->mc_name_ar;

              if(isset($val->booked_staffandotherinformation->staff_member_name_ar) && $val->booked_staffandotherinformation->staff_member_name_ar!='')
              {

              $wrakername=$val->booked_staffandotherinformation->staff_member_name_ar;
              }
              else
              {
              $wrakername=''; 
              }
              }
               
              

               $basetotal = $basetotal + $val->total_price;
              // echo "<pre>";
               //print_r($val);
              @endphp
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" data-id="{{$val->id or 0}}" class="cart-delete">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                  {{ $wrakername or '' }}<br>
                    @php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             @endphp
                  <br> {{ $val->booked_staffandotherinformation->start_time or '' }} </td>
				  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 @php
              $Qran = rand(111,999);
              @endphp
              N/A
                  </td>
                <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $beautyshopname }}</td>
                <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
              @php $i++;} @endphp
                          </table>
                        </div>
                      </div>
            </div>
			@php } @endphp
                      <!-------- beauty center end------------------>

<!-------- spa center Start------------------>
            @php $totalsum = 0; if(count($cartspaitem) > 0){ $k++; @endphp
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingperfume">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperspacenter" aria-expanded="true" aria-controls="collapseperspacenter">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.SPA_CENTERS')!= '')  ?  trans(Session::get('lang_file').'.SPA_CENTERS'): trans($OUR_LANGUAGE.'.SPA_CENTERS')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartspaitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperspacenter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperspacenter">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              @php 
              $i=1;
              //echo "<pre>";
              //print_r($cartspaitem);
              foreach($cartspaitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;

                 $shopname=Helper::getparentCat($val->shop_id);
               if(Session::get('lang_file')=='en_lang'){
                $beautyshopname=$shopname->mc_name;
                if(isset($val->booked_staffandotherinformation->staff_member_name) && $val->booked_staffandotherinformation->staff_member_name!='')
                {
                $workername=$val->booked_staffandotherinformation->staff_member_name;
                }
                else
                {
                $workername='';
                }


                }else{
                $beautyshopname=$shopname->mc_name_ar;
                if(isset($val->booked_staffandotherinformation->staff_member_name_ar) && $val->booked_staffandotherinformation->staff_member_name_ar!='')
                {
                $workername=$val->booked_staffandotherinformation->staff_member_name_ar;
                }
                else
                {
                $workername='';
                }
                }


             //echo "<pre>"; 
              //print_r($val);
              @endphp
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" data-id="{{$val->id or 0}}" class="cart-delete">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                  {{ $workername or '' }}<br>
                    @php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             @endphp

                   {{ $val->booked_staffandotherinformation->start_time or '' }} </td>
                <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 @php
              $Qran = rand(111,999);
              @endphp N/A
                 </td>
		<td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} /
      {{ $beautyshopname or '' }}
    </td>
				
                <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
              @php $i++;} @endphp
                          </table>
                        </div>
                      </div>
           </div>
		    @php } @endphp
                      <!-------- spa center end------------------>


<!-------- saloon center Start------------------>
            @php $totalsum = 0; if(count($cartmen_saloonitem) > 0){ $k++; @endphp
                       <div class="goldencartitems">
					  <div class="panel-heading" role="tab" id="headingpersallon">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperbarbarcenter" aria-expanded="true" aria-controls="collapseperbarbarcenter">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.BARBAR_CENTERS')!= '')  ?  trans(Session::get('lang_file').'.BARBAR_CENTERS'): trans($OUR_LANGUAGE.'.BARBAR_CENTERS')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartmen_saloonitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperbarbarcenter" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingperspacenter">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              @php 
              $i=1;
              foreach($cartmen_saloonitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
             //echo "<pre>";
              //print_r($val);
                $saloonshopdbname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $saloonshopname=$saloonshopdbname->mc_name;
             }else{
              $saloonshopname=$saloonshopdbname->mc_name_ar;
           }
              @endphp
                            <tr>
               
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" data-id="{{$val->id or 0}}" class="cart-delete">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                  {{ $val->booked_staffandotherinformation->staff_member_name or '' }}<br>

                  
                    @php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             @endphp


                  <br> {{ $val->booked_staffandotherinformation->start_time or '' }} </td>
				  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">

           @php
              $Qran = rand(111,999);
              @endphp
               N/A

     </td>
                <td class="td td4" data-title="Services">{{$getVendorName->mer_fname or ''}} / {{ $saloonshopname or '' }}</td>
                <td class="td td4" data-title="Services">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
              @php $i++;} @endphp
                          </table>
                        </div>
                      </div>
           </div>
		    @php } @endphp
                      <!-------- saloon center end------------------>



<!-------- saloon center Start------------------>
            @php $totalsum = 0; if(count($cartmakeup_artistitem) > 0){ $k++; @endphp
			  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingpersallon">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsepermakeup_artist" aria-expanded="true" aria-controls="collapsepermakeup_artist">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.MAKEUP_ARTIST')!= '')  ?  trans(Session::get('lang_file').'.MAKEUP_ARTIST'): trans($OUR_LANGUAGE.'.MAKEUP_ARTIST')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2  sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartmakeup_artistitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsepermakeup_artist" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingpermakeup_artist">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              @php 
              $i=1;
              foreach($cartmakeup_artistitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
             //echo "<pre>";
              //print_r($val);
                $makupshopdbname=Helper::getparentCat($val->shop_id);
                 if(Session::get('lang_file')=='en_lang'){
               $makupshopname=$makupshopdbname->mc_name;
             }else{
              $makupshopname=$makupshopdbname->mc_name_ar;
           }

              @endphp
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" data-id="{{$val->id or 0}}" class="cart-delete">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                  {{ $val->booked_staffandotherinformation->staff_member_name or '' }}<br>

              @php if(isset($val->booked_staffandotherinformation->booking_date) && $val->booked_staffandotherinformation->booking_date!='')
             { 
             $ordertime=strtotime($val->booked_staffandotherinformation->booking_date);
             $orderedtime = date("d M Y",$ordertime);
             echo $orderedtime;
             } 
             @endphp

           <br> {{ $val->booked_staffandotherinformation->start_time or '' }} </td>
				  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	
            N/A
             @php
              $Qran = rand(111,999);
              @endphp
                  </td>
                <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $makupshopname }}</td>
                <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
              @php $i++;} @endphp
                          </table>
                        </div>
                      </div>
           </div>
		    @php } @endphp
            <!---saloon center end-->



            <!--Cosmetics And Laser Start-->
            @php $totalsum = 0; if(count($cartclinic_cosmeticitem) > 0){  @endphp
			  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingpercosmetic">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsepercosmetic" aria-expanded="true" aria-controls="collapsepercosmetic">
                          <table width="100%">
                            <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.COSMETICS_AND_LASER')!= '')  ?  trans(Session::get('lang_file').'.COSMETICS_AND_LASER'): trans($OUR_LANGUAGE.'.COSMETICS_AND_LASER')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2  sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}"> </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartclinic_cosmeticitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapsepercosmetic" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingpermakeup_artist">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              @php 
              $i=1;
              foreach($cartclinic_cosmeticitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);

                 $shopname=Helper::getparentCat($val->shop_id);

               $basetotal = $basetotal + $val->total_price;
             //echo "<pre>";
              //print_r($val);
              @endphp
                            <tr>
                

            
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" class="cart-delete" data-id="{{$val->id or 0}}">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                  {{ $val->booked_staffandotherinformation->booking_date or '' }}, {{ $val->booked_staffandotherinformation->start_time or '' }}<br>
     @if(isset($val->booked_staffandotherinformation->file_no) && $val->booked_staffandotherinformation->file_no!='') 

                  {{ (Lang::has(Session::get('lang_file').'.File_No')!= '')  ?  trans(Session::get('lang_file').'.File_No'): trans($OUR_LANGUAGE.'.File_No')}} <?php if(isset($val->booked_staffandotherinformation->file_no) && $val->booked_staffandotherinformation->file_no!=''){echo $val->booked_staffandotherinformation->file_no;} ?>


 @endif


                </td>
               


				  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 @php
              $Qran = rand(111,999);
              @endphp
                 </td>
                <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}}</td>
                <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
              @php $i++;} @endphp
                          </table>
                        </div>
                      </div>
            </div>
			@php  $k++; } @endphp
            <!-------- Cosmetics And Laser end------------------>


            <!-------- Dental And Dermatology Start------------------>
            @php $totalsum = 0; if(count($cartclinic_skinitem) > 0){  @endphp
			  <div class="goldencartitems">
                      <div class="panel-heading" role="tab" id="headingperskin">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseperskin" aria-expanded="true" aria-controls="collapseperskin">
                          <table width="100%">
                           <tr class="tr hall-info-tr">
							<td class="td td1 mo-td-full-width" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"> {{ (Lang::has(Session::get('lang_file').'.DENTAL_AND_DERMATOLOGY')!= '')  ?  trans(Session::get('lang_file').'.DENTAL_AND_DERMATOLOGY'): trans($OUR_LANGUAGE.'.DENTAL_AND_DERMATOLOGY')}} <span class="plus-service"><i class="more-less glyphicon glyphicon-plus"></i></span></td>
							  <td class="td td2 sp-blank" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 </td>
                              <td class="td td3 sp-blank" data-title="Service Provider"></td>
                              <td class="td td4 mo-td-full-width hall-info-price" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} @php foreach($cartclinic_skinitem as $val){ $totalsum = $totalsum + $val->total_price; } echo number_format($totalsum,2); @endphp</td>
                            </tr>
                          </table>
                          </a> </h4>
                      </div>
                      <div id="collapseperskin" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingpermakeup_skin">
                        <div class="panel-body">
                          <table width="100%" class="child-table">
              @php 
              $i=1;
               
              foreach($cartclinic_skinitem as $val){ 
               $getVendorName = Helper::getbranchmgr($val->getProduct[0]->pro_mr_id);
               $basetotal = $basetotal + $val->total_price;
          $shopname=Helper::getparentCat($val->shop_id);
              @endphp
                            <tr>
                
                 <td class="td td1 mo-td-full-width delete-icon-td" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}} / {{ (Lang::has(Session::get('lang_file').'.PRODUCT')!= '')  ?  trans(Session::get('lang_file').'.PRODUCT'): trans($OUR_LANGUAGE.'.PRODUCT')}}"><a href="#" data-id="{{$val->id or 0}}" class="cart-delete">&nbsp;</a>{{$val->getProduct[0]->pro_title}}<br>
                  {{ $val->booked_staffandotherinformation->booking_date or '' }}, {{ $val->booked_staffandotherinformation->start_time or '' }}<br>
                  <?php if(isset($val->booked_staffandotherinformation->file_no) && $val->booked_staffandotherinformation->file_no!=''){ ?>
                      {{ (Lang::has(Session::get('lang_file').'.File_No')!= '')  ?  trans(Session::get('lang_file').'.File_No'): trans($OUR_LANGUAGE.'.File_No')}} - 
                   <?php echo $val->booked_staffandotherinformation->file_no;} ?></td>
				  <td class="td td2" data-title="{{ (Lang::has(Session::get('lang_file').'.QTY')!= '')  ?  trans(Session::get('lang_file').'.QTY'): trans($OUR_LANGUAGE.'.QTY')}}">	 @php
              $Qran = rand(111,999);
              @endphp
                 </td>
                <td class="td td3" data-title="{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}">{{$getVendorName->mer_fname or ''}} / {{ $shopname->mc_name or ''}}</td>
                <td class="td td4" data-title="{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}">{{ Session::get('currency') }} {{number_format($val->total_price,2)}}</td>
                            </tr>
              @php $i++;} @endphp
                          </table>
                        </div>
                      </div>
            </div>
			@php  $k++; } @endphp
            <!-------- Dental And Dermatology end------------------>
 
                    </div>
                  </div>
                  <!-- panel-group -->
                </div></td>
            </tr>
            <tr class="tr total-prise">
              <td class="td td4" data-title="Total" colspan="5"><span>{{ (Lang::has(Session::get('lang_file').'.TOTAL')!= '')  ?  trans(Session::get('lang_file').'.TOTAL'): trans($OUR_LANGUAGE.'.TOTAL')}}</span> {{ Session::get('currency') }} {{ number_format($basetotal, 2) }}</td>
            </tr>
          </table>
          <!-- table -->
        </div>
        <div class="tatal-services-pay-area">
        <a href="{{url('/checkout')}}">  <input type="submit" value="{{ (Lang::has(Session::get('lang_file').'.PAY_NOW')!= '')  ?  trans(Session::get('lang_file').'.PAY_NOW'): trans($OUR_LANGUAGE.'.PAY_NOW')}}" class="form-btn" /> </a>
        </div>
        <!-- tatal-services-pay-area -->
      </div>
      <!-- tatal-services-inner-area -->
    </div>
  </div>
  <!-- outer_wrapper -->
</div>

<script>
jQuery(document).ready(function(){
    jQuery("button").click(function(){
        jQuery('#halldetails').toggle();
    });
});
</script>
@include('includes.footer')
<script type="text/javascript">
	
	function toggleIcon(e) {
    jQuery(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);

</script>
<style type="text/css">
	
/*******************************
* Does not work properly if "in" is added after "collapse".
* Get free snippets on bootpen.com
*******************************/
    .panel-group .panel {
        border-radius: 0;
		border:0;
        box-shadow: none;
        border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
        padding: 0;
        border-radius: 0;
        color: #212121;
        background-color: #FAFAFA;
        border-color: #EEEEEE;
    }

    .panel-title {
	font-weight:normal;
        font-size: 14px;
    }

    .panel-title > a {
        display: block;
        padding: 0px;
        text-decoration: none;
    }

    .more-less {
        float: right;
        color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #EEEEEE;
    }


</style>
@include('includes.deletemessage')
@include('includes.navbar')
<div class="outer_wrapper">
@include('includes.header')
<div class="inner_wrap">

 
<div class="search-section">
<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>
 @php 
      if(Session::get('searchdata.mainselectedvalue')=='2'){ @endphp
      @include('includes.searchweddingandoccasions')
      @php } @endphp
      @php if(Session::get('searchdata.mainselectedvalue')=='1'){ @endphp
      @include('includes.search')
      @php } @endphp
</div> <!-- search-section -->

 

<div class="page-left-right-wrapper">
@include('includes.mobile-modify')

<div class="page-right-section">

 <!-- budget-menu-outer -->
  
<div class="form_title">
  @if($hallsizetype == 4) 
@if (Lang::has(Session::get('lang_file').'.HOTEL_HALLS')!= '') {{  trans(Session::get('lang_file').'.HOTEL_HALLS') }} @else  {{ trans($OUR_LANGUAGE.'.HOTEL_HALLS') }} @endif
 @endif

  @if($hallsizetype == 5) 
@if (Lang::has(Session::get('lang_file').'.Large_Halls')!= '') {{  trans(Session::get('lang_file').'.Large_Halls') }} @else  {{ trans($OUR_LANGUAGE.'.Large_Halls') }} @endif
 @endif

 @if($hallsizetype == 6) 
@if (Lang::has(Session::get('lang_file').'.Small_Halls')!= '') {{  trans(Session::get('lang_file').'.Small_Halls') }} @else  {{ trans($OUR_LANGUAGE.'.Small_Halls') }} @endif
 @endif



 </div>
   



 <div class="budget-menu">
<div class="budget-menu-box"><input onclick="setGetParameter('within', 1)" type="checkbox" @if($within==1 || ($within=='' && $above=='' && $offer=='')  ) checked="" @endif id="withinbudget" value="withinbudget"> <label for="withinbudget">@if (Lang::has(Session::get('lang_file').'.WITHIN_BUDGET')!= '') {{  trans(Session::get('lang_file').'.WITHIN_BUDGET') }} @else  {{ trans($OUR_LANGUAGE.'.WITHIN_BUDGET') }} @endif
</label></div>
<div class="budget-menu-box"><input onclick="setGetParameter('above', 1)"  type="checkbox" @if($above==1 ) checked="" @endif  id="abovebudget" value="abovebudget"> <label for="abovebudget">@if (Lang::has(Session::get('lang_file').'.ABOVE_BUDGET')!= '') {{  trans(Session::get('lang_file').'.ABOVE_BUDGET') }} @else  {{ trans($OUR_LANGUAGE.'.ABOVE_BUDGET') }} @endif
</label></div>
<div class="budget-menu-box"><input onclick="setGetParameter('offer', 1)"  type="checkbox" @if($offer==1 ) checked="" @endif id="offers" value="offers"> <label for="offers">@if (Lang::has(Session::get('lang_file').'.OFFERS')!= '') {{  trans(Session::get('lang_file').'.OFFERS') }} @else  {{ trans($OUR_LANGUAGE.'.OFFERS') }} @endif
</label></div>
</div>


<script type="text/javascript">
function setGetParameter(paramName, paramValue) 
{
          var uri = window.location.href;
          var re = new RegExp("([?&])" + paramName + "=1", "i"); 
          var separator = uri.indexOf('?') !== -1 ? "&" : "?";
          if (uri.match(re)) {
          var newUr =  uri.replace(re, '$1');
          var newUr =  newUr.replace('&&&&', '');   
          }
          else {
          var newUr = uri + separator + paramName + "=" + paramValue;
          }
          window.location.href = newUr;
} 
</script> 

 
 


 @if($within==1)  

<div class="budget-carousel-area"> 
 
<div class="carousel-row">
    <div class="carousel-heading">@if (Lang::has(Session::get('lang_file').'.WITHIN_BUDGET')!= '') {{  trans(Session::get('lang_file').'.WITHIN_BUDGET') }} @else  {{ trans($OUR_LANGUAGE.'.WITHIN_BUDGET') }} @endif</div>
    <div class="clear"></div>
        <div class="flexslider carousel">
@if(count($shopunderbugetincity)>0) 
          <ul class="slides">
 
  
 @foreach($shopunderbugetincity as $getallcats)
  @php
  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
  @endphp           
        <li>
        <span class="carousel-product-box">
        <span class="carousel-product-img"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}"><img src="{{ $bgImg}}" /></a></span>
        <span class="carousel-product-cont">
        <span class="carousel-product-name"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">{{ $getallcats[0]->mc_name or ''}}</a></span>      
        <span class="carousel-product-view"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">@if (Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= '') {{  trans(Session::get('lang_file').'.VIEW_DETAILS') }} @else  {{ trans($OUR_LANGUAGE.'.VIEW_DETAILS') }} @endif </a></span>
        </span>
        </span>
        </li>     
        
 @endforeach
           
 
          </ul>

          @else
          <div class="no-record"> @if (Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= '') {{  trans(Session::get('lang_file').'.There_is_no_record_available') }} @else  {{ trans($OUR_LANGUAGE.'.There_is_no_record_available') }} @endif</div>
          @endif


        </div>
    </div> <!-- carousel-row -->

@endif


 @if($above==1 )

<div class="carousel-row">
<div class="carousel-heading">@if (Lang::has(Session::get('lang_file').'.ABOVE_BUDGET')!= '') {{  trans(Session::get('lang_file').'.ABOVE_BUDGET') }} @else  {{ trans($OUR_LANGUAGE.'.ABOVE_BUDGET') }} @endif</div>
  <div class="clear"></div>   
    <div class="flexslider carousel">
           @if(count($shopabovebugetincity)>0) 
          <ul class="slides">
 
  
 @foreach($shopabovebugetincity as $getallcats)
  @php
  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
  @endphp           
        <li>
        <span class="carousel-product-box">
        <span class="carousel-product-img"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}"><img src="{{ $bgImg}}" /></a></span>
        <span class="carousel-product-cont">
        <span class="carousel-product-name"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">{{ $getallcats[0]->mc_name or ''}}</a></span>      
        <span class="carousel-product-view"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}/?isabovebusget=1">@if (Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= '') {{  trans(Session::get('lang_file').'.VIEW_DETAILS') }} @else  {{ trans($OUR_LANGUAGE.'.VIEW_DETAILS') }} @endif </a></span>
        </span>
        </span>
        </li>     
        
 @endforeach
           
  
          </ul>
          @else
          <div class="no-record"> @if (Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= '') {{  trans(Session::get('lang_file').'.There_is_no_record_available') }} @else  {{ trans($OUR_LANGUAGE.'.There_is_no_record_available') }} @endif </div>
          @endif
        </div>
  </div>  <!-- carousel-row -->
@endif
 @if($offer==1 ) 
<div class="carousel-row">
<div class="carousel-heading">@if (Lang::has(Session::get('lang_file').'.OFFERS')!= '') {{  trans(Session::get('lang_file').'.OFFERS') }} @else  {{ trans($OUR_LANGUAGE.'.OFFERS') }} @endif</div>
  <div class="clear"></div>   
    <div class="flexslider carousel">
          @if(count($shopofferincity)>0) 
          <ul class="slides">
 
  
 @foreach($shopofferincity as $getallcats)
  @php
  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
  @endphp           
        <li>
        <span class="carousel-product-box">
        <span class="carousel-product-img"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}"><img src="{{ $bgImg}}" /></a></span>
        <span class="carousel-product-cont">
        <span class="carousel-product-name"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">{{ $getallcats[0]->mc_name or ''}}</a></span>      
        <span class="carousel-product-view"><a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">@if (Lang::has(Session::get('lang_file').'.VIEW_DETAILS')!= '') {{  trans(Session::get('lang_file').'.VIEW_DETAILS') }} @else  {{ trans($OUR_LANGUAGE.'.VIEW_DETAILS') }} @endif </a></span>
        </span>
        </span>
        </li>     
        
 @endforeach
           
 
          </ul>
         @else
          <div class="no-record">@if (Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= '') {{  trans(Session::get('lang_file').'.There_is_no_record_available') }} @else  {{ trans($OUR_LANGUAGE.'.There_is_no_record_available') }} @endif </div>
          @endif
        </div>
  </div>  <!-- carousel-row -->   
      </div> <!-- budget-carousel-area -->
@endif
 

@if($within=='' && $above=='' && $offer=='') 

<div class="diamond-area" id="withinbuget">
@php if(count($shopunderbugetincity)>0) { @endphp

<div class="diamond_main_wrapper">

	  <div class="diamond_wrapper_outer">
		<div class="diamond_wrapper_main">
			<div class="diamond_wrapper_inner">
		 
       @php  $i=1; $j=1;  $getC = count($shopunderbugetincity); @endphp
			@php $k=count($shopunderbugetincity);    @endphp
			@foreach($shopunderbugetincity as $getallcats)
  

<!-- TILL 5 RECORD -->
  @if($getC <=5) 
        <div class="row_{{$i}}of{{$getC}} rows{{$getC}}row">
         <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">

          @php
                if($getC<=3)
                  {
                  $bgImg = str_replace('thumb_','',$getallcats[0]->mc_img);  
                  }
                  else
                  {
                  $bgImg = $getallcats[0]->mc_img;  
                  }

           @endphp


            <div class="category_wrapper @if($getC!=5 && $getC!=4  && $getC!=2) category_wrapper{{$i}} @endif" style="background:url({{ $bgImg or '' }});">
              <div class="category_title"><div class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</div><div class="clear"></div></div>
            </div>
          </a>
        </div>
<!-- TILL 6 RECORD -->
  @elseif($getC == 6)
 
          @if($i != 3 && $i != 4) 
          @php if($i==5){ $M=4; } elseif($i==6){ $M=5; }else { $M=$i; } @endphp
          <div class="row_{{$M}}of5 rows5row">
          <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
          <div class="category_wrapper" style="background:url({{ $getallcats[0]->mc_img or '' }});">
          <div class="category_title"><div class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</div></div>
          </div>
          </a>
          </div>  
          @else
          @if($i==3) <div class="row_3of5 rows5row">  @endif
          <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
          <span class="category_wrapper  @if($i==3) category_wrapper2 @else category_wrapper3 @endif" style="background:url({{ $getallcats[0]->mc_img or '' }});">
          <span class="category_title"><span class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</span><span class="clear"></span></span>
          </span>
          </a>

          @if($i==4)  <div class="clear"></div>   
         </div>   @endif  
  @endif
<!-- TILL 7 RECORD -->
  @elseif($getC == 7)

          @if($i != 3 && $i != 4 && $i != 5) 
          @php if($i==6){ $j = 4;} if($i==7){ $j = 5;}  @endphp
          <div class="row_{{$j}}of5 rows5row">
          <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
          <div class="category_wrapper" style="background:url({{ $getallcats[0]->mc_img or '' }});">
          <div class="category_title"><div class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</div></div>
          </div>
          </a>
          </div>  
          @else
          @if($i==3) <div class="row_3of5 rows5row">  @endif
          <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
          <span class="category_wrapper  @if($i==3) category_wrapper4 @elseif($i==4) category_wrapper5 @else category_wrapper6 @endif" style="background:url({{ $getallcats[0]->mc_img or '' }});">
          <span class="category_title"><span class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</span><span class="clear"></span></span>
          </span>
          </a>

          @if($i==5)  <div class="clear"></div>   
          </div>   @endif  
  @endif
 <!-- TILL 8 RECORD -->
 @elseif($getC == 8)
 
        @if($i==1 || $i==8 )
        @php if($i==1) { $k = 1;} if($i==8) { $k = 5;}   @endphp
       
          <div class="row_{{$k}}of5 rows5row">
          <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
          <div class="category_wrapper category_wrapper1" style="background:url({{ $getallcats[0]->mc_img or '' }});">
          <div class="category_title"><div class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</div></div>
          </div>
          </a>   
          </div>
          @else
          @if($i==2 ||$i==4 ||$i==6)   <div class="row_3of5 rows5row"> @endif
          @php if($i==2) { $P = 2;} if($i==3) { $P = 3;}  if($i==4) { $P = 2;}  if($i==5) { $P = 3;}  if($i==6) { $P = 7;}  if($i==7) { $P = 8;}  @endphp

          <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
          <span class="category_wrapper category_wrapper{{$P}}" style="background:url({{ $getallcats[0]->mc_img or '' }});">
          <span class="category_title"><span class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</span><span class="clear"></span></span>
          </span>
          </a>
          @if($i==3 ||$i==5 ||$i==7)  
          <div class="clear"></div>
          </div>@endif
 @endif
  <!-- TILL 9 RECORD -->
@elseif($getC == 9)

        @if($i==1 || $i==9 )
        @php if($i==1){$k=1; } if($i==9){$k=5;   } @endphp
        <div class="row_{{$k}}of5 rows5row">
        <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
        <div class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats[0]->mc_img or '' }});">
        <div class="category_title"><div class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</div></div>
        </div>
        </a>   
        </div>
        @elseif($i==2 || $i==3 )
 
        @if($i==2) <div class="row_2of5 rows5row"> @endif 
        <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
        <span class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats[0]->mc_img or '' }});">
        <span class="category_title"><span class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</span><span class="clear"></span></span>
        </span>
        </a>

        @if($i==3)    <div class="clear"></div>
        </div>
        @endif 

        @elseif($i==4 || $i==5 || $i==6 )


        @if($i==4) <div class="row_3of5 rows5row"> @endif 
        <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
        <span class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats[0]->mc_img or '' }});">
        <span class="category_title"><span class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</span><span class="clear"></span></span>
        </span>
        </a>

        @if($i==6)    <div class="clear"></div>
        </div>
        @endif 

        @elseif($i==7 || $i==8 )

        @if($i==7) <div class="row_4of5 rows5row">@endif 
        <a href="{{ url('') }}/branchlist/{{$halltype}}/{{$typeofhallid}}/{{ $getallcats[0]->mc_id }}">
        <span class="category_wrapper category_wrapper{{$i}}" style="background:url({{ $getallcats[0]->mc_img or '' }});">
        <span class="category_title"><span class="category_title_inner">{{ $getallcats[0]->mc_name or ''}}</span><span class="clear"></span></span>
        </span>
        </a>
       
        @if($i==8) <div class="clear"></div>
        </div> @endif            
        @endif

@endif

   <!-- END ALL LOOP -->

















 
				 @php $i=$i+1; $j=$j+1; @endphp
				@endforeach
				</div>
		</div>
	  </div>
 
  </div>
  
  @php }else{ @endphp	 
	  <div class="diamond_shadow"><strong> {{ (Lang::has(Session::get('lang_file').'.There_is_no_record_available')!= '')  ?  trans(Session::get('lang_file').'.There_is_no_record_available'): trans($OUR_LANGUAGE.'.There_is_no_record_available')}} </strong> </div>
@php } @endphp	
<div class="diamond_shadow"><img src="{{ url('') }}/themes/images/diamond/shadow.png" alt=""></div>

</div>



@endif





    <!-- budget-carousel-area -->


</div> <!-- page-right-section -->




</div> <!-- page-left-right-wrapper -->






</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')


  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var jQuerywindow = jQuery(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 3 : 4;
      }

      jQuery(function() {
        SyntaxHighlighter.all();
      });

      jQuerywindow.load(function() {
        jQuery('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 200,
          itemMargin: 15,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            jQuery('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      jQuerywindow.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());

  </script>

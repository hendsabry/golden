<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content='width=device-width; initial-scale=1.0; user-scalable=0;' name='viewport' /> 
<title>Golden-Cages</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
<link href="{{url('/')}}/themes/css/reset.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/common-style.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/stylesheet.css" rel="stylesheet" />


<link href="{{url('/')}}/themes/slider/css/demo.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/slider/css/flexslider.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/user-my-account.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/interface-media.css" rel="stylesheet" />
<link href="{{url('/')}}/themes/css/diamond.css" rel="stylesheet" />
<!-- custome scroll 
<link href="mousewheel/jquery.mCustomScrollbar.css" rel="stylesheet" />
-->
<!--<link href="css/diamond.css" rel="stylesheet" />
<link href="css/diamond.css" rel="stylesheet" />-->

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script src="{{url('/')}}/themes/js/jquery.flexslider.js"></script>
<script src="{{url('/')}}/themes/js/modernizr.js"></script>
<script src="{{url('/')}}/themes/js/jquery.mousewheel.js"></script>
<script src="{{url('/')}}/themes/js/demo.js"></script>
<script src="{{url('/')}}/themes/js/froogaloop.js"></script>
<script src="{{url('/')}}/themes/js/jquery.easing.js"></script>

<!------------------ tabs----------------->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


<!------------ end tabs------------------>

</head>
<body>
<div class="popup_overlay"></div>

<div class="outer_wrapper">
<div class="vendor_header">
	<div class="inner_wrap">                
        <div class="vendor_header_left">
            <div class="vendor_logo"><a href="javascript:void(0);"><img src="{{url('/')}}/themes/images/vendor-logo.png" alt="" /></a></div>                      	
        </div> <!-- vendor_header_left -->
        
        <div class="vendor_header_right">
        	<div class="vendor_welc">
            	<div class="vendor_name">Welcome <span>@if(Session::get('customerdata.user_name')=='')
			 Guest 
			 @else 
			 {{Session::get('customerdata.user_name')}}
			 @endif</span></div>
                <a href="#" class="vendor_cart"><img src="{{url('/')}}/themes/images/basket.png" /><span>2</span></a>
            </div>
            
            <div class="select_catg">
            	<div class="select_lbl">Other Branches</div>
                <div class="search-box-field">
                	<select class="select_drp">
                    	<option>select</option>
                    </select>
                </div>
            </div>
        </div> <!-- vendor_header_right -->
	</div>
</div><!-- vemdor_header -->

  <div class="common_navbar">
  	<div class="inner_wrap">
    	<div id="menu_header" class="content">
      <ul>
        <li ><a href="#about_shop" class="active">About Shop</a></li>
        <li><a href="#video">Video</a></li>
        <li><a href="#our_client">What Our Clients Say's</a></li>
        <li><a href="#choose_package">Choose Package</a></li>
      </ul>
      </div>
      </div>
    </div>
    <!-- common_navbar -->



<div class="inner_wrap">

<!-- common_navbar -->

<div class="vendor_homepage">

<div class="heading">Our Menu</div>

<div class="vendor_navbar">
	<ul>
	@php $k=1; @endphp
		  @foreach($mainmenuitemtype as $mainmenu)
		  @php if($k==1){ $addact='class="active"'; }else{ $addact=''; } @endphp 
    	<li {{ $addact }}><a href="#{{$k}}" class="venor_active" data-toggle="tab">{{ $mainmenu->menu_name}}</a></li>
		@php $k++; @endphp
		 @endforeach
      
    </ul>
</div>

<div class="vendor_container">






 <div class="tab-content">
 	@php $j=1; $l=0; @endphp
@foreach($mainmenuwithItemAndContainer as $mainmenucatgeoryloop)

 @php if($j==1){ $addactcon='in active'; }else{ $addactcon=''; } @endphp 
<div id="{{ $j }}" class="left_container tab-pane fade {{ $addactcon }}">
	<Div class="subheading">{{ $mainmenucatgeoryloop-> menu_name }}</Div>

	<div class="table desktp">
     	<div class="tr">
          <div class="table_heading th1">{{ (Lang::has(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE')!= '')  ?  trans(Session::get('lang_file').'.DISH_NAME_CONTAINER_TYPE'): trans($OUR_LANGUAGE.'.DISH_NAME_CONTAINER_TYPE')}}</div>
          <div class="table_heading th2">{{ (Lang::has(Session::get('lang_file').'.TOTAL_QUALITY')!= '')  ?  trans(Session::get('lang_file').'.TOTAL_QUALITY'): trans($OUR_LANGUAGE.'.TOTAL_QUALITY')}}</div>
          <div class="table_heading th3">&nbsp;</div>
          <div class="table_heading th4">{{ (Lang::has(Session::get('lang_file').'.PRICE')!= '')  ?  trans(Session::get('lang_file').'.PRICE'): trans($OUR_LANGUAGE.'.PRICE')}}</div>
          
        </div>
	</div>
 <div class="content mCustomScrollbar">
   
    <div class="table">
     @php $z=1; @endphp
    @foreach($mainmenuwithItemAndContainer[$l]['food_list'] as $menudish)      
	 <div class="tr">  

	 	<div class="td td1" data-title="Dish Name / Container Type">
   <div class="item_title">{{ $menudish->dish_name}}</div>
        <div class="item_listing">
		@php $jk=1; @endphp
		 	@foreach($menudish->container_list as $dishcontainer)   
				@php if(count($dishcontainer->container_price)>0){ $isaddable=1;
				
				if($jk==1) { $sclass=''; $basecontainerprice=$dishcontainer->container_price[0]['container_price']; }else{ $sclass=''; }
				@endphp 
				<a href="#" id="{{ $jk }}{{ $menudish->id }}" onClick=" return selectcontainer('basecontainerprice{{ $z}}{{ $menudish->id }}','{{ $menudish->id }}','{{$dishcontainer['id'] }}','{{ $dishcontainer->container_price[0]['container_price'] }}','{{ $jk }}','{{ $dishcontainer['title'] }}');" class="{{ $sclass }}">{{ $dishcontainer['title'] }}</a>
				
				@php $jk++; }else{ $isaddable=0; } @endphp
				
			@endforeach 	<br>
<span id="error{{ $menudish->id }}"></span>
        <!--    <a href="#">B</a>
            <a href="#">C</a>-->
        </div>
     </div>
	    <div class="td td2 quantity" data-title="Total Quality"><button type="button" id="sub" class="sub" onClick="return pricecalculation('{{ $menudish->id }}','basecontainerprice{{ $z}}{{ $menudish->id }}','remove');">-</button>
		<input type="number" id="qty{{ $menudish->id }}" value="1" min="1" max="9" readonly />
		<button type="button" id="add" class="add" onClick="return pricecalculation('{{ $menudish->id }}','basecontainerprice{{ $z}}{{ $menudish->id }}','add');">+</button></div>
	    <div class="td td3 add_categ" data-title="">
		@php if($isaddable==1){ @endphp
		<a href="#" onClick="return addtocart('{{ $menudish->id }}');">ADD</a>
		@php }else{ @endphp
			-
		@php } @endphp
		
		</div>
        <div class="td td4 catg_price" data-title="Price">SAR <span id="basecontainerprice{{ $z }}{{ $menudish->id }}">0.00</span> 
		
		<!----------------- Menu Info--------------->
		<input type="hidden" name="dishid" id="menuid{{ $menudish->id }}" value="{{ $mainmenucatgeoryloop-> id }}">
		<input type="hidden" name="menuname" id="menuname{{ $menudish->id }}" value="{{ $mainmenucatgeoryloop-> menu_name }}">
		
		<!----------------- Menu Dish Info--------------->
		<input type="hidden" name="dishid" id="dishid{{ $menudish->id }}" value="{{ $menudish->id }}">
		<input type="hidden" name="dishname" id="dishname{{ $menudish->id }}" value="{{ $menudish->dish_name}}">
		<!-------------- container info ------------------------>
		<input type="hidden" name="selectedcontainerid" id="selectedcontainerid{{ $menudish->id }}" value="">
		<input type="hidden" name="selectedcontainername" id="selectedcontainername{{ $menudish->id }}" value="">
		
		<input type="hidden" name="selectedcontainerprice" id="selectedcontainerprice{{ $menudish->id }}" value="0.00">
		<input type="hidden" name="dishcontainerbaseprice" id="dishcontainerbaseprice{{ $menudish->id }}" value="0.00">
		</div>
		
	 </div>
	 <span id="alreadyadded{{ $menudish->id }}"></span>
	 @php $z++; $basecontainerprice='0.00'; @endphp
	@endforeach 
	 
	 

          	
	</div><!-- table -->
    
	
	
	
	
	</div>
    
 
    
</div><!-- left_container -->
	@php $j++; $l++; @endphp
@endforeach





</div>





<div class="right_container">
<Div class="subheading">{{ (Lang::has(Session::get('lang_file').'.CONTAINER_LIST')!= '')  ?  trans(Session::get('lang_file').'.CONTAINER_LIST'): trans($OUR_LANGUAGE.'.CONTAINER_LIST')}}</Div>

<div class="service_inner_wrap">
	
	
	
	@foreach($containerlistitem as $allcontainerlist)
		<div class="container_list_row">
    	<div class="food_container_listimg"><img src="{{ $allcontainerlist->img }}"></div>
        <div class="food_container_txt">
        	
            <span>No. of People {{ $allcontainerlist->no_people }}</span>
        </div>
        <div class="fodd_cont_catg">{{ $allcontainerlist->title }}</div>
    </div>
   @endforeach 

</div><!-- service_inner_wrap -->

</div><!-- right_container -->

<a name="order_summary1" class="linking">&nbsp;</a>

<div class="order_summary_bar">
<Div class="subheading">{{ (Lang::has(Session::get('lang_file').'.ORDER_SUMMARY')!= '')  ?  trans(Session::get('lang_file').'.ORDER_SUMMARY'): trans($OUR_LANGUAGE.'.ORDER_SUMMARY')}}</Div>

<div class="order_sum_list" id="myContainer">

</div>

<div class="total_food_cost">
	<div class="total_price">Total Price: <span>SAR <span id="pricetotalamount">0.00</span></span></div>
</div>    

<div class="btn_row">
<input type="hidden" name="totalprice" id="totalprice" value="0.00">
<input type="hidden" name="cartintemid[]" id="cartintemid" value="0">
<input type="button" value="Add to Cart" class="form-btn addto_cartbtn"></div>    
    
</div> <!-- order_summary_bar -->


</div><!-- vendor_container -->

</div> <!-- vendor_homepage -->





</div> <!-- outer_wrapper -->
</div>
@include('includes.footer')

<script language="javascript">

var dishidarr=[0];
 dishidarr.clear();
function addtocart(menudishid){
	var totalamount=0;
	var menuid=document.getElementById('menuid'+menudishid).value;
	var menuname=document.getElementById('menuname'+menudishid).value;
	var dishid=document.getElementById('dishid'+menudishid).value;
	var dishname=document.getElementById('dishname'+menudishid).value;
	var selectedcontainerid=document.getElementById('selectedcontainerid'+menudishid).value;
	var selectedcontainername=document.getElementById('selectedcontainername'+menudishid).value;
	var itemqty=document.getElementById('qty'+menudishid).value;
	var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
	var dishcontainerbaseprice=document.getElementById('dishcontainerbaseprice'+menudishid).value;
	var totalprice=document.getElementById('totalprice').value;
	
	 var totalamount=parseInt(totalprice)+parseInt(selectedcontainerprice);
	 document.getElementById('totalprice').value=totalamount;
	 var isalreadyadded=dishidarr.includes(dishid);
	 if(isalreadyadded===false)
	 		{
				 dishidarr.push(dishid);
				console.log(dishidarr);
				 let ndisharr=dishidarr;
				 dishidarr.toString();
				 document.getElementById('cartintemid').value=dishidarr;
					document.getElementById('pricetotalamount').innerHTML = totalamount;
							if(selectedcontainerprice==0.00)
							{
							document.getElementById('error'+dishid).innerHTML = 'please select container first';
							document.getElementById('qty'+dishid).value = 1;
							}
						
							else
							
							{
							  var div = document.createElement('div');
							div.className = 'order_sumrow';
								div.innerHTML ='<div class="order_sum_title">'+dishname+'</div><div class="order_subcol2">'+selectedcontainername+' x '+itemqty+'</div><div class="order_subcol2">SAR '+selectedcontainerprice+'</div><input type="button" value="X" onclick="removeRow(this,'+menudishid+','+ndisharr+')">';
								document.getElementById('myContainer').appendChild(div);
							}
				
			}else{
						document.getElementById('alreadyadded'+menudishid).innerHTML = 'This item alreday added in cart';
			}
}


function removeRow(input,menudishid,disharr) {
	alert(disharr.lengh);
  var index = dishidarr.indexOf(menudishid);
  alert(index);
	if (index > -1) {
	  array.splice(index, 1);
	}
    document.getElementById('myContainer').removeChild(input.parentNode);
	var selectedcontainerprice=document.getElementById('selectedcontainerprice'+menudishid).value;
	var totalprice=document.getElementById('totalprice').value;
	 var totalamount=parseInt(totalprice)- parseInt(selectedcontainerprice);
	 document.getElementById('totalprice').value=totalamount;
	 document.getElementById('pricetotalamount').innerHTML = totalamount;
}

</script>




<script language="javascript">

$('.add').click(function () {
		if ($(this).prev().val() < 99) {
    		$(this).prev().val(+$(this).prev().val() + 1);
		}
});
$('.sub').click(function () {
		if ($(this).next().val() > 1) {
    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});

</script>



<script language="javascript">
function pricecalculation(dishid,k,act){
		var selectedcontainerprice=document.getElementById('dishcontainerbaseprice'+dishid).value;
					if(selectedcontainerprice==0.00)
					{
					document.getElementById('error'+dishid).innerHTML = 'please select container first';
					document.getElementById('qty'+dishid).value = 1;
					}
				
				else
				
				{
				
		 		var did='qty'+dishid;
				var no=1;
				var currentquantity=document.getElementById(did).value;
				if(act=='add'){
				
				var qty= parseInt(currentquantity)+parseInt(no);
				}else{
					if(parseInt(currentquantity)==1){
					var qty=parseInt(currentquantity)
					}else{
					var qty=parseInt(currentquantity)-parseInt(no);
						}
				}
				
				var finalprice=qty*selectedcontainerprice;
				var inputboxcontanerid='selectedcontainerprice'+dishid;
				document.getElementById(k).innerHTML = finalprice;
				document.getElementById(inputboxcontanerid).value = finalprice;
			}
}

</script>

<script language="javascript">
function selectcontainer(k,dishid,containerid,containerprice,z,containername)
		{
					var classid=''+z+dishid;
					var inputboxcontanerid='selectedcontainerprice'+dishid;
					var dishcontainerbaseprice='dishcontainerbaseprice'+dishid;
					var selectedcontainername='selectedcontainername'+dishid;
					var selectedcontainerid='selectedcontainerid'+dishid;
					var i;
						for (i = 1; i < z; i++) { 
						var rid=''+i+dishid;
							document.getElementById(rid).classList.remove("listing_active");
						}
				
				document.getElementById(selectedcontainername).value = containername;
				document.getElementById(selectedcontainerid).value = containerid;
						document.getElementById(classid).classList.add("listing_active");
					document.getElementById(k).innerHTML = containerprice;
					document.getElementById(inputboxcontanerid).value = containerprice;
					document.getElementById(dishcontainerbaseprice).value = containerprice;
					document.getElementById('error'+dishid).innerHTML='';
					document.getElementById('qty'+dishid).value = 1;
		}

</script>


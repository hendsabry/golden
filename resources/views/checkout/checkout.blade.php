@include('includes.navbar')

@php 

global $Current_Currency;

$Current_Currency  = Session::get('currency'); 

if($Current_Currency =='') { 

$Current_Currency = 'SAR'; 

} 

@endphp

<div class="outer_wrapper">@include('includes.header')

  <div class="inner_wrap"> 

  <div class="search-section">

<div class="mobile-back-arrow"><img src="{{ url('') }}/themes/{{ url('') }}/themes/images/back-arrow.png" alt="" /></div>



  @php $basecategory_ids =Session::get('searchdata.mainselectedvalue');  @endphp 



  @if($basecategory_ids ==2)

@include('includes.checkoutsearch')

@else

@include('includes.checkoutbussinesssearch')

@endif



</div>

    <!-- search-section -->

    <div class="page-left-right-wrapper main_category not-sp">

   



        @if ($errors->any())

        <div class="alert alert-danger">

        <ul>

        @foreach ($errors->all() as $error)

        <li>{{ $error }}</li>

        @endforeach

        </ul>

        </div>

        @endif

      {!! Form::open(['route' => 'chkoutinfo', 'method' => 'post', 'name'=>'chekoutinfo', 'id'=>'chekoutinfo']) !!} 

         <div class="checkout-area">

   

   <div class="checkout-left-area">

   <div class="checkout-page-heading">{{ (Lang::has(Session::get('lang_file').'.Personal_Info')!= '')  ?  trans(Session::get('lang_file').'.Personal_Info'): trans($OUR_LANGUAGE.'.Personal_Info')}}</div>

   <div class="checkout-box">

   <div class="checkout-form">

   <div class="checkout-form-row">

   <div class="checkout-form-cell">

   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Name')!= '')  ?  trans(Session::get('lang_file').'.Name'): trans($OUR_LANGUAGE.'.Name')}}</div> 

   <div class="checkout-form-bottom"><input type="text" name="uname" value="@php if(isset($User->cus_name) && $User->cus_name!=''){echo $User->cus_name;}else{echo $getcustomershippingaddress->ship_name;}@endphp" maxlength="60" class="t-box" /></div> 

   </div> <!-- checkout-form-cell -->

   

   <div class="checkout-form-cell">

   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Email')!= '')  ?  trans(Session::get('lang_file').'.Email'): trans($OUR_LANGUAGE.'.Email')}}</div> 

   <div class="checkout-form-bottom"><input type="email" name="email"  value="{{$User->email}}" maxlength="60" class="t-box" /></div> 

   </div> <!-- checkout-form-cell -->

   <?php if(isset($getcustomershippingaddress->ship_phone) && $getcustomershippingaddress->ship_phone!=''){$rec_array = explode('-',$getcustomershippingaddress->ship_phone);} ?>

   </div> <!-- checkout-form-row --> 

   <div class="checkout-form-row">

   <div class="checkout-form-cell country_row">

   <div class="checkout-form-top">  {{ (Lang::has(Session::get('lang_file').'.Telephone_Number')!= '')  ?  trans(Session::get('lang_file').'.Telephone_Number'): trans($OUR_LANGUAGE.'.Telephone_Number')}}</div> 

   <div class="checkout-form-bottom">

    @php 
    $concode=''; 
    if(isset($User->cus_phone) && $User->cus_phone!=''){$cusphone = $User->cus_phone;}else{$cusphone = $getcustomershippingaddress->ship_phone;}
    $getphone = explode('-',$cusphone);   
    @endphp

    <select name="country_code" id="country_code" class="t-box checkout-small-box countrycode check_count">

      @foreach($getCountry as $Ccode)

        <option value="{{ $Ccode->country_code}}" @php if(isset($getphone[0]) && $getphone[0]==$Ccode->country_code){ @endphp selected @php } @endphp>+{{ $Ccode->country_code}}</option>     

        @endforeach

    </select>

  
   
   <input type="text" value="@php if(isset($getphone[1]) && $getphone[1]!=''){echo $getphone[1];}else{echo $getcustomershippingaddress->ship_phone;} @endphp" name="phone" maxlength="10" onkeypress="return isNumber(event)"  class="t-box checkout-small-box check_input" />

   </div> 

   <label for="country_code" class="error"></label>

   <label for="phone" class="error"></label>

   </div> <!-- checkout-form-cell -->

   <div class="checkout-form-cell">

   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Gender')!= '')  ?  trans(Session::get('lang_file').'.Gender'): trans($OUR_LANGUAGE.'.Gender')}}</div> 

   <div class="checkout-form-bottom">

   <div class="gender-radio-box"><input id="male" <?php if(isset($User->gender) && $User->gender==0){echo 'Checked';} ?> name="gender" value="0" type="radio"> <label for="male">{{ (Lang::has(Session::get('lang_file').'.Male')!= '')  ?  trans(Session::get('lang_file').'.Male'): trans($OUR_LANGUAGE.'.Male')}} </label></div>

   <div class="gender-radio-box"><input id="female" name="gender" <?php if(isset($User->gender) && $User->gender==1){echo 'Checked';} ?> value="1" type="radio"> <label for="female">{{ (Lang::has(Session::get('lang_file').'.Female')!= '')  ?  trans(Session::get('lang_file').'.Female'): trans($OUR_LANGUAGE.'.Female')}} </label></div>

   <label for="gender" class="error"></label>

   </div> 

   </div> <!-- checkout-form-cell -->

   </div> <!-- checkout-form-row --> 

   

    @php   



      $days = '0';

      $mon = '0';

      $yr = '0';

       

      @endphp



   <div class="checkout-form-row">

   <?php /*?>

   <div class="checkout-form-cell">

   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Date_of_Birth')!= '')  ?  trans(Session::get('lang_file').'.Date_of_Birth'): trans($OUR_LANGUAGE.'.Date_of_Birth')}}</div> 

   <div class="checkout-form-bottom checkout-dob"> 

    <select name="day">

  <option value="">{{ (Lang::has(Session::get('lang_file').'.DD')!= '')  ?  trans(Session::get('lang_file').'.DD'): trans($OUR_LANGUAGE.'.DD')}}</option>

  @for($i=1;$i<=31;$i++)

  @php if($i<=9){ $k= '0'.$i; }else{ $k= $i; } @endphp  

  <option  value="{{$k}}" @if($days== $k) {{'SELECTED'}}  @endif >{{$k}}</option>

  @endfor

  </select>

  <select name="month">

  <option value="">{{ (Lang::has(Session::get('lang_file').'.MM')!= '')  ?  trans(Session::get('lang_file').'.MM'): trans($OUR_LANGUAGE.'.MM')}}</option>

  @for($i=1;$i<=12;$i++)

  @php if($i<=9){ $k= '0'.$i; }else{ $k= $i; } @endphp  

  <option  value="{{$k}}"  @if($mon== $k) {{'SELECTED'}}  @endif >{{$k}}</option>

  @endfor

  </select>

  <select name="year">

  <option value="">  {{ (Lang::has(Session::get('lang_file').'.YYYY')!= '')  ?  trans(Session::get('lang_file').'.YYYY'): trans($OUR_LANGUAGE.'.YYYY')}}</option>

@for($i=(date("Y")-5);$i >=1960;$i--)

  <option  value="{{$i}}"  @if($yr== $i) {{'SELECTED'}}  @endif >{{$i}}</option>

  @endfor

  </select></div> 

  <label for="day" class="error"></label>  <label for="month" class="error"></label>  <label for="year" class="error"></label>

   </div> <!-- checkout-form-cell -->

   

   <div class="checkout-form-cell">

                <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.DATE_OF_BIRTH')!= '')  ?  trans(Session::get('lang_file').'.DATE_OF_BIRTH'): trans($OUR_LANGUAGE.'.DATE_OF_BIRTH')}}</div>

                <div class="checkout-form-bottom">

                <input type="text" class="t-box cal-t checkout-small-box" name="dob" id="dob" value="{{ $User->dob or ''}}" autocomplete="off"/>

                </div>

                @if($errors->has('dob'))<span class="error">{{ $errors->first('dob') }}</span>@endif

              </div><?php */?>

    <div class="checkout-form-cell">

   <div class="checkout-form-top"> {{ (Lang::has(Session::get('lang_file').'.Country')!= '')  ?  trans(Session::get('lang_file').'.Country'): trans($OUR_LANGUAGE.'.Country')}}</div> 

   <div class="checkout-form-bottom not-edit">



@php if(Session::get('lang_file')!='en_lang') { $lang='ar'; } else{ $lang='en'; }  @endphp

<select class="checkout-small-box" name="country"  id="country" onchange="getCity(this.value,'{{$lang}}');getVat(this.value,'{{$lang}}')">

  <option value="">  {{ (Lang::has(Session::get('lang_file').'.Select')!= '')  ?  trans(Session::get('lang_file').'.Select'): trans($OUR_LANGUAGE.'.Select')}} </option>

  @foreach($getCountry as $Ctry)

  <option value="{{$Ctry->co_id}}">@if(Session::get('lang_file')!='en_lang') {{$Ctry->co_name_ar}} @else {{$Ctry->co_name}}   @endif</option>

  @endforeach

  </select>





   </div> 

   </div> <!-- checkout-form-cell -->

   <div class="checkout-form-cell">

   <div class="checkout-form-top"> {{ (Lang::has(Session::get('lang_file').'.City')!= '')  ?  trans(Session::get('lang_file').'.City'): trans($OUR_LANGUAGE.'.City')}}</div> 

   <div class="checkout-form-bottom">



<select class="checkout-small-box" id="city" name="city">

<option value="">{{ (Lang::has(Session::get('lang_file').'.Select')!= '')  ?  trans(Session::get('lang_file').'.Select'): trans($OUR_LANGUAGE.'.Select')}}</option>

 



</select>



</div> 

   </div>

   

   </div><!-- checkout-form-row --> 





   <div class="checkout-form-row">

  

   <div class="checkout-form-cell address-cell">

   <div class="checkout-form-top">{{ (Lang::has(Session::get('lang_file').'.Address')!= '')  ?  trans(Session::get('lang_file').'.Address'): trans($OUR_LANGUAGE.'.Address')}}</div> 

   <div class="checkout-form-bottom">



<textarea name="address1" id="address1" rows="3" cols="40" maxlength="350">@php if(isset($User->cus_address1) && $User->cus_address1!=''){echo $User->cus_address1;}else{echo $getcustomershippingaddress->ship_address1;}@endphp</textarea>



 </div> 

   <label for="address1" class="error"></label>

   </div>

    <!-- checkout-form-cell -->

   

   <div class="checkout-form-cell">

   <div class="checkout-form-top"> {{ (Lang::has(Session::get('lang_file').'.Pin_Code')!= '')  ?  trans(Session::get('lang_file').'.Pin_Code'): trans($OUR_LANGUAGE.'.Pin_Code')}}</div> 

   <div class="checkout-form-bottom"><input type="text" id="pincode" name="pincode" value="@php if(isset($User->ship_postalcode) && $User->ship_postalcode!=''){echo $User->ship_postalcode;}else{echo $getcustomershippingaddress->ship_postalcode;}@endphp" maxlength="7" class="t-box checkout-small-box" /></div> 

   </div>

   

   

   </div> <!-- checkout-form-row --> 

   

   <div class="checkout-form-row">

    <!-- checkout-form-cell -->

   







    <!-- checkout-form-cell -->

   </div> <!-- checkout-form-row --> 

   

   </div> <!-- checkout-form -->

   

   </div> <!-- checkout-box -->



 



<div class="checkout-page-heading" id="shipbox">{{ (Lang::has(Session::get('lang_file').'.Select_Shipping_Companies_of_Following_Product')!= '')  ?  trans(Session::get('lang_file').'.Select_Shipping_Companies_of_Following_Product'): trans($OUR_LANGUAGE.'.Select_Shipping_Companies_of_Following_Product')}}</div>



<div class="checkout-box"  id="shipbox1"> 



@php $K=1; $ChkShip=0; @endphp

@foreach($vendorList as $val)

@php

$Sub = $val->cart_sub_type;

@endphp

<div class="check_shipping_area">

 <div class="shipping-companies-heading"> 

@php echo $name = $val->fname.' '.$val->lname; @endphp

</div>





@foreach($Sub as $SubProv)

@php 

$aramxe = $SubProv->products[0]->shipping_methods['aramax'];

  $merchant_id = $SubProv->products[0]->merchant_id;

$picks = $SubProv->products[0]->shipping_methods['pick_logistics'];

if($aramxe ==0 && $picks==0){ continue; }

@endphp

 

  <div class="shipping-companies-wrapper">

  <div class="shipping-companies-heading">    

  @php echo ucfirst($SubProv->cart_sub_type); @endphp

  </div>     



@if($aramxe==1)

  <!-- ARAMEX SHIPPING -->

@php $ChkShip=$ChkShip+1; @endphp

    <div class="shipping-companies-line">

    <div class="scl1">





    <div class="scl1-radio"><input type="radio" data-price="0"  data-ship="picks_{{$SubProv->products[0]->product_id}}_{{$K}}" data-aship="Aramex_{{$SubProv->products[0]->product_id}}_{{$K}}" data-value="1" data-merchant_id="{{$merchant_id}}" data-cart_sub_type="{{$SubProv->cart_sub_type}}" data-productid="{{$SubProv->products[0]->product_id}}" class="shiping" required   name="shipping_{{$SubProv->products[0]->product_id}}_{{$K}}" 

      id="Aramex_{{$SubProv->products[0]->product_id}}_{{$K}}" value="0" /> <label>&nbsp;</label></div>

    <div class="scl1-img"><label for="Aramex_{{$SubProv->products[0]->product_id}}_{{$K}}"><img src="{{url('/')}}/public/assets/img/aramex.png" alt="" /></label></div>

    <div class="scl1-name"><label for="Aramex_{{$SubProv->products[0]->product_id}}_{{$K}}">{{ (Lang::has(Session::get('lang_file').'.Aramex')!= '')  ?  trans(Session::get('lang_file').'.Aramex'): trans($OUR_LANGUAGE.'.Aramex')}}</label></div>

    </div>





    <span id="Aramex_priceCalc_{{$SubProv->products[0]->product_id}}">

    <div class="scl2">{{ Session::get('currency') }} 0</div> 

    </span>

    

    <label class="error" for="shipping_{{$SubProv->products[0]->product_id}}_{{$K}}" style="margin-top:8px;"></label>



    </div> <!-- shipping-companies-line -->

@endif



@if($picks==1)

  <!-- picklogistics SHIPPING -->

   @php $ChkShip=$ChkShip+1; @endphp

   <div class="shipping-companies-line">  

   <div class="scl1 ">

   <div class="scl1-radio"><input type="radio"  data-price="0" data-ship="picks_{{$SubProv->products[0]->product_id}}_{{$K}}" data-aship="Aramex_{{$SubProv->products[0]->product_id}}_{{$K}}" data-value="2"  data-productid="{{$SubProv->products[0]->product_id}}" class="shiping"  data-merchant_id="{{$merchant_id}}" name="shipping_{{$SubProv->products[0]->product_id}}_{{$K}}" required="" id="picks_{{$SubProv->products[0]->product_id}}_{{$K}}" data-cart_sub_type="{{$SubProv->cart_sub_type}}" value="0" /> <label>&nbsp;</label></div>

   <div class="scl1-img"><label for="picks_{{$SubProv->products[0]->product_id}}_{{$K}}"><img src="{{url('/')}}/public/assets/img/picklogistics.png" alt="" /></label></div>

   <div class="scl1-name"><label for="picks_{{$SubProv->products[0]->product_id}}_{{$K}}">{{ (Lang::has(Session::get('lang_file').'.Pick_Logistics')!= '')  ?  trans(Session::get('lang_file').'.Pick_Logistics'): trans($OUR_LANGUAGE.'.Pick_Logistics')}} </label></div>

   </div>

 <span id="Picks_priceCalc_{{$SubProv->products[0]->product_id}}">

    <div class="scl2">{{ Session::get('currency') }} 0</div> 

    </span>

   </div> <!-- shipping-companies-line -->





@endif



 <label for="shipping_1013" class="error"></label>

   </div> <!-- shipping-companies-wrapper -->

@php $K=$K + 1; @endphp

@endforeach

</div>

@endforeach 

</div>

  



@php

 $shippingneed =0;

 $shipp = array('music','shopping','occasion');

 $M=1;

 $ishallincart=0;

 foreach($cart_pro as $valss)

 {

  $Ctype = $valss->cart_type;

  if($Ctype=='hall' || $Ctype=='travel'){ $ishallincart=1; }

  $cartsub_type = $valss->cart_sub_type;

  if($cartsub_type=='invitations'){ $ishallincart=1; }

  if($cartsub_type == 'invitations'){ continue;}

  if(!in_array($Ctype,$shipp)){ continue;}else{  $M = $M+1; }

 

 }

 

@endphp

 



 <div class="checkout-page-heading"> {{ (Lang::has(Session::get('lang_file').'.Payment_Option')!= '')  ?  trans(Session::get('lang_file').'.Payment_Option'): trans($OUR_LANGUAGE.'.Payment_Option')}}</div>

   <div class="checkout-box po-checkout">

   <div class="payment-option-area">





 <div class="shipping-companies-wrapper">



   <div class="shipping-companies-line rds" id="creditCrd">

   <div class="scl1 notpadding">

   <div class="scl1-radio"><input type="radio" name="payment" id="cc" value="1" /> <label>&nbsp;</label></div> 

   <div class="scl1-name"><label for="cc">{{ (Lang::has(Session::get('lang_file').'.Credit_Debit_Cards')!= '')  ?  trans(Session::get('lang_file').'.Credit_Debit_Cards'): trans($OUR_LANGUAGE.'.Credit_Debit_Cards')}} </label></div>

   </div>   

   </div> <!-- shipping-companies-line -->

   



 



   <div class="shipping-companies-line rds" id="wallet">

   <div class="scl1 notpadding">

   <div class="scl1-radio"><input type="radio" name="payment" id="wlt" value="2" /> <label>&nbsp;</label></div> 

   <div class="scl1-name"><label for="wlt">{{ (Lang::has(Session::get('lang_file').'.Mobile_Wallets')!= '')  ?  trans(Session::get('lang_file').'.Mobile_Wallets'): trans($OUR_LANGUAGE.'.Mobile_Wallets')}} </label></div>

   </div>   

   </div> <!-- shipping-companies-line -->



@if($ChkShip >=1 && $ishallincart<1)

   <div class="shipping-companies-line rds" id="Wiretfr">

   <div class="scl1 notpadding">

   <div class="scl1-radio"><input type="radio" id="wt" name="payment" value="3" /> <label>&nbsp;</label></div> 

   <div class="scl1-name"><label for="wt">{{ (Lang::has(Session::get('lang_file').'.Wire_Transfer')!= '')  ?  trans(Session::get('lang_file').'.Wire_Transfer'): trans($OUR_LANGUAGE.'.Wire_Transfer')}} </label></div>

   </div>   

   </div> <!-- shipping-companies-line -->

@endif

   <label for="payment" class="error"></label>

   </div> <!-- shipping-companies-wrapper -->

    





   <div class="payment-option-prise" id="walletdis"><span class="pay_great">{{ (Lang::has(Session::get('lang_file').'.Wallet_Deduct')!= '')  ?  trans(Session::get('lang_file').'.Wallet_Deduct'): trans($OUR_LANGUAGE.'.Wallet_Deduct')}} </span><span id="wltddcut"> </span> </div>



   <div class="payment-option-prise" id="total_amtdis">{{ Session::get('currency') }} <span id="oTotal"> </span> </div>







   <div class="payment-option-btn"><input type="submit" value="{{ (Lang::has(Session::get('lang_file').'.Pay_Now')!= '')  ?  trans(Session::get('lang_file').'.Pay_Now'): trans($OUR_LANGUAGE.'.Pay_Now')}}" class="form-btn btn-info-wisitech" /></div>

   

   </div> <!-- payment-option-area -->

   </div> <!-- checkout-box -->

   

    </div> <!-- checkout-left-area -->



  <div class="checkout-right-area">



@php

$iscoupan=0;

foreach($cart_pro as $vals)

  {   

    $Carttype = $vals->cart_type;

    if($Carttype=='singer' || $Carttype=='band'){ $iscoupan=0; }else{ $iscoupan=$iscoupan+1; }



  }

@endphp

@if($iscoupan>0)

<div class="couponcode">

<input type="button" name="addcounpons" value="{{ (Lang::has(Session::get('lang_file').'.APPLY_COUPONS')!= '')  ?  trans(Session::get('lang_file').'.APPLY_COUPONS'): trans($OUR_LANGUAGE.'.APPLY_COUPONS')}}" class="form-btn popcoupons">

</div>

@endif



   <div class="checkout-page-heading">{{ (Lang::has(Session::get('lang_file').'.Order_Summary')!= '')  ?  trans(Session::get('lang_file').'.Order_Summary'): trans($OUR_LANGUAGE.'.Order_Summary')}}</div>

   

 

   <div class="order-summary-box pay-now-os">

    <div class="order-summary-heading">{{ (Lang::has(Session::get('lang_file').'.Pay_Now')!= '')  ?  trans(Session::get('lang_file').'.Pay_Now'): trans($OUR_LANGUAGE.'.Pay_Now')}} </div>

  

  <div class="os-row-area">



@php 

 $TP                   = 0; 

 $jk                   = 0;

 $total_amount_of_cart = 0;

 $cAmount =0;

 $final_insurance_amount=0;

 foreach($cart_pro as $vals)

 {



   $jk++;

   if($vals->cart_type=='hall')

   {



     $dprice = $vals->paid_total_amount;

     $TP =  $vals->paid_total_amount;



   }

   else

   {

     $dprice = $vals->total_price;

     $TP     =  $vals->total_price;

   }

   $final_insurance_amount=$final_insurance_amount+$vals->insurance_amount;

   $total_amount_of_cart = $total_amount_of_cart + $TP;

   $dprice=currency($dprice, 'SAR',$Current_Currency, $format = false);

   $tprice=currency($total_amount_of_cart, 'SAR',$Current_Currency, $format = false);

    if(Session::get('lang_file')=='ar_lang'){ $langtitle=$vals->pro_title_ar; }else{ $langtitle=$vals->pro_title; };

     if($vals->cart_type=='car_rental'){ $carrental='Car rental'; }else{ $carrental=''; }



     if($vals->cart_type=='travel'){ 



     //$travelrent='Travel';



     if(Lang::has(Session::get('lang_file').'.TRAVEL')!= ''){ $travelrent=trans(Session::get('lang_file').'.TRAVEL');}else{ $travelrent=trans($OUR_LANGUAGE.'.TRAVEL');}



      }else{ 

      $travelrent='';

       }



      if($vals->cart_type=='food'){ 



     //$travelrent='Travel';



     if(Lang::has(Session::get('lang_file').'.CHECKFOOD')!= ''){ $food=trans(Session::get('lang_file').'.CHECKFOOD');}else{ $food=trans($OUR_LANGUAGE.'.CHECKFOOD');}



      }else{ 

      $food='';

       }



     //echo $vals->shop_id;

    if(isset($vals->shop_id) && $vals->shop_id!='')

    {

    $shopname=Helper::getparentCat($vals->shop_id);

    if(count($shopname)< 1){  $shname = '';  }else{

        if(Session::get('lang_file')=='ar_lang')

        {

        $shname = $shopname->mc_name_ar;

          }else{

          $shname = $shopname->mc_name;

        }

    }

  }

    else

    {

    $shname = ''; 

    }



    if($vals->cart_type=='singer'){ $shname='Singer'; }elseif($vals->cart_type=='band'){ $shname='Band'; }else{ $shname=$shname; }





    



      if($vals->cart_type=='recording' ){ continue;}

   @endphp

    <div class="os-row">

    <div class="os-row-no"> {{ $jk }} </div>

    <div class="os-cell1"> @if($vals->cart_type=='hall') Hall  @endif @if($vals->pro_title=='') &nbsp; @else {{$langtitle}} @endif {{ $carrental or '' }}{{$travelrent or '' }} {{ $food }}</div>

    <div class="os-cell2"> 

      @if($vals->cart_type!='singer' && $vals->cart_type!='band')

     @if(count($vals->getProduct)>=1 && $vals->pro_title==''){{$vals->getProduct[0]->pro_title or "&nbsp;"}} <br/>



      @endif @endif {{$shname or ''}} 





   </div> 

    <div class="os-cell3"> {{ number_format($dprice, 2) }} </div>

  </div> <!-- os-row -->



@if(trim($vals->coupon_code)!='' || $vals->coupon_code != NULL)

@php $cAmount = $vals->coupon_code_amount + $cAmount; @endphp

  <div class="os-row">

    <div class="os-row-no"> &nbsp; &nbsp; </div>

    <div class="os-cell1">   {{$vals->coupon_code }} </div>

    <div class="os-cell2"> &nbsp;  &nbsp; &nbsp; </div> 

    <div class="os-cell3">  - {{ Session::get('currency') }} {{ number_format($vals->coupon_code_amount, 2) }}</div>

  </div> <!-- os-row -->



@endif

@php } @endphp

 

 

    <input type="hidden" name="couponamount" id="couponamount" value="{{$cAmount}}">

    @php $tprice= $tprice-$cAmount; @endphp

  </div> <!-- os-row-area -->

  

<div class="os-total-line">

  <div class="os-total-left">  {{ (Lang::has(Session::get('lang_file').'.VAT')!= '')  ?  trans(Session::get('lang_file').'.VAT'): trans($OUR_LANGUAGE.'.VAT')}}</div>

  <div class="os-total-right" id="vat">{{ Session::get('currency') }} 0</div>

  </div>





  <div class="os-total-line">

  <div class="os-total-left">{{ (Lang::has(Session::get('lang_file').'.Total_Shipping')!= '')  ?  trans(Session::get('lang_file').'.Total_Shipping'): trans($OUR_LANGUAGE.'.Total_Shipping')}}</div>

  <div class="os-total-right" id="totalshipping">{{ Session::get('currency') }} 0</div>

  </div>







  <div class="os-total-line">

  <div class="os-total-left">{{ (Lang::has(Session::get('lang_file').'.Total_Price')!= '')  ?  trans(Session::get('lang_file').'.Total_Price'): trans($OUR_LANGUAGE.'.Total_Price')}}</div>

  <div class="os-total-right">{{ Session::get('currency') }} <span id="gtp">{{ number_format($tprice, 2) }}</span></div>

   @php 

  $getvatamount=$tprice-$final_insurance_amount;

  @endphp

  <input type="hidden" name="OTP" id="OTP" value="{{$getvatamount}}">





  </div>

  

  



   </div> <!-- order-summary-box -->

   

  

   </div> <!-- checkout-right-area -->

  </div> <!-- checkout-area -->



<input type="hidden" name="wallet" value="{{$User->wallet}}">

<input type="hidden" name="hall_pending_amount" value="0">

<input type="hidden" name="total_amount" id="totalprice" value="{{$tprice}}">

<input type="hidden" name="orgprice" id="orgprice" value="{{$tprice}}">

<input type="hidden" name="wallet_amount_deducted" id="wallet_amount_deducted" value="0">

<input type="hidden" name="vat" id="Gvat" value="0">

<input type="hidden" name="vat_tax" id="vat_tax" value="0">

<input type="hidden" name="vtotalshipping" id="Gtotalshipping" value="0">



{!! Form::close() !!}

  

      <!-- page-right-section   -->

    </div>

    <!-- page-left-right-wrapper -->

  </div>

  <!-- outer_wrapper -->

</div>

@include('includes.footer')

 <script>

/* Mobile Number Validation */

 function isNumber(evt) {

   evt = (evt) ? evt : window.event;

   var charCode = (evt.which) ? evt.which : evt.keyCode;

   if (charCode > 31 && (charCode < 48 || charCode > 57)) {

   return false;

   }

   return true;

 }

</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

//var jq = $.noConflict();

jQuery( function() {

  jQuery( "#dob" ).datepicker({

    dateFormat: "d M, yy",

    changeMonth: true,

    changeYear: true,

    yearRange: "-80:+0" // last hundred years

  });

} );

</script>

<script>

 <?php $Cur = Session::get('currency'); ?>

 function getCity(cntryid,lan)

 {

     $.ajax({

           type:"GET",

           url:"{{url('getcitylist')}}?cntry_id="+cntryid+'&lang='+lan,

           success:function(res){ 

 

            $('#city').html(res);

           }

        });

  }

  function getVat(cntryid,lan)

  {  

    if(cntryid=='')

    {

  

            $('#Gvat').html(0);

            var op = $('#orgprice').val();

            var tp = parseFloat(op);         

            var walltamt = <?php echo $User->wallet;?>;

            var couponAmt = 0;

          if(tp > walltamt)

          {

            var PriceAfterWallet = parseFloat(tp)-parseFloat(walltamt)-parseFloat(couponAmt);

            walltamt = parseFloat(walltamt).toFixed(2);

            PriceAfterWallet = parseFloat(PriceAfterWallet).toFixed(2); 

            $('#gtp').html(PriceAfterWallet);

            $('#totalprice').val(PriceAfterWallet);

            $('#vat').html('<?php echo $Cur;?> 0');         

            $('#wallet').hide(); 

            $("#wlt").attr('checked', false);

            $('#creditCrd').show();

            $('#walletdis').hide();

            $('#total_amtdis').show();

            

            $('#Wiretfr').show();

            $('#oTotal').html(PriceAfterWallet);

            $('#wltddcut').html('<?php echo $Cur;?> '+walltamt);

            $('#wallet_amount_deducted').val(walltamt);

            $('#totalprice').val(PriceAfterWallet);

          }

          else

          {

            $('#wallet').show();

            $("#wlt").attr('checked', 'checked');

            $('#creditCrd').hide();

                $('#walletdis').show();

            $('#total_amtdis').hide();

            $('#Wiretfr').hide();

            var PriceAfterWallet = parseFloat(walltamt)-parseFloat(tp)-parseFloat(couponAmt);

            PriceAfterWallet = parseFloat(PriceAfterWallet).toFixed(2);

            $('#oTotal').html(0);

            $('#wltddcut').html('<?php echo $Cur;?> '+PriceAfterWallet);

            $('#wallet_amount_deducted').val(PriceAfterWallet);

            $('#totalprice').val(0);

          }



    }

    else

    {



      var tp =  $('#OTP').val();

        $.ajax({

           type:"GET",

           url:"{{url('getVat')}}?country_id="+cntryid+'&lang='+lan+'&totalPrice='+tp,

           success:function(res){ 

            

            var values=res.split('~');

            var vatamount=values[0];

            var vatpercent=values[1];

            $('#Gvat').val(vatamount);

            var op = $('#orgprice').val();

            var couponAmt = 0;

            var tp = parseFloat(op) + parseFloat(vatamount) -  parseFloat(couponAmt);

            tp = parseFloat(tp).toFixed(2);

            $('#gtp').html(tp);

            $('#totalprice').val(tp);

            $('#vat_tax').val(vatpercent);

            

            var walltamt = <?php echo $User->wallet;?>;

            if(tp > walltamt)

            {         

            $('#wallet').hide(); 

            $("#wlt").attr('checked', false);

             $('#creditCrd').show();

                 $('#walletdis').hide();

            $('#total_amtdis').show();

            $('#Wiretfr').show();

            var shwamt = tp-walltamt;

            var diductamt = walltamt;

            shwamt = parseFloat(shwamt).toFixed(2);

            diductamt = parseFloat(diductamt).toFixed(2);



            }

            else

            {

            $('#wallet').show();

            $("#wlt").attr('checked', 'checked');

             $('#creditCrd').hide();

                 $('#walletdis').show();

            $('#total_amtdis').hide();

            $('#Wiretfr').hide();

            var diductamt = tp;

            diductamt = parseFloat(diductamt).toFixed(2);

            var shwamt = 0;

            }

            $('#oTotal').html(shwamt);

            $('#wltddcut').html('<?php echo $Cur;?> '+diductamt);

            $('#wallet_amount_deducted').val(diductamt);

            $('#totalprice').val(shwamt);

            $('#vat').html('<?php echo $Cur;?> '+vatamount);

       

           }

        });  

    }

 

  }

  jQuery(document).ready(function(){

jQuery.validator.addMethod("laxEmail", function(value, element) {

  return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);

}, "Please enter valid email address."); 

 jQuery("#chekoutinfo").validate({

    rules: {          

          "uname" : {

            required : true,

            maxlength: 100

          },

          "email" : {

            required : true,

            email: true,

            laxEmail:true

          },

		   "country_code" : {

            required : true

          },

          "phone" : {

            required : true,

            digits: true, 

            minlength: 5,

            maxlength: 13

          },  

          "gender" : {

            required : true

          },                  

          "city" : {

            required : true

          },           

          "pincode" : {

            required : true,

            digits: true,

            maxlength: 6

          },

          "address1" : {

            required : true

          },

		  "shipping" : {

            required : true

          }, 

		  "payment" : {

            required : true

          },                  

         },

         messages: {

          "uname": {

            required:  "@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NAME') }} @endif",

            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_NAME_HERE_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_NAME_HERE_MAX') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NAME_HERE_MAX') }} @endif'

          },

          "email": {

            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_EMAIL') }} @endif',

            email: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE_VALID')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_EMAIL_HERE_VALID') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_EMAIL_HERE_VALID') }} @endif'

          },

		   "country_code": {

            required: '@if (Lang::has(Session::get('lang_file').'.COUNTRY_CODE_MSG')!= '') {{  trans(Session::get('lang_file').'.COUNTRY_CODE_MSG') }} @else  {{ trans($MER_OUR_LANGUAGE.'.COUNTRY_CODE_MSG') }} @endif'

          },

          "phone": {

            required: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_TEEPHONE_NUMBER_HERE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_TEEPHONE_NUMBER_HERE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_TEEPHONE_NUMBER_HERE') }} @endif',

            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif', 

            minlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MIN')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MIN') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NUMBER_HERE_MIN') }} @endif',

            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_NUMBER_HERE_MAX') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_NUMBER_HERE_MAX') }} @endif'

          },

          "gender": {

            required: '@if (Lang::has(Session::get('lang_file').'.SELECT_YOUR_GENDER_HERE')!= '') {{  trans(Session::get('lang_file').'.SELECT_YOUR_GENDER_HERE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_GENDER_HERE') }} @endif'

          },          

          "city": {

            required: '@if (Lang::has(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')!= '') {{  trans(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.SELECT_YOUR_CITY_HERE') }} @endif'

          },

          "pincode": {

            required: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_PIN_CODE_HERE') }} @endif',

            digits: '@if (Lang::has(Session::get('lang_file').'.ALLOW_ONLY_DIGITS')!= '') {{  trans(Session::get('lang_file').'.ALLOW_ONLY_DIGITS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ALLOW_ONLY_DIGITS') }} @endif',

            maxlength: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE_MAX')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_PIN_CODE_HERE_MAX') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_PIN_CODE_HERE_MAX') }} @endif'

          },

          "address1": {

            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS') }} @else  {{ trans($MER_OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ADDRESS') }} @endif'

          }, 

		  "shipping": {

            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_SHIPPING_METHOD') }} @endif'

          }, 

		  "payment": {

            required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_PAYMENT_METHOD') }} @endif'

          },          

         }

 });

 jQuery(".btn-info-wisitech").click(function() {

   if(jQuery("#chekoutinfo").valid()) {        

    jQuery('#chekoutinfo').submit();

   }

  });

});

 /*

 jQuery("#chekoutinfo").validate({ 

                  ignore: [],

                  rules: {

                          uname: {

                          required: true,

                          },

                          email: {

                          required: true,

                          },

                          phone: {

                          required: true,

                          number:true,

                          },

                          gender: {

                          required: true,

                          },                          

                          dob: {

                          required: true,

                          },

                          city: {

                          required: true,

                          },

                          pincode: {

                          required:true, 

                          number:true,                      

                          },

                          address1: {

                          required:true,

                          }, 

                          shipping: {

                          required:true,

                          },

                          payment: {

                          required:true,

                          },





                      },

                 highlight: function(element) {

                      $(element).removeClass('error');

                },

             

             messages: {

                    uname: {

                       required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_NAME') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_NAME') }} @endif',

                    },  

                    email: {

                      required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_EMAIL') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_EMAIL') }} @endif',

                    },  

                    phone: {

                              required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PHONE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PHONE') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PHONE') }} @endif',

                    },                    

                    gender: {

                              required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_GENDER')!= '') {{  trans(Session::get('lang_file').'.PLEASE_CHOOSE_YOUR_GENDER') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_YOUR_GENDER') }} @endif',

                    },

                    dob: {

                            required: '@if (Lang::has(Session::get('lang_file').'.ENTER_YOUR_DOB_HERE')!= '') {{  trans(Session::get('lang_file').'.ENTER_YOUR_DOB_HERE') }} @else  {{ trans($MER_OUR_LANGUAGE.'.ENTER_YOUR_DOB_HERE') }} @endif',

                    },

                    city: {

                    required: '@if (Lang::has(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE')!= '') {{  trans(Session::get('lang_file').'.SELECT_YOUR_CITY_HERE') }} @else  {{ trans($OUR_LANGUAGE.'.SELECT_YOUR_CITY_HERE') }} @endif',

                    },  



                    address1: {

                    required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_ADDRESS') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_ADDRESS') }} @endif',

                    }, 



                    pincode: {

                    required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PINCODE')!= '') {{  trans(Session::get('lang_file').'.PLEASE_ENTER_YOUR_PINCODE') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_ENTER_YOUR_PINCODE') }} @endif',

                    },  

                      shipping: {

                    required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_CHOOSE_SHIPPING_METHOD') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_SHIPPING_METHOD') }} @endif',

                    },

                    payment: {

                    required: '@if (Lang::has(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD')!= '') {{  trans(Session::get('lang_file').'.PLEASE_CHOOSE_PAYMENT_METHOD') }} @else  {{ trans($OUR_LANGUAGE.'.PLEASE_CHOOSE_PAYMENT_METHOD') }} @endif',

                    },

                                         

                     

                },



                invalidHandler: function(e, validation){

                    },



                submitHandler: function(form) {

                    form.submit();

                }

            });*/

			

$(document).ready(function(){

          var op = $('#orgprice').val();           

          tp = parseFloat(op).toFixed(2);         

          var walltamt = <?php echo $User->wallet;?>;

          var couponAmt = 0;

          if(tp > walltamt)

          {

          $('#wallet').hide(); 

          $("#wlt").attr('checked', false);

          $('#creditCrd').show();

              $('#walletdis').hide();

            $('#total_amtdis').show();

          $('#Wiretfr').show();

          var shwamt = tp-walltamt-couponAmt;

          shwamt = parseFloat(shwamt).toFixed(2);

            var diductamt = walltamt;

            diductamt = parseFloat(diductamt).toFixed(2);

          }

          else

          {

          $('#wallet').show();

         

          $("#wlt").attr('checked', 'checked');

          $('#creditCrd').hide();

              $('#walletdis').show();

            $('#total_amtdis').hide();

          $('#Wiretfr').hide();

          var diductamt = tp;

          diductamt = parseFloat(diductamt).toFixed(2);

          var shwamt = 0;

          }



          $('#oTotal').html(shwamt);

          $('#wltddcut').html('<?php echo $Cur;?> '+diductamt);

          $('#wallet_amount_deducted').val(diductamt);

          $('#totalprice').val(shwamt);

})



  

$('.shiping').click(function(){



var method = $(this).data('value');

 var shipids = $(this).data('ship');

 var ashipids = $(this).data('aship');

 





var address1 = $('#address1').val();

var address2 = $('#address2').val();

var city = $('#city').val();

var country = $('#country').val();

var address = address1+ ' ' +address2;



//var Oldprice = $(this).data('price');



var Oldprice=jQuery(this).attr("data-price");

 



var productid = $(this).data('productid');

 var ship =  $('#Gtotalshipping').val();

 var shippingprice = $(this).data('price');



 //  if(method ==2 && shippingprice != undefined)

 // {

  

 

 //  var pickprice = parseFloat(ship)-parseFloat(shippingprice);

 



 //  $('#totalshipping').html('SAR '+pickprice);

  



 //      var op = $('#orgprice').val();           

 //      tp = parseFloat(op).toFixed(2); 

 //      var t = $('#Gtotalshipping').val();

 //      $('#Gtotalshipping').val(pickprice);

 //      var Gvat = $('#Gvat').val();

 //     var rem =  parseFloat(t)-parseFloat(shippingprice);

 //      var tt = parseFloat(tp) + parseFloat(rem) + parseFloat(Gvat);



 //      $('#gtp').html(tt);









 // $(this).attr('disabled', true);

  

 

 // }

 

  

 

//url:"{{url('getaramexrates.php')}}?countryCode="+country+'&address='+address+'&city='+city+'&productid='+productid,

 var merchant_id = $(this).data('merchant_id');

 var cart_sub_type = $(this).data('cart_sub_type');



        $.ajax({

           type:"GET",

           url:"{{url('getshippingrate')}}?shipping_type="+method+'&merchant_id='+merchant_id+'&cart_sub_type='+cart_sub_type,

          success:function(res){  

       var result;   

    

        result = parseFloat(res ) - parseFloat(Oldprice);

          $('#'+ashipids).attr('data-price', res);

          $('#'+shipids).attr('data-price', res);

             if(method ==1)

            {

              $('#'+ashipids).attr('disabled', true);

              $('#'+shipids).attr('disabled', false);

            $('#Aramex_priceCalc_'+productid).html('SAR '+res);

            $('#Picks_priceCalc_'+productid).html('SAR 0.00');

            }

            else

            {

            $('#'+ashipids).attr('disabled', false);

            $('#'+shipids).attr('disabled', true);

            $('#Aramex_priceCalc_'+productid).html('SAR 0.00');

            $('#Picks_priceCalc_'+productid).html('SAR '+res);

            }

 

  





          var newPrice = parseFloat(ship)+parseFloat(result);





          $('#totalshipping').html(newPrice);

          $('#Gtotalshipping').val(newPrice);  



       



            

          var op = $('#orgprice').val();           

          tp = parseFloat(op).toFixed(2); 

          var t = $('#Gtotalshipping').val();

          var Gvat = $('#Gvat').val();

          var tt = parseFloat(tp) + parseFloat(t) + parseFloat(Gvat);          

            $('#totalprice').val(tt);

           $('#gtp').html(tt);

          $('#oTotal').html(tt);

           }

        });



  

  

}) 



  </script>





 

 <!--

  <script language="JavaScript">



 

    document.onkeypress = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

           //alert('No F-12');

            return false;

        }

    }

    document.onmousedown = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

            //alert('No F-keys');

            return false;

        }

    }

document.onkeydown = function (event) {

        event = (event || window.event);

        if (event.keyCode == 123) {

            //alert('No F-keys');

            return false;

        }

    }

 



 

var message="Sorry, right-click has been disabled";

function clickIE() {if (document.all) {(message);return false;}}

function clickNS(e) {if

(document.layers||(document.getElementById&&!document.all)) {

if (e.which==2||e.which==3) {(message);return false;}}}

if (document.layers)

{document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}

else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}

document.oncontextmenu=new Function("return false")



function disableCtrlKeyCombination(e)

{

var forbiddenKeys = new Array('a', 'n', 'c', 'x', 'v', 'j' , 'w');

var key;

var isCtrl;

if(window.event)

{

key = window.event.keyCode;     //IE

if(window.event.ctrlKey)

isCtrl = true;

else

isCtrl = false;

}

else

{

key = e.which;     //firefox

if(e.ctrlKey)

isCtrl = true;

else

isCtrl = false;

}

if(isCtrl)

{

for(i=0; i<forbiddenKeys.length; i++)

{

//case-insensitive comparation

if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())

{

alert('Key combination CTRL + '+String.fromCharCode(key) +' has been disabled.');

return false;

}

}

}

return true;

}

</script>

 -->





<!-- Coupon Code start -->



<div class="action_popup coupon-popup-area">

<div class="action_active_popup"> 

<div class="coupon-heading">{{ (Lang::has(Session::get('lang_file').'.APPLY_COUPONS')!= '')  ?  trans(Session::get('lang_file').'.APPLY_COUPONS'): trans($OUR_LANGUAGE.'.APPLY_COUPONS')}} <span class="applied closecpn" title="Close">X</span></div>

<div class="coupon_apply">   

<input type="text" name="coupon_code" id="coupon_code" maxlength="45" class="t-box"> 

<input type="button" name="apply" class="insapplied" value="{{ (Lang::has(Session::get('lang_file').'.APPLY')!= '')  ?  trans(Session::get('lang_file').'.APPLY'): trans($OUR_LANGUAGE.'.APPLY')}}">

<span class="errors" id="couponerror"></span>

</div>

<div class="afterredeem" @if(count($getCartcoupon) < 1) style="display: none;" @endif>

<div class="appliedcoupons">{{ (Lang::has(Session::get('lang_file').'.APPLIED_COUPONS')!= '')  ?  trans(Session::get('lang_file').'.APPLIED_COUPONS'): trans($OUR_LANGUAGE.'.APPLIED_COUPONS')}} </div>





<div class="couponlisting mCustomScrollbar" id="content-1" style="max-height:220px;">

<ul class="couponlistinga">



 

   @if(count($getCartcoupon) >= 1)

 @foreach($getCartcoupon as $couponcodes)

   @php $rand = rand(1111,9999); @endphp

<li class="{{$rand}}"><div class="coupon-beauty">{{$couponcodes->cart_type}}</div><div class="coupon-beauty"><span>{{$couponcodes->coupon_code}}</span></div><div class="coupon-beauty coupon-sar">SAR {{ number_format($couponcodes->coupon_code_amount, 2) }}<span class="coupon-remove" data-rmd="{{$rand}}" data-ids="{{$couponcodes->coupon_code}}">{{(Lang::has(Session::get('lang_file').'.Remove')!= '')  ?  trans(Session::get('lang_file').'.Remove'): trans($OUR_LANGUAGE.'.Remove')}}</span></div></li>

 

 @endforeach

@endif

</ul>

</div>

<div class="coupon-popup-apply">

<input type="button" name="apply" class="applied form-btn" value="{{ (Lang::has(Session::get('lang_file').'.DONE')!= '')  ?  trans(Session::get('lang_file').'.DONE'): trans($OUR_LANGUAGE.'.DONE')}}">

</div>



</div>



</div>

</div>



<!-- Coupon Code end -->





<script type="text/javascript">  

$('body').on('click','.coupon-remove',function(){

    var getCode = $(this).data('ids');

    var rmd = $(this).data('rmd');

    var del = 1;

     $.ajax({

    type:"GET",

    url:"{{url('checkcouponcode')}}?couopncode="+getCode+"&del="+del,

    success:function(res){

    $('.'+rmd).fadeOut(500);

    }

    });

    });



$('.insapplied').click(function(){

var getCode = $('#coupon_code').val();

    if($.trim(getCode)=='')

    {

    var error = "{{(Lang::has(Session::get('lang_file').'.Pleaseenter_couponcode')!= '')  ?  trans(Session::get('lang_file').'.Pleaseenter_couponcode'): trans($OUR_LANGUAGE.'.Pleaseenter_couponcode')}}";

    $('#couponerror').html(error);

    }

    else

    {

    $.ajax({

               type:"GET",

               url:"{{url('checkcouponcode')}}?couopncode="+getCode,

               success:function(res){ 

                if(res!='Invaild coupon code'){

                  

              if(res=='Already applied')

              { 

                  var error = "{{(Lang::has(Session::get('lang_file').'.CouponAlready_applied')!= '')  ?  trans(Session::get('lang_file').'.CouponAlready_applied'): trans($OUR_LANGUAGE.'.CouponAlready_applied')}}";

                 $('#couponerror').html(error); 

              }

              else

              {

              $('.afterredeem').show();

              $('#coupon_code').val('');

              $('#couponerror').html('');

              var Remove = "{{(Lang::has(Session::get('lang_file').'.Remove')!= '')  ?  trans(Session::get('lang_file').'.Remove'): trans($OUR_LANGUAGE.'.Remove')}}";

              var ran = Math.floor((Math.random() * 100000000) + 1);

              var code = res.split('~~');

              $('.couponlistinga').append('<li class="'+ran+'"><div class="coupon-beauty">'+code[0]+'</div><div class="coupon-beauty"><span>'+code[1]+'</span></div><div class="coupon-beauty coupon-sar">SAR '+code[2]+'<span class="coupon-remove" data-rmd="'+ran+'" data-ids="'+code[1]+'">'+Remove+'</span></div></li>');

              $("#content-1").mCustomScrollbar({

              scrollButtons:{

              enable:true

              }  

              });



              }

      }

      else

      {

    var error = "{{(Lang::has(Session::get('lang_file').'.Pleaseenter_validcouponcode')!= '')  ?  trans(Session::get('lang_file').'.Pleaseenter_validcouponcode'): trans($OUR_LANGUAGE.'.Pleaseenter_validcouponcode')}}";

    $('#couponerror').html(error);



      }

           }

        });

 

}

});

  



  $('.applied').click(function(){

  $('.action_popup').fadeOut(500);

  $('.overlay').fadeOut(500);

  location.reload();

  });

  $('.popcoupons').click(function(){

  $('.action_popup').fadeIn(500);

  $('.overlay').fadeIn(500);

   $('#coupon_code').val('');

   $('#couponerror').html('');

  });





 

 

  

</script>

 

 @if($ChkShip ==0)

<script type="text/javascript">

  $(window).ready(function(){

     $('#shipbox,#shipbox1').hide();

   }); 

</script>

 @endif







 
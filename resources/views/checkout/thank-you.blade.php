@include('includes.navbar')
<div class="outer_wrapper">
  <div class="inner_wrap"> @include('includes.header')
 
    <!-- search-section -->
    <div class="page-left-right-wrapper main_category">
      
    

<div class="thank_you">
	<div class="thank_you_header"> {{ (Lang::has(Session::get('lang_file').'.Thanks_heading')!= '')  ?  trans(Session::get('lang_file').'.Thanks_heading'): trans($OUR_LANGUAGE.'.Thanks_heading')}}  </div>

<div class="thank_you_text"> {{ (Lang::has(Session::get('lang_file').'.Thanks_text')!= '')  ?  trans(Session::get('lang_file').'.Thanks_text'): trans($OUR_LANGUAGE.'.Thanks_text')}}  </div>
@php 
        $sesionData = session()->all();
        if(isset($sesionData) && $sesionData!='')
        {

if(Session::get('searchdata.mainselectedvalue') ==1)
{
   


   $url = url('search-results?weddingoccasiontype=1&noofattendees='.$sesionData['searchdata']['noofattendees'].'&budget='.$sesionData['searchdata']['budget'].'&cityid='.$sesionData['searchdata']['cityid'].'&occasiondate='.$sesionData['searchdata']['startdate'].'&basecategoryid='.$sesionData['searchdata']['mainselectedvalue'].'&maincategoryid='.$sesionData['searchdata']['maincategoryid'].'&mainselectedvalue='.$sesionData['searchdata']['mainselectedvalue']);
}
else
{
     $url = url('search-results?weddingoccasiontype='.$sesionData['searchdata']['weddingoccasiontype'].'&noofattendees='.$sesionData['searchdata']['noofattendees'].'&budget='.$sesionData['searchdata']['budget'].'&cityid='.$sesionData['searchdata']['cityid'].'&occasiondate='.$sesionData['searchdata']['occasiondate'].'&gender='.$sesionData['searchdata']['gender'].'&basecategoryid='.$sesionData['searchdata']['mainselectedvalue'].'&maincategoryid='.$sesionData['searchdata']['maincategoryid'].'&mainselectedvalue='.$sesionData['searchdata']['mainselectedvalue']);
}

         
        } 
         
      @endphp
<div class="thank_you_text"><a href="{{url('/my-account-ocassion')}}"> {{ (Lang::has(Session::get('lang_file').'.please_click_here_to_view_order')!= '')  ?  trans(Session::get('lang_file').'.please_click_here_to_view_order'): trans($OUR_LANGUAGE.'.please_click_here_to_view_order')}}</a> </div>
@php if(isset($sesionData) && $sesionData!='')
        { @endphp
<div class="thank_you_text"><a href="{{ $url }}"> {{ (Lang::has(Session::get('lang_file').'.CONTINUE_PLANNING')!= '')  ?  trans(Session::get('lang_file').'.CONTINUE_PLANNING'): trans($OUR_LANGUAGE.'.CONTINUE_PLANNING')}}</a> </div>
@php } @endphp
</div>






  
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
  <!-- outer_wrapper -->
</div>
@include('includes.footer')
 
 
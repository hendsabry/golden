@include('includes.navbar')
<div class="overlay"></div>
<div class="popup_overlay"></div>
<div class="outer_wrapper"> @include('includes.header')
  <div class="inner_wrap">
    <!-- search-section -->
    <div class="page-left-right-wrapper not-sp">
      <div class="myaccount-mobile"><a href="#">{{ (Lang::has(Session::get('lang_file').'.MY_ACCOUNT')!= '')  ?  trans(Session::get('lang_file').'.MY_ACCOUNT'): trans($OUR_LANGUAGE.'.MY_ACCOUNT')}}</a></div>
      @include('includes.left_menu')
      <div class="myaccount_right">
        <h1 class="dashborad_heading">{{ (Lang::has(Session::get('lang_file').'.MyDashboard')!= '')  ?  trans(Session::get('lang_file').'.MyDashboard'): trans($OUR_LANGUAGE.'.MyDashboard')}}</h1>
        <div class="field_group top_spacing_margin"> <span class="dash_heading">{{ (Lang::has(Session::get('lang_file').'.UPCOMING_OCCASIONS')!= '')  ?  trans(Session::get('lang_file').'.UPCOMING_OCCASIONS'): trans($OUR_LANGUAGE.'.UPCOMING_OCCASIONS')}} </span>
          <div class="main_user">
            <?php 
			//echo '<pre>';print_r($order);die;
			if(!count($order)<1){?>
            <div class="myaccount-table">
              <div class="mytr">
                <div class="mytable_heading order_id"></div>
                <div class="mytable_heading"> {{ (Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')}} </div>
                <div class="mytable_heading">{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}} </div>
                <div class="mytable_heading order_id">{{ (Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')}}</div>
              </div>
              <?php 
             if(!count($order)<1)
      			 {
                     $i = 1;
                     foreach($order as $val)
      			   {               
			          ?>
              <div class="mytr">
                <div class="mytd td1" data-title="{{ (Lang::has(Session::get('lang_file').'.S.NO')!= '')  ?  trans(Session::get('lang_file').'.S.NO'): trans($OUR_LANGUAGE.'.S.NO')}} ">{{ $i }}</div>
                <?php 
				if(isset($val->search_occasion_id) && $val->search_occasion_id!='0')
				{
                $setTitle = Helper::getOccasionName($val->search_occasion_id);
                $mc_name='title';
                if(Session::get('lang_file')!='en_lang')
                $mc_name= 'title_ar'; ?>
                <div class="mytd td2" data-title="{{ (Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')}}"><a style="text-decoration:none;" href="{{ route('order-details',['id'=>$val->order_id]) }}"><?php if(isset($setTitle->$mc_name) && $setTitle->$mc_name!=''){echo $setTitle->$mc_name;}else{echo 'N/A';} ?></a> </div>
				 <?php } else { 
				 if(isset($val->main_occasion_id) && $val->main_occasion_id!='0'){ 
				 if(Session::get('lang_file')!='en_lang')
				 {
				   $getArrayOfOcc = array('1'=>'اجتماع عمل','2'=>'مناسبة الزفاف');
				 }
				 else
				 {
					$getArrayOfOcc = array('1'=>'Business Meeting','2'=>'Wedding And Occasion'); 
				 } 
				 foreach($getArrayOfOcc as $key=>$ocval)
				 {
				  if($val->main_occasion_id==$key)
				  {
				   $occasion_name = $ocval;
				  }
				 }				  
				 ?> 
				 <div class="mytd td2" data-title="Hall">
				 <a href="{{ route('order-details',['id'=>$val->order_id]) }}">@php echo $occasion_name; @endphp</a>
				 </div>
				 <?php  }} ?>                
                <div class="mytd td3" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_DATE')!= '')  ?  trans(Session::get('lang_file').'.ORDER_DATE'): trans($OUR_LANGUAGE.'.ORDER_DATE')}}">
				{{ Carbon\Carbon::parse($val->order_date)->format('d M Y')}}			
				</div>
                <div class="mytd order_id" data-title="{{ (Lang::has(Session::get('lang_file').'.ORDER_ID')!= '')  ?  trans(Session::get('lang_file').'.ORDER_ID'): trans($OUR_LANGUAGE.'.ORDER_ID')}}"><a href="{{ route('order-details',['id'=>$val->order_id]) }}">{{ $val->order_id }}</a></div>
              </div>
              <?php            
             $i++; }} ?>
            </div>
            <?php }else{ ?>
            <div class="no-record-area">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
            <!-- no-record-area -->
            <?php } ?>
          </div>
        </div>
        <?php if(count($order)>= 5){ //if(!count($order)<1){ ?>
        <div class="all_review"><a href="{{ route('my-account-ocassion') }}">{{ (Lang::has(Session::get('lang_file').'.VIEW_ALL')!= '')  ?  trans(Session::get('lang_file').'.VIEW_ALL'): trans($OUR_LANGUAGE.'.VIEW_ALL')}}</a></div>
        <?php }//} ?>
        <div class="my_photo_area lts">
          <div class="myphoto_left"><span class="dash_heading">{{ (Lang::has(Session::get('lang_file').'.LATEST_PHOTO_GALLERY')!= '')  ?  trans(Session::get('lang_file').'.LATEST_PHOTO_GALLERY'): trans($OUR_LANGUAGE.'.LATEST_PHOTO_GALLERY')}} </span>
            <?php if(isset($getImagelist[0]->id) && $getImagelist[0]->id!=''){ ?>
            @php $setAllImage = Helper::getAllOccasionImage($getImagelist[0]->id); @endphp
            @if(count($setAllImage) > 0)
            <div class="galls">
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.OCCASIONS_NAME')!= '')  ?  trans(Session::get('lang_file').'.OCCASIONS_NAME'): trans($OUR_LANGUAGE.'.OCCASIONS_NAME')}}:</div>
                <div class="wed_text">
                  <?php if(isset($getImagelist[0]->occasion_name) && $getImagelist[0]->occasion_name!=''){ echo $getImagelist[0]->occasion_name;} ?>
                </div>
              </div>
              <?php if(isset($getImagelist[0]->city_id) && $getImagelist[0]->city_id!=''){ ?>
              <div class="occs">
                <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.CITY')!= '')  ?  trans(Session::get('lang_file').'.CITY'): trans($OUR_LANGUAGE.'.CTIY')}}:</div>
                @php $city_name = Helper::getcity($getImagelist[0]->city_id); @endphp
                <div class="wed_text">{{$city_name->ci_name}}</div>
              </div>
              <?php } ?>
            </div>
            <div class="photosmy">
              <ul>
                <?php if(isset($getImagelist[0]->id) && $getImagelist[0]->id!=''){ ?>
                @php $setAllImage = Helper::getAllOccasionImage($getImagelist[0]->id); @endphp
                @if(count($setAllImage) > 0)
                @foreach($setAllImage as $val)
                <li><img src="{{ $val->image_url }}" width="125" alt="" /></li>
                @endforeach
                @else
                <div class="no-record-area" align="center">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
                @endif
                <?php } ?>
              </ul>
            </div>
            <div class="photo_view_detail"><a href="{{ route('my-account-studio') }}">View Details</a></div>
            @endif
            <?php } else{ ?>
            <div class="no-record-area" align="center">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
            <?php } ?>
          </div>
          <div class="myphoto_left"><span class="dash_heading">{{ (Lang::has(Session::get('lang_file').'.WALLET_BALANCE')!= '')  ?  trans(Session::get('lang_file').'.WALLET_BALANCE'): trans($OUR_LANGUAGE.'.WALLET_BALANCE')}} </span>
            <div class="sar_money">
              <div class="sar_text">SAR
                <?php 
			$userid  = Session::get('customerdata.user_id');
			$getInfo = Helper::getuserinfo($userid); echo number_format($getInfo->wallet,2);
			?>
              </div>
              <div class="add_money"><a href="{{route('my-account-wallet')}}">{{ (Lang::has(Session::get('lang_file').'.ADD_MONEY')!= '')  ?  trans(Session::get('lang_file').'.ADD_MONEY'): trans($OUR_LANGUAGE.'.ADD_MONEY')}}</a></div>
            </div>
          </div>
        </div>
        <div class="my_latest_area lts">
          <div class="latest_mix"><span class="dash_heading">{{ (Lang::has(Session::get('lang_file').'.LATEST_REVIEW')!= '')  ?  trans(Session::get('lang_file').'.LATEST_REVIEW'): trans($OUR_LANGUAGE.'.LATEST_REVIEW')}} </span>
            <div class="my_latest_revie" >
              <?php 
		     $i =1; 
             if(count($getReview) > 0){ 
			 foreach($getReview as $val){?>
              <div class="btm @php if($i!=1) echo 'tps'; @endphp">
                <div class="occs">
                  <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.SERVICES')!= '')  ?  trans(Session::get('lang_file').'.SERVICES'): trans($OUR_LANGUAGE.'.SERVICES')}}: </div>
                  <div class="wed_text"> @php $shop_id = $val->shop_id; 
                    $get_show_name = Helper::getCatName($shop_id);
                    echo $get_show_name;
                    @endphp</div>
                </div>
                <div class="occs">
                  <div class="occus_text">{{ (Lang::has(Session::get('lang_file').'.SERVICES_PROVIDER')!= '')  ?  trans(Session::get('lang_file').'.SERVICES_PROVIDER'): trans($OUR_LANGUAGE.'.SERVICES_PROVIDER')}}: </div>
                  <div class="wed_text"> @php 
                    $vandor_id = $val->vandor_id; 
                    $get_service_provider = Helper::getServiceProviderName($vandor_id);
                    echo $get_service_provider;
                    @endphp </div>
                  <div class="my_star"> @php 
                    $total_rate = $val->ratings;
                    for($i=1;$i<=5;$i++){ if($i==$total_rate) { @endphp <img src="{{ url('') }}/themes/images/star{{$i}}.png" alt="" /> @php }} @endphp </div>
                </div>
                <div class="lors_text">{{$val->comments}}</div>
              </div>
              <?php }$i++;?>
              <div class="all_review"><a href="{{ route('my-account-review') }}">{{ (Lang::has(Session::get('lang_file').'.ALL_REVIEWS')!= '')  ?  trans(Session::get('lang_file').'.ALL_REVIEWS'): trans($OUR_LANGUAGE.'.ALL_REVIEWS')}}</a></div>
              <?php } else{?>
              <div class="no-record-area" align="center">@if(Lang::has(Session::get('lang_file').'.NO_RECORD_FOUND')!= '') {{ trans(Session::get('lang_file').'.NO_RECORD_FOUND')}}  @else {{ trans($OUR_LANGUAGE.'.NO_RECORD_FOUND')}} @endif</div>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
      <!-- page-right-section -->
    </div>
    <!-- page-left-right-wrapper -->
  </div>
</div>
<!-- outer_wrapper -->
<!-- Footer -->
@include('includes.footer')
<script src="{{url('/')}}/themes/js/height.js"></script>

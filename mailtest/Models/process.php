<?php

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';
require("Models/class.phpmailer.php");
\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();
$app->contentType('application/json');

function getConnection() {
    try {
        $con = new MongoClient('mongodb://sellr:sellr007@167.99.160.200:27017/sellr');;
        $mongoDB = $con->selectDB('Kidzo');
    } catch (PDOException $e) {
        RETURN 'ERROR: ' . $e->getMessage();
    }
    return $mongoDB;
}

function _return_response($arr) {

    $ret_arr = array();

    foreach ($arr as $ind => $obj)
        if (is_array($arr[$ind]))
            $ret_arr[$ind] = _return_response($obj);
        else if ($obj === NULL)
            $ret_arr[$ind] = '';
        else
            $ret_arr[$ind] = urldecode($obj);

    return $ret_arr;
}

function GetSessionExpiryTime() {

    $curr_date = time();
    $searchDt = gmdate('Y-m-d H:i:s', $curr_date);
    $newtimestamp = strtotime($searchDt . ' +2 year');
    $Exp = date('Y-m-d H:i:s', $newtimestamp);

    return $Exp;
}

function _response($data) {
    header('Content-type: application/json');
    $data_new = _return_response($data);
    return json_encode($data_new);
}

function ChecktSessionExpiry($id) {

    $mongoDB = getConnection();
    $USerData = $mongoDB->selectCollection('UserData');
    $curr_date = time();
    $searchDt = gmdate('Y-m-d H:i:s', $curr_date);
    $FindEmail = $USerData->findOne(array('_id' => new MongoId($id), 'SessionExp' => array('$gt' => $searchDt)));
    $FindResult = $FindEmail['_id'];

    if ($FindResult == '')
        return 1;
    return 2;
}

function GetExistingUer($Email) {
    try {
        $mongoDB = getConnection();
        $USerData = $mongoDB->selectCollection('UserData');

        $FindEmail = $USerData->findOne(array('Email' => $Email));
        // $FindResult = '';
        //foreach ($FindEmail as $result) {
        $FindResult = (string) $FindEmail['_id'];
//        }
//        if()){
//            $UserId='';
//        }else {
//            $UserId = $FindResult['_id'];
//        }
    } catch (PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
//    ECHO $FindResult;
    return $FindResult;
}

function SendForGotPasswordMail($Email) {
    $body1 = "
    

    <div style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);\">
     <div style=\"\">

        <div align=\"left\" bgcolor=\"#f3f3f3\" style=\"width: 550px; background-color: rgb(243, 243, 243); text-align: left; margin: 0px auto 45px; padding: 0px; border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px;\">
             <div style=\"padding: 45px 45px 15px;\">
                <div style=\"font-size: 20px; margin-bottom: 30px;\">
                     <h2>
                        <strong><span class=\"il\" style=\"background-color: rgb(255, 255, 204); color: rgb(34, 34, 34); background-position: initial initial; background-repeat: initial initial;\">Kidzo</span></strong>
                    </h2>
                </div>


                <table style=\"width: 460px; margin: 30px auto; border-spacing: 0px; line-height: 0px;\">
                       <tbody>
                        <tr>
                            <td style=\"font-family: arial, sans-serif; margin: 0px; border-bottom-color: rgb(192, 192, 200); border-bottom-style: solid;\">
                                &nbsp;
                        </td>
                        <td style=\"font-family: arial, sans-serif; margin: 0px; border-bottom-color: rgb(192, 192, 200); border-bottom-style: solid;\">
                            &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style=\"font-family: arial, sans-serif; margin: 0px; border-top-color: rgb(255, 255, 255); border-top-style: solid;\">
                        &nbsp;
                </td>
                <td style=\"font-family: arial, sans-serif; margin: 0px; border-top-color: rgb(255, 255, 255); border-top-style: solid;\">
                    &nbsp;
            </td>
        </tr>
    </tbody>
</table>

<p style=\"font-size: 12px; margin: 0px 0px 10px;\">
   <span style=\"font-size: 14px;\"><strong><a href='' >Click Here</a>   For Reset the Password</strong></span>
</p>

<table style=\"width: 460px; margin: 30px auto; border-spacing: 0px; line-height: 0px;\">
       <tbody>
        <tr>
            <td style=\"font-family: arial, sans-serif; margin: 0px;\">
                &nbsp;
        </td>
        <td style=\"font-family: arial, sans-serif; margin: 0px;\">
            &nbsp;
    </td>
</tr>
";
    $mail = new PHPMailer(); //New instance, with exceptions enabled
    $mail->IsSMTP();
    $body = preg_replace('/\\\\/', '', $body1); //Strip backslashes
    $mail->Host = 'ssl://smtp.gmail.com'; //ssl://smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->SMTPKeepAlive = true;                  // SMTP connection will not close after each email sent, reduces SMTP overhead
    $mail->Port = 465; //465;
    $mail->Username = 'kiran@3embed.com'; //'howard.realview@gmail.com';
    $mail->Password = 'a15rangvarsha'; //'realview007';
    $mail->SetFrom('patelkiran3090@gmail.com', 'Kidzo Admin');
    $mail->AddReplyTo('patelkiran3090@gmail.com', 'noreply');
//        $mail->Subject = "Neue Bestellung!";
//        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
    $mail->Subject = "Password Recovery Mail From Kidzo !";
    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
    $mail->MsgHTML($body);
    $to = $Email;
    $mail->AddAddress($to);
    $mail->IsHTML(true); // send as HTML
    $send = $mail->Send();
    if ($send) {
        return 1;
    }
    return 2;
}

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */
$app->get(
        '/', function () {

    $mongoDB = getConnection();
    $products = $mongoDB->selectCollection('UserData');
    $cursor1 = $products->find(array('Email' => ''));
    $demos_result = array();
    foreach ($cursor1 as $result) {
        $demos_result[] = $result;
    }
    return _response(array('a' => $_GET['id'], 'b' => $_GET['name']));
}
);


/**
 * Signup
 *
 * Here We can signup new users in the app.
 * It takes require parameters and stores the data in database and gives a unique id for that user.
 *
 *
 */
$app->post(
        '/Signup', function () {

    if ($_POST['Email'] == '' || $_POST['FirstName'] == '' || $_POST['Password'] == '' ||
            $_POST['DeviceType'] == '' || $_POST['DeviceToken'] == '' || $_POST['DeviceOS'] == '' || $_POST['DeviceId'] == '' ||
            $_POST['DateTime'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    $mongoDB = getConnection();
    $UserId = GetExistingUer($_POST['Email']);
    if ($UserId != '') {
        echo json_encode(array('ErrorCode' => '2', 'Message' => 'User Already Available'));
        return;
    }
    try {
        $sessionExp = GetSessionExpiryTime();

        $UserTable = $mongoDB->selectCollection('UserData');
        $insertUserArr = array('SocialId' => '', 'Email' => $_POST['Email'], 'FirstName' => $_POST['FirstName'], 'LastName' => $_POST['LastName'],
            'Sex' => $_POST['Sex'], 'DOB' => $_POST['DOB'], 'Password' => $_POST['Password'], 'DeviceType' => $_POST['DeviceType'],
            'DeviceToken' => $_POST['DeviceToken'], 'DeviceOS' => $_POST['DeviceOS'], 'DeviceId' => $_POST['DeviceId'],
            'RegisterOn' => $_POST['DateTime'], 'PicUrl' => $_POST['PicUrl'], 'SocialMediaType' => '', 'SessionExp' => $sessionExp);

        $UserTable->insert($insertUserArr);
        $UserId = GetExistingUer($_POST['Email']);
        echo json_encode(array('ErrorCode' => '3', 'Message' => 'SignUp Successfully Completed', 'Data' => array('SeesionToken' => (string) $UserId)));
        return;
    } catch (Exception $ex) {
        echo json_encode(array('ErrorCode' => '5', 'Message' => 'Something missing'));
    }
}
);


/**
 * Signup with social media like fb and g+
 *
 * Here We can signup new users in the app from fb or g+.
 * It takes require parameters and stores the data in database and gives a unique id for that user.
 *
 *
 */
$app->post(
        '/LoginWithSocialMedia', function () {

    if ($_POST['Email'] == '' || $_POST['FirstName'] == '' ||
            $_POST['DeviceType'] == '' || $_POST['DeviceToken'] == '' || $_POST['DeviceOS'] == '' || $_POST['DeviceId'] == '' ||
            $_POST['DateTime'] == '' || $_POST['Type'] == '' || $_POST['SocialId'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    $sessionExp = GetSessionExpiryTime();
    $mongoDB = getConnection();
    $USerData = $mongoDB->selectCollection('UserData');

    $FindEmail = $USerData->findOne(array('SocialId' => $_POST['SocialId']));
    if ($FindEmail != '') {
        $insertIntoChatArr = array('PicUrl' => $_POST['PicUrl'], 'DeviceType' => $_POST['DeviceType'],
            'DeviceToken' => $_POST['DeviceToken'], 'DeviceOS' => $_POST['DeviceOS'], 'DeviceId' => $_POST['DeviceId'],
            'SessionExp' => $sessionExp);
//                $USerData->update(
//                        array('_id' => new MongoId($FindEmail['_id'])), array(
//                    '$set' => $insertIntoChatArr,
//                        ), array("upsert" => true)
//                );

        echo json_encode(array('ErrorCode' => '2', 'Message' => 'User Already Available', 'Data' => array('SeesionToken' => (string) $FindEmail['_id'])));
        return;
    } else {

        $UserTable = $mongoDB->selectCollection('UserData');
        $insertUserArr = array('SocialId' => $_POST['SocialId'], 'Email' => $_POST['Email'], 'FirstName' => $_POST['FirstName'], 'LastName' => $_POST['LastName'],
            'Sex' => $_POST['Sex'], 'DOB' => $_POST['DOB'], 'Password' => '', 'DeviceType' => $_POST['DeviceType'],
            'DeviceToken' => $_POST['DeviceToken'], 'DeviceOS' => $_POST['DeviceOS'], 'DeviceId' => $_POST['DeviceId'],
            'RegisterOn' => $_POST['DateTime'], 'PicUrl' => $_POST['PicUrl'], 'SocialMediaType' => $_POST['Type'],
            'SessionExp' => $sessionExp);

        $UserTable->insert($insertUserArr);
        $USerData = $mongoDB->selectCollection('UserData');
        $FindEmail = $USerData->findOne(array('SocialId' => $_POST['SocialId']));
        echo json_encode(array('ErrorCode' => '3', 'Message' => 'SignUp Successfully Completed', 'Data' => array('SeesionToken' => (string) $FindEmail['_id'])));
        return;
    }
    echo json_encode(array('ErrorCode' => '4', 'Message' => 'Something Wrong'));
    return;
}
);

/**
 * Login
 *
 * Here We allow user to login to kidzo app
 * It takes require parameters and stores the data in database and gives a unique id for that user.
 *
 *
 */
$app->post(
        '/Login', function () {

    if ($_POST['Email'] == '' || $_POST['Password'] == '' ||
            $_POST['DeviceType'] == '' || $_POST['DeviceToken'] == '' || $_POST['DeviceOS'] == '' || $_POST['DeviceId'] == '' ||
            $_POST['DateTime'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    //conection to mongodb
    $mongoDB = getConnection();
    //check user exist
    $UserId = GetExistingUer($_POST['Email']);

    if ($UserId != '') {
        $USerData = $mongoDB->selectCollection('UserData');
        $FindEmail = $USerData->findOne(array('Email' => $_POST['Email'], 'Password' => $_POST['Password']));

        if (empty($FindEmail)) {
            echo json_encode(array('ErrorCode' => '2', 'Message' => 'Password is in-correct'));
            return;
        } else {

            $sessionExp = GetSessionExpiryTime();

            $insertIntoChatArr = array('DeviceType' => $_POST['DeviceType'],
                'DeviceToken' => $_POST['DeviceToken'], 'DeviceOS' => $_POST['DeviceOS'], 'DeviceId' => $_POST['DeviceId'],
                'SessionExp' => $sessionExp);
            // echo json_encode(array('ErrorCode' => '1', 'Message' => $insertIntoChatArr));
//                    $USerData->update(
//                            array('_id' => new MongoId($FindEmail['_id'])), array(
//                        '$set' => $insertIntoChatArr,
//                            ), array("upsert" => true)
//                    );

            echo json_encode(array('ErrorCode' => '3', 'Message' => 'Login Successful', 'Data' => array('FirstName' => $FindEmail['FirstName'],
                    'LastName' => $FindEmail['LastName'], 'SeesionToken' => (string) $FindEmail['_id'])));
            return;
        }
    } else {
        echo json_encode(array('ErrorCode' => '4', 'Message' => 'User Not Available'));
        return;
    }
    echo json_encode(array('ErrorCode' => '5', 'Message' => 'Something Wrong'));
    return;
}
);

/**
 * ForgotPassword
 *
 * Here We allow user to login to kidzo app
 * It takes require parameters and stores the data in database and gives a unique id for that user.
 *
 *
 */
$app->post(
        '/ForgotPassword', function () {

    if ($_POST['Email'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    //conection to mongodb
    // $mongoDB = getConnection();
    //check user exist
    $UserId = GetExistingUer($_POST['Email']);

    if ($UserId != '') {
        $result = SendForGotPasswordMail($_POST['Email']);

        if ($result == 1) {
            echo json_encode(array('ErrorCode' => '2', 'Message' => 'Check You Email For Password Reset Link'));
            return;
        }
        echo json_encode(array('ErrorCode' => '4', 'Message' => 'Problem in email sending'));
        return;
    } else {
        echo json_encode(array('ErrorCode' => '3', 'Message' => 'User Not Available'));
        return;
    }
    echo json_encode(array('ErrorCode' => '5', 'Message' => 'Something Wrong'));
    return;
}
);

/**
 * GetUserDetails
 *
 * Here We give user's data to user
 * It takes require parameters and give the data for that user.
 *
 *
 */
$app->post(
        '/GetUserDetails', function () use ($app) {

    if ($_POST['SessionToken'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    //check session expired or not

    $app->etag('122376428');
    $mongoDB = getConnection();
    try {
        $USerData = $mongoDB->selectCollection('UserData');
        $FindEmail = $USerData->findOne(array('_id' => new MongoId($_POST['SessionToken'])));
    } catch (Exception $ex) {
        echo json_encode(array('ErrorCode' => '5', 'Message' => 'Invalid Token'));
        return;
    }
    // echo json_encode(array('ErrorCode' => '1', 'Message' => $FindEmail));
    if (empty($FindEmail)) {
        echo json_encode(array('ErrorCode' => '3', 'Message' => 'User Not Available'));
        return;
    } else {
        $checkSession = ChecktSessionExpiry($_POST['SessionToken']);
        if ($checkSession == 1) {
            echo json_encode(array('ErrorCode' => '2', 'Message' => 'Session Expired Login Again'));
            return;
        }
        $ChildData = $mongoDB->selectCollection('ChildData');
        $childs = $ChildData->find(array('ParentId' => $_POST['SessionToken']));
        $childrens = array();
        $childInterests = array();
        $GetInterets = $mongoDB->selectCollection('Interests');
        foreach ($childs as $obj) {
            try {
                $Interests = explode(',', $obj['Interests']);
                foreach ($Interests as $Int) {
                    $childInt = array();
                    $childInt = $GetInterets->findOne(array('_id' => new MongoId($Int)));
//                            $childInterests[] = $childInt['Interest'];
                    $childInterests[] = array('id' => (string) $childInt['_id'], 'Interest' => $childInt['Interest']);
                }
            } catch (Exception $x) {
                
            }

            $childrens[] = array('ChildId' => (string) $obj['_id'], 'FirstName' => $obj['FirstName'], 'LastName' => $obj['LastName'],
                'Sex' => $obj['Sex'], 'DOB' => $obj['DOB'], 'Likes' => $obj['Likes'], 'DisLikes' => $obj['DisLikes'],
                'Grade' => $obj['Grade'], 'School' => $obj['School'], 'Grade' => $obj['Grade'], 'Interests' => $childInterests);
        }
        $landline = '';
        try {
            $landline = $FindEmail['LandLine'];
        } catch (Exception $ex) {
            
        }

        $SpouseMobile = '';
        try {
            $SpouseMobile = $FindEmail['SpouseMobile'];
        } catch (Exception $ex) {
            
        }
        $SpouseFirstName = '';
        try {
            $SpouseFirstName = $FindEmail['SpouseFirstName'];
        } catch (Exception $ex) {
            
        }
        $SpouseLastName = '';
        try {
            $SpouseLastName = $FindEmail['SpouseLastName'];
        } catch (Exception $ex) {
            
        }
        $SpouseSex = '';
        try {
            $SpouseSex = $FindEmail['SpouseSex'];
        } catch (Exception $ex) {
            
        }
        $SpousePicUrl = '';
        try {
            $SpousePicUrl = $FindEmail['SpousePicUrl'];
        } catch (Exception $ex) {
            
        }
        $SpouseEmail = '';
        try {
            $SpouseEmail = $FindEmail['SpouseEmail'];
        } catch (Exception $ex) {
            
        }
        $SpouseDOB = '';
        try {
            $SpouseDOB = $FindEmail['SpouseDOB'];
        } catch (Exception $ex) {
            
        }
        $Mobile = '';
        try {
            $Mobile = $FindEmail['Mobile'];
        } catch (Exception $ex) {
            
        }

        echo json_encode(array('ErrorCode' => '4', 'Message' => 'Successful', 'Data' => array(
                'MyDetails' => array('FirstName' => $FindEmail['FirstName'], 'LandLine' => $landline,
                    'LastName' => $FindEmail['LastName'], 'Sex' => $FindEmail['Sex'], 'Email' => $FindEmail['Email'], 'DOB' => $FindEmail['DOB'],
                    'PicUrl' => $FindEmail['PicUrl'], 'Mobile' => $Mobile), 'SpouseDetails' => array(
                    'Mobile' => $SpouseMobile,
                    'FirstName' => $SpouseFirstName,
                    'LastName' => $SpouseLastName, 'Sex' => $SpouseSex, 'DOB' => $SpouseDOB,
                    'PicUrl' => $SpousePicUrl, 'Email' => $SpouseEmail), 'Childs' => $childrens)
        ));
        return;
    }

    echo json_encode(array('ErrorCode' => '5', 'Message' => 'Invalid Token'));
}
);



/**
 * UpdateProfile
 *
 * Here We Update the User Profile
 * It takes require parameters and Updates the data in database and gives a confirmation of data updated
 *
 *
 */
$app->post(
        '/UpdateProfile', function () {

    if ($_POST['FirstName'] == '' ||
            $_POST['SessionToken'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    //conection to mongodb
    $mongoDB = getConnection();
    //check user exist
    $USerData = $mongoDB->selectCollection('UserData');
    $FindEmail = $USerData->findOne(array('_id' => new MongoId($_POST['SessionToken'])));


    if (!empty($FindEmail)) {
        $checkSession = ChecktSessionExpiry($_POST['SessionToken']);
        if ($checkSession == 1) {
            echo json_encode(array('ErrorCode' => '2', 'Message' => 'Session Expired Login Again'));
            return;
        }
        $insertIntoChatArr = array('FirstName' => $_POST['FirstName'],
            'LastName' => $_POST['LastName'], 'Sex' => $_POST['Sex'], 'DOB' => $_POST['DOB'],
            'PicUrl' => $_POST['PicUrl'], 'Mobile' => $_POST['Mobile'],
            'SpouseMobile' => $_POST['SpouseMobile'],
            'LandLine' => $_POST['LandLine'],
            'SpouseFirstName' => $_POST['SpouseFirstName'],
            'SpouseLastName' => $_POST['SpouseLastName'], 'SpouseSex' => $_POST['SpouseSex'], 'SpouseDOB' => $_POST['SpouseDOB'],
            'SpousePicUrl' => $_POST['SpousePicUrl'], 'SpouseEmail' => $_POST['SpouseEmail']);
        $USerData->update(
                array('_id' => new MongoId($_POST['SessionToken'])), array(
            '$set' => $insertIntoChatArr,
                ), array("upsert" => true)
        );

        echo json_encode(array('ErrorCode' => '3', 'Message' => 'Profile Updated'));
        return;
    } else {
        echo json_encode(array('ErrorCode' => '4', 'Message' => 'User Not Available'));
        return;
    }
    echo json_encode(array('ErrorCode' => '5', 'Message' => 'Something Wrong'));
    return;
}
);
/**
 * AddChild
 *
 * Here We can Add New 
 * It takes require parameters and stores the data in database and gives a unique id for that user.
 *
 *
 */
$app->post(
        '/AddChild', function () {

    if ($_POST['FirstName'] == '' || $_POST['SessionToken'] == '' ||
            $_POST['DateTime'] == '' || $_POST['Sex'] == '' || $_POST['DOB'] == '') {

        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    $mongoDB = getConnection();
    $checkSession = ChecktSessionExpiry($_POST['SessionToken']);
    if ($checkSession == 1) {
        echo json_encode(array('ErrorCode' => '2', 'Message' => 'Session Expired Login Again'));
        return;
    }
    $date = new DateTime();
    $timestamp = $date->getTimestamp();
    $UserTable = $mongoDB->selectCollection('ChildData');
    $insertUserArr = array('ParentId' => $_POST['SessionToken'], 'FirstName' => $_POST['FirstName'], 'LastName' => $_POST['LastName'],
        'Sex' => $_POST['Sex'], 'DOB' => $_POST['DOB'], 'DisLikes' => $_POST['DisLikes'],
        'Grade' => $_POST['Grade'], 'School' => $_POST['School'], 'Likes' => $_POST['Likes'],
        'RegisterOn' => $timestamp, 'About' => $_POST['About']);

    $UserTable->insert($insertUserArr);
    $newDocID = $insertUserArr['_id'];
    echo json_encode(array('ErrorCode' => '3', 'Message' => 'Child Successfully Added', 'Data' => array('ChildId' => (string) $newDocID)));
    return;
}
);

/**
 * UpdateChildData
 *
 * Here We Update the Child Data
 * It takes require parameters and stores the data in database and gives a unique id for that user.
 *
 *
 */
$app->post(
        '/UpdateChildData', function () {

    if ($_POST['FirstName'] == '' || $_POST['SessionToken'] == '' || $_POST['ChildId'] == '' ||
            $_POST['DateTime'] == '' || $_POST['Sex'] == '' || $_POST['DOB'] == '') {

        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    $mongoDB = getConnection();
    $checkSession = ChecktSessionExpiry($_POST['SessionToken']);
    if ($checkSession == 1) {
        echo json_encode(array('ErrorCode' => '2', 'Message' => 'Session Expired Login Again'));
        return;
    }
    $date = new DateTime();
    $timestamp = $date->getTimestamp();
    $ChildTable = $mongoDB->selectCollection('ChildData');
    $findChild = $ChildTable->findOne(array('_id' => new MongoId($_POST['ChildId'])));
    if (empty($findChild)) {
        echo json_encode(array('ErrorCode' => '3', 'Message' => 'Child Not Available'));
        return;
    }
    $insertUserArr = array('FirstName' => $_POST['FirstName'], 'LastName' => $_POST['LastName'],
        'Sex' => $_POST['Sex'], 'DOB' => $_POST['DOB'], 'DisLikes' => $_POST['DisLikes'],
        'Grade' => $_POST['Grade'], 'School' => $_POST['School'], 'Likes' => $_POST['Likes'],
        'RegisterOn' => $timestamp, 'About' => $_POST['About']);

    $USerData->update(
            array('_id' => new MongoId($_POST['ChildId'])), array(
        '$set' => $insertUserArr,
            ), array("upsert" => true)
    );

    echo json_encode(array('ErrorCode' => '4', 'Message' => 'Child Successfully Updated'));
    return;
}
);
/**
 * UpdateChildInterest
 *
 * Here We Update the Child Interests
 * It takes require parameters and stores the data in database and gives a unique id for that user.
 *
 *
 */
$app->post(
        '/UpdateChildInterest', function () {

    if ($_POST['Interests'] == '' || $_POST['SessionToken'] == '' || $_POST['ChildId'] == '') {

        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    $mongoDB = getConnection();
    $checkSession = ChecktSessionExpiry($_POST['SessionToken']);
    if ($checkSession == 1) {
        echo json_encode(array('ErrorCode' => '2', 'Message' => 'Session Expired Login Again'));
        return;
    }
    $date = new DateTime();
    $timestamp = $date->getTimestamp();
    $ChildTable = $mongoDB->selectCollection('ChildData');
    $findChild = $ChildTable->findOne(array('_id' => new MongoId($_POST['ChildId'])));
    if (empty($findChild)) {
        echo json_encode(array('ErrorCode' => '3', 'Message' => 'Child Not Available'));
        return;
    }
    $insertUserArr = array('Interests' => $_POST['Interests']);

    $ChildTable->update(
            array('_id' => new MongoId($_POST['ChildId'])), array(
        '$set' => $insertUserArr,
            ), array("upsert" => true)
    );

    echo json_encode(array('ErrorCode' => '4', 'Message' => 'Interests Successfully Updated'));
    return;
}
);



/**
 * GetEvents
 *
 * Here We give user's data to user
 * It takes require parameters and give the data for that user.
 *
 *
 */
$app->post(
        '/GetEvents', function () {

    if ($_POST['SessionToken'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    //check session expired or not

    $mongoDB = getConnection();
    try {
        $USerData = $mongoDB->selectCollection('UserData');
        $FindEmail = $USerData->findOne(array('_id' => new MongoId($_POST['SessionToken'])));
    } catch (Exception $ex) {
        echo json_encode(array('ErrorCode' => '5', 'Message' => 'Invalid Token'));
        return;
    }
    // echo json_encode(array('ErrorCode' => '1', 'Message' => $FindEmail));

    $GetAllEvents = $mongoDB->selectCollection('Events');
    $ev = $GetAllEvents->find();

    foreach ($ev as $obj) {

        $childrens[] = array('EventId' => (string) $obj['_id'], 'AgeGroup' => $obj['AgeGroup'], 'Description' => $obj['Description'],
            'EndDate' => $obj['EndDate'], 'EndTime' => $obj['EndTime'], 'EventName' => $obj['EventName'], 'EventTypes' => $obj['EventTypes'],
            'FacebookLink' => $obj['FacebookLink'], 'ImageUrl' => $obj['ImageUrl'],
            'Latitude' => $obj['Latitude'], 'Location_Type' => $obj['Location_Type'], 'Latitude' => $obj['Longitude'],
            'Occasion' => $obj['Occasion'], 'OldEventImages' => $obj['OldEventImages'], 'Latitude' => $obj['Latitude'],
            'PostalCode' => $obj['PostalCode'], 'RegOpen' => $obj['RegOpen'], 'RegistrationArea' => $obj['RegistrationArea'],
            'RegistrationCountry' => $obj['RegistrationCountry'], 'RegistrationLongitude' => $obj['RegistrationLongitude'], 'RegistrationLatitude' => $obj['RegistrationLatitude'],
            'StartDate' => $obj['StartDate'], 'Registrationvenue' => $obj['Registrationvenue'], 'RegistrationPostalCode' => $obj['RegistrationPostalCode'],
            'StartTime' => $obj['StartTime'], 'Venue' => $obj['Venue'], 'Status' => $obj['Status'],);
    }



    echo json_encode(array('ErrorCode' => '4', 'Message' => 'Successful', 'Data' => $childrens));
    return;
}
);


$app->post(
        '/GetProductCatsubcat', function () {

    if ($_POST['SessionToken'] == '') {
        echo json_encode(array('ErrorCode' => '1', 'Message' => 'Mandatory Field Missing'));
        return;
    }
    //check session expired or not

    $mongoDB = getConnection();
    try {
        $USerData = $mongoDB->selectCollection('UserData');
        $FindEmail = $USerData->findOne(array('_id' => new MongoId($_POST['SessionToken'])));
    } catch (Exception $ex) {
        echo json_encode(array('ErrorCode' => '5', 'Message' => 'Invalid Token'));
        return;
    }
    // echo json_encode(array('ErrorCode' => '1', 'Message' => $FindEmail));
//
//    $GetProductCatsubcat = $mongoDB->selectCollection('OfferingCategories');
//    $ev = $GetProductCatsubcat->findOne(array('BusinessTypeId' => $_POST['businessid']));

//    foreach ($ev as $obj) {
//
//        $childrens[] = array('EventId' => (string) $obj['_id'], 'AgeGroup' => $obj['AgeGroup'], 'Description' => $obj['Description'],
//            'EndDate' => $obj['EndDate'], 'EndTime' => $obj['EndTime'], 'EventName' => $obj['EventName'], 'EventTypes' => $obj['EventTypes'],
//            'FacebookLink' => $obj['FacebookLink'], 'ImageUrl' => $obj['ImageUrl'],
//            'Latitude' => $obj['Latitude'], 'Location_Type' => $obj['Location_Type'], 'Latitude' => $obj['Longitude'],
//            'Occasion' => $obj['Occasion'], 'OldEventImages' => $obj['OldEventImages'], 'Latitude' => $obj['Latitude'],
//            'PostalCode' => $obj['PostalCode'], 'RegOpen' => $obj['RegOpen'], 'RegistrationArea' => $obj['RegistrationArea'],
//            'RegistrationCountry' => $obj['RegistrationCountry'], 'RegistrationLongitude' => $obj['RegistrationLongitude'], 'RegistrationLatitude' => $obj['RegistrationLatitude'],
//            'StartDate' => $obj['StartDate'], 'Registrationvenue' => $obj['Registrationvenue'], 'RegistrationPostalCode' => $obj['RegistrationPostalCode'],
//            'StartTime' => $obj['StartTime'], 'Venue' => $obj['Venue'], 'Status' => $obj['Status'],);
//    }

 $GetAllEvents = $mongoDB->selectCollection('Events');
    $ev = $GetAllEvents->find();

    foreach ($ev as $obj) {

        $childrens[] = array('EventId' => (string) $obj['_id'], 'AgeGroup' => $obj['AgeGroup'], 'Description' => $obj['Description'],
            'EndDate' => $obj['EndDate'], 'EndTime' => $obj['EndTime'], 'EventName' => $obj['EventName'], 'EventTypes' => $obj['EventTypes'],
            'FacebookLink' => $obj['FacebookLink'], 'ImageUrl' => $obj['ImageUrl'],
            'Latitude' => $obj['Latitude'], 'Location_Type' => $obj['Location_Type'], 'Latitude' => $obj['Longitude'],
            'Occasion' => $obj['Occasion'], 'OldEventImages' => $obj['OldEventImages'], 'Latitude' => $obj['Latitude'],
            'PostalCode' => $obj['PostalCode'], 'RegOpen' => $obj['RegOpen'], 'RegistrationArea' => $obj['RegistrationArea'],
            'RegistrationCountry' => $obj['RegistrationCountry'], 'RegistrationLongitude' => $obj['RegistrationLongitude'], 'RegistrationLatitude' => $obj['RegistrationLatitude'],
            'StartDate' => $obj['StartDate'], 'Registrationvenue' => $obj['Registrationvenue'], 'RegistrationPostalCode' => $obj['RegistrationPostalCode'],
            'StartTime' => $obj['StartTime'], 'Venue' => $obj['Venue'], 'Status' => $obj['Status'],);
    }


    echo json_encode(array('ErrorCode' => '4', 'Message' => 'Successful', 'Data' => $childrens));
    return;
}
);



/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();

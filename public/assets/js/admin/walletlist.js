var app = new Vue({
            el: "#appUserList",
            data: {
                isProcessing: false,               
                marchant: [],
                search_keyword:'',
                status:'',
                vendor:'',
                order_type:'',
                created_at:'',                
                pagination_1: {
                total_1: 0,
                per_page_1: 10,
                from_1: 1,
                to_1: 0,
                current_page_1: 1,
                last_page_1: 0
                },               
                offset_1: 5 
            },
            mounted:function()
            {
              this.makePagination_1(_marchant[0]);

            },
           computed: {
            isActived_1: function () {
            return this.pagination_1.current_page_1;
            },           
            pagesNumber_1: function () {
            if (!this.pagination_1.to_1) {
               return [];
            }
            var from_1 = this.pagination_1.current_page_1 - this.offset_1;
            if (from_1 < 1) {
               from_1 = 1;
            }
            var to_1 = from_1 + (this.offset_1 * 2);
            if (to_1 >= this.pagination_1.last_page_1) {
               to_1 = this.pagination_1.last_page_1;
            }
            var pagesArray_1 = [];
            while (from_1 <= to_1) {
               pagesArray_1.push(from_1);
               from_1++;
            }
            return pagesArray_1;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'marchant', _marchant[0].data);            
            },
            methods: {               
                changePage_1: function (page) {
                    this.pagination_1.current_page_1 = page;
                    app.filter_1(page);
                },               
                makePagination_1: function(data){
                    var pagination_1 = {
                    total_1: data.total,
                    from_1: data.from,
                    to_1: data.to,
                    current_page_1: data.current_page,
                    last_page_1: data.last_page,
                    per_page_1: data.per_page
                }
                Vue.set(this.$data, 'pagination_1', pagination_1);
                }, 
               
               filter_1:function(page)
               {  
                 var fill_date = $('#from_date').val();                  
                 this.$http.post('/admin/wallet/search',{'created_at': fill_date,'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'marchant', response.data['marchant'].data);
                    app.makePagination_1(response.data['marchant']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                }) 
               }, 
                search:function()
              { 
               var fill_date = $('#from_date').val();
                 this.$http.post('/admin/wallet/search',{'created_at': fill_date,})
                .then(function(response) {                    
                     Vue.set(this.$data,'marchant', response.data['marchant'].data);
                    app.makePagination_1(response.data['marchant']);
                    
                    setTimeout(function(){ 
                     }, 1000);    
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },          
             clearFilter: function()
              {
                $('#from_date').val("");
                this.filter_1(0);
              },
              View:function(id)
              {
                window.location = '/admin/payment/deduct/'+id;
              },   
              Transaction:function(id)
              {
                window.location = '/admin/payment/transaction/'+id;
              },           
          },
    });
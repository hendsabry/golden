var app = new Vue({
            el: "#appUserList",
            data: {
                isProcessing: false,
                users: [],
                search_keyword:'',
                status:'',
                access_group_id:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                offset: 5 
            },
            mounted:function()
            {
              this.makePagination(_users[0]);

            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            pagesNumber: function () {
            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'users', _users[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
               // alert(this.pagination.last_page);
                },

                 filter:function(page)
               {    
                 this.$http.post('/admin/sub_admin/search',{'search_keyword': this.search_keyword,'access_group_id': this.access_group_id,
                  'status':this.status,
                  'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
                  
               },  
         
         
          statusChanged:function(clickedid)
              {
                 var lang = $('#lang').val();
                
                if (document.getElementById(clickedid).checked == true) 
                {
                  if (lang == 'ar'){
                     swal({
                  title: "هل أنت واثق؟",
                  text: "ستنشط حالة المسؤول الفرعي!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "حالة المسؤول الفرعي لم يتم تغيير", "error");
                  }
                  });  
                  }else{
                     swal({
                  title: "Are you sure?",
                  text: "You will Active the status of sub admin!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "Sub Admin status not has been changed :)", "error");
                  }
                  });  
                  }
                 
                } else 
                {
                  if (lang == 'ar') {
                    swal({
                  title: "هل أنت واثق؟",
                  text: "سوف غير نشط في حالة المسؤول الفرعي!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                    swal("ألغيت", "حالة المسؤول الفرعي لم يتم تغيير :)", "error");
                  }
                  });  
                  } else {
                    swal({
                  title: "Are you sure?",
                  text: "You will Inactive the status of sub admin!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                  swal("Cancelled", "Sub Admin status not has been changed :)", "error");
                  }
                  });  
                  }
                }
              },
        
      
         confirmStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                  this.$http.post('/admin/sub_admin/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }else{
                  this.$http.post('/admin/sub_admin/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("Changed!", "Your status  has been changed.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }
              },

              inactiveStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                    this.$http.post('/admin/sub_admin/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }else{
                  this.$http.post('/admin/sub_admin/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("Changed!", "Your status  has been changed.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }
              },
               search:function()
              { 
                 this.$http.post('/admin/sub_admin/search',{'search_keyword': this.search_keyword,'access_group_id': this.access_group_id,
                  'status':this.status})
                .then(function(response) {
                     Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    
                    setTimeout(function(){ 
                     }, 1000);
                    
                    
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },
              clearFilter: function()
              {
                this.access_group_id ='';
                this.status ='';
                this.filter(0);
              },
               downloadCsvFilter:function()
              {
                window.location ='/admin/sub_admin/seach_csv?search_keyword='+this.search_keyword  +'&status='+ this.status +'&access_group_id='+ this.access_group_id;
              },
               DeleteUser:function(id)
              {
                var lang = $('#lang').val();
                if (lang == 'ar'){
                    swal({
                  title: "هل أنت واثق؟",
                  text: "سوف تحذف هذا المسؤول الفرعي!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("تغير!", "تم حذف هذا المسؤول الفرعي.", "success");
                      window.location = '/admin/sub_admin/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "هذا المسؤول الفرعي لم يتم حذف", "error");
                  }
                  });
                }else{
                    swal({
                  title: "Are you sure?",
                  text: "You want to delete this sub admin!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("Changed!", "This Sub Admin has been deleted.", "success");
                      window.location = '/admin/sub_admin/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "This Sub Admin not has been deleted :)", "error");
                  }
                  });
                } 
              },
              ViewUser:function(id)
              {
                window.location = '/admin/sub_admin/view/'+id;
              },
              addSubAdmin:function(){
                 window.location = '/admin/subadmin/add';
              },
              EditUser:function(id){
                 window.location = '/admin/subadmin/edit/'+id;
              }
          },
    });
var app = new Vue({
            el: "#appOccasionList",
            data: {
                isProcessing: false,
                occasionlist: [],
                search_keyword:'',
                status:'',
                occasion_type_id:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                offset: 5 
            },
            mounted:function()
            {
              this.makePagination(_occasionlist[0]);
            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            pagesNumber: function () {
            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'occasionlist', _occasionlist[0].data);
            },
            methods: {
               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
               // alert(this.pagination.last_page);
                },

                 filter:function(page)
                {  
                 this.$http.post('/admin/occasions/paginate',{'occasion_type_id': this.occasion_type_id,
                  'search_keyword': this.search_keyword,
                  'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'occasionlist', response.data['occasionlist'].data);
                    app.makePagination(response.data['occasionlist']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                   // Vue.set(this.$data, 'errors', response.data);
                })
                  
               },  

              filterSubmit:function()
              {
                this.$http.post('/admin/occasions/filter',{'occasion_type_id': this.occasion_type_id
                         })
                       .then(function(response) 
                       {
                            Vue.set(this.$data,'occasionlist', response.data['occasionlist'].data);
                            app.makePagination(response.data['occasionlist']);
                            setTimeout(function(){ 
                            }, 1000);
                       })
                       .catch(function(response) {
                           Vue.set(this.$data, 'errors', response.data);
                       })
              },
               search:function()
              { 
                 this.$http.post('/admin/occasions/search',{'search_keyword': this.search_keyword
                  })
                .then(function(response) {
                     Vue.set(this.$data,'occasionlist', response.data['occasionlist'].data);
                    app.makePagination(response.data['occasionlist']);
                    setTimeout(function(){ 
                     }, 1000);  
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },
              clearFilter: function()
              {
                 this.occasion_type_id ='';
                 this.search_keyword ='';
                 this.filter(0);
              },
               downloadCsvFilter:function()
              {
           
                window.location ='/admin/occasions/csvdownload?search_keyword='+this.search_keyword  +'&occasion_type_id='+ this.occasion_type_id;
              },
               DeleteUser:function(id)
               {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                      swal({
                  title: "هل أنت واثق؟",
                  text: "سوف تحذف هذه المناسبة!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("تغير!", "تم حذف هذه المناسبة.", "success");
                      window.location = '/admin/occasions/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "هذه المناسبة لم يتم حذفها :)", "error");
                  }
                  });
                }else{
                  swal({
                  title: "Are you sure?",
                  text: "You will delete this Occasion!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("Changed!", "This Occasion has been deleted.", "success");
                      window.location = '/admin/occasions/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "This Occasion not has been deleted :)", "error");
                  }
                  });
                }
              },
              ViewUser:function(id)
             {
             // alert(id);
              window.location = '/admin/occasions/view/'+id;
             },
              statusChanged:function(clickedid)
              {
                 var lang = $('#lang').val();
                
                if (document.getElementById(clickedid).checked == true) 
                {
                  if (lang == 'ar'){
                     swal({
                  title: "هل أنت واثق؟",
                  text: "ستنشط حالة المسؤول الفرعي!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "حالة المسؤول الفرعي لم يتم تغيير", "error");
                  }
                  });  
                  }else{
                     swal({
                  title: "Are you sure?",
                  text: "You will Active the status of Occasion!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "Occasion status not has been changed :)", "error");
                  }
                  });  
                  }
                 
                } else 
                {
                  if (lang == 'ar') {
                    swal({
                  title: "هل أنت واثق؟",
                  text: "سوف غير نشط في حالة المسؤول الفرعي!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                    swal("ألغيت", "حالة المسؤول الفرعي لم يتم تغيير :)", "error");
                  }
                  });  
                  } else {
                    swal({
                  title: "Are you sure?",
                  text: "You will Inactive the status of Occasion!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                  swal("Cancelled", "Occasion status not has been changed :)", "error");
                  }
                  });  
                  }
                }
              },
        
      
         confirmStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                  this.$http.post('/admin/occasion/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }else{
                  this.$http.post('/admin/occasion/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("Changed!", "Occasion status  has been changed.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }
              },

              inactiveStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                    this.$http.post('/admin/occasion/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }else{
                  this.$http.post('/admin/occasion/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("Changed!", "Occasion  has been changed.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }
              },
              
          },
    });
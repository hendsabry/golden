var app = new Vue({
            el: "#appMessageList",
            data: {
                isProcessing: false,
                msg: [],
                search_keyword:'',
                status:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                offset: 5 
            },
            mounted:function()
            {
              this.makePagination(_msg[0]);
            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            pagesNumber: function () {
            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            }
            },
            created: function () 
            {
            Vue.set(this.$data, 'msg', _msg[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
                },
                 filter:function(page)
               {   
                 this.$http.post('/admin/static_content_management/broadcasst_message/paginate',{'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'msg', response.data['msg'].data);
                    app.makePagination(response.data['msg']);
                    setTimeout(function(){ 
                     }, 1000);
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })                  
               },              
              broardcastMessage:function(){
                 window.location = '/admin/static_content_management/broadcasst_message';
              },
               view:function(id)
              {
                window.location = '/admin/static_content_management/broadcasst_message/view/'+id;
              }
          },
    });
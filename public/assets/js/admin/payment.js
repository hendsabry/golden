var app = new Vue({
            el: "#appUserList",
            data: {
                isProcessing: false,
                users: [],
                marchant: [],
                search_keyword:'',
                status:'',
                vendor:'',
                order_type:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                pagination_1: {
                total_1: 0,
                per_page_1: 10,
                from_1: 1,
                to_1: 0,
                current_page_1: 1,
                last_page_1: 0
                },
                offset: 5,
                offset_1: 5 
            },
            mounted:function()
            {
              this.makePagination(_users[0]);
              this.makePagination_1(_marchant[0]);

            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            isActived_1: function () {
            return this.pagination_1.current_page_1;
            },
            pagesNumber: function () {

            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            },
            pagesNumber_1: function () {
            if (!this.pagination_1.to_1) {
               return [];
            }
            var from_1 = this.pagination_1.current_page_1 - this.offset_1;
            if (from_1 < 1) {
               from_1 = 1;
            }
            var to_1 = from_1 + (this.offset_1 * 2);
            if (to_1 >= this.pagination_1.last_page_1) {
               to_1 = this.pagination_1.last_page_1;
            }
            var pagesArray_1 = [];
            while (from_1 <= to_1) {
               pagesArray_1.push(from_1);
               from_1++;
            }
            return pagesArray_1;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'users', _users[0].data);
            Vue.set(this.$data, 'marchant', _marchant[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                changePage_1: function (page) {
                    this.pagination_1.current_page_1 = page;
                    app.filter_1(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
                 // alert(this.pagination.current_page);
                },
                makePagination_1: function(data){
                    var pagination_1 = {
                    total_1: data.total,
                    from_1: data.from,
                    to_1: data.to,
                    current_page_1: data.current_page,
                    last_page_1: data.last_page,
                    per_page_1: data.per_page
                }
                Vue.set(this.$data, 'pagination_1', pagination_1);
                },


                 filter:function(page)
               {  
                  var fill_date = $('#from_date').val();
                 this.$http.post('/admin/payment/filter/customer',{'created_date': fill_date,'order_type': this.order_type,
                  'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
                  
               },
               filter_1:function(page)
               {  
                  var fill_date = $('#from_date').val();
                 this.$http.post('/admin/payment/filter/merchant',{'created_date': fill_date,'order_type': this.order_type,
                  'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'marchant', response.data['marchant'].data);
                    app.makePagination_1(response.data['marchant']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                }) 
               },    
               search:function()
              {  var fill_date = $('#from_date').val();
                 this.$http.post('/admin/payment/search',{'created_date': fill_date,'order_type': this.order_type,
               })
                .then(function(response) {
                     Vue.set(this.$data,'users', response.data['users'].data);
                     Vue.set(this.$data,'marchant', response.data['marchant'].data);
                    app.makePagination(response.data['users']);
                    
                    setTimeout(function(){ 
                     }, 1000);
                    
                    
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },
              clearFilter: function()
              {
                this.vendor ='';
                this.rating ='';
                this.status ='';
                $('#from_date').val("");
                this.filter(0);
              },

              ViewCredited:function(id)
              {
                window.location = '/admin/payment/credited/view/'+id;
              },
              ViewDebited:function(id)
              {
                window.location = '/admin/payment/debited/view/'+id;
              },
               downloadCsvFilterCredit:function()
              {
                var fill_date = $('#from_date').val();
                window.location ='/admin/credit/search_csv?created_date='+fill_date  +'&order_type='+ this.order_type;
              },
              downloadCsvFilterDebited:function()
              {
                var fill_date = $('#from_date').val();
                window.location ='/admin/debit/search_csv?created_date='+fill_date  +'&order_type='+ this.order_type;
              },
          },
    });
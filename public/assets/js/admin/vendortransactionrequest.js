var app = new Vue({
            el: "#appUserList",
            data: {
                isProcessing: false,
                users: [],                
                search_keyword:'',
                status:'',
                vendor:'',
                order_type:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                pagination_1: {
                total_1: 0,
                per_page_1: 10,
                from_1: 1,
                to_1: 0,
                current_page_1: 1,
                last_page_1: 0
                },
                offset: 5,
                offset_1: 5 
            },
            mounted:function()
            {
              this.makePagination(_users[0]);
              this.makePagination_1(_users[0]);

            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            isActived_1: function () {
            return this.pagination_1.current_page_1;
            },
            pagesNumber: function () {

            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            },           
            },
            created: function () 
            {            
            Vue.set(this.$data, 'users', _users[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },               
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
                 // alert(this.pagination.current_page);
                },
                 filter:function(page,id)
               {                    
                 this.$http.post('/admin/vendor_management/view/{id}',{'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
                  
               },    
              View:function(id)
              {
                window.location = '/admin/vendor_management/vendor_payment/debitview/'+id;
              },
              Pay:function(id)
              {
                window.location = '/admin/vendor_management/vendor_payment/'+id;
              },
          },
    });
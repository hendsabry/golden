var app = new Vue({
            el: "#appElectInviteList",
            data: {
                isProcessing: false,
                elect_invite: [],
                search_keyword:'',
                event_type:'',
                status:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                offset: 5 
            },
            mounted:function()
            {
              this.makePagination(_elect_invite[0]);

            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            pagesNumber: function () {
            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'elect_invite', _elect_invite[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
               // alert(this.pagination.last_page);
                },

                 filter:function(page)
               {  var fill_date = $('#from_date').val();
                 this.$http.post('/admin/electronic_invitation/list/search',{'created_date': fill_date,
                  'event_type':this.event_type})
                .then(function(response) 
                {
                    Vue.set(this.$data,'elect_invite', response.data['elect_invite'].data);
                    app.makePagination(response.data['elect_invite']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
                  
               },
               statusChanged:function(clickedid)
              {
                 var lang = $('#lang').val();
                
                if (document.getElementById(clickedid).checked == true) 
                {
                  if (lang == 'ar'){
                     swal({
                  title: "هل أنت واثق؟",
                  text: "ستنشط حالة بطاقة المصمم!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "حالة البطاقة لم يتم تغييرها", "error");
                  }
                  });  
                  }else{
                     swal({
                  title: "Are you sure?",
                  text: "You will Active the status of Designer Card!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "Designer Card status not has been changed :)", "error");
                  }
                  });  
                  }
                 
                } else 
                {
                  if (lang == 'ar') {
                    swal({
                  title: "هل أنت واثق؟",
                  text: "سوف غير نشط في حالة بطاقة مصمم!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                    swal("ألغيت", "حالة البطاقة لم يتم تغييرها", "error");
                  }
                  });  
                  } else {
                    swal({
                  title: "Are you sure?",
                  text: "You will Inactive the status of Designer Card!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                  swal("Cancelled", "Designer Card status not has been changed :)", "error");
                  }
                  });  
                  }
                }
              },
         confirmStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                  this.$http.post('/admin/electronic_invitation/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("تغير!", "تم تغيير بطاقة المصمم.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }else{
                  this.$http.post('/admin/electronic_invitation/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("Changed!", "Designer Card has been changed.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }
              },

              inactiveStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                    this.$http.post('/admin/electronic_invitation/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("تغير!", "تم تغيير بطاقة المصمم.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }else{
                  this.$http.post('/admin/electronic_invitation/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("Changed!", "Designer Card has been changed.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }
              },  
             
               search:function()
              { 
                var fill_date = $('#from_date').val();
                 this.$http.post('/admin/electronic_invitation/list/search',{'created_date': fill_date,
                  'event_type':this.event_type})
                .then(function(response) {
                     Vue.set(this.$data,'elect_invite', response.data['elect_invite'].data);
                    app.makePagination(response.data['elect_invite']);                    
                    setTimeout(function(){ 
                     }, 1000);
                    
                    
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },
              clearFilter: function()
              {
                this.event_type =''; 
                //this.created_date ='';
                $('#from_date').val("");                             
                this.filter(0);
              },
               deletecard:function(pro_id)
              {
                var lang = $('#lang').val();
                  if (lang == 'ar'){
                      swal({
                  title: "هل أنت واثق؟",
                  text: "ستحذف بطاقة المصمم هذه!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("تغير!", "تم حذف هذه الصفحة.", "success");
                      window.location = '/admin/electronic_invitation/delete/'+pro_id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "لم يتم حذف بطاقة Designer هذه :)", "error");
                  }
                  });
                  }else{
                      swal({
                  title: "Are you sure?",
                  text: "You want to delete this Designer Card!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("Changed!", "This Page has been deleted.", "success");
                      window.location = '/admin/electronic_invitation/delete/'+pro_id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "This Designer card has been not deleted :)", "error");
                  }
                  });
                  }
              },
              Edit:function(pro_id)
                {
                  
                 window.location = '/admin/electronic_invitation/edit/'+pro_id;
                },
              view:function(pro_id)
              {
                window.location = '/admin/electronic_invitation/view/'+pro_id;
              },
              addStaticelect_invite:function(){
                 window.location = '/admin/electronic_invitation/add';
              }
          },
    });
var app = new Vue({
            el: "#appsubcategoryList",
            data: {
                isProcessing: false,
                subcategory: [],
                search_keyword:'',
                status:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                offset: 5 
            },
            mounted:function()
            {
              this.makePagination(_subcategory[0]);

            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            pagesNumber: function () {
            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'subcategory', _subcategory[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
                },

                 filter:function(page)
               {    
                 this.$http.post('/admin/category_management/sub_cateory_list/',{'search_keyword': this.search_keyword,
                  'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'subcategory', response.data['subcategory'].data);
                    app.makePagination(response.data['subcategory']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
                  
               },  
             
               search:function()
              { 
                 this.$http.post('/admin/category_management/sub_cateory_list/',{'search_keyword': this.search_keyword,
                  'status':this.status})
                .then(function(response) {
                     Vue.set(this.$data,'subcategory', response.data['subcategory'].data);
                    app.makePagination(response.data['subcategory']);
                    
                    setTimeout(function(){ 
                     }, 1000);
                    
                    
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },
              clearFilter: function()
              {
                this.search_keyword ='';
                this.status ='';
                this.filter(0);
              },
               deleteCms:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                  swal({
                  title: "هل أنت واثق؟",
                  text: "سوف تحذف هذه الصفحة!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("تغير!", "تم حذف هذه الصفحة.", "success");
                      window.location = '/admin/category_management/delete/'+id;
                  } else
                  {
                  swal("ألغيت", "هذه الصفحة لم يتم حذفها :)", "error");
                  }
                  });
                }else{
                  swal({
                  title: "Are you sure?",
                  text: "You want to delete this Page!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("Changed!", "This Page has been deleted.", "success");
                      window.location = '/admin/category_management/delete/'+id;
                  } else
                  {
                  swal("Cancelled", "This page not has been deleted :)", "error");
                  }
                  });
                }
              },
              Editsub:function(mc_id)
                {
                 window.location = '/admin/category_management/sub_categroy/edit/'+mc_id;
                },
              viewcat:function(id, id2)
                {
                   window.location ='/admin/category_management/main_category/'+id2;
                  // window.location ='/admin/category_management/'+id + '/' +id2;
                },
              addCategory:function()
                {
                   window.location = '/admin/category_management/add';
                }
          },
    });
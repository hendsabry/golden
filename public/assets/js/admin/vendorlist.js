var app = new Vue({
            el: "#appUserList",
            data: {
                isProcessing: false,
                users: [],
                search_keyword:'',
                search_shop:'',
                status:'',               
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                offset: 5 
            },
            mounted:function()
            {
              this.makePagination(_users[0]);

            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            pagesNumber: function () {
            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'users', _users[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
               // alert(this.pagination.last_page);
                },

                 filter:function(page)
               {    
                 this.$http.post('/admin/vendor_management/search',{'search_keyword': this.search_keyword,'search_shop': this.search_shop,
                  'status':this.status,
                  'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
                  
               },  
                statusChanged:function(clickedid)
              {
                 var lang = $('#lang').val();
                
                if (document.getElementById(clickedid).checked == true) 
                {
                  if (lang == 'ar'){
                     swal({
                  title: "هل أنت واثق؟",
                  text: "سوف نشارك في حالة الرصاص!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "حالة الرصاص لم تتغير", "error");
                  }
                  });  
                  }else{
                     swal({
                  title: "Are you sure?",
                  text: "You will Active the status of vendor!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "Vendor status not has been changed :)", "error");
                  }
                  });  
                  }
                 
                } else 
                {
                  if (lang == 'ar') {
                    swal({
                  title: "هل أنت واثق؟",
                  text: "سوف غير نشط في حالة الرصاص!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                    swal("ألغيت", "حالة الرصاص لم تتغير :)", "error");
                  }
                  });  
                  } else {
                    swal({
                  title: "Are you sure?",
                  text: "You will Inactive the status of vendor !",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                  swal("Cancelled", "Vendor status not has been changed :)", "error");
                  }
                  });  
                  }
                }
              },
            openmerchant:function(id,newName)
              {                 
                var lang = $('#lang').val();              
               window.location.href="/mobilogin?"+"id="+id+"&"+"name=" + newName+"&lang=en";
              },

      
         confirmStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                  this.$http.post('/admin/vendor_management/changeStatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }else{
                  this.$http.post('/admin/vendor_management/changeStatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("Changed!", "Your status  has been changed.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }
              },

              inactiveStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                    this.$http.post('/admin/vendor_management/changeStatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }else{
                  this.$http.post('/admin/vendor_management/changeStatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("Changed!", "Your status  has been changed.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }
              },
               search:function()
              { 
                 this.$http.post('/admin/vendor_management/search',{'search_keyword': this.search_keyword,'search_shop': this.search_shop,
                  'status':this.status})
                .then(function(response) {
                     Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    
                    setTimeout(function(){ 
                     }, 1000);                    
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },
              clearFilter: function()
              {
                this.search_keyword ='';
                this.search_shop='';
                this.status ='';
                this.filter(0);
              },
               downloadCsvFilter:function()
              {
                window.location ='/admin/user/search_csv?search_keyword='+this.search_keyword  +'&status='+ this.status;
              },
               Delete:function(id)
              {
                var lang = $('#lang').val();
                if (lang == 'ar'){
                    swal({
                  title: "هل أنت واثق؟",
                  text: "سوف تحذف هذا الرصاص!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("تغير!", "تم حذف هذا الرصاص.", "success");
                      window.location = '/admin/vendor_management/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "لم يتم حذف هذا الرصاص", "error");
                  }
                  });
                }else{
                    swal({
                  title: "Are you sure?",
                  text: "You will delete this vendor !",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("Changed!", "This vendor has been deleted.", "success");
                      window.location = '/admin/vendor_management/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "This vendor not has been deleted :)", "error");
                  }
                  });
                } 
              },
			  ViewList:function(id)
              {
                window.location = '/admin/vendor_management/viewlist/'+id;
              },
              View:function(id)
              {
                window.location = '/admin/vendor_management/view/'+id;
              },
              add:function(){
                 window.location = '/admin/vendor_management/add';
              },
              Edit:function(id){
                 window.location = '/admin/vendor_management/edit/'+id;
              },
               downloadCsvFilter:function()
              {
                window.location ='/admin/vendor/search_csv?search_keyword='+this.search_keyword  +'&status='+ this.status;
              },
          },
    });
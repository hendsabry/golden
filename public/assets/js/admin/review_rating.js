var app = new Vue({
            el: "#appUserList",
            data: {
                isProcessing: false,
                users: [],
                search_keyword:'',
                status:'',
                vendor:'',
                rating:'',
                created_date:'',
                pagination: {
                total: 0,
                per_page: 10,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 0
                },
                offset: 5 
            },
            mounted:function()
            {
              this.makePagination(_users[0]);

            },
           computed: {
            isActived: function () {
            return this.pagination.current_page;
            },
            pagesNumber: function () {
            if (!this.pagination.to) {
               return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
               from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
               to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
               pagesArray.push(from);
               from++;
            }
            return pagesArray;
            }
            },

            created: function () 
            {
            Vue.set(this.$data, 'users', _users[0].data);
            },
            methods: {

               changePage: function (page) {
                    this.pagination.current_page = page;
                    app.filter(page);
                },
                makePagination: function(data){
                    var pagination = {
                    total: data.total,
                    from: data.from,
                    to: data.to,
                    current_page: data.current_page,
                    last_page: data.last_page,
                    per_page: data.per_page
                }
                Vue.set(this.$data, 'pagination', pagination);
               // alert(this.pagination.last_page);
                },

                 filter:function(page)
               {   
                  var fill_date = $('#from_date').val();
                 this.$http.post('/admin/reivew/search',{'created_date': fill_date,'rating': this.rating,'vendor': this.vendor,'status':this.status,
                  'page': page})
                .then(function(response) 
                {
                    Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    setTimeout(function(){ 
                     }, 1000);

                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
                  
               },  
         
         
          statusChanged:function(clickedid)
              {
                var lang = $('#lang').val();
                if (document.getElementById(clickedid).checked == true) 
                {
                    if(lang == 'ar'){
                      swal({
                  title: "هل أنت واثق؟",
                  text: "سوف نشارك في حالة هذه المراجعة والتصنيف!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "لم يتم تغيير هذا التقييم ووضع التقييم :)", "error");
                  }
                  });  
                    }else{
                      swal({
                  title: "Are you sure?",
                  text: "You will Active the status of this review and rating!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                     app.confirmStatus(clickedid);
                  } else
                  {
                  document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "This review and rating status not has been changed :)", "error");
                  }
                  });  
                    }
                } else 
                {
                  if(lang == 'ar'){
                      swal({
                  title: "هل أنت واثق؟",
                  text: "ستصبح غير نشط حالة هذه المراجعة والتقييم!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                  swal("ألغيت", "لم يتم تغيير هذا التقييم ووضع التقييم :)", "error");


                  }
                  });  
                    }else{
                      swal({
                  title: "Are you sure?",
                  text: "You will Inactive the status of this review and rating!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) {
                  if (isConfirm) 
                  {
                    app.inactiveStatus(clickedid);
                  } else
                  {
                    document.getElementById(clickedid).checked = true;
                  swal("Cancelled", "This review and rating status not has been changed :)", "error");


                  }
                  });  
                  }
                }


              },
        
        
        
         confirmStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                  this.$http.post('/admin/review/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }else{
                  this.$http.post('/admin/review/changestatus',{'id':id,'status':'1'}).then(function(response){ 
                  swal("Changed!", "Your status  has been changed.", "success");
                  return false;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })  
                }
                  
              },

              inactiveStatus:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                   this.$http.post('/admin/review/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("تغير!", "لقد تم تغيير حالتك.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }else{
                   this.$http.post('/admin/review/changestatus',{'id':id,'status':'0'})
               .then(function(response)
                { 
                  swal("Changed!", "Your status  has been changed.", "success");
                  return true;
                })
                .catch(function(response) 
                {
                    Vue.set(this.$data, 'errors', response.data);
                })
                }
              },
               search:function()
              {  var fill_date = $('#from_date').val();
                 this.$http.post('/admin/reivew/search',{'created_date': fill_date,'rating': this.rating,'vendor': this.vendor,
                  'status':this.status})
                .then(function(response) {
                     Vue.set(this.$data,'users', response.data['users'].data);
                    app.makePagination(response.data['users']);
                    
                    setTimeout(function(){ 
                     }, 1000);
                    
                    
                })
                .catch(function(response) {
                    Vue.set(this.$data, 'errors', response.data);
                })
              },
              clearFilter: function()
              {
                this.vendor ='';
                this.rating ='';
                this.status ='';
                $('#from_date').val("");
                this.filter(0);
              },
               DeleteUser:function(id)
              {
                var lang = $('#lang').val();
                if(lang == 'ar'){
                    swal({
                  title: "هل أنت واثق؟",
                  text: "تريد حذف هذا التقييم والمراجعة!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "نعم فعلا",
                  cancelButtonText: "لا",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("تغير!", "تم حذف هذا التقييم والمراجعة.", "success");
                      window.location = '/admin/review/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("ألغيت", "لم يتم حذف هذا التقييم والمراجعة :)", "error");
                  }
                  });
                }else{
                  swal({
                  title: "Are you sure?",
                  text: "You want to delete this rating and review!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Yes",
                  cancelButtonText: "No",
                  closeOnConfirm: false,
                  closeOnCancel: false
                  },
                  function(isConfirm) 
                  {
                  if (isConfirm) 
                  {
                    swal("Changed!", "This Rating and Review has been deleted.", "success");
                      window.location = '/admin/review/delete/'+id;
                  } else
                  {
                  // document.getElementById(clickedid).checked = false;
                  swal("Cancelled", "This Rating and Review has not been deleted :)", "error");
                  }
                  });
                }
              },
              ViewUser:function(id)
              {
                window.location = '/admin/review/view/'+id;
              },
			   changereviewstatus:function(id,st)
              {
				   window.location = '/admin/review/change_status/'+id+'/'+st;
                //window.location = '/admin/review/view/'+id;
              },
              addUser:function(){
                 window.location = '/admin/access_level/add';
              },
			  
			  
          },
    });
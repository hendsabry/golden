$(document).ready(function () {
    $('.slide-pages').owlCarousel({
        items: 1,
        loop: true
    });

    $('.slide-cats').owlCarousel({
        loop: true,
        dots: false,
        autoplay: true,
        margin: 20,
        responsive: {
            0: {
                items: 2,
            },
            460: {
                items: 2,
            },
            767: {
                items: 2,
            },

            992: {
                items: 6,
            }

        },
        nav: true,
        navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
    });

    $('.slide-cats2').owlCarousel({
        loop: true,
        dots: false,
        autoplay: true,
        margin: 20,
        responsive: {
            0: {
                items: 2,
            },
            460: {
                items: 2,
            },
            767: {
                items: 2,
            },

            992: {
                items: 4,
            }

        },
        nav: true,
        navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
    });


    $('#Occasions').owlCarousel({
        loop: true,
        dots: false,
        autoplay: true,
        margin: 20,
        responsive: {
            0: {
                items: 2,
            },
            460: {
                items: 2,
            },
            767: {
                items: 2,
            },

            992: {
                items: 3,
            }

        },
        nav: true,
        navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],
    });

    $("#carousel").waterwheelCarousel({
        // include options like this:
        // (use quotes only for string values, and no trailing comma after last option)
        // option: value,
        // option: value
    });

    $('.carousel-owl').owlCarousel({
        loop: true,
        dots: false,
        autoplay: true,
        items: 1,
        nav: true,
        navText: ['<i class="fa fa-angle-right"></i>', '<i class="fa fa-angle-left"></i>'],

    });

    // executes when HTML-Document is loaded and DOM is ready


    // breakpoint and up  
    $(window).resize(function () {
        if ($(window).width() >= 980) {

            // when you hover a toggle show its dropdown menu
            $(".navbar .dropdown-toggle").hover(function () {
                $(this).parent().toggleClass("show");
                $(this).parent().find(".dropdown-menu").toggleClass("show");
            });

            // hide the menu when the mouse leaves the dropdown
            $(".navbar .dropdown-menu").mouseleave(function () {
                $(this).removeClass("show");
            });

            // do something here
        }
    });




});

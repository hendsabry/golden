<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\City;
use App\Categorylist;
use App\Merchant;
use App\MenuCategory;
use App\NmServicesAttribute;
use Session;
class Help extends Model
{
  public function displayCity($cid)
  {

  $citycount = City::where('ci_id',$cid)->count();
  if($citycount) { 
  $citydata = City::where('ci_id',$cid)->first(); 


  $ci_name='ci_name' ;

  if(Session::get('mer_lang_code')!='en')
    {

      $ci_name='ci_name_'.Session::get('mer_lang_code');
    }

  $cityname = $citydata->$ci_name;

   return $cityname;
                                        
   }
 else {
    return array();
 }
}
  public function displayManager($cid)
  {

    $managerdataC = Merchant::where('mer_id',$cid)->count();  
if($managerdataC>=1)
{
  $managerdata = Merchant::where('mer_id',$cid)->first();  

  $managername = $managerdata->mer_fname;
}
else
{
 $managername = 'N/A';
}
  

   return $managername;
}
                                        
   

   public function displayHotelname($cid)
  {
  $hoteldata = Categorylist::where('mc_id',$cid)->first(); 
  $mc_name='mc_name' ;

  if(Session::get('mer_lang_code')!='en')
    {

      $mc_name='mc_name_'.Session::get('mer_lang_code');
    }

  $hotelname = $hoteldata->$mc_name;

   return $hotelname;
                                        
  }

   public function dispAttributeName($cid)
  {
    $buffetmenuC = NmServicesAttribute::where('id',$cid)->count(); 
    if($buffetmenuC >=1)
    {
    $buffetmenu = NmServicesAttribute::where('id',$cid)->first(); 
     $mc_name='attribute_title' ;
  if(Session::get('mer_lang_code')!='en')
    {
      $mc_name='attribute_title_ar';
    }

  $menuName = $buffetmenu->$mc_name;
    return $menuName;
}
else
{
   return 'N/A';
}
 
                                        
  }

 //Display categoryMenu Name

   public function dispCatmenuName($cid)
   {
  $hoteldata = MenuCategory::where('id',$cid)->first(); 

  $menuname = $hoteldata->menu_name;

   return $menuname;
                                        
  }

public function productName($cid)
   {

   $hoteldata = Products::where('pro_id',$cid)->count(); 
   if($hoteldata>0)
   {
    $hoteldata = Products::where('pro_id',$cid)->first(); 
    $pro_title = $hoteldata->pro_title;
   }
   else
   {
     $pro_title = $cid;
   }
}

public function CustName($cus_id)
   {

       $hoteldata = Customer::where('cus_id',$cus_id)->count(); 
       if($hoteldata>0)
       {
        $hoteldata = Customer::where('cus_id',$cus_id)->first(); 
        $cust_name= $hoteldata->cus_name;
       }
       else
       {
         $cust_name = $cus_id;
       }  
       return $cust_name;
                                        
   }

     public function dispchileCat($id)
     {  
         $subcat = Categorylist::where('parent_id',$id)->where('mc_status',1)->get();
     	   //print_r($subcat);die;
     	   return $subcat; 
     }
    
 
}

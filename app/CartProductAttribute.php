<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartProductAttribute extends Model 
{
	
	protected $table = 'nm_cart_product_attribute';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cart_type','cart_id','product_id','attribute_title','attribute_title_ar','value','cart_product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

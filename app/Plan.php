<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model 
{
	
	protected $table = 'nm_plan_list';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['vendor_list', 'plan_name','plan_desc','manager_id', 'status', 'updated_at', 'created_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getManager()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\AdminModel','adm_id','manager_id');
    }
    public function getSalesRep()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\AdminModel','adm_id','vendor_list');
    }
	
}

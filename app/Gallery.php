<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model 
{
	
	protected $table = 'nm_product_gallery';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

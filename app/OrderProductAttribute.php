<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderProductAttribute extends Model 
{
	
	protected $table = 'nm_order_product_attribute';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','order_id','product_type','product_id','attribute_title','attribute_title_ar','value'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartServiceStaff extends Model 
{
	
	protected $table = 'nm_cart_services_staff';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','cart_id','cart_type','file_no','shop_id','service_id','staff_id','booking_place','booking_date','start_time','end_time','staff_member_name','staff_member_name_ar','image'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	  public function getHall()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }
	

	public function getStaff()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\ServicesStaff', 'staff_id','id');
    }

    public function getDoctorServiceAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ServiceAttribute','id','staff_id');
    }
}

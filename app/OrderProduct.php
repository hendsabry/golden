<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model 
{
	
	protected $table = 'nm_order';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['order_id','order_cus_id','order_pro_id','search_occasion_id','transaction_id','payer_email','payer_name','order_shipping_amt','wallet_amount','order_paytype','order_amt','order_status','hall_pending_amount','order_taxAmt','shipping_city','order_shipping_add','payer_phone','main_occasion_id','occasion_date','vat_tax'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getSearchOccasion()
    {
        /* 2nd argument is foreign key in child (this!) table */
         return $this->hasMany('App\SearchOccasion','order_id','order_id');
    }

	
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model 
{
	
	protected $table = 'nm_lead';

	/**		id												
		

	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['lead_id','lead_name', 'service_cat_sub_cat', 'sales_executive', 'sales_head', 'client_email', 'client_ph', 'address', 'date_time', 'status', 'target', 'updated_at', 'created_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	 public function get_cat()
    {

        return $this->belongsTo('App\Category', 'service_cat_sub_cat','mc_id');
    }
    public function sub()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\AdminModel', 'sales_executive','adm_id');
    }
    public function manager()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\AdminModel', 'sales_head','adm_id');
    }
	// public function get_head()
 //    {

 //        return $this->belongsTo('App\Category', 'service_cat_sub_cat','mc_id');
 //    }
}

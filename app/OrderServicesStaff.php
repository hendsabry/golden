<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderServicesStaff extends Model 
{
	
	protected $table = 'nm_order_services_staff';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','shop_id','service_id','start_time','end_time'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	
}

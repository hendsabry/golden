<?php
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class Category extends Model
{
    protected $guarded = array('id');
    protected $table = 'nm_category';
    protected $fillable = ['parent_id', 'mc_name', 'mc_name_ar','mc_discription','mc_discription_ar','mc_img_ar', 'mc_img','mc_status'];


 public function vendor()
    {
        /* 2nd argument is foreign key in child (this!) table */        
        return $this->hasMany('App\Merchant','mer_id','vendor_id');
    }

    //Make relation for get city detail
    public function getCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\City', 'city_id','ci_id');
    }

    //Make relation for get Category Gallery
    public function CategoryGallery()
    {
        /* 2nd argument is foreign key in child (this!) table */        
        return $this->hasMany('App\CategoryGallery','category_id','mc_id');
    }

    //get client comments for particular shop
    public static function get_category_review($id)
    {
        return DB::table('nm_review')
        ->orderby('comment_id','desc')
        ->Leftjoin('nm_category','nm_review.shop_id','=','nm_category.mc_id')
        ->Leftjoin('nm_customer','nm_review.customer_id','=','nm_customer.cus_id')
        ->where('nm_review.shop_id','=',$id)
        ->where('nm_review.review_type','=','shop')
         ->where('nm_review.status', 1)
        ->select('nm_category.mc_id','nm_category.mc_name','nm_review.comment_id','nm_review.title','nm_review.comments','nm_review.ratings','nm_customer.cus_id','nm_customer.cus_name')->paginate(2);
    }
 
    //get client comments for particular product
    public static function get_product_review($id)
    {
        return DB::table('nm_review')
        ->orderby('comment_id','desc')
        ->Leftjoin('nm_category','nm_review.shop_id','=','nm_category.mc_id')
        ->Leftjoin('nm_customer','nm_review.customer_id','=','nm_customer.cus_id')
        ->where('nm_review.shop_id','=',$id)
        ->where('nm_review.review_type','=','product')
        ->select('nm_category.mc_id','nm_category.mc_name','nm_review.comment_id','nm_review.title','nm_review.comments','nm_review.ratings','nm_customer.cus_id','nm_customer.cus_name')->get();
    }

    //Himanshu Client review

    public static function get_product_carrental_review($id)
    {
       
       $subcategory =  DB::table('nm_product')->select('pro_id')->where('pro_mc_id',$id)->get();

        $proid = array();  
        foreach($subcategory as $val) {
        
          $proid[] = $val->pro_id;

         }
      $shopType = array('shop','product');
        return DB::table('nm_review')
        ->orderby('comment_id','desc')
        ->Leftjoin('nm_category','nm_review.shop_id','=','nm_category.mc_id')
        ->Leftjoin('nm_customer','nm_review.customer_id','=','nm_customer.cus_id')
        ->where('nm_review.status',1)
        ->whereIn('nm_review.product_id',$proid)
        ->whereIn('nm_review.review_type', $shopType)
        ->select('nm_category.mc_id','nm_category.mc_name','nm_review.comment_id','nm_review.title','nm_review.comments','nm_review.ratings','nm_customer.cus_id','nm_customer.cus_name')->take(10)->get();
    }


    //get client comments for Singer's and Band.
    public static function get_singer_band_review($id)
    {
        return DB::table('nm_review')
        ->orderby('comment_id','desc')
        ->Leftjoin('nm_category','nm_review.shop_id','=','nm_category.mc_id')
        ->Leftjoin('nm_customer','nm_review.customer_id','=','nm_customer.cus_id')
        ->where('nm_review.shop_id','=',$id)
        ->where('nm_review.review_type','=','music')
        ->select('nm_category.mc_id','nm_category.mc_name','nm_review.comment_id','nm_review.title','nm_review.comments','nm_review.ratings','nm_customer.cus_id','nm_customer.cus_name')->get();
    }
    
    public static function save_category($sizes)
    {
        return DB::table('nm_category')->insert($sizes);
    }
    
    public static function maincatg_list()
    {	
        return DB::table('nm_category')->get();
    }

    public static function selectedcatg_list($id)
    {
        return DB::table('nm_category')->where('mc_id', '=', $id)->get();
    }

    public static function update_category_detail($entry, $id)
    {
        return DB::table('nm_category')->where('mc_id', '=', $id)->update($entry);
    }

    public static function status_category_submit($id, $status)
    {
        return DB::table('nm_category')->where('mc_id', '=', $id)->update(array('mc_status' => $status));
    }

    // public static function delete_category($id)
    // {
    //     return DB::table('nm_category')->where('mc_id', '=', $id)->delete();
    // }
    public static function delete_category($id)
    {
        
        // To start Image delete from folder 09/11/ 
        $filename = DB::table('nm_category')->where('mc_id', '=', $id)->first();
        $getimagename= $filename->mc_img;
        $getextension=explode("/**/",$getimagename);
        foreach($getextension as $imgremove)
        {
            File::delete(base_path('assets/categoryimage/').$imgremove);
        } 
        // To End  
        return DB::table('nm_category')->where('mc_id', '=', $id)->delete();
        
    }
	
    public static function save_main_category($sizes)
    {
        return DB::table('nm_secmaincategory')->insert($sizes);
    }
    
    public static function maincatg_sub_list($catg_list)
    {
        foreach ($catg_list as $catg) {
		
		
            $catg_result          = DB::table('nm_secmaincategory')->where('smc_mc_id', '=', $catg->mc_id)->get();
            $result[$catg->mc_id] = count($catg_result);
        }

        if (isset($result)) {
            return $result;
        }
    }

    public static function sub_maincatg_list($catg_id)
    {
        return DB::table('nm_secmaincategory')->where('smc_mc_id', '=', $catg_id)->LeftJoin('nm_category', 'nm_category.mc_id', '=', 'nm_secmaincategory.smc_mc_id')->get();
    }
    
    public static function add_sub_catg_details($catg_id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $catg_id)->get();
    }

    public static function save_sub_category($sizes)
    {
        return DB::table('nm_subcategory')->insert($sizes);
    }

    public static function subcatg_count_list($catg_list)
    {
	
        foreach ($catg_list as $catg1) {
		$catg_result = DB::table('nm_subcategory')->where('sb_smc_id', '=', $catg1->smc_id)->where('mc_id', '=', $catg1->smc_mc_id)->get();		
		   if ($catg_result) {
                $result[$catg1->smc_id] = count($catg_result);
            } 
			else {
                $result[$catg1->smc_id] = 0;
            }
        }
        return $result;
    }

    public static function sub_catg_list($catg_id)
    {
        return DB::table('nm_subcategory')->where('sb_smc_id', '=', $catg_id)->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_subcategory.sb_smc_id')->get();
    }
    
    public static function secsubcatg_count_list($catg_list)
    {
       
        foreach ($catg_list as $catg) {
	
	
            $catg_result = DB::table('nm_secsubcategory')->where('ssb_sb_id', '=', $catg->sb_id)->where('ssb_smc_id', '=', $catg->mc_id)->get();
			
            if ($catg_result) {
			
                $result[$catg->sb_id] = count($catg_result);
            } else {
                $result[$catg->sb_id] = 0;
            }
            
        }
       
        return $result;
        
    }

    public static function edit_main_catg_details($catg_id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $catg_id)->leftJoin('nm_category', 'nm_category.mc_id', '=', 'nm_secmaincategory.smc_mc_id')->get();
    }

    public static function save_edit_main_category($sizes, $id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $id)->update($sizes);
    }

    public static function status_main_category_submit($id, $status)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $id)->update(array('smc_status' => $status));
    }

    public static function delete_main_category($id)
    {
        return DB::table('nm_secmaincategory')->where('smc_id', '=', $id)->delete();
    }

    public static function add_secsub_main_category($catg_id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $catg_id)->get();
    }

    public static function save_secsub_category($sizes)
    {
        return DB::table('nm_secsubcategory')->insert($sizes);
    }

    public static function edit_secsub_catg_details($catg_id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $catg_id)->LeftJoin('nm_secmaincategory', 'nm_secmaincategory.smc_id', '=', 'nm_subcategory.sb_smc_id')->get();
    }

    public static function save_editsecsub_main_category($sizes, $id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $id)->update($sizes);
    }

    public static function status_subsec_category_submit($id, $status)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $id)->update(array('sb_status' => $status));
    }

    public static function delete_subsec_category($id)
    {
        return DB::table('nm_subcategory')->where('sb_id', '=', $id)->delete();
    }

    public static function secsub_catg_list($catg_id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_sb_id', '=', $catg_id)->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_secsubcategory.ssb_sb_id')->get();
    }

    public static function status_secsub_category_submit($id, $status)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $id)->update(array('ssb_status' => $status));
    }

    public static function delete_secsub_category($id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $id)->delete();
    }

    public static function edit_sec1sub_catg_details($catg_id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $catg_id)->LeftJoin('nm_subcategory', 'nm_subcategory.sb_id', '=', 'nm_secsubcategory.ssb_sb_id')->get();
    }

    public static function save_editsec1sub_main_category($sizes, $id)
    {
        return DB::table('nm_secsubcategory')->where('ssb_id', '=', $id)->update($sizes);
    }

    public static function check_avail_cat_name($cat_name){
        return DB::table('nm_category')->where('mc_name','=',$cat_name)->count();
    }
}

?>
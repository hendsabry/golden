<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ServiceAttributes extends Model 
{
	
	protected $table = 'nm_services_product_attribute';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['attribute_title','services_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getProducts()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','services_id','pro_mc_id');
    }
	
}

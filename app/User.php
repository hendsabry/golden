<?php 
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
	
	protected $primaryKey = 'cus_id';
	protected $table = 'nm_customer';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_name','email','cus_phone','gender','dob','cus_address1','cus_address2','cus_country','cus_city','cus_zipcode','cus_status','cus_pic','password','remember_token','cus_logintype'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	

	public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

      public function getCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\City','ci_id','cus_city');
    }


     public function getCountry()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Country','co_id','cus_country');
    }

}

<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Models;
use App\Category;
use App\Products;
use App\ServiceAttribute;
use App\ProductOption;
use App\ProductOptionValue;
use App\ProductAttribute;
use App\City;
use App\CategoryGallery;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartBodyMeasurement;
use App\CartServiceAttribute;
use App\CartProductRent;
use App\CartProductAttribute;
use Auth;
use App\User;
use App\OrderProductShipping;
use Response;
use DB;


class ShoppingController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |   Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function __construct() {}

    /**Get Tailors info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Tailors info.
    */
    public function getTailorsInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();


        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['category_id'])->where('mc_status', '=', '1')->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $input['subcategory_id'])->where('mc_status', '=', '1')->first();

                

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {
                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                        //Vendor detail
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude','mc_video_url')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();

                       
                        //GET VENDOR DETAIL
                        $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price','pro_desc_ar as pro_desc','pro_disprice','deliver_day')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);

                          foreach($product as $key=>$val){
                          $optionvaluefabric = ProductOptionValue::select('option_title_ar as option_title', 'price','image')->where('product_option_id','37')->where('product_id',$val->pro_id)->orderBy('option_title_ar', 'ASC')->paginate(1000);
                           $val->febric = $optionvaluefabric;
                           if($val->deliver_day !=''){
                                $val->deliver_day = $val->deliver_day;
                                }
                                else
                                {
                                $val->deliver_day =0;      
                                }
                          }

                        if (!$product->isEmpty()) {
                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name_ar, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'product' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude','mc_video_url')->where('mc_status', '=', '1')->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL

                        $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_desc','pro_disprice','deliver_day')->where('pro_mc_id', '=', $input['vendor_id'])->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(100);

                         foreach($product as $key=>$val){
                          $optionvaluefabric = ProductOptionValue::select('option_title', 'price','image')->where('product_option_id','37')->where('product_id',$val->pro_id)->orderBy('option_title', 'ASC')->paginate(1000);
                           $val->febric = $optionvaluefabric;
                                if($val->deliver_day !=''){
                                $val->deliver_day = $val->deliver_day;
                                }
                                else
                                {
                                $val->deliver_day =0;      
                                }
                           
                          }


                        /*$optionvaluefabric = ProductOptionValue::select('option_title', 'price')->where('vandor_id', '=', $input['vendor_id'])->where('product_option_id','37')->orderBy('option_title', 'ASC')->paginate(100);
*/



                        if (!$product->isEmpty()) {
                            

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'sub_category_id' => $sub_catg->mc_id, 'sub_category_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'product' => $product);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Tailors data in add to cart
    public function setTailorsAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product Category
            'total_price' => 'required',
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();

 
                    if (!empty($cart)) { 
                        // $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        // $cart_body_measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['fabric_name'] = $input['fabric_name'];



                 //Check Duplicate record then bypass it - start
                    $checkinproductsection =  CartProduct::where('merchant_id',$category->vendor_id)->where('shop_id',$category->mc_id)->where('shop_vendor_id',$category->parent_id)->where('cart_type',$input['cart_type'])->where('cart_sub_type',$input['cart_sub_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('fabric_name',$input['fabric_name'])->count();
                    $checkinproductc = CartBodyMeasurement::where('cart_id',$cart->id)->where('customer_id',$User->cus_id)->where('product_id',$input['product_id'])->where('length',$input['length'])->where('chest_size',$input['chest_size'])->where('waistsize',$input['waistsize'])->where('soulders',$input['soulders'])->where('neck',$input['neck'])->where('arm_length',$input['arm_length'])->where('wrist_diameter',$input['wrist_diameter'])->where('cutting',$input['cutting'])->count();
                        if($checkinproductsection >=1 && $checkinproductc>=1)
                        {
                            $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                            $response = Response::json($response_array, 200);   
                            return $response;
                        }
                    //Check Duplicate record then bypass it - end




                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $getCartProID = $cart_product->id;
                    $cart_body_measurement = array();
                    if(!empty($input['length'])){

                        $cart_body_measurement['cart_id'] = $cart->id;
                        $cart_body_measurement['customer_id'] = $User->cus_id;
                        $cart_body_measurement['product_id'] = $input['product_id'];
                        $cart_body_measurement['length'] = $input['length'];                    
                        $cart_body_measurement['chest_size'] = $input['chest_size'];                  
                        
                        $cart_body_measurement['waistsize'] = $input['waistsize'];                  
                        $cart_body_measurement['soulders'] = $input['soulders'];
                        $cart_body_measurement['neck'] = $input['neck'];
                        $cart_body_measurement['arm_length'] = $input['arm_length'];                    
                        $cart_body_measurement['wrist_diameter'] = $input['wrist_diameter'];    
                        $cart_body_measurement['cutting'] = $input['cutting']; 
                        $cart_body_measurement['status'] = 1;  
                        $cart_body_measurement['cart_product_id'] = $getCartProID; 


                        CartBodyMeasurement::create($cart_body_measurement); //Product rental date and time entry 
                    }
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Dresses info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Dress info.
    */
   public function getDressesInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //Get Dress Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('status', '=', '1')->where('services_id', '=', $input['vendor_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'Insuranceamount','pro_disprice')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                                foreach($dress_style as $val) {
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title_ar as option_title,type')->get();
                                    //Get service attribute option value with container list
                                     $val->style_option = $style_Option;
                                     foreach($style_Option as $pro_opt) {
                                        $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price','discount_price','experience', 'status','value')->where('product_id', '=', $val->pro_id)->where('product_option_id', '=', $pro_opt->id)->get();
                                           $pro_opt->style_option_value = $style_Option_value;
                                     }                                    

                                   
                                    

                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'dress_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET VENDOR DETAIL
                       $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //Get Dress Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('status', '=', '1')->where('services_id', '=', $input['vendor_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'Insuranceamount','pro_disprice')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                                foreach($dress_style as $val) {
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    //Get service attribute option value with container list
                                    $val->style_option = $style_Option;
                                    foreach($style_Option as $pro_opt) {
                                        $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'experience', 'status','discount_price','value')->where('product_id', '=', $val->pro_id)->where('product_option_id', '=', $pro_opt->id)->get();
                                    $pro_opt->style_option_value = $style_Option_value;
                                     } 

                                    
                                    

                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'dress_menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Dresses data in add to cart
    public function setDressAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Product ID
            'size' => 'required', //Get Product Size
            'product_qty' => 'required', //Get Product Quantity
            'token' => 'required',
            'total_price' => 'required'
            

        ]);



        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();
                $productquantity = DB::table('nm_product_option_value')->where('option_title',$input['size'])->where('product_id', '=', $input['product_id'])->first();

               
                  //echo $input['product_qty'];die;
               // if ($product->pro_qty >= $input['product_qty']) {
               if($productquantity->value >= $input['product_qty']) {

                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                       // $cart_pro = CartProduct::where('cart_type', '=','shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                       // $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                       // $cart_pro_rent = CartProductRent::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                       // $cart_pro_opt = CartOption::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                       // $cart_pro_val = CartOptionValue::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                 

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];

                    /*if (!empty($product_options_value)) {
                   
                    $date1=date_create($input['rental_time']);
                    $date2=date_create($input['return_time']);
                    $diff=date_diff($date1,$date2);
                    $diff->format("%H:%I"); 
                    $cart_product_data['total_price'] = (($product->Insuranceamount + $product_options_value->price)*$diff->format("%H")) * $input['product_qty'];

                    } else {
                       $cart_product_data['total_price'] = $product->pro_price * $input['product_qty'];
                    } */ 
                    $cart_product_data['total_price'] = $input['total_price']; 
                    $cart_product_data['quantity'] = $input['product_qty'];
                    $cart_product_data['product_size'] = $input['size'];
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['insurance_amount'] = $product->Insuranceamount;
                    $cart_product_data['status'] = 1;

$product_options_value = [];

                    //Check Duplicate record then bypass it - start
                    $chekCartPro1 =  CartProduct::where('merchant_id',$category->vendor_id)->where('shop_id',$category->mc_id)->where('shop_vendor_id',$category->parent_id)->where('cart_type',$input['cart_type'])->where('cart_sub_type',$input['cart_sub_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('product_size',$input['size'])->count();
                    $chkserviceAttr2 =  CartServiceAttribute::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('category_id',$product->pro_mc_id)->where('product_id',$input['product_id'])->where('attribute_id',$input['attribute_id'])->count();
                    
                    if(!empty($input['rental_date'])){
                    $rd = date("Y-m-d", strtotime($input['rental_date']));     
                    $rda = date("Y-m-d", strtotime($input['return_date']));
                    $chkserviceAttr3 =  CartProductRent::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('service_id',$product->pro_mc_id)->where('product_id',$input['product_id'])->where('rental_date',$rd)->where('return_date',$rda)->count();
                    }

                    $checkinproductc4 = CartOption::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->count();

 

  $product_options_value = ProductOptionValue::select('id','product_id','price','product_option_id','option_title','option_title_ar')->where('id', '=', $input['product_option_value'])->with('getProductOption')->first();




                    $checkinproductc5 = CartOptionValue::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('product_option_id',$product_options_value->product_option_id)->where('value',$product_options_value->price)->where('option_title',$product_options_value->getProductOption->option_title)->count();


                    if(!empty($input['rental_date'])){
                    if($chekCartPro1 >=1 && $chkserviceAttr3 >=1 && $chkserviceAttr2 >=1 && $checkinproductc4 >=1 && $checkinproductc5 >=1)
                    {
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);   
                    return $response;
                    }
                    }
                    else
                    {
                    if($chekCartPro1 >=1 &&   $chkserviceAttr2 >=1 && $checkinproductc4 >=1 && $checkinproductc5 >=1)
                    {
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);   
                    return $response;
                    }

                    }
                    //Check Duplicate record then bypass it - end

 

                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                     

                    $getCartProID = $cart_product->id;
                   /* $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); */

                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first(); 

                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_attribute_data['cart_product_id'] = $getCartProID;


 

                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
                    if(!empty($input['rental_date'])){
                        $cart_product_rent = array();
                        $cart_product_rent['cart_id'] = $cart->id;
                        $cart_product_rent['cart_type'] = $input['cart_type'];
                        $cart_product_rent['service_id'] = $product->pro_mc_id;
                        $cart_product_rent['product_id'] = $input['product_id'];
                        $cart_product_rent['rental_date'] = date("Y-m-d", strtotime($input['rental_date']));          
                        $cart_product_rent['return_date'] = date("Y-m-d", strtotime($input['return_date']));
                        $cart_product_rent['rental_time'] = $input['rental_time'];
                        $cart_product_rent['return_time'] = $input['return_time'];
                        $cart_product_rent['quantity'] = $input['product_qty'];
                        $cart_product_rent['insurance_amount'] = $product->Insuranceamount;
                        $cart_product_rent['cart_product_id'] = $getCartProID;

 
                        CartProductRent::create($cart_product_rent); //Product rental date and time entry  
                    }
                    if (!empty($product_options_value)) {

                        $cart_option_data = array();
                        $cart_option_data['cart_type'] = $input['cart_type'];
                        $cart_option_data['product_option_id'] = $product_options_value->product_option_id;
                        $cart_option_data['cart_id'] = $cart->id;
                        $cart_option_data['product_id'] = $input['product_id'];
                        $cart_option_data['cart_product_id'] = $getCartProID;
 

                        CartOption::create($cart_option_data); //cart option entry

                        $cart_option_value = array();
                        $cart_option_value['cart_type'] = $input['cart_type'];
                        $cart_option_value['cart_id'] = $cart->id;
                        $cart_option_value['product_id'] = $input['product_id'];
                        $cart_option_value['product_option_id'] = $product_options_value->product_option_id;
                        $cart_option_value['product_option_value_id'] = $product_options_value->id;
                        $cart_option_value['quantity'] = $input['product_qty'];
                        $cart_option_value['value'] = $product_options_value->price;
                        $cart_option_value['status'] = 1;

                       $cart_option_value['option_title'] = $product_options_value->getProductOption->option_title;
                       $cart_option_value['option_title_ar'] = $product_options_value->getProductOption->option_title_ar;
                        $cart_option_value['option_value_title'] = $product_options_value->option_title;
                        $cart_option_value['option_value_title_ar'] = $product_options_value->option_title_ar;
                        $cart_option_value['price'] = $product_options_value->price;
                        $cart_option_value['cart_product_id'] = $getCartProID;






                       $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                    }                   
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);

                 } else if ($product->pro_qty <= $input['product_qty'] && $product->pro_qty > 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$product->pro_qty, 'code' => 200]);
                    $response = Response::json($response_array, 200);

                } else if ($product->pro_qty == 0) {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.PRODUCT_ERROR'), 'code' => 200]);
                    $response = Response::json($response_array, 200);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Abaya info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Abaya info.
    */
   public function getAbayaInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','attribute_title as title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();

                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','deliver_day','pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                $value->dress_style = $dress_style;
                                foreach($dress_style as $val) {
                                    
                                    $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    //Get service attribute option value with container list
                                    $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'experience', 'status','value')->where('product_id', '=', $val->pro_id)->get();
                                if($value->title != 'Tailor'){
                                    $val->style_option = $style_Option;
                                    $val->style_option_value = $style_Option_value;
                                }
                                   
                                    if($val->deliver_day !=''){
                                    $val->deliver_day = $val->deliver_day;
                                    }
                                    else
                                    {
                                    $val->deliver_day =0;      
                                    }
                                
                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET VENDOR DETAIL
                       $vendor = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['subcategory_id'])->get();

                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','deliver_day','pro_qty')->where('pro_mc_id', '=', $input['vendor_id'])->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                $value->dress_style = $dress_style;

                                foreach($dress_style as $val) {
                                    
                                      $name = explode(',', $val->option_id);
                                    //get service attribute option detail
                                    $style_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'service_hour')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                    //Get service attribute option value with container list
                                    $style_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'experience', 'status','value')->where('product_id', '=', $val->pro_id)->get();
                                    if($value->attribute_title != 'Tailor'){
                                    $val->style_option = $style_Option;
                                    $val->style_option_value = $style_Option_value;  
                                    }
                                    if($val->deliver_day !=''){
                                    $val->deliver_day = $val->deliver_day;
                                    }
                                    else
                                    {
                                    $val->deliver_day =0;      
                                    }

                                }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_detail' => $vendor, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Abaya data in add to cart
    public function setAbayaAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Attribute ID
            'total_price' => 'required',
        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        // $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        // $cart_body_measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        // $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    if($input['size']){
                       $cart_product_data['product_size'] = $input['size']; 
                   }
                    $cart_product_data['status'] = 1;

                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    if($input['product_quantity']!='') {
                    $cart_product_data['quantity'] = $input['product_quantity'];
                    } else {
                    $cart_product_data['quantity'] = '1';
                    }
 

                    //Check Duplicate record then bypass it - start
                    $chekCartPro =  CartProduct::where('merchant_id',$category->vendor_id)->where('shop_id',$category->mc_id)->where('shop_vendor_id',$category->parent_id)->where('cart_type',$input['cart_type'])->where('cart_sub_type',$input['cart_sub_type'])->where('cart_id',$cart->id)->where('product_id',$input['product_id'])->where('product_size',$input['size'])->count();
                    $chkserviceAttr =  CartServiceAttribute::where('cart_type',$input['cart_type'])->where('cart_id',$cart->id)->where('category_id',$product->pro_mc_id)->where('product_id',$input['product_id'])->where('attribute_id',$input['attribute_id'])->count();
                    $checkinproductc = CartBodyMeasurement::where('cart_id',$cart->id)->where('customer_id',$User->cus_id)->where('product_id',$input['product_id'])->where('length',$input['length'])->where('chest_size',$input['chest_size'])->where('waistsize',$input['waistsize'])->where('soulders',$input['soulders'])->where('neck',$input['neck'])->where('arm_length',$input['arm_length'])->where('wrist_diameter',$input['wrist_diameter'])->where('cutting',$input['cutting'])->count();
                    if($chekCartPro >=1 && $chkserviceAttr >=1 && $checkinproductc >=1)
                    {
                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);   
                    return $response;
                    }
                    //Check Duplicate record then bypass it - end

 

                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $getCartProID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;

 



                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry

                    $cart_body_measurement = array();
                    if(!empty($input['length'])){

                        $cart_body_measurement['cart_id'] = $cart->id;
                        $cart_body_measurement['customer_id'] = $User->cus_id;
                        $cart_body_measurement['product_id'] = $input['product_id'];
                        $cart_body_measurement['length'] = $input['length'];                    
                        $cart_body_measurement['chest_size'] = $input['chest_size'];                  
                        
                        $cart_body_measurement['waistsize'] = $input['waistsize'];                  
                        $cart_body_measurement['soulders'] = $input['soulders'];
                        $cart_body_measurement['neck'] = $input['neck'];
                        $cart_body_measurement['arm_length'] = $input['arm_length'];                    
                        $cart_body_measurement['wrist_diameter'] = $input['wrist_diameter'];    
                        $cart_body_measurement['cutting'] = $input['cutting']; 
                        $cart_body_measurement['status'] = 1;  
                        $cart_body_measurement['cart_product_id'] = $getCartProID;                  
                        CartBodyMeasurement::create($cart_body_measurement); //Product rental date and time entry 
                    }
                     


                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Oud & Perfumes info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Oud & Perfumes info.
    */
   public function getOudAndPerfumesInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();
                $vendor = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->first();

 

                //GET CLIENT REVIEW
               

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();

                        $mc_ids = $branch->mc_id; 
                        $client_review = Category::get_category_review($mc_ids);
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status')->where('services_id', '=', $input['branch_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                               

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                       
                        $mc_ids = $branch->mc_id; 
                        $client_review = Category::get_category_review($mc_ids);
                        //Get Oup & Perfume Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status')->where('services_id', '=', $input['branch_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->dress_style = $dress_style;
                               

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

    //Set Oud And Perfumes data in add to cart
    public function setOudAndPerfumesAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Product ID
            'total_price' => 'required',
            'product_qty' => 'required',

        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();
                 $proqntity =  $product->pro_qty;

                 if($proqntity<$input['product_qty'] && $proqntity > 0) {
                     
                      $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.QUANTITY_ERROR').$proqntity, 'code' => 200]);
                    $response = Response::json($response_array, 200);
                    return $response;
                 }

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                       //  $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                       // $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                     $cart_product_data['quantity'] = $input['product_qty'];
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $getCartProID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_attribute_data['cart_product_id'] = $getCartProID; 

                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry
                    
                    
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Gold & Jewelry info.**/

    /*
       vendor(3th lavel category in category table) become pro_mc_id in product table to get full Gold & Jewelry info.
    */
   public function getGoldAndJewelryInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'category_id' => 'required', //Main Category ID
            'subcategory_id' => 'required', //Sub Category ID
            'vendor_id' => 'required', //Vendor ID
            'branch_id' => 'required', //Branch ID
            'language_type' => 'required|max:255',
            'token' => 'required'
        ]);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'code' => 204, 'error' => 'Invalid Input']);
            $response = Response::json($response_array);
        } else {

            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['category_id'])->first();
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['subcategory_id'])->first();
                $vendor = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_status', '=', 1)->where('mc_id', '=', $input['vendor_id'])->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($input['vendor_id']);

                if (!empty($category)) {

                    if ($input['language_type'] == 'ar') {

                        /* FOR ARABIC LANGUAGE */
                       
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //Get Saloon Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'image', 'status','write_on_jewellery')->where('services_id', '=', $input['branch_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                               $value->gold_style = $dress_style;
                                foreach($dress_style as $value) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title_ar as attribute_title','value')->where('product_id', '=', $value->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $value->product_attribute =  $product_attr;

                            }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'م العثور على البيانات'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name_ar, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name_ar, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET BRANCH DETAIL
                       $branch = Category::select('mc_id', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name','longitude','latitude')->where('mc_status', '=', 1)->where('mc_id', '=', $input['branch_id'])->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //Get Oup & Perfume Menu List
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'image', 'status','write_on_jewellery')->where('services_id', '=', $input['branch_id'])->get();
                        if (isset($menu)) {

                            foreach($menu as $key => $value) {
                                //get style detail according menu
                                $dress_style = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour','pro_disprice','pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->gold_style = $dress_style;
                                foreach($dress_style as $value) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title','value')->where('product_id', '=', $value->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $value->product_attribute =  $product_attr;

                            }

                            }

                            $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'Data found'], 'category_id' => $category->mc_id, 'category_name' => $category->mc_name, 'subcategory_id' => $sub_catg->mc_id, 'subcategory_name' => $sub_catg->mc_name, 'vendor_id' => $vendor->mc_id, 'vendor_name' => $vendor->mc_name, 'branch_detail' => $branch, 'client_comments' => $client_review, 'menu' => $menu);
                        } else {
                            $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => 'server not responding']);
                            $response = Response::json($response_array);
                        }
                    }

                    $response = Response::json($response_array);

                } else {

                    $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => $category->mc_status, 'error' => 'server not responding']);
                    $response = Response::json($response_array);

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => 'User not found']);
                $response = Response::json($response_array);
            }
        }

        return $response;

    }

     //Set Gold And Jewelry data in add to cart
    public function setGoldAndJewelryAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'product_id' => 'required', //Get Product ID
            'cart_type' => 'required', // Get Cart Type
            'cart_sub_type' => 'required', //Get Product ID
            'attribute_id' => 'required', //Get Product ID
            'total_price' => 'required',

        ]);

        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {

                $User = Auth::user();
                $product = Products::where('pro_id', '=', $input['product_id'])->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                    if (!empty($cart)) {
                        // $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();

                        $cart_pro = CartProduct::where('cart_type', '=',$input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->first();
                         if($input['product_quantity']){
                        $cart_pro->quantity = $input['product_quantity'];
                        $cart_pro->save();


                         }




                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', $input['cart_type'])->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $User->cus_id;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $input['cart_type'];
                    $cart_product_data['cart_sub_type'] = $input['cart_sub_type'];
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $input['product_id'];
                    $cart_product_data['total_price'] = $input['total_price'];
                    if($input['write_on_ring']){
                      $cart_product_data['product_size'] = $input['write_on_ring'];  
                    }
                    if($input['product_quantity']){
                      $cart_product_data['quantity'] = $input['product_quantity'];  
                    }                     
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                    $getCartProID = $cart_product->id;
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $input['attribute_id'])->first();
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $input['cart_type'];
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $input['product_id'];
                    $cart_attribute_data['attribute_id'] = $input['attribute_id'];
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_attribute_data['cart_product_id'] = $getCartProID; 

                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry

                    $product_attr = ProductAttribute::select('id', 'product_id', 'attribute_title_ar','attribute_title', 'value')->where('product_id', '=', $input['product_id'])->get();
                    if(!empty($product_attr)){
                        foreach ($product_attr as $key => $value) {
                    
                    $cart_product_attr = array();
                    $cart_product_attr['cart_id'] = $cart->id;
                    $cart_product_attr['cart_type'] = $input['cart_type'];
                    $cart_product_attr['product_id'] = $input['product_id'];
                    $cart_product_attr['attribute_title'] = $value->attribute_title;
                    $cart_product_attr['attribute_title_ar'] = $value->attribute_title_ar;
                    $cart_product_attr['value'] = $value->value;
                    $cart_product_attr['cart_product_id'] = $getCartProID; 
                    CartProductAttribute::create($cart_product_attr); //Product Attribute entry
                        }
                    }
                    
                    
                    

                    $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.CART_SUCCESS'), 'code' => 200], 'cart_id' => $cart->id, 'cart_type' => $input['cart_type']);
                    $response = Response::json($response_array, 200);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => trans($locale.'_mob_lang.CART_ERROR'), 'code' => 500]);
                    $response = Response::json($response_array, 500);
                }

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;

    }

    /**Get Shopping data from add to cart**/
    public function getShoppingAddToCartData(Request $request) {  
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'product_id' => 'required',
            'language_type' => 'required|max:255',
        ]);
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();


                $shopping_pro = array(); 
                $cart_pro = CartProduct::where('cart_type', '=', 'shopping')->where('id', '=', $input['id'])->where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_Img,attribute_id,pro_status,pro_mc_id,pro_disprice')->first();


                $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status','longitude','latitude')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->first();

                //Product Attributes
                 $quantityattr = '';

                if (!empty($cart) && !empty($cart_pro)) {
                    //get  product 
                   $size =  $cart_pro->product_size;
                 $quantityattr = ProductOptionValue::select('value')->where('product_id','=',$input['product_id'])->where('option_title',$size)->first();

                            $attribute = CartServiceAttribute::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('cart_product_id', '=',$input['id'])->where('product_id', '=',$cart_pro->product_id)->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();

                            $shopping_pro['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                            if ($input['language_type'] == 'en') {
                                if(!empty($category)){
                                   $shopping_pro['category_name'] = $category->mc_name;
                                   $shopping_pro['category_img'] = $category->mc_img;
                                   $shopping_pro['address'] = $category->address; 
                                   $shopping_pro['longitude'] = $category->longitude;
                                   $shopping_pro['latitude'] = $category->latitude;
                                   $shopping_pro['write_on_rings'] = $cart_pro->product_size;
                                   $shopping_pro['pro_qty'] = $cart_pro->quantity;
                                   if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $shopping_pro['quantity'] = $quantityattr->value;
                                    } else {

                                   $shopping_pro['quantity'] = $cart_pro->getProduct[0]->pro_qty;
                                    }
                                   
                                    
                                }
                                if(!empty($attribute)){
                                  $shopping_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;  
                                }                                
                                $shopping_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                                $shopping_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                                $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title', 'value')->where('cart_product_id', '=',$input['id'])->where('product_id', '=', $input['product_id'])->orderBy('attribute_title', 'ASC')->get();
                                   if(!empty($product_attr)){
                                    $shopping_pro['product_attr'] = $product_attr; 
                                   }
                            } else {
                                if(!empty($category)){
                                   $shopping_pro['category_name'] = $category->mc_name_ar;
                                   $shopping_pro['category_img'] =  $category->mc_img;
                                   $shopping_pro['address'] =       $category->address_ar;
                                   $shopping_pro['longitude'] =     $category->longitude;
                                   $shopping_pro['latitude'] =      $category->latitude; 
                                   $shopping_pro['write_on_rings'] = $cart_pro->product_size; 
                                   $shopping_pro['pro_qty'] =        $cart_pro->quantity;
                                  if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $shopping_pro['quantity'] = $quantityattr->value;
                                    }else {
                                   $shopping_pro['quantity'] = $cart_pro->getProduct[0]->pro_qty;
                                   
                                    } 
                                }
                                if(!empty($attribute)){
                                  $shopping_pro['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;  
                                }
                                
                                $shopping_pro['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                                $shopping_pro['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                               $product_attr = CartProductAttribute::select('id', 'product_id', 'attribute_title_ar', 'value')->where('cart_product_id', '=',$input['id'])->where('product_id', '=', $input['product_id'])->orderBy('attribute_title', 'ASC')->get();
                                if(!empty($product_attr)){
                                    $shopping_pro['product_attr'] = $product_attr; 
                                   }
                            }
                            $shopping_pro['size'] = $cart_pro->product_size;
                            //$shopping_pro['pro_qty'] = $cart_pro->quantity;

                            if($cart_pro->getProduct[0]->pro_disprice=='' || $cart_pro->getProduct[0]->pro_disprice==0) {
                                 $shopping_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                               } else {

                                  $shopping_pro['pro_price'] = $cart_pro->getProduct[0]->pro_disprice;
                               }

                           // $shopping_pro['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                            $shopping_pro['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                            $shopping_pro['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                            $shopping_pro['total_price'] = $cart_pro->total_price;
                            $rental_pro = CartProductRent::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('cart_product_id', '=',$input['id'])->where('product_id', '=',$cart_pro->product_id)->first(); //get rental data
                          if (!empty($rental_pro)) {

                            $shopping_pro['rental_date'] = date("d-m-Y", strtotime($rental_pro->rental_date));
                            $shopping_pro['return_date'] = date("d-m-Y", strtotime($rental_pro->return_date));
                            $shopping_pro['insurance_amount'] = $rental_pro->insurance_amount;

                          }

                          


                          $measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->where('product_id', '=',$cart_pro->product_id)->where('cart_product_id', '=',$input['id'])->where('cart_product_id', '=',$cart_pro->id)->first(); // get measurement data
                          if (!empty($measurement)) {
                            $shopping_pro['length']     = $measurement->length;
                            $shopping_pro['chest_size'] = $measurement->chest_size;
                            $shopping_pro['waistsize']  = $measurement->waistsize;
                            $shopping_pro['soulders']   = $measurement->soulders;
                            $shopping_pro['neck']       = $measurement->neck;
                            $shopping_pro['arm_length'] = $measurement->arm_length;
                            $shopping_pro['wrist_diameter'] = $measurement->wrist_diameter;
                            $shopping_pro['cutting'] = $measurement->cutting;
                          }


                   
                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id,'shopping_data' => $shopping_pro,);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be Shopping', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**Get to add to cart  data**/
    public function getDressAddToCartData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required',
            'language_type' => 'required',
        ]);
        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                

                $dress_data = array();
                $cart_pro = CartProduct::where('cart_type', '=','shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->with('getProduct:pro_id,pro_title,pro_title_ar,pro_price,pro_desc,pro_desc_ar,pro_qty,pro_disprice,pro_Img,attribute_id,pro_status,pro_mc_id,Insuranceamount')->first();






                if (!empty($cart) && !empty($cart_pro)) {

                    $attribute = CartServiceAttribute::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=',$cart_pro->product_id)->with('getProductServiceAttribute:id,services_id,attribute_title,attribute_title_ar')->first();



                    $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'shopping')->where('product_id', '=', $input['product_id'])->with('getProductOption:id,services_id,option_title,option_title_ar,status')->with('getProductOptionValue:id,product_id,product_option_id,option_title,option_title_ar,price,status')->first();



                    

                    $rental_products = CartProductRent::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'shopping')->where('product_id', '=', $input['product_id'])->first();

                    $category = Category::select('mc_id','mc_img', 'mc_name', 'mc_name_ar','address','address_ar', 'mc_status')->where('mc_id', '=', $cart_pro->getProduct[0]->pro_mc_id)->first();

                    $quantityattr = '';
                    $pro_disprice = '';
                     $productid = $cart_option_value->product_id;
                   // $len =  strlen($input['rental_date']);



                   if(isset($input['rental_date']) && $input['rental_date']!="") {
                       
                       
                    $rentalpricedata = ProductOptionValue::where('option_title','Rent')->where('product_id',$productid)->first();
                    $price = $rentalpricedata->price;
                    

                         } else {
                      $rentalpricedata = Products::where('pro_id',$productid)->first();
                       $price = $rentalpricedata->pro_price;
                      
                       $pro_disprice = $rentalpricedata->pro_disprice;

                         } 

                // echo $quantityattr->value;die;
             
                    if (isset($cart_pro)) { //get product data
                
                $size =  $cart_pro->product_size;

                $quantityattr = ProductOptionValue::select('value','option_title')->where('product_id','=',$input['product_id'])->where('option_title',$size)->first();


                        $dress_data['pro_id'] = $cart_pro->getProduct[0]->pro_id;
                        if ($input['language_type'] == 'en') {
                            if(!empty($category)){
                               $dress_data['category_name'] = $category->mc_name;
                               $dress_data['category_img'] = $category->mc_img;
                               $dress_data['address'] = $category->address;

                            }
                             
                          
                            if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $dress_data['quantity'] = $quantityattr->value;
                                    }

                             if(isset($attribute->getProductServiceAttribute[0]->attribute_title)){
                            $dress_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title;
                         } else {
                             $dress_data['attribute_title'] = '';
                         } 
                             if(isset($cart_option_value->getProductOptionValue[0]->option_title)){
                            $dress_data['option_title'] = $cart_option_value->getProductOptionValue[0]->option_title;
                             } else {
                                 $dress_data['option_title'] = '';

                             }
                            if(isset($cart_option_value->getProductOptionValue[0]->option_title)){
                            $dress_data['pro_title'] = $cart_pro->getProduct[0]->pro_title;
                           } else {
                              $dress_data['pro_title'] = '';
                           } 
                             if(isset($cart_pro->getProduct[0]->pro_desc)){
                             $dress_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc;
                           } else {
                                $dress_data['pro_desc'] = '';
                        }

                        } else {
                            if(!empty($category)){
                               $dress_data['category_name'] = $category->mc_name_ar;
                               $dress_data['category_img'] = $category->mc_img;
                               $dress_data['address'] = $category->address_ar; 
                            }
                          
                            if(isset($quantityattr->value) && $quantityattr->value!='' ) {
                                   $dress_data['quantity'] = $quantityattr->value;
                                    }

                            
                            $dress_data['attribute_title'] = $attribute->getProductServiceAttribute[0]->attribute_title_ar;
                             if(isset($cart_option_value->getProductOptionValue[0]->option_title_ar)) {
                            $dress_data['option_title'] = $cart_option_value->getProductOptionValue[0]->option_title_ar;
                         } else {
                                $dress_data['option_title'] ='';

                         } 
                            $dress_data['pro_title'] = $cart_pro->getProduct[0]->pro_title_ar;
                            $dress_data['pro_desc'] = $cart_pro->getProduct[0]->pro_desc_ar;
                        }

                        $dress_data['product_size'] = $cart_pro->product_size;
                        $dress_data['pro_qty'] = $cart_pro->quantity;
                        $dress_data['pro_Img'] = $cart_pro->getProduct[0]->pro_Img;
                        $dress_data['pro_status'] = $cart_pro->getProduct[0]->pro_status;
                        if(isset($cart_option_value->product_option_value_id)) {

                            $dress_data['Insuranceamount'] = $cart_pro->getProduct[0]->Insuranceamount;
                           if(isset($cart_option_value->getProductOptionValue[0]->option_title_ar)) {

                            //$dress_data['pro_price'] = $cart_option_value->getProductOptionValue[0]->price;

                            $dress_data['pro_price'] = $price;
                        } else {
                              $dress_data['pro_price'] ='';

                         }

                            $dress_data['total_price'] = $cart_pro->total_price;
                            /*$dress_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;
                            $dress_data['pro_disprice'] = $cart_pro->getProduct[0]->pro_disprice;*/
                        } else {

                            $dress_data['pro_price'] = $cart_pro->getProduct[0]->pro_price;

                            $dress_data['total_price'] = $cart_pro->total_price;
                        }
                    }

                    if (!empty($rental_products)) { //get rental product

                        $dress_data['rental_date'] = date("d-m-Y", strtotime($rental_products->rental_date));
                        $dress_data['rental_time'] = $rental_products->rental_time;
                        $dress_data['return_date'] = date("d-m-Y", strtotime($rental_products->return_date));
                        $dress_data['return_time'] = $rental_products->return_time;
                        $date1=date_create($rental_products->rental_time);
                        $date2=date_create($rental_products->return_time);
                        $diff=date_diff($date1,$date2);
                        $diff->format("%H:%I");
                        $dress_data['total_time'] = $diff->format("%H").' hours';
                    }

                    $response_array = array('data' => ['success' => true, 'message' => 'Data Found', 'code' => 200], 'cart_id' => $cart->id, 'dress_data' => $dress_data);
                } else {
                    $response_array = array('data' => ['success' => false, 'message' => 'cart data not found & cart type must be dress', 'code' => 500]);
                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }

    /**delete to add to cart  data**/
    public function deleteShoppingData(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'token' => 'required'
        ]);
        $input = $request->all();
        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => 'Invalid Input', 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
                $User = Auth::user();

                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->first();
                if (!empty($cart) && !empty($cart_pro)) {

                    $cart_rental_pro = CartProductRent::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->delete();
                    //delete car rent services
                    $cart_body_measurement = CartBodyMeasurement::where('product_id', '=', $input['product_id'])->where('cart_id', '=', $cart->id)->delete();
                    //delete body measurement data

                    $cart_attribute_data= CartServiceAttribute::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->delete();
                    //delete attribute data

                    $cart_pro_opt = CartOption::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete cart option

                    $cart_pro_val = CartOptionValue::where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->where('product_id', '=', $input['product_id'])->delete();
                    //delete cart option value
                       

                    $cart_pro = CartProduct::where('product_id', '=', $input['product_id'])->where('cart_type', '=', 'shopping')->where('cart_id', '=', $cart->id)->delete();
                    //delete product

                    $response_array = array('data' => ['success' => true, 'message' => 'Successfully deleted your cart data', 'code' => 200]);

                }

                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => 'User not found', 'code' => 203]);
                $response = Response::json($response_array, 203);
            }

        }
        return $response;
    }


    //Shipping charges according product id Multiple insert

     
    public function AddShippingCharge(Request $request) {
        $validator = Validator::make($request->all(), [
            /*'product_id' => 'required',
            'token' => 'required',
            'shipping_charge'=>'required'*/
        ]);
        $locale = '';
        if (isset($request->language_type) && $request->language_type == 'ar') {
            $locale = 'ar';
        } else {
            $locale = 'en';
        }
        app()->setLocale($locale);

        $input = $request->all();

        if ($validator->fails()) {
            $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.INPUT_ERROR'), 'code' => 204]);
            $response = Response::json($response_array, 204);
        } else {
            try {
              

                        $User = Auth::user();
                        $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                        if(isset($input['product_id'][0]) && $input['product_id'][0]=='') {
                       $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.SUCCESS'), 'code' => 200]);
                        return $response_array;
                        
                        }

                         if(trim($input['product_id'][0])=='' && trim($input['shipping_charge'][0])=='' && trim($input['shipping_id'][0])=='') {

                         $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.SUCCESS'), 'code' => 200]);

                          } else {
                         $productid = explode(',',$input['product_id']);

                         $shippingcharge = explode(',',$input['shipping_charge']);

                         $shippingid = explode(',',$input['shipping_id']);

                      foreach($productid as $key =>$val) {

                        $saverecord = new OrderProductShipping;

                        $shippingids =     $shippingid[$key];
                        $prodid =          $productid[$key]; 

                         $shippingcharges =  $shippingcharge[$key];    


                        

                        $saverecord ->product_id = $prodid; 

                        $saverecord ->shipping_charge = $shippingcharges; 

                        $saverecord ->shipping_id = $shippingids;
                        $saverecord ->cus_id = $cart->user_id;

                        $cart_pro =  $saverecord->save();

                } 


                 if($cart_pro) { 
                     
                $response_array = array('data' => ['success' => true, 'message' => trans($locale.'_mob_lang.SUCCESS'), 'code' => 200]);
                 } else {

                      $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.FAIL'), 'code' => 203]);


                 } }


                $response = Response::json($response_array, 200);

            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'error' => trans($locale.'_mob_lang.FAIL'), 'code' => 203]);
                $response = Response::json($response_array, 203);
            }
            
        }
        return $response;
    }

}
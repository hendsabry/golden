<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Occasion extends Model 
{
	
	protected $table = 'nm_occasions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['id','images','posted_by','occasion_name','occasion_name_ar','occasion_date','occasion_venue','occasion_venue_ar','occasion_name_ar','hastags','occasion_type','city_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getUser()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\User','posted_by');
    }
    public function getImage()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\OccasionImage','occasion_id','id');
    }

    public function getCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\City', 'city_id','ci_id');
    }


	
}

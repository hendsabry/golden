<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class InternalFoodDish extends Model 
{
	
	protected $table = 'nm_internal_food_dish';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['vendor_id','internal_food_menu_id','end_level_category_id','dish_name','dish_name_ar','dish_image','price','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getContainerPrice()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\InternalFoodContainerPrice','id','dish_id')->withDefault();
    }
	
}

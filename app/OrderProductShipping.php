<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderProductShipping extends Model 
{
	
	protected $table = 'nm_order_product';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['cus_id','product_type','product_sub_type','order_id','category_id','product_id','quantity','total_price','currency','paid_total_amount','product_size','merchant_id','shop_vendor_id','shop_id','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','pro_price','insurance_amount','shipping_id','shipping_charge','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	
	
}

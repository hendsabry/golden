<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class MessageMerchant extends Model
{
    
    protected $guarded = array('id');
    protected $table = 'nm_message_merchant';

    protected $fillable = ['message_id','service_id','vendor_id','created_at','updated_at','status'];      
    
}

?>

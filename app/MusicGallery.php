<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class MusicGallery extends Model 
{
	
	protected $table = 'nm_music_gallery';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','image','title','music_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	
}

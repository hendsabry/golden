<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderInvitation extends Model 
{
	
	protected $table = 'nm_order_invitation';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','services_id','customer_id','order_id','occasion_name','occasion_type','venue','date','time','no_of_invitees','invitation_type','invitation_data','packge_id','total_member_invitation','card_price','status','vendor_id','invitaion_mode','packge_title','packge_title_ar','packge_desc','packge_desc_ar','packge_img','invitation_msg','pro_label','pro_label_ar'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getUser()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\User','customer_id');
    }
    public function getPackage()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','packge_id');
    }

    public function getPackageProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','packge_id','pro_id');
    }
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartInvitation extends Model 
{
	
	protected $table = 'nm_cart_invitation';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','services_id','customer_id','cart_id','occasion_name','occasion_type','venue','date','time','no_of_invitees','invitaion_mode','invitation_msg','packge_id','total_member_invitation','card_price','status','packge_title','packge_title_ar','packge_desc','packge_desc_ar','packge_img','file_url','pro_label','pro_label_ar'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	public function getInvitaionCard()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','packge_id','pro_id');
    }

    public function getInvitaionCategory()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Category','services_id','mc_id');
    }
}

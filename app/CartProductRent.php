<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartProductRent extends Model 
{
	
	protected $table = 'nm_cart_product_rent';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cart_type','rental_date','rental_time','return_date','return_time','service_id','cart_id','product_id','quantity','insurance_amount','day','cart_product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }

    public function getOptionValue()
    {
        /* 2nd argument is foreign key in child (this!) table */
         return $this->hasMany('App\ProductOptionValue','id','service_id');
    }
}

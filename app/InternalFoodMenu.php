<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class InternalFoodMenu extends Model 
{
	
	protected $table = 'nm_internal_food_menu';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['pro_id','vendor_id','menu_name','menu_name_ar','created_at','updated_at','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getDishEn()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\InternalFoodDish','internal_food_menu_id')->select('id','vendor_id','internal_food_menu_id','dish_name as option_title','dish_image');
    }

    public function getDishAr()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\InternalFoodDish','internal_food_menu_id')->select('id','vendor_id','internal_food_menu_id','dish_name_ar as dish_name','dish_image');
    }
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderProductPackage extends Model 
{
	
	protected $table = 'nm_order_product_to_package';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','order_id','product_type','product_id','package_id','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_price','pro_img'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

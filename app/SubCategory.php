<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model 
{
	
	protected $table = 'nm_subcat_to_parent';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','man_cat_id','child_cat_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getCategory()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Category','child_cat_id','mc_id');
    }

	
}

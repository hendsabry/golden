<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Foodcategorylist extends Model
{
		protected $table = 'nm_category';
		
	public function Getfoodshoplistbycity($id,$city_id,$buget,$lang){
		$getPagelimit = config('app.paginate');
			$shopwithinbugetcity=array();

 
  //$ChkList = \Helper::checkShopAvilabe($id, $city_id);



		$category = DB::table('nm_category')->select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mer_activity',1)->where('mc_id', '=', $id)->first(); 
                if(!empty($category)) {

                    if($id != '' && $lang == 'ar_lang') {

                        $foodshopwithinbugetcitya = DB::table('nm_category')->where('mer_activity',1)->where('mc_status',1)->where('parent_id',$id)->select('mc_id','mc_name_ar as mc_name','mc_img')->orderBy('mc_name')->get();
                        $shopId = array(); 
						foreach($foodshopwithinbugetcitya as $val)
		                {
							$getPid = $val->mc_id;  
							$foodshopwithinbugetcity = DB::table('nm_category')->where('parent_id',$getPid)->where('city_id',$city_id)->where('mc_status',1)->count();
							if($foodshopwithinbugetcity >=1)
							{
							   array_push($shopId,$getPid);
							}

		                } 
					    $foodshopwithinbugetcity = DB::table('nm_category')->select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description_ar as mc_video_description','terms_conditions_ar as terms_conditions','longitude','latitude')->whereIn('mc_id',$shopId)->paginate('9');
                    } 
                    else 
                    {
                       // $foodshopwithinbugetcitya = DB::table('nm_category')->where('mc_status',1)->where('parent_id',$id)->select('mc_id', 'mc_name', 'mc_img')->orderBy('mc_name')->paginate('9')->appends(request()->query());
                        $foodshopwithinbugetcitya = DB::table('nm_category')->where('mer_activity',1)->where('mc_status',1)->where('parent_id',$id)->select('mc_id', 'mc_name', 'mc_img')->orderBy('mc_name')->get();
					    $shopId = array();
		                foreach($foodshopwithinbugetcitya as $val)
		                {
							$getPid = $val->mc_id;  
							$foodshopwithinbugetcity = DB::table('nm_category')->where('parent_id',$getPid)->where('city_id',$city_id)->where('mc_status',1)->count();
							if($foodshopwithinbugetcity >=1)
							{
							   array_push($shopId,$getPid);
							}

		                } 


 
					    $foodshopwithinbugetcity = DB::table('nm_category')->select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','terms_conditions','longitude','latitude')->whereIn('mc_id',$shopId)->paginate('9');
                    }

                

            }

		return $foodshopwithinbugetcity;
	}


    public function GetfoodshoplistbycityNew($id,$city_id,$buget,$lang)
    {
		$getPagelimit         = config('app.paginate');
		$shopwithinbugetcity  = array();
		$category = DB::table('nm_category')->select('mc_id','mc_name','mc_name_ar','mc_status')->where('mer_activity',1)->where('mc_id',$id)->first(); 
        if(!empty($category)) 
        {
            if($id != '' && $lang == 'ar_lang') 
            {
                $foodshopwithinbugetcity = DB::table('nm_category')->where('mc_status',1)->where('mer_activity',1)->where('parent_id',$id)->where('city_id',$city_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->orderBy('mc_name')->paginate('9');			
            } 
            else 
            {
                $foodshopwithinbugetcity = DB::table('nm_category')->where('city_id',$city_id)->where('mer_activity',1)->where('mc_status',1)->where('parent_id',$id)->select('mc_id', 'mc_name', 'mc_img')->orderBy('mc_name')->paginate('9')->appends(request()->query());
            }
        }

			return $foodshopwithinbugetcity;
	}


	public function Getfoodparentcategory($id,$lang){
	
				   if ($id != '' && $lang == 'ar_lang') {
					   	$categoryinfo = DB::table('nm_category')->where('mc_status', '=', 1)->where('mer_activity',1)->where('mc_id', '=', $id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->get(); 
				   }else{
						 $categoryinfo = DB::table('nm_category')->where('mc_status', '=', 1)->where('mer_activity',1)->where('mc_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img')->get();
				}
				return $categoryinfo;
	
	}



}
?>
<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
class StaticContentManagement extends Model
{
    
    protected $guarded = array('id');
    protected $table = 'nm_cms';

    protected $fillable = ['title', 'title_ar', 'content','content_ar','meta_title','meta_title_ar', 'meta_description','meta_description_ar', 'tags','tags_ar', 'images','images_ar','mapurl'];
}

?>

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class PoductReview extends Model 
{
	
	protected $table = 'nm_review';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['comment_id','product_id','title','comments','review_type','ratings','title','shop_id','worker_id','vandor_id','status','parentId'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

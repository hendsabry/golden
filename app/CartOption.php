<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartOption extends Model 
{
	
	protected $table = 'nm_cart_option';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cart_type','product_option_id','cart_id','product_id','cart_product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

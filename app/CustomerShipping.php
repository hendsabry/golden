<?php 
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class CustomerShipping extends Authenticatable
{
	
	protected $primaryKey = 'id';
	protected $table = 'nm_customer_shipping_details';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','order_id','customer_id','ship_name','ship_address1','ship_address2','ship_city_id','ship_state','ship_country','ship_postalcode','ship_phone','ship_email'];

	

	

	

      public function getShippingCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\City','ci_id','ship_city_id');
    }


     public function getShippingCountry()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Country','co_id','ship_country');
    }

}

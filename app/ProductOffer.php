<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ProductOffer extends Model 
{
	
	protected $table = 'nm_product_offer';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','title'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderExternalFoodDish extends Model 
{
	
	protected $table = 'nm_order_external_food_dish';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','order_id','category_id','external_food_menu_id','external_food_dish_id','container_id','quantity','price','status','dish_name','dish_name_ar','container_title','img','no_people','container_image'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	public function getContainer()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ProductOptionValue','id','container_id');
    }

    public function getFood()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','external_food_dish_id');
    }

    public function getServiceAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ServiceAttribute','id','external_food_menu_id');
    }
	
}

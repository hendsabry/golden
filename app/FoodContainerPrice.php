<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class FoodContainerPrice extends Model 
{
	
	protected $table = 'nm_product_to_option_price';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','product_id','product_option_id','product_option_value_id','price','discount_price'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function ContainerValue()
    {
        /* 2nd argument is foreign key in child (this!) table */        
        return $this->belongsTo('App\ProductOptionValue','product_option_value_id','id');
    }
	
}

<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class StaticContent extends Model
{
		protected $table = 'nm_cms';
		
	public function Getcmsstaticcontent($id,$lang){
				
		
			
				if($lang=='ar_lang'){
	          $cmspagedata = DB::table('nm_cms')->select('id', 'title_ar as title','content_ar as content','meta_title_ar as meta_title','meta_description_ar as meta_description','tags_ar as tags','images_ar','mapurl')->where('id', '=', $id)->orderBy('title', 'ASC')->get();
                     
		}else{
				$cmspagedata = DB::table('nm_cms')->select('id', 'title','content','meta_title','meta_description','tags','images','mapurl')->where('id', '=', $id)->orderBy('title', 'ASC')->get();
                   
		}
				return $cmspagedata;
		}

}
?>
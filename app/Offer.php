<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model 
{
	
	protected $table = 'nm_offer_list';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['vendor_list','offer_name', 'offer_desc','manager_id', 'status', 'updated_at', 'created_at'];


	public function getManager()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\AdminModel','adm_id','manager_id');
    }
    public function getSalesRep()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\AdminModel','adm_id','vendor_list');
    }

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

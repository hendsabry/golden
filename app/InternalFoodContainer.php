<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class InternalFoodContainer extends Model 
{
	
	protected $table = 'nm_internal_food_container';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['img','dish_id','ven_id','title','title_ar','no_people','short_code','price','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function getContainerList()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\InternalFoodContainer','id','container_id');
    }
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class MerchantPaymentHistory extends Model 
{
	
	protected $table = 'nm_merchant_payment_history';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['merchant_id','order_id','transation_id','amount','note'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function getOrder()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Order','order_id','order_id');
    }
    public function getMarchant()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Merchant','mer_id','merchant_id');
    }
	
	
}

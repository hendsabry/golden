<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class VendorReply extends Model 
{
	
	protected $table = 'nm_vendor_comment';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','cat_id','vendor_id','user_id','enq_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderHospitality extends Model 
{
	
	protected $table = 'nm_order_reception_hospitality';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','order_id','category_id','no_of_staff','nationality','date','time','elocation','order_type'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	//Make relation for get country detail
    public function getNationality()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Country', 'nationality','co_id');
    }
}

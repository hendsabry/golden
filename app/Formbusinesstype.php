<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;
class Formbusinesstype extends Model
{
		protected $table = 'nm_business_occasion_type';
		
	public function Getbussinesstype($id,$lang){
				$id = Session::get('searchdata.mainselectedvalue');
		
				//$bussinesstypedata = DB::table('nm_business_occasion_type')->where('top_category', $id)->get()->toArray();
				
				if($lang=='ar_lang'){
	          $bussinesstypedata = DB::table('nm_business_occasion_type')->select('id', 'title_ar as title')->where('top_category', '=', $id)->orderBy('title', 'ASC')->get();
                     
		}else{
				$bussinesstypedata = DB::table('nm_business_occasion_type')->select('id', 'title')->where('top_category', '=', $id)->orderBy('title', 'ASC')->get();
                   
		}
				return $bussinesstypedata;
		}

}
?>
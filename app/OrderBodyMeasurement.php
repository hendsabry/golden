<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderBodyMeasurement extends Model 
{
	
	protected $table = 'nm_order_body_measurement';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','product_id','customer_id','order_id','length','chest_size','waistsize','soulders','neck','arm_length','wrist_diameter','cutting','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	public function getInvitaionCard()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','invitation_card','pro_id');
    }

    public function getInvitaionCategory()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Category','services_id','mc_id');
    }
}

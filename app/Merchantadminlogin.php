<?php

namespace App;

use DB;

use Session;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Authenticatable;

use Illuminate\Support\Facades\Hash;



class Merchantadminlogin extends Model

{

    protected $guarded = array('id');

    protected $table = 'nm_admin';

    

    public static function login_check($uname,$password)

    {

        $check = DB::table('nm_admin')->where('adm_email', '=' ,$uname)->where('status','=',1)->get();

		$checkpassword = 0;

        if (count($check) > 0) {

        if (Hash::check($password, $check[0]->adm_password)) {

        Session::put('siteadmin','siteadmin');

        Session::put('userid', $check[0]->adm_id);

        Session::put('username', $check[0]->adm_fname);

        Session::put('access_group_id', $check[0]->access_group_id);

        Session::put('access_permission_type', $check[0]->permission_status);

        return 1;

        }

        else

        {

        return -2;

        } 

        } else {

        return 0;

        }

    }



    public static function forgot_check($email)

    {

        $check = DB::table('nm_admin')->where('adm_email', '=', $email)->first();

        if ($check) {

            return 1;

            

        } else {

            return 0;

        }

    }



    public static function forgot_check_details($email)

    {

        return DB::table('nm_admin')->where('adm_email', '=', $email)->get();

        

    }



    public static function forgot_check_details_merchant($email)

    {

        return DB::table('nm_merchant')->where('mer_email', '=', $email)->get();

        

    }



    public static function checkmerchantlogin($uname, $pwd)
    {
        /**Check user email  **/
        $check_email = DB::table('nm_merchant')->where('mer_email',$uname)->where('mer_staus','1')->get();
        $check_password=0;
        if(count($check_email)>0)

        { 

            foreach($check_email as $check)

            {

                if($check->mer_email==$uname) 

                {
                    //$check_password = DB::table('nm_merchant')->where('mer_email', '=', $uname)

                    //->where('mer_password', '=', $pwd)->where('mer_staus','=',1)->get();





        if(\Hash::check($pwd, $check->mer_password)){
        $check_password = DB::table('nm_merchant')->where('mer_email', '=', $uname)->where('mer_staus','=','1')->get();



                        } else {



                        return -1;  



                        }  





                                                

                } else

                    {

                         return -1;   

                    }

                    

                if(count($check_password)<1)

                    {

                         return -2;   

                    } 

                    else if(count($check_password)>0)

                    { 

                       

                        if($check->vendor_parent_id =='0' || $check->vendor_parent_id==NULL)

                        {

                         Session::put('sitemerchant','sitemerchant');

                        Session::put('merchantid', $check->mer_id);

                        Session::put('LoginType', $check->mer_logintype);

                        Session::put('Parent_Vendor', $check->addedby);

                        Session::put('merchantname', $check->mer_fname);   

                        }

                        else

                        {

                         Session::put('sitemerchant','sitemerchant');

                        Session::put('merchantid', $check->vendor_parent_id);

                        Session::put('merchant_managerid', $check->mer_id);

                        Session::put('LoginType', $check->mer_logintype);

                        Session::put('Parent_Vendor', $check->addedby);

                        Session::put('merchantname', $check->mer_fname);   

                        }

                         

                        return 1;

                    }  

            }

        } 

        else 

         { 

         return 0;

         }

    }



     /*   $check_password = DB::table('nm_merchant')->where('mer_email', '=', $uname)->where('mer_password', '=', $pwd)->where('mer_staus','=',1)->get();

		

		

        if ($check) {

			foreach($check as $chk){

                echo $chk->mer_email,$uname; exit();

               

			if($chk->mer_email == $uname && $chk->mer_password == $pwd)

			{ 

                 

			Session::put('sitemerchant','sitemerchant');

            Session::put('merchantid', $chk->mer_id);

            Session::put('merchantname', $chk->mer_fname);

            return 1;

            

			}

			else if($chk->mer_email != $uname)

			{

				return -1;

			}

			else if($chk->mer_password != $pwd)

			{

				return -2;

			}

			}  

        }

		else {

            return 0;

        }

    }*/



    public static function checkvalidemail($email)

    {

        return DB::table('nm_merchant')->where('mer_email', '=', $email)->get();

        

    }

    

    public static function get_merchant_details($merchat_decode_email)

    {

        

        return DB::table('nm_merchant')->where('mer_email', '=', $merchat_decode_email)->get();

    }

    

    public static function update_newpwd($mer_id, $confirmpwd)

    {

        // return DB::table('nm_merchant')->where('mer_id', '=', $mer_id)->update(array(

        //     'mer_password' => $confirmpwd

        // ));



        return DB::table('nm_merchant')->where('mer_id', '=', $mer_id)->update(array(

            'mer_password' => bcrypt($confirmpwd)

        ));

        

    }

    

}



?>


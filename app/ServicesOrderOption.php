<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ServicesOrderOption extends Model 
{
	
	protected $table = 'nm_cart_option';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['order_id','services_order_option_id','cart_id','services_id','	status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContainerPackage extends Model
{
    protected $table = 'nm_internal_food_container';
    
}

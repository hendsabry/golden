<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model 
{
	
	protected $table = 'nm_cart_product';

	/**
	 * The attributes that are mass assignable. total_price 
	 *
	 * @var array
	 */
	protected $fillable = ['merchant_id','shop_vendor_id','shop_id','cart_type','cart_sub_type','cart_id','category_id','product_id','quantity','total_price','status','product_size','pro_price','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','status','insurance_amount','pro_price','fabric_name','rose_package_type','paid_total_amount','location','worker_price','review_type','buy_rent','hall_booking_date','noofstaff','container_id','coupon_code','coupon_code_amount','price_after_coupon_applied','deliver_day','showcartproduct'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	//Relation for get hall product detail
	public function getHall()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }
   //Relation for get product detail
    public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }
    //Relation for get Category detail
    public function getCategory()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Category','mc_id','category_id');
    }
	
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletModel extends Model
{
    protected $table = 'nm_wallet_transaction';
    protected $fillable = ['user_id', 'vendor_id','amount','transaction_type','transction_id','notes','status'];

    public function getVendor()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Merchant','mer_id','vendor_id');
    }

}

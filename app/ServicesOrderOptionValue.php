<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ServicesOrderOptionValue extends Model 
{
	
	protected $table = 'nm_product_option_value';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','product_option_id','option_title'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

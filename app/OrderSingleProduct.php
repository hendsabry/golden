<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderSingleProduct extends Model 
{
	
	protected $table = 'nm_order_product';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','product_type','product_sub_type','order_id','category_id','product_id','quantity','total_price','status','product_size','merchant_id','shop_vendor_id','shop_id','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_Img','pro_price','insurance_amount','buy_rent','paid_total_amount','currency','shipping_id','shipping_charge','review_type','convirson_rate','hall_booking_date','rose_package_type','location','fabric_id','fabric_name','fabric_img','fabric_price','coupon_code','coupon_code_amount','price_after_coupon_applied','deliver_day','showcartproduct','enquiry_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	//Relation for get hall product detail
	public function getHall()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }
   //Relation for get product detail
    public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }
    //Relation for get Category detail
    public function getCategory()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Category','mc_id','category_id');
    }

    public function getMerchant()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Merchant','mer_id','merchant_id');
    }
	
}

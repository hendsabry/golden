<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartOptionValue extends Model 
{
	
	protected $table = 'nm_cart_option_value';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cart_type','cart_id','product_id','product_option_id','product_option_value_id','value','status','quantity','option_title','option_title_ar','option_value_title','option_value_title_ar','price','no_person','cart_product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	 public function getProductOptionValue()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\ProductOptionValue','id','product_option_value_id');
    }
    public function getProductOption()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\ProductOption','product_option_id','id');
    }
    public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','product_id','pro_id');
    }
	
}

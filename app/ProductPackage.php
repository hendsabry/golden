<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ProductPackage extends Model 
{
	
	protected $table = 'nm_product_to_packeg';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['shop_id','attribute_id','item_id','packege_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function getServiceAttribute()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\ServiceAttribute','attribute_id','id');
    }

    public function getProductPackage()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Products','item_id','pro_id');
    }

	
}

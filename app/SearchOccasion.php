<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class SearchOccasion extends Model 
{
	
	protected $table = 'nm_search_occasion';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','order_id','user_id','main_cat_id','occasion_id','budget','total_member','city_id','occasion_date','end_date','gender'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
	//Make relation for get city detail
    public function getCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\City', 'city_id','ci_id');
    }

    public function getOccasionType()
    {
        /* 2nd argument is foreign key in child (this!) table */
         return $this->hasMany('App\BusinessOccasionType','id','occasion_id');
    }
}

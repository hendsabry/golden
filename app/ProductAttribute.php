<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model 
{
	
	protected $table = 'nm_product_attribute';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['attribute_title'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

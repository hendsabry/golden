<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model 
{
	
	protected $table = 'nm_cart';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

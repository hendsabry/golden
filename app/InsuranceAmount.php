<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class InsuranceAmount extends Model 
{
	
	protected $table = 'nm_merchant_payment_history';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['merchant_id','product_type','product_id','order_id','amount','requested_date','transation_id','status','pro_title','pro_title_ar','img'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	//Relation for get product detail
    public function getProduct()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Products','pro_id','product_id');
    }
	
}

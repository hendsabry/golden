<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;


class Internalfooditems extends Model
{
		protected $table = 'nm_internal_food_menu';
		
	public function Getnm_internal_food_menutype($id,$lang){			

			$getVdorID = DB::table('nm_product')->select('pro_mr_id')->where('pro_id', $id)->first();
			$vid = $getVdorID->pro_mr_id;
			if($lang=='ar_lang'){
			$bussinesstypedata = DB::table('nm_internal_food_menu')->select('id', 'menu_name_ar as menu_name')->where('pro_id', '=', $id)->where('vendor_id', '=', $vid)->orderBy('menu_name', 'ASC')->get();

			}else{
			$bussinesstypedata = DB::table('nm_internal_food_menu')->select('id', 'menu_name')->where('pro_id', '=', $id)->where('vendor_id', '=', $vid)->orderBy('menu_name', 'ASC')->get();

			} 
			return $bussinesstypedata;
		}
    public  function GetfullItemslistingWithMainmenuAndContainer($mainmenuid,$productid,$lang){
			$ItemslistingWithMainmenuAndContainer=array();
				if($lang=='ar_lang'){
	         $mainmenu = DB::table('nm_internal_food_menu')->select('id', 'menu_name_ar as menu_name')->where('pro_id', '=', $productid)->where('id', '=', $mainmenuid)->orderBy('menu_name', 'ASC')->get();
            $dishintem=  DB::table('nm_internal_food_dish')->select('id', 'dish_name_ar as dish_name')->where('relate_with', '=', $productid)->where('internal_food_menu_id', '=', $mainmenuid)->orderBy('dish_name', 'ASC')->get();
                   
		}else{
				$mainmenu = DB::table('nm_internal_food_menu')->select('id', 'menu_name')->where('pro_id', '=', $productid)->where('id', '=', $mainmenuid)->orderBy('menu_name', 'ASC')->get();
             $dishintem=  DB::table('nm_internal_food_dish')->select('id', 'dish_name')->where('relate_with', '=', $productid)->where('internal_food_menu_id', '=', $mainmenuid)->orderBy('dish_name', 'ASC')->get();  
		}
			foreach($dishintem as $getdishid){
				 $did=$getdishid->id;
				$containerprice[]= DB::table('nm_internal_food_container_price')->leftJoin('nm_internal_food_container', 'nm_internal_food_container_price.container_id', '=', 'nm_internal_food_container.id')->select('nm_internal_food_container_price.id', 'nm_internal_food_container_price.container_price','nm_internal_food_container_price.dish_id as did','nm_internal_food_container.title','nm_internal_food_container.id as cid')->where('nm_internal_food_container_price.dish_id', '=', $did)->get()->toArray();
			}
		
		
		
		
		
		
		$ItemslistingWithMainmenuAndContainer=array('mainmenu'=>$mainmenu,'dishitem'=>$dishintem,'containerprice'=>$containerprice);
		
		
		return $ItemslistingWithMainmenuAndContainer;
		
		
		
		
		
	
	}

 public function GetcontainerListByProduct($id,$lang){

		if($lang=='ar_lang'){
				 $containerdata = DB::table('nm_internal_food_container')->select('id', 'title_ar as title','img','no_people')->where('pro_id', '=', $id)->orderBy('title', 'ASC')->get();
		}else{
				 $containerdata = DB::table('nm_internal_food_container')->select('id', 'title','img','no_people')->where('pro_id', '=', $id)->orderBy('title', 'ASC')->get();
		}
		return $containerdata;
	}
}
?>
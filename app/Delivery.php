<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model 
{
	
	protected $table = 'nm_delivery';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','email','phone_no','gender','dob','address','status','image','co_id','ci_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	 public function getCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\City','ci_id','ci_id');
    }


     public function getCountry()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Country','co_id','co_id');
    }

	
}

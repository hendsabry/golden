<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Clinicshop extends Model
{
		protected $table = 'nm_category';
		################ beauty shop center #############

        ############## becauty shop center end ##########

	public function Getfoodshopdate($id,$lang){
	
				   if ($id != '' && $lang == 'ar_lang') {
					   	$categoryinfo = DB::table('nm_category')->where('mc_status', '=', 1)->where('mc_id', '=', $id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription','address_ar as address', 'mc_img','city_id','mc_video_url','mc_video_description_ar as mc_video_description','home_visit_charge','google_map_address','terms_conditions','longitude','latitude','opening_time','closing_time')->get(); 
				   }else{
						 $categoryinfo = DB::table('nm_category')->where('mc_status', '=', 1)->where('mc_id', '=', $id)->select('mc_id', 'mc_name', 'mc_img','mc_discription','city_id','mc_video_url','address','mc_video_description','home_visit_charge','google_map_address','terms_conditions','longitude','latitude','opening_time','closing_time')->get();
				}
				return $categoryinfo;
	
	}

 public function Getgalleryimages($id){
 				$categorygallery = DB::table('nm_category_gallery')->where('category_id', '=', $id)->select('id','image')->get(); 
				return $categorygallery;

 }
 
  public function GetshopReviews($id){
  				
				$shopreview=DB::table('nm_review')->orderby('comment_id','desc')->Leftjoin('nm_category','nm_review.shop_id','=','nm_category.mc_id')->Leftjoin('nm_customer','nm_review.customer_id','=','nm_customer.cus_id')->where('nm_review.shop_id','!=','NULL')->where('nm_review.shop_id','=',$id)->where('nm_review.review_type','=','shop')->get();
				return $shopreview;
  }
  

  public function Getfoodshopdateproduct($id,$lang){
					$getPagelimit = config('app.paginate');
				   if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}
				return $productdateshopinfo;
	
	}



public function Getfoodshopdatedishproduct($id,$lang){
					$getPagelimit = config('app.paginate');
				   if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}
				return $productdateshopinfo;
	
	}


	public function Getfoodshopdessertproduct($id,$lang){
					$getPagelimit = config('app.paginate');
				   if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->where('attribute_id', '=', 44)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', 44)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}
				return $productdateshopinfo;
	
	}

	public function Getfoodshopdessertdishproduct($id,$lang){
					$getPagelimit = config('app.paginate');
				   if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->where('attribute_id', '=', 45)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price', 'pro_Img','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', 45)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->orderBy('pro_title')->paginate('9')->appends(request()->query());
				}
				return $productdateshopinfo;
	
	}

public function Getfoodshopdatefirstproduct($id,$lang){


					$getPagelimit = config('app.paginate');

				   if ($id != '' && $lang == 'ar_lang') {
					  $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title','specialistion_ar as specialistion', 'pro_price','pro_desc_ar as pro_desc', 'pro_Img','pro_disprice','service_hour','pro_mr_id','pro_discount_percentage','opening_time','closing_time','video_url')->orderBy('pro_title')->first();

				   }else{
					$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice','pro_desc','service_hour','pro_discount_percentage','pro_mr_id','specialistion','opening_time','closing_time')->orderBy('pro_title')->first();
				}

			
				return $productdateshopinfo;
				
	
	}


public function Getfoodshopdatedishfirstproduct($id,$lang){
					$getPagelimit = config('app.paginate');
				   if ($id != '' && $lang == 'ar_lang') {
					  $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price','pro_desc_ar as pro_desc', 'pro_Img','pro_disprice')->orderBy('pro_title')->first();

				   }else{
					$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice','pro_desc')->orderBy('pro_title')->first();
				}
				
				/*$productdateshopproductprice = DB::table('nm_product_option_value')->where('product_id', '=', $productdateshopinfo->pro_id)->select('id', 'option_title_ar as option_title', 'product_option_id','value as product_option_value_id','discount')->orderBy('id')->get();
						$productdateshopinfo->product_price = $productdateshopproductprice;*/
						
				return $productdateshopinfo;
				
	
	}

public function Getfoodshopdatedessertdishfirstproduct($id,$lang){
					$getPagelimit = config('app.paginate');
				   if ($id != '' && $lang == 'ar_lang') {
					  $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', 45)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title', 'pro_price','pro_desc_ar as pro_desc', 'pro_Img','pro_disprice')->orderBy('pro_title')->first();

				   }else{
					$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('attribute_id', '=', 45)->where('pro_mc_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice','pro_desc')->orderBy('pro_title')->first();
				}

				
						
				$productdateshopinfo->product_price = $productdateshopproductprice;
					
				return $productdateshopinfo;
				
	
	}
	
	 public function getajaxdatefoodproduct($id,$lang='',$branchid,$tblfld,$datea=''){
					//$id='107';
				   if ($id != '' && $lang == 'ar_lang') {
					   	$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id', '=', $id)->select('pro_id', 'pro_title_ar as pro_title','pro_desc_ar as pro_desc', 'pro_price', 'pro_Img','pro_disprice','service_hour','pro_mr_id','pro_discount_percentage','packege','video_url','specialistion_ar as specialistion','opening_time','closing_time')->orderBy('pro_title')->get();
					   	
				   }else{
						 $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id', '=', $id)->select('pro_id', 'pro_title', 'pro_Img','pro_mc_id','attribute_id','pro_price','pro_disprice','option_id','pro_desc','service_hour','pro_mr_id','pro_discount_percentage','packege','video_url','specialistion','opening_time','closing_time')->orderBy('pro_title')->get();
						 
				}
					 ///////////////// discounted price calculation //////////
						$discountpercentage=$productdateshopinfo[0]->pro_discount_percentage;
						if($discountpercentage>0){
							$productprice=$productdateshopinfo[0]->pro_price*(100-$discountpercentage)/100;
						}else{
							$productprice=$productdateshopinfo[0]->pro_price;
						}
					////////////////// end discounted price calculation/////

					
					 
		 if($id!=''){    
          if ($lang == 'ar_lang') {
            $professionalbio = DB::table('nm_product_attribute')->where('product_id', '=', $id)->select('id','attribute_title_ar as attribute_title')->orderBy('id')->get();

               }else{
            $professionalbio = DB::table('nm_product_attribute')->where('product_id', '=', $id)->select('id','attribute_title')->orderBy('id')->get();
          }
          }else{
            $professionalbio=array();
          }




		$docid = $id;
        $date = strtotime($datea);   
        $shop_id = $branchid;     
         
       $getClinicInfo =  DB::table('nm_order_services_staff')->where('product_type','clinic')->where('service_id',$docid)->where('shop_id',$shop_id)->get();
      $getContent=array();


      //print_r($getClinicInfo); die;

       foreach ($getClinicInfo as $key => $value) {
          $getBookingD = strtotime($value->booking_date);
          $TimeD = $value->start_time;
          if($getBookingD == $date)
          {
        array_push($getContent, $TimeD);
          }
       }



				 
				$productdatesshopfullinfo=array('productsericeinfo'=>$productdateshopinfo,'expertstaff'=>$professionalbio,'productprice'=>$productprice,'bookeddate'=>$getContent);
				//echo "<pre>";
				//print_r($productdateshopinfo);
				return $productdatesshopfullinfo;
	
	}
///////////////// get extra cart information/////////////

	public function Getproductforcart($id,$lang){


					$productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id', '=', $id)->select('pro_id', 'pro_title','pro_title_ar', 'pro_Img','pro_price','pro_disprice','pro_desc','service_hour','pro_discount_percentage','pro_mr_id')->orderBy('pro_title')->first();
				
				return $productdateshopinfo;
				
	
	}

	public function Getstaffinformation($id,$lang){


					$servicestaffexpert = DB::table('nm_services_staff')->where('id', '=', $id)->select('staff_member_name', 'staff_member_name_ar','image')->first();
				
				return $servicestaffexpert;
				
	
	}

}
?>
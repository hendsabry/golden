<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class CartProductPackage extends Model 
{
	
	protected $table = 'nm_cart_product_to_package';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cart_id','cart_type','product_id','package_id','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_price','pro_img','pro_disprice'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class SingerAndBand extends Model 
{
	
	protected $table = 'nm_music';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','category_id','city_id','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

	//Make relation for get city detail
    public function getCity()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\City', 'city_id','ci_id');
    }

    //Make relation for get Category Gallery
    public function CategoryGallery()
    {
        /* 2nd argument is foreign key in child (this!) table */        
        return $this->hasMany('App\MusicGallery','music_id','id');
    }
	
	
}

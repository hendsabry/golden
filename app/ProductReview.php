<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model 
{
	
	protected $table = 'nm_review';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['order_id','product_id','worker_id','title','shop_id','customer_id','status'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */

		public function getUser()
		{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->hasMany('App\User','cus_id','customer_id');
		}
		//Make relation for get category review detail
	    	public function getCategoryReview()
	    	{
	        /* 2nd argument is foreign key in child (this!) table */
	        return $this->belongsTo('App\Category', 'shop_id','mc_id');
	    	}
	    	//Make relation for get worker review detail
	    	public function getWorkerReview()
		{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->belongsTo('App\ServicesStaff','worker_id','id');
		}
		//Make relation for get product review detail
		public function getProductReview()
		{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->belongsTo('App\Products','product_id','pro_id');
		}

		public function getCategory()
		{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->hasMany('App\Category','mc_id','shop_id');
		}

		public function getWorker()
		{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->hasMany('App\ServicesStaff','id','worker_id');
		}
		public function getProduct()
		{
		/* 2nd argument is foreign key in child (this!) table */
		return $this->hasMany('App\Products','pro_id','product_id');
		}
	
	
}

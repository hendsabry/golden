<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class OrderOption extends Model 
{
	
	protected $table = 'nm_order_option';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cus_id','product_type','product_option_id','order_id','product_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

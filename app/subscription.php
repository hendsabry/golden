<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class subscription extends Model 
{
	
	protected $table = 'nm_subscription_plan';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['cat_id','services_id','for_month','payment','status','description'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function getCategory()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Category','mc_id','cat_id');
    }
	
}

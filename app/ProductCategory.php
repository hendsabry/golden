<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;


class ProductCategory extends Model
{
		protected $table = 'nm_category';
		
	public function Getbasicfromcategory($id,$lang){
			
					if($lang=='ar_lang'){
						 $categorydata = DB::table('nm_category')->select('mc_id', 'mc_name_ar as mc_name','mc_img')->where('mc_id', '=', $id)->orderBy('mc_name', 'ASC')->get();
				}else{
						 $categorydata = DB::table('nm_category')->select('mc_id', 'mc_name','mc_img')->where('mc_id', '=', $id)->orderBy('mc_name', 'ASC')->get();
				}
		return $categorydata;
	
	}
	
	public function productinfo($id,$lang){
		
		if($lang=='ar_lang'){
						 $productdata = DB::table('nm_product')->select('pro_id', 'pro_title_ar as pro_title','pro_price','pro_disprice','Insuranceamount','pro_mc_id','pro_discount_percentage')->where('pro_id', '=', $id)->orderBy('pro_title', 'ASC')->get();
				}else{
						 $productdata = DB::table('nm_product')->select('pro_id', 'pro_title','pro_price','pro_disprice','Insuranceamount','pro_mc_id','pro_discount_percentage')->where('pro_id', '=', $id)->orderBy('pro_title', 'ASC')->get();
				}
		return $productdata;
	
	}
	
	public function productoption($id,$lang){
			if($lang=='ar_lang'){
						 $productdata = DB::table('nm_product_option_value')->select('id', 'option_title_ar as option_title','price')->whereIn('id', $id)->orderBy('option_title', 'ASC')->get();
				}else{
						 $productdata = DB::table('nm_product_option_value')->select('id', 'option_title','price')->whereIn('id', $id)->orderBy('option_title', 'ASC')->get();
				}
		return $productdata;
	}
	
	public function eventtypeoption($id,$lang){
			if($lang=='ar_lang'){
						 $productdata = DB::table('nm_business_occasion_type')->select('id', 'title_ar as title')->where('id', $id)->orderBy('title', 'ASC')->get();
				}else{
						 $productdata = DB::table('nm_business_occasion_type')->select('id', 'title')->where('id', $id)->orderBy('title', 'ASC')->get();
				}
		return $productdata;
	}
	


	public function foodproductbranchcontainer($id,$lang){
			if($lang=='ar_lang'){
						 $productdata = DB::table('nm_product_option_value')->select('id', 'option_title_ar as option_title','no_person', 'short_name_ar as short_name','image')->where('product_id','=', $id)->where('status','=', 1)->orderBy('option_title', 'ASC')->get();
				}else{
						 $productdata = DB::table('nm_product_option_value')->select('id', 'option_title','no_person','short_name','image')->where('product_id','=', $id)->where('status','=', 1)->orderBy('option_title', 'ASC')->get();
				}
		return $productdata;
	}

}
?>
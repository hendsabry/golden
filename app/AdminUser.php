<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    

    protected $primaryKey = 'adm_id';

	protected $table = 'nm_admin';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['adm_fname', 'adm_lname', 'adm_email','access_group_id','adm_phone','adm_address'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	




    
}

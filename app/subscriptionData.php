<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class subscriptionData extends Model 
{
	
	protected $table = 'nm_subscription';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['sub_plan_id','sub_cus_id','sub_mc_id','sub_status','sub_readstatus'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	public function getMarchant()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\Merchant','mer_id','sub_mc_id');
    }

    public function getUser()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\User','cus_id','sub_cus_id');
    }

    public function getPlan()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->hasMany('App\subscription','id','sub_plan_id');
    }
	
}

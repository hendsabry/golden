<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class StaffNationality extends Model 
{
	
	protected $table = 'nm_service_staff_nationality';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id','vendor_id','shop_id','branch_id','nation_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	//Make relation for get country detail
    public function getNationality()
    {
        /* 2nd argument is foreign key in child (this!) table */
        return $this->belongsTo('App\Country', 'nation_id','co_id');
    }
	
}

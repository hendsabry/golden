<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class DoctorPrescription extends Model 
{
	
	protected $table = 'nm_doctor_prescription';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['clinic_id','doc_id','file_number'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

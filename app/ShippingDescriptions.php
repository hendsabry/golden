<?php 
namespace App;
use DB;
use File;
use Illuminate\Database\Eloquent\Model;

class ShippingDescriptions extends Model 
{
	
	protected $table = 'nm_shipping_descriptions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['shipping_id','shipping','delivery_time','description'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *cus_pwd
	 * @var array
	 */
	
}

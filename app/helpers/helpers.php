<?php

use Illuminate\Support\Str;
use App\NmServicesAttribute;
use App\ProductAttribute;
 
class Helper {
            public static function cur_sym() {
            $currency_sym = DB::table('nm_paymentsettings')->first();
            echo $currency_sym->ps_cursymbol; 
            //return Str::slug($str);
            }
            public static function cur_code() {
            $currency_sym = DB::table('nm_paymentsettings')->first();
            return $currency_sym->ps_curcode; 
            //return Str::slug($str);

            }
            public static function customer_support_number() {
            //DB::table('nm_emailsetting')->get();
            $emailsettings = DB::table('nm_emailsetting')->get();
            foreach($emailsettings as $enquiry_det){}
            echo $enquiry_det->es_phone1; 
            //return Str::slug($str);
            }

            public static function lang_name()
            {
              echo $lang_name = 'French'; 
            }
            public static function calculatevat($orderid,$amount)
            { 
              $getvatfromorder = DB::table('nm_order')->where('order_id',$orderid)->first();

              $gettaxpercentage=$getvatfromorder->vat_tax;
              $getvatamount=$gettaxpercentage*$amount/100;
              if(isset($getvatamount) && $getvatamount>0)
                {
                  $taxamo=$getvatamount;
                }else{
                    $taxamo=$getvatfromorder->order_taxAmt;
                }
              return $taxamo;   
            }
            

            public static function updatevendororderstatus($id)
            { 
              DB::table('nm_order_product')->where('id',$id)->update(['read_status'=>1]);
              return 'done';   
            }

            public static function getAllCategoryById($parent_id,$category_id)
            { 
              $getChildID = DB::table('nm_category')->where('parent_id',$parent_id)->where('mc_id',$category_id)->where('mc_status',1)->first();
              return $getChildID;   
            }
            
            public static function getSubCategoryType($merchant_id,$product_id,$order_id)
            { 
              if(isset($merchant_id) && ($merchant_id!='' || $merchant_id!='0'))
              {
                $getChildID = DB::table('nm_order_product')->where('merchant_id',$merchant_id)->where('product_id',$product_id)->where('order_id',$order_id)->first();
              }
              else
              {
                $getChildID = DB::table('nm_order_product')->where('product_id',$product_id)->where('order_id',$order_id)->first();
              }
              return $getChildID;   
            }

            public static function getInvitationOrder($order_id,$customer_id)
            { 
              $getInOrder = DB::table('nm_order_invitation')->where('order_id',$order_id)->where('order_id',$order_id)->first();
              return $getInOrder;   
            }

            public static function getInvitationOrderdate($order_id,$oid)
            { 
              $getInOrder = DB::table('nm_order_invitation')->where('order_id',$order_id)->first();
              $todaydate=strtotime(date('Y-m-d'));
              $eventdate=strtotime($getInOrder->date);
              if($todaydate>$eventdate){
                DB::table('nm_order_product')->where('id', $oid)->update(['status'=>2]);
               $ost='complete';} else { $ost='process';}
              return $ost;   
            }
            public static function getChildCats($par_id)
            { 
              $getChildID = DB::table('nm_category')->where('parent_id',$par_id)->where('mc_id','!=',16)->get();
              return $getChildID;   
            }
            public static function getparentCat($par_id)
            { 
              $getChildID = DB::table('nm_category')->where('mc_id',$par_id)->first();
              return $getChildID;   
            }


            public static function getproductorderattribute($ord_id,$proid)
            { 
              $getorderedprodattrbute = DB::table('nm_order_services_attribute')->where('order_id',$ord_id)->where('product_id',$proid)->first();
              return $getorderedprodattrbute;   
            }

             public static function getmyaccountproductorderattribute($ord_id,$protype)
            { 
              if($protype=='design') { $atttype='Design Your Package';}else{ $atttype='Package'; }
              $getorderedprodattrbute = DB::table('nm_order_services_attribute')->where('order_id',$ord_id)->where('attribute_title',$atttype)->get();
              return $getorderedprodattrbute;   
            }

             public static function noofinvitees($ord_id)
            { 
              
              $noofinvitees = DB::table('nm_invitation_list')->where('order_id',$ord_id)->count();
              return $noofinvitees;   
            }

             public static function subscription_plans_type($ord_id)
            { 
              
              $subscriptionplanstype = DB::table('nm_subscription_plan')->where('cat_id',$ord_id)->first();
              return $subscriptionplanstype;   
            }

            public static function noofattends($ord_id)
            { 
              
              $noofattends = DB::table('nm_invitation_list')->where('order_id',$ord_id)->where('status','1')->count();
              return $noofattends;   
            }
              public static function getparenthallCat($par_id)
            { 
              $lang  = Session::get('lang_file');
               if($lang != '' && $lang == 'ar_lang') 
               {  
              $getChildID = DB::table('nm_category')->where('mc_id',$par_id)->select('mc_name_ar as mc_name')->first();
               }else{

                $getChildID = DB::table('nm_category')->where('mc_id',$par_id)->select('mc_name')->first();
               } 
               return $getChildID;   
            }
            

            public static function checkproductavailablity($product_id,$carttype,$cartsubtype,$cart_id,$quantity,$cartproducttblid){
              $getdbqty = DB::table('nm_product')->where('pro_id',$product_id)->first();
                  if(isset($quantity) && $getdbqty->pro_qty<$quantity){

                  }

            }
            
          public static function checktravelpackage($product_id){

            $ispackage = DB::table('nm_product_attribute')->where('product_id',$product_id)->where('attribute_title','Package Date')->count();
            if($ispackage<1){
              DB::table('nm_product')->where('pro_id', $product_id)->update(['pro_status'=>0]);
            }else{
                  $t=time();
                  $st=0;
                $avipackage = DB::table('nm_product_attribute')->where('product_id',$product_id)->where('attribute_title','Package Date')->first();
                  $packages=explode(",",$avipackage->value);
                  foreach ($packages as $val){
                    # code...
                    $dbpackage=strtotime($val);
                    if($t>$dbpackage){
                      $st=1;
                    }else{
                      $st=0;
                    }

                  }
                  if($st==1){
                    DB::table('nm_product')->where('pro_id', $product_id)->update(['pro_status'=>0]);
                  }

            }

          }

            public static function getbookinginformation($cartid,$produid){
                $productschu_attr = DB::table('nm_cart_reception_hospitality')->where('cart_id',$cartid)->where('category_id',$produid)->first();
                return $productschu_attr;
                }
 
             public static function getReceptionHospitality($cartid,$produid){
                $productschu_attr = DB::table('nm_cart_reception_hospitality')->where('cart_id',$cartid)->where('product_id',$produid)->first();
                return $productschu_attr;
                }

             public static function getbookinghosinformation($cartid,$produid,$ordertype){
                $productschu_attr = DB::table('nm_cart_reception_hospitality')->where('cart_id',$cartid)->where('category_id',$produid)->where('order_type',$ordertype)->first();
                return $productschu_attr;
                }
                
            public static function bodymesurment($cartid,$produid)
            { 
              $cart_body_measurement = DB::table('nm_cart_body_measurement')->where('cart_id',$cartid)->where('product_id',$produid)->first();
              return $cart_body_measurement;   
            }

            public static function disCountPriceOfProduct($product_id,$product_price,$product_dis_price)
            { 
              $getDiscountPrice = DB::table('nm_product')->where('pro_id',$product_id)->select('pro_price','pro_disprice')->first();
              if(isset($getDiscountPrice->pro_disprice) && $getDiscountPrice->pro_disprice!='')
              {
                 $totalPrice = $getDiscountPrice->pro_price - ($getDiscountPrice->pro_price * ($getDiscountPrice->pro_disprice / 100));
              }
              else
              {
                 $totalPrice = $getDiscountPrice->pro_price;  
              }
              return $totalPrice; 
            }
            
            public static function calculateDiscountedPrice($realprice,$percentagediscount){
                    $totalpriceafterdiscount=($realprice*$percentagediscount)/100;
                    return $totalpriceafterdiscount;
            }
            public static function stafftodaybookingslot($staffid){
            $todaydate=date('Y-m-d');
            $getstaffbookedslot = DB::table('nm_order_services_staff')->where('staff_id',$staffid)->where('booking_date',$todaydate)->get();
             return $getstaffbookedslot;
            }
            public static function disCountPriceOfProductpayable($product_id)
            { 
              $getDiscountPrice = DB::table('nm_product')->where('pro_id',$product_id)->select('pro_price','pro_disprice','Insuranceamount')->first();
              if(isset($getDiscountPrice->pro_disprice) && $getDiscountPrice->pro_disprice!='')
              {
                 $totalPrice = $getDiscountPrice->pro_disprice;
              }
              else
              {
                 $totalPrice = $getDiscountPrice->pro_price;  
              }
               $totalpayPrice=($totalPrice*25)/100;
               $totalpayPrice=$totalpayPrice+$getDiscountPrice->Insuranceamount;
              return $totalpayPrice; 
            }

            public static function getstaffName($s_id) 
            {
               $get_cat_name = DB::table('nm_services_staff')->where('id', $s_id)->first();
               return $get_cat_name;
            }

            public static function getproductimage($par_id) 
            {
               $get_cat_name = DB::table('nm_product')->select('pro_Img')->where('pro_id', $par_id)->first();
               return $get_cat_name;
            }
            
            public static function setExtraProductFieldType($pid)
            {
               $lang  = Session::get('lang_file');
               $mer_lang = Session::get('mer_lang_file');
               if($lang != '' && $lang == 'ar_lang' || $mer_lang == 'mer_ar_lang') 
               {  
                    $getfb = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value','short_name','price','image')->where('id',$pid)->first();
               }
               else
               {
                    $getfb = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value','short_name','price','image')->where('id',$pid)->first();
               }
              return $getfb; 
            }

            public static function setExtraProductField($pid,$vendor_id,$option_id)
            {
               $lang  = Session::get('lang_file');$mer_lang = Session::get('mer_lang_file');
               if($lang != '' && $lang == 'ar_lang' || $mer_lang == 'mer_ar_lang') 
               {  
                    $getfb = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value','short_name','price')->where('product_id',$pid)->where('vandor_id',$vendor_id)->where('product_option_id',$option_id)->first();
               }
               else
               {
                    $getfb = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value','short_name','price')->where('product_id',$pid)->where('vandor_id',$vendor_id)->where('product_option_id',$option_id)->first();
               }
              return $getfb; 
            }


            public static function setExtraProductdesignField($pid,$vendor_id,$option_id)
            {
               
                    $getfb = DB::table('nm_order_option_value')->select('product_id','product_option_id','option_title','option_title_ar','value as price')->where('product_id',$pid)->where('order_id',$vendor_id)->where('product_option_id',$option_id)->first();
              
              return $getfb; 
            }


            
            public static function setMorePackage($order_id,$product_id,$option_id)
            {
               
              $getfb = DB::table('nm_order_option_value')->where('order_id',$order_id)->where('product_id',$product_id)->where('product_option_id',$option_id)->first();          
              return $getfb; 
            }

             public static function getfabricnamefororder($pid,$lang)
            {
               
               if($lang != '' && $lang == 'mer_ar_lang') 
               {  
                    $getfb = DB::table('nm_fabric')->select('fabric_name_ar as fabric_name')->where('id',$pid)->first();
               }
               else
               {
                    $getfb = DB::table('nm_fabric')->select('fabric_name')->where('id',$pid)->first();
               }
              return $getfb; 
            }

            public static function fabricname($pid,$short_name)
            {
               $lang  = Session::get('lang_file');
               if($lang != '' && $lang == 'ar_lang') 
               {  
                    $getfb = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value','short_name','price')->where('product_id',$pid)->where('short_name',$short_name)->orderBy('option_title', 'asc')->first();
               }
               else
               {
                    $getfb = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value','short_name','price')->where('product_id',$pid)->where('short_name',$short_name)->orderBy('option_title', 'asc')->first();
               }
              return $getfb; 
            }
            public static function getProduckOptionInfo($pid,$vendorId)
            { 
               $lang  = Session::get('lang_file');
               if($lang != '' && $lang == 'ar_lang') 
               {  
                    $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value','short_name')->where('product_id',$pid)->where('vandor_id',$vendorId)->orderBy('option_title', 'asc')->get();

           foreach($getAttr as $val)
                  {

                   $OT =  $val->option_title;  
                  $Vid =  $vendorId;

                $getInfoc = DB::table('nm_fabric')->where('fabric_name_ar',$OT)->where('status',1)->where('vendor_id',$Vid)->count();
                  if($getInfoc >=1){
                  $getInfo = DB::table('nm_fabric')->where('fabric_name_ar',$OT)->where('vendor_id',$Vid)->where('status',1)->first();
                 
                  $val->fabric_price = $getInfo->price;
                  $val->fabric_img = $getInfo->image;
                  $val->fabric_title = $OT;
                  }else{
                     $val->fabric_price = '';
                  $val->fabric_img = ''; 
                  $val->fabric_title ='';
                  }
                }

               }
               else
               {
                    $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value','short_name')->where('product_id',$pid)->where('vandor_id',$vendorId)->orderBy('option_title', 'asc')->get();


                  foreach($getAttr as $val)
                  {
                  $OT =  $val->option_title;
                  $Vid =  $vendorId;
                  $getInfoc = DB::table('nm_fabric')->where('fabric_name',$OT)->where('status',1)->where('vendor_id',$Vid)->count();
                  if($getInfoc >=1){
                  $getInfo = DB::table('nm_fabric')->where('fabric_name',$OT)->where('vendor_id',$Vid)->where('status',1)->first();
                  $val->fabric_price = $getInfo->price;
                  $val->fabric_img = $getInfo->image;
                  $val->fabric_title = $OT;
                  }
                  else
                  {
                  $val->fabric_price = '';
                  $val->fabric_img = ''; 
                  $val->fabric_title ='';
                  }

                  }
 

               }
              return $getAttr;                         
            }
            
            public static function getPackage($shop_id,$product_id)
            { 
              $getAttr = DB::table('nm_product_to_packeg')->where('shop_id',$shop_id)->where('packege_id',$product_id)->get(); 
              return $getAttr;                         
            }

            public static function getFabricInfo($fabric_id,$vendor_id)
            { 
               $lang      = Session::get('lang_file');
               $dataCount = DB::table('nm_fabric')->where('status',1)->where('id',$fabric_id)->where('vendor_id',$vendor_id)->count();
               if($dataCount > 0)
               {
                 if($lang != '' && $lang == 'ar_lang') 
                 {  
                    $getAttr = DB::table('nm_fabric')->select('id','status','fabric_name_ar as fabric_name','vendor_id','price','image','fabric_desc_ar as fabric_desc')->where('status',1)->where('id',$fabric_id)->where('vendor_id',$vendor_id)->first();
                 }
                 else
                 {
                    $getAttr = DB::table('nm_fabric')->select('id','status','fabric_name','vendor_id','price','image','fabric_desc')->where('status',1)->where('id',$fabric_id)->where('vendor_id',$vendor_id)->first();
                 }
               }
               else
               {
                  $getAttr = '';
               }
              return $getAttr;                         
            }


            public static function getroseotion($pid,$cartid)
            { 
               $lang      = Session::get('lang_file');
               $dataCount = DB::table('nm_cart_option_value')->where('product_id',$pid)->where('cart_id',$cartid)->count();
               if($dataCount > 0)
               {
                 if($lang != '' && $lang == 'ar_lang') 
                 {  
                      $getAttr = DB::table('nm_cart_option_value')->select('option_title_ar as option_title')->where('product_id',$pid)->where('cart_id',$cartid)->get();
                 }
                 else
                 {
                      $getAttr = DB::table('nm_cart_option_value')->select('option_title')->where('product_id',$pid)->where('cart_id',$cartid)->get();
                 }

                  }
               else
               {
                  $getAttr = array();
               }
              return $getAttr;                         
            }



            public static function getFabricName($pid,$vendorId)
            { 
               $lang      = Session::get('lang_file');
               $dataCount = DB::table('nm_product_option_value')->where('product_id',$pid)->where('vandor_id',$vendorId)->count();
               if($dataCount > 0)
               {
                 if($lang != '' && $lang == 'ar_lang') 
                 {  
                      $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value','short_name','discount_price')->where('product_id',$pid)->where('vandor_id',$vendorId)->orderBy('id','asc')->first();
                 }
                 else
                 {
                      $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value','short_name','discount_price')->where('product_id',$pid)->where('vandor_id',$vendorId)->orderBy('id','asc')->first();
                 }
               }
               else
               {
                  $getAttr = '';
               }
              return $getAttr;                         
            }           

            public static function getProduckInfo($pid)
            { 
               $lang  = Session::get('lang_file');
               if($lang != '' && $lang == 'ar_lang') 
               {  
                    $getAttr = DB::table('nm_product')->where('pro_id',$pid)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_desc_ar as pro_desc','pro_mr_id','pro_mc_id','pro_status','notes_ar as notes','pro_price','Insuranceamount','pro_disprice','pro_discount_percentage','food_type','service_hour','deliver_day','attribute_id','packege','pro_Img','pro_netprice')->first();
               }
               else
               {
                    $getAttr = DB::table('nm_product')->where('pro_id',$pid)->select('pro_id','pro_title','pro_title_ar','pro_Img','pro_desc','pro_mr_id','pro_mc_id','pro_status','notes','pro_price','Insuranceamount','pro_disprice','pro_discount_percentage','food_type','service_hour','deliver_day','attribute_id','packege','pro_Img','pro_netprice')->first();
               }
              return $getAttr;                         
            }
            public static function getrentinformation($pid){
                $getAttr = DB::table('nm_product_option_value')->where('product_id',$pid)->whereIn('option_title',['Discount Price', 'Rent'])->get();

                if(isset($getAttr[0]->discount_price) && $getAttr[0]->discount_price>0){
                  $rprice=$getAttr[0]->discount_price;
                }else{
                  $rprice=$getAttr[1]->price;
                }
               
                return $rprice;
            }
            
            public static function getProduckInfoAttribute($pid)
            { 
               $lang  = Session::get('lang_file');
                $mer_lang = Session::get('mer_lang_file');
               if($lang != '' && $lang == 'ar_lang' || $mer_lang == 'mer_ar_lang') 
               {  
                    $getAttr = DB::table('nm_services_attribute')->where('status',1)->where('id',$pid)->select('id','services_id','vendor_id','attribute_title_ar as attribute_title','image','value')->first();
               }
               else
               {
                    $getAttr = DB::table('nm_services_attribute')->where('status',1)->where('id',$pid)->select('id','services_id','vendor_id','attribute_title','image','value')->first();
               }
              return $getAttr;                         
            }
            
            public static function getExtraInfo($pid,$order_id,$cus_id)
            { 
               $getAttr = DB::table('nm_order_reception_hospitality')->where('category_id',$pid)->where('order_id',$order_id)->where('cus_id',$cus_id)->first();
              return $getAttr;                         
            }
            
            public static function getExtraInformationOfProduct($shop_id,$order_id,$cus_id)
            { 
               $getAttr = DB::table('nm_order_reception_hospitality')->where('category_id',$shop_id)->where('order_id',$order_id)->where('cus_id',$cus_id)->first();
              return $getAttr;                         
            }

            public static function getExtraHospitalityInformationOfProduct($shop_id,$order_id,$cus_id,$ordertype)
            { 
                if($ordertype=='design') { $atttype='Design Your Package';}else{ $atttype='Package'; }
               $getAttr = DB::table('nm_order_reception_hospitality')->where('category_id',$shop_id)->where('order_id',$order_id)->where('cus_id',$cus_id)->where('order_type',$atttype)->first();
              return $getAttr;                         
            }

            public static function getStaffNameOne($staffid)
            { 
               $lang  = Session::get('lang_file');
               $mer_lang = Session::get('mer_lang_file');
               if($lang != '' && $lang == 'ar_lang' || $mer_lang == 'mer_ar_lang')              
               {  
                    $getAttr = DB::table('nm_services_staff')->where('status',1)->where('id',$staffid)->select('staff_member_name_ar as staff_member_name')->first();
               }
               else
               {
                    $getAttr = DB::table('nm_services_staff')->where('status',1)->where('id',$staffid)->select('staff_member_name')->first();
               }
              return $getAttr;                         
            }
            


            public static function getProduckOptionCheckRent($pid,$vendorId)
            { 
               $lang  = Session::get('lang_file');
               if($lang != '' && $lang == 'ar_lang') 
               {  
                    $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title_ar as option_title','value')->where('product_id',$pid)->where('option_title','Rent')->where('vandor_id',$vendorId)->first();
               }
               else
               {
                    $getAttr = DB::table('nm_product_option_value')->select('vandor_id','product_id','product_option_id','option_title','value')->where('product_id',$pid)->where('option_title','Rent')->where('vandor_id',$vendorId)->get();
               }
              return $getAttr;                         
            }
              public static function getorderedfromproducttbl2($par_id,$oid)
            { 
               $getChildID = DB::table('nm_product')->where('pro_id',$par_id)->where('order_id',$oid)->get();
              
               return $getChildID;           
            }

             public static function getorderedfromproductprice($par_id,$oid)
            { 
               $getChildID = DB::table('nm_order_product')->where('product_id',$par_id)->where('order_id',$oid)->first();
              
               return $getChildID;           
            }

            public static function getorderedfromcoupanamount($oid)
            { 
               $coupanamount = DB::table('nm_order_product')->where('order_id',$oid)->sum('coupon_code_amount');
              
               return $coupanamount;           
            }


             public static function getorderedfromjewerycoupanamount($oid,$ordersubtype)
            { 
               $coupanamount = DB::table('nm_order_product')->where('order_id',$oid)->where('product_sub_type',$ordersubtype)->sum('coupon_code_amount');
              
               return $coupanamount;           
            }

             public static function getorderedfromtotalamountsum($oid,$ordersubtype)
            { 
               $coupanamount = DB::table('nm_order_product')->where('order_id',$oid)->where('product_sub_type',$ordersubtype)->sum('total_price');
              
               return $coupanamount;           
            }

            public static function getorderedfromproducttbl($par_id)
            { 
               $getChildID = DB::table('nm_product')->where('pro_id',$par_id)->first();
               $ci_name='pro_title';
               if(Session::get('mer_lang_code')!='en')
               {
                  $ci_name='pro_title_ar';
               }
               $attname = $getChildID->$ci_name;
               return $attname;           
            }

            public static function getexternalfoodmainmenu($par_id,$catid)
            { 
                $getChildID = DB::table('nm_services_attribute')->where('id',$par_id)->where('services_id',$catid)->first();        
                return $getChildID;           
            }

            public static function getAttributes($services_id)
            { 
            $mer_id   = Session::get('merchantid');            
            $getChildID = DB::table('nm_services_attribute')->where('vendor_id',$mer_id)->where('services_id',$services_id)->where('status',1)->get();
            return $getChildID;   
            }
            public static function disablebeautyandeleganceproduct($pro_id){
                $servicestaffexpertcount = DB::table('nm_staff_experties')->where('product_id', '=', $pro_id)->count();
                if($servicestaffexpertcount<1){
                  DB::table('nm_product')->where('pro_id', $pro_id)->update(['pro_status'=>0]);
                  $r=1;
                }
                $r=0;
                return $r;
            }

			public static function getNumberOfcart(){
			 $user_id            = Session::get('customerdata.user_id');
			  $getcartID = DB::table('nm_cart')->where('user_id',$user_id)->first();
			  if(isset($getcartID) ){
			  $cartid=$getcartID->id;
			   $getChildID = DB::table('nm_cart_product')->whereNotNull('quantity')->where('cart_id',$cartid)->count();

			   if($getChildID>0){
			    $tot=$getChildID;
			   }else{
			   	$tot=0;
			   }
			   }else{
			    	$tot=0;
			   }
            return $tot;
			}
            public static function ispaymentreuqest($order_id,$product_id)
            { 
            $mer_id   = Session::get('merchantid');            
            $getChildID = DB::table('nm_merchant_payment_history')->where('order_id',$order_id)->where('product_id',$product_id)->where('request_status',1)->count();
            return $getChildID;   
            }

            public static function totalAmount($merchantid,$requestid='')
            {    
              //echo $requestid;die;
              if(empty($requestid) && $requestid=='')
              {
                $countData = DB::table('nm_merchant_payment_history')->where('merchant_id',$merchantid)->count(); 
                if($countData > 0)
                {
                   $getChildID = DB::table('nm_merchant_payment_history')->where('merchant_id',$merchantid)->sum('amount');
                } 
                else
                {
                  $getChildID = ''; 
                }
              }
              else
              {
                $countData = DB::table('nm_merchant_payment_history')->where('merchant_id',$merchantid)->where('request_status',$requestid)->count(); 
                if($countData > 0)
                {
                   $getChildID = DB::table('nm_merchant_payment_history')->where('merchant_id',$merchantid)->whereNotIn('request_status',array($requestid))->sum('amount');
                } 
                else
                {
                  $getChildID = ''; 
                }
              } 
                           
              return $getChildID;   
            }
             
            public static function getcat($services_id)
            { 

            $mer_id   = Session::get('merchantid');            
            $getChildID = DB::table('nm_services_attribute')->where('vendor_id',$mer_id)->where('id',$services_id)->first();
            return $getChildID;   
            }

            public static function getrelateditems($services_id,$shopid)
            {
            $mer_id   = Session::get('merchantid');  
            $data = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('attribute_id',$services_id)->where('packege', '=', 'no')->get();
            return $data;

            }
            //Category Attributes

            public static function getcategory($pro_mcid)
            {
            $catcount = NmServicesAttribute::where('id',$pro_mcid)->count();
            if($catcount > 0) { 
            $catdata = NmServicesAttribute::where('id',$pro_mcid)->first(); 
            $ci_name='attribute_title';
            if(Session::get('mer_lang_code')!='en')
            {

            $ci_name='attribute_title_ar';
            }

            $attname = $catdata->$ci_name;

            return $attname;

            }
            else {
            return '';
            }
            }
            //End Category ATTRIBUTES
 

            //BioGraphy

            public static function getproduct($pro_mcid)
            {
            $catcount = ProductAttribute::where('product_id',$pro_mcid)->count();
            if($catcount>0) { 
            $catdata = ProductAttribute::where('product_id',$pro_mcid)->get(); 
           
            return $catdata;

            }
            else {
            return array();
            }
            }
            //End Biography




            public static function getcontainer($containerid,$dishid)
            { 

            $mer_id   = Session::get('merchantid');            
            $getChildID = DB::table('nm_internal_food_container_price')->where('vendor_id',$mer_id)->where('dish_id',$dishid)->where('container_id',$containerid)->first();
            return $getChildID;   
            }

            public static function getcontainerPrice($productid,$optionid)
            { 

            $mer_id   = Session::get('merchantid'); 
            $getChildIDcount = DB::table('nm_product_to_option_price')->where('product_id',$productid)->where('product_option_value_id',$optionid)->count();
            if($getChildIDcount!='') {          
            $getChildID = DB::table('nm_product_to_option_price')->where('product_id',$productid)->where('product_option_value_id',$optionid)->first();
            return $getChildID; 
            } else {
            return $getChildID = array();
            }  
            }


            public static function getcitylist()
            {                 
            $getChildID = DB::table('nm_city')->where('ci_con_id',10)->get();
            return $getChildID;   
            }
            public static function getcity($cid)
            { 
              $lang     = Session::get('lang_file');
              $mer_lang = Session::get('mer_lang_file');
              if($lang != '' && $lang == 'ar_lang' || $mer_lang == 'mer_ar_lang') 
              {                
                 $getChildID = DB::table('nm_city')->where('ci_id',$cid)->select('ci_id','ci_lati','ci_name_ar as ci_name','ci_name_ar','ci_lati','ci_status')->first();
              }
              else
              {
                 $getChildID = DB::table('nm_city')->select('ci_id','ci_lati','ci_name','ci_lati','ci_status')->where('ci_id',$cid)->first(); 
              }
              return $getChildID;   
            }


               public static function getmerchantcity($cid)
            { 
              $lang     = Session::get('mer_lang_code');
              if($lang != '' && $lang == 'en') 
              {                
                $getChildID = DB::table('nm_city')->select('ci_id','ci_lati','ci_name','ci_lati','ci_status')->where('ci_id',$cid)->first();
              }
              else
              {
                  
                  $getChildID = DB::table('nm_city')->where('ci_id',$cid)->select('ci_id','ci_lati','ci_name_ar as ci_name','ci_lati','ci_status')->first();
              }
              return $getChildID;   
            }

            public static function getbranchmgr($cid)
            {                 
            $getChildID = DB::table('nm_merchant')->where('mer_id',$cid)->first();
            return $getChildID;   
            }


            public static function getmanagerlist()
            {         
              $mer_id   = Session::get('merchantid'); 
              $getMerchants = DB::table('nm_merchant')->where('vendor_parent_id',$mer_id)->get();
              return $getMerchants;   
            }

            public static function getshopname($vids)
            {         
              $getShopname = DB::table('nm_category')->where('mc_id',$vids)->first();
              return $getShopname;   
            }
             public static function getorderedabayaproduct($cid,$proid)
            {                 
            $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_id',$proid)->count();
            if($getChildID>0){
              $getChildID3 = DB::table('nm_order_product')->where('order_id',$cid)->where('product_id',$proid)->get();
            }
            return $getChildID3;   
            }
             public static function getorderedproduct($cid)
            {                 
            $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->first();
            return $getChildID;   
            }

            public static function getorderedproductdetail($cid,$type='',$subtype='')
            {                 
            $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_type',$type)->where('product_sub_type',$subtype)->first();
            return $getChildID;   
            }

            public static function getorderedproductdetailbasicdetails($cid,$type='',$subtype='',$proid='')
            {                 
            $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_type',$type)->where('product_sub_type',$subtype)->where('product_id',$proid)->first();
            return $getChildID;   
            }


            public static function getfoodorderedproducttotal($cid,$subtype)
            {                 
            $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_sub_type',$subtype)->sum('total_price');
            return $getChildID;   
            }

            public static function getfoodorderedproducttotalamountbyshop($cid,$subtype,$shopid)
            {                 
            $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_sub_type',$subtype)->where('category_id',$shopid)->sum('total_price');
            return $getChildID;   
            }

             public static function getorderedproducttailer($cid,$pid)
            {                 
            $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_id',$pid)->first();
            return $getChildID;   
            }
            public static function getorderedproductwithproductid($cid,$product_id)
            {                 
              $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_id',$product_id)->first();
              return $getChildID;   
            }

            public static function getorderedelectronicinvitation($cid,$order_subtype)
            {                 
              $getChildID = DB::table('nm_order_product')->where('order_id',$cid)->where('product_sub_type',$order_subtype)->first();
              return $getChildID;   
            }

            public static function getorderedproductwiseorder($orderid,$category_id,$vendor_id)
            {                 
              $getChildID = DB::table('nm_order_product')->where('order_id',$orderid)->where('merchant_id',$vendor_id)->get();
              return $getChildID;   
            }

        public static function getsingercid($mcid)
        { 
        $mer_id   = Session::get('merchantid');     
    
          $getCategoryIdc = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->count();

        if($getCategoryIdc >=1)
        {
        $getChildID = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->first();
        $ChkID = $getChildID->mc_id;
        }
        else
        { 
        $ChkID = '';
        } 

        return $ChkID; 

        }

            public static function getsingerid($mcid)
            { 

            $mer_id   = Session::get('merchantid');     
            $getChildIDC = DB::table('nm_music')->where('vendor_id',$mer_id)->where('category_id',$mcid)->count();
             

            if($getChildIDC >=1)
            {
            $getChildID = DB::table('nm_music')->where('vendor_id',$mer_id)->where('category_id',$mcid)->first();
            $ChkID = $getChildID->id;
            }
            else
            { $ChkID = '';
            } 
            return $ChkID;   

            }


            public static function getmakeupArtist($mcid) 
            {

            $mer_id   = Session::get('merchantid');     
            $getChildIDC = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->count();
            if($getChildIDC >=1)
            {
            $getChildID = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->first();
            $ChkID = $getChildID->mc_id;
            }
            else
            { $ChkID = '';
            } 
            return $ChkID;   

            }

            public static function getmakeup($mcid) 
            {

            $mer_id   = Session::get('merchantid');     
            $getChildIDC = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->count();
            if($getChildIDC >=1)
            {
            $getChildID = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->first();
            $ChkID = $getChildID->mc_id;
            }
            else
            { $ChkID = '';
            } 
            return $ChkID;   

            }

            public static function getCosha($mcid) 
            {

            $mer_id   = Session::get('merchantid');     
            $getChildIDC = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->count();
            if($getChildIDC >=1)
            {
            $getChildID = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->first();
            $ChkID = $getChildID->mc_id;
            }
            else
            { $ChkID = '';
            } 
            return $ChkID;   

            }

             public static function getTailor($mcid) 
            {

            $mer_id   = Session::get('merchantid');     
            $getChildIDC = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->count();
            if($getChildIDC >=1)
            {
            $getChildID = DB::table('nm_category')->where('vendor_id',$mer_id)->where('parent_id',$mcid)->first();
            $ChkID = $getChildID->mc_id;
            }
            else
            { $ChkID = '';
            } 
            return $ChkID;   

            }

            public static function getuserinfoOrder($uid)
            {
             $data = DB::table('nm_order')->where('order_cus_id',$uid)->first();
             return $data; 
            }
            
            public static function getuserinfoOrderdeatils($uid,$orderid)
            {
             $data = DB::table('nm_order')->where('order_cus_id',$uid)->where('order_id',$orderid)->first();
             return $data; 
            }


            public static function getstaffOrders($orderid,$product_id)
            {
              $userid  = Session::get('customerdata.user_id'); 
              $data = DB::table('nm_order_services_staff')->where('product_type','beauty')->where('cus_id',$userid)->where('order_id',$orderid)->where('service_id',$product_id)->first();
              return $data; 
            }

            public static function getcartinvitationinfo($uid)
            {
             $data = DB::table('nm_cart_invitation')->where('cart_id',$uid)->first();
             return $data; 
            }


            public static function getuserinfo($uid)
            {              
             $data = DB::table('nm_customer')->where('cus_id',$uid)->first();
             return $data; 
            }
            public static function getProductByAttribute($shopid,$attributeid)
            { 
            $data=array();
            $mer_id   = Session::get('merchantid');   

            $count = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('attribute_id',$attributeid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'no')->count(); 

            if($count >=1)
            {
            $data = DB::table('nm_product')->where('pro_mr_id', '=', $mer_id)->where('pro_mc_id', '=', $shopid)->where('attribute_id',$attributeid)->where('pro_type', '=', 'reception_hospitality')->where('packege', '=', 'no')->get();
            }
            else
            { $data = array();
            } 
            return $data; 
            }
            public static function getoptiondiscount($product_option_id=0,$product_option_value_id=0,$attribute_id) 
            {

            $mer_id   = Session::get('merchantid');  
            $getdiscount='';   
            $getdiscount_count = DB::table('nm_product_to_option_discount')->where('product_option_id',$product_option_id)->where('product_option_value_id',$product_option_value_id)->where('attribute_id',$attribute_id)->where('vendor_id',$mer_id)->count();
            if($getdiscount_count >=1)
            {
            $getdiscount = DB::table('nm_product_to_option_discount')->where('product_option_id',$product_option_id)->where('product_option_value_id',$product_option_value_id)->where('attribute_id',$attribute_id)->where('vendor_id',$mer_id)->first();
            }
            $getdiscountdata=array('getdiscount'=>$getdiscount,'getdiscount_count'=>$getdiscount_count);
            return $getdiscountdata; 
            }


            public static function getbeautyCat($hid)
            {
            $mer_id   = Session::get('merchantid');  
            $getAttr = DB::table('nm_services_attribute')->where('status',1)->where('vendor_id', '=', $mer_id)->where('services_id', '=', $hid)->get();
            return $getAttr;
            }


            public static function getCountry()
            {
                  $mer_id   = Session::get('merchantid'); 
                  $lang     = Session::get('lang_file');


                if(isset($mer_selected_lang_code) && $mer_selected_lang_code !=''){
                if($mer_selected_lang_code !='en')
                {
                $lang     = 'ar_lang';
                }



                }
                  if($lang != '' && $lang == 'ar_lang') 
                  {  
                    $getAttr = DB::table('nm_country')->select('co_id','co_name_ar as co_name','co_name_ar')->where('co_status',1)->orderBy('co_name', 'asc')->get();
                  }
                  else
                  {
                    $getAttr = DB::table('nm_country')->select('co_id','co_name','co_name_ar')->where('co_status',1)->orderBy('co_name', 'asc')->get(); 
                  }
                  return $getAttr;
            }


            public static function getworkernationalityCountry($id)
            {
                  $mer_id   = Session::get('merchantid'); 
                  $lang     = Session::get('lang_file');
                
                  if($lang != '' && $lang == 'ar_lang') 
                  {  
                    $getAttr = DB::table('nm_country')->select('co_id','co_name_ar as co_name')->where('co_id',$id)->first();
                  }
                  else
                  {
                    $getAttr = DB::table('nm_country')->select('co_id','co_name')->where('co_id',$id)->first(); 
                  }
                  return $getAttr;
            }





            public static function getCityb($cid)
            { 
            $getAttr = DB::table('nm_city')->where('ci_con_id', $cid)->where('ci_status', '1')->orderBy('ci_name', 'asc')->get();
            return $getAttr;

            }

            public static function getWrappingType($str)
            {
            $mer_id   = Session::get('merchantid'); 
             
                  
            $getAttr = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_option_id', $str)->where('status', '1')->orderBy('option_title', 'asc')->get();
            return $getAttr; 
            }
            

            public static function getcontainerinfo($str)
            {
            $mer_id   = Session::get('merchantid');             
                  
            $getAttr = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('id', $str)->where('status', '1')->orderBy('option_title', 'asc')->first();
            return $getAttr; 
            }

            public static function getWrapType($str,$ids)
            {
                  $mer_id   = Session::get('merchantid'); 
            $getAttr = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_id', $str)->where('product_option_id', $ids)->first();           

            return $getAttr; 
            }
             
            public static function getProAttr($proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $getAttr = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $proid)->where('attribute_title', 'Car Model')->where('attribute_title', 'Car Model')->first();
               return $getAttr; 
            }

            public static function travelattribute($orderid,$proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $getAttr = DB::table('nm_order_product_rent')->where('order_id', $orderid)->where('product_id', $proid)->where('product_type', 'travel')->first();
               return $getAttr; 
            }

            public static function searchDetails($orderid)
            {
               $userid  = Session::get('customerdata.user_id'); 
               $getAttr = DB::table('nm_search_occasion')->where('order_id',$orderid)->where('user_id',$userid)->first();
               return $getAttr; 
            }
            public static function searchorderedDetails($orderid)
            {
               $userid  = Session::get('customerdata.user_id'); 
               $getAttr = DB::table('nm_search_occasion')->where('order_id',$orderid)->first();
               return $getAttr; 
            }
            public static function travelpackagespeople($orderid,$proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $getAttr = DB::table('nm_order_option_value')->where('order_id',$orderid)->where('product_id',$proid)->first();
               return $getAttr; 
            }
          
           public static function hallPaidServiceOrderDetails($orderid,$proid)
           {   
               $lang     = Session::get('lang_file');
               $countData = DB::table('nm_order_option_value')->where('order_id',$orderid)->where('product_id',$proid)->count();
               if($countData >0)
               {
                  if($lang != '' && $lang == 'ar_lang') 
                  {  
                    $getAttr = DB::table('nm_order_option_value')->select('cus_id','product_type','order_id','product_id','product_option_id','product_option_value_id','option_value_title_ar as option_value_title','option_value_price','quantity','value')->where('order_id',$orderid)->where('product_id',$proid)->get();
                  }
                  else
                  {
                    $getAttr = DB::table('nm_order_option_value')->select('cus_id','product_type','order_id','product_id','product_option_id','product_option_value_id','option_value_title','option_value_price','quantity','value')->where('order_id',$orderid)->where('product_id',$proid)->get();
                  }
                 
               }
               else
               {
                 $getAttr = array();
               }
               
               return $getAttr; 
           }

           public static function getProAverage($proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $Averagecount = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $proid)->count();
               if($Averagecount >0) {
                 $Average = DB::table('nm_product_attribute')->where('vendor_id', $mer_id)->where('product_id', $proid)->take(3)->get();

                 return $Average;

               }

            }

public static function orderedproductattribute($proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $durationcount = DB::table('nm_product_attribute')->where('attribute_title', 'Package Duration')->where('product_id', $proid)->first();  return $durationcount;

            }


            //Sell AND Rent

             public static function getProductcheck($proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $avgcecount = DB::table('nm_product_option_value')->where('vandor_id', $mer_id)->where('product_id', $proid)->count();
               return $avgcecount;
                
            }

              public static function getProductsize($proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $sizecount = DB::table('nm_order_services_attribute')->where('product_type','shopping')->where('product_id', $proid)->count();

               if($sizecount >0) {
                 
                 $sizedata = DB::table('nm_order_services_attribute')->where('product_type','shopping')->where('product_id', $proid)->first();  
                  $attrid  = $sizedata->attribute_id;

                


                  $sizeprocount = DB::table('nm_products_size')->where('id',$attrid)->count();
                  if($sizeprocount >0) {
                  $sizepro = DB::table('nm_products_size')->where('id',$attrid)->first();

                     return  $sizepro;

                  }  
                 
                  
              
               }
                
            }
               public static function getorderedProductattr($proid,$ordid)
            {
               $mer_id   = Session::get('merchantid'); 
               $sizecount = DB::table('nm_order_product_rent')->where('product_id', $proid)->where('order_id', $ordid)->count();
               

               if($sizecount >0) {
                 
                 $sizedata = DB::table('nm_order_product_rent')->where('product_id', $proid)->where('order_id', $ordid)->first();  

                     return  $sizedata;

                  }  
                 
                  
              
               }
                
           
            public static function getProductattr($proid)
            {
               $mer_id   = Session::get('merchantid'); 
               $sizecount = DB::table('nm_cart_product_rent')->where('product_id', $proid)->count();
               

               if($sizecount >0) {
                 
                 $sizedata = DB::table('nm_cart_product_rent')->where('product_id', $proid)->first();  

                     return  $sizedata;

                  }  
                 
                  
              
               }
                
            public static function getProductattrdress($proid,$cartid,$cartprodtid)
            {
               $mer_id   = Session::get('merchantid'); 
               $sizecount = DB::table('nm_cart_product_rent')->where('product_id', $proid)->where('cart_id', $cartid)->where('cart_product_id', $cartprodtid)->count();
               

               if($sizecount >0) {
                 
                 $sizedata = DB::table('nm_cart_product_rent')->where('product_id', $proid)->where('cart_id', $cartid)->where('cart_product_id', $cartprodtid)->first();  

                     return  $sizedata;

                  }  
                 
                  
              
               }
         
            
            // Ajit Start
            public static function getCatName($cat_id) 
            {
              $countData = DB::table('nm_category')->where('mc_id', $cat_id)->count();
              if($countData > 0)
              {
                $get_cat_name = DB::table('nm_category')->where('mc_id', $cat_id)->first();
                $set_name = $get_cat_name->mc_name;
              }
              else
              {
                $set_name = '';
              }
              
              return $set_name;
            }
            
            public static function getShippingMethodName($shipping_id) 
            { 
                $countData = DB::table('nm_shipping_descriptions')->where('shipping_id',$shipping_id)->where('status',1)->count();
                if($countData > 0)
                {
                   $get_cat = DB::table('nm_shipping_descriptions')->where('shipping_id',$shipping_id)->where('status',1)->first();
                   $setdata = $get_cat->shipping;                   
                }
                else
                {
                   $setdata = '';      
                }                   
                return $setdata;
            }

            public static function getProductName($product_id=0,$vendoer_id=0,$title='')
            {
               $lang = Session::get('lang_file');
               $countData = DB::table('nm_product_attribute')->where('vendor_id',$vendoer_id)->where('product_id',$product_id)->where('attribute_title',$title)->count();
               if($countData > 0)
               {
                 if($lang != '' && $lang == 'ar_lang') 
                 { 
                   $getProductAttr = DB::table('nm_product_attribute')->where('vendor_id',$vendoer_id)->where('product_id', $product_id)->where('attribute_title',$title)->select('attribute_title','attribute_title_ar','value_ar as value')->first();
                 }
                 else
                 {
                   $getProductAttr = DB::table('nm_product_attribute')->where('vendor_id',$vendoer_id)->where('product_id', $product_id)->where('attribute_title',$title)->select('attribute_title','attribute_title_ar','value','value_ar')->first();
                 }
               }
               else
               {
                $getProductAttr = '';
               }
               return $getProductAttr; 
            }
            
            public static function getMusicInfo($id)
            {
               $lang = Session::get('lang_file');

               $issinger=DB::table('nm_music')->where('id',$id)->count();
               if($issinger>0){
                 if($lang != '' && $lang == 'ar_lang') 
                 { 
                   $getProductAttr = DB::table('nm_music')->where('id',$id)->select('name_ar as name','address_ar as address','image','google_map_url')->first();
                 }
                 else
                 {
                   $getProductAttr = DB::table('nm_music')->where('id',$id)->select('name','address','image','google_map_url')->first();
                 }   
                 }else{
                  $getProductAttr=array();
                 }         
               return $getProductAttr; 
            }

            public static function getpackagedropdownName($product_id=0,$vendoer_id=0,$title='')
            {               
               $getProductAttr = DB::table('nm_product_attribute')->where('vendor_id',$vendoer_id)->where('product_id', $product_id)->where('attribute_title',$title)->select('attribute_title','attribute_title_ar','value')->first();
            
               return $getProductAttr; 
            }

            public static function getAllProductAttribute($product_id=0,$vendor_id=0)
            {   
               $lang     = Session::get('lang_file');
               if($lang != '' && $lang == 'ar_lang') 
               {              
                $getProductAttr = DB::table('nm_product_attribute')->where('vendor_id',$vendor_id)->where('product_id', $product_id)->select('attribute_title_ar as attribute_title','value')->get();  
               }
               else
               {
                 $getProductAttr = DB::table('nm_product_attribute')->where('vendor_id',$vendor_id)->where('product_id', $product_id)->select('attribute_title','value')->get();  
               }          
                return $getProductAttr; 
            }
              public static function workerratingavarage($workerid){
                 $servicestaffratings = DB::table('nm_review')->where('status', '=', 1)->where('review_type', 'worker')->where('worker_id', '=', $workerid)->avg('ratings'); 
                     $workerratings=ceil($servicestaffratings);
                     return $workerratings;
              }

            public static function getReviewOfUser($id) 
            {
              $getcommentid = DB::table('nm_review')->where('parentId',$id)->select('comment_id','parentId','ratings','comments')->first();
              return $getcommentid;
            }

            public static function business_occasion_type($cat_id) 
            {
              $get_cat_name = DB::table('nm_business_occasion_type')->where('id', $cat_id)->first();
              //$set_name = $get_cat_name->title;
              return $get_cat_name;
            }

            public static function getAllOrderWithService($orderid) 
            {
              $getallorder = DB::table('nm_order_product')->where('order_id',$orderid)->groupBy('product_sub_type')->get();
              return $getallorder;
            }

             public static function productdishdatestype($pro_id) 
            {
              $getdishtype = DB::table('nm_product')->where('pro_id',$pro_id)->first();
              if($getdishtype->attribute_id==34 || $getdishtype->attribute_id==44 ){
                  $iskg='Kg';
              }else{
                   $iskg='';
              }
              return $iskg;
            }
            public static function productdishtype($pro_id) 
            {
              $getdishtype = DB::table('nm_product')->where('pro_id',$pro_id)->where('option_id',21)->count();
              if($getdishtype>0){
                  $iskg='Kg';
              }else{
                   $iskg='';
              }
              return $iskg;
            }

            public static function getAllReview($id) 
            {
              $userid  = Session::get('customerdata.user_id');
              $getallw = DB::table('nm_review')->where('customer_id',$userid)->where('shop_id',$id)->where('parentId',0)->get();
              return $getallw;
            }
            
            public static function getSingleReview($id)
            {             
              $getallw = DB::table('nm_review')->where('comment_id',$id)->first();
              return $getallw;
            }

            public static function getAllOccasionImage($id) 
            {
              $countData = DB::table('nm_occasions_gallery')->where('occasion_id',$id)->count();
              if($countData > 0)
              {
                $get_all_occasion = DB::table('nm_occasions_gallery')->where('occasion_id',$id)->get();
              }
              else
              {
                $get_all_occasion = array();
              }              
              return $get_all_occasion;
            }

            public static function getServiceProviderName($id) 
            {
              $countData = DB::table('nm_merchant')->where('mer_id',$id)->count();
              if($countData > 0)
              {
                $get_Data = DB::table('nm_merchant')->where('mer_id','=',$id)->first();
                $set_vendor_name = $get_Data->mer_fname.' '.$get_Data->mer_lname;
              }
              else
              {
                $set_vendor_name = '';
              }
              
              return $set_vendor_name;

            }

            public static function getOccasionName($id) 
            {
              $countData = DB::table('nm_business_occasion_type')->where('id',$id)->count();
              if($countData > 0)
              {
                  $get_Data = DB::table('nm_business_occasion_type')->where('id',$id)->first();
              }
              else
              {
                 $get_Data = '';
              }
              
              return $get_Data;
            }
            
            public static function getAllOccasionNameList($id) 
            {
              $get_Data = DB::table('nm_business_occasion_type')->where('top_category',$id)->orderBy('title','ASC')->get();
              return $get_Data;
            }

            public static function getOrderDetail($id) 
            {
              $get_Data = '';
              if(isset($id) && $id!='')
              {
                $get_Data = DB::table('nm_order')->where('order_id',$id)->first();              
              }

              return $get_Data;
            }  




            public static function checkPaidStatus($catID) 
            {
              $mer_id   = Session::get('merchantid');  
              $getC = DB::table('nm_merchant_payment_subscription')->whereDate('subscription_end_date', '>=', date('Y-m-d'))->where('category_id',$catID)->where('status',2)->count();            
                  if($getC >= 1)
                  {
                      return 1;
                  } 
                  else
                  {
                    return 0;
                  }      
            }  

         public static function checkinnerfood($hid)
         {    
                    $getC =   DB::table('nm_product')->select('food_type')->where('pro_id',$hid)->first();
                    $foodtype = $getC->food_type;
                    if($foodtype != 'external')
                    {
                    return 1;
                    } 
                    else
                    {
                    return 0;
                    }  
            
         }


            public static function getAccess($branchMgrId) 
            {
              $getC = DB::table('nm_category')->where('branch_manager_id',$branchMgrId)->count();
               $chkList = array();
              if($getC >= 1)
              {
                  $get_Data = DB::table('nm_category')->where('branch_manager_id',$branchMgrId)->get();
                  $chkList = array();
                  foreach($get_Data as $vals)
                  {
                  $GetPID = $vals->parent_id;
                  $get_Datas = DB::table('nm_category')->where('mc_id',$GetPID)->first();

                  $GetPIDa = $get_Datas->parent_id;
                  $get_Datass = DB::table('nm_category')->where('mc_id',$GetPIDa)->first();
                  $final = $get_Datass->mc_id;
                  array_push($chkList, $final);
                  }
              }           
              return $chkList;
            }  

            public static function getallimagelist($id,$vendor_id)
            {                 
              $getallimage = DB::table('nm_music_gallery')->where('music_id',$id)->where('vendor_id',$vendor_id)->get();
              return $getallimage;   
            }

            public static function getallimageCatlist($id)
            {                 
              $getallimage = DB::table('nm_category_gallery')->select('image')->where('category_id',$id)->get();
              return $getallimage;   
            } 

            public static function getProductByAttributeValue($id)
            {  
                $getProductAttributecount = DB::table('nm_product_option_value')->where('product_id',$id)->orderBy('id','ASC')->count();
                if($getProductAttributecount > 0)
                { 
                  $getProductAttribute = DB::table('nm_product_option_value')->where('product_id',$id)->orderBy('id','ASC')->get();
                  return $getProductAttribute; 
                } 
                else
                {
                    $getProductAttribute =array();
                   return $getProductAttribute; 
                }
                
            }

  public static function getNationID($WP)
  { 
    $countrydata = DB::table('nm_country')->where('co_id',$WP)->first();
    if(Session::get('lang_code')=='ar' || Session::get('mer_lang_file')=='mer_ar_lang')
    {
       $workerprice = $countrydata->co_name_ar;
    }
    else
    {
       $workerprice = $countrydata->co_name;    
    }
    return $workerprice;
  }

public static function getworkerprice($WP)
{     
      $mer_id   = Session::get('merchantid');
      $countrydataC = DB::table('nm_service_staff_nationality')->where('vendor_id',$mer_id)->where('nation_id',$WP)->count();
if($countrydataC >=1)
{

      $countrydata = DB::table('nm_service_staff_nationality')->where('vendor_id',$mer_id)->where('nation_id',$WP)->first();
      $workerprice = $countrydata->worker_price;
}
else
{
$workerprice = '';
}

return $workerprice;
}
            public static function detailid($id)
            {                 
              $detid = DB::table('nm_vendor_comment')->where('enq_id',$id)->first();
              return $detid;   
            }   
            public static function quoteRequested($id)
            {                 
              $detid = DB::table('nm_vendor_comment')->where('id',$id)->first();
              return $detid;   
            }  
			public static function quoteRequestedName($id)
            {                 
              $detid = DB::table('nm_services_inquiry')->where('id',$id)->first();
              return $detid;   
            }  
        //GetQuantity

public static function nm_music($id)
            {                 
              $detid = DB::table('nm_music')->where('id',$id)->first();
              return $detid;   
            }  





          public static function getquantity($optiontitle,$proid) {
            $getquant1 = '';
             $mer_id   = Session::get('merchantid'); 

             $getdatacount = DB::table('nm_product_option_value')->where('option_title',$optiontitle)->where('vandor_id',$mer_id)->where('product_option_id',19)->where('product_id',$proid)->count();

 

             if($getdatacount > 0) {

                  $getquant1 = DB::table('nm_product_option_value')->where('option_title',$optiontitle)->where('vandor_id',$mer_id)->where('product_option_id',19)->where('product_id',$proid)->first();
                 
                  
                 
                  return $getquant1;

             } 
 
          }  

          public static function getAllimages($pid)
          {

            $getallimage = DB::table('nm_product_gallery')->select('image')->where('product_id',$pid)->get();
            return $getallimage; 
          }

          public static function getmainimages($pid)
          { 
            $getmainimage = DB::table('nm_product')->select('pro_Img')->where('pro_id',$pid)->get();
            return $getmainimage; 
          }
            public static function getfblinks()
            { 
            $getmainimage = DB::table('nm_setting')->select('url1')->where('url1','!=','')->first();
            return $getmainimage->url1; 
            }
            public static function gettwitterlinks()
            { 
      $getmainimage = DB::table('nm_setting')->select('url2')->where('url2','!=','')->first();
            return $getmainimage->url2; 
            }
            public static function getgooglelinks()
            { 
       $getmainimage = DB::table('nm_setting')->select('url3')->where('url3','!=','')->first();
            return $getmainimage->url3; 
            }


            public static function getinstagramlinks()
            { 
        $getmainimage = DB::table('nm_setting')->select('url4')->where('url4','!=','')->first();
            return $getmainimage->url4; 
            }
            public static function getpinterestlinks()
            { 
            $getmainimage = DB::table('nm_setting')->select('url5')->where('url5','!=','')->first();
            return $getmainimage->url5; 
            }
            public static function getyoutubelinks()
            { 
           $getmainimage = DB::table('nm_setting')->select('url6')->where('url6','!=','')->first();
            return $getmainimage->url6; 
            }

            public static function getphone()
            { 
           $getmainimage = DB::table('nm_setting')->select('phone_no')->where('phone_no','!=','')->first();
            return $getmainimage->phone_no; 
            }

            public static function getSettingaddress()
            { 
           $getmainimage = DB::table('nm_setting')->select('address')->where('address','!=','')->first();
            return $getmainimage->address; 
            }
            public static function getSettingemail()
            { 
           $getmainimage = DB::table('nm_setting')->select('email')->where('email','!=','')->first();
            return $getmainimage->email; 
            }    

            public static function getshippingInfo($proIDD,$cart_sub_type,$cart_type)
            {
 
            $getProInfo = DB::table('nm_product')->where('pro_id', $proIDD)->first();
       
            $pro_mc_cid = $getProInfo->pro_mc_id;
            $merchantID = $getProInfo->pro_mr_id;

            $getCatInfo = DB::table('nm_category')->where('mc_id', $pro_mc_cid)->first();
              $parent_id = $getCatInfo->parent_id;

            $getCatInfoaa = DB::table('nm_category')->where('mc_id', $parent_id)->first();
             $parent_ida = $getCatInfoaa->parent_id;

             $getCatInfoahu= DB::table('nm_category')->where('mc_id', $parent_ida)->first();
             $parent_idas = $getCatInfoahu->parent_id;

    $pick =0; $aramax = 0;

    $getInfo = DB::table('nm_service_shipping')->where('vendor_id', $merchantID)->count();
    if($getInfo >=1)
    {
            $getShippingMethod = DB::table('nm_service_shipping')->where('vendor_id', $merchantID)->first();
             
                if($cart_sub_type=='buffet')
                {

                $Buffet = explode(",", $getShippingMethod->Buffet); 
                if(isset($Buffet[0]) && $Buffet[0] !='')
                {
                if($Buffet[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Buffet[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Buffet[1]) && $Buffet[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }


                if($cart_sub_type=='dessert')
                {

                $Desert = explode(",",  $getShippingMethod->Desert);
                if(isset($Desert[0]) && $Desert[0] !='')
                {
                if($Desert[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Desert[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Desert[1]) && $Desert[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }
                if($cart_sub_type=='dates')
                {

                $Dates = explode(",",  $getShippingMethod->Dates); 
                if(isset($Dates[0]) && $Dates[0] !='')
                {
                if($Dates[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Dates[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Dates[1]) && $Dates[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }

                if($cart_sub_type=='cosha')
                {


                $Cosha = explode(",",  $getShippingMethod->Cosha); 
                if(isset($Cosha[0]) && $Cosha[0] !='')
                {
                if($Cosha[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Cosha[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Cosha[1]) && $Cosha[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }
                if($cart_sub_type=='roses')
                {


                $Roses = explode(",",  $getShippingMethod->Roses);
                if(isset($Roses[0]) && $Roses[0] !='')
                {
                if($Roses[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Roses[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Roses[1]) && $Roses[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='events')
                {

                $Special_Events = explode(",",  $getShippingMethod->Special_Events);
                if(isset($Special_Events[0]) && $Special_Events[0] !='')
                {
                if($Special_Events[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Special_Events[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Special_Events[1]) && $Special_Events[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }



                if($cart_sub_type=='makeup')
                {

                $Makeup_Product = explode(",",  $getShippingMethod->Makeup_Product); 
                if(isset($Makeup_Product[0]) && $Makeup_Product[0] !='')
                {
                if($Makeup_Product[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Makeup_Product[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Makeup_Product[1]) && $Makeup_Product[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_type=='music')
                {

                $Acoustics = explode(",",  $getShippingMethod->Acoustics); 
                if(isset($Acoustics[0]) && $Acoustics[0] !='')
                {
                if($Acoustics[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Acoustics[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Acoustics[1]) && $Acoustics[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='tailor')
                {

                $Tailors = explode(",",  $getShippingMethod->Tailors); 
                if(isset($Tailors[0]) && $Tailors[0] !='')
                {
                if($Tailors[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Tailors[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Tailors[1]) && $Tailors[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='dress')
                {

                $Dresses = explode(",",  $getShippingMethod->Dresses);
                if(isset($Dresses[0]) && $Dresses[0] !='')
                {
                if($Dresses[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Dresses[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Dresses[1]) && $Dresses[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }
                if($cart_sub_type=='abaya')
                {

                $Abaya = explode(",",  $getShippingMethod->Abaya); 
                if(isset($Abaya[0]) && $Abaya[0] !='')
                {
                if($Abaya[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Abaya[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Abaya[1]) && $Abaya[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }
                if($cart_sub_type=='perfume')
                { 
                $Oud_and_Perfumes = explode(",",  $getShippingMethod->Oud_and_Perfumes); 
                if(isset($Oud_and_Perfumes[0]) && $Oud_and_Perfumes[0] !='')
                {
                if($Oud_and_Perfumes[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Oud_and_Perfumes[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Oud_and_Perfumes[1]) && $Oud_and_Perfumes[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                } 
 
                if(trim($cart_sub_type) == 'gold')
                {
 
                $Gold_and_Jewellery = explode(",",  $getShippingMethod->Gold_and_Jewellery);
                if(isset($Gold_and_Jewellery[0]) && $Gold_and_Jewellery[0] !='')
                {
                if($Gold_and_Jewellery[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Gold_and_Jewellery[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Gold_and_Jewellery[1]) && $Gold_and_Jewellery[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


}
         

return $aramax.'~~'.$pick;
 
            

            }






/* CHECK IF ANY SHOP HAS PRODUCT START */

// public static function checkbranchAvilable($shopId, $cityId)
// {

//    /* if shop not any branch */ 
//         $shopIds = array();
//         $Chkcategory = DB::table('nm_category')->select('mc_id')->where('parent_id', $shopId)->Where('city_id', '=', $cityId)->where('mc_status', 1)->get();
//         foreach($Chkcategory as $chkcat)
//         {
//         $shopId = $chkcat->mc_id;
//         $ChkProductCount = DB::table('nm_product')->where('pro_mc_id', $shopId)->where('pro_status', 1)->count();
//         if($ChkProductCount >= 1)
//         {
//         array_push($shopIds, $shopId);
//         }
//         }

//     /* if shop not any branch end */
// return $shopIds;
// }

public static function checkCarAvilability($carid,$startdate,$enddate)
{
// pro_qty
  $getTotalQty = DB::table('nm_product')->select('pro_qty')->where('pro_id', $carid)->count(); 

  $getTotalbookedQty = DB::table('nm_order_product_attribute')->where('product_type', 'car_rental')->where('pro_id', $carid)->where('attribute_title','bookingdate')->where('value','>=',$startdate)->where('value','>=',$startdate)->count();
}


public static function bookingClinicLaser($userid,$order_id,$product_id)
{
  $countData = DB::table('nm_order_services_staff')->where('product_type','clinic')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$product_id)->count();
  if($countData >0)
  {
  $moredetail = DB::table('nm_order_services_staff')->where('product_type','clinic')->where('cus_id',$userid)->where('order_id',$order_id)->where('service_id',$product_id)->first();
  }
  return $moredetail;
}

public static function bookingCarTravelAgency($userid,$order_id,$product_id)
{
  $countData = DB::table('nm_order_product_rent')->where('product_type','travel')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$product_id)->count();
  if($countData >0)
  {
  $moredetail = DB::table('nm_order_product_rent')->where('product_type','travel')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$product_id)->first();
  }
  return $moredetail;
}

public static function bookingCarRental($userid,$order_id,$product_id)
{
  $countData = DB::table('nm_order_product_attribute')->where('product_type','car_rental')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$product_id)->count();
  if($countData >0)
  {
  $moredetail = DB::table('nm_order_product_attribute')->where('product_type','car_rental')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$product_id)->get();
  }
  return $moredetail;
}

public static function ordredCarRental($userid,$order_id,$product_id)
{
  $countData = DB::table('nm_order_product_rent')->where('product_type','car_rental')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$product_id)->count();
  if($countData >0)
  {
  $moredetail = DB::table('nm_order_product_rent')->where('product_type','car_rental')->where('cus_id',$userid)->where('order_id',$order_id)->where('product_id',$product_id)->first();
  }
  return $moredetail;
}

public static function checkShopAvilabe($maincatId, $cityId)
{
 
        $shopIds = array();
        $noBranch = array(10,12,127,14,15,17,20,22,23,27,32,34,36,37,87);
        if(in_array($maincatId, $noBranch))
        {
                 /* if shop not any branch */ 
                $Chkcategory = DB::table('nm_category')->select('mc_id')->where('parent_id', $maincatId)->Where('city_id', '=', $cityId)->get();
                foreach($Chkcategory as $chkcat)
                {
                $shopId = $chkcat->mc_id;
                $ChkProductCount = DB::table('nm_product')->where('pro_mc_id', $shopId)->where('pro_status', 1)->count();
                if($ChkProductCount >= 1)
                {
                DB::table('nm_category')->where('mc_id', $shopId)->update(['mc_status'=>1]);   
                //array_push($shopIds, $shopId);
                }
                else
                {
                DB::table('nm_category')->where('mc_id', $shopId)->update(['mc_status'=>0]);   
                }
                }

                /* if shop not any branch end */

        }
        else
        {
 
                /* if shop have any branch */
                $Chkcategory = DB::table('nm_category')->select('mc_id')->where('parent_id', $maincatId)->where('mc_status','=',1)->get();
                
                foreach($Chkcategory as $chkcat)
                {
                    $shopId = $chkcat->mc_id;
                    $CheckShopStatus = 0;
                    $Chkcategorys = DB::table('nm_category')->select('mc_id')->where('city_id', $cityId)->where('parent_id', $shopId)->where('mc_status', 1)->get();
                  
                    foreach($Chkcategorys as $chkcats)
                    {
                         $lastCat = $chkcats->mc_id; 
                        $ChkProductCount = DB::table('nm_product')->where('pro_mc_id', $lastCat)->where('pro_status', 1)->count();                
                        if($ChkProductCount >= 1)
                        {
                        DB::table('nm_category')->where('mc_id', $lastCat)->update(['mc_status'=>1]);
                        $CheckShopStatus = $CheckShopStatus +1;
                        }
                        else
                        {                  
                        DB::table('nm_category')->where('mc_id', $lastCat)->update(['mc_status'=>0]);
                        }
                    }
     
                        if($CheckShopStatus >=1)
                        {
                        // if shop has active products and active branch then open shop
                        DB::table('nm_category')->where('mc_id', $shopId)->update(['mc_status'=>1]);     
                        }
                        else
                        {
                        // if shop has all in-active products and in-active branch then close shop
                        DB::table('nm_category')->where('mc_id', $shopId)->update(['mc_status'=>0]); 
                        }

                }
 
                /* if shop have any branch end */

        } 

        return $shopIds;

}

/* CHECK IF ANY SHOP HAS PRODUCT END */


public static function getgenralinfo($oid)
{

 $oDetils =   DB::table('nm_order')->where('order_id', $oid)->first();

return $oDetils;
}

public static function isbranchopen($bid)
{
$oDetils =   DB::table('nm_category')->where('parent_id', $bid)->where('mc_status',1)->count();
if($oDetils>0){
  $isactivebrnach=1;
}else{
  $isactivebrnach=0;
}
return $isactivebrnach;
}

public static function opentiming($bid)
{
$oDetils =   DB::table('nm_category')->where('mc_id', $bid)->first();
$otime = $oDetils->opening_time;

return $otime;
}
public static function closetiming($bid)
{
$oDetils =   DB::table('nm_category')->where('mc_id', $bid)->first();
$otime = $oDetils->closing_time;
return $otime;
}

public static function getelectronic($sid)
{
  $elect_invite =  DB::table('nm_product')->where('event_type',$sid)->where('pro_mc_id', '=',16)->where('pro_id', '!=',454)->where('pro_status', 1)->where('pro_id', '!=',455)->orderBy('created_at','desc')->get();
  return $elect_invite;
}


public static function getproductNames($pid)
{

  $getProname =  DB::table('nm_product')->where('pro_id',$pid)->first();
  return $getProname->pro_title; 
}

public static function getproducthospitalitytoolNames($pid)
{

  $getProname =  DB::table('nm_product')->where('pro_id',$pid)->first();
  return $getProname; 
}
public static function getMusicName($Artid)
{
 
$getProname =  DB::table('nm_music')->where('id',$Artid)->first();
$lang      = Session::get('lang_file');

if($lang !='ar')
{
   $Namew = $getProname->name; 
}
else
{
 $Namew = $getProname->name;  
}

  
   
return $Namew;
}



static public function getShipping($orderID)
{
$getShippingC =  DB::table('nm_shipping')->where('ship_order_id',$orderID)->count();
if($getShippingC >=1)
{
  $getShipping =  DB::table('nm_shipping')->where('ship_order_id',$orderID)->sum('shipping_price');
    $ShippingAmt = $getShipping;
}
else
{
  $ShippingAmt = 0;
}


return $ShippingAmt;
}


static public function getShippingmyaccount($orderID,$cartsubtype)
{
$getShippingC =  DB::table('nm_shipping')->where('ship_order_id',$orderID)->where('cart_sub_type',$cartsubtype)->count();
if($getShippingC >=1)
{
  $getShipping =  DB::table('nm_shipping')->where('ship_order_id',$orderID)->where('cart_sub_type',$cartsubtype)->sum('shipping_price');
    $ShippingAmt = $getShipping;
}
else
{
  $ShippingAmt = 0;
}


return $ShippingAmt;
}

}
?>
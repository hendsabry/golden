<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Config;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::toUser($request->input('token'));
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['error'=>'Token is Invalid']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['error'=>'Token is Expired']);
            }else{
                return response()->json(['error'=>'Something is wrong']);
            }
        }
        return $next($request);
    }*/



    public function handle($request, Closure $next)
    {
        Config::set('auth.providers.users.model', 'App\User');

        try {

            if (! $user = JWTAuth::parseToken()->authenticate(false, 'provider')) {
                return response()->json(['user_not_found'], 404);
            } else {
                \Auth::loginUsingId($user->id);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(['error' => 'token_expired']);

        } catch (TokenInvalidException $e) {

            return response()->json(['error' => 'token_invalid']);

        } catch (JWTException $e) {

            return response()->json(['error' => 'token_absent'] );

        }

        return $next($request);
    }
}

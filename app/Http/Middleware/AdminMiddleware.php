<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use app\Http\Controllers\Controller;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            /*Get Admin Language*/ 
           $obj = new Controller();  
           $obj->setLanguageLocaleAdmin();  
              
        return $next($request);
    }
}

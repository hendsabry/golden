<?php
namespace App\Http\Controllers;
use DB;
use Session;
use App\Http\Models;
use App\AdminUser;
use App\AdminMenu;
use App\AdminUserGroup;
use Lang;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class AdminUserGroupController  extends Controller {

	public function __construct(){
        parent::__construct();
        // set admin Panel language
        $this->setLanguageLocaleAdmin();
    }
       
       /*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
    /*admin->add brand & its commission*/


		/*Admin user manage*/


    public function manage_user_group(){
  	    if(Session::has('userid')){


			if(Lang::has(Session::get('admin_lang_file').'.BACK_UASR')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_UASR');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER');
			}
	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');
	        $usergrouplist   = AdminUserGroup::all();
           
	        return view('siteadmin.manage_admin_user_group')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('manage_user_group', $usergrouplist);	
        }else{
	        return Redirect::to('siteadmin');
	    }	
    }


    public function add_user_group(){


	    if(Session::has('userid')){
			if(Lang::has(Session::get('admin_lang_file').'.BACK_UASR')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_UASR');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_UASR');
			}	

			$AdminMenu   = AdminMenu::all();	

	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');
	        return view('siteadmin.add_admin_user_group')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('AdminMenu',$AdminMenu);	
	    }else{
	        return Redirect::to('siteadmin');
	    }	
    }




     public function add_user_group_submit(){
	    if(Session::has('userid')){
	        $data = Input::except(array('_token')) ;
	        $rule = array(
	            'group_name'   => 'required',	                      
	        );
	
	        $validator = Validator::make($data,$rule);			
	        if ($validator->fails()){
	            return Redirect::to('add-user-group')->withErrors($validator->messages())->withInput();
	        }else{
               
	            $group_name       = Input::get('group_name');
	            $group_permission        = Input::get('group_permission');
	            

	                $entry = array(
	                    'group_name'       => $group_name,
	                    'group_permission' => implode(',',$group_permission),	                    
	                    );
                        
	                AdminUserGroup::insert($entry);

			if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_INSERTED_SUCCESSFULLY');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_INSERTED_SUCCESSFULLY');
			}	
	                return Redirect::to('admin-user-group')->with('success',$session_message);	

	        }
	    }else{
	        return Redirect::to('siteadmin');
	    }	
    }//function
    
    


    /*admin->edit brand & its commission*/
    public function edit_user_group($usergroupid){
  	    if(Session::has('userid')){
			if(Lang::has(Session::get('admin_lang_file').'.BACK_USER')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_USER');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER');
			}
	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');

	        $usersgroup   = AdminUserGroup::where('id',$usergroupid)->first();       
	        $mainlist   = AdminMenu::whereNotIn('id', explode(',',$usersgroup->group_permission))->get();	        
	        $selectedlist   = AdminMenu::whereIn('id',explode(',',$usersgroup->group_permission))->get();
	
        	        //$users = DB::table('nm_admin')->where('adm_id',$userid)->get();

            return view('siteadmin.edit_user_group')->with('adminheader', $adminheader)->with('adminleftmenus', $adminleftmenus)->with('adminfooter', $adminfooter)->with('usergroup', $usersgroup)->with('mainlist', $mainlist)->with('selectedlist', $selectedlist);	
        }else{
	        return Redirect::to('siteadmin');
	    }	
    }





       public function edit_user_group_submit(){
		if(Session::has('userid')){
			$data = Input::except(array('_token')) ;
			$id   = Input::get('admin_user_group_id');
			$rule = array(
					'group_name'   => 'required',	
					);
	
			$validator = Validator::make($data,$rule);			
			if ($validator->fails()){
				return Redirect::to('edit-user-grou/'.$id)->withErrors($validator->messages())->withInput();
			}else{



	              $group_name       = Input::get('group_name');
	              $group_permission        = Input::get('group_permission');


				        $entry = array(
							'group_name'=>$group_name,
							'group_permission'=> implode(',',$group_permission),
	                   
						);

						DB::table('admin_user_group')
			            ->where('id',$id)
			            ->update($entry);

				if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_UPDATED_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UPDATED_SUCCESSFULLY');
				}	
 			    return Redirect::to('admin-user-group')->with('success',$session_message);	
		    } //else
	} //if session has
	else{
		return Redirect::to('siteadmin');
	}	
  } //function

	/*delete banner in admin*/
    	public function delete_user_group_submit($group_id){
	    if(Session::has('userid')){
			if(Lang::has(Session::get('admin_lang_file').'.BACK_USER')!= '') 
			{ 
				$session_message = trans(Session::get('admin_lang_file').'.BACK_USER');
			}  
			else 
			{ 
				$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_USER');
			}
	        $adminheader    = view('siteadmin.includes.admin_header')->with("routemenu",$session_message);	
	        $adminleftmenus	= view('siteadmin.includes.admin_left_menus');
	        $adminfooter    = view('siteadmin.includes.admin_footer');
 				 AdminUserGroup::where('id',$group_id)->delete();;
	        //DB::table('nm_admin')->where('adm_id', $user_id)->delete();

 				 if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_DELETE_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_DELETE_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_DELETE_SUCCESSFULLY');
				}	
				return Redirect::to('admin-user-group')->with('success',$session_message);	

	      
	    }else{
	        return Redirect::to('siteadmin');
	    }	
    }

    public function update_user_group_status($id,$status){
    	if(Session::has('userid')){

    		AdminUserGroup::where('id',$id)
            ->update(['status'=>$status]);

			if ($status == 0) {
				if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_UNBLOCKED_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_UNBLOCKED_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_UNBLOCKED_SUCCESSFULLY');
				}	
                return Redirect::to('admin-user-group')->with('success', $session_message);
            }else if ($status == 1) {
				if(Lang::has(Session::get('admin_lang_file').'.BACK_RECORD_BLOCKED_SUCCESSFULLY')!= '') 
				{ 
					$session_message = trans(Session::get('admin_lang_file').'.BACK_RECORD_BLOCKED_SUCCESSFULLY');
				}  
				else 
				{ 
					$session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_RECORD_BLOCKED_SUCCESSFULLY');
				}	
                return Redirect::to('admin-user-group')->with('success', $session_message);
            }
		}else{
			return Redirect::to('siteadmin');
		}	
    }

   
    



} //class

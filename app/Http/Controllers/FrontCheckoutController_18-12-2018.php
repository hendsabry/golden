<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Category;
use App\Products;
use App\ServiceAttribute;
use App\CategoryGallery;
use App\ProductAttribute;
use App\ProductOption;
use App\ProductOptionValue;
use App\ProductPackage;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartProductRent;
use App\CartInvitation;
use App\CartServiceAttribute;
use App\CartHospitality;
use App\StaffNationality;
use Auth;
use App\User;
use Response;
use App\Formbusinesstype;
use Illuminate\Support\Facades\Hash; 
use App\CustomerShipping; 
use App\CartInternalFoodDish;
use App\CartServiceStaff;
use App\CartExternalFoodDish;
use App\CartBodyMeasurement;
use App\CartProductAttribute;
use App\ShippingDescriptions;
use App\OrderProduct;
use App\OrderSingleProduct;
use App\OrderOption;
use App\OrderOptionValue;
use App\OrderProductRent;
use App\OrderServiceAttribute;
use App\OrderServiceStaff;
use App\OrderHospitality;
use App\OrderInvitation;
use App\OrderInternalFoodDish;
use App\OrderExternalFoodDish;
use App\OrderBodyMeasurement;
use App\OrderProductAttribute;
use App\SearchOccasion;
use App\Currency;
use Exception;
use Mail;
use Storage;
use App\Country;
use App\City;
use App\InsuranceAmount;

//------------wisitech code end-----------//
class FrontCheckoutController extends Controller
{  
		public function __construct(){
		parent::__construct();
		$this->middleware(function ($request, $next) {
		$this->setLanguageLocaleFront();
		return $next($request);
		});      
		}

		public function checkout(Request $request)
		{

                $Cur = Session::get('currency');
                if(Session::get('searchdata.basecategoryid')!='')
                {
                  $basecategory_id = Session::get('searchdata.basecategoryid');
                }
                else 
                {
                  $basecategory_id=Session::get('searchdata.maincategoryid');
                }
                $lang = Session::get('lang_file');
                $Formbusinesstype= new Formbusinesstype();
                $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
                $Users = Session::get('customerdata'); 
                $UserID =  $Users['user_id'];
                $User = User::find($UserID);
                $getcustomershippingaddress = DB::table('nm_customer_shipping_details')->where('customer_id', $UserID)->first();
                $getCountry = Country::where('co_status', 1)->orderby('co_name')->get();
            try {
                $Users = Session::get('customerdata'); 
                $UserID =  $Users['user_id'];
                $User = User::find($UserID);
                // $User = Auth::user();
                $cart = Cart::where('user_id', '=', $User->cus_id)->first();           //get user's detail
                if (!empty($cart)) {                    

                if ($lang == 'ar_lang') {
                /* FOR ARABIC LANGUAGE */                     
                $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->where('showcartproduct',0)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_Img,pro_price,Insuranceamount,pro_qty')->with('getCategory:mc_id,mc_name_ar as mc_name,mc_img')->orderBy('updated_at', 'DESC')->distinct()->get(); //get user's cart detail

                } else {
                /*FOR ENGLISH LANGUAGE*/ 

                $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->where('showcartproduct',0)->with('getProduct:pro_id,pro_title,pro_Img,pro_price,Insuranceamount,pro_mc_id,pro_qty')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->distinct()->get();
           
                }     
                $getCartcoupon = CartProduct::where('cart_id', '=', $cart->id)->groupBy('coupon_code')->where('coupon_code','!=','')->get();


                if(count($cart_pro) < 1)
                {
                 return redirect()->route('index')->with('message','Your cart is empty');
                }
 
                $cartproShip = CartProduct::where('cart_id', '=', $cart->id)->groupBy('shop_vendor_id')->get();

                //Check product remaining qty       
                $ProIds = array();
                $ExtraQty =  array();
                foreach($cart_pro as $cartPro)
                {
                $checkQty = $cartPro->quantity;
                $cart_sub_type = $cartPro->cart_sub_type;
                continue;
                if($cart_sub_type == 'beauty_centers' || $cart_sub_type == 'hall' || $cart_sub_type == 'invitations' || $cart_sub_type == 'travel' || $cart_sub_type == 'car'  || $cart_sub_type == 'cosmetic'   || $cart_sub_type == 'skin'  || $cart_sub_type == 'men_saloon'  || $cart_sub_type == 'spa' || $cart_sub_type == 'makeup_artists'  || $cart_sub_type == 'photography'  || $cart_sub_type == 'videography' || $cart_sub_type == 'hospitality'  || $cart_sub_type == 'tailor'  || $cart_sub_type == 'abaya') {continue; }

                $productid = $cartPro->product_id; 
              
                if($productid!='' && ($checkQty!='' || $checkQty!='0'))
                {
                $getPrototalQty = $cartPro->getProduct[0]->pro_qty;
                // In case of dress and abaya we need to get qty from option section
 
                 $productsize = $cartPro->product_size;
                if($productsize!='')
                {
                 $productOptioncount = ProductOptionValue::where('product_id',$productid)->where('option_title',$productsize)->count();
                if($productOptioncount >=1)
                {
                $productOption = ProductOptionValue::where('product_id',$productid)->where('option_title',$productsize)->first();
                $getPrototalQty = $productOption->value;
                }   
                }
                // In case of dress and abaya we need to get qty from option section
                
  
                    if($getPrototalQty < $checkQty)
                    {
                    array_push($ProIds, $productid);
                    array_push($ExtraQty, $getPrototalQty);
                    }  
                    }
                 }

                if(count($ExtraQty)>=1)
                {
                return Redirect::route( 'mycart' )->with( 'ProIds', $ProIds )->with( 'ExtraQty', $ExtraQty );
                }

                //Check product remaining qty

 


                $lang = 'en';
                $vendorList = $this->cartVendorList($lang);






                return view('checkout.checkout',compact('bussinesstype','getCartcoupon','User','getCountry','getcustomershippingaddress','cart_pro','cartproShip','vendorList'));
                }
                else
                {
                   $cart_pro = array();
                   return redirect()->route('index')->with('message','Your cart is empty');
                }
                
                }
                catch (ModelNotFoundException $e)
                {
                return back()->with('message','Something went wrong');
                }

 
		
		}
 

             

        public function checkoutThanks(Request $request)
         {
            return view('checkout.thank-you');

         }
         public function getcitylist(Request $request)
         {            
            $cntry_id = $request->cntry_id;
            $lang = $request->lang;
                if($lang=='ar') 
                {
                $getCity = City::select('ci_id','ci_name_ar as ci_name')->where('ci_con_id',$cntry_id)->orderby('ci_name')->get();
                $SELECT = 'تحديد';
                }  
                else
                {
                $getCity = City::select('ci_id','ci_name')->where('ci_con_id',$cntry_id)->orderby('ci_name')->get();
                $SELECT = 'Select';     
                }
            $City = '<option value="">'.$SELECT.'</option>';
            foreach ($getCity as $key => $value) {
            $City .= '<option value="'.$value->ci_id.'">'.$value->ci_name.'</option>';
            }
         
            return $City;

         }

         public function getVat(Request $request)
         {
         $country_id = $request->country_id;
         $totalPrice = $request->totalPrice;
         $countrylist = Country::where('co_id', '=', $country_id)->orderBy('co_name', 'ASC')->first();
        $vat = $countrylist->vat;
        $restvatpercentage=100-$vat;
        $Vats = $totalPrice - ($totalPrice * $restvatpercentage)/100;
        return $Vats.'~'.$vat;
         }

         public function checkoutCalc(Request $request)
         {                
           
                $Cur = Session::get('currency');
                $validator =  $request->validate([
                            'uname' => 'required|max:255',
                            'email' => 'required',
                            'phone' => 'required',
                            'gender' => 'required',                            
                            'city' => 'required',
                            'pincode' => 'required',
                            'address1' => 'required', 
                            'payment' => 'required',
                        ]);
                       try 
                        {
  
                       $name = $request->uname;
                       $email = $request->email;
                       $country_code = $request->country_code;
                       $phone = $request->phone;
                       $gender = $request->gender;
                       $dob = $request->dob;
                       $city = $request->city;
                       $vat_tax = $request->vat_tax;
                       $country = $request->country;
                       $pincode = $request->pincode;
                       $address = $request->address1.' '. $request->address2; 
                       $payment_method = $request->payment;
                       $newphonenumber=$country_code.'-'.$phone;


                       

                        if( $payment_method =='1')
                        {
                        $payment_method = 'Credit/Debit Cards';
                        }
                        if( $payment_method =='3')
                        {
                        $payment_method = 'COD'; 
                         }
                         if( $payment_method =='2')
                        {
                        $payment_method = 'Wallet'; 
                         }
 

                       $fulladdress=$address.', '.$city.', '.$country.' - '.$pincode;             
                  
                        $transaction_id = rand(11111,99999);
                        //$order_id = rand(11111,99999);

                        $TotalORd = DB::table('nm_order')->select('order_id')->count();
                        if($TotalORd >=1)
                        {
                        $getlastOrderID = DB::table('nm_order')->select('order_id')->orderBy('id', 'DESC')->first();

                        $lastOrderID = $getlastOrderID->order_id;  
                        $order_id = $lastOrderID + 1;
                        }
                        else
                        {
                        $order_id = 1001;
                        }
         

                        $shipping_id =  $request->shipping;
                        if($shipping_id =='') { $shipping_id =3;}

                        $hall_pending_amount = $request->hall_pending_amount;                     
                        $wallet_amount_deducted = $request->wallet_amount_deducted;
                          $total_amount = $request->total_amount;

 
 
 

                        $payment_status = 1;
                        $vat = $request->vat; 
                          
                if(Session::get('searchdata.basecategoryid')!='')
                {
                  $basecategory_id = Session::get('searchdata.basecategoryid');
                }
                else 
                {
                  $basecategory_id=Session::get('searchdata.maincategoryid');
                }
                $occasion_id  = Session::get('searchdata.weddingoccasiontype');
                $main_occasion_id = Session::get('searchdata.mainselectedvalue');
                if($occasion_id =='')
                {
                $occasion_id = '-'.$main_occasion_id;
                }

                
                $total_member = Session::get('searchdata.noofattendees');
                $budget       = Session::get('searchdata.budget');
                $cityid       = Session::get('searchdata.cityid');
                $occasiondate = Session::get('searchdata.occasiondate');
                $gendertype = Session::get('searchdata.gender');
                $lang         = Session::get('lang_file');
                $Users        = Session::get('customerdata'); 

              
                $UserID       =  $Users['user_id']; 
                $User         = User::find($UserID);
                $user_email   = $User->email;
                $user_name    = $User->cus_name;
                $cart         = Cart::where('user_id',$User->cus_id)->first();
                if(!empty($cart)){   
                $shipping = ShippingDescriptions::select('shipping_id','shipping','delivery_time','description','image','shipping_charge','status')->where('shipping_id','=',$shipping_id)->first();
                $isshippingaddress = DB::table('nm_customer_shipping_details')->where('customer_id', '=', $User->cus_id)->count();
                        $customershipping=array();
                       $customershipping['customer_id']         = $User->cus_id;
                       $customershipping['ship_name']          = $name;
                       $customershipping['ship_address1']    = $address;
                       $customershipping['ship_city_id']    = $city;
                       $customershipping['ship_country']     = $country;
                       $customershipping['ship_postalcode']  = $pincode;
                       $customershipping['ship_phone']   = $newphonenumber;
                if($isshippingaddress<1){
                    
                    $customerinsertshipping = DB::table('nm_customer_shipping_details')->insert($customershipping);

                }else{
                        $customerupdateshipping = DB::table('nm_customer_shipping_details')->where('customer_id',$User->cus_id)->update($customershipping);
                }
 
                if(!empty($shipping))
                {   
                $search_occasion                     = array();
                $search_occasion['order_id']         = $order_id;
                $search_occasion['user_id']          = $User->cus_id;
                $search_occasion['main_cat_id']      = $basecategory_id;
                $search_occasion['occasion_id']      = $occasion_id;
                $search_occasion['total_member']     = $total_member;
                $search_occasion['budget']           = $budget;
                $search_occasion['city_id']          = $cityid;
                $search_occasion['occasion_date']    = $occasiondate;
                $search_occasion['gender']    = $gendertype;
                $product = SearchOccasion::create($search_occasion);


                $order_event                         = array();
                $order_event['order_cus_id']         = $User->cus_id;
                $order_event['order_id']             = $order_id;
                $order_event['transaction_id']       = $transaction_id;
                $order_event['payer_email']          = $User->email; 
                $order_event['payer_phone']          = '+'.$country_code.'-'.$phone;  
                $order_event['order_shipping_add']   = $address;    
                $order_event['shipping_city']        = $city;              
                $order_event['payer_name']           = $User->cus_name;
                $order_event['order_shipping_amt']   = $request->vtotalshipping;
                $order_event['wallet_amount']        = $wallet_amount_deducted;
                $order_event['order_paytype']        = $payment_method;
                $order_event['order_amt']            = $total_amount;
                $order_event['order_status']         = $payment_status;
                $order_event['search_occasion_id']   = $occasion_id;
                $order_event['main_occasion_id']     = $main_occasion_id;                
                $order_event['occasion_date']        = $occasiondate;
                

                if($hall_pending_amount)
                {
                    $order_event['hall_pending_amount'] = $hall_pending_amount;
                }   




                //$order_event['search_occasion_id'] = $occasion_search->occasion_id;
                $order_event['order_taxAmt'] = $vat;
               
                $order_event['vat_tax'] = $vat_tax;


                DB::table('nm_shipping')->where('cart_id', '=', $cart->id)->update(['ship_order_id'=>$order_id]);
  

                OrderProduct::create($order_event); //cart product entry 
                if($wallet_amount_deducted){
                $dta['wallet'] = $User->wallet  - $wallet_amount_deducted;
                User::where('cus_id','=',$User->cus_id)->update($dta);
                 } 
                $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
               

                if(!$cart_pro->isEmpty()){

           
                  foreach ($cart_pro as $key => $value) 
                  {                        
                        if($value->category_id==''){ $catshop=$value->shop_id; }else{ $catshop=$value->category_id;}
                        $Order_product                     = array();
                        $insurance_amount                  = array();
                        $Order_product['cus_id']           = $User->cus_id;
                        $Order_product['order_id']         = $order_id;
                        $Order_product['product_type']     = $value->cart_type;
                        $Order_product['product_sub_type'] = $value->cart_sub_type;                    
                        $Order_product['category_id']      = $catshop;
                        $Order_product['product_id']       = $value->product_id;
                        $Order_product['quantity']         = $value->quantity;
                        $Order_product['total_price']      = $value->total_price;
                        $Order_product['product_size']     = $value->product_size;
                        $Order_product['review_type']      = $value->review_type;
                        $Order_product['merchant_id']      = $value->merchant_id;
                        $Order_product['shop_vendor_id']   = $value->shop_vendor_id;
                        $Order_product['hall_booking_date']= $value->hall_booking_date;                
                        $Order_product['shop_id']          = $value->shop_id;
                        $Order_product['pro_title']        = $value->pro_title;
                        $Order_product['pro_title_ar']     = $value->pro_title_ar;
                        $Order_product['buy_rent']         = $value->buy_rent;
                        $Order_product['pro_desc']         = $value->pro_desc;
                        $Order_product['pro_desc_ar']      = $value->pro_desc_ar;
                        $Order_product['pro_Img']          = $value->pro_Img;
                        $Order_product['pro_price']        = $value->pro_price;
                        $Order_product['shop_id']          = $value->shop_id;
                        if($value->buy_rent=='rent' || $value->hall_booking_date!='')
                        {
                          $Order_product['insurance_amount'] = $value->insurance_amount;
                        }
                        else
                        {
                          $Order_product['insurance_amount'] = 0.00;
                        }
                        
                        $Order_product['paid_total_amount']= $value->paid_total_amount;
                        $Order_product['deliver_day']= $value->deliver_day;
                        $Order_product['showcartproduct'] = $value->showcartproduct;


                        $Order_product['coupon_code']= $value->coupon_code;
                        $Order_product['coupon_code_amount']= $value->coupon_code_amount;
                        $Order_product['price_after_coupon_applied']= $value->price_after_coupon_applied;

                        if(isset($value->quantity) && $value->quantity!='')
                        {
                            $product_qty_id = $value->product_id;
                            $get_product_qty = DB::table('nm_product')->where('pro_id',$product_qty_id)->first();
                       

                        if($value->cart_type != 'car_rental')   
                        {
                        $remainingqty=$get_product_qty->pro_qty - $value->quantity;
                        DB::table('nm_product')->where('pro_id','=',$product_qty_id)->update(['pro_qty'=>$remainingqty]);
                        }

                        

                        if($value->cart_sub_type == 'abaya')   
                        {

 
                        $getPinfo = Products::where('pro_id',$value->product_id)->first();
                        $parent_attributeid = $getPinfo->parent_attribute_id;

//Only readymade qty will deduct

                        if($parent_attributeid ==2)
                        {
                        $getquant1 = DB::table('nm_product_option_value')->where('option_title',$value->product_size)->where('product_id',$value->product_id)->first();                       
                        $remainingqty=$getquant1->value - $value->quantity;


                        DB::table('nm_product_option_value')->where('product_id','=',$product_qty_id)->where('option_title',$value->product_size)->update(['value'=>$remainingqty]);
                        }

                       
                             
                        }

                        if($value->cart_sub_type == 'dress')   
                        {

                        $getquant1 = DB::table('nm_product_option_value')->where('option_title',$value->product_size)->where('product_id',$value->product_id)->first();
                        if($value->buy_rent=='buy'){
                        $remainingqty=$getquant1->value - $value->quantity;
                        DB::table('nm_product_option_value')->where('product_id','=',$product_qty_id)->where('option_title',$value->product_size)->update(['value'=>$remainingqty]);
                                }else{

                            $remainingqty=$getquant1->value_ar - $value->quantity;
                        DB::table('nm_product_option_value')->where('product_id','=',$product_qty_id)->where('option_title',$value->product_size)->update(['value_ar'=>$remainingqty]);

                                }
                        }



                        }

                         if($value->cart_type == 'singer' || $value->cart_type == 'band')   
                        {
                        $product_rc_id = $value->product_id;
                        DB::table('nm_services_inquiry')->where('user_id','=',$User->cus_id)->where('status','=',2)->update(['status'=>3]);
                        } 


                        if(isset($value->fabric_name) && $value->fabric_name!='')
                        {

                          $fabric_id = $value->fabric_name;

                          $get_fabriac = DB::table('nm_fabric')->where('id',$fabric_id)->count(); 
                            if( $get_fabriac >=1){
                            $get_fabric = DB::table('nm_fabric')->where('id',$fabric_id)->first();                          
                            $Order_product['fabric_id']        = $get_fabric->id;
                            $Order_product['fabric_name']      = $get_fabric->fabric_name;
                            $Order_product['fabric_img']       = $get_fabric->image;
                            $Order_product['fabric_price']     = $get_fabric->price; 
                            }
                            else
                            {

                            $Order_product['fabric_id']        = '';
                            $Order_product['fabric_name']      = '';
                            $Order_product['fabric_img']       = '';
                            $Order_product['fabric_price']     = '';
                            }
                     
                        }
                        $proId = 'shipping_'.$value->product_id;  

                        $getShipping = $request->$proId;
                        if($getShipping !=0)
                        {
                        $Order_product['shipping_id'] = 1; 
                        $Order_product['shipping_charge'] = $getShipping;  
                        }
                        else
                        {
                            if($getShipping=='')
                            {
                            $Order_product['shipping_id'] = 3;   
                            }
                            else
                            {
                            $Order_product['shipping_id'] = 2;           
                            }
                        
                        $Order_product['shipping_charge'] = 0;  
                        }



                        $Order_product['currency'] = $Cur;

                        $Order_product['status'] = $value->status;





                        $product = OrderSingleProduct::create($Order_product); //cart product entry 
                        if($value->insurance_amount > 0){

                        $insurance_amount['merchant_id'] = $value->merchant_id;
                        $insurance_amount['product_type'] = $value->cart_type;
                        $insurance_amount['product_id'] = $value->product_id;
                        $insurance_amount['order_id'] =$order_id;
                        $insurance_amount['amount'] = $value->insurance_amount;
                        $insurance_amount['requested_date'] = date('Y-m-d H:i:s');
                        $insurance_amount['transation_id'] = $transaction_id;
                        $insurance_amount['pro_title'] = $value->pro_title;
                        $insurance_amount['pro_title_ar'] = $value->pro_title_ar;
                        $insurance_amount['img'] = $value->pro_Img;
                        $insurance_amount['status'] = 0;
                        $product_insurance = InsuranceAmount::create($insurance_amount); //cart product entry 

                        }
                    }   
                }
 
                $Order_Products_Attribute=CartProductAttribute::where('cart_id', '=', $cart->id)->get();
                // print "<pre>";print_r($Order_Products_Attribute);
                //  die;


 


                    if(!$Order_Products_Attribute->isEmpty()){
                  foreach($Order_Products_Attribute as $key => $value) {
                            $product_attribute = array();
                            $product_attribute['cus_id'] = $User->cus_id;;
                             $product_attribute['order_id'] =$order_id;
                            $product_attribute['product_type'] = $value->cart_type;                            
                            $product_attribute['product_id'] = $value->product_id;
                            $product_attribute['attribute_title'] = $value->attribute_title;
                            $product_attribute['attribute_title_ar'] = $value->attribute_title_ar;
                            $product_attribute['value'] = $value->value;
                           $Order_Products_Attrdibute = OrderProductAttribute::create($product_attribute); //order option entry

                           
                    }  
                }

                $cart_option = CartOption::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_option->isEmpty()){
                  foreach($cart_option as $key => $value) {
                            $product_option = array();
                            $product_option['cus_id'] = $User->cus_id;;
                             $product_option['order_id'] =$order_id;
                            $product_option['product_type'] = $value->cart_type;
                            $product_option['product_option_id'] = $value->product_option_id;
                            $product_option['product_id'] = $value->product_id;
                            OrderOption::create($product_option); //order option entry
                    }  
                }

                $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_option_value->isEmpty()){
                  foreach($cart_option_value as $key => $value) {
                            $product_option_value = array();
                            $product_option_value['cus_id'] = $User->cus_id;;
                            $product_option_value['order_id'] =$order_id;
                            $product_option_value['product_type'] = $value->cart_type;
                            $product_option_value['product_id'] = $value->product_id;
                            $product_option_value['product_option_id'] = $value->product_option_id;
                            $product_option_value['product_option_value_id'] = $value->product_option_value_id;
                            $product_option_value['value'] = $value->value;
                            $product_option_value['quantity'] = $value->quantity;
                            $product_option_value['status'] = $value->status;

                            $product_option_value['option_title'] = $value->option_title;
                            $product_option_value['option_title_ar'] = $value->option_title_ar;
                             ////////////// get container ///////////
                            if($value->cart_type=='hall'){
                          $order_option_value = DB::table('nm_product_option_value')->where('id',$value->product_option_value_id)->first();
                          
                            $product_option_value['option_value_title'] = $order_option_value->option_title;
                            $product_option_value['option_value_title_ar'] = $order_option_value->option_title_ar;
                            $product_option_value['option_value_price'] = $order_option_value->price;
                        }
                            ////////////////// end fetch data section //////////////
                            OrderOptionValue::create($product_option_value); //order option value
                    }  
                }

                $cart_service_attribute = CartServiceAttribute::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_service_attribute->isEmpty()){
                  foreach($cart_service_attribute as $key => $value) {
                            $product_service_attribute = array();
                            $product_service_attribute['cus_id'] = $User->cus_id;
                            $product_service_attribute['order_id'] =$order_id;
                            $product_service_attribute['product_type'] = $value->cart_type;
                            $product_service_attribute['category_id'] = $value->category_id;
                            $product_service_attribute['product_id'] = $value->product_id;
                            $product_service_attribute['attribute_id'] = $value->attribute_id;
                            $product_service_attribute['sub_attribute_id'] = $value->sub_attribute_id;
                            $product_service_attribute['price'] = $value->price;
                            $product_service_attribute['status'] = $value->status;
                                if(isset($value->quantity) && $value->quantity>0){ $mynewqty=$value->quantity; } else{ $mynewqty=0; }
                            $product_service_attribute['quantity'] = $mynewqty;
                            if(isset($value->total_price) && $value->total_price>0){ $mynewtotal_price=$value->total_price; } else{ $mynewtotal_price=0; }
                            $product_service_attribute['total_price'] = $mynewtotal_price;

                            if(isset($value->shop_vendor_id) && $value->shop_vendor_id>0){ $mynewshop_vendor_id=$value->shop_vendor_id; } else{ $mynewshop_vendor_id=0; }


                            


                             $product_service_attribute['attribute_title'] = $value->attribute_title;
                            $product_service_attribute['attribute_title_ar'] = $value->attribute_title_ar;
                            $product_service_attribute['sub_attribute_title'] = $value->sub_attribute_title;

                            $product_service_attribute['sub_attribute_title_ar'] = $value->sub_attribute_title_ar;
                            $product_service_attribute['pro_title'] = $value->pro_title;
                            $product_service_attribute['pro_title_ar'] = $value->pro_title_ar;

                            $product_service_attribute['pro_desc'] = $value->pro_desc;
                            $product_service_attribute['pro_desc_ar'] = $value->pro_desc_ar;
                            $product_service_attribute['pro_img'] = $value->pro_img;


                                $product_service_attribute['shop_vendor_id']           = $mynewshop_vendor_id;
                                $product_service_attribute['shop_id']                = $value->shop_id;
                                $product_service_attribute['worker_price']                = $value->worker_price;
                                 $product_service_attribute['insurance_amount']   = $value->insurance_amount;

                           // print_r($product_service_attribute);
                          $product_service =  OrderServiceAttribute::create($product_service_attribute); //order service attribute entry
                    }  
                }
                 //die;

               $cart_internal_food = CartInternalFoodDish::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_internal_food->isEmpty()){
                  foreach($cart_internal_food as $key => $value) {


                            $product_internal_food = array();
                            $product_internal_food['cus_id'] = $User->cus_id;
                            $product_internal_food['order_id'] =$order_id;
                            $product_internal_food['product_type'] = $value->cart_type;
                            $product_internal_food['product_id'] = $value->product_id;
                            $product_internal_food['internal_food_menu_id'] = $value->internal_food_menu_id;
                            $product_internal_food['internal_food_dish_id'] = $value->internal_food_dish_id;
                            $product_internal_food['container_id'] = $value->container_id;
                            $product_internal_food['quantity'] = $value->quantity;
                            $product_internal_food['price'] = $value->price;
                            $product_internal_food['status'] = $value->status;

                            ////////////// get container ///////////
                          $internal_food_container = DB::table('nm_internal_food_container')->where('id',$value->container_id)->first();
                            $product_internal_food['container_title'] = $internal_food_container->title;
                            $product_internal_food['container_title_ar'] = $internal_food_container->title_ar;
                            $product_internal_food['container_image'] = $internal_food_container->img;
                            ////////////////// end fetch data section //////////////
                            ////////////// get food item ///////////
                          $internal_food_dish = DB::table('nm_internal_food_dish')->where('id',$value->internal_food_dish_id)->first();
                            $product_internal_food['internal_dish_name'] = $internal_food_dish->dish_name;
                            $product_internal_food['internal_dish_name_ar'] = $internal_food_dish->dish_name_ar;
                            $product_internal_food['dish_image'] = $internal_food_dish->dish_image;
                            ////////////////// end fetch data section //////////////
                            
                            OrderInternalFoodDish::create($product_internal_food); //Order internal food entry
                    }  
                }

                $cart_rent_product = CartProductRent::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                
                if(!$cart_rent_product->isEmpty()){
                  foreach($cart_rent_product as $key => $value) {
                                    //if($value->cart_type=='travel'){
                             //$ord_product_id = DB::table('nm_order_product')->where('order_id',$order_id)->where('product_id',$value->cart_type)->first();

                        $ord_product_id = OrderSingleProduct::where('order_id',$order_id)->where('product_id',$value->product_id)->first();

                        if(isset($ord_product_id->id) && $ord_product_id->id!='')
                        {
                        $order_pro_id=$ord_product_id->id;
                        }
                        else
                        { $order_pro_id=0;
                        }
                    
                            
                        ///}else{$order_pro_id='';}
                   
                            $product_rent = array();
                            $product_rent['cus_id'] = $User->cus_id;
                            $product_rent['order_id'] =$order_id;
                            $product_rent['product_type'] = $value->cart_type;
                            $product_rent['service_id'] = $value->service_id;
                            $product_rent['product_id'] = $value->product_id;
                            $product_rent['rental_date'] = $value->rental_date;
                            $product_rent['rental_time'] = $value->rental_time;
                            $product_rent['return_date'] = $value->return_date;
                            $product_rent['return_time'] = $value->return_time;                            
                            $product_rent['quantity'] = $value->quantity;
                            $product_rent['insurance_amount'] = $value->insurance_amount;
                            $product_rent['order_product_id'] = $order_pro_id;
                            OrderProductRent::create($product_rent); //Order rent product entry
                    }  
                }

                $cart_service_staff = CartServiceStaff::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_service_staff->isEmpty()){
                  foreach($cart_service_staff as $key => $value) {
                            $product_service_staff = array();
                            $product_service_staff['cus_id'] = $User->cus_id;
                            $product_service_staff['order_id'] =$order_id;
                            $product_service_staff['product_type'] = $value->cart_type;
                            $product_service_staff['shop_id'] = $value->shop_id;
                            $product_service_staff['service_id'] = $value->service_id;                            
                            $product_service_staff['file_no'] = $value->file_no;
                            $product_service_staff['staff_id'] = $value->staff_id;
                            $product_service_staff['booking_place'] = $value->booking_place;
                            $product_service_staff['booking_date'] = $value->booking_date;                            
                            $product_service_staff['start_time'] = $value->start_time;
                            $product_service_staff['end_time'] = $value->end_time;
                           
                            OrderServiceStaff::create($product_service_staff); //Order service staff entry
                    }  
                }

                $cart_hospitality = CartHospitality::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_hospitality->isEmpty()){
                  foreach($cart_hospitality as $key => $value) {
                            $product_hospitality = array();
                            $product_hospitality['cus_id'] = $User->cus_id;
                            $product_hospitality['order_id'] =$order_id;
                            $product_hospitality['category_id'] = $value->category_id;
                            $product_hospitality['no_of_staff'] = $value->no_of_staff;
                            $product_hospitality['nationality'] = $value->nationality;                            
                            $product_hospitality['date'] = $value->date;
                            $product_hospitality['time'] = $value->time;
                            $product_hospitality['elocation'] = $value->elocation;
                            $product_hospitality['order_type'] = $value->order_type;

                            OrderHospitality::create($product_hospitality); //Order hospitality entry
                    }  
                }

                $cart_invitaion = CartInvitation::where('cart_id',$cart->id)->get(); //get user's cart product detail
                if(!$cart_invitaion->isEmpty()){
                  foreach($cart_invitaion as $key => $value) 
                  {
                            $product_invitation                            = array();
                            $product_invitation['customer_id']             = $User->cus_id;
                            $product_invitation['order_id']                = $order_id;
                            $product_invitation['services_id']             = $value->services_id;
                            $product_invitation['occasion_name']           = $value->occasion_name;
                            $product_invitation['occasion_type']           = $value->occasion_type;                          
                            $product_invitation['venue']                   = $value->venue;
                            $product_invitation['date']                    = $value->date;
                            $product_invitation['time']                    = $value->time;
                            $product_invitation['no_of_invitees']          = $value->no_of_invitees;
                            //$product_invitation['invitation_mode']       = $value->invitation_mode;
                            $product_invitation['invitaion_mode']          = $value->invitaion_mode;
                            $product_invitation['invitation_msg']          = $value->invitation_msg; 
                            $product_invitation['file_url']                = $value->file_url;                         
                            $product_invitation['packge_id']               = $value->packge_id;
                            $product_invitation['total_member_invitation'] = $value->total_member_invitation;
                            $product_invitation['card_price']              = $value->card_price;
                            $product_invitation['status']                  = $value->status;                      
                            $product_invitation['pro_label'] = $value->pro_label;
                            $product_invitation['pro_label_ar'] = $value->pro_label_ar;
                            $product_invitation['packge_img'] = $value->packge_img;
                            $product_invitation['packge_title'] = $value->packge_title;
                            $product_invitation['packge_title_ar'] = $value->packge_title_ar;

                            OrderInvitation::create($product_invitation); //Order Invitation entry
                            $EventDate =  date("Y-m-d",strtotime($value->date));
                            $currentdate = date('Y-m-d H:i:s');
                            if(($handle = fopen($value->file_url,'r'))!== FALSE){
                                while(($data = fgetcsv($handle,1000,',')) !== FALSE){
                                   DB::table('nm_invitation_list')->insert(array('customer_id'=>$User->cus_id,'packge_id'=>$value->packge_id,'invite_name'=>$data[0],'invite_email'=>$data[1],'invite_phone'=>$data[2],'address'=>$User->cus_address1,'created_at'=>$currentdate,'updated_at'=>$currentdate,'order_id'=>$order_id,'occasion_date'=>$EventDate));
                               }
                              fclose($handle);
                            }
                    }
                }

                $cart_external_food = CartExternalFoodDish::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_external_food->isEmpty()){
                  foreach($cart_external_food as $key => $value) {
                            $product_external_food = array();
                            $product_external_food['cus_id'] = $User->cus_id;
                            $product_external_food['order_id'] =$order_id;
                            $product_external_food['category_id'] = $value->category_id;
                            $product_external_food['external_food_menu_id'] = $value->external_food_menu_id;
                            $product_external_food['external_food_dish_id'] = $value->external_food_dish_id;                            
                            $product_external_food['container_id'] = $value->container_id;
                            $product_external_food['quantity'] = $value->quantity;
                            $product_external_food['price'] = $value->price;
                            $product_external_food['status'] = $value->status;

                            OrderExternalFoodDish::create($product_external_food); //Order hospitality entry
                    }  
                } 

                $cart_measurement = CartBodyMeasurement::where('cart_id', '=', $cart->id)->get(); //get user's cart product detail
                if(!$cart_measurement->isEmpty()){
                  foreach($cart_measurement as $key => $value) 
                  {
                            $product_measurement                   = array();
                            $product_measurement['customer_id']    = $User->cus_id;
                            $product_measurement['order_id']       = $order_id;
                            $product_measurement['product_id']     = $value->product_id;
                            $product_measurement['length']         = $value->length;
                            $product_measurement['chest_size']     = $value->chest_size;
                            $product_measurement['waistsize']      = $value->waistsize;                            
                            $product_measurement['soulders']       = $value->soulders;
                            $product_measurement['neck']           = $value->neck;
                            $product_measurement['arm_length']     = $value->arm_length;
                            $product_measurement['wrist_diameter'] = $value->wrist_diameter;
                            $product_measurement['cutting']        = $value->cutting;
                            $product_measurement['status']         = $value->status;

                            OrderBodyMeasurement::create($product_measurement); //Order Measurement entry
                    }  
                }
                $social_url = DB::table('nm_setting')->first();

				if($occasion_id==''){ $occasion_id='N/A'; }
                if(isset($lang) && $lang == 'ar_lang')
                {
                    $locale='ar';
                }
                else
                {
                    $locale='en';
                }
                $send_mail_data = array(
                'user_id' => $User->cus_id,
                'user_name' => $User->cus_name, 
                'user_email' => $User->email,     
                'user_mobile' => $User->cus_phone, 
                'user_address' => $User->cus_address1,
                'user_map_url' => $User->map_url, 
                'order' => $order_id,    
                'occasion' => $occasion_id,
                'getFbLinks' => $social_url->url1,
                'gettwitterLinks' => $social_url->url2, 
                'getgoogleLinks' => $social_url->url3,    
                'getinstagramLinks' => $social_url->url4,
                'getpinterestLinks' => $social_url->url5,    
                'getyoutubeLinks' => $social_url->url6,
                'lang' => 'ar_mob_lang',
                'LANUAGE' => 'en_mob_lang',
                );
               
                 Mail::send('emails.orderusermail', $send_mail_data, function($message)
                 {
                    $adminEmail = DB::table('nm_admin')->where('admin_type',1)->where('status',1)->first();
                    $admin_email = $adminEmail->adm_email;
                    if (Lang::has(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY')!= '')
                    { 
                        $session_message =  trans(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY');
                    }        
                    $message->to('vinay@wisitech.com','Test')->subject($session_message);
                 });
                     



                 Mail::send('emails.orderusermail',$send_mail_data, function($message)
                 {
                    $Users        = Session::get('customerdata'); 
                    $UserID       = $Users['user_id']; 
                    $UserCustomer  = User::find($UserID);
                    $user_email   = $UserCustomer->email;
                    $user_name    = $UserCustomer->cus_name;
                     if(Lang::has(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY')!= '')
                     { 
                         $session_message =  trans(Session::get('lang_file').'.ENQUIRY_SUCCESSFULLY');
                     }                      
                     $message->to($user_email,@$user_name)->subject($session_message);
                 });
                 


                /*$total_product = OrderSingleProduct::select('merchant_id')->where('order_id',$order_id)->groupBy('merchant_id')->get();
                foreach($total_product as $key => $value) 
                {
                   $merchant = DB::table('nm_merchant')->select('mer_id','mer_fname','mer_lname','mer_email')->where('mer_id',$value->merchant_id)->first();
                   if(!empty($merchant))
                   {
                      $this->confirmation_merchant_name = $merchant->mer_fname.' '.$merchant->mer_lname;
                      $this->confirmation_merchant_email = $merchant->mer_email;
                      $subject = 'New Order'; 
                        Mail::send('emails.ordermerchantmail',
                        array(

                        'mer_name' => $merchant->mer_fname.' '.$merchant->mer_lname,
                        'mer_id' => $value->merchant_id, 
                        'user_id' => $User->cus_id,
                        'user_name' => $User->cus_name, 
                        'user_email' => $User->email,     
                        'user_mobile' => $User->cus_phone, 
                        'user_address' => $User->cus_address1,
                        'user_map_url' => $User->map_url, 
                        'order' => $order_id,    
                        'occasion' => $order_id,
                        'lang' => 'ar_mob_lang',
                        'LANUAGE' => 'en_mob_lang',
                        'getFbLinks' => $social_url->url1,
                        'gettwitterLinks' => $social_url->url2, 
                        'getgoogleLinks' => $social_url->url3,    
                        'getinstagramLinks' => $social_url->url4,
                        'getpinterestLinks' => $social_url->url5,    
                        'getyoutubeLinks' => $social_url->url6,
                        ), function($message) use($subject)
                        {
                         $message->to($this->confirmation_merchant_email,$this->confirmation_merchant_name)->subject($subject);
                        });
                   }
               }*/
                           

                $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartOption::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartOptionValue::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartServiceAttribute::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartInternalFoodDish::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartProductRent::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartServiceStaff::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartProductRent::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartHospitality::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartInvitation::where('cart_id', '=', $cart->id)->delete();

                $cart_pro = CartExternalFoodDish::where('cart_id', '=', $cart->id)->delete();

                $cart_pro_rent = CartBodyMeasurement::where('cart_id', '=', $cart->id)->delete();
                $cart = Cart::where('user_id', '=', $User->cus_id)->delete();

                 //IF PAYMENT TYPE IS CARD 

                        if($payment_method == 1)
                        {
                        $productName = 'Order #'.$order_id;
                        $returnURls = url('/').'/order-payment-success';
                        $returnErrorURls = url('/').'/order-payment-error';
                        $paygateway_url = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx";
                        $user = 'testapi@myfatoorah.com';
                        $password = 'E55D0';
                        $code = "999999";
                        $amount = $total_amount;
                        $firstname = $name;
                        $email = $email;
                        $phone = $phone;
                        $productdata = "";
                        $productdata .= '<ProductDC>';
                        $productdata .= '<product_name>'.$productName.'</product_name>';
                        $productdata .= '<unitPrice>' . $amount . '</unitPrice>';
                        $productdata .= '<qty>1</qty>';
                        $productdata .= '</ProductDC>';
                        // Soap xml
                        $post_string = '<?xml version="1.0" encoding="windows-1256"?>
                        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                        <soap12:Body>
                        <PaymentRequest xmlns="http://tempuri.org/">
                          <req>
                            <CustomerDC>
                              <Name>' . $firstname . ' </Name>
                              <Email>' . $email . '</Email>
                              <Mobile>' . $phone . '</Mobile>
                            </CustomerDC>
                            <MerchantDC>
                              <merchant_code>' . $code . '</merchant_code>
                              <merchant_username>' . $user . '</merchant_username>
                              <merchant_password>' . $password . '</merchant_password>
                              <merchant_ReferenceID>' . time() . '</merchant_ReferenceID>
                              <ReturnURL>'.$returnURls.'</ReturnURL>
                              <merchant_error_url>'.$returnErrorURls.'</merchant_error_url>
                            </MerchantDC>
                            <lstProductDC>' . $productdata . '</lstProductDC>
                            <totalDC>
                                <subtotal>' . $amount . '</subtotal>
                                </totalDC>
                                <paymentModeDC>
                            <paymentMode>BOTH</paymentMode>
                            </paymentModeDC>
                            <paymentCurrencyDC>
                              <paymentCurrrency>SAR</paymentCurrrency>
                            </paymentCurrencyDC>
                          </req>
                        </PaymentRequest>
                        </soap12:Body>
                        </soap12:Envelope>';
                        $soap_do = curl_init();
                        curl_setopt($soap_do, CURLOPT_URL, $paygateway_url);
                        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
                        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
                        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($soap_do, CURLOPT_POST, true);
                        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
                        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array(
                        'Content-Type: text/xml; charset=utf-8',
                        'Content-Length: ' . strlen($post_string)
                        ));
                        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
                        $result = curl_exec($soap_do);
                        $err = curl_error($soap_do);
                        $file_contents = htmlspecialchars($result);
                        curl_close($soap_do);
                        $doc = new \DOMDocument();

                        if ($doc != null) {
                        $doc->loadXML(html_entity_decode($file_contents));
                        $ResponseCode = $doc->getElementsByTagName("ResponseCode");
                        $ResponseCode = $ResponseCode->item(0)->nodeValue;
                        $paymentUrl = $doc->getElementsByTagName("paymentURL");
                        $paymentUrl = $paymentUrl->item(0)->nodeValue;
                        $referenceID = $doc->getElementsByTagName("referenceID");
                        $referenceID = $referenceID->item(0)->nodeValue;
                        $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
                        $ResponseMessage = $ResponseMessage->item(0)->nodeValue;
                        }  
                        if($ResponseCode == 0) {
                        $transaction_id = str_replace('=','',strstr($paymentUrl,'='));
                        echo '<script language="javascript">window.location.href="' . $paymentUrl . '";</script>';
                        }

                        die;
                        }
                    //IF PAYMENT TYPE IS CARD 



                  return redirect()->route('thank-you')->with('message','Order placed successfully');
                  
                }   
                } 
                       
            }
            catch (ModelNotFoundException $e)
            {
            return back()->with('message','Something went wrong');
            }
 
    
         }

         public function paymentSuccess(Request $request)
         {
            return redirect()->route('thank-you')->with('message','Order placed successfully');
         }

          public function paymentError(Request $request)
         {
           return redirect()->route('thank-you')->with('message','Something went wrong'); 
         }
         
         

         public function getaramaxrate(Request $request)
         {
        $params = array(
        'ClientInfo'            => array(
                                    'AccountCountryCode'    => 'SA',
                                    'AccountEntity'         => 'DHA',
                                    'AccountNumber'         => '149022',
                                    'AccountPin'            => '116216',
                                    'UserName'              => 'ceo.founder@goldencages.com',
                                    'Password'              => 'Golden-800',
                                    'Version'               => 'v1.0'
                                ),
                                
        'Transaction'           => array(
                                    'Reference1'            => '001' 
                                ),
                                
        'OriginAddress'         => array(
                                    'City'                  => 'Dhahran',
                                    'CountryCode'               => 'SA'
                                ),
                                
        'DestinationAddress'    => array(
                                    'City'                  => 'Dhahran',
                                    'CountryCode'           => 'SA'
                                ),
        'ShipmentDetails'       => array(
                                    'PaymentType'            => 'P',
                                    'ProductGroup'           => 'EXP',
                                    'ProductType'            => 'PPX',
                                    'ActualWeight'           => array('Value' => 1, 'Unit' => 'KG'),
                                    'ChargeableWeight'       => array('Value' => 1, 'Unit' => 'KG'),
                                    'NumberOfPieces'         => 1
                                )
    );
     $Urld = url('/aramex-rates-calculator-wsdl.wsdl');
    $soapClient = new SoapClient($Urld, array('trace' => 1));
    $results = $soapClient->CalculateRate($params); 
 
    print_r($results);
    die();

            
         }
 
public function checkCouponcode(Request $request)
        {
            try
            {
            $valid = 0; $already=0;$delte = 0; $coupnAmt=0;
            $CCode = $request->couopncode; 
            $del = $request->del; 
            $Users = Session::get('customerdata'); 
            $UserID =  $Users['user_id'];
            $User = User::find($UserID);
            $cart = Cart::where('user_id', '=', $User->cus_id)->first();       
            $cart_pro = CartProduct::where('cart_id', '=', $cart->id)->get();
            $curr =  date("Y-m-d");

 

            foreach($cart_pro as $pro)
            {
            $shopVid = $pro->merchant_id;
            $productId = $pro->product_id;

            
            $carttype = $pro->cart_type;
            $cart_sub_type = $pro->cart_sub_type;


            $coountPro = Products::where('pro_id',$productId)->count();
            if($coountPro >=1)
            {
            $getProinfo = Products::where('pro_id',$productId)->first();
              $getMid = $getProinfo->pro_mc_id;  
               
              if($carttype=='car_rental' || $carttype=='clinic' || $carttype=='music' || $carttype=='shopping' || $carttype=='occasion' || $carttype=='travel' || $carttype=='food'){
                $allOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->count();
               
              }elseif($cart_sub_type=='makeup_artists' || $cart_sub_type=='beauty_centers' || $cart_sub_type=='men_saloon' || $cart_sub_type=='spa'  || $cart_sub_type=='makeup'){
                    $allOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->count();
              }else{
            $allOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$shopVid)->where('pro_id',$productId)->count();
           
                 } 
                       
                if($allOffers >=1)
                {
                $valid = 1;  
                if($carttype=='car_rental' || $carttype=='clinic'   || $carttype=='music' || $carttype=='shopping' || $carttype=='occasion' || $carttype=='travel' || $carttype=='food'){
                $getOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->first();
              }elseif($cart_sub_type=='makeup_artists' || $cart_sub_type=='beauty_centers' || $cart_sub_type=='men_saloon' || $cart_sub_type=='spa'  || $cart_sub_type=='makeup'){
                    $getOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$pro->merchant_id)->where('pro_id',$pro->shop_id)->first();
              }else{                
                $getOffers = DB::table('nm_product_offer')->whereDate('start_date','<=', $curr)->whereDate('end_date', '>=', $curr)->where('coupon',$CCode)->where('vendor_id',$shopVid)->where('pro_id',$productId)->first();
                } 

                
                $discount = $getOffers->discount;
                $start_date = $getOffers->start_date;
                $end_date = $getOffers->end_date;
                $total_price = $pro->total_price;
                $cart_type = $pro->cart_type;
                if($cart_type == 'hall')
                {
                   $total_price = $pro->paid_total_amount; 
                }
                                


                $quantity = $pro->quantity;
                if($quantity =='' || $quantity==NULL)
                {
                $quantity = 1;
                }
                $PerQtyAmt = $total_price / $quantity;
                $NewAmount = $PerQtyAmt - ($PerQtyAmt * $discount)/100;
                $AmtAccQty = $NewAmount * $quantity;

                 $NewAmounts = ($PerQtyAmt * $discount)/100;
                 $NewAmountsa =  $NewAmounts * $quantity;
                $getC = CartProduct::where('cart_id', '=', $cart->id)->where('shop_id',$getMid)->where('product_id',$productId)->where('coupon_code',$CCode)->count();
                if($getC >=1)
                { 
                    if($del ==1)
                    {
                    CartProduct::where('cart_id', '=', $cart->id)->where('coupon_code',$CCode)->where('shop_id',$getMid)->where('product_id',$productId)->update(['coupon_code_amount'=>'delete','price_after_coupon_applied'=>'']);
                    CartProduct::where('cart_id', '=', $cart->id)->where('coupon_code_amount','delete')->where('shop_id',$getMid)->where('product_id',$productId)->update(['coupon_code'=>'','price_after_coupon_applied'=>'']);

                    $delte = 1; 
                     }
                     else
                     {
                      $already = 1;  
                     }
                    
                }
                else
                {
                CartProduct::where('cart_id', '=', $cart->id)->where('shop_id',$getMid)->where('product_id',$productId)->update(['coupon_code'=>$CCode,'coupon_code_amount'=>$NewAmountsa,'price_after_coupon_applied'=>$AmtAccQty]);  

                $coupnAmt = $NewAmountsa + $coupnAmt;


                }
                }

            }
            }
 
            if($delte == 1)
            {
            $code = 'Coupon code deleted successfully';
            return $code;
            }
            elseif($valid == 1)
            {
            $getInfo =  CartProduct::where('cart_id', '=', $cart->id)->where('coupon_code',$CCode)->first();
            $cart_type = $getInfo->cart_type;
            $coupon_code_amount = $getInfo->coupon_code_amount;
            if($already==1){ return 'Already applied';}
            if($coupnAmt < 1)
            {
            return $cart_type.'~~'.$CCode.'~~0';
            }
            else
            {
              return $cart_type.'~~'.$CCode.'~~'.$coupnAmt;   
            }
           
             }
            else
            {
            $code = 'Invaild coupon code';
            return $code;
            }
 
            }
            catch (ModelNotFoundException $e)
            {
            return back()->with('message','Something went wrong');
            }
        }





public static function getshippingInfo($proIDD,$cart_sub_type,$cart_type)
            {
        // $cart_sub_type='cosha';
                
                  $getProInfocount = DB::table('nm_product')->where('pro_id', $proIDD)->count(); 
               
                $getProInfo = DB::table('nm_product')->where('pro_id', $proIDD)->first();

                
               
               if($getProInfocount >0) {
                $pro_mc_cid = $getProInfo->pro_mc_id;

              }
              
                if($cart_sub_type=='invitations'  ) {
                     $merchantID = '';

                } else {
                $merchantID = $getProInfo->pro_mr_id;
                 }
 

                if(isset($pro_mc_cid)!='') {
                $getCatInfo = DB::table('nm_category')->where('mc_id', $pro_mc_cid)->first();
                 }

               if($cart_sub_type=='invitations') {
                     $parent_id = '';

                } else {
                $parent_id = $getCatInfo->parent_id;
                }


                $getCatInfoaa = DB::table('nm_category')->where('mc_id', $parent_id)->first();

                  if($cart_sub_type=='invitations') {
                     $parent_ida = '';

                } else {
                $parent_ida = $getCatInfoaa->parent_id;
                }

                $getCatInfoahu= DB::table('nm_category')->where('mc_id', $parent_ida)->first();

                 if($cart_sub_type=='invitations') {
                     $parent_idas = '';

                } else {
                $parent_idas = $getCatInfoahu->parent_id;
               }

                $pick =0; $aramax = 0;

                  $getInfo = DB::table('nm_service_shipping')->where('vendor_id', $merchantID)->count();
 


                if($getInfo >=1)
                {
                $getShippingMethod = DB::table('nm_service_shipping')->where('vendor_id', $merchantID)->first();
            
                if($cart_sub_type=='buffet')
                {


                 $Buffet = explode(",", $getShippingMethod->Buffet);
               
                if(isset($Buffet[0]) && $Buffet[0] !='')
                {
                if($Buffet[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Buffet[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Buffet[1]) && $Buffet[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }


                if($cart_sub_type=='dessert')
                {

                $Desert = explode(",",  $getShippingMethod->Desert);
                if(isset($Desert[0]) && $Desert[0] !='')
                {
                if($Desert[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Desert[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Desert[1]) && $Desert[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }
                if($cart_sub_type=='dates')
                {

                $Dates = explode(",",  $getShippingMethod->Dates); 
                if(isset($Dates[0]) && $Dates[0] !='')
                {
                if($Dates[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Dates[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Dates[1]) && $Dates[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }

                
                if($cart_sub_type=='roses')
                {


                $Roses = explode(",",  $getShippingMethod->Roses);
                if(isset($Roses[0]) && $Roses[0] !='')
                {
                if($Roses[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Roses[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Roses[1]) && $Roses[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='events')
                {

                $Special_Events = explode(",",  $getShippingMethod->Special_Events);
                if(isset($Special_Events[0]) && $Special_Events[0] !='')
                {
                if($Special_Events[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Special_Events[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Special_Events[1]) && $Special_Events[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }



                if($cart_sub_type=='makeup')
                {

                $Makeup_Product = explode(",",  $getShippingMethod->Makeup_Product); 
                if(isset($Makeup_Product[0]) && $Makeup_Product[0] !='')
                {
                if($Makeup_Product[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Makeup_Product[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Makeup_Product[1]) && $Makeup_Product[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_type=='music')
                {

                $Acoustics = explode(",",  $getShippingMethod->Acoustics); 
                if(isset($Acoustics[0]) && $Acoustics[0] !='')
                {
                if($Acoustics[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Acoustics[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Acoustics[1]) && $Acoustics[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='tailor')
                {

                $Tailors = explode(",",  $getShippingMethod->Tailors); 
                if(isset($Tailors[0]) && $Tailors[0] !='')
                {
                if($Tailors[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Tailors[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Tailors[1]) && $Tailors[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


                if($cart_sub_type=='dress')
                {

                $Dresses = explode(",",  $getShippingMethod->Dresses);
                if(isset($Dresses[0]) && $Dresses[0] !='')
                {
                if($Dresses[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Dresses[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Dresses[1]) && $Dresses[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }
                if($cart_sub_type=='abaya') 
                {

                $Abaya = explode(",",  $getShippingMethod->Abaya); 
                if(isset($Abaya[0]) && $Abaya[0] !='')
                {
                if($Abaya[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Abaya[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Abaya[1]) && $Abaya[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }

                }
                if($cart_sub_type=='perfume')
                { 
                $Oud_and_Perfumes = explode(",",  $getShippingMethod->Oud_and_Perfumes); 
                if(isset($Oud_and_Perfumes[0]) && $Oud_and_Perfumes[0] !='')
                {
                if($Oud_and_Perfumes[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Oud_and_Perfumes[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Oud_and_Perfumes[1]) && $Oud_and_Perfumes[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                } 
 
                if(trim($cart_sub_type) == 'gold')
                {
 
                $Gold_and_Jewellery = explode(",",  $getShippingMethod->Gold_and_Jewellery);
                if(isset($Gold_and_Jewellery[0]) && $Gold_and_Jewellery[0] !='')
                {
                if($Gold_and_Jewellery[0]==1) {  $aramax = 1;  $pick = 0; }
                if($Gold_and_Jewellery[0]==2) {  $aramax = 0;  $pick = 1;}
                }
                if(isset($Gold_and_Jewellery[1]) && $Gold_and_Jewellery[1] !='')
                {
                $pick = 1;  $aramax = 1;
                }
                }


}
         

return ['aramax'=>$aramax,'pick_logistics'=>$pick];
 
            

            }


public static function getshopname($shopid,$lang)
{
    $shopname = '';
    if($shopid!=''){
    $getProInfo = DB::table('nm_category')->where('mc_id', $shopid)->first(); 
   
    if($lang =='en')
    {
    $shopname = $getProInfo->mc_name;
    }
    else
    {
    $shopname = $getProInfo->mc_name_ar;     
    }
    }
    return $shopname;

}

   public function cartVendorList($lang) {      
        
        
      

            try {
              
            $Users = Session::get('customerdata'); 
            $UserID =  $Users['user_id'];
            $User = User::find($UserID);


                $cart = Cart::where('user_id', '=', $User->cus_id)->first();
                //get user's detail
                if (!empty($cart)) { 

                // buffet case only- because we are displaying only branch in cart  //
                $artarrya = array(0); $arshop = array();


                $getBuffetCount =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->count();
                if($getBuffetCount >=1)
                {
                $getBuffet  =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->get(); 
                foreach ($getBuffet as $getBuffetue) 
                {
                $Bshop_id = $getBuffetue->shop_id;
                $Bid = $getBuffetue->id; 
                if(in_array($Bshop_id, $arshop))
                {
                array_push($artarrya, $Bid);   
                }
                array_push($arshop, $Bshop_id);
                }

                $getBuffetSum  =   CartProduct::where('cart_sub_type','=','buffet')->where('cart_id', '=', $cart->id)->groupBy('category_id')->get(array(DB::raw('SUM(total_price) as total'),'shop_id')); 
                foreach( $getBuffetSum as $vals)
                {
                $shop_id = $vals->shop_id;
                $total = $vals->total; 
                CartProduct::where('cart_sub_type','=','buffet')->where('shop_id', '=', $shop_id)->where('cart_id', '=', $cart->id)->update(['new_price'=>$total]);
                }
                }
                // buffet case only- because we are displaying only branch in cart  //

                // Kosha case only- because we are displaying only branch in cart  //

                $Kcosha = array('design','readymade');

                $getcoshaCount =   CartProduct::where('cart_sub_type','=','cosha')->where('quantity', '>=', 1)->where('cart_id', '=', $cart->id)->count();
                if($getcoshaCount >=1)
                {
                $getcosha  =   CartProduct::where('cart_sub_type','=','cosha')->where('quantity', '>=', 1)->where('cart_id', '=', $cart->id)->get(); 

                foreach ($getcosha as $getcoshaue) 
                {                  
                $Bid = $getcoshaue->id;                      
                array_push($artarrya, $Bid); 
                }
                }
                // Kosha case only- because we are displaying only branch in cart  //

                if ($lang == 'ar') {

                /* FOR ARABIC LANGUAGE */                       
                $getcartPro = CartProduct::distinct()->select('merchant_id')->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

                foreach($getcartPro as $merval)
                {

                $merID = $merval->merchant_id;
                if($merID != 0)
                {
                $getMerID = DB::table('nm_merchant')->where('mer_id',$merID)->first();



                if(isset($getMerID->mer_fname_ar) && $getMerID->mer_fname_ar!='')
                {
                $merval->fname = $getMerID->mer_fname_ar;
                $merval->lname = $getMerID->mer_lname_ar; 
                }
                
                else
                {
                  $merval->fname = '';
                $merval->lname = '';   
                }

 

                if(isset($getMerID->mer_fname_ar) && $getMerID->mer_fname_ar=='')
                {
                    if(isset($getMerID->mer_fname) && $getMerID->mer_fname!='')
                    {
                    $merval->fname = $getMerID->mer_fname;
                    $merval->lname = $getMerID->mer_lname; 
                    }
                    else
                    {
                    $merval->fname = '';
                    $merval->lname = '';     
                    }
               
                }

                }
                if($merID == 0)
                {
                $merval->fname = 'Admin';
                $merval->lname = '';

                }

            $cartPro = CartProduct::distinct()->select('cart_sub_type')->where('merchant_id', '=', $merID)->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

            $merval->cart_sub_type = $cartPro;

            foreach ($cartPro as  $value) { 

            $Subtype = $value->cart_sub_type;


                $cart_pro = CartProduct::select('id','new_price','merchant_id','shop_id','shop_vendor_id','cart_type','fabric_name','cart_sub_type','cart_id','category_id','product_id','pro_title_ar as pro_title','pro_desc_ar as pro_desc','pro_Img','quantity','paid_total_amount','total_price','product_size','pro_price','insurance_amount','status','created_at','rose_package_type','buy_rent','coupon_code_amount','price_after_coupon_applied','coupon_code')->where('cart_id', '=', $cart->id)->whereNotIn('id', $artarrya)->where('cart_sub_type', '=', $Subtype)->where('merchant_id', '=', $merID)->where('status', '!=', 0)->with('getProduct:pro_id,pro_title_ar as pro_title,pro_Img,pro_price,Insuranceamount,pro_disprice')->with('getCategory:mc_id,mc_name,mc_img')->orderBy('updated_at', 'DESC')->get(); //get user's cart detail

              $value->products = $cart_pro;
                foreach($cart_pro as $val) {
                $CProID = $val->id;

                $cart_type = $val->cart_type;
                $buy_rent = $val->buy_rent;
                $category_id = $val->category_id;
                //display all related cosha products to cart //
                $val->cosha_products = '';
                $ChekCosha = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->count();
                if($ChekCosha >=1 && $buy_rent!='readymade')
                {

                $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();  
                $Title = '';
                foreach ($ChekCoshaInfo as $key => $value) {
                $Title = $value->pro_title_ar.','.$Title;
                }
                $val->cosha_products = rtrim($Title,',');
                } 
                //display all related cosha products to cart //
                if($val->cart_sub_type=='buffet'){
                $shop_id = $val->shop_id; 
                $getexternalprod=CartExternalFoodDish::select('id','external_food_dish_id')->where('cart_id','=', $val->cart_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title_ar as pro_title,pro_Img')->get();
                $val->buffetProduct = $getexternalprod;
                }

                $cart_type = $val->cart_type;
                if($cart_type == 'hall')
                {
                $val->total_price = $val->paid_total_amount;
                }

                $proIDD = $val->product_id;
                $ChkRent = DB::table('nm_cart_product_rent')->where('cart_id','=', $val->cart_id)->where('cart_product_id','=', $CProID)->count();
                if($ChkRent >=1)
                {
                $val->rental_product = '1';
                }
                else
                {
                $val->rental_product = '0'; 
                }

                $shopid = $val->shop_id;
                $cart_sub_type = $val->cart_sub_type;
                $cart_type = $val->cart_type;
                $getinfo = $this->getshippingInfo($proIDD,$cart_sub_type,$cart_type);
                $lang = 'ar';
                $shopname = $this->getshopname($shopid,$lang);
                $val->shipping_methods =  $getinfo;
                $val->shopname =  $shopname;
                }
                }
            }

                if (!empty($getcartPro)) {
                $response_array = array('data' => ['success' => true, 'code' => 200, 'message' => 'تم العثور على البيانات'],'view_cart' => $getcartPro);
                } else {
                $response_array = array('data' => ['success' => false, 'error_code' => 500, 'message' => 'لم يتم العثور على بيانات']);
                }

                } else {
                /*FOR ENGLISH LANGUAGE*/                     



                $getcartPro = CartProduct::distinct()->select('merchant_id')->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

                foreach($getcartPro as $merval)
                {

                $merID = $merval->merchant_id;

                $getMerID = DB::table('nm_merchant')->where('mer_id',$merID)->first();
                if($merID != 0)
                {
                $getMerID = DB::table('nm_merchant')->where('mer_id',$merID)->first();
                if(isset($getMerID->mer_fname) && $getMerID->mer_fname!='')
                {
                $merval->fname = $getMerID->mer_fname;
                $merval->lname = $getMerID->mer_lname;
                }
                else
                {
                $merval->fname = '';
                $merval->lname = ''; 
                }

                  
                }
                if($merID == 0)
                {
                $merval->fname = 'Admin';
                $merval->lname = '';
                }

                $cartPro = CartProduct::distinct()->select('cart_sub_type')->where('merchant_id', '=', $merID)->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->get();

                $merval->cart_sub_type = $cartPro;

                foreach ($cartPro as  $value) { 

                $Subtype = $value->cart_sub_type;
               

                $cart_pro = CartProduct::select('id','new_price','merchant_id','shop_id','shop_vendor_id','cart_type','fabric_name','cart_sub_type','cart_id','category_id','product_id','pro_title','pro_desc','pro_Img','quantity','paid_total_amount','total_price','product_size','pro_price','insurance_amount','status','created_at','rose_package_type','buy_rent','coupon_code_amount','price_after_coupon_applied','coupon_code')->where('cart_sub_type', '=', $Subtype)->where('merchant_id', '=', $merID)->where('cart_id', '=', $cart->id)->where('status', '!=', 0)->whereNotIn('id', $artarrya)->with('getProduct:pro_id,pro_title,pro_Img,pro_price,Insuranceamount,pro_disprice')->with('getCategory:mc_id,mc_name,mc_img')->get();

 

                $value->products = $cart_pro;


                foreach($cart_pro as $val) {
 
                $CProID = $val->id;
                $cart_type = $val->cart_type;
                $category_id = $val->category_id;

                //display all related cosha products to cart //
                $val->cosha_products = '';
                $ChekCosha = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->count();


                if($ChekCosha >=1 && $val->buy_rent !='readymade')
                {

                $ChekCoshaInfo = DB::table('nm_cart_services_attribute')->where('cart_type','occasion')->where('attribute_title','Design Your Kosha')->where('category_id',$category_id)->where('cart_id',$cart->id)->get();  
                $Title = '';
                foreach ($ChekCoshaInfo as $key => $value) {
                $Title = $value->pro_title.','.$Title;
                }
                $val->cosha_products = rtrim($Title,',');
                } 

                //display all related cosha products to cart //
                if($val->cart_sub_type=='buffet'){
                $shop_id = $val->shop_id; 
                $getexternalprod=CartExternalFoodDish::select('id','external_food_dish_id')->where('cart_id','=', $val->cart_id)->where('category_id','=', $shop_id)->with('getFood:pro_id,pro_title,pro_Img')->get();
                $val->buffetProduct = $getexternalprod;
                }

                if($cart_type == 'hall')
                {
                $val->total_price = $val->paid_total_amount;
                }
                $proIDD = $val->product_id;
                $ChkRent = DB::table('nm_cart_product_rent')->where('cart_id','=', $val->cart_id)->where('cart_product_id','=', $CProID)->count();
                if($ChkRent >=1)
                {
                $val->rental_product = '1';
                }
                else
                {
                $val->rental_product = '0'; 
                }
                $shopid = $val->shop_id;
                  $cart_sub_type = $val->cart_sub_type;

 
                $getinfo = $this->getshippingInfo($proIDD,$cart_sub_type,$cart_type);

                $val->shipping_methods =  $getinfo;


                $lang = 'en';
                if($shopid!=''){
                $shopname = $this->getshopname($shopid,$lang);
                $val->shopname =  $shopname;  
                }
                else
                {
                $val->shopname = '';
                }

                }
                }
                } 
                //get user's cart detail
                if (!empty($getcartPro)) {
                $response_array =  $getcartPro;
                } else {
                $response_array =  'Data not found';
                }

                }

             

                } else {

                $response_array = array('data' => ['success' => false, 'code' => 500, 'status' => 0, 'error' => trans($locale.'_mob_lang.CART_EMPTY')]);
                

                }
            } catch (ModelNotFoundException $e) {
                $response_array = array('data' => ['success' => false, 'code' => 203, 'error' => trans($locale.'_mob_lang.PROFILE_UPDATE_ERROR')]);
              
            }
        

        return $response_array;

   }
 


public function getshippingrate(Request $request)
{

$validator = Validator::make($request->all(), [
              'merchant_id' => 'required',
              'cart_sub_type'=> 'required',
              'shipping_type'=> 'required' 
        ]);   

        $input = $request->all();
       
  
            try { 

            $Users = Session::get('customerdata'); 
            $UserID =  $Users['user_id'];
            $User = User::find($UserID); 
            $merchant_id = $input['merchant_id'];
            $cart_sub_type = $input['cart_sub_type'];
            $shipping_type = $input['shipping_type'];
            $price = 0;
            $cartc = Cart::where('user_id',$User->cus_id)->count();
            if($cartc >=1)
            {


            $cart = Cart::where('user_id',$User->cus_id)->first();

          $getproducts = DB::table('nm_cart_product')->where('cart_id',$cart->id)->where('cart_sub_type',$cart_sub_type)->where('merchant_id',$merchant_id)->get();
            $shop_id =  0;
            $totalWgt = 0; 
          foreach ($getproducts as $key => $value) {
                $getProID =  $value->product_id;
                $getproductsWight = DB::table('nm_product')->select('pro_weight')->where('pro_id',$getProID)->first();
                $totalWgt = $totalWgt + $getproductsWight->pro_weight;
                $shop_id =  $value->shop_id;
          }

                //For Aramx
                if($shipping_type ==1)
                {
                    if($totalWgt <=15)
                    {
                        $price = 33;
                    }
                    else
                    {
                        $Extrawgt = $totalWgt-15;
                        $Extaweight = $Extrawgt * 2.5;
                        $newP = 28 + $Extaweight;
                        if($newP <=33)
                        {
                        $price = 33;   
                        }
                        else
                        {
                        $price = $newP;   
                        }
                    }


                }

                //For Pick
                if($shipping_type ==2)
                {
                 if($totalWgt <=20)
                    {
                        $price = 33;
                    }
                    else
                    {
                        $Extrawgt = $totalWgt-20;
                        $Extaweight = $Extrawgt * 3;
                        $newP = 24 + $Extaweight;
                        if($newP <=33)
                        {
                        $price = 33;   
                        }
                        else
                        {
                        $price = $newP;   
                        }
                    }

                }
 
             

            if($price >= 33)
            {
            $pOutput = $price;
            }
            else
            {
            $pOutput = 33;
            }
 

            

            $ChkShip = DB::table('nm_shipping')->where('cart_sub_type',$cart_sub_type)->where('cart_id',$cart->id)->where('shop_id',$shop_id)->where('ship_cus_id',$User->cus_id)->where('ship_order_id','=',0)->count();
            if($ChkShip >=1)
            {
            $ChkShip = DB::table('nm_shipping')->where('cart_sub_type',$cart_sub_type)->where('cart_id',$cart->id)->where('shop_id',$shop_id)->where('ship_cus_id',$User->cus_id)->where('ship_order_id','=',0)->delete();
            }





        DB::table('nm_shipping')->insert(array('cart_sub_type'=>$cart_sub_type,'cart_id'=>$cart->id,'shop_id'=>$shop_id,'ship_cus_id'=>$User->cus_id,'shipping_type'=>$shipping_type,'shipping_price'=>$pOutput));



        $response_array = array('data' => ['success' => true, 'code' => 200, 'status' => 1, 'price' => $pOutput]);
            }
            else
            {
                $pOutput = 0;
              $response_array = array('data' => ['success' => true,'error_code' => 200, 'error' => 'Cart is empty']);  
            }
            $response = Response::json($response_array);
            } catch (ModelNotFoundException $e) {
                 $pOutput = 0;
            $response_array = array('data' => ['success' => false,'error_code' => 500, 'error' => 'Something went wrong']);
            $response = Response::json($response_array);
            }
              

 return $pOutput;

}




 
}

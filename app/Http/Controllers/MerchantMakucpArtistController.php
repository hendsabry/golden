<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
 

class MerchantMakucpArtistsController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info

// Start For Popular band info
   
    public function spaBranchList(Request $request)
     {
 
             //$q->where('name', 'like', "%{$breed}%");
        if (Session::has('merchantid')) 
        {   
                $getPagelimit = config('app.paginate');
                $id = request()->id;  
                $status = $request->status;
                $search = $request->input('search');
                $mer_id   = Session::get('merchantid');
                $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');
               

                if($search !='')
                {
                $mc_name='mc_name';
                $mer_selected_lang_code = Session::get('mer_lang_code'); 
                if($mer_selected_lang_code !='en')
                {
                $mc_name= 'mc_name_'.$mer_selected_lang_code;
                }
                $alldata = $alldata->where('nm_category.'.$mc_name,'like', '%' .$search. '%');
                }


                if($status !='')
                {
                $alldata = $alldata->where('mc_status','=',$status);
                }

                $alldata=$alldata->paginate($getPagelimit)->appends(request()->query());
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                return view('sitemerchant.spa.spa-shop', compact('merchantheader','merchantfooter','alldata','id','search'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
   


    public function makeupArtistShopInfo(Request $request)
     {  
       
	    if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $getPagelimit = config('app.paginate');
$status = $request->status;
$search = $request->search;
$productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
if($status!='')
{
$productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->where('pro_status',$status)->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
}
if($search !='')
{
    $productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->where('pro_title','like','%'.$search.'%')->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
}



 
                return view('sitemerchant.makeup-artist.makeup-artist-shop-info', compact('merchantheader','merchantfooter','id','productdata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function spaAddShop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = request()->hid;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');   

                $getlisting = Categorylist::where('mc_id', $hid)->where('vendor_id',$mer_id)->first(); 
                return view('sitemerchant.spa.spa-add-shop', compact('merchantheader','merchantfooter','id','hid','getlisting'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function spaManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 

    public function spaServices(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-service', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 
    public function spaAddServices(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-add-service', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		 	  
	 
	 
    public function spaAddManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-add-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 	
	  public function MakeupArtistReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.spa.spa-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 
	  public function spaWorkerReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.spa.worker-review', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		   
	 	
		
		
    public function spaPictures(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-picture', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function spaAddServiceCatogory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-add-service-catogory', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	 
	 public function spaOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-offer', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	  public function spaAddOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-add-offer', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 

	 
	 
	 
    public function spaServiceCatogory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-service-category', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	 
	  public function spaPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-package', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	  public function spaWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-worker', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 public function spaAddWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-add-worker', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 public function spaOrder(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-order', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 
	 
	 
	 
	 	  public function spaAddBranch(Request $request)
            {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');                
 
                return view('sitemerchant.spa.spa-add-branch', compact('merchantheader','merchantfooter','id','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 public function spaAddPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-add-package', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 	 	 	 
	 			 
	 
   
   
    public function spashop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.spa.spa-shop-info', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
   
   

     public function savebeautyservice(Request $request)
        {    
          

              $merid  = Session::get('merchantid');
                if($request->mc_img){ 
                $file             = $request->mc_img; 
                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;
                //$file->move('hallpics', $fileName);               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
 
                $thumb_width = 100;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
 
                 $img->save('beautyupload' . '/'. $thumbName);
                $file->move('beautyupload/', $Adressimgname);
                $Adressimgnames = url('').'/beautyupload/'.$thumbName;
                }
                else
                {
                $Adressimgnames = $request->mcimg; 
                }
                 if($request->hid!='')
                 {   
                    $pid = $request->hid;  
                    $saveservice = Categorylist::where('mc_id',$pid)->where('vendor_id',$merid)->first();
                 }
                 else
                 {
                   $saveservice = new Categorylist; 
                 }
                
                $saveservice ->parent_id = $request->parent_id;
                $saveservice ->mc_name = $request->mc_name;
                $saveservice ->mc_name_ar = $request->mc_name_ar;
                $saveservice ->mc_discription = $request->description;
                $saveservice ->mc_discription_ar = $request->description_ar;
                $saveservice ->vendor_id = $merid;
                $saveservice ->mc_img = $Adressimgnames; 
                $saveservice->mc_status = 1;
                if($saveservice->save()){
                 $id = $request->parent_id;
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الفندق بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Shop successfully saved");
                    }
              
                }
             return redirect()->route('spa-shop',['id' => $id]);
            
        }



   
   
 } // end 



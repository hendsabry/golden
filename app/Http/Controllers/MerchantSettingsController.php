<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Lang;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant;
use App\Blog;
use App\Dashboard;
use App\Admodel;
use App\Deals;
use App\Auction;
use App\Customer;
use App\Transactions;
use App\Merchantadminlogin;
use App\Merchantproducts;
use App\Merchantsettings;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image; 
class MerchantSettingsController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

 public function __construct(){
        parent::__construct();
       
        // set admin Panel language
        $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleMerchant();
                    return $next($request);
    });
       
    }

    public function show_settings()
    {
         if (Session::get('merchantid')) {
			 if(Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $merchantheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
            $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
            $merchantfooter    = view('sitemerchant.includes.merchant_footer');
            return view('sitemerchant.merchant_settings')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter);
        } else {
            
            return Redirect::to('sitemerchant');
            
        }
    }

    public function merchant_profile()
    {
        
        if (Session::get('merchantid')) {
            $merchant_id = Session::get('merchantid');
            if(Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '') 
			{
				$session_message = trans(Session::get('mer_lang_file').'.MER_SETTINGS');
			}
			else 
			{
				$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
			}
            $merchantheader = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
            
            $merchantfooter           = view('sitemerchant.includes.merchant_footer');
            $merchant_setting_details = Merchant::get_merchant_profile_details($merchant_id);
            return view('sitemerchant.merchant_profile')->with('merchantheader', $merchantheader)->with('merchantfooter', $merchantfooter)->with('merchant_setting_details', $merchant_setting_details);
        } else {
            
            return Redirect::to('sitemerchant');
            
        }
    }

    public function edit_info($id)
    {
		if(Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.MER_SETTINGS');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
		}
        $merchantheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
        $merchantfooter    = view('sitemerchant.includes.merchant_footer');
        
        $country_return  = Merchant::get_country_detail();
        if(Session::get('LoginType') =='4')
        {
            $id = Session::get('merchant_managerid'); 
        }
        $getCountry = DB::table('nm_country')->where('co_status', 1)->orderby('co_name')->get();
        $merchant_return = Merchant::get_induvidual_merchant_detail($id);
       
        return view('sitemerchant.edit_merchant_account')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('country_details', $country_return)->with('merchant_details', $merchant_return)->with('getCountry', $getCountry);
        
    }
    
    public function change_pwd($id)
    {
		if(Lang::has(Session::get('mer_lang_file').'.MER_SETTINGS')!= '') 
		{
			$session_message = trans(Session::get('mer_lang_file').'.MER_SETTINGS');
		}
		else 
		{
			$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_SETTINGS');
		}
        if(Session::get('LoginType') =='4')
        {
            $id = Session::get('merchant_managerid'); 
        }
		$merchantheader    = view('sitemerchant.includes.merchant_header')->with('routemenu', $session_message);
        $merchantleftmenus = view('sitemerchant.includes.merchant_left_menu_settings');
        $merchantfooter    = view('sitemerchant.includes.merchant_footer');
        return view('sitemerchant.merchant_change_password')->with('merchantheader', $merchantheader)->with('merchantleftmenus', $merchantleftmenus)->with('merchantfooter', $merchantfooter)->with('mer_id', $id);
    }
    
    public function edit_merchant_account_submit(Request $request)
    {
        $mer_id = Input::get('mer_id');
        if(Session::get('LoginType') =='4')
        {
            $mer_id = Session::get('merchant_managerid'); 
        }
       
        $data   = Input::except(array(
            '_token'
        ));
        $rule   = array(
            'first_name' => 'required|alpha_dash',
            'last_name' => 'required|alpha_dash',
            'email_id' => 'required|email',
            'select_mer_country' => 'required',
            'select_mer_city' => 'required|numeric',
            'phone_no' => 'required|numeric',
            'addreess_one' => 'required',
            'zipcode' => 'required'
            
        );


            if(!empty($request->company_logo)){ 
            
            $file = $request->company_logo;
            $Coname = $file->getClientOriginalName();
            $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
            $extension = $file->getClientOriginalExtension();
            $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
            $file->move('./public/assets/storeimage/', $filename_c);                      
            $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
            }
            else{
            $Cname = $request->certificates;
            $Coname = $request->certificatesname;
            } 



 


        $validator = Validator::make($data, $rule);
        if ($validator->fails()) {
            return Redirect::to('edit_merchant_info/' . $mer_id)->withErrors($validator->messages())->withInput();
        } else {
            $mer_email         = Input::get('email_id');
              $check_merchant_id = Merchant::check_merchant_email_edit($mer_email, $mer_id);


 
            if (count($check_merchant_id) > 0 ) {
				if(Lang::has(Session::get('mer_lang_file').'.MER_MERCHANT_EMAIL_EXIST')!= '') 
				{
					$session_message = trans(Session::get('mer_lang_file').'.MER_MERCHANT_EMAIL_EXIST');
				}
				else 
				{
					$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_MERCHANT_EMAIL_EXIST');
				}
                return Redirect::to('edit_merchant_info/' . $mer_id)->with('mail_exist', $session_message)->withInput();
            } else {
                
$shopimage_Img ='';
                //Insert images in folder
                   if($file=$request->file('stor_img'))
                    {          
                        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());          
                        $extension = $file->getClientOriginalExtension();
                        $fileName = rand(11111,99999).'_'.time().'.'.$extension;                 
                        $imageRealPath  =   $file->getRealPath();
                        $thumbName      =   'thumb_'. $fileName;       
                        $img = Image::make($imageRealPath);
                        $img->resize(150,150);
                        $img->save('receptionspic' . '/'. $thumbName);
                        $file->move('receptionspic/', $fileName);            
                        $shopimage_Img = url('/').'/receptionspic/'.$thumbName; 

                        $country_code = Input::get('country_code');
                        $phone_no = Input::get('phone_no');

                         $merchant_entry       = array(
                    'mer_fname' => Input::get('first_name'),
                    'mer_lname' => Input::get('last_name'),
                    'mer_email' => Input::get('email_id'),
                    'mer_phone' => '+'.$country_code.'-'.$phone_no,
                    'mer_address1' => Input::get('addreess_one'),
                    'mer_address2' => Input::get('address_two'),
                    'mer_co_id' => Input::get('select_mer_country'),
                    'mer_ci_id' => Input::get('select_mer_city'),
                    'mer_payment' => Input::get('mer_payment'),
                    'zipcode'    =>   Input::get('zipcode'),
                    'shop_link_in_maroof'    =>   Input::get('shop_link_in_maroof'),
                    'certificates_name'    =>  $Coname,
                    'stor_img'    =>   $shopimage_Img,
                    'certificates'=>$Cname,
                    'mer_payu_key'    =>   Input::get('mer_payu_key'),
                    'mer_payu_salt'    =>   Input::get('mer_payu_salt'),
                    //'mer_commission' => Input::get('commission') /* if anyone edit in console means value will change */
                );

                           
                     }
                     else
                     {
                        $country_code = Input::get('country_code');
                        $phone_no = Input::get('phone_no');
                          $merchant_entry       = array(
                    'mer_fname' => Input::get('first_name'),
                    'mer_lname' => Input::get('last_name'),
                    'mer_email' => Input::get('email_id'),
                    'mer_phone' => '+'.$country_code.'-'.$phone_no,
                    'mer_address1' => Input::get('addreess_one'),
                    'mer_address2' => Input::get('address_two'),
                    'mer_co_id' => Input::get('select_mer_country'),
                    'mer_ci_id' => Input::get('select_mer_city'),
                    'mer_payment' => Input::get('mer_payment'),
                    'shop_link_in_maroof'    =>   Input::get('shop_link_in_maroof'),
                    'zipcode'    =>   Input::get('zipcode'),
                    'certificates_name'    =>  $Coname,
                    'certificates'=>$Cname,
                     'mer_payu_key'    =>   Input::get('mer_payu_key'),
                    'mer_payu_salt'    =>   Input::get('mer_payu_salt'),
                    //'mer_commission' => Input::get('commission') /* if anyone edit in console means value will change */
                );
                     }


              
                $inserted_merchant_id = Merchant::edit_merchant($merchant_entry, $mer_id);
                $check = DB::table('nm_merchant')->where('mer_id', '=', $mer_id)->first();
                if(count($check)>0)
                Session::put('merchantname', $check->mer_fname);


				if(Lang::has(Session::get('mer_lang_file').'.MER_RECORD_UPDATED_SUCCESSFULLY')!= '') 
				{
					$session_message = trans(Session::get('mer_lang_file').'.MER_RECORD_UPDATED_SUCCESSFULLY');
				}
				else 
				{
					$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_RECORD_UPDATED_SUCCESSFULLY');
				}
                return Redirect::to('edit_merchant_info/' . $mer_id)->with('result', $session_message);
                
            }
        }
    }
       
    public function change_password_submit()
    {
        $inputs      = Input::all();
        $merchant_id = Input::get('merchant_id');
        $oldpwd      = Input::get('oldpwd');
        $pwd         = Input::get('pwd');
        $confirmpwd  = Input::get('confirmpwd');
        $oldpwdcheck = Merchantsettings::check_oldpwd($merchant_id, $oldpwd);
        
        if ($oldpwdcheck) {
            if ($pwd != $confirmpwd) {
				if(Lang::has(Session::get('mer_lang_file').'.MER_BOTH_PASSWORDS_DONOT_MATCH')!= '') 
				{
					$session_message = trans(Session::get('mer_lang_file').'.MER_BOTH_PASSWORDS_DONOT_MATCH');
				}
				else 
				{
					$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_BOTH_PASSWORDS_DONOT_MATCH');
				}
                return Redirect::to('change_merchant_password/' . $merchant_id)->with('pwd_error', $session_message);
            } else {
				if(Lang::has(Session::get('mer_lang_file').'.MER_PASSWORD_CHANGED_SUCCESSFULLY')!= '') 
				{
					$session_message = trans(Session::get('mer_lang_file').'.MER_PASSWORD_CHANGED_SUCCESSFULLY');
				}
				else 
				{
					$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_PASSWORD_CHANGED_SUCCESSFULLY');
				}
                Merchantsettings::update_newpwd($merchant_id, $confirmpwd);
                
                return Redirect::to('change_merchant_password/' . $merchant_id)->with('success', $session_message);
                
            }
        } else {
				if(Lang::has(Session::get('mer_lang_file').'.MER_OLD_PASSWORD_DONOT_MATCH')!= '') 
				{
					$session_message = trans(Session::get('mer_lang_file').'.MER_OLD_PASSWORD_DONOT_MATCH');
				}
				else 
				{
					$session_message =  trans($this->MER_OUR_LANGUAGE.'.MER_OLD_PASSWORD_DONOT_MATCH');
				}
            return Redirect::to('change_merchant_password/' . $merchant_id)->with('pwd_error', $session_message);
            
        }
    }
	/*Set user select language*/
	public function merchant_new_change_languages()
	{      $lang_code =  $_REQUEST['language_code'];  
         Session::put('mer_lang_code',$lang_code);
		 Session::put('mer_lang_file','mer_'.Session::get('mer_lang_code').'_lang');
		echo "1";
       //return redirect()->back();


	}


    public function ajax_select_city(){
        
        $cityid    = $_GET['city_id'];
        $city_ajax = DB::table('nm_city')->where('ci_con_id', '=', $cityid)->orderby('ci_name','asc')
        ->where('ci_status', '=', 1)
        ->get();
        if ($city_ajax) {
            $return = "";
             if (Lang::has(Session::get('admin_lang_file').'.BACK_SELECT')!= '')
            { 
                $session_message =  trans(Session::get('admin_lang_file').'.BACK_SELECT');
            }  
            else 
            { 
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_SELECT');
            }
            $return = "<option value='0'> $session_message </option>";
            foreach ($city_ajax as $fetch_city_ajax) {
                if((Session::get('lang_code'))== '' || (Session::get('lang_code'))== 'en') { 
                        $ci_name = 'ci_name';
                }else {  $ci_name = 'ci_name_'.Session::get('lang_code'); }
                
                
                $return .= "<option value='" . $fetch_city_ajax->ci_id . "'> " . $fetch_city_ajax->$ci_name . " </option>";
            
        }
            echo $return;
        } else {
             if (Lang::has(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND')!= '')
            { 
                $session_message =  trans(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND');
            }  
            else 
            { 
                $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_NO_DATAS_FOUND');
            }
            echo $return = "<option value=''> $session_message </option>";
        }
    }


     public function ajax_select_city_edit()
    {
         $cityid    = $_GET['city_id_ajax'];
         $countryid    = $_GET['country_id_ajax'];
         $city_ajax = DB::table('nm_city')->where('ci_id', '=', $cityid )->get();
         $country_ajax = DB::table('nm_city')->where('ci_con_id', '=', $countryid)->orderby('ci_name','asc')
        ->where('ci_status', '=', 1)
        ->get();
         if ($city_ajax) {
                    $return = "";
          foreach ($city_ajax as $fetch_city_ajax) {
                        $return .= "<option value='" . $fetch_city_ajax->ci_id . "' selected > " . $fetch_city_ajax->ci_name . " </option>";
                    } 
         
           if($country_ajax) {       
        foreach ($country_ajax as $city_list) {

               if($cityid!=$city_list->ci_id) {
                        $return .= "<option value='" . $city_list->ci_id . "'> " . $city_list->ci_name . " </option>";
                  }

                    }
                }

                  
                    echo $return;
                } else {
                     if (Lang::has(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND')!= '')
                    { 
                        $session_message =  trans(Session::get('admin_lang_file').'.BACK_NO_DATAS_FOUND');
                    }  
                    else 
                    { 
                        $session_message =  trans($this->ADMIN_OUR_LANGUAGE.'.BACK_NO_DATAS_FOUND');
                    }
                    echo $return = "<option value=''> $session_message </option>";
                }
            }
}

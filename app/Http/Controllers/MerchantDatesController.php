<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use App\City;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\HallOffer;
use App\Merchant;
use App\ProductsMerchant;
use App\NmServicesAttribute;
use App\ProductOption;
use App\NmproductOptionValue;
use App\NmProductGallery;
class MerchantDatesController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 

 // Start For Dates Info
    public function datesInfo(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                 $shopid = $request->shopid;
                     $itemid = request()->itemid;
                     $fetchdata = Categorylist::where('mc_id',$sid)->where('vendor_id',$mer_id)->first();

                      $shopdata = Categorylist::where('mc_id',$shopid)->where('vendor_id',$mer_id)->first();
                     $city = City::where('ci_con_id',10)->get();
                     $manager = Merchant::where('vendor_parent_id',$mer_id)->where('mer_staus',1)->get();
                     $city = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

          
 
                return view('sitemerchant.dates.dates-info', compact('merchantheader','merchantfooter','id','getCities','manager','shopdata','fetchdata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }



// Start For dates Info
// Start For dates Shop
    public function datesShop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                echo $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id )->get();

 
                return view('sitemerchant.dates.dates-shop', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid','alldata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For dates Shop

  // Start For dates Add Shop 
    public function datesAddShop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $services_id = $request->services_id;
                $shopid = $request->shopid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $alldata = Categorylist::where('mc_id',$shopid)->where('vendor_id',$mer_id )->get();
 
                return view('sitemerchant.dates.dates-add-shop', compact('merchantheader','merchantfooter','services_id','getCities','alldata','shopid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }


 
   // Start For dates Picture & Video
    public function datesPictureVideo(Request $request)
     {
      if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = request()->id;
                $services_id = request()->sid;
                 
               
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $mer_id)->where('category_id', $services_id)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$services_id)->where('vendor_id',$mer_id)->first();
                            if($getDbC >=1)
                            {
                                $getDb = DB::table('nm_category_gallery')->where('vendor_id', $mer_id)->where('category_id', $services_id)->get(); 

                            }
                            else
                            {
                                $getDb = '';    
                            }
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                return view('sitemerchant.dates.dates-picture-video', compact('merchantheader','merchantfooter','services_id','getCities','services_id','branch_id','shopid','getDbC','getDb','getVideos' )); 
      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For dates Picture & Video


// Start For dates List Container
    public function datesListContainer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dates.dates-list-container', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For dates List Container

// Start For dates Add Container
    public function datesAddContainer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dates.dates-add-container', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For dates Add Container



// Start For dates Offer
    public function datesListOffer(Request $request)
     {
       if (Session::has('merchantid')) 
             {   $getPagelimit = config('app.paginate');
                $id = $request->id;
                 $sid = $request->sid;
             
                $mer_id   = Session::get('merchantid');
                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                $hallofferlist = HallOffer::where('vendor_id',$mer_id)->where('pro_id','=',$sid);
                $ourlang=\Config::get('app.locale') ;
                $search=isset($request->search)? $request->search:'';
                $status=isset($request->status)? $request->status:'';
                $language = $ourlang !='en' ? '_'.$ourlang :'';
                if(isset($request->search) && $request->search != '')
                {
                   $hallofferlist=$hallofferlist->where('title'.$language,'LIKE','%' .$request->search . '%');
                }
                if(isset($request->status) && $request->status != '')
                {
                   $hallofferlist=$hallofferlist->where('status',$request->status);
                }



                $hallofferlist= $hallofferlist->paginate($getPagelimit)->appends(request()->query());
           return view('sitemerchant.dates.dates-list-offer', compact('merchantheader','merchantfooter','branch_id','hallofferlist','shopid','services_id','search','status'));
    
            } else {
                return Redirect::to('sitemerchant');
            }  
     
     
     
     }
// End For dates Offer

// Start For dates Add Offer
    public function datesAddOffer(Request $request)
     {
       if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;       
                  $offer_id = $request->offerid;  
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = City::where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $dessertoffer = HallOffer::where('vendor_id',$mer_id)->where('id',$offer_id)->first();
 
                return view('sitemerchant.dates.dates-add-offer', compact('merchantheader','merchantfooter','branch_id','getCities','dessertoffer','shopid','services_id','offer_id')); 
 
               
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For dates Add Offer

// Start For dates Order
    public function datesOrder(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;


                $sid=$request->sid;
                 $searchkeyword=$request->searchkeyword;
                 $date_to=$request->date_to;
                 $End_Date=$request->from_to;
                 $order_days=$request->order_days;
                 $status=$request->status;                
                 $serachfirstfrm=$request->serachfirstfrm;
                    $hid=$request->sid;
                 

                  $getorderedproducts = DB::table('nm_order_product')->where('product_type','food')->where('product_sub_type','dates');
                  if($searchkeyword!=''){
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }

                  if($status!=''){
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 

                  if($date_to!='' && $End_Date!=''){
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 

                  $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();


                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dates.dates-order', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid','sid','getorderedproducts','mer_id'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }

      public function getorderdetail(Request $request){
      if (Session::has('merchantid')) 
             {

              $mer_id   = Session::get('merchantid');
                 $id = $request->id;
                 $opid = $request->opid;
                 $oid=$request->oid;
                 $cusid=$request->cusid;
                 $hid=$request->hid;
                  $sid=$request->sid;

              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
             
              $getbody_measurement = DB::table('nm_order_services_attribute')->where('product_type','food')->where('cus_id',$cusid)->where('order_id',$oid)->where('category_id',$sid)->get();
             foreach ($getbody_measurement as $dateotherattribute) {
                 
                $getbuy_dates = DB::table('nm_order_option_value')->where('product_type','food')->where('cus_id',$cusid)->where('order_id',$oid)->where('product_id',$dateotherattribute->product_id)->get();
                $dateotherattribute->order_option_value=$getbuy_dates;
 
   $getShippingC = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','food')->count();
          $dateotherattribute->shippingMethod = 'N/A';
          $dateotherattribute->shippingPrice = '0';
          if($getShippingC >=1)
          { 
          $getShipping = DB::table('nm_shipping')->where('ship_order_id',$oid)->where('cart_sub_type','food')->first();
          $shipping_type = $getShipping->shipping_type;
          if($shipping_type == 1){ $shipping_type = 'Aramex';}else { $shipping_type = 'Pick Logistics'; }
          $shipping_price =  $getShipping->shipping_price;
          $dateotherattribute->shippingMethod = $shipping_type;
          $dateotherattribute->shippingPrice = $shipping_price;
          }



              }
 
                return view('sitemerchant.dates.dates-orderdetail', compact('merchantheader','merchantfooter','id','hid','sid','getbody_measurement'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
  }
// End For dates Order


// Start For dates Reviews & Comments
    public function datesReviewsComments(Request $request)
     {
        if (Session::has('merchantid')) 
             {  $getPagelimit = config('app.paginate');
               $merid  = Session::get('merchantid');
               $shopid = request()->sid;
        $this->setLanguageLocaleMerchant();
        $mer_id              = Session::get('merchantid');             
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
        $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$shopid)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
    
                return view('sitemerchant.dates.dates-reviews-and-comments', compact('merchantheader','merchantfooter','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   
// End For dates Reviews & Comments


// Start For dates Type
    public function datesType(Request $request)
     {
       if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->itemid;
                
                $id = $request->id;
                $sid = $request->sid;
               


                $getPagelimit = config('app.paginate');
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $status = $request->status;
                $search = $request->search;
                $getdesart = ProductsMerchant::where('pro_mc_id',$sid)->where('pro_mr_id',$mer_id);
                if($status!='')
                {
                $getdesart = $getdesart->where('pro_status', $status);
                }
                if($search!='')
                {
                $getdesart = $getdesart->where('pro_title', 'like', '%'.$search.'%');
                }
                $getdesart = $getdesart->orderBy('pro_id','DESC')->paginate($getPagelimit);                
               return view('sitemerchant.dates.dates-type', compact('merchantheader','merchantfooter','shopid','getCities','getdesart','services_id','branch_id'));     
     
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For dates Type


// Start For Add dates Type
    public function datesAddType(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $shopid = $request->shopid;
                $services_id = $request->services_id;
                $branch_id = $request->itemid;
                $product_id = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
                $attributes = NmServicesAttribute::where('services_id',$branch_id)->get();

                $option = ProductOption::where('services_id','72')->get();
                $option_id='';
           
                foreach ($option  as $key => $value) {

                  if($value->option_title=='Shop by')
                  {
                   $option_id=$value->id;
                  }

                 } 
 
              $optionvalue = NmproductOptionValue::where('product_option_id',$option_id)->get();

            


                 
            $getproductOption = DB::table('nm_product_option_value')->where('vandor_id',$mer_id)->where('product_id',$product_id)->get();

                 $getdesart = ProductsMerchant::where('pro_id',$product_id)->where('pro_mr_id',$mer_id)->first();

                 //print_r($getdesart );
 $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$product_id)->get();
                return view('sitemerchant.dates.dates-add-type', compact('merchantheader','merchantfooter','shopid','getCities','getdesart','services_id','branch_id','attributes','option_id','optionvalue','product_id','getproductOption','productGallery')); 
 
                  
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Add dates Type


// Start For Add dates Add Branch
    public function datesAddBranch(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.dates.dates-add-branch', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
// End For Add dates Add Branch




       //Buffet store branch
 public function storeBranch(Request $request)
    {
            $merid  = Session::get('merchantid');
          //Store Resturant Image
              $sid = $request->shopid;
              $itemid = $request->itemid;
             
 
               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
          if($request->file('branchimage')){ 
                  $file = $request->branchimage;  

                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('datesimage' . '/'. $thumbName);
                  $file->move('datesimage/', $Image);
                  $savebranch->mc_img = url('').'/datesimage/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('datesimage' . '/'. $thumbName);
                  $file->move('datesimage/', $Image);
                  $savebranch->address_image = url('').'/datesimage/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
               }

             //Terms and condition in arabic

                //Termaand condition
               if(!empty($request->mc_tnc_ar)){ 
                  $file = $request->mc_tnc_ar;
                  $orgname_ar =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cnamear =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions_ar =$Cnamear;
                  
               }
               else {
                    $orgname_ar = $request->tmcvalue_ar; 
               }


                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address; 
                  $savebranch ->longitude = $request->longitude;
                  $savebranch ->latitude = $request->latitude;                 
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $sid;
                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
                   $savebranch ->terms_condition_name_ar = $orgname_ar;
                   $savebranch ->mc_status = $request->mc_status;
                

                if($savebranch->save()){
                  $id = $request->parent_id;
                  if($itemid=='')
                  {
                  $itemid =  $savebranch->mc_id;
                  }
                

            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ المتجر بنجاح ");
             }
             else
             {
             Session::flash('message', "Shop successfully saved");
             }

                 return redirect()->route('dates-info',['id' =>$sid,'sid' =>$itemid]);
                }   
            }



     public function StoreDatesPictureVideo(Request $request)
             { 
                if (Session::has('merchantid')) 
                {
 
                    $validatedData = $request->validate([ 
                          'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 

                    $shopid = $request->id;
                    $services_id = $request->sid;
                    
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');             
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                
                  
                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/datesimage' . '/'. $thumbName);
                    $file->move('uploadimage/datesimage/', $fileName);
                    $shop_Img = url('/').'/uploadimage/datesimage/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$services_id,
                    'vendor_id' => $merid,
                    ]);

                   
                    } 
                  }
 

                  $mc_video_description = $request->about_video;
                  $mc_video_description_ar = $request->about_video_ar;
                  $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                  DB::table('nm_category')->where('mc_id',$services_id)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>  $youtubevideoa,'mc_video_description'=>  $mc_video_description,'mc_video_description_ar'=>  $mc_video_description_ar]);

                 if($mc_video_description!='' ||  $mc_video_description_ar!='' || $request->file('image')!='')
                 {
                     // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ السجل بنجاح ");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    }
                    // language for display message //   

                 }

                   



                    return Redirect::back();      
                } else {
                   return Redirect::to('sitemerchant');
                }

             }

 public function dateStoreType(Request $request)
 {

 if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $sid = $request->sid;
 
                $attribute_id =  $request->attribute_id;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();
 
      
                $attributessingal = NmServicesAttribute::where('id',$attribute_id)->first();
                $option_id='';
                $optionvalue='';
                $autoid=$request->proid;


                if($autoid!='') {
                  $dishsave =ProductsMerchant::where('pro_id',$autoid)->first();
                 } else {
                 $dishsave = new ProductsMerchant;
                }
              
             if($request->file('pro_img')){   
                      $file = $request->pro_img;  
                      $extension = $file->getClientOriginalExtension();
                      $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                      $imageRealPath  =   $file->getRealPath();
                      $thumbName      =   'thumb_'. $Image;       
                      $img = Image::make($imageRealPath); // use this if you want facade style code
                      $thumb_width = 150;
                     list($width,$height) = getimagesize($imageRealPath);
                        $thumb_height = ($thumb_width/$width) * $height;
                        $img->resize($thumb_width,$thumb_height);
                        $img->save('dessartimage' . '/'. $thumbName);
                        $file->move('dessartimage/', $Image);
                        $dishsave->pro_Img = url('').'/dessartimage/'.$thumbName;
                 } 
                     $dishsave->pro_mc_id     = $sid;
                     $dishsave->attribute_id  = $attribute_id;                     
                     $dishsave->pro_title    = $request->title;
                     $dishsave->pro_title_ar  = $request->title_ar;
                      $dishsave->pro_desc  = $request->pro_description;
                       $dishsave->pro_desc_ar  = $request->pro_description_ar;
                       $dishsave->pro_weight  = $request->pro_weight;    
               if($request->attribute_id=='34')
                 {

                  $dishsave->option_id='8';

                  }
                  if($request->attribute_id=='35')

                    { 
                        $dishsave->option_id='9';
                         }
                                      
                     $dishsave->pro_mr_id  = $mer_id;
                     $dishsave->pro_qty  = $request->aval_qty !=''? $request->aval_qty : 0;                     
                     $dishsave->pro_status  = 1;
                     $dishsave->save();

                     if($autoid=='') { 
                       $proid = DB::getPdo()->lastInsertId();
                      } else {
                       $proid = $autoid; 
                      }
                   

  

                      $type_perkg = $request->type_perkg;
                      $type_perkg_discount = $request->type_perkg_discount;
                      $type_perpiece = $request->type_perpiece;
                      $type_perpiece_discount = $request->type_perpiece_discount;
                      $dish_per_qty = $request->dish_per_qty;
                      $dish_per_qty_discount = $request->dish_per_qty_discount;
                      $aval_qty = $request->aval_qty;

                       //Per kg 
                       if($type_perkg_discount!='' && $type_perkg_discount!='0') {
                                         $discountprice =   $type_perkg-($type_perkg * ($type_perkg_discount/100));
                                        } else {
                                           $discountprice = 0;

                                        }

                        //Per Dish
                         
                         if($dish_per_qty_discount!='' && $dish_per_qty_discount!='0') {
                                         $dishdiscountprice =   $dish_per_qty-($dish_per_qty * ($dish_per_qty_discount/100));
                                        } else {
                                           $dishdiscountprice = 0;

                                        }



                      DB::table('nm_product_option_value')->where('product_id',$proid)->where('vandor_id', $mer_id)->delete();
                      if($request->attribute_id=='35')
                      {
                      DB::table('nm_product_option_value')->insert([
                      'product_id' =>$proid,
                      'vandor_id' =>$mer_id,
                      'product_option_id' =>9,
                      'option_title'=>'Per Qty',
                      'option_title_ar'=>'لكل الكمية',
                      'value' =>$request->dish_per_qty, 
                      'discount' =>$request->dish_per_qty_discount,
                      'discount_price' =>$dishdiscountprice, 
                      ]);
                      }
                      else
                      {
                      DB::table('nm_product_option_value')->insert([
                      'product_id' =>$proid,
                      'vandor_id' =>$mer_id,
                      'product_option_id' =>'22',
                      'option_title'=>'Per Kilo',
                      'option_title_ar'=>'لكل كيلو',
                      'value' =>$request->type_perkg,
                      'discount' =>$request->type_perkg_discount,
                      'discount_price'=>$discountprice,

                      ]);
                     
                      }



// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
        $files=$request->file('image');
        $newFiles = array();
        $merid  = Session::get('merchantid');
        if($files=$request->file('image')){
        foreach($files as $key=>$val){
        array_push($newFiles, $key);
        }

        $privius=$request->privius;
        $newFilesMatch = array();
        if(isset($privius) && $privius!='')
        {
        foreach($privius as $key=>$val){
        array_push($newFilesMatch, $key);
        }  
        }

        $CheckInfo = array_intersect($newFiles, $newFilesMatch);
        if(isset($privius) && $privius!='')
        {
        foreach($privius as $key=>$val){
        if(in_array($key, $CheckInfo))
        {
        DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
        }
        }
        } 
        foreach($files as $file){
        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111,99999).'_'.time().'.'.$extension;
        $imageRealPath  =   $file->getRealPath();
        $thumbName      =   'thumb_'. $fileName;       
        $img = Image::make($imageRealPath); // use this if you want facade style code
        $thumb_width = 150;
        list($width,$height) = getimagesize($imageRealPath);
        $thumb_height = ($thumb_width/$width) * $height;
        $img->resize($thumb_width,$thumb_height);                  
        $img->save('dessartimage' . '/'. $thumbName);
        $file->move('dessartimage/', $fileName);
        $shop_Img = url('/').'/dessartimage/'.$thumbName; 
        /*Insert data to db*/
        DB::table('nm_product_gallery')->insert( [
        'image'=>   $shop_Img,
        'product_id' =>$proid,
        'status' => 1,
        'vendor_id' => $merid,
        ]);
        }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //



                      
                   if (\Config::get('app.locale') == 'ar'){
                     Session::flash('message', "النوع حفظ بنجاح");
                     }
                     else
                     {
                     Session::flash('message', "Record successfully saved");
                     }

      return redirect()->route('dates-type',['id'=>$id,'sid'=>$sid]);                               
          } else {
                 return Redirect::to('sitemerchant');
            }

 }

      public function storeDatesOffer(Request $request)
       {
             $mer_id   = Session::get('merchantid');
             $id = $request->id;
             $sid = $request->sid;  
             $bid = $request->bid;

             if(isset($request->offer_id) && $request->offer_id !='')
             {
                  $offersave=  HallOffer::where('id','=',$request->offer_id)->first();
             }
             else
             {
                  $offersave = new HallOffer; 

             }
             $offersave->vendor_id  = $mer_id; 
             $offersave->title   =$request->title;
             $offersave->title_ar    = $request->title_ar;
             $offersave->start_date     = $request->Date_from;
             $offersave->end_date    = $request->date_to;
             $offersave->discount   = $request->discount;
              $offersave->coupon   = $request->coupon;
             $offersave->pro_id  = $sid;                
             $offersave->status    = 1;

              if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تمت إضافة العرض بنجاح  ");
             }
             else
             {
             Session::flash('message', "Offer successfully saved");
             }
             if($offersave->save()){  
             
               return redirect()->route('dates-list-offer',['id' => $id,'sid' =>$sid]);
             
             }     
       }

   } 


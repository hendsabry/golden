<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Shoplisthall;
//------------wisitech code end -----------//
class VendorlistController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function vendorlistbycity(Request $request,$halltype,$typeofhallid)
    {
		$halltype=$halltype;
		$hallsizetype = $request->id;
		$within = $request->within;
		$above = $request->above;
		$offer = $request->offer;
		 


		$city_id=Session::get('searchdata.cityid');
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
			 
		  
 
		 $category_id=$typeofhallid;
		 $typeofhallid=$typeofhallid;
		 $budget=Session::get('searchdata.budget');
		 $gender=Session::get('searchdata.gender');
		$lang=Session::get('lang_file');
		//print_r(Session::get('searchdata'));
		$request->session()->push('searchdata.typeofhallid', $category_id);
		
		$Formbusinesstype= new Formbusinesstype();
		if($basecategory_id==''){$basecategory_id=Session::get('searchdata.mainselectedvalue');}
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $value = $request->session()->get('searchdata');
		       $getsubcategories = '';
			   
			   $Shoplisthall= new Shoplisthall();
			 


				if($within == 1 && $above=='' && $offer=='' || $within == '' && $above=='' && $offer=='')
				{
				$shopunderbugetincity=$Shoplisthall->Gethalllistbycityunderbugetgender($category_id,$city_id,$budget,$lang,$gender);
				$shopabovebugetincity= array();
				}
				elseif($within == '' && $above==1 && $offer=='')
				{ 
				$shopunderbugetincity=$Shoplisthall->Getshoplistbycityabovebuget($category_id,$city_id,$budget,$lang);
				$shopabovebugetincity=array();

				}
				elseif($within == '' && $above=='' && $offer==1)
				{ 

				$shopunderbugetincity=$Shoplisthall->Getshoplistbyoffer($category_id,$city_id,$budget,$lang);
				$shopabovebugetincity=array();
				}
				else
				{ 

				$shopunderbugetincity=$Shoplisthall->Getshoplistbyall($category_id,$city_id,$budget,$lang,$within,$above,$offer);
				$shopabovebugetincity=array();
				}

 
 



$shopunderbugetincity=$Shoplisthall->Gethalllistbycityunderbugetgender($category_id,$city_id,$budget,$lang,$gender);
$shopabovebugetincity=array();
$shopofferincity=array();

 if($within == 1 )
 {
$shopunderbugetincity=$Shoplisthall->Gethalllistbycityunderbugetgender($category_id,$city_id,$budget,$lang,$gender);
 }
  if($above == 1 )
 {
$shopabovebugetincity= $Shoplisthall->Getshoplistbycityabovebuget($category_id,$city_id,$budget,$lang);
 }
  if($offer == 1 )
 {
$shopofferincity=$Shoplisthall->Getshoplistbyoffer($category_id,$city_id,$budget,$lang);
 }
 




        return view('vendorlistbycity', compact('getsubcategories','bussinesstype','shopunderbugetincity','shopabovebugetincity','shopofferincity','halltype','typeofhallid','hallsizetype','within','above','offer'));
 
    }
   
}

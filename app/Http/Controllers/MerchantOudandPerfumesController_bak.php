<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\ServiceStaff;
use App\StaffExperties;
use App\HallOffer;
use App\NmProductGallery;

class MerchantOudandPerfumesController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }
    
 
//Add Shop info oudandperfumes

// Start For Popular band info
   
    public function oudandperfumesBranchList(Request $request)
     {
 
             //$q->where('name', 'like', "%{$breed}%");
        if (Session::has('merchantid')) 
        {   
                $getPagelimit = config('app.paginate');
                    $id = request()->id;  
                $status = $request->status;
                $search = $request->input('search');
                $mer_id   = Session::get('merchantid');
                //$alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');
               
                if(Session::get('LoginType') =='4')
                {
                //= CHECK THE MARCHANT ACCESS ==//
                $merchantManagerid = Session::get('merchant_managerid');
                $fetchB = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->get();
                $ChekArr = array();
                foreach($fetchB as $chekB)
                {
                $mcid  = $chekB->mc_id;
                $fetchBC = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->count();
                if($fetchBC >=1)
                {
                $BId = Categorylist::where('parent_id',$mcid)->where('vendor_id',$mer_id)->where('branch_manager_id',$merchantManagerid)->first();
                $CP= $BId->parent_id;
                array_push($ChekArr, $CP);
                }

                } 
                $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->whereIn('mc_id', $ChekArr)->orderBy('mc_id','DESC');
                //= CHECK THE MARCHANT ACCESS ==//
                }
                else
                {
                $alldata = Categorylist::where('parent_id',$id)->where('vendor_id',$mer_id)->orderBy('mc_id','DESC');
                }



                if($search !='')
                {
                $mc_name='mc_name';
                $mer_selected_lang_code = Session::get('mer_lang_code'); 
                if($mer_selected_lang_code !='en')
                {
                $mc_name= 'mc_name_'.$mer_selected_lang_code;
                }
                $alldata = $alldata->where('nm_category.'.$mc_name,'like', '%' .$search. '%');
                }


                if($status !='')
                {
                $alldata = $alldata->where('mc_status','=',$status);
                }

                $alldata=$alldata->orderBy('mc_id', 'DESC')->paginate($getPagelimit)->appends(request()->query());
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                return view('sitemerchant.oudandperfumes.oudandperfumes-shop', compact('merchantheader','merchantfooter','alldata','id','search'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
   


    public function oudandperfumesShopInfo(Request $request)
     {
        if (Session::has('merchantid')) 
             {   $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                 $getPagelimit = config('app.paginate');
                    $status = $request->status;
                    $search = $request->search;
                    $cityname = $request->cityname;

            //$productdata = Categorylist::where('parent_id',$hid)->where('vendor_id',$mer_id);

           if(Session::get('LoginType') =='4')
           {
            //= CHECK THE MARCHANT ACCESS ==//
             $merchantManagerid = Session::get('merchant_managerid');
            $productdata = Categorylist::where('parent_id',$hid)->where('branch_manager_id',$merchantManagerid)->where('vendor_id',$mer_id);
             //= CHECK THE MARCHANT ACCESS ==//
            }
            else
            {
             $productdata = Categorylist::where('parent_id',$hid)->where('vendor_id',$mer_id);  
            }
 


            if($status!='')
            {
            $productdata = $productdata->where('mc_status',$status);
            }
            if($search !='')
            {
            $productdata = $productdata->where('mc_name','like','%'.$search.'%');
            }
            if($cityname !='')
            {
            $productdata = $productdata->where('city_id',$cityname);
            }
            $productdata = $productdata->orderBy('mc_id','desc')->paginate($getPagelimit)->appends(request()->query());


                return view('sitemerchant.oudandperfumes.oudandperfumes-shop-branch', compact('merchantheader','merchantfooter','id','productdata'));   
             } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function oudandperfumesAddShop(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = request()->hid;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');   

                $getlisting = Categorylist::where('mc_id', $hid)->where('vendor_id',$mer_id)->first(); 
                return view('sitemerchant.oudandperfumes.oudandperfumes-add-shop', compact('merchantheader','merchantfooter','id','hid','getlisting'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function oudandperfumesManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.oudandperfumes.oudandperfumes-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 

    public function oudandperfumesServices(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $itemid = $request->itemid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');
                $category = $request->category;
                $status = $request->status;
                $search = $request->input('search');


                $allservice =  ProductsMerchant::where('pro_mc_id', $itemid)->where('packege','!=', 'yes')->where('pro_mr_id',$mer_id);
                if($search !='')
                {
                    $mc_name='pro_title';
                    $mer_selected_lang_code = Session::get('mer_lang_code'); 
                    if($mer_selected_lang_code !='en')
                    {
                    $mc_name= 'pro_title_ar';
                    }
                    $allservice = $allservice->where($mc_name,'like', '%' .$search. '%');
                }

               if($category!='')
                {
               $allservice = $allservice->where('attribute_id',$category);

                }

                if($status !='')
                {
                     $allservice = $allservice->where('pro_status','=',$status);
                }

                 $allservice=$allservice->orderBy('pro_id','desc')->paginate($getPagelimit); 
 
 
           return view('sitemerchant.oudandperfumes.oudandperfumes-service', compact('merchantheader','merchantfooter', 'allservice','category')); 
  
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 
    public function oudandperfumesAddServices(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                 

                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                 $serviceid = $request->serviceid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');                 
                $getservice =  ProductsMerchant::where('pro_id', $serviceid)->where('pro_mr_id',$mer_id)->first(); 
$productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$serviceid)->get();

                return view('sitemerchant.oudandperfumes.oudandperfumes-add-service', compact('merchantheader','merchantfooter','getservice','productGallery'));
 
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		 	  
	 
	 
    public function oudandperfumesAddManager(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getCities = DB::table('nm_city')->where('ci_con_id',10)->where('ci_status',1)->orderBy('ci_name','ASC')->get();

                $getSinger = DB::table('nm_music')->where('vendor_id', $mer_id)->where('category_type', 'band')->where('category_id', $id)->first();
 
                return view('sitemerchant.oudandperfumes.oudandperfumes-add-manager', compact('merchantheader','merchantfooter','id','getCities','getSinger','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 	
	 public function oudandperfumesReview(Request $request)
     {
        if(Session::has('merchantid')) 
        {
                $mer_id          = Session::get('merchantid');
                $id              = $request->id;
                $getPagelimit    = config('app.paginate');
                $hid             = $request->hid;
                $itemid          = $request->itemid;
                $merchantheader  = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter  = view('sitemerchant.includes.merchant_footer'); 
                $reviewrating    = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$itemid)->where('status',1)->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.oudandperfumes.oudandperfumes-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
        } 
        else 
        {
           return Redirect::to('sitemerchant');
        }
     }	
	 
	 
	  public function oudandperfumesWorkerReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.oudandperfumes.worker-review', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		   
	 	
		
		
    public function oudandperfumesPictures(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $merid   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $itemid = request()->itemid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $proid = $request->id;
                $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();
                return view('sitemerchant.oudandperfumes.oudandperfumes-picture', compact('merchantheader','merchantfooter','id','getDb','getVideos','hid')); 
       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
	 
    public function oudandperfumesAddServiceCatogory(Request $request)
     {
        if (Session::has('merchantid')) 
             { 

                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $itemid  = $request->itemid;
                $catitemid  = $request->catitemid;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');              
                 $getAttr= DB::table('nm_services_attribute')->where('id',$catitemid)->where('vendor_id',$mer_id)->first();  
                return view('sitemerchant.oudandperfumes.oudandperfumes-add-service-catogory', compact('merchantheader','merchantfooter','id','getAttr'));  






            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	 
	 public function oudandperfumesOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               $getPagelimit = config('app.paginate');
                $id = $request->id;
                $hid = $request->hid;
                $itemid = $request->itemid;
                $status = $request->status;
                $search = $request->search;

                $mer_id   = Session::get('merchantid');
            $hallofferlist =  HallOffer::where('pro_id',$itemid);
            if($status!='')
            {
            $hallofferlist = $hallofferlist->where('status',$status);
            }
            if($search !='')
            {
            $mer_selected_lang_code = Session::get('mer_lang_code'); 
                if($mer_selected_lang_code !='en')
                {
                $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
                }
                else
                {
                 $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');
       
                }
            }   
            $hallofferlist =  $hallofferlist->where('vendor_id',$mer_id)->orderBy('id','DESC')->paginate($getPagelimit);

  




                $this->setLanguageLocaleMerchant();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
            return view('sitemerchant.oudandperfumes.oudandperfumes-offer', compact('merchantheader','merchantfooter','hallofferlist','status','itemid','bid','search'));  
                  
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	  public function oudandperfumesAddOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             { $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $itemid = $request->itemid;
                 $serviceid = $request->serviceid;  
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $categorysave = HallOffer::where('pro_id',$itemid)->where('id',$serviceid)->where('vendor_id',$mer_id)->first();     
 
                return view('sitemerchant.oudandperfumes.oudandperfumes-add-offer', compact('merchantheader','merchantfooter','id','categorysave','hid'));   
   
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 

	 
	 
	 
    public function oudandperfumesServiceCatogory(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $getPagelimit = config('app.paginate'); 
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                  $cid = $request->itemid; 
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $status = $request->status;
                $search = $request->search;
                $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $cid);

                if($search !='')
                {
                $getAttr = $getAttr->where('attribute_title', 'like', '%'.$search.'%');         
                }
                if($status !='')  
                {  
                $getAttr = $getAttr->where('status', '=', $status);        
                }  

                $getAttr = $getAttr->orderBy('id','DESC')->paginate($getPagelimit);

 
                return view('sitemerchant.oudandperfumes.oudandperfumes-service-category', compact('merchantheader','merchantfooter','id','getAttr','hid'));  
 
               
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	 
	  public function oudandperfumesPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;         
                $itemid = $request->itemid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');

                $status = $request->status;
                $search = $request->input('search');
                $allservice =  ProductsMerchant::where('pro_mc_id', $itemid)->where('packege', 'yes')->where('pro_mr_id',$mer_id); 
                if($search !='')
                {
                    $mc_name='pro_title';
                    $mer_selected_lang_code = Session::get('mer_lang_code'); 
                    if($mer_selected_lang_code !='en')
                    {
                    $mc_name= 'pro_title_ar';
                    }
                    $allservice = $allservice->where($mc_name,'like', '%' .$search. '%');
                }

                if($status !='')
                {
                     $allservice = $allservice->where('pro_status','=',$status);
                }
                $allservice=$allservice->orderBy('pro_id','DESC')->paginate($getPagelimit);
                 return view('sitemerchant.oudandperfumes.oudandperfumes-package', compact('merchantheader','merchantfooter','allservice'));
        
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	  public function oudandperfumesWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $itemid = $request->itemid;

                $status = $request->status;
                $search = $request->search;
                $getPagelimit = config('app.paginate');
                $mer_id   = Session::get('merchantid');
                $alldata        = ServiceStaff::where('service_id',$itemid);
                  if($status != '')
                 {
                  $alldata = $alldata->where('status',$status);
                  }
                  if($search != '')
                  {
                      
                  $alldata = $alldata->where('staff_member_name','like', '%'.$search.'%');
                 }

                 $alldata = $alldata->orderBy('id','desc')->paginate($getPagelimit)->appends(request()->query());



               // $alldata = ServiceStaff::where('shop_id',$sid)->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               return view('sitemerchant.oudandperfumes.oudandperfumes-worker', compact('merchantheader','merchantfooter','id','sid','alldata','search'));  
                     
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 public function oudandperfumesAddWorker(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid'); 
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                $id = request()->id; 
                $hid = request()->hid;  
                $itemid = request()->itemid;  
                $workerid = $request->workerid; 
          
  
                 
                 $procuctidcount = StaffExperties::select('product_id')->where('staff_id',$workerid)->count();
                  if($procuctidcount) {
                 $procuctid = StaffExperties::select('product_id')->where('staff_id',$workerid)->get();
                 foreach($procuctid as $val ) {

                            $proid[]  = $val->product_id;
                        }
                    } else
                    {    $proid = array();
                         
                    }
             
          
                $fetchdata =ServiceStaff::where('id',$workerid)->first();

                

                 $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $itemid)->get();
           
 
                $getPkg = DB::table('nm_product_to_packeg')->where('packege_id', '=', $itemid)->get(); 



 
                return view('sitemerchant.oudandperfumes.oudandperfumes-add-worker',  compact('merchantheader','merchantfooter','id','sid','getAttr','fetchdata','proid','autoid','getPkg'));     
     
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 public function oudandperfumesOrder(Request $request)
     {
        if(Session::has('merchantid')) 
        {
          $subproducttype   = 'perfume';
          $mer_id           = Session::get('merchantid');
          $id               = $request->id;                 
          $sid              = $request->sid;
          $searchkeyword    = $request->searchkeyword;
          $date_to          = $request->date_to;
          $End_Date         = $request->from_to;
          $order_days       = $request->order_days;
          $status           = $request->status;                
          $serachfirstfrm   = $request->serachfirstfrm;
          
          $getorderedproducts=DB::table('nm_order_product')->where('product_type','shopping')->where('product_sub_type','perfume');
          if($searchkeyword!='')
          {
             $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
          }
          if($status!='')
          {
            $getorderedproducts = $getorderedproducts->where('status',$status);
          } 
          if($date_to!='' && $End_Date!='')
          {
            $getorderedproducts=$getorderedproducts->whereDate('created_at','>=',$date_to)->whereDate('created_at','<=',$End_Date);
          }
          $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();
          
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer');

          return view('sitemerchant.oudandperfumes.oudandperfumes-order', compact('merchantheader','merchantfooter','id','hid','getorderedproducts','mer_id','searchkeyword'));       
        } 
        else 
        {
           return Redirect::to('sitemerchant');
        }
     }	
	 
	 
	     //ORDER DETAILS

         public function getOudperfumeorderdetail(Request $request)
         {
           if(Session::has('merchantid')) 
           {
             $mer_id    = Session::get('merchantid');
             $id        = $request->id;
             $sid       = $request->hid;
             $proid     = $request->proid;
             $cusid     = $request->cusid;
             $productid = $request->productid;
             $ordid     = $request->ordid;
             $autoid    = $request->autoid;
             $branchid=$request->itemid;
             $productdata = DB::table('nm_order_product')->where('product_type','shopping')->where('order_id',$ordid)->where('category_id',$branchid)->where('product_sub_type','perfume')->where('cus_id',$cusid)->where('order_id',$ordid)->orderBy('created_at','DESC')->get();
             $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
             $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
             return view('sitemerchant.oudandperfumes.oudperfumeorderdetail', compact('merchantheader','merchantfooter','id','sid','proid','cusid','productid','ordid','autoid','productdata'));       
           } 
           else 
           {
             return Redirect::to('sitemerchant');
           }
      }
        
         
	 
	 
	 	  public function oudandperfumesAddBranch(Request $request)
            {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');                
 
                return view('sitemerchant.oudandperfumes.oudandperfumes-add-branch', compact('merchantheader','merchantfooter','id','hid'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 public function oudandperfumesAddPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                 $itemid = request()->itemid;  
                $serviceid = request()->serviceid;  
                $getservice =  ProductsMerchant::where('pro_id', $serviceid)->where('packege', 'yes')->where('pro_mr_id',$mer_id)->first();
                $getAttr = DB::table('nm_services_attribute')->where('vendor_id', '=', $mer_id)->where('services_id', '=', $itemid)->get(); 
                $getPkg = DB::table('nm_product_to_packeg')->where('packege_id', '=', $serviceid)->get();                
                return view('sitemerchant.oudandperfumes.oudandperfumes-add-package', compact('merchantheader','merchantfooter','getAttr','getPkg','getservice'));
      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 	 	 	 
	 			 
	 
   
   
    public function oudandperfumesshop(Request $request)
     { 
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;
                $itemId = request()->itemid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    $getbranchinfo = Categorylist::where('mc_id',$itemId)->where('vendor_id',$mer_id)->first();
 
                return view('sitemerchant.oudandperfumes.oudandperfumes-shop-info', compact('merchantheader','merchantfooter','id','getbranchinfo'));   
 
                 
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
   
   

     public function saveoudandperfumesservice(Request $request)
        {    
          

              $merid  = Session::get('merchantid');
                if($request->mc_img){ 
                $file             = $request->mc_img; 
                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;
                //$file->move('hallpics', $fileName);               
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); // use this if you want facade style code
 
                $thumb_width = 100;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height);
 
                 $img->save('beautyupload' . '/'. $thumbName);
                $file->move('beautyupload/', $Adressimgname);
                $Adressimgnames = url('').'/beautyupload/'.$thumbName;
                }
                else
                {
                $Adressimgnames = $request->mcimg; 
                }
                 if($request->hid!='')
                 {   
                    $pid = $request->hid;  
                    $saveservice = Categorylist::where('mc_id',$pid)->where('vendor_id',$merid)->first();
                 }
                 else
                 {
                   $saveservice = new Categorylist; 
                   $saveservice->mc_status = 1;
                 }
                
                $saveservice ->parent_id = $request->parent_id;
                $saveservice ->mc_name = $request->mc_name;
                $saveservice ->mc_name_ar = $request->mc_name_ar;
                $saveservice ->mc_discription = $request->description;
                $saveservice ->mc_discription_ar = $request->description_ar;
                $saveservice ->vendor_id = $merid;
                $saveservice ->mc_img = $Adressimgnames; 
                
                if($saveservice->save()){
                 $id = $request->parent_id;
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الفندق بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Shop successfully saved");
                    }
              
                }
             return redirect()->route('oudandperfumes-shop',['id' => $id]);
            
        }



public function addoudandperfumesbranch(Request $request)
{

if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $hid = $request->hid;  
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                if($request->itemid!='')
                 {   
                    $pid = $request->itemid;  
                    $saveservice = Categorylist::where('mc_id',$pid)->where('vendor_id',$mer_id)->first();
                 }
                 else
                 {
                   $saveservice = new Categorylist; 
                 }

                if($request->mc_img){ 
                $file             = $request->mc_img; 
                $extension = $file->getClientOriginalExtension();
                $Adressimgname = rand(11111,99999).'_'.time().'.'.$extension;           
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname;       
                $img = Image::make($imageRealPath); 
                $thumb_width = 100;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height); 
                 $img->save('uploadimage/oudandperfumesupload' . '/'. $thumbName);
                $file->move('uploadimage/oudandperfumesupload/', $Adressimgname);
                $Adressimgnames = url('').'/uploadimage/oudandperfumesupload/'.$thumbName;
                }
                else
                {
                $Adressimgnames = $request->mc_imgname; 
                }



                if($request->hall_addressimg){ 
                $file             = $request->hall_addressimg; 
                $extension = $file->getClientOriginalExtension();
                $Adressimgname_t = rand(11111,99999).'_'.time().'.'.$extension;           
                $imageRealPath  =   $file->getRealPath();
                $thumbName      =   'thumb_'. $Adressimgname_t;       
                $img = Image::make($imageRealPath); 
                $thumb_width = 100;
                list($width,$height) = getimagesize($imageRealPath);
                $thumb_height = ($thumb_width/$width) * $height;
                $img->resize($thumb_width,$thumb_height); 
                 $img->save('uploadimage/oudandperfumesupload' . '/'. $thumbName);
                $file->move('uploadimage/oudandperfumesupload/', $Adressimgname_t);
                $Adressimgname_tt = url('').'/uploadimage/oudandperfumesupload/'.$thumbName;
                }
                else
                {
                $Adressimgname_tt = $request->mc_addimgname; 
                }

                $saveservice ->address_image = $Adressimgname_tt;
                $saveservice ->google_map_address = $request->google_map_address;

                if($request->tnc){ 
                $file     = $request->tnc; 
                $TNCname =   $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $tncs = rand(11111,99999).'_'.time().'.'.$extension;           
                $imageRealPath  =   $file->getRealPath();                  
                $file->move('uploadimage/oudandperfumesupload/', $tncs);
                $tncs_pic = url('').'/uploadimage/oudandperfumesupload/'.$tncs;
                $saveservice ->terms_conditions = $tncs_pic; 
                $saveservice ->terms_condition_name = $TNCname; 
                }
                else
                {
                $tncs_pic = $request->tnc_img;         
                $saveservice ->terms_conditions = $tncs_pic; 
                } 

                if($request->mc_tnc_ar){ 
                $file     = $request->mc_tnc_ar; 
                $TNCname_ar =   $file->getClientOriginalName();  
                $extension = $file->getClientOriginalExtension();
                $tncs_ar = rand(11111,99999).'_'.time().'.'.$extension;           
                $imageRealPath  =   $file->getRealPath();                  
                $file->move('uploadimage/oudandperfumesupload/', $tncs_ar);
                $tncs_pic_ar = url('').'/uploadimage/oudandperfumesupload/'.$tncs_ar;
                $saveservice->terms_conditions_ar = $tncs_pic_ar; 
                $saveservice->terms_condition_name_ar = $TNCname_ar; 
                }
                else
                {
                $tncs_pic_ar = $request->tnc_img_ar;         
                $saveservice ->terms_conditions_ar = $tncs_pic_ar; 
                } 
                $saveservice ->parent_id = $hid;

  $saveservice ->longitude = $request->longitude;
                  $saveservice ->latitude = $request->latitude;
                
                $saveservice ->branch_manager_id = $request->manager;
                $saveservice ->city_id = $request->city_id;
                $saveservice ->mc_name = $request->branchname;
                $saveservice ->mc_name_ar = $request->branchname_ar;
                $saveservice ->mc_discription = $request->description;
                $saveservice ->mc_discription_ar = $request->description_ar;
                $saveservice ->address_ar = $request->address_ar;
                $saveservice ->address = $request->address;
                $saveservice ->vendor_id = $mer_id;
                $saveservice ->mc_img = $Adressimgnames; 
               
                $saveservice->mc_status = 1;  
                if($saveservice->save()){
                    $itemid = $saveservice->mc_id;                 
                if (\Config::get('app.locale') == 'ar'){
                Session::flash('message', "تمت إضافة الفندق بنجاح");
                }
                else
                {
                Session::flash('message', "Shop successfully saved");
                }
                } 
 
                 return redirect()->route('oudandperfumes-shop-info',['id' => $id,'hid' => $hid,'itemid' => $itemid]);
        
            } else {
               return Redirect::to('sitemerchant');
            }

 
}

   public function oudandperfumesUpdatecategory(Request $request)
     {

        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
 
                $id = $request->id;
                $cid = $request->hid;
                $itemid = $request->itemid;
                $serviceid = $request->catitemid;

                $categoryname = $request->category_name;
                $categoryname_ar = $request->category_name_ar;
                $categorydescription = $request->category_description;
                $categorydescription_ar = $request->category_description_ar;
                $status = 1;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');

                 if($request->file('catimage')){ 
                  $file = $request->catimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                   $img->save('uploadimage/categoryimage' . '/'. $thumbName);
                   $file->move('uploadimage/categoryimage/', $Image);
                   $fileimage = url('').'/uploadimage/categoryimage/'.$thumbName;
                } else {

                    $fileimage =  $request->updatecatimage;
                }

                if($serviceid =='')
                {
            // insert attributes into table
                DB::table('nm_services_attribute')->insert( [
                'attribute_title' => $categoryname,
                'attribute_title_ar' => $categoryname_ar,
                'attribute_description'=> $categorydescription,
                'attribute_description_ar'=>$categorydescription_ar, 
                'services_id' =>$itemid,
                'parent' =>$cid,
                'vendor_id' =>$mer_id, 
                'status' =>$status,
                'image' =>$fileimage,            
                ]);
            // insert attributes into table


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully saved");
                    }
                    // language for display message // 

                } 
                else
                {


                DB::table('nm_services_attribute')->where('id',$serviceid)->where('services_id',$itemid)->where('vendor_id',$mer_id)->update( ['attribute_title'=>  $categoryname,'attribute_title_ar'=>  $categoryname_ar,'attribute_description'=>  $categorydescription,'attribute_description_ar'=> $categorydescription_ar,'image' =>$fileimage, ]);  


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Category successfully updated");
                    }
                    // language for display message // 

                }
 
                return redirect()->route('oudandperfumes-service-category', ['id' => $id,'hid' =>$cid,'itemid' =>$itemid]);

       
            } else {
                return Redirect::to('sitemerchant');
            }
     }



    public function storeoudandperfumesservice(Request $request)
    {
      if (Session::has('merchantid')) 
        {
        $mer_id   = Session::get('merchantid');

        $serviceid = $request->serviceid;

        if($serviceid != '')
        {
        $saveservice =  ProductsMerchant::where('pro_id', $serviceid)->where('pro_mr_id',$mer_id)->first();
        }
        else
        {
        $saveservice = new ProductsMerchant;
        }

        if($request->file('stor_img')){ 
        $file = $request->stor_img;                     
        $extension = $file->getClientOriginalExtension();
        $Image = rand(11111,99999).'_'.time().'.'.$extension;               
        $imageRealPath  =   $file->getRealPath();
        $thumbName      =   'thumb_'. $Image;       
        $img = Image::make($imageRealPath); // use this if you want facade style code
        $thumb_width = 272;
        list($width,$height) = getimagesize($imageRealPath);
        $thumb_height = ($thumb_width/$width) * $height;
        $img->resize($thumb_width,$thumb_height);
        $img->save('uploadimage/oudandperfumesupload' . '/'. $thumbName);
        $file->move('uploadimage/oudandperfumesupload/', $Image);
        $saveservice->pro_Img = url('').'/uploadimage/oudandperfumesupload/'.$thumbName;
        }
        $saveservice->pro_title = $request->service_name;
        $saveservice->pro_title_ar = $request->service_name_ar;
        $saveservice->attribute_id  = $request->category;
        $saveservice->pro_mc_id  = $request->itemid;
        $saveservice->pro_price = $request->amount;
        $saveservice->pro_discount_percentage = $request->discount;
        $saveservice->about = $request->description;
        $saveservice->about_ar = $request->description_ar;
        $saveservice->pro_mr_id = $mer_id;

        $saveservice->pro_qty = $request->quantity;
         
          $price =  $request->amount;
          $discount = $request->discount;

          if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                    }   
        $saveservice->pro_disprice = $discountprice;
        
        $saveservice->pro_status = 1;

        
        $id = $request->id;
        $hid = $request->hid;
        $itemid = $request->itemid; 

        $saveservice->save();



$getProID = $saveservice->pro_id;




// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
        $files=$request->file('image');
        $newFiles = array();
        $merid  = Session::get('merchantid');
        if($files=$request->file('image')){
        foreach($files as $key=>$val){
        array_push($newFiles, $key);
        }

        $privius=$request->privius;
        $newFilesMatch = array();
        if(isset($privius) && $privius!='')
        {
        foreach($privius as $key=>$val){
        array_push($newFilesMatch, $key);
        }  
        }

        $CheckInfo = array_intersect($newFiles, $newFilesMatch);
        if(isset($privius) && $privius!='')
        {
        foreach($privius as $key=>$val){
        if(in_array($key, $CheckInfo))
        {
        DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
        }
        }
        } 
        foreach($files as $file){
        $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111,99999).'_'.time().'.'.$extension;
        $imageRealPath  =   $file->getRealPath();
        $thumbName      =   'thumb_'. $fileName;       
        $img = Image::make($imageRealPath); // use this if you want facade style code
        $thumb_width = 272;
        list($width,$height) = getimagesize($imageRealPath);
        $thumb_height = ($thumb_width/$width) * $height;
        $img->resize($thumb_width,$thumb_height);                  
        $img->save('uploadimage/oudandperfumesupload' . '/'. $thumbName);
        $file->move('uploadimage/oudandperfumesupload/', $fileName);
        $shop_Img = url('/').'/uploadimage/oudandperfumesupload/'.$thumbName; 
        /*Insert data to db*/
        DB::table('nm_product_gallery')->insert( [
        'image'=>   $shop_Img,
        'product_id' =>$getProID,
        'status' => 1,
        'vendor_id' => $merid,
        ]);
        }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //




        // language for display message //
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "حفظ السجل بنجاح");
        }
        else
        {
        Session::flash('message', "Record successfully saved");        }
        // language for display message //   
        return redirect()->route('oudandperfumes-service',['id' => $id,'hid' => $hid,'itemid' => $itemid]);
      } else {
      return Redirect::to('sitemerchant');
      }


    }
 
public function storeoudandperfumespackageservice(Request $request)
    {
    if (Session::has('merchantid')) 
    {
        $mer_id   = Session::get('merchantid');

        $serviceid = $request->serviceid;

        if($serviceid != '')
        {
        $saveservice =  ProductsMerchant::where('pro_id', $serviceid)->where('packege', 'yes')->where('pro_mr_id',$mer_id)->first();
        }
        else
        {
        $saveservice = new ProductsMerchant;
         $saveservice->pro_status = 1;
        }

        if($request->file('img'))
        { 
        $file = $request->img;                     
        $extension = $file->getClientOriginalExtension();
        $Image = rand(11111,99999).'_'.time().'.'.$extension;               
        $imageRealPath  =   $file->getRealPath();
        $thumbName      =   'thumb_'. $Image;       
        $img = Image::make($imageRealPath); // use this if you want facade style code
        $thumb_width = 150;
        list($width,$height) = getimagesize($imageRealPath);
        $thumb_height = ($thumb_width/$width) * $height;
        $img->resize($thumb_width,$thumb_height);
        $img->save('uploadimage/oudandperfumesupload' . '/'. $thumbName);
        $file->move('uploadimage/oudandperfumesupload/', $Image);
        $saveservice->pro_Img = url('').'/uploadimage/oudandperfumesupload/'.$thumbName;
        }

        $saveservice->packege = 'yes';
        $saveservice->pro_title = $request->title;
        $saveservice->pro_title_ar = $request->title_ar; 
        $saveservice->pro_mc_id  = $request->itemid;
        $saveservice->service_hour  = $request->duration;
        $saveservice->pro_price = $request->pro_price;
        $saveservice->pro_discount_percentage = $request->discount;
        

        $saveservice->pro_mr_id = $mer_id;
        $id = $request->id;
        $hid = $request->hid;
        $itemid = $request->itemid;     
        $saveservice->save();
        if($request->cid !='')
        {
        $packeg_id=$request->cid;
        }
        else
        {
        $packeg_id=$saveservice->pro_id;
        }
        DB::table('nm_product_to_packeg')->where('packege_id',$packeg_id)->delete();             
        $iteminpkg = $request->productitems;

        foreach ($iteminpkg as $key => $value) {
        DB::table('nm_product_to_packeg')->insertGetId( [
        'shop_id' => $itemid,
        'attribute_id' => 0,
        'item_id'=> $value,
        'packege_id'=>$packeg_id, 
        ]);
        } 
        // language for display message //
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "حفظ السجل بنجاح");
        }
        else
        {
        Session::flash('message', "Record successfully saved");
        }
        // language for display message //   

        return redirect()->route('oudandperfumes-package',['id' => $id,'hid' => $hid,'itemid' => $itemid]);
    } else {
        return Redirect::to('sitemerchant');
    }

    }

     
public function storeoudandperfumesworker(Request $request)
{


if (Session::has('merchantid')) 
      {      
        $mer_id   = Session::get('merchantid');
        $id = $request->id;
        $hid = $request->hid;
        $itemid = $request->itemid;
        $autoid = $request->workerid;
         

            if($autoid){
            $saveworker =ServiceStaff::where('id',$autoid)->first();
            } else { 
            $saveworker = new ServiceStaff;
             $saveworker ->status =   1;
            }
            $saveworker ->staff_member_name = $request->name;
            $saveworker ->staff_member_name_ar = $request->name_ar;
            $saveworker ->experience = $request->year_of_exp;
            $saveworker ->service_id =$itemid;
            $saveworker ->shop_id =   $hid;
           

            if($request->file('stor_img')){ 
            $file = $request->stor_img;  
            $merid  = Session::get('merchantid');
            $extension = $file->getClientOriginalExtension();
            $Image = rand(11111,99999).'_'.time().'.'.$extension;               
            $imageRealPath  =   $file->getRealPath();
            $thumbName      =   'thumb_'. $Image;       
            $img = Image::make($imageRealPath); // use this if you want facade style code
            $thumb_width = 150;
            list($width,$height) = getimagesize($imageRealPath);
            $thumb_height = ($thumb_width/$width) * $height;
            $img->resize($thumb_width,$thumb_height);
            $img->save('beautyupload' . '/'. $thumbName);
            $file->move('beautyupload/', $Image);
            $saveworker->image = url('').'/beautyupload/'.$thumbName;
            }
            if($saveworker->save()){

            $pro_id = $saveworker->id;
            $productitems1 = $request->productitems;


            StaffExperties::where('staff_id',$pro_id)->delete();
            foreach($productitems1 as $val) {
            $saveitem = new StaffExperties;
            $saveitem->staff_id = $pro_id;
            $saveitem->product_id = $val;
            $saveitem->save();
            }
            }  

 


        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تعامل حفظ بنجاح  ");
        }
        else
        {
        Session::flash('message', "Worker successfully saved");
        }
        
        return redirect()->route('oudandperfumes-worker',['id' => $id,'hid' => $hid,'itemid' => $itemid]);
       } else {
      return Redirect::to('sitemerchant');
      }

}



public function beautyShopPicture(Request $request)
{ 

                if (Session::has('merchantid')) 
                {

                    $validatedData = $request->validate([  
                         'image.*' => 'image|mimes:jpg,jpeg,png,gif'
 
                    ]); 
 

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');             
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                 
                            $catid = $request->id;
                            $sid = $request->hid;
                            $itemid = $request->itemid;


                  
                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/oudandperfumesupload' . '/'. $thumbName);
                    $file->move('uploadimage/oudandperfumesupload/', $fileName);
                    $shop_Img = url('/').'/uploadimage/oudandperfumesupload/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$itemid,
                    'vendor_id' => $merid,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = $request->about_video;
                    $mc_video_description_ar = $request->about_video_ar;

                       
                    /*Update data to db*/
                    DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);
                   
                     if($mc_video_description != '' || $mc_video_description_ar != '' || $youtubevideoa != '' || $request->file('image') !='')
                      {
                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "حفظ السجل بنجاح ");                   }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    } }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }

             }


public function storeoudandperfumesoffer(Request $request)
{

if (Session::has('merchantid')) 
      {      
        $mer_id   = Session::get('merchantid');
        $id = $request->id;
        $hid = $request->hid;
        $itemid = $request->itemid;
        $serviceid = $request->serviceid;
        if($serviceid =='')
        {
        $categorysave = new HallOffer; 
         $categorysave->status    = 1;
        }
        else
        {
        $categorysave = HallOffer::where('pro_id',$itemid)->where('id',$serviceid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id  = $mer_id; 
        $categorysave->title   =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $itemid;
       
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('oudandperfumes-offer',['id' => $id,'hid' => $hid,'itemid' => $itemid]);
       } else {
      return Redirect::to('sitemerchant');
      }

}


   
 } // end 



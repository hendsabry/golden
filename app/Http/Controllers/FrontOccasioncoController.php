<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Category;
use App\Products;
use App\City;
use App\ServiceAttribute;
use App\CategoryGallery;
use App\ProductAttribute;
use App\ProductOption;
use App\ProductOptionValue;
use App\ProductPackage;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartProductRent;
use App\CartInvitation;
use App\CartServiceAttribute;
use App\CartHospitality;
use App\StaffNationality;
use App\CartProductPackage;
use Auth;
use App\User;
use Response;
use Storage;
use App\Formbusinesstype;
//------------wisitech code end -----------//
class FrontOccasioncoController extends Controller
{  
        public function __construct(){
        parent::__construct();
        $this->middleware(function ($request, $next) {
        $this->setLanguageLocaleFront();
        return $next($request);
        });      
        }

        public function occasionCoordinator(Request $request)
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
              $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else
            {
              $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
          return view('occasioncoordinator.occasion-co-ordinator',compact('bussinesstype'));
        }

        //DISPLAY ALL CATEGORY VENDORS // 
        public function manageVendors(Request $request)
        {   
            try
            {
                $category_id    = request()->id;
                $subcategory_id = request()->sid;            
                $CityID         = Session::get('searchdata.cityid');
                $lang           = Session::get('lang_file');
                $User           = Auth::user();
                $category       = Category::select('mc_id','mc_name','mc_name_ar','mc_status')->where('mc_id',$category_id)->where('mc_status',1)->first(); 
               

                $vendor =array();
                //$ChkList = \Helper::checkShopAvilabe($subcategory_id, $CityID);
 
 
                if(!empty($category))
                {
                 if($subcategory_id != '' && $lang != 'en_lang') 
                 {
                   $sub_catg = Category::where('mc_status',1)->where('mc_id',$subcategory_id)->select('mc_id','mc_name_ar')->first(); 
                   $vendor = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_img','vendor_id', 'budget_in_percentages','city_id','mc_name as name')->where('mc_status', '=', 1)->Where('parent_id', '=', $subcategory_id)->Where('city_id', '=', $CityID)->with('getCity:ci_id,ci_name_ar as ci_name')->orderBy('name', 'ASC')->paginate(9);                       

                 } 
                 else 
                 {
                    $sub_catg = Category::where('mc_status',1)->where('mc_id',$subcategory_id)->select('mc_id', 'mc_name')->first();
                    $vendor = Category::select('mc_id', 'mc_name', 'mc_img','vendor_id', 'budget_in_percentages','city_id')->where('mc_status', '=', 1)->Where('parent_id', '=', $subcategory_id)->Where('city_id', '=', $CityID)->with('getCity:ci_id,ci_name')->orderBy('mc_name', 'ASC')->paginate(9);
                }

                foreach($vendor as $vendorproductcategory) 
                {
                    $Getcategories = DB::table('nm_services_attribute')->where('services_id',$vendorproductcategory->mc_id)->where('status',1)->count(); 
                    # code...
                    if($Getcategories<1){ 
                        $isshopready=0; 
                    }else{
                         $Getactiveproduct = DB::table('nm_product')->where('pro_mc_id',$vendorproductcategory->mc_id)->where('pro_status', '=', 1)->count(); 
                         if($Getactiveproduct<1){
                        $isshopready=0;
                         }else{
                            $isshopready=$Getcategories;
                         }
                      
                    }

                    $vendorproductcategory->isshopredytoshow=$isshopready;
                }


                }


                // IF NO RECORD FOUND START // 
                if(count($vendor) < 1)
                {
                if ($lang != 'en_lang'){
                Session::flash('message', "لا يوجد Vender في المدينة التي اخترتها");
                }
                else
                {
                Session::flash('message', "No Vender exists in your selected city");
                }
                return redirect()->route('occasion-co-ordinator',['id'=>11]);
                }
                // IF NO RECORD FOUND END // 



                if(Session::get('searchdata.basecategoryid')!=''){
                  $basecategory_id=Session::get('searchdata.basecategoryid');
                  }else {
                  $basecategory_id=Session::get('searchdata.maincategoryid');
                  }
                  
                $Formbusinesstype= new Formbusinesstype();
                $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);






                if($subcategory_id=='12')
                {
                //Kosha 
                return view('occasioncoordinator.kosha-shops',compact('sub_catg','vendor','category','bussinesstype')); 
                }

                if($subcategory_id=='15')
                {
                //Roses
                return view('occasioncoordinator.roses-shops',compact('sub_catg','vendor','category','bussinesstype')); 
                }

                if($subcategory_id=='17')
                { //Specialevent
                return view('occasioncoordinator.special-events',compact('sub_catg','vendor','category','bussinesstype'));  
                }

                if($subcategory_id=='127')
                { // Photography studio
                return view('occasioncoordinator.photography-studio',compact('sub_catg','vendor','category','bussinesstype'));  
                }

                if($subcategory_id=='14')
                { //Reciptiona hospitality
                return view('occasioncoordinator.reception-and-hospitality',compact('sub_catg','vendor','category','bussinesstype'));   
                }
 
        }
        catch(Exception $e){
            return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');
        }

      }

    //DISPLAY ALL CATEGORY VENDORS END // 
   

    // GET THE COSHA DETAIL PAGE // 

        public function koshaShopdetails(Request $request)
        { 
            $VendorID = request()->id;
            $type = request()->type;
            $SubCatID = 12;
            $CatID = 11;
            $lang=Session::get('lang_file');
 
            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $CatID)->first();
                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($VendorID);


                if (!empty($category)) {

                    if ($SubCatID != '' && $lang  != 'en_lang') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id','mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('*')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //GET VENDOR DETAIL
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','parent', 'image', 'status','attribute_title as title')->where('status', '=', 1)->where('services_id', '=', $SubCatID)->orderBy('title', 'ASC')->get();


                        if (!$menu->isEmpty()) {                    

                            foreach($menu as $key => $value) {
                                if($value->id == 47 && $type!=2){
 
                                $productchk = Products::select( 'attribute_id')->where('pro_mc_id', '=', $VendorID)->where('parent_attribute_id', '=','1')->where('pro_status', '=', 1)->get();
                                $serviceChk = [];
                                foreach ($productchk as $productchk) {
                                array_push($serviceChk, $productchk->attribute_id);
                                }
 

                                    $sub_menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','parent', 'image', 'status','attribute_title as title')->where('status', '=', 1)->where('services_id', '=', $VendorID)->where('parent', '=', $SubCatID)->whereIn('id', $serviceChk)->orderBy('title', 'ASC')->get();



                                $value->sub_menu = $sub_menu;
                                    //get cosha detail according cosha sub category.
                            foreach($sub_menu as $key => $menu_val) {
                                $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','Insuranceamount','pro_disprice', 'pro_mc_id', 'attribute_id','pro_qty', 'option_id', 'service_hour','parent_attribute_id','pro_discount_percentage','pro_title as title')->where('pro_mc_id', '=', $VendorID)->where('parent_attribute_id', '=','1')->where('attribute_id', '=', $menu_val->id)->where('pro_status', '=', 1)->orderBy('title', 'ASC')->paginate(9);
 



                                $menu_val->product = $product;
                               

                            }

                                }else if($value->id == 46){
                                 $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc','Insuranceamount', 'pro_price','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty','service_hour','pro_discount_percentage','pro_title as title')->where('pro_mc_id', '=', $VendorID)->where('parent_attribute_id', '=','2')->where('pro_status', '=', 1)->orderBy('title', 'ASC')->paginate(9);
                                $value->product = $product;   
                                }
                                
                                
                               
                            }


                          
                        } else {
                                 
                                 // IF NO RECORD FOUND START // 
                                            if($menu->isEmpty() )
                                            {
                                            if ($lang != 'en_lang'){
                                            Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                            }
                                            else
                                            {
                                            Session::flash('message', "Shop has no item. Please try again");
                                            }
                                            return redirect()->route('kosha-shops',['id'=>11,'sid'=>$SubCatID]);
                                            }
 

                                 // IF NO RECORD FOUND END //  
                        }
                    } else {
                        /*
                        Language English
                        */
                         //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id','mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('*')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL
                        $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title','parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $SubCatID)->orderBy('attribute_title', 'ASC')->get();


                        if (!$menu->isEmpty()) {                    

                            foreach($menu as $key => $value) {
                                if($value->id == 47 && $type!=2){

                                       $productchk = Products::select( 'attribute_id')->where('pro_mc_id', '=', $VendorID)->where('parent_attribute_id', '=','1')->where('pro_status', '=', 1)->get();
                                $serviceChk = [];
                                foreach ($productchk as $productchk) {
                                array_push($serviceChk, $productchk->attribute_id);
                                }
 



                                    $sub_menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title','parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $VendorID)->whereIn('id', $serviceChk)->where('parent', '=', $SubCatID)->orderBy('attribute_title', 'ASC')->get(); 
                                $value->sub_menu = $sub_menu;
                              
                                    //get cosha detail according cosha sub category.
                            foreach($sub_menu as $key => $menu_val) {

                               
 
                                $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id','Insuranceamount', 'attribute_id','pro_discount_percentage', 'pro_disprice','pro_qty','option_id', 'service_hour','parent_attribute_id')->where('pro_mc_id', '=', $VendorID)->where('parent_attribute_id', '=','1')->where('attribute_id', '=', $menu_val->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                           
 
                                $menu_val->product = $product;
                               

                            }
 
                                }else if($value->id == 46){
                                 $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_qty','pro_desc', 'pro_price', 'pro_mc_id','Insuranceamount', 'attribute_id','pro_disprice','pro_discount_percentage', 'option_id', 'service_hour')->where('pro_mc_id', '=', $VendorID)->where('parent_attribute_id', '=','2')->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->product = $product;   
                                }
                                
                            }
               
 
                        } else {
                                // IF NO RECORD FOUND START // 
                                            if($menu->isEmpty() )
                                            {
                                            if ($lang != 'en_lang'){
                                            Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                            }
                                            else
                                            {
                                            Session::flash('message', "Shop has no item");
                                            }
                                            return redirect()->route('kosha-shops',['id'=>11,'sid'=>$SubCatID]);
                                            }
                                            // IF NO RECORD FOUND END // 
                        }
                    }
               $vendordetails = $vendor; 
       
               $allreview  = $client_review;
                return view('newWebsite.kosha-design',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang'));
                } else {

                 return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');

                }
            } catch (ModelNotFoundException $e) {
               return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');
            }



            
        }
 // Quantity Management

        public function checkcoshaquantity(Request $request){
                $product_id = $request->product_id;
                $qty= $request->qty;
                    $productqty = Products::select('pro_id', 'pro_qty')->where('pro_id', '=', $product_id)->first();
                    $proddbqty=$productqty->pro_qty;
                    if($proddbqty>=$qty){
                        $st='ok';
                    }else{
                        $st='error';
                    }
                    return $st; 
        }

//Set Cosha Data in add to Cart
    public function setCoshaAddToCart(Request $request) 
              {
            try {
               //echo '<pre>';print_r($_REQUEST);die;
                $sid = $request->sid;
                $vid= $request->vid;
                $id= $request->id;
                $product_id= $request->product_id;
                $cart_type= $request->cart_type;
                $cart_sub_type= $request->cart_sub_type;
                $attribute_id= $request->attribute_id;
                $input = $request->all();
                $User = Session::get('customerdata'); 
                $UserID =  $User['user_id'];
                 $Qran = rand(111,999);
                $product = Products::where('pro_mc_id', '=', $product_id)->first();

                $product_merchant = Category::where('mc_id', '=', $product_id)->first();
                    
                if (!empty($product)) {
                    $cart = Cart::where('user_id', '=', $UserID)->first();
                    if (!empty($cart)) 
                    {
                        //$cart_pro = CartProduct::where('cart_type','occasion')->where('cart_id',$cart->id)->where('category_id',$product_id)->delete();
                        //$cart_service_attribute = CartServiceAttribute::where('cart_type','occasion')->where('cart_id',$cart->id)->where('category_id',$product_id)->delete();
                    } 
                    else 
                    {
                        $cart_data = array();
                        $cart_data['user_id'] = $UserID;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                    
                    $total=0;

                    $cart_product_data   = array();              
                    $cart_attribute_data = array();
                    if($attribute_id == 47)
                    {
                     if(isset($input['product']['pro_id'])) 
                     {
                        foreach($input['product']['pro_id'] as $key => $value) 
                        {                        
                           $itemqty='itemqty'.$value;
                           $productqty=$request->$itemqty;
                           if($productqty==''){ $productqty=1;}
                           
                           $cosha_product = Products::select('pro_id','attribute_id','pro_price','pro_mc_id','pro_mr_id','pro_disprice','pro_discount_percentage','pro_title','pro_title_ar','pro_Img','pro_desc_ar','pro_desc','Insuranceamount')->where('pro_id',$value)->first();
                            $cart_product_data['cart_type']      = $cart_type;
                            $cart_product_data['cart_sub_type']  = $cart_sub_type;
                            $cart_product_data['cart_id']        = $cart->id;
                            $cart_product_data['category_id']    = $cosha_product->pro_mc_id;
                            $cart_product_data['product_id']     = $value;
                            $cart_product_data['merchant_id']    = $cosha_product->pro_mr_id;
                            $cart_product_data['shop_id']        = $cosha_product->pro_mc_id;
                            $cart_product_data['status']         = 1;

                    $cart_product_data['pro_title']   = $cosha_product->pro_title;
                    $cart_product_data['pro_title_ar']   = $cosha_product->pro_title_ar;
                    $cart_product_data['pro_Img']   = $cosha_product->pro_Img;
                    $cart_product_data['pro_desc_ar']   = $cosha_product->pro_desc_ar;
                    $cart_product_data['pro_desc']   = $cosha_product->pro_desc;
                    $cart_product_data['review_type']  = 'shop';
                    $cart_product_data['shop_vendor_id']  = $sid;
                    $cart_product_data['container_id']  = $Qran;
                    $cart_product_data['buy_rent']  = 'design';

                            $CosaPrice = $cosha_product->pro_price;
                            $pro_disprice = $cosha_product->pro_disprice;
                            $pro_discount_percentage  = $cosha_product->pro_discount_percentage;
                            $Insuranceamount  = $cosha_product->Insuranceamount;
                            if($pro_discount_percentage !='')
                            {
                                $pro_disprice = $CosaPrice - (($CosaPrice * $pro_discount_percentage)/100);
                            }

                            if($pro_disprice < 1){ $pro_disprice = $CosaPrice; }
                                $productunitprice=$pro_disprice;
                            if($Insuranceamount >0){
                               $pro_disprice = $pro_disprice+$Insuranceamount;
                               $cart_product_data['insurance_amount']    = $Insuranceamount*$productqty;
                            }
                                    $totalprice=$pro_disprice*$productqty;
                            $cart_product_data['quantity']    = $productqty;
                            $cart_product_data['total_price']    = $totalprice;
                            $cart_product_data['pro_price']    = $productunitprice;
                            $total=$total+$totalprice;
                            
                            $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                                  $mainattribute = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $attribute_id)->first();
                                  $subattribute = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $cosha_product->attribute_id)->first(); 
                                $cart_attribute_data['cart_id']          = $cart->id;
                                $cart_attribute_data['cart_type']        = $cart_type;
                                $cart_attribute_data['category_id']      = $cosha_product->pro_mc_id;
                                $cart_attribute_data['attribute_id']     = $attribute_id;

                                $cart_attribute_data['attribute_title']     = $mainattribute->attribute_title;
                                $cart_attribute_data['attribute_title_ar']     = $mainattribute->attribute_title_ar;

                                $cart_attribute_data['sub_attribute_title']     = $subattribute->attribute_title;
                                $cart_attribute_data['sub_attribute_title_ar']     = $subattribute->attribute_title_ar;

                                $cart_attribute_data['pro_title']   = $cosha_product->pro_title;
                                 $cart_attribute_data['pro_title_ar']   = $cosha_product->pro_title_ar;
                                    $cart_attribute_data['pro_Img']   = $cosha_product->pro_Img;
                                    $cart_attribute_data['pro_desc_ar']   = $cosha_product->pro_desc_ar;
                                    $cart_attribute_data['pro_desc']   = $cosha_product->pro_desc;
                                     $cart_attribute_data['cart_product_id']    = $cart_product->id;
                                $cart_attribute_data['sub_attribute_id'] = $cosha_product->attribute_id;
                                $cart_attribute_data['product_id']       = $cosha_product->pro_id;
                                $cart_attribute_data['price']            = $productunitprice;
                                $cart_attribute_data['quantity']    = $productqty;
                                $cart_attribute_data['insurance_amount']    = $Insuranceamount;
                                $cart_attribute_data['total_price']    = $productqty*($productunitprice + $Insuranceamount);
                                $cart_attribute_data['shop_vendor_id']    = $cosha_product->pro_mr_id;
                                
                              $cart_value =  CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                           
                           
                            }
                        }   
                    }

                   //////////New code added for API support start/////////////

                     $cart_product_branch_data = array();
                    $cart_product_branch_data['merchant_id'] = $product_merchant->vendor_id;
                    $cart_product_branch_data['shop_id'] = $product_merchant->mc_id;
                    $cart_product_branch_data['shop_vendor_id'] = $product_merchant->parent_id;
                    $cart_product_branch_data['cart_type'] = $cart_type;
                    $cart_product_branch_data['cart_sub_type'] = $cart_sub_type;
                    $cart_product_branch_data['cart_id'] = $cart->id;
                    $cart_product_branch_data['category_id'] = $product_id;
                    $cart_product_branch_data['product_id'] = $product->pro_id;
                    $cart_product_branch_data['status'] = 1;
                    $cart_product_branch_data['pro_title'] = $product_merchant->mc_name;
                    $cart_product_branch_data['pro_title_ar'] = $product_merchant->mc_name_ar;
                    $cart_product_branch_data['pro_desc'] = $product_merchant->mc_discription;
                    $cart_product_branch_data['pro_desc_ar'] = $product_merchant->mc_discription_ar;
                    $cart_product_branch_data['pro_Img'] = $product_merchant->mc_img;
                    $cart_product_branch_data['total_price'] = $total;
                    $cart_product_branch_data['showcartproduct'] = 1;
                    $cart_product_branch_data['container_id']  = $Qran;
                    $cart_product_branch_data['buy_rent']  = 'design';
                    $cart_product_branch = CartProduct::create($cart_product_branch_data); //cart product entry 

                    //////////New code added for API support end/////////

                    /*$cart_cosha_price = CartServiceAttribute::where('cart_id',$cart->id)->where('category_id',$product_id)->get();
                    $total = 0;
                    foreach($cart_cosha_price as $key => $value) {
                        $total += $value->price;
                    }
                    $total_price['total_price'] = $total;
                    $update_product = CartProduct::where('category_id', '=', $product_id)->update($total_price);
                    */
                  if(\Config::get('app.locale') == 'ar')
                    {
                    Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                    Session::flash('status', "Product added in the cart.");
                    } 
                   $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {
                      return Redirect::back()->with('message','تمت إضافة البيانات كوشا في عربة بنجاح'); 
                    }

                    else
                    {
                     return Redirect::back()->with('message','Product added in the cart.');
                    }


                } else {
                    $product = Products::where('pro_id', '=', $product_id)->first();

                if (!empty($product)) {
                    $cart = Cart::where('user_id', '=', $UserID)->first();
                    if (!empty($cart)) {
                        //$cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();
                        //$cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $UserID;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    
                    $cart_product_data                  = array();
                    $cart_product_data['cart_type']     = $cart_type;
                    $cart_product_data['cart_sub_type'] = $cart_sub_type;
                    $cart_product_data['cart_id']       = $cart->id;
                    $cart_product_data['product_id']    = $product_id;

                    //$pro_disprice = $product_id;
                    $pro_disprice = $product->pro_disprice;
 
                    if($pro_disprice < 1){ $TPrice = $product->pro_price;}else { $TPrice =$pro_disprice; }
                    $ptotal='totalpaidamount_'.$product_id;
                   
                    $insu='insuranceamount_'.$product_id;
                    $TP=$request->$ptotal;
                    $prodqty=$request->itemqty;

                    $Insuranceamount=$request->$insu;
                    if($Insuranceamount >0){
                               $readymadecoshainsurace=$Insuranceamount*$prodqty;
                               $cart_product_data['insurance_amount']    = $Insuranceamount*$prodqty;
                            }


                    $cart_product_data['total_price']   = $TP;
                    $cart_product_data['status']        = 1;                    
                    $cart_product_data['merchant_id']   = $product->pro_mr_id;
                    $cart_product_data['shop_id']       = $product->pro_mc_id;
                    $cart_product_data['category_id']   = $id;

                    $cart_product_data['pro_title']   = $product->pro_title;
                    $cart_product_data['pro_title_ar']   = $product->pro_title_ar;
                    $cart_product_data['pro_Img']   = $product->pro_Img;
                    $cart_product_data['pro_desc']   = $product->pro_desc;
                    $cart_product_data['pro_desc_ar']   = $product->pro_desc_ar;
                    $cart_product_data['shop_vendor_id']  = $sid;
                    $cart_product_data['container_id']  = $Qran;
                    $cart_product_data['pro_price']    = $TPrice;
                    $cart_product_data['buy_rent']  = 'readymade';

                    $cart_product_data['quantity']   = $prodqty;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 

                     $mainattribute = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $attribute_id)->first();
                                  

                    $cart_attribute_data                   = array();
                    $cart_attribute_data['cart_type']      = $cart_type;
                    $cart_attribute_data['cart_id']        = $cart->id;
                    $cart_attribute_data['product_id']     = $product_id;
                    $cart_attribute_data['attribute_id']   = $attribute_id;
                    $cart_attribute_data['category_id']    = $id;


                    $cart_attribute_data['attribute_title']     = $mainattribute->attribute_title;
                                $cart_attribute_data['attribute_title_ar']     = $mainattribute->attribute_title_ar;

                               

                                $cart_attribute_data['pro_title']   = $product->pro_title;
                    $cart_attribute_data['pro_title_ar']   = $product->pro_title_ar;
                    $cart_attribute_data['pro_Img']   = $product->pro_Img;
                    $cart_attribute_data['pro_desc']   = $product->pro_desc;
                    $cart_attribute_data['pro_desc_ar']   = $product->pro_desc_ar;

                    $cart_attribute_data['quantity']   = $prodqty;
                    $cart_attribute_data['insurance_amount']    = $Insuranceamount;

                    $cart_attribute_data['cart_product_id']    = $cart_product->id;


                    $cart_attribute_data['price']          = $TPrice;
                    $total=($TPrice+$Insuranceamount)*$prodqty;
                     $cart_attribute_data['total_price']    = $total;
                                $cart_attribute_data['shop_vendor_id']    = $product->pro_mr_id;
                   
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry 
                    
                   //////////New code added for API support start/////////////
                     $product_merchant_for_readymade = Category::where('mc_id', '=', $product->pro_mc_id)->first();
                   
                     $cart_product_branch_data = array();
                    $cart_product_branch_data['merchant_id'] = $product_merchant_for_readymade->vendor_id;
                    $cart_product_branch_data['shop_id'] = $product_merchant_for_readymade->mc_id;
                    $cart_product_branch_data['shop_vendor_id'] = $product_merchant_for_readymade->parent_id;
                    $cart_product_branch_data['cart_type'] = $cart_type;
                    $cart_product_branch_data['cart_sub_type'] = $cart_sub_type;
                    $cart_product_branch_data['cart_id'] = $cart->id;
                    $cart_product_branch_data['category_id'] = $product_merchant_for_readymade->mc_id;
                    $cart_product_branch_data['product_id'] = $product->pro_id;
                    $cart_product_branch_data['status'] = 1;
                    $cart_product_branch_data['pro_title'] = $product_merchant_for_readymade->mc_name;
                    $cart_product_branch_data['pro_title_ar'] = $product_merchant_for_readymade->mc_name_ar;
                    $cart_product_branch_data['pro_desc'] = $product_merchant_for_readymade->mc_discription;
                    $cart_product_branch_data['pro_desc_ar'] = $product_merchant_for_readymade->mc_discription_ar;
                    $cart_product_branch_data['pro_Img'] = $product_merchant_for_readymade->mc_img;
                    $cart_product_branch_data['total_price'] = $total;
                    $cart_product_branch_data['showcartproduct'] = 1;
                    $cart_product_branch_data['container_id']  = $Qran;
                    $cart_product_branch_data['buy_rent']  = 'readymade';
                    $cart_product_branch = CartProduct::create($cart_product_branch_data); //cart product entry 

                    //////////New code added for API support end/////////


                } else {
                 
                 return redirect()->route('kosha-shop-details',['id'=>$id,'sid'=>$sid,'vid'=>$vid])->with('message','issue adding product into cart');
                }





                
                    if(\Config::get('app.locale') == 'ar')
                    {
                    Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                    Session::flash('status', "Product added in the cart.");
                    } 


                    $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {

                          return Redirect::back()->with('message','تمت إضافة البيانات كوشا في عربة بنجاح'); 

                  
                    }

                    else
                    {
                      return Redirect::back()->with('message','Product added in the cart.');
                    }

                
                }



            } catch (ModelNotFoundException $e) {           
            return redirect()->route('kosha-shop-details',['id'=>$id,'sid'=>$sid,'vid'=>$vid])->with('message','Something Went Wrong!');
            }
         
      

    }
 

        // GET THE PHOTOGRAPHY DETAIL PAGE // 
        public function photographyShopdetails(Request $request)
        {
 
            $VendorID = request()->id;
            $type = request()->type;
            $SubCatID = 127;
            if($type!='2')
            {
            $AttrID = 142;
            }
            else
            {
            $AttrID = 143;      
            }         
           
             $CatID = 11;
            $lang=Session::get('lang_file');
 
            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $CatID)->first();
                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($VendorID);
                if (!empty($category)) {

                if ($SubCatID != '' && $lang  != 'en_lang') {
                /* FOR ARABIC LANGUAGE */
                //GET SUB CATEGORY
                $sub_catg = Category::select('mc_id','mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                //GET VENDOR DETAIL
                $vendor = Category::select('mc_id', 'address_ar as address','mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id','latitude','longitude', 'mc_video_url', 'mc_img','google_map_address','mc_video_description','mc_video_description_ar', 'budget_in_percentages','opening_time','closing_time')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                //GET VENDOR DETAIL
                $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title','parent', 'image', 'status')->where('services_id', '=', $SubCatID)->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();
 
                        if (!$menu->isEmpty()) {                    

                                foreach($menu as $key => $value) {
                                $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_discount_percentage', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour')->where('pro_mc_id', '=', $VendorID)->where('attribute_id', '=', $AttrID)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10);
                                $value->product = $product;
                                foreach($product as $pro_attr) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title_ar as attribute_title','value','status')->where('status', '=', 1)->where('product_id', '=', $pro_attr->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $pro_attr->product_attribute =  $product_attr;

                            }
                               
                            }

                          
                        } else {
                                 
                            // IF NO RECORD FOUND START // 
                                if($menu->isEmpty() )
                                {
                                if ($lang != 'en_lang'){
                                Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                }
                                else
                                {
                                Session::flash('message', "Shop has no item. Please try again");
                                }
                                return redirect()->route('photography-studio',['id'=>11,'sid'=>$SubCatID]);
                                }


                             // IF NO RECORD FOUND END //  
                        }
                    } else {
                        /*
                        Language English
                        */
                         //GET SUB CATEGORY
                            $sub_catg = Category::select('mc_id','mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                            //GET VENDOR DETAIL
                            $vendor = Category::select('mc_id','address','mc_video_description','mc_video_description_ar', 'mc_name', 'mc_discription','latitude','longitude', 'city_id', 'google_map_address','mc_video_url', 'mc_img', 'budget_in_percentages','opening_time','closing_time')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                            //GET VENDOR DETAIL
                            $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title','parent', 'image', 'status')->where('services_id', '=', $SubCatID)->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();


                        if (!$menu->isEmpty()) {                    

                               foreach($menu as $key => $value) {
                                $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'pro_discount_percentage', 'option_id', 'service_hour')->where('pro_mc_id', '=', $VendorID)->where('attribute_id', '=', $AttrID)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(10000);
                                $value->product = $product;
                                foreach($product as $pro_attr) {
                                $product_attr = ProductAttribute::select('id','product_id','vendor_id','attribute_title','value','status')->where('status', '=', 1)->where('product_id', '=', $pro_attr->pro_id)->get(); //GET PRODUCT ATTRIBUTE
                               $pro_attr->product_attribute =  $product_attr;

                            }
                               
                            }
                            
 
 
                        } else {
                                // IF NO RECORD FOUND START // 
                                if($menu->isEmpty() )
                                {
                                if ($lang != 'en_lang'){
                                Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                }
                                else
                                {
                                Session::flash('message', "Shop has no item");
                                }
                                return redirect()->route('photography-studio',['id'=>11,'sid'=>$SubCatID]);
                                }
                                // IF NO RECORD FOUND END // 
                        }
                    }

 

                   $vendordetails = $vendor; 
           
                   $allreview  = $client_review;

                    return view('newWebsite.photography',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang'));
                    } else {

                     return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');

                    }
                } catch (ModelNotFoundException $e) {
                   return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');
                }

        }



 public function setPhotographyStudioAddToCart(Request $request) {
             try {
                $User = Session::get('customerdata'); 
                 $UserID =  $User['user_id'];
                
                $product_id = $request->product_id;
                $cart_type = $request->cart_type;
                $cart_sub_type = $request->cart_sub_type;
                $attribute_id = $request->attribute_id;
                $service_date = $request->service_date;
                $service_time= $request->service_time;
                $sid = $request->sid;
                $vid= $request->vid;
                $id= $request->id;
                $bdate='bookingdate'.$product_id;
                $bookingdate=$request->$bdate;
                $btime='bookingtime'.$product_id;
                $bookingtime=$request->$btime;

                $blocation='location'.$product_id;
                $elocation=$request->$blocation;

                $product = Products::where('pro_id', '=',$product_id)->first();

               // dd($product);
                if (!empty($product)) {
                    $cart = Cart::where('user_id', '=', $UserID)->first();
                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();
                        $cart_pro_rent = CartProductRent::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $UserID;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['cart_type'] = $cart_type;
                    $cart_product_data['cart_sub_type'] = $cart_sub_type;
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $product_id;
                    $cart_product_data['merchant_id'] = $product->pro_mr_id;
                    $cart_product_data['shop_id'] = $product->pro_mc_id;
                    $pro_disprice = $product->pro_disprice;
                    $cart_product_data['review_type']      = 'shop';
                    

                    if( $pro_disprice >=1)
                    {
                    $Tp = $pro_disprice;
                    }
                    else
                    {
                    $Tp = $product->pro_price;
                    }

                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['quantity'] = 1;
                        $cart_product_data['total_price'] = $Tp;
                    $cart_product_data['status'] = 1;
                    $cart_product_data['category_id'] = $product_id;
                    $cart_product_data['shop_vendor_id'] = $product->pro_mc_id;
                    $cart_product_data['location']    = $elocation;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 

                    
                     $cart_photography_data                = array();
                    $cart_photography_data['cart_id']     = $cart->id;
                    $cart_photography_data['category_id'] = $product_id;                    
                    $cart_photography_data['date']        = date("Y-m-d", strtotime($bookingdate));
                    $cart_photography_data['time']        = $bookingtime;
                    $cart_photography_data['elocation']    = $elocation;
                    $cart_photography_data = CartHospitality::create($cart_photography_data); //cart hospitality data entry 




                    $cart_attribute_data = array();  
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['cart_type'] = $cart_type;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['attribute_id'] =  $attribute_id;
                    $cart_attribute_data['sub_attribute_id'] = $product->attribute_id;
                    $cart_attribute_data['product_id'] = $product_id;
                    CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                    
                    $cart_product_rent = array();
                    $cart_product_rent['cart_id'] = $cart->id;
                    $cart_product_rent['cart_type'] = $cart_type;
                    $cart_product_rent['product_id'] = $product_id;
                        if(isset($service_date) && $service_date!=''){ $nd=$service_date; } else{ $nd=$bookingdate; }


                    $cart_product_rent['rental_date'] = date("Y-m-d", strtotime($bookingdate));
                    $cart_product_rent['rental_time'] = $bookingtime;


  

                    CartProductRent::create($cart_product_rent); //Product rental date and time entry
                    

                    if(\Config::get('app.locale') == 'ar')
                    {
                    Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                    Session::flash('status', "Product added in the cart.");
                    } 



                    $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {
                    return Redirect::back()->with('message','وأضاف حزمة في العربة'); 
                    }

                    else
                    {
                   return Redirect::back()->with('message','Package added in the cart.');
                    }



                } else {                    

                    $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {
                    return Redirect::back()->with('message','هناك خطأ ما'); 
                    }

                    else
                    {
                    return Redirect::back()->with('message','Something went wrong');
                    }

                }

            } catch (ModelNotFoundException $e) {
                 $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {
                    return redirect()->route('photography-studio-details',['id'=>$id,'sid'=>$sid,'vid'=>$vid,])->with('message','هناك خطأ ما'); 
                    }
                    else
                    {
                    return redirect()->route('photography-studio-details',['id'=>$id,'sid'=>$sid,'vid'=>$vid,])->with('message','Something went wrong');
                    }
            }

         
        

    }

 

        // GET THE RECEPTION  DETAIL PAGE //    
        public function receptionShopdetails(Request $request)
        {

            $VendorID = request()->id;
            $type = request()->type;
            $SubCatID = 15;          
            $AttrID = 50;    
            $CatID = 11;
              $lang=Session::get('lang_file');
 
            try {
                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $CatID)->first();
                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($VendorID);

                if (!empty($category)) {

                    if ($SubCatID != '' && $lang == 'ar_lang') {

                        /* FOR ARABIC LANGUAGE */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id','address_ar as address', 'mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url','google_map_address','latitude','longitude','mc_video_description','mc_video_description_ar', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                        //GET VENDOR DETAIL
                        $data = array('dsign_your_package' => 'Dsign Your Package',
                            'packages' => 'Packages'
                        );
                        if (!empty($data)) {
                            foreach($data as $key => $cat_menu) {
                                if ($cat_menu == 'Dsign Your Package') {
                                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $VendorID)->orderBy('attribute_title', 'ASC')->get();
                                    if (!$menu->isEmpty()) {
                                        foreach($menu as $key => $value) {
                                            //get staff price
                                            $staff_price = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'value', 'status')->where('status', '=', 1)->where('services_id', '=', $VendorID)->where('attribute_title', '=', 'Worker Price')->get();
                                            $value->staff_price = $staff_price;
                                            //get staff nationality
                                            $staff_nationality = StaffNationality::select('id', 'vendor_id', 'shop_id', 'nation_id','worker_price')->where('shop_id', '=', $VendorID)->with('getNationality:co_id,co_name_ar as co_name')->get();
                                            $value->staff_nationality = $staff_nationality;

                                            //get product detail
                                            $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'no_of_staff', 'staff_nationality','packege','pro_qty')->where('packege', '=', 'no')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->with('getNationality:co_id,co_name_ar')->orderBy('pro_title', 'ASC')->paginate(9);
                                            $value->product = $product;
                                        }

                                    }
                                } else if ($cat_menu == 'Packages') {
                                    $product = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price', 'pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'packege', 'no_of_staff', 'staff_nationality','pro_qty')->where('packege', '=', 'yes')->where('pro_mc_id', '=', $VendorID)->where('pro_status', '=', 1)->with('getNationality:co_id,co_name_ar')->orderBy('pro_title', 'ASC')->paginate(9);
                                    if (!$product->isEmpty()) {
                                        foreach($product as $pro_val) {
                                            if ($pro_val->packege == 'yes') {
                                                $packege = ProductPackage::where('shop_id', '=', $pro_val->pro_mc_id)->where('packege_id', '=', $pro_val->pro_id)->with('getServiceAttribute:id,services_id,attribute_title_ar')->with('getProductPackage:pro_id,pro_title_ar as pro_title,pro_Img,pro_desc_ar as pro_desc,pro_price,attribute_id')->get();
                                                $pro_val->product_packege = $packege;
                                            }
                                        }
                                    }
                                }
                            }

                                    $vendordetails = $vendor; 

                    $allreview  = $client_review;
                    $type = request()->type;

                    if($type!=2){
                    return view('newWebsite.hospitality',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang'));
                    }
                    else
                    {
                    return view('occasioncoordinator.package-reception-and-hospitality-details',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang'));  
                    }
            
                        } else {
                          return Redirect::back()->with('message','Something went wrong');
                        }
                    } else {
                        /*
                        Language English
                        */
                        //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                        //GET VENDOR DETAIL
                        $vendor = Category::select('mc_id','address', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'mc_video_description','google_map_address','mc_video_description_ar','latitude','longitude','budget_in_percentages','vendor_id','terms_conditions','terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                        //GET VENDOR DETAIL
                        $data = array('dsign_your_package' => 'Dsign Your Package',
                            'packages' => 'Packages'
                        );
                        if (!empty($data)) {
                            foreach($data as $key => $cat_menu) {
                                if ($cat_menu == 'Dsign Your Package') {
                                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->where('services_id', '=', $VendorID)->orderBy('attribute_title', 'ASC')->get();
                                    if (!$menu->isEmpty()) {
                                        foreach($menu as $key => $value) {
                                            //get staff price
                                            $staff_price = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'value', 'status')->where('status', '=', 1)->where('services_id', '=', $VendorID)->where('attribute_title', '=', 'Worker Price')->get();
                                            $value->staff_price = $staff_price;
                                            //get staff nationality
                                            $staff_nationality = StaffNationality::select('id', 'vendor_id', 'shop_id', 'nation_id','worker_price')->where('shop_id', '=', $VendorID)->with('getNationality:co_id,co_name')->get();
                                            $value->staff_nationality = $staff_nationality;

                                            //get product detail
                                            $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'no_of_staff', 'staff_nationality','packege','pro_qty')->where('packege', '=', 'no')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->with('getNationality:co_id,co_name')->orderBy('pro_title', 'ASC')->paginate(9);
                                            $value->product = $product;
                                        }

                                    }
                                } else if ($cat_menu == 'Packages') {
                                    $product = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_discount_percentage','pro_disprice', 'pro_mc_id', 'attribute_id', 'option_id', 'service_hour', 'packege', 'no_of_staff', 'staff_nationality','pro_qty')->where('packege', '=', 'yes')->where('pro_mc_id', '=', $VendorID)->where('pro_status', '=', 1)->with('getNationality:co_id,co_name')->orderBy('pro_title', 'ASC')->paginate(9);
                                    if (!$product->isEmpty()) {
                                        foreach($product as $pro_val) {
                                            if ($pro_val->packege == 'yes') {
                                                $packege = ProductPackage::where('shop_id', '=', $pro_val->pro_mc_id)->where('packege_id', '=', $pro_val->pro_id)->with('getServiceAttribute:id,services_id,attribute_title')->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                                                $pro_val->product_packege = $packege;
                                            }
                                        }
                                    }
                                }
                            }




                        $vendordetails = $vendor; 

                    $allreview  = $client_review;
 
                  $type = request()->type;





  // DB::table('nm_service_staff_nationality')->select('vendor_id','shop_id','nation_id','country_name','country_name_ar','worker_price')->where('vendor_id',);




                    if($type!=2){
                    return view('newWebsite.hospitality',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang'));
                    }
                    else
                    {
                    return view('occasioncoordinator.package-reception-and-hospitality-details',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang'));  
                    }


                           
                        } else {
                           return Redirect::back()->with('message','Something went wrong');
                        }
                    }
 

                } else {

                   
return Redirect::back()->with('message','Something went wrong');
                }
            } catch (ModelNotFoundException $e) {
             return Redirect::back()->with('message','Something went wrong');
            }



   
        }



public function getcountryWrkerprice(Request $request)
{
$VendorID = $request->vid;
$WP = $request->cid;

    $Current_Currency  = Session::get('currency'); 

    if($Current_Currency =='') { 
    $Current_Currency = 'SAR'; 
    } 


 $countrydata = DB::table('nm_service_staff_nationality')->where('vendor_id',$VendorID)->where('nation_id',$WP)->first();
  $workerprice = $countrydata->worker_price;  

  $workerprice = currency($workerprice, 'SAR',$Current_Currency, $format = false);


 return $workerprice;

}


public function setRecieptionaddtoCart(Request $request)
{
 
   $staff_price   = '';
   $User          = Session::get('customerdata'); 
   $UserID        = $User['user_id'];
   $product_id    = $request->product_id;
   $cart_type     = $request->cart_type;
   $language_type = $request->language_type;
   $attribute_id  = $request->attribute_id;
   $cart_sub_type = $request->cart_sub_type;
   $price         = $request->price; 
   $products      = $request->products;
   $product_qty   = $request->product_qty;
   $nationality   = $request->nationality;
   $bookingtime   = $request->bookingtime;
   $bookingdate   = $request->bookingdate;
   $elocation   = $request->location;
   $tp            = $request->tp;
   $werkerP       = $request->werkerP;
   $TotalPrice    = $request->p_total_price;
    $Qran = rand(111,999);
   if($TotalPrice=='')
   {
     $TotalPrice = ($werkerP * $product_qty) + $tp; 
   }
   $cart_hospitality_data['category_id'] = $product_id;
   $cart_hospitality_data['no_of_staff'] = $product_qty;
   $cart_hospitality_data['nationality'] = $nationality;
   $cart_hospitality_data['date'] = date("Y-m-d", strtotime($bookingdate));
   $cart_hospitality_data['time'] = $bookingtime;
   try 
   {
     $catg_pro = Products::where('pro_mc_id',$product_id)->first();

     $product_merchant = Category::where('mc_id', '=', $product_id)->first();

     if(!empty($catg_pro)) 
     {
       $product = Category::where('mc_id',$product_id)->first();
       $cart    = Cart::where('user_id',$UserID)->first();
       if(!empty($cart)) 
       {
          $cart_pro = CartProduct::where('cart_type','occasion')->where('cart_id',$cart->id)->where('category_id',$product_id)->delete();
          $cart_service_attribute = CartServiceAttribute::where('cart_type','occasion')->where('cart_id',$cart->id)->where('category_id',$product_id)->delete();
          $cart_hospitality = CartHospitality::where('cart_id',$cart->id)->where('category_id',$product_id)->delete();
       } 
       else 
       {
          $cart_data = array();
          $cart_data['user_id'] = $UserID;
          $cart = Cart::create($cart_data); //cart entery
       }
        $cart_attribute_data = array();
        $Current_Currency  = Session::get('currency');
                    if(isset($products)) 
                    {

                            foreach($products as $key => $value) 
                            {
                               $hos_product = Products::where('pro_id',$value)->first();
                               $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id',$hos_product->attribute_id)->first();

                               $staff_price = ServiceAttribute::select('id','services_id','attribute_title','value','status')->where('status',1)->where('services_id',$hos_product->pro_id)->where('attribute_title','Worker Price')->first();

                               if(isset($hos_product->pro_discount_percentage) && $hos_product->pro_discount_percentage>'0')
                                {
                                   $getPricenew = currency($hos_product->pro_disprice,'SAR',$Current_Currency,$format = false);
                                }
                                else
                                {
                                   $getPricenew = currency($hos_product->pro_price,'SAR',$Current_Currency,$format = false);
                                }


                                $prodqty=$request->qty[$key];
                                $total_price_with_qty=$getPricenew*$prodqty;

                                $cart_attribute_data['cart_id']                = $cart->id;
                                $cart_attribute_data['cart_type']              = $cart_type;
                                $cart_attribute_data['category_id']            = $product_id;
                                $cart_attribute_data['attribute_id']           = $attribute_id;
                                $cart_attribute_data['sub_attribute_id']       = $hos_product->attribute_id;
                                $cart_attribute_data['product_id']             = $hos_product->pro_id;
                                $cart_attribute_data['price']                  = $getPricenew;
                                $cart_attribute_data['attribute_title']        = 'Design Your Package';
                                $cart_attribute_data['attribute_title_ar']     = 'تصميم الحزمة الخاصة بك';
                                $cart_attribute_data['sub_attribute_title']    = $menu->attribute_title;
                                $cart_attribute_data['sub_attribute_title_ar'] = $menu->attribute_title_ar;
                                $cart_attribute_data['pro_title']              = $hos_product->pro_title;
                                $cart_attribute_data['pro_title_ar']           = $hos_product->pro_title_ar;
                                $cart_attribute_data['pro_desc']               = $hos_product->pro_desc;
                                $cart_attribute_data['pro_desc_ar']            = $hos_product->pro_desc_ar;
                                $cart_attribute_data['pro_img']                = $hos_product->pro_Img;

                                $cart_attribute_data['quantity']                = $prodqty;
                                $cart_attribute_data['total_price']                = $total_price_with_qty;
                                $cart_attribute_data['shop_vendor_id']           = $hos_product->pro_mr_id;
                                $cart_attribute_data['shop_id']                = $hos_product->pro_mc_id;
                                $cart_attribute_data['worker_price']                = $werkerP;
                                
                                $cart_value =  CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                          
                            }
                        } 
                    
                    $cart_hospitality_data                = array();
                    $cart_hospitality_data['cart_id']     = $cart->id;
                    $cart_hospitality_data['category_id'] = $product_id;
                     $cart_hospitality_data['product_id'] = $catg_pro->pro_id;
                    $cart_hospitality_data['no_of_staff'] = $product_qty;
                    $cart_hospitality_data['nationality'] = $nationality;
                     $cart_hospitality_data['order_type'] = 'Design Your Package';
                    $cart_hospitality_data['date']        = date("Y-m-d", strtotime($bookingdate));
                    $cart_hospitality_data['time']        = $bookingtime;
                    $cart_hospitality_data['elocation']        = $elocation;
                    $cart_hospitality_data = CartHospitality::create($cart_hospitality_data); //cart hospitality data entry 

                    $staff_price = ServiceAttribute::select('id','services_id','attribute_title','value','status')->where('status',1)->where('services_id',$product_id)->where('attribute_title','Worker Price')->first(); 
                    $cart_pro_value = CartServiceAttribute::where('category_id',$product_id)->get();
                    
                    /*$pro_price = 0;
                    foreach($cart_pro_value as $key => $value) 
                    {
                        $pro_price += $value->price;
                    }*/
                    $totalofdesignyourhospitality=0;
                    $onltfirstrecord=1;
                    $total_price_with_qty=0;
                    if(isset($products)) 
                    {                         
                        foreach($products as $key => $value) 
                        {
                            

                            $productnew = Products::select('pro_id','attribute_id','pro_price','pro_disprice','pro_title','pro_title_ar','pro_desc','pro_desc_ar','pro_mc_id','pro_Img','pro_mr_id')->where('pro_id',$value)->first();
                            if(isset($productnew->pro_disprice) && $productnew->pro_disprice!='0.00' && $productnew->pro_disprice!='0')
                            {
                               $getPrice = currency($productnew->pro_disprice,'SAR',$Current_Currency,$format = false);
                            }
                            else
                            {
                               $getPrice = currency($productnew->pro_price,'SAR',$Current_Currency,$format = false);
                            } 

                        
                             $prodqty=$request->qty[$key];
                            $total_price_with_qty=$total_price_with_qty+$getPrice*$prodqty;
                                $totalofdesignyourhospitality=$totalofdesignyourhospitality+$total_price_with_qty;
                            $staff_price = DB::table('nm_service_staff_nationality')->select('id','vendor_id','shop_id','nation_id','worker_price')->where('vendor_id',$productnew->pro_mr_id)->where('shop_id',$product_id)->where('nation_id',$nationality)->first();                                                        
                            $wkprice = currency($staff_price->worker_price,'SAR',$Current_Currency,$format = false);
                               
                               
                            $getshopname=Helper::getshopname($productnew->pro_mc_id); 
                            
                            $cart_product_data                    = array();
                            $cart_product_data['merchant_id']     = $productnew->pro_mr_id;
                            $cart_product_data['shop_id']         = $product_id;
                            $cart_product_data['cart_type']       = $cart_type;
                            $cart_product_data['cart_sub_type']   = $cart_sub_type;
                            $cart_product_data['cart_id']         = $cart->id;
                            $cart_product_data['category_id']     = $product_id;
                            $cart_product_data['total_price']     = $total_price_with_qty+($wkprice * $product_qty);                  
                            $cart_product_data['status']          = 1;
                            $cart_product_data['noofstaff']        = $product_qty;
                            $cart_product_data['quantity']        = $prodqty;
                            $cart_product_data['product_id']      = $value;
                            $cart_product_data['buy_rent']      = 'design';
                            $cart_product_data['location']      = $elocation;
                            $cart_product_data['shop_vendor_id']  = $productnew->pro_mr_id;
                            $cart_product_data['pro_title']       = $getshopname->mc_name;
                            $cart_product_data['pro_title_ar']    = $getshopname->mc_name_ar;
                            $cart_product_data['pro_desc']        = $getshopname->mc_discription;
                            $cart_product_data['pro_desc_ar']     = $getshopname->mc_discription_ar;
                            $cart_product_data['pro_Img']         = $getshopname->mc_img;
                            $cart_product_data['pro_price']       = $productnew->pro_price;
                            $cart_product_data['worker_price']    = $wkprice * $product_qty;
                            $cart_product_data['review_type']     = 'shop';
                             $cart_product_data['container_id']  = $Qran;
                              if($onltfirstrecord==1){
                                $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                                }
                                $totalwithworker=$total_price_with_qty+($wkprice * $product_qty);
                                CartProduct::where('id',$cart_product->id)->update(['total_price'=>$totalwithworker]);
                                    $onltfirstrecord++;



                        }



                     







                    }


                     if(\Config::get('app.locale') == 'ar')
        {
          $stcart= "وأضاف المنتج في العربة.";
        }
        else
        {
          $stcart="Product successfully added in cart.";
        } 

                return Redirect::back()->with('status',$stcart);

                    $response = Response::json($response_array, 200);
                } else {


                    $product = Products::where('pro_id', '=', $product_id)->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $UserID)->first();
                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        $cart_package = CartProductPackage::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();


                        $cartHospital_pro = CartHospitality::where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $UserID;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $cart_type;
                    $cart_product_data['cart_sub_type'] = $cart_sub_type;
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $product_id;
                    $cart_product_data['quantity'] = $product_qty;

if($product->pro_disprice >=1){$cart_product_data['total_price'] = $product_qty * $product->pro_disprice; } else {$cart_product_data['total_price'] = $product_qty *  $product->pro_price; }
                    
                    $cart_product_data['buy_rent']      = 'package';
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['location']      = $elocation;
                    $cart_product_data['review_type']      = 'shop';
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 
                     
                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $attribute_id)->first();

                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $cart_type;
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['product_id'] = $product_id;                  
                    $cart_attribute_data['attribute_id'] = $attribute_id;
                    $cart_attribute_data['attribute_title'] = 'Package';
                    $cart_attribute_data['attribute_title_ar'] = 'صفقة';

                    $cart_attribute_data['pro_title'] = $product->pro_title;
                    $cart_attribute_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_attribute_data['pro_desc'] = $product->pro_desc;
                    $cart_attribute_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_attribute_data['pro_Img'] = $product->pro_Img;
if($product->pro_disprice >=1){$cart_attribute_data['price'] = $product->pro_disprice; } else {$cart_attribute_data['price'] =  $product->pro_price; }

                    $cart_attribute_data['quantity'] = $product_qty;
                    $cart_attribute_data['shop_vendor_id'] = $category->vendor_id;
                    $cart_attribute_data['shop_id'] = $category->mc_id;
                    if($product->pro_disprice >=1){$cart_attribute_data['total_price'] = $product_qty * $product->pro_disprice; } else {$cart_attribute_data['total_price'] = $product_qty *  $product->pro_price; }


                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/ 



                    $cart_hospitality_data                = array();
                    $cart_hospitality_data['cart_id']     = $cart->id;
                    $cart_hospitality_data['category_id'] = $product->pro_mc_id;
                     $cart_hospitality_data['product_id'] = $product->pro_id; 
                     $cart_hospitality_data['order_type'] = 'Package';                   
                    $cart_hospitality_data['date']        = date("Y-m-d", strtotime($bookingdate));
                    $cart_hospitality_data['time']        = $bookingtime;
                    $cart_hospitality_data['elocation']        = $elocation;
                    $cart_hospitality_data = CartHospitality::create($cart_hospitality_data); //cart hospitality data entry 




                    $packege = ProductPackage::select('id', 'shop_id', 'attribute_id', 'item_id', 'packege_id')->where('packege_id','=',$product_id)->with('getProductPackage:pro_id,pro_title,pro_title_ar,pro_Img,pro_desc,pro_desc_ar,pro_price,attribute_id')->get();
                    if(!empty($packege)){
                        $cart_package = array();
                        foreach ($packege as $key => $value) {
                            
                        $cart_package['cart_id'] = $cart->id;
                        $cart_package['cart_type'] = $cart_type;
                        $cart_package['product_id'] = $product_id;
                        $cart_package['package_id'] = $value->getProductPackage->pro_id;
                        $cart_package['pro_title'] = $value->getProductPackage->pro_title;
                        $cart_package['pro_title_ar'] = $value->getProductPackage->pro_title_ar;
                        $cart_package['pro_desc'] = $value->getProductPackage->pro_desc;
                        $cart_package['pro_desc_ar'] = $value->getProductPackage->pro_desc_ar;
                        $cart_package['pro_price'] = $value->getProductPackage->pro_price;
                        $cart_package['pro_img'] = $value->getProductPackage->pro_Img;
                        CartProductPackage::create($cart_package); //Cart Service Entery entery
                        }
                        
                    } 

                     if(\Config::get('app.locale') == 'ar')
        {
          $stcart= "وأضاف المنتج في العربة.";
        }
        else
        {
          $stcart="Product successfully added in cart.";
        } 
                    return Redirect::back()->with('status',$stcart);

                    
                } else {
                
                    return Redirect::back()->with('status','Something went wrong');
                }
                }

            } catch (ModelNotFoundException $e) {
              return Redirect::back()->with('status','Something went wrong');
            }
 

}








        // GET THE ROSES DETAIL PAGE //     
        public function rosesShopdetails(Request $request)
        {


            $VendorID = request()->id;
            $type = request()->type;
            $SubCatID = 15;          
            $AttrID = 50;    
            $CatID = 11;
            $lang=Session::get('lang_file');
          //echo $VendorID;die;
            try {

                    $User = Auth::user();
                    //GET MAIN CATEGORY
                    $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $CatID)->first();
                    //GET CLIENT REVIEW
                    $client_review = Category::get_category_review($VendorID);

                    if(!empty($category)) {

                  if ($SubCatID != '' && $lang  != 'en_lang') {
                                /* FOR ARABIC LANGUAGE */
                                //GET SUB CATEGORY


                                $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                                //GET VENDOR DETAIL
                            if(isset($lang) && $lang == 'ar_lang') 
                            {
                                $vendor = Category::select('mc_id', 'address_ar as address','mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'mc_video_description','google_map_address','mc_video_description_ar','budget_in_percentages','latitude','longitude','vendor_id','terms_conditions','terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                                //GET VENDOR DETAIL
                            }
                            else
                            {
                                $vendor = Category::select('mc_id', 'address','mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'mc_video_description','google_map_address','budget_in_percentages','latitude','longitude','vendor_id','terms_conditions','terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first(); 
                            }
                                $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('services_id', '=', $SubCatID)->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();


                                if (!$menu->isEmpty()) {                    

                                foreach($menu as $key => $value) {
                                $product = Products::select('pro_id','packege', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $VendorID)->where('attribute_id', '=', $AttrID)->where('pro_status', '=', 1)->where('packege', '=', 'yes')->orderBy('pro_title', 'ASC')->paginate(9);
                                $value->product = $product;
                                foreach($product as $val) {
                                if($val->packege == 'yes'){
                                $packege = ProductPackage::where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title_ar as pro_title,pro_Img,pro_desc_ar as pro_desc,pro_price,attribute_id')->get();
                                $val->product_packege =  $packege;

                                }  
                                $name = explode(',', $val->option_id);
                                //get product option detail
                                $pro_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'services_id')->where('services_id', $val->pro_mc_id)->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                                $val->product_option = $pro_Option;
                                foreach($pro_Option as $option_val) {

                                $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('product_option_id', '=', $option_val->id)->where('product_id', '=', $val->pro_id)->get();


                                $option_val->product_option_value = $product_Option_value;
                                }

                                }    
                                }

                                } else {
                                // IF NO RECORD FOUND START // 
                                if($menu->isEmpty() )
                                {
                                if ($lang != 'en_lang'){
                                Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                }
                                else
                                {
                                Session::flash('message', "Shop has no item");
                                }
                                return redirect()->route('roses',['id'=>11,'sid'=>$SubCatID]);
                                }
                                // IF NO RECORD FOUND END // 
                                }


                    } else {
                    /*
                    Language English
                    */
                    //GET SUB CATEGORY



                    $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();

                    $catg = Category::where('mc_id', '=', $SubCatID)->get();
                    //GET VENDOR DETAIL
                    
                    if(isset($lang) && $lang == 'ar_lang') 
                    {
                    $vendor = Category::select('mc_id','address', 'mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','google_map_address','mc_video_description_ar as mc_video_description','vendor_id','terms_conditions','latitude','longitude','terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                    }
                    else
                    {
                      $vendor = Category::select('mc_id','address', 'mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','google_map_address','mc_video_description','vendor_id','terms_conditions','latitude','longitude','terms_condition_name')->where('mc_status',1)->where('mc_id',$VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();  
                    }
                    
                    //GET VENDOR DETAIL
                    $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('services_id', '=', $SubCatID)->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();


                    if (!$menu->isEmpty()) {                    

                    foreach($menu as $key => $value) {
                    $product = Products::select('pro_id','packege', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $VendorID)->where('attribute_id', '=', $AttrID)->where('pro_status', '=', 1)->where('packege', '=', 'yes')->orderBy('pro_title', 'ASC')->paginate(9);
                    $value->product = $product;
                    foreach($product as $val) {
                    if($val->packege == 'yes'){
                    $packege = ProductPackage::where('shop_id','=',$val->pro_mc_id)->where('packege_id','=',$val->pro_id)->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
                    $val->product_packege =  $packege;

                               }  
                    $name = explode(',', $val->option_id);
                    //get product option detail
                    //$pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->where('services_id', $val->pro_mc_id)->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                    $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->whereIn('id', $name)->with('getBoxType:id,option_title,type')->get();
                    $val->product_option = $pro_Option;
                    foreach($pro_Option as $option_val) {

                    $product_Option_value = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('product_option_id', '=', $option_val->id)->where('product_id', '=', $val->pro_id)->get();




                    $option_val->product_option_value = $product_Option_value;
                    }

                    }    
                    }



                    } else {
                    // IF NO RECORD FOUND START // 
                    if($menu->isEmpty() )
                    {
                    if ($lang != 'en_lang'){
                    Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                    }
                    else
                    {
                    Session::flash('message', "Shop has no item");
                    }
                    return redirect()->route('roses',['id'=>11,'sid'=>$SubCatID]);
                    }
                    // IF NO RECORD FOUND END // 
                    }
                    }



                    $vendordetails = $vendor; 

                    $allreview  = $client_review;

                    return view('newWebsite.rose_shops',compact('product','catg','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang'));
                    } else {

                    return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');

                    }
                } catch (ModelNotFoundException $e) {
                   return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');
                }

     
        }



public function getpagination(Request $request)
{
      
$page = $request->page;
$tabid = $request->tabid;
$pro_mc_id = $request->pro_mc_id;
$attribute_id = $request->attribute_id;


               
$lang=Session::get('lang_file');
if ($lang  != 'en_lang') {

$product = Products::select('pro_id', 'pro_title_ar as pro_title','pro_desc', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', 126)->where('attribute_id', '=', $attribute_id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9);
}
else
{
   $product = Products::select('pro_id', 'pro_title','pro_desc', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', 126)->where('attribute_id', '=', $attribute_id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->paginate(9); 
}

return view('occasioncoordinator.specialevent_pagination',compact('product','tabid'));
   
}

public function setSpecialEventAddToCart(Request $request)
{
 try {
        $product_qty = $request->product_qty;
        $total_price = $request->total_price;
        $product_id = $request->product_id;
        $attribute_id = $request->attribute_id;
        $cart_sub_type = $request->cart_sub_type;
        $cart_type = $request->cart_type;
        $product_type=$request->product_type;
 
                //$User = Auth::user();

                $User = Session::get('customerdata'); 
                 $UserID =  $User['user_id'];
                
                $product = Products::where('pro_id', '=', $product_id)->first();

               if($product->pro_qty >= $product_qty){
                    $category = Category::select('mc_id','vendor_id','parent_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $UserID)->first();
                    if (!empty($cart)) {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        
                    } else {
                        $cart_data = array();
                        $cart_data['user_id'] = $UserID;
                        $cart = Cart::create($cart_data); //cart entery
                    }
                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $category->vendor_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['category_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $cart_type;
                    $cart_product_data['cart_sub_type'] = $cart_sub_type;
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $product_id;
                    $cart_product_data['quantity'] = $product_qty;
                    if($product_type=='sepeevent'){
                            $cart_product_data['total_price'] = $total_price*$product_qty;
                    }else{
                         $cart_product_data['total_price'] = $total_price;
                    }
                    $cart_product_data['status'] = 1;
                    $cart_product_data['shop_vendor_id'] = $category->parent_id;
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_price;
                    $cart_product_data['review_type']      = 'shop';
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 

                    $menu = ServiceAttribute::select('id','attribute_title_ar','attribute_title')->where('id', '=', $attribute_id)->first(); 
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $cart_type;
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $product_id;
                    $cart_attribute_data['attribute_id'] = $attribute_id;
                    $cart_attribute_data['attribute_title'] = $menu->attribute_title;
                    $cart_attribute_data['attribute_title_ar'] = $menu->attribute_title_ar;
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                    if(\Config::get('app.locale') == 'ar')
                    {
                    Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                    Session::flash('status', "Product added in the cart.");
                    } 
                      $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {
                    return Redirect::back()->with('message','وأضاف المنتج بنجاح في العربة'); 
                    }

                    else
                    {
                   return Redirect::back()->with('message','Product succesfully added in the cart.');
                    }


                 }
                
                else if ($product->pro_qty <= $product_qty && $product->pro_qty > 0)
                {
                    $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {
                    return Redirect::back()->with('message','الكمية المنتج غير متوفرة'); 
                    }

                    else
                    {
                   return Redirect::back()->with('message','Product qty is not available.');
                    }
                    

                } 
                else if ($product->pro_qty == 0) 
                {
                      $lang=Session::get('lang_file'); 
                    if( $lang  != 'en_lang') {
                    return Redirect::back()->with('message','هناك خطأ ما'); 
                    }

                    else
                    {
                   return Redirect::back()->with('message','Something went wrong.');
                    }
                }

            } catch (ModelNotFoundException $e) {
                  
                   return Redirect::back()->with('message','Something went wrong.');
                    
            }





}
 


        // GET THE SPECIAL EVENT DETAIL PAGE // 
        public function specialeventsShopdetails(Request $request)
        {

   
            $VendorID = request()->id;
            $VID = request()->vid;
            $type = request()->type;
            $SubCatID = 17;
            if($type!='2')
            {
            $AttrID = 142;
            }
            else
            {
            $AttrID = 143;      
            }         
           
             $CatID = 11;
            $lang=Session::get('lang_file');
 
            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $CatID)->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($VendorID);

                if (!empty($category)) {

                if ($SubCatID != '' && $lang  != 'en_lang') {
                /* FOR ARABIC LANGUAGE */
                //GET SUB CATEGORY
           


                        $sub_catg = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();

                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'address_ar as address','google_map_address','latitude','longitude','mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url','mc_video_description','mc_video_description_ar','mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                         //GET VENDOR DETAIL
                       
                        $getV = $vendor->vendor_id;
                        $AttrLoop = array();
                        $ActivemenuC = ServiceAttribute::select('id')->where('status', '=', 1)->where('vendor_id', '=', $getV)->count();
                        if($ActivemenuC >=1)
                        {
                        $Activemenu = ServiceAttribute::select('id')->where('status', '=', 1)->where('vendor_id', '=', $getV)->get();

                        foreach ($Activemenu as $key => $value)
                        {
                        $Vid =  $value->id;


                        $Cproduct = Products::select('pro_id')->where('attribute_id', '=',  $Vid)->where('pro_status', '=', 1)->count();
                        if($Cproduct >=1)
                        {
                        array_push($AttrLoop, $Vid); 
                        }

                        }


                        } 


                           // $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->whereIn('id', $AttrLoop)->where('services_id', '=', $VendorID)->orderBy('attribute_title', 'ASC')->get();


                       $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title','attribute_title_ar', 'parent', 'image', 'status')->whereIn('id', $AttrLoop)->where('status', '=', 1)->where('services_id', '=', $VendorID)->orderBy('attribute_title', 'ASC')->get();
 
 
                        if (!$menu->isEmpty()) {                    
                         foreach($menu as $key => $value) {
                       $product = Products::select('pro_id', 'pro_title_ar as pro_title','pro_desc_ar as pro_desc', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage','pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->get();

                                 $value->product = $product;
                              
                                    
                             }
                        $product_Option_value_type = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=',25)->where('vandor_id', '=', $VID)->get();

                        $product_Option_value_design = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=', 26)->where('vandor_id', '=', $VID)->get();
                        
                        } else {
                                 
                            // IF NO RECORD FOUND START // 
                                if($menu->isEmpty() )
                                {
                                if ($lang != 'en_lang'){
                                Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                }
                                else
                                {
                                Session::flash('message', "Shop has no item. Please try again");
                                }
                                return redirect()->route('roses',['id'=>11,'sid'=>$SubCatID]);
                                }


                             // IF NO RECORD FOUND END //  
                        }
                    } else {
                        /*
                        Language English
                        */
                         //GET SUB CATEGORY
                            


                        $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'address','mc_name','google_map_address', 'latitude','longitude','mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','mc_video_description','mc_video_description_ar','terms_conditions','terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                         //GET VENDOR DETAIL
                     
                        $getV = $vendor->vendor_id;
                        $AttrLoop = array();
                        $ActivemenuC = ServiceAttribute::select('id')->where('status', '=', 1)->where('vendor_id', '=', $getV)->count();
                        if($ActivemenuC >=1)
                        {
                        $Activemenu = ServiceAttribute::select('id')->where('status', '=', 1)->where('vendor_id', '=', $getV)->get();

                        foreach ($Activemenu as $key => $value)
                        {
                        $Vid =  $value->id;


                        $Cproduct = Products::select('pro_id')->where('attribute_id', '=',  $Vid)->where('pro_status', '=', 1)->count();
                        if($Cproduct >=1)
                        {
                        array_push($AttrLoop, $Vid); 
                        }

                        }


                        } 


                            $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('status', '=', 1)->whereIn('id', $AttrLoop)->where('services_id', '=', $VendorID)->orderBy('attribute_title', 'ASC')->get();
 
                        if (!$menu->isEmpty()) {                    

                         foreach($menu as $key => $value) {  
                                  

                        $product = Products::select('pro_id', 'pro_title','pro_desc', 'pro_Img', 'pro_desc', 'pro_price', 'pro_mc_id','pro_disprice','pro_discount_percentage', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $value->services_id)->where('attribute_id', '=', $value->id)->where('pro_status', '=', 1)->orderBy('pro_title', 'ASC')->get();

                                 $value->product = $product;
                              
                             }

                            $product_Option_value_type = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=', 25)->where('vandor_id', '=', $VID)->get();

                            $product_Option_value_design = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=', 26)->where('vandor_id', '=', $VID)->get();
 
 
                        } else {
                                // IF NO RECORD FOUND START // 
                                if($menu->isEmpty() )
                                {
                                if ($lang != 'en_lang'){
                                Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                }
                                else
                                {
                                Session::flash('message', "Shop has no item");
                                }
                                return redirect()->back();
                                }
                                // IF NO RECORD FOUND END // 
                        }
                    }

 
 
                   $vendordetails = $vendor; 
           
                   $allreview  = $client_review;

                    return view('occasioncoordinator.special-events-details',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang','product_Option_value_type','product_Option_value_design'));
                    } else {

                     return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');

                    }
                } catch (ModelNotFoundException $e) {
                   return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');
                }


     
        }


        public function singleRosesdetails(Request $request)
        {

             
            $VendorID = request()->id;
            $VID = request()->vid;
            $type = request()->type;
            $SubCatID = 15;
            if($type!='2')
            {
            $AttrID = 142;
            }
            else
            {
            $AttrID = 143;      
            }         
           
             $CatID = 11;
            $lang=Session::get('lang_file');
 
            try {

                $User = Auth::user();
                //GET MAIN CATEGORY
                $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $CatID)->first();

                //GET CLIENT REVIEW
                $client_review = Category::get_category_review($VendorID);

                if (!empty($category)) {

                if ($SubCatID != '' && $lang  != 'en_lang') {
                /* FOR ARABIC LANGUAGE */
                //GET SUB CATEGORY
                        $sub_catg = Category::select('mc_id', 'mc_name_ar as mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id','address_ar as address', 'google_map_address','latitude','longitude','mc_name_ar as mc_name', 'mc_discription_ar as mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions_ar as terms_conditions','terms_condition_name_ar as terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name_ar as ci_name')->with('CategoryGallery:id,image,title_ar as title,category_id')->first();
                         //GET VENDOR DETAIL
                         $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title_ar as attribute_title', 'parent', 'image', 'status')->where('services_id', '=', $SubCatID)->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();

 
 
                        if (!$menu->isEmpty()) {  
                             foreach($menu as $key => $value) {
                                 $product = Products::select('pro_id','packege', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $VendorID)->where('attribute_id', '=', 51)->where('pro_status', '=', 1)->orderBy('pro_id', 'ASC')->paginate(9);
                                 $value->product = $product;
                             }
                        $product_Option_value_type = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=',25)->where('vandor_id', '=', $VID)->get();

                        $product_Option_value_design = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=', 26)->where('vandor_id', '=', $VID)->get();
                        
                        } else {
                                 
                            // IF NO RECORD FOUND START // 
                                if($menu->isEmpty() )
                                {
                                if ($lang != 'en_lang'){
                                Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                }
                                else
                                {
                                Session::flash('message', "Shop has no item. Please try again");
                                }
                                return redirect()->route('roses',['id'=>11,'sid'=>$SubCatID]);
                                }


                             // IF NO RECORD FOUND END //  
                        }
                    } else {
                        /*
                        Language English
                        */
                         //GET SUB CATEGORY
                            


                        $sub_catg = Category::select('mc_id', 'mc_name', 'mc_status')->where('mc_id', '=', $SubCatID)->first();
                         //GET VENDOR DETAIL
                         $vendor = Category::select('mc_id', 'address','google_map_address','latitude','longitude','mc_name', 'mc_discription', 'city_id', 'mc_video_url', 'mc_img', 'budget_in_percentages','vendor_id','terms_conditions','terms_condition_name')->where('mc_status', '=', 1)->where('mc_id', '=', $VendorID)->with('getCity:ci_id,ci_name')->with('CategoryGallery:id,image,title,category_id')->first();
                         //GET VENDOR DETAIL
                         $menu = ServiceAttribute::select('id', 'services_id', 'vendor_id', 'attribute_title', 'parent', 'image', 'status')->where('services_id', '=', $SubCatID)->where('parent', '=', '0')->orderBy('attribute_title', 'ASC')->get();
 
                        if (!$menu->isEmpty()) {                    

                         foreach($menu as $key => $value) {  
                                 $product = Products::select('pro_id','packege', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_mc_id', '=', $VendorID)->where('attribute_id', '=', 51)->where('pro_status', '=', 1)->orderBy('pro_id', 'ASC')->paginate(9);
                                 $value->product = $product;
                              
                             }

                            $product_Option_value_type = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=', 25)->where('vandor_id', '=', $VID)->get();

                            $product_Option_value_design = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('status', '=', 1)->where('product_option_id', '=', 26)->where('vandor_id', '=', $VID)->get();
 
 
                        } else {
                                // IF NO RECORD FOUND START // 
                                if($menu->isEmpty() )
                                {
                                if ($lang != 'en_lang'){
                                Session::flash('message', "هناك خطأ ما. حاول مرة اخرى");
                                }
                                else
                                {
                                Session::flash('message', "Shop has no item");
                                }
                                return redirect()->route('roses',['id'=>11,'sid'=>$SubCatID]);
                                }
                                // IF NO RECORD FOUND END // 
                        }
                    }

 
 
                   $vendordetails = $vendor; 
           
                   $allreview  = $client_review;

                    return view('newWebsite.roses_single',compact('product','sub_catg','vendor','menu','client_review','vendordetails','allreview','lang','product_Option_value_type','product_Option_value_design'));
                    } else {

                     return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');

                    }
                } catch (ModelNotFoundException $e) {
                   return redirect()->route('occasion-co-ordinator')->with('message','Something Went Wrong!');
                }

        }


public function getsingleProductInfo(Request $request)
{
    try
    {
        $lang=Session::get('lang_file');
        $getPid = $request->product_id;
        if($lang  != 'en_lang')
        {
        $product = Products::select('pro_id','packege', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_id', '=', $getPid)->get();
        }
        else
        {
        $product = Products::select('pro_id','packege', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_id', '=', $getPid)->get();
        }
 

        $pro_Img = $product[0]->pro_Img;
        $pro_desc = $product[0]->pro_desc;
        $proTitle = $product[0]->pro_title; 
        $OriginalP = $product[0]->pro_price; 
        $OriginalDis = $product[0]->pro_discount_percentage;
        $pro_qty = $product[0]->pro_qty;
        if($OriginalDis==''){
          $Disp = $OriginalP;  
        }
        else
        {
            $Disp = $OriginalP - ($OriginalP*$OriginalDis)/100;
        }
        
        $Disp = number_format((float)$Disp, 2, '.', '');  
        $Current_Currency = Session::get('currency'); 
        $OriginalP = currency($OriginalP, 'SAR',$Current_Currency, $format = false);
        $Disp = currency($Disp, 'SAR',$Current_Currency, $format = false);


        return $proTitle.'~~~'.$OriginalP.'~~~'.$Disp.'~~~'.$pro_Img.'~~~'.$pro_desc.'~~~'.$pro_qty;
    }
    catch (ModelNotFoundException $e) 
    {
        return 0;  
    }
       
 
}


public function getProductInfo(Request $request)
{ 
     try
    {
            $lang=Session::get('lang_file');
            $getPid = $request->product_id;

           if($lang  != 'en_lang')
            { 

            $product = Products::select('pro_id','packege', 'pro_title_ar as pro_title', 'pro_Img', 'pro_desc_ar as pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_id', '=', $getPid)->first();    
            $packege = ProductPackage::where('shop_id','=',$product->pro_mc_id)->where('packege_id','=',$product->pro_id)->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
            $product->product_packege =  $packege;
            $name = explode(',', $product->option_id);
            //get product option detail
            $pro_Option = ProductOption::select('id', 'option_title_ar as option_title', 'button_type_id', 'services_id')->where('services_id', $product->pro_mc_id)->whereIn('id', [25,26])->with('getBoxType:id,option_title,type')->get();
            $product->product_option = $pro_Option;
            $product_Option_value_Type = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('product_option_id', '=', 25)->where('product_id', '=', $getPid)->get();
            $product_Option_value_Design = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title_ar as option_title', 'image', 'price', 'status')->where('product_option_id', '=', 26)->where('product_id', '=', $getPid)->get();

            }
            else
            {
            $product = Products::select('pro_id','packege', 'pro_title', 'pro_Img', 'pro_desc', 'pro_price','pro_disprice','pro_discount_percentage', 'pro_mc_id', 'attribute_id', 'option_id', 'pro_qty')->where('pro_id', '=', $getPid)->first();    
            $packege = ProductPackage::where('shop_id','=',$product->pro_mc_id)->where('packege_id','=',$product->pro_id)->with('getProductPackage:pro_id,pro_title,pro_Img,pro_desc,pro_price,attribute_id')->get();
            $product->product_packege =  $packege;
            $name = explode(',', $product->option_id);
            //get product option detail
             $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->whereIn('id', [25,26])->with('getBoxType:id,option_title,type')->get();
           // $pro_Option = ProductOption::select('id', 'option_title', 'button_type_id', 'services_id')->where('services_id', $product->pro_mc_id)->whereIn('id', [25,26])->with('getBoxType:id,option_title,type')->get();
            $product->product_option = $pro_Option;
            $product_Option_value_Type = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('product_option_id', '=', 25)->where('product_id', '=', $getPid)->get();
            $product_Option_value_Design = ProductOptionValue::select('id', 'vandor_id', 'product_id', 'product_option_id', 'option_title', 'image', 'price', 'status')->where('product_option_id', '=', 26)->where('product_id', '=', $getPid)->get();
            }

                $product->product_Option_value_Type = $product_Option_value_Type;
                $product->product_Option_value_Design = $product_Option_value_Design;
                $pro_Img = $product->pro_Img;
                $pro_id = $product->pro_id;
                $pro_qty = $product->pro_qty;
                $proTitle = $product->pro_title;
                $proCount = count($product->product_packege);
                $OriginalP = $product->pro_price; 
                $OriginalDis = $product->pro_discount_percentage;
                $Disp = $OriginalP - ($OriginalP*$OriginalDis)/100;
                $Disp = number_format((float)$Disp, 2, '.', '');
                $Wrappingtype = $product->product_option[0]->option_title; 
                $Wrappingdesigna = $product->product_option[1]->option_title;
                $getType = $product->product_Option_value_Type; 
                $Type_OptionTitle =  $getType[0]->option_title;
                $Type_image =  $getType[0]->image;
                $Type_price =  $getType[0]->price;
                $WrappingDesign = $product->product_Option_value_Design; 
                $Design_OptionTitle =  $WrappingDesign[0]->option_title;
                $Design_image =  $WrappingDesign[0]->image;
                $Design_price =  $WrappingDesign[0]->price;

                $Current_Currency = Session::get('currency');

                $Design_price = currency($Design_price, 'SAR',$Current_Currency, $format = false);
                 $Type_price = currency($Type_price, 'SAR',$Current_Currency, $format = false);
                $Disp = currency($Disp, 'SAR',$Current_Currency, $format = false);
                $OriginalP = currency($OriginalP, 'SAR',$Current_Currency, $format = false);

                return $proTitle.'~~~'.$proCount.'~~~'.$OriginalP.'~~~'.$Disp.'~~~'.$Wrappingtype.'~~~'.$Wrappingdesigna.'~~~'.$pro_Img.'~~~'.$Type_OptionTitle.'~~~'.$Type_image.'~~~'.$Type_price.'~~~'.$Design_OptionTitle.'~~~'.$Design_image.'~~~'.$Design_price.'~~~'.$pro_id.'~~~'.$pro_qty;
       
    }
    catch (ModelNotFoundException $e) 
    {
    return 0;
    }
 

} 

//Set roses data in add to cart
    public function setRosesPackageAddToCart(Request $request) {
                try {

                 
 if (isset($request->product_option_type) && $request->product_option_type!='') {
        $Otype =  $request->product_option_type; 
        $Dtype =  $request->product_option_design;
        $product_option_value = array($Otype,$Dtype);
 }


                $lang=Session::get('lang_file'); 
                $product_id = $request->product_id;
                $cart_type = $request->cart_type;
                $total_price = $request->total_price;
                $language_type = $request->language_type;
                //$product_qty = $request->product_qty;
               $page_type=$request->page_type;
                $product_qty = $request->itemqty;
                if($product_qty==''){
                    $product_qty = $request->product_qty;
                }
               



                $attribute_id = $request->attribute_id;
                $cart_sub_type = $request->cart_sub_type;
 
                $User = Session::get('customerdata'); 
                 $UserID =  $User['user_id'];
                
                $product = Products::where('pro_id', '=', $product_id)->first();

                if (!empty($product)) {
                    $category = Category::select('mc_id','vendor_id')->where('mc_id', '=', $product->pro_mc_id)->first();
                    $cart = Cart::where('user_id', '=', $UserID)->first();
                    if (!empty($cart)) 
                    {
                        $cart_pro = CartProduct::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        $cart_option = CartOption::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();

                        $cart_option_value = CartOptionValue::where('cart_type', '=', 'occasion')->where('cart_id', '=', $cart->id)->where('product_id', '=', $product_id)->delete();
                    } 
                    else 
                    {
                        $cart_data = array();
                        $cart_data['user_id'] = $UserID;
                        $cart = Cart::create($cart_data); //cart entery
                    }

                    $cart_product_data = array();
                    $cart_product_data['merchant_id'] = $product->pro_mr_id;
                    $cart_product_data['shop_id'] = $category->mc_id;
                    $cart_product_data['cart_type'] = $cart_type;
                    $cart_product_data['cart_sub_type'] = $cart_sub_type;
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $product_id;

                    $getProRec = Products::where('pro_id',$product_id)->first();
                    $cart_product_data['pro_title'] = $getProRec->pro_title;
                    $cart_product_data['pro_title_ar'] = $getProRec->pro_title_ar;
                    $cart_product_data['pro_Img'] = $getProRec->pro_Img;
                    $cart_product_data['pro_desc'] = $getProRec->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $getProRec->pro_desc_ar;
                    $cart_product_data['review_type']      = 'shop';

                    if ($product_qty != '') {
                        $cart_product_data['quantity'] = $product_qty;
                    } else {
                        $cart_product_data['quantity'] = '';
                    }
                    //
                    if($page_type=='singlerose'){
                     $cart_product_data['total_price'] =  $total_price;
                    }else{
                        $cart_product_data['total_price'] = $product_qty * $total_price;
                    }
                    $cart_product_data['status'] = 1;
                    $cart_product_data['category_id'] = $category->mc_id;
                    
                    //echo "<pre>"; print_r($cart_product_data);
                    //die;
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry 

                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $cart_type;
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $product_id;
                    $cart_attribute_data['attribute_id'] = $attribute_id;
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); //cart product attribute entry*/
                    

                    if (isset($product_option_value)) {
                        $product_options = ProductOptionValue::select('product_option_id')->whereIn('id', $product_option_value)->distinct()->get()->toArray();
                        $product_options_ids = array();
                        foreach($product_options as $key => $value) {
                            $product_options_ids[] = $value['product_option_id'];
                        }

                        foreach($product_options_ids as $key => $value) {
                            $cart_option_data = array();
                            $cart_option_data['cart_type'] = $cart_type;
                            $cart_option_data['product_option_id'] = $value;
                            $cart_option_data['cart_id'] = $cart->id;
                            $cart_option_data['product_id'] = $product_id;
                            CartOption::create($cart_option_data); //cart option entry
                        }

                        $product_options_value = ProductOptionValue::whereIn('id', $product_option_value)->get();
                        $total_price = array();
                        foreach($product_options_value as $value) {
                            $cart_option_value = array();
                            $cart_option_value['cart_type'] = $cart_type;
                            $cart_option_value['cart_id'] = $cart->id;
                            $cart_option_value['product_id'] = $product_id;
                            $cart_option_value['product_option_id'] = $value->product_option_id;
                            $cart_option_value['product_option_value_id'] = $value->id;
                            $cart_option_value['option_title_ar'] = $value->option_title_ar;
                            $cart_option_value['option_title'] = $value->option_title;
                            $cart_option_value['price'] = $value->price;
                            $cart_option_value['file'] = $value->image;
                            $cart_option_value['value'] = $value->price;
                           $cart_option_value['status'] = 1;
                           $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                           $cart_product = CartProduct::where('product_id','=',$cart_value->product_id)->first();
                           if($page_type=='singlerose'){
                                $total_price['total_price'] = $cart_product->total_price;
                           }else{
                                 $total_price['total_price'] = $cart_product->total_price + $cart_value->value;
                           }
                          
                           $update_product = CartProduct::where('product_id','=',$cart_value->product_id)->update($total_price);

                        }
                    }
                        if(\Config::get('app.locale') == 'ar')
                    {
                    Session::flash('status', "وأضاف المنتج في العربة.");
                    }
                    else
                    {
                    Session::flash('status', "Product added in the cart.");
                    } 

                        if ($lang != 'en_lang'){
                        return Redirect::back()->with('message','تمت إضافة بيانات  ارتفع في السلة بنجاح');
                        }
                        else{
                        return Redirect::back()->with('message','Product added in the cart.');

                        }
      
                } else {
                    return Redirect::back()->with('message','Something went wrong');
                }

            } catch (ModelNotFoundException $e) {
                return Redirect::back()->with('message','Something went wrong');
            }

        
        //return $response;

    }



    public function getElectroniclavel(Request $request)
    {
        $getPid = $request->id;
         $product = Products::select('pro_label', 'pro_label_ar')->where('pro_id', '=', $getPid)->first(); 
      
     

          if(\Config::get('app.locale') == 'ar')
                    {
                    $proname =  $product->pro_label_ar;
                    }
                    else
                    {
                    $proname =   $product->pro_label;       
                    }
            return $proname;

    }



}
<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\HallOffer;
//------------wisitech code end -----------//
class BranchhallslistController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function hallslist(Request $request,$halltype,$typeofhallid,$shopid,$branchid)
    {
 		//print_r($_REQUEST);
		//echo $halltype;
		$id=$request->id;
		$halltype=$halltype;
		$typeofhallid=$typeofhallid;
		$shopid=$shopid;
		$branchid=$branchid;
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		 $noofattendees = Session::get('searchdata.noofattendees'); 
		$budget=Session::get('searchdata.budget');
		$category_id=$shopid;
		 $gender=Session::get('searchdata.gender');
    if($gender=='male'){
        $searchgender='men';
      }elseif($gender=='female'){
        $searchgender='women';
      }else {
        $searchgender='both';
      }
      $searchgender=explode(',', $searchgender);
		$value = $request->session()->get('searchdata');
		$lang=Session::get('lang_file');
		$Formbusinesstype= new Formbusinesstype();
		if($basecategory_id==''){$basecategory_id=Session::get('searchdata.mainselectedvalue');}
		if($basecategory_id==1){ $hall_category_type=2; }else{ $hall_category_type=1; }
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
		  
			 $product = Products::select('pro_id', 'pro_title', 'pro_title_ar')->where('pro_mc_id', '=', $branchid)->first(); 
			   if (!empty($product)) {
                 if ($branchid != '' && $lang == 'ar_lang') { 
                  $branchhalls = Products::where('pro_mc_id', '=', $branchid)->where('hallcapicity', '>=', "'.$noofattendees.'")->where('pro_netprice', '<=', $budget)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->whereIn('hall_type', $searchgender)->where('pro_status', '=', 1) ->where('pro_mc_id', '=', $branchid)->select('pro_id', 'pro_title_ar as pro_title', 'pro_img','pro_price')->get();   
 
				  $abovebudget = Products::where('pro_mc_id', '=', $branchid)->where('hallcapicity', '>=', "'.$noofattendees.'")->where('pro_netprice', '>', $budget)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->whereIn('hall_type', $searchgender)->where('pro_status', '=', 1) ->where('pro_mc_id', '=', $branchid)->select('pro_id', 'pro_title_ar as pro_title', 'pro_img','pro_price')->get();

				  $offerbudget = Products::where('pro_mc_id', '=', $branchid)->where('hallcapicity', '>=', "'.$noofattendees.'")->whereIn('hall_type', $searchgender)->where('pro_disprice', '!=', '')->where('pro_mc_id', '=', $branchid)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->where('hall_category_type', '=', $basecategory_id)->where('pro_status', '=', 1) ->select('pro_id', 'pro_title_ar as pro_title', 'pro_img','pro_price','pro_disprice')->get(); 
				             } else {
				             	
                   $branchhalls = Products::where('pro_mc_id', '=', $branchid)->where('hallcapicity', '>=', "'.$noofattendees.'")->where('pro_netprice', '<=', $budget)->where('pro_status', '=', 1) ->where('pro_mc_id', '=', $branchid)->whereIn('hall_type', $searchgender)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title', 'pro_img','pro_price','pro_disprice')->get();
                  
					$abovebudget = Products::where('pro_mc_id', '=', $branchid)->where('pro_status', '=', 1) ->where('hallcapicity', '>=', "'.$noofattendees.'")->where('pro_netprice', '>', $budget)->where('pro_mc_id', '=', $branchid)->whereIn('hall_type', $searchgender)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title', 'pro_img','pro_price','pro_disprice')->get();


					  $offerbudget = Products::where('pro_mc_id', '=', $branchid)->where('pro_status', '=', 1) ->where('hallcapicity', '>=', "'.$noofattendees.'")->where('pro_disprice', '!=', '')->where('pro_mc_id', '=', $branchid)->whereIn('hall_type', $searchgender)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title', 'pro_img','pro_price','pro_disprice','pro_disprice')->get();     
                  }
               }
			
			
            $getbranchhalls = $branchhalls;
			$abovecustomerbudget=$abovebudget;
			$offercustomerbudget=$offerbudget;
			
        return view('newWebsite.halls_list', compact('getbranchhalls','bussinesstype','halltype','typeofhallid','category_id','shopid','branchid','abovecustomerbudget','offercustomerbudget','id'));
 
    }

    public function checkflxslider(Request $request,$halltype,$typeofhallid,$shopid,$branchid,$showData_One,$showData_Two,$showData_three)
    {

    	$halltype=$halltype;
		$typeofhallid=$typeofhallid;
		$shopid=$shopid;
		$branchid=$branchid;

		 $gender=Session::get('searchdata.gender');
    if($gender=='male'){
        $searchgender='men';
      }elseif($gender=='female'){
        $searchgender='women';
      }else {
        $searchgender='both';
      }
		$searchgender=explode(',', $searchgender);
		
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		$budget=Session::get('searchdata.budget');
		$category_id=$shopid;
		//$request->session()->push('searchdata.shopid', $category_id);
		//print_r(Session::get('searchdata'));
		 $value = $request->session()->get('searchdata');
		$lang=Session::get('lang_file');
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	


		  if($basecategory_id==''){$basecategory_id=Session::get('searchdata.mainselectedvalue');}
		if($basecategory_id==1){ $hall_category_type=2; }else{ $hall_category_type=1; }
		  
			 $product = Products::select('pro_id', 'pro_title', 'pro_title_ar')->where('pro_mc_id', '=', $branchid)->first(); 
			   if (!empty($product)) {
                 if ($branchid != '' && $lang == 'ar_lang') {

                  $branchhalls = Products::where('pro_mc_id', '=', $branchid)->where('pro_netprice', '<=', $budget)->where('pro_status', '=', 1)->whereIn('hall_type', $searchgender)->where('pro_mc_id', '=', $branchid)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title_ar as pro_title', 'pro_img','pro_price','pro_disprice','pro_discount_percentage')->get(); 
				  
				  $abovebudget = Products::where('pro_mc_id', '=', $branchid)->where('pro_netprice', '>', $budget)->where('pro_status', '=', 1)->whereIn('hall_type', $searchgender)->where('pro_mc_id', '=', $branchid)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title_ar as pro_title', 'pro_img','pro_price','pro_disprice','pro_discount_percentage')->get();
				  

				  //$offerbudget = Products::where('pro_mc_id', '=', $branchid)->where('pro_disprice', '!=', '')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title_ar as pro_title', 'pro_img','pro_price','pro_disprice','pro_discount_percentage')->get();  

				  $todaydate = date('Y-m-d');
                            $chkOferHall = HallOffer::where('status',1)->where('start_date', '<=',$todaydate)->where('end_date', '>=',$todaydate)->get();
                            $hallArr = array();
                            foreach ($chkOferHall as $chkOferHallue) {
                            $pro_ida = $chkOferHallue->pro_id;
                            array_push($hallArr, $pro_ida);
                            } 

				  $offerbudget = Products::select('pro_id', 'pro_title_ar as pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'services_offer_id', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $branchid)->whereIn('hall_type', $searchgender)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->where('pro_status', '=', 1)->whereIn('pro_id', $hallArr)->orderBy('pro_title', 'ASC')->paginate(10000);
				            
				             } else {

                    $branchhalls = Products::where('pro_mc_id', '=', $branchid)->where('pro_netprice', '<=', $budget)->where('pro_status', '=', 1)->whereIn('hall_type', $searchgender)->where('pro_mc_id', '=', $branchid)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title', 'pro_img','pro_price','pro_disprice','pro_discount_percentage')->get();
					

					$abovebudget = Products::where('pro_mc_id', '=', $branchid)->where('pro_netprice', '>', $budget)->where('pro_status', '=', 1)->whereIn('hall_type', $searchgender)->where('pro_mc_id', '=', $branchid)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title', 'pro_img','pro_price','pro_disprice','pro_discount_percentage')->get();


					  //$offerbudget = Products::where('pro_mc_id', '=', $branchid)->where('pro_disprice', '!=', '')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchid)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->select('pro_id', 'pro_title', 'pro_img','pro_price','pro_disprice')->get(); 



						$todaydate = date('Y-m-d');
                            $chkOferHall = HallOffer::where('status',1)->where('start_date', '<=',$todaydate)->where('end_date', '>=',$todaydate)->get();
                            $hallArr = array();
                            foreach ($chkOferHall as $chkOferHallue) {
                            $pro_ida = $chkOferHallue->pro_id;
                            array_push($hallArr, $pro_ida);
                            }

  

                            $offerbudget = Products::select('pro_id', 'pro_title', 'pro_Img', 'pro_price','pro_disprice', 'pro_discount_percentage', 'services_offer_id', 'hallcapicity','Insuranceamount','food_type')->where('pro_mc_id', '=', $branchid)->whereIn('hall_type', $searchgender)->whereRaw("find_in_set($hall_category_type,hall_category_type)")->where('pro_status', '=', 1)->whereIn('pro_id', $hallArr)->orderBy('pro_title', 'ASC')->get();

                           


                 
                  }
               }
			
			
            $getbranchhalls = $branchhalls;
			$abovecustomerbudget=$abovebudget;
			$offercustomerbudget=$offerbudget;
			 
return view('includes.flxslider', compact('getbranchhalls','bussinesstype','halltype','typeofhallid','category_id','shopid','branchid','abovecustomerbudget','offercustomerbudget','showData_One','showData_Two','showData_three'));

    }
   
}

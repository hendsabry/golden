<?php

namespace App\Http\Controllers;

use DB;

use Session;

use Helper;

use App\Http\Models;

use App\Register;

use App\Home;

use App\Footer;

use App\Settings;

use App\Merchant; 

use Lang;

use File;

use Intervention\Image\ImageManagerStatic as Image; 

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;

use App\Categorylist;

//------------wisitech code atart----------//



use App\Category;

use App\Products;

use App\Gallery;

use App\ProductOption;

use App\ProductAttribute;

use App\ProductButtonType;

use App\ProductOptionValue;

use App\PoductReview;

use App\City;

use App\InternalFoodContainerPrice;

use App\InternalFoodContainer;

use App\InternalFoodMenu;

use App\InternalFoodDish;

use App\SaveCart;

use App\Cart;

use App\CartProduct;

use App\CartOption;

use App\CartOptionValue;

use App\CartInternalFoodDish;

use App\ServicesOrderOption;

use App\ServicesOrderOptionValue;

use App\CartServiceAttribute;

use App\Formbusinesstype;

//------------wisitech code end -----------//

class ShopbranchlistController extends Controller

{  

    public function __construct(){

        parent::__construct();

         $this->middleware(function ($request, $next) {

                    $this->setLanguageLocaleFront();

                    return $next($request);

    });      

    }

     

    public function branchlist(Request $request,$halltype,$typeofhallid,$shopid)

    {

 		//print_r($_REQUEST);

		//echo $halltype;

		$halltype=$halltype;

		$typeofhallid=$typeofhallid;

		$shopid=$shopid;

    $isabovebusget=0;

    if(isset($_REQUEST['isabovebusget']) && $_REQUEST['isabovebusget']>0){

    $isabovebusget=$_REQUEST['isabovebusget'];

  }

	  $getPagelimit = config('app.paginate');

    

		if(Session::get('searchdata.basecategoryid')!=''){

		$basecategory_id=Session::get('searchdata.basecategoryid');

		}else {

		$basecategory_id=Session::get('searchdata.maincategoryid');

		}

		$buget=Session::get('searchdata.budget');

		

     $gender=Session::get('searchdata.gender');

    if($gender=='male'){

        $searchgender='men';

      }elseif($gender=='female'){

        $searchgender='women';

      }elseif($gender=='Both'){

        $searchgender='both';

      }else{

        $searchgender='men,women,both';

      }

      $searchgender=explode(',', $searchgender);

		//$request->session()->push('searchdata.halltypeid', $halltype);

		$category_id=$shopid;

		$request->session()->push('searchdata.shopid', $category_id);

		//print_r(Session::get('searchdata'));

		$lang=Session::get('lang_file');

		$Formbusinesstype= new Formbusinesstype();

    if($basecategory_id==''){$basecategory_id=Session::get('searchdata.mainselectedvalue');}

		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	

			 $category = Category::select('mc_id', 'mc_name', 'mc_name_ar', 'mc_status')->where('mc_id', '=', $category_id)->first(); 

                if (!empty($category)) {

                 if ($category_id != '' && $lang == 'ar_lang') {

                  $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name_ar as mc_name', 'mc_img')->paginate('9')->appends(request()->query());

                  	foreach ($hallsubcategory as $getchildelement) {

                          $cid=$getchildelement->mc_id;

                          if(isset($isabovebusget) && $isabovebusget>0){

                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->where('pro_price', '>=', $buget)->whereIn('hall_type', $searchgender)->count();

                          }else{

                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->where('pro_price', '<=', $buget)->whereIn('hall_type', $searchgender)->count();

                            }

                            $hallsubcategory->ischild=$ischildthere;

                         }



				             } else {

                    $hallsubcategory = Category::where('mc_status', '=', 1)->where('parent_id', '=', $category_id)->select('mc_id', 'mc_name', 'mc_img')->paginate('9')->appends(request()->query());



                    	foreach ($hallsubcategory as $getchildelement) {

                          $cid=$getchildelement->mc_id;



                          if(isset($isabovebusget) && $isabovebusget>0){

                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->where('pro_price', '>=', $buget)->whereIn('hall_type', $searchgender)->count();

                          }else{



                            $ischildthere[] = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $cid)->where('pro_price', '<=', $buget)->whereIn('hall_type', $searchgender)->count();

                              }





                            $hallsubcategory->ischild=$ischildthere;

                         }



                  }

               }

			

			 $value = $request->session()->get('searchdata');

            $getsubcategories = $hallsubcategory;

           // echo "<pre>";

		//print_r($getsubcategories);



        return view('shopbranchlist', compact('getsubcategories','bussinesstype','halltype','typeofhallid','category_id','shopid','isabovebusget'));

 

    }

   

}


<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Models;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Carbon;
use File;
use App\Reviewrating;
use App\Categorylist;
use Intervention\Image\ImageManagerStatic as Image; 
use Auth;
use Lang;
use App\ProductsMerchant;
use App\City;
use App\HallOffer; 
use App\NmproductOptionValue;
use App\NmProductGallery;
class MerchantRosesController extends Controller
{
      public function __construct(){
            parent::__construct();
            // set admin Panel language
            $this->middleware(function ($request, $next) {
                        $this->setLanguageLocaleMerchant();
                        Session::get('mer_lang_code');
                        return $next($request);
           });
     }


    public function rosesShopInfo(Request $request)
     { 
        if (Session::has('merchantid')) 
             {
               
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->itemid;
               $city = City::where('ci_con_id',10)->get();
               $fetchdata = Categorylist::where('mc_id',$itemid)->where('vendor_id',$mer_id)->first();

               $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');
                $status = $request->status;
                $search = $request->search;
                $productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
                if($status!='')
                {
                     $productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->where('pro_status',$status)->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
                }
                if($search !='')
                {
                     $productdata = ProductsMerchant::where('pro_mc_id',$id)->where('pro_mr_id',$mer_id)->where('pro_title','like','%'.$search.'%')->orderBy('pro_id','desc')->paginate($getPagelimit)->appends(request()->query());
                }
                return view('sitemerchant.roses.roses-shop-info', compact('merchantheader','merchantfooter','id','itemid','fetchdata','city'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
      }
 
	 
   
   
    public function rosesPictures(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $merid  = Session::get('merchantid');
                            $parentid = $request->id;
                              $itemid = $request->itemid;
                            $this->setLanguageLocaleMerchant();
                            $mer_id              = Session::get('merchantid');             
                            $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                            $merchantfooter     = view('sitemerchant.includes.merchant_footer');           
                            
                            $getDbC = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->count();
                            
                            $getVideos = DB::table('nm_category')->where('mc_id',$itemid)->where('vendor_id',$merid)->first();
                            if($getDbC >=1)
                            {
                          
                            $getDb = DB::table('nm_category_gallery')->where('vendor_id', $merid)->where('category_id', $itemid)->get();

                            }
                            else
                            {
                            $getDb = '';    
                            }

                          return view('sitemerchant.roses.roses-picture', compact('merchantheader','merchantfooter','getCatlists','GetAlreadyServices','getDb','getDbC','getVideos','parentid','itemid')); 

            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
    public function rosesPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $mer_id   = Session::get('merchantid');
              $id = $request->id;
              $itemid = $request->itemid;
              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $getPagelimit = config('app.paginate');
              $status = $request->status;
              $search = $request->search;
              $alldata        = ProductsMerchant::where('pro_mc_id',$itemid)->where('packege','yes')->where('pro_mr_id',$mer_id);
              if($status != '')
              {
              $alldata = $alldata->where('pro_status',$status);
              }
              if($search != '')
              {

              $alldata = $alldata->where('pro_title','like', '%'.$search.'%');
              }
              $alldata = $alldata->orderBy('pro_id','desc')->paginate($getPagelimit);
              return view('sitemerchant.roses.roses-package', compact('merchantheader','merchantfooter','alldata')); 
 
              
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	
	 
	 
    public function rosesAddPackage(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $mer_id   = Session::get('merchantid');
              $id = $request->id;
              $itemid = $request->itemid;
              $autoid = $request->autoid; 
              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              $getinfos   = ProductsMerchant::where('pro_mc_id',$itemid)->where('packege','yes')->where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first(); 
              $simpleProductsMerchant  = ProductsMerchant::where('pro_mc_id',$itemid)->where('packege','no')->where('pro_mr_id',$mer_id)->orderBy('pro_title','asc')->get();

              $GetrelatedProductsMerchant =  DB::table('nm_product_to_packeg')->where('packege_id',$autoid)->get();
              $ChkrelArray = array();
              foreach($GetrelatedProductsMerchant as $vals)
              {
                array_push($ChkrelArray, $vals->item_id);
              }


              
              $GetrelatedWTypesC =  DB::table('nm_product_option_value')->where('product_id',$autoid)->where('product_option_id','25')->count();
              if( $GetrelatedWTypesC >=1)
              {
              $GetrelatedWTypes =  DB::table('nm_product_option_value')->where('product_id',$autoid)->where('product_option_id','25')->first();
              $GetrelatedWType = $GetrelatedWTypes->parent_id;
              $GetrelatedWDesigns =  DB::table('nm_product_option_value')->where('product_id',$autoid)->where('product_option_id','26')->first();
              $GetrelatedWDesign = $GetrelatedWDesigns->parent_id;
              }
              else
              {
              $GetrelatedWType = 0;
              $GetrelatedWDesign = 0;
              }


              return view('sitemerchant.roses.roses-add-package', compact('merchantheader','merchantfooter','id','getinfos','simpleProductsMerchant','ChkrelArray','GetrelatedWType','GetrelatedWDesign'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		  
	 
	 
    public function rosesAddType(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->itemid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $productGallery = NmProductGallery::where('vendor_id',$mer_id)->where('product_id',$autoid)->get(); 
 
           $getinfos   = ProductsMerchant::where('pro_mc_id',$itemid)->where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first(); return view('sitemerchant.roses.roses-add-type', compact('merchantheader','merchantfooter','id','getinfos','productGallery'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }		  
	 	 
    
	 
    public function rosesType(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $itemid = $request->itemid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                $getPagelimit = config('app.paginate');
                $status = $request->status;
                $search = $request->search;
                 $alldata        = ProductsMerchant::where('pro_mc_id',$itemid)->where('packege','no')->where('pro_mr_id',$mer_id);
                if($status != '')
                 {
                  $alldata = $alldata->where('pro_status',$status);
                  }
                  if($search != '')
                  {
                      
                  $alldata = $alldata->where('pro_title','like', '%'.$search.'%');
                 }
 
                $alldata = $alldata->orderBy('pro_id','desc')->paginate($getPagelimit);


                return view('sitemerchant.roses.roses-type', compact('merchantheader','merchantfooter','alldata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }
	 
   
	  public function rosesOrder(Request $request)
     {
        if(Session::has('merchantid')) 
        {
          $subproducttype = 'roses';
          $mer_id         = Session::get('merchantid');
          $id             = $request->id;
          $sid            = $request->itemid;
          $searchkeyword  = $request->searchkeyword;
          $date_to        = $request->date_to;
          $End_Date       = $request->from_to;
          $order_days     = $request->order_days;
          $status         = $request->status;                
          $serachfirstfrm = $request->serachfirstfrm;

               $getorderedproducts = DB::table('nm_order_product')->where('product_type','occasion')->where('product_sub_type','roses');

                  if($searchkeyword!=''){
                  $getorderedproducts = $getorderedproducts->where('order_id',$searchkeyword);
                  }

                  if($status!=''){
                  $getorderedproducts = $getorderedproducts->where('status',$status);
                  } 

                  if($date_to!='' && $End_Date!=''){
                  $getorderedproducts = $getorderedproducts->whereDate('created_at', '>=', $date_to)->whereDate('created_at', '<=', $End_Date);
                  } 

                $getorderedproducts = $getorderedproducts->where('merchant_id',$mer_id)->groupBy('order_id')->orderBy('created_at','DESC')->selectRaw('*,sum(nm_order_product.total_price) as sum')->get();
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
                
 
                return view('sitemerchant.roses.roses-order', compact('merchantheader','merchantfooter','id','getCities','getSinger','sid','getorderedproducts','searchkeyword','mer_id'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }	 
	  
        //ORDER DETAILS

         public function getRoseorderdetail(Request $request)
         {
           if(Session::has('merchantid')) 
            {
                $mer_id         = Session::get('merchantid');
                $id             = $request->id;
                $itemid         = $request->itemid;
                $opid           = $request->opid;
                $prodid         = $request->prodid;
                $cusid          = $request->cusid;
                $oid            = $request->oid;
                $productdata = DB::table('nm_order_product')->where('product_type','occasion')->where('order_id',$prodid)->where('product_sub_type','roses')->where('cus_id',$cusid)->where('order_id',$prodid)->where('merchant_id',$mer_id)->orderBy('created_at','DESC')->get();

   foreach($productdata as $value)
         {            

          $getShippingC = DB::table('nm_shipping')->where('ship_order_id',$prodid)->where('cart_sub_type','roses')->count();
          $value->shippingMethod = 'N/A';
          $value->shippingPrice = '0';
          if($getShippingC >=1)
          { 
          $getShipping = DB::table('nm_shipping')->where('ship_order_id',$prodid)->where('cart_sub_type','roses')->first();
          $shipping_type = $getShipping->shipping_type;
          if($shipping_type == 1){ $shipping_type = 'Aramex';}else { $shipping_type = 'Pick Logistics'; }
          $shipping_price =  $getShipping->shipping_price;
          $value->shippingMethod = $shipping_type;
          $value->shippingPrice = $shipping_price;
          }
         }
 


                
                 
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
             
                return view('sitemerchant.roses.roseorderdetail', compact('merchantheader','merchantfooter','id','itemid','opid','prodid','cusid','oid','productdata'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
      }
     


     public function rosesReview(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $hid = $request->itemid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
               $reviewrating = Reviewrating::where('vandor_id',$mer_id)->where('shop_id',$hid)->orderBy('created_at','desc')->paginate($getPagelimit)->appends(request()->query());
 
                return view('sitemerchant.roses.roses-reviews-and-comments', compact('merchantheader','merchantfooter','id','reviewrating'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
	  public function rosesOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
              $getPagelimit = config('app.paginate');
              $id = $request->id;
              $itemid = $request->itemid;
         
              $status = $request->status;
              $search = $request->search;
              $mer_id   = Session::get('merchantid');


              $hallofferlist =  HallOffer::where('vendor_id',$mer_id)->where('pro_id',$itemid);
              if($status!='')
              {
              $hallofferlist = $hallofferlist->where('status',$status);
              }
              if($search !='')
              {
              $mer_selected_lang_code = Session::get('mer_lang_code'); 
              if($mer_selected_lang_code !='en')
              {
              $hallofferlist = $hallofferlist->where('title_ar','LIKE','%' .$search.'%');
              }
              else
              {
              $hallofferlist = $hallofferlist->where('title','LIKE','%' .$search.'%');

              }
              }   
              $hallofferlist =  $hallofferlist->orderBy('id','DESC')->paginate($getPagelimit);

              $this->setLanguageLocaleMerchant();
              $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
              $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              return view('sitemerchant.roses.roses-offer', compact('merchantheader','merchantfooter','hallofferlist','status','autoid','itemid','id','search'));   
  
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
	  public function rosesAddOffer(Request $request)
     {
        if (Session::has('merchantid')) 
             {
               $mer_id   = Session::get('merchantid');
                $id = $request->id;
                $getPagelimit = config('app.paginate');
                $sid = $request->itemid;
                $autoid = $request->autoid;
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');          
                $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first(); 
 
                return view('sitemerchant.roses.roses-add-offer', compact('merchantheader','merchantfooter','id','sid','autoid','reviewrating','categorysave')); 
 
                      
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   	
   
    

 //Store shop Info
            public function storeRosesShopInfo(Request $request)

            {
                $merid  = Session::get('merchantid');
                $parent_id = $request->parent_id;
                $itemid = $request->itemid;
               if($itemid){
                  $savebranch =Categorylist::where('mc_id',$itemid)->first();
                } else { 
                 $savebranch = new Categorylist;
               }
           if($request->file('branchimage')){ 
                  $file = $request->branchimage;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                   $thumb_width = 220;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/roses' . '/'. $thumbName);
                  $file->move('uploadimage/roses/', $Image);
                  $savebranch->mc_img = url('').'/uploadimage/roses/'.$thumbName;
                }
                //Addressimage
                if($request->file('address_image')){ 
                  $file = $request->address_image;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/roses' . '/'. $thumbName);
                  $file->move('uploadimage/roses/', $Image);
                  $savebranch->address_image = url('').'/uploadimage/roses/'.$thumbName;
                }
              //Termaand condition
               if(!empty($request->mc_tnc)){ 
                  $file = $request->mc_tnc;
                  $orgname =  $file->getClientOriginalName();
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_c = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_c);                      
                  $Cname =   url('').'/public/assets/storeimage/'.$filename_c;
                  $savebranch->terms_conditions =$Cname;
                  
               }
               else {
                    $orgname = $request->tmcvalue; 
                }

//Termaand condition
               if(!empty($request->mc_tnc_ar)){ 
                  $file = $request->mc_tnc_ar;
                    $orgname_ar =  $file->getClientOriginalName(); 
                  $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName());                
                  $extension = $file->getClientOriginalExtension();
                  $filename_ca = rand(11111,99999).'_'.time().'.'.$extension;                       
                  $file->move('./public/assets/storeimage/', $filename_ca);                      
                  $Cname_ar =   url('').'/public/assets/storeimage/'.$filename_ca;
                  $savebranch->terms_conditions_ar =$Cname_ar;
                  $savebranch ->terms_condition_name_ar = $orgname_ar;
               }
               else {
                    $orgname_ar = $request->tmcvalue_ar; 
                }
 $savebranch ->longitude =  $request->longitude;
                  $savebranch ->latitude =  $request->latitude;

                  $savebranch ->mc_name = $request->mc_name;
                  $savebranch ->mc_name_ar = $request->mc_name_ar;
                  $savebranch ->vendor_id = $merid;
                  $savebranch ->google_map_address = $request->google_map_address;
                  $savebranch ->address = $request->address;
                  $savebranch ->address_ar = $request->address_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->parent_id = $parent_id;
                  $savebranch ->mc_discription = $request->about;
                  $savebranch ->mc_discription_ar = $request->about_ar;
                  $savebranch ->city_id = $request->city_id;
                  $savebranch ->terms_condition_name = $orgname;
               
                 $savebranch ->mc_status = $request->mc_status;
                  $serviceabailable =  $request->service_available;
                  
                 // echo $serviceabailable;die;
                 
                if($savebranch->save()){
                  $id = $request->parent_id;

            if($itemid==''){
            $itemid = $savebranch->mc_id;
            }
            if (\Config::get('app.locale') == 'ar'){
             Session::flash('message', "تم حفظ معلومات المتجر بنجاح");
             }
             else
             {
             Session::flash('message', "Shop information successfully saved");
             }

                 return redirect()->route('roses-shop-info',['id' => $id,'itmemid' =>$itemid]);
                }   
            }

      //End shop info
   
 //Store picture and  video url
      public function storePictureVideourl(Request $request)
      {
          
         if (Session::has('merchantid')) 
                {

                   

                    $merid  = Session::get('merchantid');
                    $this->setLanguageLocaleMerchant();
                    $mer_id              = Session::get('merchantid');  
                    $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                    $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              
                    //Insert images in folder
                          $catid = $request->parent_id;
                          $sid = $request->itemid;

                    if($files=$request->file('image')){
                    foreach($files as $file){
                    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
                    //$file->move('hallpics',$name); 
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
                    //$file->move('hallpics', $fileName);               
                    $imageRealPath  =   $file->getRealPath();
                    $thumbName      =   'thumb_'. $fileName;       
                    $img = Image::make($imageRealPath); // use this if you want facade style code
                     $thumb_width = 100;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   $img->resize($thumb_width,$thumb_height);

                  
                    $img->save('uploadimage/roses/gallery' . '/'. $thumbName);
                    $file->move('uploadimage/roses/gallery/', $fileName);
                    $shop_Img = url('/').'/uploadimage/roses/gallery/'.$thumbName; 
                       
                    /*Insert data to db*/
                    DB::table('nm_category_gallery')->insert( [
                    'image'=>   $shop_Img,
                    'category_id' =>$sid,
                    'vendor_id' => $mer_id,
                    ]);
                   }  

                 } 
                   
                    $youtubevideoa = str_replace('youtu.be','www.youtube.com/embed',$request->youtubevideo);
                    $mc_video_description = $request->about_video;
                    $mc_video_description_ar = $request->about_video_ar;
                     /*Update data to db*/
                    DB::table('nm_category')->where('mc_id',$sid)->where('vendor_id',$mer_id)->update( ['mc_video_url'=>$youtubevideoa,'mc_video_description'=> $mc_video_description,'mc_video_description_ar'=> $mc_video_description_ar]);
 
                      // language for display message //
                    if($mc_video_description != '' || $mc_video_description_ar != '' || $youtubevideoa != '' || $request->file('image') !='')
                      {
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تمت إضافة الخدمة");
                    }
                    else
                    {
                    Session::flash('message', "Record saved successfully");
                    }
                  }
                    // language for display message //   
                    return Redirect::back();      
                } else {
                        return Redirect::to('sitemerchant');
                }
      }
     
    public function storerosestype(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');                  
                 $title = $request->title; 
                 $title_ar = $request->title_ar; 
                 $price = $request->price; 
                 $qty = $request->qty;                  
                 $discount = $request->discount; 
                  if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                     }
                    $id = $request->id; 
                    $itemid = $request->itemid; 
                    $autoid = $request->autoid; 


 
                if($autoid=='')
                {
                $savedata = new ProductsMerchant;
                }
                else
                {
                $savedata = ProductsMerchant::where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();
                }


            if($request->file('img')){ 
                  $file = $request->img;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 254;
                  list($width,$height) = getimagesize($imageRealPath);
                  $thumb_height = ($thumb_width/$width) * $height;
                  $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/roses/rosetype' . '/'. $thumbName);
                  $file->move('uploadimage/roses/rosetype/', $Image);
                  $savedata->pro_Img = url('').'/uploadimage/roses/rosetype/'.$thumbName;
                }

                  $savedata->pro_title = $title;
                  $savedata->pro_title_ar = $title_ar;
                  $savedata->pro_discount_percentage = $discount;
                  $savedata->pro_qty = $qty;
                  $savedata->pro_price = $price;
                  $savedata->pro_mc_id = $itemid;
                  $savedata->pro_mr_id = $mer_id;
                  $savedata->pro_status = 1;
                  $savedata->pro_disprice = $discountprice;

 


                  $savedata->pro_desc =  $request->description;
                 $savedata->pro_desc_ar = $request->description_ar;
                    $savedata->attribute_id = '51';
                  $savedata->save(); 


$getProID = $savedata->pro_id;


// FOR PRODUCT MULTIPLE IMAGES UPLOAD //
    $files=$request->file('image');
    $newFiles = array();
    $merid  = Session::get('merchantid');
    if($files=$request->file('image')){
    foreach($files as $key=>$val){
    array_push($newFiles, $key);
    }

    $privius=$request->privius;
    $newFilesMatch = array();
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    array_push($newFilesMatch, $key);
    }  
    }

    $CheckInfo = array_intersect($newFiles, $newFilesMatch);
    if(isset($privius) && $privius!='')
    {
    foreach($privius as $key=>$val){
    if(in_array($key, $CheckInfo))
    {
    DB::table('nm_product_gallery')->where('id',$val)->where('vendor_id',$merid)->delete(); 
    }
    }
    } 
    foreach($files as $file){
    $name=str_replace(' ','_', time().'_'.$file->getClientOriginalName()); 
    $extension = $file->getClientOriginalExtension();
    $fileName = rand(11111,99999).'_'.time().'.'.$extension;
    $imageRealPath  =   $file->getRealPath();
    $thumbName      =   'thumb_'. $fileName;       
    $img = Image::make($imageRealPath); // use this if you want facade style code
    $thumb_width = 254;
    list($width,$height) = getimagesize($imageRealPath);
    $thumb_height = ($thumb_width/$width) * $height;
    $img->resize($thumb_width,$thumb_height);                  
    $img->save('uploadimage/roses/rosetype' . '/'. $thumbName);
    $file->move('uploadimage/roses/rosetype/', $fileName);
    $shop_Img = url('/').'/uploadimage/roses/rosetype/'.$thumbName; 
    /*Insert data to db*/
    DB::table('nm_product_gallery')->insert( [
    'image'=>   $shop_Img,
    'product_id' =>$getProID,
    'status' => 1,
    'vendor_id' => $merid,
    ]);
    }  
        } 


 
// FOR PRODUCT MULTIPLE IMAGES UPLOAD END //




 
              if (\Config::get('app.locale') == 'ar'){
              Session::flash('message', "نوع روز المحفوظة بنجاح ");
              }
              else
              {
              Session::flash('message', "Rose type successfully saved");
              }

              return redirect()->route('roses-type',['id' => $id,'itemid' =>$itemid]);
        
            } else {
                 return Redirect::to('sitemerchant');
            }
     }    
   

  public function storerosespackages(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                  $mer_id   = Session::get('merchantid');                  
                  $title = $request->title; 
                  $title_ar = $request->title_ar; 
                  $price = $request->price; 
                  $qty = $request->qty;                  
                  $discount = $request->discount; 
                  $id = $request->id; 
                  $itemid = $request->itemid; 
                  $autoid = $request->autoid; 
                  $pro_desc = $request->description;
                  $pro_desc_ar = $request->description_ar;

                   if($discount!='' && $discount!='0') {
                     $discountprice =   $price-($price * ($discount/100));
                    } else {
                       $discountprice = 0;

                     }


                  if($autoid=='')
                  {
                  $savedata = new ProductsMerchant;
                  }
                  else
                  {
                  $savedata = ProductsMerchant::where('pro_id',$autoid)->where('pro_mr_id',$mer_id)->first();
                  }
            if($request->file('img')){ 
                  $file = $request->img;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                  $img = Image::make($imageRealPath); // use this if you want facade style code
                  $thumb_width = 254;
                  list($width,$height) = getimagesize($imageRealPath);
                  $thumb_height = ($thumb_width/$width) * $height;
                  $img->resize($thumb_width,$thumb_height);
                  $img->save('uploadimage/roses/rosepackages' . '/'. $thumbName);
                  $file->move('uploadimage/roses/rosepackages/', $Image);
                  $savedata->pro_Img = url('').'/uploadimage/roses/rosepackages/'.$thumbName;
                }
                  if($autoid=='')
                  {
                  $savedata->pro_status = 1;
                  }
                  $savedata->pro_title = $title;
                  $savedata->attribute_id = '50';
                  $savedata->packege = 'yes';
                  $savedata->pro_title_ar = $title_ar;
                  $savedata->pro_discount_percentage = $discount;
                  $savedata->pro_qty = $qty;
                  $savedata->pro_price = $price;
                  $savedata->pro_mc_id = $itemid;
                  $savedata->pro_mr_id = $mer_id;
                  $savedata->option_id = '25,26'; 
                  $savedata->pro_disprice = $discountprice;
                  $savedata->pro_desc = $pro_desc;
                  $savedata->pro_desc_ar = $pro_desc_ar;

                  $savedata->save();
                  if($autoid=='')
                  {
                  $lastinsertedID = $savedata->pro_id; 
                  }
                  else
                  {
                   $lastinsertedID = $autoid;
                   DB::table('nm_product_to_packeg')->where('shop_id',  $itemid)->where('packege_id',  $lastinsertedID)->delete();
                  }
                $flowertype = $request->flowertype;
                foreach($flowertype as $vals)
                { 
                DB::table('nm_product_to_packeg')->insert(['packege_id' => $lastinsertedID, 'item_id' => $vals, 'shop_id' => $itemid]);
                }
                $wraptype = $request->wraptype;
                $wrapdesign = $request->wrapdesign;

                $WType = DB::table('nm_product_option_value')->where('id',$wraptype)->first();
                $WDesign =DB::table('nm_product_option_value')->where('id',$wrapdesign)->first();
               
                $optiontitle = $WType->option_title;
                $optionoption_title_ar = $WType->option_title_ar;
                $optionimage = $WType->image;
                $optionprice = $WType->price;

                $optiontitle1 = $WDesign->option_title;
                $optionoption_title_ar1 = $WDesign->option_title_ar;
                $optionimage1 = $WDesign->image;
                $optionprice1 = $WDesign->price;
                $OID = array('25','26');


                 DB::table('nm_product_option_value')->where('vandor_id',  $mer_id)->where('product_id',  $lastinsertedID)->whereIN('product_option_id',  $OID)->delete();

                DB::table('nm_product_option_value')->insert(['vandor_id' => $mer_id,'parent_id' => $wraptype, 'product_id' => $lastinsertedID, 'product_option_id' => '25', 'option_title' => $optiontitle, 'option_title_ar' => $optionoption_title_ar, 'image' => $optionimage, 'price' => $optionprice]);
                DB::table('nm_product_option_value')->insert(['vandor_id' => $mer_id,'parent_id' => $wrapdesign, 'product_id' => $lastinsertedID, 'product_option_id' => '26', 'option_title' => $optiontitle1, 'option_title_ar' => $optionoption_title_ar1, 'image' => $optionimage1, 'price' => $optionprice1]); 
              if (\Config::get('app.locale') == 'ar'){
              Session::flash('message', " تم حفظ الحزمة بنجاح ");
              }
              else
              {
              Session::flash('message', "Package successfully saved");
              }

              return redirect()->route('roses-package',['id' => $id,'itemid' =>$itemid]);
        
            } else {
                 return Redirect::to('sitemerchant');
            }
     }    
   

   //STORE OFFER
       public function storeMakeOffer(Request $request)
       {
            if (Session::has('merchantid')) 
        {      
        $mer_id   = Session::get('merchantid');
        $id = $request->parentid;
        $sid = $request->sid;
        $autoid = $request->autoid;
        if($autoid =='')
        {
        $categorysave = new HallOffer; 
        }
        else
        {
        $categorysave = HallOffer::where('id',$autoid)->where('vendor_id',$mer_id)->first();
        }
        $categorysave->vendor_id    = $mer_id; 
        $categorysave->title        =$request->title;
        $categorysave->title_ar    = $request->title_ar;
        $categorysave->start_date     = $request->Date_from;
        $categorysave->end_date    = $request->date_to;
        $categorysave->discount   = $request->discount;
        $categorysave->coupon   = $request->coupon;
        $categorysave->pro_id   = $sid;
        $categorysave->status    = 1;
        if (\Config::get('app.locale') == 'ar'){
        Session::flash('message', "تمت إضافة العرض بنجاح  ");
        }
        else
        {
        Session::flash('message', "Offer successfully saved");
        }
        $categorysave->save(); 
        return redirect()->route('roses-offer',['id' => $id,'itemid' => $sid]);
       } else {
      return Redirect::to('sitemerchant');
      } 
       }
     //END OFFER  


       public function wrappingType(Request $request)
       {
        if (Session::has('merchantid')) 
        { 
        $mer_id   = Session::get('merchantid');
        $id = $request->id;
        $getPagelimit = config('app.paginate');
        $sid = $request->itemid;
        $autoid = $request->autoid;
        $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
        $merchantfooter     = view('sitemerchant.includes.merchant_footer');   

        $wrappingtype = NmproductOptionValue::where('vandor_id',$mer_id)->where('product_option_id',25); 

 
          $status = $request->status;
          $search = $request->search;
              
              if($status!='')
              {
              $wrappingtype = $wrappingtype->where('status',$status);
              }

              if($search !='')
              {
              $mer_selected_lang_code = Session::get('mer_lang_code'); 
              if($mer_selected_lang_code !='en')
              {
              $wrappingtype = $wrappingtype->where('option_title_ar','LIKE','%' .$search.'%');
              }
              else
              {
              $wrappingtype = $wrappingtype->where('option_title','LIKE','%' .$search.'%');

              }
              }   

              $wrappingtype =  $wrappingtype->orderBy('id','DESC')->paginate($getPagelimit);



        return view('sitemerchant.roses.wrapping-type', compact('merchantheader','merchantfooter','id','sid','autoid','wrappingtype')); 
        } else {
        return Redirect::to('sitemerchant');
        }

       }

       public function wrappingDesign(Request $request)
       {
          if (Session::has('merchantid')) 
          { 
          $mer_id   = Session::get('merchantid');
          $id = $request->id;
          $getPagelimit = config('app.paginate');
          $sid = $request->itemid;
          $autoid = $request->autoid;
          $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
          $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 

          $wrappingdesign = NmproductOptionValue::where('vandor_id',$mer_id)->where('product_option_id',26); 

          $status = $request->status;
          $search = $request->search;
              
              if($status!='')
              {
              $wrappingdesign = $wrappingdesign->where('status',$status);
              }

              if($search !='')
              {
              $mer_selected_lang_code = Session::get('mer_lang_code'); 
              if($mer_selected_lang_code !='en')
              {
              $wrappingdesign = $wrappingdesign->where('option_title_ar','LIKE','%' .$search.'%');
              }
              else
              {
              $wrappingdesign = $wrappingdesign->where('option_title','LIKE','%' .$search.'%');

              }
              }   

              $wrappingdesign =  $wrappingdesign->orderBy('id','DESC')->paginate($getPagelimit);

 

          return view('sitemerchant.roses.wrapping-design', compact('merchantheader','merchantfooter','id','sid','autoid','wrappingdesign')); 
          } else {
          return Redirect::to('sitemerchant');
          }
        
       }


  public function addwrappingDesign(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
           
                $itemid  = $request->itemid;
                $autoid  = $request->autoid;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              

              $getAttr= DB::table('nm_product_option_value')->where('id',$autoid)->where('vandor_id',$mer_id)->first();  
                return view('sitemerchant.roses.roses-add-wraping-design', compact('merchantheader','merchantfooter','id','getAttr'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   
     

       public function addwrappingType(Request $request)
     {
        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
                $id = $request->id;
          
                $itemid  = $request->itemid;
                $autoid  = $request->autoid;

                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer'); 
              

              $getAttr= DB::table('nm_product_option_value')->where('id',$autoid)->where('vandor_id',$mer_id)->first();  
                return view('sitemerchant.roses.roses-add-wraping-type', compact('merchantheader','merchantfooter','id','getAttr'));       
            } else {
                 return Redirect::to('sitemerchant');
            }
     }   

      public function updateWrap(Request $request)
     {

        if (Session::has('merchantid')) 
             {
                $mer_id   = Session::get('merchantid');
 
                $id = $request->id;
         
                $itemid = $request->itemid;
                $auto = $request->autoid;
                $type = $request->type;
                $categoryname = $request->category_name;
                $categoryname_ar = $request->category_name_ar;
                 $price = $request->price;

                if($request->file('mc_img')){ 
                  $file = $request->mc_img;  
                  $merid  = Session::get('merchantid');
                  $extension = $file->getClientOriginalExtension();
                  $Image = rand(11111,99999).'_'.time().'.'.$extension;               
                  $imageRealPath  =   $file->getRealPath();
                  $thumbName      =   'thumb_'. $Image;       
                 $img = Image::make($imageRealPath); // use this if you want facade style code
                   $thumb_width = 150;
                    list($width,$height) = getimagesize($imageRealPath);
                   $thumb_height = ($thumb_width/$width) * $height;
                   //$img->resize($thumb_width,$thumb_height);
                    $img->resize(80,60);
                  $img->save('uploadimage/roses' . '/'. $thumbName);
                  $file->move('uploadimage/roses/', $Image);
                  $mc_img_url  = url('').'/uploadimage/roses/'.$thumbName;
                }
                else
                {
                  $mc_img_url = '';
                }

            
                $merchantheader     = view('sitemerchant.includes.merchant_header')->with("routemenu", "dashboard");
                $merchantfooter     = view('sitemerchant.includes.merchant_footer');
                if($type ==1)
                {
                  $product_option_id = 25;
                }
                else
                {
                  $product_option_id = 26;
                }
                if($auto =='')
                {
            // insert attributes into table
                  $status = 1;
                DB::table('nm_product_option_value')->insert( [
                'option_title' => $categoryname,
                'option_title_ar' => $categoryname_ar,
                'product_option_id'=> $product_option_id, 
                'vandor_id' =>$mer_id, 
                'status' =>$status, 
                'image' =>$mc_img_url, 
                'price' =>$price,            
                ]);
            // insert attributes into table


                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم حفظ الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully saved");
                    }
                    // language for display message // 

                } 
                else
                {
                    if($mc_img_url=='')
                    {
                    DB::table('nm_product_option_value')->where('id',$auto)->where('vandor_id',$mer_id)->update( ['option_title'=>  $categoryname,'option_title_ar'=>  $categoryname_ar,'price'=>  $price]);
                    }
                    else
                    {
                      DB::table('nm_product_option_value')->where('id',$auto)->where('vandor_id',$mer_id)->update( ['option_title'=>  $categoryname,'option_title_ar'=>  $categoryname_ar,'image'=>  $mc_img_url,'price'=>  $price]);
                    }
 

                    // language for display message //
                    if (\Config::get('app.locale') == 'ar'){
                    Session::flash('message', "تم تحديث الفئة بنجاح");
                    }
                    else
                    {
                    Session::flash('message', "Record successfully updated");
                    }
                    // language for display message // 

                }
                if( $type ==1)
                {
                return redirect()->route('wrapping-type', ['id' => $id,'itemid' =>$itemid]);
                }
                else
                {
                return redirect()->route('wrapping-design', ['id' => $id,'itemid' =>$itemid]);
                }
                

       
            } else {
                return Redirect::to('sitemerchant');
            }
     }



 } // end wrapping-design.blade.phpwrapping-type.blade



<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//
use App\BusinessOccasionType;
use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;

use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Internalfooditems;
use App\ProductCategory;
//------------wisitech code end -----------//
class AddtocartController extends Controller
{  
    public function __construct(){
        parent::__construct();
		
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function savecartitem(Request $request)
    {
 		$itemarray=array();
		$mid=array();
		$nm=array();
		$k=array();
		//echo "<pre>";
		
		//dd($request->all());
	
		$addeddishitemid=$request->addeddishid;
		$addedmainmenuids=$request->addedmainmenuid;
		$addedcontainerid=$request->addeddishcontainerid;
		$food_type=$request->food_type;
		//print_r($addedmainmenuids);
		
		/*foreach($addedmainmenuids as  $key => $mvalue){
		//print_r($mvalue);
				if($mvalue!=''){
					
				 	$menuid=$mvalue;
			 
				 if (!in_array($menuid, $mid))
					  {
					  		$mid[]=$mvalue;
							$nm[]=array('menu_id'=>$menuid);
					  }
				}
		}
		*/
		if(count($addedcontainerid)>0){

			 
		 foreach($addedcontainerid as  $key => $value){
		 			
				if($value!=''){
					$dishid=$request->dishid[$key];
				   //$itemqty=$request->itemqty[$key];
				    // $container_id=$value;

				    $itemqty=$request->addeddishcontainerqty[$key];
				     $container_id=$value;					
				 	 $menuid=$request->mainmenuid[$key];
				 	 $newcontainerids=explode(',',$container_id);
				 	 $newitemqty=explode(',',$itemqty);
				   foreach (array_unique($newcontainerids) as $key => $value) {
				 	 //foreach ($newcontainerids as $key => $value) {
							 if($value ==''){ continue;	}
						 $k[]=array('menu_id'=>$menuid,'dish'=>array('dish_id'=>$dishid,'container_id'=>$value,'qty'=>$newitemqty[$key]));
						
						}
				}
				
		} 
		}
// total_price

		$cartmenuitem['item']=$k;

		//dd($cartmenuitem['item']);
		
		 if(!Session::has('customerdata.token')) 
	    		{
          	return Redirect::to('login-signup');
			}  
	
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
	
		$userid=Session::get('customerdata.user_id');
		$productsessionid=Session::get('searchdata.prodid');

 


		$productid=$request->product_id;
 
		if($productid == '')
		{
		$productid=$productsessionid[0];
		}
       $occasiondate=Session::get('searchdata.occasiondate');
	 
		$lang=Session::get('lang_file');
		$basecategoryid=Session::get('searchdata.basecategoryid');
		$cid=Session::get('searchdata.catgeoryid');
		$categoryid=$cid[0];
		$scid=Session::get('searchdata.typeofhallid');
		$subcategoryid=$scid[0];
		$shid=Session::get('searchdata.shopid');

		$shopid=$request->shop_id;
		if($shopid == '')
		{
		$shopid=$shid[0];
		}


		
		$bid=Session::get('searchdata.branchid');
		


		$branchid=$request->branch_id;
		if($branchid == '')
		{
		$branchid=$bid[0];
		}


		
		$adoptedservices=Session::get('searchdata.hallchoosenservices');
		
		 $occasiontype=Session::get('searchdata.weddingoccasiontype');
		
		
		
		
		$Internalfooditemstype= new Internalfooditems();
		$ProductCategoryselected= new ProductCategory();
		  $mainmenuitemtype=$Internalfooditemstype->Getnm_internal_food_menutype($productid,$lang);	
		  $containerlistitem=$Internalfooditemstype->GetcontainerListByProduct($productid,$lang);	
		  ///////// get catgeory data///////////////
		  
	
		   $eventtype=$ProductCategoryselected->eventtypeoption($occasiontype,$lang);

		   if(count($eventtype) >=1)
		   {
       $otype=$eventtype[0]->title; 
		   }
		   else
		   {
		$otype='';    	
		   }
		  	  
		  $basecategory=$ProductCategoryselected->Getbasicfromcategory($basecategoryid,$lang);
		  $category=$ProductCategoryselected->Getbasicfromcategory($categoryid,$lang);
		  $subcategory=$ProductCategoryselected->Getbasicfromcategory($subcategoryid,$lang);
		  $shopname=$ProductCategoryselected->Getbasicfromcategory($shopid,$lang);
		   $branchname=$ProductCategoryselected->Getbasicfromcategory($branchid,$lang);
		    ////////////////// product info ////////////


		   $productinfo=$ProductCategoryselected->productinfo($productid,$lang);
		  
		   if($productinfo[0]->pro_disprice<1){
		   	 $newproductprice=$productinfo[0]->pro_price;
		   }else{
		   		$newproductprice=$productinfo[0]->pro_disprice;
		   }


		  //$mkbasecatgory=$basecategory[0]->mc_name.' &raquo; '.$eventtype[0]->title.' &raquo; '.$category[0]->mc_name.' &raquo; '.$subcategory[0]->mc_name.' &raquo; '.$shopname[0]->mc_name.' &raquo; '.$branchname[0]->mc_name;

		  $mkbasecatgory=$branchname[0]->mc_name;
		 
		  ///////////////product services//////////////
		  
		   $productservices=$ProductCategoryselected->productoption($adoptedservices,$lang);
		    $occasiontysspe=Session::get('searchdata');
 


$basecategoryid=Session::get('searchdata.basecategoryid');

if($basecategoryid ==1)
{
    $occasiondate=Session::get('searchdata.startdate');

		 
}
if($occasiondate)
{
	$heckAlreadyBooking = DB::table('nm_order_product')->where('product_type', '=', 'hall')->where('hall_booking_date', '=', $occasiondate)->where('product_id', $productid)->count();

			if($heckAlreadyBooking >=1)
			{

			$lang=Session::get('lang_file');
			if ($lang != 'en_lang') {
			return Redirect::back()->with('message','حجزت بالفعل مع التاريخ الحالي. يرجى تحديد تاريخ آخر');
			}
			else
			{
			return Redirect::back()->with('message','Already booked with the current date. Please select other date');	
			}


			}

}

  
		   /////////////////// add  to cart/////////////////////////
		  		   $product = Products::where('pro_id', '=', $productid)->first();
		  $baseprice=0;
		  $baseprice=$product->pro_netprice; 
		  
                if (!empty($product)) {
                $cart = Cart::where('user_id', '=', $userid)->first();
               
                 if (!empty($cart)) {
                   
                    $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', $productid)->delete();
                   $cart_option = CartOption::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', $productid)->delete();
                  $cart_option_value = CartOptionValue::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', $productid)->delete();
                  $cart_service_attribute = CartServiceAttribute::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->where('product_id', $productid)->delete();
                    $cart_food_dish = CartInternalFoodDish::where('cart_id', '=', $cart->id)->where('product_id', $productid)->delete();

                    
                }else{
                    $cart_data = array();
                    $cart_data['user_id'] = $userid;
                    $cart = Cart::create($cart_data); //cart entery
                }



                    $cart_product_data = array();
                    $cart_product_data['cart_type'] = $request->cart_type;
                    $cart_product_data['cart_id'] = $cart->id;
                    $cart_product_data['product_id'] = $productid;




                    $cart_product_data['total_price'] = $baseprice + $product->Insuranceamount;
                    $cart_product_data['status'] = 1;
					$cart_product_data['hall_booking_date'] = $occasiondate;
					$cart_product_data['shop_vendor_id'] = $product->pro_mr_id;
                    
                    $cart_product_data['pro_title'] = $product->pro_title;
                    $cart_product_data['pro_title_ar'] = $product->pro_title_ar;
                    $cart_product_data['pro_desc'] = $product->pro_desc;
                    $cart_product_data['pro_desc_ar'] = $product->pro_desc_ar;
                    $cart_product_data['pro_Img'] = $product->pro_Img;
                    $cart_product_data['pro_price'] = $product->pro_netprice;
                    $cart_product_data['insurance_amount'] = $product->Insuranceamount;
                    $cart_product_data['category_id'] = $product->pro_mc_id;
                    $cart_product_data['shop_id'] = $product->pro_mc_id;
                    $cart_product_data['review_type']   = 'shop';
                    $cart_product_data['cart_sub_type']   = 'hall';
                    $cart_product_data['merchant_id']   = $product->pro_mr_id;
                    $paidamount=($baseprice*25)/100;

                    $paidamountwithInsurance=$paidamount+$product->Insuranceamount;


                    $cart_product_data['paid_total_amount'] = $paidamountwithInsurance;



                    
                    $cart_product = CartProduct::create($cart_product_data); //cart product entry




                    if (count($adoptedservices)>0) {
                        $product_options = ProductOptionValue::select('product_option_id')->whereIn('id', $adoptedservices)->distinct()->get()->toArray();
                        $product_options_ids = array();
                        foreach($product_options as $key => $value) {
                            $product_options_ids[] = $value['product_option_id'];
                        }

                        foreach($product_options_ids as $key => $value) {
                            $cart_option_data = array();
                            $cart_option_data['cart_type'] = $request->cart_type;
                            $cart_option_data['product_option_id'] = $value;
                            $cart_option_data['cart_id'] = $cart->id;
                            $cart_option_data['product_id'] = $productid;
                            CartOption::create($cart_option_data); //cart option entry
                        }

                        $product_options_value = ProductOptionValue::whereIn('id', $adoptedservices)->get();

                        foreach($product_options_value as $value) {
                            $cart_option_value = array();
                            $cart_option_value['cart_type'] = $request->cart_type;
                            $cart_option_value['cart_id'] = $cart->id;
                            $cart_option_value['product_id'] = $productid;
                            $cart_option_value['product_option_id'] = $value->product_option_id;
                            $cart_option_value['product_option_value_id'] = $value->id;
                            $cart_option_value['value'] = $value->price;
                            $cart_option_value['status'] = 1;
                           $cart_value = CartOptionValue::create($cart_option_value); //cart option value entry
                           $cart_product = CartProduct::where('product_id','=',$cart_value->product_id)->where('cart_id','=',$cart->id)->first();

                           $npaidprice=($baseprice*25)/100;

                           $npaidamount=$npaidprice+$cart_value->value;
                           	if(isset($cart_product->paid_total_amount) && $cart_product->paid_total_amount>0){
                           		$totalpaidamount=$cart_product->paid_total_amount+$cart_value->value;
                           	}else{
                           $totalpaidamount=$npaidamount+$cart_product->insurance_amount;
								}
								
                           $total_price['paid_total_amount'] = $totalpaidamount;
                           $total_price['total_price'] = $cart_product->total_price + $cart_value->value;


                           $update_product = CartProduct::where('product_id', '=', $cart_product->product_id)->where('cart_id','=',$cart->id)->update($total_price);
                        }
                       
           		}
 
              //dd($cartmenuitem['item']);
				  if(count($cartmenuitem['item'])>0){

				 $disid = $request->disid;
				 $conId = $request->conId;
					$M=0;
                    foreach($cartmenuitem['item'] as $k=>$value){
					
                    $cart_attribute_data = array();
                    $cart_attribute_data['cart_type'] = $request->cart_type;
                    $cart_attribute_data['cart_id'] = $cart->id;
                    $cart_attribute_data['category_id'] = $product->pro_mc_id;
                    $cart_attribute_data['product_id'] = $productid;
                    $cart_attribute_data['attribute_id'] = $value['menu_id'];				
                    $cart_product = CartServiceAttribute::create($cart_attribute_data); 
                    
				
                 
                   //foreach($value['dish'] as $key=>$val){ 
                    
                    $container_price = InternalFoodContainerPrice::where('dish_id','=', $value['dish']['dish_id'])->where('container_id','=',$value['dish']['container_id'])->first();

                    if($container_price!='') {
                    $containertitle = DB::table('nm_internal_food_container')->where('id','=',$value['dish']['container_id'])->first();

                    $internalfood = DB::table('nm_internal_food_dish')->where('id','=',$value['dish']['dish_id'])->first();

                   

                  }



                    $cart_internal_food = array();
                    $cart_internal_food['cart_id'] = $cart->id;
                    $cart_internal_food['product_id'] = $productid;
                    $cart_internal_food['internal_food_menu_id'] = $value['menu_id'];


                      $cart_internal_food['internal_food_dish_id'] = $value['dish']['dish_id'];  
                      $cart_internal_food['container_id'] = $value['dish']['container_id'];   
				   $cart_internal_food['quantity'] = $value['dish']['qty'];
                    $cart_internal_food['status'] = 1;
                    $cart_internal_food['container_title'] = $containertitle->title;
                    $cart_internal_food['container_image'] = $containertitle->img;
                    $cart_internal_food['no_people'] 	   = $containertitle->no_people;

                    $cart_internal_food['dish_name'] 	   = $internalfood->dish_name;
                    $cart_internal_food['dish_name_ar']    = $internalfood->dish_name_ar;
                    $cart_internal_food['img']    		   = $internalfood->dish_image;

                   

                    	if($container_price->discount_price==0 || $container_price->discount_price==null){
                    			$cprice=$container_price->container_price;
                    	}else{
                    		$cprice=$container_price->discount_price;
                    	}
                         $cdprice=$value['dish']['qty']*$cprice;
                    	 $cart_internal_food['price']=$cdprice;
				 

					$cart_value = CartInternalFoodDish::create($cart_internal_food);




$cart_product = CartProduct::where('product_id','=',$cart_value->product_id)->first();
$total_price['paid_total_amount'] = $cart_product->paid_total_amount + $cprice * $cart_value->quantity;
$total_price['total_price'] = $cart_product->total_price + $cprice * $cart_value->quantity;
$update_product = CartProduct::where('product_id', '=', $cart_product->product_id)->update($total_price); 



         
                    
                   }
					

                    }

                 
 
			$conId = $request->conId;
			$disid = $request->disid;
			$validarr = array();



			//Check internal food start
			if(count($disid)>=1)
			{
				$i=0;
				foreach($disid as $dishs)
				{
					$chkC = CartInternalFoodDish::where('product_id',$productid)->where('container_id',$conId[$i])->where('internal_food_dish_id',$disid[$i])->where('cart_id',$cart->id)->count();
					if($chkC >=1)
					{
					$chk = CartInternalFoodDish::where('product_id',$productid)->where('container_id',$conId[$i])->where('internal_food_dish_id',$disid[$i])->where('cart_id',$cart->id)->first();
					array_push($validarr, $chk->id);
					}
					$i++;  
				}

 
				$FoodDish = CartInternalFoodDish::whereNotIn('id',$validarr)->where('cart_id',$cart->id)->where('product_id',$productid)->get();
				$Total = 0;
				foreach($FoodDish as $foods)
				{
				$Total = $Total + $foods->price;
				}			 
				$ptotal_price = array();
				$cart_products = CartProduct::where('product_id','=',$productid)->first();
 
				$ptotal_price['paid_total_amount'] = $cart_products->paid_total_amount - $Total;
				$ptotal_price['total_price'] = $cart_products->total_price - $Total;
				$update_product = CartProduct::where('product_id', '=', $cart_products->product_id)->update($ptotal_price);
				CartInternalFoodDish::whereNotIn('id',$validarr)->where('cart_id',$cart->id)->where('product_id',$productid)->delete();

			 }
 

			//Check internal food end
			 
				/////////////// get cart data from db ///////////////
				
				
				///////// get catgeory data///////////////
				$ProductCategoryselected= new ProductCategory();

				$occasiontypedata=$ProductCategoryselected->Getbasicfromcategory($occasiontype,$lang);
				//print_r($occasiontypedata);
				$basecategory=$ProductCategoryselected->Getbasicfromcategory($basecategoryid,$lang);
				$category=$ProductCategoryselected->Getbasicfromcategory($categoryid,$lang);
				$subcategory=$ProductCategoryselected->Getbasicfromcategory($subcategoryid,$lang);
				$shopname=$ProductCategoryselected->Getbasicfromcategory($shopid,$lang);
				$branchname=$ProductCategoryselected->Getbasicfromcategory($branchid,$lang);
				////////////////// product info ////////////
				
				if($food_type=='extrnalfood'){
		
					$lang=Session::get('lang_file');
					if ($lang != 'en_lang') {
					return redirect('foodcatgeory/7')->with('message','قاعة محفوظة ، يرجى تحديد الطعام');
					}
					else
					{
					return redirect('foodcatgeory/7')->with('message','Hall reserved, please select the food');	
					}

					
				}else{
				return redirect('mycart');

				} 

				$cart = Cart::where('user_id', '=', $userid)->first();
                $cart_pro = CartProduct::where('cart_type', '=', 'hall')->where('cart_id', '=', $cart->id)->first();
                $hall = Products::select('pro_id','pro_mc_id')->where('pro_id', '=', $cart_pro->product_id)->first();
                $branch_city = Category::select('mc_id','city_id')->where('mc_id', '=', $hall->pro_mc_id)->with('getCity:ci_id,ci_name_ar,ci_name')->first();
                if (!empty($cart) && $cart_pro->cart_type = 'hall') {
                    $cart_option_value = CartOptionValue::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'hall')->with('getProductOption')->get();

                    $internal_food = CartInternalFoodDish::where('cart_id', '=', $cart->id)->with('getServiceAttribute')->with('getFood')->with('getContainer')->get();

                    $hall = CartProduct::where('cart_id', '=', $cart->id)->where('cart_type', '=', 'hall')->with('getHall')->get();
                    
                    $cart_service = array();
                    $internal_foods = array();
                    $hall_data = array();
					//echo "<pre>";
					//print_r($cart_option_value);
					//die;
                    if (isset($cart_option_value)) { //get service
                        foreach($cart_option_value as $key => $value) {
                            if ($lang=='en_lang') {
                                $cart_service[$key]['option_title'] = $value->getProductOption['option_title'];
                            } else {
                                $cart_service[$key]['option_title'] = $value->getProductOption['option_title_ar'];
                            }

                            $cart_service[$key]['price'] = $value->getProductOption['price'];
                            $cart_service[$key]['no_person'] = $value->getProductOption['no_person'];
                            $cart_service[$key]['status'] = $value->getProductOption['status'];
                        }
                    }

                    if (isset($internal_food)) {
                        foreach($internal_food as $key => $value) { //get internal food

                        $container_price = InternalFoodContainerPrice::where('dish_id', '=', $value->internal_food_dish_id)->where('container_id','=',$value->container_id)->first();    
                           // print_r($container_price);

                            if ($lang=='en_lang') {
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title;
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title;
                            } else {
                                $internal_foods[$key]['menu_name'] = $value->getServiceAttribute[0]->attribute_title_ar;
                                $internal_foods[$key]['dish_name'] = $value->getFood[0]->dish_name_ar;
                                $internal_foods[$key]['container_title'] = $value->getContainer[0]->title_ar;
                            }
                            $internal_foods[$key]['dish_id'] = $value->getFood[0]->id;
                            $internal_foods[$key]['quantity'] = $internal_food[$key]->quantity;
                            $internal_foods[$key]['container_price'] = $container_price->container_price;
                            $internal_foods[$key]['dish_image'] = $value->getFood[0]->dish_image;
                            $internal_foods[$key]['status'] = $value->getFood[0]->status;

                            $internal_foods[$key]['no_people'] = $value->getContainer[0]->no_people;
							
                           if(!empty($container_price)) {
						 
                          		 $internal_foods[$key]['final_price'] = $container_price->container_price * $internal_food[$key]->quantity;

                           } 
                            
                        }
                    }
				

                    if (isset($hall)) {
                        foreach($hall as $key => $value) { //get hall
                            if ($lang=='en_lang') {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name;
                            } else {
                                $hall_data[$key]['pro_title'] = $value->getHall[0]->pro_title_ar;
                                $hall_data[$key]['pro_desc'] = $value->getHall[0]->pro_desc_ar;
                                $hall_data[$key]['city'] = $branch_city->getCity->ci_name_ar;
                            }
                            $hall_data[$key]['price'] = $value->getHall[0]->pro_price;
                            $hall_data[$key]['pro_qty'] = $value->getHall[0]->pro_qty;
                            $hall_data[$key]['pro_Img'] = $value->getHall[0]->pro_Img;
                            $hall_data[$key]['pro_status'] = $value->getHall[0]->pro_status;
                        }
                    }
					
					
                } 
 	
			 /////////////////////// end cart data from db ////////////  
				  
		     return view('addtocart', compact('mainmenuitemtype','mainmenuwithItemAndContainer','category_id','containerlistitem','mkbasecatgory','productinfo','newproductprice','productservices','otype','cart_service','internal_foods','hall_data'));
 
    }
   }
}

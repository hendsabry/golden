<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\Shoplisthall;
use App\Foodcategorylist;
//------------wisitech code end -----------//
class BeautycenterbranchController extends Controller
{  
    public function __construct(){
        parent::__construct();
         $this->middleware(function ($request, $next) {
                    $this->setLanguageLocaleFront();
                    return $next($request);
    });      
    }
     
    public function listbeautycenterbranch(Request $request,$halltype,$typeofhallid,$branchid)
    {
		$halltype=$halltype;
		$subcategory_id=$halltype;
		$city_id=Session::get('searchdata.cityid');
		if(Session::get('searchdata.basecategoryid')!=''){
		$basecategory_id=Session::get('searchdata.basecategoryid');
		}else {
		$basecategory_id=Session::get('searchdata.maincategoryid');
		}
		 $category_id=$typeofhallid;
		 $branchid=$branchid;
		 $typeofhallid=$typeofhallid;
		 $budget=Session::get('searchdata.budget');
		$lang=Session::get('lang_file');
		
		//$request->session()->push('searchdata.typeofhallid', $category_id);
		
		 $foodsessioninfo = array('maincategory_id' => $halltype, "category_id" => $category_id); 
				Session::put('fooddata', $foodsessioninfo);
		
		
		$Formbusinesstype= new Formbusinesstype();
		  $bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);	
			 $value = $request->session()->get('searchdata');
		       $getsubcategories = '';
			   
			   $foodShoplisthall= new Foodcategorylist();
			 
			 $foodshopunderbugetincity=$foodShoplisthall->GetfoodshoplistbycityNew($branchid,$city_id,$budget,$lang);

			 $foodmaincategory=$foodShoplisthall->Getfoodparentcategory($branchid,$lang);
			 
			 foreach($foodshopunderbugetincity as $branchlist) 
			 {
	 	       $isproduct = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_mc_id', '=', $branchlist->mc_id)->select('pro_id', 'pro_title', 'pro_Img','pro_price','pro_disprice')->count();
				$iscatproduct = DB::table('nm_services_attribute')->where('status', '=', 1)->where('services_id', '=', $branchlist->mc_id)->count();

				$isproductstaff = DB::table('nm_services_staff')->where('status', '=', 1)->where('shop_id', '=', $branchlist->mc_id)->orWhere('service_id', '=', $branchlist->mc_id)->count();

				

				if($iscatproduct>0){

				 		if($isproduct>0 && $isproductstaff>0)
				 		{
				 			$branchlist->isproduct=$isproduct;
				 		}
				 		else
				 		{
				 			$branchlist->isproduct='';
				 		}
		 		}else{
		 				$branchlist->isproduct='';
		 		}
           }
        return view('beautyandelegance.beautycenterbranch', compact('getsubcategories','bussinesstype','foodshopunderbugetincity','halltype','foodmaincategory','category_id','subcategory_id','branchid'));
 
    }
   
}

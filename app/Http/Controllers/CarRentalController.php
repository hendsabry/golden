<?php
namespace App\Http\Controllers;
use DB;
use Session;
use Helper;
use App\Http\Models;
use App\Register;
use App\Home;
use App\Footer;
use App\Settings;
use App\Merchant; 
use Lang;
use File;
use Intervention\Image\ImageManagerStatic as Image; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Categorylist;
//------------wisitech code atart----------//

use App\Category;
use App\Products;
use App\Gallery;
use App\ProductOption;
use App\ProductAttribute;
use App\ProductButtonType;
use App\ProductOptionValue;
use App\PoductReview;
use App\City;
use App\InternalFoodContainerPrice;
use App\InternalFoodContainer;
use App\InternalFoodMenu;
use App\InternalFoodDish;
use App\SaveCart;
use App\Cart;
use App\CartProduct;
use App\CartOption;
use App\CartOptionValue;
use App\CartInternalFoodDish;
use App\ServicesOrderOption;
use App\ServicesOrderOptionValue;
use App\CartServiceAttribute;
use App\Formbusinesstype;
use App\CartProductRent;
//------------wisitech code end -----------//

class CarRentalController extends Controller
{
	 public function __construct()
   {
      parent::__construct();
      $this->middleware(function ($request, $next){
                    $this->setLanguageLocaleFront();
                    return $next($request);
      });      
    }
	
	 public function carrental($id)
	 {	
	 	   $category_id = $id;
       //$getPagelimit = config('app.paginate');
        if(Session::has('customerdata.user_id')) 
        {
      	 	if(Session::get('searchdata.basecategoryid')!='')
      	 	{
                   $basecategory_id=Session::get('searchdata.basecategoryid');
          }
          else 
          {
               $basecategory_id=Session::get('searchdata.maincategoryid');
          }
      	 	$lang = Session::get('lang_file');
      		$Formbusinesstype= new Formbusinesstype();
          $city_id = Session::get('searchdata.cityid');
      		$bussinesstype=$Formbusinesstype->Getbussinesstype($basecategory_id,$lang);
          if(!empty($category_id)) 
          {
              if(isset($lang) && $lang == 'ar_lang') 
              {
                  $getsubcategories = Category::where('mc_status',1)->where('mer_activity',1)->where('parent_id',$category_id)->where('city_id',$city_id)->select('mc_id','mc_name_ar as mc_name','mc_img')->paginate(9)->appends(request()->query());
                  foreach($getsubcategories as $val) 
                  {
                    $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                    if($isproduct > 0)
                    {
                      $val->isproduct = $isproduct;
                    }
                    else
                    {
                      $val->isproduct = '';
                    }
                   }
              } 
              else 
              {                       
                  $getsubcategories = Category::where('mc_status',1)->where('mer_activity',1)->where('parent_id',$category_id)->where('city_id',$city_id)->select('mc_id','mc_name','mc_img')->paginate(9)->appends(request()->query());
                  foreach($getsubcategories as $val) 
                  {
                    $isproduct = DB::table('nm_product')->where('pro_status',1)->where('pro_mc_id',$val->mc_id)->count();
                    if($isproduct > 0)
                    {
                      $val->isproduct = $isproduct;
                    }
                    else
                    {
                      $val->isproduct = '';
                    }
                   }
              }
          }          
          return view('car-rental.carrentals',compact('getsubcategories','bussinesstype','category_id')); 
        } 
        else
        {
         return Redirect::to('login-signup');
        }      
     }
	
	 public function carrentaldetail($id,$sid)
     {          
        $category_id = $id;
        $subcat_id   = $sid;
        if(Session::has('customerdata.user_id')) 
        {
            if(Session::get('searchdata.basecategoryid')!='')
            {
                 $basecategory_id = Session::get('searchdata.basecategoryid');
            }
            else 
            {
                 $basecategory_id = Session::get('searchdata.maincategoryid');
            }
            $city_id = Session::get('searchdata.cityid');
            $lang = Session::get('lang_file');
            $Formbusinesstype = new Formbusinesstype();
            $bussinesstype = $Formbusinesstype->Getbussinesstype($basecategory_id,$lang);            
            if(!empty($subcat_id)) 
            {
                if($category_id != '' && $lang == 'ar_lang') 
                {
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name_ar as mc_name','mc_discription_ar as mc_discription','city_id','occasion_id','mc_img','mc_video_url','address_ar as address','mc_video_description_ar as mc_video_description','google_map_address','terms_conditions_ar as terms_conditions','longitude','latitude')->where('parent_id',$category_id)->where('mc_id',$subcat_id)->first();
                    $vendor_id = $vendorinfo->vendor_id;
                    $alldata = Products::where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_qty','>',0)->where('pro_mc_id',$subcat_id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc_ar as pro_desc','notes_ar as notes','pro_price','Insuranceamount','pro_disprice')->paginate(9)->appends(request()->query());
                } 
                else 
                {   
                    $vendorinfo = Category::select('mc_id','vendor_id','mc_name','mc_discription','city_id','occasion_id','mc_img','mc_video_url','address','mc_video_description','google_map_address','terms_conditions','longitude','latitude')->where('parent_id',$category_id)->where('mc_id',$subcat_id)->first(); 
                    $vendor_id = $vendorinfo->vendor_id;                   
                    $alldata = Products::where('pro_status',1)->where('pro_mr_id',$vendor_id)->where('pro_mc_id',$subcat_id)->where('pro_qty','>',0)->select('pro_id','pro_title','pro_Img','pro_mr_id','pro_mc_id','pro_status','pro_desc','notes','pro_price','Insuranceamount','pro_disprice')->paginate(9)->appends(request()->query());
                }
            }
            $allreview =   DB::table('nm_review')->where('shop_id',$subcat_id)->where('review_type','shop')->where('status',1)->get();
            $vendordetails = $vendorinfo;
            $productlist   = $alldata;
            return view('car-rental.carrentaldetail',compact('vendordetails','productlist','bussinesstype','category_id','subcat_id','allreview')); 
        } 
        else
        {
          return Redirect::to('login-signup');
        }      
     }

     function getCarRentalInfo(Request $request)
    {
        $id         = $request->product_id;
        $vendorId   = $request->vendor_id;
        $lang       = Session::get('lang_file');
        if($id != '' && $lang == 'ar_lang') 
        {
            $productdateshopinfo = DB::table('nm_product')->where('pro_status', 1)->where('pro_id', $id)->select('pro_id','pro_title_ar as pro_title','pro_Img','pro_desc_ar as pro_desc','pro_mr_id','pro_mc_id','pro_status','notes_ar as notes','pro_price','Insuranceamount','pro_disprice')->orderBy('pro_title')->get();   
        }
        else
        {
           $productdateshopinfo = DB::table('nm_product')->where('pro_status', '=', 1)->where('pro_id',$id)->select('pro_id','pro_title','pro_title_ar','pro_Img','pro_desc','pro_mr_id','pro_mc_id','pro_status','notes','pro_price','Insuranceamount','pro_disprice')->orderBy('pro_title')->get();       
        }

           $productatrcarmodel = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Car Model')->select('attribute_title','value')->first();
           if(isset($productatrcarmodel->value) && $productatrcarmodel->value!='')
           {             
             $productatrcarmodel1        = $productatrcarmodel->value;
           }
           else
           {
             $productatrcarmodel1        = 1;
           }
           $productattrpriceperkm = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Price Per KM')->select('attribute_title','value')->first();
           if(isset($productattrpriceperkm->value) && $productattrpriceperkm->value!='')
           {             
             $productattrpriceperkm2        = $productattrpriceperkm->value;
           }
           else
           {
             $productattrpriceperkm2        = 2;
           }

           $productattrservicedate = DB::table('nm_product_attribute')->where('product_id',$id)->where('vendor_id',$vendorId)->where('attribute_title','Service Date')->select('attribute_title','value')->first();
           if(isset($productattrservicedate->value) && $productattrservicedate->value!='')
           {             
              $productattrservicedate3 = $productattrservicedate->value;
           }
           else
           {
              $productattrservicedate3     = 3;
           }           
           
        $productdatesshopfullinfo = array('productdateshopinfo'=>$productdateshopinfo,'productatrcarmodel'=>$productatrcarmodel1,'productattrpriceperkm'=>$productattrpriceperkm2,'productattrservicedate'=>$productattrservicedate3);
        return response()->json($productdatesshopfullinfo);
    }
}
